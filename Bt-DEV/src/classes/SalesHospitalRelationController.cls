/**
 * 作者:Bill
 * 时间：2013-8-14
 * 说明：根据用户选中的角色去执行自动发送邮件给主管的Batch
**/
public class SalesHospitalRelationController {
	
	public string[] rolesName{get;set;}
	public boolean IsView{get;set;}
	
	public SalesHospitalRelationController()
	{
		IsView = true;
		rolesName = new string[]{};
	}

    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('SS','SS'));
        options.add(new SelectOption('ASM','ASM'));
        options.add(new SelectOption('DSM','DSM'));
		return options;
    }
    
    public void SalesHospitalRelation()
    { 
    	string sqlWhere = string.valueOf(rolesName);
    	ClsRemindSalesHospitalRelationBatch SaleBatch = new ClsRemindSalesHospitalRelationBatch();
    	SaleBatch.strWhere = sqlWhere;
        database.executeBatch(SaleBatch,10);
        IsView = false;
    	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM , '系统已经自动发送邮件，稍后会请通知主管查看邮箱') ;            
        ApexPages.addMessage(msg) ;
    }
    
    static testMethod void myUnitTest() {
    	SalesHospitalRelationController sale = new SalesHospitalRelationController();
    	sale.getItems();
    	sale.SalesHospitalRelation();
    }
}