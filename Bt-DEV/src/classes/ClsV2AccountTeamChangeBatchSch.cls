/**
 * Author : Bill
 * 每月1号凌晨，替换、新增操作后，根据产品类型修改对应数据的所有人
 * SP医院信息 ： 产品类型为SP，修改当前数据的所有人
 * IVT医院信息 ： 产品类型为IVT，修改当前数据的所有人
 * 记录类型为IN Project的业务机会重新共享给新代表
 * 挥发罐的所有人
 **/
public class ClsV2AccountTeamChangeBatchSch implements Schedulable{
	//global
	public void execute(SchedulableContext SC) 
	{
        ClsV2AccountTeamChangeBatch accountBatch = new ClsV2AccountTeamChangeBatch();
        Database.executeBatch(accountBatch, 10);
	}
}