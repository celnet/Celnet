/**
 * Henry
 *2013-10-11
 *病人拜访总表  列表页面控制类——测试类
 */
@isTest
private class PM_Test_VisitPatientDevController {

    static testMethod void myUnitTest() 
    {
        PM_Test_AllConfig.myUnitTest(); //公用方法，内涵必要的一些测试数据
        system.Test.startTest();
    	PM_Patient__c patient = new PM_Patient__c();
    	ApexPages.StandardController controller = new ApexPages.StandardController(patient);
    	//实例化控制类
    	PM_VisitPatientDevController vp = new PM_VisitPatientDevController(controller);
    	//初始化get部分字段
    	ApexPages.StandardSetController conset = vp.conset;
    	list<PM_ExportFieldsCommon.Patient> list_Patient = vp.list_Patient;
    	vp.first();
	    vp.last();
	    vp.previous();
	    vp.next();
    	//查询流程
    	list<Profile> list_Profile = [Select Id,Name From Profile Where Name = '系统管理员'];
        User user = new User();
        user.ProfileId = list_Profile[0].Id;
        user.Username = 'asfdasdf@acme.com';
        user.FirstName = 'Test';
        user.LastName = 'Test';
        user.Email = 'asdf@aa.com';
        user.Alias = 'asdf';
        user.EmailEncodingKey = 'UTF-8';
        user.TimeZoneSidKey = 'Asia/Shanghai';
        user.LanguageLocaleKey = 'zh_CN';
        user.LocaleSidKey = 'zh_CN';
        insert user;
	 	vp.Patient.Name = '张先生';
	 	vp.MobilePhone = '18611112222';
 	 	vp.Patient.PM_Current_Saler__c = user.Id;
        vp.Patient.PM_VisitFailCount__c = 1;
        vp.BigArea = '东一区';
	    vp.Province = '北京';
        vp.PatientProvince = '北京';
        vp.City = '北京市';
        vp.PatientCity = '北京市';
	    vp.County = '海淀区';
        vp.PatientCounty = '海淀区';
        vp.PatientStatus = 'Active';
        vp.Patient.PM_Payment__c = '自费';
        vp.Patient.PM_HighImpac__c = '是';
        vp.Patient.PM_IsStarPatient__c = '是';
        vp.Patient.PM_HDAndPD__c = '是';
        vp.Patient.PM_UsCoGoSameTime__c = '是';
    	vp.Patient.PM_VisitState__c = '成功';
    	vp.Patient.PM_Pdtreatment__c = 'APD';
	    vp.CreatedDate_Start = '2013-10-09';
	    vp.CreatedDate_End = '2013-10-09';
	    vp.InDate_Start = '2013-10-09';
	    vp.InDate_End = '2013-10-09';
	    vp.InHospital = '朝阳区第三医院';  
	    vp.Check();
	    //排序流程
	    vp.strOrder = 'desc';
	    vp.sortOrders();
	    vp.sortOrders();
	    vp.getItems(); 
	    //导出
	    for(PM_ExportFieldsCommon.Patient pa :vp.list_PatientExport)
	    {
	    	pa.IsExport = true;
	    }
	    vp.exportExcel();
	    system.Test.stopTest();
    }
}