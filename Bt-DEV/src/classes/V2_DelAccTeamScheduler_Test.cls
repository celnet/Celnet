/* Function: 
 * Author: Sunny
 * Date:2012-1-4
 */
@isTest
private class V2_DelAccTeamScheduler_Test {

    static testMethod void SchedulableTest() {
        system.Test.startTest() ;
        String jobId = System.schedule('testDelAccTeam',
			V2_DelAccTeamScheduler.CRON_EXP, 
		    new V2_DelAccTeamScheduler());
		         
		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
	        NextFireTime
	        FROM CronTrigger WHERE id = :jobId];
	    
	    System.assertEquals(V2_DelAccTeamScheduler.CRON_EXP, 
        	ct.CronExpression);
		
		System.assertEquals(0, ct.TimesTriggered);

		//System.assertEquals('2012-01-07 02:00:00', 
         //String.valueOf(ct.NextFireTime));
		
	    

		
        	
        system.Test.stopTest() ;
    }
}