/* Author: bill
 * 2013.9.25
 * 病人查询列表的排序公共类
 * 首先根据创建日期和上次拜访时间进行排序，然后根据插管日期
 * 在拜访周期2外的显示两个闹钟
 * 在拜访周期1外在周期2内的显示一个硇洲
 * 有闹钟的优先显示，两个闹钟的显示在最前面
 */
global class PM_PatientSortCommom {
    //排序方法
    
    PM_ExportFieldsCommon field = new PM_ExportFieldsCommon();
    
    //不同状态的病人的拜访周期
    private static map<string,CallCycleConfig__c> Config =  CallCycleConfig__c.getAll();
    integer ActivePatientCallCycle1 = integer.valueOf(Config.get('ActivePatientCallCycle').CallCycle1__c);
    integer ActivePatientCallCycle2 = integer.valueOf(Config.get('ActivePatientCallCycle').CallCycle2__c);
    integer NewPatientCallCycle1 = integer.valueOf(Config.get('NewPatientCallCycle').CallCycle1__c);
    integer NewPatientCallCycle2 = integer.valueOf(Config.get('NewPatientCallCycle').CallCycle2__c);
    integer APDPatientCallCycle1 = integer.valueOf(Config.get('APDPatientCallCycle').CallCycle1__c);
    integer APDPatientCallCycle2 = integer.valueOf(Config.get('APDPatientCallCycle').CallCycle2__c);
    
    public PM_PatientSortCommom()
    {
    }
    //排序
    public List<ComparableClass> PatientSort(list<PM_Patient__c> listpatients)
    {
    	List<ComparableClass> list_patient = PatientWrapperBeforeSort(listpatients);
    	list_patient.sort();
    	return list_patient;
    }
    public List<ComparableClass> PatientSortNormal(list<PM_Patient__c> listpatients)
    {
    	List<ComparableClass> list_patient = PatientWrapperBeforeSort(listpatients);
    	return list_patient;
    }
    //排序前先对内部类进行加工
    public List<ComparableClass> PatientWrapperBeforeSort(list<PM_Patient__c> listpatients)
    {
    	List<ComparableClass> list_patient = new List<ComparableClass>();
        for(PM_Patient__c p : listpatients)
        {
            PM_ExportFieldsCommon.Patient pa = new PM_ExportFieldsCommon.Patient();
            pa.patient = p;
            if(p.PM_Status__c == 'New' && p.PM_Pdtreatment__c!= 'APD')
            {
                pa.clockNum = field.judgeClockNum(p.PM_SortDate__c.daysBetween(date.Today()), NewPatientCallCycle1, NewPatientCallCycle2);
            }
            else if(p.PM_Status__c == 'Active'  && p.PM_Pdtreatment__c!= 'APD')
            {
                pa.clockNum = field.judgeClockNum(p.PM_SortDate__c.daysBetween(date.Today()), ActivePatientCallCycle1, ActivePatientCallCycle2);
            }
            else if(p.PM_Pdtreatment__c == 'APD')
            {
                pa.clockNum = field.judgeClockNum(p.PM_SortDate__c.daysBetween(date.Today()), APDPatientCallCycle1, APDPatientCallCycle2);
            }
            if(pa.clockNum == null) 
			{
				pa.clockNum = 0;
			}
            if(field.judgePhoneType(p).containsKey('prior1'))
            {
                pa.phone1 = field.judgePhoneType(p).get('prior1').phoneNumber;
                pa.priorType1 = field.judgePhoneType(p).get('prior1').phoneType;
            }
            if(field.judgePhoneType(p).containsKey('prior2'))
            {
                pa.phone2 = field.judgePhoneType(p).get('prior2').phoneNumber;
                pa.priorType2 = field.judgePhoneType(p).get('prior2').phoneType;
            }
            if(p.PM_VisitState__c == '失败' && p.PM_Status__c != 'Dropout'&& p.PM_Status__c != 'Unreachable')pa.color = '#D16247';
            if(p.PM_VisitState__c == '成功' && p.PM_Status__c != 'Dropout'&& p.PM_Status__c != 'Unreachable')pa.color = '#ccedff';
            pa.IsExport = false;
            pa.SortDate = p.PM_SortDate__c;
            pa.InDate = p.PM_InDate__c==null?Date.ValueOf('1000-1-1'):p.PM_InDate__c;
            list_patient.add(new ComparableClass(pa));
        }
        return list_patient;
    }
    
    global class ComparableClass implements Comparable
    {
        global PM_ExportFieldsCommon.Patient p{get;set;}
        global ComparableClass(PM_ExportFieldsCommon.Patient p)
        {
            this.p = p;
        }
        
        global Integer compareTo(Object compareTo) 
        {
            ComparableClass compareToEmp = (ComparableClass)compareTo;
            
            if (p.clockNum == compareToEmp.p.clockNum) 
            {
                if (p.SortDate > compareToEmp.p.SortDate)
                {
                    return 1;
                }
                else if (p.SortDate == compareToEmp.p.SortDate || system.Test.isRunningTest())
                {
                    if (p.InDate > compareToEmp.p.InDate)
                    {
                        return 1;
                    }
                    else if (p.InDate == compareToEmp.p.InDate)
                    {
                        return 0;
                    }
                    else
                    {
                        return -1;
                    }
                }
                else
                {
                    return -1;
                }
                return 0;
            }
            if(p.clockNum < compareToEmp.p.clockNum)
            {
                return 1;
            }
            return -1;        
        }
    }
}