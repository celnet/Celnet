/**
 * 作者:bill
 * date : 2013-8-3
 * discription:SalesReportImport的trigger的测试类
 */
@isTest
private class Test_Trigger_SalesReportImport {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Account acc = new Account();
        acc.Name = 'test2013-8-3';
        insert acc;
        
        SalesReport__c sr = new SalesReport__c();
        sr.ActualQty__c = 100;
        sr.TargetQty__c = 300;
        sr.Account__c = acc.Id;
        sr.Time__c = date.today();
        insert sr;
    }
}