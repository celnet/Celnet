/*
Author：Bill
Created on：2013-7-16
Description: 
 * 每月1号凌晨，自动处理替换、删除操作
*/
public class ClsSalesHospitalRelationBatchSchedule implements Schedulable {
	public void execute(SchedulableContext SC) 
	{
		ClsSalesHospitalRelationBatch relationBatch = new ClsSalesHospitalRelationBatch();
		Database.executeBatch(relationBatch, 10);
	}
	
	static testMethod void myUnitTest() {		
        system.test.startTest();
        string sch=DateTime.now().second()+' '+DateTime.now().minute()+' '+DateTime.now().hour()+' * * ?';
        ClsSalesHospitalRelationBatchSchedule sa = new ClsSalesHospitalRelationBatchSchedule();
        System.schedule('test', sch , sa);
        system.test.stopTest();
	}
}