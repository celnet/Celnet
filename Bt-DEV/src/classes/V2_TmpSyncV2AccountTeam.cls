public class V2_TmpSyncV2AccountTeam {
	/*Steps:
	1.backup the related data
	2.test & valid the record
	3.batch executing
	*/
	
	/**Related sObject and will backup:
	V2_Account_Team__c 
	AccountTeamMember
	*/
	/**Source:
	Select v.Id --记录ID
	, v.isTmpSync__c --是否已处理临时同步
	, v.V2_IsSyn__c --已同步
	, v.V2_Is_Delete__c --是否删除
	, v.V2_ApprovalStatus__c --审批状态
	, v.V2_Account__c --关联客户
	, v.V2_AccountPermissions__c --客户访问权限
	, v.V2_OppPermissions__c --业务机会访问权限
	, v.V2_CasePermissions__c --个案访问权限
	, v.V2_Role__c --角色
	, v.V2_Effective_Year__c --生效年份
	, v.V2_Effective_Month__c --生效月份
	, v.V2_Effective_NewYear__c --新生效年份
	, v.V2_Effective_NewMonth__c --新生效月份
	, v.V2_Delete_Year__c --失效年份
	, v.V2_Delete_Month__c --失效月份
	, v.V2_BatchOperate__c --操作类型
	From V2_Account_Team__c v
	
	Conditions:
	1.V2_ApprovalStatus__c=='审批通过'
	2.V2_Is_Delete__c==false
	3.isTmpSync__c=false
	
	Logic: 1 AND 2 AND 3
	
	Completed:
	Update: isTmpSync__c=true, V2_IsSyn__c=true
	*/
	
	/**Target:
	Select a.Id --记录ID	
	, a.UserId --用户ID
	, a.TeamMemberRole --成员角色
	, a.AccountId --关联客户
	, a.AccountAccessLevel --客户访问权限
	From AccountTeamMember a
	
	*/
	
	/**Data Mapping:
	Source							Target
	V2_Account__c					AccountId
	V2_Role__c						TeamMemberRole
	V2_AccountPermissions__c		AccountAccessLevel
	*/
	
	/**Question:
	Q1.是否考虑(新)生效年月和失效年月,可能存在失效年月过期货失效年月已到但没有标记删除
	A1.数据到过期的话应该会标记未删除,所以不用考虑
	Q2.如果销售医院关系中角色为空,客户小组成员的角色设置为何值
	A2.保持空
	*/
	
	//每次批量执行记录数,对应1家医院,如果值为10,也可能只对应一家医院
	final Integer BATCH_COUNT_EVERYTIME=100;//大概可能关联上10家医院
	
	List<String> debugMsgs=new List<String>();
	
	//根据此来获取医院数据,然后再根据医院获取batchATs
	List<V2_Account_Team__c> batchATs_tmp=new List<V2_Account_Team__c>();
	List<Account> batchAccs_tmp=new List<Account>();//batchATs_tmp对应的医院
	Set<ID> batchAccIds_tmp=new Set<ID>();//batchATs_tmp对应的医院ID
	
	//根据此来同步对应的真正的有效的符合同步要求的销售医院关系数据
	List<V2_Account_Team__c> validBatchATs=new List<V2_Account_Team__c>();
	
	//获取batchATs_tmp关联医院[batchAccIds_tmp]的所有客户小组成员,将要清除并重建
	List<AccountTeamMember> batchATMs_tmp=new List<AccountTeamMember>();
	
	Datetime currentDT=System.now();
	/*有很多数据未经过审批,但已同步到客户小组.
	例如:001200000044kmRAAQ
	*/
	void check0(){
		
	}
	
	//检查尚未到生效日期的记录
	void check1(){
		List<V2_Account_Team__c> t=new List<V2_Account_Team__c>();
		t = [Select v.Id
			, v.name
			, v.V2_IsSyn__c
			, v.isTmpSync__c
			, v.V2_Is_Delete__c
			, v.V2_ApprovalStatus__c
			, v.V2_Account__c
			, v.V2_Account__r.name
			, v.V2_AccountPermissions__c
			, v.V2_OppPermissions__c
			, v.V2_CasePermissions__c
			, v.V2_Role__c
			, v.V2_Effective_Year__c
			, v.V2_Effective_Month__c
			, v.V2_Effective_NewYear__c
			, v.V2_Effective_NewMonth__c
			, v.V2_Delete_Year__c
			, v.V2_Delete_Month__c
			, v.V2_LastAccessDate__c
			From V2_Account_Team__c v
			Where
				v.V2_Is_Delete__c=false
				AND v.V2_LastAccessDate__c>:currentDT.date()];
		String exaId='无';
		if(t.size()>0)exaId=t[0].id;
		debugMsgs.add('*****尚未到生效日期的记录,生效日期在['+currentDT.date()+']之后, 未标记删除的记录数:'+t.size()+', 如:'+exaId);
	}
	//检查已到期但未标记删除的记录
	void check2(){
		List<V2_Account_Team__c> t=new List<V2_Account_Team__c>();
		t = [Select v.Id
			, v.name
			, v.V2_IsSyn__c
			, v.isTmpSync__c
			, v.V2_Is_Delete__c
			, v.V2_ApprovalStatus__c
			, v.V2_Account__c
			, v.V2_Account__r.name
			, v.V2_AccountPermissions__c
			, v.V2_OppPermissions__c
			, v.V2_CasePermissions__c
			, v.V2_Role__c
			, v.V2_Effective_Year__c
			, v.V2_Effective_Month__c
			, v.V2_Effective_NewYear__c
			, v.V2_Effective_NewMonth__c
			, v.V2_Delete_Year__c
			, v.V2_Delete_Month__c
			, v.V2_LastAccessDate__c
			From V2_Account_Team__c v
			Where 
				v.V2_Delete_Year__c!=null
				AND v.V2_Delete_Month__c!=null
				AND v.V2_Delete_Year__c<= :String.valueOf(currentDT.year())
				AND v.V2_Delete_Month__c< :String.valueOf(currentDT.month())
				AND v.V2_Is_Delete__c=false];
		String exaId='无';
		if(t.size()>0)exaId=t[0].id;
		debugMsgs.add('*****已到失效日期,失效日期在['+currentDT.date()+']之前,但未标记删除的记录数:'+t.size()+', 如:'+exaId);
	}
	
	//不删除也未同步
	void check3(){
		List<V2_Account_Team__c> t=new List<V2_Account_Team__c>();
		t=[Select v.Id
			, v.name
			, v.V2_IsSyn__c
			, v.isTmpSync__c
			, v.V2_Is_Delete__c
			, v.V2_ApprovalStatus__c
			, v.V2_Account__c
			, v.V2_Account__r.name
			, v.V2_AccountPermissions__c
			, v.V2_OppPermissions__c
			, v.V2_CasePermissions__c
			, v.V2_Role__c
			, v.V2_Effective_Year__c
			, v.V2_Effective_Month__c
			, v.V2_Effective_NewYear__c
			, v.V2_Effective_NewMonth__c
			, v.V2_Delete_Year__c
			, v.V2_Delete_Month__c
			, v.V2_LastAccessDate__c
			From V2_Account_Team__c v
			Where
				v.V2_Is_Delete__c=false
                AND v.V2_IsSyn__c=false];
		String exaId='无';
		if(t.size()>0)exaId=t[0].id;
		debugMsgs.add('*****未标记删除且未同步的记录数:'+t.size()+', 如:'+exaId);
	}
	
	//同步但没有状态的记录
	void check4(){
		List<V2_Account_Team__c> t=new List<V2_Account_Team__c>();
		t=[Select v.Id
			, v.name
			, v.V2_IsSyn__c
			, v.isTmpSync__c
			, v.V2_Is_Delete__c
			, v.V2_ApprovalStatus__c
			, v.V2_Account__c
			, v.V2_Account__r.name
			, v.V2_AccountPermissions__c
			, v.V2_OppPermissions__c
			, v.V2_CasePermissions__c
			, v.V2_Role__c
			, v.V2_Effective_Year__c
			, v.V2_Effective_Month__c
			, v.V2_Effective_NewYear__c
			, v.V2_Effective_NewMonth__c
			, v.V2_Delete_Year__c
			, v.V2_Delete_Month__c
			, v.V2_LastAccessDate__c
			From V2_Account_Team__c v
			Where
				v.V2_Is_Delete__c=false
				AND (v.V2_ApprovalStatus__c=null or v.V2_ApprovalStatus__c='')
                AND v.V2_IsSyn__c=true];
		String exaId='无';
		if(t.size()>0)exaId=t[0].id;
		debugMsgs.add('*****未标记删除且同步了,但没有操作的记录数:'+t.size()+', 如:'+exaId);
	}
	
	void check5(){
		AggregateResult[] aggs=[select COUNT(Id)total From V2_Account_Team__c v];
		String r1='';
		if(aggs.size()>0)
		{
			AggregateResult agg=aggs[0];
			r1=String.valueOf(agg.get('total'));
		}
		
		String r2='';
		
		aggs=[select COUNT(Id)total From V2_Account_Team__c v where V2_Is_Delete__c=false];
		if(aggs.size()>0)
		{
			AggregateResult agg=aggs[0];
			r2=String.valueOf(agg.get('total'));
		}
		debugMsgs.add('*****销售医院关系记录数:'+r1+', 需要同步的记录数:'+r2);
	}
	
	Set<String> getActiveUserIDs(){
		List<User> us=[Select u.Name, u.IsActive, u.Id From User u where u.IsActive=true];
		Set<String> ids=new Set<String>();
		for(User u: us){
			ids.add(u.Id);
		}
		return ids;
	}
	
	List<V2_Account_Team__c> getInitV2ATs(){
		List<V2_Account_Team__c> t=new List<V2_Account_Team__c>();
		t = [Select v.Id
			, v.name
			, v.V2_IsSyn__c
			, v.isTmpSync__c
			, v.V2_Is_Delete__c
			, v.V2_ApprovalStatus__c
			, v.V2_Account__c
			, v.V2_Account__r.name
			, v.V2_AccountPermissions__c
			, v.V2_OppPermissions__c
			, v.V2_CasePermissions__c
			, v.V2_Role__c
			, v.V2_Effective_Year__c
			, v.V2_Effective_Month__c
			, v.V2_Effective_NewYear__c
			, v.V2_Effective_NewMonth__c
			, v.V2_Delete_Year__c
			, v.V2_Delete_Month__c
			, v.V2_NewAccUser__c
			, v.V2_User__c
			From V2_Account_Team__c v
			Where 
				v.V2_Is_Delete__c=false
				AND v.isTmpSync__c=false
				//AND v.V2_Account__c='001200000043tb0'
				limit :BATCH_COUNT_EVERYTIME];
		return t;
	}
	
	void exec(){
		debugMsgs.add('==================================本批次执行同步开始==================================');
		check1();
		check2();
		check3();
		check4();
		check5();
		debugMsgs.add('开始批量初始化');
		
		//获取临时销售医院关系,用来限制批量执行同步医院数量.
		batchATs_tmp=getInitV2ATs();
				
		debugMsgs.add('初始化"销售医院关系"记录数:'+batchATs_tmp.size());
		
		for(V2_Account_Team__c v:batchATs_tmp){
			batchAccIds_tmp.add(v.V2_Account__c);
		}
				
		//将要清除并重建
		batchATMs_tmp=[Select a.Id
				, a.UserId
				, a.TeamMemberRole
				, a.AccountId
				, a.AccountAccessLevel
				From AccountTeamMember a
				Where a.AccountId in:batchAccIds_tmp
			];
			
		/*Action:清除相关医院下的所有AccountTeamMember*/ 
		if(batchATMs_tmp.size()>0)delete batchATMs_tmp;
		
		//获取batchATs_tmp关联医院[batchAccIds_tmp]及其下的所有的销售医院关系
		List<Account> batchAcc_tmp=[Select a.Id, a.name
			, (
				Select Id
					, name
					, V2_IsSyn__c
					, isTmpSync__c
					, V2_Is_Delete__c
					, V2_ApprovalStatus__c
					, V2_Account__c
					, V2_AccountPermissions__c
					, V2_OppPermissions__c
					, V2_CasePermissions__c
					, V2_Role__c
					, V2_Effective_Year__c
					, V2_Effective_Month__c
					, V2_Effective_NewYear__c
					, V2_Effective_NewMonth__c
					, V2_Delete_Year__c
					, V2_Delete_Month__c
					, V2_BatchOperate__c
					, V2_NewAccUser__c
					, V2_User__c
				From V2_Account__r
			) 
			From Account a
			Where a.Id in:batchAccIds_tmp
			];
			
		debugMsgs.add('根据"销售医院关系"过滤出的关联医院的记录数:'+batchAccIds_tmp.size());
		debugMsgs.add('....将删除客户小组成员数量['+batchATMs_tmp.size()+']....\n');
		
		for(V2_Account_Team__c v:batchATs_tmp){
			debugMsgs.add('--"销售医院关系['+v.name+']"详细:, \n'
				+'-----客户='+v.V2_Account__r.name
				+', 审批状态='+v.V2_ApprovalStatus__c+', 标记删除='+v.V2_Is_Delete__c
				+', 生效年='+v.V2_Effective_Year__c+', 生效月='+v.V2_Effective_Month__c
				+', 失效年='+v.V2_Delete_Year__c+', 失效月='+v.V2_Delete_Month__c);
		}
		
		List<V2_Account_Team__c> willUpdateSyncSign=new List<V2_Account_Team__c>();
		for(Account a:batchAcc_tmp){
			 debugMsgs.add('\n');
			 debugMsgs.add('******同步校验医院['+a.id+']['+a.name+']');
			 List<V2_Account_Team__c> ats_tmp=a.V2_Account__r;
			 if(ats_tmp==null || ats_tmp.size()<1){
			 	debugMsgs.add('==医院下无销售医院关系');
			 	continue;
			 }
			 debugMsgs.add('==校验是否需要同步的记录总数['+ats_tmp.size()+'], 也即销售医院关系数量');
			 List<V2_Account_Team__c> tts=new List<V2_Account_Team__c>();//该医院实际需要同步的记录,符合同步要求的记录
			 for(V2_Account_Team__c t: ats_tmp){
				t.isTmpSync__c=true;//所有的销售医院关系都需要更新是否临时同步标志,避免下次继续运行
				//debugMsgs.add('--给"销售医院关系"['+t.Id+']加入已临时同步标志');
				debugMsgs.add('----检查医院销售关系['+t.name+']ApprovalStatus为'+t.V2_ApprovalStatus__c+',Is_Delete为'+t.V2_Is_Delete__c);
				if(
					t.V2_Is_Delete__c==true
					){
					String ems='不符合同步条件:';
					if(t.V2_Is_Delete__c==true || t.V2_Is_Delete__c!=false){
						ems+='是否删除应为[为选中]';
					}
					debugMsgs.add('------no,不符合同步条件,需要标记已临时同步.{'+ems+'}');
					continue;
				}
				debugMsgs.add('------ok.符合同步条件,需要同步');
				tts.add(t);
			 }
			 debugMsgs.add('==实际需要同步的"销售医院关系"记录总数['+tts.size()+']');
			 if(tts.size()>0)validBatchATs.addAll(tts);
			 
			 willUpdateSyncSign.addAll(ats_tmp);
		}
		debugMsgs.add('\n');
		debugMsgs.add('该批次所有医院相关的["销售医院关系"]需要更新已临时同步标志的记录总数['+willUpdateSyncSign.size()+']');
		
		Map<ID, V2_Account_Team__c> wussMap=new Map<ID, V2_Account_Team__c>(willUpdateSyncSign);
		
		Set<String> validUserIDs=getActiveUserIDs();
		List<AccountTeamMember> insertATMs=new List<AccountTeamMember>();
		if(validBatchATs.size()>0){
			for(V2_Account_Team__c atc_tmp : validBatchATs){
				V2_Account_Team__c atc=wussMap.get(atc_tmp.id);//willUpdateSyncSign中包含validBatchATs
	            Boolean canAdded=false;
	            //审批通过,和为空
	            if(atc.V2_ApprovalStatus__c!=null && atc.V2_ApprovalStatus__c!='' && atc.V2_ApprovalStatus__c!='审批通过'){
	            	debugMsgs.add('--['+atc.name+']审批状态不满足要求,跳过,将不做同步');
	            	continue;
	            }
	            String uid=null;
	            if(atc.V2_NewAccUser__c!=null){
                	uid  = atc.V2_NewAccUser__c; //新负责人
	                canAdded=true;
	            }else{
	                uid  = atc.V2_User__c; //小组成员
	                canAdded=true;
	            }
				if(uid==null || uid=='' || !validUserIDs.contains(uid)){
					debugMsgs.add('--['+atc.name+']指定的用户['+uid+']为未启用状态,将不做同步');
	            	continue;
				}
            	if(!canAdded)continue;
	            AccountTeamMember atm = new AccountTeamMember();
	            atm.AccountId = atc.V2_Account__c;
	            //atm.TeamMemberRole = atc.V2_Role__c;//小组角色不同步
	            atm.UserId = uid;
	            //atc.V2_IsSyn__c =true;
	            debugMsgs.add('--该批次的"销售医院关系"['+atc.name+']更新(已同步)标志:UserId='+atm.UserId+'['+uid+'], AccountId='+atm.AccountId+'['+atc.V2_Account__c+']');
	            insertATMs.add(atm);
			}
		}
		
		debugMsgs.add('....更新有效的销售医院关系(已同步)标志['+validBatchATs.size()+']....');
		/*if(validBatchATs.size()>0)update validBatchATs;//更新有效的销售医院关系已同步标志*/
		
		//willUpdateSyncSign中包含validBatchATs
		debugMsgs.add('....更新所有相关的销售医院关系(已临时同步)标志['+willUpdateSyncSign.size()+']....');
		/*Action:更新所有相关的销售医院关系已临时同步标志*/
		if(willUpdateSyncSign.size()>0)update willUpdateSyncSign;
		
		debugMsgs.add('....同步创建客户小组成员['+insertATMs.size()+']....');
		/*Action:创建小组成员*/
		if(insertATMs.size()>0){
			insert insertATMs;
			for(AccountTeamMember atm:insertATMs){
				debugMsgs.add('....['+atm.id+']新成员');
			}
		}
		debugMsgs.add('检验数据执行结果');
		for(V2_Account_Team__c v:willUpdateSyncSign){
			debugMsgs.add('....['+v.name+']已同步标志:V2_IsSyn__c='+v.V2_IsSyn__c+', 临时同步标志:isTmpSync__c='+v.isTmpSync__c);
		}
		
		debugMsgs.add('==================================本批次执行同步结束==================================');
		
		debug(debugMsgs);
	}
	
	void debug(List<String> msgs){
		String rs='\n';
		if(msgs!=null && msgs.size()>0){
			for(String msg: msgs){
				rs+=msg+'\n';
			}
		}
		System.debug(rs);
	}
	
}