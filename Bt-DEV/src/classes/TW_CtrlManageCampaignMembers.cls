/*
Author：Scott
Created on：2013-10-17
Description: 
1.批量审批市场活动成员 
2市场部审批通过即为参加,
新增功能：管理活动成员开放给到销售，销售通过此逻辑来勾选其添加的活动成员是否为已参加。
*/
public without sharing class TW_CtrlManageCampaignMembers {
	public List<CampaignMember> camlist{get;set;}
	public Boolean IsClose{get;set;}
	public String CamId{get;set;}
	//用户角色Id
	public Id userRoleId{get;set;} 
	//用户角色名称
	public String userRoleName{get;set;}
	public String userEmail{get;set;}
	public Boolean IsDisabled{get;set;}
	public Boolean IsMarketing{get;set;}
	public Boolean IsRep{get;set;}
	public TW_CtrlManageCampaignMembers() 
	{
		
		//市场活动Id
		CamId = ApexPages.currentPage().getParameters().get('id');
		//当前登录用户信息
		User CurrentUser=[select Profile.Name,UserRole.Name,UserRoleId,Email from User Where Id=:UserInfo.getUserId()];
		userRoleId = CurrentUser.UserRoleId; 
		userRoleName = CurrentUser.UserRole.Name;
		userEmail =  CurrentUser.Email;
		
		if(CurrentUser.Profile.Name =='系统管理员' || CurrentUser.Profile.Name =='System Administrator' || userRoleName.contains('Marketing'))
		{
			IsMarketing =true;
			IsRep = false;
		}
		else
		{
			IsMarketing = false;
			IsRep = true;
		}
		
		List<Campaign> cam = [select IsActive,Status from Campaign where id=:CamId];
		if(!cam[0].IsActive)
		{
			IsDisabled = true;
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING , '此行銷活動沒有啓用，不能進行此操作！');            
            ApexPages.addMessage(msg);
            return;
		}
		else if(cam[0].Status != 'Sign Up Closed')
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, '只有是當前行銷活動狀態為“報名已結束”時，才允許使用此功能。');            
            ApexPages.addMessage(msg);
            IsDisabled = true;
            return;
		}
		
		//市场部门或系统管理员用户查看
		if(CurrentUser.Profile.Name =='系统管理员' || CurrentUser.Profile.Name =='System Administrator' || userRoleName.contains('Marketing'))
		{
			
			camlist = [select Campaign.IsActive,SpCurrentUserLevel__c,V2_SupervisoApprove__c,V2_AreaManagerApprove__c,V2_MarketingApprove__c,V2_Participated__c,V2_ContactType__c, 
				       ContactId,V2_Hospital__c,V2_Comment__c,User__c,V2_DepartDate__c,V2_DepartFlight__c,V2_ArriveDate__c,V2_ArriveFlight__c,V2_RejectReason__c,UserFullName__c 
				       from CampaignMember where CampaignId=:CamId order by V2_MarketingApprove__c asc];
		}
		//销售查看
		//只能查看自己添加的且市场部审批通过的人员
		else 
		{
			camlist = [select Feedback_of_Campaign__c,Campaign.IsActive,SpCurrentUserLevel__c,V2_SupervisoApprove__c,V2_AreaManagerApprove__c,V2_MarketingApprove__c,V2_Participated__c,V2_ContactType__c, 
				       ContactId,V2_Hospital__c,V2_Comment__c,User__c,V2_DepartDate__c,V2_DepartFlight__c,V2_ArriveDate__c,V2_ArriveFlight__c,V2_RejectReason__c,UserFullName__c 
				       from CampaignMember where CampaignId=:CamId and V2_MarketingApprove__c ='通过'];
			
		}
		
		if(camlist == null || camlist.size()==0 )
		{
			IsDisabled = true;
			
			//市场部
			if(IsMarketing)
			{
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING , '您只能查看您下屬提交的成員，或當前行銷活動沒有可供顯示的行銷活動成員。');            
            	ApexPages.addMessage(msg);
			}
			//销售
			else
			{	
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING , '沒有可供顯示的行銷活動成員記錄，請查看是否有您提交的活動成員或您提交的活動成員是否已經通過市場部審批。');            
            	ApexPages.addMessage(msg);
			}
			
            return;
		}
		
	}
	//保存 
	public PageReference SaveCampaignMember()
	{
		try
		{
			/*for(CampaignMember cam:camlist)
			{
				if(cam.V2_MarketingApprove__c == '通过' || cam.V2_MarketingApprove__c == '通過' )
				{
					cam.V2_Participated__c = true;
				}
				else
				{
					cam.V2_Participated__c = false;
				}
			}*/
			
			update camlist;
			IsClose = true;
			PageReference pageRef = new PageReference(System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+CamId);
            return pageRef;
		}catch(Exception e)
		{
			
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING , String.valueOf(e));            
            ApexPages.addMessage(msg);
            return null;
		}
	}
	public PageReference Cancel()
	{
		PageReference pageRef = new PageReference(System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+CamId);
        return pageRef;
	}
	
	/****************************************************Test*********************************/
    static testMethod void TW_CtrlManageCampaignMembers()
    {
    	/*客户*/
		RecordType accrecordtype = [select Id from RecordType where DeveloperName='TW_Hospital' and SobjectType='Account' limit 1];
		Account acc = new Account();
		acc.RecordTypeId = accrecordtype.Id;
		acc.Name = 'AccTest';
		insert acc;
		
		
		/*用户角色*/
		//市场
		UserRole MarketingUserRole = new UserRole() ;
	    MarketingUserRole.Name = 'TW Marketing Product Manager' ;
	    insert MarketingUserRole ;
	     //销售
	     UserRole RepUserRole = new UserRole() ;
	     RepUserRole.Name = 'TW South Sales Rep';
	     insert RepUserRole ;
	     
		/*用户简档*/
		//rep简档
	    Profile RepProRenal = [select Id from Profile where Name  = 'TW Sales Rep' limit 1];
	    //marketing
	    Profile MarketProRenal = [select Id from Profile where Name='TW Marketing' limit 1];
		
		/************User************/
		List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        
		/*市场部*/
		 User MarketUser = new User();
	     MarketUser.Username='MarketUser@123.com';
	     MarketUser.LastName='MarketUser';
	     MarketUser.Email='MarketUser@123.com';
	     MarketUser.Alias=user[0].Alias;
	     MarketUser.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     MarketUser.ProfileId=MarketProRenal.Id;
	     MarketUser.LocaleSidKey=user[0].LocaleSidKey;
	     MarketUser.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     MarketUser.EmailEncodingKey=user[0].EmailEncodingKey;
	     MarketUser.CommunityNickname='MarketUser';
	     MarketUser.MobilePhone='123456789112';
	     MarketUser.UserRoleId = MarketingUserRole.Id ;
	     MarketUser.IsActive = true;
	     insert MarketUser;
		
	     
	    /*销售*/
	     User RepSu = new User();
	     RepSu.Username='RepSu@123.com';
	     RepSu.LastName='RepSu';
	     RepSu.Email='RepSu@123.com';
	     RepSu.Alias=user[0].Alias;
	     RepSu.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     RepSu.ProfileId=RepProRenal.Id;
	     RepSu.LocaleSidKey=user[0].LocaleSidKey;
	     RepSu.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     RepSu.EmailEncodingKey=user[0].EmailEncodingKey;
	     RepSu.CommunityNickname='RepSu';
	     RepSu.MobilePhone='12345678912';
	     RepSu.UserRoleId = RepUserRole.Id ;
	     RepSu.IsActive = true;
	     insert RepSu;
		/*联系人*/
		
		RecordType conrecordtype = [select Id from RecordType where DeveloperName = 'TW_Contact' and SobjectType='Contact' and IsActive = true limit 1 ];
		Contact con1 = new Contact();
		con1.RecordTypeId = conrecordtype.Id;
		con1.LastName = 'AccTestContact1';
		con1.AccountId=acc.Id;
		insert con1;
		
		Contact con2 = new Contact();
		con2.RecordTypeId = conrecordtype.Id;
		con2.LastName = 'AccTestContact2';
		con2.AccountId=acc.Id;
		insert con2;
		
    	/*市场活动*/
		Campaign cam = new Campaign();
		cam.Name = 'CamTest';
		cam.StartDate = date.today().addMonths(1);
		cam.EndDate = date.today().addMonths(2);
		cam.IsActive = true;
		cam.OwnerId = MarketUser.Id;
		cam.Status = 'Sign Up Closed';
		insert cam;
		
		
		
		Test.startTest();
		ApexPages.currentPage().getParameters().put('id',cam.Id);
		TW_CtrlManageCampaignMembers ccm = new TW_CtrlManageCampaignMembers();
		ccm.SaveCampaignMember();
		/*市场活动成员*/
		CampaignMember cm = new CampaignMember();
		cm.CampaignId = cam.Id;
		cm.ContactId = con1.Id;
		cm.User__c = RepSu.Id;
		insert cm;
		
		CampaignMember cm2 = new CampaignMember();
		cm2.CampaignId = cam.Id;
		cm2.ContactId = con2.Id;
		cm2.User__c = RepSu.Id;
		insert cm2;
		
		TW_CtrlManageCampaignMembers ccm2 = new TW_CtrlManageCampaignMembers();
		ccm2.SaveCampaignMember();
		ccm2.Cancel();
		
		//销售
		//市场部
		System.runAs(RepSu)
		{
			TW_CtrlManageCampaignMembers ccm3 = new TW_CtrlManageCampaignMembers();
			ccm3.SaveCampaignMember();
			ccm3.Cancel();
		}
		//市场部
		System.runAs(MarketUser)
		{
			TW_CtrlManageCampaignMembers Maccm = new TW_CtrlManageCampaignMembers();
			Maccm.camlist[0].V2_MarketingApprove__c = '通过';
			Maccm.SaveCampaignMember();
		}
		Test.stopTest();
    }
}