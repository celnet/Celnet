/*
 * 作者:bill
 * 日期：2013-11-9
 * 说明：导出数据页面的控制类测试类
 */
@isTest
public class PM_Test_PatinetExportController 
{
    public static testMethod void myUnitTest() 
    {
    	PM_Soql__c soql = new PM_Soql__c();
    	soql.Name = userInfo.getUserId();
    	soql.PM_Field__c = 'Select Name,PM_Sex__c,PM_Province__r.Name,PM_City__r.Name,PM_County__r.Name,PM_Address__c,PM_PatientDegree__c,PM_DateOfBirthExport__c,PM_Profession__c,PM_IdNumber__c,PM_Email__c,PM_FAIncome__c,PM_Payment__c,PM_PaySelf__c,PM_LastVisitStatus__c,PM_Visit_Interval__c,PM_ZipCode__c,PM_Internet__c,PM_ContactState__c,PM_Disappearanc__c,PM_CreateDateExport__c,PM_Binformation__c,PM_LastModfiiedDateExport__c,LastModifiedBy.Alias,CreatedBy.Alias,PM_HomeTel__c,PM_FamilyTel__c,PM_FamilyPhone__c,PM_PmTel__c,PM_PmPhone__c,PM_OtherTel2__c,PM_BVTime1__c,PM_BVTime2__c,PM_BVTime3__c,PM_BVTime4__c,PM_BVTime5__c,PM_BVTime6__c,PM_InDateExport__c,PM_InHospital__r.Name,PM_InType__c,PM_InNurser__r.FirstName,PM_InDoctor__r.FirstName,PM_InUser__r.Alias,PM_InInformation__c,PM_Protopathy__c,PM_DaUroutput__c,PM_DiBlpressure__c,PM_SyBlpressure__c,PM_SeCreatinine__c,PM_Hemoglobin__c,PM_Albumin__c,PM_UrNitrogen__c,PM_LiParalysis__c,PM_ViImpairment__c,PM_PeEqtest__c,PM_ClInformation__c,PM_LoGifts__c,PM_AcHoVisits__c,PM_WhTocall__c,PM_WhTosend__c,PM_IsFree__c,PM_IsAcceptCall__c,PM_Received_Diary__c,PM_ReCommunication__c,PM_ReCarepackages__c,PM_IsJoined__c,PM_IsInstruction__c,PM_Shmethod__c,PM_Activities_Satisfactied__c,PM_Is_Personal_Information__c,PM_OHCEducation__c,PM_PeReInformation__c,PM_CaInformation__c,PM_EcDifficulties__c,PM_WhTransplants__c,PM_TuHeIntention__c,PM_CeDisease__c,PM_AdAge__c,PM_TuToCoPro__c,PM_DrAcProblems__c,PM_HighRisk__c,PM_HRInformation__c,PM_Optimistic__c,PM_Talkative__c,PM_ATUnderstand__c,PM_ReToBaite__c,PM_ToDrFofive2__c,PM_AdTomovement__c,PM_HiHobbies__c,PM_FaPeace__c,PM_BeWorking__c,PM_BeStudent__c,PM_HiSostatus__c,PM_HighImpac__c,PM_HIInformation__c,PM_Treat_Hospital__r.Name,PM_Treat_Doctor__r.Name,PM_Treat_Nurse__r.Name,PM_Pdtreatment__c,PM_Treat_Saler__r.Alias,PM_WaConsumption__c,PM_Heatingmode__c,PM_HDAndPD__c,PM_FromVendor__c,PM_TrInformation__c,PM_Dealers__r.Name,PM_TeDistributors__r.Name,PM_DealerRemark__c,PM_DropOff_One_Reason__c,PM_DropOff_Two_Reason__c,PM_Hipper2__c,PM_DropoutActualDateExport__c,PM_DropOff_DateExport__c from PM_Patient__c where Id In :';
    	soql.PM_SOQL_0__c = 'a1qN00000000cGLIAY,a1qN00000000cGUIAY,a1qN00000000cGZIAY,a1qN00000000cGqIAI,a1qN00000000cH3IAI,a1qN00000000cH4IAI,a1qN00000000d6oIAA,a1qN00000000d6pIAA,a1qN00000000d6qIAA,a1qN00000000d74IAA,a1qN00000000d75IAA,a1qN00000000d76IAA,a1qN00000000d7FIAQ,a1qN00000000d7GIAQ,a1qN00000000d7HIAQ,a1qN00000000d7IIAQ,a1qN00000000d7JIAQ,a1qN00000000dFIIAY,a1qN00000000dFSIAY,a1qN00000000dFXIAY,a1qN00000000dFcIAI,a1qN00000000dIMIAY,a1qN00000000dfWIAQ,a1qN00000000dlDIAQ,a1qN00000000dlIIAQ,a1qN00000000dlNIAQ,a1qN00000000dlOIAQ,a1qN00000000dmwIAA,';
    	soql.PM_SOQL_1__c ='a1qN00000000cG8IAI,';
    	soql.PM_SOQL_2__c ='a1qN00000000cGFIAY,';
    	insert soql;
    	
    	system.Test.startTest();
    	PM_PatinetExportController ex = new PM_PatinetExportController();
    	string ss = ex.ExcelTime;
    	system.Test.stopTest();
    }
}