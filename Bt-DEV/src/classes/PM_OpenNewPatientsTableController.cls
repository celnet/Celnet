/**
 * Hank
 * 2013-10-16
 * 功能：新病人审批页面，根据新病人去跟病人比较姓名地址和电话，有重复数据点击详情弹出该页面，显示重复数据
 */
public class PM_OpenNewPatientsTableController 
{
	public PatientApply__c patientApply{get;set;}
	public string newId{get;set;}
	public list<InProjectWrapper> list_repeat{get;set;}
	public PM_OpenNewPatientsTableController(ApexPages.StandardController controller)
	{
		patientApply = new PatientApply__c();
		newId = ApexPages.currentPage().getParameters().get('newid');
		system.debug('newId:'+newId);
		list_repeat = new list<InProjectWrapper>();
		set<string> set_newname = new set<string>();//存放新病人姓名
		set<string> set_newtel = new set<string>();//存放新病人电话
		set<string> set_newaddress = new set<string>();//存放新病人地址
    	
		list<PatientApply__c> queryResultnew = [SELECT Id,PatientName__c,IntubationHospital__c,IntubationHospital__r.name,Distributor__c,Distributor__r.name,PatientApply__r.OwnerId,CreatedDate,Status__c,PM_Patient_Phone__c,PM_Patient_MPhone__c,PM_Famliy_Member_Phone__c,PM_Family_Member_Mphone__c,PM_Famliy_Phone__c,PM_Other_Phone__c,Address__c FROM PatientApply__c WHERE Id =: newId ];
		//system.debug('result size:'+queryResultnew.size());
		//system.debug('result:'+queryResultnew);
    	if(queryResultnew.size()!=null && queryResultnew.size() > 0)
		{
			for(PatientApply__c p:queryResultnew)
			{
				if(p.PatientName__c != '' && p.PatientName__c != null)
    			{
					set_newname.add(p.PatientName__c);
					//system.debug('姓名有值'+p.PatientName__c);
    			}
    			if(p.PM_Patient_Phone__c != '' && p.PM_Patient_Phone__c != null)
    			{
					set_newtel.add(p.PM_Patient_Phone__c);
					//system.debug('电话有值'+p.PM_Patient_Phone__c);
    			}
    			if(p.PM_Patient_MPhone__c != '' && p.PM_Patient_MPhone__c != null)
    			{
					set_newtel.add(p.PM_Patient_MPhone__c);
					//system.debug('电话有值'+p.PM_Patient_MPhone__c);
    			}
    			if(p.PM_Famliy_Member_Phone__c != '' && p.PM_Famliy_Member_Phone__c != null)
    			{
					set_newtel.add(p.PM_Famliy_Member_Phone__c);
					//system.debug('电话有值'+p.PM_Famliy_Member_Phone__c);
    			}
    			if(p.PM_Family_Member_Mphone__c != '' && p.PM_Family_Member_Mphone__c != null)
    			{
					set_newtel.add(p.PM_Family_Member_Mphone__c);
					//system.debug('电话有值'+p.PM_Family_Member_Mphone__c);
    			}
    			if(p.PM_Famliy_Phone__c != '' && p.PM_Famliy_Phone__c != null)
    			{
					set_newtel.add(p.PM_Famliy_Phone__c);
					//system.debug('电话有值'+p.PM_Famliy_Phone__c);
    			}
    			if(p.PM_Other_Phone__c != '' && p.PM_Other_Phone__c != null)
    			{
					set_newtel.add(p.PM_Other_Phone__c);
					//system.debug('电话有值'+p.PM_Other_Phone__c);
    			}
    			if(p.Address__c != '' && p.Address__c != null)
    			{
					set_newaddress.add(p.Address__c);
					//system.debug('地址有值'+p.Address__c);
    			}
			}
		}
		list<PM_Patient__c> queryResult = [SELECT Id,Name,PM_Address__c,PM_PmTel__c,PM_InHospital__c,PM_InHospital__r.Name,PM_Status__c,PM_FamilyTel__c,PM_PmPhone__c,PM_FamilyPhone__c,PM_HomeTel__c,PM_OtherTel2__c FROM PM_Patient__c WHERE Name in: set_newname or PM_HomeTel__c in: set_newtel  or PM_PmPhone__c in: set_newtel  or PM_PmTel__c in: set_newtel  or PM_FamilyPhone__c in: set_newtel  or PM_FamilyTel__c in: set_newtel or PM_OtherTel2__c in: set_newtel or PM_Address__c in: set_newaddress ];
		system.debug('result size:'+queryResult.size());
		system.debug('result:'+queryResult);
    	if(queryResult.size()!=null && queryResult.size() > 0)
    	{
    		for(PM_Patient__c pa:queryResult)
			{
				InProjectWrapper Inpw = new InProjectWrapper();
				if(set_newname.contains(pa.Name))
				{
					Inpw.repeattype='姓名';
					
					if(pa.PM_HomeTel__c!='' && pa.PM_HomeTel__c!=null)
					{
						Inpw.tel= pa.PM_HomeTel__c;
					}
					if(pa.PM_PmPhone__c!='' && pa.PM_PmPhone__c!=null)
					{
						if(Inpw.tel=='' || Inpw.tel==null)
						{
							Inpw.tel= pa.PM_PmPhone__c;
						}
						else
						{
							Inpw.tel+=','+pa.PM_PmPhone__c;
						}
					}
					if(pa.PM_PmTel__c!='' && pa.PM_PmTel__c!=null)
					{
						if(Inpw.tel=='' || Inpw.tel==null)
						{
							Inpw.tel= pa.PM_PmTel__c;
						}
						else
						{
							Inpw.tel+=','+pa.PM_PmTel__c;
						}
					}
					if(pa.PM_FamilyPhone__c!='' && pa.PM_FamilyPhone__c!=null)
					{
						if(Inpw.tel=='' || Inpw.tel==null)
						{
							Inpw.tel= pa.PM_FamilyPhone__c;
						}
						else
						{
							Inpw.tel+=','+pa.PM_FamilyPhone__c;
						}
					}
					if(pa.PM_FamilyTel__c!='' && pa.PM_FamilyTel__c!=null)
					{
						if(Inpw.tel=='' || Inpw.tel==null)
						{
							Inpw.tel= pa.PM_FamilyTel__c;
						}
						else
						{
							Inpw.tel+=','+pa.PM_FamilyTel__c;
						}
					}
					if(pa.PM_OtherTel2__c!='' && pa.PM_OtherTel2__c!=null)
					{
						if(Inpw.tel=='' || Inpw.tel==null)
						{
							Inpw.tel= pa.PM_OtherTel2__c;
						}
						else
						{
							Inpw.tel+=','+pa.PM_OtherTel2__c;
						}
					}
				}
			    if(set_newaddress.contains(pa.PM_Address__c))
				{
					if(Inpw.repeattype=='' || Inpw.repeattype==null)
					{
						Inpw.repeattype='地址';
					}
					else
					{
						Inpw.repeattype+=',地址';
					}
					if(Inpw.tel==null)
					{
						if(pa.PM_HomeTel__c!='' && pa.PM_HomeTel__c!=null)
						{
							Inpw.tel= pa.PM_HomeTel__c;
						}
						if(pa.PM_PmPhone__c!='' && pa.PM_PmPhone__c!=null)
						{
							if(Inpw.tel=='' || Inpw.tel==null)
							{
								Inpw.tel= pa.PM_PmPhone__c;
							}
							else
							{
								Inpw.tel+=','+pa.PM_PmPhone__c;
							}
						}
						if(pa.PM_PmTel__c!='' && pa.PM_PmTel__c!=null)
						{
							if(Inpw.tel=='' || Inpw.tel==null)
							{
								Inpw.tel= pa.PM_PmTel__c;
							}
							else
							{
								Inpw.tel+=','+pa.PM_PmTel__c;
							}
						}
						if(pa.PM_FamilyPhone__c!='' && pa.PM_FamilyPhone__c!=null)
						{
							if(Inpw.tel=='' || Inpw.tel==null)
							{
								Inpw.tel= pa.PM_FamilyPhone__c;
							}
							else
							{
								Inpw.tel+=','+pa.PM_FamilyPhone__c;
							}
						}
						if(pa.PM_FamilyTel__c!='' && pa.PM_FamilyTel__c!=null)
						{
							if(Inpw.tel=='' || Inpw.tel==null)
							{
								Inpw.tel= pa.PM_FamilyTel__c;
							}
							else
							{
								Inpw.tel+=','+pa.PM_FamilyTel__c;
							}
						}
						if(pa.PM_OtherTel2__c!='' && pa.PM_OtherTel2__c!=null)
						{
							if(Inpw.tel=='' || Inpw.tel==null)
							{
								Inpw.tel= pa.PM_OtherTel2__c;
							}
							else
							{
								Inpw.tel+=','+pa.PM_OtherTel2__c;
							}
						}
					}
				}
				if(set_newtel.contains(pa.PM_HomeTel__c) || set_newtel.contains(pa.PM_PmPhone__c) || set_newtel.contains(pa.PM_PmTel__c) || set_newtel.contains(pa.PM_FamilyPhone__c) || set_newtel.contains(pa.PM_FamilyTel__c) || set_newtel.contains(pa.PM_OtherTel2__c))
				{
					if(Inpw.repeattype=='' || Inpw.repeattype==null)
					{
						Inpw.repeattype='电话';
					}
					else
					{
						Inpw.repeattype+=',电话';
					}
					if(Inpw.tel==null)
					{
						if(set_newtel.contains(pa.PM_HomeTel__c))
						{
							Inpw.tel= pa.PM_HomeTel__c;
						}
						if(set_newtel.contains(pa.PM_PmPhone__c))
						{
							if(Inpw.tel=='' || Inpw.tel==null)
							{
								Inpw.tel= pa.PM_PmPhone__c;
							}
							else
							{
								Inpw.tel+=','+pa.PM_PmPhone__c;
							}
						}
						if(set_newtel.contains(pa.PM_PmTel__c))
						{
							if(Inpw.tel=='' || Inpw.tel==null)
							{
								Inpw.tel= pa.PM_PmTel__c;
							}
							else
							{
								Inpw.tel+=','+pa.PM_PmTel__c;
							}
						}
						if(set_newtel.contains(pa.PM_FamilyPhone__c))
						{
							if(Inpw.tel=='' || Inpw.tel==null)
							{
								Inpw.tel= pa.PM_FamilyPhone__c;
							}
							else
							{
								Inpw.tel+=','+pa.PM_FamilyPhone__c;
							}
						}
						if(set_newtel.contains(pa.PM_FamilyTel__c))
						{
							if(Inpw.tel=='' || Inpw.tel==null)
							{
								Inpw.tel= pa.PM_FamilyTel__c;
							}
							else
							{
								Inpw.tel+=','+pa.PM_FamilyTel__c;
							}
						}
						if(set_newtel.contains(pa.PM_OtherTel2__c))
						{
							if(Inpw.tel=='' || Inpw.tel==null)
							{
								Inpw.tel= pa.PM_OtherTel2__c;
							}
							else
							{
								Inpw.tel+=','+pa.PM_OtherTel2__c;
							}
						}
					}
				}
				Inpw.pat = pa;
				list_repeat.add(Inpw);
			}
    	}
	}
		//表示行的封装类
	public class InProjectWrapper
	{
		public String repeattype{get;set;}
		public PM_Patient__c pat{get;set;}
		public String tel{get;set;}
	}
}