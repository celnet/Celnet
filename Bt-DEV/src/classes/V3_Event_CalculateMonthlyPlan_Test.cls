/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class V3_Event_CalculateMonthlyPlan_Test {
static testMethod void TestCreateEvent() 
    {
		V3_EventWithKA eventWithKa = new V3_EventWithKA();
        List<Account> list_Account = new List<Account>() ;
        Account objAcc1 = new Account() ;
        objAcc1.Name = 'Acc1' ;
        list_Account.add(objAcc1) ;
        Account objAcc2 = new Account() ;
        objAcc2.Name = 'Acc2' ;
        list_Account.add(objAcc2) ;
        insert list_Account ;

        List<Contact> list_Contact = new List<Contact>() ;
        Contact objCon1 = new Contact() ;
        objCon1.LastName = 'liu' ;
        objCon1.AccountId = objAcc1.Id ;
        objCon1.DepartmentType__c = '肾科' ;
        objCon1.Title = '主任';
        objCon1.Email = 'con1@con.com' ;
        list_Contact.add(objCon1) ;
        Contact objCon2 = new Contact() ;
        objCon2.LastName = 'li' ;
        objCon2.AccountId = objAcc2.Id ;
        objCon2.DepartmentType__c = '护理科' ;
        objCon2.Title = '主任';
        objCon2.Email = 'con2@con.com' ;
        list_Contact.add(objCon2) ;
        Contact objCon3 = new Contact() ;
        objCon3.LastName = 'zhang' ;
        objCon3.AccountId = objAcc2.Id ;
        objCon3.DepartmentType__c = '院办';
        objCon3.Title = '主任';        
        objCon3.Email = 'con3@con.com' ;
        list_Contact.add(objCon3) ;
        insert list_Contact ;

        List<MonthlyPlan__c> list_MonthlyPlan = new List<MonthlyPlan__c>() ;
        MonthlyPlan__c objMonth1 = new MonthlyPlan__c() ;
        objMonth1.Month__c = '2' ;
        objMonth1.Year__c = '2012' ;
        list_MonthlyPlan.add(objMonth1) ;
        insert list_MonthlyPlan ;

        List<Event> list_Events = new List<Event>() ;
        Event objEve1 = new Event() ;
        objEve1.WhoId = objCon1.Id ;
        //objEve1.AccountId = objAcc1.Id ;
        objEve1.StartDateTime = datetime.now() ;
        objEve1.EndDateTime = datetime.now().addHours(1) ;
        objEve1.SubjectType__c = '' ;
        list_Events.add(objEve1) ;
        Event objEve2 = new Event() ;
        objEve2.WhoId = objCon2.Id ;
        //objEve2.AccountId = objAcc2.Id ;
        objEve2.StartDateTime = datetime.now() ;
        objEve2.EndDateTime = datetime.now().addHours(1) ;
        objEve2.SubjectType__c = '拜访' ;
        list_Events.add(objEve2) ;
        
        system.test.startTest() ;
        insert list_Events ;
        system.test.stopTest() ; 
        //--------------------------------------
    }
    static testMethod void TestUpdateEvent() 
    {
    	
        List<Account> list_Account = new List<Account>() ;
        Account objAcc1 = new Account() ;
        objAcc1.Name = 'Acc1' ;
        list_Account.add(objAcc1) ;
        Account objAcc2 = new Account() ;
        objAcc2.Name = 'Acc2' ;
        list_Account.add(objAcc2) ;
        insert list_Account ;

        List<Contact> list_Contact = new List<Contact>() ;
        Contact objCon1 = new Contact() ;
        objCon1.LastName = 'lname1' ;
        objCon1.AccountId = objAcc1.Id ;
        objCon1.DepartmentType__c = '院办' ;
        objCon1.Email = 'con1@con.com' ;
        list_Contact.add(objCon1) ;
        Contact objCon2 = new Contact() ;
        objCon2.LastName = 'lname2' ;
        objCon2.AccountId = objAcc2.Id ;
        objCon2.DepartmentType__c = 'PIVA' ;
        objCon2.Email = 'con2@con.com' ;
        list_Contact.add(objCon2) ;
        Contact objCon3 = new Contact() ;
        objCon3.LastName = 'lname3' ;
        objCon3.AccountId = objAcc2.Id ;
        objCon3.DepartmentType__c = '肾科' ;
        objCon3.Email = 'con3@con.com' ;
        list_Contact.add(objCon3) ;
        insert list_Contact ;

        List<Campaign> list_Campaign = new List<Campaign>() ;
        Campaign objCam1 = new Campaign() ;
        objCam1.Name = 'cam1' ;
        objCam1.StartDate = date.today() ;
        objCam1.EndDate = date.today().addDays(1) ;
        list_Campaign.add(objCam1) ;
        Campaign objCam2 = new Campaign() ;
        objCam2.Name = 'cam2' ;
        objCam2.StartDate = date.today() ;
        objCam2.EndDate = date.today().addDays(1) ;
        list_Campaign.add(objCam2) ;
        insert list_Campaign ;

        List<MonthlyPlan__c> list_MonthlyPlan = new List<MonthlyPlan__c>() ;
        MonthlyPlan__c objMonth1 = new MonthlyPlan__c() ;
        objMonth1.Month__c = '2' ;
        objMonth1.Year__c = '2012' ;
        list_MonthlyPlan.add(objMonth1) ;
        insert list_MonthlyPlan ;

        List<Event> list_Events = new List<Event>() ;
        Event objEve1 = new Event() ;
        objEve1.WhoId = objCon1.Id ;
        objEve1.WhatId = objCam1.Id ;
        //objEve1.AccountId = objAcc1.Id ;
        objEve1.StartDateTime = datetime.now() ;
        objEve1.EndDateTime = datetime.now().addHours(1) ;
        objEve1.SubjectType__c = '拜访' ;
        list_Events.add(objEve1) ;
        Event objEve2 = new Event() ;
        objEve2.WhoId = objCon2.Id ;
        objEve2.WhatId = objCam2.Id ;
        //objEve2.AccountId = objAcc2.Id ;
        objEve2.StartDateTime = datetime.now() ;
        objEve2.EndDateTime = datetime.now().addHours(1) ;
        objEve2.SubjectType__c = '拜访' ;
        list_Events.add(objEve2) ;
        insert list_Events ;
        
        system.test.startTest() ;
        List<Event> list_Ev = new List<Event>() ;
        objEve1.V2_FollowEventFlag__c = true ;
        objEve1.Done__c = true ;
        list_Ev.add(objEve1) ;
        objEve2.V2_FollowEventFlag__c = true ;
        objEve2.Done__c = true ;
        list_Ev.add(objEve2) ;
        update list_Ev ;
        
        system.test.stopTest() ; 

    }
    static testMethod void TestDeleteEvent() 
    {

        List<Account> list_Account = new List<Account>() ;
        Account objAcc1 = new Account() ;
        objAcc1.Name = 'Acc1' ;
        list_Account.add(objAcc1) ;
        Account objAcc2 = new Account() ;
        objAcc2.Name = 'Acc2' ;
        list_Account.add(objAcc2) ;
        insert list_Account ;

        List<Contact> list_Contact = new List<Contact>() ;
        Contact objCon1 = new Contact() ;
        objCon1.LastName = 'lname1' ;
        objCon1.AccountId = objAcc1.Id ;
        objCon1.DepartmentType__c = '骨科' ;
        objCon1.Email = 'con1@con.com' ;
        list_Contact.add(objCon1) ;
        Contact objCon2 = new Contact() ;
        objCon2.LastName = 'lname2' ;
        objCon2.AccountId = objAcc2.Id ;
        objCon2.DepartmentType__c = '护理科' ;
        objCon2.Email = 'con2@con.com' ;
        list_Contact.add(objCon2) ;
        Contact objCon3 = new Contact() ;
        objCon3.LastName = 'lname3' ;
        objCon3.AccountId = objAcc2.Id ;
        objCon3.DepartmentType__c = '肾科' ;
        objCon3.Email = 'con3@con.com' ;
        list_Contact.add(objCon3) ;
        insert list_Contact ;

        List<MonthlyPlan__c> list_MonthlyPlan = new List<MonthlyPlan__c>() ;
        MonthlyPlan__c objMonth1 = new MonthlyPlan__c() ;
        objMonth1.Month__c = '2' ;
        objMonth1.Year__c = '2012' ;
        list_MonthlyPlan.add(objMonth1) ;
        insert list_MonthlyPlan ;

        List<Event> list_Events = new List<Event>() ;
        Event objEve1 = new Event() ;
        objEve1.WhoId = objCon1.Id ;
        //objEve1.AccountId = objAcc1.Id ;
        objEve1.StartDateTime = datetime.now() ;
        objEve1.EndDateTime = datetime.now().addHours(1) ;
        objEve1.SubjectType__c = '拜访' ;
        list_Events.add(objEve1) ;
        Event objEve2 = new Event() ;
        objEve2.WhoId = objCon2.Id ;
        //objEve2.AccountId = objAcc2.Id ;
        objEve2.StartDateTime = datetime.now() ;
        objEve2.EndDateTime = datetime.now().addHours(1) ;
        objEve2.SubjectType__c = '拜访' ;
        list_Events.add(objEve2) ;
        insert list_Events ;
        
        system.test.startTest() ;
        List<Event> list_EvDel = new List<Event>() ;
        //objEve1.Done__c = true ;
        list_EvDel.add(objEve1) ;
        //objEve2.Done__c = true ;
        list_EvDel.add(objEve2) ;
        delete list_EvDel ;
        
        system.test.stopTest() ; 
        //--------------------------------------
    }
    static testMethod void TestUnDeleteEvent() 
    {

        List<Account> list_Account = new List<Account>() ;
        Account objAcc1 = new Account() ;
        objAcc1.Name = 'Acc1' ;
        list_Account.add(objAcc1) ;
        Account objAcc2 = new Account() ;
        objAcc2.Name = 'Acc2' ;
        list_Account.add(objAcc2) ;
        insert list_Account ;

        List<Contact> list_Contact = new List<Contact>() ;
        Contact objCon1 = new Contact() ;
        objCon1.LastName = 'lname1' ;
        objCon1.AccountId = objAcc1.Id ;
        objCon1.DepartmentType__c = '骨科' ;
        objCon1.Email = 'con1@con.com' ;
        list_Contact.add(objCon1) ;
        Contact objCon2 = new Contact() ;
        objCon2.LastName = 'lname2' ;
        objCon2.AccountId = objAcc2.Id ;
        objCon2.DepartmentType__c = '护理科' ;
        objCon2.Email = 'con2@con.com' ;
        list_Contact.add(objCon2) ;
        Contact objCon3 = new Contact() ;
        objCon3.LastName = 'lname3' ;
        objCon3.AccountId = objAcc2.Id ;
        objCon3.DepartmentType__c = '肾科' ;
        objCon3.Email = 'con3@con.com' ;
        list_Contact.add(objCon3) ;
        insert list_Contact ;

        List<MonthlyPlan__c> list_MonthlyPlan = new List<MonthlyPlan__c>() ;
        MonthlyPlan__c objMonth1 = new MonthlyPlan__c() ;
        objMonth1.Month__c = '2' ;
        objMonth1.Year__c = '2012' ;
        list_MonthlyPlan.add(objMonth1) ;
        insert list_MonthlyPlan ;

        List<Event> list_Events = new List<Event>() ;
        Event objEve1 = new Event() ;
        objEve1.WhoId = objCon1.Id ;
        //objEve1.AccountId = objAcc1.Id ;
        objEve1.StartDateTime = datetime.now() ;
        objEve1.EndDateTime = datetime.now().addHours(1) ;
        objEve1.SubjectType__c = '拜访' ;
        list_Events.add(objEve1) ;
        Event objEve2 = new Event() ;
        objEve2.WhoId = objCon2.Id ;
        //objEve2.AccountId = objAcc2.Id ;
        objEve2.StartDateTime = datetime.now() ;
        objEve2.EndDateTime = datetime.now().addHours(1) ;
        objEve2.SubjectType__c = '拜访' ;
        list_Events.add(objEve2) ;
        insert list_Events ;
        
        system.test.startTest() ;
        List<Event> list_EvDel = new List<Event>() ;
        //objEve1.Done__c = true ;
        list_EvDel.add(objEve1) ;
        //objEve2.Done__c = true ;
        list_EvDel.add(objEve2) ;
        delete list_EvDel ;
        undelete list_EvDel ;
        system.test.stopTest() ; 

    }
}