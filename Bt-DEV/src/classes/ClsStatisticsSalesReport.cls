/**
 * author:sunny
 * 统计销售数据
 */
public class ClsStatisticsSalesReport {
	
    public static Map<Integer , List<SalesReport__c>> GetSalesReports(String strYear ,Id UserRoleId , Id UserId){
    	Map<Integer , List<SalesReport__c>> map_salesReport = new Map<Integer , List<SalesReport__c>>();
    	//获取下属，包含下属的下属
    	V2_UtilClass uc = new V2_UtilClass();
    	List<ID> list_SubUserIds = uc.getSubordinateIds(UserRoleId);
    	system.debug('xiashu ids:'+list_SubUserIds);
    	//找到下属对应年份的销售数据，包含自己的
    	list_SubUserIds.add(UserId);
    	Date startDate = date.valueOf(strYear+'-1-1');
    	Date endDate = startDate.addYears(1) ;
    	//查找所有下属的销售数据，并且按月份分组
    	for(SalesReport__c sr : [Select Id,Time__c,TargetAmount__c,ActualAmount__c,Account__c From SalesReport__c Where Time__c >=: startDate And Time__c <: endDate And OwnerId in: list_SubUserIds]){
    		if(map_salesReport.containsKey(sr.Time__c.month())){
    			List<SalesReport__c> list_sr = map_salesReport.get(sr.Time__c.month()) ;
    			list_sr.add(sr);
    			map_salesReport.put(sr.Time__c.month() , list_sr);
    		}else{
    			List<SalesReport__c> list_sr = new List<SalesReport__c>();
                list_sr.add(sr);
                map_salesReport.put(sr.Time__c.month() , list_sr);
    		}
    	}
    	return map_salesReport ;
    }
    
}