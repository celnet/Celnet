/* 
 * Author: Sunny
 * Created on: 2011-12-26
 * Description: 
 */
global class V2_DelAccTeamScheduler implements Schedulable 
{
	global static final String CRON_EXP = '0 0 2 7 * ?';
	global void execute(SchedulableContext SC) 
	{
		String strDelYear ;
		String strDelYear2 ;
        String strDelMonth ;
        String strDelMonth2 ;
        
        strDelMonth = String.valueOf(date.today().month()) ;
        strDelYear = String.valueOf(date.today().year()) ;
        strDelMonth2 = String.valueOf(date.today().addMonths(-1).month()) ;
        strDelYear2 = String.valueOf(date.today().addMonths(-1).year()) ;
        
        String strQuery = 'Select v.V2_IsSyn__c, v.CreatedDate, v.V2_NewAccUser__c, v.V2_BatchOperate__c, v.V2_User__c, v.V2_Account__c, v.V2_Account__r.OwnerId, v.V2_History__c, v.V2_Delete_Year__c, v.V2_Delete_Month__c From V2_Account_Team__c v Where (v.V2_Delete_Year__c = \''+strDelYear+'\' And v.V2_Delete_Month__c = \''+ strDelMonth +'\' And v.V2_History__c = false And v.V2_Is_Delete__c = true ) Or (v.V2_Delete_Year__c = \''+strDelYear2+'\' And v.V2_Delete_Month__c = \''+ strDelMonth2 +'\' And v.V2_History__c = false And v.V2_Is_Delete__c = true)';
        V2_BatchDeleteAccountTeam DelAccTeam = new V2_BatchDeleteAccountTeam(strQuery);
		
		ID batchprocessid = Database.executeBatch(DelAccTeam,200);
	}
}