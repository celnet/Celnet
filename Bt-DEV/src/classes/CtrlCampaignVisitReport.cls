/*
Author：Tommy 
Created on：2011-3-18
Description: 
SEP系统每周自动发送当月的所有拜访信息至“市场活动所有人” CtrlCampaignVisitReport 属于CampaignVisitReport.component的相关Controller
CampaignVisitReport.component将被VF类型的邮件模板引用
*/
public class CtrlCampaignVisitReport 
{
	public class VisitRow
	{
		public Event event{get; set;}
		public Map<ID, Contact> contactMap{get;set;}
		public String Finish
		{
			get
			{
				if(event.Done__c)
				{
					return '是';
				}
				else
				{
					return '否';
				}
			}
		}
		public Contact contact
		{
			get
			{
				if(event.WhoId == null)
				{
					return null;
				}
				return this.contactMap.get(event.WhoId);
			}
		}
		public Account account
		{
			get
			{
				if(this.contact == null)
				{
					return null;
				}
				return this.contact.Account;
			}
		}
		public Provinces__c province
		{
			get
			{
				if(this.account == null)
				{
					return null;
				}
				return this.account.Provinces__r;
			}
		}
	}
	
	private Id userId; 
	
	public Id ReportToUserId 
	{
		get
		{
			return this.userId;
		}
		set
		{
			this.userId  = value;
			this.RunReport();
		}
	}
	
	public List<VisitRow> visitRowList{get; set;}
	public Map<ID, Contact> contactMap;
	public CtrlCampaignVisitReport()
	{
		this.visitRowList = new List<VisitRow>();
		this.contactMap = new map<ID, Contact>();
	}
	public void RunReport()
	{
		if(this.userId == null)
		{
			system.debug('####In null');
			return;
		}
		Set<ID> contactIdSet = new Set<ID>();
		for(Campaign cp :[Select Id, OwnerId, Name,
			(Select Id,
					Subject, 
					Who.Id, //contactId
					Who.Type,
					WhoId,
					Who.Name,
					WhatId,
					What.Name,
					StartDateTime,
					EndDateTime,
					GAPlan__c,
					GAExecuteResult__c,
					Done__c,
					OwnerId,
					Owner.Name,
					Owner.Alias,
					LastModifiedDate
				From Events 
				//Where StartDateTime=this_month
				) 
			From Campaign Where OwnerId =: this.userId])
		{
			if(cp.Events == null || cp.Events.size() == 0)
			{
				continue;
			}
			for(Event event : cp.Events)
			{
				event.WhatId = cp.Id;
				if(event.Who != null && event.Who.Type == 'Contact')
				{
					contactIdSet.add(event.WhoId);
				}
				VisitRow row = new VisitRow();
				row.contactMap = this.contactMap;
				row.event = event;
				visitRowList.add(row);
			}
		}
		
		for(Contact ct : [Select Id, Name, 
			AccountId, 
			Account.Id, 
			Account.Name, 
			Account.Provinces__c, 
			Account.Provinces__r.Id, 
			Account.Provinces__r.Name 
			From Contact Where Id IN:contactIdSet])
		{
			this.contactMap.put(ct.Id, ct);
		}
	}
	
	public static User CreateUserForTest(String lastName, String firstName, String alias)
	{
		List<User> user = [select id,ProfileId, Profile.Name, Profile.Id, Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey, Email, MobilePhone from User where id =: UserInfo.getUserId()];
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username=firstName + 'bacd@123.com';
    	use1.LastName=lastName;
    	use1.FirstName=firstName;
    	use1.Email=user[0].Email;
    	use1.Alias=alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.Profile = new Profile();
    	use1.Profile.Id = use1.ProfileId;
    	use1.Profile.Name = user[0].Profile.name;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname=firstName + 'abc';
    	use1.MobilePhone=user[0].MobilePhone;
    	use1.IsActive = true;
    	list_User.add(use1) ;
    	insert list_User;
		return use1;
	}
    static testMethod void myUnitTest() 
    {
		User user = CreateUserForTest('Marketing', 'Rep', 'Mp171');
		CampaignVisitReportSetting__c setting = new CampaignVisitReportSetting__c();
		setting.IsReportTo__c = true;
		setting.Name = user.Profile.Name;
		insert setting;
		
		Provinces__c province = new Provinces__c();
        province.Name = '上海';
        insert province;
        Cities__c city = new Cities__c();
        city.Name = '上海';
        city.BelongToProvince__c = province.Id;
        insert city;
		Account acc1 = new Account();
        acc1.Name = 'T_医院1';
        acc1.Cities__c = city.Id;
        acc1.Provinces__c = province.Id;
        insert new Account[] {acc1};
        
        Contact ct1 = new Contact();
        ct1.AccountId = acc1.Id;
        ct1.LastName = 'T_医院1_医生A';
        insert new Contact[]{ct1};
		
		Campaign cp = new Campaign();
		cp.Name = 'T_CPA';
		cp.IsActive = true;
		cp.StartDate = Date.today();
		cp.EndDate = Date.today().addDays(30);
		cp.OwnerId = user.Id;
		insert(cp);
		
		ID recordTypeId = [Select Id, Name From RecordType Where DeveloperName = 'V2_Event'].Id;
		Event ev = new Event();
		ev.Subject = '拜访'; 
		ev.WhoId = ct1.Id;
		ev.WhatId = cp.Id;
		ev.StartDateTime = Datetime.now();
		ev.EndDateTime = Datetime.now();
		ev.OwnerId = user.Id;
		ev.RecordTypeId = recordTypeId;
		insert ev;
        
        //开始测试
        test.startTest();
      	CtrlCampaignVisitReport cpReport = new CtrlCampaignVisitReport();
      	cpReport.ReportToUserId = user.Id;
      	List<VisitRow> visitRowList = cpReport.visitRowList;
        test.stopTest();
       	System.assert(visitRowList.size() == 1);
        system.assertEquals(visitRowList[0].contact.Id, ct1.Id);
        system.assertEquals(visitRowList[0].account.Id, acc1.Id);
    }
}