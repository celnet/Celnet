/**
 * author:bill
 * AutoVapRetDetailInfo的trigger的测试类
 */
@isTest
private class Test_AutoVapRetDetailInfo {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        //挥发罐 
        VaporizerInfo__c vap = new VaporizerInfo__c();
        vap.Name = 'huifaguan';
        vap.Status__c = '使用';
        vap.location__c = '医院';
        insert vap;
        
        //挥发罐维修/退回申请
        Vaporizer_ReturnAndMainten__c vapAplyReturn = new Vaporizer_ReturnAndMainten__c();
        insert vapAplyReturn;
        
        //挥发罐维修/退回申请明细
        Vaporizer_ReturnAndMainten_Detail__c vapAplyReturnDetail = new Vaporizer_ReturnAndMainten_Detail__c();
        vapAplyReturnDetail.Vaporizer_ReturnAndMainten__c = vapAplyReturn.Id;
        vapAplyReturnDetail.VaporizerInfo__c = vap.Id;
        vapAplyReturnDetail.ReqireNewOne__c = true;
        vapAplyReturnDetail.DeliveredAmount__c = 3;
        
        test.startTest();
        insert vapAplyReturnDetail;
        test.stopTest();
        
    }
}