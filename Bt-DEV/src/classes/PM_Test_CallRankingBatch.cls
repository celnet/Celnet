/**
 * Author：Dean
 * Date：2013-11-10
 * Function：PM_CallRankingBatch的测试类
 */
@isTest
private class PM_Test_CallRankingBatch {

    static testMethod void myUnitTest() {
    	Datetime startday = datetime.now();
    	DateTime endday =  datetime.now();  	
        List<UserRole> listur= new List<UserRole>();
        UserRole ur0 = new UserRole();
        ur0.Name = ' PM-PSR Admin';
        listur.add(ur0);
        UserRole ur1 = new UserRole();
        ur1.Name = 'PM-Manager';
        listur.add(ur1);
        UserRole ur2 = new UserRole();
        ur2.Name = ' PM-Vice Manager GZ';
        listur.add(ur2);
        insert listur;
        ur2.ParentRoleId=ur1.Id;
        update ur2;
        ur1.ParentRoleId = ur0.id;
        update ur1;
        
        //user 
        list<User> user=[Select TimeZoneSidKey,ProfileId,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname, Alias From User Where IsActive = true limit 1];
        List<User> list_user = new List<User>();
        User user0=new User();
        user0.username='user0r2@023.com';
        user0.LastName='user0r2';
        user0.Email='user0r2@023.com';
        user0.Alias=user[0].Alias;
        user0.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user0.ProfileId=[Select p.Name, p.Id From Profile p where p.Name like '%PM%'][0].Id;
        user0.LocaleSidKey=user[0].LocaleSidKey;
        user0.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user0.EmailEncodingKey=user[0].EmailEncodingKey;
        user0.CommunityNickname='chequ23';
        user0.MobilePhone='02345678902';
        user0.IsActive = true;
        user0.UserRoleId=ur0.Id ;
        user0.Title = 'Service Team';
        user0.Alias='zhangsan';
        list_user.add(user0);
        User user1=new User();
        user1.username='user1r2@123.com';
        user1.LastName='user1r2';
        user1.Email='user1r2@123.com';
        user1.Alias=user[0].Alias;
        user1.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user1.ProfileId=[Select p.Name, p.Id From Profile p where p.Name like '%PM%'][0].Id;
        user1.LocaleSidKey=user[0].LocaleSidKey;
        user1.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user1.EmailEncodingKey=user[0].EmailEncodingKey;
        user1.CommunityNickname='chequ';
        user1.MobilePhone='12345678912';
        user1.Alias='aaaa';
        user1.IsActive = true;
        user1.UserRoleId=ur0.Id ;
        user1.Title = 'Education Team';
        list_user.add(user1);
        insert list_user;

		List<PM_PSRMonthPlan__c> listPMPS = new List<PM_PSRMonthPlan__c>();
		PM_PSRMonthPlan__c pmPsr23 = new PM_PSRMonthPlan__c();
		pmPsr23.PM_TargetCall__c = 100;
		//pmPsr23.PM_Date__c = date.today();
		pmPsr23.PM_Year__c = string.valueOf(date.today().year());
		pmPsr23.PM_Month__c = string.valueOf(date.today().month());
		pmPsr23.OwnerId = user0.Id;
		listPMPS.add(pmPsr23);
		PM_PSRMonthPlan__c pmPsr24 = new PM_PSRMonthPlan__c();
        pmPsr24.OwnerId = user1.Id;
        pmPsr24.PM_TargetCall__c = 100;
		pmPsr24.PM_Year__c = string.valueOf(date.today().year());
		pmPsr24.PM_Month__c = string.valueOf(date.today().month());
        //pmPsr24.PM_Date__c = date.today();
        listPMPS.add(pmPsr24);
		insert listPMPS;
		
		ID ReId = [Select r.DeveloperName, r.Name, r.Id From RecordType r where r.DeveloperName = 'RoutineEvent'][0].Id;
		List<Event> lisev = new List<Event>();
		Event event11 = new Event();
        event11.RecordTypeId = ReId;
        event11.OwnerId = user0.Id;
        event11.Done__c = true;
        event11.StartDateTime = startday.addDays(-1).addHours(1); 
        event11.EndDateTime = startday.addDays(-1).addHours(3);
        event11.type = '电话随访';
        event11.Done__c = true;
        lisev.add(event11); 	
        insert lisev;       
        
        system.Test.startTest();
		PM_CallRankingBatch callBatch = new PM_CallRankingBatch();
        database.executeBatch(callBatch);
        system.Test.stopTest();
    }
}