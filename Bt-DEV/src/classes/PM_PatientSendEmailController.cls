/*
*Author：Dean
*Date:2013-11-1
*功能：发送邮件，并请邮件信息以及附件保存到历史记录中
*/ 
public class PM_PatientSendEmailController 
{
	//接受URL传来的调阅ID
	String SetID = null;
	//页面多个附件
	public List<Attachment> listAtt {get;set;}
	//收件人地址
	public String SendA {get;set;}
	//收件人地址
	public String SendAPSR {get;set;}
	//邮件标题
	public String SendT {get;set;}
	//邮件内容
	public String SendText {get;set;}
	//页面布局
	public boolean IsView{get;set;}
	public boolean IsViews{get;set;}
	public boolean IsTest{get;set;}
	/*********************构造函数，初始化页面值************************/
	public PM_PatientSendEmailController()
    {
    	IsView = true;
    	IsViews = false;
    	IsTest = false;
    	listAtt = new list<Attachment>();
    	String Sendacc = '';
    	String ApName = '';
    	String OwnIDa = '';
    	addFile();
     	for(Application__c aplica : [Select a.OwnerId,a.Owner.Alias From Application__c a where a.Id = :Apexpages.currentPage().getParameters().get('StringId')])
     	{
     		if(aplica.Owner.Alias != null)
     		ApName = aplica.Owner.Alias;
     		if(aplica.OwnerId != null)
     		OwnIDa = aplica.OwnerId;
     	}
     	
     	SendT = '附件是您的下属'+ApName+'的病人信息调阅，请留意。'; 
     	String testlast = '                                                                             -患者管理团队';
     	SendText = '附件是您的下属'+ApName+'的病人信息调阅，请留意。'+'\r\n'+'温馨提醒：本邮件包含信息数据全部为百特内部数据，仅供百特内部使用。'+'\r\n'+'\r\n'+'\r\n'+'\r\n'+testlast;
     	
     	for(User us : [Select a.Manager.Email From User a where a.Id = :OwnIDa])
     	{
     		if(us.Manager.Email != null)
     		{
     			Sendacc = us.Manager.Email;
     		}
     	}
    	if(Sendacc != null)
    	{
    		SendA = Sendacc;
    	}
    	else
    	{
    		SendA = '上级邮箱为空';
    	}
    	for(User SendAccd : [Select u.Email From User u where u.Profile.Name = 'PM-PSR Manager'])
		{
				SendAPSR = SendAccd.Email;
		}
    }
    /*********************构造函数，初始化页面值************************/
    /****************添加多个附件***************/
    public void addFile()
    {
    	Attachment a = new Attachment();
    	//a.Name = Attach.Name;
    	listAtt.add(a);
 		
    }
    /****************添加多个附件***************/
	public void SendEmail()
	{

/***********************创建关于发送邮件的任务***************************************/	
		boolean Bs = true;
		boolean badySt = true;
		boolean badyStFi = true;
		for(Attachment bo : listAtt)
		{
			if(bo.Body == null)
			{
				badyStFi = false;
			}
		}
		if(IsTest)
		{
			badyStFi = true;
		}
		
/**********************将附加关联到邮件相关任务中***************************************/
/***********************发送邮件**************************************/
		if(Bs && EmailAYes(SendAPSR) && EmailAYes(SendA))
		{
		if(!badyStFi)
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING , '请上传附件之后发送邮件') ;            
        	ApexPages.addMessage(msg);
        	Bs = false;
		}
		else{
		Task tas = new Task();
		tas.Subject = SendT;
		tas.WhatId = Apexpages.currentPage().getParameters().get('StringId');
		tas.ActivityDate = Date.today();
		tas.Status = '已完成';
		tas.Description = SendText;
		insert tas;
		
/***********************创建关于发送邮件的任务***************************************/
/***********************将附加关联到邮件相关任务中**************************************/
		
		List<Attachment> List_DeAt = new List<Attachment>();
		for(Attachment Attach: listAtt)
		{
			if(Attach.Name != null)
			{
				Attachment attachA = new Attachment();
				attachA.ParentId = tas.Id;
				SetID = tas.Id;
				attachA.Name = Attach.Name;
				attachA.Body = Attach.Body;
				if(Attach.Body == null)
				{
					badySt = false; 
				}
				List_DeAt.add(attachA);
			}
		}
		System.debug(List_DeAt.size());
		if(badySt)
		{
			insert List_DeAt;
		}
		/*************************将邮件发送给PSRManager*****************************/
		EmailSend(SendAPSR);	
		/*************************将邮件发送给PSRManager*****************************/
		EmailSend(SendA);
		
		
		IsView = false;
		IsViews = true;
		listAtt.clear();
		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM , '邮件已发出，请查收') ;            
        ApexPages.addMessage(msg);
		}
		}
		else
		{
			if(Bs)
			{
				listAtt.clear();
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL , '收件人邮箱有误，请重新填写邮件信息') ;            
        		ApexPages.addMessage(msg);
			}
		}
/***********************发送邮件**************************************/
	}
	
	
	public boolean EmailAYes(String Assec)
	{
		if(Assec.length() > 4 && !Assec.startsWith('@') && !Assec.startsWith('.') && !Assec.endsWith('@') 
		&& !Assec.endsWith('.') && Assec.indexOf('@') != -1 && Assec.indexOf('.') != -1 && Assec.indexOf('，') == -1&& Assec.indexOf('。') == -1)
		{
			return true;
		}
		else
		return false;
	}
/***********************发送邮件**************************************/
	public void EmailSend(String SendAcd)
	{
			String AptName = '';
			for(Application__c aplica : [Select a.OwnerId,a.Owner.Alias From Application__c a where a.Id = :Apexpages.currentPage().getParameters().get('StringId')])
	     	{
	     		if(aplica.Owner.Alias != null)
	     		AptName = aplica.Owner.Alias;
	     	}
	     	
	     	SendT = '附件是您的下属'+AptName+'的病人信息调阅，请留意。';
	     	SendText = SendT;
			List<Messaging.EmailFileAttachment> list_att= new  List<Messaging.EmailFileAttachment>();
			List<String> ListId = new List<String>();
			String emailAddress = SendAcd;/*UserInfo.getUserEmail();*/
		    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			
            SendText+='<br><br>';
            SendText+='<br><br><p align="center">-患者管理团队</p>';
            SendText+='<br><br><font color="#008000">温馨提醒：本邮件包含信息数据全部为百特内部数据，仅供百特内部使用。</font>';
            if(SetID != null)
            {
	            for(Attachment emats :[Select a.ParentId, a.name, a.body ,a.Id From Attachment a where a.ParentId = :SetID])
	            {
	            	 Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        			efa.setFileName(emats.name);
        			efa.setBody(emats.body);
					list_att.add(efa);
	            }   
            }
            String[] repAddress =new string[]{emailAddress};
            mail.setToAddresses(repAddress);
	        mail.setHtmlBody(SendText);
	        mail.setSubject(SendT);
	        mail.setSenderDisplayName('Salesforce');
	        if(list_att.size()>0)
	        mail.setFileAttachments(list_att);
	       // mail.setDocumentAttachments(ListId);
	        
		    if(emailAddress != null)
	        {
	        	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	        }
	        String Id = [Select a.Id From Application__c a where a.Id = :Apexpages.currentPage().getParameters().get('StringId')][0].Id;	
			Application__c upA = new Application__c();
			upA.Id = Id;
			upA.Status__c = '完成';
			upsert upA;
	}
/***********************发送邮件**************************************/
}