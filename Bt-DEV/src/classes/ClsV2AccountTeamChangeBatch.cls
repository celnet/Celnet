/**
 * Author : Bill
 * 每月1号凌晨，替换、新增操作后，根据产品类型修改对应数据的所有人
 * SP医院信息 ： 产品类型为SP，修改当前数据的所有人
 * IVT医院信息 ： 产品类型为IVT，修改当前数据的所有人
 * 记录类型为IN Project的业务机会重新共享给新代表
 * 挥发罐的所有人 
 **/
global class ClsV2AccountTeamChangeBatch implements Database.Batchable<sObject>{
	global Database.QueryLocator start(Database.BatchableContext BC){
    	//获取应处理的销售医院关系
    	//1.新增操作：操作为新增；生效日期小于等于今天;失效日期为空
    	/*******************bill 2013-12-6 add**********************/
    	//获取上个月的1号，为开始日期，这样获取只去关注上月份
    	Date startDate = date.today().addMonths(-1);
    	/*******************bill 2013-12-6 add**********************/
    	return Database.getQueryLocator([Select Id,V2_Account__c,V2_Account__r.Name,V2_User__c,V2_BatchOperate__c,V2_NewAccUser__c,V2_UserProduct__c, IsPending__c 
				From V2_Account_Team__c  
				Where V2_LastAccessDate__c <= TODAY 
				/*******************bill 2013-12-6 add**********************/
				//最终生效日期大于上月一号
				AND V2_LastAccessDate__c >=: startDate
				/*******************bill 2013-12-6 add**********************/
				AND V2_UserProduct__c IN ('IVT','SP')
				AND IsPending__c = true
				AND V2_BatchOperate__c IN ('新增','替换')]);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope){
    	List<V2_Account_Team__c> list_atUp = new List<V2_Account_Team__c>();
    	Set<ID> Set_AccountId = new Set<ID>();//需要同步的AccountId
    	Set<ID> Set_AccountId_IVT = new Set<ID>();//ivt需要同步的AccountId
    	Set<ID> Set_AccountId_SP = new Set<ID>();//sp需要同步的
    	Map<string,ID> map_acc_sales = new Map<string,ID>();//客户的最新 小组成员
    	
    	for(sObject sObj : scope){
    		V2_Account_Team__c vat = (V2_Account_Team__c)sObj;
    		Set_AccountId.add(vat.V2_Account__c);
    		if(vat.V2_UserProduct__c == 'IVT' && vat.V2_BatchOperate__c == '新增')
    		{
    			Set_AccountId_IVT.add(vat.V2_Account__c);
    			map_acc_sales.put(vat.V2_Account__c+'IVT',vat.V2_User__c);
    		}
    		if(vat.V2_UserProduct__c == 'IVT' && vat.V2_BatchOperate__c == '替换')
    		{
    			Set_AccountId_IVT.add(vat.V2_Account__c);
    			map_acc_sales.put(vat.V2_Account__c+'IVT',vat.V2_NewAccUser__c);
    		}
    		if(vat.V2_UserProduct__c == 'SP' && vat.V2_BatchOperate__c == '新增')
    		{
    			Set_AccountId_SP.add(vat.V2_Account__c);
    			map_acc_sales.put(vat.V2_Account__c+'SP',vat.V2_User__c);
    		}
    		if(vat.V2_UserProduct__c == 'SP' && vat.V2_BatchOperate__c == '替换')
    		{
    			Set_AccountId_SP.add(vat.V2_Account__c);
    			map_acc_sales.put(vat.V2_Account__c+'SP',vat.V2_NewAccUser__c);
    		}
    		vat.IsPending__c = false;
    		list_atUp.add(vat);
    	}
    	
    	//当前年份
    	string year = string.valueOf(date.today().year());
    	//处理IVT医院信息
    	list<IVSHospitalInfo__c> list_Ivt = [Select i.OwnerId, i.Account__c From IVSHospitalInfo__c i where Year__c = : year and Account__c in : Set_AccountId_IVT];
    	if(list_Ivt != null && list_Ivt.size() > 0)
    	{
	    	for(IVSHospitalInfo__c IVT : list_Ivt)
	    	{
	    		if(map_acc_sales.get(IVT.Account__c+'IVT') != null)
	    		{
	    			IVT.OwnerId = map_acc_sales.get(IVT.Account__c+'IVT');
	    		}
	    	}
    		update list_Ivt;
    	}
    	//处理SP医院信息
    	list<CCHospitalInfo__c> list_sp = [Select c.OwnerId, c.Account__c From CCHospitalInfo__c c where Year__c = : year and Account__c in : Set_AccountId_SP];
    	system.debug(list_sp+'klm');
    	if(list_sp != null && list_sp.size() > 0)
    	{
	    	for(CCHospitalInfo__c sp : list_sp)
	    	{
	    		if(map_acc_sales.get(sp.Account__c+'SP') != null)
	    		{
	    			sp.OwnerId = map_acc_sales.get(sp.Account__c+'SP');
	    		}
	    	}
    		update list_sp;	
    	}
    	//处理记录类型为IN Project的业务机会共享
    	//记录类型为inproject的ID
		ID projectId = [Select r.Name, r.Id From RecordType r where r.Name in ('IN Project') and IsActive = true and r.SobjectType = 'Opportunity'][0].Id;
		List<Opportunity> list_opp = [Select o.Id, o.AccountId, OwnerId From Opportunity o where o.RecordTypeId = : projectId and AccountId in : Set_AccountId];
		if(list_opp != null && list_opp.size() > 0)
    	{
    		List<OpportunityShare> list_share = new List<OpportunityShare>();
			for(Opportunity opp : list_opp)
			{
				if(map_acc_sales.get(opp.AccountId+'IVT') != null && map_acc_sales.get(opp.AccountId+'IVT') != opp.OwnerId)
	    		{
					OpportunityShare share = new OpportunityShare(); 
			        share.OpportunityId = opp.Id;
			        share.UserOrGroupId = map_acc_sales.get(opp.AccountId+'IVT');
			        share.OpportunityAccessLevel = 'Read';
		        	list_share.add(share);
	    		}
			}
			if(list_share != null && list_share.size()>0)
			{
	    		insert list_share;
			}
    	}
    	//处理挥发罐的所有人
    	list<VaporizerInfo__c> list_vap = [Select v.OwnerId, v.Hospital__c From VaporizerInfo__c v where Hospital__c in : Set_AccountId_SP];
    	if(list_vap != null && list_vap.size() > 0)
    	{
    		for(VaporizerInfo__c vap : list_vap)
    		{
    			if(map_acc_sales.get(vap.Hospital__c+'SP') != null)
	    		{
    				vap.OwnerId = map_acc_sales.get(vap.Hospital__c+'SP');
	    		}
    		}
    		update list_vap;
    	}
    	update list_atUp;
    }
    global void finish(Database.BatchableContext BC){
    	
    }
}