/*
 * Tobe
 * 2013.10.11
 * AutoConvertPatient Trigger测试类
 */
@isTest
private class PM_Test_AutoConvertPatientTrigger 
{
    static testMethod void myUnitTest() 
    {
    	system.Test.startTest();
    	PatientApply__c patientApply = new PatientApply__c();
    	PatientApply__c patientApply2 = new PatientApply__c();
    	PM_Patient__c pat =new PM_Patient__c();
    	
        MonthlyPlan__c MonPlan =new MonthlyPlan__c();
        //MonPlan.OwnerId = user.Id;
        insert MonPlan;
        //新建省市
        Region__c zone = new Region__c();
        zone.Name = '南区';
        insert zone;
		Provinces__c pro = new Provinces__c();
    	pro.Name = '浙江省';
    	insert pro;
    	Cities__c city = new Cities__c();
    	city.Name = '杭州市';
    	city.BelongToProvince__c = pro.Id;
    	insert city;
    	
    	//客户
    	list<RecordType> record = [Select Id From RecordType Where Name = '医院'];
    	
    	list<RecordType> record2 = [Select Id From RecordType Where Name = '经销商'];
    	
        Account acc = new  Account();
        acc.Name = '浙江医院';
        acc.RecordTypeId = record[0].Id;
        acc.Provinces__c = pro.Id;
        acc.Cities__c = city.Id;
        acc.Region__c = zone.Id;
        insert acc;
        Account acc2 = new  Account();
        acc2.Name = '浙江医药控股';
        acc2.RecordTypeId = record2[0].Id;
        acc2.Provinces__c = pro.Id;
        acc2.Cities__c = city.Id;
        acc2.Region__c = zone.Id;
        insert acc2;
    	patientApply.Status__c = '待审批';
    	patientApply.Distributor__c = acc2.Id;
    	patientApply.IntubationHospital__c = acc.Id;
    	patientApply.PatientApply__c = MonPlan.Id;
    	patientApply.PatientName__c = 'aaa';
    	patientApply.PM_Famliy_Phone__c = '010-12345678';
    	patientApply.PM_Patient_MPhone__c = '12345678901';
    	patientApply.PM_Patient_Phone__c = '010-98765432';
    	patientApply.PM_Family_Member_Mphone__c = '12345678901';
    	patientApply.PM_Famliy_Member_Phone__c = '12345678901';
    	patientApply.PM_Other_Phone__c = '1234567890';
    	patientApply.Address__c = 'Test123';
    	insert patientApply;
    	patientApply.Status__c = '通过';
    	update patientApply;
    	system.Test.stopTest();
    }
}