/**
 * author : bill
 * ReturnDetailCanNotEdit的trigger的测试类
 */
@isTest
private class Test_ReturnDetailCanNotEdit {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        //挥发罐维修/退回申请
        Vaporizer_ReturnAndMainten__c vapAplyReturn = new Vaporizer_ReturnAndMainten__c();
        vapAplyReturn.Approve_Result__c = '待审批';
        insert vapAplyReturn;
        
        //挥发罐维修/退回申请明细
        Vaporizer_ReturnAndMainten_Detail__c vapAplyReturnDetail = new Vaporizer_ReturnAndMainten_Detail__c();
        vapAplyReturnDetail.Vaporizer_ReturnAndMainten__c = vapAplyReturn.Id;
        vapAplyReturnDetail.DeliveredAmount__c = 3;
        insert vapAplyReturnDetail;
        Vaporizer_ReturnAndMainten_Detail__c vapAplyReturnDetail1 = new Vaporizer_ReturnAndMainten_Detail__c();
        vapAplyReturnDetail1.Vaporizer_ReturnAndMainten__c = vapAplyReturn.Id;
        vapAplyReturnDetail1.DeliveredAmount__c = 3;
        
        test.startTest();
        vapAplyReturn.Approve_Result__c = '审批中';
        update vapAplyReturn;
        try{
        insert vapAplyReturnDetail1;
        }catch(Exception e){}
        try{
        vapAplyReturnDetail.DeliveredAmount__c = 4;
        update vapAplyReturnDetail;
        }catch(Exception e){}
        try{
        delete vapAplyReturnDetail;
        }catch(Exception e){}
        test.stopTest();
    }
}