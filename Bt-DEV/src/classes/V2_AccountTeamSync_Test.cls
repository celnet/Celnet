/* 
 * Author: Sunny
 * Created on: 2012-2-6
 * Description: 
 */
@isTest
private class V2_AccountTeamSync_Test 
{
    static testMethod void TestAddAccountTeam() 
    {
    	//----------------Create User-------------
    	List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
        //----------------New Account--------------------
        List<Account> list_Account = new List<Account>() ;
        Account objAcc1 = new Account() ;
        objAcc1.Name = 'test acc1' ;
        objAcc1.MID__c = '12345654321' ;
		list_Account.add(objAcc1) ;
		insert list_Account ;
		//----------------New Account Team--------------------
		List<V2_Account_Team__c> list_AccountTeam = new List<V2_Account_Team__c>() ;
		V2_Account_Team__c objAccTeam1 = new V2_Account_Team__c() ;
		objAccTeam1.V2_Account__c = objAcc1.Id ;
		objAccTeam1.V2_BatchOperate__c = '新增' ;
		objAccTeam1.V2_ApprovalStatus__c = '待审批' ;
		objAccTeam1.V2_User__c = use1.Id ;
		list_AccountTeam.add(objAccTeam1) ;
		V2_Account_Team__c objAccTeam2 = new V2_Account_Team__c() ;
		objAccTeam2.V2_Account__c = objAcc1.Id ;
		objAccTeam2.V2_BatchOperate__c = '新增' ;
		objAccTeam2.V2_ApprovalStatus__c = '审批拒绝' ;
		objAccTeam2.V2_User__c = use2.Id ;
		list_AccountTeam.add(objAccTeam2) ;
		V2_Account_Team__c objAccTeam3 = new V2_Account_Team__c() ;
		objAccTeam3.V2_Account__c = objAcc1.Id ;
		objAccTeam3.V2_BatchOperate__c = '新增' ;
		objAccTeam3.V2_ApprovalStatus__c = '审批通过' ;
		objAccTeam3.V2_User__c = use3.Id ;
		list_AccountTeam.add(objAccTeam3) ;
		insert list_AccountTeam ;
		//----------------New AccountTeamM--------------------
		List<AccountTeamMember> list_AccTeamM = new List<AccountTeamMember>() ;
		AccountTeamMember objAccTeamM1 = new AccountTeamMember() ;
		objAccTeamM1.AccountId = objAcc1.Id ;
		objAccTeamM1.UserId = use2.Id ;
		list_AccTeamM.add(objAccTeamM1) ;
		//insert list_AccTeamM ;
		
		//----------------Start Test--------------------
		system.test.startTest() ;
		update list_AccountTeam ;
		List<AccountTeamMember> list_accTeam = [Select Id From AccountTeamMember Where AccountId =: objAcc1.Id] ;
		//system.assertEquals(1, list_accTeam.size()) ;
		system.test.stopTest() ;
		
    }
    static testMethod void TestDelAccountTeam() 
    {
    	//----------------Create User-------------
    	List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
        //----------------New Account--------------------
        List<Account> list_Account = new List<Account>() ;
        Account objAcc1 = new Account() ;
        objAcc1.Name = 'test acc1' ;
        objAcc1.MID__c = '12345654321' ;
		list_Account.add(objAcc1) ;
		insert list_Account ;
		//----------------New Account Team--------------------
		List<V2_Account_Team__c> list_AccountTeam = new List<V2_Account_Team__c>() ;
		V2_Account_Team__c objAccTeam1 = new V2_Account_Team__c() ;
		objAccTeam1.V2_Account__c = objAcc1.Id ;
		objAccTeam1.V2_BatchOperate__c = '删除' ;
		objAccTeam1.V2_ApprovalStatus__c = '待审批' ;
		objAccTeam1.V2_User__c = use1.Id ;
		list_AccountTeam.add(objAccTeam1) ;
		V2_Account_Team__c objAccTeam2 = new V2_Account_Team__c() ;
		objAccTeam2.V2_Account__c = objAcc1.Id ;
		objAccTeam2.V2_BatchOperate__c = '删除' ;
		objAccTeam2.V2_ApprovalStatus__c = '审批拒绝' ;
		objAccTeam2.V2_User__c = use2.Id ;
		list_AccountTeam.add(objAccTeam2) ;
		V2_Account_Team__c objAccTeam3 = new V2_Account_Team__c() ;
		objAccTeam3.V2_Account__c = objAcc1.Id ;
		objAccTeam3.V2_BatchOperate__c = '删除' ;
		objAccTeam3.V2_ApprovalStatus__c = '审批通过' ;
		objAccTeam3.V2_ImmediateDelete__c = true ;
		objAccTeam3.V2_User__c = use3.Id ;
		list_AccountTeam.add(objAccTeam3) ;
		insert list_AccountTeam ;
		//----------------New AccountTeamM--------------------
		List<AccountTeamMember> list_AccTeamM = new List<AccountTeamMember>() ;
		AccountTeamMember objAccTeamM1 = new AccountTeamMember() ;
		objAccTeamM1.AccountId = objAcc1.Id ;
		objAccTeamM1.UserId = use2.Id ;
		list_AccTeamM.add(objAccTeamM1) ;
		AccountTeamMember objAccTeamM2 = new AccountTeamMember() ;
		objAccTeamM2.AccountId = objAcc1.Id ;
		objAccTeamM2.UserId = use3.Id ;
		list_AccTeamM.add(objAccTeamM2) ;
		AccountTeamMember objAccTeamM3 = new AccountTeamMember() ;
		objAccTeamM3.AccountId = objAcc1.Id ;
		objAccTeamM3.UserId = use1.Id ;
		list_AccTeamM.add(objAccTeamM3) ;
		insert list_AccTeamM ;
		
		//----------------Start Test--------------------
		system.test.startTest() ;
		update list_AccountTeam ;
		List<AccountTeamMember> list_accTeam = [Select Id From AccountTeamMember Where AccountId =: objAcc1.Id] ;
		//system.assertEquals(2, list_accTeam.size()) ;
		system.test.stopTest() ;
		
    }
    
    static testMethod void TestChangeAccountTeam() 
    {
    	//----------------Create User-------------
    	List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
        //----------------New Account--------------------
        List<Account> list_Account = new List<Account>() ;
        Account objAcc1 = new Account() ;
        objAcc1.Name = 'test acc1' ;
        objAcc1.MID__c = '12345654321' ;
		list_Account.add(objAcc1) ;
		insert list_Account ;
		//----------------New Account Team--------------------
		List<V2_Account_Team__c> list_AccountTeam = new List<V2_Account_Team__c>() ;
		V2_Account_Team__c objAccTeam1 = new V2_Account_Team__c() ;
		objAccTeam1.V2_Account__c = objAcc1.Id ;
		objAccTeam1.V2_BatchOperate__c = '替换' ;
		objAccTeam1.V2_ApprovalStatus__c = '待审批' ;
		objAccTeam1.V2_Effective_NewYear__c = '2012' ;
		objAccTeam1.V2_Effective_NewMonth__c = '2';
		objAccTeam1.V2_User__c = use1.Id ;
		list_AccountTeam.add(objAccTeam1) ;
		V2_Account_Team__c objAccTeam2 = new V2_Account_Team__c() ;
		objAccTeam2.V2_Account__c = objAcc1.Id ;
		objAccTeam2.V2_BatchOperate__c = '替换' ;
		objAccTeam2.V2_ApprovalStatus__c = '审批拒绝' ;
		objAccTeam2.V2_Effective_NewYear__c = '2012' ;
		objAccTeam2.V2_Effective_NewMonth__c = '1';
		objAccTeam2.V2_User__c = use2.Id ;
		list_AccountTeam.add(objAccTeam2) ;
		V2_Account_Team__c objAccTeam3 = new V2_Account_Team__c() ;
		objAccTeam3.V2_Account__c = objAcc1.Id ;
		objAccTeam3.V2_BatchOperate__c = '替换' ;
		objAccTeam3.V2_ApprovalStatus__c = '审批通过' ;
		objAccTeam3.V2_Effective_NewYear__c = '2012' ;
		objAccTeam3.V2_Effective_NewMonth__c = '1';
		//objAccTeam3.V2_ImmediateDelete__c = true ;
		objAccTeam3.V2_User__c = use3.Id ;
		objAccTeam3.V2_NewAccUser__c = use3.Id ;
		list_AccountTeam.add(objAccTeam3) ;
		insert list_AccountTeam ;
		//----------------New AccountTeamM--------------------
		List<AccountTeamMember> list_AccTeamM = new List<AccountTeamMember>() ;
		AccountTeamMember objAccTeamM1 = new AccountTeamMember() ;
		objAccTeamM1.AccountId = objAcc1.Id ;
		objAccTeamM1.UserId = use2.Id ;
		//list_AccTeamM.add(objAccTeamM1) ;
		AccountTeamMember objAccTeamM2 = new AccountTeamMember() ;
		objAccTeamM2.AccountId = objAcc1.Id ;
		objAccTeamM2.UserId = use3.Id ;
		list_AccTeamM.add(objAccTeamM2) ;
		AccountTeamMember objAccTeamM3 = new AccountTeamMember() ;
		objAccTeamM3.AccountId = objAcc1.Id ;
		objAccTeamM3.UserId = use1.Id ;
		list_AccTeamM.add(objAccTeamM3) ;
		insert list_AccTeamM ;
		
		//----------------Start Test--------------------
		system.test.startTest() ;
		update list_AccountTeam ;
		List<AccountTeamMember> list_accTeam = [Select Id From AccountTeamMember Where AccountId =: objAcc1.Id] ;
		system.assertEquals(2, list_accTeam.size()) ;
		system.test.stopTest() ;
		
    }
}