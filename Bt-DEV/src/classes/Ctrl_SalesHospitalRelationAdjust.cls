/**
 * Author: Sunny
 * 调整销售医院关系页面控制器
 * 1.主管可以在此页面调整销售医院关系（收到日期的限制）
 * 2.管理员可以在此页面不受限制的调整销售医院关系
 * 3.调整时，如果该医院有存货病人或有指标或当月有报过新病人的，只能转给主管或者其他销售
 * BILL ADD 2013-8-14
 * 医院指标，现在系统已经有了医院的指标数据，不需要新建对象来做。
 * MD的医院指标：看本财年该医院，GBU为MD的销售数据是否有目标销量（只要本财年有指标就可以，不需要看当前月）
 * RENAL的医院指标：看本财年该医院的病人关爱数据，Surviving Target，newpatient字段是否有值。
 * 不同的BU在做销售医院关系调整时，会有不同的新病人、存活病人、医院指标的限制，下图为ceci提供不同bu的限制：
 * BU 限制权限
 * IVT 只看目标销量
 * SP 只看目标销量
 * PD 目标销量，新病人，有存活病人
 * HD 都不需要看
**/
public class Ctrl_SalesHospitalRelationAdjust {
    public List<SelectOption> listSubUser{get;set;}//原成员列表
    public List<SelectOption> listOperate{get{//操作列表
       List<SelectOption> list_so = new List<SelectOption>();
       list_so.add(new SelectOption( '新增' , '新增'));
       list_so.add(new SelectOption( '替换' , '替换'));
       list_so.add(new SelectOption( '删除' , '删除'));
       return list_so;
    }set;}
    public String SelectedOperate{get;set;}//选择的操作
    public Boolean EnableAddMember{get;set;}//是否显示新增页面
    public Boolean EnableReplaceMember{get;set;}//是否显示替换页面
    public Boolean EnableDeleteMember{get;set;}//是否显示删除页面
    public Boolean EnablePage{get;set;}//是否显示页面
    public Boolean blnReplace{get;set;}//是否选择替换原成员
    public String SelectedSubUser{get;set;}//选择的被替换的成员
    public String strWarningMsg{get;set;}
    public Boolean blnShowWarning{get;set;}
    public V2_Account_Team__c objAccountTeam{get;set;}
    private Boolean isBuAdmin ;//是否为admin
    private Set<ID> set_SubUserIds ;//若当前用户为主管，其下属用户id集合
    private ID HostipalId ;//医院ID
    private Map<ID,V2_Account_Team__c> Map_SalesAccTeam = new Map<ID,V2_Account_Team__c>();
    public void SelectOperate(){//根据所选择的操作，初始化页面
        EnableAddMember = false;
        EnableReplaceMember = false;
        EnableDeleteMember = false;
        system.debug('In Select Oper :'+SelectedOperate);
        if(SelectedOperate == '替换'){//替换
            EnableReplaceMember = true;
        }else if(SelectedOperate == '删除'){//删除
            EnableDeleteMember = true;
        }else{//新增，默认显示新增
            SelectedOperate = '新增';
            EnableAddMember = true;
        }
    }
    public Ctrl_SalesHospitalRelationAdjust(Apexpages.Standardcontroller controller){//构造方法
        EnablePage = true;
        HostipalId = ApexPages.currentPage().getParameters().get('HosId');
        if(HostipalId == null){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '必须指定一个医院。');            
            ApexPages.addMessage(msg);
            EnablePage = false;
            return;
        }
        listSubUser = new  List<SelectOption>();
        objAccountTeam = new V2_Account_Team__c();
        this.initCurrentUser();
        if(!isBuAdmin){
            //初始化下属信息
            this.initSubUser();
        }else{
            this.initCurrentSubUserList();
        }
        
        this.checkHospatil();
        if(strWarningMsg != null){
            strWarningMsg = strWarningMsg + '<br/>' + '因此该销售医院关系只能转移给其他主管或者销售。' ;
            blnShowWarning = true ;
        }else{
            blnShowWarning = false;
        }
        SelectedOperate = '新增';
        this.SelectOperate();
    }
    /**
    *初始化当前用户信息
    **/
    private void initCurrentUser(){
        User objuser = [SELECT ID,Profile.Name,UserRole.Name from User WHERE Id =: UserInfo.getUserId()] ;
        if(objuser.Profile.Name.toUpperCase().contains('ADMIN') || objuser.Profile.Name=='系统管理员'){
            isBuAdmin = true;
        }else{
            isBuAdmin = false ;
        }
        if(objuser.UserRole.Name == null && !isBuAdmin){
        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '无法确定您的角色。');            
            ApexPages.addMessage(msg);
            EnablePage = false;
        }
        if(objuser.UserRole.Name != null && objuser.UserRole.Name.toUpperCase().contains('REP')){
        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '您没有权限进行此操作。');            
            ApexPages.addMessage(msg);
        	EnablePage = false;
        }
    }
    /**
    *初始化主管下属信息
    **/
    private void initSubUser(){
        V2_UtilClass cls = new V2_UtilClass();
        //主管下属，包含下属的下属
        set_SubUserIds = cls.GetUserSubIds();
        //包含自己
        set_SubUserIds.add(UserInfo.getUserId());
        this.initCurrentSubUserList();
    }
    /**
    *初始化当前已在销售医院关系内的下属
    *若当前用户为管理员则显示所有有效的销售医院关系
    **/
    private void initCurrentSubUserList(){
        for(V2_Account_Team__c objAT : [select id,V2_User__c,V2_User__r.Name,V2_Role__c,V2_Is_Delete__c,V2_ApprovalStatus__c,
                   V2_LastAccessDate__c,V2_Effective_Year__c,V2_Effective_Month__c,V2_ImmediateDelete__c,EffectiveDate__c
                   from V2_Account_Team__c 
                   where V2_Account__c =: HostipalId And V2_History__c = false]){
            //判断是否有下属处于审批中
            if(objAT.V2_ApprovalStatus__c=='审批中'){
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '请在审批结束后添加小组成员。');            
                ApexPages.addMessage(msg);
                EnablePage = false;
                return;
            }
            //判断是否存在下属
            if(!objAT.V2_Is_Delete__c){
                Map_SalesAccTeam.put(objAT.V2_User__c,objAT);
                //ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO , '您在该医院下已存在下属，请选择是否替换已存在的下属。');            
                //ApexPages.addMessage(msg);
                if(this.isBuAdmin){
                    listSubUser.add(new SelectOption(String.valueOf(objAT.V2_User__c),String.valueOf(objAT.V2_User__r.Name))) ;
                }else{
                    if(set_SubUserIds.contains(objAT.V2_User__c)){
                        listSubUser.add(new SelectOption(String.valueOf(objAT.V2_User__c),String.valueOf(objAT.V2_User__r.Name))) ;
                    }
                }
            }
        }
    }
    /**
    *取消按钮
    **/
    public PageReference doCancel(){
        return new PageReference('/'+HostipalId) ;
    }
    /**
    *保存按钮
    **/
    public PageReference doSave(){
        //检查信息是否填写完整
        if(this.SelectedOperate == '新增' || this.SelectedOperate == '替换'){
            if(this.objAccountTeam.V2_User__c == null){
                objAccountTeam.V2_User__c.addError('请填写新成员。');
                return null;
            }
        }
        if(this.SelectedOperate == '替换' || this.SelectedOperate == '删除'){
            if(SelectedSubUser == null){
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '请选择被'+SelectedOperate+'人');            
                ApexPages.addMessage(msg);
                return null;
            }
        }
        if(this.objAccountTeam.EffectiveDate__c == null){
            objAccountTeam.EffectiveDate__c.addError('请填写日期。');
            return null;
        }
        if(this.objAccountTeam.V2_AdjustReson__c == null){
            objAccountTeam.V2_AdjustReson__c.addError('请填写调整原因。');
            return null;
        }
        //判断所填写的日期
        String strErrorMsg = this.checkEffectiveDate(this.objAccountTeam.EffectiveDate__c , this.SelectedOperate);
        if(strErrorMsg != null){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, strErrorMsg);            
            ApexPages.addMessage(msg);
            return null;
        }
        if(this.SelectedOperate == '新增' || this.SelectedOperate == '替换'){
            
            //若填写的新成员不是当前用户的下属
            if(!this.isBuAdmin && !set_SubUserIds.contains(objAccountTeam.V2_User__c)){
                objAccountTeam.V2_User__c.addError('只能添加您自己的下属。') ;
                return null ;
            }
            //若填写的新成员已经在该医院下了
            if(this.Map_SalesAccTeam.containsKey(objAccountTeam.V2_User__c)){
                objAccountTeam.V2_User__c.addError('您选择用户已在该医院下了。') ;
                return null ;
            }
            //检查同一产品是否有多人负责
            if(checkProduct()){
                objAccountTeam.V2_User__c.addError('同一个产品不能有多人负责。') ;
                return null ;
            }
        }else if(this.SelectedOperate == '删除'){
            //删除操作需要验证是否可以删除，即如果医院有指标、新病人、存货病人则不可以删除代表
            if(blnShowWarning){
            	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '原代表所在部门对于该医院不允许删除操作，您只能将销售医院关系转给其他主管或销售。');            
	            ApexPages.addMessage(msg);
	            return null;
            }
        }
        
        Id ApprovalId ;//提交审批的记录ID
        if(this.SelectedOperate == '新增'){
            objAccountTeam.V2_Account__c = this.HostipalId ;
            objAccountTeam.V2_NewAccUser__c = objAccountTeam.V2_User__c;
            objAccountTeam.V2_BatchOperate__c='新增';
            objAccountTeam.V2_ApprovalStatus__c='待审批';
            objAccountTeam.ownerid=objAccountTeam.V2_User__c;
            objAccountTeam.V2_Effective_Year__c = String.valueOf(objAccountTeam.EffectiveDate__c.year());
            objAccountTeam.V2_Effective_Month__c = String.valueOf(objAccountTeam.EffectiveDate__c.month());
            insert objAccountTeam ;
            ApprovalId = objAccountTeam.id;
        }else if(this.SelectedOperate == '替换'){
        	
            V2_Account_Team__c objAccTeamDel = this.Map_SalesAccTeam.get(SelectedSubUser);
            //替换操作时，不允许被替换成员的生效日期大于替换操作的生效日期
            system.debug(this.isBuAdmin+'ys'+objAccTeamDel.EffectiveDate__c+'  ns'+objAccountTeam.EffectiveDate__c);
            //2013-8-9,根据ceci要求，管理员也要受此限制
            //if(!this.isBuAdmin && objAccTeamDel.EffectiveDate__c >= objAccountTeam.EffectiveDate__c){
            if(objAccTeamDel.EffectiveDate__c >= objAccountTeam.EffectiveDate__c){
            	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '新的生效日期必须大于原销售医院关系的生效日期。');            
                ApexPages.addMessage(msg);
                return null;
            }
            
                        
            objAccTeamDel.V2_BatchOperate__c='替换';
            objAccTeamDel.V2_ApprovalStatus__c='待审批';
            objAccTeamDel.V2_NewAccUser__c=objAccountTeam.V2_User__c;
            objAccTeamDel.NewEffDate__c = objAccountTeam.EffectiveDate__c;
            objAccTeamDel.V2_Effective_NewYear__c = String.valueOf(objAccountTeam.EffectiveDate__c.year());
            objAccTeamDel.V2_Effective_NewMonth__c = String.valueOf(objAccountTeam.EffectiveDate__c.month());
            objAccTeamDel.DeleteDate__c = objAccountTeam.EffectiveDate__c.addDays(-1);
            //2013-10-23修改，因为QV的问题，写失效年、月需要写成失效日期后一个月的年份、月份
            objAccTeamDel.V2_Delete_Year__c = String.valueOf(objAccTeamDel.DeleteDate__c.addMonths(1).year());
            objAccTeamDel.V2_Delete_Month__c = String.valueOf(objAccTeamDel.DeleteDate__c.addMonths(1).month());
            objAccTeamDel.V2_AdjustReson__c=objAccountTeam.V2_AdjustReson__c;
            ApprovalId = objAccTeamDel.id;
            update objAccTeamDel;
            system.debug('tihuanaaaa'+objAccTeamDel) ;
        }else if(this.SelectedOperate == '删除'){
            V2_Account_Team__c objAccTeamDel = this.Map_SalesAccTeam.get(SelectedSubUser);
            //删除操作时，不允许被替换成员的生效日期大于删除操作的生效日期
            if(!this.isBuAdmin && objAccTeamDel.EffectiveDate__c > objAccountTeam.EffectiveDate__c){
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '您所填写的生效日期，比被删除销售医院关系的生效日期更早。');            
                ApexPages.addMessage(msg);
                return null;
            }
            
            objAccTeamDel.V2_Is_Delete__c = true;
            objAccTeamDel.V2_BatchOperate__c='删除';
            objAccTeamDel.V2_ApprovalStatus__c='待审批';
            //objAccTeamDel.V2_NewAccUser__c=objAccountTeam.V2_User__c;
            objAccTeamDel.DeleteDate__c = objAccountTeam.EffectiveDate__c;
            //2013-10-23修改，因为QV的问题，写失效年、月需要写成失效日期后一个月的年份、月份
            objAccTeamDel.V2_Delete_Year__c = String.valueOf(objAccTeamDel.DeleteDate__c.addMonths(1).year());
            objAccTeamDel.V2_Delete_Month__c = String.valueOf(objAccTeamDel.DeleteDate__c.addMonths(1).month());
            objAccTeamDel.V2_AdjustReson__c=objAccountTeam.V2_AdjustReson__c;
            ApprovalId = objAccTeamDel.id;
            update objAccTeamDel;
        }
        //提交审批
        if(ApprovalId != null)
        {
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            req.setObjectId(ApprovalId);//客户联系人或自定义对象
            Approval.ProcessResult result = Approval.process(req);
        }
        return new PageReference('/'+HostipalId) ;
    }
    /**
    *检查同一产品是否有多人负责
    **/
    private boolean checkProduct(){
        if(objAccountTeam.V2_User__c == null){
            return false;
        }
        User u =[select V2_UserProduct__c from User where id=:objAccountTeam.V2_User__c];
        V2_Account_Team__c[] teams =[select v2_User__c,
            V2_Account__c,V2_History__c,V2_UserProduct__c,UserProduct__c from V2_Account_Team__c 
            where V2_Account__c =: HostipalId  and V2_History__c = false and V2_Is_Delete__c=false
            and UserProduct__c=: u.V2_UserProduct__c And V2_User__c !=: SelectedSubUser]; 
        if(teams.size() > 0){
            return true;
        }else{
            return false;
        }
    }
    /**
    *检查日期
    **/
    private String checkEffectiveDate(Date effDate , String strSelectedOperate){
        if(strSelectedOperate == '新增' || strSelectedOperate == '替换'){
            if(effDate.day() != 1){
                return '新增/替换操作，所填写的生效日期必须为自然月的第一天。' ;
            }
        }else if(strSelectedOperate == '删除'){
            if(effDate.day() != effDate.toStartOfMonth().addMonths(1).addDays(-1).day()){
                return '删除操作，失效日期必须为自然月的最后一天。';
            }
        }
        if(!this.isBuAdmin){
            //判断所填写日期是否为当前月之前的月份
            if(effDate.year() < date.today().year() || (effDate < date.today() && effDate.month() != date.today().month())){//为当月之前的月份
                if(effDate.month() == date.today().addMonths(-1).month()){//当前月的前一月
                    if(date.today().day() > 15){//当前日期是否大于15
                        return '由于您需要调整上月销售医院关系，且本月已过15日，所以您不能在此进行操作，请线下通过BUD批准，并交由admin进行转代表操作。';
                    }else{
                        return null;
                    }
                }else{//历史月份
                    return '销售医院关系历史数据不可修改。';
                }
            }else{//为当月或者当月之后的月份
                if(date.today().day() > 25 && date.today().month() == effDate.month() && date.today().year() == effDate.year()){
                    return '每月25日之后若需要调整本月销售医院关系，需要线下通过BUD审批，并交由admin进行转代表操作。';
                }else{
                    return null;
                }
            }
        }else{
            return null;
        }
        
    }
    private void checkHospatil(){
    	string role = '';
        List<UserRole> URole = [Select Id,Name From UserRole Where Id =: UserInfo.getUserRoleId()];
        if(URole.size() <= 0){
            return;
        }
        role = URole[0].Name;
        checkHospatilByRole(role);
    }
    
 	/**
    *	提取方法，通过传过的角色来判断需要执行那部门的判断
    **/
    private void checkHospatilByRole(string role)
    {
    	String strHaveTargetIVT;
    	String strHaveTargetSP;
    	String strHaveTargetPDSurviving;
    	String strHaveTargetPDNewPatient;
        String strHavePatient;
        String strHaveNewP;
    	if(role.toUpperCase().contains('IVT'))
        {
        	strHaveTargetIVT = checkHaveTargetIVT();
        }
        else If(role.toUpperCase().contains('SP'))
        {
        	strHaveTargetSP = checkHaveTargetSP();
        }
        else If(role.toUpperCase().contains('PD'))
        {
        	strHaveTargetPDSurviving = checkHaveTargetPDSurviving();
        	strHaveTargetPDNewPatient = checkHaveTargetPDNewPatient();
        	strHavePatient = checkHavePatient();
        	strHaveNewP = checkHaveNewPatient();
        }else If(role.toUpperCase().contains('HD'))
        {
        	
        }else{
        	strHaveTargetIVT = checkHaveTargetIVT();
        	strHaveTargetSP = checkHaveTargetSP();
        	strHaveTargetPDSurviving = checkHaveTargetPDSurviving();
        	strHaveTargetPDNewPatient = checkHaveTargetPDNewPatient();
        	strHavePatient = checkHavePatient();
        	strHaveNewP = checkHaveNewPatient();
        }
        if(strHaveTargetIVT != null)
        {
            strWarningMsg =  strHaveTargetIVT;
        }
        if(strHaveTargetSP != null)
        {
            strWarningMsg =  (strWarningMsg==null?'':(strWarningMsg+'<br/>')) + strHaveTargetSP;
        }
        if(strHaveTargetPDSurviving != null)
        {
            strWarningMsg =  (strWarningMsg==null?'':(strWarningMsg+'<br/>')) + strHaveTargetPDSurviving;
        }
        if(strHaveTargetPDNewPatient != null)
        {
        	strWarningMsg =  (strWarningMsg==null?'':(strWarningMsg+'<br/>')) + strHaveTargetPDNewPatient;
        }
        if(strHavePatient != null){
            strWarningMsg = (strWarningMsg==null?'':(strWarningMsg+'<br/>')) + strHavePatient;
        }
        if(strHaveNewP != null){
            strWarningMsg = (strWarningMsg==null?'':(strWarningMsg+'<br/>')) + strHaveNewP;
        }
    }
    
    /**
    *判断是否有存活病人
    **/
    private String checkHavePatient(){
        Date startDate = date.today().toStartOfMonth();
        Date endDate = startDate.addMonths(1).addDays(-1);
        List<PatientCaring__c> list_patientCaring = [Select Id From PatientCaring__c Where Account__c =: this.HostipalId And Surviving_This_Year__c > 0 And Time__c >=: startDate And Time__c <=: endDate];
        if(list_patientCaring.size() > 0){
            return '该医院PD当月有存活病人';
        }else{
            return null;
        }
    }
    /**
    *判断是否有提交新病人
    **/
    private String checkHaveNewPatient(){
        Date startDate = date.today().toStartOfMonth();
        Date endDate = startDate.addMonths(1).addDays(-1);
        List<PatientApply__c> list_PatientApply = [Select Id From PatientApply__c Where IntubationHospital__c =: this.HostipalId And CreatedDate >=: startDate And CreatedDate <=: endDate] ;
        if(list_PatientApply.size() > 0){
            return '该医院PD当月有新报病人';
        }else{
            return null;
        }
    }
    /**
    *判断IVT是否有指标
    *当前年有指标
    **/
    private String checkHaveTargetIVT(){
        List<SalesReport__c> list_IVTTargetHospital = [Select s.Id From SalesReport__c s Where s.Account__c =: this.HostipalId And GBU__c = 'MD' and SBU__c = 'IVT' and Time__c = THIS_YEAR and s.TargetQty__c > 0];
        if(list_IVTTargetHospital.size() > 0){
            return '该医院有IVT医院指标存在';
        }else{
            return null;
        }
    }
    
    /**
    *判断SP是否有指标
    *当前年有指标
    **/
    private String checkHaveTargetSP(){
        List<SalesReport__c> list_SPTargetHospital = [Select s.Id From SalesReport__c s Where s.Account__c =: this.HostipalId And GBU__c = 'MD' and SBU__c = 'SP' and Time__c = THIS_YEAR and s.TargetAmount__c > 0];
        if(list_SPTargetHospital.size() > 0){
            return '该医院有SP医院指标存在';
        }else{
            return null;
        }
    }
    
    /**
    *判断PD是否有存活病人指标
    *当前年有指标
    **/
    private String checkHaveTargetPDSurviving(){
        List<PatientCaring__c> list_PDTargetHospital = [Select Id From PatientCaring__c Where Account__c  =: this.HostipalId And Time__c = THIS_YEAR AND Surviving_Target__c > 0] ;
        if(list_PDTargetHospital.size() > 0){
            return '该医院有PD存活病人指标存在';
        }else{
            return null;
        }
    }
    
    /**
    *判断PD是否有新病人指标
    *当前年有指标
    **/
    private String checkHaveTargetPDNewPatient(){
        List<PatientCaring__c> list_PDTargetHospital = [Select Id From PatientCaring__c Where Account__c  =: this.HostipalId And Time__c = THIS_YEAR AND New_Patient_Target__c >0] ;
        if(list_PDTargetHospital.size() > 0){
            return '该医院有PD新病人指标存在';
        }else{
            return null;
        }
    }
    
    /**
    *管理员修改时，单独处理，很据管理员选择的原来的用户去判断
    **/
    public void CheckedAdminSelect()
    {
    	string role = '';
    	system.debug(SelectedSubUser + 'zgxm1');
        List<User> URole = [Select u.UserRole.Name From User u where Id = : SelectedSubUser];
        if(URole.size() <= 0){
            return;
        }
        role = URole[0].UserRole.Name;
        strWarningMsg = null;
        checkHospatilByRole(role);
        system.debug('zgxmzgxm'+role+'   '+strWarningMsg);
    	if(strWarningMsg != null)
    	{
    		strWarningMsg += '<br/>因此该销售医院关系只能转移给其他主管或者销售。';
    		blnShowWarning = true;
    	}else{
    		blnShowWarning = false;
    	}
    }
    
    
    
    static testMethod void myUnitTest() {
    	List<UserRole> listur= new List<UserRole>();
        UserRole ur1 = new UserRole();
        ur1.Name = 'Renal-Supervisor-华南-PD-Supervisor(U1)';
        listur.add(ur1);
        UserRole ur2 = new UserRole();
        ur2.Name = 'Renal-Rep-华南-Ps-Rep(U2)';
        listur.add(ur2);
        UserRole ur3 = new UserRole();
        ur3.Name = 'Renal-Rep-华南-Pa-Rep(U3)';
        listur.add(ur3);
        insert listur;
        ur2.ParentRoleId = ur1.Id;
        update ur2;
        ur3.ParentRoleId = ur1.Id;
        update ur3;
        //user 
        list<User> user=[Select TimeZoneSidKey,ProfileId,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname, Alias From User Where IsActive = true limit 1];
        List<User> list_user = new List<User>();
        User user0=new User();
        user0.username='user0r2@023.com';
        user0.LastName='user0r2';
        user0.Email='user0r2@023.com';
        user0.Alias=user[0].Alias;
        user0.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user0.ProfileId=UserInfo.getProfileId();
        user0.LocaleSidKey=user[0].LocaleSidKey;
        user0.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user0.EmailEncodingKey=user[0].EmailEncodingKey;
        user0.CommunityNickname='chequ34';
        user0.MobilePhone='02345678902';
        user0.IsActive = true;
        user0.UserRoleId=ur3.Id ;
        list_user.add(user0);
        User user1=new User();
        user1.username='user1r2@123.com';
        user1.LastName='user1r2';
        user1.Email='user1r2@123.com';
        user1.Alias=user[0].Alias;
        user1.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user1.ProfileId=UserInfo.getProfileId();
        user1.LocaleSidKey=user[0].LocaleSidKey;
        user1.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user1.EmailEncodingKey=user[0].EmailEncodingKey;
        user1.CommunityNickname='chequ';
        user1.MobilePhone='12345678912';
        user1.IsActive = true;
        user1.UserRoleId=ur1.Id ;
        list_user.add(user1);
        User user2=new User();
        user2.username='user2r2@223.com';
        user2.LastName='user2r2';
        user2.Email='user2r2@223.com';
        user2.Alias=user[0].Alias;
        user2.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user2.ProfileId=UserInfo.getProfileId();
        user2.LocaleSidKey=user[0].LocaleSidKey;
        user2.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user2.EmailEncodingKey=user[0].EmailEncodingKey;
        user2.CommunityNickname='chequ1';
        user2.MobilePhone='22345678922';
        user2.IsActive = true;
        user2.UserRoleId=ur2.Id ;
        list_user.add(user2);
        insert list_user;
        
        //Account
        Account acc = new Account();
        acc.Name = 'ssdfe';
        insert acc;
        
        //Account Team
        V2_Account_Team__c ateam = new V2_Account_Team__c();
        ateam.V2_Account__c = acc.Id;
        ateam.V2_User__c = user0.Id;
        ateam.EffectiveDate__c = date.today().toStartOfMonth().addMonths(-1);
        ateam.V2_BatchOperate__c='新增';
        ateam.V2_ApprovalStatus__c = '审批通过';
        insert ateam;
        
        
        system.Test.startTest();
        Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(new V2_Account_Team__c());
        Ctrl_SalesHospitalRelationAdjust a1 = new Ctrl_SalesHospitalRelationAdjust(controller);
        
        system.runAs(user2){
        	
        	Apexpages.currentPage().getParameters().put('HosId' , acc.Id);
	        Ctrl_SalesHospitalRelationAdjust a2 = new Ctrl_SalesHospitalRelationAdjust(controller);
	        List<SelectOption> ls=a2.listOperate;
	        a2.initSubUser();
	        
	        a2.SelectedOperate='新增';
	        a2.SelectOperate();
	        a2.doSave();
	        a2.objAccountTeam.V2_User__c = user1.Id;
	        a2.doSave();
	        a2.objAccountTeam.EffectiveDate__c = date.today().toStartOfMonth().addDays(1);
	        a2.doSave();
	        a2.objAccountTeam.V2_AdjustReson__c = 'xxx';
	        a2.doSave();
	        a2.objAccountTeam.EffectiveDate__c = date.today().toStartOfMonth();
            a2.doSave();
	        
	        /*
	        Apexpages.currentPage().getParameters().put('HosId' , acc.Id);
            Ctrl_SalesHospitalRelationAdjust a3 = new Ctrl_SalesHospitalRelationAdjust(controller);
	        a3.SelectedOperate='替换';
	        a3.SelectOperate();
	        a3.objAccountTeam.V2_User__c = user1.Id;
	        //a3.doSave();
	        system.debug(user0.Id+'****44'+a3.SelectedSubUser);
	        a3.SelectedSubUser = user0.Id;
	        system.debug('****44'+a3.SelectedSubUser);
            a3.objAccountTeam.EffectiveDate__c = date.today().toStartOfMonth();
            a3.objAccountTeam.V2_AdjustReson__c = 'xxx';
            a3.doSave();
            
            
            Apexpages.currentPage().getParameters().put('HosId' , acc.Id);
            Ctrl_SalesHospitalRelationAdjust a4 = new Ctrl_SalesHospitalRelationAdjust(controller);
            a4.SelectedOperate='删除';
            a4.SelectOperate();
            a4.SelectedSubUser = user0.Id;
            a3.objAccountTeam.EffectiveDate__c = date.today().toStartOfMonth().addMonths(1).addDays(-1);
            a3.objAccountTeam.V2_AdjustReson__c = 'xxx';
            a4.doSave();
            
            a4.doCancel();
            */
        }
        system.Test.stopTest();
    }
    static testMethod void myUnitTest2() {
    	List<UserRole> listur= new List<UserRole>();
        UserRole ur1 = new UserRole();
        ur1.Name = 'Renal-Supervisor-华南-PD-Supervisor(U1)';
        listur.add(ur1);
        UserRole ur2 = new UserRole();
        ur2.Name = 'Renal-Rep-华南-Ps-Rep(U2)';
        listur.add(ur2);
        UserRole ur3 = new UserRole();
        ur3.Name = 'Renal-Rep-华南-Pa-Rep(U3)';
        listur.add(ur3);
        insert listur;
        ur2.ParentRoleId = ur1.Id;
        update ur2;
        ur3.ParentRoleId = ur1.Id;
        update ur3;
        //user 
        list<User> user=[Select TimeZoneSidKey,ProfileId,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname, Alias From User Where IsActive = true limit 1];
        List<User> list_user = new List<User>();
        User user0=new User();
        user0.username='user0r2@023.com';
        user0.LastName='user0r2';
        user0.Email='user0r2@023.com';
        user0.Alias=user[0].Alias;
        user0.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user0.ProfileId=UserInfo.getProfileId();
        user0.LocaleSidKey=user[0].LocaleSidKey;
        user0.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user0.EmailEncodingKey=user[0].EmailEncodingKey;
        user0.CommunityNickname='chequ34';
        user0.MobilePhone='02345678902';
        user0.IsActive = true;
        user0.UserRoleId=ur3.Id ;
        list_user.add(user0);
        User user1=new User();
        user1.username='user1r2@123.com';
        user1.LastName='user1r2';
        user1.Email='user1r2@123.com';
        user1.Alias=user[0].Alias;
        user1.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user1.ProfileId=UserInfo.getProfileId();
        user1.LocaleSidKey=user[0].LocaleSidKey;
        user1.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user1.EmailEncodingKey=user[0].EmailEncodingKey;
        user1.CommunityNickname='chequ';
        user1.MobilePhone='12345678912';
        user1.IsActive = true;
        user1.UserRoleId=ur1.Id ;
        list_user.add(user1);
        User user2=new User();
        user2.username='user2r2@223.com';
        user2.LastName='user2r2';
        user2.Email='user2r2@223.com';
        user2.Alias=user[0].Alias;
        user2.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user2.ProfileId=UserInfo.getProfileId();
        user2.LocaleSidKey=user[0].LocaleSidKey;
        user2.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user2.EmailEncodingKey=user[0].EmailEncodingKey;
        user2.CommunityNickname='chequ1';
        user2.MobilePhone='22345678922';
        user2.IsActive = true;
        user2.UserRoleId=ur2.Id ;
        list_user.add(user2);
        insert list_user;
        
        //Account
        Account acc = new Account();
        acc.Name = 'ssdfe';
        insert acc;
        
        //Account Team
        V2_Account_Team__c ateam = new V2_Account_Team__c();
        ateam.V2_Account__c = acc.Id;
        ateam.V2_User__c = user0.Id;
        ateam.EffectiveDate__c = date.today().toStartOfMonth().addMonths(-1);
        ateam.V2_BatchOperate__c='新增';
        ateam.V2_ApprovalStatus__c = '审批通过';
        insert ateam;
        
        system.Test.startTest();
        Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(new V2_Account_Team__c());
        system.runAs(user2){
        	Apexpages.currentPage().getParameters().put('HosId' , acc.Id);
            Ctrl_SalesHospitalRelationAdjust a3 = new Ctrl_SalesHospitalRelationAdjust(controller);
            a3.SelectedOperate='替换';
            a3.SelectOperate();
            a3.objAccountTeam.V2_User__c = user1.Id;
            //a3.doSave();
            system.debug(user0.Id+'****44'+a3.SelectedSubUser);
            a3.SelectedSubUser = user0.Id;
            system.debug('****44'+a3.SelectedSubUser);
            a3.objAccountTeam.EffectiveDate__c = date.today().toStartOfMonth();
            a3.objAccountTeam.V2_AdjustReson__c = 'xxx';
            a3.doSave();
        }
        system.test.stopTest();
    }
    static testMethod void myUnitTest3() {
        List<UserRole> listur= new List<UserRole>();
        UserRole ur1 = new UserRole();
        ur1.Name = 'Renal-Supervisor-华南-PD-Supervisor(U1)';
        listur.add(ur1);
        UserRole ur2 = new UserRole();
        ur2.Name = 'Renal-Rep-华南-Ps-Rep(U2)';
        listur.add(ur2);
        UserRole ur3 = new UserRole();
        ur3.Name = 'Renal-Rep-华南-Pa-Rep(U3)';
        listur.add(ur3);
        insert listur;
        ur2.ParentRoleId = ur1.Id;
        update ur2;
        ur3.ParentRoleId = ur1.Id;
        update ur3;
        //user 
        list<User> user=[Select TimeZoneSidKey,ProfileId,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname, Alias From User Where IsActive = true limit 1];
        List<User> list_user = new List<User>();
        User user0=new User();
        user0.username='user0r2@023.com';
        user0.LastName='user0r2';
        user0.Email='user0r2@023.com';
        user0.Alias=user[0].Alias;
        user0.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user0.ProfileId=UserInfo.getProfileId();
        user0.LocaleSidKey=user[0].LocaleSidKey;
        user0.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user0.EmailEncodingKey=user[0].EmailEncodingKey;
        user0.CommunityNickname='chequ34';
        user0.MobilePhone='02345678902';
        user0.IsActive = true;
        user0.UserRoleId=ur3.Id ;
        list_user.add(user0);
        User user1=new User();
        user1.username='user1r2@123.com';
        user1.LastName='user1r2';
        user1.Email='user1r2@123.com';
        user1.Alias=user[0].Alias;
        user1.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user1.ProfileId=UserInfo.getProfileId();
        user1.LocaleSidKey=user[0].LocaleSidKey;
        user1.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user1.EmailEncodingKey=user[0].EmailEncodingKey;
        user1.CommunityNickname='chequ';
        user1.MobilePhone='12345678912';
        user1.IsActive = true;
        user1.UserRoleId=ur1.Id ;
        list_user.add(user1);
        User user2=new User();
        user2.username='user2r2@223.com';
        user2.LastName='user2r2';
        user2.Email='user2r2@223.com';
        user2.Alias=user[0].Alias;
        user2.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user2.ProfileId=UserInfo.getProfileId();
        user2.LocaleSidKey=user[0].LocaleSidKey;
        user2.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user2.EmailEncodingKey=user[0].EmailEncodingKey;
        user2.CommunityNickname='chequ1';
        user2.MobilePhone='22345678922';
        user2.IsActive = true;
        user2.UserRoleId=ur2.Id ;
        list_user.add(user2);
        insert list_user;
        
        //Account
        Account acc = new Account();
        acc.Name = 'ssdfe';
        insert acc;
        
        //Account Team
        V2_Account_Team__c ateam = new V2_Account_Team__c();
        ateam.V2_Account__c = acc.Id;
        ateam.V2_User__c = user0.Id;
        ateam.EffectiveDate__c = date.today().toStartOfMonth().addMonths(-1);
        ateam.V2_BatchOperate__c='新增';
        ateam.V2_ApprovalStatus__c = '审批通过';
        insert ateam;
        
        system.Test.startTest();
        Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(new V2_Account_Team__c());
        system.runAs(user2){
            Apexpages.currentPage().getParameters().put('HosId' , acc.Id);
            Ctrl_SalesHospitalRelationAdjust a4 = new Ctrl_SalesHospitalRelationAdjust(controller);
            a4.SelectedOperate='删除';
            a4.SelectOperate();
            a4.SelectedSubUser = user0.Id;
            a4.objAccountTeam.EffectiveDate__c = date.today().toStartOfMonth().addMonths(1).addDays(-1);
            a4.objAccountTeam.V2_AdjustReson__c = 'xxx';
            a4.doSave();
            
            a4.doCancel();
        }
        system.test.stopTest();
    }
    
}