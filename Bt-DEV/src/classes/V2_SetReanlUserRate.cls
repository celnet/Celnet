/**
设置每个用户的联系人权重
harvery 2011.12.26 creat
2011-12-27 Scott 修改
1.只能为其角色下属添加肾科用户权重，管理员可以为任何人
2.如果所选用户不是其下属则给出提示信息并将其下属名单列出
3.修改各个数值要保证总和不超过100
4.如果查询用户不存在数据则默认为 权重默认值
*/
public with sharing class V2_SetReanlUserRate {
	//标准控制器；
   // ApexPages.StandardController con{get;set;}
    //默认设置-医生
    public V2_ContactDefaultValue__c DefaultValue1{get;set;}
    //默认设置-护士
    public V2_ContactDefaultValue__c DefaultValue2{get;set;}
    //默认设置-行政
    public V2_ContactDefaultValue__c DefaultValue3{get;set;}
    //默认设置 集合
    public List<V2_ContactDefaultValue__c> lstDefaultValue{get;set;}
    //查找用户
    public RenalUserRate__c inputUser{get;set;}
    //显示肾科用户权重
    public List<RenalUserRate__c> RenalUserRlist{get;set;}
    public Boolean IsNew{get;set;}
    //如果是新加记录显示提示信息
    public String divstyle{get;set;}
    //当前用户所有下属的Id
    public Set<Id> Subordinateids = new Set<Id>();
    public List<String> SubordinateNames = new List<String>();
    //判断是否为管理员
    public boolean IsAdmin{get;set;}
     /**
 	 标准构造器
	*/
	public V2_SetReanlUserRate(ApexPages.StandardController p_con)
	{
		divstyle = 'display:none';
		IsNew = false;
		IsAdmin= true;
		inputUser = new RenalUserRate__c();
		
		
    	//获取所有设置
    	lstDefaultValue= [Select v.Id, v.Name, v.OwnerId, 
    	v.SystemModstamp, v.V2_ContactType__c, 
    	v.V2_Education__c, v.V2_RateBaxBusiness__c, 
    	v.V2_RateBaxRelationship__c, v.V2_RateBeds__c, 
    	v.V2_RateExperience__c, v.V2_RateGeology__c, 
    	v.V2_RateHospGrade__c, v.V2_RateLeadership__c, 
    	v.V2_RatePatientDeliver__c, v.V2_RatePDCenter__c, 
    	v.V2_RatePDImpPatients__c, v.V2_RatePDPatients__c, 
    	v.V2_RateTech__c, v.V2_RateTitle__c, 
    	v.V2_TotalScore__c from V2_ContactDefaultValue__c v
     	where v.V2_ContactType__c in ('医生','护士','行政')
    	];
    	if(lstDefaultValue==null || lstDefaultValue.size()==0 ||lstDefaultValue.size()!=3){
    		 ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning , '联系人默认权重取得失败，请联系管理员。错误：数据条数不等于3！');            
             ApexPages.addMessage(msg);
             return;
    	}
    	//分别得到医生，护士，行政的设置；
    	for(V2_ContactDefaultValue__c temp : lstDefaultValue){
    		if(temp.V2_ContactType__c=='医生'){
    			DefaultValue1 = temp;
    		} else if(temp.V2_ContactType__c=='护士'){ 
    			DefaultValue2 = temp;
    		} else if (temp.V2_ContactType__c=='行政'){
    			DefaultValue3 = temp;
    		}
    	}
    	//判断是否获取到医生，护士，行政的设置；
    	if(DefaultValue1==null){
    		 ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning , '联系人默认权重取得失败，请联系管理员。错误：未获取到医生的默认值设置！');            
             ApexPages.addMessage(msg);
             return ;
    	}
    	if(DefaultValue2==null){
    		 ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning , '联系人默认权重取得失败，请联系管理员。错误：未获取到护士的默认值设置！');            
             ApexPages.addMessage(msg);
             return ;
    	}
     	if(DefaultValue3==null){
    		 ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning , '联系人默认权重取得失败，请联系管理员。错误：未获取到行政的默认值设置！');            
             ApexPages.addMessage(msg);
             return;
    	}
    	
    	//查询当前用户下属
    	Profile p = [Select Name From Profile where Id=:UserInfo.getProfileId()];
    	if(p.Name =='系统管理员' || p.Name =='System Administrator')
    	{
    		IsAdmin =false;
    	}
    	else if(UserInfo.getUserRoleId()!=null)
    	{
    		SubordinateInfo();
    	}
    }
    //查询
    public void SearchRenalUserRate()
    {
    	try 
    	{
    		//判读是否是管理员
    		if(IsAdmin)
    		{
    			//没有下属
    			if(Subordinateids == null || Subordinateids.size()==0)
    			{
    				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning ,'没有可供您设置的属下，请您核实后再操作！');            
           			ApexPages.addMessage(msg);
            	    return;
    			}
    			else if(!Subordinateids.contains(inputUser.UserSalesRep__c))
    			{
    				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning ,'所选用户不是您的属下，您只能选择您的下属： '+SubordinateNames+' 其中一人。');            
           			ApexPages.addMessage(msg);
            	    return;
    			}
    		}
    		RenalUserRlist = [select ContactType__c,HospGrade__c,Geology__c,Leadership__c,Title__c,Education__c,BaxRelationship__c,
	    					  PatientDeliver__c,PDPatients__c,Experience__c,PDImpPatients__c,Beds__c,BaxBusiness__c,PDCenter__c,
	    					  PDTech__c,Total_score__c from RenalUserRate__c where UserSalesRep__c =: inputUser.UserSalesRep__c];
	    	if(RenalUserRlist == null || RenalUserRlist.size()==0)
	    	{
	    		divstyle ='display:block;color: #FF0000';
	    		IsNew = true;
	    		RenalUserRlist = new List<RenalUserRate__c>();
	    		for(V2_ContactDefaultValue__c cdv:lstDefaultValue)
	    		{
	    			RenalUserRate__c rurate = new RenalUserRate__c();
	    			rurate.ContactType__c = cdv.V2_ContactType__c;
	    			rurate.HospGrade__c= cdv.V2_RateHospGrade__c;
	    			rurate.Geology__c= cdv.V2_RateGeology__c;
	    			rurate.Leadership__c= cdv.V2_RateLeadership__c; 
	    			rurate.Title__c= cdv.V2_RateTitle__c;
	    			rurate.Education__c= cdv.V2_Education__c;
	    			rurate.BaxRelationship__c= cdv.V2_RateBaxRelationship__c;
	    			rurate.PatientDeliver__c= cdv.V2_RatePatientDeliver__c;
	    			rurate.PDPatients__c= cdv.V2_RatePDPatients__c;
	    			rurate.Experience__c= cdv.V2_RateExperience__c;
	    			rurate.PDImpPatients__c= cdv.V2_RatePDImpPatients__c;
	    			rurate.Beds__c = cdv.V2_RateBeds__c;
	    			rurate.BaxBusiness__c= cdv.V2_RateBaxBusiness__c;
	    			rurate.PDCenter__c= cdv.V2_RatePDCenter__c;
	    			rurate.PDTech__c= cdv.V2_RateTech__c;
	    			rurate.UserSalesRep__c = inputUser.UserSalesRep__c;
	    			RenalUserRlist.add(rurate);
	    		}
	    	}
	    	else
	    	{
	    		divstyle = 'display:none';
	    	}
    	}catch(Exception e)
    	{
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR , String.valueOf(e)+' 第'+e.getLineNumber()+'行');            
            ApexPages.addMessage(msg);
            return;
    	}
    }
    //保存
    public void SaveRenalUserRate()
    {
    	try
    	{
    		if(RenalUserRlist != null && RenalUserRlist.size()>0)
	    	{
	    		double sum1 = 0;
	    		double sum2 = 0;
	    		double sum3 = 0;
	    		for(RenalUserRate__c rur:RenalUserRlist)
	    		{
	    			if(rur.ContactType__c =='医生')
	    			{
	    				sum1 = rur.PatientDeliver__c + rur.BaxBusiness__c + rur.PDImpPatients__c + rur.Beds__c + rur.Geology__c + rur.Experience__c + rur.PDCenter__c + rur.PDPatients__c + rur.Education__c + rur.PDTech__c + rur.HospGrade__c + rur.Leadership__c + rur.BaxRelationship__c + rur.Title__c;
	    			}
	    			if(rur.ContactType__c =='护士')
	    			{
	    				sum2 = rur.PatientDeliver__c + rur.BaxBusiness__c + rur.PDImpPatients__c + rur.Beds__c + rur.Geology__c + rur.Experience__c + rur.PDCenter__c + rur.PDPatients__c + rur.Education__c + rur.PDTech__c + rur.HospGrade__c + rur.Leadership__c + rur.BaxRelationship__c + rur.Title__c;
	    			}
	    			if(rur.ContactType__c =='行政')
	    			{
	    				sum3 = rur.PatientDeliver__c + rur.BaxBusiness__c + rur.PDImpPatients__c + rur.Beds__c + rur.Geology__c + rur.Experience__c + rur.PDCenter__c + rur.PDPatients__c + rur.Education__c + rur.PDTech__c + rur.HospGrade__c + rur.Leadership__c + rur.BaxRelationship__c + rur.Title__c;
	    			}
	    		}
	    		if(sum1 != 100)
	    		{
	    			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning  , '类型为“医生”数据总分为 '+sum1+' 不等于100分，请您核对后再操作！');            
		            ApexPages.addMessage(msg);
		            return;
	    		}
	    		if(sum2 != 100)
	    		{
	    			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning  , '类型为“护士”数据总分为 '+sum2+' 不等于100分，请您核对后再操作！');            
		            ApexPages.addMessage(msg);
		            return;
	    		}
	    		if(sum3 != 100)
	    		{
	    			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning  , '类型为“行政”数据总分为 '+sum3+' 不等于100分，请您核对后再操作！');            
		            ApexPages.addMessage(msg);
		            return;
	    		}
	    		User u = [select Name from User where Id =: inputUser.UserSalesRep__c];
	    		if(IsNew)
	    		{
	    			insert RenalUserRlist;
	    			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM  , '成功插入用户'+u.Name+'的权重信息。');            
		            ApexPages.addMessage(msg);
	    		}
	    		else
	    		{
	    			update RenalUserRlist;
	    			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM  , '成功更新用户'+u.Name+'的权重信息。');            
		            ApexPages.addMessage(msg);
	    		}
	    		IsNew = false;
	    		SearchRenalUserRate();
	    	}
    	}
    	catch(Exception e)
    	{
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR , String.valueOf(e)+' 第'+e.getLineNumber()+'行');            
            ApexPages.addMessage(msg);
            return;
    	}
    }
    //查询当前用户下级信息
    public void SubordinateInfo()
    {
    	try
		{	
			V2_UtilClass Subordinate = new V2_UtilClass();
	    	List<Id>ids = Subordinate.getSubordinateIds(UserInfo.getUserRoleId());
	    	for(Id userid:ids)
	    	{
	    		Subordinateids.add(userid);
	    	}
	    	for(User u:[select Name from User where Id in:Subordinateids])
	    	{
	    		SubordinateNames.add(u.Name);
	    	}
		}catch(Exception e)
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR , String.valueOf(e)+' 第'+e.getLineNumber()+'行');            
            ApexPages.addMessage(msg);
            return;
		}
    }
     /****************************************************Test*********************************/
    static testMethod void V2_SetReanlUserRate()
	{
		/*V2_ContactDefaultValue__c defaultv = new V2_ContactDefaultValue__c();
		//类别
		V2_ContactType__c
		//ESRD进入透析人数
		V2_RatePatientDeliver__c
		//百特业务规模
		V2_RateBaxBusiness__c
		//插管腹透病人数
		V2_RatePDImpPatients__c
		//床位数
		V2_RateBeds__c
		//地理位置
		V2_RateGeology__c
		//腹透经验
		V2_RateExperience__c
		//腹透中心规模
		V2_RatePDCenter__c*/
		
		/*新建用户*/
		//----------------New MarketingRole ------------------
	     UserRole objUserRole = new UserRole() ;
	     objUserRole.Name = 'Renal-Supervisor-华北-PD-Supervisor(杨洪玲)' ;
	     insert objUserRole ;
	     UserRole objUserRole2 = new UserRole() ;
	     objUserRole2.Name = 'Renal-Rep-华北-PD-Rep(张寰宇)' ;
	     objUserRole2.ParentRoleId = objUserRole.Id ;
	     insert objUserRole2 ;
	     
	     UserRole objUserRole3 = new UserRole() ;
	     objUserRole3.Name = 'Renal-Supervisor-华北-PD-Supervisor(杨洪玲)111' ;
	     insert objUserRole3 ;
	     UserRole objUserRole4 = new UserRole() ;
	     objUserRole4.Name = 'Renal-Rep-华北-PD-Rep(张寰宇)111';
	     objUserRole4.ParentRoleId = objUserRole3.Id ;
	     insert objUserRole4 ;
	    //rep简档
	    Profile RepPro = [select Id from Profile where Name  = 'Standard User - Renal Sales Rep' limit 1];
	    //Supr简档
	    Profile SupPro = [select Id from Profile where Name='Standard User - Renal Sales Supervisor' limit 1];
	//----------------Create User-------------
	     List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
         
         User sup = new User();
	     sup.Username='sup@123.com';
	     sup.LastName='sup';
	     sup.Email='sup@123.com';
	     sup.Alias=user[0].Alias;
	     sup.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     sup.ProfileId=SupPro.Id;
	     sup.LocaleSidKey=user[0].LocaleSidKey;
	     sup.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     sup.EmailEncodingKey=user[0].EmailEncodingKey;
	     sup.CommunityNickname='chequ1';
	     sup.MobilePhone='12345678912';
	     sup.UserRoleId = objUserRole2.Id ;
	     sup.IsActive = true;
	     insert sup;
         
         User rep = new User();
	     rep.Username='rep@123.com';
	     rep.LastName='rep';
	     rep.Email='rep@123.com';
	     rep.Alias=user[0].Alias;
	     rep.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     rep.ProfileId=RepPro.Id;
	     rep.LocaleSidKey=user[0].LocaleSidKey;
	     rep.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     rep.EmailEncodingKey=user[0].EmailEncodingKey;
	     rep.CommunityNickname='chequ12222';
	     rep.MobilePhone='12345678912';
	     rep.UserRoleId = objUserRole2.Id ;
	     rep.ManagerId = sup.Id;
	     rep.IsActive = true;
	     insert rep;
	     
	     User rep2 = new User();
	     rep2.Username='rep2@123.com';
	     rep2.LastName='rep2';
	     rep2.Email='rep2@123.com';
	     rep2.Alias=user[0].Alias;
	     rep2.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     rep2.ProfileId=RepPro.Id;
	     rep2.LocaleSidKey=user[0].LocaleSidKey;
	     rep2.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     rep2.EmailEncodingKey=user[0].EmailEncodingKey;
	     rep2.CommunityNickname='chequ122222222';
	     rep2.MobilePhone='12345678912';
	     rep2.UserRoleId = objUserRole4.Id ;
	     rep2.IsActive = true;
	     insert rep2;
	     
	     
		Test.startTest();
		ApexPages.StandardController STcon = new ApexPages.StandardController(new RenalUserRate__c());
		V2_SetReanlUserRate srur = new V2_SetReanlUserRate(STcon);
		srur.inputUser.UserSalesRep__c = rep.Id;
		srur.SearchRenalUserRate();
		srur.SaveRenalUserRate();
		
		srur.inputUser.UserSalesRep__c = rep.Id;
		srur.SearchRenalUserRate();
		for(RenalUserRate__c rur : srur.RenalUserRlist)
		{
			if(rur.ContactType__c == '医生')
			{
				rur.Experience__c = 100;
			}
		}
		try
		{
			srur.SaveRenalUserRate();
		}catch(Exception e)
		{
			System.debug('分数超过100提示'+String.valueOf(e));
		}
		
		srur.inputUser.UserSalesRep__c = rep.Id;
		srur.SearchRenalUserRate();
		for(RenalUserRate__c rur : srur.RenalUserRlist)
		{
			if(rur.ContactType__c == '护士')
			{
				rur.Experience__c = 100;
			}
		}
		try
		{
			srur.SaveRenalUserRate();
		}catch(Exception e)
		{
			System.debug('分数超过100提示'+String.valueOf(e));
		}
		
		srur.inputUser.UserSalesRep__c = rep.Id;
		srur.SearchRenalUserRate();
		for(RenalUserRate__c rur : srur.RenalUserRlist)
		{
			if(rur.ContactType__c == '行政')
			{
				rur.Experience__c = 100;
			}
		}
		try
		{
			srur.SaveRenalUserRate();
		}catch(Exception e)
		{
			System.debug('分数超过100提示'+String.valueOf(e));
		}
		System.runAs(sup)
		{
			V2_SetReanlUserRate srur2 = new V2_SetReanlUserRate(STcon);
			srur2.inputUser.UserSalesRep__c = rep.Id;
			srur2.SearchRenalUserRate();
			srur2.SaveRenalUserRate();
			
			V2_SetReanlUserRate srur3 = new V2_SetReanlUserRate(STcon);
			srur3.inputUser.UserSalesRep__c = rep2.Id;
			try
			{
				srur3.SearchRenalUserRate();
				srur3.SaveRenalUserRate();
			}catch(Exception e)
			{
				
			}
			
		}
		Test.stopTest();
	}      
}