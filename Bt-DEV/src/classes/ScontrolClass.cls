global without sharing class ScontrolClass {
    webService static Boolean isAdmins(){
        User objuser = [SELECT ID,Profile.Name from User WHERE Id =: UserInfo.getUserId()] ;
        if(objuser.Profile.Name.toUpperCase().contains('ADMIN') || objuser.Profile.Name=='系统管理员'){
        	return true;
        }else{
        	return false ;
        }
    }
    webService static Boolean isMarketingAssistant(){
    	User objuser = [SELECT ID,Profile.Name,UserRole.Name from User WHERE Id =: UserInfo.getUserId()] ;
        if(objuser.UserRole.Name.toUpperCase().contains('ASSISTANT') || objuser.Profile.Name=='系统管理员'){
            return true;
        }else{
            return false ;
        }
    }
    webService static Boolean isVaporizerManager(){//Engineer
        User objuser = [SELECT ID,Profile.Name,UserRole.Name from User WHERE Id =: UserInfo.getUserId()] ;
        if(objuser.UserRole.Name.toUpperCase().contains('VAPORIZER') || objuser.Profile.Name=='系统管理员'){
            return true;
        }else{
            return false ;
        }
    }
    /*********************BILL ADD 2013-8-8 start*****************************/
    //获取静态资源的url
    webService static string getStaticResourceId(){//Engineer
        list<StaticResource> res =[Select s.Name, s.LastModifiedDate From StaticResource s where name = 'VisitHelp'];
        if(res != null && res.size()>0)
        {
        	string timestamp = string.valueOf(res[0].LastModifiedDate.getTime());
        	return timestamp;
        }
        return '';
    }
    /*********************BILL ADD 2013-8-8 end*****************************/
    /**************bill add 2013年6月4号 start***********************/
    webService static Boolean isRep(){//rep
        User objuser = [SELECT ID,Profile.Name,UserRole.Name from User WHERE Id =: UserInfo.getUserId()] ;
        if(objuser.UserRole.Name.toUpperCase().contains('Rep')){
            return true;
        }else{
            return false;
        }
    }
    static testMethod void myUnitTest()
    {
    	//ScontrolClass sc = new ScontrolClass();
    	isAdmins();
    	isMarketingAssistant();
    	isVaporizerManager();
    	isRep();
    }
    /**************bill add 2013年6月4号 end***********************/
    /*
    webService static void ContinueApprove(ID VapApproveId){
    	List<Vaporizer_Approve__c> list_VapApprove = [Select v.Vaporizer_ReturnAndMainten__c, v.Vaporizer_Application__c, v.VaporizerAdmin__c, 
    	v.VaporizerAdmin_Approve__c, v.VaporizerAdminDefAgree__c, v.VapAdmin_Comments__c, v.Supervisor__c, v.Supervisor_Comments__c, v.Supervisor_Approve__c, 
    	v.SupervisorDefAgree__c, v.Submitter__c, v.SevoProductManager__c, v.SevoProductManager_Comments__c, v.SevoProductManager_Approve__c, 
    	v.SevoProductManagerDefAgree__c, v.RegionalManager__c, v.RegionalManager_Comments__c, v.RegionalManager_Approve__c, v.RegionalManagerDefAgree__c, 
    	v.NationalDirector__c, v.NationalDirector_Comments__c, v.NationalDirector_Approve__c, v.NationalDirectorDefAgree__c, v.DirectManager__c, v.DirectManager_Comments__c, 
    	v.DirectManager_Approve__c, v.DirectManagerDefAgree__c, v.DesProductManager__c, v.DesProductManager_Comments__c, v.DesProductManager_Approve__c, v.DesProductManagerDefAgree__c, 
    	v.AreaManager__c, v.AreaManager_Comments__c, v.AreaManager_Approve__c, v.AreaManagerDefAgree__c, v.ApproveURL__c, v.ApproveStep__c, v.ApproveResult__c 
    	From Vaporizer_Approve__c v 
    	Where Id =: VapApproveId];
    	if(list_VapApprove.size() > 0){
    		Vaporizer_Approve__c NewVapAppove=list_VapApprove[0].clone(false,true,false,false);
    		if(NewVapAppove.ApproveStep__c == '挥发罐管理员审批'){
    			NewVapAppove.VaporizerAdmin_Approve__c = null;
    			NewVapAppove.VapAdmin_Comments__c = null;
    		}else if(NewVapAppove.ApproveStep__c == '销售主管审批'){
    			NewVapAppove.Supervisor_Approve__c = null;
    			NewVapAppove.Supervisor_Comments__c = null;
    		}else if(NewVapAppove.ApproveStep__c == '地区经理(DSM)审批'){
                NewVapAppove.DirectManager_Approve__c = null;
                NewVapAppove.DirectManager_Comments__c = null;
            }else if(NewVapAppove.ApproveStep__c == '区域经理(ASM)审批'){
                NewVapAppove.AreaManager_Approve__c=null;
                NewVapAppove.AreaManager_Comments__c = null;
            }else if(NewVapAppove.ApproveStep__c == '大区经理(RSM)审批'){
                NewVapAppove.RegionalManager_Approve__c = null;
                NewVapAppove.RegionalManager_Comments__c=null;
            }else if(NewVapAppove.ApproveStep__c == '产品经理审批'){
                NewVapAppove.DesProductManager_Approve__c = NewVapAppove.DesProductManager_Approve__c=='拒绝'?null:NewVapAppove.DesProductManager_Approve__c;
                NewVapAppove.DesProductManager_Comments__c = null;
                NewVapAppove.SevoProductManager_Approve__c = NewVapAppove.SevoProductManager_Approve__c=='拒绝'?null:NewVapAppove.SevoProductManager_Approve__c;
                NewVapAppove.SevoProductManager_Comments__c = null;
            }else if(NewVapAppove.ApproveStep__c == '全国总监审批'){
                NewVapAppove.NationalDirector_Approve__c = null;
                NewVapAppove.NationalDirector_Comments__c = null;
            }
    		
    	}
    }
    */
    
    
}