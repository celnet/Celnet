/**
 * Author : Bill
 * date: 2013-9-9
 * 功能：
 	//SP客户等级计算
	//销量	联系人职务
	//七氟烷潜力销量	主任/副主任	带组医生	住院医师	领/加药人员	采购	    库管	其他职务
	//销量>=5	       V	    A	    A	    A	    A	    A	     A
	//5>销量>=1	       V	    B	    B	    B	    B	    B	     B
	//1>销量>  0	       V  	    B	    C	    C	    D	    D	     C
	//销量=0	           V	    B	    C	    C	    D	    D	          其他
**/
public class SP_AccountLevel {
	public String AccountLevelExec(Double SevoPotentialSale,String job)
	{
		string level = '其他';
		if(job == '主任' || job == '副主任')
		{
			return 'V';
		}
		else if(job == '带组医生')
		{
			if(SevoPotentialSale >= 5)
			{
				return 'A';
			}
			return 'B';
		}
		else if(job == '住院医师' || job == '领/加药人员')
		{
			if(SevoPotentialSale >= 5)
			{
				return 'A';
			}
			else if(SevoPotentialSale < 5 && SevoPotentialSale >= 1)
			{
				return 'B';
			}
			return 'C';
		}
		else if(job == '采购' || job == '库管')
		{
			if(SevoPotentialSale >= 5)
			{
				return 'A';
			}
			else if(SevoPotentialSale < 5 && SevoPotentialSale >= 1)
			{
				return 'B';
			}
			return 'D';
		}else
		{
			if(SevoPotentialSale >= 5)
			{
				return 'A';
			}
			else if(SevoPotentialSale < 5 && SevoPotentialSale >= 1)
			{
				return 'B';
			}
			else if(SevoPotentialSale < 1 && SevoPotentialSale > 0)
			{
				return 'C';
			}
			return '其他';
		}
		return '其他';
	}
}