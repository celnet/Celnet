/* 
 * Author: Sunny
 * Created on: 2011-12-26
 * Description: 
 *修改日期：2012-1-11
 *修改人：scott
 *新加功能，保存并提交审批。
 */
public with sharing  class V2_CreateAccountTeam_Con 
{
	public String Month{get;set;}
	public String Year{get;set;}
	public V2_Account_Team__c objAccountTeam{get;set;}
	public boolean blnIsMesShow{get;set;}
	public boolean blnError  ;
	public string strMessage{get;set;}
	
	public boolean blnReplace{get;set;}
	public boolean blnUserShow{get;set;}
	/**
	public boolean blnUnReplace{get;set;}
	public boolean blnNewShow{get;set;}
	**/
	public boolean blnRepMsgShow{get;set;}
	public string strRepMsgShow{get;set;}
	public string strUserId{get;set;}//选择的替换成员
	private String strId ;
	private Set<ID> set_UserIds = new Set<ID>() ;
	private Set<String> set_StrRole = new Set<String>() ;
	private Map<String,Date> map_RoleMonth = new Map<String,Date>() ;
	private set<String> Set_UserId = new set<String>() ;
	private Set<ID> set_AccTeamUserIds = new Set<ID>() ;
	private Map<Id,V2_Account_Team__c> map_AccTeam = new Map<Id,V2_Account_Team__c>() ;
	public List<SelectOption> ListUser{get;set;}
	public boolean blnSubordinateShow{get;set;}//是否存在可被替换的成员
	public List<SelectOption> ListYears{get;set;}
	
	//完成 按钮失效
	public Boolean Isdisabled{get;set;}
	
	public List<SelectOption> getListMonths()
    {
        list<SelectOption> options=new list<SelectOption>();
        options.add(new SelectOption('1','1'));options.add(new SelectOption('2','2'));
        options.add(new SelectOption('3','3'));options.add(new SelectOption('4','4'));
        options.add(new SelectOption('5','5'));options.add(new SelectOption('6','6'));
        options.add(new SelectOption('7','7'));options.add(new SelectOption('8','8'));
        options.add(new SelectOption('9','9'));options.add(new SelectOption('10','10'));
        options.add(new SelectOption('11','11'));options.add(new SelectOption('12','12'));
        return options;
    }
	public V2_CreateAccountTeam_Con(ApexPages.StandardController controller)
	{
		//2011-1-11
		Isdisabled = false;
		blnError = false ;
		blnRepMsgShow = false ;
		blnUserShow = false;
		blnSubordinateShow = false ;
		strId = ApexPages.currentPage().getParameters().get('camid');
		if(strId==null || strId==''){
					blnError = true ;
					//blnNewShow = false ;
					strMessage = '未取得客户ID。' ;
					//2011-1-11
					Isdisabled= true;
					blnIsMesShow = true ;
					strId='001/o';
					return ;
			
		}
		
		//取自己的下属
		V2_UtilClass cls = new V2_UtilClass();
		set_UserIds = cls.GetUserSubIds();

		//判断是否可以添加和是否有下属
		this.checkUser(strId) ;
		system.debug('have error?'+blnError+set_UserIds.size()) ;
		if(!blnError)
		{
			objAccountTeam = new V2_Account_Team__c() ;
			//this.initDateList() ;
		}else
		{
			blnIsMesShow = true ;
		}
		//init year
		ListYears = new  List<SelectOption>();
        ListYears.add(new SelectOption(String.valueOf(date.today().addYears(-1).year()),String.valueOf(date.today().addYears(-1).year())));
        ListYears.add(new SelectOption(String.valueOf(date.today().year()),String.valueOf(date.today().year())));
        ListYears.add(new SelectOption(String.valueOf(date.today().addYears(1).year()),String.valueOf(date.today().addYears(1).year())));
		//init current year & month
		Year = String.valueOf(date.today().year());
        Month = String.valueOf(date.today().Month());
        
        
        
	}


	public PageReference doCancel()
	{
		return new PageReference('/'+strId) ;
	}
	
	/**
	检查是否有下属
	**/
	private void checkUser(String strAccountId)
	{
		if(strAccountId == null)
		{
			return ;
		}
		//已有下属List
		ListUser = new  List<SelectOption>();
		//非历史
		for(V2_Account_Team__c objAT : [select id,V2_User__c,V2_User__r.Name,V2_Role__c,V2_Is_Delete__c,V2_ApprovalStatus__c,V2_LastAccessDate__c,V2_Effective_Year__c,V2_Effective_Month__c from V2_Account_Team__c where V2_Account__c =: strAccountId  and V2_History__c = false])
		{
			//判断是否下属处于审批中
			if(objAT.V2_ApprovalStatus__c=='审批中'){
					blnError = true ;
					//blnNewShow = false ;
					strMessage = '请在审批结束后添加小组成员。' ;
					//2011-1-11
					Isdisabled= true;
					return ;
			}
			if(!objAT.V2_Is_Delete__c){
				blnSubordinateShow = true ;	
				blnRepMsgShow = true ;
				strRepMsgShow = '您在该客户下已存在下属，请选择是否替换原成员。' ;
				ListUser.add(new SelectOption(String.valueOf(objAT.V2_User__c),String.valueOf(objAT.V2_User__r.Name))) ;
				
			}
			//保存客户小组对象,包含被标记为删除的客户小组。
			map_AccTeam.put(objAT.V2_User__c , objAT) ;
			set_AccTeamUserIds.add(objAT.V2_User__c) ;
		}
		
		
	}
	
	//2012-1-11修改：添加保存并提交
	public PageReference saveChange2()
	{
	
		try
		{
			Date newDate=Date.newInstance(Integer.ValueOf(this.year), Integer.ValueOf(this.Month), 1);
			
			//Check 是否可以保存
			if(!set_UserIds.contains(objAccountTeam.V2_User__c))
			{
				objAccountTeam.V2_User__c.addError('只能添加您自己的下属。') ;
				return null ;
			}
			
			//check 替换日期是否大于生效日期
			/**
			if(this.blnReplace==true){
				V2_Account_Team__c objAccTeamDel = map_AccTeam.get(strUserId);
				Date oldDate=Date.newInstance(Integer.ValueOf(objAccTeamDel.V2_Effective_Year__c), Integer.ValueOf(objAccTeamDel.V2_Effective_Month__c), 1);
				if(newDate<=oldDate){
					blnIsMesShow = true ;
					strMessage = '新的生效日期必须大于。' ;
					return null ;
					
				}
			}
			**/
			if(map_AccTeam.containsKey(objAccountTeam.V2_User__c))
			{
				//将要被删除的小组成员，如果失效日期和新增加的生效日期不重叠，允许添加
				
				V2_Account_Team__c objAccTeamExist = map_AccTeam.get(objAccountTeam.V2_User__c);
				if(!objAccTeamExist.V2_Is_Delete__c){
					blnIsMesShow = true ;
					strMessage = '已有该小组成员。' ;
					return null ;
					
				} else {
					if(newDate<objAccTeamExist.V2_LastAccessDate__c){
						blnIsMesShow = true ;
						strMessage = '已有该小组成员,新的成员生效必须在旧的成员失效日期之后。' ;
						return null ;
					}					
				}
			}
			//Check 同一产品下不能有多个人
			User u =[select V2_UserProduct__c from User where id=:objAccountTeam.V2_User__c];
			//2013-4-8 sunny:销售医院关系上产品字段更换
			V2_Account_Team__c[] teams =[select v2_User__c,
			V2_Account__c,V2_History__c,V2_UserProduct__c,UserProduct__c from V2_Account_Team__c 
			where V2_Account__c =: strId  and V2_History__c = false and V2_Is_Delete__c=false
			and UserProduct__c=: u.V2_UserProduct__c]; 
			//and V2_UserProduct__c=: u.V2_UserProduct__c];
			if(teams.size()>0){
				if(!blnReplace){
					blnIsMesShow = true ;
					strMessage = '同一个产品不能有多人负责。' ;
					return null ;
					
				} else {
					
					//sunny:被替换的成员和新成员不一样
					for(V2_Account_Team__c acc:teams){
						System.debug('dddddddd%' + acc.v2_User__c + '%xxx%' + strUserId);
						if(acc.v2_User__c !=strUserId){
									blnIsMesShow = true ;
									strMessage = '同一个产品不能有多人负责。' ;
									return null ;
						}
					}
					
				}
			}
			Date lastDate = this.GetUserLastAccTeamMemberDate(strId,u.V2_UserProduct__c,strUserId);
			//已经存在该产品销售
			if(lastDate!=null){
			
				if(newDate<=lastDate){
					blnIsMesShow = true ;
					strMessage = '同一个时间内不可以有多人负责。' ;
					return null;
					
				} 
			}
			
	
	
			
			Id ApprovalId ;
			//存在下属
			system.debug('???'+this.blnSubordinateShow) ;
			if(this.blnSubordinateShow == true)
			{
				//替换
				system.debug('????'+this.blnReplace) ;
				if(this.blnReplace == true)
				{
					system.debug(strUserId+'==='+map_AccTeam.containsKey(strUserId)+'==='+map_AccTeam) ;
					if(map_AccTeam.containsKey(strUserId))
					{
						V2_Account_Team__c objAccTeamDel = map_AccTeam.get(strUserId);
						
						//this.delDate(objAccountTeam, objAccTeamDel) ;
						objAccTeamDel.V2_BatchOperate__c='替换';
						objAccTeamDel.V2_ApprovalStatus__c='待审批';
						objAccTeamDel.V2_NewAccUser__c=objAccountTeam.V2_User__c;
						objAccTeamDel.V2_Effective_NewYear__c=this.Year ;
						objAccTeamDel.V2_Effective_NewMonth__c=this.Month;
						objAccTeamDel.V2_AdjustReson__c=objAccountTeam.V2_AdjustReson__c;
						ApprovalId = objAccTeamDel.id;
						update objAccTeamDel;
						system.debug('tihuanaaaa'+objAccTeamDel) ;
					}
					
				} else {
					//新增
					objAccountTeam.V2_Account__c = this.strId ;
					objAccountTeam.V2_Effective_Year__c = this.Year ;
					objAccountTeam.V2_Effective_Month__c = this.Month ;
					objAccountTeam.V2_NewAccUser__c = objAccountTeam.V2_User__c;
					objAccountTeam.V2_Account__c = this.strId ;
					objAccountTeam.V2_BatchOperate__c='新增';
					objAccountTeam.V2_ApprovalStatus__c='待审批';
					objAccountTeam.ownerid=objAccountTeam.V2_User__c;
					
					insert objAccountTeam ;
					ApprovalId = objAccountTeam.id;
				}
			} else {
				//没有下属，新增一个成员
				objAccountTeam.V2_Account__c = this.strId ;
				objAccountTeam.V2_Effective_Year__c = this.Year ;
				objAccountTeam.V2_Effective_Month__c = this.Month ;
				objAccountTeam.V2_NewAccUser__c = objAccountTeam.V2_User__c;
				objAccountTeam.V2_BatchOperate__c='新增';
				objAccountTeam.V2_ApprovalStatus__c='待审批';
				objAccountTeam.ownerid=objAccountTeam.V2_User__c;
				insert objAccountTeam ;
			    ApprovalId = objAccountTeam.id;
			}
			//提交审批
			//Sunny:判断不为空才提交
			if(ApprovalId != null)
			{
				Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
		        req.setObjectId(ApprovalId);//客户联系人或自定义对象
		       	Approval.ProcessResult result = Approval.process(req);
			}
			
			return new PageReference('/'+strId) ;
		}catch(Exception e)
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning , e.getMessage());            
         	ApexPages.addMessage(msg);
         	return null;
		}
		
	}
	
	/**
	获取一个用户所有下属的最后负责日期，按产品分组
	**/
	public Date GetUserLastAccTeamMemberDate(String strAccountId,String pd,String userId){
		
		Date ret = null;
		AggregateResult[] teams = null;
		if(this.blnReplace == true){
			 teams=[select 
			max(V2_LastAccessDate__c) maxField from V2_Account_Team__c 
			//2013-4-8 sunny修改：销售医院关系上，产品字段更换。
			//where V2_Account__c =: strAccountId  and V2_History__c = false and V2_UserProduct__c=:pd
			where V2_Account__c =: strAccountId  and V2_History__c = false and UserProduct__c=:pd
			and v2_user__c !=:userId
			group by V2_Account__c];
			
		} else{
			 teams=[select 
			max(V2_LastAccessDate__c) maxField from V2_Account_Team__c 
			//where V2_Account__c =: strAccountId  and V2_History__c = false and V2_UserProduct__c=:pd
			where V2_Account__c =: strAccountId  and V2_History__c = false and UserProduct__c=:pd
			group by V2_Account__c];
		}		
		if(teams!=null && teams.size()>0){
			if(teams[0].get('maxField')!=null){
				ret = (Date)teams[0].get('maxField');
			}
		}
		return ret;
	}
	
	
	/**
	测试
	**/
	static testMethod void TestPage1() 
	{
		//----------------Create User-------------
    	List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
		//----------------New Account--------------------
        List<Account> list_Account = new List<Account>() ;
        Account objAcc1 = new Account() ;
        objAcc1.Name = 'test acc1' ;
        objAcc1.MID__c = '12345654321' ;
		list_Account.add(objAcc1) ;
		insert list_Account ;
		//----------------New Account Team--------------------
		List<V2_Account_Team__c> list_AccountTeam = new List<V2_Account_Team__c>() ;
		V2_Account_Team__c objAccTeam1 = new V2_Account_Team__c() ;
		objAccTeam1.V2_Account__c = objAcc1.Id ;
		objAccTeam1.V2_BatchOperate__c = '新增' ;
		objAccTeam1.V2_ApprovalStatus__c = '待审批' ;
		objAccTeam1.V2_User__c = use1.Id ;
		//list_AccountTeam.add(objAccTeam1) ;
		V2_Account_Team__c objAccTeam2 = new V2_Account_Team__c() ;
		objAccTeam2.V2_Account__c = objAcc1.Id ;
		objAccTeam2.V2_BatchOperate__c = '新增' ;
		objAccTeam2.V2_ApprovalStatus__c = '审批拒绝' ;
		objAccTeam2.V2_User__c = use2.Id ;
		list_AccountTeam.add(objAccTeam2) ;
		V2_Account_Team__c objAccTeam3 = new V2_Account_Team__c() ;
		objAccTeam3.V2_Account__c = objAcc1.Id ;
		objAccTeam3.V2_BatchOperate__c = '新增' ;
		objAccTeam3.V2_ApprovalStatus__c = '审批通过' ;
		objAccTeam3.V2_User__c = use3.Id ;
		list_AccountTeam.add(objAccTeam3) ;
		insert list_AccountTeam ;
		
		//--------------------Start Test-----------------------
		
		system.test.startTest() ;
		ApexPages.StandardController STController = new ApexPages.StandardController(new V2_Account_Team__c());
		ApexPages.currentPage().getParameters().put('camid', objAcc1.Id);
		V2_CreateAccountTeam_Con CreateAccTeam = new V2_CreateAccountTeam_Con(STController);
		CreateAccTeam.objAccountTeam.V2_User__c = use1.Id ;
		CreateAccTeam.getListMonths() ;
		CreateAccTeam.saveChange2() ;
		system.test.stopTest() ;
	}
	static testMethod void TestPage2() 
	{
		//----------------Create User-------------
    	List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
		//----------------New Account--------------------
        List<Account> list_Account = new List<Account>() ;
        Account objAcc1 = new Account() ;
        objAcc1.Name = 'test acc1' ;
        objAcc1.MID__c = '12345654321' ;
		list_Account.add(objAcc1) ;
		insert list_Account ;
		//----------------New Account Team--------------------
		List<V2_Account_Team__c> list_AccountTeam = new List<V2_Account_Team__c>() ;
		V2_Account_Team__c objAccTeam1 = new V2_Account_Team__c() ;
		objAccTeam1.V2_Account__c = objAcc1.Id ;
		objAccTeam1.V2_BatchOperate__c = '新增' ;
		objAccTeam1.V2_ApprovalStatus__c = '待审批' ;
		objAccTeam1.V2_User__c = use1.Id ;
		//list_AccountTeam.add(objAccTeam1) ;
		V2_Account_Team__c objAccTeam2 = new V2_Account_Team__c() ;
		objAccTeam2.V2_Account__c = objAcc1.Id ;
		objAccTeam2.V2_BatchOperate__c = '新增' ;
		objAccTeam2.V2_ApprovalStatus__c = '审批中' ;
		objAccTeam2.V2_User__c = use2.Id ;
		list_AccountTeam.add(objAccTeam2) ;
		V2_Account_Team__c objAccTeam3 = new V2_Account_Team__c() ;
		objAccTeam3.V2_Account__c = objAcc1.Id ;
		objAccTeam3.V2_BatchOperate__c = '新增' ;
		objAccTeam3.V2_ApprovalStatus__c = '审批通过' ;
		objAccTeam3.V2_User__c = use3.Id ;
		list_AccountTeam.add(objAccTeam3) ;
		insert list_AccountTeam ;
		
		//--------------------Start Test-----------------------
		
		system.test.startTest() ;
		ApexPages.StandardController STController = new ApexPages.StandardController(new V2_Account_Team__c());
		ApexPages.currentPage().getParameters().put('camid', objAcc1.Id);
		V2_CreateAccountTeam_Con CreateAccTeam = new V2_CreateAccountTeam_Con(STController);
		system.debug(CreateAccTeam.objAccountTeam+'hahahahah'+use1.Id) ;
		//CreateAccTeam.objAccountTeam.V2_User__c = use1.Id ;
		CreateAccTeam.getListMonths() ;
		//CreateAccTeam.saveChange2() ;
		CreateAccTeam.doCancel() ;
		system.test.stopTest() ;
	}
	static testMethod void TestPage3()
	{
		ApexPages.StandardController STController = new ApexPages.StandardController(new V2_Account_Team__c());
		ApexPages.currentPage().getParameters().put('camid', null);
		V2_CreateAccountTeam_Con CreateAccTeam = new V2_CreateAccountTeam_Con(STController);
	}
	static testMethod void TestPage4() 
	{
		//----------------New UserRole ------------------
    	//List<UserRole> list_userRole = new List<UserRole >() ;
    	UserRole objUserRole = new UserRole() ;
    	objUserRole.Name = 'Renal-Rep-大上海-PD-Rep(陈喆令)' ;
    	insert objUserRole ;
    	UserRole objUserRole2 = new UserRole() ;
    	objUserRole2.Name = 'Renal-Rep-大上海-PB-Rep(陈喆令)' ;
    	objUserRole2.ParentRoleId = objUserRole.Id ;
    	insert objUserRole2 ;
		//----------------Create User-------------
    	List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.UserRoleId = objUserRole2.Id ;
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	use2.UserRoleId = objUserRole2.Id ;
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.UserRoleId = objUserRole.Id ;
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
		//----------------New Account--------------------
        List<Account> list_Account = new List<Account>() ;
        Account objAcc1 = new Account() ;
        objAcc1.Name = 'test acc1' ;
        objAcc1.MID__c = '12345654321' ;
		list_Account.add(objAcc1) ;
		insert list_Account ;
		//----------------New Account Team--------------------
		List<V2_Account_Team__c> list_AccountTeam = new List<V2_Account_Team__c>() ;
		V2_Account_Team__c objAccTeam1 = new V2_Account_Team__c() ;
		objAccTeam1.V2_Account__c = objAcc1.Id ;
		objAccTeam1.V2_BatchOperate__c = '新增' ;
		objAccTeam1.V2_ApprovalStatus__c = '待审批' ;
		objAccTeam1.V2_User__c = use1.Id ;
		//list_AccountTeam.add(objAccTeam1) ;
		V2_Account_Team__c objAccTeam2 = new V2_Account_Team__c() ;
		objAccTeam2.V2_Account__c = objAcc1.Id ;
		objAccTeam2.V2_BatchOperate__c = '新增' ;
		objAccTeam2.V2_ApprovalStatus__c = '审批拒绝' ;
		objAccTeam2.V2_User__c = use2.Id ;
		list_AccountTeam.add(objAccTeam2) ;
		V2_Account_Team__c objAccTeam3 = new V2_Account_Team__c() ;
		objAccTeam3.V2_Account__c = objAcc1.Id ;
		objAccTeam3.V2_BatchOperate__c = '新增' ;
		objAccTeam3.V2_ApprovalStatus__c = '审批通过' ;
		objAccTeam3.V2_User__c = use3.Id ;
		//list_AccountTeam.add(objAccTeam3) ;
		insert list_AccountTeam ;
		
		//--------------------Start Test-----------------------
		
		system.test.startTest() ;
		system.runAs(use3) 
		{
			ApexPages.StandardController STController = new ApexPages.StandardController(new V2_Account_Team__c());
			ApexPages.currentPage().getParameters().put('camid', objAcc1.Id);
			V2_CreateAccountTeam_Con CreateAccTeam = new V2_CreateAccountTeam_Con(STController);
			CreateAccTeam.objAccountTeam.V2_User__c = use1.Id ;
			CreateAccTeam.strUserId = use2.Id ;
			CreateAccTeam.blnReplace = true ;
			CreateAccTeam.getListMonths() ;
			CreateAccTeam.saveChange2() ;
		}
		
		system.test.stopTest() ;
	}
}