/**
 * Tobe
 * 2013.10.30
 * PM_AutoCreatedTaskForFreeTP 测试类
 */
@isTest
private class PM_Test_AutoCreatedTaskForFreeTP 
{
    static testMethod void myUnitTest() 
    {
    	list<Profile> list_Profile = [Select Id,Name From Profile Where Name = 'PM-PSR Manager'];
        list<UserRole> list_role = [Select Id,Name From UserRole Where DeveloperName ='RenalGDAccountManager'];
        User user = new User();
        user.ProfileId = list_Profile[0].Id;
        user.Username = 'asfdasdf@acme.com';
        user.FirstName = 'Test';
        user.LastName = 'Test';
        user.Email = 'asdf@aa.com';
        user.Alias = 'asdf';
        user.EmailEncodingKey = 'UTF-8';
        user.TimeZoneSidKey = 'Asia/Shanghai';
        user.LanguageLocaleKey = 'zh_CN';
        user.LocaleSidKey = 'zh_CN';
        user.UserRoleId = list_role[0].Id;
        insert user; 
        Region__c zone = new Region__c();
        zone.Name = '南区';
        zone.PM_RegionalHead__c = user.Id;
        insert zone;
		Provinces__c pro = new Provinces__c();
    	pro.Name = '浙江省';
    	insert pro;
    	Cities__c city = new Cities__c();
    	city.Name = '杭州市';
    	city.BelongToProvince__c = pro.Id;
    	insert city;
    	list<RecordType> record = [Select Id From RecordType Where Name = '医院' and SobjectType = 'Account' ];
    	list<RecordType> record2 = [Select Id From RecordType Where Name = '经销商' and SobjectType = 'Account'];
        Account acc = new  Account();
        acc.Name = '浙江医院';
        acc.RecordTypeId = record[0].Id;
        acc.Provinces__c = pro.Id;
        acc.Cities__c = city.Id;
        acc.Region__c = zone.Id;
        insert acc;
        Account acc3 = new  Account();
        acc3.Name = '北京医院';
        acc3.RecordTypeId = record[0].Id;
        acc3.Provinces__c = pro.Id;
        acc3.Cities__c = city.Id;
        acc3.Region__c = zone.Id;
        insert acc3;
        PM_Patient__c patient1 = new PM_Patient__c();
    	patient1.Name = '张先生Test';
    	patient1.PM_Status__c = 'Acitve';
    	patient1.PM_PmPhone__c = '18911112222';
    	patient1.PM_ProblemRemarks__c = '备注';
    	patient1.PM_InHospital__c = acc.Id;
    	patient1.PM_LastVisitDateTime__c = DateTime.now().addDays(-60);
    	patient1.PM_LastSuccessDateTime__c= DateTime.now().addDays(-60);
    	insert patient1;
    	
    	PM_Free_TP_Application__c tp = new PM_Free_TP_Application__c();
    	tp.PM_Patient__c = patient1.Id;
    	tp.PM_Standard1__c = '6AB9766';
    	tp.PM_Dialysate1__c = 1;
    	insert tp;
    	PM_Free_TP_Application__c tp2 = new PM_Free_TP_Application__c();
    	tp2.PM_Standard1__c = '6AB9766';
    	tp2.PM_Dialysate1__c = 1;
    	insert tp2;
        
    }
}