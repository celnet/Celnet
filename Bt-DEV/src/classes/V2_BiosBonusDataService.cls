/*
Author：Scott
Created on：2011-1-9
Description: 
1.得到Bios部门用户奖金计算所涉及到的所有参数信息
2013-12-12 Comment By Tobe 事件字段删除：注释CommentsBySupervisor2__c字段引用
*/
public class V2_BiosBonusDataService {
	//联系人部门集合,用于与拜访科室会对照
	Set<String> deptsSet=new Set<String>{'药剂科','肝胆外科','ICU','普外科','心外科','肝移植','烧伤科'};
	//目标科室有效拜访次数
	Double visitTimesForValidDept=0;
	
    //拜访科室会有效完成次数,拜访的科室会联系人可能不是目标科室
	Double visitDeptCompletedTimes=0;
	
	//拜访科室会的联系人
	Set<Id> deptVisitContactIds=new Set<Id>();
	//月协访中不同日期的天数,即月中有多少天有协访
	Double daysCountForAssistVisit=0;
	//拜访A/B级别客户数
	Double visitABLevelCount=0;
	//已安排的拜访次数
	Double arrangedVisitCount=0;
	//总拜访客户数量,不重复计数
	Double visitContactsCount=0;
	
	//有效季度业务机会扩展天数
	Integer appendDays=3;
	

  //当月完成拜访联系人数
  Double FinishedCall = 0;
  //安排拜访数
  Double ArrangementPercent = 0;
  //相关事件
  List<Event> List_Event {get;set;}
  //目标联系人数
  public Double TargetContact = 0;
  //当月主管协访的已完成并填写评语的拜访事件数量
  Double Assistance = 0;
  //主管科室会的参加
  Double ManagerDepartmentVisit = 0;
  //拜访联系人Ids
  Set<Id> Set_Contactids = new Set<Id>();
  
  //KOL定义：职务为院长，副院长，主任，副主任，教授，副教授
  Set<String> Set_Kol = new Set<String>{'院长','副院长','主任','副主任','教授','副教授'};
  public V2_BiosBonusDataService(Id userId,Integer year,Integer month)
  {
    List<MonthlyPlan__c> mp = [select Id,Percent__c from MonthlyPlan__c 
                 			   where Year__c=:String.valueOf(year) and Month__c =:String.valueOf(month)
                 			   and OwnerId =: userId limit 1];
    if(mp != null && mp.size()>0)
    {
    	//目标联系人数
    	for(MonthlyPlanDetail__c mpd:[select Id from MonthlyPlanDetail__c where MonthlyPlan__c =:mp[0].Id and Contact__c != null])
    	{
    		TargetContact++;
    	}
    }
    //事件
    Date CurrentDate = Date.newInstance(year,month,1);
    DateTime CurrentStartMonth = DateTime.newInstance(CurrentDate.toStartOfMonth(),Time.newInstance(00, 00, 00, 00));
    DateTime CurrentEndMonth = DateTime.newInstance(CurrentDate.toStartOfMonth().addMonths(1),Time.newInstance(00, 00, 00, 00));
    List_Event = [select whoId,WhatId,Done__c,SubjectType__c,StartDateTime,CreatedDate,V2_IsExpire__c,RecordType.DeveloperName 
    				from Event
                    where IsRecurrence != true and whoId != null and StartDateTime>=:CurrentStartMonth
                  	and StartDateTime <:CurrentEndMonth and OwnerId =: userId];
                  	
    if(List_Event !=null && List_Event.size()>0){
      for(Event ev:List_Event){
        if(ev.RecordType.DeveloperName != 'V2_Event'){
        	continue;
        }
        arrangedVisitCount++;
        ArrangementPercent++;
        if(ev.Done__c){
	        if(ev.SubjectType__c=='拜访'){
	        	Set_Contactids.add(ev.WhoId);
	        	if(!ev.V2_IsExpire__c){
	        		FinishedCall++;
	        	}
	        }else if(ev.SubjectType__c == '科室会'){
	        	if(ev.WhoId!=null){
	        		deptVisitContactIds.add(ev.WhoId);
	        	}
	    		if(!ev.V2_IsExpire__c){
	        		FinishedCall++;
	    			visitDeptCompletedTimes++;
	    		}
	    	}
        }
      }
    }
    
    Set<Id> allContactsIdSet=new Set<Id>();
    if(Set_Contactids.size()>0){
    	allContactsIdSet.addAll(Set_Contactids);
    }
    if(deptVisitContactIds.size()>0){
    	allContactsIdSet.addAll(deptVisitContactIds);
    }
    visitContactsCount=allContactsIdSet.size();
    
    List<Contact> cs=[select Id,V2_BiosDepartmentType__c,V2_Level__c from Contact 
    	where (Id in:deptVisitContactIds and V2_BiosDepartmentType__c in: deptsSet)
    		or(Id in:Set_Contactids and V2_Level__c in('A','B'))];
    if(cs!=null){
    	for(Contact c:cs){
    		String id=c.id;
    		String bdt=c.V2_BiosDepartmentType__c;
    		String level=c.V2_Level__c;
    		if(deptVisitContactIds.contains(id) && deptsSet.contains(bdt)){
    			visitTimesForValidDept++;
    		}
    		if(Set_Contactids.contains(id) && (level=='A' || level=='B')){
    			visitABLevelCount++;
    		}
    	}
    }
    
    //主管协防
    Set<Date> dSet=new Set<Date>();
   /* 2013-12-12 Comment By Tobe
    for(EventAttendee  ea:[Select Event.CommentsBySupervisor__c, Event.CommentsBySupervisor2__c
    	,Event.SubjectType__c, Event.StartDateTime 
    	From EventAttendee 
    	where AttendeeId =:userId and Event.StartDateTime >=:CurrentStartMonth
        	and Event.StartDateTime <:CurrentEndMonth and EventId != null and Event.V2_IsExpire__c =false 
        	and Event.IsRecurrence != true 
        	and Event.Done__c=true  
        	and (Status='Accepted' or Event.CommentsBySupervisor__c != null or Event.CommentsBySupervisor2__c != null)])
    {
      //当月主管协访的已完成的拜访事件数量
      if((ea.Event.CommentsBySupervisor__c != null || ea.Event.CommentsBySupervisor2__c != null ) && ea.Event.SubjectType__c == '拜访')
      {
        Assistance++;
        Datetime dt=ea.Event.StartDateTime;
        if(dt!=null){
        	Date d=dt.date();
       		dSet.add(d);
        }
      }
      //当月主管协访的已完成的，行动类型为“科室会”，并填写了评语的拜访事件数量。
      if(ea.Event.SubjectType__c == '科室会')
      {
        ManagerDepartmentVisit++;
      }
    }*/
    
    for(EventAttendee  ea:[Select Event.CommentsBySupervisor__c
    	,Event.SubjectType__c, Event.StartDateTime 
    	From EventAttendee 
    	where AttendeeId =:userId and Event.StartDateTime >=:CurrentStartMonth
        	and Event.StartDateTime <:CurrentEndMonth and EventId != null and Event.V2_IsExpire__c =false 
        	and Event.IsRecurrence != true 
        	and Event.Done__c=true  
        	and (Status='Accepted' or Event.CommentsBySupervisor__c != null )])
    {
      //当月主管协访的已完成的拜访事件数量
      if((ea.Event.CommentsBySupervisor__c != null ) && ea.Event.SubjectType__c == '拜访')
      {
        Assistance++;
        Datetime dt=ea.Event.StartDateTime;
        if(dt!=null){
        	Date d=dt.date();
       		dSet.add(d);
        }
      }
      //当月主管协访的已完成的，行动类型为“科室会”，并填写了评语的拜访事件数量。
      if(ea.Event.SubjectType__c == '科室会')
      {
        ManagerDepartmentVisit++;
      }
    }
    daysCountForAssistVisit = dSet.size();
  }
  
  Integer k21InHospitalSuccessCount=0;//进院成功数量
  Integer k21HospitalDevelopCount=0;//医院开发数量
  
  Integer k22HospitalAppendAndSuccessCount=0;//业务机会类型[bio_biz_type__c]为医院上量且业务进展为'上量成功'数量累加
  Integer k22HospitalAppendCount=0;//医院上量的所有业务机会总数
  
  Integer k5PorcessSuccessCount=0;//业务机会的进展为成功数
  Integer k5RepsAndSelfCount=0;//区域人数
  
  /**
  *初始化业务机会相关
  *业务进展[Bio_bz_process__c]
  *业务机会类型[bio_biz_type__c]
  *repids为空表示为非主管用户或不带下属的用户
  */
  public void initOpportunityRelated(final String userId, final Set<Id> repids, final Integer year, final Integer month){
  	Set<Id> uIdSet=new Set<Id>();
  	uIdSet.add(userId);
  	k5RepsAndSelfCount=1;//不带人的话
  	if(repids!=null && repids.size()>0){
  		uIdSet.addAll(repids);
  		k5RepsAndSelfCount=repids.size()+1;
  	}
  	Date d=Date.newInstance(year, month, 1);
  	List<Datetime> dts=V2_WipUtil.getSeason_Start_End_DT(d, appendDays);
  	List<Opportunity> opps=[select Id, LastModifiedDate, OwnerId, TotalOpportunityQuantity 
  		, Bio_bz_process__c, bio_biz_type__c
  		from Opportunity 
  		where OwnerId in:uIdSet
  			and LastModifiedDate >=:dts[0] and LastModifiedDate <: dts[1]
  			and (
  				Bio_bz_process__c in ( '进院成功', '上量成功', '培养成功' )
  				or
  				bio_biz_type__c in ( '医院上量', '医院开发' )
  			)];
  			
  	for(Opportunity opp:opps){
    	String bp=opp.Bio_bz_process__c;
    	String bt=opp.bio_biz_type__c;
    	if(bp.indexOf('成功')>0){
    		k5PorcessSuccessCount++;
    	}
    	if(bp=='进院成功'){
    		k21InHospitalSuccessCount++;
    	}
    	if(bt=='医院开发'){
    		k21HospitalDevelopCount++;
    	}
    	if(bt=='医院上量'){
    		k22HospitalAppendCount++;
    		if(bp=='上量成功'){
    			k22HospitalAppendAndSuccessCount++;
    		}
    	}
    }
  }
  
  private void debug(String msg){
  	System.debug('Result: '+msg);
  }
  
  /**
  *获取K1.1的值
  */
  public Double getK1_1(){
  	debug('K1.1:有效完成次数: FinishedCall='+FinishedCall+', 安排次数: arrangedVisitCount='+arrangedVisitCount);
  	if(FinishedCall==0 || arrangedVisitCount==0)return 0;
  	return FinishedCall/arrangedVisitCount;
  }
  
  /**
  *获取K1.2的值
  */
  public Double getK1_2(){
  	debug('K1.2:目标科室有效拜访次数: visitTimesForValidDept='+visitTimesForValidDept+', 有效完成次数: FinishedCall='+FinishedCall);
  	if(visitTimesForValidDept==0 || FinishedCall==0)return 0;
  	return visitTimesForValidDept/FinishedCall;
  }
  
  /**
  *获取K1.3的值
  *记录到字段：targetContactRate
  */
  public Double getK1_3(){
  	debug('K1.3:拜访A/B级别客户数: visitABLevelCount='+visitABLevelCount+', 总拜访客户数量: visitContactsCount='+visitContactsCount);
  	if(visitABLevelCount==0 || visitContactsCount==0)return 0;
  	return visitABLevelCount/visitContactsCount;
  }
  
  /**
  *获取K2.1的值
  */
  public Double getK2_1(){
  	debug('K2.1:进院成功数量: k21InHospitalSuccessCount='+k21InHospitalSuccessCount+', 医院数量: k21HospitalDevelopCount='+k21HospitalDevelopCount);
  	if(k21InHospitalSuccessCount==0 || k21HospitalDevelopCount==0)return 0;
  	return k21InHospitalSuccessCount/k21HospitalDevelopCount;
  }
  
  /**
  *获取K2.2的值
  */
  public Double getK2_2(){
  	debug('K2.2:业务机会类型[bio_biz_type__c]为医院上量且业务进展为-上量成功-数量累加: k22HospitalAppendAndSuccessCount='+k22HospitalAppendAndSuccessCount
  		+', 医院上量的所有业务机会总数: k22HospitalAppendCount='+k22HospitalAppendCount);
  	if(k22HospitalAppendAndSuccessCount==0 || k22HospitalAppendCount==0)return 0;
  	return k22HospitalAppendAndSuccessCount/k22HospitalAppendCount;
  }
  
  /**
    *获取K3的值
  	*等于下属K1平均分
  	*/
  public Double getK3_RepK1ScoreAverage(Set<Id> repids,Integer year,Integer month){
  	Double AverageScore = 0;
    Double sum = 0;
    Double reps = repids.size();
    for(V2_UserBonusInFo__c vubif:[select V2_BonusScore__c, biosK1Score__c 
    				from V2_UserBonusInFo__c 
                   where V2_Year__c =: String.valueOf(year)
                   and V2_Month__c =:String.valueOf(month)
                   and biosK1Score__c != null
                   and OwnerId in: repids]) {
      sum+=vubif.biosK1Score__c;
    }
    AverageScore = sum/reps;
    debug('K3:(K1平均分): AverageScore='+AverageScore+', 总分: sum='+sum+', 下属人数: reps='+reps);
    return AverageScore;
  }
  
  /**
  *获取K4的值
  */
  public Double getK4(){
  	debug('K4:月份中协访天数: daysCountForAssistVisit='+daysCountForAssistVisit);
  	return daysCountForAssistVisit;
  }
  
  /**
  *获取K5的值
  */
  public Double getK5(){
  	debug('K5:业务机会的进展为成功数: k5PorcessSuccessCount='+k5PorcessSuccessCount+', 人数:k5RepsAndSelfCount='+k5RepsAndSelfCount);
  	if(k5PorcessSuccessCount==0 || k5RepsAndSelfCount==0)return 0;
  	return k5PorcessSuccessCount/k5RepsAndSelfCount;
  }
  
  //目标联系人覆盖率:当月完成拜访的联系人数/月计划明细的联系人数（目标联系人）(Coseal主管同样考核)
  public Double getTargetContactCoveragePercent()
  {
    Double TargetContactCoverage = 0;
    Double FinishCallContact = 0;
    if(Set_Contactids != null && Set_Contactids.size()>0)
    {
      FinishCallContact = Double.valueOf(Set_Contactids.size());
    }
    if(TargetContact>0)
    {
      TargetContactCoverage = FinishCallContact/TargetContact;
    }
    return TargetContactCoverage;
  }
  //目标联系人拜访分布 
  //当月完成拜访的A级目标联系人数/当月完成拜访的联系人数
  public Double getTargerContactAFinishedPercent(Id userId,Integer year,Integer month)
  {
    Double ContactAPercent=0;
    //当月拜访A级目标联系人数
    Double CallContactA = 0;
    //A及联系人
    Set<Id> Set_ContactA = new Set<Id>();
    for(Contact con: [select Id from Contact where Id in:Set_Contactids and V2_Level__c='A'])
    {
      Set_ContactA.add(con.Id);
    }
    
    //查出A级联系人当前月拜访完成数
    Date CurrentDate = Date.newInstance(year,month,1);
    DateTime CurrentStartMonth = DateTime.newInstance(CurrentDate.toStartOfMonth(),Time.newInstance(00, 00, 00, 00));
        DateTime CurrentEndMonth = DateTime.newInstance(CurrentDate.toStartOfMonth().addMonths(1),Time.newInstance(00, 00, 00, 00));
    for(Event ev:[select Id from Event where V2_IsExpire__c = false 
                  and IsRecurrence != true and whoId in: Set_ContactA and StartDateTime>=:CurrentStartMonth
                  and StartDateTime <:CurrentEndMonth and OwnerId =: userId and RecordType.DeveloperName = 'V2_Event'])
    {
      CallContactA++;
    }
    if(FinishedCall>0)
    {
      ContactAPercent = CallContactA/FinishedCall;
    }
    return ContactAPercent;
  }
  //  按时填写拜访记录
  public Double getFinishedCallEventPercent()
  {
    Double FinishedCallEventPercent = 0;
    //当月拜访量 
    Double denominator =0;
    if(List_Event != null && List_Event.size()>0)
    {
      for(Event ev:List_Event)
      {
        if(ev.SubjectType__c != '拜访')
        {
          continue;
        }
        //当月拜访数量
        denominator++;
      }
    }
    if(denominator>0)
    {
      FinishedCallEventPercent = FinishedCall/denominator;
    }
    return FinishedCallEventPercent;
  }
  //拜访达成率:每月实际拜访完成数/计划拜访数
  //2012-4-16取完成次数/安排次数
  public Double getCallFinishedPercent()
  {
    Double CallFinishedPercent =0; 
    if(ArrangementPercent >0)
    {
    	CallFinishedPercent = FinishedCall/ArrangementPercent;
    }
    return CallFinishedPercent;
  }
  //按时提交周计划:当月每一周（开始日期在某一个周）都至少有一个拜访是上周以前创建??
  public Boolean getLastWeekCreatedEventQuantity()
  {
    //默认没有符合要求的拜访
    Boolean flag = false;
    Double Quantity = 0;
    
    Date StartOfWeek;
    
    if(List_Event != null && List_Event.size()>0)
    {
      for(Event ev:List_Event)
      {
        if(ev.SubjectType__c != '拜访')
        {
          continue;
        }
        if(ev.V2_IsExpire__c)
        {
          continue;
        }
        //将日期时间类型转换为日期类型
        Date srartdate = Date.valueOf(ev.StartDateTime);
        Date createdate = Date.valueOf(ev.CreatedDate);
        
        if(StartOfWeek !=null)
        {
          if(srartdate.toStartOfWeek().addDays(1) == StartOfWeek)
          {
            if( createdate <StartOfWeek )
            {
              flag = true;
            }
          }
          else
          {
            StartOfWeek = srartdate.toStartOfWeek().addDays(1);
            flag = false;
            if(createdate < StartOfWeek)
            {
              flag = true;
            }
          }
        }
        else
        {
          StartOfWeek = srartdate.toStartOfWeek().addDays(1);
          if(createdate < StartOfWeek)
          {
            flag = true;
          }
        }
      }
    }
    return flag;
  }
  
  
  
  //科室会的举行:当月完成的行动类型为“科室会”的事件数量。
  public Double getDepartmentVisitQuantity()
  {
    Double DepartmentVisit = 0;
    if(List_Event != null && List_Event.size()>0)
    {
      for(Event ev:List_Event)
      {
        if(ev.Done__c && ev.SubjectType__c == '科室会')
        {  
          DepartmentVisit++;
        }
      }
    }
    return DepartmentVisit;
  }
  
  //业务机会的制定:当月创建的业务机会数量和修改的阶段数量
  public Double getOppEstablishQuantity(Integer year,Integer month,Id userid)
  {
    Double Quantity = 0;
    
    Datetime StartYear = Datetime.newInstance(year, month, 1,0,0,0);
     Datetime EndYear =StartYear.addMonths(1);
    for(Opportunity opp:[select Id from Opportunity where OwnerId =: userid 
               and CreatedDate >=:StartYear and CreatedDate <: EndYear])
    {
      Quantity++;
    }
    for(OpportunityHistory oh:[Select Id From OpportunityHistory where IsDeleted=false and 
                     CreatedDate >=:StartYear and CreatedDate <: EndYear
                     and Opportunity.OwnerId =: userid] )
    {
      Quantity++;
    }
    return Quantity;
  }
  
  //跟台数量：当月完成的行动类型为“跟台”的事件数量。
  public Double getGenTaiQuantity()
  {
    Double Quantity = 0;
    if(List_Event != null && List_Event.size()>0)
    {
      for(Event ev:List_Event)
      {
        if(ev.Done__c && ev.SubjectType__c =='跟台')
        {
          Quantity++;
        }
      }
    }
    return Quantity;
  }
  
  //Albumin主管
  //得到用户下属业绩平均值:团队拜访计划与执行
  public Double getRepAverageScore(Set<Id> repids,Integer year,Integer month)
  {
    Double AverageScore = 0;
    Double sum = 0;
    Double reps = 0;
    for(V2_UserBonusInFo__c vubif:[select V2_BonusScore__c from V2_UserBonusInFo__c 
                   where V2_Year__c =: String.valueOf(year)
                   and V2_Month__c =:String.valueOf(month)
                   and V2_BonusScore__c != null
                   and OwnerId in: repids])
    {
      reps++;
      sum+=vubif.V2_BonusScore__c;
    }
    
    if(reps>0)
    {
      AverageScore = sum/reps;
    }
    return AverageScore;
  }
  //主管协访频率：当月主管协访的已完成的并填写了评语的拜访事件数量。（主管是被邀请人，且该拜访已完成）
  public Double getManagerAssistance()
  {
    return Assistance;
  }
  //科室会的参加：当月主管协访的已完成的，行动类型为“科室会”，并填写了评语的拜访事件数量。（主管是被邀请人，且该拜访已完成）
  public Double getManagerDepartmentVisitQuantity()
  {
    return ManagerDepartmentVisit;
  }
  //区域活动执行campaign:百特代表报名计数>=1次，其主管得分 1分，否则，主管不得分。
  public Double getCampaignRegistrationQuantity(Set<Id> userids,Integer year,Integer month)
  {
    Double Quantity = 0;
    
    Date CurrentDate = Date.newInstance(year,month,1);
    DateTime CurrentStartMonth = DateTime.newInstance(CurrentDate.toStartOfMonth(),Time.newInstance(00, 00, 00, 00));
        DateTime CurrentEndMonth = DateTime.newInstance(CurrentDate.toStartOfMonth().addMonths(1),Time.newInstance(00, 00, 00, 00));
    for(CampaignMember cm:[select Id from CampaignMember where CreatedDate>=:CurrentStartMonth and CreatedDate <: CurrentEndMonth
                 and User__c in:userids])
    {
      Quantity++;
    }  
    return Quantity;
  }
  
  
  //KOL覆盖率:每月实际完成的拜访KOL数/每月实际完成的拜访数；KOL定义：职务为院长，副院长，主任，副主任，教授，副教授
  public Double getKOLContactQuantity()
  {
    Double KolQuantity = 0;
    //KOL联系人数
    Double Kol = 0;
    for(Contact con:[select Id from Contact where Id in: Set_Contactids and V2_BiosTitle__c in: Set_Kol])
    {
      for(Event ev:List_Event)
      {
        if(ev.WhoId != con.Id)
        {
          continue;
        }
        if(ev.Done__c)
        {
          Kol++;
        }
      }
    }
    if(FinishedCall>0)
    {
      KolQuantity = Kol/FinishedCall;
    }
    return KolQuantity;
  }
  /***************************************************测试类***************************************************/
   static testMethod void V2_BiosBonusDataService() {
   	/*
       //**************************Create User************************
       //*用户角色
       //经理
       UserRole SupervisorUserRole = new UserRole() ;
       SupervisorUserRole.Name = 'Renal-Supervisor-Shanghai-PD-Supervisor';
       //SupervisorUserRole.ParentRoleId = RegionalUserRole.Id ;
       insert SupervisorUserRole ;
       //销售
       UserRole RepUserRole = new UserRole() ;
       RepUserRole.Name = 'Renal-Rep-Shanghai-HD-Rep';
       RepUserRole.ParentRoleId = SupervisorUserRole.Id ;
       insert RepUserRole ;
       
    //*用户简档
    //rep简档
      Profile RepProRenal = [select Id from Profile where Name  = 'Standard User - Renal Sales Rep' limit 1];
      //Supr简档renal
      Profile SupProRenal = [select Id from Profile where Name='Standard User - Renal Sales Supervisor' limit 1];
    
    //************User***********
    List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: 

UserInfo.getUserId()] ;
    //*销售
       User RepSu = new User();
       RepSu.Username='RepSu@123.com';
       RepSu.LastName='RepSu';
       RepSu.Email='RepSu@123.com';
       RepSu.Alias=user[0].Alias;
       RepSu.TimeZoneSidKey=user[0].TimeZoneSidKey;
       RepSu.ProfileId=RepProRenal.Id;
       RepSu.LocaleSidKey=user[0].LocaleSidKey;
       RepSu.LanguageLocaleKey=user[0].LanguageLocaleKey;
       RepSu.EmailEncodingKey=user[0].EmailEncodingKey;
       RepSu.CommunityNickname='RepSu';
       RepSu.MobilePhone='12345678912';
       RepSu.UserRoleId = RepUserRole.Id ;
       RepSu.IsActive = true;
       insert RepSu;
   
       //********主管*******
       User RepSs = new User();
       RepSs.Username='RepSs@123.com';
       RepSs.LastName='RepSs';
       RepSs.Email='RepSs@123.com';
       RepSs.Alias=user[0].Alias;
       RepSs.TimeZoneSidKey=user[0].TimeZoneSidKey;
       RepSs.ProfileId=SupProRenal.Id;
       RepSs.LocaleSidKey=user[0].LocaleSidKey;
       RepSs.LanguageLocaleKey=user[0].LanguageLocaleKey;
       RepSs.EmailEncodingKey=user[0].EmailEncodingKey;
       RepSs.CommunityNickname='RepSs';
       RepSs.MobilePhone='123456789112';
       RepSs.UserRoleId = SupervisorUserRole.Id ;
       RepSs.IsActive = true;
       insert RepSs;
       
        //*客户
    RecordType accrecordtype = [select Id from RecordType where Name='医院' and SobjectType='Account' limit 1];
    Account acc = new Account();
    acc.RecordTypeId = accrecordtype.Id;
    acc.Name = 'AccTest';
    insert acc;
    //*客户小组
    AccountTeamMember atm = new AccountTeamMember();
    atm.AccountId = acc.Id;
    atm.UserId = RepSu.Id;
    insert atm;
    //*Acc医院信息
    //CCHospitalInfo__c cch = new CCHospitalInfo__c();
    //cch.Account__c = acc.Id;
    //cch.
    //*联系人
    RecordType conrecordtype = [select Id from RecordType where Name = 'MD' and SobjectType='Contact' and IsActive = true limit 1 ];
    Contact con1 = new Contact();
    con1.LastName = 'AccTestContact1';
    con1.AccountId=acc.Id;
    insert con1;
    
    Contact con2 = new Contact();
    con2.LastName = 'AccTestContact2';
    con2.AccountId=acc.Id;
    con2.V2_Level__c = 'A';
    insert con2;
    //Ka
    Contact con3 = new Contact();
    con3.LastName = 'AccTestContact2';
    con3.AccountId=acc.Id;
    con3.Title = '院长';
    insert con3;
    //*业务机会
    Opportunity opp = new Opportunity();
    opp.Name = 'OppTest';
    opp.AccountId = acc.Id; 
    opp.StageName = '发现/验证机会';
    opp.Type = '其他';
    opp.CloseDate = date.today().addmonths(1);
    opp.OwnerId = RepSu.Id;
    insert opp;
    
    Opportunity opp1 = new Opportunity();
    opp1.Name = 'OppTest1';
    opp1.AccountId = acc.Id;
    opp1.StageName = '发现/验证机会';
    opp1.Type = '其他';
    opp1.CloseDate = date.today().addmonths(1);
    opp.OwnerId = RepSu.Id;
    insert opp1;
    //*市场活动
    Campaign cam = new Campaign();
    cam.Name = 'CamTest';
    cam.StartDate = date.today().addMonths(1);
    cam.EndDate = date.today().addMonths(2);
    cam.IsActive = true;
    insert cam;
    //*市场活动成员
    CampaignMember cm = new CampaignMember();
    cm.CampaignId = cam.Id;
    cm.ContactId = con2.Id;
    cm.User__c = RepSu.Id;
    insert cm;
    //*拜访事件
    RecordType callRt = [Select Id from RecordType where DeveloperName = 'V2_Event' Limit 1];
    
    Event CallEv = new Event();
    CallEv.RecordTypeId = callRt.Id;
    CallEv.WhoId = con1.Id;
    CallEv.StartDateTime = datetime.now();
    CallEv.EndDateTime = datetime.now().addMinutes(1);
    CallEv.SubjectType__c = '拜访';
    CallEv.OwnerId = RepSu.Id;
    CallEv.Done__c = true;
    CallEv.V2_2levelScore__c =String.valueOf(3);
    insert CallEv;
    
    Event CallEv2 = new Event();
    CallEv2.RecordTypeId = callRt.Id;
    CallEv2.WhoId = con2.Id;
    CallEv2.StartDateTime = datetime.now();
    CallEv2.EndDateTime = datetime.now().addMinutes(1);
    CallEv2.WhatId = opp1.Id;
    CallEv2.SubjectType__c = '拜访';
    CallEv2.OwnerId = RepSu.Id;
    CallEv2.Done__c = true;
    insert CallEv2;
    
    Event CallEv3 = new Event();
    CallEv3.RecordTypeId = callRt.Id;
    CallEv3.WhoId = con1.Id;
    CallEv3.StartDateTime = datetime.now();
    CallEv3.EndDateTime = datetime.now().addMinutes(1);
    CallEv3.WhatId = cam.Id;
    CallEv3.V2_FollowEventFlag__c = true;
    CallEv3.SubjectType__c = '拜访';
    CallEv3.OwnerId = RepSu.Id;
    CallEv3.V2_IsExpire__c = true;
    insert CallEv3;
    
    Event CallEv4 = new Event();
    CallEv4.RecordTypeId = callRt.Id;
    CallEv4.WhoId = con3.Id;
    CallEv4.StartDateTime = datetime.now();
    CallEv4.EndDateTime = datetime.now().addMinutes(1);
    CallEv4.WhatId = cam.Id;
    CallEv4.V2_FollowEventFlag__c = true;
    CallEv4.SubjectType__c = '科室会';
    CallEv4.Done__c = true;
    CallEv4.OwnerId = RepSu.Id;
    insert CallEv4;
    
    Event CallEv41 = new Event();
    CallEv41.RecordTypeId = callRt.Id;
    CallEv41.WhoId = con3.Id;
    CallEv41.StartDateTime = datetime.now();
    CallEv41.EndDateTime = datetime.now().addMinutes(1);
    CallEv41.WhatId = cam.Id;
    CallEv41.V2_FollowEventFlag__c = true;
    CallEv41.SubjectType__c = '跟台';
    CallEv41.Done__c = true;
    CallEv41.OwnerId = RepSu.Id;
    insert CallEv41;
    
    //*月计划
    MonthlyPlan__c mp = new MonthlyPlan__c();
    mp.Year__c = String.valueOf(Date.today().Year());
    mp.Month__c = String.valueOf(Date.today().Month());
    mp.ownerId = RepSu.Id;
    insert mp;
    //*月计划明细
    MonthlyPlanDetail__c mpd1 = new MonthlyPlanDetail__c();
    mpd1.Contact__c = con1.Id;
    mpd1.Account__c = acc.Id;
    mpd1.MonthlyPlan__c = mp.Id;
    mpd1.ArrangedTimes__c = 5;
    mpd1.AdjustedTimes__c = 6;
    mpd1.Planned_Finished_Calls__c = 5;
    insert mpd1;
    
    //*下属分数
    RecordType rt = [select Id from RecordType where SobjectType ='V2_UserBonusInFo__c' and DeveloperName ='V2_BiosRep' limit 1];
    V2_UserBonusInFo__c ubif = new V2_UserBonusInFo__c();
    ubif.RecordTypeId = rt.Id;
    ubif.V2_Year__c = String.valueOf(Date.today().Year());
    ubif.V2_Month__c = String.valueOf(Date.today().Month());
    ubif.V2_BonusScore__c = 90;
    ubif.OwnerId = RepSu.Id;
    insert ubif;
    
    */
    String repUserId='00590000000cTdP';//RepSu.Id;//张华平(Bio)
    String repSupUserId='00520000000lk1D';//RepSs.Id;//朱吴飚(Bio)
    Test.startTest();
  	
  	V2_BiosBonusDataService bds = new V2_BiosBonusDataService(repUserId,Date.today().Year(),Date.today().month());
  	bds.initOpportunityRelated(repUserId, null, Date.today().Year(),Date.today().month());
  	
  	bds.getTargetContactCoveragePercent();
  	bds.getTargerContactAFinishedPercent(repUserId,Date.today().Year(),Date.today().month());
  	bds.getFinishedCallEventPercent();
  	bds.getCallFinishedPercent();
  	bds.getLastWeekCreatedEventQuantity();
  	bds.getDepartmentVisitQuantity();
  	bds.getOppEstablishQuantity(Date.today().Year(),Date.today().month(),repUserId);
  	bds.getGenTaiQuantity();
  	bds.getK1_1();
  	bds.getK1_2();
  	bds.getK1_3();
  	bds.getK2_1();
  	bds.getK2_2(); 
    
    Set<Id> repids = new Set<Id>();
    repids.add(repUserId);
    
    V2_BiosBonusDataService bds1 = new V2_BiosBonusDataService(repSupUserId,Date.today().Year(),Date.today().month());
    bds1.initOpportunityRelated(repSupUserId, repids, Date.today().Year(),Date.today().month());
    
    bds1.getRepAverageScore(repids,Date.today().Year(),Date.today().month());
    bds1.getManagerDepartmentVisitQuantity();
    bds1.getCampaignRegistrationQuantity(repids,Date.today().Year(),Date.today().month());
    bds1.getKOLContactQuantity();
    bds1.getK3_RepK1ScoreAverage(repids, Date.today().Year(),Date.today().month());
    bds1.getK4();
   	bds1.getK5();
       
    Test.stopTest();
    }
}