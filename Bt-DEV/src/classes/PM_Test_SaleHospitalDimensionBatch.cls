/*
*作者：Tobe
*时间：2013-10-52
*功能：PM_SaleHospitalDimensionBatch的测试类
*/
@isTest
private class PM_Test_SaleHospitalDimensionBatch 
{

    static testMethod void myUnitTest() 
    {
    	system.Test.startTest();
        //新建省市
        Region__c zone = new Region__c();
        zone.Name = '南区';
        insert zone;
		Provinces__c pro = new Provinces__c();
    	pro.Name = '浙江省';
    	insert pro;
    	Cities__c city = new Cities__c();
    	city.Name = '杭州市';
    	city.BelongToProvince__c = pro.Id;
    	insert city;
    	list<RecordType> record = [Select Id From RecordType Where Name = '医院'];
    	list<RecordType> record2 = [Select Id From RecordType Where Name = '经销商'];
        Account acc = new  Account();
        acc.Name = '浙江医院';
        acc.RecordTypeId = record[0].Id;
        acc.Provinces__c = pro.Id;
        acc.Cities__c = city.Id;
        acc.Region__c = zone.Id;
        insert acc;
        Account acc3 = new  Account();
        acc3.Name = '北京医院';
        acc3.RecordTypeId = record[0].Id;
        acc3.Provinces__c = pro.Id;
        acc3.Cities__c = city.Id;
        acc3.Region__c = zone.Id;
        insert acc3;
        list<Profile> list_Profile = [Select Id,Name From Profile Where Name = 'PM-PSR Manager'];
        list<UserRole> list_role = [Select Id,Name From UserRole Where DeveloperName ='RenalGDAccountManager'];
        User user = new User();
        user.ProfileId = list_Profile[0].Id;
        user.Username = 'asfdasdf@acme.com';
        user.FirstName = 'Test';
        user.LastName = 'Test';
        user.Email = 'asdf@aa.com';
        user.Alias = 'asdf';
        user.EmailEncodingKey = 'UTF-8';
        user.TimeZoneSidKey = 'Asia/Shanghai';
        user.LanguageLocaleKey = 'zh_CN';
        user.LocaleSidKey = 'zh_CN';
        user.UserRoleId = list_role[0].Id;
        insert user; 
    	V2_Account_Team__c vat = new V2_Account_Team__c();
    	vat.V2_History__c = false;
    	vat.V2_Is_Delete__c = false;
    	vat.V2_ApprovalStatus__c = '审批通过';
    	vat.V2_BatchOperate__c = '新增';
    	vat.V2_Effective_Year__c = String.valueOf(Date.today().year());
    	vat.V2_Effective_Month__c = String.valueOf(Date.today().month());
    	vat.V2_Account__c = acc.Id;
    	vat.UserProduct__c = 'PD';
    	vat.V2_User__c = user.id;
    	insert vat;
    	
        PM_SaleHospitalDimensionBatch btBatch = new PM_SaleHospitalDimensionBatch();
		database.executeBatch(btBatch,10);
		system.Test.stopTest();
    }
}