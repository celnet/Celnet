/**
 * Author: Sunny
 * 2013-5-26
**/
public class CtrlVaporizerApplicationApprove {
    private ID VaporizerApplicationId;
    public Vaporizer_Application__c VaporizerApplication{get;set;}
    public Vaporizer_Approve__c VaporizerApprove {get;set;}
    private ID CurrentUserId ;
    private User NextApprover ;
    private Boolean FinalApproval = false;
    private Boolean FinalReject = false;
    public Boolean IsEnableApprove{get;set;}
    public Boolean IsEnableTable{get;set;}
    public Boolean ShowReturnButton{get;set;}
    public Boolean IsVaporizerApprove{get;set;}
    public Boolean IsSupervisorApprove{get;set;}
    public Boolean IsDSMApprove{get;set;}
    public Boolean IsASMApprove{get;set;}
    public Boolean IsRSMApprove{get;set;}
    public Boolean IsDesProductApprove{get;set;}
    public Boolean IsSevoProductApprove{get;set;}
    public Boolean IsNationalDirectorApprove{get;set;}
    
    public CtrlVaporizerApplicationApprove(Apexpages.Standardcontroller controller){
        ShowReturnButton = true;
        this.VaporizerApplicationId = Apexpages.currentPage().getParameters().get('vaid') ;
        if(this.VaporizerApplicationId == null){
            this.VaporizerApplicationId = controller.getId();
            ShowReturnButton = false;
        }
        this.IsEnableApprove = false;
        this.IsEnableTable =false;
        if(this.VaporizerApplicationId == null){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '必须指定一个挥发罐申请') ;            
            ApexPages.addMessage(msg) ;
            
            return ;
        }else{
            this.VaporizerApplication = [Select Id,Name,CurrentApprover__c,
               Hospital__c,Hospital__r.Name,VapOpportunity__c,OwnerId,Owner.Name,Owner.Alias,Vaporizer_Apply_Quantity__c,
               (Select Name, AreaManager__c, RegionalManager__c, Supervisor_Approve__c, Supervisor__c, AreaManager_Approve__c, 
               RegionalManager_Approve__c, SupervisorDefAgree__c, AreaManagerDefAgree__c, RegionalManagerDefAgree__c, ApproveStep__c, 
               ApproveResult__c, Submitter__c, ApproveURL__c, DirectManager__c, DirectManager_Approve__c, DirectManagerDefAgree__c, NationalDirectorDefAgree__c, 
               Supervisor_Comments__c, DirectManager_Comments__c, AreaManager_Comments__c, RegionalManager_Comments__c, VaporizerAdmin__c, 
               VaporizerAdmin_Approve__c, VaporizerAdminDefAgree__c, DesProductManager__c, DesProductManager_Approve__c, DesProductManagerDefAgree__c, 
               SevoProductManager__c, SevoProductManager_Approve__c, SevoProductManagerDefAgree__c, NationalDirector__c, NationalDirector_Approve__c, 
               DesProductManager_Comments__c,SevoProductManager_Comments__c,VapAdmin_Comments__c,NationalDirector_Comments__c,
               VaporizerAdmin__r.Name, VaporizerAdmin__r.Email,VaporizerAdmin__r.Alias,
               Supervisor__r.Name, Supervisor__r.Email, Supervisor__r.Alias,
               DirectManager__r.Name, DirectManager__r.Email, DirectManager__r.Alias,
               AreaManager__r.Name, AreaManager__r.Email, AreaManager__r.Alias,
               RegionalManager__r.Name, RegionalManager__r.Email, RegionalManager__r.Alias,
               DesProductManager__r.Name , DesProductManager__r.Email, DesProductManager__r.Alias,
               SevoProductManager__r.Name , SevoProductManager__r.Email , SevoProductManager__r.Alias,
               NationalDirector__r.Name, NationalDirector__r.Email , NationalDirector__r.Alias, 
               DesProductManagerApproveTime__c, SevoProductManagerApproveTime__c, RegionalManagerApproveTime__c, DirectManagerApproveTime__c, 
               AreaManagerApproveTime__c, NationalDirectorApproveTime__c , SalesSupervisorApproveTime__c, VapAdminApproveTime__c
               From Vaporizer_Applications__r Order by CreatedDate desc) 
               From Vaporizer_Application__c 
               Where Id =: this.VaporizerApplicationId] ;
               
        }
        //检查是否有相应的审批记录
        if(!HaveApproveRecord()){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO , '该挥发罐申请尚未提交审批') ;            
            ApexPages.addMessage(msg) ;
            
            return ;
        }
        this.IsEnableTable = true;
        
        this.CurrentUserId = UserInfo.getUserId();
        
        this.initApproveLine();
    }
    private Boolean HaveApproveRecord(){
        Boolean HaveApproveRecord = false;
        //system.debug(this.VaporizerApplication.Vaporizer_Applications__r.size()+'klm');
        if(this.VaporizerApplication.Vaporizer_Applications__r.size() > 0){
            HaveApproveRecord = true;
            this.VaporizerApprove = this.VaporizerApplication.Vaporizer_Applications__r[0];
        }
        return HaveApproveRecord;
    }
    private void initApproveLine(){
        this.IsEnableApprove = false;
        this.IsASMApprove = false ;
        this.IsDesProductApprove = false;
        this.IsDSMApprove = false;
        this.IsNationalDirectorApprove = false;
        this.IsRSMApprove = false;
        this.IsVaporizerApprove = false;
        this.IsSupervisorApprove = false;
        this.IsSevoProductApprove = false;
        //初始化是否需要当前用户审批
        //system.debug('klm@@@@@@@@@@@@@@@'+this.VaporizerApprove.ApproveStep__c+'**'+this.VaporizerApprove.Supervisor_Approve__c);
        if(this.VaporizerApprove.ApproveStep__c == '挥发罐管理员审批' && this.VaporizerApprove.VaporizerAdmin__c == this.CurrentUserId && this.VaporizerApprove.VaporizerAdmin_Approve__c == null){
            this.IsEnableApprove = true;
            this.IsVaporizerApprove = true;
        }else if(this.VaporizerApprove.ApproveStep__c == '销售主管审批' && this.VaporizerApprove.Supervisor__c == this.CurrentUserId && this.VaporizerApprove.Supervisor_Approve__c == null){
            this.IsEnableApprove = true;
            this.IsSupervisorApprove = true;
        }else if(this.VaporizerApprove.ApproveStep__c == '地区经理(DSM)审批' && this.VaporizerApprove.DirectManager__c == this.CurrentUserId && this.VaporizerApprove.DirectManager_Approve__c == null){
            this.IsEnableApprove = true;
            this.IsDSMApprove = true;
        }else if(this.VaporizerApprove.ApproveStep__c == '区域经理(ASM)审批' && this.VaporizerApprove.AreaManager__c == this.CurrentUserId && this.VaporizerApprove.AreaManager_Approve__c == null){
            this.IsEnableApprove = true;
            this.IsASMApprove = true;
        }else if(this.VaporizerApprove.ApproveStep__c == '大区经理(RSM)审批' && this.VaporizerApprove.RegionalManager__c == this.CurrentUserId && this.VaporizerApprove.RegionalManager_Approve__c == null){
            this.IsEnableApprove = true;
            this.IsRSMApprove = true;
        }else if(this.VaporizerApprove.ApproveStep__c == '产品经理审批' && this.VaporizerApprove.DesProductManager__c == this.CurrentUserId && this.VaporizerApprove.DesProductManager_Approve__c == null){
            this.IsEnableApprove = true;
            this.IsDesProductApprove = true;
        }else if(this.VaporizerApprove.ApproveStep__c == '产品经理审批' && this.VaporizerApprove.SevoProductManager__c == this.CurrentUserId && this.VaporizerApprove.SevoProductManager_Approve__c == null){
            this.IsEnableApprove = true;
            this.IsSevoProductApprove = true;
        }else if(this.VaporizerApprove.ApproveStep__c == '全国总监审批' && this.VaporizerApprove.NationalDirector__c == this.CurrentUserId && this.VaporizerApprove.NationalDirector_Approve__c == null){
            this.IsEnableApprove = true;
            this.IsNationalDirectorApprove =true;
        }
        
    }
    public void CustomApproval(){
        String strOperate = 'Approval';
        this.CustomApprove(strOperate);
    }
    public void CustomReject(){
        String strOperate = 'Reject';
        //无论谁拒绝了，都是最终拒绝
        this.FinalReject = true;
        this.CustomApprove(strOperate);
    }
    public Pagereference ReturnApplication(){
        return new Pagereference('/'+this.VaporizerApplication.Id);
    }
    private void CustomApprove(String strOperate){
        if(this.IsVaporizerApprove){
            this.CustomVaporizerApprove(strOperate);
        }else if(this.IsSupervisorApprove){
            this.CustomSupervisorApprove(strOperate);
        }else if(this.IsDSMApprove){
            this.CustomDSMApprove(strOperate);
        }else if(this.IsASMApprove){
            this.CustomASMApprove(strOperate);
        }else if(this.IsRSMApprove){
            this.CustomRSMApprove(strOperate);
        }else if(this.IsDesProductApprove){
            this.CustomDesProductApprove(strOperate);
        }else if(this.IsSevoProductApprove){
            this.CustomSevoProductApprove(strOperate);
        }else if(this.IsNationalDirectorApprove){
            this.CustomNationalDirectorApprove(strOperate);
        }
        //更新审批
        try{
            update this.VaporizerApprove ;
            this.initApproveLine();
            //如果是最终审批通过，则更新申请上的审批结果字段。
            if(this.FinalApproval){
                this.VaporizerApplication.Approve_Result__c = '通过';
            }
            if(this.FinalReject){
            	this.VaporizerApplication.Approve_Result__c = '拒绝';
            	system.debug(FinalReject + '&&&&&&&&&&&&&');
            }
            system.debug('~~'+this.VaporizerApplication.CurrentApprover__c+' ~~ '+VaporizerApplication.Approve_Result__c);
            update this.VaporizerApplication;
            system.debug('~~'+this.VaporizerApplication.CurrentApprover__c+' ~~ '+VaporizerApplication.Approve_Result__c);
            this.SendEmailToNextApprover();
        }catch(exception e){
            
        }
    }
    private void CustomVaporizerApprove(String strOperate){
        if(strOperate == 'Approval'){//批准
            this.VaporizerApprove.VaporizerAdmin_Approve__c = '通过' ;
            this.VaporizerApprove.VapAdminApproveTime__c = datetime.now();
            this.CustomSupervisorApprove('NextApprover');
        }else if(strOperate == 'Reject'){//拒绝
            this.VaporizerApprove.VaporizerAdmin_Approve__c = '拒绝' ;
            this.VaporizerApprove.VapAdminApproveTime__c = datetime.now();
            this.VaporizerApprove.ApproveResult__c = '审批拒绝';
        }
    }
    private void CustomSupervisorApprove(String strOperate){
        if(strOperate == 'Approval'){//批准
            this.VaporizerApprove.Supervisor_Approve__c = '通过' ;
            this.VaporizerApprove.SalesSupervisorApproveTime__c = datetime.now();
            this.CustomDSMApprove('NextApprover');
        }else if(strOperate == 'Reject'){//拒绝
            this.VaporizerApprove.Supervisor_Approve__c = '拒绝' ;
            this.VaporizerApprove.SalesSupervisorApproveTime__c = datetime.now();
            this.VaporizerApprove.ApproveResult__c = '审批拒绝';
        }else if(strOperate == 'NextApprover'){//作为下一审批人
            if(this.VaporizerApprove.Supervisor__c != null){
                //审批步骤变为销售主管审批
                this.VaporizerApprove.ApproveStep__c = '销售主管审批';
                this.NextApprover = this.VaporizerApprove.Supervisor__r ;
                this.VaporizerApplication.CurrentApprover__c = this.VaporizerApprove.Supervisor__c;
            }else{
                //若没有销售主管则审批步骤进入DSM
                this.CustomDSMApprove('NextApprover');
            }
        }
    }
    private void CustomDSMApprove(String strOperate){
        if(strOperate == 'Approval'){//批准
            this.VaporizerApprove.DirectManager_Approve__c = '通过' ;
            this.VaporizerApprove.DirectManagerApproveTime__c = datetime.now();
            this.CustomASMApprove('NextApprover');
        }else if(strOperate == 'Reject'){//拒绝
            this.VaporizerApprove.DirectManager_Approve__c = '拒绝' ;
            this.VaporizerApprove.DirectManagerApproveTime__c = datetime.now();
            this.VaporizerApprove.ApproveResult__c = '审批拒绝';
        }else if(strOperate == 'NextApprover'){//作为下一审批人
            if(this.VaporizerApprove.DirectManager__c != null){
                //审批步骤变为DSM审批
                this.VaporizerApprove.ApproveStep__c = '地区经理(DSM)审批';
                this.NextApprover = this.VaporizerApprove.DirectManager__r ;
                this.VaporizerApplication.CurrentApprover__c = this.VaporizerApprove.DirectManager__c;
            }else{
                //若没有DSM则审批步骤进入ASM
                this.CustomASMApprove('NextApprover');
            }
        }
    }
    private void CustomASMApprove(String strOperate){
        if(strOperate == 'Approval'){//批准
            this.VaporizerApprove.AreaManager_Approve__c = '通过' ;
            this.VaporizerApprove.AreaManagerApproveTime__c = datetime.now();
            this.CustomRSMApprove('NextApprover');
        }else if(strOperate == 'Reject'){//拒绝
            this.VaporizerApprove.AreaManager_Approve__c = '拒绝' ;
            this.VaporizerApprove.AreaManagerApproveTime__c = datetime.now();
            this.VaporizerApprove.ApproveResult__c = '审批拒绝';
        }else if(strOperate == 'NextApprover'){//作为下一审批人
            if(this.VaporizerApprove.AreaManager__c != null){
                //审批步骤变为ASM审批
                this.VaporizerApprove.ApproveStep__c = '区域经理(ASM)审批';
                this.NextApprover = this.VaporizerApprove.AreaManager__r ;
                this.VaporizerApplication.CurrentApprover__c = this.VaporizerApprove.AreaManager__c;
            }else{
                //若没有ASM则审批步骤进入RSM
                this.CustomRSMApprove('NextApprover');
            }
        }
    }
    private void CustomRSMApprove(String strOperate){
        if(strOperate == 'Approval'){//批准
            this.VaporizerApprove.RegionalManager_Approve__c = '通过' ;
            this.VaporizerApprove.RegionalManagerApproveTime__c = datetime.now();
            this.CustomDesProductApprove('NextApprover');
        }else if(strOperate == 'Reject'){//拒绝
            this.VaporizerApprove.RegionalManager_Approve__c = '拒绝' ;
            this.VaporizerApprove.RegionalManagerApproveTime__c = datetime.now();
            this.VaporizerApprove.ApproveResult__c = '审批拒绝';
        }else if(strOperate == 'NextApprover'){//作为下一审批人
            if(this.VaporizerApprove.RegionalManager__c != null){
                //审批步骤变为RSM审批
                this.VaporizerApprove.ApproveStep__c = '大区经理(RSM)审批';
                this.NextApprover = this.VaporizerApprove.RegionalManager__r ;
                this.VaporizerApplication.CurrentApprover__c = this.VaporizerApprove.RegionalManager__c;
            }else{
                //若没有RSM则审批步骤进入Product
                this.CustomDesProductApprove('NextApprover');
            }
        }
    }
    private void CustomDesProductApprove(String strOperate){
        if(strOperate == 'Approval'){//批准
            this.VaporizerApprove.DesProductManager_Approve__c = '通过' ;
            this.VaporizerApprove.DesProductManagerApproveTime__c = datetime.now();
            this.CustomSevoProductApprove('NextApprover');
        }else if(strOperate == 'Reject'){//拒绝
            this.VaporizerApprove.DesProductManager_Approve__c = '拒绝' ;
            this.VaporizerApprove.DesProductManagerApproveTime__c = datetime.now();
            this.VaporizerApprove.ApproveResult__c = '审批拒绝';
        }else if(strOperate == 'NextApprover'){//作为下一审批人
            if(this.VaporizerApprove.DesProductManager__c != null){
                //审批步骤变为DesProduct审批
                this.VaporizerApprove.ApproveStep__c = '产品经理审批';
                this.NextApprover = this.VaporizerApprove.DesProductManager__r ;
                this.VaporizerApplication.CurrentApprover__c = this.VaporizerApprove.DesProductManager__c;
            }else{
                //若没有DesProduct则审批步骤进入SevoProduct
                this.CustomSevoProductApprove('NextApprover');
            }
        }
    }
    private void CustomSevoProductApprove(String strOperate){
        if(strOperate == 'Approval'){//批准
            this.VaporizerApprove.SevoProductManager_Approve__c = '通过' ;
            this.VaporizerApprove.SevoProductManagerApproveTime__c = datetime.now();
            this.CustomNationalDirectorApprove('NextApprover');
        }else if(strOperate == 'Reject'){//拒绝
            this.VaporizerApprove.SevoProductManager_Approve__c = '拒绝' ;
            this.VaporizerApprove.SevoProductManagerApproveTime__c = datetime.now();
            this.VaporizerApprove.ApproveResult__c = '审批拒绝';
        }else if(strOperate == 'NextApprover'){//作为下一审批人
            if(this.VaporizerApprove.SevoProductManager__c != null){
                //审批步骤变为SevoProduct审批
                this.VaporizerApprove.ApproveStep__c = '产品经理审批';
                this.NextApprover = this.VaporizerApprove.SevoProductManager__r ;
                this.VaporizerApplication.CurrentApprover__c = this.VaporizerApprove.SevoProductManager__c;
            }else{
                //若没有SevoProduct则审批步骤进入NationalDirector
                this.CustomNationalDirectorApprove('NextApprover');
            }
        }
    }
    private void CustomNationalDirectorApprove(String strOperate){
        if(strOperate == 'Approval'){//批准
            this.VaporizerApprove.NationalDirector_Approve__c = '通过' ;
            this.VaporizerApprove.NationalDirectorApproveTime__c = datetime.now();
            this.VaporizerApprove.ApproveResult__c = '审批通过';
            FinalApproval = true;
        }else if(strOperate == 'Reject'){//拒绝
            this.VaporizerApprove.NationalDirector_Approve__c = '拒绝' ;
            this.VaporizerApprove.NationalDirectorApproveTime__c = datetime.now();
            this.VaporizerApprove.ApproveResult__c = '审批拒绝';
        }else if(strOperate == 'NextApprover'){//作为下一审批人
            if(this.VaporizerApprove.NationalDirector__c != null){
                //审批步骤变为SevoProduct审批
                this.VaporizerApprove.ApproveStep__c = '全国总监审批';
                this.NextApprover = this.VaporizerApprove.NationalDirector__r ;
                system.debug('zhixing zheli');
                this.VaporizerApplication.CurrentApprover__c = this.VaporizerApprove.NationalDirector__c;
            }else{
                //若没有NationalDirector则审批变为最终通过
                this.VaporizerApprove.ApproveResult__c = '审批通过';
                FinalApproval = true;
            }
        }
    }
    private void SendEmailToNextApprover(){
        if(this.NextApprover != null){
            String strSubject = '来自SEP系统通知：挥发罐使用申请审批' ;
            String strMessage = '您好：'+this.NextApprover.Alias + '\n\n'
            /**************bill update start 2013/6/5******************/
            + this.VaporizerApplication.Hospital__r.Name+'有新的挥发罐使用申请('+this.VaporizerApplication.Name+')需要您进行审批。\n\n'
            /**************bill update end 2013/6/5******************/
            + '请点击链接查看：'+URL.getSalesforceBaseUrl().toExternalForm()+'/'+this.VaporizerApplication.Id + '\n\n'
            + '祝您工作愉快!\n' +
            + '__________________________________________________ \n' +
            + '本邮件由Baxter Salesforce.com CRM系统产生，请勿回复。\n' +
            + '如有任何疑问或者要求，请联系系统管理人员。\n' +
            + 'Baxter SEP System';
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {this.NextApprover.Email};
            mail.setToAddresses(toAddresses);
            mail.setReplyTo('no-reply@salesforce.com');
            mail.setSubject(strSubject);
            mail.setSenderDisplayName('Baxter SEP System');
            mail.setPlainTextBody(strMessage);
            if(!Test.isRunningTest()) 
            {
                List<Messaging.Sendemailresult> listResult = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                system.debug('Fa song you jian'+listResult);
            }
            
        }
    }
    
    /*******************test start bill add 2013/5/31************************/
    static testMethod void myUnitTest() {
    	
    	//挥发罐申请
    	Vaporizer_Application__c vapApply = new Vaporizer_Application__c();
    	insert vapApply;
    	
    	//挥发罐审批
    	Vaporizer_Approve__c vapApprove = new Vaporizer_Approve__c();
    	vapApprove.Vaporizer_Application__c = vapApply.Id;
    	vapApprove.ApproveStep__c = '挥发罐管理员审批';
    	vapApprove.VaporizerAdmin__c = UserInfo.getUserId();
    	vapApprove.Supervisor__c = UserInfo.getUserId();
    	vapApprove.DirectManager__c = UserInfo.getUserId();
    	vapApprove.AreaManager__c = UserInfo.getUserId();
    	vapApprove.RegionalManager__c = UserInfo.getUserId();
    	vapApprove.DesProductManager__c = UserInfo.getUserId();
    	vapApprove.SevoProductManager__c = UserInfo.getUserId();
    	vapApprove.NationalDirector_Approve__c = UserInfo.getUserId();
    	insert vapApprove; 		
    	
        system.test.startTest();
        CtrlVaporizerApplicationApprove vaa = new CtrlVaporizerApplicationApprove(new Apexpages.Standardcontroller(vapApply));
        vaa.CustomApproval();
        vaa.CustomApproval();
        vaa.CustomApproval();
        vaa.CustomApproval();
        vaa.CustomApproval();
        vaa.CustomApproval();
        vaa.CustomApproval();
        vaa.CustomApproval();
        
        vaa.CustomVaporizerApprove('Reject');
        vaa.CustomSupervisorApprove('Reject');
        vaa.CustomDSMApprove('Reject');
        vaa.CustomASMApprove('Reject');
        vaa.CustomRSMApprove('Reject');
        vaa.CustomDesProductApprove('Reject');
        vaa.CustomSevoProductApprove('Reject');
        vaa.CustomNationalDirectorApprove('Reject');
        system.test.stopTest();
    }
    /*******************test end bill add 2013/5/31**************************/
}