/*
* Tobe
* 2013.10.25
* 自动根据Active APD病人的拜访周期判断该病人的上次拜访状态是否置为“未拜访”
*/
global class PM_TransformStateToNoVisitBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful
{
	private static map<string,CallCycleConfig__c> Config =  CallCycleConfig__c.getAll();
	private integer ActivePatientVisitCycle;
	private integer APDPatientVisitCycle;
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		if(Config.get('ActivePatientVisitCycle').CallCycle1__c !=null)
		{
			ActivePatientVisitCycle = integer.valueOf(Config.get('ActivePatientVisitCycle').CallCycle1__c);
		}
		if(Config.get('APDPatientVisitCycle').CallCycle1__c !=null)
		{
			APDPatientVisitCycle = integer.valueOf(Config.get('APDPatientVisitCycle').CallCycle1__c);
		}
		return Database.getQueryLocator([Select PM_LastSuccessDateTime__c,PM_VisitState__c,PM_Status__c,PM_Pdtreatment__c,PM_WhTocall__c,(Select Id From HotlineirCp__r limit 1)
										 From PM_Patient__c 
										 Where PM_Patient__c.PM_Status__c ='Active' 
												Or PM_Patient__c.PM_Pdtreatment__c ='APD']);
	}
	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		list<PM_Patient__c> list_Patient = new list<PM_Patient__c>();
		for(sObject sObj  : scope)
		{
			boolean isChange = false;
			PM_Patient__c patient = (PM_Patient__c)sObj;
			if(patient.HotlineirCp__r != null && patient.HotlineirCp__r.size()>0)
			{
				patient.PM_WhTocall__c = '是';
				isChange = true;
			}
			if(patient.PM_LastSuccessDateTime__c != null)
			{
				integer daysBetween = patient.PM_LastSuccessDateTime__c.date().daysBetween(date.Today());
				if(patient.PM_Status__c == 'Active' 
				&& ActivePatientVisitCycle != null 
				&& ActivePatientVisitCycle != 0
				&& daysBetween > ActivePatientVisitCycle
				&& patient.PM_VisitState__c=='成功')
				{
					patient.PM_VisitState__c ='未拜访';
					isChange = true;
				}
				else if(patient.PM_Pdtreatment__c == 'APD' 
				&& ActivePatientVisitCycle != null 
				&& ActivePatientVisitCycle != 0
				&& daysBetween > APDPatientVisitCycle
				&& patient.PM_VisitState__c=='成功')
				{
					patient.PM_VisitState__c ='未拜访';
					isChange = true;
				}
			}
			if(isChange)
			{
				list_Patient.add(patient);
			}
		}
		
		update list_Patient;
	}
	global void finish(Database.BatchableContext BC)
	{
	}
}