/**
 * author：bill
 * date：2013-11-11
 * description：PM_PatientDataStatisticsBatch的Schedule
 */
public class PM_PatientDataStatisticsSchedule implements Schedulable
{
    public void execute(SchedulableContext sc) {
    	PM_PatientDataStatisticsBatch patientBatch = new PM_PatientDataStatisticsBatch();
    	//patientBatch.NOW_Date = date.valueOf('2013-7-1');
        database.executeBatch(patientBatch,10);
    }
    
	static testMethod void myUnitTest() 
	{		
        system.test.startTest();
        string sch=DateTime.now().second()+' '+DateTime.now().minute()+' '+DateTime.now().hour()+' * * ?';
        PM_PatientDataStatisticsSchedule pa = new PM_PatientDataStatisticsSchedule();
        System.schedule('test', sch , pa);
        system.test.stopTest();
	}
}