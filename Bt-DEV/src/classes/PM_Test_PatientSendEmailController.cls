/**
 * Author：Dean
 * Date：2013-11-08
 * Function：PM_PatientSendEmailController测试类
 */
@isTest
private class PM_Test_PatientSendEmailController {

    static testMethod void myUnitTest() {
    	
		List<UserRole> listur= new List<UserRole>();
        UserRole ur0 = new UserRole();
        ur0.Name = 'Renal-Supervisor-华南-IVT-Supervisor(U0)';
        listur.add(ur0);
        UserRole ur1 = new UserRole();
        ur1.Name = 'Renal-Supervisor-华南-SP-Supervisor(U1)';
        listur.add(ur1);
        UserRole ur2 = new UserRole();
        ur2.Name = 'Renal-Rep-华南-PD-Rep(U2)';
        listur.add(ur2);
        insert listur;
        ur2.ParentRoleId=ur1.Id;
        update ur2;
        ur1.ParentRoleId = ur0.id;
        update ur1;
        //user 
        list<User> user=[Select TimeZoneSidKey,ProfileId,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname, Alias From User Where IsActive = true limit 1];
        List<User> list_user = new List<User>();
        User user0=new User();
        user0.username='user0r2@023.com';
        user0.LastName='user0r2';
        user0.Email='1095758646@qq.com';
        user0.Alias=user[0].Alias;
        user0.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user0.ProfileId=UserInfo.getProfileId();
        user0.LocaleSidKey=user[0].LocaleSidKey;
        user0.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user0.EmailEncodingKey=user[0].EmailEncodingKey;
        user0.CommunityNickname='chequ23';
        user0.MobilePhone='02345678902';
        user0.IsActive = true;
        user0.UserRoleId=ur0.Id ;
        user0.Alias='zhangsan';
        list_user.add(user0);
        User user1=new User();
        user1.username='user1r2@123.com';
        user1.LastName='user1r2';
        user1.Email='1095758646@qq.com';
        user1.Alias=user[0].Alias;
        user1.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user1.ProfileId=UserInfo.getProfileId();
        user1.LocaleSidKey=user[0].LocaleSidKey;
        user1.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user1.EmailEncodingKey=user[0].EmailEncodingKey;
        user1.CommunityNickname='chequ';
        user1.MobilePhone='12345678912';
        user1.Alias='aaaa';
        user1.IsActive = true;
        user1.UserRoleId=ur0.Id ;
        list_user.add(user1);
        User user2=new User();
        user2.username='user2r2@223.com';
        user2.LastName='user2r2';
        user2.Email='1095758646@qq.com';
        user2.Alias=user[0].Alias;
        user2.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user2.ProfileId=UserInfo.getProfileId();
        user2.LocaleSidKey=user[0].LocaleSidKey;
        user2.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user2.EmailEncodingKey=user[0].EmailEncodingKey;
        user2.CommunityNickname='chequ1';
        user2.MobilePhone='22345678922';
        user2.IsActive = true;
        user2.UserRoleId=ur1.Id ;
        user2.Alias ='bbb';
        list_user.add(user2);
        insert list_user;


		
    	Application__c apli = new Application__c();
    	apli.OwnerId = user2.Id;
    	insert apli;
        system.Test.startTest();
        Apexpages.currentPage().getParameters().put('StringId',apli.Id);
        PM_PatientSendEmailController PmPatient = new PM_PatientSendEmailController();
        PM_PatientSendEmailController PmPatientKnow = new PM_PatientSendEmailController();
        PM_PatientSendEmailController PmPatientKnowE = new PM_PatientSendEmailController();
        
        
        Task tat = new Task();
       
        for(Attachment Attachtd : [Select a.SystemModstamp, 
        a.ParentId, a.OwnerId, a.Name, a.LastModifiedDate, 
        a.LastModifiedById, a.IsPrivate, a.IsDeleted, a.Id, 
        a.Description, a.CreatedDate, a.CreatedById, a.ContentType, 
        a.BodyLength, a.Body From Attachment a])
        {
        	System.debug('###############!!!!!!!!!!!!^^^^^^^^^^^^^^^^^');
        	PmPatient.listAtt.add(Attachtd);
        	PmPatientKnowE.listAtt.add(Attachtd);
        }
        EmailTemplate EmailTemplatelt = new EmailTemplate();
        //PmPatient.ta = ;
        PmPatient.SendA = '10m';
       	PmPatient.SendT = '测试';
        PmPatient.SendText = '测试内容';
        PmPatientKnow.SendA = '1095758646@qq.com';
       	PmPatientKnow.SendT = '测试';
        PmPatientKnow.SendText = '测试内容';
        PmPatientKnowE.SendA = '1095758646@qq.com';
       	PmPatientKnowE.SendT = '测试';
        PmPatientKnowE.SendText = '测试内容';
        
        
         //Blob eass = new Blob();
       Attachment a = new Attachment();
       //a.Body = eass;
       a.Name = 'tes';
       
        //PmPatient.Attach;
       PmPatient.listAtt.add(a);
       PmPatientKnowE.listAtt.add(a);
        //PmPatient.EmailTemplatel;
        PmPatient.IsView = true;
        PmPatient.IsViews = true;
       	PmPatient.IsTest = true;
       	PmPatientKnow.IsTest = true;
       	PmPatientKnowE.IsTest = true;
        PmPatientKnow.SendEmail();
        PmPatientKnowE.SendEmail();
        PmPatient.SendEmail();
        PmPatient.EmailSend('zheyanggdelv@sina.com');
        PmPatient.EmailAYes('1');
        PmPatient.EmailAYes('zheyanggdelv@sina.com');
        system.Test.stopTest();
    }
}