/**
 * Henry
 *2013-10-11
 *Unreachable病人拜访列表页面控制类——测试类
 */
@isTest
private class PM_Test_UnreachableDevController {

    static testMethod void myUnitTest()
    {
        PM_Test_AllConfig.myUnitTest(); //公用方法，内涵必要的一些测试数据
        system.Test.startTest();
    	PM_Patient__c patient = new PM_Patient__c();
    	ApexPages.StandardController controller = new ApexPages.StandardController(patient);
    	//实例化控制类
    	PM_UnreachablevisitlistDevController uv = new PM_UnreachablevisitlistDevController(controller);
    	//初始化get部分字段
    	ApexPages.StandardSetController conset = uv.conset;
    	list<PM_ExportFieldsCommon.Patient> list_Patient = uv.list_Patient;
    	uv.first();
	    uv.last();
	    uv.previous();
	    uv.next();
    	//查询流程
    	list<Profile> list_Profile = [Select Id,Name From Profile Where Name = '系统管理员'];
        User user = new User();
        user.ProfileId = list_Profile[0].Id;
        user.Username = 'asfdasdf@acme.com';
        user.FirstName = 'Test';
        user.LastName = 'Test';
        user.Email = 'asdf@aa.com';
        user.Alias = 'asdf';
        user.EmailEncodingKey = 'UTF-8';
        user.TimeZoneSidKey = 'Asia/Shanghai';
        user.LanguageLocaleKey = 'zh_CN';
        user.LocaleSidKey = 'zh_CN';
        insert user;
	 	uv.Patient.Name = '张先生';
	 	uv.MobilePhone = '18611112222';
 	 	uv.Patient.PM_Current_Saler__c = user.Id;
 	 	uv.BigArea = '东一区';
	    uv.Province = '北京';
 	 	uv.PatientProvince = '北京';
 	 	uv.City = '北京市';
 	 	uv.PatientCity = '北京市';
 	 	uv.County = '海淀区';
 	 	uv.PatientCounty = '海淀区';
 	 	uv.PatientStatus = 'Unreachable';
 	 	uv.RawStatus = 'New';
 	 	uv.LastVisitDate_Start = '2013-10-09';
 	 	uv.LastVisitDate_End = '2013-10-09';
 	 	uv.LastFailDate_Start = '2013-10-09';
 	 	uv.LastFailDate_End = '2013-10-09';
 	 	uv.InHospital = '朝阳区第三医院';
 	 	uv.Patient.PM_Disappearanc__c = '关机';

	    uv.Check();
	    //排序流程
	    uv.strOrder = 'desc';
	    uv.util.patSql = null;
	    uv.sortOrders();
	    uv.sortOrders();
	    //导出
	    uv.exportExcel(); 
	    for(PM_ExportFieldsCommon.Patient pa :uv.list_PatientExport)
	    {
	    	pa.IsExport = true;
	    }
	    uv.exportExcel(); 
	    system.Test.stopTest();
    }
}