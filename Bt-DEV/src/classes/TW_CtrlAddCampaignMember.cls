/*
 *Scott
 *参考大陆地区市场活动成员添加功能
 *
*/
public with sharing class TW_CtrlAddCampaignMember {
	public String CampaignIds{get;set;}
    public List<SobjCampaignMember> ListSobjCam = new List<SobjCampaignMember>();
    public Map<Id,String> contactmap = new Map<Id,String>();
    //public Boolean disabled{get;set;}
    public Boolean IsClose{get;set;}
    //当前用户角色Id
    public Id CurrentRoleId{get;set;}
    //当前用户角色名称
    public String CurrentRoleName{get;set;}
    //当前用户Id
    public Id CurrentUserId{get;set;}
    public String CurrentProfileName{get;set;}
    //判断是否是市场部
    public Boolean IsMarketing{get;set;}
    //市场部邮箱
    public String MarketOwnerEmail{get;set;}
    //不需要逐级审批
    private Boolean IsNotapprovalBylevel{get;set;}
    public Boolean IsDisabled{get;set;}
    public List<SobjCampaignMember> getListSobjCam()
    {
        return ListSobjCam;
    }
	
	public TW_CtrlAddCampaignMember()
	{
		IsClose=false;
        //disabled = false;
        //当前用户信息
        User u = [select UserRole.Name,Id,UserRoleId,Profile.Name from User where Id=:UserInfo.getUserId()];
        CurrentRoleName = u.UserRole.Name;
        CurrentUserId = u.Id;
        CurrentRoleId = u.UserRoleId;
        CurrentProfileName = u.Profile.Name;
        //判读是否是市场部如果是
        if(CurrentRoleName !=null && CurrentRoleName.contains('Marketing')&& CurrentRoleName.contains('TW'))
        {
            IsMarketing = true;
        }
        else
        {
            IsMarketing = false;
        }
        //市场活动Id
        CampaignIds = ApexPages.currentPage().getParameters().get('camid');
        /*********************bill add 2013-8-5 start*********************************/
        //判断是否存在该市场活动
        Campaign CurrentCampaign = new Campaign();
        List<Campaign> list_CurrentCampaign = [select IsActive,Status,Owner.Email,IsNotapprovalBylevel__c,Registration_Starts_approval__c from Campaign where Id=:CampaignIds];
        if(list_CurrentCampaign != null && list_CurrentCampaign.size()>0)
        {
        	CurrentCampaign = list_CurrentCampaign[0];
        }else{
        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, '您所添加的行銷活動不存在，請檢查!');            
            ApexPages.addMessage(msg);
            return;
        }
        if(!CurrentCampaign.IsActive)
        {
        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, '行銷活動還沒有啟用，不能添加行銷活動成員!');            
            ApexPages.addMessage(msg);
            IsDisabled = true;
            return;
        }
        else if(CurrentCampaign.Status !='Sign Up Begin')
        {
        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, '只有是當前行銷活動狀態為“報名已開始”時，才允許添加行銷活動成員。');            
            ApexPages.addMessage(msg);
            IsDisabled = true;
            return;
        }
        
        
        
        /*********************bill add 2013-8-5 end*********************************/
       	MarketOwnerEmail = CurrentCampaign.Owner.Email;
        IsNotapprovalBylevel = CurrentCampaign.Registration_Starts_approval__c;
        for(CampaignMember cam: [select Campaign.Owner.Email,ContactId,Contact.Name,ID_card__c,TW_Department__c,MobilePhone__c from CampaignMember where CampaignId =: CampaignIds])
        {
            cam.MarketOwnerEmail__c = MarketOwnerEmail;
            
            contactmap.put(cam.ContactId,cam.Contact.Name);
        }
        
        CampaignMember cam = new CampaignMember();
        cam.MarketOwnerEmail__c = MarketOwnerEmail;
        SobjCampaignMember sc = new SobjCampaignMember();
        sc.IsDelete = false;
        sc.cm = cam;
        ListSobjCam.add(sc);
	}
	//添加
    public void AddCampaignMember()
    {
        CampaignMember cam = new CampaignMember();
        SobjCampaignMember sc = new SobjCampaignMember();
        sc.IsDelete = false;
        sc.cm = cam;
        ListSobjCam.add(sc);
    }
    //删除
    public void DeleteCampaignMember()
    {
        //新加联系人是否已经是市场活动成员
        for(Integer i=ListSobjCam.size()-1;i>=0;i-- )
        {
            if(ListSobjCam[i].IsDelete)
            {
                ListSobjCam.remove(i);
            }
        }
    }
    //保存
    public PageReference SaveCampaignMember()
    {
    	try
        {
            //如果当期用户是市场部的则将联系人Id存入set
            Set<Id> contactids = new Set<Id>();
            List<CampaignMember> insertCampaignMember = new List<CampaignMember>();
            List<CampaignMember> insertCM = new List<CampaignMember>();
            for(SobjCampaignMember scm:ListSobjCam)
            {
                CampaignMember cam = scm.cm;
                if(cam.ContactId ==null)
                {
                    continue;   
                }
                //如果是市场部
                if(IsMarketing && cam.User__c != null)
                {
                    cam.CampaignId = CampaignIds; 
                    if(scm.ArriveDate != null && scm.ArriveDate !='')
                    {
                        cam.V2_ArriveDate__c = Date.valueOf(scm.ArriveDate);
                    }
                    if(scm.DepartDate != null && scm.DepartDate != '')
                    {
                        cam.V2_DepartDate__c = Date.valueOf(scm.DepartDate);
                    }
                    cam.V2_Participated__c = true;
                    cam.V2_MarketingApprove__c = '通过';
                    insertCampaignMember.add(cam);
                }
                else
                {
                    cam.CampaignId = CampaignIds; 
                    cam.User__c = CurrentUserId;
                    if(scm.ArriveDate != null && scm.ArriveDate !='')
                    {
                        cam.V2_ArriveDate__c = Date.valueOf(scm.ArriveDate);
                    }
                    if(scm.DepartDate != null && scm.DepartDate != '')
                    {
                        cam.V2_DepartDate__c = Date.valueOf(scm.DepartDate);
                    }
                    insertCampaignMember.add(cam);
                    //提交人Id
                
                }
            }
           
            if(insertCampaignMember != null && insertCampaignMember.size()>0)
            {
                insert insertCampaignMember;
            }
            IsClose = true;
            PageReference pageRef = new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/' + CampaignIds);
            return pageRef;
        }catch(Exception e)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR , String.valueOf(e)+' 第'+e.getLineNumber()+'行');            
            ApexPages.addMessage(msg);
            return null;
        }
    }
     //判断是否已经存在此联系人信息
    public void CheckContact() 
    {
        system.debug(contactmap+'Herere1'+ListSobjCam);
        //判断新加联系人是否已经是市场活动成员
        if(contactmap != null && contactmap.size()>0)
        {
            for(SobjCampaignMember scm:ListSobjCam)
            {
                if(scm.cm.ContactId == null)
                {
                    continue;
                }
                if(contactmap.containsKey(scm.cm.ContactId))
                {
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, '已經將聯系人  ’'+contactmap.get(scm.cm.ContactId)+'‘ 添加爲市場活動成員，不允許重複添加爲市場活動成員,已將新加重複聯系人清空！');            
                    ApexPages.addMessage(msg);
                    scm.cm.ContactId = null;
                    return;
                }
            }
        }
        system.debug('Herere1'+ListSobjCam);
        //判断同一联系人是否为多次添加
        if(ListSobjCam != null && ListSobjCam.size()>1)
        {
            for(Integer i=ListSobjCam.size()-1;i>=0;i--)   
            {
                Integer flag=0;
                for(SobjCampaignMember scm2 : ListSobjCam)
                {
                    if(scm2.cm.ContactId == ListSobjCam[i].cm.ContactId)
                    {
                        flag++;
                    }
                }
                if(flag >=2)
                {
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, '同壹HCP，不允許重複添加爲成員,已將新加重複HCP清空。 ');            
                    ApexPages.addMessage(msg);
                    ListSobjCam[i].cm.ContactId = null;
                    return;
                }
            }
        }
        //显示联系人客户名称
        Set<Id> contactids = new Set<Id>();
        for(SobjCampaignMember scm:ListSobjCam)
        {
            if(scm.cm.ContactId != null )
            {
                contactids.add(scm.cm.ContactId);
            }
        }
        system.debug('HERERER:'+contactids);
        for(Contact co:[select AccountId,Id,ID_card__c,Department,MobilePhone,OwnerId,Owner.Name from Contact where id in:contactids])
        {
            for(SobjCampaignMember scm:ListSobjCam)
            {
                if(scm.cm.ContactId == co.Id)
                {
                    scm.cm.V2_Account__c = co.AccountId;
                    scm.cm.TW_Department__c = co.Department;
					scm.cm.MobilePhone__c = co.MobilePhone;
					if(IsMarketing)
					{
						list<SelectOption> AccMembers=new list<SelectOption>();
						AccMembers.add(new SelectOption(co.OwnerId,co.Owner.Name));
					 	scm.UserList=AccMembers;
					}
                }
            }
        }
        // 如果是市场部的则要根据联系人显示 其所属客户下 的客户小组
        if(IsMarketing)
        {
            getUserlist();
        }
    }
    //通过客户得出其下的客户小组成员
    public void getUserlist()
    {
        try
        {
            //客户Ids
            Set<Id> AccIds = new Set<Id>();
            for(SobjCampaignMember scm:ListSobjCam)
            {
                if(scm.cm.V2_Account__c ==null)
                {
                    continue;
                }
                AccIds.add(scm.cm.V2_Account__c);
            }
            
           //客户小组
            List<AccountTeamMember> List_AccTeam = new List<AccountTeamMember>();
            List_AccTeam = [Select User.Name,UserId,AccountId From AccountTeamMember where AccountId in:AccIds];
        
            for(SobjCampaignMember scm:ListSobjCam)
            {
                list<SelectOption> AccMembers=new list<SelectOption>();
                for(AccountTeamMember atm:List_AccTeam)
                {
                    if(atm.AccountId != scm.cm.V2_Account__c)
                    {
                        continue;
                    }
                    AccMembers.add(new SelectOption(atm.UserId,atm.User.Name));
                }
                scm.UserList.addAll(AccMembers);
            }
        }catch(Exception e)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR , String.valueOf(e)+' 第'+e.getLineNumber()+'行');            
            ApexPages.addMessage(msg);
            return;
        }
        
    }
    public PageReference ReturnCampaign()
	{
		return new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/' + CampaignIds);
	}
	
//封装类
    public class SobjCampaignMember
    {
        public Id UserId{get;set;}
        public List<SelectOption>UserList{get;set;}
        public CampaignMember cm {get;set;}
        public Boolean IsDelete{get;set;}
        public String DepartDate{get;set;}
        public String ArriveDate{get;set;}
    }
    /*================================测试类============================*/
    static testMethod void TW_CtrlAddCampaignMember()
    {
    	  //销售
	    UserRole RepUserRole = new UserRole() ;
	    RepUserRole.Name = 'TW South Sales Rep';
	    insert RepUserRole ;
	    //市场部
	    UserRole MarketUserRole = new UserRole() ;
	    MarketUserRole.Name = 'TW Marketing Product Manager';
	    insert MarketUserRole ;
	    /*用户简档*/
		//rep简档
	    Profile RepPro = [select Id from Profile where Name  = 'TW Sales Rep' limit 1];
		Profile MarketPro = [select Id from Profile where Name  = 'TW Marketing' limit 1];
		/************User************/
		List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ; 
	     
	    /*销售*/
	    User RepSu = new User();
	    RepSu.Username='RepSu@123.com';
	    RepSu.LastName='RepSu';
	    RepSu.Email='RepSu@123.com';
	    RepSu.Alias=user[0].Alias;
	    RepSu.TimeZoneSidKey=user[0].TimeZoneSidKey;
	    RepSu.ProfileId=RepPro.Id;
	    RepSu.LocaleSidKey=user[0].LocaleSidKey;
	    RepSu.LanguageLocaleKey=user[0].LanguageLocaleKey;
        RepSu.EmailEncodingKey=user[0].EmailEncodingKey;
	    RepSu.CommunityNickname='RepSu';
	    RepSu.MobilePhone='12345678912';
	    RepSu.UserRoleId = RepUserRole.Id ;
	    RepSu.IsActive = true;
     	insert RepSu;
    	/*市场部*/
    	User MarketSu = new User();
	    MarketSu.Username='MarketSu@123.com';
	    MarketSu.LastName='MarketSu';
	    MarketSu.Email='MarketSu@123.com';
	    MarketSu.Alias=user[0].Alias;
	    MarketSu.TimeZoneSidKey=user[0].TimeZoneSidKey;
	    MarketSu.ProfileId=MarketPro.Id;
	    MarketSu.LocaleSidKey=user[0].LocaleSidKey;
	    MarketSu.LanguageLocaleKey=user[0].LanguageLocaleKey;
        MarketSu.EmailEncodingKey=user[0].EmailEncodingKey;
	    MarketSu.CommunityNickname='MarketSu';
	    MarketSu.MobilePhone='12345678912';
	    MarketSu.UserRoleId = MarketUserRole.Id ;
	    MarketSu.IsActive = true;
     	insert MarketSu;
    	
    	/*客户*/
        RecordType accrecordtype = [select Id from RecordType where DeveloperName='TW_Hospital' and SobjectType='Account' limit 1];
        Account acc = new Account();
        acc.RecordTypeId = accrecordtype.Id;
        acc.Name = 'AccTest';
        insert acc;
        /*联系人*/
        RecordType conrecordtype = [select Id from RecordType where DeveloperName = 'TW_Contact' and SobjectType='Contact' and IsActive = true limit 1 ];
        Contact con1 = new Contact();
        con1.RecordTypeId = conrecordtype.Id;
        con1.LastName = 'AccTestContact1';
        con1.AccountId=acc.Id;
        insert con1;
        
        Contact con2 = new Contact();
        con2.RecordTypeId = conrecordtype.Id;
        con2.LastName = 'AccTestContact2';
        con2.AccountId=acc.Id;
        insert con2;
        Contact con3 = new Contact();
    	con3.LastName = 'AccTestContact3';
    	con3.AccountId=acc.Id;
    	insert con3;
        /*市场活动*/
        Campaign cam = new Campaign();
        cam.Name = 'CamTest';
        cam.StartDate = date.today().addMonths(1);
        cam.EndDate = date.today().addMonths(2);
        cam.IsActive = true;
        cam.Status ='Sign Up Begin';
        insert cam;
        
        Campaign cam2 = new Campaign();
        cam2.Name = 'CamTest2';
        cam2.StartDate = date.today().addMonths(1);
        cam2.EndDate = date.today().addMonths(2);
        cam2.IsActive = true;
        cam2.Status ='Sign Up Begin';
        insert cam2;
        /*市场活动成员*/
        CampaignMember cm = new CampaignMember();
        cm.CampaignId = cam.Id;
        cm.ContactId = con2.Id;
        insert cm;
        
        Test.startTest();
        //当前用户Id
        Id CurrentUserId = UserInfo.getUserId();
        ApexPages.currentPage().getParameters().put('camid',cam.Id);
        TW_CtrlAddCampaignMember cacm = new TW_CtrlAddCampaignMember();
        //添加
        cacm.AddCampaignMember();
        //删除
        cacm.AddCampaignMember();
        SobjCampaignMember scm = cacm.getListSobjCam()[cacm.getListSobjCam().size()-1];
        scm.IsDelete = true;
        cacm.DeleteCampaignMember();
        //检查联系人
        cacm.AddCampaignMember();
        SobjCampaignMember scm1 = cacm.getListSobjCam()[cacm.getListSobjCam().size()-1];
        scm1.cm.ContactId = con1.Id;
        scm1.DepartDate = String.valueOf(Date.today());
        scm1.ArriveDate = String.valueOf(Date.today().addDays(5));
        cacm.CheckContact();
        //相同联系人报错
        cacm.AddCampaignMember();
        SobjCampaignMember scm2 = cacm.getListSobjCam()[cacm.getListSobjCam().size()-1];
        scm2.cm.ContactId = con1.Id;
        try
        {
            cacm.CheckContact();
        }catch(Exception e)
        {
            System.debug('相同联系人报错'+String.valueOf(e));
        }
        //保存
        cacm.SaveCampaignMember();
        /**市场部添加**/
        System.runAs(MarketSu)
        {
        	Campaign cam3 = new Campaign();
        	cam3.Name = 'CamTest3';
        	cam3.StartDate = date.today().addMonths(1);
        	cam3.EndDate = date.today().addMonths(2);
        	cam3.IsActive = true;
        	cam.Status ='Sign Up Begin';
        	insert cam3;
        
            ApexPages.currentPage().getParameters().put('camid',cam3.Id);
            TW_CtrlAddCampaignMember cacm3 = new TW_CtrlAddCampaignMember();
            //添加
            cacm3.AddCampaignMember();
            SobjCampaignMember scm3 = cacm3.getListSobjCam()[cacm3.getListSobjCam().size()-1];
            scm3.ArriveDate  = String.valueOf(date.today());
            scm3.DepartDate = String.valueOf(date.today().addDays(-2));
            scm3.cm.ContactId = con1.Id;
            scm3.cm.User__c = RepSu.Id;
            scm3.cm.V2_Account__c = acc.Id;
            cacm3.CheckContact();
            //客户小组成员
            cacm3.getUserlist();
        }
        ApexPages.currentPage().getParameters().put('camid','333');
        TW_CtrlAddCampaignMember cacm4 = new TW_CtrlAddCampaignMember();
        Campaign cam4 = new Campaign();
        cam4.Name = 'CamTest';
        cam4.StartDate = date.today().addMonths(1);
        cam4.EndDate = date.today().addMonths(2);
        insert cam4;
        ApexPages.currentPage().getParameters().put('camid',cam4.Id);
        TW_CtrlAddCampaignMember cacm5 = new TW_CtrlAddCampaignMember();
        cam4.IsActive = true;
        update cam4;
        ApexPages.currentPage().getParameters().put('camid',cam4.Id);
        TW_CtrlAddCampaignMember cacm6 = new TW_CtrlAddCampaignMember();
        Test.stopTest();
    }
}