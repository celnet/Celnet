/*
*功能：重新计算病人的拜访失败次数
*作者：Alisa
*时间：2014-2-19
*条件：根据病人查询出其对应的事件，根据最近一个事件中的PM_VisitStatus__c是“成功”的事件然后根据StartDateTime大于
*      这个事件的StartDateTime以后的所有事件的个数即是这个病人拜访失败次数(情况，若这个病人下无事件呢，若有事件
*      没成功的事件或成功事件为最后一个事件时、成功事件在中间或第一个（平常情况）
*/
global class PM_CalculateHisVisiteFailedNumBatch implements Database.Batchable<sObject>{
	
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		return Database.getQueryLocator([select Id,Name from PM_Patient__c]);
	}
	global void execute(Database.BatchableContext BC,List<PM_Patient__c> scope)
	{
		list<PM_Patient__c> listUpPatients = new list<PM_Patient__c>();
		set<Id> setPId = new set<Id>();
		for(PM_Patient__c p : scope)
		{
			setPId.add(p.Id);
		}
		list<Event> listSuccessEvents = new list<Event>();
		listSuccessEvents = [select Id,WhatId,PM_VisitStatus__c,StartDateTime
							 from Event
							 where WhatId IN :setPId and PM_VisitStatus__c = '完成'
							 order by StartDateTime desc];
		map<Id,list<Event>> mapPIdAndEvents = new map<Id,list<Event>>();
		if(listSuccessEvents != null && listSuccessEvents.size()>0)
		{
			for(Event e : listSuccessEvents)
			{
				if(mapPIdAndEvents.containsKey(e.WhatId))
				{
					mapPIdAndEvents.get(e.WhatId).add(e);
				}
				else
				{
					list<Event> listEvents = new list<Event>();
					listEvents.add(e);
					mapPIdAndEvents.put(e.WhatId,listEvents);
				}
			}
		}
		
		for(PM_Patient__c p : scope)
		{
			list<Event> listEverySuccessEvents = new list<Event>();
			if(mapPIdAndEvents.containsKey(p.Id))
			{
				listEverySuccessEvents = mapPIdAndEvents.get(p.Id);
			}
			if(listEverySuccessEvents != null && listEverySuccessEvents.size()>0)
			{
				list<Event> listFailEvents = new list<Event>();
				listFailEvents = [select Id,WhatId,PM_VisitStatus__c,StartDateTime
				      			  from Event
				      			  where WhatId =: p.Id and PM_VisitStatus__c = '失败' and StartDateTime >: listEverySuccessEvents[0].StartDateTime];
				if(listFailEvents != null && listFailEvents.size() > 0)
				{
					p.PM_VisitFailCount__c = listFailEvents.size();
					listUpPatients.add(p);
				}  
				else
				{
					p.PM_VisitFailCount__c = null;
					listUpPatients.add(p);
				}     			  
			}
			else
			{
				list<Event> listAllFailEvents = new list<Event>();
				listAllFailEvents = [select Id,WhatId from Event where WhatId =: p.Id and PM_VisitStatus__c = '失败'];
				if(listAllFailEvents != null && listAllFailEvents.size()>0)
				{
					p.PM_VisitFailCount__c = listAllFailEvents.size();
					listUpPatients.add(p);
				} 
				else
				{
					p.PM_VisitFailCount__c = null;
					listUpPatients.add(p);
				} 
			}
		}
		update listUpPatients;
	}
	global void finish(Database.BatchableContext BC)
	{
		
	}
}