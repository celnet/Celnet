/**
 * author:bill
 * date : 2013-12-2
 * trigger：PM_PSRMonthyPlanOnlyone的测试类
 */
@isTest
private class PM_Test_PSRMonthyPlanOnlyone 
{
	static testMethod void myUnitTest()
    {
        PM_PSRMonthPlan__c plan = new PM_PSRMonthPlan__c();
        plan.PM_Year__c = '2013';
        plan.PM_Month__c = '12';
        system.Test.startTest();
        insert plan;
        plan.PM_ActualCall__c = 100;
        update plan;
        system.Test.stopTest();
    }
}