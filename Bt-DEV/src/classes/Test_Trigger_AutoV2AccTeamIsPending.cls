/**
 * AutoV2AccTeamIsPending的trigger测试类
 */
@isTest
private class Test_Trigger_AutoV2AccTeamIsPending {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        V2_Account_Team__c team1 = new V2_Account_Team__c();
		team1.V2_ApprovalStatus__c = '审批中';
		insert team1;
		
		system.Test.startTest();
		team1.V2_ApprovalStatus__c = '审批通过';
		update team1;
		system.debug(team1.IsPending__c+'zgxm');
		system.Test.stopTest();
    }
}