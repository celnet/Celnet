/**
 * 作者：Sunny Sun
 * 说明：SEP PD KPI计算
**/
public class ClsPdKpiService {
    ID UserId ;
    Integer IntYear ;
    Integer IntMonth ;
    List<Event> list_ValidEvent ;
    Set<ID> set_ValidOppIds ;
    public ClsPdKpiService(){
        
    }
    public ClsPdKpiService(ID sUserId,Integer sIntYear,Integer sIntMonth){
        UserId = sUserId;
        IntYear = sIntYear;
        IntMonth = sIntMonth ; 
    }
    
    //REP KPI 
    
    //KPI:院长及药剂科拜访率
    //说明：针对行政科及药剂科医生的拜访量/拜访总量(renal联系人部门为行政科和药剂科-拜访相关联系人，记录类型为Renal,部门为“行政”或“药剂科”)
    public Map<String , Double> DirectorVisitRate(){
        Map<String , Double> map_Result = new Map<String , Double>();
        //获取有效的拜访
        if(list_ValidEvent==null){
            list_ValidEvent = getValidVisit();
        }
        List<Event> list_ValidVisit = list_ValidEvent;
        //拜访总量
        Double VistQty = list_ValidVisit.size();
        //院长&药剂科拜访量
        Double DirectorVisitQty = 0 ;
        List<ID> list_whoId = new List<ID>();
        for(Event objEvent : list_ValidVisit){
            list_whoId.add(objEvent.WhoId);
            //if(objEvent.Who.RecordType.DeveloperName != null && objEvent.Who.RecordType.DeveloperName == 'RecordType'){
                //DirectorVisitQty++;
            //}
        }
        Set<ID> set_WhoId = new Set<ID>();
        //2013-4-1 去掉联系人为renal记录类型的限制。
        List<String> list_Department = new List<String>{'药剂科','行政','院办','采购中心','医务科','医院医保办','市医保处(政府部门)','财务科','招标办','设备科'};
        for(Contact objContact : [Select Id,RecordTypeId,RecordType.DeveloperName From Contact Where Id in: list_whoId  And V2_RenalDepartmentType__c in: list_Department]){
            set_WhoId.add(objContact.Id);
        }
        for(Event objEvent : list_ValidVisit){
            if(set_WhoId.contains(objEvent.WhoId)){
                DirectorVisitQty++;
            }
        }
        map_Result.put('Target' , VistQty);
        map_Result.put('Finish' , DirectorVisitQty);
        system.debug(VistQty+' +++ '+DirectorVisitQty);
        if(VistQty==0){
            map_Result.put('Rate' , 0);
        }else{
            map_Result.put('Rate' , DirectorVisitQty/VistQty);
        }
        return map_Result;
    } 
    ///已完成、未过期的拜访
    private List<Event> getValidVisit(){
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        //List<Event> list_Visit=[Select Id,GAPlan__c,WhoId,Who.Name,Who.RecordTypeId,GAExecuteResult__c From Event ];
        List<Event> list_Visit=[Select Id,GAPlan__c,WhoId,GAExecuteResult__c From Event Where IsChild = false And RecordType.DeveloperName = 'V2_Event' And Done__c = true And V2_IsExpire__c = false And StartDateTime >: CheckStartDateTime And StartDateTime <: CheckEndDateTime And OwnerId =: UserId];
        return list_Visit ;
    }
    
    //KPI:业务机会数
    //说明：当月进行中的业务机会
    public Map<String , Double> OpportunityQuantity(){
        Map<String , Double> map_Result = new Map<String , Double>();
        if(set_ValidOppIds == null){
            set_ValidOppIds = getValidOpportunity();
        }
        Set<ID> set_OppId = set_ValidOppIds;
        map_Result.put('Target' , 4);
        map_Result.put('Finish' , set_OppId.size());
        return map_Result;
    }
    //获取有效的业务机会
    private Set<ID> getValidOpportunity(){
        Date CheckDate = date.valueOf(IntYear+'-'+IntMonth+'-1');
        Date endMonthDate = CheckDate.addMonths(1);
        
        Set<ID> set_ValidOppId = new Set<ID>();
        //查找用户对应月份负责的业务机会
        Set<ID> set_OppId = V2_UtilClass.GetOpportunity(IntYear,IntMonth,UserId);
        
        //一直在进行中的业务机会
        List<String> list_OppStage = new List<String>{'发现/验证机会','建立沟通渠道','需求分析','提交合作方案/谈判','跟进合作方案'};
        for(Opportunity opp : [Select Id,Renal_Opp_MGT__c From Opportunity 
                Where StageName in: list_OppStage 
                And RecordType.DeveloperName = 'RENAL'  
                And CloseDate >=: CheckDate
                And Id in: set_OppId
                And CreatedDate <: endMonthDate]){
            if(opp.Renal_Opp_MGT__c == '接受' ||  opp.Renal_Opp_MGT__c == null){
                set_ValidOppId.add(opp.Id);
            }
        }
        //对应月份曾经在进行中的业务机会 2013-3-29 Sunny修改：对曾经进行中的业务机会，只需要判断查询月曾经有过进行中的阶段，不需要判断结束日期
        for(OpportunityHistory__c OppHistory : [Select Id,Opportunity__c,Opportunity__r.Renal_Opp_MGT__c From OpportunityHistory__c 
                Where ChangedDate__c >=: CheckDate 
                And ChangedDate__c <: endMonthDate 
                And (NewOppStage__c in: list_OppStage OR PastOppStage__c in: list_OppStage)
                And Opportunity__r.RecordType.DeveloperName = 'RENAL'  
                //And Opportunity__r.CloseDate >=: CheckDate
                And Opportunity__c in: set_OppId]){
            if(OppHistory.Opportunity__r.Renal_Opp_MGT__c == '接受' ||  OppHistory.Opportunity__r.Renal_Opp_MGT__c == null){
                set_ValidOppId.add(OppHistory.Opportunity__c);
            }
        }
        return set_ValidOppId;
        
    }
    
    //KPI:业务机会相关拜访达成率
    //2013-3-28 sunny 修改，超简单逻辑，该指标所有BU不需要考虑曾经进行中的业务机会只要检查当前进行中的业务机会
    public Map<String , Double> OpportunityVisitRate(){
        Map<String , Double> map_Result = new Map<String , Double>();
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        //获取所有有效的业务机会的ID
        Set<ID> set_OppId = getValidOpportunityForVisitRate();
        //有效跟进的业务机会数量
        Double ValidOppQty = 0;
        //所有进行中的业务机会数量
        Double OppQty = set_OppId.size();
        //标准为4个
        Double OppStaQty = 4 ;
        //查找这些业务机会的有效拜访（已完成，未过期）
        for(Opportunity objOpp : [Select Id,(Select StartDateTime, OwnerId, Done__c, V2_IsExpire__c From Events Where IsChild = false And RecordType.DeveloperName = 'V2_Event' And Done__c = true And V2_IsExpire__c = false And StartDateTime>:CheckStartDateTime And StartDateTime<:CheckEndDateTime) From Opportunity o Where Id in: set_OppId]){
            if(objOpp.Events!=null && objOpp.Events.size() >= 4){
                ValidOppQty++;
            }
        }
        map_Result.put('Target' , 4);
        map_Result.put('Finish' , ValidOppQty);
        map_Result.put('Rate' , ValidOppQty / OppStaQty);
        return map_Result;
    }
    private Set<ID> getValidOpportunityForVisitRate(){
        Date CheckDate = date.valueOf(IntYear+'-'+IntMonth+'-1');
        Date endMonthDate = CheckDate.addMonths(1);
        
        Set<ID> set_ValidOppId = new Set<ID>();
        //查找用户对应月份负责的业务机会
        //Set<ID> set_OppId = V2_UtilClass.GetOpportunity(IntYear,IntMonth,UserId);
        
        //一直在进行中的业务机会  2013-3-29 sunny修改：添加结束日期限制
        List<String> list_OppStage = new List<String>{'发现/验证机会','建立沟通渠道','需求分析','提交合作方案/谈判','跟进合作方案'};
        for(Opportunity opp : [Select Id,Renal_Opp_MGT__c From Opportunity 
                Where StageName in: list_OppStage 
                And CloseDate >=: CheckDate
                And RecordType.DeveloperName = 'RENAL'  
                And OwnerId =: UserId
                And CreatedDate <: endMonthDate]){
            if(opp.Renal_Opp_MGT__c == '接受' ||  opp.Renal_Opp_MGT__c == null){
                set_ValidOppId.add(opp.Id);
            }
        }
        return set_ValidOppId;
    }
    
    
    //KPI:拜访完成率
    //当月已完成且未过期的拜访数/计划拜访数。
    public Map<String , Double> VisitRate(){
        Map<String , Double> map_Result = new Map<String , Double>();
        //获取已完成未过期的拜访
        if(list_ValidEvent==null){
            list_ValidEvent = getValidVisit();
        }
        List<Event> list_Event = list_ValidEvent;
        //已完成未过期拜访数
        Double VisitQty = list_Event.size();
        
        //计划拜访数（当月月计划明细中的所有“计划次数”字段的值之和）
        List<MonthlyPlan__c> monthPlan = [Select Id,V2_TotalCallRecords__c From MonthlyPlan__c Where Year__c =: String.valueOf(IntYear) And Month__c =: String.valueOf(IntMonth) And OwnerId =: UserId Limit 1] ;
        Double PlanQty = 0;
        if(monthPlan.size() > 0){
            PlanQty =  monthPlan[0].V2_TotalCallRecords__c;
        }
        
        map_Result.put('Target' , PlanQty);
        map_Result.put('Finish' , VisitQty);
        
        if(PlanQty==null || PlanQty==0){
            map_Result.put('Rate' , 0);
        }else{
            map_Result.put('Rate' , VisitQty/PlanQty) ;
        }
        return map_Result;
    }
    
    //KPI:科室会数量
    //组织召开科室会的数量
    //2013-3-28 sunny 修改，科室会添加限制条件，要求是“已完成”
    public Map<String , Double> DepartmentVisitQuantity(){
        Map<String , Double> map_Result = new Map<String , Double>();
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        List<Event> list_DepartmentVisit=[Select Id From Event Where RecordType.DeveloperName = 'V2_RecordType' And Done__c = true And SubjectType__c = '科室会' And StartDateTime >: CheckStartDateTime And StartDateTime <: CheckEndDateTime And OwnerId =: UserId];
        map_Result.put('Target' , 2);
        map_Result.put('Finish' , list_DepartmentVisit.size());
        return map_Result ;
    }
    //PD部门科室会指标辅助，季度科室会数量，2014-1-15 Sunny添加
    public Map<String , Double> DepartmentVisitQuarterQuantity(){
    	Map<String , Double> map_Result = new Map<String , Double>();
    	DateTime CheckStartDateTime;
        if(intMonth == 1 || intMonth == 4 || intMonth == 7 || intMonth == 10){
        	CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        }else if(intMonth == 2 || intMonth == 5 || intMonth == 8 || intMonth == 11){
        	CheckStartDateTime = datetime.newInstance(IntYear, (IntMonth-1), 1, 0, 0, 0) ;
        }else{
        	CheckStartDateTime = datetime.newInstance(IntYear, (IntMonth-2), 1, 0, 0, 0) ;
        }
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        List<Event> list_DepartmentVisit=[Select Id From Event Where RecordType.DeveloperName = 'V2_RecordType' And Done__c = true And SubjectType__c = '科室会' And StartDateTime >: CheckStartDateTime And StartDateTime <: CheckEndDateTime And OwnerId =: UserId];
    	map_Result.put('Finish' , list_DepartmentVisit.size());
    	return map_Result;
    }
    
    //KPI:访前计划及访后分析完成率
    //说明：当月已完成的未过期的拜访中填写访前计划和访后分析的比例((访前计划完成%+访后分析完成%)/2)
    public Map<String , Double> VisitPlanRate(){
        Map<String , Double> map_Result = new Map<String , Double>();
        //拜访次数
        double VisitNum;
        //访前计划次数
        double VisitPlanNum;
        //访后分析次数
        double VisitAnalysisNum;
        //访前计划完成率
        double VisitPlan;
        //访后反洗完成率
        double VisitAnalysis;
        
        //获取已完成未过期的拜访
        if(list_ValidEvent==null){
            list_ValidEvent = getValidVisit();
        }
        List<Event> list_Event = list_ValidEvent;
        
        VisitNum = list_Event.size() ;
        for(Event objEvent : list_Event){
            if(objEvent.GAPlan__c != null){
                VisitPlanNum = (VisitPlanNum==null?0:VisitPlanNum) + 1 ;
            }
            if(objEvent.GAExecuteResult__c != null){
                VisitAnalysisNum = (VisitAnalysisNum==null?0:VisitAnalysisNum) + 1 ;
            }
        }
        
        if(VisitNum != null && VisitPlanNum != null){
            VisitPlan = VisitPlanNum / VisitNum ;
        }
        if(VisitNum != null && VisitAnalysisNum != null){
            VisitAnalysis = VisitAnalysisNum / VisitNum;
        }
        map_Result.put('Target' , VisitNum);
        map_Result.put('Finish Before' , VisitPlanNum);
        map_Result.put('Finish After' , VisitAnalysisNum);
        map_Result.put('Rate' , ((VisitPlan==NULL?0:VisitPlan)+(VisitAnalysis==NULL?0:VisitAnalysis))/2);
        
        return map_Result ;
        
    }
    
    
    //KPI:拜访质量评分
    //主管协访后，在协访中根据拜访质量标准给予代表评分的平均分
    public Map<String , Double> VisitGrade(){
        Map<String , Double> map_Result = new Map<String , Double>();
        //获取已完成未过期的拜访
        if(list_ValidEvent==null){
            list_ValidEvent = getValidVisit();
        }
        List<Event> list_Visit = list_ValidEvent;
        List<ID> list_EventId = new List<ID>();
        for(Event eve : list_Visit){
            list_EventId.add(eve.Id);
        }
        Double Grade = 0;
        List<AssVisitComments__c> list_VisitComm = [Select Id,Grade__c From AssVisitComments__c Where EventId__c in: list_EventId And BeReviewed__c =: UserId And Grade__c != null];
        for(AssVisitComments__c VisitComm : list_VisitComm){
            if(Grade == null){
                Grade = Integer.valueOf(VisitComm.Grade__c) ;
            }else{
                Grade = Grade + Integer.valueOf(VisitComm.Grade__c);
            }
        }
        map_Result.put('TotalGrade' , Grade);
        map_Result.put('TotalNum' , list_VisitComm.size());
        if(list_VisitComm.size() > 0){
            map_Result.put('AVGGrade',Grade/list_VisitComm.size());
        }else{
            map_Result.put('AVGGrade',0);
        }
        return map_Result;
    }
    
    
    //主管 KPI
    
    //KPI：直接管理的销售团队的平均SEP业绩
    //直接管理的销售团队的平均SEP业绩
    public Map<String , Double> SepTeamPerformanceAVG(Boolean IsHaveHospital , Double SuperValue){
        Map<String , Double> map_Result = new Map<String , Double>();
        Double AVGGrade = 0;
        //直接汇报关系的下属
        List<ID> list_uId = new List<ID>();
        //查询所有在查询月份直接汇报下属
        for(V2_RoleHistory__c RoleHistory : [Select v.Role__c, v.Manager__c, v.Renal_valid_super__c, v.Name__c, v.IsOnHoliday__c, v.IsLeave__c, v.Department__c, v.PG__c, v.Level__c
            From V2_RoleHistory__c v 
            Where Month__c =: String.valueOf(intMonth) And Year__c =: String.valueOf(intYear) And IsOnHoliday__c = false And IsLeave__c = false And Status__c != '停用' And Manager__c=:UserId]){
            list_uId.add(RoleHistory.Name__c);
        }
        //下属人数
        Double TotalNum = 0 ;
        //下属得分
        Double TotalGrade = 0 ;
        //直接下属的奖金评分
        //2013-4-23 Sunny修改bug，查找奖金数据添加年月的条件
        for(Bonus_data__c bd:[Select Id,Total_Score__c From Bonus_data__c Where The_User__c in: list_uId And Month__c =: String.valueOf(intMonth) And Year__c =: String.valueOf(intYear)]){
            TotalNum++;
            
            TotalGrade = TotalGrade + (bd.Total_Score__c==null?0:bd.Total_Score__c);
        }
        system.debug(IsHaveHospital+'xiashu'+TotalGrade+' - '+TotalNum);
        if(IsHaveHospital == true){
            TotalNum+=1;
            TotalGrade += (SuperValue==null?0:SuperValue) ;
        }
        if(TotalNum!=0){
            AVGGrade = TotalGrade / TotalNum;
        }
        map_Result.put('TotalGrade' , TotalGrade);
        map_Result.put('TotalNum' , TotalNum);
        map_Result.put('AVGGrade' , AVGGrade);
        
        system.debug('pingjunfen'+map_Result);
        return map_Result ;
    }
    
    //KPI:院长及药剂科拜访率(包含协访)
    //针对行政科及药剂科医生的(拜访量+协访量(协访可过期))/(拜访总量+协访总量(协访可过期))
    public Map<String , Double> DirectorVisitRateIncludeAss(){
        Map<String , Double> map_Result = new Map<String , Double>();
        //获取协访ID
        Set<ID> set_AssVisitId = getAssVisit();
        //协访数量
        Double AssVisitQty = set_AssVisitId.size();
        //有效协访数量
        Double AssValidVisitQty = 0 ;
        
        
        //获取有效的拜访
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        List<Event> list_ValidVisit = [Select Id,GAPlan__c,WhoId,GAExecuteResult__c From Event Where ( IsChild = false And RecordType.DeveloperName = 'V2_Event' And Done__c = true And V2_IsExpire__c = false And StartDateTime >: CheckStartDateTime And StartDateTime <: CheckEndDateTime And OwnerId =: UserId) Or Id in: set_AssVisitId];
        //拜访总量
        Double VistQty =0;
        //院长&药剂科拜访量
        Double DirectorVisitQty = 0 ;
        List<ID> list_whoId = new List<ID>();
        for(Event objEvent : list_ValidVisit){
            list_whoId.add(objEvent.WhoId);
        }
        Set<ID> set_WhoId = new Set<ID>();
        //2013-4-1 去掉联系人为renal记录类型的限制。
        List<String> list_Department = new List<String>{'药剂科','行政','院办','采购中心','医务科','医院医保办','市医保处(政府部门)','财务科','招标办','设备科'};
        for(Contact objContact : [Select Id,RecordTypeId,RecordType.DeveloperName From Contact Where Id in: list_whoId  And V2_RenalDepartmentType__c in: list_Department]){
            set_WhoId.add(objContact.Id);
        }
        for(Event objEvent : list_ValidVisit){
            if(set_AssVisitId.contains(objEvent.Id)){
                if(set_WhoId.contains(objEvent.WhoId)){
                    AssValidVisitQty++;
                }
            }else{
                if(set_WhoId.contains(objEvent.WhoId)){
                    DirectorVisitQty++;
                }
                VistQty++;
            }
            
        }
        map_Result.put('Target' , AssVisitQty+VistQty);
        map_Result.put('Finish' , DirectorVisitQty+AssValidVisitQty);
        
        if((AssVisitQty == null || AssVisitQty==0) && (VistQty==null || VistQty==0)){
            map_Result.put('Rate' , 0);
        }else{
            map_Result.put('Rate' , (DirectorVisitQty+AssValidVisitQty)/(AssVisitQty+VistQty));
        }
        return map_Result;
    }
    
    
    //KPI:协访数量（且完成评语）
    //当月主管协访的已完成的，且填写评语的拜访事件数量
    public Map<String , Double> AssVisitQuantity(){
        Map<String , Double> map_Result = new Map<String , Double>();
        Set<ID> list_AssVisitIds = getAssVisit();
        map_Result.put('Target' , 8);
        map_Result.put('Finish' , list_AssVisitIds.size());
        return  map_Result;
    }
    //获取有效的协访，PD部门要求接受了协访(接受协访包含点击过接受，或者评语中“是否接受协访”打钩)，并且填写过评语才算是有效的协访。
    private Set<ID> getAssVisit(){
        //拜访的起止时间
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        //有效协访ID
        Set<ID> Set_AssVisitId = new Set<ID>();
        //接受邀请的协访ID
        Set<ID> set_EventId = new Set<ID>();
        //修改为使用EventRelation而不是EventAttendee
        for(EventRelation er : [Select EventId,Status From EventRelation Where IsInvitee = true And Event.RecordType.DeveloperName = 'V2_Event' And Event.Done__c = true And RelationId =: UserId And Event.StartDateTime >: CheckStartDateTime And Event.StartDateTime <: CheckEndDateTime]){
            if(er.Status=='已接受' || er.Status=='Accepted'){
                set_EventId.add(er.EventId) ;
            }
        }
        //考察月份及下个月进行的评价
        Date startDate = date.valueOf(IntYear+'-'+IntMonth+'-1');
        Date endDate = startDate.addMonths(2);
        
        Set<ID> set_UEventId = new Set<ID>();
        for(AssVisitComments__c AssVisitComm : [Select Id,EventId__c,IsAssVisit__c From AssVisitComments__c Where CreatedDate >=: startDate And CreatedDate <: endDate And ReUser__c =: UserId]){
            //Set_AssVisitId.add(AssVisitComm.EventId__c);
            //如果填写了点评，且接受了协访，是为有效协访
            if(set_EventId.contains(AssVisitComm.EventId__c) ){
                Set_AssVisitId.add(AssVisitComm.EventId__c);
            }else if(AssVisitComm.IsAssVisit__c){
                //勾选了 “是否接受协访” 按钮的点评,需要进一步判断协访的日期。
                set_UEventId.add(AssVisitComm.EventId__c);
            }
        }
        
        for(Event e: [Select Id,GAPlan__c,WhoId,GAExecuteResult__c,Key_Information__c From Event Where Id in: set_UEventId And RecordType.DeveloperName = 'V2_Event' And Done__c = true And StartDateTime >: CheckStartDateTime And StartDateTime <: CheckEndDateTime]){
            Set_AssVisitId.add(e.Id);
        }
        return Set_AssVisitId;
    }
    
    //KPI:协访质量(二级评分)
    //
    public Map<String , Double> AssVisitLevelTwoQuantity(){
        //sunny:找到查找月份及下一个月创建的所有被点评的协访点评（二级）。然后看这些点评的拜访有哪些是属于查找月份的。
        Map<String , Double> map_Result = new Map<String , Double>();
        Double Grade ;
        Integer num =0;
        Date startDate = date.valueOf(IntYear+'-'+IntMonth+'-1');
        Date endDate = startDate.addMonths(2);
        
        //拜访的起止时间
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        Set<ID> set_UEventId = new Set<ID>();
        Map<ID,AssVisitComments__c> map_AssVisit = new Map<ID,AssVisitComments__c>();
        for(AssVisitComments__c AssVisitComm : [Select Id,EventId__c,Grade__c From AssVisitComments__c Where CreatedDate >=: startDate And CreatedDate <: endDate And BeReviewed__c =: UserId And Grade__c != null]){
            //Set_AssVisitId.add(AssVisitComm.EventId__c);
            set_UEventId.add(AssVisitComm.EventId__c);
            map_AssVisit.put(AssVisitComm.EventId__c , AssVisitComm);
        }
        for(Event e: [Select Id,GAPlan__c,WhoId,GAExecuteResult__c,Key_Information__c From Event Where Id in: set_UEventId And RecordType.DeveloperName = 'V2_Event' And Done__c = true And StartDateTime >: CheckStartDateTime And StartDateTime <: CheckEndDateTime]){
            //set_EventId.add(e.Id);
            if(map_AssVisit.containsKey(e.Id)){
                AssVisitComments__c AssVisitComm = map_AssVisit.get(e.Id) ;
                num++;
                Grade = (Grade==null?0:Grade) + Integer.valueOf(AssVisitComm.Grade__c) ;
            }
        }
        /*
        Map<String , Double> map_Result = new Map<String , Double>();
        //sunny:找到被邀请已完成，已经接受的拜访，然后看该拜访下二级主管有给该一级主管评分的二级评分（可以过期）
        //拜访的起止时间
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        Set<ID> set_EventId = new Set<ID>();
        for(EventAttendee EventAtt : [Select e.Status, e.Event.Done__c, e.Event.OwnerId, e.Event.StartDateTime, e.EventId, e.AttendeeId From EventAttendee e Where Event.RecordType.DeveloperName = 'V2_Event' And Event.Done__c = true And AttendeeId =: UserId And Event.StartDateTime >: CheckStartDateTime And Event.StartDateTime <: CheckEndDateTime ]){
            if(EventAtt.Status=='已接受'){
                set_EventId.add(EventAtt.EventId) ;
            }
        }
        Double Grade = 0 ;
        Double Num = 0 ;
       for(AssVisitComments__c AssVisitComm : [Select Id,EventId__c,Grade__c From AssVisitComments__c Where EventId__c in: set_EventId And BeReviewed__c =: UserId And Grade__c != null] ){
            Grade = Grade + Integer.valueOf(AssVisitComm.Grade__c);
            Num++;
       }
       */
        map_Result.put('TotalScore' , Grade);
        map_Result.put('TotalNum' , Num);
        map_Result.put('AVGScore' , (Num==0?0:Grade/Num));
        return map_Result; 
    }
    
    //KPI:业务机会策略评分及评语完成率
    //主管对代表当月进行中的业务机会评分并评语的比例；
    //已经被评分评语的代表当月进行中的业务机会数/代表当月进行中的业务机会总数；
    //2013-3-29 sunny 修改：超简单逻辑
    public Map<String , Double> OppEvaluationRate(){
        Map<String , Double> map_Result = new Map<String , Double>();
        Date CheckDate = date.valueOf(IntYear+'-'+IntMonth+'-1');
        Date CheckEndDate = CheckDate.addMonths(1);
        Set<ID> set_OppId = new Set<ID>();
        //获取直接下属ID
        Set<ID> list_uid = new Set<ID>();
        for(V2_RoleHistory__c RoleHistory : [Select v.Role__c, v.Manager__c, v.Renal_valid_super__c, v.Name__c, v.IsOnHoliday__c, v.IsLeave__c, v.Department__c, v.PG__c, v.Level__c
            From V2_RoleHistory__c v 
            Where Month__c =: String.valueOf(intMonth) And Year__c =: String.valueOf(intYear) And IsOnHoliday__c = false And IsLeave__c = false And Manager__c=:UserId]){
            list_uId.add(RoleHistory.Name__c);
        }
        //下属对应月份负责的业务机会
        //Set<ID> set_OppIds = V2_UtilClass.GetOpportunity(IntYear,IntMonth,list_uid);
        //进行中的业务机会阶段
        List<String> list_OppStage = new List<String>{'发现/验证机会','建立沟通渠道','需求分析','提交合作方案/谈判','跟进合作方案'};
        for(Opportunity opp : [Select Id,Renal_Opp_MGT__c From Opportunity 
                Where OwnerId in: list_uId 
                And StageName in: list_OppStage 
                And RecordType.DeveloperName = 'RENAL'  
                And CloseDate >=: CheckDate
                And CreatedDate <: CheckEndDate]){
            if(opp.Renal_Opp_MGT__c == '接受' ||  opp.Renal_Opp_MGT__c == null){
                set_OppId.add(opp.Id);
            }
        }
        //查找这些业务机会中我填写了点评的。
        Set<ID> set_eids = new Set<ID>();
        for(OppEvaluation__c oppEva: [Select Id,Score__c,Opportunity__c From OppEvaluation__c Where Opportunity__c in: set_OppId And Score__c!= null And Commentator__c =: UserId And Year__c =: String.valueOf(IntYear) And Month__c =: String.valueOf(IntMonth)]){
            set_eids.add(oppEva.Opportunity__c);
        }
        system.debug('opp shuliang:'+set_eids.size());
        map_Result.put('Target' , set_OppId.size());
        map_Result.put('Finish' , set_eids.size());
        
        //sunny 添加逻辑：如果以点评业务机大于20个，则按100%算
        //2013-4-2sunny: 
        //如果需要点评的业务机会数超过20个，按20个计算
        if(set_OppId.size() != 0 ){
            system.debug(set_eids.size()+'~~'+set_OppId.size()+'.....'+set_eids.size() / set_OppId.size());
            if(set_OppId.size() > 20){
            	map_Result.put('Rate' ,Double.valueOf(set_eids.size()) / 20 );
            }else{
            	map_Result.put('Rate' ,Double.valueOf(set_eids.size()) / set_OppId.size() );
            }
             
        }else{
            map_Result.put('Rate' , 0);
        }
        system.debug(map_Result);
        return map_Result;
    }
    
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        //user role
        List<UserRole> listur= new List<UserRole>();
        UserRole ur1 = new UserRole();
        ur1.Name = 'IVT-Supervisor-华南-PD-Supervisor(U1)';
        listur.add(ur1);
        UserRole ur2 = new UserRole();
        ur2.Name = 'Renal-Rep-华南-PD-Rep(U2)';
        listur.add(ur2);
        insert listur;
        //user 
        list<User> user=[Select TimeZoneSidKey,ProfileId,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname, Alias From User Where IsActive = true and id =:userInfo.getUserId()];
        List<User> list_user = new List<User>();
        User user1=new User();
        user1.username='user1r2@123.com';
        user1.LastName='user1r2';
        user1.Email='user1r2@123.com';
        user1.Alias=user[0].Alias;
        user1.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user1.ProfileId=UserInfo.getProfileId();
        user1.LocaleSidKey=user[0].LocaleSidKey;
        user1.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user1.EmailEncodingKey=user[0].EmailEncodingKey;
        user1.CommunityNickname='chequ';
        user1.MobilePhone='12345678912';
        user1.IsActive = true;
        user1.UserRoleId=ur1.Id ;
        list_user.add(user1);
        User user2=new User();
        user2.username='user2r2@223.com';
        user2.LastName='user2r2';
        user2.Email='user2r2@223.com';
        user2.Alias=user[0].Alias;
        user2.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user2.ProfileId=UserInfo.getProfileId();
        user2.LocaleSidKey=user[0].LocaleSidKey;
        user2.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user2.EmailEncodingKey=user[0].EmailEncodingKey;
        user2.CommunityNickname='chequ1';
        user2.MobilePhone='22345678922';
        user2.IsActive = true;
        user1.UserRoleId=ur1.Id ;
        list_user.add(user2);
        insert list_user;
        
        //联系人
        List<Contact> listct= new List<Contact>();
        Contact ct1 = new Contact();
        ct1.FirstName = 'jim';
        ct1.LastName = 'Green';
        ct1.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='RecordType' And SobjectType = 'Contact'][0].Id;
        listct.add(ct1);
        Contact ct2 = new Contact();
        ct2.FirstName = 'Tom';
        ct2.LastName = 'Frank';
        ct2.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='RecordType' And SobjectType = 'Contact'][0].Id;
        listct.add(ct2);
        insert listct;
        
        //业务机会
        List<Opportunity> List_Opportunity = new List<Opportunity>();
        Opportunity opp = new Opportunity();
        opp.OwnerId = user1.Id;
        opp.Name = 'yewujihui';
        opp.StageName = '建立沟通渠道';
        opp.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='RENAL' And SobjectType = 'Opportunity'][0].Id;
        opp.CloseDate = date.valueOf(Datetime.now().year()+'-'+Datetime.now().month()+'-1');
        opp.Renal_Opp_MGT__c = '接受';
        List_Opportunity.add(opp);
        Opportunity opp1 = new Opportunity();
        opp1.OwnerId = user1.Id;
        opp1.Name = 'yewujihui1';
        opp1.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='RENAL' And SobjectType = 'Opportunity'][0].Id;
        opp1.StageName = '建立沟通渠道';
        opp1.CloseDate = date.valueOf(Datetime.now().year()+'-'+Datetime.now().month()+'-1');
        opp1.Renal_Opp_MGT__c = '接受';
        List_Opportunity.add(opp1);
        Opportunity opp2 = new Opportunity();
        opp2.OwnerId = user1.Id;
        opp2.Name = 'yewujihui2';
        opp2.StageName = '发现需求';
        opp2.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='IVT' And SobjectType = 'Opportunity'].Id;
        opp2.CloseDate = date.valueOf(Datetime.now().year()+'-'+Datetime.now().month()+'-1');
        opp2.Renal_Opp_MGT__c = '接受';
        List_Opportunity.add(opp2);
        insert List_Opportunity;
        
        //业务机会历史记录
        List<OpportunityHistory__c> List_OpportunityHistory = new List<OpportunityHistory__c>();
        OpportunityHistory__c oppc = new OpportunityHistory__c();
        oppc.ChangedDate__c = date.valueOf(Datetime.now().year()+'-'+Datetime.now().month()+'-2');
        oppc.NewOppStage__c ='需求分析';
        oppc.PastOppStage__c = '提交合作方案/谈判';
        oppc.Opportunity__c = opp.Id;
        List_OpportunityHistory.add(oppc);
        insert List_OpportunityHistory;
        
        //业务机会策略评估
        List<OppEvaluation__c> List_OppEvaluation = new list<OppEvaluation__c>();
        OppEvaluation__c oppe = new OppEvaluation__c();
        oppe.BeCommentUser__c = user1.Id;
        oppe.Opportunity__c = opp2.Id;
        oppe.Score__c = '4';
        oppe.Year__c = string.valueOf(Datetime.now().year());
        oppe.Month__c = string.valueOf(Datetime.now().month());
        List_OppEvaluation.add(oppe);
        OppEvaluation__c oppe1 = new OppEvaluation__c();
        oppe1.Opportunity__c = opp2.Id;
        oppe1.BeCommentUser__c = user1.Id;
        oppe1.Score__c = '3';
        oppe1.Year__c = string.valueOf(Datetime.now().year());
        oppe1.Month__c = string.valueOf(Datetime.now().month());
        List_OppEvaluation.add(oppe1);
        OppEvaluation__c oppe2 = new OppEvaluation__c();
        oppe2.Opportunity__c = opp2.Id;
        oppe2.BeCommentUser__c = user1.Id;
        oppe2.Score__c = '5';
        oppe2.Year__c = string.valueOf(Datetime.now().year());
        oppe2.Month__c = string.valueOf(Datetime.now().month());
        List_OppEvaluation.add(oppe2);
        insert List_OppEvaluation;
        
        //活动
        List<Event> list_Visit = new List<Event>();
        Event ev1 = new Event();
        ev1.DurationInMinutes = 1;
        ev1.StartDateTime = datetime.now();
        ev1.OwnerId = user1.Id ;
        ev1.GAPlan__c = '1111111';
        ev1.GAExecuteResult__c = '111111';
        ev1.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='V2_Event' And SobjectType = 'Event'][0].Id;
        ev1.Done__c = true;
        ev1.V2_IsExpire__c = false; 
        ev1.WhoId = ct1.Id;
        ev1.WhatId = opp.Id;
        list_Visit.add(ev1);
        Event ev2 = new Event();
        ev2.DurationInMinutes = 2;
        ev2.StartDateTime = datetime.now();
        ev2.OwnerId = user1.Id ;
        ev2.GAPlan__c = '222222';
        ev2.GAExecuteResult__c = '222222';
        ev2.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='V2_Event' And SobjectType = 'Event'][0].Id;
        ev2.Done__c = true;
        ev2.V2_IsExpire__c = false; 
        ev2.WhoId = ct1.Id;
        ev2.WhatId = opp.Id;
        list_Visit.add(ev2);
        Event ev3 = new Event();
        ev3.DurationInMinutes = 3;
        ev3.StartDateTime = datetime.now();
        ev3.OwnerId = user1.Id ;
        ev3.GAPlan__c = '333333';
        ev3.GAExecuteResult__c = '3333333';
        ev3.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='V2_Event' And SobjectType = 'Event'][0].Id;
        ev3.Done__c = true;
        ev3.V2_IsExpire__c = false; 
        ev3.WhoId = ct1.Id;
        ev3.WhatId = opp.Id;
        list_Visit.add(ev3);
        Event ev4 = new Event();
        ev4.DurationInMinutes = 4;
        ev4.StartDateTime = datetime.now();
        ev4.OwnerId = user1.Id ;
        ev4.GAPlan__c = '333333';
        //ev4.GAExecuteResult__c = '3333333';
        ev4.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='V2_RecordType' And SobjectType = 'Event'][0].Id;
        ev4.Done__c = true;
        ev4.V2_IsExpire__c = false; 
        ev4.WhoId = ct2.Id;
        ev4.WhatId = opp.Id;
        ev4.SubjectType__c = '科室会';
        list_Visit.add(ev4);
        insert list_Visit ;
        
        //协防事件
        //List<EventAttendee> list_EventAttendee = new List<EventAttendee>();
        //EventAttendee eva = new EventAttendee();
        //eva.EventId = ev1.Id;
        //eva.AttendeeId = user1.Id;
        //eva.Status = '已接受';
        //list_EventAttendee.add(eva);
        //insert list_EventAttendee;
        
        //月计划
        List<MonthlyPlan__c> List_MonthlyPlan = new List<MonthlyPlan__c>();
        MonthlyPlan__c mc1 = new MonthlyPlan__c();
        mc1.OwnerId = user1.Id;
        mc1.Year__c = string.valueOf(datetime.now().year());
        mc1.Month__c = string.valueOf(datetime.now().month());
        //mc1.V2_TotalCallRecords__c = 10;
        List_MonthlyPlan.add(mc1);
        insert List_MonthlyPlan;
        
        //月计划明细
        List<MonthlyPlanDetail__c> List_MonthlyPlanDetail = new List<MonthlyPlanDetail__c>();
        MonthlyPlanDetail__c mcc1 = new MonthlyPlanDetail__c();
        mcc1.Contact__c = ct1.Id;
        mcc1.Planned_Finished_Calls__c = 5;
        mcc1.MonthlyPlan__c = mc1.id;
        mcc1.AdjustedTimes__c = 10;
        List_MonthlyPlanDetail.add(mcc1);
        MonthlyPlanDetail__c mcc2 = new MonthlyPlanDetail__c();
        mcc2.Contact__c = ct1.Id;
        mcc2.Planned_Finished_Calls__c = 5;
        mcc2.MonthlyPlan__c = mc1.Id;
        mcc2.AdjustedTimes__c = 10;
        List_MonthlyPlanDetail.add(mcc2);
        MonthlyPlanDetail__c mcc3 = new MonthlyPlanDetail__c();
        mcc3.Contact__c = ct1.Id;
        mcc3.Planned_Finished_Calls__c = 10;
        mcc3.MonthlyPlan__c = mc1.Id;
        mcc3.AdjustedTimes__c = 10;
        List_MonthlyPlanDetail.add(mcc3);
        insert List_MonthlyPlanDetail;
        
        //协访质量点评
        List<AssVisitComments__c> List_AssVisitComments = new List<AssVisitComments__c>();
        AssVisitComments__c ac = new AssVisitComments__c();
        ac.EventId__c = ev1.Id;
        ac.BeReviewed__c = user1.Id;
        ac.Grade__c = '90';
        List_AssVisitComments.add(ac);
        AssVisitComments__c ac1 = new AssVisitComments__c();
        ac1.EventId__c = ev2.Id;
        ac1.BeReviewed__c = user1.Id;
        ac1.Grade__c = '90';
        List_AssVisitComments.add(ac1);
        AssVisitComments__c ac2 = new AssVisitComments__c();
        ac2.EventId__c = ev3.Id;
        ac2.BeReviewed__c = user1.Id;
        ac2.Grade__c = '90';
        List_AssVisitComments.add(ac2);
        AssVisitComments__c ac3 = new AssVisitComments__c();
        ac3.EventId__c = ev4.Id;
        ac3.BeReviewed__c = user1.Id;
        ac3.Grade__c = '90';
        List_AssVisitComments.add(ac3);
        insert List_AssVisitComments;
        
        system.test.startTest();
        ClsPdKpiService cls = new ClsPdKpiService(user1.Id , datetime.now().year() , datetime.now().month());
        //销售代表KPI
        Map<String , Double> i = cls.DirectorVisitRate();
        Map<String , Double> a = cls.DirectorVisitRateIncludeAss();
        Map<String , Double> b = cls.OpportunityQuantity();
        Map<String , Double> c = cls.OpportunityVisitRate();
        Map<String , Double> d = cls.VisitRate();
        Map<String , Double> j = cls.VisitGrade();
        Map<String , Double> e = cls.DepartmentVisitQuantity();
        Map<String , Double> f = cls.VisitPlanRate();
        //主管 KPI
        Map<String , Double> g = cls.DirectorVisitRateIncludeAss();
        Map<String , Double> k = cls.AssVisitLevelTwoQuantity();
        Map<String , Double> h = cls.OppEvaluationRate();
        Map<String , Double> l = cls.SepTeamPerformanceAVG(true,0);
        system.debug(i+'@@'+a+'@@'+b+'@@'+c+'@@'+d+'@@'+e+'@@'+f+'@@'+g+'@@'+h+'@@'+j);
        system.test.stopTest();
     }
    
    
}