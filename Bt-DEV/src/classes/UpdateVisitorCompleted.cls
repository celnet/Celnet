/**
 * 作者:Bill
 * 说明：当主管点评完拜访后，选择是否协防
 * 1.若该主管不是被邀请协防人员，则把该主管直接加到已接受事件中
 * 2.若该主管是被邀请协防人员，但是没有回复接受事件，则修改其为变为接受事件
**/
public class UpdateVisitorCompleted {

   //构造方法
   public UpdateVisitorCompleted(){}
   
   //判断点评人是否存在被邀请协防
   //若存在判断该点评人是否接受协防
   public void IsReUserExist(ID eventId, ID userId)
   {
   	  List<EventRelation> list_eRelation = [Select e.Status From EventRelation e  where eventId = :eventId and RelationId = :userId and IsInvitee = true];
   	  //判断点评人是否存在被邀请协防
   	  if(list_eRelation != null && list_eRelation.size() > 0)
   	  {
   	  	//判断该点评人是否接受协防
   	  	if(list_eRelation[0].Status == 'new')
   	  	{
   	  		list_eRelation[0].Status = 'Accepted';
   	  		system.debug('***********************************'+userId);
   	  		update list_eRelation[0];
   	  	}
   	  }else{
   	  	    EventRelation eRelation = new EventRelation();
   	  	    eRelation.EventId = eventId;
   	  	    eRelation.RelationId = userId;
   	  	    eRelation.IsInvitee = true;
   	  	    eRelation.IsParent = false ;
   	  	    eRelation.IsWhat = false ;
   	  	    eRelation.Status = 'Accepted';
   	  	    system.debug(eventId+'***********************************'+userId);
   	  	    insert eRelation;
   	  }
   }
   
   static testMethod void myUnitTest() {
   	    //user rolev 
        List<UserRole> listur= new List<UserRole>();
        UserRole ur0 = new UserRole();
        ur0.Name = 'Renal-Supervisor-华南-PD-Supervisor(U0)';
        listur.add(ur0);
        UserRole ur1 = new UserRole();
        ur1.Name = 'Renal-Supervisor-华南-PD-Supervisor(U1)';
        listur.add(ur1);
        UserRole ur2 = new UserRole();
        ur2.Name = 'Renal-Rep-华南-PD-Rep(U2)';
        listur.add(ur2);
        insert listur;
        ur2.ParentRoleId=ur1.Id;
        update ur2;
        ur1.ParentRoleId = ur0.id;
        update ur1;
        //user 
        list<User> user=[Select TimeZoneSidKey,ProfileId,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname, Alias From User Where IsActive = true limit 1];
        List<User> list_user = new List<User>();
        User user0=new User();
        user0.username='user0r2@023.com';
        user0.LastName='user0r2';
        user0.Email='user0r2@023.com';
        user0.Alias=user[0].Alias;
        user0.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user0.ProfileId=UserInfo.getProfileId();
        user0.LocaleSidKey=user[0].LocaleSidKey;
        user0.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user0.EmailEncodingKey=user[0].EmailEncodingKey;
        user0.CommunityNickname='chequ23';
        user0.MobilePhone='02345678902';
        user0.IsActive = true;
        user0.UserRoleId=ur0.Id ;
        list_user.add(user0);
        User user1=new User();
        user1.username='user1r2@123.com';
        user1.LastName='user1r2';
        user1.Email='user1r2@123.com';
        user1.Alias=user[0].Alias;
        user1.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user1.ProfileId=UserInfo.getProfileId();
        user1.LocaleSidKey=user[0].LocaleSidKey;
        user1.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user1.EmailEncodingKey=user[0].EmailEncodingKey;
        user1.CommunityNickname='chequ';
        user1.MobilePhone='12345678912';
        user1.IsActive = true;
        user1.UserRoleId=ur1.Id ;
        list_user.add(user1);
        User user2=new User();
        user2.username='user2r2@223.com';
        user2.LastName='user2r2';
        user2.Email='user2r2@223.com';
        user2.Alias=user[0].Alias;
        user2.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user2.ProfileId=UserInfo.getProfileId();
        user2.LocaleSidKey=user[0].LocaleSidKey;
        user2.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user2.EmailEncodingKey=user[0].EmailEncodingKey;
        user2.CommunityNickname='chequ1';
        user2.MobilePhone='22345678922';
        user2.IsActive = true;
        user2.UserRoleId=ur2.Id ;
        list_user.add(user2);
        insert list_user;
        //event
        RecordType rt =[Select Id From RecordType Where DeveloperName = 'V2_Event' And SobjectType = 'Event'];
        Event objEvent = new Event();
        objEvent.OwnerId = user2.Id;
        objEvent.StartDateTime = datetime.now();
        objEvent.EndDateTime = datetime.now().addHours(1);
        objEvent.RecordTypeId = rt.Id;
        objEvent.Subject = '拜访';
        insert objEvent ;
        
        //事件
        List<Event> list_event = new List<Event>();
        Event event = new Event();
        event.OwnerId = user2.Id;
        event.SubjectType__c = '拜访';
        event.DurationInMinutes = 200;
        event.ActivityDate = Date.today();
        event.ActivityDateTime = dateTime.now();
        list_event.add(event);
        Event event1 = new Event();
        event1.OwnerId = user1.Id;
        event1.SubjectType__c = '拜访';
        event1.DurationInMinutes = 200;
        event1.ActivityDate = Date.today();
        event1.ActivityDateTime = dateTime.now();
        list_event.add(event1);
        insert list_event;
        
        AssVisitComments__c avc = new AssVisitComments__c();
        avc.EventId__c = objEvent.Id;
        avc.BeReviewed__c = user2.Id;
        avc.ReUser__c = user1.Id;
        avc.Comment__c = 'sss';
        avc.Grade__c='4';
        insert avc ;
        
        List<EventRelation> list_eRelation = new List<EventRelation>();
        EventRelation eRelation = new EventRelation();
        eRelation.EventId = event.Id;
        eRelation.RelationId = user1.Id;
        eRelation.IsInvitee = true;
        eRelation.Status = 'new';
        list_eRelation.add(eRelation);
        insert list_eRelation;
        
        system.Test.startTest();
        UpdateVisitorCompleted visit = new UpdateVisitorCompleted();
        visit.IsReUserExist(event.Id, user1.Id);
        visit.IsReUserExist(event1.Id, user2.Id);
        system.Test.stopTest();
   }
}