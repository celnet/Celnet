/*
*作者：Hank
*时间：2013-10-18
*功能：PM_BasicTableBatch的测试类
*/
@isTest
private class PM_Test_BasicTableBatch 
{

    static testMethod void myUnitTest() 
    {
        V2_Account_Team__c vat = new V2_Account_Team__c();
        vat.UserProduct__c = 'PD';
        vat.V2_History__c = false;
        vat.V2_ApprovalStatus__c = '审批通过';
        vat.V2_Effective_Year__c = String.valueOf(Date.today().year());
        vat.V2_Effective_Month__c = String.valueOf(Date.today().month());
        //vat.V2_LastAccessDate__c = date.newinstance(2013, 10, 17);
        insert vat;
        //新建省市
        Region__c zone = new Region__c();
        zone.Name = '南区';
        insert zone;
        Provinces__c pro = new Provinces__c();
        pro.Name = '浙江省';
        insert pro;
        Cities__c city = new Cities__c();
        city.Name = '杭州市';
        city.BelongToProvince__c = pro.Id;
        insert city;
        list<RecordType> record = [Select Id From RecordType Where Name = '医院'];
        list<RecordType> record2 = [Select Id From RecordType Where Name = '经销商'];
        Account acc = new  Account();
        acc.Name = '浙江医院';
        acc.RecordTypeId = record[0].Id;
        acc.Provinces__c = pro.Id;
        acc.Cities__c = city.Id;
        acc.Region__c = zone.Id;
        insert acc;
        Account acc2 = new  Account();
        acc2.Name = '浙江医药控股';
        acc2.RecordTypeId = record2[0].Id;
        acc2.Provinces__c = pro.Id;
        acc2.Cities__c = city.Id;
        acc2.Region__c = zone.Id;
        insert acc2;
        Account acc3 = new  Account();
        acc3.Name = '北京医院';
        acc3.RecordTypeId = record[0].Id;
        acc3.Provinces__c = pro.Id;
        acc3.Cities__c = city.Id;
        acc3.Region__c = zone.Id;
        insert acc3;
        Account acc4 = new  Account();
        acc4.Name = '北京医药控股';
        acc4.RecordTypeId = record2[0].Id;
        acc4.Provinces__c = pro.Id;
        acc4.Cities__c = city.Id;
        acc4.Region__c = zone.Id;
        insert acc4;
        PM_PatientPlan__c pap = new PM_PatientPlan__c();
        pap.PM_Year__c = Date.today();
        pap.PM_Hospital__c = acc.Id;
        insert pap;
        PM_Patient__c pat =new PM_Patient__c();
        pat.Name = 'aaa';
        pat.PM_InHospital__c = acc.Id;
        pat.PM_Distributor__c = acc2.Id;
        pat.PM_Status__c = 'New';
        pat.PM_DropoutCreated_Date__c = Date.today();
        pat.PM_PreviousStatus__c = 'New';
        pat.PM_NewPatientDate__c = Date.today();
        insert pat;
        PM_Patient__c pat2 =new PM_Patient__c();
        pat2.Name = 'bbb';
        pat2.PM_InHospital__c = acc3.Id;
        pat2.PM_Distributor__c = acc4.Id;
        pat2.PM_Status__c = 'Dropout';
        pat2.PM_PreviousStatus__c = 'New';
        pat2.PM_NewPatientDate__c = Date.today();
        insert pat2;
        PM_Patient__c pat3 =new PM_Patient__c();
        pat3.Name = 'bbb';
        pat3.PM_InHospital__c = acc.Id;
        pat3.PM_DropOut_One_Reason__c = '转用竞争产品';
        pat3.PM_UnreachableDate__c = Date.Today();
        pat3.PM_TransfornCreateDate__c = Date.Today();
        pat3.PM_Distributor__c = acc2.Id;
        pat3.PM_Status__c = 'New';
        pat3.PM_PreviousStatus__c = 'Dropout';
        pat3.PM_NewPatientDate__c = Date.today();
        insert pat3;
        //V2_Account__r
        PM_BasicTableBatch btBatch = new PM_BasicTableBatch();
        database.executeBatch(btBatch,5);
    }
}