/**
 * Author:Bill
 * date: 2013-9-10
 * 功能：当SP客户联系人修改申请提交后要发送邮件，邮件里要通知哪些值变化了
 **/
public without sharing class SP_ContactChangeController {
	public List<ReportRow> list_Report{get;set;}
	//联系人修改申请ID
	public ID ContactModId{get;set
		{
			ContactModId = value;
			generateTableDynamic();
		}}
	//old值在新增时不显示
	public Boolean IsViewOld{get;set;}
	
	public string RecordUrl{get;set;}
	
	public SP_ContactChangeController(){
        list_Report = new List<ReportRow>();
    }
    //提交时间
    public String AppTime{get;set;}
    
    //页面组件绑定数据
    private void generateTableDynamic()
    {		 	
    	List<Contact_Mod__c> list_conMod = [Select c.V2_interest__c, c.Type__c,c.Status__c,   
	    c.V2_PassPortNo__c, c.V2_OfficerNo__c, c.V2_Level__c, c.V2_Education__c,c.V2_ApprovalManager__c,   
	    c.SP_SevoFGF__c, c.SP_SevoConcentration__c, c.SP_OperationTime__c, c.SP_OldV2interest__c,  
	    c.SP_OldV2PassPortNo__c, c.SP_OldV2OfficerNo__c, c.SP_OldSevoFGF__c, c.SP_OldSevoConcentration__c,  
	    c.SP_OldPhone__c, c.SP_OldOperationTime__c, c.SP_OldMobile__c, c.SP_OldJob__c, c.SP_OldInhaleMonthyNum__c,  
	    c.SP_OldIdcard2__c, c.SP_OldGraduateCollege__c, c.SP_OldGender__c, c.SP_OldFax__c,  
	    c.SP_OldEmail__c, c.SP_OldDepartment__c, c.SP_OldContactType__c, c.SP_OldComment__c, c.SP_OldBirthday__c,  
	    c.SP_OldAnesthesiaMonthyNum__c, c.SP_Job__c, c.SP_InhaleMonthyNum__c, c.SP_Department__c, c.SP_AnesthesiaMonthyNum__c,  
	    c.RenalMarketContactApp__c, c.RecordTypeId, c.Phone__c, c.OwnerId, c.NewContact__c, c.Name__c, c.Name__r.LastName, c.Name,  
	    c.Mobile__c, c.LastModifiedDate, c.LastModifiedById, c.IsDeleted, c.Id, c.ID_card2__c, c.ID__c,  
	    c.Graduate_College__c, c.Gender__c, c.GAState__c, c.Fax__c, c.Email__c,  
	    c.ElectrostaticTherapy__c, c.Department_Type__c, c.CreatedDate, c.CreatedById, c.Contact_Type__c,  
	    c.Comment__c, c.Birthday__c, c.Account__c, c.Account__r.Name, c.Owner.Alias, c.SP_DesFGF__c, c.SP_OldDesFGF__c,
	    SP_DesConcentration__c, SP_OldDesConcentration__c From Contact_Mod__c c where Id = : ContactModId];
	    
	    string Url = string.valueOf(System.URL.getSalesforceBaseUrl());
	 	Url = Url.substring(Url.indexOf('=')+1,Url.length()-1);
	 	RecordUrl = Url+'/'+ContactModId;
	    
    	if(list_conMod != null && list_conMod.size()>0)
    	{
    		Contact_Mod__c conMod = list_conMod[0];
    		if(conMod.Type__c == '新增')
    		{
    			IsViewOld = false;
    		}else if(conMod.Type__c == '编辑'){
			    IsViewOld = true; 
    		}
    		AppTime = conMod.LastModifiedDate.format('yyyy.MMMMM.dd GGG hh:mm aaa');
    		
    		if(conMod.NewContact__c != conMod.Name__r.LastName)
	    	{
	    		ReportRow row = new ReportRow();
	    		row.FieldName = '姓名';
	    		row.NowValue = conMod.NewContact__c==null?'无':conMod.NewContact__c;
	    		row.OldValue = conMod.Name__r.LastName==null?'无':conMod.Name__r.LastName;
	    		list_Report.add(row);
	    	}
	    	if(conMod.Gender__c != conMod.SP_OldGender__c)
	    	{
	    		ReportRow row = new ReportRow();
	    		row.FieldName = '性别';
	    		row.NowValue = conMod.Gender__c==null?'无':conMod.Gender__c;
	    		row.OldValue = conMod.SP_OldGender__c==null?'无':conMod.SP_OldGender__c;
	    		
	    		list_Report.add(row);
	    	}
	    	if(conMod.Birthday__c != conMod.SP_OldBirthday__c)
	    	{
	    		ReportRow row = new ReportRow();
	    		row.FieldName = '生日';
	    		row.NowValue = conMod.Birthday__c==null?'无':string.valueOf(conMod.Birthday__c);
	    		row.OldValue = conMod.SP_OldBirthday__c==null?'无':string.valueOf(conMod.SP_OldBirthday__c);
	    		
	    		list_Report.add(row);
	    	}
	    	if(conMod.Mobile__c != conMod.SP_OldMobile__c)
	    	{
	    		ReportRow row = new ReportRow();
	    		row.FieldName = '电话';
	    		row.NowValue = conMod.Mobile__c==null?'无':conMod.Mobile__c;
	    		row.OldValue = conMod.SP_OldMobile__c==null?'无':conMod.SP_OldMobile__c;
	    		
	    		list_Report.add(row);
	    	}
	    	if(conMod.Phone__c != conMod.SP_OldPhone__c)
	    	{
	    		ReportRow row = new ReportRow();
	    		row.FieldName = '手机';
	    		row.NowValue = conMod.Phone__c==null?'无':conMod.Phone__c;
	    		row.OldValue = conMod.SP_OldPhone__c==null?'无':conMod.SP_OldPhone__c;
	    		
	    		list_Report.add(row);
	    	}
	    	if(conMod.Fax__c != conMod.SP_OldFax__c)
	    	{
	    		ReportRow row = new ReportRow();
	    		row.FieldName = '传真';
	    		row.NowValue = conMod.Fax__c==null?'无':conMod.Fax__c;
	    		row.OldValue = conMod.SP_OldFax__c==null?'无':conMod.SP_OldFax__c;
	    		
	    		list_Report.add(row);
	    	}
	    	if(conMod.Email__c != conMod.SP_OldEmail__c)
	    	{
	    		ReportRow row = new ReportRow();
	    		row.FieldName = '电子邮件';
	    		row.NowValue = conMod.Email__c==null?'无':conMod.Email__c;
	    		row.OldValue = conMod.SP_OldEmail__c==null?'无':conMod.SP_OldEmail__c;
	    		
	    		list_Report.add(row);
	    	}
	    	if(conMod.V2_interest__c != conMod.SP_OldV2interest__c)
	    	{
	    		ReportRow row = new ReportRow();
	    		row.FieldName = '兴趣爱好';
	    		row.NowValue = conMod.V2_interest__c==null?'无':conMod.V2_interest__c;
	    		row.OldValue = conMod.SP_OldV2interest__c==null?'无':conMod.SP_OldV2interest__c;
	    		
	    		list_Report.add(row);
	    	}
	    	if(conMod.SP_Job__c != conMod.SP_OldJob__c)
	    	{
	    		ReportRow row = new ReportRow();
	    		row.FieldName = '职务(SP)';
	    		row.NowValue = conMod.SP_Job__c==null?'无':conMod.SP_Job__c;
	    		row.OldValue = conMod.SP_OldJob__c==null?'无':conMod.SP_OldJob__c;
	    		
	    		list_Report.add(row);
	    	}
	    	if(conMod.SP_Department__c != conMod.SP_OldDepartment__c)
	    	{
	    		ReportRow row = new ReportRow();
	    		row.FieldName = '部门(SP)';
	    		row.NowValue = conMod.SP_Department__c==null?'无':conMod.SP_Department__c;
	    		row.OldValue = conMod.SP_OldDepartment__c==null?'无':conMod.SP_OldDepartment__c;
	    		
	    		list_Report.add(row);
	    	}
	    	if(conMod.Contact_Type__c != conMod.SP_OldContactType__c)
	    	{
	    		ReportRow row = new ReportRow();
	    		row.FieldName = '联系人类别';
	    		row.NowValue = conMod.Contact_Type__c==null?'无':conMod.Contact_Type__c;
	    		row.OldValue = conMod.SP_OldContactType__c==null?'无':conMod.SP_OldContactType__c;
	    		
	    		list_Report.add(row);
	    	}
	    	if(conMod.ID_card2__c != conMod.SP_OldIdcard2__c)
	    	{
	    		ReportRow row = new ReportRow();
	    		row.FieldName = '身份证号';
	    		row.NowValue = conMod.ID_card2__c==null?'无':conMod.ID_card2__c;
	    		row.OldValue = conMod.SP_OldIdcard2__c==null?'无':conMod.SP_OldIdcard2__c;
	    		
	    		list_Report.add(row);
	    	}
	    	if(conMod.V2_OfficerNo__c != conMod.SP_OldV2OfficerNo__c)
	    	{
	    		ReportRow row = new ReportRow();
	    		row.FieldName = '军官号';
	    		row.NowValue = conMod.V2_OfficerNo__c==null?'无':conMod.V2_OfficerNo__c;
	    		row.OldValue = conMod.SP_OldV2OfficerNo__c==null?'无':conMod.SP_OldV2OfficerNo__c;
	    		
	    		list_Report.add(row);
	    	}
	    	if(conMod.V2_PassPortNo__c != conMod.SP_OldV2PassPortNo__c)
	    	{
	    		ReportRow row = new ReportRow();
	    		row.FieldName = '护照号';
	    		row.NowValue = conMod.V2_PassPortNo__c==null?'无':conMod.V2_PassPortNo__c;
	    		row.OldValue = conMod.SP_OldV2PassPortNo__c==null?'无':conMod.SP_OldV2PassPortNo__c;
	    		
	    		list_Report.add(row);
	    	}
	    	if(conMod.Graduate_College__c != conMod.SP_OldGraduateCollege__c)
	    	{
	    		ReportRow row = new ReportRow();
	    		row.FieldName = '毕业院校';
	    		row.NowValue = conMod.Graduate_College__c==null?'无':conMod.Graduate_College__c;
	    		row.OldValue = conMod.SP_OldGraduateCollege__c==null?'无':conMod.SP_OldGraduateCollege__c;
	    		
	    		list_Report.add(row);
	    	}
	    	if(conMod.SP_SevoFGF__c != conMod.SP_OldSevoFGF__c)
	    	{
	    		ReportRow row = new ReportRow();
	    		row.FieldName = '七氟烷FGF(L/min)';
	    		row.NowValue = conMod.SP_SevoFGF__c==null?'无':string.valueOf(conMod.SP_SevoFGF__c);
	    		row.OldValue = conMod.SP_OldSevoFGF__c==null?'无':string.valueOf(conMod.SP_OldSevoFGF__c);
	    		
	    		list_Report.add(row);
	    	}
	    	if(conMod.SP_SevoConcentration__c != conMod.SP_OldSevoConcentration__c)
	    	{
	    		ReportRow row = new ReportRow();
	    		row.FieldName = '七氟烷浓度%';
	    		row.NowValue = conMod.SP_SevoConcentration__c==null?'无':string.valueOf(conMod.SP_SevoConcentration__c);
	    		row.OldValue = conMod.SP_OldSevoConcentration__c==null?'无':string.valueOf(conMod.SP_OldSevoConcentration__c);
	    		
	    		list_Report.add(row);
	    	}
	    	if(conMod.SP_OperationTime__c != conMod.SP_OldOperationTime__c)
	    	{
	    		ReportRow row = new ReportRow();
	    		row.FieldName = '平均手术时长h';
	    		row.NowValue = conMod.SP_OperationTime__c==null?'无':string.valueOf(conMod.SP_OperationTime__c);
	    		row.OldValue = conMod.SP_OldOperationTime__c==null?'无':string.valueOf(conMod.SP_OldOperationTime__c);
	    		
	    		list_Report.add(row);
	    	}
	    	if(conMod.SP_InhaleMonthyNum__c != conMod.SP_OldInhaleMonthyNum__c)
	    	{
	    		ReportRow row = new ReportRow();
	    		row.FieldName = '月吸入比例（100%）';
	    		row.NowValue = conMod.SP_InhaleMonthyNum__c==null?'无':string.valueOf(conMod.SP_InhaleMonthyNum__c);
	    		row.OldValue = conMod.SP_OldInhaleMonthyNum__c==null?'无':string.valueOf(conMod.SP_OldInhaleMonthyNum__c);
	    		
	    		list_Report.add(row);
	    	}
	    	if(conMod.SP_AnesthesiaMonthyNum__c != conMod.SP_OldAnesthesiaMonthyNum__c)
	    	{
	    		ReportRow row = new ReportRow();
	    		row.FieldName = '月全麻数量/台';
	    		row.NowValue = conMod.SP_AnesthesiaMonthyNum__c==null?'无':string.valueOf(conMod.SP_AnesthesiaMonthyNum__c);
	    		row.OldValue = conMod.SP_OldAnesthesiaMonthyNum__c==null?'无':string.valueOf(conMod.SP_OldAnesthesiaMonthyNum__c);
	    		
	    		list_Report.add(row);
	    	}
	    	if(conMod.Comment__c != conMod.SP_OldComment__c)
	    	{
	    		ReportRow row = new ReportRow();
	    		row.FieldName = '备注';
	    		row.NowValue = conMod.Comment__c==null?'无':conMod.Comment__c;
	    		row.OldValue = conMod.SP_OldComment__c==null?'无':conMod.SP_OldComment__c;
	    		
	    		list_Report.add(row);
	    	}
	    	if(conMod.SP_DesConcentration__c != conMod.SP_OldDesConcentration__c)
	    	{
	    		ReportRow row = new ReportRow();
	    		row.FieldName = '地氟烷浓度%';
	    		row.NowValue = conMod.SP_DesConcentration__c==null?'无':string.valueOf(conMod.SP_DesConcentration__c);
	    		row.OldValue = conMod.SP_OldDesConcentration__c==null?'无':string.valueOf(conMod.SP_OldDesConcentration__c);
	    		list_Report.add(row);
	    	}
	    	if(conMod.SP_DesFGF__c != conMod.SP_OldDesFGF__c)
	    	{
	    		ReportRow row = new ReportRow();
	    		row.FieldName = '地氟烷FGF(L/min)';
	    		row.NowValue = conMod.SP_DesFGF__c==null?'无':string.valueOf(conMod.SP_DesFGF__c);
	    		row.OldValue = conMod.SP_OldDesFGF__c==null?'无':string.valueOf(conMod.SP_OldDesFGF__c);
	    		
	    		list_Report.add(row);
	    	}
    	}
    }
    
    //封装报表展示行
    public class ReportRow{
        public String FieldName{get;set;}
        public String NowValue{get;set;}
		public String OldValue{get;set;}
		public String UpdateTime{get;set;}
    }
/**
 * Author:Dean Lv
 * date: 2013-9-11
 * 功能：测试SP_ContactChangeController
 * 当SP客户联系人修改申请提交后要发送邮件，邮件里要通知哪些值变化了
 **/
    static testMethod void myUnitTest() {
    	List<ReportRow> list_Report;
		list_Report = new List<ReportRow>();
		Boolean IsViewOld = true;
	 	Account acc1 = new Account();
	    acc1.Name = '第一医院';
	    insert acc1;
		Contact con = new Contact();
		con.LastName = '张先生';
		con.AccountId = acc1.Id;
		insert con;
    	Contact_Mod__c modc = new Contact_Mod__c();
    	modc.Name__c = con.Id;
    	modc.Type__c = '编辑';
    	modc.NewContact__c = '修改前';
    	modc.Birthday__c = Date.today(); 
    	modc.SP_OldBirthday__c = Date.today()+1;
    	modc.Mobile__c = '13012344321';
    	modc.SP_OldMobile__c = '13009877890';
    	modc.SP_OldGender__c = '男';
    	modc.Gender__c = '女';
    	modc.V2_interest__c = '篮球';
    	modc.SP_OldV2interest__c = '足球';
    	modc.Phone__c = '13112345678';
    	modc.SP_OldPhone__c = '18909098989';
    	modc.SP_SevoFGF__c = 1;
    	modc.SP_OldSevoFGF__c = 3;
    	modc.Fax__c = '123@163.com';
    	modc.SP_OldFax__c = '321@163.com';
    	modc.SP_OldIdcard2__c = '230119199023122101';
    	modc.ID_card2__c = '230113122109121201';
    	modc.SP_OperationTime__c = 10;
    	modc.SP_OldOperationTime__c = 1;
    	modc.SP_OldAnesthesiaMonthyNum__c = 1;
    	modc.SP_OldAnesthesiaMonthyNum__c = 2;
    	modc.SP_OldInhaleMonthyNum__c = 2;
    	modc.SP_InhaleMonthyNum__c = 1;
    	modc.SP_OldV2PassPortNo__c = '34543';
    	modc.V2_PassPortNo__c = '12321';
    	modc.Email__c = '1321@212.com';
    	modc.SP_OldEmail__c = '12322@ww.com';
    	modc.SP_Job__c = '护士';
    	modc.SP_OldJob__c = '医生';
    	modc.SP_Department__c = '妇科';
    	modc.SP_OldDepartment__c = '骨科';
    	modc.Contact_Type__c = '重要';
    	modc.SP_OldComment__c = '非重要';
    	ID ContactId = con.Id;  	
       	system.Test.startTest();
		insert modc;
		SP_ContactChangeController spc = new SP_ContactChangeController();
		spc.ContactModId = modc.Id;
		string Url = string.valueOf(System.URL.getSalesforceBaseUrl());
	 	Url = Url.substring(Url.indexOf('=')+1,Url.length()-1);
	 	spc.RecordUrl = Url+'/'+modc.Id;
		spc.generateTableDynamic();
		modc.Type__c = '编辑';
		update modc;
    	system.Test.stopTest();   
	}
}