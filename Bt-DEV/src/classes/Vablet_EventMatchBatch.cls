/*
 * Author: Steven Ke
 * Date: 2014-2-20
 * Description: Vablet文件调阅 和 拜访Event之间根据 销售代表 拜访时间的匹配 , 对匹配成功的生成Vablet拜访数据记录, 并对拜访Event的Vablet拜访赋值
 */
global class Vablet_EventMatchBatch implements Database.Batchable<Event>
{
    global Iterable<Event> start(Database.BatchableContext BC)
    {
        Date yday = Date.today().addDays(-1);
        Date tday = Date.today().addDays(1);
        
        return [Select 
                    Vablet_IsVabletVisit__c, 
                    StartDateTime, 
                    OwnerId, 
                    EndDateTime, 
                    Id 
                From 
                    Event 
                Where 
                    RecordType.Name = '拜访' And CreatedDate >: yday And CreatedDate <: tday And (not Owner.Profile.Name like'TW%')];
    }
    
    global void execute(Database.BatchableContext BC, List<Event> scope)
    {
        Date sevenDaysAgo = Date.today().addDays(-6);
        Date tday = Date.today().addDays(1);
        
        System.debug('>>>>>>' + scope.size());
        
        List<Vablet_DocumentRetrival__c> vdrList = [Select 
                                                        OwnerId, 
                                                        Vablet_StartTime__c, 
                                                        Vablet_Duration__c, 
                                                        Vablet_isMatchedVisit__c,
                                                        Id 
                                                    From 
                                                        Vablet_DocumentRetrival__c
                                                    Where 
                                                        Vablet_StartTime__c >: sevenDaysAgo And Vablet_StartTime__c <: tday];
        
        List<Vablet_VisitDate__c> vvdList = new List<Vablet_VisitDate__c>();
        Set<Event> eSet = new Set<Event>();
        
        for(Vablet_DocumentRetrival__c vdr : vdrList)
        {
            // 存放距该文件调阅时间最小的拜访和最小的时间
            Map<Long,Event> gapEvent = new Map<Long, Event>();
            
            // 存放该文件调阅是否匹配上的boolean值
            Boolean isVDRNotMatched = true;
            
            for(Event e : scope)
            {
                if(e.OwnerId == vdr.OwnerId)
                {
                    Datetime vdrStartTime = vdr.Vablet_StartTime__c;
                    Datetime vdrEndTime = vdr.Vablet_StartTime__c.addSeconds((Integer)vdr.Vablet_Duration__c);
                    Datetime eStartTime = e.StartDateTime;
                    Datetime eEndTime = e.EndDateTime;
                    
                    // eStartTime 在 vdrStartTime 和 vdrEndTime之间
                    Boolean eStartTimeBetweenVDR = vdrStartTime <= eStartTime && eStartTime <= vdrEndTime;
                    
                    // vdrStartTime 在 eStartTime 和 eEndTime之间
                    Boolean vdrStartTimeBetweenEvent = eStartTime <= vdrStartTime && vdrStartTime <= eEndTime;
                    
                    // 间隔时间
                    Long gap1 = Math.abs(vdrEndTime.getTime() - eStartTime.getTime());
                    Long gap2 = Math.abs(vdrStartTime.getTime() - eEndTime.getTime());
                    
                    // 间隔时间是否小于1小时
                    Boolean lessThanOneHour1 = gap1 <= 3600000;
                    Boolean lessThanOneHour2 = gap2 <= 3600000;
                    
                    if(eStartTimeBetweenVDR || vdrStartTimeBetweenEvent)
                    {
                        e.Vablet_IsVabletVisit__c = true;
                        vdr.Vablet_isMatchedVisit__c = true;
                        
                        Vablet_VisitDate__c vvd = new Vablet_VisitDate__c();
                        vvd.Vablet_EventId__c = e.Id;
                        vvd.Vablet_DocumentRetrival__c = vdr.Id;
                        vvd.Vablet_ExternalID__c = vvd.Vablet_EventId__c+vvd.Vablet_DocumentRetrival__c;
                        vvd.OwnerId = vdr.OwnerId;
                        vvdList.add(vvd);
                        isVDRNotMatched = false;
                        System.debug('member number of List' + vvdList.size());
                        if(!eSet.contains(e))
                        {
                            eSet.add(e);
                        }
                        System.debug('member number of Set' + eSet.size());
                    }
                    else if(lessThanOneHour1 || lessThanOneHour2)
                    {
                        if(gap1 <= gap2)
                        {
                            gapEvent.put(gap1, e);
                        }
                        else if(gap2 < gap1)
                        {
                            gapEvent.put(gap2, e);
                        }
                    }
                }
            }
            if(isVDRNotMatched && gapEvent.size() > 0)
            {
                Set<Long> gapSet = gapEvent.keySet();
                List<Long> gapList = new List<Integer>();
                gapList.addAll(gapSet);
                gapList.sort();
                
                Event matchedEvent = new Event();
                matchedEvent = gapEvent.get(gapList[0]);
                matchedEvent.Vablet_IsVabletVisit__c = true;
                vdr.Vablet_isMatchedVisit__c = true;
                
                Vablet_VisitDate__c vvd = new Vablet_VisitDate__c();
                vvd.Vablet_EventId__c = matchedEvent.Id;
                vvd.Vablet_DocumentRetrival__c = vdr.Id;
                vvd.OwnerId = vdr.OwnerId;
                vvd.Vablet_ExternalID__c = vvd.Vablet_EventId__c+vvd.Vablet_DocumentRetrival__c;
                vvdList.add(vvd);
                
                if(!eSet.contains(matchedEvent))
                {
                    eSet.add(matchedEvent);
                }
            }
        }
        List<Event> eList = new List<Event>();
        eList.addAll(eSet);
        //insert vvdList;
        upsert vvdList Vablet_ExternalID__c;
        update eList;
        update vdrList;
    }
    
    global void finish(Database.BatchableContext BC)
    {
        
    }
}