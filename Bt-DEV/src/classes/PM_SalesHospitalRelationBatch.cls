/**
 * Author : bill
 * Date：2013-10-27
 * 每月1号凌晨,如果销售医院关系发生变化，则病人上的当前负责销售发生变化
 **/
global class PM_SalesHospitalRelationBatch implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC){
    	//获取应处理的销售医院关系
    	//执行日期开始时间
    	Date startDate = date.today().addMonths(-1); 
    	return Database.getQueryLocator([Select Id,V2_Account__c,V2_Account__r.Name,V2_User__c,V2_BatchOperate__c,V2_NewAccUser__c From V2_Account_Team__c 
    			Where UserProduct__c = 'PD' And IsPending__c = true and V2_LastAccessDate__c <= TODAY 
    			and V2_LastAccessDate__c >=: startDate]); 								  
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope){
    	//需要处理的客户医院Set
    	Set<ID> set_AccIds = new Set<ID>();
    	//销售和医院对应关系
    	Map<ID,ID> map_saleHos = new Map<ID,ID>();
    	
    	for(sObject sObj : scope){
    		V2_Account_Team__c vat = (V2_Account_Team__c)sObj;
    		set_AccIds.add(vat.V2_Account__c);
    		if(!map_saleHos.containsKey(vat.V2_Account__c))
    		{
    			if(vat.V2_BatchOperate__c == '新增')
    			{
    				map_saleHos.put(vat.V2_Account__c, vat.V2_User__c);
    			}
    			else if(vat.V2_BatchOperate__c == '替换')
				{
    				map_saleHos.put(vat.V2_Account__c, vat.V2_NewAccUser__c);
    			}
    		}
    	}
    	
    	List<Account> list_acc = [Select a.Id, (Select Id, PM_Current_Saler__c From PMMNib__r),(Select Id ,PM_Treat_Saler__c From patientcqKQ__r) From Account a where Id IN : set_AccIds];
    	list<PM_Patient__c> list_patient = new list<PM_Patient__c>();
    	list<PM_Patient__c> list_Treatpatient = new list<PM_Patient__c>();
    	for(Account acc : list_acc)
    	{
    		for(PM_Patient__c pa : acc.PMMNib__r)
    		{
    			pa.PM_Current_Saler__c = map_saleHos.get(acc.Id);
    		}
    		for(PM_Patient__c treat : acc.patientcqKQ__r)
    		{
    			treat.PM_Treat_Saler__c = map_saleHos.get(acc.Id);
    		}
    		list_patient.addAll(acc.PMMNib__r);  
    		list_Treatpatient.addAll(acc.patientcqKQ__r);		
    	}
    	//更新病人的插管医院下当前负责插管销售
    	If(list_patient != null && list_patient.size()>0)
    	{
    		update list_patient;
    	}
    	//更新病人的治疗医院下当前负责治疗销售
    	If(list_Treatpatient != null && list_Treatpatient.size()>0)
    	{
    		update list_Treatpatient;
    	}
    }
    global void finish(Database.BatchableContext BC){
    	
    }
}