/**
 * author: bill
 * AutoGetInfoForApplication的trigger的测试类
 */
@isTest
private class Test_AutoGetInfoForApplication {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        //客户
        Account acc = new Account();
        acc.Name = 'aaaa';
        insert acc;
        
        //联系人
        Contact contact = new Contact();
        contact.AccountId = acc.Id;
        contact.FirstName = 'zhang';
        contact.LastName = 'san';
        contact.Phone = '010-1234567';
        insert contact;
        
        //挥发罐申请
        Vaporizer_Application__c vapApply = new Vaporizer_Application__c();
        vapApply.Contact__c = contact.Id;
        
        test.startTest();
        try{
        insert vapApply;
        }catch(Exception e){}
        test.stopTest();
    }
}