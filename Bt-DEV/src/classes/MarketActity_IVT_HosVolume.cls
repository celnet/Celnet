/**
 * 作者:Bill
 * 说明：市场活动相关医院销量表-- For IVT
**/
public class MarketActity_IVT_HosVolume { 
    //pageMessage是否显示
    public boolean pageMessage{get;set;}
    //内容是否显示
    public boolean output{get;set;}
    //列表列数
    public integer tableCols{get;set;}  
    //市场活动ID
    public ID campaignId{get;set;}
    //市场活动
    private Campaign campaign;
    //市场活动中联系人
    private List<Contact> list_Contact;
    //市场活动名称
    public String accountName{get;set;}
    //销售数据
    private List<SalesReport__c> list_SalesReport;
    //市场活动中的客户
    private List<Account> list_Account;
    //月份开始时间
    private datetime startTime;
    //客户最新所有人
    Map<ID,String> map_ContactUserName = new Map<ID,String>();
    //销售数据
    Map<String,SalesReport__c> map_Sales = new Map<String,SalesReport__c>();
    //查询时间阶段
    public String StartYear{get;set;}
    public String StartMonth{get;set;}
    public String EndYear{get;set;}
    public String EndMonth{get;set;}
    //查询月份
    public string[] Strings{get;set;}
    //年
    public List<SelectOption> ListYears{get;set;}
    //月
    public List<SelectOption> getListMonths()
    {
        list<SelectOption> options=new list<SelectOption>();
        options.add(new SelectOption('1','1'));options.add(new SelectOption('2','2'));
        options.add(new SelectOption('3','3'));options.add(new SelectOption('4','4'));
        options.add(new SelectOption('5','5'));options.add(new SelectOption('6','6'));
        options.add(new SelectOption('7','7'));options.add(new SelectOption('8','8'));
        options.add(new SelectOption('9','9'));options.add(new SelectOption('10','10'));
        options.add(new SelectOption('11','11'));options.add(new SelectOption('12','12'));
        return options;
    }
    
    //构造函数
    public MarketActity_IVT_HosVolume(apexpages.Standardcontroller controller){
      tableCols = 2;
      pageMessage = false;
      output = true;
      campaignId = controller.getId();
       startTime = datetime.now().addYears(-1);
       //市场活动开始时间
       campaign = [Select c.StartDate From Campaign c where Id = :campaignId];
       if(campaign.StartDate != null){
         startTime = campaign.StartDate.addYears(-1);
       }
       //默认年月
       StartYear = String.valueOf(startTime.year());
       StartMonth = String.valueOf(startTime.Month());
       EndYear = String.valueOf(startTime.year()+2);
       EndMonth = String.valueOf(startTime.Month());
       getList_Contact();
       Months();
       Set<ID> accountId = new Set<ID>();
       for(Contact con : list_Contact)
       {
          accountId.add(con.AccountId);
       }
       list_Account = [Select a.Id, a.Name From Account a where id in : accountId];
       //Ivt部门产品是IVT,月份没有失效的记录的最新负责人
       list<V2_Account_Team__c> list_V2_Account_Team = [Select  v.V2_User__r.Name, v.V2_Effective_Year__c, v.V2_Effective_Month__c, v.V2_Delete_Year__c, v.V2_Delete_Month__c, v.V2_NewAccUser__c, v.V2_Account__c, V2_NewAccUser__r.Name From V2_Account_Team__c v where V2_Account__c in :accountId  and  V2_BatchOperate__c != '删除' and V2_UserProduct__c = 'IVT' and V2_ImmediateDelete__c =false];
       for(V2_Account_Team__c accTeam : list_V2_Account_Team){
            //失效年月为空的最新负责人
            if((accTeam.V2_Delete_Year__c == null && accTeam.V2_Delete_Month__c == null)||(accTeam.V2_Delete_Year__c == '' && accTeam.V2_Delete_Month__c == ''))
            {
                map_ContactUserName.put(accTeam.V2_Account__c , accTeam.V2_User__r.Name);
            }
       }
       //获取销售数据
       //2013-5-23 Sunny: 不可以查询所有销售数据，需要添加限制条件，1.销售数据所属客户2.销售数据日期限制
       Date sDate = Date.valueOf(StartYear+'-1-1');
       Date eDate = Date.valueOf(EndYear+'-12-31');
       /***********************优化 start****************************/
       list_SalesReport = [Select s.TargetQty__c, s.TargetAmount__c, s.SBU__c, s.ActualQty__c, s.ActualAmount__c, s.Account__c , Time__c
                   From SalesReport__c s 
                   Where Time__c >=: sDate And Time__c <=: eDate And SBU__c = 'IVT' And Account__c in: accountId order BY Time__c asc];
       for(SalesReport__c sr : list_SalesReport){
            map_Sales.put(string.valueOf(sr.Time__c.year())+string.valueOf(sr.Time__c.Month())+sr.Account__c,sr);
       }
       /***********************优化 end****************************/
       //list_SalesReport = [Select s.Year__c, s.TargetQty__c, s.TargetAmount__c, s.SBU__c, s.Month__c, s.ActualQty__c, s.ActualAmount__c, s.Account__c From SalesReport__c s];
       ListYears = new  List<SelectOption>();
       ListYears.add(new SelectOption(String.valueOf(date.today().addYears(-1).year()),String.valueOf(date.today().addYears(-1).year())));
       ListYears.add(new SelectOption(String.valueOf(date.today().year()),String.valueOf(date.today().year())));
       ListYears.add(new SelectOption(String.valueOf(date.today().addYears(1).year()),String.valueOf(date.today().addYears(1).year())));
    }
    
    //获取活动对应的前后24个月
    public void Months() {
        Strings = new string[]{};
        Date startDate = date.valueOf(StartYear+'-'+StartMonth+'-1');
        Date endDate = date.valueOf(EndYear+'-'+EndMonth+'-1');
                system.debug(startDate +'@@@');
        //2013-3-16 Sunny:修改年月显示方式，为使报表展现更简洁。
        if(endDate >= startDate){
            pageMessage = false;
            output = true;
            for(integer i=0; i<=((endDate.year()-startDate.year())*12+(endDate.month()-startDate.month()));i++ ){
                if((startDate.month()+i)<=24){
                      if((startDate.month()+i)<=12){
                         Strings.Add(string.valueOf(startDate.year())+'.'+string.valueOf(startDate.month()+i)+'');
                      }else{
                         Strings.Add(string.valueOf(startDate.year()+1)+'.'+string.valueOf((startDate.month()+i)-12)+'');
                      }
                }else{
                    Strings.Add(string.valueOf(startDate.year()+2)+'.'+string.valueOf(startDate.month()+i-24)+'');
                }
                tableCols = tableCols+i;
            }
        }else{
            //时间不合法
            pageMessage = true;
            output = false;
        }
    }
    
    //查询
    public PageReference SelectResult()
    {
        Months();
        return null;
    }
    
    //获取用户
     public void getList_Contact(){
        //联系人Id
        List<ID> list_ContactId = new List<ID>();
        
        /*********************bill update 2013-6-26 start*******************************/
        ID CampaignOwnweId = [Select c.OwnerId From Campaign c where c.Id = : campaignId][0].OwnerId;
        List<Campaign> list_Campaign = new List<Campaign>();
        List<Profile> UPro = [Select p.Name From Profile p Where Id =: UserInfo.getProfileId()];
        if(UPro.size() <= 0){
            return;
        }
        system.debug(UPro[0].Name.toUpperCase()+'zgxm');
        if(CampaignOwnweId == UserInfo.getUserId() || UPro[0].Name.toUpperCase().contains('IVT ADMIN') || UPro[0].Name.toUpperCase().contains('RENAL ADMIN') || UPro[0].Name.toUpperCase().contains('系统管理员'))
        {
            list_Campaign = [Select c.Id, c.Name, (Select ContactId From CampaignMembers where V2_Participated__c = true) From Campaign c where c.Id = : campaignId];
        }else{
            V2_UtilClass util = new V2_UtilClass();
            //用户下属Id
            Set<ID> userIds = util.GetUserSubIds();
            userIds.add(UserInfo.getUserId());
            //获取市场活动中参加的联系人
            list_Campaign = [Select c.Id, c.Name, (Select ContactId From CampaignMembers where User__c in : userIds and V2_Participated__c = true) From Campaign c where c.Id = : campaignId];
        }
        /*********************bill update 2013-6-26 end*******************************/
        accountName = list_Campaign[0].Name;
        if(list_Campaign[0].CampaignMembers != null && list_Campaign[0].CampaignMembers.size()>0){
             for(CampaignMember CamMembers : list_Campaign[0].CampaignMembers){
                list_ContactId.add(CamMembers.ContactId);
             }
        }
        list_Contact = [Select c.Name, c.AccountId From Contact c where id in: list_ContactId];
     }

    //ivt页面数据所需LIst
    public List<MarketActivityField> getList_MarketActivityFields(){
        List<MarketActivityField> List_MarketActivityFields  = new List<MarketActivityField>();
        for(Account acc : list_Account){
            MarketActivityField maField = new MarketActivityField();
            mafield.AccountName = acc.Name;
            //获取最新销售数据所有人
            maField.NewOwner = map_ContactUserName.get(acc.Id);
            //选定开始日期和结束日期
            Date startDate = date.valueOf(StartYear+'-'+StartMonth+'-1');
            Date endDate = date.valueOf(EndYear+'-'+EndMonth+'-1');
            for(integer i=0; i<=((endDate.year()-startDate.year())*12+(endDate.month()-startDate.month())); i++ ){
                //实际销售量
                string actSale =' ';
                //目标销售量
                string goalSale =' ';
                //完成率对象
                string rate =' ';
                //年
                string year;
                //月
                string month;
                if((startDate.month()+i)<=24){
                  if((startDate.month()+i)<=12){
                     year = string.valueOf(startDate.year());
                     month = string.valueOf(startDate.month()+i);
                  }else{
                     year = string.valueOf(startDate.year()+1);
                     month = string.valueOf(startDate.month()+i-12);
                  }
                }else{
                    year = string.valueOf(startDate.year()+2);
                    month = string.valueOf(startDate.month()+i-24);
                }
                SalesReport__c sreport = map_Sales.get(year+month+acc.Id);
                if(sreport != null){
                  if(sreport.ActualQty__c != null){
                    actSale = string.valueOf(sreport.ActualQty__c);
                  }
                  if(sreport.ActualQty__c != null && sreport.ActualQty__c > 0){
                    goalSale =  string.valueOf(sreport.TargetQty__c);
                    if(sreport.TargetQty__c != null && sreport.TargetQty__c > 0){
                        Double DD1 = double.valueOf((sreport.ActualQty__c/sreport.TargetQty__c)*10000);
                        Integer value = DD1.intValue();
                        string str = string.valueOf(double.valueOf(value)/100);
                        if(str.LastIndexOf('.') == str.length()-2){
                            rate = str +'0%';
                        }else{
                            rate = str +'%';
                        }
                    }
                  }
                } 
                maField.list_ActualSales.add(actSale);
                maField.list_GoalSales.add(goalSale);
                maField.list_CompletionRates.add(rate);
            }
                List_MarketActivityFields.add(maField);
            }
            return List_MarketActivityFields;           
        }
    
    

/****************************************************Test*********************************/
    static testMethod void myUnitTest()
    {
        //user role
        List<UserRole> listur= new List<UserRole>();
        UserRole ur1 = new UserRole();
        ur1.Name = 'Renal-Supervisor-华南-IVT-Supervisor(U1)';
        listur.add(ur1);
        UserRole ur2 = new UserRole();
        ur2.Name = 'Renal-Rep-华南-SP-Rep(U2)';
        listur.add(ur2);
        insert listur;
        
        //user 
        list<User> user=[Select TimeZoneSidKey,ProfileId,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname, Alias From User Where IsActive = true limit 1];
        List<User> list_user = new List<User>();
        User user1=new User();
        user1.username='user1r2@123.com';
        user1.LastName='user1r2';
        user1.Email='user1r2@123.com';
        user1.Alias=user[0].Alias;
        user1.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user1.ProfileId=UserInfo.getProfileId();
        user1.LocaleSidKey=user[0].LocaleSidKey;
        user1.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user1.EmailEncodingKey=user[0].EmailEncodingKey;
        user1.CommunityNickname='chequ';
        user1.MobilePhone='12345678912';
        user1.IsActive = true;
        user1.UserRoleId=ur1.Id ;
        list_user.add(user1);
        insert list_user;
        
        //客户
        List<Account> list_acc = new List<Account>();
        Account acc = new Account();
        acc.Name = 'klm测试市场活动';
        list_acc.add(acc);
        insert list_acc;
        
        //联系人
        List<Contact> list_contact = new List<Contact>();
        Contact ct = new Contact();
        ct.FirstName = 'zhang';
        ct.LastName = 'san';
        ct.AccountId = acc.Id;
        list_contact.add(ct);
        insert list_contact;
        
        //销售医院关系
        List<V2_Account_Team__c> list_V2_Account_Team = new  List<V2_Account_Team__c>();
        V2_Account_Team__c v2acc = new V2_Account_Team__c();
        v2acc.V2_NewAccUser__c = user1.Id;
        v2acc.V2_ApprovalStatus__c = '审批通过';
        v2acc.V2_BatchOperate__c = '新增';
        list_V2_Account_Team.add(v2acc);
        insert list_V2_Account_Team;
        
        //市场活动
        List<Campaign> list_campaign = new List<Campaign>();
        Campaign cam = new Campaign();
        cam.Name = '市场活动咔咔咔';
        cam.StartDate = Date.valueOf('2013-1-1');
        list_campaign.add(cam);
        insert list_campaign;
        
        //市场活动成员
        List<CampaignMember> list_CampaignMember = new List<CampaignMember>();
        CampaignMember camer = new CampaignMember();
        camer.ContactId = ct.Id;
        camer.CampaignId = cam.Id;
        camer.V2_Participated__c = true;
        list_CampaignMember.add(camer);
        insert list_CampaignMember;
        
        //销售数据
        List<SalesReport__c> list_salesReport = new List<SalesReport__c>();
        SalesReport__c sr = new SalesReport__c();
        sr.Account__c = acc.Id;
        sr.TargetQty__c = 100.00;
        sr.ActualQty__c = 80.00;
        sr.SBU__c = 'IVT';
        sr.Time__c = date.valueOf('2012-1-1');
        list_salesReport.add(sr);
        SalesReport__c sr1 = new SalesReport__c();
        sr1.Account__c = acc.Id;
        sr1.TargetQty__c = 100.00;
        sr1.ActualQty__c = 80.00;
        sr1.Time__c = date.valueOf('2012-2-1');
        sr1.SBU__c = 'IVT';
        list_salesReport.add(sr1);
        SalesReport__c sr2 = new SalesReport__c();
        sr2.Account__c = acc.Id;
        sr2.TargetQty__c = 100.00;
        sr2.ActualQty__c = 80.00;
        sr2.Time__c = date.valueOf('2012-3-1');
        sr2.SBU__c = 'IVT';
        list_salesReport.add(sr2);
        SalesReport__c sr3 = new SalesReport__c();
        sr3.Account__c = acc.Id;
        sr3.TargetQty__c = 100.00;
        sr3.ActualQty__c = 80.00;
        sr3.Time__c = date.valueOf('2012-4-1');
        sr3.SBU__c = 'IVT';
        list_salesReport.add(sr3);
        SalesReport__c sr4 = new SalesReport__c();
        sr4.Account__c = acc.Id;
        sr4.TargetQty__c = 100.00;
        sr4.ActualQty__c = 80.00;
        sr4.Time__c = date.valueOf('2012-5-1');
        sr4.SBU__c = 'IVT';
        list_salesReport.add(sr4);
        SalesReport__c sr5 = new SalesReport__c();
        sr5.Account__c = acc.Id;
        sr5.TargetQty__c = 100.00;
        sr5.ActualQty__c = 80.00;
        sr5.Time__c = date.valueOf('2012-6-1');
        list_salesReport.add(sr5);
        SalesReport__c sr6 = new SalesReport__c();
        sr6.Account__c = acc.Id;
        sr6.TargetQty__c = 100.00;
        sr6.ActualQty__c = 80.00;
        sr6.Time__c = date.valueOf('2012-7-1');
        sr6.SBU__c = 'IVT';
        list_salesReport.add(sr6);
        SalesReport__c sr7 = new SalesReport__c();
        sr7.Account__c = acc.Id;
        sr7.TargetQty__c = 100.00;
        sr7.ActualQty__c = 80.00;
        sr7.Time__c = date.valueOf('2012-8-1');
        list_salesReport.add(sr7);
        SalesReport__c sr8 = new SalesReport__c();
        sr8.Account__c = acc.Id;
        sr8.TargetQty__c = 100.00;
        sr8.ActualQty__c = 80.00;
        sr8.Time__c = date.valueOf('2012-8-1');
        sr8.SBU__c = 'IVT';
        list_salesReport.add(sr8);
        SalesReport__c sr9 = new SalesReport__c();
        sr9.Account__c = acc.Id;
        sr9.TargetQty__c = 100.00;
        sr9.ActualQty__c = 80.00;
        sr9.Time__c = date.valueOf('2012-10-1');
        sr9.SBU__c = 'IVT';
        list_salesReport.add(sr9);
        SalesReport__c sr10 = new SalesReport__c();
        sr10.Account__c = acc.Id;
        sr10.TargetQty__c = 100.00;
        sr10.ActualQty__c = 80.00;
        sr10.Time__c = date.valueOf('2012-11-1');
        list_salesReport.add(sr10);
        SalesReport__c sr11 = new SalesReport__c();
        sr11.Account__c = acc.Id;
        sr11.TargetQty__c = 100.00;
        sr11.ActualQty__c = 80.00;
        sr11.Time__c = date.valueOf('2012-12-1');
        list_salesReport.add(sr11);
        SalesReport__c sr12 = new SalesReport__c();
        sr12.Account__c = acc.Id;
        sr12.TargetQty__c = 100.00;
        sr12.ActualQty__c = 80.00;
        sr12.Time__c = date.valueOf('2013-1-1');
        list_salesReport.add(sr12);
        SalesReport__c sr13 = new SalesReport__c();
        sr13.Account__c = acc.Id;
        sr13.TargetQty__c = 100.00;
        sr13.ActualQty__c = 80.00;
        sr13.Time__c = date.valueOf('2013-2-1');
        list_salesReport.add(sr13);
        SalesReport__c sr14 = new SalesReport__c();
        sr14.Account__c = acc.Id;
        sr14.TargetQty__c = 100.00;
        sr14.ActualQty__c = 80.00;
        sr14.Time__c = date.valueOf('2013-3-1');
        list_salesReport.add(sr14);
        SalesReport__c sr15 = new SalesReport__c();
        sr15.Account__c = acc.Id;
        sr15.TargetQty__c = 100.00;
        sr15.ActualQty__c = 80.00;
        sr15.Time__c = date.valueOf('2013-4-1');
        list_salesReport.add(sr15);
        SalesReport__c sr16 = new SalesReport__c();
        sr16.Account__c = acc.Id;
        sr16.TargetQty__c = 100.00;
        sr16.ActualQty__c = 80.00;
        sr16.Time__c = date.valueOf('2013-5-1');
        list_salesReport.add(sr16);
        SalesReport__c sr18 = new SalesReport__c();
        sr18.Account__c = acc.Id;
        sr18.TargetQty__c = 100.00;
        sr18.ActualQty__c = 80.00;
        sr18.Time__c = date.valueOf('2013-6-1');
        list_salesReport.add(sr18);
        SalesReport__c sr19 = new SalesReport__c();
        sr19.Account__c = acc.Id;
        sr19.TargetQty__c = 100.00;
        sr19.ActualQty__c = 80.00;
        sr19.Time__c = date.valueOf('2013-7-1');
        list_salesReport.add(sr19);
        SalesReport__c sr20 = new SalesReport__c();
        sr20.Account__c = acc.Id;
        sr20.TargetQty__c = 100.00;
        sr20.ActualQty__c = 80.00;
        sr20.Time__c = date.valueOf('2013-8-1');
        list_salesReport.add(sr20);
        SalesReport__c sr21 = new SalesReport__c();
        sr21.Account__c = acc.Id;
        sr21.TargetQty__c = 100.00;
        sr21.ActualQty__c = 80.00;
        sr21.Time__c = date.valueOf('2013-9-1');
        list_salesReport.add(sr21);
        SalesReport__c sr22 = new SalesReport__c();
        sr22.Account__c = acc.Id;
        sr22.TargetQty__c = 100.00;
        sr22.ActualQty__c = 80.00;
        sr22.Time__c = date.valueOf('2013-10-1');
        list_salesReport.add(sr22);
        SalesReport__c sr23 = new SalesReport__c();
        sr23.Account__c = acc.Id;
        sr23.TargetQty__c = 100.00;
        sr23.ActualQty__c = 80.00;
        sr23.Time__c = date.valueOf('2013-11-1');
        list_salesReport.add(sr23);
        SalesReport__c sr24 = new SalesReport__c();
        sr24.Account__c = acc.Id;
        sr24.TargetQty__c = 100.00;
        sr24.ActualQty__c = 80.00;
        sr24.Time__c = date.valueOf('2013-12-1');
        list_salesReport.add(sr24);
        insert list_salesReport;
        
        apexpages.Standardcontroller controller = new apexpages.Standardcontroller(cam);
        MarketActity_IVT_HosVolume mh = new MarketActity_IVT_HosVolume(controller);
        mh.getListMonths();
        mh.SelectResult();
        mh.getList_MarketActivityFields();
    }
}