/*
 * Author: Steven 
 * Date: 2014-2-24
 * Description: Test
 */
@isTest
private class Test_NewCountoryPage {
	static testmethod void testNCPage()
	{
		Provinces__c p1 = new Provinces__c();
		p1.Name = '上海';
		insert p1;
		Cities__c c1 = new Cities__c();
		c1.Name = '上海市';
		List<Provinces__c> pList = [Select Id From Provinces__c Where Name = '上海'];
		c1.BelongToProvince__c = pList[0].Id;
		insert c1;
		NewCountory__c tccp = new NewCountory__c();
		tccp.GA_HD_ContainsAdjuvantDrug__c = '是';
		tccp.GA_HD_PatientPayScale__c = 3;
		tccp.GA_HD_DialysisPayment__c = '按病种定额付费';
		tccp.GA_HD_StartToPay__c = 1000;
		tccp.GA_HD_FirstPayScale__c = 3;
		tccp.GA_HD_HealthInsuranceType__c = '甲';
		tccp.GA_HD_HealthInsurancePayScale__c = 3;
		tccp.GA_HD_PolicyTendency__c = '有利于血透';
		tccp.GA_HD_PayObstacle__c = '1231';
		tccp.GA_HD_MaximumLimit__c = 199;
		tccp.GA_PD_ContainsAdjuvantDrug__c = '是';
		tccp.GA_PD_PatientPayScale__c = 3;
		tccp.GA_PD_DialysisPayment__c = '按病种定额付费';
		tccp.GA_PD_StartToPay__c = 1000;
		tccp.GA_PD_FirstPayScale__c = 3;
		tccp.GA_PD_HealthInsuranceType__c = '甲';
		tccp.GA_PD_HealthInsurancePayScale__c = 3;
		tccp.GA_PD_PolicyTendency__c = '有利于血透';
		tccp.GA_PD_PayObstacle__c = '1231';
		tccp.GA_PD_MaximumLimit__c = 199;
		List<Cities__c> ctcList = [Select Id From Cities__c Where Name = '上海市'];
		tccp.City__c = ctcList[0].Id;
		insert tccp;
		
		System.test.startTest();
		Apexpages.Standardcontroller controll = new Apexpages.Standardcontroller(c1);
		Ctrl_NewCountoryPage uarCtrl = new Ctrl_NewCountoryPage(controll);
		uarCtrl.Editit1();
		uarCtrl.Editit2();
		uarCtrl.Saveit1();
		uarCtrl.Saveit2();
		uarCtrl.Editit1();
		uarCtrl.CancelEdit1();
		uarCtrl.Editit2();
		uarCtrl.CancelEdit2();
		System.test.stopTest();
	}
}