/*
 * Tobe
 * 2013.10.30
 * PM_AutoFillContactPriorityForPatient测试类
 */
@isTest
private class PM_Test_AutoFillContactPriority {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        list<Profile> list_Profile = [Select Id,Name From Profile Where Name = 'PM-PSR Manager'];
        list<UserRole> list_role = [Select Id,Name From UserRole Where DeveloperName ='RenalGDAccountManager'];
        User user = new User();
        user.ProfileId = list_Profile[0].Id;
        user.Username = 'asfdasdf@acme.com';
        user.FirstName = 'Test';
        user.LastName = 'Test';
        user.Email = 'asdf@aa.com';
        user.Alias = 'asdf';
        user.EmailEncodingKey = 'UTF-8';
        user.TimeZoneSidKey = 'Asia/Shanghai';
        user.LanguageLocaleKey = 'zh_CN';
        user.LocaleSidKey = 'zh_CN';
        user.UserRoleId = list_role[0].Id;
        insert user; 
        Region__c zone = new Region__c();
        zone.Name = '南区';
        zone.PM_RegionalHead__c = user.Id;
        insert zone;
		Provinces__c pro = new Provinces__c();
    	pro.Name = '浙江省';
    	insert pro;
    	Cities__c city = new Cities__c();
    	city.Name = '杭州市';
    	city.BelongToProvince__c = pro.Id;
    	insert city;
    	list<RecordType> record = [Select Id From RecordType Where Name = '医院'];
    	list<RecordType> record2 = [Select Id From RecordType Where Name = '经销商'];
        Account acc = new  Account();
        acc.Name = '浙江医院';
        acc.RecordTypeId = record[0].Id;
        acc.Provinces__c = pro.Id;
        acc.Cities__c = city.Id;
        acc.Region__c = zone.Id;
        insert acc;
        Account acc3 = new  Account();
        acc3.Name = '北京医院';
        acc3.RecordTypeId = record2[0].Id;
        acc3.Provinces__c = pro.Id;
        acc3.Cities__c = city.Id;
        acc3.Region__c = zone.Id;
        insert acc3;
        PM_Patient__c patient1 = new PM_Patient__c();
    	patient1.Name = '张先生Test';
    	patient1.PM_Status__c = 'Active';
    	patient1.PM_PmPhone__c = '18911112222';
    	patient1.PM_ProblemRemarks__c = '备注';
    	patient1.PM_InHospital__c = acc.Id;
    	patient1.PM_LastFailDateTime__c = datetime.newInstance(2013,3,12,5,3,3);
    	patient1.PM_LastSuccessDateTime__c= datetime.newInstance(2013,3,12,5,3,3);
    	insert patient1;
    	PM_Patient__c patient2 = new PM_Patient__c();
    	patient2.Name = '张先生Test';
    	patient2.PM_Status__c = 'Active';
    	patient2.PM_PmPhone__c = '18911112222';
    	patient2.PM_ProblemRemarks__c = '备注';
    	patient2.PM_InHospital__c = acc.Id;
    	patient2.PM_LastFailDateTime__c = datetime.newInstance(2013,3,12,15,13,3);
    	patient2.PM_LastSuccessDateTime__c= datetime.newInstance(2013,3,12,15,13,3);
    	insert patient2;
    	PM_Patient__c patient3 = new PM_Patient__c();
    	patient3.Name = '张先生Test';
    	patient3.PM_Status__c = 'Active';
    	patient3.PM_PmPhone__c = '18911112222';
    	patient3.PM_ProblemRemarks__c = '备注';
    	patient3.PM_InHospital__c = acc.Id;
    	patient3.PM_TelPriorityFirst__c = '家属电话';
    	patient3.PM_TelPrioritySecond__c = '家属手机';
    	insert patient3;
    	PM_Patient__c patient4 = new PM_Patient__c();
    	patient4.Name = '张先生Test';
    	patient4.PM_Status__c = 'Active';
    	patient4.PM_PmPhone__c = '18911112222';
    	patient4.PM_ProblemRemarks__c = '备注';
    	patient4.PM_InHospital__c = acc.Id;
    	patient4.PM_HomeTel__c = '18611112222';
    	patient4.PM_PmPhone__c = '18611112222';
    	patient4.PM_PmTel__c = '18611112222';
    	patient4.PM_FamilyPhone__c = '18611112222';
    	patient4.PM_FamilyTel__c = '18611112222';
    	patient4.PM_OtherTel2__c = '18611112222';
    	insert patient4;
    	patient1.PM_Terminal__c = acc3.Id;
    	update patient1;
    }
}