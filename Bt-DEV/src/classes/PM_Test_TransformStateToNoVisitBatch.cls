/*
 * Tobe 
 * 2013.10.25
 * PM_TransformStateToNoVisitBatch测试类
 */
@isTest
global class PM_Test_TransformStateToNoVisitBatch 
{
	static testMethod void myUnitTest() 
    {
        system.Test.startTest(); 
        PM_Test_AllConfig.myUnitTest(); //公用方法，内涵必要的一些测试数据
        PM_TransformStateToNoVisitBatch transform = new PM_TransformStateToNoVisitBatch();
		database.executeBatch(transform,500);
        system.Test.stopTest();    
    }
}