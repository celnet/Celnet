/**
 *Henry 
 *2013-10-11
 *Dropout病人拜访列表页面控制类——测试类
 */
@isTest
private class PM_Test_DropoutpatientDevController {

    static testMethod void myUnitTest() 
    {
        PM_Test_AllConfig.myUnitTest(); //公用方法，内涵必要的一些测试数据
        system.Test.startTest();
        PM_Patient__c patient = new PM_Patient__c();
        ApexPages.StandardController controller = new ApexPages.StandardController(patient);
        //实例化控制类
        PM_DropoutpatientvisitlistDevController dp = new PM_DropoutpatientvisitlistDevController(controller);
        //初始化get部分字段
        ApexPages.StandardSetController conset = dp.conset;
        list<PM_ExportFieldsCommon.Patient> list_Patient = dp.list_Patient;
        dp.first();
        dp.last();
        dp.previous();
        dp.next();
        //查询流程
        list<Profile> list_Profile = [Select Id,Name From Profile Where Name = '系统管理员'];
        User user = new User();
        user.ProfileId = list_Profile[0].Id;
        user.Username = 'asfdasdf@acme.com';
        user.FirstName = 'Test';
        user.LastName = 'Test';
        user.Email = 'asdf@aa.com';
        user.Alias = 'asdf';
        user.EmailEncodingKey = 'UTF-8';
        user.TimeZoneSidKey = 'Asia/Shanghai';
        user.LanguageLocaleKey = 'zh_CN';
        user.LocaleSidKey = 'zh_CN';
        insert user;
        
        dp.Patient.Name = '张先生';
        dp.MobilePhone = '18611112222';
        dp.Patient.PM_Current_Saler__c = user.Id;
        dp.BigArea = '东一区';
        dp.Province = '北京';
        dp.PatientProvince = '北京';
        dp.City = '北京市';
        dp.PatientCity = '北京市';
        dp.County = '海淀区';
        dp.PatientCounty = '海淀区';
        dp.PatientStatus = 'Dropout';
        //dp.RawStatus = 'New';
        dp.DropoutActualDate_Start = '2013-10-09';
        dp.DropoutActualDate_End = '2013-10-09';
        dp.InHospital = '朝阳区第三医院';
        dp.Patient.PM_DropOut_One_Reason__c = '死亡';
        dp.Patient.PM_DropOut_Two_Reason__c = '';

        dp.Check();
        //排序流程
	    dp.strOrder = 'desc';
	    dp.util.patSql = null;
	    dp.sortOrders();
	    dp.sortOrders();
	    dp.getTransfornBrands();
	    //导出
	    dp.exportExcel(); 
	    for(PM_ExportFieldsCommon.Patient pa :dp.list_PatientExport)
	    {
	    	pa.IsExport = true;
	    }
        dp.exportExcel();
        system.Test.stopTest();
    }
}