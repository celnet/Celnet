/**
 * Batch的ClsSalesHospitalRelationBatch的测试类
 */
@isTest
private class Test_Batch_ClsSalesHospitalRelationBatch {

    static testMethod void myUnitTest() {
        //----------------New UserRole ------------------
    	//List<UserRole> list_userRole = new List<UserRole >() ;
    	UserRole objUserRole = new UserRole() ;
    	objUserRole.Name = 'Renal-Rep-大上海-PD-Rep(陈喆令)' ;
    	insert objUserRole ;
    	UserRole objUserRole2 = new UserRole() ;
    	objUserRole2.Name = 'Renal-Rep-大上海-PB-Rep(陈喆令)' ;
    	objUserRole2.ParentRoleId = objUserRole.Id ;
    	insert objUserRole2 ;
    	UserRole objUserRole3 = new UserRole() ;
    	objUserRole3.Name = 'Renal-Rep-大上海-PC-Rep(陈喆令)' ;
    	objUserRole3.ParentRoleId = objUserRole.Id ;
    	insert objUserRole3 ;
		//----------------Create User-------------
    	List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.UserRoleId = objUserRole2.Id ;
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	use2.UserRoleId = objUserRole3.Id ;
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.UserRoleId = objUserRole.Id ;
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
    	
        //客户
        list<Account> list_acc = new list<Account>();
        Account acc = new Account();
        acc.Name = 'beijingyiyaun';
        list_acc.add(acc);
        Account acc1 = new Account();
        acc1.Name = 'beijingyiyaun';
        list_acc.add(acc1);
        insert list_acc;
        
        //销售医院关系
        List<V2_Account_Team__c> list_V2_Account = new List<V2_Account_Team__c>();
        V2_Account_Team__c v2_Account = new V2_Account_Team__c();
        v2_Account.V2_User__c = use1.Id;
        v2_Account.V2_Account__c = acc.Id;
        v2_Account.V2_BatchOperate__c = '替换';
        v2_Account.V2_NewAccUser__c = use3.Id;
        v2_Account.EffectiveDate__c = date.today().addDays(-1);
        v2_Account.NewEffDate__c = date.today().addDays(-1);
        v2_Account.V2_ApprovalStatus__c = '审批通过';
        v2_Account.V2_History__c = false;
        list_V2_Account.add(v2_Account);
        V2_Account_Team__c v2_Account1 = new V2_Account_Team__c();
        v2_Account1.V2_User__c = use2.Id;
        v2_Account1.V2_Account__c = acc1.Id;
        v2_Account1.V2_BatchOperate__c = '删除';
        v2_Account1.DeleteDate__c = date.today().addDays(-1);
        v2_Account1.V2_ApprovalStatus__c = '审批通过';
        v2_Account1.V2_History__c = false;
        list_V2_Account.add(v2_Account1);
        insert list_V2_Account;
        
        List<AccountTeamMember> list_atm = new List<AccountTeamMember>();
        AccountTeamMember atm = new AccountTeamMember();
        atm.UserId = use1.Id;
        atm.AccountId = acc.Id;
        list_atm.add(atm);
        AccountTeamMember atm1 = new AccountTeamMember();
        atm1.UserId = use2.Id;
        atm1.AccountId = acc.Id;
        list_atm.add(atm1);
        insert list_atm;
        
        system.Test.startTest();
        ClsSalesHospitalRelationBatch relationBatch = new ClsSalesHospitalRelationBatch();
        Database.executeBatch(relationBatch, 10);
        system.Test.stopTest();
    }
}