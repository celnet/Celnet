/**
 * Tobe 
 * 2013.10.11
 * PM_DuplicateCheckingEvent Trigger测试类 
 */
@isTest
private class PM_Test_DuplicateCheckingEventTrigger 
{
    static testMethod void myUnitTest() 
    {
    	list<Profile> list_Profile = [Select Id,Name From Profile Where Name = '系统管理员'];
        User user = new User();
        user.ProfileId = list_Profile[0].Id;
        user.Username = 'asfdasdf@acme.com';
        user.FirstName = 'Test';
        user.LastName = 'Test';
        user.Email = 'asdf@aa.com';
        user.Alias = 'asdf';
        user.EmailEncodingKey = 'UTF-8';
        user.TimeZoneSidKey = 'Asia/Shanghai';
        user.LanguageLocaleKey = 'zh_CN';
        user.LocaleSidKey = 'zh_CN';
        insert user;
    	RecordType recordId = [Select r.Id From RecordType r Where r.developername = 'RoutineEvent'];
    	RecordType recordId2 = [Select r.Id From RecordType r Where r.developername = 'V2_RecordType'];
    	Event e1 = new Event();
    	e1.OwnerId = user.Id;
    	e1.RecordTypeId = recordId.Id;
    	e1.StartDateTime = DateTime.now().addHours(-1);
    	e1.EndDateTime = DateTime.now();
    	e1.Done__c = true;
    	e1.Type = '邮件处理';
    	try
    	{
	    	insert e1;
	    }
    	catch(Exception e){}
    	Event e2 = new Event();
    	e2.OwnerId = user.Id;
    	e2.RecordTypeId = recordId.Id;
    	e2.StartDateTime = DateTime.now().addHours(-4);
    	e2.EndDateTime = DateTime.now().addHours(-3);
    	e2.Type = '邮件处理';
    	try
    	{
	    	insert e2;
    	}
	    catch(Exception e){}
	    Event e3 = new Event();
    	e3.OwnerId = user.Id;
    	e3.RecordTypeId = recordId.Id;
    	e3.StartDateTime = DateTime.now().addHours(-1);
    	e3.EndDateTime = DateTime.now();
    	e3.Type = '沟通协调';
	    try
    	{
	    	insert e3;
	    }
	    catch(Exception e){}
	    Event e4 = new Event();
    	e4.OwnerId = user.Id;
    	e4.RecordTypeId = recordId.Id;
    	e4.StartDateTime = DateTime.now().addHours(-1);
    	e4.EndDateTime = DateTime.now();
    	e4.Type = '保存信息';
	   	try
    	{
	    	insert e4;
    	}
	    catch(Exception e){}
	    Event e5 = new Event();
    	e5.OwnerId = user.Id;
    	e5.RecordTypeId = recordId2.Id;
    	e5.StartDateTime = DateTime.now().addHours(-1);
    	e5.EndDateTime = DateTime.now();
    	e5.Type = '邮件处理';
	    try
    	{
	    	insert e5;
	    }
	    catch(Exception e){}
	    Event e6 = new Event();
    	e6.OwnerId = user.Id;
    	e6.RecordTypeId = recordId.Id;
    	e6.StartDateTime = DateTime.now().addHours(-1);
    	e6.EndDateTime = DateTime.now();
    	e6.Type = '邮件处理';
	   	try
    	{
			insert e6;
		}
	    catch(Exception e){}
	    try
    	{
    		e1.Done__c = false;
			update e1;
		}
	    catch(Exception e){}
    	
    }
}