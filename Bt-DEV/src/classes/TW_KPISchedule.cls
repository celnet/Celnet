/*
*功能：用于执行台湾KPI的Batch类---TW_SalesManKPIBatch
*作者：Alisa
*时间：2013/11/29
*/
global class TW_KPISchedule implements Schedulable{
	global void execute (SchedulableContext sc)
	{
		TW_SalesManKPIBatch twKpi = new TW_SalesManKPIBatch();
		twKpi.strYear = String.valueOf(date.today().year());
    	twKpi.strMonth = String.valueOf(date.today().month());
		Database.executeBatch(twKpi,100);
	}
	 //测试schedule类-----TW_KPISchedule
    static testMethod void testKPISchedule() {
        try
    	{
    		Test.startTest();
        	string sch=DateTime.now().second()+' '+DateTime.now().minute()+' '+DateTime.now().hour()+' * * ?';
    		TW_KPISchedule kpiSchedule = new TW_KPISchedule();
    		system.schedule('testKPISchedule',sch,kpiSchedule);
    		Test.stopTest();
    	}
    	catch(Exception e)
    	{
    		
    	}
    }
}