/**
 * author:bill
 * AutoUpdateVaporizerStatus的trigger的测试类
 */
@isTest
private class Test_AutoUpdateVaporizerStatus {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        //挥发罐 
        VaporizerInfo__c vap = new VaporizerInfo__c();
        vap.Status__c = '库存';
        insert vap;
        
        //挥发罐申请
        Vaporizer_Application__c vapApp = new Vaporizer_Application__c();
        insert vapApp;
        
        //挥发罐维修/退回申请
        Vaporizer_ReturnAndMainten__c vapAplyReturn = new Vaporizer_ReturnAndMainten__c();
        insert vapAplyReturn;
        
        //挥发罐申请明细
        Vaporizer_Apply_Detail__c vapApply = new Vaporizer_Apply_Detail__c();
        vapApply.Vaporizer_Application__c = vapApp.Id;
        vapApply.DeliveredAmount__c = 2;
        insert vapApply;
        
        //挥发罐维修/退回申请明细
        Vaporizer_ReturnAndMainten_Detail__c vapAplyReturnDetail = new Vaporizer_ReturnAndMainten_Detail__c();
        vapAplyReturnDetail.Vaporizer_ReturnAndMainten__c = vapAplyReturn.Id;
        //vapAplyReturnDetail.VaporizerInfo__c = vap.Id;
        vapAplyReturnDetail.DeliveredAmount__c = 3;
        insert vapAplyReturnDetail;
        
        //发货明细
        Vaporizer_Application_Detail__c vapApplicationDetail = new Vaporizer_Application_Detail__c();
        vapApplicationDetail.Vaporizer_Application__c = vapApp.Id;
        vapApplicationDetail.Vaporizer_Apply_Detail__c = vapApply.Id;
        vapApplicationDetail.VaporizerInfo__c = vap.Id;
        insert vapApplicationDetail;
        Vaporizer_Application_Detail__c vapApplicationDetail2 = new Vaporizer_Application_Detail__c();
        vapApplicationDetail2.VaporizerInfo__c = vap.Id;
        vapApplicationDetail2.Vaporizer_ReturnAndMainten__c = vapAplyReturn.Id;
        vapApplicationDetail2.Vaporizer_ReturnAndMainten_Detail__c =vapAplyReturnDetail.Id; 
        insert vapApplicationDetail2;
        
        //发货信息
        List<Vaporizer_Shipping__c> list_vapShop = new List<Vaporizer_Shipping__c>();
        Vaporizer_Shipping__c vapShop = new Vaporizer_Shipping__c();
        vapShop.RecordTypeId = [select Id from RecordType r where r.DeveloperName ='DeliverInfo'][0].Id;
        //vapShop.v
        vapShop.Vaporizer_ReturnAndMainten_Detail__c =vapAplyReturnDetail.Id;
        vapShop.Type__c = '销售发货';
        list_vapShop.add(vapShop);
        Vaporizer_Shipping__c vapShop1 = new Vaporizer_Shipping__c();
        vapShop1.RecordTypeId = [select Id from RecordType r where r.DeveloperName ='DeliverInfo'][0].Id;
        vapShop1.Type__c = '工程师发货';
        vapShop1.Vaporizer_Application_Detail__c = vapApplicationDetail.Id;
        list_vapShop.add(vapShop1);
        Vaporizer_Shipping__c vapShop3 = new Vaporizer_Shipping__c();
        vapShop3.RecordTypeId = [select Id from RecordType r where r.DeveloperName ='DeliverInfo'][0].Id;
        vapShop3.Type__c = '工程师发货';
        vapShop3.Vaporizer_Application_Detail__c = vapApplicationDetail2.Id;
        list_vapShop.add(vapShop3);
        Vaporizer_Shipping__c vapShop2 = new Vaporizer_Shipping__c();
        vapShop2.RecordTypeId = [select Id from RecordType r where r.DeveloperName ='ReceiveInfo'][0].Id;
        vapShop2.Type__c = '销售收货';
        vapShop2.Vaporizer_Application_Detail__c = vapApplicationDetail2.Id;
        list_vapShop.add(vapShop2);
        
        test.startTest();
        try{
        insert list_vapShop;
        }catch(Exception e){}
        test.stopTest();
    }
}