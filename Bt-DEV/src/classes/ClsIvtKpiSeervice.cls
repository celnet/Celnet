/**
 * 作者：Sunny
 * IVT KPI
 * 2014-1-14 Sunny ：
 * 1.调整<业务机会相关拜访达成率>，不再考核考核月15日之后创建的业务机会
**/
public class ClsIvtKpiSeervice {
    ID UserId ;
    Integer IntYear ;
    Integer IntMonth ;
    List<Event> list_ValidEvent ;
    Set<ID> set_ValidOppIds ;
    MonthlyPlan__c MonthlyPlan;
    public ClsIvtKpiSeervice(ID sUserId,Integer sIntYear,Integer sIntMonth){
        UserId = sUserId;
        IntYear = sIntYear;
        IntMonth = sIntMonth ; 
    }
    
    //代表KPI
    
    //KPI:业务机会策略评分及建议
    //说明：业务机会策略评分的平均分
    public Map<String , Double> OppEvaluationScore(){
        Map<String , Double> map_Result = new Map<String , Double>();
        //获取进行中的业务机会ID
        if(set_ValidOppIds == null){
            set_ValidOppIds = GetValidOpportunity();
        }
        Set<ID> set_OppId = set_ValidOppIds;
        //策略评分平均分
        Double ScoreAVG = 0 ;
        Double ScoreTotal = 0;
        Double num = 0;
        //获取有效的业务机会策略评分
        for(OppEvaluation__c OppEva : [Select Id,Score__c From OppEvaluation__c Where Opportunity__c in: set_OppId And Score__c != null And Year__c =: String.valueOf(IntYear) And Month__c =: String.valueOf(IntMonth)]){
            ScoreTotal += Double.valueOf(OppEva.Score__c);
            num++;
        }
        if(num != 0){
             ScoreAVG = ScoreTotal/num;
        }
        map_Result.put('TotalScore' , ScoreTotal);
        map_Result.put('AVGScore' , ScoreAVG);
        map_Result.put('Num' , num);
        return map_Result ;

    }
    //进行中的业务机会ID,不包含曾经为进行中的业务机会。
    //2013-3-28  sunny: 不需要再查找  对应月份负责的业务机会,不需要限制关闭日期了。 只看当前业务机会的所有人是查找用户即可。
    private Set<Id> GetValidOpportunity(){
        //Date CheckDate = date.valueOf(IntYear+'-'+IntMonth+'-1');
        Date endMonthDate;
        if(IntMonth == 12){
        	endMonthDate = date.valueOf((IntYear+1)+'-'+'1'+'-1');
        }else{
        	endMonthDate = date.valueOf(IntYear+'-'+(IntMonth+1)+'-1');
        }
        
        
        Set<ID> set_ValidOppId = new Set<ID>();
        //查找用户对应月份负责的业务机会
        //Set<ID> set_OppId = V2_UtilClass.GetOpportunity(IntYear,IntMonth,UserId);
        //system.debug(set_OppId);
        //一直在进行中的业务机会
        List<String> list_OppStage = new List<String>{'搜集信息','甑选机会','沟通/导入理念','发现需求','达成共识','导入产品','业务推进'};
        for(Opportunity opp : [Select Id,Renal_Opp_MGT__c From Opportunity 
                Where StageName in: list_OppStage 
                And (RecordType.DeveloperName = 'IVT' OR RecordType.DeveloperName = 'IVT_Approval')
                And OwnerId =: UserId
                And ApproveStatus__c = '通过'
                And CreatedDate <:endMonthDate]){
            
            set_ValidOppId.add(opp.Id);
            
        }
        
        return set_ValidOppId;
        
    }
    
    
    //KPI:拜访完成率
    //说明：当月完成的拜访且未过期/计划拜访数
    public Map<String , Double> VisitRate(){
        Map<String , Double> map_Result = new Map<String , Double>();
        //获取已完成未过期的拜访
        if(list_ValidEvent==null){
            list_ValidEvent = getValidVisit();
        }
        List<Event> list_Event = list_ValidEvent;
        //已完成未过期拜访数
        Double VisitQty = list_Event.size();
        
        //计划拜访数（当月月计划明细中的所有“计划次数”字段的值之和）
        List<MonthlyPlan__c> monthPlan = [Select Id,V2_TotalCallRecords__c From MonthlyPlan__c Where Year__c =: String.valueOf(IntYear) And Month__c =: String.valueOf(IntMonth) And OwnerId =: UserId Limit 1] ;
        Double PlanQty = 0;
        if(monthPlan.size() > 0){
            PlanQty = monthPlan[0].V2_TotalCallRecords__c;
        }
        
        map_Result.put('Target' , PlanQty);
        map_Result.put('Finish' , VisitQty);
        
        if(PlanQty==null || PlanQty==0){
            map_Result.put('Rate' , 0);
        }else{
            map_Result.put('Rate' , VisitQty/PlanQty );
        }
        return map_Result;
    }
    //获取当月月计划
    private MonthlyPlan__c getMonthlyPlan(){
        List<MonthlyPlan__c> list_MonthlyPlan = [Select m.Year__c, m.OwnerId, m.Month__c, m.Id, m.V2_TotalCallRecords__c, (Select Contact__c From MonthlyPlanDetail__r Where Contact__c != null And Planned_Finished_Calls__c > 0) From MonthlyPlan__c m Where OwnerId =: UserId And Year__c =: String.valueOf(IntYear) And Month__c =: String.valueOf(IntMonth)];
        return list_MonthlyPlan.size()==0?list_MonthlyPlan[0]:null ;
    }
    //获取已完成未过期的拜访
    private List<Event> getValidVisit(){
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        //List<Event> list_Visit=[Select Id,GAPlan__c,WhoId,Who.Name,Who.RecordTypeId,GAExecuteResult__c From Event ];
        List<Event> list_Visit=[Select Id,GAPlan__c,WhoId,GAExecuteResult__c,
        Key_Information__c From Event Where IsChild = false And 
        RecordType.DeveloperName = 'V2_Event' And Done__c = true 
        And V2_IsExpire__c = false And StartDateTime >: CheckStartDateTime 
        And StartDateTime <: CheckEndDateTime And OwnerId =: UserId];
        return list_Visit ;
    }
    
    //KPI:访前计划及访后分析完成率
    //
    //KPI:访前计划及访后分析完成率
    //说明：当月已完成的未过期的拜访中填写访前计划和访后分析的比例((访前计划完成%+访后分析完成%)/2)
    public Map<String , Double> VisitPlanRate(){
        Map<String , Double> map_Result = new Map<String , Double>();
        //拜访次数
        double VisitNum;
        //访前计划次数
        double VisitPlanNum;
        //访后分析次数
        double VisitAnalysisNum;
        //访前计划完成率
        double VisitPlan;
        //访后反洗完成率
        double VisitAnalysis;
        
        //获取已完成未过期的拜访
        if(list_ValidEvent==null){
            list_ValidEvent = getValidVisit();
        }
        List<Event> list_Event = list_ValidEvent;
        
        VisitNum = list_Event.size() ;
        for(Event objEvent : list_Event){
            if(objEvent.GAPlan__c != null){
                VisitPlanNum = (VisitPlanNum==null?0:VisitPlanNum) + 1 ;
            }
            if(objEvent.GAExecuteResult__c != null){
                VisitAnalysisNum = (VisitAnalysisNum==null?0:VisitAnalysisNum) + 1 ;
            }
        }
        
        if(VisitNum != null && VisitPlanNum != null){
            VisitPlan = VisitPlanNum / VisitNum ;
        }
        if(VisitNum != null && VisitAnalysisNum != null){
            VisitAnalysis = VisitAnalysisNum / VisitNum;
        }
        map_Result.put('Target' , VisitNum);
        map_Result.put('Finish Before' , VisitPlanNum);
        map_Result.put('Finish After' , VisitAnalysisNum);
        map_Result.put('Rate' , ((VisitPlan==NULL?0:VisitPlan)+(VisitAnalysis==NULL?0:VisitAnalysis))/2);
        return  map_Result;
        
    }
    
    //KPI:拜访质量评分
    //
    public Map<String , Double> VisitGrade(){
        Map<String , Double> map_Result = new Map<String , Double>();
        //获取已完成未过期的拜访
        if(list_ValidEvent==null){
            list_ValidEvent = getValidVisit();
        }
        List<Event> list_Visit = list_ValidEvent;
        List<ID> list_EventId = new List<ID>();
        for(Event eve : list_Visit){
            list_EventId.add(eve.Id);
        }
        Double Grade = 0;
        Double num = 0;
        for(AssVisitComments__c VisitComm : [Select Id,Grade__c From AssVisitComments__c Where EventId__c in: list_EventId And BeReviewed__c =: UserId And Grade__c != null]){
                Grade += Integer.valueOf(VisitComm.Grade__c);
                num++;
        }
        map_Result.put('TotalSorce' , Grade);
        map_Result.put('TotalNum' , num);
        if(num != 0){
            map_Result.put('AVGSorce' , Grade/num);
        }else{
            map_Result.put('AVGSorce' , 0);
        }
        
        return map_Result;
    }
    
    //KPI:关键信息传递率（3Why的传递）
    //
    public Map<String , Double> Visit3WhyRate(){
        Map<String , Double> map_Result = new Map<String , Double>();
        //获取已完成未过期的拜访
        if(list_ValidEvent==null){
            list_ValidEvent = getValidVisit();
        }
        //已填写关键信息的拜访次数
        Double Visit3WhySum =0;
        //已完成未过期的拜访次数
        Double VisitSum = list_ValidEvent.size() ;
        for(Event objVisit : list_ValidEvent){
            if(objVisit.Key_Information__c != null){
                Visit3WhySum++;
            }
        }
        map_Result.put('Target' , VisitSum);
        map_Result.put('Finish' , Visit3WhySum);
        
        if(VisitSum == null || VisitSum == 0){
            map_Result.put('Rate' , 0);
        }else{
            map_Result.put('Rate' , Visit3WhySum / VisitSum);
        }
        return map_Result;
    }
    
    //KPI:业务机会相关拜访达成率
    //2013-3-28 sunny 修改，该指标所有BU不需要考虑曾经进行中的业务机会只要检查当前进行中的业务机会(IVT部门本来就不计算曾经在进行中的业务机会，所以不需要修改代码)
    public Map<String , Double> OpportunityVisitRate(){
        Map<String , Double> map_Result = new Map<String , Double>();
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        //获取所有有效的业务机会的ID
        if(set_ValidOppIds == null){
            set_ValidOppIds = GetValidOpportunity();
        }
        Set<ID> set_OppId = set_ValidOppIds ;
        //有效跟进的业务机会数量
        Double ValidOppQty = 0;
        //所有进行中的业务机会数量
        //Double OppQty = set_OppId.size();
        Double OppQty = 0;
        system.debug('Opp idssss:'+set_OppId);
        //查找这些业务机会的有效拜访（已完成，未过期）
        //2014-1-14 Sunny 修改，业务机会不再考核 考核月15日之后创建的业务机会
        Date midDay = date.valueOf(IntYear+'-'+IntMonth+'-15');
        for(Opportunity objOpp : [Select Id,
        	(Select StartDateTime, OwnerId, Done__c, V2_IsExpire__c 
        	From Events 
        	Where IsChild = false And RecordType.DeveloperName = 'V2_Event' And Done__c = true And V2_IsExpire__c = false And StartDateTime>:CheckStartDateTime And StartDateTime<:CheckEndDateTime) 
        	From Opportunity o 
        	Where Id in: set_OppId And CreatedDate <: midDay]){
        		system.debug('opp s events:'+objOpp.Id+' --'+objOpp.Events.size());
        	OppQty += 1;
            if(objOpp.Events!=null && objOpp.Events.size() >= 4){
                ValidOppQty++;
            }
        }
        //sunny ：IVT部门不是默认4个，而是所有有效业务机会
        map_Result.put('Target' , OppQty);
        map_Result.put('Finish' , ValidOppQty);
        if(OppQty == 0 || OppQty == null){
            map_Result.put('Rate' , 0);
        }else{
            map_Result.put('Rate' , ValidOppQty / OppQty);
        }
        system.debug('opp visit rate result:'+map_Result);
        return map_Result;
    }
    
    //KPI:市场活动拜访跟进（扣分项）
    //2013-3-31修改：在活动结束日期后的一个月内市场活动的参加成员必须至少有一次回访,如果没有则每个未回访的成员都需要在KPI中扣除一分
    public double CampaignMemberFollow(){
    	//市场活动结束日期范围，为计算奖金月份的上个月。
    	Date CheckDate;
    	if(IntMonth == 1){
    		CheckDate = date.valueOf((IntYear-1)+'-'+12+'-1');
    	}else{
    		CheckDate = date.valueOf(IntYear+'-'+(IntMonth-1)+'-1');
    	}
        
        Date endMonthDate = CheckDate.addMonths(1);
        
        Set<ID> list_ContactId = new Set<ID>();
        List<ID> List_CampaignId = new List<ID>();
        for(CampaignMember cm:[Select Id,ContactId,CampaignId From CampaignMember Where V2_Participated__c=true And User__c =: UserId And Campaign.EndDate >=: CheckDate And Campaign.EndDate <: endMonthDate And Campaign.Campaign_type__c != '其他']){
            list_ContactId.add(cm.ContactId);
            
            List_CampaignId.add(cm.CampaignId);
        }
        Set<ID> set_ContactID = new Set<ID>();
        for(Event e : [Select Id,WhoId From Event Where IsChild = false And RecordType.DeveloperName = 'V2_Event' And WhoId in: list_ContactId And WhatId in: List_CampaignId And OwnerId =:UserId And Done__c = true]){
            set_ContactID.add(e.WhoId);
        }
        if(list_ContactId.size() - set_ContactID.size() > 20){
            return 20 ;
        }else{
            return list_ContactId.size() - set_ContactID.size() ;
        }
    }
    
    
    //主管KPI
    
    //直接管理的销售团队的平均SEP业绩
    public Map<String , Double> SepTeamPerformanceAVG(Boolean IsHaveHospital , Double SuperValue){
        Map<String , Double> map_Result = new Map<String , Double>();
        Double AVGGrade = 0;
        //直接汇报关系的下属
        List<ID> list_uId = new List<ID>();
        //查询所有在查询月份直接汇报下属
        for(V2_RoleHistory__c RoleHistory : [Select v.Role__c, v.Manager__c, v.Renal_valid_super__c, v.Name__c, v.IsOnHoliday__c, v.IsLeave__c, v.Department__c, v.PG__c, v.Level__c
            From V2_RoleHistory__c v 
            Where Month__c =: String.valueOf(intMonth) And Year__c =: String.valueOf(intYear) And IsOnHoliday__c = false And IsLeave__c = false And Status__c != '停用' And Manager__c=:UserId]){
            list_uId.add(RoleHistory.Name__c);
        }
        //下属人数
        Double TotalNum = 0 ;
        //下属得分
        Double TotalGrade = 0 ;
        //直接下属的奖金评分
        //2013-4-23 Sunny修改bug，查找奖金数据添加年月的条件
        for(Bonus_data__c bd:[Select Id,Total_Score__c From Bonus_data__c Where The_User__c in: list_uId And Month__c =: String.valueOf(intMonth) And Year__c =: String.valueOf(intYear)]){
            TotalNum++;
            
            TotalGrade = TotalGrade + (bd.Total_Score__c==null?0:bd.Total_Score__c);
        }
        system.debug(IsHaveHospital+'xiashu'+TotalGrade+' - '+TotalNum);
        if(IsHaveHospital == true){
            TotalNum+=1;
            TotalGrade += (SuperValue==null?0:SuperValue) ;
        }
        if(TotalNum!=0){
            AVGGrade = TotalGrade / TotalNum;
        }
        map_Result.put('TotalGrade' , TotalGrade);
        map_Result.put('TotalNum' , TotalNum);
        map_Result.put('AVGGrade' , AVGGrade);
        
        system.debug('pingjunfen'+map_Result);
        return map_Result ;
    }
    
    
    //KPI:协访数量
    //当月主管协访的已完成的，填写评语或者接收协访
    public Map<String , Double> AssVisitQuantity(){
        Map<String , Double> map_Result = new Map<String , Double>();
        Set<ID> list_AssVisitIds = getAssVisit();
        map_Result.put('Target' , 24);
        map_Result.put('Finish' , list_AssVisitIds.size());
        return map_Result ;
    }
    //获取有效的协访，部门要求接受了协访或者填写过评语就是有效协访
    private Set<ID> getAssVisit(){
        //拜访的起止时间
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        //有效协访ID
        Set<ID> Set_AssVisitId = new Set<ID>();
        //接受邀请的协访ID
        Set<ID> set_EventId = new Set<ID>();
        //收到邀请，但未接受的协访ID
        Set<ID> set_NotAccEvent = new Set<ID>();
        for(EventRelation er : [Select EventId,Status From EventRelation Where IsInvitee = true And Event.RecordType.DeveloperName = 'V2_Event' And Event.Done__c = true And RelationId =: UserId And Event.StartDateTime >: CheckStartDateTime And Event.StartDateTime <: CheckEndDateTime]){
            if(er.Status=='已接受' || er.Status=='Accepted'){
                set_EventId.add(er.EventId) ;
            }else{
                set_NotAccEvent.add(er.EventId);
            }
        }
        //sunny:不能查找该月所有完成的拜访，否则会超限。所以使用时间来限制。查找月份-查找月份后一个月之间内创建的评价，而且填写评价的拜访是属于查找月份的拜访
        //找到该月份的点评拜访ID
        //List<ID> list_DoneEventId = new List<ID>();
        //找到该月已完成的拜访，
        //for(Event DoneEvent : getValidDoneVisit()){
        //  list_DoneEventId.add(DoneEvent.Id);
        //}
        //找到其中我填写了点评的拜访
        Date startDate = date.valueOf(IntYear+'-'+IntMonth+'-1');
        Date endDate = startDate.addMonths(2);
        
        Set<ID> set_UEventId = new Set<ID>();
        for(AssVisitComments__c AssVisitComm : [Select Id,EventId__c,IsAssVisit__c From AssVisitComments__c Where CreatedDate >=: startDate And CreatedDate <: endDate And ReUser__c =: UserId]){
            //Set_AssVisitId.add(AssVisitComm.EventId__c);
            //如果填写了点评，协访是收到邀请，但未接受的，是为有效协访
            if(set_NotAccEvent.contains(AssVisitComm.EventId__c) ){
                Set_AssVisitId.add(AssVisitComm.EventId__c);
            }else if(AssVisitComm.IsAssVisit__c){
                //勾选了 “是否接受协访” 按钮的点评,需要进一步判断协访的日期。
                set_UEventId.add(AssVisitComm.EventId__c);
            }
            
        }
        for(Event e: [Select Id,GAPlan__c,WhoId,GAExecuteResult__c,Key_Information__c From Event Where Id in: set_UEventId And RecordType.DeveloperName = 'V2_Event' And Done__c = true And StartDateTime >: CheckStartDateTime And StartDateTime <: CheckEndDateTime]){
            Set_AssVisitId.add(e.Id);
        }
        Set_AssVisitId.addAll(set_EventId);
        return Set_AssVisitId;
    }
    /*
    //获取该月已完成的拜访（不限制是否过期，不限制是我的拜访）
    private List<Event> getValidDoneVisit(){
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        //List<Event> list_Visit=[Select Id,GAPlan__c,WhoId,Who.Name,Who.RecordTypeId,GAExecuteResult__c From Event ];
        List<Event> list_Visit=[Select Id,GAPlan__c,WhoId,GAExecuteResult__c,Key_Information__c From Event Where RecordType.DeveloperName = 'V2_Event' And Done__c = true And StartDateTime >: CheckStartDateTime And StartDateTime <: CheckEndDateTime];
        return list_Visit ;
    }
    */
    //KPI:业务机会策略评分及评语完成率
    //主管对代表当月进行中的业务机会评分并评语的比例；
    //已经被评分评语的代表当月进行中的业务机会数/代表当月进行中的业务机会总数；
    public Map<String , Double> OppEvaluationRate(){
        Map<String , Double> map_Result = new Map<String , Double>();
        Date endCheckDate ;
        if(IntMonth == 12){
        	endCheckDate = date.valueOf((IntYear+1)+'-'+'1'+'-1');
        }else{
        	endCheckDate = date.valueOf(IntYear+'-'+(IntMonth+1)+'-1');
        }
        
        Set<ID> set_OppId = new Set<ID>();
        //获取直接下属ID
        Set<ID> list_uid = new Set<ID>();
        for(V2_RoleHistory__c RoleHistory : [Select v.Role__c, v.Manager__c, v.Renal_valid_super__c, v.Name__c, v.IsOnHoliday__c, v.IsLeave__c, v.Department__c, v.PG__c, v.Level__c
            From V2_RoleHistory__c v 
            Where Month__c =: String.valueOf(intMonth) And Year__c =: String.valueOf(intYear) And IsOnHoliday__c = false And IsLeave__c = false And Manager__c=:UserId]){
            list_uId.add(RoleHistory.Name__c);
        }
        //下属对应月份负责的业务机会
        //Set<ID> set_OppIds = V2_UtilClass.GetOpportunity(IntYear,IntMonth,list_uid);
        //进行中的业务机会阶段
        
        List<String> list_OppStage = new List<String>{'搜集信息','甑选机会','沟通/导入理念','发现需求','达成共识','导入产品','业务推进'};
        for(Opportunity opp : [Select Id,Renal_Opp_MGT__c From Opportunity 
                Where StageName in: list_OppStage 
                And (RecordType.DeveloperName = 'IVT' OR RecordType.DeveloperName = 'IVT_Approval')
                And OwnerId in: list_uId
                And ApproveStatus__c = '通过'
                And CreatedDate <: endCheckDate]){
            
            set_OppId.add(opp.Id);
            
        }
        //
        Set<ID> set_eids = new Set<ID>();
        for(OppEvaluation__c oppEva: [Select Id,Score__c,Opportunity__c From OppEvaluation__c Where Opportunity__c in: set_OppId And Score__c!= null And Commentator__c =: UserId And Year__c =: String.valueOf(IntYear) And Month__c =: String.valueOf(IntMonth)]){
        	set_eids.add(oppEva.Opportunity__c);
        }
        system.debug('opp shuliang:'+set_eids.size());
        map_Result.put('Target' , set_OppId.size());
        map_Result.put('Finish' , set_eids.size());
        if(set_OppId.size() == 0){
            map_Result.put('Rate' , 0);
        }else{
            map_Result.put('Rate' , Double.valueOf(set_eids.size()) / set_OppId.size());
        }
        return  map_Result; 
    }
    
    //KPI:业务机会策略评估质量(二级评分)
    //由二级经理对主管业务机会管理能力的整体评分;
    public Map<String , Double> ScoreFinishRate(){
        Map<String , Double> map_Result = new Map<String , Double>();
        double score = 0;
        list<OppEvaluation__c> list_OppEvaluation = [Select o.Score__c From OppEvaluation__c o where Year__c =: string.valueOf(IntYear) and Month__c =:string.valueOf(IntMonth) and BeCommentUser__c =:UserId and Score__c!=null];
        if(list_OppEvaluation.size()>0){
            for(OppEvaluation__c opp:list_OppEvaluation){
                score += double.valueOf(opp.Score__c);
            }
        }
        map_Result.put('TotalScore' , score);
        map_Result.put('TotalNum' , list_OppEvaluation.size());
        map_Result.put('AVGScore' , (score>0?(score/list_OppEvaluation.size()):0));
        return map_Result;
    }
    
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        //user role
        List<UserRole> listur= new List<UserRole>();
        UserRole ur1 = new UserRole();
        ur1.Name = 'IVT-Supervisor-华南-PD-Supervisor(U1)';
        listur.add(ur1);
        UserRole ur2 = new UserRole();
        ur2.Name = 'Renal-Rep-华南-PD-Rep(U2)';
        listur.add(ur2);
        insert listur;
        //user 
        list<User> user=[Select TimeZoneSidKey,ProfileId,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname, Alias From User Where IsActive = true and id =:userInfo.getUserId()];
        List<User> list_user = new List<User>();
        User user1=new User();
        user1.username='user1r2@123.com';
        user1.LastName='user1r2';
        user1.Email='user1r2@123.com';
        user1.Alias=user[0].Alias;
        user1.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user1.ProfileId=UserInfo.getProfileId();
        user1.LocaleSidKey=user[0].LocaleSidKey;
        user1.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user1.EmailEncodingKey=user[0].EmailEncodingKey;
        user1.CommunityNickname='chequ';
        user1.MobilePhone='12345678912';
        user1.IsActive = true;
        user1.UserRoleId=ur1.Id ;
        list_user.add(user1);
        User user2=new User();
        user2.username='user2r2@223.com';
        user2.LastName='user2r2';
        user2.Email='user2r2@223.com';
        user2.Alias=user[0].Alias;
        user2.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user2.ProfileId=UserInfo.getProfileId();
        user2.LocaleSidKey=user[0].LocaleSidKey;
        user2.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user2.EmailEncodingKey=user[0].EmailEncodingKey;
        user2.CommunityNickname='chequ1';
        user2.MobilePhone='22345678922';
        user2.IsActive = true;
        user1.UserRoleId=ur1.Id ;
        list_user.add(user2);
        insert list_user;
        
        //联系人
        List<Contact> listct= new List<Contact>();
        Contact ct1 = new Contact();
        ct1.FirstName = 'jim';
        ct1.LastName = 'Green';
        ct1.GISContactType__c ='关键客户';
        listct.add(ct1);
        Contact ct2 = new Contact();
        ct2.FirstName = 'Tom';
        ct2.LastName = 'Frank';
        ct2.GISContactType__c ='普通客户';
        listct.add(ct2);
        insert listct;
        
        //业务机会
        List<Opportunity> List_Opportunity = new List<Opportunity>();
        Opportunity opp = new Opportunity();
        opp.OwnerId = user1.Id;
        opp.Name = 'yewujihui';
        opp.StageName = '建立沟通渠道';
        opp.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='RENAL' And SobjectType = 'Opportunity'][0].Id;
        opp.CloseDate = date.valueOf(Datetime.now().year()+'-'+Datetime.now().month()+'-1');
        opp.Renal_Opp_MGT__c = '接受';
        List_Opportunity.add(opp);
        Opportunity opp1 = new Opportunity();
        opp1.OwnerId = user1.Id;
        opp1.Name = 'yewujihui1';
        opp1.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='RENAL' And SobjectType = 'Opportunity'][0].Id;
        opp1.StageName = '建立沟通渠道';
        opp1.CloseDate = date.valueOf(Datetime.now().year()+'-'+Datetime.now().month()+'-1');
        opp1.Renal_Opp_MGT__c = '接受';
        List_Opportunity.add(opp1);
        Opportunity opp2 = new Opportunity();
        opp2.OwnerId = user1.Id;
        opp2.Name = 'yewujihui2';
        opp2.StageName = '发现需求';
        opp2.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='IVT' And SobjectType = 'Opportunity'].Id;
        opp2.CloseDate = date.valueOf(Datetime.now().year()+'-'+Datetime.now().month()+'-1');
        opp2.Renal_Opp_MGT__c = '接受';
        List_Opportunity.add(opp2);
        insert List_Opportunity;
        
        //业务机会历史记录
        List<OpportunityHistory__c> List_OpportunityHistory = new List<OpportunityHistory__c>();
        OpportunityHistory__c oppc = new OpportunityHistory__c();
        oppc.ChangedDate__c = date.valueOf(Datetime.now().year()+'-'+Datetime.now().month()+'-2');
        oppc.NewOppStage__c ='需求分析';
        oppc.PastOppStage__c = '提交合作方案/谈判';
        oppc.Opportunity__c = opp.Id;
        List_OpportunityHistory.add(oppc);
        insert List_OpportunityHistory;
        
        //业务机会策略评估
        List<OppEvaluation__c> List_OppEvaluation = new list<OppEvaluation__c>();
        OppEvaluation__c oppe = new OppEvaluation__c();
        oppe.BeCommentUser__c = user1.Id;
        oppe.Opportunity__c = opp2.Id;
        oppe.Score__c = '4';
        oppe.Year__c = string.valueOf(Datetime.now().year());
        oppe.Month__c = string.valueOf(Datetime.now().month());
        List_OppEvaluation.add(oppe);
        OppEvaluation__c oppe1 = new OppEvaluation__c();
        oppe1.Opportunity__c = opp2.Id;
        oppe1.BeCommentUser__c = user1.Id;
        oppe1.Score__c = '3';
        oppe1.Year__c = string.valueOf(Datetime.now().year());
        oppe1.Month__c = string.valueOf(Datetime.now().month());
        List_OppEvaluation.add(oppe1);
        OppEvaluation__c oppe2 = new OppEvaluation__c();
        oppe2.Opportunity__c = opp2.Id;
        oppe2.BeCommentUser__c = user1.Id;
        oppe2.Score__c = '5';
        oppe2.Year__c = string.valueOf(Datetime.now().year());
        oppe2.Month__c = string.valueOf(Datetime.now().month());
        List_OppEvaluation.add(oppe2);
        insert List_OppEvaluation;
        
        //活动
        List<Event> list_Visit = new List<Event>();
        Event ev1 = new Event();
        ev1.DurationInMinutes = 1;
        ev1.StartDateTime = datetime.now();
        ev1.OwnerId = user1.Id ;
        ev1.GAPlan__c = '1111111';
        ev1.GAExecuteResult__c = '111111';
        ev1.Key_Information__c ='静脉输液安全非常重要，关系到医疗质量，关系到医患安全及病人满意度。';
        ev1.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='V2_Event' And SobjectType = 'Event'][0].Id;
        ev1.Done__c = true;
        ev1.V2_IsExpire__c = false; 
        ev1.WhoId = ct1.Id;
        ev1.WhatId = opp.Id;
        list_Visit.add(ev1);
        Event ev2 = new Event();
        ev2.DurationInMinutes = 2;
        ev2.StartDateTime = datetime.now();
        ev2.OwnerId = user1.Id ;
        ev2.GAPlan__c = '222222';
        ev2.GAExecuteResult__c = '222222';
        ev2.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='V2_Event' And SobjectType = 'Event'][0].Id;
        ev2.Done__c = true;
        ev2.V2_IsExpire__c = false; 
        ev2.WhoId = ct1.Id;
        ev2.WhatId = opp.Id;
        ev2.Key_Information__c ='静脉输液安全非常重要，关系到医疗质量，关系到医患安全及病人满意度。';
        list_Visit.add(ev2);
        Event ev3 = new Event();
        ev3.DurationInMinutes = 3;
        ev3.StartDateTime = datetime.now();
        ev3.OwnerId = user1.Id ;
        ev3.GAPlan__c = '333333';
        ev3.GAExecuteResult__c = '3333333';
        ev3.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='V2_Event' And SobjectType = 'Event'][0].Id;
        ev3.Done__c = true;
        ev3.V2_IsExpire__c = false; 
        ev3.WhoId = ct1.Id;
        ev3.WhatId = opp.Id;
        list_Visit.add(ev3);
        Event ev4 = new Event();
        ev4.DurationInMinutes = 4;
        ev4.StartDateTime = datetime.now();
        ev4.OwnerId = user1.Id ;
        ev4.GAPlan__c = '333333';
        //ev4.GAExecuteResult__c = '3333333';
        ev4.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='V2_Event' And SobjectType = 'Event'][0].Id;
        ev4.Done__c = true;
        ev4.V2_IsExpire__c = false; 
        ev4.WhoId = ct2.Id;
        ev4.WhatId = opp.Id;
        list_Visit.add(ev4);
        Event ev5 = new Event();
        ev5.DurationInMinutes = 1;
        ev5.StartDateTime = datetime.now();
        ev5.OwnerId = user1.Id ;
        ev5.GAPlan__c = '1111111';
        ev5.GAExecuteResult__c = '111111';
        ev5.Key_Information__c ='静脉输液安全非常重要，关系到医疗质量，关系到医患安全及病人满意度。';
        ev5.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='V2_Event' And SobjectType = 'Event'][0].Id;
        ev5.Done__c = true;
        ev5.V2_IsExpire__c = false; 
        ev5.WhoId = ct1.Id;
        ev5.WhatId = opp2.Id;
        list_Visit.add(ev5);
        Event ev6 = new Event();
        ev6.DurationInMinutes = 2;
        ev6.StartDateTime = datetime.now();
        ev6.OwnerId = user1.Id ;
        ev6.GAPlan__c = '222222';
        ev6.GAExecuteResult__c = '222222';
        ev6.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='V2_Event' And SobjectType = 'Event'][0].Id;
        ev6.Done__c = true;
        ev6.V2_IsExpire__c = false; 
        ev6.WhoId = ct1.Id;
        ev6.WhatId = opp2.Id;
        ev6.Key_Information__c ='静脉输液安全非常重要，关系到医疗质量，关系到医患安全及病人满意度。';
        list_Visit.add(ev6);
        Event ev7 = new Event();
        ev7.DurationInMinutes = 3;
        ev7.StartDateTime = datetime.now();
        ev7.OwnerId = user1.Id ;
        ev7.GAPlan__c = '333333';
        ev7.GAExecuteResult__c = '3333333';
        ev7.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='V2_Event' And SobjectType = 'Event'][0].Id;
        ev7.Done__c = true;
        ev7.V2_IsExpire__c = false; 
        ev7.WhoId = ct1.Id;
        ev7.WhatId = opp2.Id;
        list_Visit.add(ev7);
        Event ev8 = new Event();
        ev8.DurationInMinutes = 4;
        ev8.StartDateTime = datetime.now();
        ev8.OwnerId = user1.Id ;
        ev8.GAPlan__c = '333333';
        //ev8.GAExecuteResult__c = '3333333';
        ev8.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='V2_Event' And SobjectType = 'Event'][0].Id;
        ev8.Done__c = true;
        ev4.V2_IsExpire__c = false; 
        ev8.WhoId = ct2.Id;
        ev8.WhatId = opp2.Id;
        list_Visit.add(ev8);
        insert list_Visit ;
        
        //协防事件
        //List<EventAttendee> list_EventAttendee = new List<EventAttendee>();
        //EventAttendee eva = new EventAttendee();
        //eva.EventId = ev1.Id;
        //eva.AttendeeId = user1.Id;
        //eva.Status = '已接受';
        //list_EventAttendee.add(eva);
        //insert list_EventAttendee;
        
        //月计划
        List<MonthlyPlan__c> List_MonthlyPlan = new List<MonthlyPlan__c>();
        MonthlyPlan__c mc1 = new MonthlyPlan__c();
        mc1.OwnerId = user1.Id;
        mc1.Year__c = string.valueOf(datetime.now().year());
        mc1.Month__c = string.valueOf(datetime.now().month());
        //mc1.V2_TotalCallRecords__c = 10;
        List_MonthlyPlan.add(mc1);
        insert List_MonthlyPlan;
        
        //月计划明细
        List<MonthlyPlanDetail__c> List_MonthlyPlanDetail = new List<MonthlyPlanDetail__c>();
        MonthlyPlanDetail__c mcc1 = new MonthlyPlanDetail__c();
        mcc1.Contact__c = ct1.Id;
        mcc1.Planned_Finished_Calls__c = 5;
        mcc1.MonthlyPlan__c = mc1.id;
        mcc1.AdjustedTimes__c = 10;
        List_MonthlyPlanDetail.add(mcc1);
        MonthlyPlanDetail__c mcc2 = new MonthlyPlanDetail__c();
        mcc2.Contact__c = ct1.Id;
        mcc2.Planned_Finished_Calls__c = 5;
        mcc2.MonthlyPlan__c = mc1.Id;
        mcc2.AdjustedTimes__c = 10;
        List_MonthlyPlanDetail.add(mcc2);
        MonthlyPlanDetail__c mcc3 = new MonthlyPlanDetail__c();
        mcc3.Contact__c = ct1.Id;
        mcc3.Planned_Finished_Calls__c = 10;
        mcc3.MonthlyPlan__c = mc1.Id;
        mcc3.AdjustedTimes__c = 10;
        List_MonthlyPlanDetail.add(mcc3);
        insert List_MonthlyPlanDetail;
        
        //协访质量点评
        List<AssVisitComments__c> List_AssVisitComments = new List<AssVisitComments__c>();
        AssVisitComments__c ac = new AssVisitComments__c();
        ac.EventId__c = ev1.Id;
        ac.BeReviewed__c = user1.Id;
        ac.Grade__c = '90';
        List_AssVisitComments.add(ac);
        AssVisitComments__c ac1 = new AssVisitComments__c();
        ac1.EventId__c = ev2.Id;
        ac1.BeReviewed__c = user1.Id;
        ac1.Grade__c = '90';
        List_AssVisitComments.add(ac1);
        AssVisitComments__c ac2 = new AssVisitComments__c();
        ac2.EventId__c = ev3.Id;
        ac2.BeReviewed__c = user1.Id;
        ac2.Grade__c = '90';
        List_AssVisitComments.add(ac2);
        AssVisitComments__c ac3 = new AssVisitComments__c();
        ac3.EventId__c = ev4.Id;
        ac3.BeReviewed__c = user1.Id;
        ac3.Grade__c = '90';
        List_AssVisitComments.add(ac3);
        insert List_AssVisitComments;
        
        system.test.startTest();
        ClsIvtKpiSeervice cls = new ClsIvtKpiSeervice(user1.Id , datetime.now().year() , datetime.now().month());
        //代表KPI
        Map<String , Double> a = cls.OppEvaluationScore();
        Map<String , Double> b = cls.VisitRate();
        Map<String , Double> c = cls.VisitPlanRate();
        Map<String , Double> d = cls.VisitGrade();
        Map<String , Double> e = cls.Visit3WhyRate();
        Map<String , Double> f = cls.OpportunityVisitRate();
        //主管KPI
         Map<String , Double> g = cls.SepTeamPerformanceAVG(true , 90);
        Map<String , Double> h = cls.AssVisitQuantity();
        Map<String , Double> i = cls.OppEvaluationRate();
        Map<String , Double>  k = cls.ScoreFinishRate();
        system.debug(a+'@@'+b+'@@'+c+'@@'+d);
        system.debug(e+'@@'+f+'@@'+h+'@@'+i);
        system.debug(k+'@@');
        system.test.stopTest();
     }
}