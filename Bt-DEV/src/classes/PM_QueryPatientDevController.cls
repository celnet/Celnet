/*
 * 作者：Ziyue
 * 时间：2013-8-30
 * 功能描述：病人查询页面
*/
public class PM_QueryPatientDevController 
{
    //初始页面显示列表的查询条件 
    private static final string query = 'select Id From PM_Patient__c Where Name = \'null\'';
    public long Limit_Size
    {
    	get
    	{
    		return PM_QueryUtil.LIMIT_SIZE;
    	}
    	set;
    }
    //病人对象
    public PM_Patient__c Patient{get;set;}//姓名(Patient.Name)，医院  (Patient.PM_InHospital__c)
    public string BigArea{get;set;}//插管大区
    public string Province{get;set;}//插管省份
    public string City{get;set;}//插管城市
    public string County{get;set;}//插管县
    public string PatientProvince{get;set;}//病人省份
    public string PatientCity{get;set;}//病人城市
    public string PatientCounty{get;set;}//病人县
    public string PatientStatus{get;set;}//病人状态
    public string InHospital{get;set;}//插管医院
    public string CreatedDate_Start{get;set;}//病人创建日期开始
    public string CreatedDate_End{get;set;}//病人创建日期截止
    public string InDate_Start{get;set;}//插管日期开始
    public string InDate_End{get;set;}//插管日期结束
    public string DropDate_Start{get;set;}//掉队日期开始
    public string DropDate_End{get;set;}//掉队日期结束
    public string DropActDate_Start{get;set;}//掉队实际日期开始
    public string DropActDate_End{get;set;}//掉队实际日期结束
    public string MobilePhone{get;set;}//联系电话
    public string TeDistributors{get;set;}//终端配送商
    private PM_QueryUtil util = new PM_QueryUtil();//查询工具类
    
    public string TeDistributors_Fuzzy{set;get;}
    public string InHospital_Fuzzy{set;get;}
    public string TeDistributorsProvince{get;set;}//终端配送商省份
    public string TeDistributorsCity{get;set;}//终端配送商城市
    public string TeDistributorsCounty{get;set;}//终端配送商县
    public string TransfornBrand{get;set;}
    public string ToDrFofive2{get;set;}
    public string TransfornCreatedDate_Start{set;get;}//转竞品后掉队创建日期
	public string TransfornCreatedDate_End{set;get;}
	public string TransfornActDate_Start{set;get;}//转竞品后掉队实际日期
	public string TransfornActDate_End{set;get;}
	
	public string TransfornDate_Start{set;get;}//实际转归日期 PM_TransfornDate__c
	public string TransfornDate_End{set;get;}
	public string TransfornCreateDate_Start{set;get;}//转归创建日期PM_TransfornCreateDate__c
	public string TransfornCreateDate_End{set;get;}
	public string UnreachableDate_Start{set;get;}//Unreachable创建日期 PM_UnreachableDate__c
	public string UnreachableDate_End{set;get;}
	public string PM_Terminal{set;get;}
	
	public string BestVisitTime{set;get;} //最佳拜访时间
	public string Age_Lower{set;get;} //年龄下限
	public string Age_Upper{set;get;} //年龄上限
	
	public string Visit_Interval{set;get;}//要求拜访隔天数
	public List<SelectOption> getVisit_Intervals() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('--无--','--无--'));
            options.add(new SelectOption('空','空'));
            options.add(new SelectOption('小于31天','小于31天'));
            options.add(new SelectOption('31-60天','31-60天'));
            options.add(new SelectOption('61-90天','61-90天'));
            options.add(new SelectOption('91-180天','91-180天'));
            options.add(new SelectOption('大于180天','大于180天'));
            return options;
    }   
	
	
    public List<SelectOption> getTransfornBrands() {
            List<SelectOption> options = new List<SelectOption>();
            Schema.DescribeFieldResult payList = PM_Patient__c.PM_TransformBrand__c.getDescribe();
            List<Schema.PicklistEntry> ListPay = payList.getPicklistValues();
            options.add(new SelectOption('--无--','--无--'));
            for(Schema.PicklistEntry sp : ListPay)
            {
                options.add(new SelectOption(sp.getValue(),sp.getValue()));
            }
            return options;
    }       
    public list<PM_ExportFieldsCommon.Patient> list_Patient//经筛选查询出来的信息
    {
        get
        {
            list_Patient = new list<PM_ExportFieldsCommon.Patient>();
            for(PM_Patient__c c : (list<PM_Patient__c>)conset.getRecords())
            {
                PM_ExportFieldsCommon.Patient pa = new PM_ExportFieldsCommon.Patient();
                pa.patient = c;
                pa.IsExport = false;
                list_patient.add(pa);  
            }   
            list_PatientExport.addAll(list_patient);
            result = list_patient.size();
            return list_patient;
        }
        set;
    }
    //构造器
    public PM_QueryPatientDevController(ApexPages.StandardController controller)
    {
        Patient = new PM_Patient__c();
        //初始化导出所需要的字段
        InitFieldsSet();
    }
    // 分页字段
    public integer result{get;set;}//结果个数
    public Boolean hasPrevious {get {return conset.getHasPrevious();}set;}
    public Boolean hasNext {get {return conset.getHasNext();}set;}
    public Integer pageNumber {get {return conset.getPageNumber();}set;}
    public Integer categoryNumber {get {return conset.getResultSize();}set;}
    public ApexPages.StandardSetController conset //实现分页功能 ApexPages.StandardSetController
    {
        get 
        {
            if(conset == null)
            {
                conset = util.getSortConset(query);
                conset.setPageSize(PM_QueryUtil.pageSize);
            }
            return conset;
        }
        set;
    }
    //分页方法
    public void first() {conset.first();}
    public void last() {conset.last();}
    public void previous() {conset.previous();}
    public void next() {conset.next();}
    //查询方法
    public void Check()
    {
        //Select field...
        string field = ' PM_Pdtreatment__c,PM_PatientNo__c,Name,PM_Address__c,PM_Province__c,PM_City__c,PM_InHospital__c,PM_InHospital__r.PM_Province__c,PM_Current_Saler__c,PM_Current_PSR__c,PM_Distributor__c,PM_Status__c ';
        map<string,string> map_Condition = new map<string,string>();
        map_Condition.put('PM_PatientNo__c',Patient.PM_PatientNo__c);
        map_Condition.put('Name',Patient.Name);
        map_Condition.put('BigArea',BigArea);
        map_Condition.put('Province',Province);
        map_Condition.put('PatientProvince',PatientProvince);
        map_Condition.put('City',City);
        map_Condition.put('PatientCity',PatientCity);
        map_Condition.put('County',County);
        map_Condition.put('PM_InHospital__c',Patient.PM_InHospital__c);
        map_Condition.put('PatientCounty',PatientCounty);
        map_Condition.put('PatientStatus',PatientStatus);
        map_Condition.put('CreatedDate_Start',CreatedDate_Start);
        map_Condition.put('CreatedDate_End',CreatedDate_End);
        map_Condition.put('InDate_Start',InDate_Start);
        map_Condition.put('InDate_End',InDate_End);
        map_Condition.put('DropOutCreatedDate_Start',DropDate_Start);
        map_Condition.put('DropOutCreatedDate_End',DropDate_End);
        map_Condition.put('DropActDate_Start',DropActDate_Start);
        map_Condition.put('DropActDate_End',DropActDate_End);
        map_Condition.put('PM_Sex__c',Patient.PM_Sex__c);
        map_Condition.put('PM_IdNumber__c',Patient.PM_IdNumber__c);
        map_Condition.put('PM_InUser__c',Patient.PM_InUser__c);
        map_Condition.put('PM_Protopathy__c',Patient.PM_Protopathy__c);
        map_Condition.put('PM_LiParalysis__c',Patient.PM_LiParalysis__c);
        map_Condition.put('PM_ReCommunication__c',Patient.PM_ReCommunication__c);
        map_Condition.put('PM_EcDifficulties__c',Patient.PM_EcDifficulties__c);
        map_Condition.put('PM_WhTransplants__c',Patient.PM_WhTransplants__c);
        map_Condition.put('PM_Optimistic__c',Patient.PM_Optimistic__c);
        map_Condition.put('PM_Talkative__c',Patient.PM_Talkative__c);
        map_Condition.put('PM_Pdtreatment__c',Patient.PM_Pdtreatment__c);
        map_Condition.put('PM_Heatingmode__c',Patient.PM_Heatingmode__c);
        map_Condition.put('PM_Distributor__c',Patient.PM_Distributor__c);
        map_Condition.put('MobilePhone',MobilePhone);
        map_Condition.put('TeDistributors',TeDistributors);
        map_Condition.put('PM_Address__c',Patient.PM_Address__c);
        map_Condition.put('InHospital',InHospital);
        map_Condition.put('PM_Terminal__c',PM_Terminal);
        map_Condition.put('PM_InHospital__r.Provinces__r.Name',patient.PM_InHospital__r.Provinces__r.Name);
        map_Condition.put('TeDistributorsProvince',TeDistributorsProvince);
        map_Condition.put('TeDistributorsCity',TeDistributorsCity);
        map_Condition.put('TeDistributorsCounty',TeDistributorsCounty);
        /*******************基本信息************************/
        if(Patient.PM_Birthday__c != null )
        {
            map_Condition.put('PM_Birthday__c',string.valueOf(Patient.PM_Birthday__c));
        }
        map_Condition.put('PM_Sex__c',Patient.PM_Sex__c);
        map_Condition.put('PM_PatientDegree__c',Patient.PM_PatientDegree__c);
        map_Condition.put('PM_Profession__c',Patient.PM_Profession__c);
        map_Condition.put('PM_IdNumber__c', Patient.PM_IdNumber__c);
        map_Condition.put('PM_Email__c',Patient.PM_Email__c);
        map_Condition.put('PM_FAIncome__c',Patient.PM_FAIncome__c);
        map_Condition.put('PM_Payment__c',Patient.PM_Payment__c);
        map_Condition.put('PM_LastVisitStatus__c', Patient.PM_LastVisitStatus__c);
        if(Patient.PM_Visit_Interval__c != null)
        {
            map_Condition.put('PM_Visit_Interval__c',string.valueOf(Patient.PM_Visit_Interval__c));
        }
        map_Condition.put('PM_ZipCode__c',Patient.PM_ZipCode__c);
        map_Condition.put('PM_Internet__c',Patient.PM_Internet__c);
        map_Condition.put('PM_Disappearanc__c',Patient.PM_Disappearanc__c);
        /*******************联系拜访************************/
        map_Condition.put('PM_HomeTel__c',Patient.PM_HomeTel__c);
        map_Condition.put('PM_PmPhone__c',Patient.PM_PmPhone__c);
        map_Condition.put('PM_PmTel__c', Patient.PM_PmTel__c);
        map_Condition.put('PM_FamilyPhone__c',Patient.PM_FamilyPhone__c);
        map_Condition.put('PM_FamilyTel__c',Patient.PM_FamilyTel__c);
        map_Condition.put('PM_OtherTel2__c',Patient.PM_OtherTel2__c);
        map_Condition.put('PM_BVTime1__c', Patient.PM_BVTime1__c);
        map_Condition.put('PM_BVTime2__c', Patient.PM_BVTime2__c);
        map_Condition.put('PM_BVTime3__c',Patient.PM_BVTime3__c);
        map_Condition.put('PM_BVTime4__c',Patient.PM_BVTime4__c);
        map_Condition.put('PM_BVTime5__c',Patient.PM_BVTime5__c);
        map_Condition.put('PM_BVTime6__c',Patient.PM_BVTime6__c);
        map_Condition.put('PM_PhoneState1__c', Patient.PM_PhoneState1__c);
        map_Condition.put('PM_PhoneState2__c', Patient.PM_PhoneState2__c);
        map_Condition.put('PM_PhoneState3__c',Patient.PM_PhoneState3__c);
        map_Condition.put('PM_PhoneState4__c',Patient.PM_PhoneState4__c);
        map_Condition.put('PM_PhoneState5__c',Patient.PM_PhoneState5__c);
        map_Condition.put('PM_PhoneState6__c',Patient.PM_PhoneState6__c);
        /*******************插管信息************************/
        map_Condition.put('PM_InType__c',Patient.PM_InType__c);
        map_Condition.put('PM_InDoctor__c', Patient.PM_InDoctor__c);
        map_Condition.put('PM_InNurser__c', Patient.PM_InNurser__c);
        map_Condition.put('PM_InUser__c',Patient.PM_InUser__c);
        map_Condition.put('PM_Current_PSR__c',Patient.PM_Current_PSR__c);
        map_Condition.put('PM_InInformation__c',Patient.PM_InInformation__c);
        /*******************临床信息************************/
        map_Condition.put('PM_Protopathy__c',Patient.PM_Protopathy__c);
        if(Patient.PM_DaUroutput__c != null)
        {
            map_Condition.put('PM_DaUroutput__c',string.valueOf(Patient.PM_DaUroutput__c));
        }
        if(Patient.PM_DiBlpressure__c != null)
        {
            map_Condition.put('PM_DiBlpressure__c', string.valueOf(Patient.PM_DiBlpressure__c));
        }
         if(Patient.PM_SyBlpressure__c != null)
        {
            map_Condition.put('PM_SyBlpressure__c',string.valueOf(Patient.PM_SyBlpressure__c));
        }
        if(Patient.PM_SeCreatinine__c != null)
        {
            map_Condition.put('PM_SeCreatinine__c',string.valueOf(Patient.PM_SeCreatinine__c));
        }
        if(Patient.PM_Hemoglobin__c != null)
        {
            map_Condition.put('PM_Hemoglobin__c',string.valueOf(Patient.PM_Hemoglobin__c));
        }
        if(Patient.PM_Albumin__c != null)
        {
            map_Condition.put('PM_Albumin__c', string.valueOf(Patient.PM_Albumin__c));
        }
        if(Patient.PM_UrNitrogen__c != null)
        {
            map_Condition.put('PM_UrNitrogen__c',string.valueOf(Patient.PM_UrNitrogen__c));
        }
        map_Condition.put('PM_LiParalysis__c',Patient.PM_LiParalysis__c);
        map_Condition.put('PM_ViImpairment__c',Patient.PM_ViImpairment__c);
        map_Condition.put('PM_PeEqtest__c',Patient.PM_PeEqtest__c);
        map_Condition.put('PM_ClInformation__c',Patient.PM_ClInformation__c);
         /*******************关爱信息************************/
        map_Condition.put('PM_LoGifts__c',Patient.PM_LoGifts__c);
        map_Condition.put('PM_AcHoVisits__c',Patient.PM_AcHoVisits__c);
        map_Condition.put('PM_WhTocall__c', Patient.PM_WhTocall__c);
        map_Condition.put('PM_WhTosend__c',Patient.PM_WhTosend__c);
        map_Condition.put('PM_IsFree__c',Patient.PM_IsFree__c);
        map_Condition.put('PM_IsAcceptCall__c',Patient.PM_IsAcceptCall__c);
        map_Condition.put('PM_Received_Diary__c', Patient.PM_Received_Diary__c);
        map_Condition.put('PM_ReCommunication__c', Patient.PM_ReCommunication__c);
        map_Condition.put('PM_ReCarepackages__c',Patient.PM_ReCarepackages__c);
        map_Condition.put('PM_IsJoined__c',Patient.PM_IsJoined__c);
        map_Condition.put('PM_IsInstruction__c',Patient.PM_IsInstruction__c);
        map_Condition.put('PM_Delivery__c',Patient.PM_Delivery__c);
        map_Condition.put('PM_Activities_Satisfactied__c', Patient.PM_Activities_Satisfactied__c);
        map_Condition.put('PM_Is_Personal_Information__c',Patient.PM_Is_Personal_Information__c);
        map_Condition.put('PM_OHCEducation__c',Patient.PM_OHCEducation__c);
        map_Condition.put('PM_PeReInformation__c',Patient.PM_PeReInformation__c);
        map_Condition.put('PM_CaInformation__c',Patient.PM_CaInformation__c);
         /*******************高风险掉队信息****************************/
        map_Condition.put('PM_EcDifficulties__c',Patient.PM_EcDifficulties__c);
        map_Condition.put('PM_WhTransplants__c',Patient.PM_WhTransplants__c);
        map_Condition.put('PM_TuHeIntention__c',Patient.PM_TuHeIntention__c);
        map_Condition.put('PM_CeDisease__c',Patient.PM_CeDisease__c);
        map_Condition.put('PM_IsOld__c', Patient.PM_IsOld__c);
        map_Condition.put('PM_TuToCoPro__c',Patient.PM_TuToCoPro__c);
        map_Condition.put('PM_DrAcProblems__c',Patient.PM_DrAcProblems__c);
        map_Condition.put('PM_HighRisk__c',Patient.PM_HighRisk__c);
        map_Condition.put('PM_HRInformation__c',Patient.PM_HRInformation__c);
         /*******************高影响信息****************************/
        map_Condition.put('PM_Optimistic__c',Patient.PM_Optimistic__c);
        map_Condition.put('PM_Talkative__c',Patient.PM_Talkative__c);
        map_Condition.put('PM_ATUnderstand__c',Patient.PM_ATUnderstand__c);
        map_Condition.put('PM_ReToBaite__c',Patient.PM_ReToBaite__c);
        map_Condition.put('PM_ToDrFofive__c', ToDrFofive2);
        map_Condition.put('PM_AdTomovement__c',Patient.PM_AdTomovement__c);
        map_Condition.put('PM_FaPeace__c',Patient.PM_FaPeace__c);
        map_Condition.put('PM_BeWorking__c',Patient.PM_BeWorking__c);
        map_Condition.put('PM_BeStudent__c',Patient.PM_BeStudent__c);
        map_Condition.put('PM_HiSostatus__c',Patient.PM_HiSostatus__c);
        map_Condition.put('PM_HighImpac__c',Patient.PM_HighImpac__c);
        map_Condition.put('PM_HIInformation__c',Patient.PM_HIInformation__c);
        map_Condition.put('PM_HiHobbies__c',Patient.PM_HiHobbies__c);
         /***********************治疗信息****************************/
        map_Condition.put('PM_Treat_Hospital__c',Patient.PM_Treat_Hospital__c);
        map_Condition.put('PM_Treat_Doctor__c', Patient.PM_Treat_Doctor__c);
        map_Condition.put('PM_Treat_Nurse__c',Patient.PM_Treat_Nurse__c);
        map_Condition.put('PM_Pdtreatment__c',Patient.PM_Pdtreatment__c);
        map_Condition.put('PM_Treat_Saler__c',Patient.PM_Treat_Saler__c);
        if(Patient.PM_WaConsumption__c !=null)
        {
            map_Condition.put('PM_WaConsumption__c',string.valueOf(Patient.PM_WaConsumption__c));
        }
        map_Condition.put('PM_Heatingmode__c',Patient.PM_Heatingmode__c);
        map_Condition.put('PM_HDAndPD__c',Patient.PM_HDAndPD__c);
        map_Condition.put('PM_FromVendor__c',Patient.PM_FromVendor__c);
        map_Condition.put('PM_TrInformation__c',Patient.PM_TrInformation__c);
         /***********************经销商信息****************************/
        map_Condition.put('PM_Distributor__c',Patient.PM_Distributor__c);
        map_Condition.put('PM_DealerRemark__c',Patient.PM_DealerRemark__c);
         /***********************掉队信息****************************/
        map_Condition.put('PM_DropOff_One_Reason__c',Patient.PM_DropOut_One_Reason__c);
        map_Condition.put('PM_DropOff_Two_Reason__c',Patient.PM_DropOut_Two_Reason__c);
        map_Condition.put('PM_TransformBrand__c',TransfornBrand);
        map_Condition.put('TransfornCreatedDate_Start',TransfornCreatedDate_Start);
        map_Condition.put('TransfornCreatedDate_End',TransfornCreatedDate_End);
        map_Condition.put('TransfornActDate_Start',TransfornActDate_Start);
        map_Condition.put('TransfornActDate_End',TransfornActDate_End);
        
        map_Condition.put('TransfornDate_Start',TransfornDate_Start);
        map_Condition.put('TransfornDate_End',TransfornDate_End);
        map_Condition.put('TransfornCreateDate_Start',TransfornCreateDate_Start);
        map_Condition.put('TransfornCreateDate_End',TransfornCreateDate_End);
        map_Condition.put('UnreachableDate_Start',UnreachableDate_Start);
        map_Condition.put('UnreachableDate_End',UnreachableDate_End);
        
        map_Condition.put('PM_TransfornReason__c',Patient.PM_TransfornReason__c);
        map_Condition.put('PM_SecondaryReason__c',Patient.PM_SecondaryReason__c);
        map_Condition.put('PM_Disappearanc__c',Patient.PM_Disappearanc__c);
        
        map_Condition.put('BestVisitTime',BestVisitTime);
        map_Condition.put('Age_Lower',Age_Lower);
        map_Condition.put('Age_Upper',Age_Upper);
        map_Condition.put('PM_Active_PMT__c',Patient.PM_Active_PMT__c);
        
        map_Condition.put('Visit_Interval',Visit_Interval);
        system.debug('***************BestVisit**************');
        util.connectQueryString(field,map_Condition);
        this.conset = util.getNormalConset(); 
    }
     
    //排序方法
    public string strOrder{get;set;}
    public string strSortField{get;set;}
    public string strPreviousSortField{get;set;}
    //sort the orders by field selected
    public void sortOrders()
    {
        strOrder ='desc';
        if(strSortField==null)
        {
            strSortField='Name';
        }
        if(strPreviousSortField == strSortField)
        {
            strOrder = 'asc';
            strPreviousSortField = null;
        }
        else
        {
            strPreviousSortField = strSortField;
        }
        string patSql = util.getPatSql();
        if(patSql == null || patSql == '')
        {
            patSql = query;
        }
        sortSql(patSql,strSortField,strOrder);
    }
    public void sortSql(String sql,String strSortField,String strOrder)
    {
        String strSqlSort;
        if(strSortField!=null)
        {
            strSqlSort=sql+' order by '+strSortField+' '+strOrder;
        }
        strSqlSort=strSqlSort;
        this.conset = util.sortOrders(strSqlSort); 
    }
    
    /*********************导出功能 start****************************/
    PM_ExportFieldsCommon field = new PM_ExportFieldsCommon();
    private list<PM_ExportFieldsCommon.Patient> list_PatientExport = new list<PM_ExportFieldsCommon.Patient>();//导出时需要判断是否选中的list
    //基本信息
    public List<PM_ExportFieldsCommon.ExportField> list_BaseInfo{get;set;}
    public List<List<PM_ExportFieldsCommon.ExportField>> list_BaseInfoWrapper{get;set;}
    //联系拜访
    public List<PM_ExportFieldsCommon.ExportField> list_Contact{get;set;}
    public List<List<PM_ExportFieldsCommon.ExportField>> list_ContactWrapper1{get;set;}
    public List<List<PM_ExportFieldsCommon.ExportField>> list_ContactWrapper2{get;set;}
    public List<List<PM_ExportFieldsCommon.ExportField>> list_ContactWrapper3{get;set;}
    //插管信息
    public List<PM_ExportFieldsCommon.ExportField> list_InInfo{get;set;}
    public List<List<PM_ExportFieldsCommon.ExportField>> list_InInfoWrapper{get;set;}
    //临床信息
    public List<PM_ExportFieldsCommon.ExportField> list_ClinicalInfo{get;set;}
    public List<List<PM_ExportFieldsCommon.ExportField>> list_ClinicalInfoWrapper{get;set;}
    //关爱信息
    public List<PM_ExportFieldsCommon.ExportField> list_CareInfo{get;set;}
    public List<List<PM_ExportFieldsCommon.ExportField>> list_CareInfoWrapper{get;set;}
    //高风险掉队信息
    public List<PM_ExportFieldsCommon.ExportField> list_HighRisk{get;set;}
    public List<List<PM_ExportFieldsCommon.ExportField>> list_HighRiskWrapper{get;set;}
    //高影响信息
    public List<PM_ExportFieldsCommon.ExportField> list_HighEffect{get;set;}
    public List<List<PM_ExportFieldsCommon.ExportField>> list_HighEffectWrapper{get;set;}
    //治疗信息
    public List<PM_ExportFieldsCommon.ExportField> list_TreatInfo{get;set;}
    public List<List<PM_ExportFieldsCommon.ExportField>> list_TreatInfoWrapper{get;set;}
    //经销商信息
    public List<PM_ExportFieldsCommon.ExportField> list_DealerInfo{get;set;}
    public List<List<PM_ExportFieldsCommon.ExportField>> list_DealerInfoWrapper{get;set;}
    //掉队信息
    public List<PM_ExportFieldsCommon.ExportField> list_DropInfo{get;set;}
    public List<List<PM_ExportFieldsCommon.ExportField>> list_DropInfoWrapper{get;set;}
    //页面需要导出的列
    public Transient List<PM_ExportFieldsCommon.ExportField> List_ExportView{get;set;}
    private Transient List<PM_Patient__c> list_PatientExcel;
    private Transient List<List<PM_Patient__c>> list_PatientExcellist;
    //public string contentType{get;set;}
    //public Boolean IsExcel{get;set;}
    
    //初始化字段集
    private void InitFieldsSet()
    {
        list_BaseInfo = field.getBaseInfoFields();
        list_Contact=field.getContactFields();
        list_InInfo=field.getInInfoFields();
        list_ClinicalInfo=field.getClinicalInfoFields();
        list_CareInfo=field.getCareInfoFields();
        list_HighRisk=field.getHighRiskFields();
        list_HighEffect=field.getHighEffectFields();
        list_TreatInfo=field.getTreatInfoFields();
        list_DealerInfo=field.getDealerInfoFields();
        list_DropInfo=field.getDropInfoFields();
        //IsExcel = false;
        //contentType = '';
        this.FormatFieldsSet();
    }
    //Sunny :格式化字段级
    private void FormatFieldsSet()
    {
        this.list_BaseInfoWrapper = this.FieldSetWarp(this.list_BaseInfo , 4);
        //this.list_ContactWrapper1 = this.FieldSetWarp(this.list_Contact);
        //单独处理联系方式
        this.FieldSetWarpForContact();
        this.list_InInfoWrapper = this.FieldSetWarp(this.list_InInfo,4);
        this.list_ClinicalInfoWrapper = this.FieldSetWarp(this.list_ClinicalInfo,4);
        this.list_CareInfoWrapper = this.FieldSetWarp(this.list_CareInfo,4);
        this.list_HighRiskWrapper = this.FieldSetWarp(this.list_HighRisk,4);
        this.list_HighEffectWrapper = this.FieldSetWarp(this.list_HighEffect,4);
        this.list_TreatInfoWrapper = this.FieldSetWarp(this.list_TreatInfo,4);
        this.list_DealerInfoWrapper = this.FieldSetWarp(this.list_DealerInfo,4);
        this.list_DropInfoWrapper = this.FieldSetWarp(this.list_DropInfo,4);
    }
    //Sunny :二次封装字段级
    private List<List<PM_ExportFieldsCommon.ExportField>> FieldSetWarp(List<PM_ExportFieldsCommon.ExportField> listSource , Integer iSize)
    {
        List<List<PM_ExportFieldsCommon.ExportField>> listResult = new List<List<PM_ExportFieldsCommon.ExportField>>();
        List<PM_ExportFieldsCommon.ExportField> list_eField = new List<PM_ExportFieldsCommon.ExportField>();
        for(PM_ExportFieldsCommon.ExportField eField : listSource)
        {
            if(list_eField.size() == iSize)
            {
                listResult.add(list_eField);
                list_eField = new List<PM_ExportFieldsCommon.ExportField>();
                list_eField.add(eField);
            }
            else
            {
                list_eField.add(eField);
            }
        }
        if(list_eField.size()<=iSize)
        listResult.add(list_eField);
        return listResult;
    }
    private void FieldSetWarpForContact()
    {
        List<PM_ExportFieldsCommon.ExportField> listContactPhone = new List<PM_ExportFieldsCommon.ExportField>();
        List<PM_ExportFieldsCommon.ExportField> listContactPhoneState = new List<PM_ExportFieldsCommon.ExportField>();
        List<PM_ExportFieldsCommon.ExportField> listBVTime = new List<PM_ExportFieldsCommon.ExportField>();
        for(PM_ExportFieldsCommon.ExportField eField : this.list_Contact)
        {
            if(eField.apiName.getFieldPath().toUpperCase().contains('BVTIME'))
            {
                listBVTime.add(eField);
            }
            if(eField.apiName.getFieldPath().toUpperCase().contains('PHONESTATE'))
            {
            	listContactPhoneState.add(eField);
            }
            else
            {
            	if(!eField.apiName.getFieldPath().toUpperCase().contains('PHONESTATE') && !eField.apiName.getFieldPath().toUpperCase().contains('BVTIME'))
            	{
            		listContactPhone.add(eField);
            	}
            }
        }
        this.list_ContactWrapper1 = this.FieldSetWarp(listContactPhoneState,3);
        this.list_ContactWrapper2 = this.FieldSetWarp(listBVTime,3);
        this.list_ContactWrapper3 = this.FieldSetWarp(listContactPhone,3);
    }
    
     //导出excel
    public PageReference exportExcel()
    {
        List_ExportView = new List<PM_ExportFieldsCommon.ExportField>();
        //实例化导出的内容集合
        list_PatientExcel = new List<PM_Patient__c>();
        list_PatientExcellist = new List<List<PM_Patient__c>>();
        //IsExcel = true;
        //确定页面选定的记录
        Set<ID> set_patientIds = new Set<ID>();
        set_patientIds.clear();
        if(list_PatientExport != null && list_PatientExport.size()>0)
        {
            for(PM_ExportFieldsCommon.patient pa : list_PatientExport)
            {
                if(pa.IsExport)
                {
                    set_patientIds.add(pa.patient.Id);
                }
            }
        }
        system.debug(set_patientIds.size() + '$$$$$$$$$$$$$$$$$$');
        /*
        Sunny 修改找到哪些字段勾选的逻辑
        for(PM_ExportFieldsCommon.ExportField field : list_BaseInfo)
        {
            if(field.IsChecked)List_ExportView.add(field);
        }
        for(PM_ExportFieldsCommon.ExportField field : list_Contact)
        {
            if(field.IsChecked)List_ExportView.add(field);
        }
        for(PM_ExportFieldsCommon.ExportField field : list_InInfo)
        {
            if(field.IsChecked)List_ExportView.add(field);
        }
        for(PM_ExportFieldsCommon.ExportField field : list_ClinicalInfo)
        {
            if(field.IsChecked)List_ExportView.add(field);
        }
        for(PM_ExportFieldsCommon.ExportField field : list_CareInfo)
        {
            if(field.IsChecked)List_ExportView.add(field);
        }
        for(PM_ExportFieldsCommon.ExportField field : list_HighRisk)
        {
            if(field.IsChecked)List_ExportView.add(field);
        }
        for(PM_ExportFieldsCommon.ExportField field : list_HighEffect)
        {
            if(field.IsChecked)List_ExportView.add(field);
        }
        for(PM_ExportFieldsCommon.ExportField field : list_TreatInfo)
        {
            if(field.IsChecked)List_ExportView.add(field);
        }
        for(PM_ExportFieldsCommon.ExportField field : list_DealerInfo)
        {
            if(field.IsChecked)List_ExportView.add(field);
        }
        for(PM_ExportFieldsCommon.ExportField field : list_DropInfo)
        {
            if(field.IsChecked)List_ExportView.add(field);
        }
        */
        if(this.list_BaseInfoWrapper != null) List_ExportView.addAll(this.getSelectedField(this.list_BaseInfoWrapper));
        if(this.list_ContactWrapper1 != null) List_ExportView.addAll(this.getSelectedField(this.list_ContactWrapper1));
        if(this.list_ContactWrapper2 != null) List_ExportView.addAll(this.getSelectedField(this.list_ContactWrapper2));
        if(this.list_ContactWrapper3 != null) List_ExportView.addAll(this.getSelectedField(this.list_ContactWrapper3));
        if(this.list_InInfoWrapper != null) List_ExportView.addAll(this.getSelectedField(this.list_InInfoWrapper));
        if(this.list_ClinicalInfoWrapper != null) List_ExportView.addAll(this.getSelectedField(this.list_ClinicalInfoWrapper));
        if(this.list_CareInfoWrapper != null) List_ExportView.addAll(this.getSelectedField(this.list_CareInfoWrapper));
        if(this.list_HighRiskWrapper != null) List_ExportView.addAll(this.getSelectedField(this.list_HighRiskWrapper));
        if(this.list_HighEffectWrapper != null) List_ExportView.addAll(this.getSelectedField(this.list_HighEffectWrapper));
        if(this.list_TreatInfoWrapper != null) List_ExportView.addAll(this.getSelectedField(this.list_TreatInfoWrapper));
        if(this.list_DealerInfoWrapper != null) List_ExportView.addAll(this.getSelectedField(this.list_DealerInfoWrapper));
        if(this.list_DropInfoWrapper != null) List_ExportView.addAll(this.getSelectedField(this.list_DropInfoWrapper));
        
        system.debug('xuan qu de size:'+List_ExportView.size());
        if(List_ExportView.size()<=0)
        {
            List_ExportView.addAll(list_BaseInfo);
            List_ExportView.addAll(list_Contact);
            List_ExportView.addAll(list_InInfo);
            List_ExportView.addAll(list_ClinicalInfo);
            List_ExportView.addAll(list_CareInfo);
            List_ExportView.addAll(list_HighRisk);
            List_ExportView.addAll(list_HighEffect);
            List_ExportView.addAll(list_TreatInfo);
            List_ExportView.addAll(list_DealerInfo);
            List_ExportView.addAll(list_DropInfo);
        }
        string strSoql = field.getApiNameFields(List_ExportView);
        String exportSql = strSoql;
        system.debug('Sql info:'+exportSql);
        if(set_patientIds.size()>0)
        {
            exportSql += ' Where Id in : set_patientIds';
        }
        else
        {
            string patSql = util.getPatSql();
            if(patSql == null || patSql == '')
            {
                if(query.indexOf('Where') >=0)
                {
                    exportSql += query.substring(query.indexOf('Where'), query.length());
                }
            }
            else
            {
                list_PatientExcellist = util.getPatientExcellist(exportSql, patSql);
            }
            //list_PatientExcel = Database.query(exportSql);
        }
        
        if(list_PatientExcellist.size() <= 0)
        {
            for(PM_Patient__c p : util.getPatientlist((exportSql+' limit '+PM_QueryUtil.LIMIT_SIZE),set_patientIds))
            {
                if(list_PatientExcel.size() < 999)
                {
                    list_PatientExcel.add(p);
                }else{
                	list_PatientExcel.add(p);
                    list_PatientExcellist.add(list_PatientExcel);
                    list_PatientExcel = new List<PM_Patient__c>();
                }
            }
            list_PatientExcellist.add(list_PatientExcel);
        }
        field.SaveSoql(strSoql, list_PatientExcellist);
        //contentType = 'application/vnd.ms-excel#QueryPatientExcel.xls';
        return new PageReference('/apex/PM_PatientExport');
    }
    private List<PM_ExportFieldsCommon.ExportField> getSelectedField(List<List<PM_ExportFieldsCommon.ExportField>> list_FieldWrapper)
    {
        List<PM_ExportFieldsCommon.ExportField> listSelectedField = new List<PM_ExportFieldsCommon.ExportField>();
        for(List<PM_ExportFieldsCommon.ExportField> listField : list_FieldWrapper)
        {
            for(PM_ExportFieldsCommon.ExportField field : listField)
            {
                if(field.IsChecked)listSelectedField.add(field);
            }
        }
        return listSelectedField;
    }
    /*********************导出功能      end****************************/
}