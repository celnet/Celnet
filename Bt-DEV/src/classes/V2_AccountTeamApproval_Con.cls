/**
批量批准和拒绝

**/
public with sharing class V2_AccountTeamApproval_Con {

	public boolean blnIsMesShow{get;set;}
	public string strMessage{get;set;}

	public String strInfo{get;set;}
	List<V2_Account_Team__c> selectObjs;
	Set<ID> selectIds = new Set<ID>();
	public V2_AccountTeamApproval_Con(ApexPages.StandardController controller)
	{
	}
	//list button
	public V2_AccountTeamApproval_Con(ApexPages.StandardSetController controller)
	{
		blnIsMesShow=false;
		List<sObject> lst = controller.getSelected();
		if(lst.size()==0){
			blnIsMesShow = true;
			strMessage='请至少选择一条记录';
		}
		selectObjs = lst;
		for(V2_Account_Team__c atc:selectObjs){
			
			selectIds.add(atc.Id);
		}
	}
	/**
	审批过程，
	action="Approval" || "Reject"
	
	**/
	public void ApprovalProcess(String action){
		try
		{
			User[] us = [select id,DelegatedApproverId from User where isactive=true];
			Map<String,String> userMap = new Map<String,String>();
			for(User u :us){
				userMap.put(u.id,u.DelegatedApproverId);
			}
			
			V2_UtilClass ut = new V2_UtilClass();
			
			//ProcessInstance每一条待审批的记录都会对应一条ProcessInstance记录，外键TargetObjectId
			Set<ID> piIds = new Set<ID>();
			//通过选中记录的Id查询出对应的ProcessInstance集合，把ProcessInstance的ID添加到集合中。
			for(ProcessInstance pi : [Select p.Id From ProcessInstance p where p.TargetObjectId IN:selectIds])
			{			
				piIds.add(pi.Id);
			}
			//待审批的集合
			List<Approval.ProcessWorkitemRequest> piwList = new List<Approval.ProcessWorkitemRequest>();
			//根据ProcessInstanceID集合查询出ProcessInstanceWorkitem
			for(ProcessInstanceWorkitem piw : [Select p.Id,p.OriginalActorId From ProcessInstanceWorkitem p where p.ProcessInstanceId IN:piIds])
			{		
				String s=userMap.get(piw.OriginalActorId);
				//当前用户既不是审批者，也不是代理审批者，也不是管理员
				if(Userinfo.getUserId() != piw.OriginalActorId && Userinfo.getUserId()!=s && !ut.IfAdmin()){
					continue;
				}
				
				//创建ProcessWorkitemRequest
				Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
				req.setComments('Approving request.');
				//设置Approve
	        	req.setAction(action);
	        	//把workitem的ID
	        	req.setWorkitemId(piw.Id);
	        	piwList.add(req);
			}
			
			if(piwList.size()==0){
				blnIsMesShow = true;
				strMessage='您勾选的记录中，没有符合审批条件的记录。';
				return;
			}
			
			//执行批量审批
			Approval.ProcessResult[] result2 =  Approval.process(piwList);
			for(Approval.ProcessResult pr : result2)
			{
				if(!pr.isSuccess())
				{
					for(Database.Error e : pr.getErrors())
					{
						blnIsMesShow = true;
						strMessage += e.Message;
					}
				}
			}
			if(!blnIsMesShow){
				strInfo ='成功审批' + piwList.size() + '条记录.';
			}
		}catch(Exception e)
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING , e.getMessage());            
            ApexPages.addMessage(msg);
            return;
		}
	}
	
	
	/**
	审批通过
	**/
	public Pagereference ApprovalPass(){
		ApprovalProcess('Approve');
		
		return null;
	}
	/**
	审批拒绝
	**/
	public Pagereference ApprovalReject(){
		ApprovalProcess('Reject');
		
		return null;
	}
	
	public Pagereference doCancel(){
		return new Pagereference('/a08');
	}
	static testMethod void TestApprove()
	{
		//----------------New UserRole ------------------
    	//List<UserRole> list_userRole = new List<UserRole >() ;
    	UserRole objUserRole = new UserRole() ;
    	objUserRole.Name = 'Renal-Rep-大上海-PD-Rep(陈喆令)' ;
    	insert objUserRole ;
    	UserRole objUserRole2 = new UserRole() ;
    	objUserRole2.Name = 'Renal-Rep-大上海-PB-Rep(陈喆令)' ;
    	objUserRole2.ParentRoleId = objUserRole.Id ;
    	insert objUserRole2 ;
		//----------------Create User-------------
    	List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.UserRoleId = objUserRole2.Id ;
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	use2.UserRoleId = objUserRole2.Id ;
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.UserRoleId = objUserRole.Id ;
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
    	use2.ManagerId = use3.Id ;
    	use2.DelegatedApproverId = use3.Id ;
    	update use2 ;
    	//------------------New Account---------------------
    	List<Account> list_Accs = new List<Account>();
    	Account objAcc1 = new Account() ;
    	objAcc1.Name = 'Acc1' ;
    	objAcc1.MID__c = 'sldnfnwl' ;
    	list_Accs.add(objAcc1) ;
    	insert list_Accs ;
    	//----------------New Account Team------------------
    	List<V2_Account_Team__c> list_AccT = new List<V2_Account_Team__c>() ;
    	V2_Account_Team__c objAccT1 = new V2_Account_Team__c() ;
    	objAccT1.V2_Account__c = objAcc1.Id ;
    	objAccT1.V2_User__c = use1.Id ;
    	objAccT1.V2_ApprovalStatus__c = '待审批' ;
    	V2_Account_Team__c objAccT2 = new V2_Account_Team__c() ;
    	objAccT2.V2_Account__c = objAcc1.Id ;
    	objAccT2.V2_User__c = use2.Id ;
    	objAccT2.V2_ApprovalStatus__c = '待审批' ;
    	list_AccT.add(objAccT1) ;
    	insert list_AccT ;
    	system.runAs(use2)
    	{
    		for(V2_Account_Team__c objAT : list_AccT)
    		{
    			//提交审批
				Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
		        req.setObjectId(objAT.Id);//客户联系人或自定义对象
		       	Approval.ProcessResult result = Approval.process(req);
    		}
    	}
    	system.runAs(use3)
    	{
    		ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(list_AccT);
    		ssc.setSelected(list_AccT) ;
    		V2_AccountTeamApproval_Con ATApproval = new V2_AccountTeamApproval_Con(ssc) ;
    		ATApproval.ApprovalPass() ;
    		ATApproval.doCancel() ;
    		
    	}
	}
	static testMethod void TestReject()
	{
		//----------------New UserRole ------------------
    	//List<UserRole> list_userRole = new List<UserRole >() ;
    	UserRole objUserRole = new UserRole() ;
    	objUserRole.Name = 'Renal-Rep-大上海-PD-Rep(陈喆令)' ;
    	insert objUserRole ;
    	UserRole objUserRole2 = new UserRole() ;
    	objUserRole2.Name = 'Renal-Rep-大上海-PB-Rep(陈喆令)' ;
    	objUserRole2.ParentRoleId = objUserRole.Id ;
    	insert objUserRole2 ;
		//----------------Create User-------------
    	List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.UserRoleId = objUserRole2.Id ;
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	use2.UserRoleId = objUserRole2.Id ;
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.UserRoleId = objUserRole.Id ;
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
    	use2.ManagerId = use3.Id ;
    	use2.DelegatedApproverId = use3.Id ;
    	update use2 ;
    	//------------------New Account---------------------
    	List<Account> list_Accs = new List<Account>();
    	Account objAcc1 = new Account() ;
    	objAcc1.Name = 'Acc1' ;
    	objAcc1.MID__c = 'sldnfnwl' ;
    	list_Accs.add(objAcc1) ;
    	insert list_Accs ;
    	//----------------New Account Team------------------
    	List<V2_Account_Team__c> list_AccT = new List<V2_Account_Team__c>() ;
    	V2_Account_Team__c objAccT1 = new V2_Account_Team__c() ;
    	objAccT1.V2_Account__c = objAcc1.Id ;
    	objAccT1.V2_User__c = use1.Id ;
    	objAccT1.V2_ApprovalStatus__c = '待审批' ;
    	V2_Account_Team__c objAccT2 = new V2_Account_Team__c() ;
    	objAccT2.V2_Account__c = objAcc1.Id ;
    	objAccT2.V2_User__c = use2.Id ;
    	objAccT2.V2_ApprovalStatus__c = '待审批' ;
    	list_AccT.add(objAccT1) ;
    	insert list_AccT ;
    	system.runAs(use2)
    	{
    		for(V2_Account_Team__c objAT : list_AccT)
    		{
    			//提交审批
				Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
		        req.setObjectId(objAT.Id);//客户联系人或自定义对象
		       	Approval.ProcessResult result = Approval.process(req);
    		}
    	}
    	system.runAs(use3)
    	{
    		ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(list_AccT);
    		ssc.setSelected(list_AccT) ;
    		V2_AccountTeamApproval_Con ATApproval = new V2_AccountTeamApproval_Con(ssc) ;
    		ATApproval.ApprovalReject() ;
    		ATApproval.doCancel() ;
    		
    	}
	}
}