/**
 * SUNNY
 * 
 **/
public class Ctrl_OppStrategyCommentLevelTwoForAdmin {
    //年、月
    public String strYear{get;set;}
    public String strMonth{get;set;}
    public List<SelectOption> getNewMonths(){
        List<SelectOption> options = new List<SelectOption>();
        //options.add(new SelectOption('--无--','--无--'));
        if(strMonth!=null && strMonth != String.valueOf(date.today().month())){
            options.add(new SelectOption(strMonth,strMonth));
        }else{
            options.add(new SelectOption(String.valueOf(date.today().month()),String.valueOf(date.today().month())));
            if(date.today().day() <= 7){
                options.add(new SelectOption(String.valueOf(date.today().addMonths(-1).month()),String.valueOf(date.today().addMonths(-1).month())));
            }
        }
        
        
        return options;
    }
    //修改的行数（可用其找到修改的list内的哪一条记录）
    public String strIndex{get;set;}
    public List<OppEvaLevelTwo> list_oppEvaTwo{get;set;}
    private List<OppEvaLevelTwo> list_oppEvaTwoBack{get;set;}
    private string strUrole;
    public List<SelectOption> getMonths(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption(String.valueOf(date.today().Month()),String.valueOf(date.today().Month())));
        if(date.today().day() <= 7){
            options.add(new SelectOption(String.valueOf(date.today().addMonths(-1).Month()),String.valueOf(date.today().addMonths(-1).Month())));
        }
        return options;
    }
    private String strUId ;
    private String strURId ;
    //构造函数
    public Ctrl_OppStrategyCommentLevelTwoForAdmin(Apexpages.Standardcontroller controller){
        //strMonth = String.valueOf(date.today().Month());
        //strYear = String.valueOf(date.today().Year());
        string syear=Apexpages.currentPage().getParameters().get('year');
        if(syear!=null){
            strYear = syear;
        }
        string smonth = Apexpages.currentPage().getParameters().get('month');
        if(smonth!=null){
            strMonth=smonth;
        }
        strUId=Apexpages.currentPage().getParameters().get('uid');
        strURId = Apexpages.currentPage().getParameters().get('urid');
        list_oppEvaTwo = new List<OppEvaLevelTwo>();
        list_oppEvaTwoBack = new List<OppEvaLevelTwo>();
        
        
        //List<UserRole> listur = [Select Id,Name From UserRole Where Id =: Userinfo.getUserRoleId()];
        List<UserRole> listur = [Select Id,Name From UserRole Where Id =: strURId];
        if(listur.size() > 0 ){
            String strRolName = listur[0].Name;
            if(strRolName.contains('-')){
                List<String> list_s = strRolName.split('-');
                if(list_s.size() == 5){
                    strUrole=list_s[1];
                }else{
                    strUrole=strRolName;
                }
            }else{
                strUrole=strRolName;
            }
        }
        
        
        getOppEvaluations();
    }
    //获取业务机会评分的记录
    public void getOppEvaluations(){
        if(strMonth == String.valueOf(date.today().addMonths(-1).month())){
            strYear = String.valueOf(date.today().addMonths(-1).year());
        }else{
            strYear = String.valueOf(date.today().year());
        }
        list_oppEvaTwo.clear();
        list_oppEvaTwoBack.clear();
        //获取当前用户所有下属的user id
        V2_UtilClass uc = new V2_UtilClass();
        Set<ID> set_userIds = new Set<ID>();
        set_userIds.addAll(uc.getSubordinateIds(strURId));
        //获取直接汇报给当前用户的下属角色（直接汇报关系）
        Set<ID> listUserId = new Set<ID>();
        //List<ID> listuserroleid=uc.map_RoleIds.get(UserInfo.getUserRoleId());
        List<ID> listuserroleid=uc.map_RoleIds.get(strURId);
        //查询直接汇报关系的下属userid
        for(User u:[Select Id From User Where UserRoleId in: listuserroleid And IsActive = true]){
            listUserId.add(u.Id);
        }
        system.debug('find xiashurole:'+listuserroleid);
        system.debug('find xiashu:'+listUserId);
        
        
        List<ID> list_BeCommentUserId = new List<ID>();
        Map<ID,list<OppEvaluation__c>> map_listoppe=new Map<ID,list<OppEvaluation__c>>();
        //先查找当前年、当前月份、当前用户下属为被点评人，业务机会不为空的数据(即一级评价)
        For(OppEvaluation__c oe : [Select Id,BeCommentUser__c,Commentator__c,Score__c,Comments__c ,Opportunity__c 
           From OppEvaluation__c Where Year__c =: strYear And Month__c =: strMonth And (Score__c != null Or Comments__c != null) And Opportunity__c != null And BeCommentUser__c in: set_userIds Order by BeCommentUser__r.Name]){
            system.debug(oe);
            if(oe.BeCommentUser__c!=null){
                list_BeCommentUserId.add(oe.BeCommentUser__c);
                if(map_listoppe.containsKey(oe.Commentator__c)){
                    list<OppEvaluation__c> listoe=map_listoppe.get(oe.Commentator__c);
                    listoe.add(oe);
                    map_listoppe.put(oe.Commentator__c , listoe);
                }else{
                    list<OppEvaluation__c> listoe=new List<OppEvaluation__c>();
                    listoe.add(oe);
                    map_listoppe.put(oe.Commentator__c , listoe);
                }
            }
        }
        //找到rep的上级，页面打分是给直接管代表的主管打分
        List<ID> list_parentRoleId = new List<ID>();
        Map<ID,String> map_Name = new Map<ID,String>();
        for(User objU : [Select Id,UserRole.ParentRoleId,UserRoleId From User Where Id in: list_BeCommentUserId]){
            list_parentRoleId.add(objU.UserRole.ParentRoleId);
        }
        List<ID> list_parentId = new List<ID>();
        for(User objU : [Select Id,Name From User Where UserRoleId in: list_parentRoleId]){
            list_parentId.add(objU.Id);
            map_Name.put(objU.Id,objU.Name);
        }
        //查询rep的上级（主管）被评价（被当前登陆人评价）的记录
        //组成 主管-评价记录 的Map，每个主管应该只会有一个评价
        Map<ID,OppEvaluation__c> map_oppe=new Map<ID,OppEvaluation__c>();
        
        for(OppEvaluation__c oe : [Select Id,BeCommentUser__c,BeCommentUser__r.Name,Commentator__c,Score__c,Comments__c 
           //From OppEvaluation__c Where Year__c =: strYear And Month__c =: strMonth And (Score__c != null Or Comments__c != null) And BeCommentUser__c in: list_parentId And Commentator__c =: UserInfo.getUserId() And BeCommentUser__c  in: listUserId]){
           From OppEvaluation__c Where Year__c =: strYear And Month__c =: strMonth And (Score__c != null Or Comments__c != null) And BeCommentUser__c in: list_parentId And Commentator__c =: strUId And BeCommentUser__c  in: listUserId]){
            map_oppe.put(oe.BeCommentUser__c , oe);
            map_Name.put(oe.BeCommentUser__c,oe.BeCommentUser__r.Name);
        }
        for(ID pId : list_parentId){
            if(!listUserId.contains(pId)){
                continue;
            }
            OppEvaLevelTwo oet = new OppEvaLevelTwo();
            oet.strName = map_Name.get(pId);
            if(map_oppe.containsKey(pId)){
                oet.blnRead=true;
                oet.oppEva = map_oppe.get(pId);
                if(date.today().day() <= 7  ){
                    oet.CanEdit=true;
                }else if(strMonth == String.valueOf(date.today().addMonths(-1).month())){
                    oet.CanEdit=false;
                }else{
                    oet.CanEdit=true;
                }
                system.debug('dianpingren:'+oet.oppEva.Commentator__c+' dangqiandengluren:'+strUId+' kebianji:'+oet.CanEdit);
                //if(oet.oppEva.Commentator__c == Userinfo.getUserId() && oet.CanEdit == true){
                if(oet.oppEva.Commentator__c == strUId && oet.CanEdit == true){
                    
                }else{
                    system.debug('gaiwei fou');
                    oet.CanEdit = false;
                }
                oet.CanEdit = true;
            }else{
                OppEvaluation__c oppEva = new OppEvaluation__c();
                //oppEva.Commentator__c=UserInfo.getUserId();
                oppEva.Commentator__c=strUId;
                oppEva.BeCommentUser__c=pId;
                oppEva.Year__c = strYear;
                oppEva.Month__c = strMonth;
                oppEva.CommentDate__c = date.today();
                oet.oppEva = oppEva;
                oet.blnEdit=true;
            }
            system.debug(pId+'~~~'+map_listoppe);
            if(map_listoppe.containsKey(pId)){
                system.debug('fuzhi');
                oet.list_oppEva = map_listoppe.get(pId);
                system.debug('fuzhi'+oet.list_oppEva);
            }
            oet.intIndex = list_oppEvaTwo.size();
            list_oppEvaTwo.add(oet);
            //list_oppEvaTwoBack.add(oet);
        }
        system.debug('!!!'+list_oppEvaTwo);
    }
    public void saveComment(){
        if(strIndex!=null){
            Integer i = Integer.valueOf(strIndex);
            OppEvaLevelTwo oet=list_oppEvaTwo[i];
            oet.oppEva.CommentDate__c=date.today();
            oet.oppEva.CommentatorRole__c=strUrole;
            oet.oppEva.IsQualityEva__c=true;
    		/***************bill add 2013/4/26 Begin******************/
    		//2013-5-14 Sunny 注释掉自动发邮件
    		//发送邮件
    		//AutoSendmail autoSendmail = new AutoSendmail();
    		if(oet.oppEva.Id !=null){
    			update oet.oppEva; 
    			//autoSendmail.AutoSendmailOpportunityTwo(oet.oppEva.Id, '评分修改');
    		}else{
    			insert oet.oppEva;
    			//autoSendmail.AutoSendmailOpportunityTwo(oet.oppEva.Id, '评分');
    		}
    		/***************bill add 2013/4/26 END******************/
            getOppEvaluations();
        }
    }
    public void editComment(){
        if(strIndex!=null){
            Integer i = Integer.valueOf(strIndex);
            OppEvaLevelTwo oet=list_oppEvaTwo[i];
            oet.blnEdit=true;
            oet.CanEdit=false;
            oet.blnRead=false;
            oet.CanCancel=true;
        }
    }
    public void cancelComment(){
        getOppEvaluations();
        /*
        if(strIndex!=null){
            Integer i = Integer.valueOf(strIndex);
            OppEvaLevelTwo oet=list_oppEvaTwo[i];
            oet.blnEdit=false;
            oet.blnRead=true;
            oet.CanEdit=true;
            oet.CanCancel=false;
            //system.debug(oet.oppEva+' &&&&&&& '+list_oppEvaTwoBack[i].oppEva);
            //oet.oppEva=list_oppEvaTwoBack[i].oppEva;
        }
        */
    }
    
    public class OppEvaLevelTwo{
        public Integer intIndex{get;set;}
        public boolean CanCancel{get;set;}
        public Boolean CanEdit{get;set;}
        public Boolean blnEdit{get;set;}
        public Boolean blnRead{get;set;}
        public String strName{get;set;}
        public OppEvaluation__c oppEva{get;set;}
        public List<OppEvaluation__c> list_oppEva{get;set;}
    }
    
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        //user role
        List<UserRole> listur= new List<UserRole>();
        UserRole ur0 = new UserRole();
        ur0.Name = 'Renal-Supervisor-华南-PD-Supervisor(U0)';
        listur.add(ur0);
        UserRole ur1 = new UserRole();
        ur1.Name = 'Renal-Supervisor-华南-PD-Supervisor(U1)';
        listur.add(ur1);
        UserRole ur2 = new UserRole();
        ur2.Name = 'Renal-Rep-华南-PD-Rep(U2)';
        listur.add(ur2);
        insert listur;
        ur2.ParentRoleId=ur1.Id;
        update ur2;
        ur1.ParentRoleId = ur0.id;
        update ur1;
        //user 
        list<User> user=[Select TimeZoneSidKey,ProfileId,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname, Alias From User Where IsActive = true limit 1];
        List<User> list_user = new List<User>();
        User user0=new User();
        user0.username='user0r2@023.com';
        user0.LastName='user0r2';
        user0.Email='user0r2@023.com';
        user0.Alias=user[0].Alias;
        user0.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user0.ProfileId=UserInfo.getProfileId();
        user0.LocaleSidKey=user[0].LocaleSidKey;
        user0.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user0.EmailEncodingKey=user[0].EmailEncodingKey;
        user0.CommunityNickname='chequ23';
        user0.MobilePhone='02345678902';
        user0.IsActive = true;
        user0.UserRoleId=ur0.Id ;
        list_user.add(user0);
        User user1=new User();
        user1.username='user1r2@123.com';
        user1.LastName='user1r2';
        user1.Email='user1r2@123.com';
        user1.Alias=user[0].Alias;
        user1.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user1.ProfileId=UserInfo.getProfileId();
        user1.LocaleSidKey=user[0].LocaleSidKey;
        user1.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user1.EmailEncodingKey=user[0].EmailEncodingKey;
        user1.CommunityNickname='chequ';
        user1.MobilePhone='12345678912';
        user1.IsActive = true;
        user1.UserRoleId=ur1.Id ;
        list_user.add(user1);
        User user2=new User();
        user2.username='user2r2@223.com';
        user2.LastName='user2r2';
        user2.Email='user2r2@223.com';
        user2.Alias=user[0].Alias;
        user2.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user2.ProfileId=UserInfo.getProfileId();
        user2.LocaleSidKey=user[0].LocaleSidKey;
        user2.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user2.EmailEncodingKey=user[0].EmailEncodingKey;
        user2.CommunityNickname='chequ1';
        user2.MobilePhone='22345678922';
        user2.IsActive = true;
        user2.UserRoleId=ur2.Id ;
        list_user.add(user2);
        insert list_user;
        //Opportunity
        List<Opportunity> list_Opp = new List<Opportunity>();
        Opportunity objOpp1 = new Opportunity();
        objOpp1.Name = 'test opp';
        objOpp1.Type = '新业务' ;
        objOpp1.StageName = '发现/验证机会';
        objOpp1.ProductType__c = 'IVS' ;
        objOpp1.CloseDate = date.today().addMonths(1);
        objOpp1.OwnerId = user2.Id;
        list_Opp.add(objOpp1);
        insert list_Opp ;
        
        //OppEvaluation__c
        List<OppEvaluation__c> list_oppEva = new List<OppEvaluation__c>();
        OppEvaluation__c oppEva1 = new OppEvaluation__c();
        oppEva1.Year__c = String.valueOf(date.today().year());
        oppEva1.Month__c = String.valueOf(date.today().month());
        oppEva1.Commentator__c = user1.Id;
        oppEva1.Opportunity__c = objOpp1.Id;
        oppEva1.BeCommentUser__c = user2.Id ;
        oppEva1.Score__c = '4';
        list_oppEva.add(oppEva1);
        OppEvaluation__c oppEva2 = new OppEvaluation__c();
        oppEva2.Year__c = String.valueOf(date.today().year());
        oppEva2.Month__c = String.valueOf(date.today().addMonths(-1).month());
        oppEva2.Commentator__c = userinfo.getUserId();
        oppEva2.Commentator__c = user1.Id;
        oppEva2.Opportunity__c = objOpp1.Id;
        oppEva2.BeCommentUser__c = user2.Id ;
        oppEva2.Score__c = '4';
        list_oppEva.add(oppEva2);
        insert list_oppEva ;
        
        system.Test.startTest();
        //system.runAs(user0){
        	Apexpages.currentPage().getParameters().put('year' , String.valueOf(date.today().year()));
        	Apexpages.currentPage().getParameters().put('month' , String.valueOf(date.today().month()));
        	Apexpages.currentPage().getParameters().put('uid' , user0.id);
        	Apexpages.currentPage().getParameters().put('urid' , ur0.Id);
            Apexpages.Standardcontroller controll = new Apexpages.Standardcontroller(objOpp1);
            Ctrl_OppStrategyCommentLevelTwoForAdmin oppEva = new Ctrl_OppStrategyCommentLevelTwoForAdmin(controll);
            oppEva.getMonths();
            oppEva.getNewMonths();
            oppEva.strIndex = '0';
            oppEva.saveComment();
            oppEva.editComment();
            oppEva.cancelComment();
        //}
        
        system.Test.stopTest();
    }
}