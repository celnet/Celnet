/*
Author: Eleen
Created on: 2012-2-7
Description: 
	This batch include the method to save role history.
	It is run by V2_ClsSaveRoleHistory. 
*/
global class V2_ClsSaveRoleHistoryBatch implements Database.Batchable<UserRole>
{
	
	global List<UserRole> list_ur;
	global String str_year;
	global String str_month;
	global Map<ID,String> map_GBU;
	global Map<ID,String> map_Level;
	global Map<ID,String> map_Territory;
	global Map<ID,String> map_PG;
	global List<V2_RoleHistory__c> list_vr;
	
	global V2_ClsSaveRoleHistoryBatch()
	{
		list_vr=new List<V2_RoleHistory__c>();
	}
	
	global List<UserRole> start(Database.BatchableContext BC){return list_ur;}
	
	global void execute(Database.BatchableContext BC, List<UserRole> scope)
	{
		for(UserRole ur:scope)
		{
			User user=V2_ClsSaveRoleHistory.GetUserMethod(ur.Users);//Get User
			if(user.Name!=null)
			{
				V2_RoleHistory__c vr=new V2_RoleHistory__c();
				vr.Name=str_year+'-'+str_month+'-'+user.Name;
				vr.Key__c=str_year+'-'+str_month+'-'+user.Id;
				vr.Year__c=str_year;
				vr.Month__c=str_month;
				vr.Name__c=user.Id;
				vr.IsLeave__c=user.IsLeave__c;
				vr.IsOnHoliday__c=user.IsOnHoliday__c;
				vr.Role__c=ur.Name;
				vr.Department__c=user.Department;
				vr.Manager__c=user.ManagerId;
				vr.Renal_valid_super__c=user.Renal_valid_super__c;
				vr.EmployeeNumber__c=user.EmployeeNumber;
				if(user.isActive)
					vr.Status__c='启用';
				else
					vr.Status__c='停用';
				if(map_GBU.containsKey(ur.Id))vr.GBU__c=map_GBU.get(ur.Id);//Get GBU
				if(map_Level.containsKey(ur.Id))vr.Level__c=map_Level.get(ur.Id);//Get Level
				if(map_Territory.containsKey(ur.Id))vr.Territory_Name__c=map_Territory.get(ur.Id);//Get Territory
				if(map_PG.containsKey(ur.Id))vr.PG__c=map_PG.get(ur.Id);//Get PG
				if(vr.Level__c!=null)
				{
					//init UserId collection
					Map<String,Id> map_Id=new Map<String,Id>();
					//Get ParentRole
					if(ur.ParentRoleId!=null)map_Id=V2_ClsSaveRoleHistory.GetRoleTree(ur.ParentRoleId);
					//Get SalesName
					if(vr.Level__c=='BU Head')vr.SalesName7__c=user.Id;
					else if(vr.Level__c=='National')
					{
						vr.SalesName6__c=user.Id;
						if(map_Id.keySet().contains('BU Head'))vr.SalesName7__c=map_Id.get('BU Head');
						else vr.SalesName7__c=null;
					}
					else if(vr.Level__c=='Regional')
					{
						vr.SalesName5__c=user.Id;
						if(map_Id.keySet().contains('National'))vr.SalesName6__c=map_Id.get('National');
						else vr.SalesName6__c=null;
						if(map_Id.keySet().contains('BU Head'))vr.SalesName7__c=map_Id.get('BU Head');
						else vr.SalesName7__c=null;
					}
					else if(vr.Level__c=='Area ASM')
					{
						vr.SalesName4__c=user.Id;
						if(map_Id.keySet().contains('Regional'))vr.SalesName5__c=map_Id.get('Regional');
						else vr.SalesName5__c=null;
						if(map_Id.keySet().contains('National'))vr.SalesName6__c=map_Id.get('National');
						else vr.SalesName6__c=null;
						if(map_Id.keySet().contains('BU Head'))vr.SalesName7__c=map_Id.get('BU Head');
						else vr.SalesName7__c=null;
					}
					else if(vr.Level__c=='District DSM')
					{
						vr.SalesName3__c=user.Id;
						if(map_Id.keySet().contains('Area ASM'))vr.SalesName4__c=map_Id.get('Area ASM');
						else vr.SalesName4__c=null;
						if(map_Id.keySet().contains('Regional'))vr.SalesName5__c=map_Id.get('Regional');
						else vr.SalesName5__c=null;
						if(map_Id.keySet().contains('National'))vr.SalesName6__c=map_Id.get('National');
						else vr.SalesName6__c=null;
						if(map_Id.keySet().contains('BU Head'))vr.SalesName7__c=map_Id.get('BU Head');
						else vr.SalesName7__c=null;
					}
					else if(vr.Level__c=='Supervisor')
					{
						/*if(user.Renal_valid_super__c)vr.SalesName1__c=user.Id;
						else vr.SalesName2__c=user.Id;*/
						vr.SalesName2__c=user.Id;
						if(map_Id.keySet().contains('District DSM'))vr.SalesName3__c=map_Id.get('District DSM');
						else vr.SalesName3__c=null;
						if(map_Id.keySet().contains('Area ASM'))vr.SalesName4__c=map_Id.get('Area ASM');
						else vr.SalesName4__c=null;
						if(map_Id.keySet().contains('Regional'))vr.SalesName5__c=map_Id.get('Regional');
						else vr.SalesName5__c=null;
						if(map_Id.keySet().contains('National'))vr.SalesName6__c=map_Id.get('National');
						else vr.SalesName6__c=null;
						if(map_Id.keySet().contains('BU Head'))vr.SalesName7__c=map_Id.get('BU Head');
						else vr.SalesName7__c=null;
					}
					else if(vr.Level__c=='Rep')
					{
						vr.SalesName1__c=user.Id;
						if(map_Id.keySet().contains('Supervisor'))vr.SalesName2__c=map_Id.get('Supervisor');
						else vr.SalesName2__c=null;
						if(map_Id.keySet().contains('District DSM'))vr.SalesName3__c=map_Id.get('District DSM');
						else vr.SalesName3__c=null;
						if(map_Id.keySet().contains('Area ASM'))vr.SalesName4__c=map_Id.get('Area ASM');
						else vr.SalesName4__c=null;
						if(map_Id.keySet().contains('Regional'))vr.SalesName5__c=map_Id.get('Regional');
						else vr.SalesName5__c=null;
						if(map_Id.keySet().contains('National'))vr.SalesName6__c=map_Id.get('National');
						else vr.SalesName6__c=null;
						if(map_Id.keySet().contains('BU Head'))vr.SalesName7__c=map_Id.get('BU Head');
						else vr.SalesName7__c=null;
					}
				}
				list_vr.add(vr);
			}
		}
		if(list_vr.size()>0)upsert list_vr Key__c;
	}
	global void finish(Database.BatchableContext BC){
		
	}
}