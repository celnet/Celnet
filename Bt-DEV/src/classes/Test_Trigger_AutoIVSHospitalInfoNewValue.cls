/**
 * author : bill create
 * date : 2013-8-5
 * AutoIVSHospitalInfoNewValue的测试类
 */
@isTest
private class Test_Trigger_AutoIVSHospitalInfoNewValue {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Account acc= new Account();
        acc.Name = 'testing 2013-8-5';
        insert acc;
        
        IVT_salesAmount__c sale = new IVT_salesAmount__c();
        sale.time__c = date.today();
        sale.Q1TotalSales__c = 1000;
        sale.totalsales__c = 2000;
        sale.Account__c = acc.Id;
        insert sale;
        
        
        IVSHospitalInfo__c ivt = new IVSHospitalInfo__c();
        ivt.Account__c = acc.Id;
        ivt.Year__c = '2013';
        system.Test.startTest();
        insert ivt;
        IVSHospitalInfo__c ivt1 = new IVSHospitalInfo__c();
        ivt1.Account__c = acc.Id;
        ivt1.Year__c = '2013';
        try{
        insert ivt1;
        }catch(Exception e){}
        system.Test.stopTest();
    }
}