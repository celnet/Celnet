/* 
 * Author: Sunny
 * Created on: 2011-12-26
 * Description: 
 *修改日期：2012-1-11
 *修改人：scott
 *新加功能，保存并提交审批。
 */
public without sharing class V2_CreateLeadTeam_Con 
{
	public String Month{get;set;}
	public String Year{get;set;}
	public V2_Lead_Team__c objLeadTeam{get;set;}
	public boolean blnIsMesShow{get;set;}
	public boolean blnFieldShow{get;set;}
	public boolean blnError  ;
	public string strMessage{get;set;}
	public String strIds{get;set;}
	private Set<ID> set_UserIds = new Set<ID>() ;
	private Set<String> set_StrAccountUser = new Set<String>() ;
	private Set<String> set_StrLeadUser = new Set<String>() ;
	private Lead objLead ;
	private Boolean IsBuAdmin ;
	public List<SelectOption> ListYears{get;set;}
	//完成 按钮失效
	public Boolean Isdisabled{get;set;}
	
	public List<SelectOption> getListMonths()
    {
        list<SelectOption> options=new list<SelectOption>();
        options.add(new SelectOption('1','1'));options.add(new SelectOption('2','2'));
        options.add(new SelectOption('3','3'));options.add(new SelectOption('4','4'));
        options.add(new SelectOption('5','5'));options.add(new SelectOption('6','6'));
        options.add(new SelectOption('7','7'));options.add(new SelectOption('8','8'));
        options.add(new SelectOption('9','9'));options.add(new SelectOption('10','10'));
        options.add(new SelectOption('11','11'));options.add(new SelectOption('12','12'));
        return options;
    }
	public V2_CreateLeadTeam_Con(ApexPages.StandardController controller)
	{
		User objU = [Select Id,ProfileId,Profile.Name From User Where Id =: UserInfo.getUserId()];
		if(objU.Profile.Name.toUpperCase().contains('ADMIN') || objU.Profile.Name == '系统管理员'){
			IsBuAdmin = true;
		}else{
			IsBuAdmin = false ;
		}
		//2011-1-11
		Isdisabled= false;
		
		strIds = ApexPages.currentPage().getParameters().get('camid');
		blnError = false ;
		this.initSubordinate() ;
		this.checkMid(strIds) ;
		system.debug('have error?'+blnError+set_UserIds.size()) ;
		if(!blnError)
		{
			blnFieldShow = true ;
			objLeadTeam = new V2_Lead_Team__c() ;
		}else
		{
			blnFieldShow = false ;
			blnIsMesShow = true ;
		}
		//init year
		ListYears = new  List<SelectOption>();
        ListYears.add(new SelectOption(String.valueOf(date.today().addYears(-1).year()),String.valueOf(date.today().addYears(-1).year())));
        ListYears.add(new SelectOption(String.valueOf(date.today().year()),String.valueOf(date.today().year())));
        ListYears.add(new SelectOption(String.valueOf(date.today().addYears(1).year()),String.valueOf(date.today().addYears(1).year())));
		//init current year & month
		Year = String.valueOf(date.today().year());
        Month = String.valueOf(date.today().Month());
		
	}

	public PageReference doCancel()
	{
		return new PageReference('/'+strIds) ;
	}
	private void checkMid(String strLeadId)
	{
		if(strLeadId == null)
		{
			return ;
		}
		List<Account> list_Accs ;
		List<V2_Account_Team__c> list_AccTeam ;
		objLead = [select id,V2_Mid__c,V2_ApprovalStauts__c from Lead where id =: strLeadId] ;
		if(objLead.V2_ApprovalStauts__c == '审批中')
		{
			blnError = true ;
			strMessage = '请在审批结束后添加小组成员。' ;
			//2011-1-11
			Isdisabled= true;
			return ;
		}
		//查询当前Lead上是否有未同步到Account的相同成员
		for(V2_Lead_Team__c objV2Lts : [select id,V2_User__c from V2_Lead_Team__c where V2_Lead__c =: strLeadId And V2_IsSyn__c = false])
		{
			set_StrLeadUser.add(objV2Lts.V2_User__c) ;
		}
		//通过MID查询是否有匹配该Lead的Account
		if(objLead.V2_Mid__c != null)
		{
			list_Accs = [select Id from Account where Mid__c =: objLead.V2_Mid__c] ;
		}else
		{
			list_Accs = [select id from Account where V2_Lead__c =: objLead.Id] ;
		}
		//找到匹配的Account，查询该Account下是的成员
		if(list_Accs != null && list_Accs.size() != 0)
		{
			for(V2_Account_Team__c objAT : [select id,V2_User__c,V2_Role__c,V2_Is_Delete__c from V2_Account_Team__c where V2_Account__c =: list_Accs[0].Id and V2_History__c = false])
			{
				if(!objAT.V2_Is_Delete__c)
				{
					set_StrAccountUser.add(objAT.V2_User__c) ;
				}
			}
			//查找真实客户小组内是否有该主管的下属
			for(AccountTeamMember objATM : [Select a.UserId From AccountTeamMember a where AccountId =: list_Accs[0].Id])
			{
				if(set_UserIds.contains(objATM.UserId))
				{
					blnError = true ;
					strMessage = '请到相应客户添加客户小组成员。' ;
					//2011-1-11
					Isdisabled= true;
				}
			}
		}
		
	}
	//查找当前用户的下属
	private void initSubordinate()
	{
		V2_UtilClass clsUtilClass = new V2_UtilClass() ;
		List<ID> list_UserId = new List<ID>() ;
		list_UserId = clsUtilClass.getSubordinateIds(UserInfo.getUserRoleId()) ;
		if(list_UserId != null && list_UserId.size() != 0)
		{
			set_UserIds.addAll(list_UserId) ;
		}
		/*
		List<Id> list_UserRoleId = new List<Id>() ;
		for(UserRole objUserRole : [Select u.Id, u.ParentRoleId From UserRole u Where u.ParentRoleId =: UserInfo.getUserRoleId()])
		{
			list_UserRoleId.add(objUserRole.Id) ;
		}
		if(list_UserRoleId.size() != 0)
		{
			for(User objUser : [select Id from User where UserRoleId in: list_UserRoleId])
			{
				set_UserIds.add(objUser.Id) ;
			}
		}	
		*/
	}
	//2012-1-11新加功能，提交并审批。
	public PageReference saveChange2()
	{
		try
		{
			if(blnError)
			{
				return null ;
			}
			boolean blnErrors = false;
			if(objLeadTeam.V2_User__c == null)
			{
				blnErrors = true ;
				objLeadTeam.V2_User__c.addError('请填写小组成员') ;
			}
			if(!IsBuAdmin){
				if(!set_UserIds.contains(objLeadTeam.V2_User__c))
	            {
	                blnErrors = true ;
	                objLeadTeam.V2_User__c.addError('您只能添加您的下属') ;
	            }
	            //生效日期不能在本月之前
	            
	            if(Integer.valueOf(this.Year) == Date.today().year()){
	                if(Integer.valueOf(this.Month) < Date.today().Month()){
	                        blnErrors = true ;
	                        objLeadTeam.V2_User__c.addError('生效日期不能在当月之前。') ;
	                    
	                }
	                
	                
	            } else{
	                if(Integer.valueOf(this.Year) < Date.today().year()){
	                        blnErrors = true ;
	                        objLeadTeam.V2_User__c.addError('生效日期不能在当月之前。') ;
	                    
	                }
	            }
			}
			
			
			if(blnErrors)
			{
				return null ;
			}
			objLeadTeam.V2_Lead__c = this.strIds ;
			objLeadTeam.V2_Effective_Month__c = this.Month ;
			objLeadTeam.V2_Effective_Year__c = this.Year ;
			insert objLeadTeam ;
			objLead.V2_ApprovalStauts__c = '待审批' ;
			update objLead ;
			//提交审批
			Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
	        req.setObjectId(strIds);//客户联系人或自定义对象
	       	Approval.ProcessResult result = Approval.process(req);
			
			return new PageReference('/'+strIds) ;
		}catch(Exception e)
		{
			 ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING , e.getMessage());            
             ApexPages.addMessage(msg);
             return null;
		}
		
	}
	/**
	测试
	**/
	static testMethod void TestPage() 
	{
		//----------------New UserRole ------------------
    	//List<UserRole> list_userRole = new List<UserRole >() ;
    	UserRole objUserRole = new UserRole() ;
    	objUserRole.Name = 'Renal-Rep-大上海-PD-Rep(陈喆令)' ;
    	insert objUserRole ;
    	UserRole objUserRole2 = new UserRole() ;
    	objUserRole2.Name = 'Renal-Rep-大上海-PB-Rep(陈喆令)' ;
    	objUserRole2.ParentRoleId = objUserRole.Id ;
    	insert objUserRole2 ;
		//----------------Create User-------------
    	List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.UserRoleId = objUserRole2.Id ;
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	use2.UserRoleId = objUserRole2.Id ;
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.UserRoleId = objUserRole.Id ;
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
    	use3.ManagerId = use2.Id ;
    	update use3;
    	//-------------New Lead------------------
    	List<Lead> list_lead = new List<Lead>() ;
    	Lead objLead = new Lead() ;
    	objLead.LastName = 'testlead' ;
    	objLead.FirstName = 'T' ;
    	objLead.Company = 'testcom' ;
    	objLead.V2_Mid__c = 'poiuuiop' ;
    	objLead.V2_ApprovalStauts__c = '待审批' ;
    	list_lead.add(objLead) ;
    	insert list_lead ;
    	//---------------New Account-------------------
    	List<Account> list_Acc = new List<Account>() ;
    	Account objAcc = new Account() ;
    	objAcc.Name = 'testac' ;
    	objAcc.MID__c = 'poiuuiop' ;
    	insert objAcc ;
    	//------------------Start Test-----------------------
    	system.test.startTest() ;
    	system.runAs(use3)
    	{
    		ApexPages.StandardController STController = new ApexPages.StandardController(new V2_Lead_Team__c());
			ApexPages.currentPage().getParameters().put('camid', objLead.Id);
			V2_CreateLeadTeam_Con CreateLeadTeam = new V2_CreateLeadTeam_Con(STController);
			CreateLeadTeam.getListMonths() ;
			CreateLeadTeam.objLeadTeam.V2_User__c = use1.Id ;
			CreateLeadTeam.doCancel() ;
			CreateLeadTeam.saveChange2() ;
    	}
    	system.test.stopTest() ;
	}
}