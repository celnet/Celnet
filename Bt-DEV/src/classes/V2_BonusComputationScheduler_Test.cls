@isTest
private class V2_BonusComputationScheduler_Test {

    static testMethod void myUnitTest() {
        Test.startTest();
        V2_BonusComputationScheduler bcs = new V2_BonusComputationScheduler();
        string startTime='0 0 2 * * ?';
        System.schedule('test',startTime,bcs);
        Test.stopTest();
    }
}