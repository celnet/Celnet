/*
*作者：Tobe
*时间：2013-10-26
*功能：医院销售维度表，从医院的销售医院关系下获取自医院创建日期起，每个月的医院与销售维度
* bill add：每个部门的负责情况都添加，清楚知道每个部门的负责情况
* 2014-03-05 Sunny add:销售医院维度表的唯一键不应该是《医院+销售+年+月+产品》应该是《医院+年+月+产品》，否则无法体现销售的变化。
* 
*/
global class PM_SaleHospitalDimensionBatch implements Database.Batchable<sObject>
{
	global Database.QueryLocator start(Database.BatchableContext BC)
	{ 
		return Database.getQueryLocator([Select Id,CreatedDate
										 From Account 
										 where RecordType.DeveloperName = 'RecordType']);
	}
	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		//销售医院维度表list：PM_Sale_Hospital_Dimension__c
		map<string,PM_Sale_Hospital_Dimension__c> HospitalSaleDim_list = new map<string,PM_Sale_Hospital_Dimension__c>();
		
		Set<ID> set_AccIds = new Set<ID>();
		//医院循环
		for(sObject sObj : scope)
		{
			Account acc = (Account)sObj;
			set_AccIds.add(acc.Id);
		}
		
		//1.新增操作V2_BatchOperate__c，V2_ApprovalStatus__c审批状态是审批通过
		//2.替换操作V2_BatchOperate__c，不需要审批状态
		//3.删除操作V2_BatchOperate__c，不需要审批状态
		//医院下的医院与销售关系循环
		for(V2_Account_Team__c SHRelationship:[Select V2_Account__r.CreatedDate,Id,V2_Account__c,V2_User__c,UserProduct__c ,V2_Effective_Year__c,
												V2_Effective_Month__c,V2_Delete_Year__c,V2_Delete_Month__c
											 From V2_Account_Team__c 
											 where V2_Account__c IN :set_AccIds AND (NOT(V2_User__r.UserRole.Name LIKE '%BIOS%')) AND
											 ((V2_BatchOperate__c = '新增' and V2_ApprovalStatus__c = '审批通过') 
											 OR V2_BatchOperate__c IN ('替换','删除')) order by createddate])
		{
			//开始时间，及某销售在负责该医院的生效时间区间段的左闭区间
			Date startDate;
			if(SHRelationship.V2_Effective_Year__c == null || SHRelationship.V2_Effective_Year__c == ''
		    || SHRelationship.V2_Effective_Month__c == null || SHRelationship.V2_Effective_Month__c == '')
			{	//如果生效日期为空或空字符，左闭区间为医院的创建时间
				startDate = date.newinstance(SHRelationship.V2_Account__r.CreatedDate.year(), SHRelationship.V2_Account__r.CreatedDate.month(), 1);
			}
			else
			{
				//否则，左闭区间为生效日期
				startDate = date.newinstance(integer.valueOf(SHRelationship.V2_Effective_Year__c), integer.valueOf(SHRelationship.V2_Effective_Month__c), 1);
			}
			
			
			//结束时间，及某销售在负责该医院的生效时间区间段的右开区间
			Date endDate;
			if(SHRelationship.V2_Delete_Year__c != null && SHRelationship.V2_Delete_Year__c != ''
			&& SHRelationship.V2_Delete_Month__c != null && SHRelationship.V2_Delete_Month__c != '')
			{
				//如果失效日期不为空，右开区间为失效日期
				endDate = date.newinstance(integer.valueOf(SHRelationship.V2_Delete_Year__c), integer.valueOf(SHRelationship.V2_Delete_Month__c), 1);
			}
			else
			{
				//如果失效日期为空，右开区间为明年的第一个月
				endDate = date.newinstance(date.today().addYears(1).Year(), 1, 1);
			}
			//循环当前医院下某销售的负责月份
			/**********************过滤掉失效年月与生效年月相等的负责信息***********************************/
			if(SHRelationship.V2_Effective_Year__c != SHRelationship.V2_Delete_Year__c && SHRelationship.V2_Effective_Month__c != SHRelationship.V2_Delete_Month__c)
			{
				for(Date ResponsibleDate = startDate; ResponsibleDate < endDate;ResponsibleDate = ResponsibleDate.addMonths(1))
				{
					//销售医院维度表实例化
					PM_Sale_Hospital_Dimension__c HospitalSaleDim = new PM_Sale_Hospital_Dimension__c();
					//“SBU”字段取销售医院关系上的部门
					HospitalSaleDim.PM_UserProduct__c = SHRelationship.UserProduct__c;
					//“年月”字段取循环当前的日期
					HospitalSaleDim.PM_Date__c = ResponsibleDate;
					//“销售”字段取销售医院关系上的销售
					HospitalSaleDim.PM_Saler__c = SHRelationship.V2_User__c;
					//“医院”字段取当前医院ID
					HospitalSaleDim.PM_Account__c = SHRelationship.V2_Account__c;
					//外部ID：医院ID+循环当前年+循环当前月
					//2014-03-05 sunny :销售医院维度表的唯一键不应该是《医院+销售+年+月+产品》应该是《医院+年+月+产品》，否则无法体现销售的变化。
					HospitalSaleDim.PM_UniquelyId__c = string.valueOf(SHRelationship.V2_Account__c)
													//+ string.valueOf(HospitalSaleDim.PM_Saler__c)
													+ string.valueOf(ResponsibleDate.year())
													+ string.valueOf(ResponsibleDate.month())
													+ SHRelationship.UserProduct__c;
					HospitalSaleDim_list.put(HospitalSaleDim.PM_UniquelyId__c,HospitalSaleDim);
				}
			}
			/**********************过滤掉失效年月与生效年月相等的负责信息***********************************/
		}
		 
		if(HospitalSaleDim_list.size()>0)
		{
			upsert HospitalSaleDim_list.values() PM_UniquelyId__c;
		}
	}
	global void finish(Database.BatchableContext BC)
	{
    	
    }
}