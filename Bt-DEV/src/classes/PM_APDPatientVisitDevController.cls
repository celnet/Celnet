/*
 * 作者:Tobe
 * 日期：2013-9-25
 * 说明：APD病人拜访列表
 */

public class PM_APDPatientVisitDevController 
{
    //初始页面显示列表的查询条件
    private string query = 'Select  PM_BVTime1__c,PM_BVTime2__c,PM_BVTime3__c,PM_BVTime4__c,PM_BVTime5__c,PM_BVTime6__c,PM_Pdtreatment__c,PM_Status__c, PM_SortDate__c, PM_InDate__c,Name,PM_Sex__c,PM_PmTel__c,PM_PmPhone__c,PM_FamilyTel__c,PM_FamilyPhone__c,PM_HomeTel__c,PM_OtherTel2__c,PM_TelPriorityFirst__c,PM_TelPrioritySecond__c,PM_Address__c,PM_Current_Saler__c,PM_VisitState__c,PM_LastFailDate2__c,PM_LastFailTime2__c ,PM_VisitFailCount__c,PM_LastSuccessDate2__c,PM_LastSuccessTime2__c From PM_Patient__c  Where isDeleted=false and PM_Pdtreatment__c = \'APD\' and PM_Status__c <> \'Dropout\' ';
    public long Limit_Size
    {
    	get
    	{
    		return PM_QueryUtil.LIMIT_SIZE;
    	}
    	set;
    }
    public PM_Patient__c Patient{get;set;}//病人编号，姓名，医院  
    //基本信息字段  姓名{Name} 当前负责销售{PM_Current_Saler__c} 失败次数{PM_VisitFailCount__c}
    public string MobilePhone{get;set;}//联系电话
    public string PatientStatus = 'APD';//病人状态
    //联动大区、省份、城市、县
    public string BigArea{get;set;}
    public string Province{get;set;}
    public string City{get;set;}
    public string County{get;set;}
    public string PatientProvince{get;set;}
    public string PatientCity{get;set;}
    public string PatientCounty{get;set;}
    public string InHospital{get;set;}//插管医院
    //日期
    public string CreatedDate_Start{get;set;}//病人创建日期开始
    public string CreatedDate_End{get;set;}//病人创建日期截止
    public string InDate_Start{get;set;}//插管日期开始
    public string InDate_End{get;set;}//插管日期结束
    //public string PD_Start{get;set;}//腹透日期开始
    //public string PD_End{get;set;}//腹透日期结束
    public string BestVisitTime{set;get;} //最佳拜访时间
    public string Age_Lower{set;get;} //年龄下限
	public string Age_Upper{set;get;} //年龄上限
	public string Visit_Interval{set;get;}//要求拜访隔天数
	public List<SelectOption> getVisit_Intervals() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('--无--','--无--'));
            options.add(new SelectOption('空','空'));
            options.add(new SelectOption('小于31天','小于31天'));
            options.add(new SelectOption('31-60天','31-60天'));
            options.add(new SelectOption('61-90天','61-90天'));
            options.add(new SelectOption('91-180天','91-180天'));
            options.add(new SelectOption('大于180天','大于180天'));
            return options;
    }  
    //查询工具类
    public PM_QueryUtil util = new PM_QueryUtil();
    private PM_PatientSortCommom patientSort= new PM_PatientSortCommom();
    //建议可拨打时间
	public string SuggestVisitTime{get;set;}
 	public List<SelectOption> getItems() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('',''));
            options.add(new SelectOption('9:00-10:00','9:00-10:00'));
            options.add(new SelectOption('10:01-11:00','10:01-11:00'));
            options.add(new SelectOption('11:01-12:00','11:01-12:00'));
            options.add(new SelectOption('12:01-13:00','12:01-13:00'));
            options.add(new SelectOption('13:01-15:00','13:01-15:00'));
            options.add(new SelectOption('15:01-17:00','15:01-17:00'));
            options.add(new SelectOption('17:01-18:00','17:01-18:00'));
            options.add(new SelectOption('18:01-20:00','18:01-20:00'));
            return options;
    }
    public boolean isSort = false; //是否为自定义排序
    public list<PM_ExportFieldsCommon.Patient> list_Patient//经筛选查询出来的信息
    {
        get
        {
            list_patient = new list<PM_ExportFieldsCommon.Patient>();
            if(isSort)
			{
				for(PM_PatientSortCommom.ComparableClass psort : patientSort.PatientSortNormal((list<PM_Patient__c>)conset.getRecords()))
				{
					list_patient.add(psort.p);
				}
			}
			else
			{
				for(PM_PatientSortCommom.ComparableClass psort : patientSort.PatientSort((list<PM_Patient__c>)conset.getRecords()))
				{
					list_patient.add(psort.p);
				}
			}
            list_PatientExport.addAll(list_patient);
            result = list_patient.size();
            return list_patient;    
        }
        set;
    }
    //页面查询需要的字段
    //实现分页功能
    //构造器
    public PM_APDPatientVisitDevController(ApexPages.StandardController controller)
    {
        Patient = new PM_Patient__c();
        getExportField();
    }
    // 分页字段 
    public integer result{get;set;}//结果个数
    public Boolean hasPrevious {get {return conset.getHasPrevious();}set;}
    public Boolean hasNext {get {return conset.getHasNext();}set;}
    public Integer pageNumber {get {return conset.getPageNumber();}set;}
    public Integer categoryNumber {get {return conset.getResultSize();}set;}
    public ApexPages.StandardSetController conset //实现分页功能 ApexPages.StandardSetController
    {
        get 
        {
            if(conset == null)
            {
            	if(Userinfo.getProfileId() == util.PSR_Admin_Profile.Id || Userinfo.getProfileId() == util.PSR_Hotline_Profile.Id)
            	{
            		query += ' and OwnerId = \''+ Userinfo.getUserId()+'\' ';
            	}
                conset = util.getSortConset(query + 'Order By PM_LastSuccessDateTime__c asc ');
                conset.setPageSize(PM_QueryUtil.pageSize);
            }
            return conset;
        }
        set;
    }
    //分页方法
    public void first() {conset.first();}
    public void last() {conset.last();}
    public void previous() {conset.previous();}
    public void next() {conset.next();}
    
    //查询方法
    public void Check()
    {
    	isSort = false;
        string field = ' PM_BVTime1__c,PM_BVTime2__c,PM_BVTime3__c,PM_BVTime4__c,PM_BVTime5__c,PM_BVTime6__c,PM_Pdtreatment__c,PM_InDate__c,PM_SortDate__c,PM_Status__c,Name,PM_Sex__c,PM_PmTel__c,PM_PmPhone__c,PM_FamilyTel__c,PM_FamilyPhone__c,PM_HomeTel__c,PM_OtherTel2__c,PM_TelPriorityFirst__c,PM_TelPrioritySecond__c,PM_Address__c,PM_Current_Saler__c,PM_VisitState__c,PM_LastFailDate2__c,PM_LastFailTime2__c ,PM_VisitFailCount__c,PM_LastSuccessDate2__c,PM_LastSuccessTime2__c,PM_InNurser__c ';
        map<string,string> map_Condition = new map<string,string>();
        map_Condition.put('Name',Patient.Name);
        map_Condition.put('MobilePhone',MobilePhone);
        map_Condition.put('PM_Current_Saler__c',Patient.PM_Current_Saler__c);
        if(Patient.PM_VisitFailCount__c != null && Patient.PM_VisitFailCount__c != 0)
        {
            map_Condition.put('PM_VisitFailCount__c',string.valueOf(Patient.PM_VisitFailCount__c));
        }
        map_Condition.put('BigArea',BigArea);
        map_Condition.put('Province',Province);
        map_Condition.put('PatientProvince',PatientProvince);
        map_Condition.put('City',City);
        map_Condition.put('PatientCity',PatientCity);
        map_Condition.put('County',County);
        map_Condition.put('PatientCounty',PatientCounty);
        map_Condition.put('PM_Pdtreatment__c',PatientStatus);
        map_Condition.put('PM_Payment__c',Patient.PM_Payment__c);
        map_Condition.put('PM_HighImpac__c',Patient.PM_HighImpac__c);
        map_Condition.put('PM_IsStarPatient__c',Patient.PM_IsStarPatient__c);
        map_Condition.put('PM_HDAndPD__c',Patient.PM_HDAndPD__c);
        map_Condition.put('PM_UsCoGoSameTime__c',Patient.PM_UsCoGoSameTime__c);
        map_Condition.put('PM_Address__c',Patient.PM_Address__c);
        map_Condition.put('PM_HighRisk__c',Patient.PM_HighRisk__c);
        map_Condition.put('CreatedDate_Start',CreatedDate_Start);
        map_Condition.put('CreatedDate_End',CreatedDate_End);
        map_Condition.put('InDate_Start',InDate_Start);
        map_Condition.put('InDate_End',InDate_End);
        map_Condition.put('InHospital',InHospital);
        map_Condition.put('SuggestVisitTime',SuggestVisitTime);
        map_Condition.put('PM_VisitState__c',Patient.PM_VisitState__c);
        map_Condition.put('NotDropoutContactState','');
        map_Condition.put('PSRAdminUser','true');
        map_Condition.put('PM_Current_PSR__c',Patient.PM_Current_PSR__c);
        map_Condition.put('BestVisitTime',BestVisitTime);
        map_Condition.put('Age_Lower',Age_Lower);
        map_Condition.put('Age_Upper',Age_Upper);
        map_Condition.put('PM_Active_PMT__c',Patient.PM_Active_PMT__c);
        map_Condition.put('Visit_Interval',Visit_Interval);
        map_Condition.put('PM_InNurser__c',Patient.PM_InNurser__c);
        util.connectQueryString(field,map_Condition);
        this.conset = util.getSortConset(util.getPatSql()+' Order By PM_LastSuccessDateTime__c asc ');  
    }
     //排序方法
    public string strOrder{get;set;}
    public string strSortField{get;set;}
    public string strPreviousSortField{get;set;}
    //sort the orders by field selected
    public void sortOrders()
    {
    	isSort = true;
        strOrder ='desc';
        if(strSortField==null)
        {
            strSortField='Name';
        }
        if(strPreviousSortField == strSortField)
        {
            strOrder = 'asc';
            strPreviousSortField = null;
        }
        else
        {
            strPreviousSortField = strSortField;
        }
        string patSql = util.getPatSql();
        if(patSql == null || patSql == '')
        {
            patSql = query;
        }
        sortSql(patSql,strSortField,strOrder);
    }
    public void sortSql(String sql,String strSortField,String strOrder)
    {
        String strSqlSort;
        if(strSortField!=null)
        {
            strSqlSort=sql+' order by '+strSortField+' '+strOrder;
        }
        strSqlSort=strSqlSort;
        this.conset = util.sortOrders(strSqlSort); 
    }
    /********************************导出功能Start****************************************/
    //导出排序工具类
    public PM_ExportFieldsCommon field = new PM_ExportFieldsCommon();
    public list<PM_ExportFieldsCommon.Patient> list_PatientExport  = new list<PM_ExportFieldsCommon.Patient>();//导出时需要判断是否选中的list
    //导出excel功能需要的参数  点击导出按钮，原页面隐藏 只显示下面的要导出的列表
    public List<PM_ExportFieldsCommon.ExportField> list_ExportField{get;set;}
    
   // public boolean IsExcel{get;set;}
    private Transient List<PM_Patient__c> list_PatientExcel;
    private Transient List<List<PM_Patient__c>> list_PatientExcellist;
    //public String contentType{get;set;}

    //页面需要显示的字段集合
    private void getExportField() 
    {
        list_ExportField = field.getActivePatientListFields();
        //IsExcel = false;
        //contentType = '';
    }
    
    //导出excel
    public PageReference exportExcel()
    {
        //实例化导出的内容集合
        list_PatientExcel = new List<PM_Patient__c>();
        list_PatientExcellist = new List<List<PM_Patient__c>>();
        //IsExcel = true;
        //确定页面选定的记录
        Set<ID> set_patientIds = new Set<ID>();
        set_patientIds.clear();
        for(PM_ExportFieldsCommon.patient pa : list_PatientExport)
        {
            if(pa.IsExport)
            {
                set_patientIds.add(pa.patient.Id);
            }
        }
        system.debug(set_patientIds.size() + '$$$$$$$$$$$$$$$$$$');
        string strSoql = field.getApiNameFields(list_ExportField);
        String exportSql = strSoql;
        if(set_patientIds.size()>0)
        {
            exportSql += ' Where Id in : set_patientIds';
        }else{
            string patSql = util.patSql;
            system.debug(patSql + '*************************zgxm');
            if(patSql == null || patSql == '')
            {
                if(query.indexOf('Where') >=0)
                {
                    exportSql += query.substring(query.indexOf('Where'), query.length());
                }
            }else{
            	list_PatientExcellist = util.getPatientExcellist(exportSql, patSql);
                /*if(patSql.indexOf('Where') >=0)
                {
                    exportSql += patSql.substring(patSql.indexOf('Where'), patSql.length());
                }*/
            } 
        }
        
        if(list_PatientExcellist.size() <= 0)
        {
	        for(PM_Patient__c p : util.getPatientlist((exportSql+' limit '+PM_QueryUtil.LIMIT_SIZE),set_patientIds))
	        {
	        	if(list_PatientExcel.size() < 999)
	        	{
	        		list_PatientExcel.add(p);
	        	}else{
	        		list_PatientExcel.add(p);
	        		list_PatientExcellist.add(list_PatientExcel);
	        		list_PatientExcel = new List<PM_Patient__c>();
	        	}
	        }
	        list_PatientExcellist.add(list_PatientExcel);
        }
        field.SaveSoql(strSoql, list_PatientExcellist);
        //contentType = 'application/vnd.ms-excel#ActivePatientExcel.xls';
        return new PageReference('/apex/PM_PatientExport');
    }
    /********************************导出功能End****************************************/
}