/** 
 * Author:sunny sun
 * 功能：检查主管是否负责医院，如果是，将用户下的“是否负责医院主管”字段打钩，否去掉勾
①判断主管角色，职位不为REP
②找到有效的销售医院关系（有效：审批通过、不为删除、不是历史）
③有医院的主管，User上的字段打钩，是主管但是没带医院的，User上的钩去掉
**/
global class CheckIsHaveHospitalBatch implements Database.Batchable<sObject>{
	
    global Database.QueryLocator start(Database.BatchableContext BC){
    	//所有非销售的角色ID
    	List<ID> list_RoleId = new List<ID>();
    	for(UserRole ur : [Select Id,Name From UserRole]){
    		if(ur.Name.contains('-')){
    			List<String> list_UserRoleName = ur.Name.split('-');
    			if(list_UserRoleName.size() == 5){
    				if(list_UserRoleName[1].toUpperCase() != 'REP'){
    					list_RoleId.add(ur.Id);
    				}
    			}
    		}
    	}
    	//查询所有非销售的User
        return Database.getQueryLocator([Select Id,Renal_valid_super__c From User Where UserRoleId in: list_RoleId]);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope){
    	//主管ID
    	//Set<ID> set_UserId = new Set<ID>();
    	Map<ID,Boolean> map_SupervisorHaveHos =  new Map<ID,Boolean>();
    	for(sObject sObj : scope){
            User objUser = (User)sObj;
            //set_UserId.add(objUser.Id) ;
            map_SupervisorHaveHos.put(objUser.Id , false);
        }
    	//记录带医院的主管
    	//查询所有有效状态的销售医院关系记录
    	for(V2_Account_Team__c accTeam : [Select Id,V2_NewAccUser__c,V2_User__c 
    	       From V2_Account_Team__c 
    	       Where V2_ApprovalStatus__c = '审批通过' And V2_Is_Delete__c = false And V2_History__c = false And (V2_NewAccUser__c in: map_SupervisorHaveHos.keySet() Or V2_User__c in: map_SupervisorHaveHos.keySet())]){
    		if(accTeam.V2_NewAccUser__c != null){
    			if(map_SupervisorHaveHos.containsKey(accTeam.V2_NewAccUser__c)){
    				map_SupervisorHaveHos.put(accTeam.V2_NewAccUser__c , true);
    			}
    		}else if(accTeam.V2_User__c != null){
    			if(map_SupervisorHaveHos.containsKey(accTeam.V2_User__c)){
                    map_SupervisorHaveHos.put(accTeam.V2_User__c , true);
                }
    		}
    	}
    	//
    	List<User> list_UserUp = new List<User>();
    	for(sObject sObj : scope){
    		User objUser = (User)sObj;
    		if(objUser.Renal_valid_super__c != map_SupervisorHaveHos.get(objUser.Id)){
    			objUser.Renal_valid_super__c = map_SupervisorHaveHos.get(objUser.Id);
    			list_UserUp.add(objUser);
    		}
    	}
    	if(list_UserUp.size()>0){
    		update list_UserUp;
    	}
    	
    }
    global void finish(Database.BatchableContext BC){
    	
    }
    
}