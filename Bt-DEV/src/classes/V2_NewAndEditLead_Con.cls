/* 
 * Author: Sunny
 * Created on: 2011-12-26
 * Description: 
 */
public without sharing class V2_NewAndEditLead_Con 
{
	public Lead objLead{get;set;}
	public String AccCode{get;set;}
	public V2_NewAndEditLead_Con(ApexPages.StandardController controller)
	{
		ID strLeadId = controller.getId() ;
		if(strLeadId != null)
		{
			objLead = [Select V2_Address__c, V2_BillingCity__c, V2_BillingCountry__c, 
				V2_BillingPostalCode__c, V2_BillingState__c, V2_BillingStreet__c, V2_Cities__c, 
				V2_GAccountType__c, V2_Grade__c, V2_InsuranceAmtByYear__c, V2_IsEducation__c, 
				V2_ManTimeHospital__c, V2_MedicineAmtByYear__c, V2_Mid__c,  V2_OperationTime__c, 
				V2_OutPatientAmtByMonth__c, V2_Ownership__c,  V2_PersonInHospital__c, V2_PostCode__c, 
				V2_Provinces__c, V2_RenalMarketType__c, V2_ShippingCity__c, V2_ShippingCountry__c, 
				V2_ShippingPostalCode__c, V2_ShippingState__c, V2_ShippingStreet__c, V2_Sic__c, 
				V2_Site__c, V2_Stauts__c, V2_TickerSymbol__c, V2_TotalBedsActual__c,V2_ApprovalStauts__c ,  
				V2_TotalBedsStandard__c, V2_TotalDoctors__c, V2_AccountNumber__c, V2_Type__c, Phone, OwnerId ,
				Fax, Website, Industry, LastName, Description, Company, AnnualRevenue,  NumberOfEmployees, 
				Rating, Name 
				From Lead Where Id =: strLeadId] ;
			AccCode = objLead.Company ;
		}else
		{
			objLead = new Lead() ;
		}
	}
	public PageReference saveChange() 
	{
		if(AccCode != null && AccCode != '')
		{
			objLead.Company = AccCode ;
		}else
		{
			objLead.Company = objLead.LastName ;
		}
		system.debug('Mid value :'+objLead.V2_Mid__c) ;
		
		if(objLead.Id != null)
		{
			if(objLead.V2_Mid__c != null)
			{
				List<Lead> list_Lead = [Select Id From Lead Where V2_Mid__c =: objLead.V2_Mid__c] ;
				system.debug('list !?:'+list_Lead) ;
				if(list_Lead == null)
				{
					
				}else if(list_Lead.size() > 1)
				{
					objLead.V2_Mid__c.addError('同步ID与记录值重复') ;
					return null ;
				}else if(list_Lead.size() == 1)
				{
					if(objLead.Id != list_Lead[0].Id)
					{
						objLead.V2_Mid__c.addError('同步ID与记录值重复') ;
						return null ;
					}
				}
			}
			update objLead ;
		}else
		{
			if(objLead.V2_Mid__c != null)
			{
				List<Lead> list_Lead = [Select Id From Lead Where V2_Mid__c =: objLead.V2_Mid__c] ;
				system.debug('list !?:'+list_Lead) ;
				if(list_Lead != null && list_Lead.size() != 0)
				{
					objLead.V2_Mid__c.addError('同步ID与记录值重复') ;
					return null ;
				}
			}
			insert objLead ;
		}
		return new PageReference('/'+objLead.Id) ;
	}
	//测试
	static testMethod void TestCreateLead() 
	{
		Lead objLead = new Lead() ;
		objLead.LastName = 'wonf' ;
		objLead.Company = 'wef' ;
		objLead.V2_Mid__c = 'wnfnekl' ;
		//insert objLead ;
		//-------------------------------
		ApexPages.StandardController STController = new ApexPages.StandardController(objLead);
		V2_NewAndEditLead_Con NewLead = new V2_NewAndEditLead_Con(STController);
		
		NewLead.AccCode = 'eieiei' ;
		NewLead.objLead.LastName = 'woenf' ;
		NewLead.objLead.V2_Mid__c = 'testmids' ;
		NewLead.saveChange() ;
		
	}
	static testMethod void TestEditLead() 
	{
		//-------------New Lead------------------
		Lead objLead = new Lead() ;
		objLead.LastName = 'wonf' ;
		objLead.Company = 'wef' ;
		objLead.V2_Mid__c = 'wnfnekl' ;
		insert objLead ;
		//----------------Start Test---------------------
		
		ApexPages.StandardController STController = new ApexPages.StandardController(objLead);
		V2_NewAndEditLead_Con NewLead = new V2_NewAndEditLead_Con(STController);
		
		NewLead.AccCode = 'eieiei' ;
		NewLead.objLead.LastName = 'woenf' ;
		NewLead.saveChange() ;
		
	}
}