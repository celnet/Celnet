/*
 * Author : BIll
 * description : 病人拜访VF选项卡
 * 新病人、病人，热线，邮件，经销商反馈
 */
public class PM_PatientVisitTabController {
    public String strAccessUrl{get;set;}
    public String strNewPatientUrl{get;set;}
    public String strPatientUrl{get;set;}
    public String strHotlineUrl{get;set;}
    public String strMailUrl{get;set;}
    public String strDealerUrl{get;set;}
    public String strQuestionUrl{get;set;}
    public String strpatientPlanUrl{get;set;}
    public String strAreaUrl{get;set;}
    public String strProvinceUrl{get;set;}
    public String strProductFeedbackUrl{get;set;}
    public String strFreeWalkerTPUrl{get;set;} 
    public String strOtherPatientFeedbackUrl{get;set;}
    public String strPSRUrl{get;set;}
    public String strHDUrl{get;set;}
    public String strHospitalProjectUrl{get;set;}
    public String strPlanUrl{get;set;}
    //拜访周期设置只能由系统管理员和PSR Admin点击
    public boolean RoleView{get;set;}
   
    public PM_PatientVisitTabController()
    {
    	RoleView = false;
    	list<Profile> listPro = [Select name from Profile where Id = :UserInfo.getProfileId()];
    	if(listPro != null && listPro.size()>0)
    	{
    		if(listPro[0].Name.toUpperCase().contains('PSR ADMIN') || listPro[0].Name.toUpperCase().contains('PSR MANAGER') || listPro[0].Name.toUpperCase().contains('PM-VICE MANAGER') || listPro[0].Name.IndexOf('管理员')>=0)
    		{
    			RoleView = true;
    		}
    	}
        this.initUrl();
    }
    private void initUrl(){
        //病人调阅申请
        //Schema.DescribeSObjectResult access = PM_Patient_Access__c.SObjectType.getDescribe();
        Schema.DescribeSObjectResult access = Application__c.SObjectType.getDescribe();
        strAccessUrl = '../'+access.getKeyPrefix();
        
        //新病人提交
        Schema.DescribeSObjectResult NewPatient = PatientApply__c.SObjectType.getDescribe();
        strNewPatientUrl = '../'+NewPatient.getKeyPrefix();
        
        //病人
        Schema.DescribeSObjectResult Patient = PM_Patient__c.SObjectType.getDescribe();
        strPatientUrl = '../'+Patient.getKeyPrefix();
        
        //热线接听
        Schema.DescribeSObjectResult Hotline = PM_Hotline__c.SObjectType.getDescribe();
        strHotlineUrl = '../'+Hotline.getKeyPrefix();
        
        //邮寄
        Schema.DescribeSObjectResult Mail = PM_Mail__c.SObjectType.getDescribe();
        strMailUrl = '../'+Mail.getKeyPrefix();
        
        //经销商信息反馈
        Schema.DescribeSObjectResult Dealer = PM_Dealer_Feedback__c.SObjectType.getDescribe();
        strDealerUrl = '../'+Dealer.getKeyPrefix();
        
        //问题题库
        Schema.DescribeSObjectResult Questions = PM_Question__c.SObjectType.getDescribe();
        strQuestionUrl = '../'+Questions.getKeyPrefix();
        
        //问题题库
        Schema.DescribeSObjectResult patientPlan = PM_PatientPlan__c.SObjectType.getDescribe();
        strpatientPlanUrl = '../'+patientPlan.getKeyPrefix();
        
        //区域
        Schema.DescribeSObjectResult BigZone = Region__c.SObjectType.getDescribe();
        strAreaUrl = '../'+BigZone.getKeyPrefix();
        
        //省份
        Schema.DescribeSObjectResult Province = Provinces__c.SObjectType.getDescribe();
        strProvinceUrl = '../'+Province.getKeyPrefix();
        
        //产品反馈
        Schema.DescribeSObjectResult ProductFeedback = PM_Product_Feedback__c.SObjectType.getDescribe();
        strProductFeedbackUrl = '../'+ProductFeedback.getKeyPrefix();
        
        //自由行-TP
        Schema.DescribeSObjectResult FreeWalkerTP = PM_Free_TP_Application__c.SObjectType.getDescribe();
        strFreeWalkerTPUrl = '../'+FreeWalkerTP.getKeyPrefix();
        
        //医院项目
        Schema.DescribeSObjectResult HospitalProject = PM_Hospital_Project__c.SObjectType.getDescribe();
        strHospitalProjectUrl = '../'+HospitalProject.getKeyPrefix();
        
        //其他病人反馈
        Schema.DescribeSObjectResult OtherPatientFeedback = PM_Patient_Feedback__c.SObjectType.getDescribe();
        strOtherPatientFeedbackUrl = '../'+OtherPatientFeedback.getKeyPrefix();
        
        //医院与PSR关系
        Schema.DescribeSObjectResult PM_PSRRelation = PM_PSRRelation__c.SObjectType.getDescribe();
        strPSRUrl = '../' + PM_PSRRelation.getKeyPrefix();
        
        //医院与经销商关系
        Schema.DescribeSObjectResult PM_HDRelation = PM_HosDealerRelation__c.SObjectType.getDescribe();
        strHDUrl = '../' + PM_HDRelation.getKeyPrefix();
        
        //PSR拜访月计划
        Schema.DescribeSObjectResult PM_MonthPlan = PM_PSRMonthPlan__c.SObjectType.getDescribe();
        strPlanUrl = '../' + PM_MonthPlan.getKeyPrefix();
    }

     /*******************test start***********************/
    static testMethod void MyTest() 
    {
        PM_PatientVisitTabController tab = new PM_PatientVisitTabController();
    }
}