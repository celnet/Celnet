/**
 * Author : Bill
 * date : 2013-8-27
 * deacription : IVT业务机会年度销量预估报表，考虑到一家医院可能有多个业务机会，因此我们需要做些改动
 * 若一家医院有多个业务机会，算出单个业务机会的”业务机会成功后的销量预估”, “业务机会按目前可能性销量预估” 然后汇总到医院
 * 其中, 若“产品大类”相同，“目前百特用量”和“开发后百特用量取最大值”；不同“产品大类”则累加
 **/
global with sharing class OpportunityReportDataDistinctBatch implements Database.Batchable<sObject>{
	global integer curMonth;
	private String pointEmail = UserInfo.getUserEmail();
	global Database.QueryLocator start(Database.BatchableContext BC){
		//获取所有业务机会
    	return Database.getQueryLocator([Select o.Id, o.AccountId, o.OwnerId From Opportunity o Where o.RecordType.Name IN ('IVT','IVT Approval')  AND CloseDate = THIS_YEAR AND o.Owner.IsActive = true AND StageName != '关闭 (失败，放弃)' ORDER BY AccountId asc]);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope){
    	//查看业务机会映射表是否已经存在
    	//客户Id集合
    	Set<ID> set_Acc =new Set<ID>();
    	//基本信息的公式字段字段的客户Id集合
    	Set<String> set_AccIds =new Set<String>();
		for(sObject sObj : scope)
		{
			Opportunity opp = (Opportunity)sObj;
			if(opp.AccountId != null)
			{
				set_Acc.add(opp.AccountId);
				string accId = opp.AccountId;
				accId = accId.substring(0,accId.length()-3);
				set_AccIds.add(accId);
			}
		}
    	//业务机会list
    	List<Opportunity> list_opps = [Select o.Id, o.AccountId, o.OwnerId From Opportunity o Where o.RecordType.Name IN ('IVT','IVT Approval')  AND CloseDate = THIS_YEAR and o.AccountId in : set_Acc AND o.Owner.IsActive = true];
    	Map<ID,OpportunityBaseData__c> map_oppBase = new Map<ID,OpportunityBaseData__c>();
    	List<OpportunityBaseData__c> list_oppBase = [Select o.Opportunity__c, o.Id, o.OwnerId From OpportunityBaseData__c o 
    													WHERE AccountId__c IN : set_AccIds 
    													AND O.CloseDate__c = THIS_YEAR 
    													AND DistinctOpp__c = true];
    	for(OpportunityBaseData__c oppBase : list_oppBase)
    	{ 
    		map_oppBase.put(oppBase.Opportunity__c, oppBase);
    	}
    	List<OpportunityBaseData__c> list_newOppBase = new List<OpportunityBaseData__c>();
		for(Opportunity opp : list_opps)
		{
    		if(!map_oppBase.containsKey(opp.Id))
    		{
    			OpportunityBaseData__c newOppBase = new OpportunityBaseData__c();
    			newOppBase.Opportunity__c = opp.Id;
    			newOppBase.OwnerId = opp.OwnerId;
    			newOppBase.DistinctOpp__c = true;
    			list_newOppBase.add(newOppBase);
    		}
    		if(!map_oppBase.isEmpty() && map_oppBase.get(opp.Id) != null)
    		{
    			map_oppBase.get(opp.Id).January__c = null;
				map_oppBase.get(opp.Id).February__c = null;
				map_oppBase.get(opp.Id).March__c = null;
				map_oppBase.get(opp.Id).April__c = null; 
				map_oppBase.get(opp.Id).May__c = null;
				map_oppBase.get(opp.Id).June__c = null;
				map_oppBase.get(opp.Id).July__c = null;
				map_oppBase.get(opp.Id).August__c = null;
				map_oppBase.get(opp.Id).September__c = null;
				map_oppBase.get(opp.Id).October__c = null;
				map_oppBase.get(opp.Id).November__c = null;
				map_oppBase.get(opp.Id).December__c = null;
    			map_oppBase.get(opp.Id).OwnerId = opp.OwnerId;
    		}
		}
		if(list_oppBase != null && list_oppBase.size()>0)
		{
			update list_oppBase;
		}
		if(list_newOppBase != null && list_newOppBase.size()>0)
		{
			insert list_newOppBase;
		}
		
		List<OpportunityBaseData__c> list_aLLOppBase = 
						[Select o.AccountId__c, o.SuccessGap__c, o.Possible__c, o.PossibleGap__c, o.After_Total_Sales__c,
						o.Opportunity__c, o.OppSuccessSalesEstimate__c, o.OppPossibleSalesEstimate__c, o.NowBaxterDosage__c,  
						o.CloseDate__c, o.AnnualIndex__c, o.AfterBaxterDosage__c, o.ProductType__c, o.Id, o.Now_Total_Sales__c
						From OpportunityBaseData__c o WHERE o.DistinctOpp__c = true and O.CloseDate__c = THIS_YEAR and o.AccountId__c IN : set_AccIds];
		//业务机会销量数据子表数据
		Map<string,OppSalesChildData__c> map_childData = new Map<string,OppSalesChildData__c>();
		List<OppSalesChildData__c> list_oppChild = new List<OppSalesChildData__c>();
		
		//去除产品大类相同的业务机会，保留开发后百特用量大的
		//过滤掉产品大类相同的业务机会
		Map<string,OpportunityBaseData__c> map_same = new Map<string,OpportunityBaseData__c>();
		Set<ID> set_sameAcc = new Set<ID>();
		for(OpportunityBaseData__c aLLOppBase : list_aLLOppBase)
		{
			if(!map_same.containsKey(aLLOppBase.AccountId__c + aLLOppBase.ProductType__c))
			{
				map_same.put(aLLOppBase.AccountId__c + aLLOppBase.ProductType__c, aLLOppBase);
			}else{
				if(aLLOppBase.AfterBaxterDosage__c >= map_same.get(aLLOppBase.AccountId__c + aLLOppBase.ProductType__c).AfterBaxterDosage__c)
				{
					map_same.put(aLLOppBase.AccountId__c + aLLOppBase.ProductType__c, aLLOppBase);
				}else{
					continue;
				}
			}
			//创建12月销售数据
			for(integer i = 1; i <= 12; i++)
			{
				integer year = aLLOppBase.CloseDate__c.year();
				integer month = aLLOppBase.CloseDate__c.month();
				if(aLLOppBase.CloseDate__c.day()>=15)
				{
					month = month+1;
				}
				OppSalesChildData__c childData = new OppSalesChildData__c();
				if( month > i)
				{
					childData.OpportunityBaseData__c = aLLOppBase.Id;
					childData.ImportDate__c = date.valueOf(string.valueOf(year)+'-'+ string.valueOf(i)+'-'+'15');
				}else{
					childData.OpportunityBaseData__c = aLLOppBase.Id;
					childData.ImportDate__c = date.valueOf(string.valueOf(year)+'-'+ string.valueOf(i)+'-'+'15');
					childData.SaleQuantity__c = aLLOppBase.AfterBaxterDosage__c;
				}
				string strdata = string.valueOf(aLLOppBase.Id)+string.valueOf(i);
				map_childData.put(strdata, childData);
				list_oppChild.add(childData);
			}
			//基本信息对象的客户Id
			set_Acc.add(aLLOppBase.AccountId__c);
		}
		
		Map<string,SalesTotalData> map_sale = new Map<string,SalesTotalData>();
		//取出本年度导入的所有销售数据
		for(SalesReport__c sale : [Select s.Time__c, s.ActualQty__c, s.TargetQty__c, s.Account__c 
			From SalesReport__c s where s.SBU__c = 'IVT' AND s.Time__c = THIS_YEAR AND ProductGroup__c IN ('Premix', 'BaseIV') AND s.Account__c IN : set_Acc])
		{
			string strSale = string.valueOf(sale.Account__c);
			strSale = strSale.substring(0,strSale.length()-3) + string.valueOf(sale.Time__c.month());
			SalesTotalData saledata = new SalesTotalData();
			saledata.ActualQty = sale.ActualQty__c==null?0:sale.ActualQty__c;
			saledata.TargetQty = sale.TargetQty__c==null?0:sale.TargetQty__c;
			if(!map_sale.containsKey(strSale))
			{
				map_sale.put(strSale, saledata);
			}else{
				map_sale.get(strSale).ActualQty += saledata.ActualQty;
				map_sale.get(strSale).TargetQty += saledata.TargetQty;
			}
		}
		
		//Trend函数严格按照excel去运算
		Map<double,double> map_trend = new Map<double,double>();
		
		for(OpportunityBaseData__c aLLOppBase : map_same.values())
		{
			//结束日期和中间15比较
			integer midmonth = aLLOppBase.CloseDate__c.month();
			if(aLLOppBase.CloseDate__c.day()>=15)
			{
				midmonth = midmonth+1;
			}
			
			//销售数据同步业务机会销售数据
			for(integer i=1; i <= 12; i++)
			{
				OppSalesChildData__c oppC = map_childData.get(aLLOppBase.Id + string.valueOf(i));
				SalesTotalData sr = map_sale.get(aLLOppBase.AccountId__c + string.valueOf(i));
				if(sr != null && oppC != null)
				{
					oppC.TargetQty__c = sr.TargetQty; 
				}
				if(i <= curMonth)
				{
					oppC.SaleQuantity__c = sr != null?sr.ActualQty:0;
					map_trend.put(i,(oppC.SaleQuantity__c == null)?0:oppC.SaleQuantity__c);
				}
			}
			
			//年度目标
			double Target =0;
			//结束日期前的实际销量之和
			double ActQuantity = 0;
			//年度实际销量
			double ActYearQuantity = 0;
			
			for(integer i=1; i <= 12; i++)
			{
				OppSalesChildData__c oppC = map_childData.get(aLLOppBase.Id + string.valueOf(i));
				if(oppC.TargetQty__c != null)
				{
					Target += oppC.TargetQty__c;
				}
				if(i > curMonth)
				{
					oppC.SaleQuantity__c = TrendMethod(i,map_trend)>=0?TrendMethod(i,map_trend):0;
				}
				if(oppC.SaleQuantity__c != null)
				{
					if(i==1)aLLOppBase.January__c = oppC.SaleQuantity__c;
					if(i==2)aLLOppBase.February__c = oppC.SaleQuantity__c;
					if(i==3)aLLOppBase.March__c = oppC.SaleQuantity__c;
					if(i==4)aLLOppBase.April__c = oppC.SaleQuantity__c; 
					if(i==5)aLLOppBase.May__c = oppC.SaleQuantity__c;
					if(i==6)aLLOppBase.June__c = oppC.SaleQuantity__c;
					if(i==7)aLLOppBase.July__c = oppC.SaleQuantity__c;
					if(i==8)aLLOppBase.August__c = oppC.SaleQuantity__c;
					if(i==9)aLLOppBase.September__c = oppC.SaleQuantity__c;
					if(i==10)aLLOppBase.October__c = oppC.SaleQuantity__c;
					if(i==11)aLLOppBase.November__c = oppC.SaleQuantity__c;
					if(i==12)aLLOppBase.December__c = oppC.SaleQuantity__c;
				}
				if(oppC.SaleQuantity__c != null)
				{
					ActYearQuantity += oppC.SaleQuantity__c;
				}
			}
			double nowSale = aLLOppBase.NowBaxterDosage__c!=null?aLLOppBase.NowBaxterDosage__c:0;
			double afterSale = aLLOppBase.AfterBaxterDosage__c!=null?aLLOppBase.AfterBaxterDosage__c:0;
			//开发百特总用量
			aLLOppBase.After_Total_Sales__c = afterSale;
			//目前百特总用量
			aLLOppBase.Now_Total_Sales__c = nowSale;
			//业务机会成功后的年度销量预估
			aLLOppBase.OppSuccessSalesEstimate__c = nowSale*(midmonth-1) + afterSale*(13-midmonth);
			//成功Gap
			aLLOppBase.SuccessGap__c = Target-aLLOppBase.OppSuccessSalesEstimate__c;
			//年度指标
			aLLOppBase.AnnualIndex__c = Target;
			//年度实际销量
			aLLOppBase.ActualQty__c = ActYearQuantity;
			//按目前可能性的年度销量预估
			aLLOppBase.OppPossibleSalesEstimate__c = nowSale*(midmonth-1) + ((afterSale - nowSale)*(aLLOppBase.Possible__c/100) + nowSale)*(13-midmonth);
			//可能性Gap
			aLLOppBase.PossibleGap__c =  Target-aLLOppBase.OppPossibleSalesEstimate__c;
		}
		//upsert map_same.values();
		//system.debug(map_same.values()+'zgxm');
		//相同客户的不同业务机会进行合并
		//按医院累加是指：
		//若一家医院有多个业务机会，算出单个业务机会的”业务机会成功后的销量预估”, “业务机会按目前可能性销量预估” 然后汇总到医院
     	//其中, 若“产品大类”相同，“目前百特用量”和“开发后百特用量取最大值”；不同“产品大类”则累加！     
     	//不同产品大类的基本信息进行累加，累加结束后要删除不需要的基本信息表
		List<OpportunityBaseData__c> list_delBase = new List<OpportunityBaseData__c>();
		Map<String,OpportunityBaseData__c> map_DisOppBase = new Map<String,OpportunityBaseData__c>();
		for(OpportunityBaseData__c distinctBase : map_same.values())
		{
			if(!map_DisOppBase.containsKey(distinctBase.AccountId__c))
    		{
    			map_DisOppBase.put(distinctBase.AccountId__c, distinctBase);
    		}else{
    			map_DisOppBase.get(distinctBase.AccountId__c).After_Total_Sales__c += distinctBase.After_Total_Sales__c;
    			map_DisOppBase.get(distinctBase.AccountId__c).Now_Total_Sales__c += distinctBase.Now_Total_Sales__c;
				map_DisOppBase.get(distinctBase.AccountId__c).OppPossibleSalesEstimate__c += distinctBase.OppPossibleSalesEstimate__c;
				map_DisOppBase.get(distinctBase.AccountId__c).OppSuccessSalesEstimate__c += distinctBase.OppSuccessSalesEstimate__c;
				map_DisOppBase.get(distinctBase.AccountId__c).SuccessGap__c = distinctBase.AnnualIndex__c - map_DisOppBase.get(distinctBase.AccountId__c).OppSuccessSalesEstimate__c;
				map_DisOppBase.get(distinctBase.AccountId__c).PossibleGap__c = distinctBase.AnnualIndex__c - map_DisOppBase.get(distinctBase.AccountId__c).OppPossibleSalesEstimate__c;
				list_delBase.add(distinctBase);
    		}
		}
		update map_DisOppBase.values();
		if(list_delBase != null && list_delBase.size()>0)
		{
			delete list_delBase;
		}
    }
    
    global void finish(Database.BatchableContext BC){
    	//一个客户下只能有一条，删除具有两条的
    	//具备两条的原因是因为存在相同的产品大类，在初始化过滤时去掉了，但是还存在数据
    	List<OpportunityBaseData__c> list_delNull = [Select o.Id From OpportunityBaseData__c o WHERE DistinctOpp__c = true and o.Now_Total_Sales__c = NULL AND o.After_Total_Sales__c = NULL];
		if(list_delNull != null && list_delNull.size()>0)
		{
			delete list_delNull;
		}
		//batch运行结束后，需要邮件通知运行该batch的用户
	 	string baseUrl = string.valueOf(System.URL.getSalesforceBaseUrl());
	 	String reportUrl= baseUrl.substring(baseUrl.indexOf('=')+1,baseUrl.length()-1);
	 	if(!system.Test.isRunningTest())
	 	{
	 		List<Report> report = [Select r.Name, r.Id, r.DeveloperName From Report r where DeveloperName = 'Trend_IVT_OppSalesTraget'];
	 		if(report != null && report.size()>0)
	 		{
				reportUrl = reportUrl + '/' + report[0].Id;
	 		}
	 	}
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		String repBody = '';
		repBody += '您好: <br><br>';     
   		repBody += curMonth + '月份销售报表预测运行完毕，请查看<br>';
        repBody += '报表查看链接：<a href="' + reportUrl + '">' + reportUrl + '</a><br><br>';
        repBody += '本邮件由Baxter Salesforce.com CRM系统产生，请勿回复。<br>'; 
        repBody += '如有任何疑问或者要求，请联系系统管理人员。<br>';
        String[] repAddress = new String[]{pointEmail};
        mail.setToAddresses(repAddress);
        mail.setHtmlBody(repBody);
        mail.setSubject('销售报表预测运行完毕');
        mail.setSaveAsActivity(false);//存为活动
        mail.setSenderDisplayName('Salesforce');
        if(pointEmail != null && pointEmail.indexOf('@') >= 0)
        {
        	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
    
    private class SalesTotalData
    {
    	public double ActualQty{get;set;}
        public double TargetQty{get;set;}
    }
    
    //trend 运算函数
    private double TrendMethod(double month, Map<double,double> map_trend)
    {
		//运行月之前的月份的实际销量之和
		double curActQty = 0;
		//线性回归参数x1*y1+x2*y2+...+xn*yn
		double xyValue = 0;
		//线性回归参数x1^2+x2^2+...+xn^2
		double xxValue = 0;
		//线性回归参数
		double SumX = 0;
		//线性回归参数
		double SumN = 0;
		
		for(double i=1; i<= curMonth; i++)
		{
			curActQty += map_trend.get(i);
			xyValue += map_trend.get(i)*i;
			xxValue += i*i;
			SumN = i;
			SumX += i;
		}
		
		//回归系数b和常数a
		double b = 0;
		double a = 0;
		if(SumN == 1)
		{
			return curActQty;
		}
		if(SumN != 0 && SumN != 1 && (xxValue -SumN*(SumX/SumN)*(SumX/SumN)) != 0)
		{
			b = (xyValue - SumN*(curActQty/SumN)*(SumX/SumN))/(xxValue -SumN*(SumX/SumN)*(SumX/SumN));
			a = curActQty/SumN - (SumX/SumN) * b;
		}
		return month*b+a;
    }
}