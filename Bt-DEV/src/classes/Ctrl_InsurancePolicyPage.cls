/*
 * Author: Steven Ke
 * Date: 2014-2-24
 * Description: controller
 */
public class Ctrl_InsurancePolicyPage 
{
	public GA_UrbanAndRural__c uar1{get;set;}
	public TownCoverPeople__c tcp1{get;set;}
	public TownCitizenCoverPeople__c tccp1{get;set;}
	public NewCountory__c nc1{get;set;}
	public Boolean isEditing1{get;set;}
	public Boolean isNotEditing1{get;set;}
	public Boolean isEditing3{get;set;}
	public Boolean isNotEditing3{get;set;}
	public Boolean isEditing5{get;set;}
	public Boolean isNotEditing5{get;set;}
	public Boolean isEditing7{get;set;}
	public Boolean isNotEditing7{get;set;}
	
	public Ctrl_InsurancePolicyPage(ApexPages.standardController controller)
	{
		Id cityID = controller.getId();
		
		uar1 = new GA_UrbanAndRural__c();
		tcp1 = new TownCoverPeople__c();
		tccp1 = new TownCitizenCoverPeople__c();
		nc1 = new NewCountory__c();
		
		List<GA_UrbanAndRural__c> uarList = new List<GA_UrbanAndRural__c>();
		uarList = [Select 
						GA_HD_ContainsAdjuvantDrug__c, 
						GA_HD_PatientPayScale__c, 
						GA_HD_DialysisPayment__c,
						GA_HD_StartToPay__c,
						GA_HD_FirstPayScale__c,
						GA_HD_HealthInsuranceType__c,
						GA_HD_HealthInsurancePayScale__c,
						GA_HD_PolicyTendency__c,
						GA_HD_PayObstacle__c,
						GA_HD_MaximumLimit__c,
						GA_PD_ContainsAdjuvantDrug__c, 
						GA_PD_PatientPayScale__c, 
						GA_PD_DialysisPayment__c,
						GA_PD_StartToPay__c,
						GA_PD_FirstPayScale__c,
						GA_PD_HealthInsuranceType__c,
						GA_PD_HealthInsurancePayScale__c,
						GA_PD_PolicyTendency__c,
						GA_PD_PayObstacle__c,
						GA_PD_MaximumLimit__c
					 From 
					 	GA_UrbanAndRural__c
					 Where 
					 	City__c =: cityId];
		
		List<TownCoverPeople__c> tcpList = new List<TownCoverPeople__c>();
		tcpList = [Select 
						GA_HD_ContainsAdjuvantDrug__c, 
						GA_HD_PatientPayScale__c, 
						GA_HD_DialysisPayment__c,
						GA_HD_StartToPay__c,
						GA_HD_FirstPayScale__c,
						GA_HD_HealthInsuranceType__c,
						GA_HD_HealthInsurancePayScale__c,
						GA_HD_PolicyTendency__c,
						GA_HD_PayObstacle__c,
						GA_HD_MaximumLimit__c,
						GA_PD_ContainsAdjuvantDrug__c, 
						GA_PD_PatientPayScale__c, 
						GA_PD_DialysisPayment__c,
						GA_PD_StartToPay__c,
						GA_PD_FirstPayScale__c,
						GA_PD_HealthInsuranceType__c,
						GA_PD_HealthInsurancePayScale__c,
						GA_PD_PolicyTendency__c,
						GA_PD_PayObstacle__c,
						GA_PD_MaximumLimit__c
					 From 
					 	TownCoverPeople__c
					 Where 
					 	City__c =: cityId];
		
		List<TownCitizenCoverPeople__c> tccpList = new List<TownCitizenCoverPeople__c>();
		tccpList = [Select 
						GA_HD_ContainsAdjuvantDrug__c, 
						GA_HD_PatientPayScale__c, 
						GA_HD_DialysisPayment__c,
						GA_HD_StartToPay__c,
						GA_HD_FirstPayScale__c,
						GA_HD_HealthInsuranceType__c,
						GA_HD_HealthInsurancePayScale__c,
						GA_HD_PolicyTendency__c,
						GA_HD_PayObstacle__c,
						GA_HD_MaximumLimit__c,
						GA_PD_ContainsAdjuvantDrug__c, 
						GA_PD_PatientPayScale__c, 
						GA_PD_DialysisPayment__c,
						GA_PD_StartToPay__c,
						GA_PD_FirstPayScale__c,
						GA_PD_HealthInsuranceType__c,
						GA_PD_HealthInsurancePayScale__c,
						GA_PD_PolicyTendency__c,
						GA_PD_PayObstacle__c,
						GA_PD_MaximumLimit__c
					 From 
					 	TownCitizenCoverPeople__c
					 Where 
					 	City__c =: cityId];
		
		List<NewCountory__c> ncList = new List<NewCountory__c>();
		ncList = [Select 
						GA_HD_ContainsAdjuvantDrug__c, 
						GA_HD_PatientPayScale__c, 
						GA_HD_DialysisPayment__c,
						GA_HD_StartToPay__c,
						GA_HD_FirstPayScale__c,
						GA_HD_HealthInsuranceType__c,
						GA_HD_HealthInsurancePayScale__c,
						GA_HD_PolicyTendency__c,
						GA_HD_PayObstacle__c,
						GA_HD_MaximumLimit__c,
						GA_PD_ContainsAdjuvantDrug__c, 
						GA_PD_PatientPayScale__c, 
						GA_PD_DialysisPayment__c,
						GA_PD_StartToPay__c,
						GA_PD_FirstPayScale__c,
						GA_PD_HealthInsuranceType__c,
						GA_PD_HealthInsurancePayScale__c,
						GA_PD_PolicyTendency__c,
						GA_PD_PayObstacle__c,
						GA_PD_MaximumLimit__c
					 From 
					 	NewCountory__c
					 Where 
					 	City__c =: cityId];
					 	
		if(uarList.size() == 0)
		{
			uar1.City__c = cityId;
		}
		else
		{
			uar1 = uarList[0];
		}
		
		if(tcpList.size() == 0)
		{
			tcp1.City__c = cityId;
		}
		else
		{
			tcp1 = tcpList[0];
		}
		
		if(tccpList.size() == 0)
		{
			tccp1.City__c = cityId;
		}
		else
		{
			tccp1 = tccpList[0];
		}
		
		if(ncList.size() == 0)
		{
			nc1.City__c = cityId;
		}
		else
		{
			nc1 = ncList[0];
		}
		
		isEditing1 = false;
		isNotEditing1 = true;
		isEditing3 = false;
		isNotEditing3 = true;
		isEditing5 = false;
		isNotEditing5 = true;
		isEditing7 = false;
		isNotEditing7 = true;
	}
	
	public void Save1()
	{
		if(uar1.Id == null)
		{
			insert uar1;
		}
		else
		{
			update uar1;
		}
		
		isEditing1 = false;
		isNotEditing1 = true;
	}
	
	public void Editit1()
	{
		isEditing1 = true;
		isNotEditing1 = false;
	}
	
	public void CancelEdit1()
	{
		isEditing1 = false;
		isNotEditing1 = true;
	}
	
	public void Saveit3()
	{
		if(tcp1.Id == null)
		{
			insert tcp1;
		}
		else
		{
			update tcp1;
		}
		isEditing3 = false;
		isNotEditing3 = true;
	}
	
	public void Editit3()
	{
		isEditing3 = true;
		isNotEditing3 = false;
	}
	
	public void CancelEdit3()
	{
		isEditing3 = false;
		isNotEditing3 = true;
	}
	
	public void Saveit5()
	{
		if(tccp1.Id == null)
		{
			insert tccp1;
		}
		else
		{
			update tccp1;
		}
		isEditing5 = false;
		isNotEditing5 = true;
	}
	
	public void Editit5()
	{
		isEditing5 = true;
		isNotEditing5 = false;
	}
	
	public void CancelEdit5()
	{
		isEditing5 = false;
		isNotEditing5 = true;
	}
	
	public void Saveit7()
	{
		if(nc1.Id == null)
		{
			insert nc1;
		}
		else
		{
			update nc1;
		}
		isEditing7 = false;
		isNotEditing7 = true;
	}
	
	public void Editit7()
	{
		isEditing7 = true;
		isNotEditing7 = false;
	}
	
	public void CancelEdit7()
	{
		isEditing7 = false;
		isNotEditing7 = true;
	}
}