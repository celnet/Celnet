/*
Author：Scott
Created on：2011-1-9
Description: 
1.得到各个部门奖金参数信息
2.得到用户下属的业绩平均值
3.得到下属Id集合
*/
public class V2_BonusParameterInfo {
    //得到各个部门奖金参数信息
    public List<V2_BonusParameterDetails__c> getBonusParameterInfo(String Department,String BonusType)
    {
        List<V2_BonusParameterDetails__c> List_BPD = new List<V2_BonusParameterDetails__c>();
        for(V2_BonusParameterDetails__c bpd: [Select Name,V2_BonusStandard__c,V2_Weight__c From V2_BonusParameterDetails__c 
                                              where V2_BonusStandard__c != null and V2_Weight__c != null 
                                              and V2_Bonus__r.V2_BonusType__c =: BonusType and V2_Bonus__r.V2_Department__c =: Department order by Name asc])
        {
            V2_BonusParameterDetails__c vbp = new V2_BonusParameterDetails__c();
            //奖金计算参数明细名称
            vbp.Name = bpd.Name;
            
            //参数标准
            vbp.V2_BonusStandard__c = bpd.V2_BonusStandard__c;
            //权重
            vbp.V2_Weight__c = bpd.V2_Weight__c;
            List_BPD.add(vbp);
            System.debug('***************bpd.Name='+bpd.Name);
            System.debug('***************bpd.V2_BonusStandard__c='+bpd.V2_BonusStandard__c);
            System.debug('***************bpd.V2_Weight__c='+bpd.V2_Weight__c);
        }
        return List_BPD;
    }
    //得到所有下属Ids
    public Set<Id> getRepids(Id currentuserid)
    {
        Set<Id> repids = new Set<Id>();
        for(User u:[select Id from User where ManagerId =:currentuserid and IsActive = true])
        {
            repids.add(u.Id);
        }
        return repids;
    }
    /***************************************************测试类***************************************************/
     static testMethod void V2_MdBonusDataService() {
     
        
        /**************************Create User*************************/
       /*用户角色*/
         //经理
         UserRole SupervisorUserRole = new UserRole() ;
         SupervisorUserRole.Name = 'Renal-Supervisor-Shanghai-PD-Supervisor';
         //SupervisorUserRole.ParentRoleId = RegionalUserRole.Id ;
         insert SupervisorUserRole ;
         //销售
         UserRole RepUserRole = new UserRole() ;
         RepUserRole.Name = 'Renal-Rep-Shanghai-HD-Rep';
         RepUserRole.ParentRoleId = SupervisorUserRole.Id ;
         insert RepUserRole ;
         
        /*用户简档*/
        //rep简档
        Profile RepProRenal = [select Id from Profile where Name  = 'Standard User - Renal Sales Rep' limit 1];
        //Supr简档renal
        Profile SupProRenal = [select Id from Profile where Name='Standard User - Renal Sales Supervisor' limit 1];
        
        /************User************/
        List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        /********主管********/
         User RepSs = new User();
         RepSs.Username='RepSs@123.com';
         RepSs.LastName='RepSs';
         RepSs.Email='RepSs@123.com';
         RepSs.Alias=user[0].Alias;
         RepSs.TimeZoneSidKey=user[0].TimeZoneSidKey;
         RepSs.ProfileId=SupProRenal.Id;
         RepSs.LocaleSidKey=user[0].LocaleSidKey;
         RepSs.LanguageLocaleKey=user[0].LanguageLocaleKey;
         RepSs.EmailEncodingKey=user[0].EmailEncodingKey;
         RepSs.CommunityNickname='RepSs';
         RepSs.MobilePhone='123456789112';
         RepSs.UserRoleId = SupervisorUserRole.Id ;
         RepSs.IsActive = true;
         insert RepSs; 
        /*销售*/
         User RepSu = new User();
         RepSu.Username='RepSu@123.com';
         RepSu.LastName='RepSu';
         RepSu.Email='RepSu@123.com';
         RepSu.Alias=user[0].Alias;
         RepSu.TimeZoneSidKey=user[0].TimeZoneSidKey;
         RepSu.ProfileId=RepProRenal.Id;
         RepSu.LocaleSidKey=user[0].LocaleSidKey;
         RepSu.LanguageLocaleKey=user[0].LanguageLocaleKey;
         RepSu.EmailEncodingKey=user[0].EmailEncodingKey;
         RepSu.CommunityNickname='RepSu';
         RepSu.MobilePhone='12345678912';
         RepSu.UserRoleId = RepUserRole.Id ;
         RepSu.IsActive = true;
         RepSu.ManagerId = RepSs.Id;
         insert RepSu;
        //奖金参数
        V2_BonusParameter__c bp = new V2_BonusParameter__c();
        bp.V2_Department__c = 'RENAL';
        bp.V2_BonusType__c = 'CRRT';
        insert bp;
        
        V2_BonusParameterDetails__c bd = new V2_BonusParameterDetails__c();
        bd.V2_Bonus__c = bp.Id;
        bd.V2_BonusStandard__c = 12;
        bd.V2_Weight__c  = 2;
        insert bd;
        
        V2_BonusParameterInfo bpi = new V2_BonusParameterInfo();
        bpi.getBonusParameterInfo('RENAL','CRRT');
        
        bpi.getRepids(RepSs.Id);
       
     }
}