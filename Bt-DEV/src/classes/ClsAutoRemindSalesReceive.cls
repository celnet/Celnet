/** 
 * Author : Sunny
 * 工程师发货15天后如果代表没有收货，自动发送邮件给代表提醒其收货
 * 维修退回申请回退旧罐子时，销售发货15天后如果工程师没有收货，自动发邮件给工程师提醒其收货
**/
global class ClsAutoRemindSalesReceive implements Database.Batchable<Vaporizer_Shipping__c>{
    global List<Vaporizer_Shipping__c> start(Database.BatchableContext BC){
        List<Vaporizer_Shipping__c> list_VapShipping = new List<Vaporizer_Shipping__c>();
        Date CheckDate = Date.today().addDays(-15) ;
        //查找15天前发货的并且没有没有收获的记录
        for(Vaporizer_Shipping__c VapShipping : [Select v.Vaporizer_Application_Detail__r.IsReceived__c, 
                v.Vaporizer_Application_Detail__c, v.RecordTypeDevName__c, v.Id, 
                v.Vaporizer_ReturnAndMainten_Detail__c, v.Vaporizer_ReturnAndMainten_Detail__r.IsReceived__c, v.Vaporizer_ReturnAndMainten_Detail__r.Vaporizer_ReturnAndMainten__c,
                v.Vaporizer_Application_Detail__r.Vaporizer_Application__c, v.Vaporizer_Application_Detail__r.Vaporizer_Application__r.Name, v.Vaporizer_Application_Detail__r.Vaporizer_Application__r.OwnerId, v.Vaporizer_Application_Detail__r.Vaporizer_Application__r.Owner.Name, v.Vaporizer_Application_Detail__r.Vaporizer_Application__r.Owner.Email, v.Vaporizer_Application_Detail__r.Vaporizer_Application__r.Owner.Alias, v.Vaporizer_Application_Detail__r.Vaporizer_Application__r.Hospital__r.Name,
                v.Vaporizer_Application_Detail__r.Vaporizer_ReturnAndMainten__c, v.Vaporizer_Application_Detail__r.Vaporizer_ReturnAndMainten__r.Name, v.Vaporizer_Application_Detail__r.Vaporizer_ReturnAndMainten__r.OwnerId, v.Vaporizer_Application_Detail__r.Vaporizer_ReturnAndMainten__r.Owner.Name, v.Vaporizer_Application_Detail__r.Vaporizer_ReturnAndMainten__r.Owner.Email, v.Vaporizer_Application_Detail__r.Vaporizer_ReturnAndMainten__r.Owner.Alias, v.Vaporizer_Application_Detail__r.Vaporizer_ReturnAndMainten__r.Hospital__r.Name
                From Vaporizer_Shipping__c v Where CreatedDate =: CheckDate]){
                	system.debug(VapShipping + 'klmklmklm');
            if(VapShipping.Vaporizer_Application_Detail__c != null && !VapShipping.Vaporizer_Application_Detail__r.IsReceived__c){
            	list_VapShipping.add(VapShipping);
            }else if(VapShipping.Vaporizer_ReturnAndMainten_Detail__c != null && !VapShipping.Vaporizer_ReturnAndMainten_Detail__r.IsReceived__c){
            	list_VapShipping.add(VapShipping);
            }
        }
        return list_VapShipping;
    }
    global void execute(Database.BatchableContext BC, List<Vaporizer_Shipping__c> scope){
    	List<User> listVapmanagement = [Select Id,Email From User Where UserRole.Name like: '%Vaporizer Management%' And IsActive = true] ;
    	Map<ID , String> map_userEmail = new Map<ID , String>();
    	Map<ID , String> map_userAlias = new Map<ID , String>();
    	Map<ID , Set<ID>> map_SalesApplyIds = new Map<ID , Set<ID>>();
    	Map<ID , String> map_ApplyidsHospital = new Map<ID , String>();
    	Map<ID , String> map_ApplyidsName = new Map<ID , String>();
    	//解析没有发货的收货发货信息 
        for(Vaporizer_Shipping__c VapShipping : scope){
        	//如果收货发货信息是在发货明细下的
        	if(VapShipping.Vaporizer_Application_Detail__c != null && !VapShipping.Vaporizer_Application_Detail__r.IsReceived__c){
        		//如果发货明细是使用申请下的
                if(VapShipping.Vaporizer_Application_Detail__r.Vaporizer_Application__c != null){
                	User salesUser = VapShipping.Vaporizer_Application_Detail__r.Vaporizer_Application__r.Owner;
                	map_userEmail.put(salesUser.Id , salesUser.Email);
                	map_userAlias.put(salesUser.Id , salesUser.Alias);
                	map_ApplyidsHospital.put(VapShipping.Vaporizer_Application_Detail__r.Vaporizer_Application__c , VapShipping.Vaporizer_Application_Detail__r.Vaporizer_Application__r.Hospital__r.Name);
                	map_ApplyidsName.put(VapShipping.Vaporizer_Application_Detail__r.Vaporizer_Application__c , VapShipping.Vaporizer_Application_Detail__r.Vaporizer_Application__r.Name);
                	if(map_SalesApplyIds.containsKey(salesUser.Id)){
                		Set<ID> set_ApplyIds = map_SalesApplyIds.get(salesUser.Id);
                		set_ApplyIds.add(VapShipping.Vaporizer_Application_Detail__r.Vaporizer_Application__c);
                		map_SalesApplyIds.put(salesUser.Id , set_ApplyIds);
                	}else{
                		Set<ID> set_ApplyIds = new Set<ID>();
                        set_ApplyIds.add(VapShipping.Vaporizer_Application_Detail__r.Vaporizer_Application__c);
                        map_SalesApplyIds.put(salesUser.Id , set_ApplyIds);
                	}
                //如果发货明细时维修/退回申请下的
                }else if(VapShipping.Vaporizer_Application_Detail__r.Vaporizer_ReturnAndMainten__c != null){
                	User salesUser = VapShipping.Vaporizer_Application_Detail__r.Vaporizer_ReturnAndMainten__r.Owner;
                	map_userEmail.put(salesUser.Id , salesUser.Email);
                	map_userAlias.put(salesUser.Id , salesUser.Alias);
                	map_ApplyidsHospital.put(VapShipping.Vaporizer_Application_Detail__r.Vaporizer_ReturnAndMainten_Detail__c , VapShipping.Vaporizer_Application_Detail__r.Vaporizer_ReturnAndMainten__r.Hospital__r.Name);
                	map_ApplyidsName.put(VapShipping.Vaporizer_Application_Detail__r.Vaporizer_ReturnAndMainten_Detail__c , VapShipping.Vaporizer_Application_Detail__r.Vaporizer_ReturnAndMainten__r.Name);
                	if(map_SalesApplyIds.containsKey(salesUser.Id)){
                        Set<ID> set_ApplyIds = map_SalesApplyIds.get(salesUser.Id);
                        set_ApplyIds.add(VapShipping.Vaporizer_Application_Detail__r.Vaporizer_ReturnAndMainten_Detail__c);
                        map_SalesApplyIds.put(salesUser.Id , set_ApplyIds);
                    }else{
                        Set<ID> set_ApplyIds = new Set<ID>();
                        set_ApplyIds.add(VapShipping.Vaporizer_Application_Detail__r.Vaporizer_ReturnAndMainten_Detail__c);
                        map_SalesApplyIds.put(salesUser.Id , set_ApplyIds);
                    }
                }
            }
            /*
            //如果发货收货信息是在退回罐子时
            else if(VapShipping.Vaporizer_ReturnAndMainten_Detail__c != null && !VapShipping.Vaporizer_ReturnAndMainten_Detail__r.IsReceived__c && listVapmanagement.size() > 0){
            	User EngUser = listVapmanagement[0];
            	map_userEmail.put(EngUser.Id , EngUser.Email);
                if(map_SalesApplyIds.containsKey(EngUser.Id)){
                    Set<ID> set_ApplyIds = map_SalesApplyIds.get(EngUser.Id);
                    set_ApplyIds.add(VapShipping.Vaporizer_ReturnAndMainten_Detail__r.Vaporizer_ReturnAndMainten__c);
                    map_SalesApplyIds.put(EngUser.Id , set_ApplyIds);
                }else{
                    Set<ID> set_ApplyIds = new Set<ID>();
                    set_ApplyIds.add(VapShipping.Vaporizer_ReturnAndMainten_Detail__r.Vaporizer_ReturnAndMainten__c);
                    map_SalesApplyIds.put(EngUser.Id , set_ApplyIds);
                }
            }
            */
        }
        if(map_userEmail.size() > 0){
        	List<Messaging.SingleEmailMessage > listMail = new List<Messaging.SingleEmailMessage>();
        	for(ID u : map_userEmail.keySet()){
        		for(ID applyId : map_SalesApplyIds.get(u)){
        			Messaging.SingleEmailMessage mail = SendEmail(map_userAlias.get(u) , map_userEmail.get(u) , applyId , map_ApplyidsHospital.get(applyId),map_ApplyidsName.get(applyId));
                    listMail.add(mail);
        		}
        	}
        	//Messaging.sendEmail(new Messaging.Singleemailmessage[] { listMail });
        	if(!Test.isRunningTest()) 
            {
                Messaging.sendEmail( listMail );
            }
        }
    }
    public Messaging.SingleEmailMessage SendEmail(String strAlias , String sEmail , ID applyId , String strHospital , String strApplyName){
    	Schema.DescribeSObjectResult VapApp = Vaporizer_Application__c.SObjectType.getDescribe();
    	String strVapAppKey = VapApp.getKeyPrefix();
    	Schema.DescribeSObjectResult VapRet = Vaporizer_ReturnAndMainten__c.SObjectType.getDescribe();
    	String strVapRetKey = VapRet.getKeyPrefix();
    	
    	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    	
    	String strSubject = '来自SEP系统通知：挥发罐'+(String.valueOf(applyId).startsWith(strVapAppKey)?'使用':'退回')+'申请工程师已发货';

        String repBody = '您好: '+strAlias+'<br><br>';
                    
            repBody += '您的'+strHospital+'挥发罐';
            repBody += (String.valueOf(applyId).startsWith(strVapAppKey)?'使用':'退回') ;
            repBody += '申请('+strApplyName+')工程师已发货，请查收！<br>';
            repBody += '请点击链接查看：';
            repBody += URL.getSalesforceBaseUrl().toExternalForm()+'/'+applyid + '<br>';
    
            repBody += '祝您工作愉快! <br>';
            repBody += '__________________________________________________ <br>';
            repBody += '本邮件由Baxter Salesforce.com CRM系统产生，请勿回复。<br>'; 
            repBody += '如有任何疑问或者要求，请联系系统管理人员。<br>'; 
            String emailAddress = String.ValueOf(sEmail);
            String[] repAddress =new string[]{emailAddress};
            mail.setToAddresses(repAddress);
            mail.setHtmlBody(repBody);
            mail.setSubject(strSubject);
            mail.setSaveAsActivity(false);//存为活动
            mail.setSenderDisplayName('Baxter SEP System');
            return mail;
    }
    global void finish(Database.BatchableContext BC){
        
    }
}