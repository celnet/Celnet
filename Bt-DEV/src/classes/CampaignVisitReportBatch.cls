/*
Author：Tommy 
Created on：2011-3-18
Description: 
SEP系统每周自动发送当月的所有拜访信息至“市场活动所有人”, 通过Batch绕过10次邮件的限制
CampaignVisitReportBatch reportBatch = new CampaignVisitReportBatch(); 
Database.executeBatch(reportBatch, 10);
*/
public class CampaignVisitReportBatch implements Database.Batchable<User>
{
	public List<User> start(Database.BatchableContext BC)
	{
		Set<String> allowProfileNameSet = new Set<String>();
		for(CampaignVisitReportSetting__c setting : [Select Name From CampaignVisitReportSetting__c Where IsReportTo__c = true])
		{
			if(setting.Name != null)
			{
				allowProfileNameSet.add(setting.Name);
			}
		}
		//允许接收报表邮件的市场部门用户，（市场活动的Owner）
		List<User> allowUserList = [Select Name, Id, Email, Alias
			From User
			Where IsActive =true
			And ProfileId IN
				( 
					Select Id From Profile Where Name IN: allowProfileNameSet
				)];
		System.debug(allowUserList);
		String str0 = '';
		for(User u: allowUserList)
		{  
			str0 += u.Name + '/' +u.Alias + ', ';
		}
		System.Debug('####:允许用户范围' + allowUserList.size() + ',用户：' + str0);
		
		Set<ID> allowUserIdSet = new Set<ID>();
		for(User u : allowUserList)
		{
			allowUserIdSet.add(u.Id);
		}
		//判断是否用户有数据（有市场活动并且当月有拜访），无数据的用户不进行发送
		Map<ID, User> targetUserIdMap = new Map<ID, User>();
		for(Campaign cp :[Select Id, OwnerId, Owner.Id, Owner.Name, Owner.Alias,
			(Select Id From Events Where StartDateTime=this_month) 
			From Campaign Where OwnerId In: allowUserIdSet])
		{
			if(cp.Events != null && cp.Events.size() > 0)
			{
				System.debug('IN' + cp.Events.size());
				targetUserIdMap.put(cp.OwnerId, cp.Owner);
			}
		}
		List<User> targetUserList = targetUserIdMap.values();
		String str = '';
		for(User u: targetUserList)
		{
			str += u.Name + '/' +u.Alias + ', ';
		}
		System.Debug('####:发送邮件的用户范围' + targetUserList.size() + ',用户：' + str);
		return targetUserList;
		/*
		List<ID> targetUserIdList = new List<ID>();
		for(ID userId : targetUserIdSet.)
		{
			targetUserIdList.add(userId);
		}
		System.Debug('####:发送邮件的用户范围' + targetUserIdList.size());
		return targetUserIdList;
		*/
	}
	
	public void execute(Database.BatchableContext BC, List<User> scope)
	{
		for(User user: scope)
		{
    		this.SendEmailReportTo(user.Id);
		}
	}  
	
	public void finish(Database.BatchableContext BC)
	{
		
	}
	
	private void SendEmailReportTo(ID reportToUserId)
	{	
		EmailTemplate temp =[Select Name, IsActive, Id, DeveloperName From EmailTemplate Where DeveloperName = 'CampaignVisitReport'];
	   	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setSaveAsActivity(false);
	   	mail.setTemplateId(temp.Id);
	   	mail.setTargetObjectId(reportToUserId);
	   	//mail.setToAddresses( new String[] {'tommyliu@cideatech.com'});//测试用
	   	mail.setReplyTo('no-reply@salesforce.com');
	   	mail.setSenderDisplayName('Baxter SEP System');
	  	if(!Test.isRunningTest()) 
	  	{
	   		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	  	}
	}
	
	public static User CreateUserForTest(String lastName, String firstName, String alias)
	{
		List<User> user = [select id,ProfileId, Profile.Name, Profile.Id, Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey, Email, MobilePhone from User where id =: UserInfo.getUserId()];
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username=firstName + 'bacd@123.com';
    	use1.LastName=lastName;
    	use1.FirstName=firstName;
    	use1.Email=user[0].Email;
    	use1.Alias=alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.Profile = new Profile();
    	use1.Profile.Id = use1.ProfileId;
    	use1.Profile.Name = user[0].Profile.name;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname=firstName + 'abc';
    	use1.MobilePhone=user[0].MobilePhone;
    	use1.IsActive = true;
    	list_User.add(use1) ;
    	insert list_User;
		return use1;
	}
    static testMethod void myUnitTest() 
    {
		User user = CreateUserForTest('Marketing', 'Rep', 'Mp171');
		CampaignVisitReportSetting__c setting = new CampaignVisitReportSetting__c();
		setting.IsReportTo__c = true;
		setting.Name = user.Profile.Name;
		insert setting;
		
		Provinces__c province = new Provinces__c();
        province.Name = '上海';
        insert province;
        Cities__c city = new Cities__c();
        city.Name = '上海';
        city.BelongToProvince__c = province.Id;
        insert city;
		Account acc1 = new Account();
        acc1.Name = 'T_医院1';
        acc1.Cities__c = city.Id;
        acc1.Provinces__c = province.Id;
        insert new Account[] {acc1};
        
        Contact ct1 = new Contact();
        ct1.AccountId = acc1.Id;
        ct1.LastName = 'T_医院1_医生A';
        insert new Contact[]{ct1};
		
		Campaign cp = new Campaign();
		cp.Name = 'T_CPA';
		cp.IsActive = true;
		cp.StartDate = Date.today();
		cp.EndDate = Date.today().addDays(30);
		cp.OwnerId = user.Id;
		insert(cp);
		
		ID recordTypeId = [Select Id, Name From RecordType Where DeveloperName = 'V2_Event'].Id;
		Event ev = new Event();
		ev.Subject = '拜访'; 
		ev.WhoId = ct1.Id;
		ev.WhatId = cp.Id;
		ev.StartDateTime = Datetime.now();
		ev.EndDateTime = Datetime.now();
		ev.OwnerId = user.Id;
		ev.RecordTypeId = recordTypeId;
		insert ev;
        
        //开始测试
        test.startTest();
        CampaignVisitReportSchedule sch = new CampaignVisitReportSchedule();
        sch.execute(null);
  		//CampaignVisitReportBatch reportBatch = new CampaignVisitReportBatch(); 
  		//Database.executeBatch(reportBatch, 10);
		//List<User> toUserList = reportBatch.start(null);
		//System.assert(toUserList.size() == 1);
		//System.assertEquals(user.Id, toUserList[0].Id);
        test.stopTest();
    }
}