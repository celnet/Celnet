/**
 * Hank
 * 2013-10-16
 *PM_AccToApplication  Trigger测试类
 */
@isTest
private class PM_Test_AccToApplication {

    static testMethod void myUnitTest() 
    {
       system.Test.startTest();
       list<Profile> list_Profile = [Select Id,Name From Profile Where Name = 'PM-PSR Manager'];
        list<UserRole> list_role = [Select Id,Name From UserRole Where DeveloperName ='RenalGDAccountManager'];
        User user = new User();
        user.ProfileId = list_Profile[0].Id;
        user.Username = 'asfdasdf@acme.com';
        user.FirstName = 'Test';
        user.LastName = 'Test';
        user.Email = 'asdf@aa.com';
        user.Alias = 'asdf';
        user.EmailEncodingKey = 'UTF-8';
        user.TimeZoneSidKey = 'Asia/Shanghai';
        user.LanguageLocaleKey = 'zh_CN';
        user.LocaleSidKey = 'zh_CN';
        user.UserRoleId = list_role[0].Id;
        insert user;
       Application__c ac = new Application__c();
       ac.Content__c = 'test';
       ac.Reason__c = '医院核对';
       ac.Status__c = '申请';
       insert ac;
       //ProcessInstanceHistory pih = new ProcessInstanceHistory();
       //pih.StepStatus = '申请';
       //pih.CreatedDate = date.today();
       ac.PM_SuperviorApproveDate__c = date.today();
       ac.PM_PSRApproveDate__c = date.today();
       update ac;
       system.Test.stopTest();
    }
}