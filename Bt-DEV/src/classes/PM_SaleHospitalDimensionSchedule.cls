/*
*作者：Tobe
*时间：2013-10-26
*功能：控制PM_SaleHospitalDimensionBatch
*/
public class PM_SaleHospitalDimensionSchedule implements Schedulable
{
	 public void execute(SchedulableContext sc) 
	 {
    	PM_SaleHospitalDimensionBatch btBatch = new PM_SaleHospitalDimensionBatch();
		database.executeBatch(btBatch,50);
     }
	
	static testMethod void myUnitTest() 
	{		
        system.test.startTest();
        string sch=DateTime.now().second()+' '+DateTime.now().minute()+' '+DateTime.now().hour()+' * * ?';
        PM_SaleHospitalDimensionSchedule pa = new PM_SaleHospitalDimensionSchedule();
        System.schedule('test', sch , pa);
        system.test.stopTest();
	}
}