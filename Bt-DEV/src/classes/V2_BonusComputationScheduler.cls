/*
Author：Scott
Created on：2011-1-16
Description: 
1.计划执行所有部门的奖金计算
*/ 
global class V2_BonusComputationScheduler implements Schedulable{ 
	global static final String CRON_EXP = '0 0 1 * * ?';
	global void execute(SchedulableContext sc) 
	{
		Integer year = Date.today().year();
		Integer month = Date.today().month();
	  	//Md & Bios
		Set<Id> Set_RepIds = new Set<Id>();
		Set<Id> Set_SupervisorIds = new Set<Id>();
		//Renal
		Set<Id> RenalRepIds = new Set<Id>(); 
		Set<Id> RenalSsIds = new Set<Id>();
		String ids;
	   
	   
	   
	   for(User u:[select UserRole.Name,Id,Renal_valid_super__c,Department from User 
					//已启用用户
					Where IsActive = true 
					//未休假用户
					and IsOnHoliday__c =false
					//未离职用户
					and IsLeave__c = false
					//角色不能为空
					and UserRoleId !=null
					and UserRole.Name != null])
	   {
	   		List<String> RoleNameInfos = String.valueOf(u.UserRole.Name).trim().split('-');
	   		if(RoleNameInfos == null)
	   		{
	   			continue;
	   		}
	   		if(RoleNameInfos.size()<4)
	   		{
	   			continue;
	   		}
	   		//当前用户部门
			String Department = RoleNameInfos[0].toUpperCase();
			
	   		if(Department != 'RENAL' && Department != 'MD' && Department != 'BIOS')
	   		{
	   			continue;
	   		}
	   		if(ids == null)
			{
				ids = u.id+',';
			}
			else
			{
				ids +=u.id+',';
			}
	   		/*//当前用户Leve
			String UserLeve = RoleNameInfos[1].toUpperCase();
			if(Department =='RENAL')
			{
				if(u.Department == 'Renal-SalesRep')
				{
					RenalRepIds.add(u.Id);
				}
				else if(u.Department =='Renal-SalesSupervisor')
				{
					RenalSsIds.add(u.Id);
				}
			}
			else
			{
				if(UserLeve == 'REP')
				{
					Set_RepIds.add(u.Id);
				}
				else if(UserLeve =='SUPERVISOR')
				{
					Set_SupervisorIds.add(u.Id);
				}
			}*/
	   }
	   
	  /* //Renal
		if(RenalRepIds != null && RenalRepIds.size()>0)
		{
			
			for(Id userid: RenalRepIds)
			{
				if(ids == null)
				{
					ids = userid;
				}
				else
				{
					ids +=userid+',';
				}
			}
		}
		if(RenalSsIds != null && RenalRepIds.size()>0)
		{
			for(Id userid: RenalSsIds)
			{
				if(ids == null)
				{
					ids = userid;
				}
				else
				{
					ids +=userid+',';
				}
			}
		}
		
		//Md & Bios
		if(Set_RepIds != null && Set_RepIds.size()>0)
		{
			for(Id userid: Set_RepIds)
			{
				if(ids == null)
				{
					ids = userid;
				}
				else
				{
					ids +=userid+',';
				}
				
			}
		}
		if(Set_SupervisorIds != null && Set_SupervisorIds.size()>0) 
		{
			for(Id userid: Set_SupervisorIds)
			{
				if(ids == null)
				{
					ids = userid;
				}
				else
				{
					ids +=userid+',';
				}
			}
		}*/
		//调用batch
		if(ids !=null && ids.length()>0)
		{
			V2_BatchComputationBonus BonusBatch = new V2_BatchComputationBonus();
			BonusBatch.userids = ids;  
			BonusBatch.year = year;
			BonusBatch.month = month;
			Database.executeBatch(BonusBatch,1);
		}
	}
}