/*
Author：Scott
Created on：2011-1-16
Description: 
1.各部门年度奖金数据报表
*/
public with sharing class V2_CtrlUserBonusReport {
	public String Year{get;set;}
	public String Department{get;set;}
	public List<SelectOption> ListYears{get;set;}
	//用户部门
	public String UserDepartment{get;set;}
	//用户奖金信息
	public List<UserBonus> UBList{get;set;}
	//是否是主管
	public Boolean IsSystemAdmin = false;
	//部门是否可选
	public Boolean Isreadonly{get;set;}
	//button 
	public Boolean ButtonDisabled{get;set;}
	//Map
	Map<Id,List<V2_UserBonusInFo__c>> Map_UserBonus = new Map<Id,List<V2_UserBonusInFo__c>>();
	
	//Map2
	Map<Id,List<V2_UserBonusInFo__c>> Map_UserBonus2 = new Map<Id,List<V2_UserBonusInFo__c>>();
	
	//部门
    public List<SelectOption> getListDepartment()
    {
    	list<SelectOption> options=new list<SelectOption>();
        options.add(new SelectOption('RENAL','RENAL'));
        options.add(new SelectOption('MD','MD'));
        options.add(new SelectOption('BIOS','BIOS'));
        return options;
    }
    public V2_CtrlUserBonusReport()
    {
    	//ButtonDisabled = false;
    	//年
		ListYears = new  List<SelectOption>();
        ListYears.add(new SelectOption(String.valueOf(date.today().addYears(-1).year()),String.valueOf(date.today().addYears(-1).year())));
        ListYears.add(new SelectOption(String.valueOf(date.today().year()),String.valueOf(date.today().year())));
        ListYears.add(new SelectOption(String.valueOf(date.today().addYears(1).year()),String.valueOf(date.today().addYears(1).year())));
        //默认当前年当前月
        Year = String.valueOf(date.today().year());
        CheckCurrentUser();
        ButtonDisabled = false;
        
    }
    
    /*
    *检查当前用户
    */
	public void CheckCurrentUser()
	{
		//当前用户信息
        User CurrentUser = [select UserRole.Name,Profile.Name from User where Id =: UserInfo.getUserId()];
       	if(CurrentUser.Profile.Name == '系统管理员' || CurrentUser.Profile.Name =='System Administrator')
        {
        	Isreadonly = false;
        }
        else
        {
        	Isreadonly = true;
        	if(CurrentUser.Profile.Name != null)
	        {
	        		UserDepartment = CurrentUser.Profile.Name.toUpperCase();
	        		if(UserDepartment.contains('RENAL'))
	        		{
	        			Department = 'RENAL';
	        		}
	        		else if(UserDepartment.contains('MD')||UserDepartment.contains('IVT') || UserDepartment.contains('SP')||UserDepartment.contains('IS'))
	        		{
	        			Department = 'MD';
	        		}
	        		else if(UserDepartment.contains('BIOS'))
	        		{
	        			Department = 'BIOS';
	        		}
	        		else
	        		{
	        			 ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING , '无法通过您的简档名称判断所属部门，请您联系管理员。');            
                		 ApexPages.addMessage(msg);
                		 ButtonDisabled = true;
                		 return;
	        		}
	        }
	        else
	        {
	        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING , '您的简档名称信息不正确，请您联系管理员。');            
            	ApexPages.addMessage(msg);
            	ButtonDisabled = true;
            	return;
	        }
        }
	}
	/*
	 *查询信息
	*/
	public void SearchUserBonus()
	{
		try
		{
			//ButtonDisabled = true;
			UBList = new List<UserBonus>();
			UBList.clear();
			Map_UserBonus.clear();
			Map_UserBonus2.clear();
			for(V2_UserBonusInFo__c bonusInfo:[select V2_RenalIsHosSS__c,Id,OwnerId,Owner.Alias,V2_BonusScore__c,V2_MdTargetAccSet__c,V2_MdPotentialAnalysis__c,
												   V2_MdCampaignFollowEvent__c,V2_MdCampaignInvitation__c,V2_MdOpp__c,V2_YearScore__c,V2_Month__c
												   from V2_UserBonusInFo__c where V2_Year__c =:year and V2_Department__c=:Department order by V2_Month__c asc])
			{
				if(!bonusInfo.V2_RenalIsHosSS__c)
				{
					if(Map_UserBonus.containsKey(bonusInfo.OwnerId))
					{
						List<V2_UserBonusInFo__c> Bonuslist = Map_UserBonus.get(bonusInfo.OwnerId);
						Bonuslist.add(bonusInfo);
						Map_UserBonus.put(bonusInfo.OwnerId,Bonuslist);
					}
					else
					{
						List<V2_UserBonusInFo__c> Bonuslist = new List<V2_UserBonusInFo__c>();
						Bonuslist.add(bonusInfo);
						Map_UserBonus.put(bonusInfo.OwnerId,Bonuslist);
					}
				}
				else
				{
					//主管带医院
					if(Map_UserBonus2.containsKey(bonusInfo.OwnerId))
					{
						List<V2_UserBonusInFo__c> Bonuslist = Map_UserBonus2.get(bonusInfo.OwnerId);
						Bonuslist.add(bonusInfo);
						Map_UserBonus2.put(bonusInfo.OwnerId,Bonuslist);
					}
					else
					{
						List<V2_UserBonusInFo__c> Bonuslist = new List<V2_UserBonusInFo__c>();
						Bonuslist.add(bonusInfo);
						Map_UserBonus2.put(bonusInfo.OwnerId,Bonuslist);
					}
				}
			}
			
			if(Map_UserBonus != NULL && Map_UserBonus.size()>0)
			{
				for(Id userid : Map_UserBonus.keySet())
				{
					UserBonus UserAllYearBonus = getAllYearDate(Map_UserBonus.get(userid));
					UBList.add(UserAllYearBonus);
				}
			}
			else
			{
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING , '当前年没有可供查看的有效数据！');    		
	    		ApexPages.addMessage(msg);
	    		return;	
			}
			//主管带医院
			if(Map_UserBonus2 != NULL && Map_UserBonus2.size()>0)
			{
				for(Id userid : Map_UserBonus2.keySet())
				{
					UserBonus UserAllYearBonus = getAllYearDate(Map_UserBonus2.get(userid));
					UBList.add(UserAllYearBonus);
				}
			}
		}
		catch(Exception e)
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR , String.valueOf(e)+' 第'+e.getLineNumber()+'行');            
            ApexPages.addMessage(msg);
            return;
		}
		
	}
	/*
	*用户一年的信息
	*/
	public UserBonus getAllYearDate(List<V2_UserBonusInFo__c> List_Ub)
	{
		UserBonus ub = new UserBonus();
		//Q1
		Double quarter1 = 0;
		Double months1 = 0;
		//Q2
		Double quarter2 = 0;
		Double months2 = 0;
		//Q3
		Double quarter3 = 0;
		Double months3 = 0;
		//Q4
		Double quarter4 = 0;
		Double months4 = 0;
		
		for(V2_UserBonusInFo__c bonusInfo : List_Ub)
		{
			ub.UserName = bonusInfo.Owner.Alias;
			if(bonusInfo.V2_Month__c =='1')
			{
				ub.BonusId1 = bonusInfo.Id;
				ub.Jan = bonusInfo.V2_BonusScore__c;
				quarter1 +=ub.Jan;
				months1++;
			}
			else if(bonusInfo.V2_Month__c =='2')
			{
				ub.BonusId2 = bonusInfo.Id;
				ub.Feb = bonusInfo.V2_BonusScore__c;
				quarter1 +=ub.Feb;
				months1++;
			}
			else if(bonusInfo.V2_Month__c =='3')
			{
				ub.BonusId3 = bonusInfo.Id;
				ub.Mar = bonusInfo.V2_BonusScore__c;
				quarter1 +=ub.Mar;
				months1++;
			}
			else if(bonusInfo.V2_Month__c =='4')
			{
				ub.BonusId4 = bonusInfo.Id;
				ub.Apr = bonusInfo.V2_BonusScore__c;
				quarter2 +=ub.Apr;
				months2++;
			}
			else if(bonusInfo.V2_Month__c =='5')
			{
				ub.BonusId5 = bonusInfo.Id;
				ub.May = bonusInfo.V2_BonusScore__c;
				quarter2 +=ub.May;
				months2++;
			}
			else if(bonusInfo.V2_Month__c =='6')
			{
				ub.BonusId6 = bonusInfo.Id;
				ub.June = bonusInfo.V2_BonusScore__c;
				quarter2 +=ub.June;
				months2++;
			}
			else if(bonusInfo.V2_Month__c =='7')
			{
				ub.BonusId7 = bonusInfo.Id;
				ub.July = bonusInfo.V2_BonusScore__c;
				quarter3 +=ub.July;
				months3++;
			}
			else if(bonusInfo.V2_Month__c =='8')
			{
			 	ub.BonusId8 = bonusInfo.Id;
			 	ub.Aug = bonusInfo.V2_BonusScore__c;
			 	quarter3 +=ub.Aug;
				months3++;
			}
			else if(bonusInfo.V2_Month__c =='9')
			{
				ub.BonusId9 = bonusInfo.Id;
				ub.Sept = bonusInfo.V2_BonusScore__c;
				quarter3 +=ub.Sept;
				months3++;
			}
			else if(bonusInfo.V2_Month__c =='10')
			{
				ub.BonusId10 = bonusInfo.Id;
				ub.Oct = bonusInfo.V2_BonusScore__c;
				quarter4 += ub.Oct;
				months4++;
			}
			else if(bonusInfo.V2_Month__c =='11')
			{
				ub.BonusId11 = bonusInfo.Id;
				ub.Nov = bonusInfo.V2_BonusScore__c;
				quarter4 +=ub.Nov;
				months4++;
			}
			else if(bonusInfo.V2_Month__c =='12')
			{
				ub.BonusId12 = bonusInfo.Id;
				ub.Dec = bonusInfo.V2_BonusScore__c;
				quarter4 +=ub.Dec;
				months4++;
			}
		}
		if(months1>0)
		{
			ub.Quarter1 =quarter1/months1;
		}
		if(months2>0)
		{
			ub.Quarter2 = quarter2/months2;
		}
		if(months3>0)
		{
			ub.Quarter3 = quarter3/months3;
		}
		if(months4>0)
		{
			ub.Quarter4 =quarter4/months4;
		}
		if(months1+months2+months3+months4>0)
		{
			ub.YearAvg=(quarter1+quarter2+quarter3+quarter4)/(months1+months2+months3+months4);
		}
		return 	ub;	
	}
	/*
	*奖金封装类
	*/
	public class UserBonus
	{
	  public String UserName{get; set;}//代表名称
	  //public Id BonusId{get;set;}//奖金纪录Id
	  public Double Jan{get;set;}//1月
	  public Id BonusId1{get;set;}
	  public Double Feb{get;set;}//2月
	  public Id BonusId2{get;set;}
	  public Double Mar{get;set;}//3月
	  public Id BonusId3{get;set;}
	  public Double Quarter1{get;set;}//第一季度
	  public Double Apr{get;set;}//4月
	  public Id BonusId4{get;set;}
	  public Double May{get;set;}//5月
	  public Id BonusId5{get;set;}
	  public Double June{get;set;}//6月
	  public Id BonusId6{get;set;}
	  public Double Quarter2{get;set;}//第二季度
	  public Double July{get;set;}//7月
	  public Id BonusId7{get;set;}
	  public Double Aug{get;set;}//8月
	  public Id BonusId8{get;set;}
	  public Double Sept{get;set;}//9月
	  public Id BonusId9{get;set;}
	  public Double Quarter3{get;set;}//第三季度
	  public Double Oct{get;set;}//10月
	  public Id BonusId10{get;set;}
	  public Double Nov{get;set;}//11月
	  public Id BonusId11{get;set;}
	  public Double Dec{get;set;}//12月
	  public Id BonusId12{get;set;}
	  public Double Quarter4{get;set;}//第四季度
	  Public Double MonthAvg{get;set;}//月平均成绩
	  public Double OppAvg{get;set;}//业务机会成绩
	  public Double YearAvg{get;set;}//年度成绩
	}
	/****************************************************Test*********************************/
    static testMethod void V2_CtrlUserBonusReport()
    {
    	 //----------------New Role ------------------
       	 //Bios
       	 UserRole biosRep_role = new UserRole();
       	 biosRep_role.Name = 'BIOS-Rep-北京-Albumin-Rep' ;
       	 insert biosRep_role;
       	 //Md
	     UserRole mdRep_role = new UserRole();
       	 mdRep_role.Name = 'MD-Rep-北京-IVT-Rep' ;
       	 insert mdRep_role;
       	 //Renal
       	 UserRole renalRep_role = new UserRole();
       	 renalRep_role.Name = 'Renal-Rep-北京-CRRT-Rep' ;
       	 insert renalRep_role;
       	 //错误简档
       	 UserRole error_role = new UserRole();
       	 error_role.Name = 'ssss-Rep-北京-CRRT-Rep' ;
       	 insert error_role;
       	 
       	 //Bios简档
	    Profile BiosRepPro = [select Id from Profile where Name  = 'Standard User - BIOS Sales Rep' limit 1];
	    //Md简档
	    Profile MdRepPro = [select Id from Profile where Name='Standard User - IVT Sales Rep' limit 1];
	    //Renal简档
	    Profile RenalRepPro = [select Id from Profile where Name='Standard User - Renal Sales Rep' limit 1];
	    //----------------Create User-------------
	    List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
         //Bios
	     User BiosRep1 = new User();
	     BiosRep1.Username='BiosRep1@123.com';
	     BiosRep1.LastName='BiosRep1';
	     BiosRep1.Email='BiosRep1@123.com';
	     BiosRep1.Alias=user[0].Alias;
	     BiosRep1.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     BiosRep1.ProfileId=BiosRepPro.Id;
	     BiosRep1.LocaleSidKey=user[0].LocaleSidKey;
	     BiosRep1.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     BiosRep1.EmailEncodingKey=user[0].EmailEncodingKey;
	     BiosRep1.CommunityNickname='BiosRep1';
	     BiosRep1.MobilePhone='12345678912';
	     BiosRep1.UserRoleId = biosRep_role.Id ;
	     BiosRep1.IsActive = true;
	     insert BiosRep1;
	      //Md
	     User mdRep = new User();
	     mdRep.Username='mdRep@123.com';
	     mdRep.LastName='mdRep';
	     mdRep.Email='mdRep@123.com';
	     mdRep.Alias=user[0].Alias;
	     mdRep.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     mdRep.ProfileId=MdRepPro.Id;
	     mdRep.LocaleSidKey=user[0].LocaleSidKey;
	     mdRep.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     mdRep.EmailEncodingKey=user[0].EmailEncodingKey;
	     mdRep.CommunityNickname='mdRep';
	     mdRep.MobilePhone='12345678912';
	     mdRep.UserRoleId = mdRep_role.Id ;
	     mdRep.IsActive = true;
	     insert mdRep;
	     //Renal
         User renalRep = new User();
	     renalRep.Username='renalRep@123.com';
	     renalRep.LastName='renalRep';
	     renalRep.Email='renalRep@123.com';
	     renalRep.Alias=user[0].Alias;
	     renalRep.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     renalRep.ProfileId=RenalRepPro.Id;
	     renalRep.LocaleSidKey=user[0].LocaleSidKey;
	     renalRep.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     renalRep.EmailEncodingKey=user[0].EmailEncodingKey;
	     renalRep.CommunityNickname='renalRep';
	     renalRep.MobilePhone='12345678912';
	     renalRep.UserRoleId = renalRep_role.Id ;
	     renalRep.IsActive = true;
	     renalRep.Renal_valid_super__c  = false;
	     insert renalRep;
	     
	     //Bios
    	 V2_UserBonusInFo__c bios = new V2_UserBonusInFo__c();
    	 bios.OwnerId = BiosRep1.Id;
    	 bios.V2_Year__c = String.valueOf(date.today().Year());
    	 bios.V2_Department__c = 'BIOS';
    	 bios.V2_Month__c = String.valueOf(date.today().Month());
    	 bios.V2_BonusScore__c = 20;
    	 insert bios;
    	 V2_UserBonusInFo__c bios1 = new V2_UserBonusInFo__c();
    	 bios1.OwnerId = BiosRep1.Id;
    	 bios1.V2_Year__c = String.valueOf(date.today().Year());
    	 bios1.V2_Department__c = 'BIOS';
    	 bios1.V2_Month__c = String.valueOf(date.today().addMonths(1).Month());
    	 bios1.V2_BonusScore__c = 20;
    	 insert bios1;
    	 V2_UserBonusInFo__c bios2 = new V2_UserBonusInFo__c();
    	 bios2.OwnerId = BiosRep1.Id;
    	 bios2.V2_Year__c = String.valueOf(date.today().Year());
    	 bios2.V2_Department__c = 'BIOS';
    	 bios2.V2_Month__c = String.valueOf(date.today().addMonths(2).Month());
    	 bios2.V2_BonusScore__c = 20;
    	 insert bios2;
    	 V2_UserBonusInFo__c bios13 = new V2_UserBonusInFo__c();
    	 bios13.OwnerId = BiosRep1.Id;
    	 bios13.V2_Year__c = String.valueOf(date.today().Year());
    	 bios13.V2_Department__c = 'BIOS';
    	 bios13.V2_Month__c = String.valueOf(date.today().addMonths(3).Month());
    	 bios13.V2_BonusScore__c = 20;
    	 insert bios13;
    	//md
    	V2_UserBonusInFo__c md = new V2_UserBonusInFo__c();
    	 md.OwnerId = mdRep.Id;
    	 md.V2_Year__c = String.valueOf(date.today().Year());
    	 md.V2_Department__c = 'MD';
    	 md.V2_Month__c = String.valueOf(date.today().addMonths(4).Month());
    	 md.V2_BonusScore__c = 20;
    	 insert md;
    	 V2_UserBonusInFo__c md1 = new V2_UserBonusInFo__c();
    	 md1.OwnerId = mdRep.Id;
    	 md1.V2_Year__c = String.valueOf(date.today().Year());
    	 md1.V2_Department__c = 'MD';
    	 md1.V2_Month__c = String.valueOf(date.today().addMonths(5).Month());
    	 md1.V2_BonusScore__c = 20;
    	 insert md1;
    	 V2_UserBonusInFo__c md2 = new V2_UserBonusInFo__c();
    	 md2.OwnerId = mdRep.Id;
    	 md2.V2_Year__c = String.valueOf(date.today().Year());
    	 md2.V2_Department__c = 'MD';
    	 md2.V2_Month__c = String.valueOf(date.today().addMonths(6).Month());
    	 md2.V2_BonusScore__c = 20;
    	 insert md2;
    	 V2_UserBonusInFo__c md3 = new V2_UserBonusInFo__c();
    	 md3.OwnerId = mdRep.Id;
    	 md3.V2_Year__c = String.valueOf(date.today().Year());
    	 md3.V2_Department__c = 'MD';
    	 md3.V2_Month__c = String.valueOf(date.today().addMonths(7).Month());
    	 md3.V2_BonusScore__c = 20;
    	 insert md3;
    	 
    	 //Renal
    	 V2_UserBonusInFo__c renal = new V2_UserBonusInFo__c();
    	 renal.OwnerId = renalRep.Id;
    	 renal.V2_Year__c = String.valueOf(date.today().Year());
    	 renal.V2_Department__c = 'RENAL';
    	 renal.V2_Month__c = String.valueOf(date.today().addMonths(8).Month());
    	 renal.V2_BonusScore__c = 20;
    	 insert renal;
    	 V2_UserBonusInFo__c renal1 = new V2_UserBonusInFo__c();
    	 renal1.OwnerId = renalRep.Id;
    	 renal1.V2_Year__c = String.valueOf(date.today().Year());
    	 renal1.V2_Department__c = 'RENAL';
    	 renal1.V2_Month__c = String.valueOf(date.today().addMonths(9).Month());
    	 renal1.V2_BonusScore__c = 20;
    	 insert renal1;
    	 V2_UserBonusInFo__c renal2 = new V2_UserBonusInFo__c();
    	 renal2.OwnerId = renalRep.Id;
    	 renal2.V2_Year__c = String.valueOf(date.today().Year());
    	 renal2.V2_Department__c = 'RENAL';
    	 renal2.V2_Month__c = String.valueOf(date.today().addMonths(10).Month());
    	 renal2.V2_BonusScore__c = 20;
    	 insert renal2;
    	 V2_UserBonusInFo__c renal3 = new V2_UserBonusInFo__c();
    	 renal3.OwnerId = renalRep.Id;
    	 renal3.V2_Year__c = String.valueOf(date.today().Year());
    	 renal3.V2_Department__c = 'RENAL';
    	 renal3.V2_Month__c = String.valueOf(date.today().addMonths(11).Month());
    	 renal3.V2_BonusScore__c = 20;
    	 insert renal3;
    	 
    	 User error = new User();
	     error.Username='error@123.com';
	     error.LastName='error';
	     error.Email='error@123.com';
	     error.Alias=user[0].Alias;
	     error.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     error.ProfileId=BiosRepPro.Id;
	     error.LocaleSidKey=user[0].LocaleSidKey;
	     error.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     error.EmailEncodingKey=user[0].EmailEncodingKey;
	     error.CommunityNickname='error';
	     error.MobilePhone='12345678912';
	     error.UserRoleId = renalRep_role.Id ;
	     error.IsActive = true;
	     insert error;
    	 
    	 Test.startTest();
    	 	System.runAs(BiosRep1)
    	 	{
    	 		V2_CtrlUserBonusReport biosbouns = new V2_CtrlUserBonusReport();
    	 		biosbouns.getListDepartment();
    	 		biosbouns.SearchUserBonus();
    	 	}
    	 	System.runAs(mdRep)
    	 	{
    	 		V2_CtrlUserBonusReport mdbouns = new V2_CtrlUserBonusReport();
    	 		mdbouns.getListDepartment();
    	 		mdbouns.SearchUserBonus();
    	 	}
    	 	System.runAs(renalRep)
    	 	{
    	 		V2_CtrlUserBonusReport renalbouns = new V2_CtrlUserBonusReport();
    	 		renalbouns.getListDepartment();
    	 		renalbouns.SearchUserBonus();
    	 	}
    	 	V2_CtrlUserBonusReport admin = new V2_CtrlUserBonusReport();
    	    admin.getListDepartment();
    	    admin.SearchUserBonus();
    	    
    	    System.runAs(error)
    	    {
    	    	try
	    	    {
	    	    	V2_CtrlUserBonusReport errorbouns = new V2_CtrlUserBonusReport();
    	 			errorbouns.getListDepartment();
    	 			errorbouns.SearchUserBonus();
    	 			
	    	    }catch(Exception e)
	    	    {
	    	    	System.debug('部门错误');
	    	    }
    	    }
    	    
    	    try
    	    {
    	    	V2_CtrlUserBonusReport admin1 = new V2_CtrlUserBonusReport();
    	    	admin1.Year = String.valueOf(date.today().addDays(-11));
    	    	admin1.getListDepartment();
    	    	admin1.SearchUserBonus();
    	    }catch(Exception e)
    	    {
    	    	System.debug('当前年没有可供查看的有效数据！'+String.valueOf(e));
    	    }
    	    
    	    
    	 Test.stopTest();
    }
}