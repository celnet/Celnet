/*
 * Author : Henry
 * description : 主页左侧栏自定义链接
 * 热线、邮寄、自由行—TP、经销商信息反馈、产品信息反馈、其他信息反馈
 */
public class PM_HomeUrlDevController 
{
    public String strHotlineUrl{get;set;}
    public String strMailUrl{get;set;}
    public String strFreeWalkerTPUrl{get;set;} 
    public String strDealerUrl{get;set;}
    public String strProductFeedbackUrl{get;set;}
    public String strOtherPatientFeedbackUrl{get;set;}
    public String strPlanUrl{get;set;}
    
    public PM_HomeUrlDevController()
    {
        this.initUrl();
    }
    private void initUrl(){
    	//热线
        Schema.DescribeSObjectResult Hotline = PM_Hotline__c.SObjectType.getDescribe();
        strHotlineUrl = '../'+Hotline.getKeyPrefix();
    	 //邮寄
        Schema.DescribeSObjectResult Mail = PM_Mail__c.SObjectType.getDescribe();
        strMailUrl = '../'+Mail.getKeyPrefix();
    	//自由行-TP
        Schema.DescribeSObjectResult FreeWalkerTP = PM_Free_TP_Application__c.SObjectType.getDescribe();
        strFreeWalkerTPUrl = '../'+FreeWalkerTP.getKeyPrefix();
        //经销商信息反馈
        Schema.DescribeSObjectResult Dealer = PM_Dealer_Feedback__c.SObjectType.getDescribe();
        strDealerUrl = '../'+Dealer.getKeyPrefix();
        //产品反馈
        Schema.DescribeSObjectResult ProductFeedback = PM_Product_Feedback__c.SObjectType.getDescribe();
        strProductFeedbackUrl = '../'+ProductFeedback.getKeyPrefix();
        //其他病人反馈
        Schema.DescribeSObjectResult OtherPatientFeedback = PM_Patient_Feedback__c.SObjectType.getDescribe();
        strOtherPatientFeedbackUrl = '../'+OtherPatientFeedback.getKeyPrefix();
        //PSR拜访月计划
        Schema.DescribeSObjectResult PM_MonthPlan = PM_PSRMonthPlan__c.SObjectType.getDescribe();
        strPlanUrl = '../' + PM_MonthPlan.getKeyPrefix();	
    }

     /*******************test start***********************/
    static testMethod void MyTest() 
    {
        PM_HomeUrlDevController tab = new PM_HomeUrlDevController();
    }

}