/**
 * Author：Dean
 * Date：2013-11-08
 * Function：PM_PatientDataStatisticsBatch的测试类
 */
@isTest
private class PM_Test_PatientDataStatisticsBatch {

    static testMethod void myUnitTest() {
        V2_Account_Team__c vat = new V2_Account_Team__c();
        vat.UserProduct__c = 'PD';
        vat.V2_History__c = false;
        vat.V2_ApprovalStatus__c = '审批通过';
        vat.V2_Effective_Year__c = String.valueOf(Date.today().year());
        vat.V2_Effective_Month__c = String.valueOf(Date.today().month());
        //vat.V2_LastAccessDate__c = date.newinstance(2013, 10, 17);
        insert vat;
        //新建省市
        Region__c zone = new Region__c();
        zone.Name = '南区';
        insert zone;
        Provinces__c pro = new Provinces__c();
        pro.Name = '浙江省';
        insert pro;
        Cities__c city = new Cities__c();
        city.Name = '杭州市';
        city.BelongToProvince__c = pro.Id;
        insert city;
        list<RecordType> record = [Select Id From RecordType Where Name = '医院'];
        list<RecordType> record2 = [Select Id From RecordType Where Name = '经销商'];
        Account acc = new  Account();
        acc.Name = '浙江医院';
        acc.RecordTypeId = record[0].Id;
        acc.Provinces__c = pro.Id;
        acc.Cities__c = city.Id;
        acc.Region__c = zone.Id;
        insert acc;
        Account acc2 = new  Account();
        acc2.Name = '浙江医药控股';
        acc2.RecordTypeId = record2[0].Id;
        acc2.Provinces__c = pro.Id;
        acc2.Cities__c = city.Id;
        acc2.Region__c = zone.Id;
        insert acc2;
        Account acc3 = new  Account();
        acc3.Name = '北京医院';
        acc3.RecordTypeId = record[0].Id;
        acc3.Provinces__c = pro.Id;
        acc3.Cities__c = city.Id;
        acc3.Region__c = zone.Id;
        insert acc3;
        Account acc4 = new  Account();
        acc4.Name = '北京医药控股';
        acc4.RecordTypeId = record2[0].Id;
        acc4.Provinces__c = pro.Id;
        acc4.Cities__c = city.Id;
        acc4.Region__c = zone.Id;
        insert acc4;
        /*****************************Steven update 2014-2-13*****************************/
        PM_Hospital_Project__c pm1 = new PM_Hospital_Project__c();
        pm1.PM_Hospital__c = acc.Id;
        pm1.PM_StartTime__c = Date.today().addMonths(-1);
        pm1.PM_Project__c = 'cdc';
        insert pm1;
        PM_Hospital_Project__c pm2 = new PM_Hospital_Project__c();
        pm2.PM_Hospital__c = acc2.Id;
        pm2.PM_StartTime__c = Date.today().addMonths(-1);
        pm2.PM_Project__c = 'cdc';
        insert pm2;
        PM_Hospital_Project__c pm3 = new PM_Hospital_Project__c();
        pm3.PM_Hospital__c = acc3.Id;
        pm3.PM_StartTime__c = Date.today().addDays(5);
        pm3.PM_Project__c = 'cdc';
        insert pm3;
        PM_Hospital_Project__c pm4 = new PM_Hospital_Project__c();
        pm4.PM_Hospital__c = acc4.Id;
        pm4.PM_StartTime__c = Date.today().addDays(-3);
        pm4.PM_Project__c = null;
        insert pm4;
        /*****************************Steven update 2014-2-13*****************************/
        PM_PatientPlan__c pap = new PM_PatientPlan__c();
        pap.PM_Year__c = Date.today();
        pap.PM_Hospital__c = acc.Id;
        insert pap;
        PM_Patient__c pat =new PM_Patient__c();
        pat.Name = 'aaa';
        pat.PM_InHospital__c = acc.Id;
        pat.PM_Distributor__c = acc2.Id;
        pat.PM_Status__c = 'New';
        pat.PM_PreviousStatus__c = 'New';
        pat.PM_DropOut_One_Reason__c = '转用竞争产品';
        pat.PM_TransformBrand__c = '天安';
        pat.PM_DropOut_Two_Reason__c = '价格更低';
        pat.PM_NewPatientDate__c = Date.today();
        insert pat;
        PM_Patient__c pat2 =new PM_Patient__c();
        pat2.Name = 'bbb';
        pat2.PM_InHospital__c = acc3.Id;
        pat2.PM_Distributor__c = acc4.Id;
        pat2.PM_DropOut_One_Reason__c = '转用竞争产品';
        pat2.PM_TransformBrand__c = '天安';
        pat2.PM_Status__c = 'Dropout';
        pat2.PM_PreviousStatus__c = 'New';
        pat2.PM_NewPatientDate__c = Date.today();
        insert pat2;
        PM_Patient__c pat3 =new PM_Patient__c();
        pat3.Name = 'bbb';
        pat3.PM_InHospital__c = acc.Id;
        pat3.PM_DropOut_One_Reason__c = '转用竞争产品';
        pat3.PM_DropOut_Two_Reason__c = '价格更低';
        pat3.PM_Distributor__c = acc2.Id;
        pat3.PM_Status__c = 'New';
        pat3.PM_PreviousStatus__c = 'Dropout';
        pat3.PM_NewPatientDate__c = Date.today();
        insert pat3;
        /*
       	List<PM_HospitalBasie__c> pmholist = new List<PM_HospitalBasie__c>();
        PM_HospitalBasie__c pmho = new PM_HospitalBasie__c();
        pmho.PM_YearMonth__c = Date.today();
        
        pmho.PM_Surviving__c = 10;
        pmho.PM_NewPatient__c = 10;
        pmho.PM_Hospital__c = acc.Id;
        pmho.PM_TransfornDropOut__c = 10;
        pmho.PM_TOTMonths__c = 10;
        pmho.PM_BaxterTOTMonths__c = 10;
        pmho.PM_UniquelyId__c = String.valueOf(acc.Id)+String.valueOf(Date.today().year())+String.valueOf(Date.today().addmonths(-1).month());
        pmho.PM_Dropout__c = 10;
        pmho.PM_Transforn__c = 10;
        pmho.PM_TranfirmParient__c = 10;
        pmholist.add(pmho);
        PM_HospitalBasie__c pmho1 = new PM_HospitalBasie__c();
        pmho1.PM_YearMonth__c = Date.today();
       
        pmho1.PM_Surviving__c = 10;
        pmho1.PM_NewPatient__c = 10;
        pmho1.PM_Hospital__c = acc3.Id;
        pmho1.PM_TransfornDropOut__c = 10;
        pmho1.PM_TOTMonths__c = 10;
        pmho1.PM_BaxterTOTMonths__c = 10;
        pmho1.PM_UniquelyId__c = String.valueOf(acc3.Id)+String.valueOf(Date.today().year())+String.valueOf(Date.today().month());
        pmho1.PM_Dropout__c = 10;
        pmho1.PM_Transforn__c = 10;
        pmho1.PM_TranfirmParient__c = 10;
        pmholist.add(pmho1);
        
        
        PM_HospitalBasie__c pmho15 = new PM_HospitalBasie__c();
        pmho15.PM_YearMonth__c = Date.today();
       
        pmho15.PM_Surviving__c = 10;
        pmho15.PM_NewPatient__c = 10;
        pmho15.PM_Hospital__c = acc3.Id;
        pmho15.PM_TransfornDropOut__c = 10;
        pmho15.PM_TOTMonths__c = 10;
        pmho15.PM_BaxterTOTMonths__c = 10;
        pmho15.PM_UniquelyId__c = String.valueOf(acc3.Id)+String.valueOf(Date.today().year())+String.valueOf(Date.today().addmonths(-11).month());
        pmho15.PM_Dropout__c = 10;
        pmho15.PM_Transforn__c = 10;
        pmho15.PM_TranfirmParient__c = 10;
        pmholist.add(pmho15);
        
        
        
        PM_HospitalBasie__c pmho2 = new PM_HospitalBasie__c();
        pmho2.PM_YearMonth__c = Date.today();
        
        pmho2.PM_Surviving__c = 10;
        pmho2.PM_NewPatient__c = 10;
        pmho2.PM_Hospital__c = acc.Id;
        pmho2.PM_TransfornDropOut__c = 10;
        pmho2.PM_TOTMonths__c = 10;
        pmho2.PM_BaxterTOTMonths__c = 10;
        pmho2.PM_UniquelyId__c = String.valueOf(acc.Id)+String.valueOf(Date.today().year())+String.valueOf(Date.today().month());
        pmho2.PM_Dropout__c = 10;
        pmho2.PM_Transforn__c = 10;
        pmho2.PM_TranfirmParient__c = 10;
        pmholist.add(pmho2);
        
        
        PM_HospitalBasie__c pmho3 = new PM_HospitalBasie__c();
        pmho3.PM_YearMonth__c = Date.today();
        
        pmho3.PM_Surviving__c = 10;
        pmho3.PM_NewPatient__c = 10;
        pmho3.PM_Hospital__c = acc.Id;
        pmho3.PM_TransfornDropOut__c = 10;
        pmho3.PM_TOTMonths__c = 10;
        pmho3.PM_BaxterTOTMonths__c = 10;
        pmho3.PM_UniquelyId__c = String.valueOf(acc.Id)+String.valueOf(Date.today().year())+String.valueOf(Date.today().addmonths(-1).month());
        pmho3.PM_Dropout__c = 10;
        pmho3.PM_Transforn__c = 10;
        pmho3.PM_TranfirmParient__c = 10;
        pmholist.add(pmho3);
        
        PM_HospitalBasie__c pmho4 = new PM_HospitalBasie__c();
        pmho4.PM_YearMonth__c = Date.today();
        
        pmho4.PM_Surviving__c = 10;
        pmho4.PM_NewPatient__c = 10;
        pmho4.PM_Hospital__c = acc.Id;
        pmho4.PM_TransfornDropOut__c = 10;
        pmho4.PM_TOTMonths__c = 10;
        pmho4.PM_BaxterTOTMonths__c = 10;
        pmho4.PM_UniquelyId__c = String.valueOf(acc.Id)+String.valueOf(Date.today().year())+String.valueOf(Date.today().addmonths(-2).month());
        pmho4.PM_Dropout__c = 10;
        pmho4.PM_Transforn__c = 10;
        pmho4.PM_TranfirmParient__c = 10;
        pmholist.add(pmho4);
        
        PM_HospitalBasie__c pmho5 = new PM_HospitalBasie__c();
        pmho5.PM_YearMonth__c = Date.today();
        
        pmho5.PM_Surviving__c = 10;
        pmho5.PM_NewPatient__c = 10;
        pmho5.PM_Hospital__c = acc.Id;
        pmho5.PM_TransfornDropOut__c = 10;
        pmho5.PM_TOTMonths__c = 10;
        pmho5.PM_BaxterTOTMonths__c = 10;
        pmho5.PM_UniquelyId__c = String.valueOf(acc.Id)+String.valueOf(Date.today().year())+String.valueOf(Date.today().addmonths(-3).month());
        pmho5.PM_Dropout__c = 10;
        pmho5.PM_Transforn__c = 10;
        pmho5.PM_TranfirmParient__c = 10;
        pmholist.add(pmho5);
        
        
        PM_HospitalBasie__c pmho6 = new PM_HospitalBasie__c();
        pmho6.PM_YearMonth__c = Date.today();   
        pmho6.PM_Surviving__c = 10;
        pmho6.PM_NewPatient__c = 10;
        pmho6.PM_Hospital__c = acc.Id;
        pmho6.PM_TransfornDropOut__c = 10;
        pmho6.PM_TOTMonths__c = 10;
        pmho6.PM_BaxterTOTMonths__c = 10;
        pmho6.PM_UniquelyId__c = String.valueOf(acc.Id)+String.valueOf(Date.today().addyears(-1).year())+String.valueOf(Date.today().month());
        pmho6.PM_Dropout__c = 10;
        pmho6.PM_Transforn__c = 10;
        pmho6.PM_TranfirmParient__c = 10;
        pmholist.add(pmho6);

        insert pmholist;
        */
        //V2_Account__r
        PM_PatientDataStatisticsBatch btBatch = new PM_PatientDataStatisticsBatch();
        database.executeBatch(btBatch);
    }
}