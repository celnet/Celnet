/*
 * Author: Steven Ke
 * Date: 2014-2-18
 * Description: Vablet系统-SEP系统 销售代表 匹配
 */
global class Vablet_RepMatchBatch implements Database.Batchable<Vablet_DocumentRetrival__c>
{
	global Iterable<Vablet_DocumentRetrival__c> start(Database.BatchableContext BC)
	{
		Date yday = Date.today().addDays(-1);
		Date tday = Date.today().addDays(1);
		return [Select 
					Vablet_Folder__c, 
					Vablet_Folder1__c, 
					Vablet_Folder2__c, 
					Vablet_Folder3__c, 
					Vablet_Folder4__c, 
					Vablet_Folder5__c, 
					Vablet_Folder6__c, 
					Vablet_Folder7__c, 
					Vablet_SalesEmail__c, 
					Id,
					OwnerId 
				From 
					Vablet_DocumentRetrival__c
				Where CreatedDate >: yday And Vablet_StartTime__c <: tday];
	
	}
	
	global void execute(Database.BatchableContext BC, List<Vablet_DocumentRetrival__c> scope)
	{
		List<User> userList = [Select Id, Email From User Where IsActive = true And Profile.UserLicense.Name = 'Salesforce'];
		List<Vablet_DocumentRetrival__c> vdrList = new List<Vablet_DocumentRetrival__c>();
		for(Vablet_DocumentRetrival__c vdr : scope)
		{
			// 对 Vablet_Folder根据/进行拆分
			if(vdr.Vablet_Folder__c != null)
			{
				List<String> vfList = vdr.Vablet_Folder__c.split('/');
				if(vfList.size() == 1)
				{
					vdr.Vablet_Folder1__c = vfList[0];
				}
				if(vfList.size() == 2)
				{
					vdr.Vablet_Folder1__c = vfList[0];
					vdr.Vablet_Folder2__c = vfList[1];
				}
				if(vfList.size() == 3)
				{
					vdr.Vablet_Folder1__c = vfList[0];
					vdr.Vablet_Folder2__c = vfList[1];
					vdr.Vablet_Folder3__c = vfList[2];
				}
				if(vfList.size() == 4)
				{
					vdr.Vablet_Folder1__c = vfList[0];
					vdr.Vablet_Folder2__c = vfList[1];
					vdr.Vablet_Folder3__c = vfList[2];
					vdr.Vablet_Folder4__c = vfList[3];
				}
				if(vfList.size() == 5)
				{
					vdr.Vablet_Folder1__c = vfList[0];
					vdr.Vablet_Folder2__c = vfList[1];
					vdr.Vablet_Folder3__c = vfList[2];
					vdr.Vablet_Folder4__c = vfList[3];
					vdr.Vablet_Folder5__c = vfList[4];
				}
				if(vfList.size() == 6)
				{
					vdr.Vablet_Folder1__c = vfList[0];
					vdr.Vablet_Folder2__c = vfList[1];
					vdr.Vablet_Folder3__c = vfList[2];
					vdr.Vablet_Folder4__c = vfList[3];
					vdr.Vablet_Folder5__c = vfList[4];
					vdr.Vablet_Folder6__c = vfList[5];
				}
				if(vfList.size() >= 7)
				{
					vdr.Vablet_Folder1__c = vfList[0];
					vdr.Vablet_Folder2__c = vfList[1];
					vdr.Vablet_Folder3__c = vfList[2];
					vdr.Vablet_Folder4__c = vfList[3];
					vdr.Vablet_Folder5__c = vfList[4];
					vdr.Vablet_Folder6__c = vfList[5];
					vdr.Vablet_Folder7__c = vfList[6];
				}
			}
			
			
			// 遍历userList 根据Email匹配用户, 并把用户赋值给Vablet_DocumentRetrival所有人
			if(vdr.Vablet_SalesEmail__c != null)
			{
				for(User u : userList)
				{
					if(vdr.Vablet_SalesEmail__c == u.Email)
					{
						vdr.OwnerId = u.Id;
					}
				}
			}
			vdrList.add(vdr);
		}
		update vdrList;
	}
	
	global void finish(Database.BatchableContext BC)
	{
		Vablet_EventMatchBatch vemb = new Vablet_EventMatchBatch();
		Database.executeBatch(vemb);
	}
}