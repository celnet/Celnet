/**
 * Author : Sunny
 * 业务机会页面下面，In Project列表，具备添加、修改
 **/
public class Ctrl_InProjectExt {
    private ID OppId;//业务机会ID
    private User CurrentUser;
    private Opportunity opp;
    public Integer SelectedIndex{get;set;}
    public Boolean blnCanNew{get;set;}
    public List<InProjectWrapper> list_InProWra{get;set;}//已存在费用信息的集合
    public Ctrl_InProjectExt(Apexpages.Standardcontroller controller){
        OppId = controller.getId();
        opp=[select id,ownerid from Opportunity where id=:OppId];
        list_InProWra = new List<InProjectWrapper>();
        this.initInProjectRecord();
        this.initCurrentUserInfo();
    }
    //初始化当前登陆人信息
    private void initCurrentUserInfo(){
        CurrentUser =[Select Id,UserRoleId,UserRole.Name,ProfileId,Profile.Name From User Where Id =: UserInfo.getUserId()];
        system.debug(opp.OwnerId );
        system.debug(CurrentUser.Id);
        system.debug(CurrentUser.UserRole.Name);
        /****************bill update 2013-7-22 start*******************/
        //任何有权限访问某业务机会的人都有权限向该业务机会中添加费用信息；
        //if(opp.OwnerId == CurrentUser.Id || CurrentUser.UserRole.Name.toUpperCase().contains('REP')){
        //    blnCanNew = false;
        //}else{
            blnCanNew = true;
        //}
        /****************bill update 2013-7-22 end*******************/
    }
    //初始化已存在的费用信息
    private void initInProjectRecord(){
        list_InProWra.clear();
        for(InProject_Expense__c InProject:[Select Id,Project_Expense__c,Product_Remark__c, Owner.Alias, OwnerId, CreatedDate From InProject_Expense__c Where Opportunity__c =: OppId Order By CreatedDate Desc]){
            InProjectWrapper InProWra = new InProjectWrapper();
            InProWra.InPro = InProject;
            InProWra.IsEditing = false;
            InProWra.IsShowBtn = true;
            InProWra.intIndex = list_InProWra.size()+1;
            list_InProWra.add(InProWra);
        }
        list_InProWra.add(new InProjectWrapper());
        this.initTotalLine();
    }
    //添加新行
    public void addLine(){
        InProjectWrapper InProWra = new InProjectWrapper();
        InProWra.InPro = new InProject_Expense__c();
        InProWra.InPro.Opportunity__c = OppId ;
        InProWra.IsEditing = true;
        InProWra.IsShowBtn = true;
        InProWra.intIndex = list_InProWra.size() + 1;
        if(list_InProWra.size() > 0){
            list_InProWra.add(0,InProWra);
        }else{
            list_InProWra.add(InProWra);
        }
    } 
    //删除行
    public void deleteLine(){
        InProject_Expense__c inPe;
        integer num = 0;
        for(Integer i=0;i<list_InProWra.size();i++){
            if(list_InProWra[i].intIndex == SelectedIndex){
                inPe = list_InProWra[i].InPro;
                //list_InProWra.remove(i);
                num = i;
                break;
            }
        }
        if(inPe.Id ==null)
        {
            list_InProWra.remove(num);
        }
        if(inPe!=null && inPe.Id !=null){
            if(opp.OwnerId == CurrentUser.Id || CurrentUser.Profile.Name.toUpperCase().contains('ADMIN') || CurrentUser.Profile.Name.toUpperCase().contains('系统管理员') || inPe.OwnerId == CurrentUser.Id){
                delete inPe;
                list_InProWra.remove(num);
            }else{
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '你没有权限删除该条费用信息') ;            
                ApexPages.addMessage(msg) ;
            }
        }
        this.initTotalLine();
    }
    //保存行
    public void saveLine(){
        InProjectWrapper InProWra = getSelectedLine();
        if(InProWra == null){
            
        }
        if(InProWra.InPro.Project_Expense__c == null){
            InProWra.InPro.Project_Expense__c.addError('您必须填写金额');
            return;
            //ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '您必须填写一个') ;            
            //ApexPages.addMessage(msg) ;
        }
        if(InProWra.InPro.Id == null){
            insert InProWra.InPro;
        }else{
            update InProWra.InPro;
        }
        InProWra.IsEditing=false;
        this.initInProjectRecord();
    }
    //编辑行
    public void editLine(){
        InProjectWrapper InProWra = getSelectedLine();
        if(opp.OwnerId == CurrentUser.Id || CurrentUser.Profile.Name.toUpperCase().contains('ADMIN') || CurrentUser.Profile.Name.toUpperCase().contains('系统管理员') || InProWra.InPro.OwnerId == CurrentUser.Id){
            InProWra.IsEditing = true;
            this.initTotalLine();
        }else{
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '你没有权限编辑该条费用信息') ;            
            ApexPages.addMessage(msg) ;
        }
    }
    //取消编辑
    public void cancelEdit(){
        InProjectWrapper InProWra = getSelectedLine();
        if(InProWra.InPro.Id == null){
            deleteLine();
        }else{
            //InProWra.IsEditing = false;
            initInProjectRecord();
        }
    }
    private void initTotalLine(){
        if(this.list_InProWra.size() > 0){
            list_InProWra.remove(this.list_InProWra.size() - 1);
        }
        InProjectWrapper TotalIpw = new InProjectWrapper();
        TotalIpw.IsEditing=false;
        TotalIpw.IsShowBtn = false;
        TotalIpw.InPro = new InProject_Expense__c();
        system.debug(TotalIpw.InPro.Project_Expense__c);
        system.debug(list_InProWra);
        for(InProjectWrapper ipw : list_InProWra){
            TotalIpw.InPro.Project_Expense__c 
            = (TotalIpw.InPro.Project_Expense__c==null?0:TotalIpw.InPro.Project_Expense__c) 
            + (ipw.InPro.Project_Expense__c==null?0:ipw.InPro.Project_Expense__c);
        }
        system.debug('total line sum?:'+TotalIpw.InPro.Project_Expense__c );
        list_InProWra.add(TotalIpw);
        system.debug(list_InProWra);
    }
    private InProjectWrapper getSelectedLine(){
        InProjectWrapper SelectedInPro ;
        system.debug(list_InProWra);
        for(InProjectWrapper ipw : list_InProWra){
            if(ipw.intIndex == SelectedIndex){
                SelectedInPro = ipw;
                break;
            }
        }
        return SelectedInPro;
    }
    //表示行的封装类
    public class InProjectWrapper{
        public Boolean blnSelected{get;set;}
        public Integer intIndex{get;set;}
        public InProject_Expense__c InPro{get;set;}
        public Boolean IsEditing{get;set;}
        public Boolean IsReadOnly{get{
            return !IsEditing;
        }set;}
        public Boolean IsShowBtn{get;set;}
    }
    
    static testMethod void myUnitTest() {
    	//Opp
    	Opportunity opp = new Opportunity();
    	opp.Name='Tessss';
    	opp.StageName = '发现需求';
    	opp.CloseDate = date.today().addMonths(1);
    	opp.Type = '新业务' ;
    	opp.ProductType__c = 'IVS' ;
    	insert opp;
    	system.Test.startTest();
    	//Apexpages.currentPage().getParameters().put();
    	Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(opp);
    	Ctrl_InProjectExt ext = new Ctrl_InProjectExt(controller);
    	
    	ext.addLine();
    	ext.SelectedIndex = ext.list_InProWra.size();
    	ext.list_InProWra[0].InPro.Project_Expense__c=100;
    	ext.saveLine();
    	ext.SelectedIndex = 1;
    	ext.editLine();
    	ext.cancelEdit();
    	ext.addLine();
    	ext.deleteLine();
    	
    	
    	system.Test.stopTest();
    }
}