/**
 * 作者:Bill
 * 时间：2013-8-5
 * 说明：根据用户选中的月份去执行销售报表预测的Batch
**/
public class SalesEstimateReportController {
	
	public integer month{get;set;}
	public boolean IsView{get;set;}
	
	public SalesEstimateReportController()
	{
		IsView = true;
		month = date.today().month();
	}
    //月
    public List<SelectOption> getListMonths()
    {
        list<SelectOption> options=new list<SelectOption>();
        options.add(new SelectOption('1','1'));options.add(new SelectOption('2','2'));
        options.add(new SelectOption('3','3'));options.add(new SelectOption('4','4'));
        options.add(new SelectOption('5','5'));options.add(new SelectOption('6','6'));
        options.add(new SelectOption('7','7'));options.add(new SelectOption('8','8'));
        options.add(new SelectOption('9','9'));options.add(new SelectOption('10','10'));
        options.add(new SelectOption('11','11'));options.add(new SelectOption('12','12'));
        return options;
    }
    
    public void SalesReport()
    { 
    	OpportunityReportDataBatch oppBatch = new OpportunityReportDataBatch();
    	oppBatch.curMonth = month;
        database.executeBatch(oppBatch,10);
        IsView = false;
    	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM , '数据已经执行运算，稍后会发送邮件告知您查看报表') ;            
        ApexPages.addMessage(msg) ;
    }
    
    public void TrendSalesReport()
    { 
    	OpportunityReportDataDistinctBatch oppTrendBatch = new OpportunityReportDataDistinctBatch();
    	oppTrendBatch.curMonth = month;
        database.executeBatch(oppTrendBatch,10);
        IsView = false;
    	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM , '数据已经执行运算，稍后会发送邮件告知您查看报表') ;            
        ApexPages.addMessage(msg);
    }
    
    /*******************test start***********************/
    static testMethod void MyTest() 
    {
    	SalesEstimateReportController con = new SalesEstimateReportController();
    	con.getListMonths();
    	con.SalesReport();
    }
    /*******************test end***********************/
}