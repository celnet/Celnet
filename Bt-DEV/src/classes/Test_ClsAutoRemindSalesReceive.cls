/**
 *  author : bill
 *  ClsAutoRemindSalesReceive的测试类
 */
@isTest
private class Test_ClsAutoRemindSalesReceive {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        //挥发罐
        List<VaporizerInfo__c> list_Vap = new List<VaporizerInfo__c>();
        VaporizerInfo__c vap = new VaporizerInfo__c();
        vap.Name = '627test';
        vap.Status__c = '库存';
        vap.location__c = '上海仓库';
        list_Vap.add(vap);
        VaporizerInfo__c vap1 = new VaporizerInfo__c();
        vap1.Name = '627test1';
        vap1.Status__c = '库存';
        vap1.location__c = '上海仓库';
        list_Vap.add(vap1);
        VaporizerInfo__c vap2 = new VaporizerInfo__c();
        vap2.Name = '627test2';
        vap2.Status__c = '使用';
        vap2.location__c = '医院';
        list_Vap.add(vap2);
        insert list_Vap;
        
        Date applyDate = Date.today().addDays(-15) ;
        //使用申请
        List<Vaporizer_Application__c> list_App = new List<Vaporizer_Application__c>();
        Vaporizer_Application__c vapApp = new Vaporizer_Application__c();
        vapApp.ApplyDate__c = applyDate;
        list_App.add(vapApp);
        insert list_App;
        //申请明细
        List<Vaporizer_Apply_Detail__c> list_ApplyDetail = new List<Vaporizer_Apply_Detail__c>();
        Vaporizer_Apply_Detail__c ApplyDetail = new Vaporizer_Apply_Detail__c();
        ApplyDetail.ApplyQty__c = 1;
        ApplyDetail.Vaporizer_Application__c = vapApp.Id;
        list_ApplyDetail.add(ApplyDetail);
        insert list_ApplyDetail;
        //退回申请
        List<Vaporizer_ReturnAndMainten__c> list_Ret = new List<Vaporizer_ReturnAndMainten__c>();
        Vaporizer_ReturnAndMainten__c vapRet = new Vaporizer_ReturnAndMainten__c();
        vapRet.ApplyDate__c = applyDate;
        list_Ret.add(vapRet);
        insert list_Ret;
        //发货明细
        List<Vaporizer_Application_Detail__c> list_AppDetail = new List<Vaporizer_Application_Detail__c>();
        Vaporizer_Application_Detail__c AppDetail = new Vaporizer_Application_Detail__c();
        AppDetail.Vaporizer_Application__c = vapApp.Id;
        AppDetail.Vaporizer_Apply_Detail__c = ApplyDetail.Id;
        AppDetail.IsDelivered__c = true;
        AppDetail.IsReceived__c = false;
        AppDetail.VaporizerInfo__c = vap.Id;
        list_AppDetail.add(AppDetail);
        Vaporizer_Application_Detail__c AppDetail1 = new Vaporizer_Application_Detail__c();
        AppDetail1.Vaporizer_ReturnAndMainten__c = vapRet.Id;
        AppDetail1.IsDelivered__c = true;
        AppDetail1.IsReceived__c = false;
        AppDetail1.VaporizerInfo__c = vap2.Id;
        list_AppDetail.add(AppDetail1);
        insert list_AppDetail;
        //退回明细
        List<Vaporizer_ReturnAndMainten_Detail__c> list_RetDetail = new List<Vaporizer_ReturnAndMainten_Detail__c>();
        Vaporizer_ReturnAndMainten_Detail__c RetDetail = new Vaporizer_ReturnAndMainten_Detail__c();
        RetDetail.Vaporizer_ReturnAndMainten__c = vapRet.Id;
        RetDetail.ReqireNewOne__c = true;
        RetDetail.IsReceived__c = false;
        RetDetail.VaporizerInfo__c = vap2.Id;
        list_RetDetail.add(RetDetail);
        insert list_RetDetail;
        //发货统计
        List<Vaporizer_Shipping__c> list_VapShipping = new List<Vaporizer_Shipping__c>();
        Vaporizer_Shipping__c VapShipping = new Vaporizer_Shipping__c();
        VapShipping.Type__c = '工程师发货';
        VapShipping.Vaporizer_Application_Detail__c = AppDetail.Id;
        VapShipping.VaporizerInfo__c = vap.Id;
        list_VapShipping.add(VapShipping);
  
        Vaporizer_Shipping__c VapShipping1 = new Vaporizer_Shipping__c();
        VapShipping1.Type__c = '销售收货';
        VapShipping1.Vaporizer_ReturnAndMainten_Detail__c = RetDetail.Id;
        VapShipping1.VaporizerInfo__c = vap2.Id;
        list_VapShipping.add(VapShipping1);
        insert list_VapShipping;
        
        system.Test.startTest();
        ClsAutoRemindSalesReceive remindReceive = new ClsAutoRemindSalesReceive();
        database.executeBatch(remindReceive);
        system.Test.stopTest();
    }
}