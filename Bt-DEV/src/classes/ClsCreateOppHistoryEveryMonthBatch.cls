/**
 * Sunny
 * 每个月最后一天创建业务机会历史记录(给所有进行中的业务机会做业务机会历史)
**/
global class ClsCreateOppHistoryEveryMonthBatch implements Database.Batchable<sObject>{
    global Database.QueryLocator  start(Database.BatchableContext BC){
        List<String> list_PDOppStage = new List<String>{'发现/验证机会','建立沟通渠道','需求分析','提交合作方案/谈判','跟进合作方案'};
        List<String> list_SPOppStage = new List<String>{'发现机会'};
        List<String> list_IVTOppStage = new List<String>{'搜集信息','甑选机会','沟通/导入理念','发现需求','达成共识','导入产品','业务推进'};
        List<String> list_HDOppStage = new List<String>{'发现/验证机会','建立沟通渠道','需求分析','提交合作方案/谈判','跟进合作方案'};
        
        return Database.getQueryLocator([SELECT Id,AccountId,StageName,OwnerId,Owner.V2_UserProduct__c FROM Opportunity 
                WHERE (StageName in: list_PDOppStage AND RecordType.DeveloperName = 'RENAL')
                OR (StageName in: list_SPOppStage AND RecordType.DeveloperName = 'ACC')
                OR (StageName in: list_SPOppStage AND RecordType.DeveloperName = 'ACC_Supervisor')  
                OR (StageName in: list_IVTOppStage AND RecordType.DeveloperName = 'IVT')
                OR (StageName in: list_IVTOppStage AND RecordType.DeveloperName = 'IVT_Approval')  
                OR (StageName in: list_HDOppStage AND RecordType.DeveloperName = 'RENAL')]);  
        
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope){
    	List<OpportunityHistory__c> list_OppHis = new List<OpportunityHistory__c>();
    	for(sObject sObj : scope){
    		Opportunity opp = (Opportunity)sObj;
    		OpportunityHistory__c oppHis = new OpportunityHistory__c();
            oppHis.ChangedDate__c = datetime.now();
            oppHis.Opportunity__c = opp.Id;
            oppHis.Account__c = opp.AccountId;
            oppHis.NewOppStage__c = opp.StageName;
            oppHis.PastOppStage__c = opp.StageName;
            oppHis.NewOwner__c = opp.OwnerId;
            oppHis.LastOwner__c = opp.OwnerId;
            oppHis.NewOwnerProduct__c = opp.Owner.V2_UserProduct__c ;
            oppHis.LastOwnerProduct__c = opp.Owner.V2_UserProduct__c ;
            list_OppHis.add(oppHis);
    	}
    	if(list_OppHis.size() > 0){
    		insert list_OppHis;
    	}
    }
    global void finish(Database.BatchableContext BC){
    	
    }
}