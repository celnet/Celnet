@isTest
private class V2_ClsSaveRoleHistory_Test {

    static testMethod void myUnitTest() {
    	system.Test.startTest();
        //Test V2_ClsSaveRoleHistory
        V2_ClsSaveRoleHistory.V2_ClsSaveRoleHistory('2012', '1');
        List<UserRole> list_urs=[select Id from UserRole limit 1];
        if(list_urs.size()>0)V2_ClsSaveRoleHistory.GetRoleTree(list_urs[0].Id);
        List<User> list_user=[select Id,Name,isActive,IsLeave__c from User order by Name];
        if(list_user.size()>0)
        {
        	User u=V2_ClsSaveRoleHistory.GetUserMethod(list_user);
        	//V2_ClsSaveRoleHistory.CheckUser('Supervisor', u);
        }
        //Test V2_ClsSaveRoleHistoryBatch
        set<ID> set_Role=new set<ID>();//User's Role ID set collection
        set<String> set_GBU=new set<String>{'Renal','MD','Bio'};//GBU set collection
        set<String> set_Level=new set<String>{'BU Head','National','Regional','Area ASM','District DSM','Supervisor','Rep'};//Level set collection
        List<UserRole> list_ur=[select Id,Name from UserRole];//Select all User's Role
        Map<ID,String> map_GBU=new Map<ID,String>();//UserRoleId and GBU map collection
        Map<ID,String> map_Level=new Map<ID,String>();//UserRoleId and Level map collection
        Map<ID,String> map_Territory=new Map<ID,String>();//UserRoleId and Territory map collection
        Map<ID,String> map_PG=new Map<ID,String>();//UserRoleId and PG map collection
        for(UserRole ur:list_ur)
        {
            if(ur.Name.contains('-') && ur.Name.length()-ur.Name.replaceAll('-','').length()==4)
            {
                Integer i=0;//init index
                string str_GBU,str_Level,str_Territory,str_PG,str_Title;//init GBU,Level,Territory,PG,Title
                i=ur.Name.lastIndexOf('-');//GET INDEX
                str_GBU=ur.Name.substring(0, ur.Name.indexOf('-'));//GET GBU
                str_Territory=ur.Name.substring(ur.Name.indexOf('-')+1, i);//GET Level+Territory+PG
                str_Level=str_Territory.substring(0, str_Territory.indexOf('-'));//GET Level
                str_Territory=str_Territory.substring(str_Territory.indexOf('-')+1,str_Territory.length());//GET Territory+PG
                str_PG=str_Territory.substring(str_Territory.indexOf('-')+1,str_Territory.length());//GET PG
                str_Territory=str_Territory.substring(0,str_Territory.indexOf('-'));//GET Territory
                str_Title=ur.Name.substring(i+1, ur.Name.length());//GET Title
                
                if(set_Level.contains(str_Level))
                {
                    if(set_GBU.contains(str_GBU))
                    {
                        set_Role.add(ur.Id);
                        if(!map_GBU.containsKey(ur.Id))map_GBU.put(ur.Id,str_GBU);
                        if(!map_Level.containsKey(ur.Id))map_Level.put(ur.Id,str_Level);
                        if(!map_Territory.containsKey(ur.Id))map_Territory.put(ur.Id,str_Territory);
                        if(!map_PG.containsKey(ur.Id))map_PG.put(ur.Id,str_PG);
                    }
                }
            }
        }
        
        if(set_Role.size()==0)return;
        else
        {
            String str_year='2012';
            String str_month='1';
            //select all user that match the rule
            List<UserRole> list_UserRole=[select Id,Name,ParentRoleId,(select Id,Name,isActive,IsLeave__c,IsOnHoliday__c,EmployeeNumber,Department,ManagerId,Renal_valid_super__c from Users order by Name)from UserRole where Id in:set_Role];
            //Execute the batch
            V2_ClsSaveRoleHistoryBatch vc=new V2_ClsSaveRoleHistoryBatch();
            vc.list_ur=list_UserRole;
            vc.str_year=str_year;
            vc.str_month=str_month;
            vc.map_GBU=map_GBU;
            vc.map_Level=map_Level;
            vc.map_PG=map_PG;
            vc.map_Territory=map_Territory;
            Database.executeBatch(vc,list_UserRole.size());
        }
        //Test V2_ClsSaveRoleHistoryScheduler
        V2_ClsSaveRoleHistoryScheduler vc=new V2_ClsSaveRoleHistoryScheduler();
        string startTime='0 0 0 * * ?';
        System.schedule('test',startTime,vc);
        //Test V2_ClsSaveRoleHistoryWebservice
        V2_ClsSaveRoleHistoryWebservice.V2_ClsCheckRoleHistoryHadExisted('2012', '1');
        V2_ClsSaveRoleHistoryWebservice.V2_ClsSaveRoleHistoryWebservice('2012', '1');
        //Test V2_Con_SaveRoleHistory
        V2_RoleHistory__c vr=new V2_RoleHistory__c(Name='TestName');insert vr;
        Apexpages.Standardcontroller controller=new Apexpages.Standardcontroller(vr);
        V2_Con_SaveRoleHistory vcs=new V2_Con_SaveRoleHistory(controller);
        List<V2_RoleHistory__c> list_vr=new List<V2_RoleHistory__c>();
        Apexpages.StandardSetController setController=new Apexpages.StandardSetController(list_vr);
        V2_Con_SaveRoleHistory vcsh=new V2_Con_SaveRoleHistory(setController);
        system.Test.stopTest();
    }
}