/*
 *Scott
 *2013-10-23
 *此功能只针对台湾地区用户
 *通过VF页面实现客户所有人的更改后自动提交审批。
*/
public with sharing class TW_CtrlAccChangeOwnerApproval {
	public String AccId {get;set;}
	public Boolean IsDisabled{get;set;}
	public Account Acc{get;set;}
	public TW_CtrlAccChangeOwnerApproval(ApexPages.StandardController controller)
	{
		 IsDisabled = false;
		 AccId = ApexPages.currentPage().getParameters().get('AccId');
		 if(AccId == null)
		 {
		  	 ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning, '必須指定壹個Account記錄！');            
             ApexPages.addMessage(msg);
             IsDisabled = true;
             return;
		 }
		 List<Account >Accs = [select Id,OwnerId,TW_ApprovalStatus__c,TW_ChangeOwnerApprovalStatus__c,TW_NewOwner__c from Account where Id =:AccId limit 1];
		 if(Accs==null || Accs.size()==0)
		 {
		 	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning, '必須指定壹個Account記錄！');            
         	ApexPages.addMessage(msg);
         	IsDisabled = true;
         	return;
		 }
		 Acc = Accs[0];
		 if(Acc.TW_ApprovalStatus__c !='通過')
		 {
		 	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning, '當前Account記錄還未審批通過無法進行此操作！');            
         	ApexPages.addMessage(msg);
         	IsDisabled = true;
         	return;
		 }
		 else if(Acc.TW_ChangeOwnerApprovalStatus__c =='審批中')
		 {
		 	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning, '當前Account記錄正在進行擁有者變更審批，需等此次審批結束後才可再次提交擁有者變更審批！');            
         	ApexPages.addMessage(msg);
         	IsDisabled = true;
         	return;
		 }
	}
	public PageReference SubmitApproval()
	{
		try
		{
			if(Acc.TW_NewOwner__c == Acc.OwnerId)
			{
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning, '新更改用戶和原用戶相同無需提交審批！');            
	         	ApexPages.addMessage(msg);
	         	return null;
			}
			Acc.TW_ChangeOwnerApprovalStatus__c = '未提交';
			update Acc;
			Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
	        req.setObjectId(Acc.Id);
	        Approval.ProcessResult result = Approval.process(req);
			return new PageReference('/'+AccId) ;
		}catch(Exception e)
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning, e.getMessage()+'   '+e.getlineNumber());            
         	ApexPages.addMessage(msg);
         	return null;
		}
	}
	public PageReference Cancel()
	{
		if(AccId!=null)
		{
			 return new PageReference('/'+AccId) ;
		}
		else
		{
			return new PageReference('/home/home.jsp') ;
		}
       
    }
    
    /*========================================测试类===================================*/
    static testMethod void TW_CtrlAccChangeOwnerApproval()
    {
	    //销售
	    UserRole RepUserRole = new UserRole() ;
	    RepUserRole.Name = 'TW South Sales Rep';
	    insert RepUserRole ;
	    /*用户简档*/
		//rep简档
	    Profile RepPro = [select Id from Profile where Name  = 'TW Sales Rep' limit 1];
		
		/************User************/
		List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ; 
	     
	    /*销售*/
	     User RepSu = new User();
	     RepSu.Username='RepSu@123.com';
	     RepSu.LastName='RepSu';
	     RepSu.Email='RepSu@123.com';
	     RepSu.Alias=user[0].Alias;
	     RepSu.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     RepSu.ProfileId=RepPro.Id;
	     RepSu.LocaleSidKey=user[0].LocaleSidKey;
	     RepSu.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     RepSu.EmailEncodingKey=user[0].EmailEncodingKey;
	     RepSu.CommunityNickname='RepSu';
	     RepSu.MobilePhone='12345678912';
	     RepSu.UserRoleId = RepUserRole.Id ;
	     RepSu.IsActive = true;
	     insert RepSu;
	     
    	Account Acc = new Account();
    	Acc.Name = 'TestAccount';
    	insert Acc;
    	
    	ApexPages.StandardController STcon = new ApexPages.StandardController(new Account());
    	TW_CtrlAccChangeOwnerApproval AccChangeOwnerApproval1 = new TW_CtrlAccChangeOwnerApproval(STcon);
    	//AccChangeOwnerApproval1.Cancel();
    	ApexPages.currentPage().getParameters().put('AccId',Acc.Id);
    	TW_CtrlAccChangeOwnerApproval AccChangeOwnerApproval2 = new TW_CtrlAccChangeOwnerApproval(STcon);
    	Acc.TW_ApprovalStatus__c ='通過';
    	Acc.TW_ChangeOwnerApprovalStatus__c ='審批中';
    	update Acc;
    	TW_CtrlAccChangeOwnerApproval AccChangeOwnerApproval3 = new TW_CtrlAccChangeOwnerApproval(STcon);
    	Acc.TW_ChangeOwnerApprovalStatus__c =null;
    	Acc.TW_NewOwner__c  = UserInfo.getUserId();
    	update Acc;
    	TW_CtrlAccChangeOwnerApproval AccChangeOwnerApproval4 = new TW_CtrlAccChangeOwnerApproval(STcon);
    	AccChangeOwnerApproval4.SubmitApproval();
    	Acc.TW_NewOwner__c  = RepSu.Id;
    	update Acc;
    	TW_CtrlAccChangeOwnerApproval AccChangeOwnerApproval5 = new TW_CtrlAccChangeOwnerApproval(STcon);
    	AccChangeOwnerApproval5.SubmitApproval();
    	//AccChangeOwnerApproval5.Cancel();
    	ApexPages.currentPage().getParameters().put('AccId','2222222222222');
    	TW_CtrlAccChangeOwnerApproval AccChangeOwnerApproval21 = new TW_CtrlAccChangeOwnerApproval(STcon);
    }
}