/*
*作者：Hank
*时间：2013-10-14 
*功能：往病人基础表、医院基础表这俩张基础表填充数据，往中间表填充数据，新增，退出，存活病人数
*2014-2-23 Sunny 调整，该batch不再生成医院基础表数据，而是生成病人数据统计表数据，
*赋值逻辑没有变化，病人数据统计表上将会有医院就出表的所有字段。
*/
global class PM_BasicTableBatch implements Database.Batchable<sObject>
{
    //时间坐标
    static final Date FIRST_DAY_THIS_MONTH = date.today().toStartOfMonth();//本月第一天
    static final Date TODAY = date.today();//今日
    static final Date TWELVE_MONTHS_AGO = date.today().addMonths(-11).toStartOfMonth();//12个月前
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator([Select Id From Account where RecordType.DeveloperName = 'RecordType']);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
    	/**************************TObe 2013.11.20 start********************************/
    	set<Id> HospitalId = new set<Id>();
    	for(sObject sObj : scope)
        {
            HospitalId.add(((Account)sObj).Id);
        }
        map<string,list<PM_Patient__c>> map_HosPm = new map<string,list<PM_Patient__c>>();
        for(PM_Patient__c pm  : [Select PM_Terminal__c,PM_InUser__c,PM_InUser__r.V2_UserProduct__c,PM_InUser__r.IsActive,PM_Disappearanc__c,Id,PM_InHospital__c,Name,PM_DropOut_Two_Reason__c, PM_Status__c,PM_TransfornDropout_Date__c,PM_TransfornCreateDate__c,PM_Distributor__c,PM_DropoutActualDate__c,PM_PreviousStatus__c,PM_NewPatientDate__c,PM_DropoutCreated_Date__c,PM_UnreachableDate__c,PM_DropOut_One_Reason__c,PM_InDate__c,PM_TransfornCreated_Date__c, PM_Payment__c, PM_Delivery__c From PM_Patient__c Where PM_InHospital__c IN:HospitalId])
        {
        	if(map_HosPm.containsKey(pm.PM_InHospital__c))
        	{
        		list<PM_Patient__c> list_pm = map_HosPm.get(pm.PM_InHospital__c);
        		list_pm.add(pm);
        		map_HosPm.put(pm.PM_InHospital__c,list_pm);
        	}
        	else
        	{
        		list<PM_Patient__c> list_pm = new list<PM_Patient__c>();
        		list_pm.add(pm);
        		map_HosPm.put(pm.PM_InHospital__c,list_pm);
        	}
        }
        /**************************TObe 2013.11.20 end********************************/
        //病人基础表list
        list<PM_PatientBasis__c> PatientBasis_list = new list<PM_PatientBasis__c>();

        //Key:医院ID+年月 value：PM_HospitalBasie__c
        //list<PM_HospitalBasie__c> HospitalBasie_list = new list<PM_HospitalBasie__c>();
        //病人数据统计表list
        List<PM_IntermediateTable__c> iTable_list = new List<PM_IntermediateTable__c>();
        
        //销售ID
        for(sObject sObj : scope)
        {
            Account acc = (Account)sObj;
            //医院基础表对象实例化
            //PM_HospitalBasie__c HospitalBasie = new PM_HospitalBasie__c();
            PM_IntermediateTable__c iTable = new PM_IntermediateTable__c();
            //“插管医院”字段关联插管医院
            //HospitalBasie.PM_Hospital__c = acc.Id;
            iTable.PM_HospitalID__c = acc.Id;
            //“年月”字段赋值为“今天”，公式字段（年月）自动获取当前年月
            //HospitalBasie.PM_YearMonth__c = date.today();
            iTable.PM_Date__c = date.today();
            //医院基础表外部ID：医院ID+当前年月，用于upsert操作
            //HospitalBasie.PM_UniquelyId__c = string.valueOf(acc.Id) + string.valueOf(date.today().year()) + string.valueOf(date.today().month());
            iTable.PM_UniquelyId__c = string.valueOf(acc.Id) + string.valueOf(date.today().year()) + string.valueOf(date.today().month());
            //“新病人数”初始值为0
            //HospitalBasie.PM_NewPatient__c = 0;
            iTable.PM_NewPatient__c = 0;
            //“存活病人数”初始值为0
            //HospitalBasie.PM_Surviving__c = 0;
            iTable.PM_Surviving__c = 0 ;
            //转竞品掉队人数
            //HospitalBasie.PM_TransfornDropOut__c = 0;
            iTable.PM_TransfornDropOut__c = 0;
            //转用竞品人数
            //HospitalBasie.PM_TranfirmParient__c = 0;
            iTable.PM_TranfirmParient__c = 0 ;
            //退出腹透时间
            //HospitalBasie.PM_TOTMonths__c = 0;
            iTable.PM_TOTMonths__c = 0;
            //百特退出腹透时间
            //HospitalBasie.PM_BaxterTOTMonths__c = 0;
            iTable.PM_BaxterTOTMonths__c = 0;
            //转归次数
            //HospitalBasie.PM_Transforn__c = 0;
            iTable.PM_Transforn__c = 0;
            //实际掉队病人数
            //HospitalBasie.PM_ActDropOut__c = 0;
            iTable.PM_ActDropOut__c = 0;
            //“退出病人数”初始值为0
            //HospitalBasie.PM_Dropout__c = 0;
            iTable.PM_Dropout__c = 0;
            //“无法联系病人数”初始值为0
            //HospitalBasie.PM_Unreachable__c = 0;
            //2014-3-5 Alisa 调整iTable.PM_Unreachable__c = 0;
            //“活动插管医院数”初始值置为0，表示默认当前医院为休眠状态
            //HospitalBasie.PM_ActiveInHospital__c = 0;
            iTable.PM_ActiveInHospital__c = 0;
            //“休眠插管医院数”初始值置为1，表示默认当前医院为休眠状态
            //HospitalBasie.PM_SleepInHospital__c = 1;
            iTable.PM_SleepInHospital__c = 1;
            //“新增插管医院数”初始值置为0，表示默认当前医院为非新增医院
            //HospitalBasie.PM_NewInHospital__c = 0;
            iTable.PM_NewInHospital__c = 0;
            //“插管医院数”初始值置为0，表示默认当前医院下无插管病人
            //HospitalBasie.PM_InHospital__c = 0;
            iTable.PM_InHospital__c = 0;
            //标志位：本月前当前医院下不存在插管病人 默认值true 表示本月前当前医院下不存在插管病人
            Boolean NoPatientBeforeThisMonth = true;
            //标志位：本月内当前医院下存在插管病人 默认值false 表示本月内当前医院下不存在插管病人
            Boolean HasPatientThisMonth = false;
            //遍历当前医院下的病人对象
            //为DropOut的病人计算百特腹透TOT时间
            //getBaxterTOTMonths为内部方法,获取病人转归或掉队的历史情况
            /**************************TObe 2013.11.20 start********************************/
            list<PM_Patient__c> list_pm = new list<PM_Patient__c>();
            if(map_HosPm.containsKey(acc.Id))
            {
            	list_pm = map_HosPm.get(acc.Id);
            }
            /**************************TObe 2013.11.20 end********************************/
            Map<ID,List<DATE>> map_length = getBaxterTOTMonths(list_pm);
            for(PM_Patient__c pat:list_pm)
            {
            	/***********************上月掉队的病人不在生成病人基本表信息*************************/
            	//2013-12-30 update bill
            	if(pat.PM_Status__c =='Dropout'&& pat.PM_DropoutCreated_Date__c < FIRST_DAY_THIS_MONTH)
            	{
            		continue;
            	}
            	/***********************上月掉队的病人不在生成病人基本表信息*************************/
                //病人基础表对象实例化
                PM_PatientBasis__c PatientBasis = new PM_PatientBasis__c();
                //2014-2-23 Sunny 设置病人基础表数据所有人为病人插管销售
                if(pat.PM_InUser__c != null && pat.PM_InUser__r.IsActive && pat.PM_InUser__r.V2_UserProduct__c=='PD')
                PatientBasis.OwnerId = pat.PM_InUser__c;
                //“病人”字段关联病人ID
                PatientBasis.PM_Patient__c = pat.Id;
                //"插管医院"字段关联医院ID
                PatientBasis.PM_Hospital__c = acc.Id;
                //“年月”字段赋值为“今天”，公式字段（年月）自动取当前年月
                PatientBasis.PM_Date__c = date.today();
                //“状态”字段值从医院下的病人上的“病人状态”字段获取
                PatientBasis.PM_Status__c = pat.PM_Status__c;
                //“原状态”字段值从医院下的病人上的“原状态”字段获取，如果病人对象上的“原状态”为空或空字符，则赋值为“Null”
                if(pat.PM_PreviousStatus__c == null || pat.PM_PreviousStatus__c =='')
                {
                    PatientBasis.PM_LastState__c = 'Null';
                }
                else
                {
                    PatientBasis.PM_LastState__c = pat.PM_PreviousStatus__c;
                }
                /**********************BILL add 2013-11-21**************************/
                //Unreachable病人数,病人状态是“Unreachable”，则该病人即是Unreachable病人，对应该字段的值是1，否则为0
                /*2014-3-5 Alisa 修改，要删除Unreachable病人数（PM_Unreachable__c），这个逻辑暂时不需要
             	if(pat.PM_Status__c == 'Unreachable')
                {
                    PatientBasis.PM_Unreachable__c = 1;
                }else{
                	PatientBasis.PM_Unreachable__c = 0;
                }
                */
                //无法联系原因
                //PatientBasis.PM_Disappearanc__c = pat.PM_Disappearanc__c;
                //2014-3-5 Alisa 修改 病人基础表中的无法联系原因的Api名字修改了
                PatientBasis.PM_Unreachable_Reason__c = pat.PM_Disappearanc__c;
                //终端配送商
                PatientBasis.PM_Terminal__c = pat.PM_Terminal__c;
                /**********************BILL add 2013-11-21**************************/
                //病人基础表外部ID：病人ID+当前年月，用于upsert操作
                PatientBasis.PM_UniquelyId__c = string.valueOf(pat.Id) + string.valueOf(date.today().year()) + string.valueOf(date.today().month());
                //因为病人对象上的“病人状态”字段有默认值“New”，值不存在Null或''的情况，这里不予判断
                //2013-12-31 Sunny 添加如果病人的创建日期是本月，无论病人状态是不是NEW，都需要将病人“新病人标志”置为1；
                //若创建日期不是本月，那么无论病人状态是什么，都需要将“新病人标志”置为0
                if(pat.PM_NewPatientDate__c >= FIRST_DAY_THIS_MONTH && pat.PM_NewPatientDate__c <= today)
                {
                    
                    PatientBasis.PM_NewPatientSign__c = 1;
                    /*2014-3-5 Alisa 新增，新病人数是1且当病人上的是否收到个人信息回执的值是“是”，新病人个人信息确认数是1，否则为0*/
                    /*
                    if(pat.PM_PeReInformation__c == '是')
                    {
                    	PatientBasis.PM_IsNewReceivedInfo__c = 1;
                    }
                    else 
                    {
                    	PatientBasis.PM_IsNewReceivedInfo__c = 0;
                    }
                    */
                    /*2014-3-5 Alisa 新增--------------End---------------*/
                }
                else
                {
                    //否则，病人基础表对象上的“新病人标志”置为0
                    PatientBasis.PM_NewPatientSign__c = 0;
                    //2014-3-5 Alisa 新增，新病人数是0，新病人个人信息确认数为0
                    //PatientBasis.PM_IsNewReceivedInfo__c = 0;
                }
                //2014-2-22 Alisa 添加如果病人上的转归创建日期是本月
                //则当前生成的病人基础表记录将转归病人标志置为1，否则置为0
                if(pat.PM_TransfornCreateDate__c >= FIRST_DAY_THIS_MONTH && pat.PM_TransfornCreateDate__c <= today)
                {
                    
                    PatientBasis.PM_TransformSign__c = 1;
                }
                else
                {
                    //否则，病人基础表对象上的“转归病人标志”置为0
                    PatientBasis.PM_TransformSign__c = 0;
                }
                if(pat.PM_Status__c=='Dropout')
                {
                    //如果病人对象上的病人状态为“Dropout”，病人基础表对象上的“退出病人标志”置为1
                    //如果病人对象上的病人状态为“Dropout”，病人基础表对象上的“存活病人标志”置为0
                    PatientBasis.PM_PatientSurvivalSign__c = 0;
                    PatientBasis.PM_DropoutPatientSign__c = 0;
                    //2014-2-23 Sunny 当病人为退出病人时，设置掉队一级原因、掉队二级原因、掉队实际日期
                    PatientBasis.PM_DropOut_One_Reason__c = pat.PM_DropOut_One_Reason__c;
                    PatientBasis.PM_DropOut_Two_Reason__c = pat.PM_DropOut_Two_Reason__c;
                    PatientBasis.PM_DropoutActualDate__c = pat.PM_DropoutActualDate__c;
                }
                else
                {
                    //如果病人对象上的病人状态不为“Dropout”，病人基础表对象上的“退出病人标志”置为0
                    //如果病人对象上的病人状态不为“Dropout”，病人基础表对象上的“存活病人标志”置为1
                    PatientBasis.PM_PatientSurvivalSign__c = 1;
                    /*2014-3-5 Alisa 新增，存活病人数是1且当病人上的是否收到个人信息回执的值是“是”，存活病人个人信息确认数是1，否则为0*/
                    /*
                    if(pat.PM_PeReInformation__c == '是')
                    {
                    	PatientBasis.PM_IsSurivingReceivedInfo__c = 1;
                    }
                    else 
                    {
                    	PatientBasis.PM_IsSurivingReceivedInfo__c = 0;
                    }
                    */
                    /*2014-3-5 Alisa 新增--------------End---------------*/
                    PatientBasis.PM_DropoutPatientSign__c = 0;
                    //如果当前病人状态不为Dropout，当前医院下的存活病人数+1
                    //HospitalBasie.PM_Surviving__c += 1;
                    iTable.PM_Surviving__c+=1;
                }
                /*******************************bill add 2013-11-7 start******************************/
                //送货方式
                PatientBasis.PM_Delivery__c = pat.PM_Delivery__c;
                //费用支付方式
                PatientBasis.PM_Payment__c = pat.PM_Payment__c; 
                /*******************************bill add 2013-11-7  end ******************************/
                /*******************************Dean add 2013-11-7  update ******************************/
                /* 2014-3-5 Alisa 修改，把病人基础表上的新病人提交数删掉，该段逻辑暂时不需要
                //当New病人创建日期在本月时为1，不在本月则为0
                if(pat.PM_NewPatientDate__c != null && pat.PM_NewPatientDate__c >= FIRST_DAY_THIS_MONTH && pat.PM_NewPatientDate__c <= today)
                {
                	PatientBasis.PM_IsNewApply__c = 1; 
                }
                else
                {
                	PatientBasis.PM_IsNewApply__c = 0; 
                }
                */
                /*******************************Dean add 2013-11-7  update ******************************/
                /*******************************Dean add 2013-11-7  update ******************************/
               	//将病人下的经销商放入病人基础表中
               	if(pat.PM_Distributor__c != null)
               	{
               		PatientBasis.PM_Distributor__c = pat.PM_Distributor__c;
               	}
                /*******************************Dean add 2013-11-7  update ******************************/
                //病人基础表字段赋值完毕
                PatientBasis_list.add(PatientBasis);
                
                //如果当前病人的“病人创建日期”为本月，当前医院下的新病人数+1
                if(pat.PM_NewPatientDate__c != null && pat.PM_NewPatientDate__c >= FIRST_DAY_THIS_MONTH && pat.PM_NewPatientDate__c <= TODAY)
                {
                    //HospitalBasie.PM_NewPatient__c += 1;
                    iTable.PM_NewPatient__c+=1;
                    //本月存在新病人，HasPatientThisMonth 置为 true
                    HasPatientThisMonth = true;
                    //如果循环结束时没有病人的“病人创建日期”为本月，则表示本月内当前医院下未增加插管病人
                }
                //如果当前病人的“掉队创建日期”为本月，当前医院下的退出病人数+1
                if(pat.PM_DropoutCreated_Date__c != null && pat.PM_DropoutCreated_Date__c >= FIRST_DAY_THIS_MONTH && pat.PM_DropoutCreated_Date__c <= TODAY)
                {
                    //HospitalBasie.PM_Dropout__c += 1;
                    iTable.PM_Dropout__c+=1;
                    //转竞品掉队人数
                    if(pat.PM_InDate__c != null)
                    /*******************************bill add 2013-11-7 update******************************/
                    //百特持续腹透时间
                    if(map_length.containsKey(pat.Id))
                    {
                        //HospitalBasie.PM_BaxterTOTMonths__c = HospitalBasie.PM_BaxterTOTMonths__c + ExecteTimeLength(map_length.get(pat.Id));
                        iTable.PM_BaxterTOTMonths__c = iTable.PM_BaxterTOTMonths__c + ExecteTimeLength(map_length.get(pat.Id));
                    }
                    //HospitalBasie.PM_BaxterTOTMonths__c = HospitalBasie.PM_BaxterTOTMonths__c + + pat.PM_InDate__c.daysBetween(pat.PM_DropoutActualDate__c) / 30;
                    if(pat.PM_DropOut_One_Reason__c == '转用竞争产品')
                    {
                        //转用竞品人数
                        //HospitalBasie.PM_TranfirmParient__c = HospitalBasie.PM_TranfirmParient__c + 1;
                        iTable.PM_TranfirmParient__c = iTable.PM_TranfirmParient__c + 1;
                        if(pat.PM_TransfornCreated_Date__c != null && pat.PM_TransfornCreated_Date__c >= FIRST_DAY_THIS_MONTH && pat.PM_TransfornCreated_Date__c <= TODAY)
                        {
                            //转竞品掉队
                            //HospitalBasie.PM_TransfornDropOut__c = HospitalBasie.PM_TransfornDropOut__c + 1;
                            iTable.PM_TransfornDropOut__c = iTable.PM_TransfornDropOut__c + 1;
                        }
                        if(pat.PM_InDate__c != null && pat.PM_TransfornDropout_Date__c != null)
                        {
	                        //退出腹透时间长度
	                        //HospitalBasie.PM_TOTMonths__c = HospitalBasie.PM_TOTMonths__c + pat.PM_InDate__c.daysBetween(pat.PM_TransfornDropout_Date__c);
	                        iTable.PM_TOTMonths__c = iTable.PM_TOTMonths__c + pat.PM_InDate__c.daysBetween(pat.PM_TransfornDropout_Date__c);
                        }
                    }
                    else
                    {
                        if(pat.PM_InDate__c != null && pat.PM_DropoutActualDate__c != null)
                        {
	                        //退出腹透时间长度
	                        //HospitalBasie.PM_TOTMonths__c = HospitalBasie.PM_TOTMonths__c + pat.PM_InDate__c.daysBetween(pat.PM_DropoutActualDate__c);
	                        iTable.PM_TOTMonths__c = iTable.PM_TOTMonths__c + pat.PM_InDate__c.daysBetween(pat.PM_DropoutActualDate__c);
                        }
                    }
                /*******************************bill add 2013-11-7  update ******************************/
                }
                //如果当前病人的“Unreachable创建日期”为本月，当前医院下的无法联系病人数+1
                /*2014-3-5 Alisa 修改，要删除Unreachable病人数（PM_Unreachable__c），这个逻辑暂时不需要
                if(pat.PM_UnreachableDate__c != null && pat.PM_UnreachableDate__c >= FIRST_DAY_THIS_MONTH && pat.PM_UnreachableDate__c <= TODAY)
                {
                    //HospitalBasie.PM_Unreachable__c += 1;
                    iTable.PM_Unreachable__c += 1;
                }
                */
                //如果当前病人的“实际掉队日期”为本月，当前医院下的实际掉队病人数+1
                if(pat.PM_DropoutActualDate__c != null && pat.PM_DropoutActualDate__c >= FIRST_DAY_THIS_MONTH && pat.PM_DropoutActualDate__c <= TODAY)
                {
                    //HospitalBasie.PM_ActDropOut__c += 1;
                    iTable.PM_ActDropOut__c += 1;
                }
                /*******************************bill add 2013-11-7  update ******************************/
                //如果当前病人的“转归创建创建日期”为本月，当前医院下的转归病人数+1
                if(pat.PM_TransfornCreateDate__c != null && pat.PM_TransfornCreateDate__c >= FIRST_DAY_THIS_MONTH && pat.PM_TransfornCreateDate__c <= TODAY)
                {
                    //HospitalBasie.PM_Transforn__c += 1;
                    iTable.PM_Transforn__c += 1;
                }
                /*******************************bill add 2013-11-7  update ******************************/  
            }
            //医院状态重新循环
            for(PM_Patient__c pat:list_pm)
            {
            	//如果进入病人循环，则表示当前医院下存在一个或以上病人，当前医院下的“插管医院数”置为1
                //HospitalBasie.PM_InHospital__c = 1;
                iTable.PM_InHospital__c = 1;
                //如果当前病人的“病人创建日期”在12个月之内，则表示12个月内该医院至少新增加1例插管病人
                if(pat.PM_NewPatientDate__c != null &&pat.PM_NewPatientDate__c >= TWELVE_MONTHS_AGO && pat.PM_NewPatientDate__c <= TODAY)
                {
                    //“活动插管医院数”初始值置为1，表示当前医院处于活动状态
                    //HospitalBasie.PM_ActiveInHospital__c = 1;
                    iTable.PM_ActiveInHospital__c = 1;
                    //“休眠插管医院数”初始值置为0，表示当前医院处于活动状态
                    //HospitalBasie.PM_SleepInHospital__c = 0;
                    iTable.PM_SleepInHospital__c = 0;
                }//如果循环结束都不存在病人的“病人创建日期”在12个月之内，则表示该医院仍处于休眠状态
                
                //如果当前病人的“病人创建日期”为本月之前的任意一天，则表示该医院存在历史病人 NoPatientBeforeThisMonth置为false
                if(pat.PM_NewPatientDate__c != null && pat.PM_NewPatientDate__c < FIRST_DAY_THIS_MONTH)
                {
                    //本月存在新病人，HasPatientThisMonth 置为 true
                     NoPatientBeforeThisMonth = false;
                    //如果循环结束时没有病人的“病人创建日期”为本月之前的任意一天，则表示当前医院下没有历史插管病人
                } 
            }
            //如果当前医院没有历史插管病人，并且本月存在新增病人，则该医院为新增医院
            if(NoPatientBeforeThisMonth && HasPatientThisMonth)
            {
                //HospitalBasie.PM_NewInHospital__c = 1;
                iTable.PM_NewInHospital__c = 1;
            }
            //医院基础表字段赋值完毕
            if(map_HosPm.containsKey(acc.Id))
            {
           	 	//HospitalBasie_list.add(HospitalBasie);
           	 	iTable_list.add(iTable);
            }
        }
        /*
        for(PM_HospitalBasie__c hb : HospitalBasie_list)
        {
        	if(hb.PM_TOTMonths__c != null)hb.PM_TOTMonths__c = hb.PM_TOTMonths__c/30;
        	if(hb.PM_BaxterTOTMonths__c != null)hb.PM_BaxterTOTMonths__c = hb.PM_BaxterTOTMonths__c/30;
        }
        */
        for(PM_IntermediateTable__c it : iTable_list)
        {
        	if(it.PM_TOTMonths__c != null)it.PM_TOTMonths__c = it.PM_TOTMonths__c/30;
        	if(it.PM_BaxterTOTMonths__c != null)it.PM_BaxterTOTMonths__c = it.PM_BaxterTOTMonths__c/30;
        	
        }
        system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@:'+PatientBasis_list.size());
        if(PatientBasis_list.size()>0)
        {
            upsert PatientBasis_list PM_UniquelyId__c;
        }
        //if(HospitalBasie_list.size()>0)
        if(iTable_list.size()>0)
        {
            //upsert HospitalBasie_list PM_UniquelyId__c;
            upsert iTable_list PM_UniquelyId__c;
        }
    }
    global void finish(Database.BatchableContext BC)
    {
        PM_WebServiceUtil.sendMail('医院基础表');
        //2014-2-23 Sunny 该batch运行完之后继续调用PM_PatientDataStatisticsBatch，生成病人数据统计表其他信息
        PM_PatientDataStatisticsBatch btBatch = new PM_PatientDataStatisticsBatch();
        database.executeBatch(btBatch , 10);
    }
    
    //百特TOT持续时间
    private Map<ID,List<DATE>> getBaxterTOTMonths(List<PM_Patient__c> list_pa)
    {
        Set<ID> set_pa = new Set<ID>();
        Map<ID,List<DATE>> map_timeLength = new Map<ID,List<DATE>>();
        //筛选出退出的病人
        for(PM_Patient__c pa : list_pa)
        {
            if(pa.PM_DropoutCreated_Date__c != null && pa.PM_DropoutCreated_Date__c >= FIRST_DAY_THIS_MONTH && pa.PM_DropoutCreated_Date__c <= TODAY)
            {
                List<DATE> list_date = new List<DATE>();
                if(pa.PM_InDate__c != null && pa.PM_DropoutActualDate__c != null)
                {
                	set_pa.Add(Pa.Id);
	                list_date.add(pa.PM_InDate__c);
	                map_timeLength.put(pa.Id, list_date);
                }
            }
        }
        //logger
        for(PM_Patient__History his : [Select p.ParentId, p.NewValue From PM_Patient__History p 
                where Field in ('PM_TransfornDate__c','PM_DropoutActualDate__c')
                and p.ParentId in : set_pa order by CreatedDate,Id asc])
        {
            //把转归日期历史和掉队历史考虑进来
            if(map_timeLength.containsKey(his.ParentId) && his.NewValue != null)
            {
                map_timeLength.get(his.ParentId).Add(Date.ValueOf(his.NewValue));
            }           
        }
        //筛选出退出的病人
        for(PM_Patient__c pa : list_pa)
        {
            if(map_timeLength.containsKey(pa.Id) && pa.PM_DropoutActualDate__c != null)
            {
                map_timeLength.get(pa.Id).add(pa.PM_DropoutActualDate__c);
            } 
        }
        return map_timeLength;
    }
    
    //计算在百特的腹透时间长度
    private double ExecteTimeLength(List<DATE> list_date)
    {
        double length = 0;
        Date IastDate;
        if(list_date.size()>1)
        {
            for(Integer i=0;i< list_date.size();i++)
            {
            	system.debug(list_date[i]+'$$$$$$$$$$$$$$$$$$$$$$$$');
            	if(i!=0)
            	{
	                if(Math.mod(i,2)==0)
	                {
	                    length -= IastDate.daysBetween(list_date[i]);
	                }else{
	                    length += IastDate.daysBetween(list_date[i]);
	                }
            	}
                IastDate = list_date[i];
            }
        }
        return length;
    }
}