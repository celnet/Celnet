/**
 * 作者：Sunny Sun
 * 说明：SEP PD KPI计算
**/
public class ClsHospitalDirectorKpiService {
    ID UserId ;
    Integer IntYear ;
    Integer IntMonth ;
    List<Event> list_ValidEvent ;
    public ClsHospitalDirectorKpiService(){
        
    }
    public ClsHospitalDirectorKpiService(ID sUserId,Integer sIntYear,Integer sIntMonth){
        UserId = sUserId;
        IntYear = sIntYear;
        IntMonth = sIntMonth ; 
    }
    
    //已完成、未过期的拜访
    private List<Event> getValidVisit(){
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        //List<Event> list_Visit=[Select Id,GAPlan__c,WhoId,Who.Name,Who.RecordTypeId,GAExecuteResult__c From Event ];
        List<Event> list_Visit=[Select Id,GAPlan__c,WhoId,GAExecuteResult__c From Event Where IsChild = false And RecordType.DeveloperName = 'V2_Event' And Done__c = true And V2_IsExpire__c = false And StartDateTime >: CheckStartDateTime And StartDateTime <: CheckEndDateTime And OwnerId =: UserId];
        return list_Visit ;
    }
    
    //获取有效的业务机会
    //2013-3-28 sunny 修改，该指标(相关事件达成率，相关拜访达成率)所有BU不需要考虑曾经进行中的业务机会只要检查当前进行中的业务机会
    //2013-3-29 sunny : 执行超简单逻辑，不用找曾经进行中的业务机会，
    private Set<ID> getValidOpportunity(){
        Date CheckDate = date.valueOf(IntYear+'-'+IntMonth+'-1');
        Date endMonthDate = CheckDate.addMonths(1);
        
        Set<ID> set_ValidOppId = new Set<ID>();
        //查找用户对应月份负责的业务机会
        //Set<ID> set_OppId = V2_UtilClass.GetOpportunity(IntYear,IntMonth,UserId);
        
        //一直在进行中的业务机会
        List<String> list_OppStage = new List<String>{'发现/验证机会','建立沟通渠道','需求分析','提交合作方案/谈判','跟进合作方案'};
        for(Opportunity opp : [Select Id,Renal_Opp_MGT__c From Opportunity 
                Where StageName in: list_OppStage 
                And RecordType.DeveloperName = 'RENAL'  
                And CloseDate >=: CheckDate
                And OwnerId =: UserId
                And CreatedDate <: endMonthDate]){
            if(opp.Renal_Opp_MGT__c == '接受' ||  opp.Renal_Opp_MGT__c == null){
                set_ValidOppId.add(opp.Id);
            }
        }
        /*
        //对应月份曾经在进行中的业务机会
        for(OpportunityHistory__c OppHistory : [Select Id,Opportunity__c,Opportunity__r.Renal_Opp_MGT__c From OpportunityHistory__c 
                Where ChangedDate__c >=: CheckDate 
                And ChangedDate__c <: endMonthDate 
                And (NewOppStage__c in: list_OppStage OR PastOppStage__c in: list_OppStage)
                And Opportunity__r.RecordType.DeveloperName = 'RENAL'  
                And Opportunity__r.CloseDate >=: CheckDate
                And Opportunity__c in: set_OppId]){
            if(OppHistory.Opportunity__r.Renal_Opp_MGT__c == '接受' ||  OppHistory.Opportunity__r.Renal_Opp_MGT__c == null){
                set_ValidOppId.add(OppHistory.Opportunity__c);
            }
        }
        */
        return set_ValidOppId;
        
    }
    
    //PD BU 负责医院的主管作为代表考核的KPI
    //KPI:业务机会相关拜访达成率
    public Map<String , Double> OpportunityVisitRate(){
        Map<String , Double> map_Result = new Map<String , Double>();
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        //获取所有有效的业务机会的ID
        Set<ID> set_OppId = getValidOpportunity();
        //有效跟进的业务机会数量
        Double ValidOppQty = 0;
        //所有进行中的业务机会数量
        Double OppQty = set_OppId.size();
        //查找这些业务机会的有效拜访（已完成，未过期）
        for(Opportunity objOpp : [Select Id,(Select StartDateTime, OwnerId, Done__c, V2_IsExpire__c From Events Where IsChild = false And RecordType.DeveloperName = 'V2_Event' And Done__c = true And V2_IsExpire__c = false And StartDateTime>:CheckStartDateTime And StartDateTime<:CheckEndDateTime) From Opportunity o Where Id in: set_OppId]){
            if(objOpp.Events!=null && objOpp.Events.size() >= 4){
                ValidOppQty++;
            }
        }
        map_Result.put('Target' , OppQty);
        map_Result.put('Finish' , ValidOppQty);
        if(OppQty == 0 || OppQty == null){
        	map_Result.put('Rate' , 0);
        }else{
        	map_Result.put('Rate' , ValidOppQty / OppQty);
        }
        
        return map_Result ;
    }
    //HD CRRT BU 负责医院的主管作为代表考核的KPI
    //KPI:业务机会相关拜访达成率
    public Map<String , Double> OpportunityVisitRateForHd(){
        Map<String , Double> map_Result = new Map<String , Double>();
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        //获取所有有效的业务机会的ID
        Set<ID> set_OppId = getValidOpportunity();
        //有效跟进的业务机会数量
        Double ValidOppQty = 0;
        //所有进行中的业务机会数量
        Double OppQty = set_OppId.size();
        //查找这些业务机会的有效拜访（已完成，未过期）
        for(Opportunity objOpp : [Select Id,(Select StartDateTime, OwnerId, Done__c, V2_IsExpire__c From Events Where IsChild = false And RecordType.DeveloperName = 'V2_Event' And Done__c = true And V2_IsExpire__c = false And StartDateTime>:CheckStartDateTime And StartDateTime<:CheckEndDateTime) From Opportunity o Where Id in: set_OppId]){
            if(objOpp.Events!=null && objOpp.Events.size() >= 4){
                ValidOppQty++;
            }
        }
        map_Result.put('Target' , OppQty);
        map_Result.put('Finish' , ValidOppQty);
        if(OppQty ==null || OppQty == 0){
        	map_Result.put('Rate' , 0);
        }else{
        	map_Result.put('Rate' , ValidOppQty / OppQty);
        }
        
        return map_Result ;
    }
    //sunny add
    //KPI:业务机会相关事件达成率
    //说明：
    public Map<String , Double> OpportunityEventRate(){
        Map<String , Double> map_Result = new Map<String , Double>();
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        //获取所有有效的业务机会的ID
        Set<ID> set_OppId = getValidOpportunity();
        //有效跟进的业务机会数量
        Double ValidOppQty = 0;
        //所有进行中的业务机会数量
        Double OppQty = set_OppId.size();
        //标准为8个
        Double OppStaQty = 8 ;
        //查找这些业务机会的有效事件（已完成）
        for(Opportunity objOpp : [Select Id,(Select StartDateTime, OwnerId, Done__c, V2_IsExpire__c From Events Where IsChild = false And RecordType.DeveloperName = 'V2_RecordType' And Done__c = true And StartDateTime>:CheckStartDateTime And StartDateTime<:CheckEndDateTime) From Opportunity o Where Id in: set_OppId]){
            if(objOpp.Events!=null && objOpp.Events.size() > 0){
                ValidOppQty++;
            }
        }
        map_Result.put('Target' , OppQty);
        map_Result.put('Finish' , ValidOppQty);
        if(OppQty ==null || OppQty == 0){
            map_Result.put('Rate' , 0);
        }else{
            map_Result.put('Rate' , ValidOppQty / OppQty);
        }
        return map_Result;
    }
    //KPI:拜访完成率
    //当月已完成且未过期的拜访数/计划拜访数。
    public Map<String , Double> VisitRate(){
        Map<String , Double> map_Result = new Map<String , Double>();
        //获取已完成未过期的拜访
        if(list_ValidEvent==null){
            list_ValidEvent = getValidVisit();
        }
        List<Event> list_Event = list_ValidEvent;
        //已完成未过期拜访数
        Double VisitQty = list_Event.size();
        
        //计划拜访数（当月月计划明细中的所有“计划次数”字段的值之和）
        List<MonthlyPlan__c> monthPlan = [Select Id,V2_TotalCallRecords__c From MonthlyPlan__c Where Year__c =: String.valueOf(IntYear) And Month__c =: String.valueOf(IntMonth) And OwnerId =: UserId Limit 1] ;
        Double PlanQty = 0;
        if(monthPlan.size()>0){
            PlanQty =  monthPlan[0].V2_TotalCallRecords__c;
        }
        
        
        map_Result.put('Target' , PlanQty);
        map_Result.put('Finish' , VisitQty);
        if(PlanQty==null || PlanQty==0){
            map_Result.put('Rate' , 0);
        }else{
            map_Result.put('Rate' ,  VisitQty/PlanQty );
        }
        return map_Result;
    }
    
    //KPI:访前计划及访后分析完成率
    //说明：当月已完成的未过期的拜访中填写访前计划和访后分析的比例((访前计划完成%+访后分析完成%)/2)
    public Map<String , Double> VisitPlanRate(){
        Map<String , Double> map_Result = new Map<String , Double>();
        //拜访次数
        double VisitNum;
        //访前计划次数
        double VisitPlanNum;
        //访后分析次数
        double VisitAnalysisNum;
        //访前计划完成率
        double VisitPlan;
        //访后反洗完成率
        double VisitAnalysis;
        
        //获取已完成未过期的拜访
        if(list_ValidEvent==null){
            list_ValidEvent = getValidVisit();
        }
        List<Event> list_Event = list_ValidEvent;
        
        VisitNum = list_Event.size() ;
        for(Event objEvent : list_Event){
            if(objEvent.GAPlan__c != null){
                VisitPlanNum = (VisitPlanNum==null?0:VisitPlanNum) + 1 ;
            }
            if(objEvent.GAExecuteResult__c != null){
                VisitAnalysisNum = (VisitAnalysisNum==null?0:VisitAnalysisNum) + 1 ;
            }
        }
        
        if(VisitNum != null && VisitPlanNum != null){
            VisitPlan = VisitPlanNum / VisitNum ;
        }
        if(VisitNum != null && VisitAnalysisNum != null){
            VisitAnalysis = VisitAnalysisNum / VisitNum;
        }
        map_Result.put('Target' , VisitNum);
        map_Result.put('Finish Before' , VisitPlanNum);
        map_Result.put('Finish After' , VisitAnalysisNum);
        map_Result.put('Rate' , ((VisitPlan==NULL?0:VisitPlan)+(VisitAnalysis==NULL?0:VisitAnalysis))/2);
        return  map_Result;
    }
    
    //KPI:拜访质量评分
    //主管协访后，在协访中根据拜访质量标准给予代表评分的平均分
    public Map<String , Double> VisitGrade(){
        Map<String , Double> map_Result = new Map<String , Double>();
        //获取已完成未过期的拜访
        if(list_ValidEvent==null){
            list_ValidEvent = getValidVisit();
        }
        List<Event> list_Visit = list_ValidEvent;
        List<ID> list_EventId = new List<ID>();
        for(Event eve : list_Visit){
            list_EventId.add(eve.Id);
        }
        Double Grade =0;
        Double Num = 0 ;
        for(AssVisitComments__c VisitComm : [Select Id,Grade__c From AssVisitComments__c Where EventId__c in: list_EventId And BeReviewed__c =: UserId And Grade__c != null]){
            Grade = Grade + Integer.valueOf(VisitComm.Grade__c);
            Num++;
        }
        map_Result.put('TotalGrade' , Grade);
        map_Result.put('TotalNum' , Num);
        if(Num == 0){
            map_Result.put('AVGGrade' , 0);
        }else{
            map_Result.put('AVGGrade' , Grade/Num);
        }
        
        return map_Result;
    }
    
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        //user role
        List<UserRole> listur= new List<UserRole>();
        UserRole ur1 = new UserRole();
        ur1.Name = 'Renal-Supervisor-华南-PD-Supervisor(U1)';
        listur.add(ur1);
        UserRole ur2 = new UserRole();
        ur2.Name = 'Renal-Rep-华南-PD-Rep(U2)';
        listur.add(ur2);
        insert listur;
        //user 
        list<User> user=[Select TimeZoneSidKey,ProfileId,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname, Alias From User Where IsActive = true limit 1];
        List<User> list_user = new List<User>();
        User user1=new User();
        user1.username='user1r2@123.com';
        user1.LastName='user1r2';
        user1.Email='user1r2@123.com';
        user1.Alias=user[0].Alias;
        user1.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user1.ProfileId=UserInfo.getProfileId();
        user1.LocaleSidKey=user[0].LocaleSidKey;
        user1.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user1.EmailEncodingKey=user[0].EmailEncodingKey;
        user1.CommunityNickname='chequ';
        user1.MobilePhone='12345678912';
        user1.IsActive = true;
        user1.UserRoleId=ur1.Id ;
        list_user.add(user1);
        User user2=new User();
        user2.username='user2r2@223.com';
        user2.LastName='user2r2';
        user2.Email='user2r2@223.com';
        user2.Alias=user[0].Alias;
        user2.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user2.ProfileId=UserInfo.getProfileId();
        user2.LocaleSidKey=user[0].LocaleSidKey;
        user2.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user2.EmailEncodingKey=user[0].EmailEncodingKey;
        user2.CommunityNickname='chequ1';
        user2.MobilePhone='22345678922';
        user2.IsActive = true;
        user1.UserRoleId=ur1.Id ;
        list_user.add(user2);
        insert list_user;
        
        //联系人
        List<Contact> listct= new List<Contact>();
        Contact ct1 = new Contact();
        ct1.FirstName = 'jim';
        ct1.LastName = 'Green';
        ct1.GISContactType__c ='关键客户';
        listct.add(ct1);
        Contact ct2 = new Contact();
        ct2.FirstName = 'Tom';
        ct2.LastName = 'Frank';
        ct2.GISContactType__c ='普通客户';
        listct.add(ct2);
        insert listct;
        
        //业务机会
        List<Opportunity> List_Opportunity = new List<Opportunity>();
        Opportunity opp = new Opportunity();
        opp.OwnerId = user1.Id;
        opp.Name = 'yewujihui';
        opp.StageName = '建立沟通渠道';
        opp.RecordTypeId = '012200000004SMJAA2';
        opp.CloseDate = date.valueOf(Datetime.now().year()+'-'+Datetime.now().month()+'-1');
        opp.Renal_Opp_MGT__c = '接受';
        List_Opportunity.add(opp);
        Opportunity opp1 = new Opportunity();
        opp1.OwnerId = user1.Id;
        opp1.Name = 'yewujihui1';
        opp1.RecordTypeId = '012200000004SMJAA2';
        opp1.StageName = '建立沟通渠道';
        opp1.CloseDate = date.valueOf(Datetime.now().year()+'-'+Datetime.now().month()+'-1');
        opp1.Renal_Opp_MGT__c = '接受';
        List_Opportunity.add(opp1);
        insert List_Opportunity;
        
        //业务机会历史记录
        List<OpportunityHistory__c> List_OpportunityHistory = new List<OpportunityHistory__c>();
        OpportunityHistory__c oppc = new OpportunityHistory__c();
        oppc.ChangedDate__c = date.valueOf(Datetime.now().year()+'-'+Datetime.now().month()+'-2');
        oppc.NewOppStage__c ='需求分析';
        oppc.PastOppStage__c = '提交合作方案/谈判';
        oppc.Opportunity__c = opp.Id;
        List_OpportunityHistory.add(oppc);
        insert List_OpportunityHistory;
        
        //活动
        List<Event> list_Visit = new List<Event>();
        Event ev1 = new Event();
        ev1.DurationInMinutes = 1;
        ev1.StartDateTime = datetime.now();
        ev1.OwnerId = user1.Id ;
        ev1.GAPlan__c = '1111111';
        ev1.GAExecuteResult__c = '111111';
        ev1.RecordTypeId = '01290000000NapsAAC';
        ev1.Done__c = true;
        ev1.V2_IsExpire__c = false; 
        ev1.WhoId = ct1.Id;
        ev1.WhatId = opp.Id;
        list_Visit.add(ev1);
        Event ev2 = new Event();
        ev2.DurationInMinutes = 2;
        ev2.StartDateTime = datetime.now();
        ev2.OwnerId = user1.Id ;
        ev2.GAPlan__c = '222222';
        ev2.GAExecuteResult__c = '222222';
        ev2.RecordTypeId = '01290000000NapsAAC';
        ev2.Done__c = true;
        ev2.V2_IsExpire__c = false; 
        ev2.WhoId = ct1.Id;
        ev2.WhatId = opp.Id;
        list_Visit.add(ev2);
        Event ev3 = new Event();
        ev3.DurationInMinutes = 3;
        ev3.StartDateTime = datetime.now();
        ev3.OwnerId = user1.Id ;
        ev3.GAPlan__c = '333333';
        ev3.GAExecuteResult__c = '3333333';
        ev3.RecordTypeId = '01290000000NapsAAC';
        ev3.Done__c = true;
        ev3.V2_IsExpire__c = false; 
        ev3.WhoId = ct1.Id;
        ev3.WhatId = opp.Id;
        list_Visit.add(ev3);
        Event ev4 = new Event();
        ev4.DurationInMinutes = 4;
        ev4.StartDateTime = datetime.now();
        ev4.OwnerId = user1.Id ;
        ev4.GAPlan__c = '333333';
        //ev4.GAExecuteResult__c = '3333333';
        ev4.RecordTypeId = '01290000000NapsAAC';
        ev4.Done__c = true;
        ev4.V2_IsExpire__c = false; 
        ev4.WhoId = ct2.Id;
        ev4.WhatId = opp.Id;
        list_Visit.add(ev4);
        insert list_Visit ;
        
        //月计划
        List<MonthlyPlan__c> List_MonthlyPlan = new List<MonthlyPlan__c>();
        MonthlyPlan__c mc1 = new MonthlyPlan__c();
        mc1.OwnerId = user1.Id;
        mc1.Year__c = string.valueOf(datetime.now().year());
        mc1.Month__c = string.valueOf(datetime.now().month());
        //mc1.V2_TotalCallRecords__c = 10;
        List_MonthlyPlan.add(mc1);
        insert List_MonthlyPlan;
        
        //月计划明细
        List<MonthlyPlanDetail__c> List_MonthlyPlanDetail = new List<MonthlyPlanDetail__c>();
        MonthlyPlanDetail__c mcc1 = new MonthlyPlanDetail__c();
        mcc1.Contact__c = ct1.Id;
        mcc1.Planned_Finished_Calls__c = 5;
        mcc1.MonthlyPlan__c = mc1.id;
        mcc1.AdjustedTimes__c = 1;
        List_MonthlyPlanDetail.add(mcc1);
        MonthlyPlanDetail__c mcc2 = new MonthlyPlanDetail__c();
        mcc2.Contact__c = ct1.Id;
        mcc2.Planned_Finished_Calls__c = 12;
        mcc2.MonthlyPlan__c = mc1.Id;
        mcc2.AdjustedTimes__c = 2;
        List_MonthlyPlanDetail.add(mcc2);
        MonthlyPlanDetail__c mcc3 = new MonthlyPlanDetail__c();
        mcc3.Contact__c = ct1.Id;
        mcc3.Planned_Finished_Calls__c = 10;
        mcc3.MonthlyPlan__c = mc1.Id;
        mcc3.AdjustedTimes__c = 1;
        List_MonthlyPlanDetail.add(mcc3);
        insert List_MonthlyPlanDetail;
        
        //协访质量点评
        List<AssVisitComments__c> List_AssVisitComments = new List<AssVisitComments__c>();
        AssVisitComments__c ac = new AssVisitComments__c();
        ac.EventId__c = ev1.Id;
        ac.BeReviewed__c = user1.Id;
        ac.Grade__c = '90';
        List_AssVisitComments.add(ac);
        AssVisitComments__c ac1 = new AssVisitComments__c();
        ac1.EventId__c = ev2.Id;
        ac1.BeReviewed__c = user1.Id;
        ac1.Grade__c = '90';
        List_AssVisitComments.add(ac1);
        AssVisitComments__c ac2 = new AssVisitComments__c();
        ac2.EventId__c = ev3.Id;
        ac2.BeReviewed__c = user1.Id;
        ac2.Grade__c = '90';
        List_AssVisitComments.add(ac2);
        AssVisitComments__c ac3 = new AssVisitComments__c();
        ac3.EventId__c = ev4.Id;
        ac3.BeReviewed__c = user1.Id;
        ac3.Grade__c = '90';
        List_AssVisitComments.add(ac3);
        insert List_AssVisitComments;
        
        system.test.startTest();
        ClsHospitalDirectorKpiService cls = new ClsHospitalDirectorKpiService(user1.Id , datetime.now().year() , datetime.now().month());
        Map<String , Double> a = cls.OpportunityVisitRate();
        Map<String , Double> b = cls.VisitPlanRate();
        Map<String , Double> c = cls.VisitRate();
        Map<String , Double> d = cls.VisitGrade();
        system.debug(a+'@@'+b+'@@'+c+'@@'+d);
        system.test.stopTest();
     }
}