public with sharing class V2_WipUtil {
	
	/**
	*获取指定时间日期的所在季节索引
	*/
	public static Integer getSeasonIndex(Datetime dt){ 
		return getSeasonIndex(dt.date());
	}
	
	/**
	*获取指定日期所在季节索引
	*/
	public static Integer getSeasonIndex(Date d){
		return getSeasonIndex(d.month());
	}
	
	/**
	*获取月份所在季节索引
	*/
	public static Integer getSeasonIndex(Integer month){
		Integer mod=Math.mod(month, 3);
		Integer idx=month/3;
		if(mod>0){
			idx=idx+1;
		}
		return idx;
	}
	
	/**
	*获取季度开始月份
	*/
	public static Integer getFirstMonthInSeason(Integer seasonIdx){
		if(seasonIdx<1)seasonIdx=1;
		if(seasonIdx>4)seasonIdx=4;
		Integer mI=3*(seasonIdx-1)+1;
		return mI;
	}
	
	/**
	*获取季度结束月份
	*/
	public static Integer getLastMonthInSeason(Integer seasonIdx){
		if(seasonIdx<1)seasonIdx=1;
		if(seasonIdx>4)seasonIdx=4;
		Integer mI=3*seasonIdx;
		return mI;
	}
	
	/**
	*获取指定日期所在季节的开始和结束日期,用来确定Bios有效业务机会时间范围,
	*考虑进Bios扩展3天(daysAppend=3).
	*d:日期,通常由月计划所在月份产生的日期
	*appendDays:在结束日期追加的天数
	*return
	*idx_0: start_datetime;
	*idx_1: end_datetime;
	*产生查询条件应为:
	*lastModifiedDatetime>=start_datetime && lastModifiedDatetime<end_datetime;
	*/
	public static List<Datetime> getSeason_Start_End_DT(Date d, Integer appendDays){
		Integer year=d.year();
		Integer monthIdx=d.month();
		
		Integer seasonIdx=getSeasonIndex(monthIdx);
		Integer startMonthIdx=getFirstMonthInSeason(seasonIdx);
		Integer endMonthIdx=getLastMonthInSeason(seasonIdx);
		
		Time t=Time.newInstance(00, 00, 00, 00);
		
		Datetime startDate=Date.newInstance(year, startMonthIdx, 1);
		Datetime endDate=Date.newInstance(year, endMonthIdx, 1);//季节末月的第一天
		endDate=endDate.addMonths(1);//获取季节末下一月份的第一天,查询条件中小于该日期即可
		if(appendDays>0){//扩展有效天数
			//比如,如果扩展3天,则要算到第四天开始的时候,因为条件是小于该结束日期
			endDate=endDate.addDays(appendDays);
		}
		Datetime startDT=startDate;//Datetime.newInstance(startDate);
		Datetime endDT=endDate;//Datetime.newInstance(endDate);
		
		List<Datetime> start_end_dt_list=new List<Datetime>();
		start_end_dt_list.add(startDT);
		start_end_dt_list.add(endDT);
		return start_end_dt_list;
	}
	
	 static testMethod void unitTest() {
	 	Date d=Date.newInstance(2012, 10, 10);
	 	Datetime dt=System.now();
	 	V2_WipUtil.getSeason_Start_End_DT(d, 3);
	 	V2_WipUtil.getLastMonthInSeason(0);
	 	V2_WipUtil.getLastMonthInSeason(1);
	 	V2_WipUtil.getLastMonthInSeason(5);
	 	
	 	V2_WipUtil.getFirstMonthInSeason(0);
	 	V2_WipUtil.getFirstMonthInSeason(1);
	 	V2_WipUtil.getFirstMonthInSeason(5);
	 	
	 	V2_WipUtil.getSeasonIndex(3);
	 	V2_WipUtil.getSeasonIndex(d);
	 	V2_WipUtil.getSeasonIndex(dt);
	 }
}