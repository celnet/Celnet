/**
 * Author：Bill
 * Date:2013.11.10
 * DESCRIPTION:PM_PSRRelationmanageController测试类 
 */
@isTest
private class PM_Test_PSRRelationmanageController 
{
    static testMethod void myUnitTest() 
    {
    	PM_Test_AllConfig.myUnitTest(); //公用方法，内涵必要的一些测试数据
    	
        List<UserRole> listur= new List<UserRole>();
        UserRole ur1 = new UserRole();
        ur1.Name = 'Renal-Supervisor-华南-PD-Supervisor(U1)';
        listur.add(ur1);
        UserRole ur2 = new UserRole();
        ur2.Name = 'Renal-Rep-华南-Ps-Rep(U2)';
        listur.add(ur2);
        UserRole ur3 = new UserRole();
        ur3.Name = 'Renal-Rep-华南-Pa-Rep(U3)';
        listur.add(ur3);
        insert listur;
        ur2.ParentRoleId = ur1.Id;
        update ur2;
        ur3.ParentRoleId = ur1.Id;
        update ur3;
        //user 
        list<User> user=[Select TimeZoneSidKey,ProfileId,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname, Alias From User Where IsActive = true limit 1];
        List<User> list_user = new List<User>();
        User user0=new User();
        user0.username='user0r2@023.com';
        user0.LastName='user0r2';
        user0.Email='user0r2@023.com';
        user0.Alias=user[0].Alias;
        user0.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user0.ProfileId=UserInfo.getProfileId();
        user0.LocaleSidKey=user[0].LocaleSidKey;
        user0.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user0.EmailEncodingKey=user[0].EmailEncodingKey;
        user0.CommunityNickname='chequ34';
        user0.MobilePhone='02345678902';
        user0.IsActive = true;
        user0.UserRoleId=ur3.Id ;
        list_user.add(user0);
        User user1=new User();
        user1.username='user1r2@123.com';
        user1.LastName='user1r2';
        user1.Email='user1r2@123.com';
        user1.Alias=user[0].Alias;
        user1.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user1.ProfileId=UserInfo.getProfileId();
        user1.LocaleSidKey=user[0].LocaleSidKey;
        user1.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user1.EmailEncodingKey=user[0].EmailEncodingKey;
        user1.CommunityNickname='chequ';
        user1.MobilePhone='12345678912';
        user1.IsActive = true;
        user1.UserRoleId=ur1.Id ;
        list_user.add(user1);
        User user2=new User();
        user2.username='user2r2@223.com';
        user2.LastName='user2r2';
        user2.Email='user2r2@223.com';
        user2.Alias=user[0].Alias;
        user2.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user2.ProfileId=UserInfo.getProfileId();
        user2.LocaleSidKey=user[0].LocaleSidKey;
        user2.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user2.EmailEncodingKey=user[0].EmailEncodingKey;
        user2.CommunityNickname='chequ1';
        user2.MobilePhone='22345678922';
        user2.IsActive = true;
        user2.UserRoleId=ur2.Id ;
        list_user.add(user2);
        insert list_user;

        //新建省市
        Region__c region = new Region__c();
        region.Name = '南区';
        insert region;
        Region__c region1 = new Region__c();
        region1.Name = '北区';
        insert region1;
        Provinces__c pro1 = new Provinces__c();
    	pro1.Name = '浙江省';
    	insert pro1;
		PM_Province__c pro = new PM_Province__c();
    	pro.Name = '浙江省';
    	insert pro;
    	Cities__c city = new Cities__c();
    	city.Name = '杭州市';
    	city.PM_Province__c = pro.Id;
    	city.BelongToProvince__c = pro1.Id;
    	insert city;
    	
    	//客户
    	list<RecordType> record = [Select Id From RecordType Where SobjectType = 'Account' and DeveloperName = 'RecordType'];
    	
        Account acc = new  Account();
        acc.Name = '浙江医院';
        acc.RecordTypeId = record[0].Id;
        acc.PM_Province__c = pro.Id;
        acc.Cities__c = city.Id;
        acc.Region__c = region.Id;
        insert acc;
        
        Account acc1 = new  Account();
        acc1.Name = '广州医院';
        acc1.RecordTypeId = record[0].Id;
        acc1.Region__c = region1.Id;
        insert acc1;
        
    	//病人
    	List<PM_Patient__c> list_pa = new List<PM_Patient__c>();
    	PM_Patient__c Patient = new PM_Patient__c();
    	Patient.PM_InHospital__c = acc.Id;
    	Patient.Name = '张先生';
		Patient.PM_Status__c = 'New';
		Patient.PM_NewPatientDate__c = Date.today().addMonths(-3);
		Patient.PM_LastSuccessDateTime__c  = DateTime.now().addMonths(-2);
		Patient.PM_VisitState__c = '失败';
		Patient.PM_InDate__c = Date.today().addMonths(-5);
    	list_pa.add(Patient);
    	PM_Patient__c Patient2 = new PM_Patient__c();
    	Patient2.Name = '张先生2';
    	Patient2.PM_InHospital__c = acc.Id;
		Patient2.PM_Status__c = 'Active';
		Patient2.PM_NewPatientDate__c = Date.today().addMonths(-2);
		Patient2.PM_LastSuccessDateTime__c  = Date.today().addMonths(-1);
		Patient2.PM_VisitState__c = '成功';
		Patient2.PM_InDate__c = Date.today().addMonths(-4);
    	list_pa.add(Patient2);
    	PM_Patient__c Patient3 = new PM_Patient__c();
    	Patient3.Name = '张先生3';
    	Patient3.PM_InHospital__c = acc.Id;
		Patient3.PM_Status__c = 'Unreachable';
		Patient3.PM_NewPatientDate__c = Date.today().addMonths(-6);
		Patient3.PM_LastSuccessDateTime__c  = DateTime.now().addMonths(-4);
		Patient3.PM_VisitState__c = '失败';
    	list_pa.add(Patient3);
    	PM_Patient__c Patient4 = new PM_Patient__c();
    	Patient4.Name = '张先生4';
    	Patient4.PM_InHospital__c = acc.Id;
		Patient4.PM_Status__c = 'Dropout';
		Patient4.PM_NewPatientDate__c = Date.today().addMonths(-1);
		Patient4.PM_LastSuccessDateTime__c  = Date.today();
		Patient4.PM_VisitState__c = '成功';
		Patient4.PM_InDate__c = Date.today().addMonths(-4);
    	list_pa.add(Patient4);
    	PM_Patient__c Patient5 = new PM_Patient__c();
    	Patient5.Name = '张先生5';
    	Patient5.PM_InHospital__c = acc1.Id;
		Patient5.PM_Status__c = 'Dropout';
		Patient5.PM_NewPatientDate__c = Date.today().addMonths(-1);
		Patient5.PM_LastSuccessDateTime__c  = DateTime.now();
		Patient5.PM_VisitState__c = '成功';
		Patient5.PM_InDate__c = Date.today().addMonths(-4);
    	list_pa.add(Patient5);
    	insert list_pa;
				
		PM_PSRRelation__c relation = new PM_PSRRelation__c();
		relation.PM_Status__c = '启用';
		relation.PM_Hospital__c = acc.Id;
        insert relation;
        
     	//销售医院关系
     	V2_Account_Team__c team1 = new V2_Account_Team__c();
 		team1.V2_BatchOperate__c = '新增';
		team1.V2_Account__c = acc1.Id;
		team1.V2_ApprovalStatus__c = '审批通过';
		team1.V2_Effective_Year__c = string.valueOf(date.today().year());
		team1.V2_Effective_Month__c = string.valueOf(date.today().month());
		team1.V2_User__c = user1.Id;
		team1.V2_History__c = false;
		insert team1;
        
        system.Test.startTest();
    	PM_PSRRelation__c psrr = new PM_PSRRelation__c();
    	ApexPages.StandardController controller = new ApexPages.StandardController(psrr);
        PM_PSRRelationmanageController psr = new PM_PSRRelationmanageController(controller);
        ApexPages.StandardSetController conset = psr.conset;
		psr.InHospital = '浙江医院';
		psr.PSR.PM_NewPSR__c = user0.Id;
    	psr.Province = null;
    	psr.City = null;
    	psr.CheckWithOutPSR();
        List<PM_PSRRelationmanageController.PSRWrapper> list_pagePSR = psr.list_pagePSR;
    	psr.next();
    	psr.first();
    	psr.last();
    	psr.previous();
    	psr.Check();
    	for(PM_PSRRelationmanageController.PSRWrapper wra : psr.list_Allpsr)
    	{
    		wra.IsChecked = true;
    	}
    	psr.Save();
    	psr.ContinueSave();

        PM_PSRRelationmanageController psr1 = new PM_PSRRelationmanageController(controller);
    	psr1.InHospital = null;
    	psr1.Province = '浙江省';
    	psr1.Province = null;
    	psr1.City = null;
    	psr1.Check();
		psr1.Save();
    	psr1.ContinueSave();

        PM_PSRRelationmanageController psr2 = new PM_PSRRelationmanageController(controller);
    	psr2.City = '杭州市';
    	psr2.PSR.PM_NewPSR__c = user0.Id;
    	psr2.InHospital = null;
    	psr2.Province = null;
    	psr2.Check();
    	psr2.Save();
    	psr2.ContinueSave();
    	
        PM_PSRRelationmanageController psr3 = new PM_PSRRelationmanageController(controller);
        psr3.BigArea = '北区';
    	psr3.City = null;
    	psr3.PSR.PM_PDSaler__c = user1.Id;
    	psr3.PSR.PM_NewPSR__c = user0.Id;
    	psr3.InHospital = null;
    	psr3.Province = null;
    	psr3.CheckWithSaler();
    	psr3.CheckWithOutPSR();
    	List<PM_PSRRelationmanageController.PSRWrapper> list_pagePSR3 = psr3.list_pagePSR;
    	psr3.Save();
    	system.Test.stopTest();
    }
}