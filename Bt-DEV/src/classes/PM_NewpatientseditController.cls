/**
 * Hank
 * 2013-10-16
 * 功能：新病人创建根据电话姓名地址和新病人进行查重，重复的让用户判断是否保存，不重复直接保存
 */
public class PM_NewpatientseditController 
{
    public PatientApply__c patientApply{get;set;}
    public PatientApply__c patientApply_Old;
    public Boolean showformedit{get;set;}
    public Boolean showformRepeat{get;set;}
    public Boolean Verification{get;set;}//验证规则
    public list<InProjectWrapper> list_repeat{get;set;}
    public Boolean saveNews;
    public Boolean saveSuccess;
    private Boolean HaveDuplicatePatient = false;
    public PM_NewpatientseditController(ApexPages.StandardController controller)
    {
        patientApply_Old = (PatientApply__c)controller.getRecord();
        init();
    }
    public void init()
    {
        patientApply = new PatientApply__c();
        patientApply.IntubationHospital__c = patientApply_Old.IntubationHospital__c;
        patientApply.PatientApply__c = patientApply_Old.PatientApply__c;
        list_repeat = new list<InProjectWrapper>();
        showformedit = true;
        showformRepeat = false;
        saveNews = true;
    }
    //表示行的封装类
    public class InProjectWrapper
    {
        public String repeattype{get;set;}
        public PM_Patient__c pat{get;set;}
        public String tel{get;set;}
    }
    //确定保存新病人申请
    public Pagereference Hold()
    {
        list_repeat = new list<InProjectWrapper>();
        set<string> set_moblie = new set<string>();
        if(patientApply.PM_Famliy_Phone__c != '' && patientApply.PM_Famliy_Phone__c != null)
        {
            set_moblie.add(patientApply.PM_Famliy_Phone__c);
        }
        if(patientApply.PM_Patient_MPhone__c != '' && patientApply.PM_Patient_MPhone__c != null)
        {
            set_moblie.add(patientApply.PM_Patient_MPhone__c);
        }
        if(patientApply.PM_Patient_Phone__c != '' && patientApply.PM_Patient_Phone__c != null)
        {
            set_moblie.add(patientApply.PM_Patient_Phone__c);
        }
        if(patientApply.PM_Family_Member_Mphone__c != '' && patientApply.PM_Family_Member_Mphone__c != null)
        {
            set_moblie.add(patientApply.PM_Family_Member_Mphone__c);
        }
        if(patientApply.PM_Famliy_Member_Phone__c != '' && patientApply.PM_Famliy_Member_Phone__c != null)
        {
            set_moblie.add(patientApply.PM_Famliy_Member_Phone__c);
        }
        if(patientApply.PM_Other_Phone__c != '' && patientApply.PM_Other_Phone__c != null)
        {
            set_moblie.add(patientApply.PM_Other_Phone__c);
        }
        list <PM_Patient__c> queryResult = [SELECT Id,Name,PM_Address__c,PM_PmTel__c,PM_InHospital__c,PM_InHospital__r.Name,PM_Status__c,PM_FamilyTel__c,PM_PmPhone__c,PM_FamilyPhone__c,PM_HomeTel__c,PM_OtherTel2__c FROM PM_Patient__c WHERE Name =: patientApply.PatientName__c or PM_HomeTel__c in: set_moblie  or PM_PmPhone__c in: set_moblie  or PM_PmTel__c in: set_moblie  or PM_FamilyPhone__c in: set_moblie  or PM_FamilyTel__c in: set_moblie or PM_OtherTel2__c in: set_moblie or PM_Address__c =:patientApply.Address__c ];
        if(queryResult.size()!=null && queryResult.size() > 0)
        {
            Verification = false;
            HaveDuplicatePatient = true;
            if(patientApply.PM_Family_Member_Mphone__c !=null && patientApply.PM_Family_Member_Mphone__c !='')
            {
                system.debug(string.valueOf(patientApply.PM_Family_Member_Mphone__c).isNumeric());
                system.debug(string.valueOf(patientApply.PM_Family_Member_Mphone__c).length());
                if(string.valueOf(patientApply.PM_Family_Member_Mphone__c).length() != 11 && !string.valueOf(patientApply.PM_Family_Member_Mphone__c).isNumeric())
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, '家属手机号必须为11位不能包含文本');
                    ApexPages.addMessage(myMsg);
                    Verification = true;
                }
            }
            if(patientApply.PM_Patient_MPhone__c !=null && patientApply.PM_Patient_MPhone__c !='')
            {
                if(string.valueOf(patientApply.PM_Patient_MPhone__c).length() != 11 && !string.valueOf(patientApply.PM_Patient_MPhone__c).isNumeric())
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, '病人手机号必须为11位不能包含文本');
                    ApexPages.addMessage(myMsg);
                    Verification = true;
                }
            }
            if(patientApply.PM_Famliy_Member_Phone__c !=null && patientApply.PM_Famliy_Member_Phone__c !='')
            {
                if(!string.valueOf(patientApply.PM_Famliy_Member_Phone__c).contains('-'))
                {
                    if(!string.valueOf(patientApply.PM_Famliy_Member_Phone__c).isNumeric())
                    {
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, '家属电话不能包含文本');
                        ApexPages.addMessage(myMsg);
                        Verification = true;
                    }
                }
                else if(string.valueOf(patientApply.PM_Famliy_Member_Phone__c).contains('-') && (string.valueOf(patientApply.PM_Famliy_Member_Phone__c).split('-')).size() > 2)
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, '家属电话不能包含文本');
                    ApexPages.addMessage(myMsg);
                    Verification = true;
                }
                else if(string.valueOf(patientApply.PM_Famliy_Member_Phone__c).contains('-') && (string.valueOf(patientApply.PM_Famliy_Member_Phone__c).split('-')).size() <= 2)
                {
                    if(!(string.valueOf(patientApply.PM_Famliy_Member_Phone__c).replace('-','')).isNumeric())
                    {
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, '家属电话不能包含文本');
                        ApexPages.addMessage(myMsg);
                        Verification = true;
                    }
                }
            }
            if(patientApply.PM_Famliy_Phone__c !=null && patientApply.PM_Famliy_Phone__c !='')
            {
                if(!string.valueOf(patientApply.PM_Famliy_Phone__c).contains('-'))
                {
                    if(!string.valueOf(patientApply.PM_Famliy_Phone__c).isNumeric())
                    {
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, '家庭电话不能包含文本');
                        ApexPages.addMessage(myMsg);
                        Verification = true;
                    }
                }
                else if(string.valueOf(patientApply.PM_Famliy_Phone__c).contains('-') && (string.valueOf(patientApply.PM_Famliy_Phone__c).split('-')).size() > 2)
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, '家庭电话不能包含文本');
                    ApexPages.addMessage(myMsg);
                    Verification = true;
                }
                else if(string.valueOf(patientApply.PM_Famliy_Phone__c).contains('-') && (string.valueOf(patientApply.PM_Famliy_Phone__c).split('-')).size() <= 2)
                {
                    if(!(string.valueOf(patientApply.PM_Famliy_Phone__c).replace('-','')).isNumeric())
                    {
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, '家庭电话不能包含文本');
                        ApexPages.addMessage(myMsg);
                        Verification = true;
                    }
                }
            }
            if(patientApply.PM_Patient_Phone__c !=null && patientApply.PM_Patient_Phone__c !='')
            {
                if(!string.valueOf(patientApply.PM_Patient_Phone__c).contains('-'))
                {
                    if(!string.valueOf(patientApply.PM_Patient_Phone__c).isNumeric())
                    {
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, '病人电话不能包含文本');
                        ApexPages.addMessage(myMsg);
                        Verification = true;
                    }
                }
                else if(string.valueOf(patientApply.PM_Patient_Phone__c).contains('-') && (string.valueOf(patientApply.PM_Patient_Phone__c).split('-')).size() > 2)
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, '病人电话不能包含文本');
                    ApexPages.addMessage(myMsg);
                    Verification = true;
                }
                else if(string.valueOf(patientApply.PM_Patient_Phone__c).contains('-') && (string.valueOf(patientApply.PM_Patient_Phone__c).split('-')).size() <= 2)
                {
                    if(!(string.valueOf(patientApply.PM_Famliy_Member_Phone__c).replace('-','')).isNumeric())
                    {
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, '病人电话不能包含文本');
                        ApexPages.addMessage(myMsg);
                        Verification = true;
                    }
                }
            }
            /*
            if(patientApply.PM_Famliy_Phone__c !=null && patientApply.PM_Famliy_Phone__c !='')
            {
                system.debug('@@@@@@@@@@'+string.valueOf(patientApply.PM_Famliy_Phone__c).isNumeric());
                if(!string.valueOf(patientApply.PM_Famliy_Phone__c).isNumeric())
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, '家庭电话不能包含文本');
                    ApexPages.addMessage(myMsg);
                    Verification = true;
                }
            }
            if(patientApply.PM_Patient_Phone__c !=null && patientApply.PM_Patient_Phone__c !='')
            {
                if(!string.valueOf(patientApply.PM_Patient_Phone__c).isNumeric())
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, '病人电话不能包含文本');
                    ApexPages.addMessage(myMsg);
                    Verification = true;
                }
            }*/
            if(patientApply.PM_Famliy_Phone__c ==null && patientApply.PM_Patient_MPhone__c == null)
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, '家庭电话和病人手机必须要填写一个');
                ApexPages.addMessage(myMsg);
                Verification = true;
            }
            if(patientApply.IntubationDate__c > date.today())
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, '病人插管日期不能大于提交的日期');
                ApexPages.addMessage(myMsg);
                Verification = true;
            }
            if(patientApply.PatientApply__c != null)
            {
                MonthlyPlan__c monplan = [SELECT Year__c,Month2__c,OwnerId FROM MonthlyPlan__c WHERE Id =: patientApply.PatientApply__c];
                if(userinfo.getUserId() != monplan.OwnerId)
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, '该月计划不是你的，请不要在此报病人');
                    ApexPages.addMessage(myMsg);
                    Verification = true;
                }
                integer Years = integer.valueOf(monplan.Year__c);
                integer Months = integer.valueOf(monplan.Month2__c);
                if((date.today().year()!= Years || date.today().month()!=Months) &&  !(userinfo.getProfileId().contains('00e20000000kXBn')))
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, '只能在当月的月计划中提交病人');
                    ApexPages.addMessage(myMsg);
                    Verification = true;
                }
            }
            if(patientApply.IntubationHospital__c != null)
            {
                ID RecordTypeId = [SELECT RecordTypeId From Account WHERE Id =: patientApply.IntubationHospital__c].RecordTypeId;
                if(RecordTypeId != '012200000000DBD')
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, '请选择医院');
                    ApexPages.addMessage(myMsg);
                    Verification = true;
                }
            }
            if(patientApply.Distributor__c != null)
            {
                ID RecordTypeId = [SELECT RecordTypeId From Account WHERE Id =: patientApply.Distributor__c].RecordTypeId;
                if(RecordTypeId != '012200000000DCa')
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, '请选择经销商');
                    ApexPages.addMessage(myMsg);
                    Verification = true;
                }
            }
            if(Verification)
            {
                return ApexPages.currentPage();
            }
            
            
            
            showformedit = false;
            showformRepeat = true;
            for(PM_Patient__c p:queryResult)
            {
                InProjectWrapper Inpw = new InProjectWrapper();
                if(p.Name== patientApply.PatientName__c)
                {
                    Inpw.repeattype='姓名';
                    
                    if(p.PM_HomeTel__c!='' && p.PM_HomeTel__c!=null)
                    {
                            Inpw.tel= p.PM_HomeTel__c;
                    }
                    if(p.PM_PmPhone__c!='' && p.PM_PmPhone__c!=null)
                    {
                        if(Inpw.tel=='' || Inpw.tel==null)
                        {
                            Inpw.tel= p.PM_PmPhone__c;
                        }
                        else
                        {
                            Inpw.tel+=','+p.PM_PmPhone__c;
                        }
                    }
                    if(p.PM_PmTel__c!='' && p.PM_PmTel__c!=null)
                    {
                        if(Inpw.tel=='' || Inpw.tel==null)
                        {
                            Inpw.tel= p.PM_PmTel__c;
                        }
                        else
                        {
                            Inpw.tel+=','+p.PM_PmTel__c;
                        }
                    }
                    if(p.PM_FamilyPhone__c!='' && p.PM_FamilyPhone__c!=null)
                    {
                        if(Inpw.tel=='' || Inpw.tel==null)
                        {
                            Inpw.tel= p.PM_FamilyPhone__c;
                        }
                        else
                        {
                            Inpw.tel+=','+p.PM_FamilyPhone__c;
                        }
                    }
                    if(p.PM_FamilyTel__c!='' && p.PM_FamilyTel__c!=null)
                    {
                        if(Inpw.tel=='' || Inpw.tel==null)
                        {
                            Inpw.tel= p.PM_FamilyTel__c;
                        }
                        else
                        {
                            Inpw.tel+=','+p.PM_FamilyTel__c;
                        }
                    }
                    if(p.PM_OtherTel2__c!='' && p.PM_OtherTel2__c!=null)
                    {
                        if(Inpw.tel=='' || Inpw.tel==null)
                        {
                            Inpw.tel= p.PM_OtherTel2__c;
                        }
                        else
                        {
                            Inpw.tel+=','+p.PM_OtherTel2__c;
                        }
                    }
                }
                if(p.PM_Address__c== patientApply.Address__c)
                {
                    if(Inpw.repeattype=='' || Inpw.repeattype==null)
                    {
                        Inpw.repeattype='地址';
                    }
                    else
                    {
                        Inpw.repeattype+=',地址';
                    }
                    if(Inpw.tel==null)
                    {
                        if(p.PM_HomeTel__c!='' && p.PM_HomeTel__c!=null)
                        {
                            Inpw.tel= p.PM_HomeTel__c;
                        }
                        if(p.PM_PmPhone__c!='' && p.PM_PmPhone__c!=null)
                        {
                            if(Inpw.tel=='' || Inpw.tel==null)
                            {
                                Inpw.tel= p.PM_PmPhone__c;
                            }
                            else
                            {
                                Inpw.tel+=','+p.PM_PmPhone__c;
                            }
                        }
                        if(p.PM_PmTel__c!='' && p.PM_PmTel__c!=null)
                        {
                            if(Inpw.tel=='' || Inpw.tel==null)
                            {
                                Inpw.tel= p.PM_PmTel__c;
                            }
                            else
                            {
                                Inpw.tel+=','+p.PM_PmTel__c;
                            }
                        }
                        if(p.PM_FamilyPhone__c!='' && p.PM_FamilyPhone__c!=null)
                        {
                            if(Inpw.tel=='' || Inpw.tel==null)
                            {
                                Inpw.tel= p.PM_FamilyPhone__c;
                            }
                            else
                            {
                                Inpw.tel+=','+p.PM_FamilyPhone__c;
                            }
                        }
                        if(p.PM_FamilyTel__c!='' && p.PM_FamilyTel__c!=null)
                        {
                            if(Inpw.tel=='' || Inpw.tel==null)
                            {
                                Inpw.tel= p.PM_FamilyTel__c;
                            }
                            else
                            {
                                Inpw.tel+=','+p.PM_FamilyTel__c;
                            }
                        }
                        if(p.PM_OtherTel2__c!='' && p.PM_OtherTel2__c!=null)
                        {
                            if(Inpw.tel=='' || Inpw.tel==null)
                            {
                                Inpw.tel= p.PM_OtherTel2__c;
                            }
                            else
                            {
                                Inpw.tel+=','+p.PM_OtherTel2__c;
                            }
                        }
                    }
                }
                if(set_moblie.contains(p.PM_HomeTel__c) || set_moblie.contains(p.PM_PmPhone__c) || set_moblie.contains(p.PM_PmTel__c) || set_moblie.contains(p.PM_FamilyPhone__c ) || set_moblie.contains(p.PM_FamilyTel__c) || set_moblie.contains(p.PM_OtherTel2__c))
                {
                    if(Inpw.repeattype=='' || Inpw.repeattype==null)
                    {
                        Inpw.repeattype='电话';
                    }
                    else
                    {
                        Inpw.repeattype+=',电话';
                    }
                    if(Inpw.tel==null)
                    {
                        if(set_moblie.contains(p.PM_HomeTel__c))
                        {
                            Inpw.tel= p.PM_HomeTel__c;
                        }
                        if(set_moblie.contains(p.PM_PmPhone__c))
                        {
                            if(Inpw.tel=='' || Inpw.tel==null)
                            {
                                Inpw.tel= p.PM_PmPhone__c;
                            }
                            else
                            {
                                Inpw.tel+=','+p.PM_PmPhone__c;
                            }
                        }
                        if(set_moblie.contains(p.PM_PmTel__c))
                        {
                            if(Inpw.tel=='' || Inpw.tel==null)
                            {
                                Inpw.tel= p.PM_PmTel__c;
                            }
                            else
                            {
                                Inpw.tel+=','+p.PM_PmTel__c;
                            }
                        }
                        if(set_moblie.contains(p.PM_FamilyPhone__c ))
                        {
                            if(Inpw.tel=='' || Inpw.tel==null)
                            {
                                Inpw.tel= p.PM_FamilyPhone__c;
                            }
                            else
                            {
                                Inpw.tel+=','+p.PM_FamilyPhone__c;
                            }
                        }
                        if(set_moblie.contains(p.PM_FamilyTel__c))
                        {
                            if(Inpw.tel=='' || Inpw.tel==null)
                            {
                                Inpw.tel= p.PM_FamilyTel__c;
                            }
                            else
                            {
                                Inpw.tel+=','+p.PM_FamilyTel__c;
                            }
                        }
                        if(set_moblie.contains(p.PM_OtherTel2__c))
                        {
                            if(Inpw.tel=='' || Inpw.tel==null)
                            {
                                Inpw.tel= p.PM_OtherTel2__c;
                            }
                            else
                            {
                                Inpw.tel+=','+p.PM_OtherTel2__c;
                            }
                        }
                    }
                }
                Inpw.pat = p;
                list_repeat.add(Inpw);
            }
            return null;
        }
        else
        {
            system.debug('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&((((((((((((((((()))))))))))))))))');
            if(patientApply.PatientApply__c != null)
            {
                system.debug('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&');
                MonthlyPlan__c monplan = [SELECT Year__c,Month2__c,OwnerId FROM MonthlyPlan__c WHERE Id =: patientApply.PatientApply__c];
                integer Years = integer.valueOf(monplan.Year__c);
                integer Months = integer.valueOf(monplan.Month2__c);
                if((date.today().year()!= Years || date.today().month()!=Months) &&  !(userinfo.getProfileId().contains('00e20000000kXBn')))
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, '只能在当月的月计划中提交病人');
                    ApexPages.addMessage(myMsg);
                    return ApexPages.currentPage();
                }
            }
            try
            {
                /********************************bill add 2013-10-21 Add**************************************/
                //获取当前的PSR部门的负责的PSR
                getCurrentPSR();
                if(map_salePSR.containsKey(patientApply.IntubationHospital__c))
                {
                    patientApply.PM_PSR__c = map_salePSR.get(patientApply.IntubationHospital__c);
                }
                /********************************bill add 2013-10-21 end**************************************/
                //2014-03-19 Sunny设置新病人提交的存在疑似病人字段
                patientApply.PM_HaveDuplicatePatient__c = HaveDuplicatePatient;
                insert patientApply;
            }
            catch(DmlException ex)
            {
                ApexPages.addMessages(ex);
                return ApexPages.currentPage();
            }
            saveSuccess = true;
            string URL= '/' + patientApply.Id;
            //system.debug('%%%%%%%%%%%%%%%%%%%%%%%%%%%'+URL);
            return new Pagereference(URL);
        }
        /*
        try
        {
            insert patientApply;
        }
        catch(DmlException ex)
        {
            ApexPages.addMessages(ex);
            return ApexPages.currentPage();
        }
        string URL='https://'+ApexPages.currentPage().getHeaders().get('Host')+'/apex/PM_ReduplicatePatient?PatientId='+patientApply.id;
        return new Pagereference(URL);*/
    }
    public Pagereference Confirm ()
    {
        try
        {
            /********************************bill add 2013-10-21 Add**************************************/
            //获取当前的PSR部门的负责的PSR
            getCurrentPSR();
            if(map_salePSR.containsKey(patientApply.IntubationHospital__c))
            {
                patientApply.PM_PSR__c = map_salePSR.get(patientApply.IntubationHospital__c);
            }
            /********************************bill add 2013-10-21 end**************************************/
            patientApply.PM_HaveDuplicatePatient__c = HaveDuplicatePatient;
            insert patientApply;
        }
        catch(DmlException ex)
        {
            showformedit = true;
            showformRepeat = false;
            ApexPages.addMessages(ex);
            return ApexPages.currentPage();
        }
        //Schema.DescribeSObjectResult objectResult = Schema.getGlobalDescribe().get('PatientApply__c').getDescribe();
        string URL= '/' + patientApply.Id;
        if(saveNews == false)
        {
            init();
            return null;
        }
        else
        {
            return new Pagereference(URL);
        }
    }
    public void saveNew()
    {
        saveNews = false;
        Hold();
        if(showformedit == true && showformRepeat == false && saveSuccess == true)
        {
            init();
        }
    }
   /* //确定保存新病人申请
    public void HoldAndNew()
    {
        insert patientApply;
        patientApply = new PatientApply__c();
    }*/
    public void back()
    {
        //patientApply = new PatientApply__c();
        //list_repeat = new list<InProjectWrapper>();
        showformedit = true;
        showformRepeat = false;
    }
    //取消保存新病人申请
    public Pagereference Cancel()
    {//https://cs5.salesforce.com/003/o
        Schema.DescribeSObjectResult objectResult = Schema.getGlobalDescribe().get('PatientApply__c').getDescribe();
        string URL='https://'+ApexPages.currentPage().getHeaders().get('Host');
        URL+='/'+objectResult.getKeyPrefix()+'/o';
        return new Pagereference(URL);
    }

    /********************************bill add 2013-10-21 Add**************************************/
    //获取当前的PSR部门的负责的PSR
    private Map<ID,ID> map_salePSR;
    private void getCurrentPSR()
    {
        map_salePSR = NEW Map<ID,ID>();
        for(PM_PSRRelation__c psr : [Select p.PM_PSR__c , p.PM_Hospital__c  From PM_PSRRelation__c p where  
                                      PM_Hospital__c = : patientApply.IntubationHospital__c and PM_Status__c = '启用' AND PM_UneffectDate__c = null])
        {
            map_salePSR.put(psr.PM_Hospital__c,psr.PM_PSR__c);
        }
    }
    /********************************bill add 2013-10-21  end**************************************/
}