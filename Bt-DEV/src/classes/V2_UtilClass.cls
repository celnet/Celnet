/* 
 * Author: Sunny
 * Created on: 2011-12-31
 * Description:
 * edit by harvery 2012.1.14  重写了getuserRole方法的递归算法。
 * edit by Sunny 2013.2.1 添加方法，获取两个人之间所有角色的ID
 * Sunny: 2013-3-14 ,添加方法，获取某月份某人负责的业务机会
 */
public without sharing class V2_UtilClass 
{
	//Map,Key-父ID，value-子ID集合（包含下属的下属）。
	public Map<ID,List<ID>> map_RoleIds = new Map<ID,List<ID>>();
	//从map中获取所有下属
	Set<ID> set_UserRoleId ;
	//Map,Key-子ID，value-父ID
	public Map<ID,ID> map_Role = new Map<ID,ID>();
	//Map,Key-roleID，value-RoleName
	public Map<ID,String> map_roldName = new Map<ID,String>();
	
	/**
	获取用户的下属集合
	@pam userRoleid 要查找下属的用户id
	**/
	public Set<ID> GetUserSubIds()
	{
		Set<ID> retSet = new Set<ID> ();
		List<ID> list_UserId = new List<ID>() ;
		list_UserId = getSubordinateIds(UserInfo.getUserRoleId()) ;
		if(list_UserId != null && list_UserId.size() != 0)
		{
			retSet.addAll(list_UserId) ;
		}
		return retSet;
	}
	/**
	获取用户的下属集合
	@pam userRoleid 要查找下属的用户id
	**/
	public List<Id> getSubordinateIds(ID userRoleId)
	{
		List<ID> list_UserIds = new List<ID>() ;
		if(userRoleId == null)
		{
			return list_UserIds ;
		}
		this.initRole() ;
		set_UserRoleId = new Set<ID>();
		this.getUserRoleIds(userRoleId) ;
		if(set_UserRoleId!= null && set_UserRoleId.size() != 0)
		{
			set_UserRoleId.remove(userRoleId) ;
			system.debug('最终角色ID：'+set_UserRoleId) ;
			for(User objUser : [select id from user where UserRoleId in: set_UserRoleId And IsActive = true])
			{
				list_UserIds.add(objUser.Id) ;
			}
		}
		return list_UserIds ;
	}
	
	/**
	从map中获取所有下属id集合	
	**/
	private void getUserRoleIds(ID set_RoleIds)
	{
		List<ID> tempList;
		//获取直属下属
		if(map_RoleIds.ContainsKey(set_RoleIds)){
			tempList = map_RoleIds.get(set_RoleIds) ;
			set_UserRoleId.addAll(tempList);
		} else {
			return;
		}
		//递归获取下属的下属
		while(tempList.size()>0){
			tempList = getUserRoleIds(tempList);
			set_UserRoleId.addAll(tempList);
		}
		
	}
	/**
	 	获取集合中的每一个人的下属；
	**/
	private List<ID> getUserRoleIds(List<ID> lst){
		List<ID> retLst = new List<Id>();
		for(Id id : lst){
			if(map_RoleIds.ContainsKey(id)){
				List<ID> tempList = map_RoleIds.get(id) ;
				retLst.addAll(tempList);
			}
		}
		return retLst;
	}
	/**
	获取系统角色架构集合
	Map key=父id obj=直属下级id list
	**/
	
	public void initRole()
	{
		if(map_RoleIds.size() > 0){
			return;
		}
		List<ID> list_Ids  ;
		for(UserRole userRole : [Select u.ParentRoleId, u.Id, u.Name From UserRole u])
		{
			if(map_RoleIds.containsKey(userRole.ParentRoleId))
			{
				list_Ids = map_RoleIds.get(userRole.ParentRoleId) ;
				list_Ids.add(userRole.Id) ;
				map_RoleIds.put(userRole.ParentRoleId,list_Ids) ;
			}else
			{
				list_Ids = new List<ID>() ;
				list_Ids.add(userRole.Id) ;
				map_RoleIds.put(userRole.ParentRoleId,list_Ids) ;
			}
			map_Role.put(userRole.Id , userRole.ParentRoleId);
			map_roldName.put(userRole.Id , userRole.Name);
		}
	}
	
	/**
	获取当前登录用户是否是管理员
	**/
	public Boolean IfAdmin(){
    	Boolean flag = false;
    	Profile p = [Select Name From Profile where Id=:UserInfo.getProfileId()];
    	if(p.Name =='系统管理员' || p.Name =='System Administrator')
    	{
    		flag =true;
    	}
		return flag;
	}
	
	/**
	获取两个角色ID之间的所有角色ID
	**/
	public List<ID> getRoleId(ID HighLevelRoleId , ID LowLevelRoleId){
		List<ID> list_RoleId = new List<ID>();
		List<UserRole> list_Role = [Select u.ParentRoleId, u.Id From UserRole u Where Id =: LowLevelRoleId And ParentRoleId != null];
		if(list_Role!=null && list_Role.size() > 0){
			if(list_Role[0].ParentRoleId == HighLevelRoleId){
				//return list_RoleId;
				list_RoleId.add(list_Role[0].ParentRoleId);
			}else{
				list_RoleId.add(list_Role[0].ParentRoleId);
				list_RoleId.addAll(getRoleId(HighLevelRoleId , list_Role[0].ParentRoleId));
				
			}
		}
		return list_RoleId;
	}
	/**
            获取某角色的上级，直到RSM
    **/
    public List<ID> getRoleIdToRSM(ID roleId){
    	List<ID> list_RoleIdToRSM = new List<ID>();
        if(map_Role.size() == 0)this.initRole();
        
        if(map_Role.containsKey(roleId)){
        	ID rId = map_Role.get(roleId) ;
        	list_RoleIdToRSM.add(rId);
        	system.debug(map_roldName);
        	if(map_roldName.containsKey(rId) && !map_roldName.get(rId).toUpperCase().contains('REGIONAL')){
        		system.debug(map_roldName.get(rId));
        		list_RoleIdToRSM.addAll(getRoleIdToRSM(rId));
        	}
        }
        return list_RoleIdToRSM ;
    }
    /*
    //获取所需查询用户在某月份负责的业务机会ID
    */
    public static Set<ID> GetOpportunity(Integer sIntYear , Integer sIntMonth , ID UserId){
    	Date CheckDate = date.valueOf(sIntYear+'-'+sIntMonth+'-1');
        Date endMonthDate = CheckDate.addMonths(1);
    	//查询月之后owner变化为我的业务机会ID集合
        Set<ID> OppChangeToMe = new Set<ID>();
        //查询月之后owner由我转给别人的业务机会ID集合
        Set<ID> OppChangeFromMe = new Set<ID>();
        //查询业务机会历史，查找，查询月之后与我有关有所有人变化的业务机会
        for(OpportunityHistory__c OppHistory : [Select Id,Opportunity__c,LastOwner__c,NewOwner__c From OpportunityHistory__c
            Where ChangedDate__c >=: endMonthDate And (NewOwner__c =: UserId Or LastOwner__c =: UserId) ]){
            if(OppHistory.NewOwner__c != OppHistory.LastOwner__c){
                if(OppHistory.LastOwner__c == UserId){
                    OppChangeFromMe.add(OppHistory.Opportunity__c);
                }else if(OppHistory.NewOwner__c == UserId){
                    OppChangeToMe.add(OppHistory.Opportunity__c);
                }
            }
        }
        Set<ID> set_OppId = new Set<ID>();
        //查询业务机会
        //当前负责人是我，但是不是检查月份之后才转给我的
        for(Opportunity opp : [Select Id From Opportunity Where OwnerId =: UserId And Id Not in: OppChangeToMe]){
        	set_OppId.add(opp.Id);
        }
        //当前负责人不是我，但是是检查月份之后才由我转出去的
        for(Opportunity opp : [Select Id From Opportunity Where OwnerId !=: UserId And Id in: OppChangeFromMe]){
        	set_OppId.add(opp.Id);
        }
        return set_OppId;
    }
    public static Set<ID> GetOpportunity(Integer sIntYear , Integer sIntMonth , Set<ID> UserId){
        Date CheckDate = date.valueOf(sIntYear+'-'+sIntMonth+'-1');
        Date endMonthDate = CheckDate.addMonths(1);
        //查询月之后owner变化为我的业务机会ID集合
        Set<ID> OppChangeToMe = new Set<ID>();
        //查询月之后owner由我转给别人的业务机会ID集合
        Set<ID> OppChangeFromMe = new Set<ID>();
        //查询业务机会历史，查找，查询月之后与我有关有所有人变化的业务机会
        for(OpportunityHistory__c OppHistory : [Select Id,Opportunity__c,LastOwner__c,NewOwner__c From OpportunityHistory__c
            Where ChangedDate__c >=: endMonthDate And (NewOwner__c in: UserId Or LastOwner__c in: UserId) ]){
            if(OppHistory.NewOwner__c != OppHistory.LastOwner__c){
                if(UserId.contains(OppHistory.LastOwner__c)){
                    OppChangeFromMe.add(OppHistory.Opportunity__c);
                }else if(UserId.contains(OppHistory.NewOwner__c)){
                    OppChangeToMe.add(OppHistory.Opportunity__c);
                }
            }
        }
        Set<ID> set_OppId = new Set<ID>();
        //查询业务机会
        //当前负责人是我，但是不是检查月份之后才转给我的
        for(Opportunity opp : [Select Id From Opportunity Where OwnerId in: UserId And Id Not in: OppChangeToMe]){
            set_OppId.add(opp.Id);
        }
        //当前负责人不是我，但是是检查月份之后才由我转出去的
        for(Opportunity opp : [Select Id From Opportunity Where OwnerId not in: UserId And Id in: OppChangeFromMe]){
            set_OppId.add(opp.Id);
        }
        return set_OppId;
    }
	
	static testMethod void TestSub()
	{
		//----------------New UserRole ------------------
    	//List<UserRole> list_userRole = new List<UserRole >() ;
    	UserRole objUserRole = new UserRole() ;
    	objUserRole.Name = 'Renal-Rep-大上海-PD-Rep(陈喆令)' ;
    	insert objUserRole ;
    	UserRole objUserRole2 = new UserRole() ;
    	objUserRole2.Name = 'Renal-Rep-大上海-PB-Rep(陈喆令)' ;
    	objUserRole2.ParentRoleId = objUserRole.Id ;
    	insert objUserRole2 ;
    	UserRole objUserRole3 = new UserRole() ;
    	objUserRole3.Name = 'Renal-Rep-大上海-PC-Rep(陈喆令)' ;
    	objUserRole3.ParentRoleId = objUserRole.Id ;
    	insert objUserRole3 ;
		//----------------Create User-------------
    	List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.UserRoleId = objUserRole2.Id ;
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	use2.UserRoleId = objUserRole3.Id ;
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.UserRoleId = objUserRole.Id ;
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
    	//-------------Start Test------------------
    	system.test.startTest() ;
    	system.runAs(use3){
    		V2_UtilClass utilClass = new V2_UtilClass() ;
    		utilClass.GetUserSubIds() ;
    		
    	}
    	V2_UtilClass vClass = new V2_UtilClass() ;
    	vClass.getRoleIdToRSM(objUserRole.Id) ;
    	vClass.IfAdmin();
    	vClass.getSubordinateIds(null);
    	V2_UtilClass.GetOpportunity(date.today().year(), date.today().month(), use1.Id);
    	Set<ID> list_uId = new Set<ID>();
    	list_uId.add(use1.Id);
    	list_uId.add(use2.Id);
    	V2_UtilClass.GetOpportunity(date.today().year(), date.today().month(), list_uId);
    	system.test.stopTest() ;
	}
}