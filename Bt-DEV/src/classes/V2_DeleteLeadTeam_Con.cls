/* 
 * Author: Sunny
 * Created on: 2011-12-26
 * Description: 
 */
public without sharing class V2_DeleteLeadTeam_Con 
{
	public String strId{get;set;}
	public V2_Lead_Team__c objLeadTeam{get;set;}
	public String strMessage{get;set;}
	public boolean blnIsMesShow{get;set;}
	public boolean blnMeg{get;set;}
	private ID LeadId ;
	public V2_DeleteLeadTeam_Con(ApexPages.StandardController controller)
	{
		strId = controller.getId() ;
		//objLeadTeam = (V2_Lead_Team__c)controller.getSubject() ;
		objLeadTeam = [select id,V2_ImmediateDelete__c,V2_IsSyn__c,V2_Effective_date__c,V2_Lead__c,V2_Lead__r.V2_ApprovalStauts__c,V2_Lead__r.Name,V2_User__c,V2_User__r.Name,V2_Role__c,V2_Is_Delete__c from V2_Lead_Team__c where Id =: strId] ;
		LeadId = objLeadTeam.V2_Lead__c ;
		//取自己的下属
		V2_UtilClass cls = new V2_UtilClass();
		Set<Id> set_UserIds = cls.GetUserSubIds();
		
		//获取当前用户是否管理员
		Boolean ifAdmin = cls.IfAdmin();
		//判断是否是自己的下属
		if(!set_UserIds.contains(objLeadTeam.V2_User__c) && !ifAdmin)
		{
				blnIsMesShow = true ;
				strMessage = '您只能设置您的下属。' ;
				return ;
		}
		
		
		if(objLeadTeam.V2_Lead__r.V2_ApprovalStauts__c == '审批中' && !ifAdmin)
		{
			blnIsMesShow = true ;
			strMessage = '请在审批结束后执行删除操作。' ;
		} else if(objLeadTeam.V2_IsSyn__c==true && !ifAdmin){
			blnIsMesShow = true ;
			strMessage = '已经同步的记录不可以删除。' ;
			
		}		
		else
		{
			//this.delRecord() ;
			blnMeg = true ;
			strMessage = '确认删除？' ;
		}
	}
	public PageReference delRecord()
	{
		delete objLeadTeam ;
		return  new PageReference('/'+LeadId);
	}
	public PageReference saveChange()
	{
		return  new PageReference('/'+LeadId);
	}
	
	//测试
	static testMethod void TestPage() 
	{
		//----------------New UserRole ------------------
    	//List<UserRole> list_userRole = new List<UserRole >() ;
    	UserRole objUserRole = new UserRole() ;
    	objUserRole.Name = 'Renal-Rep-大上海-PD-Rep(陈喆令)' ;
    	insert objUserRole ;
    	UserRole objUserRole2 = new UserRole() ;
    	objUserRole2.Name = 'Renal-Rep-大上海-PB-Rep(陈喆令)' ;
    	objUserRole2.ParentRoleId = objUserRole.Id ;
    	insert objUserRole2 ;
		//----------------Create User-------------
    	List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.UserRoleId = objUserRole2.Id ;
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	use2.UserRoleId = objUserRole2.Id ;
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.UserRoleId = objUserRole.Id ;
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
    	//-------------New Lead------------------
    	List<Lead> list_lead = new List<Lead>() ;
    	Lead objLead = new Lead() ;
    	objLead.LastName = 'testlead' ;
    	objLead.FirstName = 'T' ;
    	objLead.Company = 'testcom' ;
    	objLead.V2_Mid__c = 'poiuuiop' ;
    	objLead.V2_ApprovalStauts__c = '待审批' ;
    	list_lead.add(objLead) ;
    	insert list_lead ;
    	//-----------------New Lead Team-----------------------
    	List<V2_Lead_Team__c> list_leadTeam = new List<V2_Lead_Team__c>();
    	V2_Lead_Team__c objLeadTeam1 = new V2_Lead_Team__c() ;
    	objLeadTeam1.V2_User__c = use1.Id ;
    	objLeadTeam1.V2_Lead__c = objLead.Id ;
    	list_leadTeam.add(objLeadTeam1) ;
    	insert list_leadTeam ;
    	//------------------Start Test-----------------------
    	system.test.startTest() ;
    	system.runAs(use3)
    	{
    		ApexPages.StandardController STController = new ApexPages.StandardController(objLeadTeam1);
			ApexPages.currentPage().getParameters().put('camid', objLead.Id);
			V2_DeleteLeadTeam_Con DelLeadTeam = new V2_DeleteLeadTeam_Con(STController);
			//DelLeadTeam.getListMonths() ;
			DelLeadTeam.objLeadTeam.V2_User__c = use1.Id ;
			
			DelLeadTeam.saveChange() ;
			DelLeadTeam.delRecord() ;
    	}
    	system.test.stopTest() ;
	}
	static testMethod void TestPage2() 
	{
		//----------------New UserRole ------------------
    	//List<UserRole> list_userRole = new List<UserRole >() ;
    	UserRole objUserRole = new UserRole() ;
    	objUserRole.Name = 'Renal-Rep-大上海-PD-Rep(陈喆令)' ;
    	insert objUserRole ;
    	UserRole objUserRole2 = new UserRole() ;
    	objUserRole2.Name = 'Renal-Rep-大上海-PB-Rep(陈喆令)' ;
    	objUserRole2.ParentRoleId = objUserRole.Id ;
    	insert objUserRole2 ;
		//----------------Create User-------------
    	List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.UserRoleId = objUserRole2.Id ;
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	//use2.UserRoleId = objUserRole2.Id ;
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.UserRoleId = objUserRole.Id ;
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
    	//-------------New Lead------------------
    	List<Lead> list_lead = new List<Lead>() ;
    	Lead objLead = new Lead() ;
    	objLead.LastName = 'testlead' ;
    	objLead.FirstName = 'T' ;
    	objLead.Company = 'testcom' ;
    	objLead.V2_Mid__c = 'poiuuiop' ;
    	objLead.V2_ApprovalStauts__c = '待审批' ;
    	list_lead.add(objLead) ;
    	insert list_lead ;
    	//-----------------New Lead Team-----------------------
    	List<V2_Lead_Team__c> list_leadTeam = new List<V2_Lead_Team__c>();
    	V2_Lead_Team__c objLeadTeam1 = new V2_Lead_Team__c() ;
    	objLeadTeam1.V2_User__c = use2.Id ;
    	objLeadTeam1.V2_Lead__c = objLead.Id ;
    	list_leadTeam.add(objLeadTeam1) ;
    	insert list_leadTeam ;
    	//------------------Start Test-----------------------
    	system.test.startTest() ;
    	system.runAs(use3)
    	{
    		ApexPages.StandardController STController = new ApexPages.StandardController(objLeadTeam1);
			ApexPages.currentPage().getParameters().put('camid', objLead.Id);
			V2_DeleteLeadTeam_Con DelLeadTeam = new V2_DeleteLeadTeam_Con(STController);
			//DelLeadTeam.getListMonths() ;
			DelLeadTeam.objLeadTeam.V2_User__c = use2.Id ;
			
			DelLeadTeam.saveChange() ;
			DelLeadTeam.delRecord() ;
    	}
    	system.test.stopTest() ;
	}
	
}