/**
 * Author : Bill
 * date : 2013-10-12
 * deacription :  PSR销售医院关系同步病人所有人
 **/
global class PM_PSRRelationSyncOwner  implements Database.Batchable<sObject>{
    //医院ID集合
    global Set<ID> set_Hospital;
    //新PSR负责人
    global ID NewPSR_Id;
    //经销商ID
    global ID NewAcc_Id;
    //是否医院经销商关系
    global boolean IsHosDealerSync;
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator([Select a.Id From Account a where Id IN : set_Hospital]);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
    	
    	//新病人
    	Set<ID> set_apply = new Set<ID>();
    	//病人
        List<PM_Patient__c> list_patient = new List<PM_Patient__c>();
        //医院与PSR关系
        List<PM_PSRRelation__c> list_psr = new List<PM_PSRRelation__c>();

        for(sObject sObj : scope)
        {
            Account acc = (Account)sObj;
            set_apply.Add(acc.Id);
        }
        list_patient = [Select Id, PM_Current_PSR__c, OwnerId From PM_Patient__c where PM_InHospital__c IN : set_apply];
        list_psr = [Select PM_Hospital__c, PM_PSR__c,PM_NewPSR__c From PM_PSRRelation__c where PM_IsPending__c = true and PM_Hospital__c IN : set_apply];
        if(IsHosDealerSync)
        {
	        /************************bill 2013-12-29 update*****************************/
	        //当调整病人的终端配送商,把终端配送商下病人的经销商修改为对应的关系的经销商
	        List<PM_Patient__c> list_Terminalpatient = [Select Id, PM_Current_PSR__c, OwnerId From PM_Patient__c where PM_Terminal__c IN : set_apply];
	        /************************bill 2013-12-29 update*****************************/
            for(PM_Patient__c pa : list_Terminalpatient)
            {
                //经销商
                pa.PM_Distributor__c = NewAcc_Id;
            }
            if(list_Terminalpatient != null && list_Terminalpatient.size()>0)
            {
                update list_Terminalpatient;
            }
        }
        else
        {
            if(list_patient != null && list_patient.size()>0)
            {
	            for(PM_Patient__c pa : list_patient)
	            {
	                //病人owner
	                pa.OwnerId = NewPSR_Id;
	                //病人当前负责PSR
	                pa.PM_Current_PSR__c = NewPSR_Id;
	            }
                update list_patient;
            }
            if(list_psr.size() > 0)
            {
                for(PM_PSRRelation__c p :list_psr)
                {
                    p.PM_IsPending__c = false;
                }
                update list_psr;
                if(list_psr[0].PM_PSR__c != list_psr[0].PM_NewPSR__c)
                {
	                list<AccountTeamMember> list_delTeam = [Select a.Id From AccountTeamMember a where 
	                                    a.UserId = : list_psr[0].PM_PSR__c and a.AccountId = : list_psr[0].PM_Hospital__c];
	                if(list_delTeam != null && list_delTeam.size()>0)
	                {
	                    delete list_delTeam;
	                }
                }
            }
            //List<PatientApply__c> list_patientApply = [Select p.PM_PSR__c, p.Id From PatientApply__c p where IntubationHospital__c IN : set_apply];
            //更新新病人提交记录的负责PSR时会触发很多验证，导致报错信息，先暂时注释，等业务部门确认处理方法再处理
            //if(list_patientApply != null && list_patientApply.size()>0)
            //{
	            //for(PatientApply__c apply : list_patientApply)
	            //{
	            	//负责PSR
	            	//apply.PM_PSR__c = NewPSR_Id;
	            //}
                //update list_patientApply;
            //}
        }
    }
    global void finish(Database.BatchableContext BC)
    {
        
    }
}