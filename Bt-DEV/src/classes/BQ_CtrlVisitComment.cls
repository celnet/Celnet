/**
 * 作者：Sunny
 * 功能：协防质量评分
 * 2013-12-12 Tobe : 事件字段清除，去掉有关点评相关的4、5的字段的引用
**/
public class BQ_CtrlVisitComment {
  	//事件ID
  	public ID eventId{get;set;}
  	public boolean isAdmin{get;set;}
  	//构造函数
    public BQ_CtrlVisitComment(Apexpages.Standardcontroller controller){
      eventId = controller.getId();
      Profile pro = [select Name From Profile Where Id =: UserInfo.getProfileId()];
      if(pro.Name.contains('Admin') || pro.Name.contains('系统管理员'))
      {
      	isAdmin = true;
      }
      else 
      {
      	isAdmin= false; 
      }
    } 
    static testMethod void myUnitTest() {
       //user role
        List<UserRole> listur= new List<UserRole>();
        UserRole ur0 = new UserRole();
        ur0.Name = 'Renal-Supervisor-华南-PD-Supervisor(U0)';
        listur.add(ur0);
        UserRole ur1 = new UserRole();
        ur1.Name = 'Renal-Supervisor-华南-PD-Supervisor(U1)';
        listur.add(ur1);
        UserRole ur2 = new UserRole();
        ur2.Name = 'Renal-Rep-华南-PD-Rep(U2)';
        listur.add(ur2);
        insert listur;
        ur2.ParentRoleId=ur1.Id;
        update ur2;
        ur1.ParentRoleId = ur0.id;
        update ur1;
        //user 
        list<User> user=[Select TimeZoneSidKey,ProfileId,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname, Alias From User Where IsActive = true limit 1];
        User user2=new User();
        user2.username='user2r2@223.com';
        user2.LastName='user2r2';
        user2.Email='user2r2@223.com';
        user2.Alias=user[0].Alias;
        user2.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user2.ProfileId=UserInfo.getProfileId();
        user2.LocaleSidKey=user[0].LocaleSidKey;
        user2.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user2.EmailEncodingKey=user[0].EmailEncodingKey;
        user2.CommunityNickname='chequ1';
        user2.MobilePhone='22345678922';
        user2.IsActive = true;
        user2.UserRoleId=ur2.Id ;
        insert user2;
        //event
      RecordType rt =[Select Id From RecordType Where DeveloperName = 'V2_Event' And SobjectType = 'Event'];
      Event objEvent = new Event();
      objEvent.OwnerId = user2.Id;
      objEvent.StartDateTime = datetime.now();
      objEvent.EndDateTime = datetime.now().addHours(1);
      objEvent.RecordTypeId = rt.Id;
      objEvent.Subject = '拜访';
      insert objEvent ;
      
      system.test.startTest();
      BQ_CtrlVisitComment vc = new BQ_CtrlVisitComment(new Apexpages.Standardcontroller(objEvent));
      system.test.stopTest();
    }
}