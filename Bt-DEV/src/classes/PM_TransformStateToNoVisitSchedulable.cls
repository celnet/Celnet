/*
* Tobe
* 2013.10.25
* 自动根据Active APD病人的拜访周期判断该病人的上次拜访状态是否置为“未拜访”
*/

public class PM_TransformStateToNoVisitSchedulable implements Schedulable
{
	public void execute(SchedulableContext sc) 
	{
		PM_TransformStateToNoVisitBatch transform = new PM_TransformStateToNoVisitBatch();
		database.executeBatch(transform,500);
	}
}