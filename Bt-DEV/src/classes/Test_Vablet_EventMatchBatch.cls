/*
 * Author: Steven Ke
 * Date: 2014-2-18
 * Description: 测试Vablet_EventMatchBatch
 */
@isTest
private class Test_Vablet_EventMatchBatch {
	static testmethod void testMultiEventMultiDocumentRetrivalBatch()
	{
		// 准备数据
		List<User> uList = [Select Id, Email From User Where IsActive = true And Profile.UserLicense.Name = 'Salesforce'];
		RecordType[] rt = [Select Id From RecordType Where Name = '拜访'];
		
		Event e1 = new Event();
		e1.StartDateTime = Datetime.now();
		e1.EndDateTime = Datetime.now().addHours(2);
		e1.OwnerId = uList[1].Id;
		e1.RecordTypeId = rt[0].Id;
		Event e2 = new Event();
		e2.StartDateTime = Datetime.now().addHours(-2);
		e2.EndDateTime = Datetime.now().addHours(1);
		e2.OwnerId = uList[1].Id;
		e2.RecordTypeId = rt[0].Id;
		Event e3 = new Event();
		e3.StartDateTime = Datetime.now().addHours(-4);
		e3.EndDateTime = Datetime.now();
		e3.OwnerId = uList[1].Id;
		e3.RecordTypeId = rt[0].Id;
		Event e4 = new Event();
		e4.StartDateTime = Datetime.now().addHours(-3);
		e4.EndDateTime = Datetime.now().addHours(2);
		e4.OwnerId = uList[1].Id;
		e4.RecordTypeId = rt[0].Id;
		Event e5 = new Event();
		e5.StartDateTime = Datetime.now().addHours(-1);
		e5.EndDateTime = Datetime.now().addHours(1);
		e5.OwnerId = uList[1].Id;
		e5.RecordTypeId = rt[0].Id;
		insert new List<Event>{e1,e2,e3,e4,e5};
		
		Vablet_DocumentRetrival__c vdr1 = new Vablet_DocumentRetrival__c();
		vdr1.Vablet_StartTime__c = Datetime.now().addHours(-7);
		vdr1.Vablet_Duration__c = 60;
		vdr1.OwnerId = uList[1].Id;
		Vablet_DocumentRetrival__c vdr2 = new Vablet_DocumentRetrival__c();
		vdr2.Vablet_StartTime__c = Datetime.now().addHours(2);
		vdr2.Vablet_Duration__c = 40;
		vdr2.OwnerId = uList[1].Id;
		Vablet_DocumentRetrival__c vdr3 = new Vablet_DocumentRetrival__c();
		vdr3.Vablet_StartTime__c = Datetime.now();
		vdr3.Vablet_Duration__c = 20;
		vdr3.OwnerId = uList[1].Id;
		Vablet_DocumentRetrival__c vdr4 = new Vablet_DocumentRetrival__c();
		vdr4.Vablet_StartTime__c = Datetime.now();
		vdr4.Vablet_Duration__c = 20;
		vdr4.OwnerId = uList[1].Id;
		Vablet_DocumentRetrival__c vdr5 = new Vablet_DocumentRetrival__c();
		vdr5.Vablet_StartTime__c = Datetime.now();
		vdr5.Vablet_Duration__c = 20;
		vdr5.OwnerId = uList[1].Id;
		insert new List<Vablet_DocumentRetrival__c>{vdr1,vdr2,vdr3,vdr4,vdr5};
		
		// 执行Batch
		System.Test.startTest();
		Vablet_EventMatchBatch vemb = new Vablet_EventMatchBatch();
		Database.executeBatch(vemb);
		System.Test.stopTest();
		
		// 验证结果
		List<Event> eList = [Select Vablet_IsVabletVisit__c, Id From Event Where RecordType.Name = '拜访' And OwnerId =: uList[1].Id];
		System.assertEquals(eList[4].Vablet_IsVabletVisit__c, false);
		System.assertEquals(eList.size(), 5);
		List<Vablet_VisitDate__c> vvdList = [Select Vablet_EventId__c, Vablet_DocumentRetrival__c From Vablet_VisitDate__c];
		System.assertEquals(0, vvdList.size());
	}
}