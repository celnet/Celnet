/* 
 * Author: Sunny
 * Created on: 2011-12-26
 * Description: 当潜在客户审批通过之后，根据该潜在客户创建一个新的客户
 * 1.根据MID查找Account，若不存在则创建，并且同步客户小组
 * 2.若MID值为空，根据Lead ID查找Account，若不存在则创建，并且同步客户小组
 */
@isTest
private class V2_Lead_AU_CreateAccount_Test {

    static testMethod void TestCreateAccount() {
    	//----------------Create User-------------
    	List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
    	//--------------Create Lead---------------
    	List<Lead> list_Leads = new List<Lead>() ;
        Lead objLead1 = new Lead() ;
        objLead1.LastName = '测试医院客户' ;
        objLead1.Company = '测试医院客户' ;
        objLead1.V2_Mid__c = '123321123' ;
        list_Leads.add(objLead1) ;
        Lead objLead2 = new Lead() ;
        objLead2.LastName = '测试医院客户' ;
        objLead2.Company = '测试医院客户' ;
        objLead2.V2_Mid__c = '223322223' ;
        list_Leads.add(objLead2) ;
        Lead objLead3 = new Lead() ;
        objLead3.LastName = '测试医院客户' ;
        objLead3.Company = '测试医院客户' ;
        objLead3.V2_Mid__c = '333333333' ;
        list_Leads.add(objLead3) ;
        insert list_Leads ;
        //------------Create Lead Team-----------------
        List<V2_Lead_Team__c> list_V2LeadTeam = new List<V2_Lead_Team__c>() ;
        V2_Lead_Team__c objLeadTeam1 = new V2_Lead_Team__c() ;
        objLeadTeam1.V2_User__c = use1.Id;
        objLeadTeam1.V2_Lead__c =objLead1.Id;
        objLeadTeam1.V2_Effective_Year__c = '2012';
        objLeadTeam1.V2_Effective_Month__c = '1';
        list_V2LeadTeam.add(objLeadTeam1) ;
        V2_Lead_Team__c objLeadTeam2 = new V2_Lead_Team__c() ;
        objLeadTeam2.V2_User__c = use2.Id;
        objLeadTeam2.V2_Lead__c =objLead2.Id;
        objLeadTeam2.V2_Effective_Year__c = '2012';
        objLeadTeam2.V2_Effective_Month__c = '2';
        list_V2LeadTeam.add(objLeadTeam2) ;
        V2_Lead_Team__c objLeadTeam3 = new V2_Lead_Team__c() ;
        objLeadTeam3.V2_User__c = use3.Id;
        objLeadTeam3.V2_Lead__c =objLead3.Id;
        objLeadTeam3.V2_Effective_Year__c = '2012';
        objLeadTeam3.V2_Effective_Month__c = '3';
        list_V2LeadTeam.add(objLeadTeam3) ;
        V2_Lead_Team__c objLeadTeam4 = new V2_Lead_Team__c() ;
        objLeadTeam4.V2_User__c = use1.Id;
        objLeadTeam4.V2_Lead__c =objLead3.Id;
        objLeadTeam4.V2_Effective_Year__c = '2012';
        objLeadTeam4.V2_Effective_Month__c = '4';
        list_V2LeadTeam.add(objLeadTeam4) ;
        insert list_V2LeadTeam ;
        //--------------start test---------------
        system.test.startTest() ;
        objLead1.V2_ApprovalStauts__c = '审批通过' ;
        update objLead1 ;
        List<Account> list_Acc1 = [Select a.Mid__c,(Select V2_User__c From V2_Account__r), (Select UserId From AccountTeamMembers) From Account a Where Mid__c =: objLead1.V2_Mid__c] ;
        system.assertEquals(1, list_Acc1.size()) ;
        system.assertEquals(1, list_Acc1[0].V2_Account__r.size()) ;
        system.assertEquals(1, list_Acc1[0].AccountTeamMembers.size()) ;
        system.assertEquals(use1.Id, list_Acc1[0].V2_Account__r[0].V2_User__c) ;
        system.assertEquals(use1.Id, list_Acc1[0].AccountTeamMembers[0].UserId) ;
        
        List<Lead> list_leadUp = new List<Lead>() ;
        objLead2.V2_ApprovalStauts__c = '审批通过' ;
        list_leadUp.add(objLead2) ;
        objLead3.V2_ApprovalStauts__c = '审批通过' ;
        list_leadUp.add(objLead3) ;
        update list_leadUp ;
        Map<string,Account> map_midAcc = new Map<string,Account>() ;
        for(Account objAcc : [Select a.Mid__c,(Select V2_User__c From V2_Account__r), (Select UserId From AccountTeamMembers) From Account a Where Mid__c =: objLead2.V2_Mid__c Or Mid__c =: objLead3.V2_Mid__c])
        {
        	map_midAcc.put(objAcc.Mid__c, objAcc) ;
        }
        Account objAcc2 = map_midAcc.get(objLead2.V2_Mid__c) ;
        system.assertEquals(1, objAcc2.V2_Account__r.size()) ;
        system.assertEquals(1, objAcc2.AccountTeamMembers.size()) ;
        system.assertEquals(use2.Id, objAcc2.V2_Account__r[0].V2_User__c) ;
        system.assertEquals(use2.Id, objAcc2.AccountTeamMembers[0].UserId) ;
        
        Account objAcc3 = map_midAcc.get(objLead3.V2_Mid__c) ;
        system.assertEquals(2, objAcc3.V2_Account__r.size()) ;
        system.assertEquals(2, objAcc3.AccountTeamMembers.size()) ;
        //system.assertEquals(use3.Id, objAcc2.V2_Account__r[0].V2_User__c) ;
        //system.assertEquals(use3.Id, objAcc2.AccountTeamMembers[0].UserId) ;
        
        system.test.stopTest() ;
        
    }
    
    static testMethod void TestUpAccountTeam() {
    	//----------------Create User-------------
    	List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
    	
    	//--------------Create Lead---------------
    	List<Lead> list_Leads = new List<Lead>() ;
        Lead objLead1 = new Lead() ;
        objLead1.LastName = '测试医院客户' ;
        objLead1.Company = '测试医院客户' ;
        objLead1.V2_Mid__c = '123321123' ;
        list_Leads.add(objLead1) ;
        Lead objLead2 = new Lead() ;
        objLead2.LastName = '测试医院客户' ;
        objLead2.Company = '测试医院客户' ;
        objLead2.V2_Mid__c = '223322223' ;
        list_Leads.add(objLead2) ;
        Lead objLead3 = new Lead() ;
        objLead3.LastName = '测试医院客户' ;
        objLead3.Company = '测试医院客户' ;
        objLead3.V2_Mid__c = '333333333' ;
        list_Leads.add(objLead3) ;
        insert list_Leads ;
        
        //------------Create Lead Team-----------------
        List<V2_Lead_Team__c> list_V2LeadTeam = new List<V2_Lead_Team__c>() ;
        V2_Lead_Team__c objLeadTeam1 = new V2_Lead_Team__c() ;
        objLeadTeam1.V2_User__c = use1.Id;
        objLeadTeam1.V2_Lead__c =objLead1.Id;
        objLeadTeam1.V2_Effective_Year__c = '2012';
        objLeadTeam1.V2_Effective_Month__c = '1';
        list_V2LeadTeam.add(objLeadTeam1) ;
        V2_Lead_Team__c objLeadTeam2 = new V2_Lead_Team__c() ;
        objLeadTeam2.V2_User__c = use2.Id;
        objLeadTeam2.V2_Lead__c =objLead2.Id;
        objLeadTeam2.V2_Effective_Year__c = '2012';
        objLeadTeam2.V2_Effective_Month__c = '2';
        list_V2LeadTeam.add(objLeadTeam2) ;
        V2_Lead_Team__c objLeadTeam3 = new V2_Lead_Team__c() ;
        objLeadTeam3.V2_User__c = use3.Id;
        objLeadTeam3.V2_Lead__c =objLead3.Id;
        objLeadTeam3.V2_Effective_Year__c = '2012';
        objLeadTeam3.V2_Effective_Month__c = '3';
        list_V2LeadTeam.add(objLeadTeam3) ;
        V2_Lead_Team__c objLeadTeam4 = new V2_Lead_Team__c() ;
        objLeadTeam4.V2_User__c = use1.Id;
        objLeadTeam4.V2_Lead__c =objLead3.Id;
        objLeadTeam4.V2_Effective_Year__c = '2012';
        objLeadTeam4.V2_Effective_Month__c = '4';
        list_V2LeadTeam.add(objLeadTeam4) ;
        insert list_V2LeadTeam ;
        
        //----------Create Account-------------
        List<Account> list_Account = new List<Account>() ;
        Account objAccount1 = new Account() ;
        objAccount1.Name = '测试医院客户' ;
        objAccount1.Mid__c = '123321123' ;
        list_Account.add(objAccount1) ;
        Account objAccount2 = new Account() ;
        objAccount2.Name = '测试医院客户' ;
        objAccount2.Mid__c = '223322223' ;
        list_Account.add(objAccount2) ;
        Account objAccount3 = new Account() ;
        objAccount3.Name = '测试医院客户' ;
        objAccount2.Mid__c = '333333333' ;
        list_Account.add(objAccount3) ;
        insert list_Account ;
        
        system.test.startTest() ;
        objLead1.V2_ApprovalStauts__c = '审批通过' ;
        update objLead1 ;
        List<Account> list_Acc1 = [Select a.Mid__c,(Select V2_User__c From V2_Account__r), (Select UserId From AccountTeamMembers) From Account a Where Mid__c =: objLead1.V2_Mid__c] ;
        system.assertEquals(1, list_Acc1.size()) ;
        system.assertEquals(1, list_Acc1[0].V2_Account__r.size()) ;
        system.assertEquals(1, list_Acc1[0].AccountTeamMembers.size()) ;
        system.assertEquals(use1.Id, list_Acc1[0].V2_Account__r[0].V2_User__c) ;
        system.assertEquals(use1.Id, list_Acc1[0].AccountTeamMembers[0].UserId) ;
        
        List<Lead> list_LeadUp = new List<Lead>() ;
        objLead2.V2_ApprovalStauts__c = '审批通过' ;
        list_LeadUp.add(objLead2) ;
        objLead3.V2_ApprovalStauts__c = '审批通过' ;
        list_LeadUp.add(objLead3) ;
        update list_LeadUp ;
        
        Map<string,Account> map_midAcc = new Map<string,Account>() ;
        for(Account objAcc : [Select a.Mid__c,(Select V2_User__c From V2_Account__r), (Select UserId From AccountTeamMembers) From Account a Where Mid__c =: objLead2.V2_Mid__c Or Mid__c =: objLead3.V2_Mid__c])
        {
        	map_midAcc.put(objAcc.Mid__c, objAcc) ;
        }
        Account objAcc2 = map_midAcc.get(objLead2.V2_Mid__c) ;
        system.assertEquals(1, objAcc2.V2_Account__r.size()) ;
        system.assertEquals(1, objAcc2.AccountTeamMembers.size()) ;
        system.assertEquals(use2.Id, objAcc2.V2_Account__r[0].V2_User__c) ;
        system.assertEquals(use2.Id, objAcc2.AccountTeamMembers[0].UserId) ;
        
        Account objAcc3 = map_midAcc.get(objLead3.V2_Mid__c) ;
        system.assertEquals(2, objAcc3.V2_Account__r.size()) ;
        system.assertEquals(2, objAcc3.AccountTeamMembers.size()) ;
        system.test.stopTest() ;
        
    }
}