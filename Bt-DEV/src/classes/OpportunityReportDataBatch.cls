/**
 * Author : Bill
 * date : 2013-8-1
 * deacription : 当销售数据发生变化时同时更新对应的业务机会的报表所需数据
 **/
global with sharing class OpportunityReportDataBatch implements Database.Batchable<sObject>{   
	global integer curMonth;
	private String pointEmail = UserInfo.getUserEmail();
	global Database.QueryLocator start(Database.BatchableContext BC){
		//获取所有业务机会
    	return Database.getQueryLocator([Select o.AccountId, o.Id, o.OwnerId From Opportunity o Where o.RecordType.Name IN ('IVT','IVT Approval')  AND CloseDate = THIS_YEAR  ORDER BY AccountId asc]);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope){
    	//查看业务机会映射表是否已经存在
    	//基本表Id集合
    	Set<ID> set_base = new Set<ID>();
    	//客户Id集合
    	Set<ID> set_acc =new Set<ID>();
    	
    	/****************bill 修改 2013-8-26 start********************/
    	//业务机会基本信息的公式客户ID的集合
    	Set<ID> set_AccIds =new Set<ID>();
    	//业务机会list  之前的代码存在风险，需要重新修改下  逻辑不修改
    	List<Opportunity> list_opps = new List<Opportunity>();
		for(sObject sObj : scope)
		{
			Opportunity opp = (Opportunity)sObj;
			if(opp.AccountId != null)
			{
				string accId = opp.AccountId;
				accId = accId.substring(0,accId.length()-3);
				set_AccIds.add(accId);
				list_opps.add(opp);
			}
		}
		/****************bill 修改 2013-8-26 end********************/
    	
    	Map<ID,OpportunityBaseData__c> map_oppBase = new Map<ID,OpportunityBaseData__c>();
    	List<OpportunityBaseData__c> list_oppBase = [Select o.Opportunity__c, o.Id, o.OwnerId From OpportunityBaseData__c o WHERE o.DistinctOpp__c = false and O.CloseDate__c = THIS_YEAR and o.AccountId__c in : set_AccIds];
    	for(OpportunityBaseData__c oppBase : list_oppBase)
    	{ 
    		map_oppBase.put(oppBase.Opportunity__c, oppBase);
    	}
    	
    	List<OpportunityBaseData__c> list_newOppBase = new List<OpportunityBaseData__c>();
		for(Opportunity opp : list_opps)
		{
    		set_base.add(opp.Id);
    		if(!map_oppBase.containsKey(opp.Id))
    		{
    			OpportunityBaseData__c newOppBase = new OpportunityBaseData__c();
    			newOppBase.Opportunity__c = opp.Id;
    			newOppBase.OwnerId = opp.OwnerId;
    			list_newOppBase.add(newOppBase);
    		}
    		else if(map_oppBase.containsKey(opp.Id) && !map_oppBase.isEmpty())
    		{
    			map_oppBase.get(opp.Id).OwnerId = opp.OwnerId;
    		}
		}
		if(list_oppBase != null && list_oppBase.size()>0)
		{
			update list_oppBase;
		}
		if(list_newOppBase != null && list_newOppBase.size()>0)
		{
			insert list_newOppBase;
		}
		
		List<OpportunityBaseData__c> list_aLLOppBase = [Select o.AccountId__c, o.SuccessGap__c, o.Possible__c, o.PossibleGap__c, 
														o.Opportunity__c, o.OppSuccessSalesEstimate__c, o.OppPossibleSalesEstimate__c, o.NowBaxterDosage__c, o.Id, 
														o.CloseDate__c, o.AnnualIndex__c, o.AfterBaxterDosage__c 
														From OpportunityBaseData__c o WHERE o.DistinctOpp__c = false and O.CloseDate__c = THIS_YEAR and o.Opportunity__c in : set_base];
		
		//业务机会销量数据子表数据
		Map<string,OppSalesChildData__c> map_childData = new Map<string,OppSalesChildData__c>();
		List<OppSalesChildData__c> list_oppChild = new List<OppSalesChildData__c>();
		for(OpportunityBaseData__c aLLOppBase : list_aLLOppBase)
		{
			//创建12月销售数据
			for(integer i = 1; i <= 12; i++)
			{
				integer year = aLLOppBase.CloseDate__c.year();
				integer month = aLLOppBase.CloseDate__c.month();
				if(aLLOppBase.CloseDate__c.day()>=15)
				{
					month = month+1;
				}
				OppSalesChildData__c childData = new OppSalesChildData__c();
				if( month > i)
				{
					childData.OpportunityBaseData__c = aLLOppBase.Id;
					childData.ImportDate__c = date.valueOf(string.valueOf(year)+'-'+ string.valueOf(i)+'-'+'15');
				}else{
					childData.OpportunityBaseData__c = aLLOppBase.Id;
					childData.ImportDate__c = date.valueOf(string.valueOf(year)+'-'+ string.valueOf(i)+'-'+'15');
					childData.SaleQuantity__c = aLLOppBase.AfterBaxterDosage__c;
				}
				string strdata = string.valueOf(aLLOppBase.Id)+string.valueOf(i);
				map_childData.put(strdata, childData);
				list_oppChild.add(childData);
			}
			//基本信息对象的客户Id
			set_acc.add(aLLOppBase.AccountId__c);
		}
		//insert list_oppChild;
		
		Map<string,SalesTotalData> map_sale = new Map<string,SalesTotalData>();
		//取出本年度导入的所有销售数据
		for(SalesReport__c sale : [Select s.Time__c, s.ActualQty__c, s.TargetQty__c, s.Account__c From SalesReport__c s where s.SBU__c = 'IVT' AND s.Time__c = THIS_YEAR AND s.Account__c IN : set_acc])
		{
			string strSale = string.valueOf(sale.Account__c);
			strSale = strSale.substring(0,strSale.length()-3) + string.valueOf(sale.Time__c.month());
			SalesTotalData saledata = new SalesTotalData();
			saledata.ActualQty = sale.ActualQty__c;
			saledata.TargetQty = sale.TargetQty__c;
			if(!map_sale.containsKey(strSale))
			{
				map_sale.put(strSale, saledata);
			}else{
				map_sale.get(strSale).ActualQty += sale.ActualQty__c;
				map_sale.get(strSale).TargetQty += sale.TargetQty__c;
			}
		}
		system.debug(map_sale + '1234');
		//第一月没有销售数据，从第二月开始有
		for(OpportunityBaseData__c aLLOppBase : list_aLLOppBase)
		{
			//预估值 当前选定月前三月之和
			double estimate =0;
			//结束日期和中间15比较
			integer midmonth = aLLOppBase.CloseDate__c.month();
			if(aLLOppBase.CloseDate__c.day()>=15)
			{
				midmonth = midmonth+1;
			}
			
			//运行月的销量
			double curSale = 0;
			SalesTotalData curSaleTotal = map_sale.get(aLLOppBase.AccountId__c + string.valueOf(curMonth));
			if(curSaleTotal!=null)
			{
				curSale = curSaleTotal.ActualQty!=null?curSaleTotal.ActualQty:0;
			}
			
			//销售数据同步业务机会销售数据
			for(integer i=1; i <= 12; i++)
			{
				OppSalesChildData__c oppC = map_childData.get(aLLOppBase.Id + string.valueOf(i));
				SalesTotalData sr = map_sale.get(aLLOppBase.AccountId__c + string.valueOf(i));
				if(sr != null && oppC != null)
				{
					oppC.TargetQty__c = sr.TargetQty; 
					if(i <= curMonth)
					{
						oppC.SaleQuantity__c = (sr.ActualQty == null)?0:sr.ActualQty;
					}
					else if(curMonth > (midmonth-1))
					{
						oppC.SaleQuantity__c = curSale;
					}
				}
			}
			
			//1月份数据
			SalesTotalData sp1 = map_sale.get(aLLOppBase.AccountId__c + '1');
			if(sp1 != null)estimate = (sp1.ActualQty == null)?0:sp1.ActualQty;
			//2月份数据
			SalesTotalData sp2 = map_sale.get(aLLOppBase.AccountId__c + '2');
			//3月份数据
			SalesTotalData sp3 = map_sale.get(aLLOppBase.AccountId__c + '3');
			
			OppSalesChildData__c oppBaseChild2 = map_childData.get(aLLOppBase.Id + '2');
			OppSalesChildData__c oppBaseChild3 = map_childData.get(aLLOppBase.Id + '3');
			if((curMonth == 2 || system.Test.isRunningTest()) && sp1 != null && oppBaseChild2 != null)
			{
				if(oppBaseChild2.SaleQuantity__c == null)
				{
					oppBaseChild2.SaleQuantity__c = (sp1.ActualQty == null)?0:sp1.ActualQty;
					estimate = (sp1.ActualQty == null)?0:sp1.ActualQty;
				}else{
					estimate = ((sp1.ActualQty == null)?0:sp1.ActualQty)/2 + ((sp2.ActualQty == null)?0:sp2.ActualQty)/2;
				}
			}
			else if(curMonth >= 3)
			{
				//获取当前月份的前三月份销售数据更新到业务机会销售数据中去
				OppSalesChildData__c child = map_childData.get(aLLOppBase.Id + string.valueOf(curMonth));
				
				SalesTotalData spbefore1 = map_sale.get(aLLOppBase.AccountId__c + string.valueOf(curMonth));
				SalesTotalData spbefore2 = map_sale.get(aLLOppBase.AccountId__c + string.valueOf(curMonth-1));
				SalesTotalData spbefore3 = map_sale.get(aLLOppBase.AccountId__c + string.valueOf(curMonth-2));
				
				//当前月取前三月的销量的平均值
				if(spbefore1 != null && spbefore2 != null && spbefore3 != null)
				{
					estimate = (((spbefore1.ActualQty == null)?0:spbefore1.ActualQty) + ((spbefore2.ActualQty == null)?0:spbefore2.ActualQty) + ((spbefore3.ActualQty == null)?0:spbefore3.ActualQty))/3;
				}	
			}
			for(integer i= curMonth+1; i < midmonth; i++)
			{
				OppSalesChildData__c oppC = map_childData.get(aLLOppBase.Id + string.valueOf(i));
				if(oppC !=null)
				{
					oppC.SaleQuantity__c = estimate;
				}
			}
			//业务机会成功后的年度销量预估
			double Quantity =0;
			//年度目标
			double Target =0;
			//结束日期前的实际销量之和
			double ActQuantity = 0;
			//当前月份前的实际销量之和
			double curActQty = 0;
			for(integer i=1; i <= 12; i++)
			{
				OppSalesChildData__c oppC = map_childData.get(aLLOppBase.Id + string.valueOf(i));
				if(oppC.SaleQuantity__c != null)
				{
					Quantity += oppC.SaleQuantity__c;
					if(i==1)aLLOppBase.January__c = oppC.SaleQuantity__c;
					if(i==2)aLLOppBase.February__c = oppC.SaleQuantity__c;
					if(i==3)aLLOppBase.March__c = oppC.SaleQuantity__c;
					if(i==4)aLLOppBase.April__c = oppC.SaleQuantity__c; 
					if(i==5)aLLOppBase.May__c = oppC.SaleQuantity__c;
					if(i==6)aLLOppBase.June__c = oppC.SaleQuantity__c;
					if(i==7)aLLOppBase.July__c = oppC.SaleQuantity__c;
					if(i==8)aLLOppBase.August__c = oppC.SaleQuantity__c;
					if(i==9)aLLOppBase.September__c = oppC.SaleQuantity__c;
					if(i==10)aLLOppBase.October__c = oppC.SaleQuantity__c;
					if(i==11)aLLOppBase.November__c = oppC.SaleQuantity__c;
					if(i==12)aLLOppBase.December__c = oppC.SaleQuantity__c;
				}
				if(oppC.TargetQty__c != null)
				{
					Target += oppC.TargetQty__c;
				}
				if(i <= curMonth && oppC.SaleQuantity__c != null)
				{
					curActQty += oppC.SaleQuantity__c;
				}
			}
			aLLOppBase.OppSuccessSalesEstimate__c = Quantity;
			aLLOppBase.SuccessGap__c = Target-Quantity;
			aLLOppBase.AnnualIndex__c = Target;

			if(curMonth>(midmonth-1)) 
			{
				aLLOppBase.OppPossibleSalesEstimate__c = curSale*(12-curMonth) + curActQty;
			}
			else if(curMonth==(midmonth-1))
			{
				aLLOppBase.OppPossibleSalesEstimate__c = ((aLLOppBase.AfterBaxterDosage__c - estimate)*(aLLOppBase.Possible__c/100) + estimate)*(12-curMonth) + curActQty;
			}
			else if(curMonth<(midmonth-1))
			{
				aLLOppBase.OppPossibleSalesEstimate__c = ((aLLOppBase.AfterBaxterDosage__c - estimate)*(aLLOppBase.Possible__c/100) + estimate)*(13-midmonth)+ curActQty + estimate*(midmonth-1-curMonth);
			}
			aLLOppBase.PossibleGap__c = Target - aLLOppBase.OppPossibleSalesEstimate__c;
		}
		//update list_oppChild;
		update list_aLLOppBase;
    }
    
    global void finish(Database.BatchableContext BC){
	 	string baseUrl = string.valueOf(System.URL.getSalesforceBaseUrl());
	 	String reportUrl= baseUrl.substring(baseUrl.indexOf('=')+1,baseUrl.length()-1);
	 	if(!system.Test.isRunningTest())
	 	{
	 		List<Report> report = [Select r.Name, r.Id, r.DeveloperName From Report r where DeveloperName = 'Report0802'];
	 		if(report != null && report.size()>0)
	 		{
				reportUrl = reportUrl + '/' + report[0].Id;
	 		}
	 	}
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		String repBody = '';
		repBody += '您好: <br><br>';     
   		repBody += curMonth + '月份销售报表预测运行完毕，请查看<br>';
        repBody += '报表查看链接：<a href="' + reportUrl + '">' + reportUrl + '</a><br><br>';
        repBody += '本邮件由Baxter Salesforce.com CRM系统产生，请勿回复。<br>'; 
        repBody += '如有任何疑问或者要求，请联系系统管理人员。<br>';
        system.debug(pointEmail+'zgxm');
        String[] repAddress = new String[]{pointEmail};
        mail.setToAddresses(repAddress);
        mail.setHtmlBody(repBody);
        mail.setSubject('销售报表预测运行完毕');
        mail.setSaveAsActivity(false);//存为活动
        mail.setSenderDisplayName('Salesforce');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    private class SalesTotalData
    {
    	public double ActualQty{get;set;}
        public double TargetQty{get;set;}
    }
}