/* Tobe
 * 2013-11-9
 * PM_QueryUtil 测试类
 */
@isTest
private class PM_Test_QueryUtil
{
	 static testMethod void myUnitTest() 
    {
    	PM_QueryUtil util = new PM_QueryUtil();
    	string field = 'PM_PatientNo__c,Name,PM_Address__c,PM_Province__c,PM_City__c,PM_InHospital__c,PM_InHospital__r.PM_Province__c,PM_Current_Saler__c,PM_Current_PSR__c,PM_Distributor__c,PM_Status__c ';
        map<string,string> map_Condition = new map<string,string>();
        map_Condition.put('PM_PatientNo__c','1');
       	map_Condition.put('TeDistributorsProvince','1');
		map_Condition.put('TeDistributorsCity','1');
		map_Condition.put('TeDistributorsCounty','1');
		map_Condition.put('PM_Terminal__c','1');
		map_Condition.put('PM_InHospital__c','1');
		map_Condition.put('PM_Treat_Hospital__c','1');
		map_Condition.put('PM_Treat_Doctor__c','1');
		map_Condition.put('PM_Treat_Nurse__c','1');
		map_Condition.put('PM_Treat_Saler__c','1');
		map_Condition.put('PM_WaConsumption__c','1');
		map_Condition.put('PM_FromVendor__c','1');
		map_Condition.put('PM_TrInformation__c','1');
		map_Condition.put('PM_HDAndPD__c','1');
		map_Condition.put('PM_Pdtreatment__c','1');
    	map_Condition.put('PM_Heatingmode__c','1');
		map_Condition.put('PM_ATUnderstand__c','1');
		map_Condition.put('PM_ReToBaite__c','1');
		map_Condition.put('PM_ToDrFofive__c','1');
		map_Condition.put('PM_AdTomovement__c','1');
		map_Condition.put('PM_HiHobbies__c','1');
		map_Condition.put('PM_FaPeace__c','1');
		map_Condition.put('PM_BeWorking__c','1');
		map_Condition.put('PM_BeStudent__c','1');
		map_Condition.put('PM_HiSostatus__c','1');
		map_Condition.put('PM_HIInformation__c','1');
		map_Condition.put('PM_HighImpac__c','1');
		map_Condition.put('PM_Optimistic__c','1');
    	map_Condition.put('PM_Talkative__c','1');
		map_Condition.put('PM_TuHeIntention__c','1');
		map_Condition.put('PM_CeDisease__c','1');
		map_Condition.put('PM_TuToCoPro__c','1');
		map_Condition.put('PM_DrAcProblems__c','1');
		map_Condition.put('PM_HighRisk__c','1');
		map_Condition.put('PM_HRInformation__c','1');
		map_Condition.put('PM_EcDifficulties__c','1');
    	map_Condition.put('PM_WhTransplants__c','1');
		map_Condition.put('PM_LoGifts__c','1');
		map_Condition.put('PM_AcHoVisits__c','1');
		map_Condition.put('PM_WhTocall__c','1');
		map_Condition.put('PM_WhTosend__c','1');
		map_Condition.put('PM_IsFree__c','1');
		map_Condition.put('PM_IsAcceptCall__c','1');
		map_Condition.put('PM_Received_Diary__c','1');
		map_Condition.put('PM_ReCarepackages__c','1');
		map_Condition.put('PM_IsJoined__c','1');
		map_Condition.put('PM_IsInstruction__c','1');
		map_Condition.put('PM_Activities_Satisfactied__c','1');
		map_Condition.put('PM_Is_Personal_Information__c','1');
		map_Condition.put('PM_OHCEducation__c','1');
		map_Condition.put('PM_PeReInformation__c','1');
		map_Condition.put('PM_CaInformation__c','1');
		map_Condition.put('PM_Delivery__c','1');
    	map_Condition.put('PM_ReCommunication__c','1');
		map_Condition.put('PM_Protopathy__c','1');
    	map_Condition.put('PM_LiParalysis__c','1');
		map_Condition.put('PM_SeCreatinine__c','1');
		map_Condition.put('PM_Hemoglobin__c','1');
		map_Condition.put('PM_Albumin__c','1');
		map_Condition.put('PM_ViImpairment__c','1');
		map_Condition.put('PM_PeEqtest__c','1');
		map_Condition.put('PM_ClInformation__c','1');
		map_Condition.put('PM_DiBlpressure__c','1');
		map_Condition.put('PM_SyBlpressure__c','1');
		map_Condition.put('PM_UrNitrogen__c','1');
		map_Condition.put('PM_DaUroutput__c','1');
		map_Condition.put('PM_InType__c','1');
		map_Condition.put('PM_InDoctor__c','1');
		map_Condition.put('PM_InNurser__c','1');
		map_Condition.put('PM_Current_PSR__c','1');
		map_Condition.put('PM_InInformation__c','1');
		map_Condition.put('PM_InUser__c','1');
		map_Condition.put('InHospital','1');
    	map_Condition.put('County','1');
    	map_Condition.put('City','1');
    	map_Condition.put('Province','1');
    	map_Condition.put('BigArea','1');
		map_Condition.put('PM_BVTime1__c' ,'1');
	     map_Condition.put('PM_BVTime3__c' ,'1');
	     map_Condition.put('PM_BVTime4__c' ,'1');
	     map_Condition.put('PM_BVTime5__c' ,'1');
	     map_Condition.put('PM_BVTime6__c' ,'1');
		 map_Condition.put('PM_HomeTel__c' ,'1');
	     map_Condition.put('PM_PmPhone__c' ,'1');
	     map_Condition.put('PM_PmTel__c' ,'1');
	     map_Condition.put('PM_FamilyPhone__c' ,'1');
	     map_Condition.put('PM_FamilyTel__c' ,'1');
	     map_Condition.put('PM_OtherTel2__c' ,'1');
		map_Condition.put('PM_PhoneState1__c' ,'1');
        map_Condition.put('PM_PhoneState3__c' ,'1');
        map_Condition.put('PM_PhoneState4__c' ,'1');
        map_Condition.put('PM_PhoneState5__c' ,'1');
        map_Condition.put('PM_PhoneState6__c' ,'1');
		map_Condition.put('PM_Internet__c' ,'1');
        map_Condition.put('PM_Disappearanc__c' ,'1');
		map_Condition.put('PM_Visit_Interval__c' ,'1');
	    map_Condition.put('PM_ZipCode__c' ,'1');
		map_Condition.put('PM_LastVisitStatus__c' ,'1');
		map_Condition.put('PM_FAIncome__c' ,'1');
		map_Condition.put('PM_Profession__c' ,'1');
		map_Condition.put('PM_PatientDegree__c' ,'1');
		map_Condition.put('PM_Profession__c' ,'1');
	    map_Condition.put('PM_Email__c' ,'1');
		map_Condition.put('PM_PatientNo__c','1');
		map_Condition.put('Name','1');
		map_Condition.put('PatientCounty','1');
    	map_Condition.put('PatientCity','1');
    	map_Condition.put('PatientProvince','1');
    	map_Condition.put('PatientStatus','1');
    	map_Condition.put('RawStatus','1');
    	map_Condition.put('PM_Sex__c','1');
    	map_Condition.put('PM_IdNumber__c','1');
    	map_Condition.put('MobilePhone','1');
    	map_Condition.put('PM_Current_Saler__c','1');
    	map_Condition.put('PM_VisitFailCount__c','1');
    	map_Condition.put('PM_Address__c','1');
    	map_Condition.put('PM_VisitState__c','1');
		map_Condition.put('PM_Dealers__c','1');
		map_Condition.put('PM_DealerRemark__c','1');
		map_Condition.put('TeDistributors','1');
		map_Condition.put('PM_DropOff_One_Reason__c','1');
		map_Condition.put('PM_DropOff_Two_Reason__c','1');
		map_Condition.put('PM_TransformBrand__c','1');
		map_Condition.put('PM_Payment__c','1');
		map_Condition.put('PM_IsStarPatient__c','1');
		map_Condition.put('PM_UsCoGoSameTime__c','1');
		map_Condition.put('PM_CompetingReason__c','1');
		map_Condition.put('CreatedDate_Start','2013-1-1');
    	map_Condition.put('CreatedDate_End','2013-1-1');
    	map_Condition.put('InDate_Start','2013-1-1');
    	map_Condition.put('InDate_End','2013-1-1');
    	map_Condition.put('DropOutCreatedDate_Start','2013-1-1');
    	map_Condition.put('DropOutCreatedDate_End','2013-1-1');
    	map_Condition.put('DropActDate_Start','2013-1-1');
    	map_Condition.put('DropActDate_End','2013-1-1');
		map_Condition.put('LastVisitDate_Start','2013-1-1');
    	map_Condition.put('LastVisitDate_End','2013-1-1');
		map_Condition.put('LastFailDate_Start','2013-1-1');
    	map_Condition.put('LastFailDate_End','2013-1-1');
		map_Condition.put('DropoutActualDate_Start','2013-1-1');
    	map_Condition.put('DropoutActualDate_End','2013-1-1');
		map_Condition.put('TransfornCreatedDate_Start','2013-1-1');
    	map_Condition.put('TransfornCreatedDate_End','2013-1-1');
		map_Condition.put('TransfornActDate_Start','2013-1-1');
    	map_Condition.put('TransfornActDate_End','2013-1-1');
		map_Condition.put('PM_Birthday__c','2013-1-1');
		map_Condition.put('SuggestVisitTime','1');
		map_Condition.put('NotAPDPdtreatment','1');
		map_Condition.put('NotDropoutContactState','1');
		map_Condition.put('PM_FTCValid__c','1');
		map_Condition.put('PM_CAddress__c','1');
		map_Condition.put('PM_Binformation__c','1');
		map_Condition.put('PM_FaViTime1__c','2013-1-1');
		map_Condition.put('PM_FaViTime2__c','2013-1-1');
		map_Condition.put('PM_Stay_Treat__c','1');
        util.connectQueryString(field,map_Condition);
        string sql = util.getPatSql();
        try
        {
        	util.getPatientExcellist(sql,'');
        }
        catch(dmlexception e){}
        try
        {
        	util.getNormalConset(); 
        }
        catch(dmlexception e){}
        try
        {
        	util.getSortConset();
        }
        catch(dmlexception e){}
        try
        {
        	util.getSortConset(sql);
        }
        catch(dmlexception e){}
        try
        {
        	util.sortOrders(sql);
        }
        catch(dmlexception e){}
    }
}