/*
 * Hank
 * 2013.10.11
 * 病人调阅申请
 * 规则：病人调阅申请审批时，根据审批时间对应生成审批人
 */
trigger PM_AccToApplication on Application__c (before update) 
{
    //set<ID> set_Ids = new set<ID>();
    for(Application__c app:trigger.new)
    {
        Application__c old = trigger.oldMap.get(app.Id);
        if(app.PM_PSRApproveDate__c != old.PM_PSRApproveDate__c )
        {
            app.PM_PSRApproveUser__c = userinfo.getUserId();
        }
        if(app.PM_SuperviorApproveDate__c != old.PM_SuperviorApproveDate__c)
        {
            app.PM_Supervior__c = userinfo.getUserId();
        }
    }
    /*
    if(set_Ids.size() > 0)
    {
        map<ID,Application__c> map_App = new map<ID,Application__c>([Select Id,(Select StepStatus, ActorId, CreatedDate From ProcessSteps order by CreatedDate desc limit 1) From Application__c where Id in: set_Ids]);
        for(Application__c apps:trigger.new)
        {
            Application__c olds = trigger.oldMap.get(apps.Id);
            Application__c app = map_App.get(apps.Id);
            if(app.ProcessSteps != null && app.ProcessSteps.size() > 0)
            {
                ProcessInstanceHistory pih = app.ProcessSteps[0];
                if(apps.PM_PSRApproveDate__c != olds.PM_PSRApproveDate__c)
                {
                    apps.PM_PSRApproveUser__c = userinfo.getUserId();
                }
                if(apps.PM_SuperviorApproveDate__c != olds.PM_SuperviorApproveDate__c)
                {
                    apps.PM_Supervior__c = userinfo.getUserId();
                }
            }
        }
    }*/
}