/*
 * Tobe
 * 2013.10.28
 * 新建自由行申请时 生成任务给病人所在插管大区的负责人
 *
 */
trigger PM_AutoCreatedTaskForFreeTP on PM_Free_TP_Application__c (after insert) 
{
	set<Id> patient_id = new set<Id>();
	for(PM_Free_TP_Application__c tp : trigger.new)
	{
		if(tp.PM_Patient__c == null)
		{
			continue;
		}
		patient_id.add(tp.PM_Patient__c);
	}
	list<Task> list_Task = new list<Task>();
	map<Id,PM_Patient__c> map_Patient = new map<Id,PM_Patient__c>();
	for(PM_Patient__c patient :[select Name,PM_InHospital__r.Region__r.PM_RegionalHead__c,
									   PM_InHospital__r.Region__r.PM_RegionalHead__r.Name 
								From PM_Patient__c Where Id IN: patient_id])
	{
		if(patient.PM_InHospital__r.Region__r.PM_RegionalHead__c == null)
		{
			continue;
		}
		map_Patient.put(patient.Id,patient);
	}
	for(PM_Free_TP_Application__c tp : trigger.new)
	{
		if(!map_Patient.containsKey(tp.PM_Patient__c))
		{
			continue;
		}
		Task task = new Task();
		PM_Patient__c patient = map_Patient.get(tp.PM_Patient__c);
		task.OwnerId = patient.PM_InHospital__r.Region__r.PM_RegionalHead__c;
		task.whatId = tp.Id;
		task.Status = '进行中';
		task.Subject = patient.Name +'的自由行申请已经由'+userinfo.getName()+'提交，请查看。';
		task.Description = patient.Name +'的自由行申请已经由'+userinfo.getName()+'提交，请查看。';		
		list_Task.add(task);
	}
	if(list_Task.size()>0)
	{
		insert list_Task;
	}
}