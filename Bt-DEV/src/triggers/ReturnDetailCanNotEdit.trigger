/**
 * Author : Sunny
 * 功能：当挥发罐维修/退回申请处于“审批中”的时候，不允许新建、修改、删除维修/退回明细
 * 2013-6-9 Bill添加功能，
 * 当挥发罐维修/退回申请在"通过"时，不允许新建、修改、删除维修/退回明细
 * 2013-6-19 bill添加功能
 * 退回明细牵连退回明细，所以使用公共类ReturnCompleted.cls
 * 确认收货后判断该退回申请是否全部完成
 * 2013-6-26 bill添加功能
 * 当挥发罐退回申请“全部完成”的时候，不允许新建、修改、删除退回明细
 */
trigger ReturnDetailCanNotEdit on Vaporizer_ReturnAndMainten_Detail__c (before delete, before insert, before update,after update) {
	List<ID> list_RetIds = new List<ID>();
	Set<ID> set_RetIds = new Set<ID>();
    if(trigger.isInsert || trigger.isUpdate){
    	for(Vaporizer_ReturnAndMainten_Detail__c VapRet:trigger.new){
    		if(VapRet.Vaporizer_ReturnAndMainten__c != null){
    			list_RetIds.add(VapRet.Vaporizer_ReturnAndMainten__c );
    		}
    	}
    }else if(trigger.isDelete){
    	for(Vaporizer_ReturnAndMainten_Detail__c VapRet:trigger.old){
            if(VapRet.Vaporizer_ReturnAndMainten__c != null){
                list_RetIds.add(VapRet.Vaporizer_ReturnAndMainten__c );
            }
        }
    }  
    
    if(list_RetIds.size() > 0){
    	for(Vaporizer_ReturnAndMainten__c VapRet:[Select Id From Vaporizer_ReturnAndMainten__c Where Id in: list_RetIds And Approve_Result__c in ('审批中','通过','全部完成')]){
    		set_RetIds.add(VapRet.Id);
    	}
    }
    if(set_RetIds.size() > 0){
    	if(trigger.isInsert){
	        for(Vaporizer_ReturnAndMainten_Detail__c VapRet:trigger.new){
	            if(set_RetIds.contains(VapRet.Vaporizer_ReturnAndMainten__c)){
	                VapRet.addError('申请状态为“审批中”、“通过”或“全部完成”时,不允许操作。');
	            }
	        }
	    }else if(trigger.isUpdate){
	    	for(Vaporizer_ReturnAndMainten_Detail__c VapRet:trigger.new){
	            if(set_RetIds.contains(VapRet.Vaporizer_ReturnAndMainten__c)){
	            	if(VapRet.IsDelivered__c != trigger.oldMap.get(VapRet.Id).IsDelivered__c || VapRet.IsReceived__c != trigger.oldMap.get(VapRet.Id).IsReceived__c || VapRet.DeliveredAmount__c != trigger.oldMap.get(VapRet.Id).DeliveredAmount__c)
	            	{}else{
	                VapRet.addError('申请状态为“审批中”、“通过”或“全部完成”时,不允许操作。');
	            	}
	            }
	        }
	    }else if(trigger.isDelete){
	        for(Vaporizer_ReturnAndMainten_Detail__c VapRet:trigger.old){
	            if(set_RetIds.contains(VapRet.Vaporizer_ReturnAndMainten__c)){
                    VapRet.addError('申请状态为“审批中”、“通过”或“全部完成”时,不允许操作。');
                }
	        }
	    }
    }
    
    /******************Bill Update 2013/6/9 start**********************/
	    //获取更新退回明细的已确认收货的明细对应的退回申请
	    Set<ID> returnApplyIds = new Set<ID>();
	    if(trigger.isAfter && trigger.isUpdate)
	    {
		     for(Vaporizer_ReturnAndMainten_Detail__c VapRet:trigger.new)
		     {
		     	returnApplyIds.add(VapRet.Vaporizer_ReturnAndMainten__c);
		     }
	    }
    
	    //退回明细牵连退回明细，所以使用公共类ReturnCompleted.cls
	    //确认收货后判断该退回申请是否全部完成
	      ReturnCompleted complete = new ReturnCompleted();
	      complete.IsReturnCompleted(returnApplyIds);
    /******************Bill Update 2013/6/9 end**********************/
}