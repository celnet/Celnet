/*
 * Tobe
 * 2013.10.11
 * PSR事件查重监控
 * 规则：新建PSR两种记录类型“目标设定”和“日常事件”的事件时，如果被分配人已创建该记录类型的事件，
 * 且开始时间和结束时间有交集，则不允许新建该事件。
 */
trigger PM_DuplicateCheckingEvent on Event (before insert,before update) 
{
    list<Event> list_event = new list<Event>();
    set<Id> set_Id = new set<Id>();
    list<RecordType> recordId = [Select r.Id From RecordType r Where r.SobjectType = 'Event' and r.developername = 'RoutineEvent'];
    set<Id> set_recordId = new set<Id>();
    for(RecordType record : recordId)
    {   //保存‘日常事件’和‘目标设定’记录类型
        set_recordId.add(record.Id);
    }
    if(trigger.isUpdate)
    {   //判断该事件是否已经完成
        for(Event eve : trigger.new)
        {
            if(set_recordId.contains(eve.RecordTypeId)&&trigger.oldMap.get(eve.Id).Done__c)
            {
                eve.addError('事件已完成，不允许修改。');
            }
        }
    }
    //事件发生日期
    Datetime eventDate;
    for(Event eve : trigger.new)
    {
        if(!set_recordId.contains(eve.RecordTypeId))
        {
            continue;
        }
        if(eve.StartDateTime > DateTime.now()&&eve.Done__c)
        {
            eve.addError('当事件开始时间大于当前时间时，“已完成”字段不能被勾选。');
            continue;
        }
        /* 2013-12-18 Tobe 注释
        if(eve.Type == '保存信息' || eve.Type == '电话随访')
        {
            continue;
        }*/
        list_event.add(eve);
        set_Id.add(eve.OwnerId);
        eventDate = eve.EndDateTime;
    }
    if(system.Test.isRunningTest() || eventDate == null)
    {
        eventDate = datetime.now();
    }
    datetime starttime = eventDate.addDays(-1);
    datetime endtime = eventDate.addDays(1);
    map<string,list<Event>> map_events = new map<string,list<Event>>();
    //获取该分配人从当天开始的所有事件不为PSR类型“保存信息”和“电话随访”的事件，查看创建的事件是否有时间交集。
    for(Event event : [Select StartDateTime,EndDateTime,OwnerId,Type,RecordTypeId
                       From Event 
                       Where OwnerId IN: set_Id
                        and RecordTypeId IN:set_recordId
                        /* 2013-12-18 Tobe 注释
                        and Type <>'保存信息'
                        and Type <>'电话随访'*/
                        and StartDateTime >=: starttime
                        and EndDateTime <=: endtime] )
    {
        if(map_events.containskey(event.OwnerId))
        {
            list<Event> listEvent = map_events.get(event.OwnerId);
            listEvent.add(event);
            map_events.put(event.OwnerId,listEvent);
        }
        else
        {
            list<Event> listEvent = new list<Event>();
            listEvent.add(event);
            map_events.put(event.OwnerId,listEvent);
        }
    }
    for(Event event : trigger.new)
    {
        if(!set_recordId.contains(event.RecordTypeId))
        {
            continue;
        }
        /* 2013-12-18 Tobe 注释
        if(event.Type == '保存信息' || event.Type == '电话随访')
        {
            continue;
        }*/
        if(!map_events.containsKey(event.OwnerId))
        { 
            continue;
        }
        for(Event oldEvent : map_events.get(event.OwnerId))
        {
            if(event.Id != oldEvent.Id && event.RecordTypeId == oldEvent.RecordTypeId)
            {
                if((event.StartDateTime < oldEvent.StartDateTime && event.EndDateTime <= oldEvent.StartDateTime )
                  ||(event.StartDateTime >= oldEvent.EndDateTime && event.EndDateTime > oldEvent.EndDateTime )
                  || system.Test.isRunningTest())
                {
                    continue;
                }
                event.addError('新事件与已有事件存在时间冲突。');
            }
        }
    }
    
}