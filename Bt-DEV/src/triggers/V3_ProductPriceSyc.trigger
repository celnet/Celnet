/*****************************************************************************
 *Owner:AndyWang
 *Date:2012年9月12日 11:47:10
 *Function:
 *规则 create ProductPrice Syc of insert and update
*****************************************************************************/ 
trigger V3_ProductPriceSyc on Product_Price__c (before insert, before update) {
    String strErro='';
    //Account 是空的话就跳过
    Set<String> idsProductPriceName = new Set<String>();
    Set<String> idsAccountMID = new Set<String>();
    for(Product_Price__c Product_Price : trigger.new){
        idsProductPriceName.add(Product_Price.Name);
        if(Product_Price.Customer_No__c != null){
            idsAccountMID.add(Product_Price.Customer_No__c);
        }
    }
    //封装Account和Product
    Map<String,Id> mapAccountId = new Map<String,Id>();
    Map<String,Id> mapProductId = new Map<String,Id>();
    
        list<Product2> lisProduct  = [Select  p.V3_Item_Number__c, p.Id 
                                        From Product2 p where p.V3_Item_Number__c IN:idsProductPriceName];
        for(Product2 product:lisProduct){
            mapProductId.put(product.V3_Item_Number__c,product.id);
        }
        if(idsAccountMID.size() > 0){
            list<Account> lisAccount = [Select 
                                 a.MID__c,a.Id
                                From Account a where MID__c IN:idsAccountMID];
            for(Account account:lisAccount){
                mapAccountId.put(account.MID__c,account.id);
            }
        }
        
        for(Product_Price__c ProductPrice:trigger.new){
            //获取product id, account id
            if(mapProductId.ContainsKey(ProductPrice.name)){
                ProductPrice.Product__c=mapProductId.get(ProductPrice.name);
            } else{
                ProductPrice.addError('Can not match Product Info by Item No:'+ProductPrice.name);
                continue;
            }
            if(ProductPrice.Customer_No__c != null){
                    if(mapAccountId.ContainsKey(ProductPrice.Customer_No__c)){
                        ProductPrice.Account__c=mapAccountId.get(ProductPrice.Customer_No__c);
                    } else{
                        ProductPrice.addError('Can not match Account Info by Address No:'+ProductPrice.Customer_No__c);
                        continue;
                    }
                
            }
            //设置type
            if(ProductPrice.Customer_No__c != null){
                ProductPrice.Type__c = 'Customer';
            } else {
                ProductPrice.Type__c = ProductPrice.Price_List_Name__c;
                
            }
            /**
            strErro='';
            //转换日期
            if(ProductPrice.Eff_From__c != null){
                try{
                    Date Eff_From = JuLianToDate(Integer.valueOf(ProductPrice.Eff_From__c));
                    if(Eff_From != null){
                        ProductPrice.Starting_Date__c = Eff_From;
                    } 
                } catch(Exception e){
                    
                    ProductPrice.addError('Convert Date error:'+e.getMessage() + ';Item No=' + ProductPrice.Name + ';JulianDatavalue=' +ProductPrice.Eff_From__c );
                        continue;
                } 
                
            }
            if(ProductPrice.Eff_To__c != null){
                try{
                    Date Eff_To = JuLianToDate(Integer.valueOf(ProductPrice.Eff_To__c));
                    if(Eff_To != null){
                            ProductPrice.Closing_Date__c = Eff_To;
                    }
                } catch(Exception e){
                    ProductPrice.addError('Convert Date error:'+e.getMessage() + ';Item No=' + ProductPrice.Name + ';JulianDatavalue=' +ProductPrice.Eff_From__c );
                        continue;
                } 
            }
            **/
            
        }
    
    ///////////////////////////
    //JuLianToDateConvertDate//
    ///////////////////////////
    /**
    public Date JuLianToDate(Integer date1){
        
            //fool
            Integer L =date1 +68569;
            Integer N =4 * L / 146097;
            L = L - (146097*N + 3)/4;
            Integer I = 4000*(L + 1)/1461001;
            L = L - 1461 * I / 4 + 31;
            Integer J = 80 * L / 2447;
            Integer K = L - (2447 * J / 80);
            L = J/11;
            J = J + 2 - 12*L;
            I = 100*(N - 49) + I + L;
        
            Date Convert = Date.valueOf(I+'-'+J+'-'+K);
            return Convert;
    }
    **/
}