/**
*@Function: For replace the workflow Rule named '协访辅助' which will be disabled
*@Author: Ken
*@Date: 2012-11-02
*
*@Add new function: use code to replace the workflow(7 days expire).
*/
trigger V21_Event on Event (before insert, after insert, before update) {
	final String onlyChangeCurrentMonthVisit_zh='无法修改已完成的非本月拜访';
	final String cantChangeDTofCompletedVisit_zh='无法修改已完成拜访的开始或结束日期';
	final String cantChangeStatusOfComplete_zh='无法修改已完成拜访的完成状态';
	final String outOfDateForSupervisor_zh='已超出修改日期范围, 截止到';
	
    final String C_VisitTypeName = '拜访';
	final Integer visitExpireDays=7;
    
	if(Trigger.isBefore && Trigger.isUpdate){
		for(Event e:Trigger.new){
			Event e_Old=Trigger.oldMap.get(e.id);
			if(e.SubjectType__c != C_VisitTypeName || e_Old.SubjectType__c != C_VisitTypeName){
				continue;//Just for Visit
			}
			//2013-5-2 sunny修改，添加条件，如果IsGroupEvent=true并且V2_LinkEventID__c字段为空也需要记录事件ID到V2_LinkEventID__c字段。
			if((e_Old.IsGroupEvent==false && e.IsGroupEvent==true) || (e.IsGroupEvent == true && e.V2_LinkEventID__c==null)){
				e.V2_LinkEventID__c=e.Id;
			}else if(e_Old.IsGroupEvent==true && e.IsGroupEvent==false){
				e.V2_LinkEventID__c=null;
			}
		}
	}
	
	//for replacing the workflow
	Datetime currDT=Datetime.now();//System.now();
	Integer currMonth=currDT.month();
	Integer currYear=currDT.year();
	if(Trigger.isBefore && Trigger.isInsert){
		for(Event e:Trigger.new){
			if(e.SubjectType__c != C_VisitTypeName){
				continue;//Just for Visit
			}
			if(e.Done__c==true && e.StartDateTime!=null){
				Datetime startDT=e.StartDateTime;
				if(startDT.addDays(visitExpireDays)<currDT){
					e.V2_IsExpire__c=true;
				}else{
					e.V2_IsExpire__c=false;
				}
			}
		}
	}
	if(Trigger.isBefore && Trigger.isUpdate){
		User u=[Select u.Id, u.Profile.Name, u.ProfileId From User u where u.Id=:UserInfo.getUserId()];
		String u_pfName=u.profile.Name==null?'':u.profile.Name;
		Boolean isRep=u_pfName.indexOf('Sales Rep')>-1;
		Boolean isSupervisor=u_pfName.indexOf('Sales Supervisor')>-1;
		Boolean checkProfile=isRep || isSupervisor;
		for(Event e:Trigger.new){
			Event e_Old=Trigger.oldMap.get(e.id);
			if(!C_VisitTypeName.equals(e.SubjectType__c) || !C_VisitTypeName.equals(e_Old.SubjectType__c)){
				continue;//Just for Visit
			}
			if(e_Old.Done__c!=true && e.Done__c==true && e.StartDateTime!=null){
				Datetime startDT=e.StartDateTime;
				if(startDT.addDays(visitExpireDays)<currDT){
					e.V2_IsExpire__c=true;
				}else{
					e.V2_IsExpire__c=false;
				}
			}
			if(e_Old.Done__c==true && checkProfile){
				if(e.Done__c!=e_Old.Done__c){
					e.Done__c.addError(cantChangeStatusOfComplete_zh);
				}
				if(e.StartDateTime!=e_Old.StartDateTime){
					e.StartDateTime.addError(cantChangeDTofCompletedVisit_zh);
				}
				if(e.EndDateTime!=e_Old.EndDateTime){
					e.EndDateTime.addError(cantChangeDTofCompletedVisit_zh);
				}
				Datetime eStartDT=e_Old.StartDateTime;
				
				//被点评人 ,			点评人			评分			评语
				//BeReviewed1__c,	ReUser1__c,		Grade1__c,	Comment1__c
				Boolean invalidDateForSup=false;
				Boolean invalidFieldsChangeForSup=false;
				Boolean invalidChangedForSup=false;
				//Datetime superDT=null;
				if(eStartDT!=null){
					/*
					if(isSupervisor){
						invalidFieldsChangeForSup=e.V2_Solution__c!=e_Old.V2_Solution__c
									||e.CommentsBySupervisor__c!=e_Old.CommentsBySupervisor__c
									||e.CommentsBySupervisor2__c!=e_Old.CommentsBySupervisor2__c
									||e.V2_Score__c!=e_Old.V2_Score__c
									||e.V2_ScoreAuthor__c!=e_Old.V2_ScoreAuthor__c
									||e.V2_ScoreAuthor2__c!=e_Old.V2_ScoreAuthor2__c;//修改了则为无效修改=true
						
						invalidFieldsChangeForSup=
							   e.BeReviewed1__c!=e_Old.BeReviewed1__c || e.Comment1__c!=e_Old.Comment1__c
							|| e.ReUser1__c!=e_Old.ReUser1__c || e.Grade1__c!=e_Old.Grade1__c
							|| e.BeReviewed2__c!=e_Old.BeReviewed2__c || e.Comment2__c!=e_Old.Comment2__c
							|| e.ReUser2__c!=e_Old.ReUser2__c || e.Grade2__c!=e_Old.Grade2__c
							|| e.BeReviewed3__c!=e_Old.BeReviewed3__c || e.Comment3__c!=e_Old.Comment3__c
							|| e.ReUser3__c!=e_Old.ReUser3__c || e.Grade3__c!=e_Old.Grade3__c
							|| e.BeReviewed4__c!=e_Old.BeReviewed4__c || e.Comment4__c!=e_Old.Comment4__c
							|| e.ReUser4__c!=e_Old.ReUser4__c || e.Grade4__c!=e_Old.Grade4__c
							|| e.BeReviewed5__c!=e_Old.BeReviewed5__c || e.Comment5__c!=e_Old.Comment5__c
							|| e.ReUser5__c!=e_Old.ReUser5__c || e.Grade5__c!=e_Old.Grade5__c
							;
						
						Date superD_t=eStartDT.date();//2012-11-10
						Date superD_tmp = superD_t.addMonths(1);//2012-12-10
						superDT=Date.newInstance(superD_tmp.year(), superD_tmp.month(), 8);
						invalidDateForSup= currDT>superDT;//7号以后无效修改,禁止
						
						invalidChangedForSup = invalidFieldsChangeForSup && invalidDateForSup;
					}
					*/
					//销售或主管[有修改其他字段但没有修改禁止字段]
					//if(isRep || (isSupervisor && !invalidFieldsChangeForSup)){
					if(isRep){
						Integer startMonth=eStartDT.month();
						Integer startYear=eStartDT.year();
						if(currMonth!=startMonth || currYear!=startYear){
							e.addError(onlyChangeCurrentMonthVisit_zh);
						}
					}
					/*
					if(invalidChangedForSup){
						e.addError(outOfDateForSupervisor_zh+' ['+superDT+']');
					}
					*/
				}
			}
		}
	}
	
	if(Trigger.isAfter && Trigger.isInsert){
		Set<String> willBeUpdatedIdSet=new Set<String>();
		for(Event e:Trigger.new){
			if(e.SubjectType__c != C_VisitTypeName ){
				continue;//Just for Visit
			}
			if(e.IsGroupEvent==true && (e.V2_LinkEventID__c==null||e.V2_LinkEventID__c=='')){
				 willBeUpdatedIdSet.add(e.id);
			}
		}
		if(willBeUpdatedIdSet.size()>0){
			/*
			List<EventAttendee> evAttendees=[Select e.Id, e.EventId 
				From EventAttendee e
				Where e.EventId in:willBeUpdatedIdSet];
			willBeUpdatedIdSet.clear();
			for(EventAttendee ea: evAttendees){
				willBeUpdatedIdSet.add(ea.EventId);
			}
			*/
			List<Event> willBeUpdated=[select id,V2_LinkEventID__c,IsGroupEvent 
				from Event 
				where IsGroupEvent=true 
				and (V2_LinkEventID__c=null or V2_LinkEventID__c='') 
				and id in:willBeUpdatedIdSet];
			
			//String msg='系统调试中,拜访暂时无法操作...请稍后重试,谢谢.';			
			//	msg+='['+willBeUpdatedIdSet+'], willBeUpdatedIdSet.size()='+willBeUpdatedIdSet.size()+',';
			
			List<Event> newUpdated=new List<Event>();
			if(willBeUpdated.size()>0){
				for(Event e: willBeUpdated){
					if(e.V2_LinkEventID__c!=null && e.V2_LinkEventID__c!='')continue;
					e.V2_LinkEventID__c=e.id;
					newUpdated.add(e);
				}
				if(newUpdated.size()>0)update(newUpdated);
			}
			
		}
	}
}