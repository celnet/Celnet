/*
*作者：Michael
*时间：2013-9-18
*功能：更新成功拜访时间时，自动生成病人成功拜访事件
*/
trigger PM_AutoCreateEventForPatient on PM_Patient__c (before update) 
{
    //EventList
    list<Event> eList = new list<Event>();
    //psr事件类型的ID
    ID PSREventId = [Select r.Id  From RecordType r where r.SobjectType = 'Event' and r.DeveloperName = 'RoutineEvent' and r.IsActive = true][0].Id;
    ID DieId = [Select r.Id  From RecordType r where r.SobjectType = 'PM_Patient__c' and r.DeveloperName = 'PM_Die' and r.IsActive = true][0].Id;
    //治疗医院
    Set<ID> set_TreatIds = new Set<ID>();
    //插管医院
    Set<ID> set_InIds = new Set<ID>();
    
    for(PM_Patient__c p : trigger.new)
    {
        /*********************bill add 2013-10-5 start 死亡的病人改变记录类型********************************/
        //病人若是死亡，名字也不允许修改
        if(p.RecordTypeId == DieId && p.Name != trigger.oldMap.get(p.Id).Name)
        {
            p.Name.addError('病人已经死亡，名字不允许修改');
        }
        if(p.PM_DropOut_One_Reason__c == '死亡' || p.PM_CompetingReason__c == '死亡')
        {
            p.RecordTypeId = DieId;
        }
        //2013Sunny该逻辑放到，病人另外一个trigger上。
        //保持终端配送商一致
        /*
        if(p.PM_TerminalID__c != null && p.PM_TerminalID__c.indexOf('001') >=0)
        {
            p.PM_Terminal__c = p.PM_TerminalID__c;
            p.PM_TerminalID__c = null;
            p.PM_TerminalName__c = null;
        }
        */
        /*********************bill add 2013-10-5 end 死亡的病人改变记录类型   ********************************/
        /*********************bill add 2013-10-5 start 转active不用生成事件********************************/
        if(p.PM_Status__c != trigger.oldMap.get(p.Id).PM_Status__c && trigger.oldMap.get(p.Id).PM_Status__c == 'Dropout' && p.PM_Status__c == 'Active')
        {
            continue;
        }
        /*********************bill add 2013-10-5 end 转active不用生成事件   ********************************/
        /*********************bill add 2013-10-5 start 当病人治疗医院发生变化，则治疗销售也随之发生变化**********/
        if(p.PM_Treat_Hospital__c != NULL && p.PM_Treat_Hospital__c != trigger.oldMap.get(p.Id).PM_Treat_Hospital__c)
        {
            set_TreatIds.add(p.PM_Treat_Hospital__c);
        }
        //当前插管医院发生变化，当前负责销售
        if(p.PM_InHospital__c != NULL && p.PM_InHospital__c != trigger.oldMap.get(p.Id).PM_InHospital__c)
        {
            set_InIds.add(p.PM_InHospital__c);
        }
        /*********************bill add 2013-10-5 end 当病人治疗医院发生变化，则治疗销售也随之发生变化   ****************/
        PM_Patient__c old = trigger.oldMap.get(p.Id);
        
        //拜访成功生成事件，用上次拜访时间来标识
        if(p.PM_LastSuccessDateTime__c != old.PM_LastSuccessDateTime__c)
        {
            eList.add(buildEventforPatient(p,'success'));   
            if(old.PM_SucceedNO__c != null)
            {
                p.PM_SucceedNO__c = old.PM_SucceedNO__c + 1;
            }
            else
            {
                p.PM_SucceedNO__c = 1;
            }   
        }
        //拜访失败生成事件，用上次拜访失败时间来标识
        //2013-10-28 sunny：排除清楚上次拜访失败日期时间的情况。
        if(p.PM_LastFailDateTime__c  != old.PM_LastFailDateTime__c && p.PM_LastFailDateTime__c !=null)
        {
            eList.add(buildEventforPatient(p,'faild'));
            if(old.PM_VisitFailCount__c != null)
            {
                p.PM_VisitFailCount__c = old.PM_VisitFailCount__c + 1;
            }
            else
            {
                p.PM_VisitFailCount__c = 1;
            }
        }
    }
    
    if(eList.size() > 0)
    {
        insert eList;
    }
    //生成事件
    Event buildEventforPatient(PM_Patient__c pm,string type)
    {
        Event e = new Event();
        e.RecordTypeId = PSREventId;
        e.StartDateTime = pm.PM_EventStartDate__c;
        e.EndDateTime = pm.PM_EventEndDate__c;
        //2013-12-02 Sunny:病人拜访生产时间，需要将事件owner设置为当前操作人而不是病人所有人。
        e.OwnerId = UserInfo.getUserId();
        //e.OwnerId = pm.OwnerId;
        e.WhatId = pm.Id;
        if(type == 'success')
        {
            e.PM_VisitStatus__c = '完成';
            e.Subject = '成功对' + pm.Name + '进行拜访';
            e.Type = '电话随访';
        }
        if(type == 'faild')
        {
            e.Subject = '对' + pm.Name + '的拜访已失败';
            e.Type = '电话随访';
            //e.Description = pm.PM_FailureReason__c;
            e.PM_VisitStatus__c = '失败';
        }
        e.Description = e.Subject;
        return e;
    }
    
    //治疗医院发生变化，治疗销售也随之发生变化
    if(set_TreatIds != null && set_TreatIds.size()>0)
    {
        //获取当前的PD部门的负责的销售
        Map<ID,ID> map_salePD = NEW Map<ID,ID>();
        for(V2_Account_Team__c team : [Select v.V2_User__c , v.V2_Account__c  From V2_Account_Team__c v where UserProduct__c =  'PD' 
                                   AND ((V2_BatchOperate__c = '新增' And V2_ApprovalStatus__c = '审批通过')
                                   OR(V2_BatchOperate__c = '替换' And V2_ApprovalStatus__c = '审批拒绝')
                                   OR(V2_BatchOperate__c = '删除' And V2_ApprovalStatus__c = '审批拒绝'))
                                   AND V2_Account__c IN : set_TreatIds])
        {
            map_salePD.put(team.V2_Account__c,team.V2_User__c);
        }
        for(PM_Patient__c p : trigger.new)
        {
            if(p.PM_Treat_Hospital__c != NULL && p.PM_Treat_Hospital__c != trigger.oldMap.get(p.Id).PM_Treat_Hospital__c)
            {
                p.PM_Treat_Saler__c = map_salePD.get(p.PM_Treat_Hospital__c);
            }   
        }
    }
    //插管医院发生变化，当前负责销售也随之发生变化
    if(set_InIds != null && set_InIds.size()>0)
    {
        //获取当前的PD部门的负责的销售
        Map<ID,ID> map_salePD = NEW Map<ID,ID>();
        for(V2_Account_Team__c team : [Select v.V2_User__c , v.V2_Account__c  From V2_Account_Team__c v where UserProduct__c =  'PD' 
                                   AND ((V2_BatchOperate__c = '新增' And V2_ApprovalStatus__c = '审批通过')
                                   OR(V2_BatchOperate__c = '替换' And V2_ApprovalStatus__c = '审批拒绝')
                                   OR(V2_BatchOperate__c = '删除' And V2_ApprovalStatus__c = '审批拒绝'))
                                   AND V2_Account__c IN : set_InIds])
        {
            map_salePD.put(team.V2_Account__c,team.V2_User__c);
        }
        for(PM_Patient__c p : trigger.new)
        {
            if(p.PM_InHospital__c != NULL && p.PM_InHospital__c != trigger.oldMap.get(p.Id).PM_InHospital__c)
            {
                p.PM_Current_Saler__c = map_salePD.get(p.PM_InHospital__c);
            }   
        }
    }
}