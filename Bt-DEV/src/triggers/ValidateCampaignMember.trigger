/*
*开发人：Tommy Liu
*服务提供商：Cideatech
*时间：2013-3-9
*模块：市场活动成员导入验证规则
*编号：
*功能描述：
Dataloader导入时，系统判断导入记录唯一性的标准为代表名+医生名。导入是要验证
（1）若导入文件中有两条记录有相同的“代表名+医生名”，导入时系统认为是重复记录，导入失败；
（2）若“代表名+医生名”在系统中不存在这样的代表和医生的关系，导入时系统认为是非法数据，导入失败。
代表名和医生名必须在系统中存在，且医生和代表的关系通过医院、医生、代表三者关系查找。
*注意事项：
*开发经验：
*修改人：Scott
*时间：2013-3-20
*功能：新增删除验证,只有在字段‘允许删除’为true时才允许删除活动成员，否则提示用户提交修改成员审批。
*/
trigger ValidateCampaignMember on CampaignMember (before insert,before delete) 
{
	
	
	/*====2013-3-20添加=====*/
	if(trigger.isDelete)
	{
		//获取系统管理员的简档
	 	Profile p = [select Id,Name from Profile where Name = '系统管理员' or Name = '系統管理員' or Name = 'System Administrator'];
	 	
		for(CampaignMember Member:trigger.old)
		{
			//当市场活动成员的提交人和当前用户不相同时，不允许删除 ，只针对台湾地区的记录
			if(UserInfo.getUserId() != Member.User__c && Member.TW_CampRecordType__c == 'TW_Campaign' && UserInfo.getProfileId() != p.Id)
			{
				Member.addError('您不是該活動成員的提交人，無法刪除');
			}
			//2014-2-26 Sunny:除了TW和BQ其他的市场活动在报名开始时，可以删除市场活动成员
			if(Member.TW_CampRecordType__c != 'TW_Campaign' && Member.TW_CampRecordType__c != 'BQ_Campaign' && (Member.Campaign_Status__c =='Sign Up Begin' || Member.Campaign_Status__c =='报名开始'))
			{
				continue;
			}
			if(!Member.AllowToDelete__c)
			{
				//TW项目，Tommy modified at 2013-11-13, 按照用户的语言显示信息
				//Member.addError('您不能直接删除此活动成员，请您提交修改成员审批，如有疑问请联系系统管理员。');
				Member.addError(Label.Lab_ValidateCampaignMember_NotDeleteNotice);
			}
		}
		return ;
	}
	/*====2013-3-20添加=====*/
	
	
	Set<String> campNameSet = new Set<String>();
	Set<String> contactNameSet = new Set<String>();
	Set<String> salesNameSet = new set<String>();
	for(CampaignMember newCamp : trigger.new)
	{
		if(!isDataload(newCamp))
		{
			continue;
		}
		if(!genericValidate(newCamp))
		{
			continue;
		}
		campNameSet.add(newCamp.CampaignAid__c);
		contactNameSet.add(newCamp.ContactAid__c);
		salesNameSet.add(newCamp.UseAid__c);
	}
	List<Campaign> campList = [Select Id, Name From Campaign Where Name In: campNameSet];
	List<Contact> contactList = [Select Id, Name, AccountId, Account.Name From Contact Where Name In: contactNameSet];
	List<User> salesList = [Select Id, Name, Alias From User Where Alias In: salesNameSet];
	List<V2_Account_Team__c> relatedAccTeamList = [Select Id, 
		V2_Account__c, V2_Account__r.Name, 
		V2_User__c, V2_User__r.Name, V2_User__r.Alias,
		V2_Effective_Year__c, 
		V2_Effective_Month__c
		From V2_Account_Team__c 
		Where V2_BatchOperate__c = '新增'
		And V2_History__c = false
		And V2_ApprovalStatus__c = '审批通过'
		And V2_User__r.Alias IN:salesNameSet
		And V2_Account__c IN(
			Select AccountId From Contact Where Name IN: contactNameSet)
		];
	//除去失时间效关系
	List<V2_Account_Team__c> accTeamList = new List<V2_Account_Team__c>();
	Date thisDay = Date.today();
	for(V2_Account_Team__c accTeam : relatedAccTeamList)
	{
		Integer startYear = 1997;
		Integer startMonth = 1;
		if(accTeam.V2_Effective_Year__c != null && accTeam.V2_Effective_Year__c != '')
		{
			startYear = Integer.valueOf(accTeam.V2_Effective_Year__c);
		}
		if(accTeam.V2_Effective_Month__c != null && accTeam.V2_Effective_Month__c != '')
		{
			startMonth = Integer.valueOf(accTeam.V2_Effective_Month__c);
		}
		Date startDate = Date.newInstance(startYear, startMonth, 1);
		if(thisDay >= startDate)
		{
			accTeamList.add(accTeam);
		}
	}
	
	//0,一般性验证
	//1,验证市场活动、代表、医生是否存在
	//2,验证销售代表与医生的关系
	for(CampaignMember newCamp : trigger.new)
	{
		if(!isDataload(newCamp))
		{
			continue;
		}
		if(!genericValidate(newCamp))
		{
			continue;
		}
		Boolean isCpOk = false;
		Boolean isContactHas = false;
		Boolean isSalesHas = false;
		String exitError = '';
		List<Campaign> cpList = getCamp(newCamp.CampaignAid__c);
		Integer cpCount = cpList.size();
		if(cpCount == 0)
		{
			//exitError += ' 指定的市场活动不存在';
			exitError += ' Campaign is not exist';
		}
		else if(cpCount > 1)
		{
			//exitError += ' 找到多个重名的市场活动，市场活动名称：' + newCamp.CampaignAid__c + '，重复数量：' + cpCount;
			exitError += ' Find multiple marketing activites which has the same name';
		}
		else if(cpCount == 1)
		{
			isCpOk = true;
			newCamp.CampaignId = cpList[0].Id;
		}
		List<Contact> ctList = getContact(newCamp.ContactAid__c);
		if(ctList.size() == 0)
		{
			//exitError += ' 指定的医生不存在';
			exitError += ' Doctor is not exist';
		}
		else
		{
			isContactHas = true;
		}
		List<User> sList = getSales(newCamp.UseAid__c);
		if(sList.size() == 0)
		{
			//exitError += ' 指定的销售代表不存在';
			exitError += ' Sales Rep is not exist';
		}
		else
		{
			isSalesHas = true;
		}
		if(exitError != '')
		{
			newCamp.addError(exitError);
		}
		
		if(!isCpOk || !isContactHas || !isSalesHas)
		{
			continue;
		}
		
		//开始验证销售代表医生关系
		Map<Id, String> matchedContactMap = new Map<Id, String>();
		Map<Id, String> matchedSalesMap = new Map<Id, String>();
		Map<Id, String> matchedAccMap = new Map<Id, String>();
		List<V2_Account_Team__c> matchedAccountTeamList = matchAccountTeam(newCamp.UseAid__c, newCamp.ContactAid__c, matchedContactMap, matchedSalesMap, matchedAccMap);
		Integer matchedContactCount = matchedContactMap.keySet().size();
		Integer matchedSalesCount = matchedSalesMap.keySet().size();
		Integer matchedAccCount = matchedAccMap.keySet().size();
		
		if(matchedContactCount == 1 && matchedSalesCount == 1)//匹配成功
		{
			for(ID ctId : matchedContactMap.keySet())
			{
				newCamp.ContactId = ctId;
				if(matchedAccCount == 1)
				{
					for(ID accId : matchedAccMap.keySet())
					{
						newCamp.V2_Account__c = accId;
					}
				}
			}
			for(ID ctId : matchedSalesMap.keySet())
			{
				newCamp.User__c = ctId;
			}
		}
		else if(matchedContactCount == 0 && matchedSalesCount == 0)//匹配失败
		{
			//newCamp.addError('销售代表与医生关系不存在');
			newCamp.addError('No relationship with Sales Rep and Doctor');
		}
		else if(matchedContactCount > 1 && matchedSalesCount == 1 && matchedAccCount == 1)//同一个匹配的医院中有同名的医生
		{
			//newCamp.addError('在一个销售代表负责的一个医院中发现同名的医生，销售代表：' + matchedSalesMap.values()[0] + '，医院： ' + matchedAccMap.values()[0] + ', 医生：' + matchedContactMap.values()[0]);
			newCamp.addError('In one hospital which a sales representative is responsible for,some doctors are found having the same name.');
		}
		else if(matchedContactCount > 1 && matchedSalesCount ==1 && matchedAccCount >1)//
		{
			//newCamp.addError('在一个销售代表负责多个医院中发现同名的医生，销售代表：' + matchedSalesMap.values()[0] + ', 医生：' + matchedContactMap.values()[0]);
			newCamp.addError('A sales representative is responsible for more than one hospitals, some doctors will be found sharing the same name.');
		}
		else if(matchedSalesCount >1)
		{
			//newCamp.addError('匹配到多个同名的销售代表，系统无法确定医生关系，销售代表：' + matchedSalesMap.values()[0] + ', 医生：' + matchedContactMap.values()[0]);
			newCamp.addError('Matching with multiple sales representatives who has the same name, the system could not determing the relationship between doctors.');
		}
	}
	
	//验证已经正常关联市场活动ID+销售代表ID+医生ID是否已经存在
	//本功能应用系统标准功能，系统不允许在一个市场活动中添加重复ID的联系人（医生），如果存在将自动报出错误
	
	//即使是关系成功匹配也可能存在同名的医院，同名的医生，同名的用户，通过返回的Map来判断
	private List<V2_Account_Team__c> matchAccountTeam(String salesName, String contactName, Map<Id, String> matchedContactMap, Map<Id, String> matchedSalesMap, Map<Id, String> matchedAccMap)
	{
		List<V2_Account_Team__c> matchedAccountTeamList = new List<V2_Account_Team__c>();
		List<Contact> contList = getContact(contactName);
		for(V2_Account_Team__c acctTeam : accTeamList)
		{
			if(acctTeam.V2_User__r.Alias == salesName)
			{
				Boolean isMatchContact = false;
				for(Contact cont : contList)
				{
					if(cont.AccountId == acctTeam.V2_Account__c)
					{
						matchedContactMap.put(cont.Id, cont.Name);
						isMatchContact = true;
					}
				}
				if(isMatchContact)
				{
					matchedSalesMap.put(acctTeam.V2_User__c, acctTeam.V2_User__r.Alias);
					matchedAccMap.put(acctTeam.V2_Account__c, acctTeam.V2_Account__r.Name);
					matchedAccountTeamList.add(acctTeam);
				}
			}
		}
		return matchedAccountTeamList; 
	}
	
	private List<Campaign> getCamp(String campName)
	{
		List<Campaign> cpList = new List<Campaign>();
		for(Campaign cp : campList)
		{
			if(cp.Name == campName)
			{
				cpList.add(cp);
			}
		}
		return cpList;
	}
	
	private List<Contact> getContact(String contactName)
	{
		List<Contact> ctList = new List<Contact>();
		for(Contact ct : contactList)
		{
			if(ct.Name == contactName)
			{
				ctList.add(ct);
			}
		}
		return ctList;
	}
	
	private List<User> getSales(String salesName)
	{
		List<User> sList = new List<User>();
		for(User u : salesList)
		{
			if(u.Alias == salesName)
			{
				sList.add(u);
			}
		}
		return sList;
	}
	
	//用来区分是否由DataLoader导入,还是用户通过UI创建
	private Boolean isDataload(CampaignMember newCamp)
	{
		if(newCamp.CampaignId != null && (newCamp.LeadId != null || newCamp.ContactId != null))
		//if(newCamp.CampaignId != null)
		{
			return false;
		}
		return true;
	}
	//1，验证是否指定必要字段
	//2，导入文档中的同一批次进行重复性验证，同一个市场活动中不允许医生-销售代表重复
	private Boolean genericValidate(CampaignMember newCamp)
	{
		String error = '';
		if(newCamp.CampaignAid__c == null || newCamp.CampaignAid__c == '')
		{
			//error += '未指定市场活动 ';
			error += 'Not specified Campaign';
		}
		if(newCamp.ContactAid__c == null || newCamp.ContactAid__c == '')
		{
			//error += '未指医生 ';
			error += 'Not specified Doctor ';
		}
		if(newCamp.UseAid__c == null || newCamp.UseAid__c == '')
		{
			//error += '未指定销售代表 ';
			error += 'Not specified Sales Rep ';
		}
		if(error != '')
		{
			newCamp.addError(error);
			return false;
		}
		
		//导入文档中的同一批次进行重复性验证，同一个市场活动中不允许医生-销售代表重复
		CampaignMember dupCm = null;
		for(CampaignMember cm : trigger.new)
		{
			if(cm != newCamp)//排除自己的情况
			{
				if(cm.CampaignAid__c == newCamp.CampaignAid__c 
					&& cm.ContactAid__c == newCamp.ContactAid__c
					&& cm.UseAid__c == newCamp.UseAid__c)
				{
					dupCm = cm;
					break;
				}
			}
		}
		if(dupCm != null)//重复的
		{
			//newCamp.addError('文档中存在重复的记录，市场活动：' + newCamp.CampaignAid__c + '，医生：' + newCamp.ContactAid__c + '，销售代表：' + newCamp.UseAid__c);
			//dupCm.addError('文档中存在重复的记录，市场活动：' + dupCm.CampaignAid__c + '，医生：' + dupCm.ContactAid__c + '，销售代表：' + dupCm.UseAid__c);
			newCamp.addError('Duplicate records in the Documents');
			dupCm.addError('Duplicate records in the Documents');
			return false;
		}
		return true;
	}
}