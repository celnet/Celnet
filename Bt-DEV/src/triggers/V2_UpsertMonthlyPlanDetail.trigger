/*
Author：Scott
Created on：2011-12-13
Description: 
1.将月计划明细上记录，以医院分组将同一医院的拜访信息进行汇总。
2.如果明细上有对新增医院的拜访，则对应的明细按医院上的也要新增对此医院的汇总。
3.如果删除明细对某医院的所有的拜访，则对应明细按医院上次医院信息也要删除。 
*/
trigger V2_UpsertMonthlyPlanDetail on MonthlyPlanDetail__c (after delete, after insert, after undelete, 
after update) {
	Set<Id> MonthPlanId =  new Set<Id>();
	if(trigger.isInsert)
	{
		for(MonthlyPlanDetail__c mpd:trigger.new)
		{
			MonthPlanId.add(mpd.MonthlyPlan__c);
		}
	}
	if(trigger.isUpdate)
	{
		for(MonthlyPlanDetail__c newmpd:trigger.new)
		{
			MonthlyPlanDetail__c oldmpd = trigger.oldMap.get(newmpd.Id);
			if(newmpd.AdjustedTimes__c !=oldmpd.AdjustedTimes__c ||  newmpd.Planned_Finished_Calls__c != oldmpd.Planned_Finished_Calls__c)
			{
				MonthPlanId.add(newmpd.MonthlyPlan__c);
			}
		}
	}
	if(trigger.isDelete)
	{
		for(MonthlyPlanDetail__c mpd:trigger.old)
		{
			MonthPlanId.add(mpd.MonthlyPlan__c);
		}
	}
	if(trigger.isUnDelete)
	{
		for(MonthlyPlanDetail__c mpd:trigger.new)
		{
			MonthPlanId.add(mpd.MonthlyPlan__c);
		}
	}
	
	//插入月计划明细按医院
	List<MonthlyPlanDetailByAccount__c> insertmpdba = new List<MonthlyPlanDetailByAccount__c>();
	//更新月计划明细按医院
	List<MonthlyPlanDetailByAccount__c> updatempdba = new List<MonthlyPlanDetailByAccount__c>();
	//删除月计划明细按医院
	List<MonthlyPlanDetailByAccount__c> deletempdba = new List<MonthlyPlanDetailByAccount__c>();
	
	
	//月计划明细
	//客户及对应计划次数
	Map<Id,Integer> AccAdjustedTimesMap = new Map<Id,Integer>();
	//客户及对应完成次数
	Map<Id,Integer> AccFinishedMap = new Map<Id,Integer>();
	//客户及对应月计划Id
	Map<Id,Id> accmpMap = new Map<Id,Id>();
	for(MonthlyPlanDetail__c mpd:[select Id,Account__c,MonthlyPlan__c,AdjustedTimes__c,Planned_Finished_Calls__c 
								  from MonthlyPlanDetail__c
								  where MonthlyPlan__c in:MonthPlanId])
	{
		accmpMap.put(mpd.Account__c,mpd.MonthlyPlan__c);
		if(AccAdjustedTimesMap.containsKey(mpd.Account__c))
		{
			if(mpd.AdjustedTimes__c != null)
			{
				AccAdjustedTimesMap.put(mpd.Account__c,(AccAdjustedTimesMap.get(mpd.Account__c)+Integer.valueOf(mpd.AdjustedTimes__c)));
			}
		}
		else
		{
			if(mpd.AdjustedTimes__c != null)
			{
				AccAdjustedTimesMap.put(mpd.Account__c,Integer.valueOf(mpd.AdjustedTimes__c));
			}
			else
			{
				AccAdjustedTimesMap.put(mpd.Account__c,0);
			}
		}
		if(AccFinishedMap.containsKey(mpd.Account__c))
		{
			if(mpd.Planned_Finished_Calls__c != null)
			{
				AccFinishedMap.put(mpd.Account__c,(AccFinishedMap.get(mpd.Account__c)+Integer.valueOf(mpd.Planned_Finished_Calls__c)));
			}
		}
		else
		{
			if(mpd.Planned_Finished_Calls__c != null )
			{
				AccFinishedMap.put(mpd.Account__c,Integer.valueOf(mpd.Planned_Finished_Calls__c));
			}
			else
			{
				AccFinishedMap.put(mpd.Account__c,0);
			}
		}
	}
	
	//月计划明细按医院
	List<MonthlyPlanDetailByAccount__c> mpdbalist = [select Account__c,PlannedTimes__c,ActualTimes__c from MonthlyPlanDetailByAccount__c
											 		 where MonthlyPlan__c in:MonthPlanId];
	
	if(mpdbalist != null && mpdbalist.size()>0)
	{
		if(AccAdjustedTimesMap != null && AccAdjustedTimesMap.size()>0)
		{
			for(Id MPDaccid:AccAdjustedTimesMap.keySet())
			{
				Boolean flag = true;//标记是否有在明细按医院中存在相同客户
				for(MonthlyPlanDetailByAccount__c mpdba:mpdbalist)
				{
					if(mpdba.Account__c != MPDaccid)
					{
						continue;
					}
					flag = false;
					mpdba.PlannedTimes__c = AccAdjustedTimesMap.get(mpdba.Account__c);
					mpdba.ActualTimes__c = AccFinishedMap.get(mpdba.Account__c);
					updatempdba.add(mpdba);
				}
				if(flag)
				{
					MonthlyPlanDetailByAccount__c mpdba = new MonthlyPlanDetailByAccount__c();
					mpdba.MonthlyPlan__c = accmpMap.get(MPDaccid);
					mpdba.Account__c = MPDaccid;
					mpdba.PlannedTimes__c = AccAdjustedTimesMap.get(MPDaccid);
					mpdba.ActualTimes__c = AccFinishedMap.get(MPDaccid);
					insertmpdba.add(mpdba);
				}
			}
			//判断是否有详细按医院中有的客户但是在详细中没有
			for(MonthlyPlanDetailByAccount__c mpdba:mpdbalist)
			{
				Boolean flag =true;
				for(Id MPDaccid:AccAdjustedTimesMap.keySet())
				{
					if(MPDaccid != mpdba.Account__c)
					{
						continue;
					}
					flag = false;
				}
				if(flag)
				{
					deletempdba.add(mpdba);
				}
			}
		} 
		else
		{
			deletempdba = mpdbalist;
		}
	}
	else
	{
		for(Id MPDaccid:AccAdjustedTimesMap.keySet())
		{
			MonthlyPlanDetailByAccount__c mpdba = new MonthlyPlanDetailByAccount__c();
			mpdba.MonthlyPlan__c = accmpMap.get(MPDaccid);
			mpdba.Account__c = MPDaccid;
			mpdba.PlannedTimes__c = AccAdjustedTimesMap.get(MPDaccid);
			mpdba.ActualTimes__c = AccFinishedMap.get(MPDaccid);
			insertmpdba.add(mpdba);
		}
	}
	insert insertmpdba;
	update updatempdba;
	delete deletempdba;
}