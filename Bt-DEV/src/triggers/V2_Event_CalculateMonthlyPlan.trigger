/*
作者: Tommy
创建时间: 2011-12-7
功能描述: 
如果是拜访类型的事件，需要对事件执行以下自动化事件任务 
更新月计划: TODO
    1〉事件创建需更新月计划明细中的“已安排”数量。
    2〉拜访完成需要更新月计划明细中的“已完成”数量。
约束: 
只有拜访类型的事件应用本过程    
风险：
本代码依赖了picklist 的选项值的中文值，有翻译的潜在风险
2011-12-16修改：
修改人：Scott
修改内容：修正新增联系人拜访事件时不向明细同步
           修正SQL语句超限隐患。
2011-12-22修改：
修改人：Scott
修改内容:
如果拜访是市场活动追踪拜访则不允许删除(系统管理员可以)
如果拜访是市场活动追踪拜访则联系人与市场活动不能修改
统计回访完成数(未过期)
统计所有与市场活动关联的拜访完成数
统计
*/

trigger V2_Event_CalculateMonthlyPlan on Event (after delete, after insert, after undelete, after update) 
{
    final String C_VisitTypeName = '拜访';
    
    //获取所有可能需要重新计算的月计划, 事件的月份、年份、和销售确定一个对应的月计划
    Set<Id> set_ownerId = new Set<Id>();
    Set<Id> set_contactId = new set<Id>();
    Set<Event> set_event = new Set<Event>();
    //年+月+提交人Id
    Set<String> Set_YearMonthUserid = new Set<String>();
    //市场活动Id
    Set<Id> campaignids = new Set<Id>();
    
    //为限定查询记录数量,增加日期时间限制, Ken
    DateTime startDate=System.now();
    DateTime endDate=System.now();
        
    if(trigger.isInsert || trigger.isUnDelete)
    {
        for(Event ev : trigger.new)
        {
        	System.debug('#############################################################33    '+ev.SubjectType__c);
            if(ev.SubjectType__c != C_VisitTypeName)
            {
                continue;
            }
            if(ev.WhoId == null)
            {
                continue;
            }
            if(ev.OwnerId != null && ev.StartDateTime != null && ev.EndDateTime != null)
            {
                String flag = String.valueOf(ev.StartDateTime.year())+String.valueOf(ev.StartDateTime.month())+String.valueOf(ev.OwnerId).trim();
                Set_YearMonthUserid.add(flag.Substring(0,flag.Length()-3));
            }
            //关联市场活动并且已经完成
            if(ev.WhatId !=null && ev.Done__c )
            {
                campaignids.add(ev.WhatId);
            }
            set_event.add(ev);
            set_ownerId.add(ev.OwnerId);
            set_contactId.add(ev.whoId);
            
            //Optimized-002 Ken
            if(ev.StartDateTime<startDate){
            	startDate=ev.startDateTime;
            }
            if(ev.EndDateTime>endDate){
            	endDate=ev.EndDateTime;
            }
            //Optimized-002
        }
    }
    if(trigger.isUpdate)
    {
        for(Event ev : trigger.new)
        {
            Event oldEv = trigger.oldMap.get(ev.Id);
            if(ev.SubjectType__c != C_VisitTypeName)
            {
                continue;
            }
            if(ev.WhoId == null)
            {
                continue;
            }
            if(ev.V2_FollowEventFlag__c)
            {
                if(ev.WhoId != oldEv.WhoId)
                {
                    ev.WhoId.addError('当前拜访是市场活动追踪拜访，联系人不允许修改！');
                }
                if(ev.WhatId != oldEv.WhatId)
                {
                    ev.WhatId.addError('当前拜访是市场活动追踪拜访，市场活动不允许修改！');
                }
            }
            
            //向市场活动统计
            if(ev.WhatId != null && ev.Done__c != oldEv.Done__c)
            {
                campaignids.add(ev.WhatId);
            }
            //所有可能引起数值变化的状态变化，需要重新计算
            if(ev.Done__c != oldEv.Done__c ||ev.OwnerId != oldEv.OwnerId || ev.StartDateTime != oldEv.StartDateTime || ev.whoId != oldEv.whoId || ev.SubjectType__c != oldEv.SubjectType__c)
            {
                if(ev.StartDateTime != null && ev.EndDateTime != null)
                {
                    String flag = String.valueOf(ev.StartDateTime.year())+String.valueOf(ev.StartDateTime.month())+String.valueOf(ev.OwnerId).trim();
                    Set_YearMonthUserid.add(flag.Substring(0,flag.Length()-3));
                }
                set_event.add(ev);
                set_ownerId.add(ev.OwnerId);
                set_contactId.add(ev.whoId);
                set_contactId.add(oldEv.whoId); 
	                
	            //Optimized-003 Ken
	            if(ev.StartDateTime<startDate){
	            	startDate=ev.startDateTime;
	            }
	            if(ev.EndDateTime>endDate){
	            	endDate=ev.EndDateTime;
	            }
	            if(oldEv.StartDateTime<startDate){
	            	startDate=oldEv.startDateTime;
	            }
	            if(oldEv.EndDateTime>endDate){
	            	endDate=oldEv.EndDateTime;
	            }
	            //Optimized-003
            }
        }
    }
    if(trigger.isDelete)
    {
        //只有系统管理员才可以删除市场活动追踪拜访
        Boolean flag = true;
        Profile p = [Select Name From Profile where Id=:UserInfo.getProfileId()];
        if(p.Name =='系统管理员' || p.Name =='System Administrator')
        {
            flag =false;
        }
        for(Event ev : trigger.old)
        {
            if(ev.SubjectType__c != C_VisitTypeName)
            {
                continue;
            }
            if(ev.whoId == null)
            {
                continue;
            }
            if(ev.V2_FollowEventFlag__c && flag)
            {
                ev.addError('此拜访是市场活动的追踪拜访不允许删除！');
            }
            if(ev.OwnerId != null && ev.StartDateTime != null && ev.EndDateTime != null)
            {
                String keyflag = String.valueOf(ev.StartDateTime.year())+String.valueOf(ev.StartDateTime.month())+String.valueOf(ev.OwnerId).trim();
                Set_YearMonthUserid.add(keyflag.Substring(0,keyflag.Length()-3));
            }
            //市场活动统计信息
            if(ev.WhatId != null && ev.Done__c )
            {
                campaignids.add(ev.WhatId);
            }
            set_event.add(ev);
            set_ownerId.add(ev.OwnerId);
            set_contactId.add(ev.whoId); 
            
            //Optimized-003 Ken
            if(ev.StartDateTime<startDate){
            	startDate=ev.startDateTime;
            }
            if(ev.EndDateTime>endDate){
            	endDate=ev.EndDateTime;
            }
            //Optimized-003
        }
    }
    /*
     *向月计划上更新事件
    */
    //更新明细
    List<MonthlyPlanDetail__c> updatempd = new List<MonthlyPlanDetail__c>();
    //插入明细       事件上WhoID 及 对应 明细
    Map<Id,MonthlyPlanDetail__c> EvIdMpdMap = new Map<Id,MonthlyPlanDetail__c>();
    
    //查出事件Owner的所有事件将其装入map，key：年月；value=List<Event>
     Map<String,List<Event>> EventMap = new Map<String,List<Event>>();
    
    //Optimized-004 Ken
    //startDate为最早Event开始时间的本月第一天,endDate为最晚日期 下月第一天 
    startDate = Date.newInstance(startDate.year(), startDate.month(), 1);//开始日期当月第一天, 2012-7-1 00:00:00.000
    Datetime dt0 = endDate.addMonths(1);//最晚日期下一个月
    endDate = Date.newInstance(dt0.year(), dt0.month(), 1);//最后日期下月第一天, 2012-10-1 00:00:00.000
    //Optimized-004
    
    List<Event> evs=[Select Id, Done__c,StartDateTime,EndDateTime,WhoId,OwnerId,V2_IsExpire__c 
    			   From Event Where OwnerId in:set_ownerId 
                   And SubjectType__c =: C_VisitTypeName 
                   and WhoId != null 
                   and RecordType.DeveloperName = 'V2_Event' 
                   And StartDateTime>:startDate And EndDateTime<:endDate
                   order by CreatedDate desc limit 5000];
     for(Event ev:evs)
     {
        String key1 = String.valueOf(ev.StartDateTime.year())+String.valueOf(ev.StartDateTime.month()+String.valueOf(ev.OwnerId).trim());
        String key = key1.Substring(0,key1.Length()-3);
        /*commented by Ken, Optimized-001
        if(EventMap.containsKey(key))
        {
            List<Event> evlist = EventMap.get(key); 
            evlist.add(ev);
            EventMap.put(key,evlist);
        }
        else
        {
            List<Event> evlist = new List<Event>();
            evlist.add(ev);
            EventMap.put(key,evlist);
        }
        */
        //Opitimized-001 Ken
        List<Event> evlist = EventMap.get(key);
        if(evlist==null){
        	evlist = new List<Event>();
        }
        evlist.add(ev);
        EventMap.put(key,evlist);
        //Opitimized-001
        
     }  
     //查出联系人及其对应客户
      Map<Id,Id> ContactAccMap = new Map<Id,Id>();
      for(Contact co:[select Id,AccountId from Contact where Id in: set_contactId])
      {
        ContactAccMap.put(co.Id,co.AccountId);
      }
    /*2012-3-15修改，因为父级向下及查询会超salesforce限制,所以分开查询*/
    Set<Id> mpids = new Set<Id>();
    List<MonthlyPlan__c> mps=[select Id,V2_MpYearMonthUserId__c,OwnerId from MonthlyPlan__c 
    	where V2_MpYearMonthUserId__c in:Set_YearMonthUserid];
    for(MonthlyPlan__c mp:mps)
    {
        mpids.add(mp.Id);
    }
    //Map 月计划Id 及其下关联月计划明细
    Map<Id,List<MonthlyPlanDetail__c>> Map_Mpd = new Map<Id,List<MonthlyPlanDetail__c>>();
    List<MonthlyPlanDetail__c> mpds=[Select Account__c,MonthlyPlan__c,MonthlyPlan__r.OwnerId,MonthlyPlan__r.V2_MpYearMonthUserId__c,
                                  AdjustedTimes__c,ArrangedTimes__c,Planned_Finished_Calls__c,Contact__c 
                                  From MonthlyPlanDetail__c where Contact__c != null
                                  and MonthlyPlan__c in:mpids];
    for(MonthlyPlanDetail__c mpd:mpds)
    {
	    /*
	    commented by Ken, Optimized-006
        if(Map_Mpd.containsKey(mpd.MonthlyPlan__c))
        {
            List<MonthlyPlanDetail__c> mplist = Map_Mpd.get(mpd.MonthlyPlan__c);
            mplist.add(mpd);
            Map_Mpd.put(mpd.MonthlyPlan__c,mplist);
        }
        else
        {
            List<MonthlyPlanDetail__c> mplist =  new List<MonthlyPlanDetail__c>();
            mplist.add(mpd);
            Map_Mpd.put(mpd.MonthlyPlan__c,mplist);
        }
	    Optimized-006
	    */
	    //Optimized-006 Ken
	    List<MonthlyPlanDetail__c> mplist = Map_Mpd.get(mpd.MonthlyPlan__c);
        if(mplist==null)
        {
             mplist =  new List<MonthlyPlanDetail__c>();
        }
        mplist.add(mpd);
        Map_Mpd.put(mpd.MonthlyPlan__c,mplist);
	    //Optimized-006
    }
    //遍历月计划
    /*
    commented by Ken, Optimized-005
    List<MonthlyPlan__c> mps2=[select Id,V2_MpYearMonthUserId__c,OwnerId from MonthlyPlan__c 
    	where V2_MpYearMonthUserId__c in:Set_YearMonthUserid];
    for(MonthlyPlan__c mp:mps2)
    Optimized-005
    */
    //Optimized-005 Ken
    for(MonthlyPlan__c mp:mps)
    //Optimized-005
    {
        if(Map_Mpd != null && Map_Mpd.containsKey(mp.Id))
        {
            for(MonthlyPlanDetail__c mpd:Map_Mpd.get(mp.Id))
            {
                 for(String key:EventMap.keySet())
                {
                    if(key != mpd.MonthlyPlan__r.V2_MpYearMonthUserId__c)
                    {
                        continue;
                    }
                    /*2012-3-30修改：新增判断是否明细中有此目标联系人；避免因为明细中没有联系人，
                                                    在重新汇总的时候找不到值导致所有明细安排和计划次数成0*/
                    if(!set_contactId.contains(mpd.Contact__c))
                    {
                        continue;
                    }
                    mpd.ArrangedTimes__c  = 0;
                    mpd.Planned_Finished_Calls__c = 0;
                    for(Event ev:EventMap.get(key))
                    {
                        if(ev.OwnerId != mpd.MonthlyPlan__r.OwnerId)
                        {
                            continue;
                        }
                        if(ev.WhoId != mpd.Contact__c)
                        {
                            continue;
                        }
                        //安排次数
                        mpd.ArrangedTimes__c ++;
                        if(ev.Done__c && ev.V2_IsExpire__c != true)
                        {
                            mpd.Planned_Finished_Calls__c ++;
                        }
                     }
                     updatempd.add(mpd);
                }
            }
            
            //新目标联系人Logic
            for(Event ev:set_event)
            {
                /*2012-3-14修改*/
                if(ev.OwnerId != mp.OwnerId)
                { 
                    continue;
                }
                Boolean flag = true;//标记是否月计划中没有此事件关联的联系人
                for(MonthlyPlanDetail__c mpd:Map_Mpd.get(mp.Id))
                {
                    if(mpd.Contact__c != ev.WhoId)
                    {
                        continue;
                    }
                    flag  = false;
                }
                if(flag)
                {
                    if(EvIdMpdMap.containsKey(ev.WhoId))
                    {
                        MonthlyPlanDetail__c mpd = EvIdMpdMap.get(ev.WhoId);
                        mpd.ArrangedTimes__c +=1;
                        if(ev.Done__c && ev.V2_IsExpire__c != true)
                        {
                            mpd.Planned_Finished_Calls__c +=1;
                        }
                    }
                    else
                    {   
                        MonthlyPlanDetail__c mpd = new MonthlyPlanDetail__c();
                        mpd.MonthlyPlan__c = mp.Id;
                        mpd.Contact__c = ev.WhoId;
                        mpd.Account__c = ContactAccMap.get(ev.WhoId);
                        mpd.AdjustedTimes__c = 0;
                        mpd.ArrangedTimes__c = 1;
                        if(ev.Done__c && ev.V2_IsExpire__c != true)
                        {
                            mpd.Planned_Finished_Calls__c = 1;
                        }
                        else
                        {
                            mpd.Planned_Finished_Calls__c = 0;
                        }
                        EvIdMpdMap.put(ev.WhoId,mpd);
                    }
                }
            }
        }
        //插入
        else
        {
            for(Event ev:set_event)
            {
                String key = String.valueOf(ev.StartDateTime.year())+String.valueOf(ev.StartDateTime.month())+String.valueOf(ev.OwnerId).trim();
                String flag = key.Substring(0,key.Length()-3);
                if(flag != mp.V2_MpYearMonthUserId__c)
                {
                    continue;
                }
                if(EvIdMpdMap.containsKey(ev.WhoId))
                {
                    MonthlyPlanDetail__c mpd = EvIdMpdMap.get(ev.WhoId);
                    mpd.ArrangedTimes__c +=1;
                    if(ev.Done__c && ev.V2_IsExpire__c != true)
                    {
                        mpd.Planned_Finished_Calls__c +=1;
                    }
                }
                else
                {
                    MonthlyPlanDetail__c mpd = new MonthlyPlanDetail__c();
                    mpd.MonthlyPlan__c = mp.Id; 
                    mpd.Contact__c = ev.WhoId;
                    mpd.Account__c = ContactAccMap.get(ev.WhoId);
                    mpd.AdjustedTimes__c = 0;
                    mpd.ArrangedTimes__c = 1;
                    if(ev.Done__c && ev.V2_IsExpire__c != true)
                    {
                        mpd.Planned_Finished_Calls__c = 1;
                    }
                    else
                    {
                        mpd.Planned_Finished_Calls__c = 0;
                    }
                    EvIdMpdMap.put(ev.WhoId,mpd);
                }
            }
        }
    }
    /*因为salesforce父级向子级查询条数限制，废弃
    //查找月计划
    for(MonthlyPlan__c mp:[select Id,V2_MpYearMonthUserId__c,
                          (Select Account__c,MonthlyPlan__c,MonthlyPlan__r.OwnerId,AdjustedTimes__c,ArrangedTimes__c,
                           Planned_Finished_Calls__c,Contact__c From MonthlyPlanDetail__r where Contact__c != null) 
                           from MonthlyPlan__c where V2_MpYearMonthUserId__c in:Set_YearMonthUserid])
    {
        //更新
        if(mp.MonthlyPlanDetail__r != null && mp.MonthlyPlanDetail__r.size()>0)
        {
            for(MonthlyPlanDetail__c mpd:mp.MonthlyPlanDetail__r)
            {
                 for(String key:EventMap.keySet())
                {
                    if(key != mp.V2_MpYearMonthUserId__c)
                    {
                        continue;
                    }
                    mpd.ArrangedTimes__c  = 0;
                    mpd.Planned_Finished_Calls__c = 0;
                    for(Event ev:EventMap.get(key))
                    {
                        if(ev.OwnerId != mpd.MonthlyPlan__r.OwnerId)
                        {
                            continue;
                        }
                        if(ev.WhoId != mpd.Contact__c)
                        {
                            continue;
                        }
                        //安排次数
                        mpd.ArrangedTimes__c ++;
                        if(ev.Done__c && ev.V2_IsExpire__c != true)
                        {
                            mpd.Planned_Finished_Calls__c ++;
                        }
                     }
                     updatempd.add(mpd);
                }
            }
            
            //新目标联系人Logic
            for(Event ev:set_event)
            {
                Boolean flag = true;//标记是否月计划中没有此事件关联的联系人
                for(MonthlyPlanDetail__c mpd:mp.MonthlyPlanDetail__r)
                {
                    if(mpd.Contact__c != ev.WhoId)
                    {
                        continue;
                    }
                    flag  = false;
                }
                if(flag)
                {
                    if(EvIdMpdMap.containsKey(ev.WhoId))
                    {
                        MonthlyPlanDetail__c mpd = EvIdMpdMap.get(ev.WhoId);
                        mpd.ArrangedTimes__c +=1;
                        if(ev.Done__c && ev.V2_IsExpire__c != true)
                        {
                            mpd.Planned_Finished_Calls__c +=1;
                        }
                    }
                    else
                    {   
                        MonthlyPlanDetail__c mpd = new MonthlyPlanDetail__c();
                        mpd.MonthlyPlan__c = mp.Id;
                        mpd.Contact__c = ev.WhoId;
                        mpd.Account__c = ContactAccMap.get(ev.WhoId);
                        mpd.AdjustedTimes__c = 0;
                        mpd.ArrangedTimes__c = 1;
                        if(ev.Done__c && ev.V2_IsExpire__c != true)
                        {
                            mpd.Planned_Finished_Calls__c = 1;
                        }
                        else
                        {
                            mpd.Planned_Finished_Calls__c = 0;
                        }
                        EvIdMpdMap.put(ev.WhoId,mpd);
                    }
                }
            }
            
        }
        //插入
        else
        {
            for(Event ev:set_event)
            {
                String key = String.valueOf(ev.StartDateTime.year())+String.valueOf(ev.StartDateTime.month())+String.valueOf(ev.OwnerId).trim();
                String flag = key.Substring(0,key.Length()-3);
                if(flag != mp.V2_MpYearMonthUserId__c)
                {
                    continue;
                }
                if(EvIdMpdMap.containsKey(ev.WhoId))
                    {
                        MonthlyPlanDetail__c mpd = EvIdMpdMap.get(ev.WhoId);
                        mpd.ArrangedTimes__c +=1;
                        if(ev.Done__c && ev.V2_IsExpire__c != true)
                        {
                            mpd.Planned_Finished_Calls__c +=1;
                        }
                    }
                    else
                    {
                        MonthlyPlanDetail__c mpd = new MonthlyPlanDetail__c();
                        mpd.MonthlyPlan__c = mp.Id; 
                        mpd.Contact__c = ev.WhoId;
                        mpd.Account__c = ContactAccMap.get(ev.WhoId);
                        mpd.AdjustedTimes__c = 0;
                        mpd.ArrangedTimes__c = 1;
                        if(ev.Done__c && ev.V2_IsExpire__c != true)
                        {
                            mpd.Planned_Finished_Calls__c = 1;
                        }
                        else
                        {
                            mpd.Planned_Finished_Calls__c = 0;
                        }
                        EvIdMpdMap.put(ev.WhoId,mpd);
                    }
            }
        }
    }*/
    update updatempd;
    System.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@2  '+updatempd.size());
    insert EvIdMpdMap.values(); 
    System.debug('################################333  '+EvIdMpdMap.values().size());
    
    /*
        汇总关联市场活动的 拜访完成数，及回访拜访完成数
    */ 
   /*
   List<Campaign> updatecamlist = new List<Campaign>();
   List<Campaign> cas=[select V2_FinishedFollowEventNo__c,V2_FinishedAllEventNo__c,
                      (Select Done__c,V2_IsExpire__c,V2_FollowEventFlag__c From Events where SubjectType__c='拜访')
                      from Campaign where Id in:campaignids];
    for(Campaign cam:cas){
        //完成
        Integer finishEvent = 0;
        //追踪拜访完成
        Integer followEvent = 0;
        if(cam.Events.size()>0)
        {
            for(Event ev:cam.Events)
            {
                if(!ev.Done__c)
                {
                    continue;
                }
                if(ev.V2_IsExpire__c )
                {
                    continue;
                }
                finishEvent++;
                if(ev.V2_FollowEventFlag__c)
                {
                    followEvent++;
                }
            }
        }
        cam.V2_FinishedAllEventNo__c = finishEvent;
        cam.V2_FinishedFollowEventNo__c = followEvent;
        updatecamlist.add(cam);
    }
    update updatecamlist;
	*/
    if(campaignids.size()>0){
    	List<Campaign> cas=[select V2_FinishedFollowEventNo__c,V2_FinishedAllEventNo__c from Campaign where Id in:campaignids];
    	Map<Id, Campaign> casMap=new Map<Id, Campaign>(cas);
    	List<AggregateResult> ars_fe=[Select count(e.Id) cot_Id, e.WhatId From Event e
			where e.whatId in:campaignids
			and e.Done__c=true and e.V2_IsExpire__c=false
			GROUP BY e.WhatId];
    	List<AggregateResult> ars_fef=[Select count(e.Id) cot_Id, e.WhatId From Event e
			where e.whatId in:campaignids
			and e.Done__c=true and e.V2_IsExpire__c=false and e.V2_FollowEventFlag__c=true 
			GROUP BY e.WhatId];
    	
		for(AggregateResult ar: ars_fe){
			String cId=(String)ar.get('whatId');
			Integer finishEvent=(Integer)ar.get('cot_Id');
			Campaign cam=casMap.get(cId);
			if(cam != null){
				cam.V2_FinishedAllEventNo__c=finishEvent;
			}
		}
		for(AggregateResult ar: ars_fef){
			String cId=(String)ar.get('whatId');
			Integer followEvent=(Integer)ar.get('cot_Id');
			Campaign cam=casMap.get(cId);
			if(cam != null){
        	   cam.V2_FinishedFollowEventNo__c = followEvent;
			}
		}
		if(casMap.values().size()>0){
			update(casMap.values());
		}
    }
    /*
    Cognizant Update 2011-06-28 Add Logic
          计算月计划的KA拜访数，拜访质量评分，关键信息传递
    */
}