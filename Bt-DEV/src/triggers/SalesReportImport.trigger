/**
 * Author : Bill
 * date : 2013-8-3
 * 功能：当销售数据被导入后触发OpportunityReportDataBatch执行
**/
trigger SalesReportImport on SalesReport__c (after insert, after update) {
	//获取导入的数据是几月份
	integer curMonth = 0;
	for(SalesReport__c sr : trigger.new)
	{
		if(sr.ActualQty__c != null)
		{
			curMonth = ((curMonth>sr.Time__c.Month())?curMonth:sr.Time__c.Month());
		}
	}
	
	OpportunityReportDataBatch oppBatch = new OpportunityReportDataBatch();
	oppBatch.curMonth = curMonth;
    database.executeBatch(oppBatch,10); 
}