/* Tobe 
 * 2013.9.29
 * 新建自由行TP 到货日期 和 实际到货日期 如果为空 则用 希望到货日期 填充
 */
trigger PM_AutoSetDeliveryDate on PM_Free_TP_Application__c (before insert) 
{
	for(PM_Free_TP_Application__c freeTp : trigger.new)
	{
		if(freeTp.PM_DeliveryDate__c == null)
		{
			freeTp.PM_DeliveryDate__c = freeTp.PM_Hope_Date__c;
		}
		if(freeTp.PM_Actual_Delivery_Date__c == null)
		{
			freeTp.PM_Actual_Delivery_Date__c = freeTp.PM_Hope_Date__c;
		}
	}
}