/*****************************************************************************
 *Owner:AndyWang
 *Date:2012年9月14日 12:01:20
 *Function:
 *规则 create Product Syc HK PriceBoox
*****************************************************************************/ 
trigger V3_ProductSycCreatePriceBook on Product2 (after insert) {
	
	
	list<PricebookEntry> lisPricebookEntry = new list<PricebookEntry>();
	list<PricebookEntry> lisPricebookEntryHK = new list<PricebookEntry>();
	map<String,PricebookEntry> mapPricebookEntry = new map<String,PricebookEntry>();
	
	list<Product2> triggers = new list<Product2>();
	
	for(Product2 product:trigger.new){
		//只针对香港的产品；
		if(product.V3_Item_Number__c!=null){
			triggers.add(product);			
		}
	}
	list<Pricebook2> lisPricebook2IsStandard = [Select p.Name, p.IsStandard, p.IsDeleted, p.IsActive, p.Id, p.Description 
										From Pricebook2 p where p.IsStandard =:true];
	list<Pricebook2> lisPricebook2notIsStandard = [Select (Select Name From PricebookEntries),p.Name, p.IsStandard, p.IsDeleted, p.IsActive, p.Id, p.Description 
										From Pricebook2 p where Name =:'HK PriceBook'];
										
    //if HK PriceBook is null , throw a error
    if(lisPricebook2notIsStandard.size() == 0){
    	for(Product2 product:triggers){
			product.addError('Current products '+product.Name+'have no price book!');
		}
    	return;
    }
    //添加标准价格手册；										
	if(lisPricebook2IsStandard.size() > 0){
		for(Pricebook2 Pricebook:lisPricebook2IsStandard){
			for(Product2 Product :triggers){
				PricebookEntry pricebookEntry = new PricebookEntry();
				pricebookEntry.Product2Id = Product.id;
				pricebookEntry.Pricebook2Id = Pricebook.id;
				pricebookEntry.IsActive = true;
				pricebookEntry.UnitPrice = 0;
				lisPricebookEntry.add(pricebookEntry);
			}
		}
		if(lisPricebookEntry.size() > 0){
			insert lisPricebookEntry;
		}
	}
	//获取已经存在的product 
	if(lisPricebook2notIsStandard.size() > 0){
		for(Pricebook2 Pricebook:lisPricebook2notIsStandard){
			if(Pricebook.PricebookEntries.size() > 0){
				for(PricebookEntry pricebookentry:Pricebook.PricebookEntries){
					if(!mapPricebookEntry.containsKey(pricebookentry.Name)){
						mapPricebookEntry.put(pricebookentry.Name,pricebookentry);
					}
				}
			}
		}
	}
	//添加香港price book
	if(lisPricebook2notIsStandard.size() > 0){
		for(Pricebook2 Pricebook:lisPricebook2notIsStandard){
			for(Product2 Product :triggers){
				if(!mapPricebookEntry.containsKey(Product.Name)){
					PricebookEntry pricebookEntry = new PricebookEntry();
					pricebookEntry.Product2Id = Product.id;
					pricebookEntry.Pricebook2Id = Pricebook.id;
					pricebookEntry.IsActive = true;
					pricebookEntry.UseStandardPrice = true;
					pricebookEntry.UnitPrice = 0;
					lisPricebookEntryHK.add(pricebookEntry);
				}
			}
		}
		if(lisPricebookEntryHK.size() > 0){
			insert lisPricebookEntryHK;
		}
	}
}