/*
*功能：1）当TW拜訪過期警告（TW_VisitExpireWarning__c）字段的值为真并且事件的记录类型为“拜访”时，
*      向事件所有人发送一份电子邮件并把TW拜訪過期警告（TW_VisitExpireWarning__c）字段的值改为假
*      2）当事件所有人的角色是以”TW“开头，其记录类型为”拜访“且其字段已过期（V2_IsExpire__c）值为真时向事件
                            所有人发送一份电子邮件
*负责人：Alisa
*时间：2013年11月8日
*限制：最多可以一次发送10封电子邮件
*/

trigger TW_VisitExpireWarning on Event (before update) 
{
    set<Id> IdCon = new set<Id>(); //存放事件记录类型为‘拜访’时的事件中名称Id
    set<Id> IdUser = new set<Id>();//存放事件记录类型为‘拜访’时的事件所有人Id
    set<Id> IdUserRole = new set<Id>();//存放事件所有人的角色Id
    
    //获取事件记录类型为“拜访”的记录类型
    RecordType recordType = [Select r.Id,r.SobjectType, r.Name, r.DeveloperName 
                             From RecordType r 
                             where r.SobjectType = 'Event' and r.DeveloperName = 'V2_Event'];
                             
    
    for(Event e : trigger.new)
    {
    	//当记录类型为”拜访“时才把事件中的WhoId、OwnerId放入集合中
    	if(e.RecordTypeId == recordType.Id)
    	{
    		IdCon.add(e.WhoId);
    		IdUser.add(e.OwnerId);
    	}
    }
    
    //获取联系人
    map<Id,Contact> mapIdAndCon = new map<Id,Contact>([select Id,Name
    												    from Contact
    												    where Id IN :IdCon]);
    //获取用户
    map<Id,User> mapIdAndUser = new map<Id,User>([Select u.UserRoleId, u.Id 
    											  From User u
    											  where u.Id IN :IdUser]);												    
    												    
    
    for(Event newE : trigger.new)
    {
        Event oldE = trigger.oldMap.get(newE.Id);
        //实现功能 1）
        if(newE.RecordTypeId == recordType.Id && oldE.TW_VisitExpireWarning__c != newE.TW_VisitExpireWarning__c && newE.TW_VisitExpireWarning__c == true)
        {   
            if(mapIdAndCon != null && mapIdAndCon.size()>0)
            {
            	//调用方法prepareEmail发送电子邮件
            	prepareEmail(newE);
            	//发完邮件后把TW拜訪過期警告的值改为false
            	newE.TW_VisitExpireWarning__c = false;
            }
        }
         
         //获取用户角色的Id
         if(mapIdAndUser != null && mapIdAndUser.size()>0)
         {
         	IdUserRole.add(mapIdAndUser.get(newE.OwnerId).UserRoleId);
         }
         
    }
    
    //获取用户角色
    map<Id,UserRole> mapIdAndUserRole = new map<Id,UserRole>([Select u.Id,u.Name From UserRole u where u.Id IN :IdUserRole]);  
    
    for(Event newE : trigger.new)
    {
    	Event oldE = trigger.oldMap.get(newE.Id);
    	
    	if(mapIdAndUser !=null && mapIdAndUser.size()>0 && mapIdAndUserRole != null && mapIdAndUserRole.size()>0)
    	{
    		//获取事件所有人
    		User EventUser = mapIdAndUser.get(newE.OwnerId);
    		//获取事件所有人的角色
    		UserRole EventUserRole = mapIdAndUserRole.get(EventUser.UserRoleId);
    		//获取事件事件所有人的角色的名称
	    	String strUserRoleName = EventUserRole.Name; 
	    	
	    	
	    	 //实现功能 2）
	         if(strUserRoleName.startsWith('TW') == true && newE.RecordTypeId == recordType.Id && oldE.V2_IsExpire__c != newE.V2_IsExpire__c && newE.V2_IsExpire__c == true)
	         {
	         	prepareEmail2(newE);
	         }
    	}
    	
    }                      
    
    
    //用于功能 1）发送电子邮件
    public void prepareEmail(Event e)
    {
        
        String emailMessage = '您好'+UserInfo.getName()
                              +'<br/><br/>'
                              +'拜訪事件"'+e.Subject+mapIdAndCon.get(e.WhoId).Name+'"即將過期，請盡快完成這個拜訪，或者重新計劃這個拜訪。'
                              +'<br/><br/>'
                              +'點擊此鏈接以進入拜訪事件：'+URL.getSalesforceBaseUrl().toExternalForm() + '/' + e.id
                              +'<br/><br/>'
                              +'祝您工作愉快!'
                              +'<br/><br/><hr/>'
                              +'本郵件由Baxter Salesforce.com CRM系統產生，請勿回复。 '
                              +'<br/>'
                              +'如有任何疑問或者要求，請聯繫系統管理人員。';
                              
        if(e.OwnerId !=null)
        {
            //定义一个email对象用于发送单个邮件
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setSubject('行銷活動報名結束通知');
            mail.setHtmlBody(emailMessage); 
            mail.setTargetObjectId(e.OwnerId);
            mail.setCharset('UTF-8'); 
            mail.saveAsActivity=false;
            Messaging.sendEmail(new Messaging.Singleemailmessage[]{ mail });
        }
        
    }
    
    //用于功能2）发送电子邮件
    public void prepareEmail2(Event e)
    {
    	String emailMessage = '您好'+UserInfo.getName()
    						  +'<br/><br/>'
    						  +'您'+e.Subject+mapIdAndCon.get(e.WhoId).Name+'的拜訪事件已過期。'
    						  +'<br/><br/>'
    						  +'您可以點擊此鏈接進入該拜訪事件：'+URL.getSalesforceBaseUrl().toExternalForm() + '/' + e.id
    						  +'<br/><br/>'
                              +'祝您工作愉快!'
                              +'<br/><br/><hr/>'
                              +'本郵件由Baxter Salesforce.com CRM系統產生，請勿回复。 '
                              +'<br/>'
                              +'如有任何疑問或者要求，請聯繫系統管理人員。';
       if(e.OwnerId !=null)
        {
            //定义一个email对象用于发送单个邮件
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setSubject('拜訪事件過期提醒');
            mail.setHtmlBody(emailMessage); 
            mail.setTargetObjectId(e.OwnerId);
            mail.setCharset('UTF-8'); 
            mail.saveAsActivity=false;
            Messaging.sendEmail(new Messaging.Singleemailmessage[]{ mail });
        }                       
    }
    
}