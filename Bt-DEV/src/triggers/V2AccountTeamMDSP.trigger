/**
 * Author : Sunny
 * 功能：当销售医院关系新增或更新SP和MD类的销售最新负责人时，更新对象 MD医院信息和SP医院信息 上的
 * 当前所有人字段的值
 * 
**/
trigger V2AccountTeamMDSP on V2_Account_Team__c (after insert, after update) {
	
	//md医院信息
	Set<ID> set_md = new Set<ID>();
	Map<ID,ID> map_md = new Map<ID,ID>();
	//sp医院信息
	Set<ID> set_sp = new Set<ID>();
	Map<ID,ID> map_sp = new Map<ID,ID>();
	
	if(trigger.isInsert || trigger.isUpdate)
	{
		for(V2_Account_Team__c team : trigger.new)
		{
			if(team.V2_BatchOperate__c == '新增' || team.V2_BatchOperate__c == '替换' || team.V2_UserProduct__c == 'IVT')
			{
				set_md.add(team.V2_Account__c);
				map_md.put(team.V2_Account__c,team.V2_NewAccUser__c);
			}
			if(team.V2_BatchOperate__c == '新增' || team.V2_BatchOperate__c == '替换' || team.V2_UserProduct__c == 'SP')
			{
				set_sp.add(team.V2_Account__c);
				map_sp.put(team.V2_Account__c,team.V2_NewAccUser__c);
			}
		}
	}
	
	//当前年份
	string year = string.valueOf(date.today().year());
	if(set_md.size()>0)
	{
		//获取MD医院信息
		list<IVSHospitalInfo__c> list_ivt = [Select i.NowResponsibility__c, i.Account__c From IVSHospitalInfo__c i where i.Account__c in : set_md and Year__c = :year];
		for(IVSHospitalInfo__c ivt:list_ivt)
		{
			ivt.NowResponsibility__c = map_md.get(ivt.Account__c);
		}
		update list_ivt;
	}
	
	if(set_sp.size()>0)
	{
		//获取SP医院信息
		list<CCHospitalInfo__c> list_cc = [Select c.NowResponsibility__c, c.Account__c From CCHospitalInfo__c c where c.Account__c in : set_sp and Year__c = :year];
		for(CCHospitalInfo__c cc:list_cc)
		{
			cc.NowResponsibility__c = map_md.get(cc.Account__c);
		}
		update list_cc;
	}
	
}