/*
 *Scott
 *2013-10-21
 *客户小组审批后，同步数据到真正的客户小组。
 *仅供台湾地区使用
*/
trigger TW_AccountTeamSync on V2_Account_Team__c ( before update) {
    /*====================判断当前用户是否为台湾========================*/
    Profile CurrentProfile = [select Id,Name from Profile where Id =:UserInfo.getProfileId()];
    if(!CurrentProfile.Name.startsWith('TW'))
    {
        return;
    }
    /*====================判断当前用户是否为台湾========================*/
    //客户小组新增用户
    List<AccountTeamMember> Insert_AccTeam = new List<AccountTeamMember >();
    //客户小组替换用户
    List<AccountTeamMember> Update_AccTeam = new List<AccountTeamMember >();
    //客户小组移除用户
    List<AccountTeamMember> Delete_AccTeam = new List<AccountTeamMember >();
    Set<Id> Set_YCAccIds = new Set<Id>();//需移除用户的客户小组所属的客户
    
    
    if(trigger.isUpdate)
    {
        for(V2_Account_Team__c NewAccTeam : trigger.new)
        {
            V2_Account_Team__c OldAccTeam =trigger.oldMap.get(NewAccTeam.Id);
            if(NewAccTeam.V2_ApprovalStatus__c =='审批通过' && NewAccTeam.V2_ApprovalStatus__c != OldAccTeam.V2_ApprovalStatus__c)
            {
                if(NewAccTeam.V2_BatchOperate__c=='新增')
                {
                    AccountTeamMember atm = new AccountTeamMember();
                    atm.AccountId = NewAccTeam.V2_Account__c;
                    atm.UserId  =NewAccTeam.V2_User__c;
                    Insert_AccTeam.add(atm);
                    NewAccTeam.V2_IsSyn__c =true;
                }
                else if(NewAccTeam.V2_BatchOperate__c=='删除')
                {
                    Set_YCAccIds.add(NewAccTeam.V2_Account__c);
                    NewAccTeam.V2_IsSyn__c =true;
                    NewAccTeam.V2_History__c = true;
                }
                else if(NewAccTeam.V2_BatchOperate__c=='替换')
                {
                    Set_YCAccIds.add(NewAccTeam.V2_Account__c);
                    NewAccTeam.V2_IsSyn__c =true;
                    NewAccTeam.V2_History__c = true;
                    AccountTeamMember atm = new AccountTeamMember();
                    atm.AccountId = NewAccTeam.V2_Account__c;
                    atm.UserId  =NewAccTeam.V2_NewAccUser__c;
                    Insert_AccTeam.add(atm);
                    
                }
            }
        }
    }
    /*//替换
    if(Set_THAccIds.size()>0)
    {
        for(AccountTeamMember AccTeamMember : [select UserId from AccountTeamMember where AccountId in:Set_THAccIds])
        {
            for(V2_Account_Team__c AccTea : trigger.new)
            {
                if(AccTea.V2_ApprovalStatus__c!='审批通过')
                {
                    continue;
                }
                if(AccTea.V2_BatchOperate__c=='替换')
                {
                    AccTeamMember.UserId = AccTea.V2_NewAccUser__c;
                    Update_AccTeam.add(AccTeamMember);
                }
            }
        }
        update Update_AccTeam;
    }*/
    //删除&替换
    if(Set_YCAccIds.size()>0)
    {
        //如果是替换的话还需再创建一条新增的记录
        List<V2_Account_Team__c> Insert_Acc_Team = new List<V2_Account_Team__c>();
        for(AccountTeamMember AccTeamMember : [select UserId,AccountId from AccountTeamMember where AccountId in:Set_YCAccIds])
        {
            for(V2_Account_Team__c AccTea : trigger.new)
            {
                if(AccTea.V2_ApprovalStatus__c!='审批通过')
                {
                    continue;
                }
                if((AccTea.V2_BatchOperate__c=='删除' || AccTea.V2_BatchOperate__c=='替换') && AccTea.V2_User__c == AccTeamMember.UserId)
                {
                    Delete_AccTeam.add(AccTeamMember);
                    
                    
                    //如果是替换的话还需再创建一条新增的记录
                    if(AccTea.V2_BatchOperate__c=='替换')
                    {
                        V2_Account_Team__c objAccountTeam = new V2_Account_Team__c();
                        objAccountTeam.V2_Account__c = AccTea.V2_Account__c ;
                        objAccountTeam.V2_NewAccUser__c = AccTea.V2_NewAccUser__c;
                        objAccountTeam.V2_User__c = AccTea.V2_NewAccUser__c;
                        objAccountTeam.V2_BatchOperate__c='新增';
                        objAccountTeam.V2_ApprovalStatus__c='审批通过';
                        objAccountTeam.ownerid=AccTea.V2_NewAccUser__c;
                        Insert_Acc_Team.add(objAccountTeam) ;
                    }
                }
                
            }
        }
        delete Delete_AccTeam;
        insert Insert_Acc_Team;
    }
    //新增
    if(Insert_AccTeam.size()>0)
    {
        insert Insert_AccTeam;
    }
}