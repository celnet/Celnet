/*
作者: Tommy
创建时间: 2011-12-2
功能描述: 
如果是拜访类型的事件，需要对事件执行以下自动化事件任务 
1，创建周期性事件时清除子事件的访前计划和访后分析（除了第一个子事件之外）。
2，创建拜访事件是自动命名主题为：拜访
3，事件7天过期，与7天过期的工作流相互配合，在工作流将事件设置为过期时监测是否已经完成
约束: 
只有拜访类型的事件应用本过程
风险：
本代码依赖了picklist 的选项值的中文值，有翻译的潜在风险
2012-2-24修改：用于导航功能：拜访是否已过期：计算3天后将过期的拜访数量。若〉0则提示“您有x个拜访将在3天后过期”，拜访开始日期+7 <= 当日+3，点击确定后跳转到“过期视图”
                                       添加标记日期字段，得出过期日期（开始日期+4天）用于做‘3天过期视图’。
2013-12-10 Sunny : 事件字段清除，去掉有关V2_3DaysExpiredFlag__c的引用
*/
trigger V2_Event_SetVisit on Event (before insert, before update) 
{
    final String C_VisitTypeName = '拜访';
    
    //清除周期事件子事件的访前计划和访后分析字段的值（除了第一个子事件之外）
    if(trigger.isInsert)
    {
        Boolean isAllSubRucurrenceEvent = true;//先假设本批次的插入是对周期事件的子事件的插入，本批中所有的事件都是子事件，不包括父事（ev.IsRecurrence == true）件和单事件（ev.RecurrenceActivityId == null）
        for(Event ev : trigger.new )
        {
            if(ev.SubjectType__c != C_VisitTypeName)
            {
                continue;
            }
            /***************2012-2-24新加***********************/
            /*2013-12-10 commented by Sunny
            if(ev.StartDateTime != null)
            {
                ev.V2_3DaysExpiredFlag__c = ev.StartDateTime.addDays(4);
            }
            */
            /***************2012-2-24新加***********************/
            if(ev.IsRecurrence == true || ev.RecurrenceActivityId == null)//排除单事件，和循环事件的父事件
            {
                isAllSubRucurrenceEvent = false;
            }
        }
        if(isAllSubRucurrenceEvent)//如果确认是循环事件的子事件的插入
        {
            for(Integer i = 1; i < trigger.new.size(); i ++)//第一个子事件不需要清除
            {
                Event ev = trigger.new[i];
                ev.GAPlan__c = null;
                ev.GAExecuteResult__c = null;
            }
        }
    }
    
    //创建拜访事件是自动命名主题为：拜访 [联系人]
    //2011/12/5：发现Salesforce标准的功能在日历中显示事件主题后会自动加[联系人]，所以修改主题为“拜访 ”
    if(trigger.isInsert)
    {
        for(Event ev : trigger.new)
        {
            if(ev.SubjectType__c != C_VisitTypeName)
            {
                continue;
            }
            if(ev.SubjectType__c == C_VisitTypeName)
            {
                //如果是追踪拜访则 主题 不用默认值 自动创建追踪拜访时 会自动赋值
                if(!ev.V2_FollowEventFlag__c)
                {
//2013-4-25 sunny 此处对拜访的主题通过验证规则进行检验。
                    //ev.Subject = C_VisitTypeName;
                }
                
            }
        }
    }
    //事件7天过期
    if(trigger.isUpdate)
    {
        for(Event ev : trigger.new)
        {
            if(ev.SubjectType__c != C_VisitTypeName)
            {
                continue;
            }
            /***************2012-2-24新加***********************/
            /*2013-12-10 commented by Sunny
            Event oldev = trigger.oldMap.get(ev.Id);
            if(ev.StartDateTime != oldev.StartDateTime)
            {
                if(ev.StartDateTime != null)
                {
                    ev.V2_3DaysExpiredFlag__c = ev.StartDateTime.addDays(4);
                }
                //if(ev.StartDateTime.addDays(7)<= dateTime.now() && ev.Done__c == false)
                //{
                    //ev.V2_IsExpire__c = true;
                //}
            }
            */
            /***************2012-2-24新加***********************/
            /*commented by Ken 2013-03-08:
            @Description: This function was overrided by the other trigger named V21_Event
            Event oldEvent  = trigger.oldMap.get(ev.Id);
            if(ev.V2_IsExpire__c != oldEvent.V2_IsExpire__c && ev.V2_IsExpire__c == true)
            {   
                if(ev.Done__c == true)
                {
                    ev.V2_IsExpire__c = false;
                }
            }
            */
        }
    }
    
    
    /*暂时取消这个功能
    //创建事件如果没有当月的月计划则通知用户创建当月的月计划，用户可以忽略这个通知，继续创建事件
    if(trigger.isInsert)
    {   
        Set<String> set_year = new Set<String>();
        Set<String> set_month = new Set<String>();
        for(Event ev : trigger.new)
        {
            if(ev.StartDateTime != null)
            {
                DateTime start = ev.StartDateTime;
                String year = String.valueOf(start.year());
                set_year.add(year);
                String month = String.valueOf(start.month());
                set_month.add(month);
            }
        }
        Set<String> set_ExistMonthPlan = new Set<String>();
        for(MonthlyPlan__c mp : [Select Id, Year__c, Month__c From MonthlyPlan__c Where Year__c In: set_year And Month__c In: set_month])
        {
            set_ExistMonthPlan.add(mp.Year__c + mp.Month__c);
        }
        for(Event ev : trigger.new)
        {
            if(ev.SubjectType__c != C_VisitTypeName)
            {
                continue;
            }
            if(ev.V2_IgnoreExistMonthlyPlan__c)
            {
                continue;
            }
            if(ev.StartDateTime == null)
            {
                continue;
            }
            DateTime start = ev.StartDateTime;
            String year = String.valueOf(start.year());
            String month = String.valueOf(start.month());
            if(!set_ExistMonthPlan.contains(year + month))
            {
                ev.addError('系统中部目前不存在您将要创建拜访对应月份的月计划，您可以勾选“忽略检测月计划的错误”来忽略这个提示，并重新保存');
            }
        }
        
    }*/
}