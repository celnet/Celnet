/**
 * Author:Bill
 * date : 2013-8-1
 * project ： 焦点业务机会
 * 1.当业务机会的焦点业务机会变为true时，对应的客户也变为焦点业务机会医院
 * 2.当业务机会变为非焦点时，查看对应的客户是否还有焦点业务机会，若没有取消焦点业务机会医院标示
 **/
trigger AutoOppFocusChangedAccount on Opportunity (after update) {
    //变为焦点的业务机会
    Set<ID> set_Focus = new Set<ID>();
    //变为非焦点的业务机会
    Set<ID> set_NotFocus = new Set<ID>();
    //焦点业务机会ID
    Map<ID,ID> map_FocusOpp = new Map<ID,ID>();
    //BQ业务机会记录类型
    for(Opportunity opp : trigger.new)
    {
        if(opp.IsFocusOpportunity__c == '是')
        {
            set_Focus.add(opp.AccountId);
            map_FocusOpp.put(opp.AccountId, opp.Id);
        }
        if(opp.IsFocusOpportunity__c != '是' && !set_Focus.contains(opp.AccountId))
        {
            set_NotFocus.add(opp.AccountId);
        }
    }
    
    //把有焦点业务机会的医院设置为焦点业务机会医院标示
    if(set_Focus != null && set_Focus.size() > 0)
    {
        List<Account> list_acc = [Select a.FocusOpportunitiesHospital__c From Account a where Id in : set_Focus];
        for(Account acc : list_acc)
        {
            acc.FocusOpportunitiesHospital__c = true;
            acc.FocusOpportunitie__c = map_FocusOpp.get(acc.Id);
        }
        update list_acc;
    }
    
    //取消业务机会的客户是否还有其他业务机会，若没有则取消焦点业务机会医院标示
    if(set_NotFocus != null && set_NotFocus.size() > 0)
    {
        List<Opportunity> list_opp = [Select o.IsFocusOpportunity__c, o.AccountId From Opportunity o where AccountId in : set_NotFocus and IsFocusOpportunity__c = '是'];
        if(list_opp != null && list_opp.size() > 0)
        {
            for(Opportunity opp : list_opp)
            {
                set_NotFocus.remove(opp.AccountId);
            }
        }
    }
    if(set_NotFocus != null && set_NotFocus.size() > 0)
    {
        List<Account> list_accNot = [Select a.FocusOpportunitiesHospital__c From Account a where Id in : set_NotFocus];
        for(Account acc : list_accNot)
        {
            acc.FocusOpportunitiesHospital__c = false;
            acc.FocusOpportunitie__c = NULL;
        }
        update list_accNot;
    }
}