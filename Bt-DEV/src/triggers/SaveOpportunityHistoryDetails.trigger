/*
*开发人：Storm Yang
*时间：2013-02-18
*模块：业务机会历史
*编号：
*功能描述：
业务机会历史数据的保存：目前系统中未将业务机会的变化情况存为历史数据，为了保证KPI数据计算的准确性，
建议保存业务机会历史(当所有人变化时产生该数据)，历史数据保存以下信息：业务机会原所有人，业务机会新所有人，
变更时间，原所有人产品，新所有人产品，医院，原业务机会阶段，新业务机会阶段；
2013-2-22 Sunny:将before update改为after update
*注意事项：
*开发经验：
*/
trigger SaveOpportunityHistoryDetails on  Opportunity(after update) {
    
    //需记录的字段：Id,Owner,Stage,Account
    //去查询的字段：当前时间(变更时间), 原所有人产品,新所有人产品
    //产品信息取自用户角色，用户角色信息符合格式：GBU-Level-Territory-Product Group-Title，则Product Group部分为其产品信息
    Map<Id,RecordType> map_RecordType = new Map<Id,RecordType>([select Id,Name from RecordType where (DeveloperName='RENAL' or 
                                                                DeveloperName='IVT_Approval' or DeveloperName='IVT' or DeveloperName='ACC_Supervisor' 
                                                                or DeveloperName='ACC') And SobjectType = 'Opportunity']);
    Set<Id> set_Ids = new Set<Id>();
    Set<Id> set_RoleIds =  new Set<Id>();
    Set<Id> set_OldUserIds =  new Set<Id>();
    Set<Id> set_OldRoleIds =  new Set<Id>();
    List<OpportunityHistory__c> oppHistory = new List<OpportunityHistory__c>();
    //Opportunity修改前的所有Owner
    for(Opportunity opp : Trigger.New){
            set_Ids.Add(opp.OwnerId);           
    }
    //Opportunity修改后的所有Owner
    for(Opportunity opp : Trigger.Old){
            set_OldUserIds.Add(opp.OwnerId);            
    }
    system.debug('triggers map key set'+trigger.newMap.keySet() + ' - Record type: '+map_RecordType.keySet());
    Map<Id,Opportunity> map_Opp = new Map<Id,Opportunity>([Select Id,OwnerId,StageName,AccountId
                                   from Opportunity  where id IN:trigger.newMap.keySet() and RecordTypeId IN:map_RecordType.keySet()]);
    //Opportunity修改前的所有Owner所对应的User Map集合
    Map<Id,User> map_User = new Map<Id,User>([select Id,UserRoleId from User where Id IN:set_Ids]);
    //Opportunity修改后的所有Owner所对应的User Map集合
    Map<Id,User> map_OldUser = new Map<Id,User>([select Id,UserRoleId from User where Id IN:set_OldUserIds]);
    for(User u:map_User.values())
    {
        //Opportunity修改后的所有Owner所对应的Role ID集合
        set_RoleIds.add(u.UserRoleId);
    }
    for(User u:map_OldUser.values())
    {
        //Opportunity修改前的所有Owner所对应的Role ID集合
        set_OldRoleIds.add(u.UserRoleId);
    }
    //Opportunity修改后的所有Owner所对应的Role Map集合
    Map<Id,UserRole> map_Role= new Map<Id,UserRole>([select Id,Name from UserRole where Id IN:set_RoleIds]);
    //Opportunity修改前的所有Owner所对应的Role Map集合
    Map<Id,UserRole> map_OldRole= new Map<Id,UserRole>([select Id,Name from UserRole where Id IN:set_OldRoleIds]);
    system.debug('Opp Values Map:'+map_Opp);
    for(Opportunity opp:map_Opp.values())
    {
        //当Owner不同或Stage变化时记录
        Id oldOwnerId =  trigger.oldMap.get(opp.Id).OwnerId;
        String strOldStage = trigger.oldMap.get(opp.Id).StageName;
        system.debug('New Opp:'+opp+' - '+oldOwnerId+' - '+strOldStage);
        if(opp.OwnerId != oldOwnerId || opp.StageName != strOldStage)
        {   
                String strNewRoleName = '';
                String strOldRoleName = '';
                Id newRoleId =null;
                Id oldRoleId =null;
                if(map_User.get(opp.OwnerId) != null)
                    newRoleId = map_User.get(opp.OwnerId).UserRoleId;
                if(map_OldUser.get( trigger.oldMap.get(opp.Id).OwnerId) != null)
                    oldRoleId= map_OldUser.get( trigger.oldMap.get(opp.Id).OwnerId).UserRoleId;
                if(newRoleId != null)
                    strNewRoleName =  map_Role.get(newRoleId).Name;
                if(oldRoleId != null)
                    strOldRoleName = map_OldRole.get(oldRoleId).Name;
                
                OpportunityHistory__c oppHis = new OpportunityHistory__c();
                oppHis.ChangedDate__c = datetime.now();
                oppHis.Opportunity__c = opp.Id;
                oppHis.Account__c = opp.AccountId;
                oppHis.NewOppStage__c = opp.StageName;
                oppHis.PastOppStage__c = strOldStage;
                oppHis.NewOwner__c = opp.OwnerId;
                oppHis.LastOwner__c = oldOwnerId;
                if(strNewRoleName != null && strNewRoleName != '')
                    if(strNewRoleName.split('-').size()>4)
                        oppHis.NewOwnerProduct__c = strNewRoleName.split('-')[3];
                if(strOldRoleName != null && strOldRoleName != '')
                    if( strOldRoleName.split('-').size()>4)
                        oppHis.LastOwnerProduct__c = strOldRoleName.split('-')[3];
                oppHistory.add(oppHis);
        }
    }
    system.debug('history list:'+oppHistory);
    if(oppHistory.size()>0)
        insert oppHistory;
}