/**
 * Author:Sunny
 * 1.创建挥发罐时，如果医院是空，并且挥发罐的状态是库存的话，产生一条历史
 * 2.Amanda&工程师编辑状态是需要记录到挥发罐状态历史中的
 * bill 添加功能 2013/6/20
 * 1.挥发罐的序列号不可重复
 * 2.选择了联系人以后，自动将联系人的医院赋值到医院字段
 * 2013-7-15 Sunny 修改。当挥发罐当前状态是使用时，自动将使用状态改为使用中。
**/
trigger VaporizerStatusHistory on VaporizerInfo__c (before insert, before update, after insert, after update) {

    List<Vaporizer_Status_History__c> list_vsh = new List<Vaporizer_Status_History__c>();
    
    /*******************bill add 2013/6/20 START***********************************/
    Set<string> set_vapNames = new Set<string>();
    Set<Id> contactIds = new Set<Id>();
    
    if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate))
    {
        for(VaporizerInfo__c vap:trigger.new)
        {
            set_vapNames.add(vap.Name);
            if(trigger.isInsert && vap.Status__c=='使用' && vap.UsedStatus__c == null){
                vap.UsedStatus__c='使用中';
            }else if(trigger.isUpdate && vap.Status__c == '使用' && trigger.oldMap.get(vap.Id).Status__c != '使用'){
                vap.UsedStatus__c='使用中';
            }
        }
    }
    if(set_vapNames.size()>0)
    {
    List<VaporizerInfo__c> List_vapInsert = [Select v.Name, v.Id From VaporizerInfo__c v where v.Name in : set_vapNames limit 1000];
    
    //若是List_vapInsert长度大于零，则存在重复序列号
    if(List_vapInsert.size()>0)
    {
        if(trigger.isBefore && trigger.isInsert)
        {
           for(VaporizerInfo__c vap:trigger.new)
           {
              if(set_vapNames.contains(vap.Name)){
                    vap.addError('挥发罐的序列号不可重复');
                }
           }
        }
        if(trigger.isBefore && trigger.isUpdate)
        {
           for(VaporizerInfo__c vap:trigger.new)
           {
              if(vap.Name != trigger.oldMap.get(vap.Id).Name){
                 if(set_vapNames.contains(vap.Name)){
                      vap.addError('挥发罐的序列号不可重复');
                 }
              }else{
                /******************bill 2013-7-4 start*****************************/
                //挥发罐状态使用变为其它时，要把使用状态置空
                if(trigger.oldMap.get(vap.Id).Status__c == '使用' && trigger.oldMap.get(vap.Id).Status__c != vap.Status__c)
                {
                    vap.UsedStatus__c = '';
                }
                /******************bill 2013-7-4 end********************************/
                if(vap.Contact__c != null){
                    contactIds.add(vap.Contact__c);
                }
                /******************bill 2013-7-22 start*****************************/
                //如果挥发罐变为使用状态，就把所有人赋值给当前负责销售
                if(vap.Status__c == '使用')
                {
                    vap.CurrentSales__c = vap.OwnerId;
                }
                else if(vap.Status__c != '使用')
                {
                    vap.CurrentSales__c = null;
                }
                /******************bill 2013-7-22 end********************************/
              }
           }
        }
    }else{
        if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate))
        {
           for(VaporizerInfo__c vap:trigger.new)
           {
              if(vap.Contact__c != null){                   
                    contactIds.add(vap.Contact__c);                   
                }
           }
        }
    }
    }
    
    if(contactIds.size()>0){
         List<Contact> contactList = [select Id,AccountId,Phone,MobilePhone from contact where Id in:contactIds limit 1000];
         Map<Id,Contact> contactAccountMap = new Map<Id,Contact>();
         for(Contact con:contactList)
         {
             contactAccountMap.put(con.Id,con);
         }
         for(VaporizerInfo__c vap : trigger.new){
             if(vap.Contact__c != null){
                  vap.Hospital__c = contactAccountMap.get(vap.Contact__c).AccountId;
                 }
        }
    }
    /********************bill add 2013/6/20 END**********************************/
    
    if(trigger.isAfter && trigger.isInsert)
    {
        for(VaporizerInfo__c vap:trigger.new)
        {
            if(vap.Hospital__c == null && vap.Status__c == '库存'){
                Vaporizer_Status_History__c vsh = new Vaporizer_Status_History__c();
                vsh.Date__c = Date.today();
                vsh.VaporizerInfo__c = vap.Id;
                vsh.NewStatus__c = vap.Status__c;
                vsh.NewLocation__c = vap.location__c;
                vsh.ActionType__c ='资产录入';
                list_vsh.add(vsh);
            }
        }
        insert list_vsh;
    }
    if(trigger.isAfter && trigger.isUpdate){
        /*List<UserRole> URole = [Select Id,Name From UserRole Where Id =: UserInfo.getUserRoleId()];
        system.debug(URole[0].Name.toUpperCase()+'dota');
        if(URole.size() <= 0){
            return;
        }
        Boolean isAssOrEng = false;
        if(URole[0].Name.toUpperCase().contains('ASSISTANT') || URole[0].Name.toUpperCase().contains('VAPORIZER') || system.Test.isRunningTest()){
            isAssOrEng = true;
        }
        if(isAssOrEng){*/
        for(VaporizerInfo__c vap:trigger.new){
            VaporizerInfo__c oldVap = trigger.oldMap.get(vap.Id);
            if(vap.location__c == oldVap.location__c && vap.Status__c == oldVap.Status__c && vap.Hospital__c == oldVap.Hospital__c){
                continue;
            }
            Vaporizer_Status_History__c vsh = new Vaporizer_Status_History__c();
            vsh.Date__c = Date.today();
            vsh.VaporizerInfo__c = vap.Id;
            vsh.OldStatus__c = oldVap.Status__c;
            vsh.NewStatus__c = vap.Status__c;
            vsh.OldLocation__c = oldVap.location__c;
            vsh.NewLocation__c = vap.location__c;
            vsh.NewHospatal__c = vap.Hospital__c;
            vsh.OldHospotal__c = oldVap.Hospital__c;
            if(oldVap.Status__c != '使用' && vap.Status__c == '使用'){
                vsh.ActionType__c = '挥发罐申请';
            }else if(oldVap.Status__c != '报废' && vap.Status__c == '报废'){
                vsh.ActionType__c = '挥发罐报废';
            }else if(oldVap.Status__c != '维修' && vap.Status__c == '维修'){
                vsh.ActionType__c = '挥发罐维修';
            }else if(oldVap.Status__c == '使用' && vap.Status__c != '使用'){
                vsh.ActionType__c = '挥发罐退回';
            }else if(oldVap.Status__c == '维修' && vap.Status__c != '维修'){
                vsh.ActionType__c = '挥发罐维修返库';
            }else if(oldVap.Status__c == '报废' && vap.Status__c != '报废'){
                vsh.ActionType__c = '挥发罐报废恢复';
            }else{
                if(oldVap.location__c == '上海仓库' && vap.location__c.contains('维修中心')){
                    vsh.ActionType__c = '挥发罐维修';
                }
            }
            
            list_vsh.add(vsh);
        }
        insert list_vsh;
        //}
    }

}