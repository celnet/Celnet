/*
Author：Dean Lv
Created on：2013-09-09
Description: 
监听七氟烷实际销量（瓶）、七氟烷潜力销量（瓶）、七氟烷客户、地氟烷实际销量（瓶）、
地氟烷潜力销量（瓶）、地氟烷客户等级等六个字段。当六个字段中任意有一个值被修改后，
记录每一次修改前的值以及修改后的数值。
Alter by: Dean lv
Alter on：2013-09-22
Description: 
Rvtadmin用户可以在页面直接修改联系人信息，而不用提交修改人申请，所以要在页面进行before update 
Trigger操作。
添加功能为：当修改页面数值时，对于七氟烷实际销量（瓶）、七氟烷潜力销量（瓶）、七氟烷客户、
地氟烷实际销量（瓶）、地氟烷潜力销量（瓶）、地氟烷客户等级等六个字段进行公示计算赋值。
*/
trigger SP_Getold_Contact_Mod on Contact(before update, after update) {
	Set<Contact_Mod__c> ContactMod = new Set<Contact_Mod__c>();
	//获得所操作记录类型
    //ID SpId = [Select r.Id, r.DeveloperName From RecordType r where SobjectType = 'Contact' and IsActive = true and DeveloperName = 'SP'][0].Id;

	if(trigger.isAfter)
	{
		List<Contact> list_QtyContact = new List<Contact>();
		List<Contact> list_ProContact = new List<Contact>();
		List<Contact> list_LevelContact = new List<Contact>();
		List<Contact> list_DesQtyContact = new List<Contact>();
		List<Contact> list_DesProContact = new List<Contact>();
		List<Contact> list_DesLevelContact = new List<Contact>();
		for(Contact newcm : trigger.new)
		{
		   if(newcm.SP_SevoQtySale__c !=null && trigger.oldMap.get(newcm.Id).SP_SevoQtySale__c != null && setDou(setDou(newcm.SP_SevoQtySale__c)) != setDou(trigger.oldMap.get(newcm.Id).SP_SevoQtySale__c)) 
			{
				list_QtyContact.add(newcm);		
			}			
			if(newcm.SP_SevoAccLevel__c !=null && trigger.oldMap.get(newcm.Id).SP_SevoAccLevel__c != null && newcm.SP_SevoAccLevel__c != trigger.oldMap.get(newcm.Id).SP_SevoAccLevel__c) 
			{
				list_LevelContact.add(newcm);
			}
			if(newcm.SP_SevoPotentialSale__c !=null && trigger.oldMap.get(newcm.Id).SP_SevoPotentialSale__c != null && newcm.SP_SevoPotentialSale__c != trigger.oldMap.get(newcm.Id).SP_SevoPotentialSale__c) 
			{
				list_ProContact.add(newcm);
			}
			if(newcm.SP_DesQtySale__c !=null && trigger.oldMap.get(newcm.Id).SP_DesQtySale__c != null && newcm.SP_DesQtySale__c != trigger.oldMap.get(newcm.Id).SP_DesQtySale__c) 
			{
				list_DesQtyContact.add(newcm);		
			}			
			if(newcm.SP_DesAccLevel__c !=null && trigger.oldMap.get(newcm.Id).SP_DesAccLevel__c != null && newcm.SP_DesAccLevel__c != trigger.oldMap.get(newcm.Id).SP_DesAccLevel__c) 
			{
				list_DesLevelContact.add(newcm);
			}
			if(newcm.SP_DesPotentialSale__c !=null && trigger.oldMap.get(newcm.Id).SP_DesPotentialSale__c != null && newcm.SP_DesPotentialSale__c != trigger.oldMap.get(newcm.Id).SP_DesPotentialSale__c) 
			{
				list_DesProContact.add(newcm);
			}		
		}
		
		//记录ID
		//bill添加   这个trigger的SOQL和DML操作太多  减少
		Map<String,ID> map_RdId = new Map<String,ID>();
		for(RecordType rd : [Select r.Id, r.DeveloperName From RecordType r where SobjectType = 'SP_Contact_Grade__c' and IsActive = true])
		{
			map_RdId.put(rd.DeveloperName,rd.Id);
		}
		
		List<SP_Contact_Grade__c> list_Grade = new List<SP_Contact_Grade__c>();
		
		if(list_QtyContact != null && list_QtyContact.size()>0)
		{
			for(Contact con : list_QtyContact)
			{
				SP_Contact_Grade__c spQty = new SP_Contact_Grade__c();
				//记录类型
				spQty.RecordTypeId = map_RdId.get('SP_SevoQtySaleHis'); 
				//七氟烷实际销量
				spQty.SP_OldSevoQtySale__c = trigger.oldMap.get(con.Id).SP_SevoQtySale__c;
				spQty.Sp_SevoQtySale__c = con.Sp_SevoQtySale__c;
				//联系人
				spQty.SP_Contact__c = con.Id;
				//操作者
				spQty.SP_Approve__c = con.SP_Approve__c;
				//时间
				spQty.SP_Modificationdate__c=Date.Today();
				list_Grade.ADD(spQty);
			}
		}
		if(list_LevelContact != null && list_LevelContact.size()>0)
		{
			for(Contact con : list_LevelContact)
			{
				SP_Contact_Grade__c spLev = new SP_Contact_Grade__c();
				//记录类型
				spLev.RecordTypeId = map_RdId.get('SP_SevoAccLevelHis'); 
				//七氟烷客户等级
				spLev.SP_OldSevoAccLevel__c = trigger.oldMap.get(con.Id).SP_SevoAccLevel__c;
				spLev.SP_SevoAccLevel__c = con.SP_SevoAccLevel__c;
				//联系人
				spLev.SP_Contact__c	= con.Id;
				//操作人
				spLev.SP_Approve__c = con.SP_Approve__c;
				//时间
				spLev.SP_Modificationdate__c=Date.Today();
				//等级变化原因
				spLev.SP_SevoAccLevelChangeReason__c = '';
				if(trigger.newMap.get(con.Id).SP_AnesthesiaMonthyNum__c != trigger.oldMap.get(con.Id).SP_AnesthesiaMonthyNum__c)
				{
					spLev.SP_SevoAccLevelChangeReason__c += '月全麻手术/台从' + string.valueOf(trigger.oldMap.get(con.Id).SP_AnesthesiaMonthyNum__c==null?'无':string.valueOf(trigger.oldMap.get(con.Id).SP_AnesthesiaMonthyNum__c))
															+ '变为' + string.valueOf(trigger.newMap.get(con.Id).SP_AnesthesiaMonthyNum__c)+';  ';
				}
				if(trigger.newMap.get(con.Id).SP_Job__c != trigger.oldMap.get(con.Id).SP_Job__c)
				{
					spLev.SP_SevoAccLevelChangeReason__c += '联系人的职务(SP)从' + string.valueOf(trigger.oldMap.get(con.Id).SP_Job__c==null?'无':trigger.oldMap.get(con.Id).SP_Job__c)
															+ '变为' + string.valueOf(trigger.newMap.get(con.Id).SP_Job__c)+';';
				}
				//spLev.SP_SevoAccLevelChangeReason__c
				list_Grade.ADD(spLev);
			}
		}
		if(list_ProContact != null && list_ProContact.size()>0)
		{
			//监听保存七氟烷潜力销量历史记录
			for(Contact con : list_ProContact)
			{
				SP_Contact_Grade__c spPro = new SP_Contact_Grade__c();
				//记录类型
				spPro.RecordTypeId = map_RdId.get('SP_SevoPotentialSaleHis'); 
				//七氟烷潜力销量
				spPro.SP_OldSevoPotentialSale__c = trigger.oldMap.get(con.Id).SP_SevoPotentialSale__c;
				spPro.SP_SevoPotentialSale__c = con.SP_SevoPotentialSale__c;
				//联系人
				spPro.SP_Contact__c	= con.Id;
				//操作者
				spPro.SP_Approve__c = con.SP_Approve__c;
				//时间
				spPro.SP_Modificationdate__c=Date.Today();
				list_Grade.ADD(spPro);
			}
		}
		
		if(list_DesQtyContact != null && list_DesQtyContact.size()>0)
		{
			//监听保存地氟烷实际销量历史记录
			for(Contact con : list_DesQtyContact)
			{
				SP_Contact_Grade__c spQty = new SP_Contact_Grade__c();
				//记录类型
				spQty.RecordTypeId = map_RdId.get('SP_DesQtySaleHis'); 
				//地氟烷实际销量
				spQty.SP_OldSevoQtySale__c = trigger.oldMap.get(con.Id).SP_DesQtySale__c;
				spQty.Sp_SevoQtySale__c = con.SP_DesQtySale__c;
				//联系人
				spQty.SP_Contact__c = con.Id;
				//操作者
				spQty.SP_Approve__c = con.SP_Approve__c;
				//时间
				spQty.SP_Modificationdate__c=Date.Today();
				list_Grade.ADD(spQty);
			}
		}
		if(list_DesLevelContact != null && list_DesLevelContact.size()>0)
		{
			//监听保存地氟烷客户等级历史记录
			for(Contact con : list_DesLevelContact)
			{
				SP_Contact_Grade__c spLev = new SP_Contact_Grade__c();
				//记录类型
				spLev.RecordTypeId = map_RdId.get('SP_DesAccLevelHis'); 
				//地氟烷客户等级
				spLev.SP_OldSevoAccLevel__c = trigger.oldMap.get(con.Id).SP_DesAccLevel__c;
				spLev.SP_SevoAccLevel__c = con.SP_DesAccLevel__c;
				//联系人
				spLev.SP_Contact__c	= con.Id;
				//操作人
				spLev.SP_Approve__c = con.SP_Approve__c;
				//时间
				spLev.SP_Modificationdate__c=Date.Today();
				//等级变化原因
				spLev.SP_SevoAccLevelChangeReason__c = '';
				if(trigger.newMap.get(con.Id).SP_AnesthesiaMonthyNum__c != trigger.oldMap.get(con.Id).SP_AnesthesiaMonthyNum__c)
				{
					spLev.SP_SevoAccLevelChangeReason__c += '月全麻手术/台从' + string.valueOf(trigger.oldMap.get(con.Id).SP_AnesthesiaMonthyNum__c==null?'无':string.valueOf(trigger.oldMap.get(con.Id).SP_AnesthesiaMonthyNum__c))
															+ '变为' + string.valueOf(trigger.newMap.get(con.Id).SP_AnesthesiaMonthyNum__c)+';  ';
				}
				if(trigger.newMap.get(con.Id).SP_Job__c != trigger.oldMap.get(con.Id).SP_Job__c)
				{
					spLev.SP_SevoAccLevelChangeReason__c += '联系人的职务(SP)从' + string.valueOf(trigger.oldMap.get(con.Id).SP_Job__c==null?'无':trigger.oldMap.get(con.Id).SP_Job__c)
															+ '变为' + string.valueOf(trigger.newMap.get(con.Id).SP_Job__c)+';';
	
		
				}
				list_Grade.ADD(spLev);
			}
		}
		if(list_DesProContact != null && list_DesProContact.size()>0)
		{
			//监听保存地氟烷潜力销量历史记录
			for(Contact con : list_DesProContact)
			{
				SP_Contact_Grade__c spPro = new SP_Contact_Grade__c();
				//记录类型
				spPro.RecordTypeId = map_RdId.get('SP_DesPotentialSaleHis'); 
				//地氟烷潜力销量
				spPro.SP_OldSevoPotentialSale__c = trigger.oldMap.get(con.Id).SP_DesPotentialSale__c;
				spPro.SP_SevoPotentialSale__c = con.SP_DesPotentialSale__c;
				//联系人
				spPro.SP_Contact__c	= con.Id;
				//操作者
				spPro.SP_Approve__c = con.SP_Approve__c;
				//时间
				spPro.SP_Modificationdate__c=Date.Today();
				list_Grade.ADD(spPro);
			}
		}
		
		//生成历史记录
		if(list_Grade != null && list_Grade.size()>0)
		{
			insert list_Grade;
		}
	}
	
	if(trigger.isBefore && trigger.isUpdate)
	{	
		Set<ID> set_conIds = new Set<ID>();
		for(Contact con : trigger.new)
		{
			set_conIds.add(con.Id);
		}
		String proName = [Select p.Name, p.Id From Profile p where id = : UserInfo.getProfileId()][0].Name;
		Map<ID,ID> map_Mods = new Map<ID,ID>();
		for(Contact conmod : [Select c.Id, (Select OwnerId, Name__c, CreatedDate From Contacts__r Order By CreatedDate DESC limit 1) From Contact c where Id IN : set_conIds])
		{
			if(conmod.Contacts__r != null && conmod.Contacts__r.size()>0)
			{
				map_Mods.put(conmod.Id, conmod.Contacts__r[0].OwnerId);
			}
		}
		for(Contact con:trigger.new)
		{
			if(proName.toUpperCase().contains('IVT ADMIN'))
			{
				con.SP_Approve__c = UserInfo.getUserId();
			}
			else if(map_Mods.containsKey(con.Id))
			{
				con.SP_Approve__c = map_Mods.get(con.Id);
			}
			//七氟烷实际销量（瓶）公式：月全麻数量/台*月吸入比例%*平均手术时长h*七氟烷浓度*七氟烷FGF (L/min)*3.3/250
			con.SP_SevoQtySale__c = setDou(
			(con.SP_AnesthesiaMonthyNum__c == null?0:con.SP_AnesthesiaMonthyNum__c)*
			((con.SP_InhaleMonthyNum__c == null?0:con.SP_InhaleMonthyNum__c)/100)*
			(con.SP_SevoConcentration__c == null?0:con.SP_SevoConcentration__c)*
			(con.SP_OperationTime__c == null?0:con.SP_OperationTime__c)*
			(con.SP_SevoFGF__c == null?0:con.SP_SevoFGF__c)*3.3/250);
			//七氟烷潜力销量（瓶）：自动计算，计算公式：=月全麻数量/台*3*2*2*3.3/250
			con.SP_SevoPotentialSale__c = setDou((con.SP_AnesthesiaMonthyNum__c == null?0:con.SP_AnesthesiaMonthyNum__c)*3*2*2*3.3/250);
			//七氟烷等级计算
			double sale = con.SP_SevoPotentialSale__c;
			string job = con.SP_Job__c;
			con.SP_SevoAccLevel__c = AccountLevelExec(sale, job);
			
			//地氟烷实际销量
			con.SP_DesQtySale__c = setDou(
			(con.SP_AnesthesiaMonthyNum__c == null?0:con.SP_AnesthesiaMonthyNum__c)*
			((con.SP_InhaleMonthyNum__c == null?0:con.SP_InhaleMonthyNum__c)/100)*
			(con.SP_DesConcentration__c == null?0:con.SP_DesConcentration__c)*
			(con.SP_OperationTime__c == null?0:con.SP_OperationTime__c)*
			(con.SP_DesFGF__c == null?0:con.SP_DesFGF__c)*3.3/240);
			//地氟烷潜力销量
			con.SP_DesPotentialSale__c = setDou((con.SP_AnesthesiaMonthyNum__c == null?0:con.SP_AnesthesiaMonthyNum__c)*3*2*6.5*3.3/240);
			double saleDes = con.SP_DesPotentialSale__c;
			//地氟烷客户等级
			con.SP_DesAccLevel__c = AccountDesLevelExec(saleDes, job);
			
		}
	}
	
	//SP客户等级计算  七氟烷
	//销量	联系人职务
	//七氟烷潜力销量	主任/副主任	带组医生	住院医师	领/加药人员	采购	    库管	其他职务
	//销量>=5	       V	    A	    A	    C	    D	    D	     A
	//5>销量>=1	       V	    B	    B	    C	    D	    D	     B
	//1>销量>  0	       V  	    B	    C	    C	    D	    D	     C
	//销量=0	           V	    B	    C	    C	    D	    D	          其他
	public String AccountLevelExec(Double SevoPotentialSale,String job)
	{
		string level;
		if(job == '主任' || job == '副主任')
		{
			return 'V';
		}
		else if(job == '带组医生')
		{
			if(SevoPotentialSale >= 5)
			{
				return 'A';
			}
			return 'B';
		}
		else if(job == '住院医师')
		{
			if(SevoPotentialSale >= 5)
			{
				return 'A';
			}
			else if(SevoPotentialSale < 5 && SevoPotentialSale >= 1)
			{
				return 'B';
			}
			return 'C';
		}
		else if(job == '领/加药人员')
		{
			return 'C';
		}
		else if(job == '采购' || job == '库管')
		{
			return 'D';
		}else
		{
			if(SevoPotentialSale >= 5)
			{
				return 'A';
			}
			else if(SevoPotentialSale < 5 && SevoPotentialSale >= 1)
			{
				return 'B';
			}
			else if(SevoPotentialSale < 1 && SevoPotentialSale > 0)
			{
				return 'C';
			}
			return '其他';
		}
	}
	
	//SP客户等级计算   地氟烷
	//销量	联系人职务
	//地氟烷潜力销量	主任/副主任	带组医生	住院医师	领/加药人员	采购	    库管	其他职务
	//销量>=18	       V	    A	    A	    C	    D	    D	     A
	//18>销量>=1	       V	    B	    B	    C	    D	    D	     B
	//1>销量>  0	       V  	    B	    C	    C	    D	    D	     C
	//销量=0	           V	    B	    C	    C	    D	    D	          其他
	public String AccountDesLevelExec(Double SevoPotentialSale,String job)
	{
		string level;
		if(job == '主任' || job == '副主任')
		{
			return 'V';
		}
		else if(job == '带组医生')
		{
			if(SevoPotentialSale >= 18)
			{
				return 'A';
			}
			return 'B';
		}
		else if(job == '住院医师')
		{
			if(SevoPotentialSale >= 18)
			{
				return 'A';
			}
			else if(SevoPotentialSale < 18 && SevoPotentialSale >= 1)
			{
				return 'B';
			}
			return 'C';
		}
		else if(job == '领/加药人员')
		{
			return 'C';
		}
		else if(job == '采购' || job == '库管')
		{
			return 'D';
		}else
		{
			if(SevoPotentialSale >= 18)
			{
				return 'A';
			}
			else if(SevoPotentialSale < 18 && SevoPotentialSale >= 1)
			{
				return 'B';
			}
			else if(SevoPotentialSale < 1 && SevoPotentialSale > 0)
			{
				return 'C';
			}
			return '其他';
		}
	}
	
	public Decimal setDou(Decimal n)
	{
		Decimal decAfter = n.setScale(1, System.RoundingMode.HALF_UP);
		return decAfter;
	}
}