/**
*Function:<br> 
*Sync the related information, such as setup the fields from the related Opportunity, etc...
*/
trigger V2_Trg_OE_SyncFields on OppEvaluation__c (before insert) {
    
    if(Trigger.isInsert && Trigger.isBefore){
        Set<String> opportunitySet=new Set<String>();
        Set<String> beReviewedUserIdSet=new Set<String>();//被点评人用户
        
        for(OppEvaluation__c oe:Trigger.new){
            if(oe.BeCommentUser__c!=null){
                beReviewedUserIdSet.add(oe.BeCommentUser__c);
            }
            if(oe.Opportunity__c!=null){
                opportunitySet.add(oe.Opportunity__c);
            }
        }
        Map<String, User> usersMap=new Map<String, User>(
                [Select id, userRoleId, UserRole.Name From User Where id in: beReviewedUserIdSet]
            );
            
        Map<String, Opportunity> opportunitiesMap=new Map<String,Opportunity>(
                [Select Id, Name, AccountId, StageName, isFocusOpportunity__c
                From Opportunity Where Id in: opportunitySet]
            );
		
        for(OppEvaluation__c oe:Trigger.new){
            //设置被点评人相关信息
            if(oe.BeCommentUser__c!=null){
                User beReviewedUser=usersMap.get(oe.BeCommentUser__c);
                oe.userRoleName__c=beReviewedUser.UserRole.name;
            }
            //设置业务机会相关信息
            if(oe.Opportunity__c!=null){
                Opportunity opportunity=opportunitiesMap.get(oe.Opportunity__c);
                if(opportunity!=null){
                    oe.opportunityStageName__c=opportunity.StageName;
                    oe.isFocusOpportunity__c=opportunity.isFocusOpportunity__c;
                }
            }
        }
    }
}