/**
 * Author:Sunny
 * 1.当业务机会所有人改变时，若次业务机会为焦点业务机会，则需要看改变后的所有人是否有其他业务机会，如果有，把当前业务机会的焦点业务机会设置为否。
 **/
trigger OpportunitySetFocus on Opportunity (before update) {
    if(trigger.isUpdate && trigger.isBefore){
        List<ID> list_oppOwnerIds = new List<ID>();
        Set<ID> set_oid = new Set<ID>();
        for(Opportunity opp : trigger.new)
        {
            if(opp.OwnerId != trigger.oldMap.get(opp.Id).OwnerId && opp.IsFocusOpportunity__c == '是'){
                list_oppOwnerIds.add(opp.OwnerId);
            }
        }
        if(list_oppOwnerIds.size() > 0 ){
            for(Opportunity opp : [Select Id,IsFocusOpportunity__c,OwnerId From Opportunity Where OwnerId in: list_oppOwnerIds And IsFocusOpportunity__c='是']){
                set_oid.add(opp.OwnerId);
            }
        }
        if(set_oid.size() > 0){
            for(Opportunity opp : trigger.new){
                if(set_oid.contains(opp.OwnerId)){
                    opp.IsFocusOpportunity__c = '否';
                }
            }
        }
    }
}