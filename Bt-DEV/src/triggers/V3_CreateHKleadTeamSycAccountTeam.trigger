/*****************************************************************************
 *Owner:AndyWang
 *Date:2012年9月11日 09:34:49
 *Function:
 *规则 create HKlead Syc update Account
*****************************************************************************/ 
trigger V3_CreateHKleadTeamSycAccountTeam on HK_Leads_Team__c (before insert,before delete,before update) {

    list<HK_Leads_Team__c> 	lstTriggers;
	if(trigger.isDelete){
		lstTriggers=trigger.old;
	} else {
		lstTriggers=trigger.new;
	}
	
	
			//获取LeadId集合	
			Map<String,String> mapLeadMid = new Map<String,String>();
			Map<String,String> mapUserName = new Map<String,String>();
			
			Set<ID> SetHKLeadId = new Set<ID>();
			Set<String> SetLeadMid = new Set<String>();
			Set<ID> SetUserId = new set<ID>();
			for(HK_Leads_Team__c HK_Leads_Team:lstTriggers){
				SetHKLeadId.add(HK_Leads_Team.HK_Leads__c);
				SetUserId.add(HK_Leads_Team.Team_Member__c);
			}
			for(HK_Leads__c lstHKlead: [select id, Address_Num__c from HK_Leads__c where id in:SetHKLeadId]){
				 setLeadMid.add(lstHKlead.Address_Num__c);
				 mapLeadMid.put(lstHKlead.id,lstHKlead.Address_Num__c);
			}
			for(User user: [select id, name from User where id in:SetUserId]){
				 
				 mapUserName.put(user.id,user.name);
			}
			
			
		System.debug('-----------'+SetLeadMid);

	//验证是否存在；
	if(trigger.isInsert){
			map<Id,HK_Leads_Team__c> mapHKLeadsTeam = new map<Id,HK_Leads_Team__c>();
			if(SetHKLeadId.size() > 0){
				list<HK_Leads_Team__c> listHKLeads =[Select Id,  Name,  Team_Member__c From HK_Leads_Team__c where HK_Leads__c IN:SetHKLeadId];
					for(HK_Leads_Team__c HKLeads:listHKLeads){
						if(!mapHKLeadsTeam.containsKey(HKLeads.Team_Member__c)){
									mapHKLeadsTeam.put(HKLeads.Team_Member__c,HKLeads);
						}
					}
				
					for(HK_Leads_Team__c HK_Leads_Team:trigger.new){
						if(mapHKLeadsTeam.containsKey(HK_Leads_Team.Team_Member__c)){
							HK_Leads_Team.addError('HKLeadsTeam Member Already exists!');
							return;
						}
					}
			}
			
		}
	//验证是否是下属
	if(Trigger.isInsert || trigger.isUpdate){
	    Set<ID> set_UserIds = new Set<ID>() ;
		V2_UtilClass clsUtilClass = new V2_UtilClass() ;
		List<ID> list_UserId = new List<ID>() ;
		list_UserId = clsUtilClass.getSubordinateIds(UserInfo.getUserRoleId()) ;
		if(list_UserId != null && list_UserId.size() != 0)
		{
			set_UserIds.addAll(list_UserId) ;
		}
		for(HK_Leads_Team__c HK_Leads_Team:trigger.new){
			if(!set_UserIds.contains(HK_Leads_Team.Team_Member__c))
			{
				//blnErrors = true ;
				HK_Leads_Team.addError('You can only edit your Subordinate.') ;
				return;
			}
			
		}

	}
	
	 if(trigger.isDelete||trigger.isUpdate){
	 		Set<ID> SetOldUserId = new set<ID>();
			for(HK_Leads_Team__c HK_Leads_Team:trigger.old){
				
				SetOldUserId.add(HK_Leads_Team.Team_Member__c);
			}
	 	
	 	//删除旧的Account Team
		list<AccountTeamMember> listAccountTeamMember_Del = new list<AccountTeamMember>();
		list<Account> listAccount_Del =new list<Account>();
		
		listAccount_Del =[SELECT Id, Mid__c,
		(SELECT AccountId,Id,UserId FROM AccountTeamMembers where UserId in:SetOldUserId) FROM Account where Mid__c in:SetLeadMid];
		for(Account acc:listAccount_Del){
			for(AccountTeamMember accT:acc.AccountTeamMembers){
				listAccountTeamMember_Del.add(accT);
			}
		}
		
		 if(listAccountTeamMember_Del.size()>0)
			 delete  listAccountTeamMember_Del;
	 	
	 }
  		 
		 
		if(Trigger.isInsert || trigger.isUpdate){
		    RecordType HKRecordType = [Select r.Name, r.Id From RecordType r where Name ='HK' 
		    and isactive=true and SobjectType='Opportunity'];

			//添加Account team
			   list<Opportunity> lisOpportunity = new List<Opportunity>(); //需要插入的opp
			    list<AccountTeamMember> listAccountTeamMember = new list<AccountTeamMember>();//需要插入的accountmember
			    
			    //获取lead对应的account
			    Map<String,Account>mapAccount = new Map<String,Account>();
			    //获取lead对应的Opportunity
			    
			    Map<String,Opportunity>mapOpportunity = new Map<String,Opportunity>();
			    
				list<Account>  listAccount = [Select (Select Id, OwnerId,AccountId From Opportunities),
				id, MID__c,name from Account where MID__c IN:SetLeadMid];
				for(Account account1:listAccount){
					
					mapAccount.put(account1.MID__c,account1);
					for(Opportunity opp:account1.Opportunities){
						mapOpportunity.put(account1.Id+'@'+opp.OwnerId,opp);
					}
				}
				
				for(HK_Leads_Team__c HKLeadsTeam:trigger.new){
					Account acc;
					if(mapAccount.ContainsKey(mapLeadMid.get(HKLeadsTeam.HK_Leads__c)))
					    acc=mapAccount.get(mapLeadMid.get(HKLeadsTeam.HK_Leads__c));
					if(acc==null){
						HKLeadsTeam.addError('Can not find account by address no:'+mapLeadMid.get(HKLeadsTeam.HK_Leads__c));
						return;
					}
					System.debug('------------'+acc.Id);
						AccountTeamMember accountteammember = new AccountTeamMember();
						accountteammember.AccountId = acc.Id;
						accountteammember.UserId = HKLeadsTeam.Team_Member__c;
						listAccountTeamMember.add(accountteammember);
						//如果没有Opportunity,则创建一个
						if(!mapOpportunity.ContainsKey(acc.id+'@'+HKLeadsTeam.Team_Member__c)){
							Opportunity Opportuniti = new Opportunity();
							Opportuniti.Name = acc.Name+' : '+mapUserName.get(HKLeadsTeam.Team_Member__c);
							Opportuniti.AccountId = acc.id;
							Opportuniti.OwnerId = HKLeadsTeam.Team_Member__c;
							Opportuniti.CloseDate = Date.Today();
							Opportuniti.StageName = 'close';
							Opportuniti.RecordTypeId = HKRecordType.Id;
							lisOpportunity.add(Opportuniti);
							
						}
				}
							if(lisOpportunity.size()>0){
								insert lisOpportunity;
							}
							if(listAccountTeamMember.size() > 0){
								insert listAccountTeamMember;
							}
				
			   
			
		}
	
		 
  
}