/*
*Author：Dean Lv
*Date：2013
*Function：根据客户下业务省份维护客户大区信息
*          插入客户信息大区为空时根据业务省份获取大区信息		
*		        插入客户信息大区为空时且业务省份为空时根据城市信息获取大区信息
*          更新客户信息时根据业务省份校准大区信息
*          更新客户信息时若省份为空根据城市维护省份信息
*/
trigger PM_AccountRegionReplace on Account (before insert, before update) 
{
	//存储更改的业务省份ID
	Set<String> SetProId = new Set<String>();
	//存储更改的业务城市ID
	Set<String> SetCityId = new Set<String>();
	//存储业务省份与大区的关系
	Map<String,String> Map_ProAndBig = new Map<String,String>();
	//存储城市与大区的关系
	Map<String,String> Map_CityAndBig = new Map<String,String>();
	//存储城市与省份的关系
	Map<String,String> Map_CityAndPro = new Map<String,String>();
	/*************************获取数据的业务省份以及城市信息******************************/
	for(Account accountbefore :trigger.new)
	{
		SetProId.add(accountbefore.PM_Province__c);
		SetCityId.add(accountbefore.Cities__c);
	}
	/*************************获取数据的业务省份以及城市信息******************************/
	/*************************根据业务省份获取大区信息******************************/
	if(SetProId.size() > 0 && SetProId != null)
	{
		for(PM_Province__c pro : [Select a.Id , a.PM_Region__c 
		From PM_Province__c a where a.Id IN: SetProId])
		{ 
			Map_ProAndBig.put(pro.Id , pro.PM_Region__c);
			System.debug('###########################'+Map_ProAndBig.get(pro.Id));
		}
	}
	/*************************根据业务省份获取大区信息******************************/
	/*************************根据城市获取省份信息以及大区信息******************************/
	if(SetCityId.size() > 0 && SetCityId != null)
	{
		for(Cities__c cit : [Select a.Id , a.BelongToProvince__c ,
		a.BelongToProvince__r.Region__c From Cities__c a where 
		a.Id IN: SetCityId])
		{
			Map_CityAndBig.put(cit.Id,cit.BelongToProvince__r.Region__c);
			Map_CityAndPro.put(cit.Id,cit.BelongToProvince__c);
			System.debug('###########################'+Map_CityAndBig.get(cit.Id));
			System.debug('###########################'+Map_CityAndPro.get(cit.Id));
			if(!Map_ProAndBig.containsKey(cit.BelongToProvince__c))
			{
				Map_ProAndBig.put(cit.BelongToProvince__c , cit.BelongToProvince__r.Region__c);
			}
		}
	}
	/*************************根据城市获取省份信息以及大区信息******************************/
	/*************************插入客户信息大区为空时根据业务省份获取大区信息******************************/
	if(trigger.isInsert)
	{
		for(Account accountbefore: trigger.new)
		{
			if(accountbefore.Region__c == null)
			{
				if(accountbefore.Provinces__c == null)
				{
					if(Map_CityAndPro.containsKey(accountbefore.Cities__c))
					accountbefore.Provinces__c = Map_CityAndPro.get(accountbefore.Cities__c);
				}
				if(accountbefore.PM_Province__c != null)
				{
					if(Map_ProAndBig.containsKey(accountbefore.PM_Province__c))
					System.debug('###########################'+Map_ProAndBig.get(accountbefore.Provinces__c));
					accountbefore.Region__c = Map_ProAndBig.get(accountbefore.PM_Province__c);
				}
			}
			
		}
	}
	/************************插入客户信息大区为空时根据业务省份获取大区信息*******************************/
	/************************更新客户信息时根据业务省份校准大区信息*******************************/
	if(trigger.isUpdate)
	{
		for(Account accountbefore: trigger.new)
		{
				if(accountbefore.Provinces__c == null)
				{
					if(Map_CityAndPro.containsKey(accountbefore.Cities__c))
					accountbefore.Provinces__c = Map_CityAndPro.get(accountbefore.Cities__c);
				}
				if(accountbefore.PM_Province__c != null) 
				{
					if(Map_ProAndBig.containsKey(accountbefore.PM_Province__c))
					System.debug('###########################'+Map_ProAndBig.get(accountbefore.PM_Province__c));
					accountbefore.Region__c = Map_ProAndBig.get(accountbefore.PM_Province__c);
				}
		}
	}
	/************************更新客户信息时根据业务省份校准大区信息*******************************/
}