/**
 * Author : bill
 * Date : 2013-7-26
 * 功能：当拜访添加附件后邮件通知上级，及时查看签收单
**/
trigger AttachmentAddMessage on Attachment (before update, after insert, after update) {
	
	//附件上传用户
	//Set<ID> set_User = new Set<ID>();
	//for(Attachment att : trigger.new)
	//{
	//	set_User.add(att.OwnerId);
	//}
	
	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    String repBody = '';
    repBody += '本邮件由Baxter Salesforce.com CRM系统产生，请勿回复。<br>'; 
    repBody += '如有任何疑问或者要求，请联系系统管理人员。<br>';
    String[] repAddress =new string[]{'371602633@qq.com'};
    mail.setToAddresses(repAddress);
    mail.setHtmlBody(repBody);
    mail.setSubject('xxxx上传附件，请查看');
    mail.setSenderDisplayName('Salesforce');
    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
}