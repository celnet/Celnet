/**
 * Author : Sunny
 * 自动汇总客户下的使用的挥发罐个数
**/
trigger VaporizerInfoAutoSumQty on VaporizerInfo__c (after update) {
    if(trigger.isUpdate){
    	List<ID> list_AccountId = new List<ID>();
    	for(VaporizerInfo__c VapInfo : trigger.new){
    		if(VapInfo.Hospital__c != trigger.oldMap.get(VapInfo.Id).Hospital__c){
    			if(VapInfo.Hospital__c != null){
    				list_AccountId.add(VapInfo.Hospital__c);
    			}
    			if(trigger.oldMap.get(VapInfo.Id).Hospital__c != null){
                    list_AccountId.add(trigger.oldMap.get(VapInfo.Id).Hospital__c);
                }
    		}
    	}
    	if(list_AccountId.size() > 0){
    		List<Account> list_AccUp = new List<Account>();
    		for(Account Acc : [Select Id,Vaporizer_Qty__c,(Select Id From VaporizerHospital__r)  From Account Where Id in: list_AccountId]){
    			Acc.Vaporizer_Qty__c = Acc.VaporizerHospital__r.size();
    			list_AccUp.add(Acc);
    		}
    		if(list_AccUp.size() > 0){
    			update list_AccUp;
    		}
    	}
    }
}