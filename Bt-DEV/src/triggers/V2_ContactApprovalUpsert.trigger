/*
Author：Scott
Created on：2011-12-28
Description: 
1.联系人修改申请提交后，审批通过需向联系人同步
2.如果是新增则在客户下insert一个数据 新增联系人owner为客户Owner
3.如果是编辑则更新此联系人
注意
如果新增修改内容相应的也在查询联系人sql中查询
2012-4-4修改：
1.原有的职务给MD用 联系人上新增 2职务 Bios 和Renal 用新的字段  ，MD部门职务为必填。
2.联系人上新增两个联系人类型：Bios：联系人类型（bios）、Md: ivt和is 用的联系人类型（IVT），acc或者sp 用的联系人类型（ACC）
2012-4-17修改：
3个部门联系人上‘部门’字段分开使用。
2013-3-5
sunny:添加同步字段“GIS客户类型”
2013-3-6
sunny:修改联系人记录类型赋值方式。
2013-4-7
sunny:添加同步字段“静疗小组”
2013-9-9
过滤掉记录类型为SP增加和SP修改
*/
trigger V2_ContactApprovalUpsert on Contact_Mod__c (after update) {
    //因为可能会出现 针对一个联系人有多条修改审批通过，如果用list去update/insert 或报异常Duplicate id in list。所以用Set去重。
    Set<Contact> insertContact = new Set<Contact>();
    Set<Contact> updateContact = new Set<Contact>();
    //联系人修改申请ids
    Set<Id> contactmods =  new Set<Id>();
    //联系人ids
    Set<Id> contactids = new Set<Id>();
    
    /**********************BILL ADD START****************************/
    //记录类型ID
    Set<ID> set_record = new Set<ID>();
    list<RecordType> list_record = [Select r.DeveloperName, r.Id From RecordType r where r.DeveloperName in ('SP_Insert','SP_Update') and IsActive = true and r.SobjectType = 'Contact_Mod__c'];
    if(list_record != null && list_record.size() > 0)
    {
        for(RecordType record:list_record)
        {
            set_record.add(record.Id);
        }
    }
    /**********************BILL ADD end****************************/
    
    if(trigger.isUpdate)
    {
        for(Contact_Mod__c newcm:trigger.new)
        {
            if(newcm.Status__c != '通过' )
            {
                continue;
            }
            if(newcm.TW_DevName__c == 'BQ_New' || newcm.TW_DevName__c =='BQ_Update')
            {
            	continue;
            }
            if(!set_record.contains(newcm.RecordTypeId))
            {
                contactmods.add(newcm.Id);
            }
            if(newcm.Type__c == '编辑' && newcm.Name__c != null)
            {
                contactids.add(newcm.Name__c);
            }
        }
    }
    //1Renal 2MD 3Bios
    //List<RecordType> Contactrtlist = [select Id from RecordType where SobjectType='Contact' order by DeveloperName];
    Map<String , ID> map_rt = new Map<String , ID>();
    for(RecordType rt : [select Id,DeveloperName,Name from RecordType where SobjectType='Contact' order by DeveloperName]){
        map_rt.put(rt.Name.toUpperCase() , rt.Id);
    }
    
    
    Map<Id,Contact> ConMap = new Map<Id,Contact>();
    for(Contact co:[select V2_RenalDepartmentType__c,V2_BiosDepartmentType__c,V2_ContactTypeByIVT__c,V2_ContactTypeByACC__c,V2_BiosTitle__c,V2_RenalTitle__c,Name,Id,LastName,DepartmentType__c,ContactType__c,Title,Gender__c,Birthdate,ID_card__c,V2_PassPortNo__c,
                      V2_OfficerNo__c,Phone,MobilePhone,Fax,Email,GraduateCollege__c,Description,V2_RenalGrade__c,V2_CampaignType__c,GISContactType__c,ElectrostaticTherapy__c,
                      V2_TotalScore__c,V2_RateHospGrade__c,V2_RateGeology__c,V2_RateLeadership__c,V2_RateTitle__c,V2_Education__c,
                      V2_RateBaxRelationship__c,V2_RatePatientDeliver__c,V2_RatePDPatients__c,V2_RateExperience__c,V2_RatePDImpPatients__c,
                      V2_RateBeds__c,V2_RateBaxBusiness__c,V2_RatePDCenter__c,interest__c from Contact where id in:contactids])
    {
        ConMap.put(co.Id,co);
    }
    
    
    for(Contact_Mod__c cm:[select V2_OwnerProfile__c,Name__c,Account__c,Account__r.OwnerId,RecordType.DeveloperName,Type__c,NewContact__c,Department_Type__c,Contact_Type__c,
                           Phone__c,Mobile__c,Fax__c,Email__c,Graduate_College__c,Title__c,
                           Gender__c,Birthday__c,ID_card2__c,V2_PassPortNo__c,V2_OfficerNo__c,
                           Comment__c,GISContactType__c,ElectrostaticTherapy__c,
                           //权重
                           V2_RenalGrade__c,V2_CampaignType__c,V2_TotalScore__c,V2_RateHospGrade__c,
                           V2_RateGeology__c,V2_RateLeadership__c,V2_RateTitle__c,V2_Education__c,V2_RateBaxRelationship__c,
                           V2_RatePatientDeliver__c,V2_RatePDPatients__c,V2_RateExperience__c,V2_RatePDImpPatients__c,V2_RateBeds__c,
                           V2_RateBaxBusiness__c,V2_RatePDCenter__c,
                           //md 
                           V2_ContactTypeByRENAL__c,V2_Level__c,V2_Relationship_with_Baxter__c,V2_Position_in_the_Society__c,
                           V2_Status_of_Academy__c,V2_RateTech__c,V2_interest__c 
                           from Contact_Mod__c where id in:contactmods])
    {
        if(cm.Type__c =='新增')
        {
            Contact contact = new Contact();
            InsertOrUpdateContact(cm,contact);
        }
        else if(cm.Type__c =='编辑' &&  cm.Name__c != null)
        {
            if(ConMap.containsKey(cm.Name__c))
            {
                Contact contact = ConMap.get(cm.Name__c);
                InsertOrUpdateContact(cm,contact);
            }
        }
        
    }
    
    
    
    
    //新增或编辑
    public void InsertOrUpdateContact(Contact_Mod__c curContact,Contact contact)
    {
        try
        {
            System.debug('######################################进入方法');
            
            if(curContact.Type__c =='新增')
            {
                //renal
                if(curContact.RecordType.DeveloperName.contains('Renal') && map_rt.containsKey('RENAL'))
                {
                    //contact.RecordTypeId = Contactrtlist[0].Id;
                    contact.RecordTypeId = map_rt.get('RENAL');
                }
                //Md
                else if(curContact.RecordType.DeveloperName.contains('MD') && map_rt.containsKey('MD'))
                {
                    //contact.RecordTypeId = Contactrtlist[1].Id;
                    contact.RecordTypeId = map_rt.get('MD');
                }
                //bios
                else if(curContact.RecordType.DeveloperName.contains('Bios') && map_rt.containsKey('BIOS'))
                {
                    //contact.RecordTypeId = Contactrtlist[2].Id;
                    contact.RecordTypeId = map_rt.get('BIOS');
                }
                //所有人
                contact.OwnerId = '00520000000rm6x';
            }
            //客户
            contact.AccountId = curContact.Account__c;
            
            /////////////////基本信息；
            //姓名
            contact.LastName=curContact.NewContact__c;
            
            //联系人类别
            contact.ContactType__c=curContact.Contact_Type__c ;
            //部门
            //职务
            //联系人类型
            if(curContact.RecordType.DeveloperName.contains('Renal'))
            {
                contact.V2_RenalTitle__c=curContact.Title__c;
                //部门
                contact.V2_RenalDepartmentType__c=curContact.Department_Type__c;
            }
            else if(curContact.RecordType.DeveloperName.contains('Bios'))
            {
                //部门
                contact.V2_BiosDepartmentType__c=curContact.Department_Type__c;
                //职务
                contact.V2_BiosTitle__c=curContact.Title__c;
                //类型
                contact.V2_ContactTypeByRENAL__c=curContact.V2_ContactTypeByRENAL__c;
            }
            else if(curContact.RecordType.DeveloperName.contains('MD'))
            {
                //部门
                contact.DepartmentType__c=curContact.Department_Type__c;
                //职务
                contact.Title=curContact.Title__c;
                //类型
                String profileName = curContact.V2_OwnerProfile__c.toUpperCase();
                //Md: ivt和is 用的联系人类型（IVT）
                if(profileName.Contains('IVT') || profileName.Contains('IS'))
                {
                    contact.V2_ContactTypeByIVT__c=curContact.V2_ContactTypeByRENAL__c;
                }
                //acc或者sp 用的联系人类型（ACC）
                else
                {
                    contact.V2_ContactTypeByACC__c=curContact.V2_ContactTypeByRENAL__c;
                }
            }
            
            //性别
            contact.Gender__c=curContact.Gender__c;
            //生日
            contact.Birthdate=curContact.Birthday__c;
            //id card
            contact.ID_card__c=curContact.ID_card2__c;
            //护照
            contact.V2_PassPortNo__c=curContact.V2_PassPortNo__c;
            //军官
            contact.V2_OfficerNo__c=curContact.V2_OfficerNo__c;
            //phone
            contact.Phone=curContact.Phone__c;
            //
            contact.MobilePhone=curContact.Mobile__c;
            contact.Fax=curContact.Fax__c;
            contact.Email=curContact.Email__c;
            contact.GraduateCollege__c=curContact.Graduate_College__c;
            //备注        
            contact.Description=curContact.Comment__c;
            //兴趣爱好
            contact.interest__c = curContact.V2_interest__c;
            //GIS客户类型
            contact.GISContactType__c = curContact.GISContactType__c;
            //静疗小组
            contact.ElectrostaticTherapy__c = curContact.ElectrostaticTherapy__c;
            
            /////////////////其他信息；
            contact.V2_RenalGrade__c =curContact.V2_RenalGrade__c;
            contact.V2_CampaignType__c =curContact.V2_CampaignType__c;
            contact.V2_TotalScore__c= curContact.V2_TotalScore__c;
            contact.V2_RateHospGrade__c =curContact.V2_RateHospGrade__c;
            contact.V2_RateGeology__c =curContact.V2_RateGeology__c;
            contact.V2_RateLeadership__c= curContact.V2_RateLeadership__c; 
            contact.V2_RateTitle__c =curContact.V2_RateTitle__c;
            contact.V2_Education__c =curContact.V2_Education__c;
            contact.V2_RateBaxRelationship__c =curContact.V2_RateBaxRelationship__c;
            contact.V2_RatePatientDeliver__c = curContact.V2_RatePatientDeliver__c;
            contact.V2_RatePDPatients__c =curContact.V2_RatePDPatients__c;
            contact.V2_RateExperience__c =curContact.V2_RateExperience__c;
            contact.V2_RatePDImpPatients__c= curContact.V2_RatePDImpPatients__c;
            contact.V2_RateBeds__c= curContact.V2_RateBeds__c;
            contact.V2_RateBaxBusiness__c= curContact.V2_RateBaxBusiness__c;
            contact.V2_RatePDCenter__c =curContact.V2_RatePDCenter__c;
            contact.V2_RatePDCenter__c =curContact.V2_RatePDCenter__c;
            contact.V2_RateTech__c = curContact.V2_RateTech__c;
            /*//Md bIOS
            //联系人类型
            contact.V2_ContactTypeByRENAL__c=curContact.V2_ContactTypeByRENAL__c;
            */
            //联系人级别
            contact.V2_Level__c=curContact.V2_Level__c ;
            //对百特产品支持程度
            contact.V2_Relationship_with_Baxter__c = curContact.V2_Relationship_with_Baxter__c;
            //社会职务
            contact.V2_Position_in_the_Society__c=curContact.V2_Position_in_the_Society__c;
            //学术影响力
            contact.V2_Status_of_Academy__c=curContact.V2_Status_of_Academy__c;
            
           
            if(curContact.Type__c =='新增')
            {
                insertContact.add(contact);
            }
            else if(curContact.Type__c =='编辑'  &&  curContact.Name__c != null)
            {
                updateContact.add(contact);
            }
        }catch(Exception e)
        {
            System.debug('###################################'+String.valueOf(e)+' 第'+e.getLineNumber()+'行');
        }
    }
    //因为Set不能直接update/insert 所以转换成list
    List<Contact> ListinsertContact = new List<Contact>();
    List<Contact> ListupdateContact = new List<Contact>();
    ListupdateContact.addAll(updateContact);
    ListinsertContact.addAll(insertContact);
    system.debug('Map :!'+map_rt);
    system.debug('Update list:'+ListupdateContact);
    update ListupdateContact;
    system.debug('Insert list:'+ListinsertContact);
    insert ListinsertContact;
}