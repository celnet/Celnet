trigger V2_ApprovalUser on Contact_Mod__c (before insert, before update) {
    Set<Id> OwnerIds = new Set<Id>();
    if(trigger.isInsert)
    {
        for(Contact_Mod__c cm:trigger.new)
        {
            OwnerIds.add(cm.OwnerId);
        }
    }
    if(trigger.isUpdate)
    {
        for(Contact_Mod__c newcm:trigger.new)
        {
            Contact_Mod__c oldcm = trigger.oldMap.get(newcm.Id);
            if(newcm != oldcm)
            {
                OwnerIds.add(newcm.OwnerId);
            }
        }
    }
    //List<Contact_Mod__c> updatecm = new List<Contact_Mod__c>();
    for(User u:[select ManagerId,Id from User where Id in: OwnerIds and ManagerId != null ])
    {
        for(Contact_Mod__c cm:trigger.new)
        {
            if(cm.OwnerId == u.Id)
            {
                cm.V2_ApprovalManager__c = u.ManagerId;
            }
        }
    }
    
}