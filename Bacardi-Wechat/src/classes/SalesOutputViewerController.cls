public without sharing class SalesOutputViewerController {
	public List<OutputWrapper> owList{get;set;}
	public String openId{get;set;}
	
	public SalesOutputViewerController(){
		// 获取微信用户的 OpenId
		openId = ApexPages.currentPage().getParameters().get('openId');
		String outputIds = ApexPages.currentPage().getParameters().get('outputIds');
		
		List<String> outputIdList = outputIds.split('separator');
		List<Output1__c> outputList = [Select Id, Product2__r.ProductSeries1__r.Name, Product2__r.Name, Date__c, Quantity__c From Output1__c Where Id IN: outputIdList];
		
		owList = new List<OutputWrapper>();
		
		for(Output1__c op : outputList){
			owList.add(new OutputWrapper(op));
		}
		
	}
	
	public class OutputWrapper{
		public String product_series{get;set;}
		public String product1{get;set;}
		public String salesdate{get;set;}
		public String salesamount{get;set;}
		public OutputWrapper(Output1__c op){
			this.product_series = op.Product2__r.ProductSeries1__r.Name;
			this.product1 = op.Product2__r.Name;
			this.salesdate = Datetime.newInstance(op.Date__c,Time.newInstance(0, 0, 0, 0)).format('yyyy-MM-dd');
			this.salesamount = String.valueOf(op.Quantity__c);
		}
	}
}