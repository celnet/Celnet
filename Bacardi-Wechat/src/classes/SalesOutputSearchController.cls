public without sharing class SalesOutputSearchController {
	public String openId{get;set;}
	
	public List<String> msvTagList{get;set;}
	
	public SalesOutputSearchController(){
		// 获取微信用户的 OpenId
		String code = ApexPages.currentPage().getParameters().get('code');
		String state = ApexPages.currentPage().getParameters().get('state');
		openId = ApexPages.currentPage().getParameters().get('openId');
		
		if(openId == null && code != null){
			Welink.WechatCalloutService wcs = new Welink.WechatCalloutService('gh_67b355ccdba5');
			openId = wcs.GetOpenIdbyCode(code);
		}
		
		List<monthlySalesVolumn__c> msvList = [Select Id, ProductCat__c, ProductCat__r.Name, Month__c, Year__c, time__c, num__c From monthlySalesVolumn__c 
												Where WeChat_OpenId__c =: openId And ProductCat__c != null And time__c != null];
		
		msvTagList = new List<String>();
		
		for(monthlySalesVolumn__c msv : msvList){
			Integer month = Integer.valueOf(msv.Month__c);
			String year_str = msv.Year__c;
			String month_str = month < 10?'0' + String.valueOf(month):String.valueOf(month);
			
			String msvTag = '<hr/><div class="';
			msvTag += year_str + '-' + month_str;
			msvTag += '"><div class="ui-grid-a"><div class="ui-block-a"><span>';
			msvTag += msv.ProductCat__r.Name;
			msvTag += '</span></div><div class="ui-block-b" style="text-align:center;"><span>';
			msvTag += String.valueOf(msv.num__c);
			msvTag += '</span></div></div></div>';
			
			msvTagList.add(msvTag);
		}
												
	}
}