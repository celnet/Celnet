trigger TCGPartnerCertification on TCG_Partner_Certification__c (before insert, before update, after delete, after insert, after update) {
  new Triggers() 
    .bind(Triggers.Evt.beforeinsert, new TCGPartnerCertificationHandler())  
    .bind(Triggers.Evt.beforeupdate, new TCGPartnerCertificationHandler()) 
    .bind(Triggers.Evt.afterinsert, new TCGPartnerCertificationHandler()) 
    .bind(Triggers.Evt.afterupdate, new TCGPartnerCertificationHandler()) 
    .bind(Triggers.Evt.afterdelete, new TCGPartnerCertificationHandler()) 
    .manage(); 
}