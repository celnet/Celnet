trigger User on User (after insert, after update, before update) {
    new Triggers()
        .bind(Triggers.Evt.afterinsert, new Lepus_UserHandler()) // 同步到EIP
        .bind(Triggers.Evt.afterupdate, new Lepus_UserHandler()) // 同步到EIP
        .bind(Triggers.Evt.beforeupdate , new Lepus_SetW3AccountHandler()) //根据联盟ID设置W3 Account
        
        .manage();
}