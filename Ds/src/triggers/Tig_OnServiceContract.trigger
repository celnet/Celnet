/**************************************************************************************************
 * Name             : Tig_OnServiceContract
 * Object           : CSS_Contract__c
 * Requirement      : 
 * Purpose          : 
 * Author           : Iverson gao
 * Create Date      : 07/12/2012
 * Modify History   : 
 * 2013-1-30 Polaris Yu
 * 		1. Replace accIdList with accIdSet to avoid duplicates
 *		2. Add try...catch to make the error messages easier to read
***************************************************************************************************/ 
trigger Tig_OnServiceContract on CSS_Contract__c(before insert, after update){
	Set<Id> accIdSet = new Set<Id>();
	Map<Id, Account> accMap = new Map<Id, Account>();
	String debugMsg = null;
	System.Debug('changeFromC---------------' + CommonConstant.changedFromContract);
	// New Service Contract
  	if(trigger.isBefore && trigger.isInsert && !CommonConstant.changedFromAccount){
  		CommonConstant.changedFromContract = true;
  		accIdSet = new Set<Id>();
  		accMap = new Map<Id, Account>();
  		for(CSS_Contract__c serviceContract : trigger.new){
  			accIdSet.add(serviceContract.Account_Name__c);
  		}
  		debugMsg = debugMsg + '\n===>The size of addIdSet: ' + accIdSet.size();
  		
		accMap = new Map<Id, Account>([SELECT Id, Region_Dept__c,Industry,Sub_industry_HW__c FROM Account WHERE Id IN: accIdSet]);
		debugMsg = debugMsg + '\n ===>The size of accMap: ' + accMap.size();
		
		// Get the region dept information from related account
		if (accMap.size() > 0){
			for (CSS_Contract__c serviceContract : trigger.new){
				serviceContract.Region_Dept__c = 
     				accMap.get(serviceContract.Account_Name__c).Region_Dept__c;
     			serviceContract.Industry__c = 
     				accMap.get(serviceContract.Account_Name__c).Industry==null?'无':accMap.get(serviceContract.Account_Name__c).Industry;
     			serviceContract.Sub_Industry__c = 
     				accMap.get(serviceContract.Account_Name__c).Sub_industry_HW__c==null?'无':accMap.get(serviceContract.Account_Name__c).Sub_industry_HW__c;
         	}
		}
  	}
  	
  	// Update Service Contract
  	if(trigger.isAfter && trigger.isUpdate && !CommonConstant.changedFromAccount){
  		CommonConstant.changedFromContract = true;
  		accIdSet = new Set<Id>();
  		accMap = new Map<Id, Account>();
  		Set<Id> newContractIdSet = new Set<Id>();
  		Set<Id> contractIdIndustry =new Set<Id>();
  		Map<Id, String> oldAccMap = new Map<Id, String>();
  		
  		// Used in the comparison of the old and new related account on service contract
  		for (CSS_Contract__c oldContract : trigger.old){
  			oldAccMap.put(oldContract.Id, oldContract.Account_Name__c);
  		}
  		for (CSS_Contract__c serviceContract : trigger.new){
  			// Judge whether the related account is changed
  			if (serviceContract.Account_Name__c != oldAccMap.get(serviceContract.Id)){
  				accIdSet.add(serviceContract.Account_Name__c);
  				newContractIdSet.add(serviceContract.Id);
  			}  			
  		}
  		
  		debugMsg = debugMsg + '\n ===> size of oldAccMap is: ' + oldAccMap.size();
  		
  		List<CSS_Contract__c> newContractList = [SELECT Id, Account_Name__c, Region_Dept__c
  												 FROM CSS_Contract__c
  												 WHERE Id IN: newContractIdSet];
  		debugMsg = debugMsg + '\n ===> size of newContractList is: ' + newContractList.size();
  		
  		// Update the region dept information
  		if (newContractList.size() > 0){
  			CommonConstant.changedFromContract = true;
      		accMap = new Map<Id, Account>([SELECT Id, Region_Dept__c,Industry,Sub_industry_HW__c FROM Account
      									   WHERE Id IN: accIdSet]);
      		debugMsg = debugMsg + '\n ===> size of accMap is: ' + accMap.size();
      		
      		// Store the related Equipment Archives, SFPs and Contract Product POs which will be
      		//  updated later
      		/********
            List<Installed_Asset__c> relatedEAList = [SELECT Id, Contracts_No__c, Account_Name__c,
            										  Region_Dept__c
            										  FROM Installed_Asset__c
            										  WHERE Contracts_No__c IN: newContractIdSet];
            debugMsg = debugMsg + '\n ===>size of relatedEAList: ' + relatedEAList.size();
            *******/
            List<SFP__c> relatedSFPList = [SELECT Id, Contract_No__c, Account_Name__c, Region_Dept__c
            							   FROM SFP__c WHERE Contract_No__c IN: newContractIdSet];
            debugMsg = debugMsg + '\n ===>size of relatedSFPList: ' + relatedSFPList.size();
            List<Contract_Product_PO__c> relatedCPOList = [SELECT Id, Contract_No__c, Region_Dept__c
            											   FROM Contract_Product_PO__c
            											   WHERE Contract_No__c IN: newContractIdSet];
            debugMsg = debugMsg + '\n ===>size of relatedCPOList: ' + relatedCPOList.size();
            
            // Update region dept information on service contracts, related equipment archives,
            //  related SFPs and related Contract Product POs
      		if (accMap.size() > 0){
      			Set<Id> sfpIdSet = new Set<Id>();
      			
				for (CSS_Contract__c serviceContract : newContractList){
					serviceContract.Region_Dept__c = 
	     				accMap.get(serviceContract.Account_Name__c).Region_Dept__c;
	     			serviceContract.Industry__c = 
     				accMap.get(serviceContract.Account_Name__c).Industry==null?'无':accMap.get(serviceContract.Account_Name__c).Industry;
     				serviceContract.Sub_Industry__c = 
     				accMap.get(serviceContract.Account_Name__c).Sub_industry_HW__c==null?'无':accMap.get(serviceContract.Account_Name__c).Sub_industry_HW__c;
             		
             		/* Update by Chenzhenghang 90003585
             		** 		   
             		if (relatedEAList.size() > 0){
             			for (Installed_Asset__c ea : relatedEAList){
             				if (ea.Contracts_No__c == serviceContract.Id){
             					ea.Account_Name__c = serviceContract.Account_Name__c;
             					ea.Region_Dept__c = serviceContract.Region_Dept__c;
             				}
             			}
             		}
             		*/
             		
             		if (relatedSFPList.size() > 0){
             			for (SFP__c sfp : relatedSFPList){
             				if (sfp.Contract_No__c == serviceContract.Id){
             					sfpIdSet.add(sfp.Id);
             					sfp.Account_Name__c = serviceContract.Account_Name__c;
             					sfp.Region_Dept__c = serviceContract.Region_Dept__c;
             				}
             			}
             		}
             		
             		if (relatedCPOList.size() > 0){
             			for (Contract_Product_PO__c cpo : relatedCPOList){
             				if (cpo.Contract_No__c == serviceContract.Id){
             					cpo.Region_Dept__c = serviceContract.Region_Dept__c;
             				}
             			}
             		}
             	}
             	
             	List<Work_Order__c> relatedWOList = [SELECT Id, Account_Name__c, Region_Dept__c,
             										 SFP_No__c
             										 FROM Work_Order__c
             										 WHERE SFP_No__c IN: sfpIdSet];
             	if (relatedWOList.size() > 0){
             		for (Work_Order__c wo : relatedWOList){
             			for (SFP__c sfp : relatedSFPList){
             				if (wo.SFP_No__c == sfp.Id){
             					wo.Account_Name__c = sfp.Account_Name__c;
             					wo.Region_Dept__c = sfp.Region_Dept__c;
             					break;
             				}
             			}
             		}
             	}
             	
             	
				 	
         		//update relatedEAList;
         		update relatedSFPList;
         		update relatedCPOList;
         		//如果是系统管理员操作，则不刷新工单，目的是在系统管理员批量刷新数据时，保持客户信息不变。
         		System.debug('----' + UserInfo.getProfileId());//得到的是18位id
         		// '00e10000000MKiC';   00e10000000MKiCAAW
         		if( UserInfo.getProfileId() != '00e10000000MKiC' && UserInfo.getProfileId() != '00e10000000MKiCAAW'){
         		update relatedWOList;
         		}
				update newContractList;

             	/*Update related EA by batch
             	**By chenzhenghang 90003585 2014-1-4
             	*/
             	//**********Begin
				Map<Id,CSS_Contract__c> serviceContractMap = new Map<Id,CSS_Contract__c>([SELECT Id, Account_Name__c, Region_Dept__c
  												 FROM CSS_Contract__c
  												 WHERE Id IN: newContractIdSet]);
             	UpdateContractEA_Batch eaBatch = new UpdateContractEA_Batch();
             	eaBatch.accMap = accMap;
             	eaBatch.serviceContractMap = serviceContractMap;
             	eaBatch.newContractIdSet = newContractIdSet;

             	eaBatch.query = 'SELECT Id, Contracts_No__c, Account_Name__c,Region_Dept__c FROM Installed_Asset__c WHERE Contracts_No__c IN :newContractIdSet';
             	System.debug('Query string---------------'+eaBatch.query);
				Id batchId = Database.executeBatch(eaBatch,100);
				//*********End            
			}
			
  		}  			
  	}
}