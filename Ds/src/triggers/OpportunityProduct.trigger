trigger OpportunityProduct on Project_Product__c (after delete, after update) {
    new Triggers()
    .bind(Triggers.Evt.afterdelete,new OpportunityProductHandler())
    .bind(Triggers.Evt.afterupdate,new Lepus_OpportunityProductHandler()) //added by kejun 2014-8-29 同步OpportunityProduct到EIP
    
    .manage();
}