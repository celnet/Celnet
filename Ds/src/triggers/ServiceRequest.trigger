/*
*@Purpose:整合了case上的所有trigger逻辑,由不同的handler方法实现
*
*@Author:Brain Zhang
*Create Date：2013-9
*/
trigger ServiceRequest on Case (after delete, after insert, after undelete, 
	after update, before delete, before insert, before update) {
		System.debug('enter the service request trigger:---------------------');
		System.debug('should run:------------' + CommonConstant.serviceRequestTriggerShouldRun);
		if(!CommonConstant.serviceRequestTriggerShouldRun){//直接在trigger中判断是否需要执行对应代码
			return ;
		}
		new Triggers()
			.bind(Triggers.Evt.beforeupdate,new ServiceRequestSurveyHandler())//回访后更新相应字段
			//新建或关联account变化时,维护自身及work order上的region dept 字段
			.bind(Triggers.Evt.beforeinsert,new ServiceRequestRegionDeptHandler())//perform the same function with Tig_OnServiceRequest trigger 
			.bind(Triggers.Evt.beforeupdate,new ServiceRequestRegionDeptHandler())
			.bind(Triggers.Evt.beforeinsert,new ServiceRequestContracstPopulaterHandler())//perform the function:populate contrast and W_M_SFP__c
			.bind(Triggers.Evt.beforeupdate,new ServiceRequestContracstPopulaterHandler()) 
			.bind(Triggers.Evt.beforeupdate,new ServiceRequestOwnerChangedHandler())//perform the function:owner change validation & update field 
			.bind(Triggers.Evt.beforeinsert,new ServiceRequestAssignFieldHandler())//update the owner__c field
			.bind(Triggers.Evt.beforeupdate,new ServiceRequestAssignFieldHandler())//update the owner__c field
			.bind(Triggers.Evt.afterupdate,new ServiceRequestUpdateSyncHandler())//SF更新同步到iCare
			.bind(Triggers.Evt.beforeupdate,new ServiceRequestUpdateSurveyDateToAccount())
			.manage();  
}