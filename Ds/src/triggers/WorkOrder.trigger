/*
*@Purpose:整合workorder下的trigger
*@Author:Brain Zhang
*Create Date：2013-9
*/
trigger WorkOrder on Work_Order__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	  if(!CommonConstant.workOrderTriggerShouldRun){//直接在trigger中判断是否需要执行对应代码
			return ; 
		}
		new Triggers()
			.bind(Triggers.Evt.beforeinsert,new WorkOrderCheckHandler()) //新建非例外工单时检查Service_Request_No__c 或Contract_No__c不为空，增加对SFP的校验
			.bind(Triggers.Evt.beforeupdate,new WorkOrderOwnerCheckHandler()) //国内、国际的用户不能互转，即国内用户不能转给国际用户，国际用户不能转给国内用户
			.bind(Triggers.Evt.beforeupdate,new WorkOrderSurveyHandler()) //回访记录更新时做对应的操作
			.bind(Triggers.Evt.afterinsert,new WorkOrderRollUpdateHandler()) //重新计算相关servic request上的工单数量 
			.bind(Triggers.Evt.afterupdate,new WorkOrderRollUpdateHandler()) //重新计算相关servic request上的工单数量 
			.bind(Triggers.Evt.afterdelete,new WorkOrderRollUpdateHandler()) //重新计算相关servic request上的工单数量 
			.bind(Triggers.Evt.afterundelete,new WorkOrderRollUpdateHandler()) //重新计算相关servic request上的工单数量 
			.bind(Triggers.Evt.beforeinsert,new WorkOrderRegionDeptHandler()) //perform the same function with origin Tig_OnWorkOrder
			.bind(Triggers.Evt.beforeinsert,new WorkOrderFieldPopulater()) //填充SMS_Notification_Message__c和Asset__c字段
			.bind(Triggers.Evt.beforeupdate,new WorkOrderFieldPopulater()) //填充SMS_Notification_Message__c和Asset__c字段
			.bind(Triggers.Evt.afterinsert,new WorkOrderAddProductHandler()) //perform the same function with origin TigOnWorkOrder_AutoAddWorkOrderProduct
			.manage();    
}