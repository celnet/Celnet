/*
根据事件值守计划上的Sum_Service_Amount__c字段来计算SFP下所有计划的已使用服务时间
*@Author:He Wei
*Create Date：2014-3-13
*/
trigger KeyEventOnDutyPlan on Key_Event_On_Duty_Plan__c (after delete, after insert, after undelete, 
after update) {
	new Triggers()
    .bind(Triggers.Evt.afterupdate,new KeyEventOnDutyPlan())
    .bind(Triggers.Evt.afterinsert,new KeyEventOnDutyPlan())
    .bind(Triggers.Evt.afterundelete,new KeyEventOnDutyPlan())
    .bind(Triggers.Evt.afterdelete,new KeyEventOnDutyPlan())
    .manage();
}