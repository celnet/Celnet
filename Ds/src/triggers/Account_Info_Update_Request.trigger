trigger Account_Info_Update_Request on Account_Info_Update_Request__c (after update) {
	new Triggers()
		.bind(Triggers.Evt.afterupdate,new AccountInfoUpdateHandler()) // update account information after approved
		.manage();  
}