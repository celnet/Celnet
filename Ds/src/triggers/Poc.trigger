trigger Poc on POC__c (before update, before delete) {
	new Triggers()
		.bind(Triggers.Evt.beforeupdate,new PocHandler())
		.bind(Triggers.Evt.beforedelete,new PocHandler()) 
		.manage();    
}