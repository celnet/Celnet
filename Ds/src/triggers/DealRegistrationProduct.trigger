/**
 * @Purpose : The Deal Registration Product Trigger 
 * @Author : Steven
 * @Date :   2014-09-05
 */   
trigger DealRegistrationProduct on Deal_Registration_Product__c (after insert, after update, after delete) {
	new Triggers()
		.bind(Triggers.Evt.afterinsert, new OverseaDealRegistrationProductHandler()) //(海外)报备创建后，刷新报备上的涉及的一级产品名称字段
		.bind(Triggers.Evt.afterupdate, new OverseaDealRegistrationProductHandler()) //(海外)报备更新后，刷新报备上的涉及的一级产品名称字段
		.bind(Triggers.Evt.afterdelete, new OverseaDealRegistrationProductHandler()) //(海外)报备删除后，刷新报备上的涉及的一级产品名称字段
		.manage(); 
}