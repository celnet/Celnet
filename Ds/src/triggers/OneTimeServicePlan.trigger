/*
根据单次服务计划上的Sum_Service_Amount__c字段来计算SFP下所有计划的已使用服务时间
*@Author:He Wei
*Create Date：2014-9-2
*/
trigger OneTimeServicePlan on Once_Time_Plan__c (after delete, after insert, after undelete, 
after update) {
	new Triggers()
    .bind(Triggers.Evt.afterupdate,new OneTimeServicePlan())
    .bind(Triggers.Evt.afterinsert,new OneTimeServicePlan())
    .bind(Triggers.Evt.afterundelete,new OneTimeServicePlan())
    .bind(Triggers.Evt.afterdelete,new OneTimeServicePlan())
    .manage();
}