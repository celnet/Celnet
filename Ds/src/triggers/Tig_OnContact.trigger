/**************************************************************************************************
 * Name             : Tig_OnContact
 * Object           : Contact
 * Requirement      : 新增Region Dept从account带过来，考虑各个联系人region dept是一样的。
 * Purpose          : 
 * Author           : Iverson gao
 * Create Date      : 07/12/2012
 * Modify History   : 
 * 2013-1-30 Polaris Yu
 *      1. Replace accIdList with accIdSet
 *      2. Add Criteria: region dept is changed
 *      3. Add some description
***************************************************************************************************/ 
trigger Tig_OnContact on Contact (before insert, before update, after insert, after update, after delete){
    // add by kejun 20141009 
    new Triggers()
        
        .bind(Triggers.Evt.afterinsert, new Lepus_ContactHandler()) // 同步到EIP
        .bind(Triggers.Evt.afterupdate, new Lepus_ContactHandler()) // 同步到EIP
        .bind(Triggers.Evt.afterdelete, new Lepus_ContactHandler()) // 同步到EIP
        
        .manage();

    if(trigger.isBefore && !CommonConstant.changedFromAccount){
        Set<Id> accIdSet = new Set<Id>();
        Map<Id, Account> accMap = new Map<Id, Account>();
        
        // New Contacts
        if(trigger.isInsert){
            accIdSet = new Set<Id>();
            accMap = new Map<Id, Account>();
            for(Contact contact : trigger.new){
                accIdSet.add(contact.AccountId);
            }
            
            accMap = new Map<Id, Account>([SELECT Id, Region_Dept__c FROM Account
                                           WHERE Id IN: accIdSet]);
                                           
            // Update the region dept information
            if(accMap.size() > 0){
                for(Contact contact : trigger.new){
                    contact.Region_Dept__c = accMap.get(contact.AccountId).Region_Dept__c;
                }
            }
        }
        
        // Update Contacts
        if(trigger.isUpdate){
            accIdSet = new Set<Id>();
            accMap = new Map<Id, Account>();
            List<Contact> newContactList = new List<Contact>();
            Map<Id, String> oldAccMap = new Map<Id, String>();
            
            for(Contact oldContact : trigger.old){
                oldAccMap.put(oldContact.Id, oldContact.Region_Dept__c);
            }
            
            for (Contact contact : trigger.new){
                // Judge whether the region dept of the contact has been changed
                if (contact.Region_Dept__c != oldAccMap.get(contact.Id)){
                    accIdSet.add(contact.AccountId);
                    newContactList.add(contact);
                }
            }
            
            accMap = new Map<Id, Account>([SELECT Id, Region_Dept__c FROM Account
                                           WHERE Id IN: accIdSet]);
            // Update the region dept information
            if ((newContactList.size() > 0) && (accMap.size() > 0)){
                for (Contact contact : newContactList){
                    contact.Region_Dept__c = accMap.get(contact.AccountId).Region_Dept__c;
                }
            }
        }
    }
}