/**************************************************************************************************
 * Name             : TigOnDealProduct
 * Object           : Deal_Registration__c
 * Requirement      : 
 * Purpose          : Update the Deal Registration Product Level1 field of all related Deal Registrations
 * Author           : Lingyanxia 
 * Create Date      : 05/31/2013 
 * Modify History   : 
 * 
***************************************************************************************************/
trigger TigOnDealProduct on Deal_Registration_Product__c (after delete, after insert, after update) {
  if (trigger.isAfter && (trigger.isInsert || trigger.isUpdate)) {
    UtilDealProduct.processDealProductChange(trigger.new);
  }

  if (trigger.isAfter && trigger.isDelete) {
    UtilDealProduct.processDealProductChange(trigger.old);
  }
}