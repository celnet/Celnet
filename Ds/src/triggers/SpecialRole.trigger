trigger SpecialRole on Special_Role__c (after delete, after insert, after update) {
    new Triggers()
        .bind(Triggers.Evt.afterinsert,new SpecialRoleHandler())
        .bind(Triggers.Evt.afterupdate,new SpecialRoleHandler())
        .bind(Triggers.Evt.afterdelete,new SpecialRoleHandler())
        .manage(); 
}