/*
根据工单上的服务区域，自动匹配关单审批人
巡检工单关闭后，自动扣减SFP上的巡检配额
*@Author:He Wei
*Create Date：2014-4-21
*/

trigger WOCloseApprove on Work_Order__c (before update,after insert,after delete,after undelete,after update) {
	new Triggers()
    .bind(Triggers.Evt.beforeupdate,new WOCloseApproveHandler())  
    .bind(Triggers.Evt.afterinsert,new CaculateSFPleftServicetime()) 
    .bind(Triggers.Evt.afterdelete,new CaculateSFPleftServicetime()) 
    .bind(Triggers.Evt.afterundelete,new CaculateSFPleftServicetime())   
    .bind(Triggers.Evt.afterupdate,new CaculateSFPleftServicetime())
    .manage();
}