trigger ComponentRequest on Component_Request__c (after delete, after insert, after undelete, 
after update) {
		new Triggers()
			.bind(Triggers.Evt.afterinsert,new ComponentRequestRollUpHandler()) //计算SR下备件总数量
			.bind(Triggers.Evt.afterupdate,new ComponentRequestRollUpHandler()) //计算SR下备件总数量
			.bind(Triggers.Evt.afterdelete,new ComponentRequestRollUpHandler()) //计算SR下备件总数量
			.bind(Triggers.Evt.afterundelete,new ComponentRequestRollUpHandler()) //计算SR下备件总数量
			.manage(); 
}