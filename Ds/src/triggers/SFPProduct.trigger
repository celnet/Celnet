trigger SFPProduct on SFP_Product__c (after delete, after insert, after undelete, 
after update) {
	new Triggers()
		.bind(Triggers.Evt.afterinsert,new SFPProductNameRollUpHandler()) //更新SFP上的product name字段
		//由于不允许reparent,因此注释此代码
		//.bind(Triggers.Evt.afterupdate,new SFPProductNameRollUpHandler()) //更新SFP上的product name字段
		.bind(Triggers.Evt.afterdelete,new SFPProductNameRollUpHandler()) //更新SFP上的product name字段
		.bind(Triggers.Evt.afterundelete,new SFPProductNameRollUpHandler()) //更新SFP上的product name字段
		.manage();
}