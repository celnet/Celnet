/**************************************************************************************************
 * Name             : TigOnAccount
 * Object           : Account, Opportunity
 * Requirement      : Update the industry field of all related opportunities 
 * Purpose          : Update the industry field of all related opportunities
 * Author           : Tulipe Lee
 * Create Date      : 05/16/2012
 * Modify History   : 
 * 
***************************************************************************************************/
trigger TigOnAccount on Account (after update) {
    // Update the industry field of all related opportunities
    if (trigger.isAfter && trigger.isUpdate) {
        Set<Id> accIds = new Set<Id>();
        for (Account newAcc : trigger.new) {
            if (newAcc.Industry != trigger.oldMap.get(newAcc.Id).Industry) {
                accIds.add(newAcc.Id);
            }
        }
        
        if (accIds.size() == 0) {
            return;
        }
        
        List<Opportunity> opps = new List<Opportunity>();
        for (Opportunity opp : [SELECT Industry__c, Account.Industry 
                                FROM Opportunity 
                                WHERE AccountId IN :accIds]) {
            opp.Industry__c = opp.Account.Industry;
            opps.add(opp);
        }
        
        // Update opps
        try {
            System.debug('opps==============>' + opps);
            update opps;
        }
        catch (DMLException e) {trigger.new[0].addError(e.getDMLMessage(0));
            System.debug('TigOnAccount Exception: ' + e);
        }
    }
}