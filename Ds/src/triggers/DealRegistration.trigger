/**
 * @Purpose : The Opportunity Trigger 
 * @Author : Steven
 * @Date :   2014-09-03 Refactor
 */   
trigger DealRegistration on Deal_Registration__c (before insert, before update, after update) {
	new Triggers()
		.bind(Triggers.Evt.beforeinsert,new ChinaDealRegistrationInsertHandler())   //(中国区)报备创建后记录提交人的文本名称和经销商名称
		.bind(Triggers.Evt.beforeupdate,new ChinaDealRegistrationUpdateHandler())   //(中国区)报备关联机会点被激活，关键字段更新及14天失效逻辑
		.bind(Triggers.Evt.afterupdate, new OverseaDealRegistrationUpdateHandler()) //(海外)报备审批通过/驳回时给总代联系人发送邮件提醒
		.manage(); 
}