/*
根据健康检查计划上的Sum_Service_Amount__c字段来计算SFP下所有计划的已使用巡检次数
*@Author:He Wei
*Create Date：2014-3-28
*/
trigger HealthCheckPlan on Health_Check_Plan__c (after insert, after undelete, 
after update) {
new Triggers()
	    .bind(Triggers.Evt.afterupdate,new HealthCheckPlan())
	    .bind(Triggers.Evt.afterinsert,new HealthCheckPlan())
	    .bind(Triggers.Evt.afterundelete,new HealthCheckPlan())
	    //.bind(Triggers.Evt.afterdelete,new HealthCheckPlan())
	    .manage();
}