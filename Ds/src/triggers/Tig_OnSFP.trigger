/**************************************************************************************************
 * Name             : Tig_OnSFP
 * Object           : Account
 * Requirement      : SFP__c
 * Purpose          : 
 * Author           : Iverson gao
 * Create Date      : 07/12/2012
 * Modify History   : 
 * 2013-1-30 Polaris Yu
 * 		1. Delete debugMsg to simplify the codes
 *		2. Replace accIdList,newContractIdList with accIdSet,newContractIdSet to avoid duplicates
***************************************************************************************************/ 
trigger Tig_OnSFP on SFP__c (Before insert, Before update,after update) {	
	new Triggers()    
    .bind(Triggers.Evt.afterupdate,new AutoCreateSFPTask())
    .manage();    
    if(trigger.isBefore
       && !(CommonConstant.changedFromAccount || CommonConstant.changedFromContract)){
    	Set<Id> accIdSet = new Set<Id>();
		Map<Id, Account> accMap = new Map<Id, Account>();
    	
    	// New SFP
    	if(trigger.isInsert){
			accIdSet = new Set<Id>();
      		accMap = new Map<Id, Account>();
      		for(SFP__c sfp : trigger.new){
      			accIdSet.add(sfp.Account_Name__c);
      			 CSS_Contract__c ccc=[Select c.Id,
                  c.Main_Distributor__c
                , c.Main_Distributor__r.Name
                , c.Gold_Silver_Distributor__c
                , c.Gold_Silver_Distributor__r.Name
                , c.End_Distributor__c
                , c.End_Distributor__r.Name
                , c.Industry__c
                , c.Sub_Industry__c
                , c.Office__c
                From CSS_Contract__c c where c.id =:sfp.Contract_No__c];
                sfp.Main_Distributor__c=ccc.Main_Distributor__r.Name; 
                sfp.Gold_Silver_Distributor__c=ccc.Gold_Silver_Distributor__r.Name;
                sfp.End_Distributor__c=ccc.End_Distributor__r.Name;
                sfp.Industry__c =ccc.Industry__c;
                sfp.Sub_Industry__c =ccc.Sub_Industry__c;
                sfp.Rep_Office_China__c =ccc.Office__c;
      		}
			accMap = new Map<Id, Account>([SELECT Id, Region_Dept__c FROM Account
										   WHERE Id IN: accIdSet]);
			
			if (accMap.size() > 0){
				for (SFP__C sfp : trigger.new){
					sfp.Region_Dept__c = 
         				accMap.get(sfp.Account_Name__c).Region_Dept__c;
             	}
			}
		}
		
		// Update SFP
		if(trigger.isUpdate){
			accIdSet = new Set<Id>();
      		accMap = new Map<Id, Account>();
      		List<SFP__c> newSFPList = new List<SFP__c>();
      		Set<Id> newContractIdSet = new Set<Id>();
      		Map<Id, String> oldContractMap = new Map<Id, String>();
      		
      		// Used in the comparison of the old and new related service contract on SFP
      		for (SFP__c sfp : trigger.old){
      			oldContractMap.put(sfp.Id, sfp.Contract_No__c);
      		}
      		for (SFP__c sfp : trigger.new){
      			// Judge whether the related service contract is changed
      			if(sfp.Contract_No__c != oldContractMap.get(sfp.Id)){
      				newSFPList.add(sfp);
      				newContractIdSet.add(sfp.Contract_No__c);
      			}
      		}
      		
      		Map<Id, CSS_Contract__c> newContractMap = 
      			new Map<Id, CSS_Contract__c>([SELECT Id, Name, Account_Name__c, Region_Dept__c,
      										  Account_Name__r.Name,Industry__c,Sub_Industry__c,
      										  Office__c
      										  FROM CSS_Contract__c
      										  WHERE Id IN: newContractIdSet]);
      		
      		for (SFP__c sfp : newSFPList){
      			sfp.Account_Name__c = newContractMap.get(sfp.Contract_No__c).Account_Name__c;
      			sfp.Region_Dept__c = newContractMap.get(sfp.Contract_No__c).Region_Dept__c;
      			sfp.Industry__c =newContractMap.get(sfp.Contract_No__c).Industry__c;
                sfp.Sub_Industry__c =newContractMap.get(sfp.Contract_No__c).Sub_Industry__c;
                sfp.Rep_Office_China__c =newContractMap.get(sfp.Contract_No__c).Office__c;
      		}
      		
		}
    }
}