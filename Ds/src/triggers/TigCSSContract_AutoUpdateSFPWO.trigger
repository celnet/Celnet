trigger TigCSSContract_AutoUpdateSFPWO on CSS_Contract__c (after update) {//2009.12.29 before update
    
    if(Trigger.isUpdate){
        List<Id> cccIdList=new List<Id>();
        Map<Id,CSS_Contract__c> cccIdcccMap=new Map<Id,CSS_Contract__c>();
        
        for(CSS_Contract__c ccc:Trigger.new){
            cccIdList.add(ccc.id);
            cccIdcccMap.put(ccc.id,ccc);
        }
        
        List<SFP__c> sfpList=[Select s.Status__c, s.Contract_No__c 
                From SFP__c s
                where s.Contract_No__c in :cccIdList and s.Status__c!='Cancelled'];
                /**
        List<Work_Order__c> woList=[Select w.Status__c, w.Contract_No__c From Work_Order__c w
                where w.Contract_No__c in :cccIdList and w.Status__c!='Cancelled'];
                **/
        List<Work_Order__c> woListTmp=[Select w.Status__c, w.Contract_No__c,w.Engineer_Setoff_Time__c, w.Engineer_Return_Time__c
                From Work_Order__c w
                where w.Contract_No__c in :cccIdList 
                    and w.Status__c!='Cancelled'];
        List<Work_Order__c> woList=new List<Work_Order__c>();
        for(Work_Order__c wo:woListTmp){
            /**
            if(wo.Engineer_Setoff_Time__c!=null && 
                wo.Engineer_Return_Time__c!=null &&
                wo.Engineer_Setoff_Time__c < wo.Engineer_Return_Time__c){
                woList.add(wo);
            }
            */
            if(wo.Status__c!='Closed' && wo.Status__c!='Cancelled')woList.add(wo);
        }
                
        if(Trigger.isAfter){
            if(sfpList!=null && sfpList.size()>0){
                for(SFP__c sfp:sfpList){
                    CSS_Contract__c ccc=cccIdcccMap.get(sfp.Contract_No__c);
                    if(ccc.Status__c=='Cancelled')sfp.Status__c='Cancelled';
                }
                try{
                    update(sfpList);
                }catch(Exception e){}
            }
            
            if(woList!=null && woList.size()>0){
                for(Work_Order__c wo:woList){
                    CSS_Contract__c ccc=cccIdcccMap.get(wo.Contract_No__c);
                    if(ccc.Status__c=='Cancelled')wo.Status__c='Cancelled'; 
                }
                try{
                    if(woList!=null && woList.size()>0)update(woList);
                }catch(Exception e){}
            }
        }
        if(Trigger.isBefore){
            /**
            Map<Id,Boolean> cccIdCanChangedMap=new Map<Id,Boolean>();
            if(woList!=null)
            for(Work_Order__c wo:woList){
                if(wo.Status__c=='Closed'){
                    if(cccIdCanChangedMap.containsKey(wo.Contract_No__c))continue;
                    cccIdCanChangedMap.put(wo.Contract_No__c,false);
                }
            }
            
            for(CSS_Contract__c ccc:Trigger.new){
                String s=ccc.Status__c;
                if(!s.equals('Cancelled'))continue;
                if(!cccIdCanChangedMap.containsKey(ccc.id))continue;
                Boolean canChange=cccIdCanChangedMap.get(ccc.id);
                if(!canChange)
                    ccc.addError('Work Order Closed already. Contract can\'t be cancelled. 已有结束的工单，合同不能取消。');
            }
            */
        }
    }
}