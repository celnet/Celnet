/*
根据SFP上合同的代表处，自动匹配备件计划的审批人
*@Author:He Wei
*Create Date：2014-3-12
*/

trigger SparePlan on Spare_Plan__c (before update,before insert) {
	new Triggers()
    .bind(Triggers.Evt.beforeupdate,new SparePlanApproverHandler())
    .bind(Triggers.Evt.beforeinsert,new SparePlanApproverHandler())
    .manage();
}