/**************************************************************************************************
 * Name             : TigOnProjectProduct
 * Object           : Account, Opportunity
 * Requirement      : Cretiera Base Sharing Rule based on Opportunity
 * Purpose          : Update the industry field of all related opportunities
 * Author           : Mouse Liu
 * Create Date      : 05/16/2012
 * Modify History   : 
 * 
***************************************************************************************************/
trigger TigOnProjectProduct on Project_Product__c(after delete, after insert, after update) {
    if (trigger.isAfter && (trigger.isInsert || trigger.isUpdate)) {
        UtilProjectProduct.processProjectProductChange(trigger.new);
    }
    
    if (trigger.isAfter && trigger.isDelete) {
        UtilProjectProduct.processProjectProductChange(trigger.old);
    }
}