/*
 * Author: Sunny.sun@celnet.com.cn
 * Date: 2014-8-27
 * Description: Lepus队列trigger,触发lepus batch
 */
trigger Lepus_Queue on Lepus_Queue__c (after insert) {
	new Triggers()
	.bind(Triggers.Evt.afterinsert,new Lepus_QueueBatchStartHandler()) // add by sunda 20140827
	.manage();
}