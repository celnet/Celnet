/**************************************************************************************************
 * Name             : TigOnChangeOI 
 * Object           : Opportunity
 * Requirement      : Change OI and OO while diffrent stage
 * Purpose          : 
 * Author           : Iverson gao
 * Create Date      : 05/21/2012
 * Modify History   : 
***************************************************************************************************/
trigger TigOnChangeOO on Opportunity (after update, before insert) {
     /*if(trigger.isBefore && trigger.isInsert){ 
            for(Opportunity opp:trigger.new){
               opp.Opportunity_Owner__c=null;
            }
     }
     if(Trigger.isAfter){ 
        if(Trigger.isUpdate){  
            System.Debug('\n############ AfterUpdate');
            //Select o.MasterLabel, o.Id From OpportunityStage o
            List<Opportunity> opps = [Select StageName, OwnerId ,Opportunity_Identifier__c,Opportunity_Owner__c,TestTriggerChangeOOOnStageUpdate__c
                                      From Opportunity Where id IN: trigger.new];         
            List<User> users =[Select Id,Name From User];
                       
            //old stage set
            Set<String> stageSeta = new set<String>();
            stageSeta.add('SS1 (Notifying Opportunity)');
            stageSeta.add('SS2 (Identifying Opportunity)');
            stageSeta.add('SS3 (Validating Opportunity)');
            //new stage set
            Set<String> stageSetb = new set<String>();
            stageSetb.add('SS4 (Developing Solution)');
            stageSetb.add('SS5 (Gaining Aggrement)');
            stageSetb.add('SS6 (Winning)');
            stageSetb.add('SS7 (Implementing)');
            stageSetb.add('SS8 (Completed)');            
            
            //System.Debug('\n\n@@@@@@@@@'+str);
            Map<Id,User> idToUserMap = new Map<Id,User>();
           
            for(User user:users){ 
                idToUserMap.put(user.Id,user);
            }
            for(Opportunity opp : opps) {
                //取原来的opp集合
                Opportunity oldOpp = trigger.oldMap.get(opp.Id);
                User user = idToUserMap.get(opp.OwnerId);
                //当ss4在回到ss4以下，oo还是继续改变
                if(stageSetb.contains(opp.StageName) && user.id == opp.OwnerId){
                           opp.Opportunity_Owner__c = user.Name;
                           opp.TestTriggerChangeOOOnStageUpdate__c=true;  
                }     
                //再一次改回SS4以上，oo继续变
                if(stageSeta.contains(opp.StageName) && user.Id == opp.OwnerId && opp.TestTriggerChangeOOOnStageUpdate__c==true){opp.Opportunity_Owner__c = user.Name;}             
            }
            if(!CommonConstant.isUpdate){
                CommonConstant.isUpdate=true;
                update opps;        
            }
        }   
     }*/
}