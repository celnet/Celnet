/**************************************************************************************************
 * Name             : Tig_OnEquipmentArchive
 * Object           : Installed_Asset__c
 * Requirement      :
 * Purpose          : 
 * Author           : Iverson gao
 * Create Date      : 07/12/2012
 * Modify History   : 
 * 2012-9-20 Polaris Yu
 * 		1. Add some debug message
 *		2. Modified the executing time for this code, added "before update"
 *      3. Used list to store the information, added installedAssetList,contractsNoList
 * 2013-1-30 Polaris Yu
 *		1. Replace contractsNoList with contractsNoSet to avoid duplicate
 *		2. Add criteria: record is new or related account/service contract is changed
***************************************************************************************************/ 
trigger Tig_OnEquipmentArchive on Installed_Asset__c (before insert, before update){
	if(trigger.isBefore && !CommonConstant.changedFromAccount){
		List<Installed_Asset__c> installedAssetList = new List<Installed_Asset__c>();
		Set<Id> contractsNoSet = new Set<Id>();
		String debugMsg = null;
		
		for(Installed_Asset__c installedAsset : trigger.new){
			// New Equipment Archive
			if(trigger.isInsert){
				installedAssetList.add(installedAsset);
				contractsNoSet.add(installedAsset.Contracts_No__c);
			}
			debugMsg = debugMsg + '\n===>size of installedAsstList is: ' + installedAssetList.size();
			debugMsg = debugMsg + '\n===>size of contractsNoSet is: ' + contractsNoSet.size();
			
			// Update Equipment Archive
			// Judge whether the EA is new or its related account/service contract is changed
			if(trigger.isUpdate
			   && (installedAsset.Account_Name__c != trigger.oldMap.get(installedAsset.Id).Account_Name__c
			   || installedAsset.Contracts_No__c != trigger.oldMap.get(installedAsset.Id).Contracts_No__c)){
				installedAssetList.add(installedAsset);
				contractsNoSet.add(installedAsset.Contracts_No__c);
			}
			debugMsg = debugMsg + '\n===>size of installedAsstList is: ' + installedAssetList.size();
			debugMsg = debugMsg + '\n===>size of contractsNoSet is: ' + contractsNoSet.size();
		}
		
		List<CSS_Contract__c> conList = [Select Account_Name__c, Account_Name__r.Region_Dept__c
										,Industry__c,Sub_Industry__c,Office__c
										 From CSS_Contract__c 
										 WHERE Id IN: contractsNoSet];
		debugMsg = debugMsg + '\n===>size of conList is: ' + conList.size();
		
		// Update EA's region dept information
		if(installedAssetList.size() > 0){
			for(Installed_Asset__c installedAsset : installedAssetList){
				if(conList.size() > 0){
					for(CSS_Contract__c con : conList){
						if(installedAsset.Contracts_No__c == con.Id
						   && con.Account_Name__c != installedAsset.Account_Name__c){
							trigger.new[0].addError('\nThe Equipment Archive and the Service '
								+ 'Contract is not associated with the same Account!');
							break;
						}
						else if(installedAsset.Contracts_No__c == con.Id){
								installedAsset.Region_Dept__c = con.Account_Name__r.Region_Dept__c;
								installedAsset.Industry__c=con.Industry__c;
								installedAsset.Sub_Industry__c=con.Sub_Industry__c;
								installedAsset.Rep_Office_China__c=con.Office__c;
							break;
						}
					}
				}
				else{
					installedAsset.Region_Dept__c = null;
					installedAsset.Industry__c=null;
					installedAsset.Sub_Industry__c=null;
					installedAsset.Rep_Office_China__c=null;
				}
			}
		}
	}
}