trigger TigOnSFP_AutoAddSFPProduct on SFP__c (after insert) {
	
	if(Trigger.isInsert){
		if(Trigger.isAfter){
			
			Map<Id,SFP__c> contractIdSFPMap=new Map<Id,SFP__c>();
			for(SFP__c sfp:Trigger.new){
				contractIdSFPMap.put(sfp.Contract_No__c,sfp);
			}
			
			List<Contract_Product_PO__c> contractProPOList=[Select c.Id, c.Contract_No__c ,c.Quantity__c
				From Contract_Product_PO__c c
				where c.Contract_No__c in :contractIdSFPMap.keySet()];
			
			List<Contract_Product_PO__c> cppListTmp=null;
			Map<Id,List<Contract_Product_PO__c>> sfpIdContractProPOListMap=new Map<Id,List<Contract_Product_PO__c>>();//id=sfp.id

			//Select s.SFP__c, s.Name, s.Id, s.Contract_Product_PO__c From SFP_Product__c s
			List<SFP_Product__c> sfpProductList = new List<SFP_Product__c>();
			SFP_Product__c sfpProduct=null;
			for(Contract_Product_PO__c cpp:contractProPOList){
				SFP__c sfp=contractIdSFPMap.get(cpp.Contract_No__c);
				if(sfpIdContractProPOListMap.containsKey(sfp.id)){
					cppListTmp=sfpIdContractProPOListMap.get(sfp.id);
				}else{
					cppListTmp=new List<Contract_Product_PO__c>();
				}
				cppListTmp.add(cpp);
				sfpIdContractProPOListMap.put(sfp.id,cppListTmp);
				
				for(Contract_Product_PO__c cpp2:cppListTmp){
					sfpProduct=new SFP_Product__c(
							 SFP__c=sfp.id
							,Contract_Product_PO__c=cpp2.id
							,Quantity__c=cpp2.Quantity__c
						);
					sfpProductList.add(sfpProduct);
				}
				cppListTmp.clear();
			}
			
			insert(sfpProductList);
			
		}
	}
}