trigger TigOnSFP on SFP__c (before update) {//ok  //原为 (before insert, before update)zj20110120 
    
    Map<Id,Integer> sfpIdSizeMap=new Map<Id,Integer>();//sfp.id
    List<id> sfpIdSet=new List<id>();//id=sfp.id
    List<Id> cssIdList=new List<Id>();//id=CSS_Contract__c.id;
    for(SFP__c sfp:Trigger.new){  
    	sfpIdSet.add(sfp.Id);
        cssIdList.add(sfp.Contract_No__c); 
    }
    //** 因删除经销商字段,去掉该部分触发
    Map<Id,CSS_Contract__c> cssIdCSSMap=new Map<Id,CSS_Contract__c>();
    List<CSS_Contract__c> cl=[Select c.Id,
                  c.Main_Distributor__c
                , c.Main_Distributor__r.Name
                , c.Gold_Silver_Distributor__c
                , c.Gold_Silver_Distributor__r.Name
                , c.End_Distributor__c
                , c.End_Distributor__r.Name
                From CSS_Contract__c c where c.id in :cssIdList];
    for(CSS_Contract__c css:cl){
        cssIdCSSMap.put(css.id,css);
    }
    //*/
    
    List<Work_Order__c> workOrderList=[select id,SFP_No__c, RecordTypeId from Work_Order__c where SFP_No__c in :sfpIdSet
    and status__c!='Cancelled'];
    for(Work_Order__c wo:workOrderList){ 
        Integer sz=0;
        if(sfpIdSizeMap.containsKey(wo.SFP_No__c) )
            sz=sfpIdSizeMap.get(wo.SFP_No__c);
        if( wo.RecordTypeId=='012900000000OIiAAM' || wo.RecordTypeId=='012900000000OIjAAM'
        || wo.RecordTypeId=='01290000000dvO7AAI' ||wo.RecordTypeId=='01290000000dvO6AAI'
        || wo.RecordTypeId=='012900000000OIXAA2'|| wo.RecordTypeId=='012900000000OIWAA2' )        
        	sz+=1;//当work order类型为health check相关类型时，才计入配额
        sfpIdSizeMap.put(wo.SFP_No__c,sz);
    }
    for(SFP__c sfp:Trigger.new){
            Integer sz=sfpIdSizeMap.containsKey(sfp.Id)?sfpIdSizeMap.get(sfp.Id):0;
            
            Decimal d=0;
            //change No. of Health Check Service Left; 
            try{
                if(sfp.No_of_Health_Check_Service__c!=null){
                	system.debug('2222222222222'+sz);
                   //if(sfp.CreatedDate < Date.newInstance(2014,04,02) ){
                   		sfp.No_of_Health_Check_Service_Used__c = sz;
	                   	d=Decimal.valueOf(''+sfp.No_of_Health_Check_Service__c);                    
	                    sfp.No_of_Health_Check_Service_Left__c=d-sz;
	                    //if(d-sz<0){
	                    	//sfp.addError('健康检查次数不能小于已使用次数');
	                    	//return;
	                   // }
                   // } else{
                    	//modify by hewei -2014-3-29
	                    //维保SFP
	                   /* if(sfp.No_of_Health_Check_Service__c <sfp.No_of_Health_Check_Service_Used__c&&
	                    sfp.No_of_Health_Check_Service_Used__c!=null){
	                    	sfp.addError('健康检查次数不能小于已使用次数');
	                    	return;
	                    }
	                    sfp.No_of_Health_Check_Service_Left__c=
	                    (sfp.No_of_Health_Check_Service__c<=0?0:sfp.No_of_Health_Check_Service__c)
	                    -(sfp.No_of_Health_Check_Service_Used__c==null?0:sfp.No_of_Health_Check_Service_Used__c);//在健康检查计划上新增tirgger赋值 等于总次数-健康检查总次数                	
                    }*/
                    
                }
                
                //事件值守SFP
            	if(sfp.Key_Event_On_Duty_Total__c!=null&&sfp.Key_Event_On_Duty_Used__c!=null&&sfp.Key_Event_On_Duty_Total__c<sfp.Key_Event_On_Duty_Used__c){
            		sfp.addError('事件值守总时长不能小于已使用时长');            		
            	}
            	//驻场服务SFP
            	if(sfp.Resident_Engineer_Support_Total__c!=null&&sfp.Resident_Engineer_Support_Used__c!=null&&sfp.Resident_Engineer_Support_Total__c<sfp.Resident_Engineer_Support_Used__c){
            		sfp.addError('驻场服务总时长不能小于已使用时长');            		
            	}
            	//单次服务SFP
            	if(sfp.No_of_On_Site_Service__c!=null&&sfp.No_of_On_Site_Service_Used__c!=null&&sfp.No_of_On_Site_Service__c<sfp.No_of_On_Site_Service_Used__c){
            		sfp.addError('单次服务总时长不能小于已使用时长');            		
            	}
            }
            catch(Exception e){sfp.addError(sfp.id+':err 1');}
            
            //change No. of Performance Tuning Service Left
            try{
                if(sfp.No_of_Performance_Tuning_Service__c!=null){
                    d=Decimal.valueOf(''+sfp.No_of_Performance_Tuning_Service__c);
                    sfp.No_of_Performance_Tuning_Service_Left__c=d-sz;
                }
            }catch(Exception e){sfp.addError(sfp.id+':err 2');}
            
            //change No. of On Site Service Left
            //单次服务SFP以单次计划为准
           /*
            try{
                if(sfp.No_of_On_Site_Service__c!=null){
                    d=Decimal.valueOf(''+sfp.No_of_On_Site_Service__c);
                    sfp.No_of_On_Site_Service_Left__c=d-sz;
                }
            }catch(Exception e){sfp.addError(sfp.id+':err 3'+e);}
            */
            
            CSS_Contract__c ccc=cssIdCSSMap.get(sfp.Contract_No__c);
           if(ccc!=null){
                sfp.Main_Distributor__c=ccc.Main_Distributor__r.Name; 
                sfp.Gold_Silver_Distributor__c=ccc.Gold_Silver_Distributor__r.Name;
                sfp.End_Distributor__c=ccc.End_Distributor__r.Name;
            }
            //sfp.No_of_Health_Check_Service__c.addError('测试：'+sfp.No_of_Health_Check_Service__c+','+sz);
            
    } 

    /////////////////////////////   
 
   /* 
    if(Trigger.isUpdate){
        List<Id> sfpIdList=new List<Id>();
        Map<Id,SFP__c> sfpIdsfpMap=new Map<Id,SFP__c>();
        
        for(SFP__c sfp:Trigger.new){
            sfpIdList.add(sfp.id);
            sfpIdsfpMap.put(sfp.id,sfp);
        }
        
      
        List<Work_Order__c> woListTmp=[Select w.Status__c, w.SFP_No__c,w.Engineer_Setoff_Time__c, w.Engineer_Return_Time__c
                From Work_Order__c w
                where w.SFP_No__c in :sfpIdList 
                    and w.Status__c!='Cancelled'];
        List<Work_Order__c> woList=new List<Work_Order__c>();
        for(Work_Order__c wo:woListTmp){
            
            if(wo.Status__c!='Closed' && wo.Status__c!='Cancelled')woList.add(wo);
        }
                
        if(Trigger.isAfter){
           
            if(woList!=null && woList.size()>0){
                for(Work_Order__c wo:woList){
                    SFP__c sfp=sfpIdsfpMap.get(wo.SFP_No__c);
                    if(sfp.Status__c=='Cancelled')wo.Status__c='Cancelled'; 
                }
                try{
                    if(woList!=null && woList.size()>0)update(woList);
                }catch(Exception e){}
            }
        }
        if(Trigger.isBefore){
           
        }
    }    */  
}