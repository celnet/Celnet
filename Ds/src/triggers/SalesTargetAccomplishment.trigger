trigger SalesTargetAccomplishment on Sales_Target_Accomplishment__c (after insert, after update) {
    new Triggers()
        .bind(Triggers.Evt.afterinsert,new SalesTargetSharingHandler())
        //.bind(Triggers.Evt.afterupdate,new SalesTargetSharingHandler()) 暂时不考虑更换account manager的情况,实现起来比较复杂
        .manage();
}