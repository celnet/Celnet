@isTest
private class OpportunityArchiveForBIHandlerTest {
    static testmethod void test(){
        Account acc = UtilUnitTest.createHWChinaAccount('testaccc');
        Opportunity opp = UtilUnitTest.createOpportunity(acc);
        opp.RecordTypeId = CONSTANTS.CHINAOPPORTUNITYRECORDTYPE;
        update opp;
        Product_HS__c phc = UtilUnitTest.createProductMaster('testlevel1');
        Project_Product__c ppc = UtilUnitTest.createProjectProduct(opp, phc);
        OpportunityTeamMember member = new OpportunityTeamMember();
        member.opportunityid = opp.id;
        member.UserId = Userinfo.getUserId();
        member.TeamMemberRole = 'test';
        insert member;
        delete opp;
        //List<Data_Maintain_History__c> dmhList = new List<Data_Maintain_History__c>();
        //dmhList = [Select Id From Data_Maintain_History__c Where Id =: opp.Id];
        //System.assertEquals(dmhList.size(), 1);
    }
}