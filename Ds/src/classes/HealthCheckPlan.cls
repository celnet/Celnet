public without sharing class HealthCheckPlan implements Triggers.Handler {
	public void handle(){
		list <Health_Check_Plan__c> allHealthPlan = new list <Health_Check_Plan__c>();
		list <SFP__c> sfp = new list <SFP__c>();
		set <ID> healthID = new set<ID>();	
		set <ID> errorHealthID = new set<ID>();	
		Map <id,id> sfpHealthMap = new Map <id,id>();
		if((trigger.isInsert || trigger.isUnDelete||trigger.isUpdate)){
			for(Health_Check_Plan__c healthPlan :(list<Health_Check_Plan__c>)trigger.new){					
				if(healthPlan.Service_Type__c!='When Customers Need Feedback'&&healthPlan.Service_Type__c!='Customers Does Not Need')
					{
						healthID.add(healthPlan.id);
						sfpHealthMap.put(healthPlan.id,healthPlan.SFP__c);	
					}						
			}
		}
		/*
		if(trigger.isDelete){
			for(Health_Check_Plan__c healthPlan :(list<Health_Check_Plan__c>)trigger.old){						
				healthID.add(healthPlan.id);
				sfpHealthMap.put(healthPlan.id,healthPlan.SFP__c);				
			}
		}*/
		if(healthID.size()>0){
			for(ID id :healthID){
				Health_Check_Plan__c healthPlan = new Health_Check_Plan__c();				
				sfp = [select id,CreatedDate,No_of_Health_Check_Service__c,No_of_Health_Check_Service_Used__c,No_of_Health_Check_Service_Left__c
				 	from SFP__c where id =:sfpHealthMap.get(id)];	
				allHealthPlan =
				[select id,Sum_Service_Amount__c from Health_Check_Plan__c 
					where SFP__c = :sfpHealthMap.get(id) ];	
					Decimal sumServiceAmount = 0;		
				if(allHealthPlan.size()>0){	
					//&&sfp[0].CreatedDate >Date.newInstance(2014,06,30)					
					for(Health_Check_Plan__c h :allHealthPlan){
						sumServiceAmount +=h.Sum_Service_Amount__c;
						if(h.id == id) 
						{
							healthPlan = h;							
							}
					}
					//sfp[0].No_of_Health_Check_Service_Used__c = sumServiceAmount;
						
					}	
				//else{sfp[0].No_of_Health_Check_Service_Used__c=0;}	
				if(sfp[0].No_of_Health_Check_Service__c!=null){
					//&&sfp[0].CreatedDate >Date.newInstance(2014,06,30)
					if(sumServiceAmount >sfp[0].No_of_Health_Check_Service__c ){
						addErrorInfoA(id);
						return;
					}
					/*if(sumServiceAmount < sfp[0].No_of_Health_Check_Service_Left__c && sfp[0].CreatedDate >Date.newInstance(2014,06,30)){
						addErrorInfoB(id);
						return;
					}*/
					/*
					if(sfp[0].No_of_Health_Check_Service__c>=0){
						sfp[0].No_of_Health_Check_Service_Left__c = sfp[0].No_of_Health_Check_Service__c - sfp[0].No_of_Health_Check_Service_Used__c;
					}else{
						sfp[0].No_of_Health_Check_Service__c = 0;					
					}								
					update sfp;
					*/
				}		
			}
		}

	}
	
	private void addErrorInfoA(Id healthPlanId){
		for(Health_Check_Plan__c healthPlan :(list<Health_Check_Plan__c>)trigger.new){					
			if(healthPlanId == healthPlan.id )healthPlan.addError('已添加的健康检查次数不能大于SFP健康检查计划总次数');
		}
	}
	private void addErrorInfoB(Id healthPlanId){
		for(Health_Check_Plan__c healthPlan :(list<Health_Check_Plan__c>)trigger.new){								
			if(healthPlanId == healthPlan.id )healthPlan.addError('请一次性添加完健康检查计划');
		}
	}
}