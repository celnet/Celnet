/**
 * @Purpose : 中国区容灾解决方案的机会点创建或更新后记录到历史机会点表中供集成抓取
 * @Author : steven
 * @Date : 2014-10-16
 */
public class ChinaSpecialOpportunityHistory implements Triggers.Handler{ //after insert after update
    public static Boolean isFirstRun = true; //避免 after update之后重新进入
    public void handle(){
        if(!AccountAfterUpdateHandler.isFirstRun) return;
        List<Data_Maintain_History__c> archiveList = new List<Data_Maintain_History__c>();
        for(Opportunity o : (List<Opportunity>)trigger.new){
            //Step1：过滤出所有中国区机会点
            if(o.RecordTypeId == CONSTANTS.CHINAOPPORTUNITYRECORDTYPE || 
                 o.recordtypeid == CONSTANTS.HUAWEICHINASERVICEOPPRECORDTYPE || 
                 o.RecordTypeId == CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE){
                //Logic1：过滤出SS123的机会点
                if(o.StageName == 'SS1 (Notifying Opportunity)' || 
                     o.StageName == 'SS2 (Identifying Opportunity)' || 
                     o.StageName == 'SS3 (Validating Opportunity)' ) {
                    Data_Maintain_History__c dmh = new Data_Maintain_History__c(Record_ID__c=o.id,Data_Type__c = 'China Opportunity Team');
                    //Logic1-1：对于新增的直接记录下来
                    if(trigger.isInsert) {
                        if('Disaster Recovery Solution' == o.Opportunity_Solution_picklist__c){
                            dmh.Operation_Type__c = 'Create';
                            archiveList.add(dmh);
                        }
                    }
                    
                    system.debug('------------------------->>>---');
                    //Logic1-2：对于修改了容灾解决方案的记录下来
                    if(trigger.isUpdate) {
                        Opportunity oldO = (Opportunity)Trigger.oldMap.get(o.id);
                        String newSolution = o.Opportunity_Solution_picklist__c;
                        String oldSolution = oldO.Opportunity_Solution_picklist__c;
                        if(('Disaster Recovery Solution' == newSolution) || 
                             ('Disaster Recovery Solution' != newSolution && 'Disaster Recovery Solution' == oldSolution)){
                            dmh.Operation_Type__c = 'Update';
                            archiveList.add(dmh);
                        }
                    }
                }
            }
        }
        //Step2：将机会点ID记录下来供集成使用
        if(archiveList.size() > 0){
            insert archiveList;
        }
        
        ChinaSpecialOpportunityHistory.isFirstRun = false;
    }
    
}