@isTest
private class WorkOrderRollUpdateHandlerTest {

    static testMethod void singleTest() {
      Account acc = UtilUnitTest.createAccount();
    	Case sr = UtilUnitTest.createChinaServiceRequest(); 
    	sr = [select id,On_Site_Work_Order_Number__c from Case where id =:sr.id];
    	System.assertNotEquals(1,sr.On_Site_Work_Order_Number__c);
    	Work_Order__c w = UtilUnitTest.createOnSiteWorkOrder(acc,sr);//insert
    	sr = [select id,On_Site_Work_Order_Number__c from Case where id =:sr.id];
    	System.assertEquals(1,sr.On_Site_Work_Order_Number__c);
    	
    	w.Service_Request_No__c = null;//update
    	update w;
    	w = [select Service_Request_No__c from Work_Order__c where id =:w.id];
    	System.assertEquals(null,w.Service_Request_No__c);
    	sr = [select id,On_Site_Work_Order_Number__c from Case where id =:sr.id];
    	System.assertEquals(0,sr.On_Site_Work_Order_Number__c);
    	
    	w.Service_Request_No__c = sr.id;//update
    	update w;
    	sr = [select id,On_Site_Work_Order_Number__c from Case where id =:sr.id];
    	System.assertEquals(1,sr.On_Site_Work_Order_Number__c);
    	
    	delete w;
    	sr = [select id,On_Site_Work_Order_Number__c from Case where id =:sr.id];
    	System.assertEquals(0,sr.On_Site_Work_Order_Number__c);
    	
    	undelete w;
    	sr = [select id,On_Site_Work_Order_Number__c from Case where id =:sr.id];
    	System.assertEquals(1,sr.On_Site_Work_Order_Number__c);
    }
    
    static testMethod void spareAmountTest(){//这个是计算SR下所有备件申请的总数量之和（求和Quantity），赋值给SR的Spare Amount(Remote)字段
    	Account acc = UtilUnitTest.createAccount();
    	Case sr = UtilUnitTest.createChinaServiceRequest(); 
    	sr = [select id,Spare_Amount__c from Case where id =:sr.id];
    	System.assertEquals(null,sr.Spare_Amount__c);
    	Work_Order__c w = UtilUnitTest.createOnSiteWorkOrder(acc,sr);//insert
    	Spare_Part__c sp = UtilUnitTest.createSparePart(w);
    	sr = [select id,Spare_Amount__c from Case where id =:sr.id];
    	System.assertEquals(1,sr.Spare_Amount__c);
    	
    	//insert another spare part
    	UtilUnitTest.createSparePart(w);
    	sr = [select id,Spare_Amount__c from Case where id =:sr.id];
    	System.assertEquals(2,sr.Spare_Amount__c);
    	
    	w.Service_Request_No__c = null;//update
    	update w;
    	w = [select Service_Request_No__c from Work_Order__c where id =:w.id];
    	System.assertEquals(null,w.Service_Request_No__c);
    	sr = [select id,Spare_Amount__c from Case where id =:sr.id];
    	System.assertEquals(0,sr.Spare_Amount__c);
    	
    	w.Service_Request_No__c = sr.id;//update
    	update w;
    	sr = [select id,Spare_Amount__c from Case where id =:sr.id];
    	System.assertEquals(2,sr.Spare_Amount__c);
    	
    	delete sp;
    	sr = [select id,Spare_Amount__c from Case where id =:sr.id];
    	System.assertEquals(1,sr.Spare_Amount__c);
    	
    	undelete sp;
    	sr = [select id,Spare_Amount__c from Case where id =:sr.id];
    	System.assertEquals(2,sr.Spare_Amount__c);
    }
}