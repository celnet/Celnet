/**************************************************************************************************
 * Purpose          : Test Named Account  Trigger
 * Author           : Brain Zhang
 * 
***************************************************************************************************/

@isTest
private class TriggerNamedAccountTest {
	//更新ACC上的NA相关字段信息
	
    static testMethod void testCreate() {//with no parent
    	String RecordTypeId = UtilUnitTest.getRecordTypeIdByName('Account',
    		 'Huawei China Customer');
        Account acc = UtilUnitTest.createHWChinaAccount('acc');
        acc = [select id,Is_Named_Account__c,Named_Account_Character__c,
        	Named_Account_Level__c,Named_Account_Property__c from Account];
        System.assertEquals(false,acc.Is_Named_Account__c);
        System.assertEquals(null,acc.Named_Account_Character__c);
        System.assertEquals(null,acc.Named_Account_Level__c);
        System.assertEquals(null,acc.Named_Account_Property__c);
        
        Named_Account__c oldNA = UtilUnitTest.createActiveNamedAccount(acc); 
        acc = [select id,Is_Named_Account__c,Named_Account_Character__c,
        	Named_Account_Level__c,Named_Account_Property__c from Account];
        System.assertEquals(true,acc.Is_Named_Account__c);
        
    }
    
    
	//NA属性未变,失效旧的NA标签，同时更新ACC上的NA相关字段信息，同步刷新相关联的机会点NA属性
    static testMethod void testCreate2() {//with child,is na not changed
    	String RecordTypeId = UtilUnitTest.getRecordTypeIdByName('Account',
    		 'Huawei China Customer');
        Account acc = UtilUnitTest.createHWChinaAccount('acc');
        Account child = UtilUnitTest.createHWChinaAccount('child');
        child.ParentId = acc.id;
        update child;
        acc = [select id,Is_Named_Account__c,Named_Account_Character__c,
        	Named_Account_Level__c,Named_Account_Property__c from Account where id = :acc.id];
        System.assertEquals(false,acc.Is_Named_Account__c);
        System.assertEquals(null,acc.Named_Account_Character__c);
        System.assertEquals(null,acc.Named_Account_Level__c);
        System.assertEquals(null,acc.Named_Account_Property__c);
        
        Named_Account__c oldNA = UtilUnitTest.createActiveNamedAccount(acc); 
        acc = [select id,Is_Named_Account__c,Named_Account_Character__c,
        	Named_Account_Level__c,Named_Account_Property__c from Account where id = :acc.id];
        System.assertEquals(true,acc.Is_Named_Account__c);
        NamedAccountAfterInsertHandler.isFirstRun = true;
        Named_Account__c newNA = UtilUnitTest.createActiveNamedAccount(acc);
        
        child = [select id,Is_Named_Account__c,Named_Account_Character__c,Named_Account__c,
        	Named_Account_Level__c,Named_Account_Property__c from Account where id = :child.id];
        System.assertEquals(true,child.Is_Named_Account__c);
        Named_Account__c na = [select id,Is_Named_Account__c from Named_Account__c where Account_Name__c = :child.id];
        System.assertEquals(true,na.Is_Named_Account__c);
    }
    
    //NA属性变化.失效旧的NA标签，同时更新ACC上的NA相关字段信息
    static testMethod void testCreate3() {//with child,is na changed(VR上限制为禁止NA客户关联到非NA客户)
    	System.debug('enter the testcreate3 method:------------------');
    	String RecordTypeId = UtilUnitTest.getRecordTypeIdByName('Account',
    		 'Huawei China Customer');
        Account acc = UtilUnitTest.createHWChinaAccount('acc');
        Account child = UtilUnitTest.createHWChinaAccount('child');
        child.ParentId = acc.id;
        update child;
        acc = [select id,Is_Named_Account__c,Named_Account_Character__c,
        	Named_Account_Level__c,Named_Account_Property__c from Account where id = :acc.id];
        System.assertEquals(false,acc.Is_Named_Account__c);
        System.assertEquals(null,acc.Named_Account_Character__c);
        System.assertEquals(null,acc.Named_Account_Level__c);
        System.assertEquals(null,acc.Named_Account_Property__c);
        Named_Account__c oldNA = UtilUnitTest.createInActiveNamedAccount(acc); 
        acc = [select id,Is_Named_Account__c,Named_Account_Character__c,
        	Named_Account_Level__c,Named_Account_Property__c from Account where id = :acc.id];
        System.assertEquals(oldNA.Is_Named_Account__c,acc.Is_Named_Account__c);
        Test.startTest();
        //NamedAccountAfterInsertHandler.isFirstRun = true;//have to reset the flag
        System.debug('before insert the newna:--------------');
        NamedAccountAfterInsertHandler.isFirstRun = true;
        AccountNAChangedHandler.isFirstRun = true;
        Named_Account__c newNA = UtilUnitTest.createActiveNamedAccount(acc);
        System.debug('the newNA:---------'+ newNA);
        Test.stopTest();
        
        child = [select id,Is_Named_Account__c,Named_Account_Character__c,Named_Account__c,
        	Named_Account_Level__c,Named_Account_Property__c from Account where id = :child.id];
        System.assertEquals(true,child.Is_Named_Account__c);
        //System.assertEquals(newNA.id,child.Named_Account__c);
    }
    
    static testMethod void cascadeInsert(){
    
        Account grandParent = UtilUnitTest.createHWChinaAccount('grandParent');
        
        Account parent = UtilUnitTest.createHWChinaAccount('parent');
        parent.ParentId = grandParent.Id;
        update parent;

        Account child = UtilUnitTest.createHWChinaAccount('child');
        child.ParentId = parent.id;
        update child;
        
        Account son = UtilUnitTest.createHWChinaAccount('son');
        son.ParentId = child.id;
        update son;
        son = [select id,Is_Named_Account__c from Account where id = :child.id];
        System.assertEquals(false,son.Is_Named_Account__c);  
        Test.startTest();
        Named_Account__c newNA = UtilUnitTest.createActiveNamedAccount(grandParent);
        Test.stopTest();
        son = [select id,Is_Named_Account__c from Account where id = :child.id];
        System.assertEquals(true,son.Is_Named_Account__c);        
        
     }
     
     
     static testMethod void bigData(){
     	List<Account> accs = new List<Account>();
     	
     }
     
}