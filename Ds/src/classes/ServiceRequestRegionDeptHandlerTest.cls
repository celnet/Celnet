@isTest
private class ServiceRequestRegionDeptHandlerTest {

    static testMethod void insertTest() {
    	Account acc = UtilUnitTest.createAccount();
    	acc.Region_Dept__c = 'test';
    	update acc;
    	Contact con = UtilUnitTest.createContact(acc);
	    Case c = UtilUnitTest.createCCRRequest(con);
    	c = [select id,Region_Dept__c from Case where id =:c.id];
    	System.assertEquals('test',c.Region_Dept__c);
    }	
    
    static testMethod void updateTest(){
    	Account acc = UtilUnitTest.createAccount();
    	acc.Region_Dept__c = 'test';
    	update acc;
    	Case c = UtilUnitTest.createChinaServiceRequest();
    	c.AccountId = acc.id;
    	update c;
    	c = [select id,Region_Dept__c from Case where id =:c.id];
    	System.assertEquals('test',c.Region_Dept__c);
    }
    
    static testMethod void cascadeUpdateTest(){
    	Account acc = UtilUnitTest.createAccountWithName('testA');
    	Account acc2 = UtilUnitTest.createAccountWithName('testB');
    	acc2.Region_Dept__c = 'test';
    	update acc2;
    	Case c = UtilUnitTest.createChinaServiceRequest();
    	Work_Order__c wo = UtilUnitTest.createOnSiteWorkOrder(acc,c);
    	wo = [select id,Region_Dept__c from Work_Order__c where id =:wo.id];
    	System.assertNotEquals('test',wo.Region_Dept__c);
    	ServiceRequestRegionDeptHandler.shouldRun = true;//reset the flag to make trigger work
    	CommonConstant.serviceRequestTriggerShouldRun = true;
    	c.AccountId = acc2.id;
    	Test.startTest();
    	update c;
    	Test.stopTest();
    	c = [select id,Region_Dept__c from Case where id =:c.id];
    	System.assertEquals('test',c.Region_Dept__c);
    	wo = [select id,Region_Dept__c from Work_Order__c where id =:wo.id];
    	System.assertEquals('test',wo.Region_Dept__c);
    }
}