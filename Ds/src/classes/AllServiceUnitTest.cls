/*****************************************************
* Name             : Test_AccountRegionDept
* Object           : Account
* Requirement      : 
* Purpose          : 
* Author           : Iverson gao
* Create Date      : 07/16/2012
* Modify History   
* 2013-2-5 Polaris Yu
*		1. Add an argument to createAccount()
**********************************************************/
@isTest
public class AllServiceUnitTest {
	 // Gets the object record type id according to the recordType name.
     public static String getRecordTypeIdByName(String sObjectType, String recordTypeName){
        Schema.SobjectType d = Schema.getGlobalDescribe().get(sObjectType);
        Map<String, Schema.RecordTypeInfo> rtMapByName = d.getDescribe().getRecordTypeInfosByName();
        Schema.RecordTypeInfo rtByName = rtMapByName.get(recordTypeName);
        return rtByName.getRecordTypeId();
     }
     
     //create a new account
     public static Account createAccount(String aName, String regionDept) {
        String recordtypeId = getRecordTypeIdByName('Account', 'HW-(Int CSS) Customer');
        Account acc = new Account(Name = aName, CurrencyIsoCode ='USD',
                                  Is_Ultimate_Parent__c = true,
                                  Global_Account_Manager__c = '005900000016UWc',
                                  Region__c ='India',Industry = 'Transportation',
                                  RecordTypeId = recordtypeId,
                                  Region_Dept__c = regionDept,
                                  blacklist_type__c='Pending'
                                  );        
        insert acc;
        return acc;
     }

     //create a new Contact
     public static Contact createContact(Account acc){
        String recordtypeId = getRecordTypeIdByName('Contact', 'HW-(Int CSS) Contact');
        Contact con = new Contact(LastName = 'TEST Contact',
                                  AccountId =acc.Id,                                  
                                  MailingState='china',
                                  MailingCity='shanghai',
                                  MailingStreet='caoyang road',
                                  MailingPostalCode ='200000',
                                  Phone='14789714199',
                                  RecordTypeId =recordtypeId
                                  );
        insert con; 
        return con;
     }
     
     //create a new ServiceContract
     public static CSS_Contract__c createServiceContract(Account acc, Contact con){
        String recordtypeId = getRecordTypeIdByName('CSS_Contract__c', 'HW-(Int CSS) Service Contract');
        CSS_Contract__c ccc = new CSS_Contract__c(Name='1234567891234567',
                                                  Project_Name__c='Test Project',
                                                  Type__c='International',Contract_Type__c='Standard Contract',
                                                  Account_Name__c=acc.Id,
                                                  Contract_Group__c='Service Contract',
                                                  Contact_Person__c=con.Id,
                                                  Contract_Signed_Date__c=Date.today()
                                                 );                                                
        insert ccc;
        return ccc;
     }
     
     //create a new sfp 
     public static SFP__c createSfp(CSS_Contract__c ccc){
        String recordtypeId = getRecordTypeIdByName('SFP__c', 'HW-(Int CSS) Installation SFP');
        SFP__c sfp = new SFP__c(SFP_Unique_No__c='Test SFP',
                                Type__c='International',
                                Status__c='Active',Account_Name__c=ccc.Account_Name__c,
                                Name='12345678901234',Contract_No__c=ccc.Id
                               );          
        insert sfp;
        return sfp;
     }
     
     // Create a new Equipment Archive
     public static Installed_Asset__c createEA(CSS_Contract__c ccc){
     	String recordtypeId = getRecordTypeIdByName('Installed_Asset__c',
     						  'HW-(Int CSS) Equipment Archive');
     	Installed_Asset__c iac = new Installed_Asset__c(Name='12345678956789',
                                 Account_Name__c=ccc.Account_Name__c,
                                 BOM_Code__c='Y0101203', Barcode1__c='12',
                                 Quantity__c=8,Contracts_No__c=ccc.Id
                                 );
        insert iac;
        return iac;
     }
     
     // Create a new Work Order Under SFP
     public static Work_Order__c createWO_1(Account acc, SFP__c sfp){
     	String woRcdTypeId = getRecordTypeIdByName('Work_Order__c'
     						 , 'HW-(Int CSS) Installation Work Order');
        Work_Order__c wo = new Work_Order__c(Name = 'Test', Type__c = 'International'
        				   , Account_Name__c = acc.Id, SFP_No__c = sfp.Id
        				   , Contract_No__c = sfp.Contract_No__c
        				   , Outsource_Onsite_Service__c = 'Unassigned'
        				   , RecordTypeId = woRcdTypeId);
        insert wo;
        return wo;
     }
     
     // Create a new Work Order Under Service Request
     public static Work_Order__c createWO_2(Account acc, Case sr){
     	String woRcdTypeId = getRecordTypeIdByName('Work_Order__c'
     						 , 'HW-(Int CSS) On Site Support Work Order');
        Work_Order__c wo = new Work_Order__c(Name = 'Test', Type__c = 'International'
        				   , Account_Name__c = acc.Id
        				   , Service_Request_No__c = sr.Id
        				   , Outsource_Onsite_Service__c = 'Unassigned'
        				   , RecordTypeId = woRcdTypeId);
        insert wo;
        return wo;
     }
     
     // Create a new ServiceContract with SFP, Equipment Archive, Work Order and
     //  Service Contract Product PO
     public static CSS_Contract__c createSCwithItems(Account acc, Contact con){
        String recordtypeId = getRecordTypeIdByName('CSS_Contract__c', 'HW-(Int CSS) Service Contract');
		// New Service Contract
        CSS_Contract__c ccc = AllServiceUnitTest.createServiceContract(acc, con);
        
        // New SFP
        SFP__c sfp = AllServiceUnitTest.createSfp(ccc);
        
        // New Equipment Archive
        Installed_Asset__c ea = AllServiceUnitTest.createEA(ccc);
        
        // New Work Order
        Work_Order__c wo = AllServiceUnitTest.createWO_1(acc, sfp);
        
        // New Service Contract Product PO
        Product_HS__c  master= UtilUnitTest.createProductMaster('1');
        Contract_Product_PO__c cpp = new Contract_Product_PO__c(Product_Search__c=master.Id,                                                               
                                     Contract_No__c=ccc.Id);
        
        return ccc;
     }
     
     // 1.test on Test_Tig_OnContact
     public static testmethod void Test_Tig_OnContact(){
     	// New Contact
     	Account acc_1 = AllServiceUnitTest.createAccount('Test Account 1'
     					, 'Russia Enterprises Business Dept');
     	CommonConstant.changedFromAccount = false;
        Contact con = AllServiceUnitTest.createContact(acc_1);
        update con;
        
        // Change the related Account of the Contact and update it
        Account acc_2 = AllServiceUnitTest.createAccount('Test Account 2'
        				, 'Southern-East Asia Enterprise Business Dept');
        CommonConstant.changedFromAccount = false;
        con.AccountId = acc_2.Id;
        update con;
     }
     
     // 2.test on Tig_OnServiceContract
     public static testmethod void Test_OnServiceContract(){
     	// New Service Contract with SFP, Equipment Archive and Work Order
     	Account acc_1 = AllServiceUnitTest.createAccount('Test Account 1'
     					, 'Russia Enterprises Business Dept');
     	CommonConstant.changedFromAccount = false;
     	Contact con = AllServiceUnitTest.createContact(acc_1);
        CSS_Contract__c ccc = AllServiceUnitTest.createSCwithItems(acc_1, con);
        
        // Change the related Account of the Service Contract and update it
        Account acc_2 = AllServiceUnitTest.createAccount('Test Account 2'
        			  , 'Southern-East Asia Enterprise Business Dept');
        CommonConstant.changedFromAccount = false;
        ccc.Account_Name__c = acc_2.Id;
        update ccc;
     }
     
     // 3.test on Tig_OnEquipmentArchive
     public static testmethod void Test_OnEquipmentArchive(){
     	// New Equipment Archive
        String recordtypeId = getRecordTypeIdByName('Installed_Asset__c'
        					  , 'HW-(Int CSS) Equipment Archive');
        Account acc = AllServiceUnitTest.createAccount('Test Account 3'
        			  , 'Southern-East Asia Enterprise Business Dept');
        CommonConstant.changedFromAccount = false;
        Contact con = AllServiceUnitTest.createContact(acc);
        CSS_Contract__c ccc = AllServiceUnitTest.createServiceContract(acc, con);
        CommonConstant.changedFromContract = false;
        Installed_Asset__c iac = AllServiceUnitTest.createEA(ccc);
        
        // Test Tig_AccountRegionDept
        acc.Region_Dept__c = 'Russia Enterprises Business Dept';
        update acc;
        
        // Set the Contracts No of the Equipment Archive to null and update it
        CommonConstant.changedFromAccount = false;
        iac.Contracts_No__c = null;
        update iac;
     }
     
     //4.test on Tig_OnServiceContractPO
     public static testmethod void Test_OnServiceContractPO(){
     	Account acc = AllServiceUnitTest.createAccount('Test Account 1'
     				  , 'Southern-East Asia Enterprise Business Dept');
     	CommonConstant.changedFromAccount = false;
     	Contact con = AllServiceUnitTest.createContact(acc);
        CSS_Contract__c ccc = AllServiceUnitTest.createServiceContract(acc, con);
        CommonConstant.changedFromContract = false;
        Product_HS__c  master= UtilUnitTest.createProductMaster('1');
        Contract_Product_PO__c cpp = new Contract_Product_PO__c(Product_Search__c=master.Id,                                                               
                                                                Contract_No__c=ccc.Id                                                            
                                                               );
        insert cpp;
     }
     
     // 5.test on Tig_OnServiceRequest
     public static testmethod void Test_OnServiceRequest(){
     	// New Service Request with Work Order
        String recordtypeId = getRecordTypeIdByName('Case'
        					  , 'HW-(Int CSS) External Post Sales-CCR');
        Account acc = AllServiceUnitTest.createAccount('Test Account 1'
        			  , 'Russia Enterprises Business Dept');
        CommonConstant.changedFromAccount = false;
        Contact con = AllServiceUnitTest.createContact(acc);
        Case cas = UtilUnitTest.createChinaServiceRequest();
        cas.Status='Waiting for response';
        cas.AccountId=acc.Id;
        cas.Customer_Type__c='Huawei';
        cas.ContactId=con.Id;
        update cas;
        
        // Test Tig_AccountRegionDept
        acc.Region_Dept__c = 'India Enterprise Business Dept';
        update acc;
        
        Work_Order__c wo = AllServiceUnitTest.createWO_2(acc, cas);
        
        // Change the related Account of the Service Request and update it
        Account acc2 = AllServiceUnitTest.createAccount('Test Account 2'
        			   , 'Southern-East Asia Enterprise Business Dept');
        CommonConstant.changedFromAccount = false;
		cas.AccountId = acc2.Id;
		update cas;
     }
     
     // 6.test on Tig_OnSFP
     public static testmethod void Test_OnSFP(){
     	Account acc = AllServiceUnitTest.createAccount('Test Account 1'
        			  , 'Russia Enterprises Business Dept');
        CommonConstant.changedFromAccount = false;
        Contact con = AllServiceUnitTest.createContact(acc);
        CSS_Contract__c ccc = AllServiceUnitTest.createServiceContract(acc, con);
        CommonConstant.changedFromContract = false;
        SFP__c sfp = AllServiceUnitTest.createSfp(ccc);
     }
     
     // 7.test on Tig_OnWorkOrder
     public static testmethod void Test_OnWorkOrder(){      
        TestTig_WorkOrder.myUnitTest();
     }

     // 8.Test on Tig_OnServiceProduct
     public static testmethod void Test_OnServiceProduct(){
        Account acc = AllServiceUnitTest.createAccount('Test Account 1'
        			  , 'Russia Enterprises Business Dept');
        CommonConstant.changedFromAccount = false;
        Contact con = AllServiceUnitTest.createContact(acc);
        CSS_Contract__c ccc = AllServiceUnitTest.createServiceContract(acc, con);
        CommonConstant.changedFromContract = false;
        SFP__c sfp = AllServiceUnitTest.createSfp(ccc);
        
        // Test Tig_AccountRegionDept
        acc.Region_Dept__c  = 'Southern-East Asia Enterprise Business Dept';
        update acc;
        
        Service_Products_Installation__c spic = new Service_Products_Installation__c(SFP_No__c=sfp.Id,
                                                                                     Quantity__c=8
                                                                                    );
        insert spic;
     }
}