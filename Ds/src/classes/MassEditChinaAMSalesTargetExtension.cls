/**
 * @Purpose ：销管批量维护中国区客户经理销售目标
 * @Author : Steven
 * @Date :   2014-08-04
 */
public class MassEditChinaAMSalesTargetExtension { 
	//构造Lead行的展示类
	public class TargetRow {
		private String result; //分发结果标识，Success，Ignore，Failed
		public Sales_Target_Accomplishment__c target {get; set;}
		public String Message {get; set;}
		public Boolean isManager {get; set;} //管理角色可以维护所有数据
		public String ResultLabel {
			get {
				if(this.result == 'Ignore')  return '忽略';
				if(this.result == 'Success') return '成功';
				if(this.result == 'Failed')  return '失败';
				return null;
			}
		}
		public Boolean Editable {
			get {
				if(this.target == null) return false; 
				if(this.target.RecordTypeId != CONSTANTS.SALESTARGETREGIONRECORDTYPE) return false; 
				if(this.target.OwnerID != UserInfo.getUserId() && !this.isManager) return false; 
				if(this.target.Inactive__c) return false; 
				return true;
			}
		}
	}
	public Boolean IsSubmitted {get; set;}    //根据是否已经提交判断是否显示状态信息列
	private List<Sales_Target_Accomplishment__c> selTargets; //视图界面选择批量修改的目标
	private List<TargetRow> selTargetRows; //批量编辑界面显示行
	public List<TargetRow> SelectedTargetRows { //界面展示线索
		get { return this.selTargetRows; }
	}

	/**
	 * 构造方法初始化相关参数
	 */
	public MassEditChinaAMSalesTargetExtension(ApexPages.StandardSetController setController) {
  	this.IsSubmitted = false;
  	this.selTargetRows = new List<TargetRow>();
  	this.selTargets = (Sales_Target_Accomplishment__c[])setController.getSelected();
		//Step1：获取所有选择的目标的ID
		List<ID> selectedIds = new List<ID>();
		for(Integer i = 0; i < this.selTargets.size(); i++ ) {
			selectedIds.add(this.selTargets[i].Id);
		}
		//Step2：根据ID查询出Target界面显示的字段值
  	Set<String> fieldNameSet = new Set<String>();
		fieldNameSet.add('Name');
		fieldNameSet.add('Inactive__c');
		fieldNameSet.add('OwnerId');
		fieldNameSet.add('Owner.Name');
		fieldNameSet.add('Year__c');
		for(Schema.FieldSetMember field : SObjectType.Sales_Target_Accomplishment__c.FieldSets.AccountManagerTargetMassEdit.getFields()) {
			fieldNameSet.add(field.FieldPath);
		}
		String soql = 'Select Id';
		for(String fieldName : fieldNameSet) {
			soql += ', ' + fieldName;
		}
		soql += ' FROM Sales_Target_Accomplishment__c Where ID In: selectedIds';
		this.selTargets = (Sales_Target_Accomplishment__c[])Database.query(soql);
		//Step3：根据查询出的Target数据拼装TargetRows
		Boolean isManager = isManagerRole();
  	for(Sales_Target_Accomplishment__c t: this.selTargets) {
  		TargetRow row = new TargetRow();
  		row.target = t;
  		row.isManager = isManager;
  		this.selTargetRows.add(row);
  	}
	}
	
	/**
	 * 界面点击提交按钮，批量保存客户经理目标
	 */
	public PageReference submit() {
		List<Sales_Target_Accomplishment__c> updateTargets = new List<Sales_Target_Accomplishment__c>();
		List<TargetRow> submitRows = new List<TargetRow>();
		//Step1: 判断界面的哪些目标可以进行更新
		for(TargetRow row: selTargetRows) {
  		if(!row.Editable) {
				row.Result = 'Ignore';
				row.Message = '没有编辑权限';
				continue;
			}
			submitRows.add(row);
  	}
		//Step2: 逐个更新客户经理目标并记录状态
		for(TargetRow tr : submitRows) {
			try {
				update tr.target;
				tr.Result = 'Success';
				tr.Message = '';
			} catch(Exception e) {
				tr.Result = 'Failed';
				tr.Message = e.getMessage();
			} 
		}
  	this.IsSubmitted = true;
		return null; //完成后停留在页面显示每个行的结果
	}
	
	/**
	 * 判断当前用户是否具有超级权限(可以编辑所有目标)
	 */
	private Boolean isManagerRole(){
		Boolean result = false;
		User currentUser = [select Id,Profile.Name from User where id =: UserInfo.getUserId()];
		if(currentUser.Profile.name.contains('(Huawei China)销售管理策略主管')|| 
			 currentUser.Profile.name.contains('系统管理员')|| 
			 currentUser.Profile.name.contains('System Administrator')){
			result = true;
		}
		return result;
	}

}