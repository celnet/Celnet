/*
Author: tommy.liu@celnet.com.cn
Created On: 2014-5-30
Function: Sync NA Account to AN List
*/
global class MassNAListSyncBatch implements Database.Batchable<sObject>, Schedulable {
	public String query;
	
	global MassNAListSyncBatch(){
	}

  global Database.QueryLocator start(Database.BatchableContext BC) {
     return Database.getQueryLocator(query);
  }

  global void execute(Database.BatchableContext BC, List<SObject> scope) {
  	List<NA_List__c> naList = new List<NA_List__c>();
  	for(SObject obj : scope) {
  		Account acc = (Account)obj;
  		NA_List__c na = new NA_List__c();
  		na.Name = acc.Name;
  		na.Account__c = acc.ID;
  		na.Account_ID__c = acc.ID; //upsert用
  		//na.National_Type__c
  		naList.add(na);
  	}
  	upsert naList Account_ID__c;
  }

  global void finish(Database.BatchableContext BC) {
  }
  
	global void execute(SchedulableContext SC) {
  	String query = 
  		'Select ID, Name From Account Where Is_Named_Account__c = true And RecordTypeId =\''+ CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE+'\'';
  	MassNAListSyncBatch naSyncBatch = new MassNAListSyncBatch();
  	naSyncBatch.query = query;
		Database.executeBatch(naSyncBatch, 1000); 
  }
}