public with sharing class OpportunityTeamMemberArchiveForBIHandler implements Triggers.Handler{ //after delete
	public void handle(){
		Set<Id> oppIds = new Set<Id>();
		Set<Id> chinaOppIds = new Set<Id>();
		Set<Id> overseaOppIds = new Set<Id>();
		//Step1:获取所有的删除了机会点团队成员的机会点ID
		for(OpportunityTeamMember member : (List<OpportunityTeamMember>)trigger.old){
			oppIds.add(member.OpportunityId);
		}
		
		//Step2:获取这些机会点的类型，只处理海外和中国区两种类型
		List<Opportunity> oppList = [Select Id,RecordTypeId From Opportunity Where Id IN: oppIds];
		for(Opportunity opp : oppList){
			if(opp.RecordTypeId == CONSTANTS.CHINAOPPORTUNITYRECORDTYPE) chinaOppIds.add(opp.Id);
			if(opp.RecordTypeId == CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE) chinaOppIds.add(opp.Id);
			if(opp.RecordTypeId == CONSTANTS.HUAWEIOVERSEAOPPRECORDTYPE) overseaOppIds.add(opp.Id);
			if(opp.RecordTypeId == CONSTANTS.HUAWEIOVERSEADEALOPPRECORDTYPE) overseaOppIds.add(opp.Id);
		}
		if(chinaOppIds.isEmpty() && overseaOppIds.isEmpty()) return;
		
		//Step3:分中国区和海外两种类型记录数据删除历史
		List<Data_Maintain_History__c> archiveList = new List<Data_Maintain_History__c>();
		for(OpportunityTeamMember otm : (List<OpportunityTeamMember>) trigger.old){
			if(chinaOppIds.contains(otm.OpportunityId)){
				Data_Maintain_History__c archive = new Data_Maintain_History__c();
				archive.Record_ID__c = otm.Id;
				archive.Operation_Type__c = 'Delete';
				archive.Data_Type__c = 'China Opportunity Team(BI)';
				archiveList.add(archive);
			}
			if(overseaOppIds.contains(otm.OpportunityId)){
				Data_Maintain_History__c archive = new Data_Maintain_History__c();
				archive.Record_ID__c = otm.Id;
				archive.Operation_Type__c = 'Delete';
				archive.Data_Type__c = 'Oversea Opportunity Team';
				archiveList.add(archive);
			}
		}
		if(archiveList.size() > 0){
			insert archiveList;
		}
	}
}