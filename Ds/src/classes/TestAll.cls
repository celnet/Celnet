@isTest
public class TestAll {
    public static testMethod void myUnitTest() {
        Test.startTest();
        //Account acc=[select a.id,a.name from Account a where a.name='CC' limit 1];
        Account acc=[select a.id,a.name from Account a limit 1];
        //name='Nation Volume Project';
        RecordType rt1=[select id,name from RecordType where id='012100000004TIeAAM' limit 1];// where name='International Volume Project'
        //System.debug('recordType\'s, name:rt1='+rt1.Name);
        System.assertEquals(rt1.name, 'C&AP Volume Project');
        Opportunity o1=new Opportunity();
        o1.name='KKK';
        o1.RecordTypeId=rt1.id;
        //o1.RecordTypeId='012100000004TIRAA2';
        o1.AccountId=acc.id;
        o1.Primary_Channel_Partner__c=acc.id;
        //o1.Primary_Channel_Partner__r=acc; 
        o1.StageName='2-Billed';
        o1.CloseDate=Date.valueOf('2009-6-6');
        o1.Opportunity_type__c='Direct Sales';//it is added on 2013/05/25
        insert o1;
        
        //rt2.id='012100000004TIRAA2';
        //name='International Volume Project';
        RecordType rt2=[select id,name from RecordType where id='012100000004TIRAA2' limit 1];// where name='Nation Volume Project'
        //System.debug('recordType\'s, name:rt2='+rt2.Name);
        System.assertEquals(rt2.name, 'International Volume Opportunity');
        //Opportunity o2=[select o.id,o.name from Opportunity o where o.name='test' limit 1];
        Opportunity o2=[select o.id,o.name from Opportunity o where o.Owner.isactive=true and o.IsClosed!=true limit 1];
        o2.name='TT';
        o2.RecordTypeId=rt2.id;
        o2.AccountId=acc.id;
        o2.Primary_Channel_Partner__c=acc.id;
        o2.StageName='2-Billed';
        o2.CloseDate=Date.valueOf('2009-6-6');       
        update o2;    
        Test.stopTest();
    }
}