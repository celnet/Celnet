public without sharing class EIPSYNCCONSTANTS {
     public static final String SINGLEEIPENDPOINT = 'https://b2b.huawei.com:3777/SOAP?host=Huawei&tpname=SF&opid=IssueReq/1/SRequest-Response&transid=';
     public static final String BATCHEIPENDPOINT = 'https://b2b.huawei.com:3777/SOAP?host=Huawei&tpname=SF&opid=IssueReq/1/Request-Response&transid=';
     public static final String GET_ENDPOINT = 'https://b2b.huawei.com:3777/SOAP?host=Huawei&tpname=SF&opid=IssueReq/1/GRequest-Response&transid=';
     public static final String UPDATEENDPOINT = 'https://b2b.huawei.com:3777/SOAP?host=Huawei&tpname=SF&opid=IssueReq/1/UpdateICareSR&transid=';
     public static final Integer TIMEOUTMS = 35*1000;//ms  //sit测试时暂时修改为20s
     public static final Integer BATCHTIMEOUTMS = 120*1000;//ms
     public static final String VALIDATIONMSG ='Request not confirming to schema';
}