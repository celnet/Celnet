/*
*@Purpose:国内、国际的用户不能互转，即国内用户不能转给国际用户，国际用户不能转给国内用户
*@Author:Brain Zhang
*Create Date：2013-9
*/
public without sharing class WorkOrderOwnerCheckHandler implements Triggers.Handler {
	public void handle(){
		Set<Id> userIds = new Set<Id>{(Id)UserInfo.getUserId()}; 
		for(Work_Order__c wo:(List<Work_Order__c>)Trigger.new){
			 if(wo.OwnerId != ((Work_Order__c)Trigger.oldmap.get(wo.id)).ownerId){
			 	userIds.add(wo.OwnerId);
			 	userIds.add(((Work_Order__c)Trigger.oldmap.get(wo.id)).ownerId);
			 }
		}
		if(userIds.size() > 1){//owner发生变化
			Map<id,User> userMap= new Map<id,User>([select u.id,u.name,u.Region_Office__c,u.MobilePhone
                ,u.UserRole.Name,u.UserRoleId 
                ,u.Profile.Name, u.ProfileId
                from User u 
                where u.id in:userIds]);
      for(Work_Order__c wo:(List<Work_Order__c>)Trigger.new){
			 if(wo.OwnerId != ((Work_Order__c)Trigger.oldmap.get(wo.id)).ownerId){
		      User newUser=userMap.get(wo.OwnerId);
		      User oldUser=userMap.get(((Work_Order__c)Trigger.oldmap.get(wo.id)).ownerId);
			 		if(oldUser!=null && oldUser.UserRole!=null && newUser!=null && newUser.UserRole!=null){
                	String msg='';
                	if(oldUser.UserRole.Name.indexOf('Int')>-1 && newUser.UserRole.Name.indexOf('(N)')>-1){
                		msg='国际用户不能转给国内用户';
                	}
                	if(oldUser.UserRole.Name.indexOf('(N)')>-1 && newUser.UserRole.Name.indexOf('Int')>-1){
                		msg='国内用户不能转给国际用户';
                	}
                	if(!msg.equals(''))
                		wo.addError(msg);
                		//考虑加入控制禁止后续代码执行
                		
             //3.当前用户角色不包含SD或External时，新的Owner用户的角色名称中不能包含External   		
             User currentUser=userMap.get(UserInfo.getUserId());
             if(currentUser.UserRole!=null && 
                    newUser.UserRole!=null &&
                    newUser.Id!=oldUser.Id ){ 
                    if( currentUser.UserRole.Name.indexOf('SD')<0 &&
                        currentUser.UserRole.Name.indexOf('External')<0 && 
                        newUser.UserRole.Name.indexOf('External')>0 ){
                        wo.addError('Only SD and external users can assign Work Order to external users. 只有SD和外部用户可以将工单派给外部用户。');
                    }
                }
         }
             wo.Owner_Mobile__c=newUser.MobilePhone; //4.设置Owner_Mobile__c,此字段在email alert中有用到
             
             if(newUser.Profile.Name.indexOf('SPM')>0 ||newUser.Profile.Name.indexOf('TM')>0 ||newUser.Profile.Name.indexOf('RSC')>0  ){
             	wo.SPM_Region__c=newUser.Region_Office__c; //5.owner字段变化时,如果是SPM/TM/RSC用户,重设SPM_Region__c字段
             }
             if(newUser.Profile.Name.indexOf('RSC')>0){
              wo.RSC_Region__c = newUser.Region_Office__c; //6.owner字段变化时,如果是RSC用户,重设RSC_Region__c字段
             }
			 }
      
      }
      
		}
	}
}