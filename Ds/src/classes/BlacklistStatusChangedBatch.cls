/*
* @Purpose : it is for changing the status on the blacklist object.after the accounts has been 
* checked, all blacklist records' status are modified from new to old after they were used 
* for checking.
*
* @Author : Jen Peng
* @Date : 		2013-5-8
*/
global class BlacklistStatusChangedBatch implements Database.Batchable<sObject>,Schedulable{
	public String query;
	
	global Database.QueryLocator start(Database.BatchableContext bc){
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC, list<sObject> scope){
		for(sObject s : scope){
			BlackList__c bl = (BlackList__c)s;
			bl.status__c='old';
		}
		update scope;
	}
	
	global void finish(Database.BatchableContext BC){
	
	}
	
	global void execute(SchedulableContext SC){
		BlacklistStatusChangedBatch bscb = new BlacklistStatusChangedBatch();
		bscb.query='select id,Name,status__c from BlackList__c where status__c<>\'del\'';
		Id batchId = Database.executeBatch(bscb,50);
	}
}