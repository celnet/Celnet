/**
 * Author           : Brain Zhang(BLS)
 * Create Date      : 2013/3/5
 * Modify History   : 
 **/
@isTest
private class AccountNAChangedTest {
	
    static testMethod void save() {//account with parent
    	//set up the record type if needed
    	String RecordTypeId = UtilUnitTest.getRecordTypeIdByName('Account',
    		 'Huawei China Customer');
        Account acc = UtilUnitTest.createHWChinaAccount('acc');
        Account child = UtilUnitTest.createHWChinaAccount('child');
        child.ParentId = acc.id;
        update child;
        Integer childNum = [select count() from Account where parentid = :acc.id];
        System.assertEquals(1,childNum);
        
        acc = [select Is_Named_Account__c from Account where id = :acc.id];
        System.assertEquals(false,acc.Is_Named_Account__c);
        AccountNAChanged controller = new AccountNAChanged(
        	new ApexPages.StandardController(acc));
        Test.setCurrentPage(Page.AccountNAChanged);
        System.assertEquals(true,controller.ableToModify);
        
        //调用save方法后,每个account应该生成一条对应的named account记录
        Test.startTest();
        controller.account.Is_Named_Account__c = true;
        System.debug('the na:---' + controller.na);
        controller.save();
        Test.stopTest();
        List<Named_Account__c> na = [select id,Is_Named_Account__c from Named_Account__c 
        	];
        System.assertEquals(2,na.size());
        System.assertEquals(true,na.get(0).Is_Named_Account__c);
        acc = [select Is_Named_Account__c from Account where id = :acc.id];
        System.assertEquals(true,acc.Is_Named_Account__c);
        
    }
    
    static testMethod void save2() {//account without parent
    	//set up the record type if needed
    	String RecordTypeId = UtilUnitTest.getRecordTypeIdByName('Account',
    		 'Huawei China Customer');
        Account acc = UtilUnitTest.createHWChinaAccount('acc');
        Account child = UtilUnitTest.createHWChinaAccount('child');
        child.ParentId = acc.id;
        child.RecordTypeId = RecordTypeId;
        update child;
        Integer childNum = [select count() from Account where parentid = :acc.id];
        System.assertEquals(1,childNum);
        
        acc = [select Is_Named_Account__c from Account where id = :acc.id];
        System.assertEquals(false,acc.Is_Named_Account__c);
        AccountNAChanged controller = new AccountNAChanged(
        	new ApexPages.StandardController(acc));
        Test.setCurrentPage(Page.AccountNAChanged);
        System.assertEquals(true,controller.ableToModify);
        controller.account.Is_Named_Account__c = true;
        
        //调用save方法后,应该生成一条对应的named account记录
        controller.save();
        List<Named_Account__c> na = [select id,Is_Named_Account__c from Named_Account__c 
        	where Account_Name__c = :acc.id];
        System.assertEquals(1,na.size());
        System.assertEquals(true,na.get(0).Is_Named_Account__c);
        acc = [select Is_Named_Account__c from Account where id = :acc.id];
        System.assertEquals(true,acc.Is_Named_Account__c);
        
        Integer nas = [select count() from Named_Account__c];
        //System.assertEquals(2,nas);
        child = [select Is_Named_Account__c from Account where id = :child.id];
        System.assertEquals(true,child.Is_Named_Account__c);
        
    }
    
    static testMethod void save3(){//account without parent,from isna to not 
    	Account acc = UtilUnitTest.createHWChinaAccount('acc');
    	Named_Account__c na = new Named_Account__c(Account_Name__c = acc.id,
    		Is_Named_Account__c = true);
    	insert na;
    	acc = [select Is_Named_Account__c from Account where id = :acc.id];
      System.assertEquals(true,acc.Is_Named_Account__c);
      AccountNAChanged controller = new AccountNAChanged(
        	new ApexPages.StandardController(acc));
      Test.setCurrentPage(Page.AccountNAChanged);
      controller.account.Is_Named_Account__c = false;
      Test.startTest();
      System.debug('going to save ,na is:---'  + controller.na);
      AccountNAChangedHandler.isFirstRun = true;
      NamedAccountAfterInsertHandler.isFirstRun = true;
      controller.save();
    	acc = [select Is_Named_Account__c from Account where id = :acc.id];
      System.assertEquals(false,acc.Is_Named_Account__c);
      Test.stopTest();
      
    }
}