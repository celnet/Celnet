@isTest
public with sharing class AccountInfoUpdateHandlerTest {
	static testMethod void noUpdate() {
    	Account acc = UtilUnitTest.createHWChinaNAAccount('TEST001');
    	Account_Info_Update_Request__c request = new Account_Info_Update_Request__c();
		request.account__c = acc.id;
		request.Representative_Office__c = acc.Representative_Office__c;
		request.Province__c = acc.Province__c;
		request.City__c = acc.City_Huawei_China__c;
		request.Industry__c = acc.Industry;
		request.Sub_industry__c = acc.Sub_industry_HW__c;
		request.Customer_Group__c = acc.Customer_Group__c;
		request.Customer_Group_Code__c = acc.Customer_Group_Code__c;
		insert request;
    	Test.startTest();
    	request.IsApproved__c = true;
        update request;
    	Test.stopTest();
    }
}