global without sharing class LeadAssignmentNotification implements Schedulable{
	public static String CRON_EXP = '0 0 23 * * ?';
   															//Seconds Minutes Hours Day_of_month Month Day_of_week 
   global void execute(SchedulableContext ctx) {
			List<Id> userIds = LeadAssignmentNotification.queryUsers();
			LeadAssignmentNotification.sendEmail(userIds);
   }  
   
   public static List<Id> queryUsers(){
   	List<Leads_Distribute_Map__c> distributeMap = [select id,Leads_Owner__c from Leads_Distribute_Map__c];
			List<Id> userIds = new List<Id>();
			for(Leads_Distribute_Map__c l : distributeMap){
				userIds.add(l.Leads_Owner__c);
			}
			
			AggregateResult[] groupedResults = [select ownerid,count(id) from lead 
				where ownerid in :userIds and RecordTypeDeveloperName__c = 'Huawei_China_NA_Lead' group by ownerid];
	    Map<id,Integer> numMap = new Map<id,Integer> ();//user id,owned lead nums
			for(AggregateResult gr : groupedResults){
				numMap.put((String)gr.get('ownerid'),(Integer)gr.get('expr0'));//parse aggregate result
			}
			userIds.clear();
			for(Id i : numMap.keySet()){
				if(numMap.get(i) > 0){//该用户名下拥有na lead,需要发邮件通知
					userIds.add(i);
				}
			}
			return userIds;
   } 
   
   public static void sendEmail(List<Id> userIds){
   	EmailTemplate et = [select id,developername from EmailTemplate where developername = 'Unassigned_NA_Lead'];
			Messaging.MassEmailMessage mail = new Messaging.MassEmailMessage();
			mail.setTargetObjectIds(userIds);
			mail.setTemplateID(et.id);
			Messaging.sendEmail(new Messaging.Email[] { mail } , false);
   }
   
}