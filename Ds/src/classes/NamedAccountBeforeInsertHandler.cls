/**
 * Purpose					: for each na record inserted by data loader,add the 'record description' field
 * Author           : BLS
 * Create Date      : 2013/3
 *     
 */     
public without sharing class NamedAccountBeforeInsertHandler implements Triggers.Handler {
	public void handle() {
        for (Named_Account__c na : (List<Named_Account__c>)trigger.new){
            if (na.Record_Description__c == null){
            	na.Record_Description__c = 'User created';
            }
        }
        
	}
}