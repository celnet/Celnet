public without sharing class EmailSender {
	@future
	public static void sendMail(String subject ,String body,String[] toAddresses){
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setSubject(subject);
	  mail.setPlainTextBody(body);
		mail.setToAddresses(toAddresses);
	  Messaging.sendEmail(new Messaging.Email[]{mail});
	    
	}
	
	@future
	public static void sendHTMLMail(String subject ,String body,String[] toAddresses){
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setSubject(subject);
	  mail.setHtmlBody(body);
		mail.setToAddresses(toAddresses);
	  Messaging.sendEmail(new Messaging.Email[]{mail});
	    
	}
	
	public static Messaging.SendEmailResult[] sendMailImmediate(String subject ,String body,String[] toAddresses){
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setSubject(subject);
	  mail.setPlainTextBody(body);
		mail.setToAddresses(toAddresses);
	  Messaging.SendEmailResult[] results = Messaging.sendEmail(new Messaging.Email[]{mail});
	  return results;
	    
	}
}