@isTest
private class Lepus_OpportunityTeamMemberHandlerTest {
	static testmethod void myUnitTest(){
		Lepus_EIPCalloutServiceTest.setCustomSettings();
		
		Account acc = Lepus_EIPCalloutServiceTest.createAccount('ddd');
		Lepus_AccountHandler.isFirstRun = false;
		insert acc;
		Opportunity opp = Lepus_EIPCalloutServiceTest.createOpportunity('testaa', acc.Id);
		Lepus_AccountHandler.isFirstRun = false;
		insert opp;
		
		OpportunityTeamMember otm = new OpportunityTeamMember();
		otm.OpportunityId = opp.id;
		otm.UserId = UserInfo.getUserId();
		insert otm;
	}
}