public without sharing class AutoCreateSFPTask implements Triggers.Handler{
	Public static Boolean isFirstRunTask = true;
	public void handle(){		
		Set<Id> sfpIds = new Set<Id>();
		List<SFP_Product__c> sfpProducts = new List<SFP_Product__c>();		
		String mapType ='Work Order';
		String productLine ='';
		String repOffice='';	
		Map<Id,String>sfpRepMap =new Map<Id,String>();
		Map<Id,String>sfpProductMap =new Map<Id,String>();	
		Map<Id,Id>sfpTaskMap =new Map<Id,Id>();			
		list <Spare_Plan_Approver_Maps__c> taskOwner =new list<Spare_Plan_Approver_Maps__c>();
		Id mapRecordTypeId = Schema.SObjectType.Spare_Plan_Approver_Maps__c.getRecordTypeInfosByName().get('Onsite WO Approver Map').getRecordTypeId();								
		if(isFirstRunTask){
			for(SFP__c sfp : (list<SFP__c>)trigger.new){					
				if(sfp.Status__c=='Active'&& ((SFP__c)trigger.oldMap.get(sfp.Id)).Status__c=='Inactive'
				&&((sfp.RecordTypeId=='012900000000OIM'&&sfp.No_of_Health_Check_Service__c>0)
					||sfp.RecordTypeId=='01290000001EtaW'||sfp.RecordTypeId=='01290000001EtaV'
					||sfp.RecordTypeId=='012900000000OIJ')&&sfp.CreatedDate >Date.newInstance(2014,06,30)){						
						sfpIds.add(sfp.Id);	
						if(sfp.Contract_Key_Accounts__c=='非大客户'||sfp.Contract_Key_Accounts__c==null){
							repOffice=sfp.Contract_Rep_Office__c;
						}else{
							repOffice=sfp.Contract_Key_Accounts__c;
						}
							sfpRepMap.put(sfp.id,repOffice);				
				}	
				system.debug('sfpIds'+sfpIds);										
			}
			//---for循环结束
			
			//if 开始
			if(sfpIds.size()>0){													
				sfpProducts =[select SFP__c,Contract_Product_PO__r.Product_Family__c from SFP_Product__c
					where SFP__c in :sfpIds];
				for(ID sfp:sfpIds){
					if(sfpProducts.size()>0){
						for(SFP_Product__c sfpProduct :sfpProducts){
							if(sfp==sfpProduct.SFP__c){
								productLine=sfpProduct.Contract_Product_PO__r.Product_Family__c;
								sfpProductMap.put(sfp,productLine);
								break;
							}else{
								productLine='';
								sfpProductMap.put(sfp,productLine);
							}
						}			
					}else{productLine='';sfpProductMap.put(sfp,productLine);}
					if((sfpRepMap.get(sfp)!=null&&sfpRepMap.get(sfp)!='')&&(sfpProductMap.get(sfp)!=''&&sfpProductMap.get(sfp)!=null)){
						taskOwner =
						[select id,Rep_Office__c,Approver__c,Type__c from Spare_Plan_Approver_Maps__c
						where RecordTypeId= :mapRecordTypeId and Rep_Office__c= :sfpRepMap.get(sfp)
						and Product_Line__c =:sfpProductMap.get(sfp) and Type__c =:mapType];
					}else if((sfpRepMap.get(sfp)!=null&&sfpRepMap.get(sfp)!='')&&(sfpProductMap.get(sfp)==''||sfpProductMap.get(sfp)==null)){
						taskOwner =
						[select id,Rep_Office__c,Approver__c,Type__c from Spare_Plan_Approver_Maps__c
						where RecordTypeId= :mapRecordTypeId and Rep_Office__c= :sfpRepMap.get(sfp)
						and Product_Line__c ='ALL' and Type__c =:mapType];
					}else if(sfpRepMap.get(sfp)==null||sfpRepMap.get(sfp)==''){
						taskOwner =[select id,Rep_Office__c,Approver__c,Type__c from Spare_Plan_Approver_Maps__c
						where RecordTypeId= :mapRecordTypeId and Rep_Office__c= '机关'
						and Type__c =:mapType];
					}
					
					if(taskOwner.size()>0){
						sfpTaskMap.put(sfp,taskOwner[0].Approver__c);
					}else{sfpTaskMap.put(sfp,'00590000000mgGh');}				
				}
			}else{return;}
			//if结束	
			//创建TASK并发送邮件			
			if(sfpIds.size()<=90){//防止查询超过101					
					for(ID sfp :sfpIds){	
							Task task = new Task();					
							task.Subject='主动服务（健康检查/驻场/事件值守/单次服务）计划任务提醒';
							task.Description='请尽快提交：主动服务（健康检查/驻场/事件值守/单次服务）的任务计划';
							task.Status='Not Started';
							task.WhatId=sfp;
							task.ActivityDate=System.today();
							task.IsReminderSet=true;
							task.ReminderDateTime=System.today();
							task.OwnerId = sfpTaskMap.get(sfp);
							Database.DMLOptions dmlo = new Database.DMLOptions(); 
							dmlo.EmailHeader.triggerUserEmail = true; 
							database.insert(task, dmlo);
				}				
			}else{				
				AutoCreateSFPTask_Batch sfpTaskBatch= new AutoCreateSFPTask_Batch(); 
				sfpTaskBatch.sfpIds=sfpIds;
				sfpTaskBatch.sfpTaskMap=sfpTaskMap;
				Id batchId = Database.executeBatch(sfpTaskBatch,100);
			}
			isFirstRunTask=false;
		}
	}	
}