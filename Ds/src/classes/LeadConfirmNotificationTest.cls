@isTest
private class LeadConfirmNotificationTest {

    static testMethod void queryTest() {
    	Account acc = UtilUnitTest.createAccount();
    	Lead l = UtilUnitTest.createLead(acc);
    	List<Id> userIds = LeadConfirmNotification.queryUsers();
    	System.assertEquals(1,userIds.size());
    	System.assertEquals(UserInfo.getUserId(),userIds[0]);
    	LeadConfirmNotification.sendEmail(userIds);
    }
    
    static testMethod void scheduleTest(){
    	Account acc = UtilUnitTest.createAccount();
    	Lead l = UtilUnitTest.createLead(acc);
    	Test.startTest();
    	
    	String jobId = System.schedule('TestLeadAssignmentNotification',
     		LeadConfirmNotification.CRON_EXP, 
         new LeadConfirmNotification());
    	 CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];

      System.assertEquals(LeadConfirmNotification.CRON_EXP, 
         ct.CronExpression);
    	
    	Test.stopTest();
    }
}