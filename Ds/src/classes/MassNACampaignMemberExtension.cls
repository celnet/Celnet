/*
Author: tommy.liu@celnet.com.cn
Created On: 2014-5-28
Function: Mass maintance NA campaign members
*/
public without sharing class MassNACampaignMemberExtension {
	//创建页面展示构造对象
	public class MemberRow {
		private MassNACampaignMemberExtension controller;
		private Integer rowNu;
		public Boolean Checked {get; set;}
		public China_Campaign_NA_Member__c NAMember {get; set;}
		public String RepresentativeOffice{get;set;}
		public String CIS{get;set;}
		//表示行号,不对外显示,用于标记需要查重的行
		public Integer RowNumber { 
			get{
				return this.rowNu;
			} 
		}
		public Boolean Saved {
			get {
				if(this.NAMember != null && this.NAMember.ID != null) {
					return true;
				} else {
					return false;
				}
			}
		}
		//构造方法
		public MemberRow(MassNACampaignMemberExtension controller) {
			this.controller = controller;
			this.rowNu = controller.generateRowNumber();
		}
	}
	 
	private ApexPages.StandardSetController setController;
	private ID CampaignId;
	private List<MemberRow> mRows;
	private Integer rowNumberSeed;
	public Campaign Camp {get; set;}
	public Boolean AllChecked {get; set;}
	public Boolean ShowList {get; set;}
	public Integer DuRowNumber {get; set;}//用于接收从客户端穿过来的需要查重的行
	public Boolean SaveDisabled {get; set;}
	public List<MemberRow> MemberRows {
		get {
			return this.mRows;
		}
	}
	
	public Integer RowSize {
		get {
			if(this.mRows == null) {
				return 0;
			}
			return this.mRows.size();
		}
	}

	public MassNACampaignMemberExtension(ApexPages.StandardSetController setController) {
  	this.SaveDisabled = false;
  	this.ShowList = true;
  	this.setController = setController;
  	this.CampaignId = ApexPages.currentPage().getParameters().get('id');
  	if(this.CampaignId == null) {
  		this.showMessage(ApexPages.Severity.Error, System.Label.VF_Easy_MassNACampaignMember_Message_Missing_Campaign_Id);
			this.ShowList = false;
  		return;
  	}
  	this.camp = [Select ID, Name, Status From Campaign Where ID =: this.CampaignId];
  	if(!this.checkAccess()) return; //判断是否有编辑权限
  	//if(!this.checkCampaignStatus()) return; //判断市场活动是否关闭了
  	this.initMemberRows();
	}
	
	private Integer generateRowNumber() {
		Integer no = this.rowNumberSeed;
		this.rowNumberSeed ++;
		return no;
	}
	
	//只有对当前市场活动有编辑权限的用户才能添加成员
	private Boolean checkAccess() {
		UserRecordAccess urAccess = [Select RecordId, HasEditAccess From UserRecordAccess Where UserId =: UserInfo.getUserId() And RecordId =: this.CampaignId];
		if(urAccess == null || !urAccess.HasEditAccess) {
			this.showMessage(ApexPages.Severity.Error, System.Label.VF_Easy_MassNACampaignMember_Message_No_Edit_Access);
			this.ShowList = false;
			return false;
		}
		return true;
	}
	
	//关闭的市场活动不能被添加NA成员
	private Boolean checkCampaignStatus() {
		if(this.camp.Status == 'Completed' || this.camp.Status == 'Aborted') {
			this.showMessage(ApexPages.Severity.Error, System.Label.VF_Easy_MassNACampaignMember_Message_Campaign_Closed);
			this.ShowList = false;
			return false;
		}
		return true;
	}
	
	//公用的错误提示方法
	private void showMessage(ApexPages.Severity sev, String msg) {
	  ApexPages.Message apexMsg = new ApexPages.Message(sev, msg);
		ApexPages.addMessage(apexMsg);
	}
	
	private void initMemberRows() {
		this.rowNumberSeed = 1;
		this.mRows = new List<MemberRow>(); 
		if(this.CampaignId == null) return;
		List<China_Campaign_NA_Member__c> memberList = 
			[Select ID, Name, Representative_Office__c, NA_Account__c, NA_Account__r.Account__c, 
							NA_Account__r.Account__r.Representative_Office__c, 
							NA_Account__r.Account__r.CIS__c, CIS_NO_c__c, Campaign__c, NA_Account__r.Representative_Office__c 
				 From China_Campaign_NA_Member__c 
				Where Campaign__c =: this.CampaignId Order By CreatedDate];
		for(China_Campaign_NA_Member__c member : memberList) {
			MemberRow row = new MemberRow(this);
			row.Checked = false;
			row.NAMember = member;
			row.RepresentativeOffice = member.Representative_Office__c;
			row.CIS = member.CIS_NO_c__c;
			this.mRows.add(row);
		}
	}
	
	public void checkDuplicate() {
		this.SaveDisabled = false;
		System.assert(this.DuRowNumber != null);
		Boolean isDu = false;
		MemberRow duRow;
		List<MemberRow> duplicatedRows = new List<MemberRow>();//与哪些重复
		for(MemberRow row : this.mRows){ //先找到自己 
			if(row.RowNumber == this.DuRowNumber && row.NAMember.Na_Account__c != null) {
				NA_List__c nal = [Select id, CIS_NO__c, Account__c, Account__r.CIS__c, Account__r.Representative_Office__c , Representative_Office__c
														From NA_List__c 
													 Where Id =: row.NAMember.Na_Account__c];
				row.RepresentativeOffice = nal.Representative_Office__c;
				row.CIS = nal.CIS_NO__c;
				duRow = row;
				break;
			}
		}
		// System.assert(duRow != null);
		if(duRow != null){
			if(duRow.NAMember.NA_Account__c == null) {
				return;
			}
			for(MemberRow row : this.mRows) {//再查重
				if(row.RowNumber != duRow.RowNumber && row.NAMember.NA_Account__c == duRow.NAMember.NA_Account__c) {//存在一个与自己相同成NA Account
					isDu = true;
					duplicatedRows.add(row);
				}
			}
			
			if(isDu) {
				duRow.NAMember.NA_Account__c.addError(System.Label.VF_Easy_MassNACampaignMember_Message_Duplicate);
				for(MemberRow duplicatedRow : duplicatedRows) {
					duplicatedRow.NAMember.NA_Account__c.addError(System.Label.VF_Easy_MassNACampaignMember_Message_Duplicate);
				}
				this.SaveDisabled = true;
			}
		}
	}
	
	public System.Pagereference save() {
		if(this.mRows == null || this.mRows.size() == 0) {
			return null;
		}
		Boolean isError = false;
		//同步NA Account字段值到Member上，如Name
		Set<ID> naAccIdSet = new Set<ID>(); 
		for(MemberRow row : this.mRows) {
			if(row.NAMember != null && row.NAMember.NA_Account__c != null) {
				naAccIdSet.add(row.NAMember.NA_Account__c);
			}
		}
		Map<ID, NA_List__c> naAccMap = new Map<ID, NA_List__c>();
		for(NA_List__c naAcc : [Select ID, Name, Account__c From NA_List__c Where ID In: naAccIdSet]) {
			naAccMap.put(naAcc.Id, naAcc);
		}
		
		//从行中抽取出Member
		List<China_Campaign_NA_Member__c> members = new List<China_Campaign_NA_Member__c>();
		for(MemberRow row : this.mRows) {
			if(row.NAMember == null) {
				continue;
			}
			if(row.NAMember.NA_Account__c == null) {
				row.NAMember.NA_Account__c.addError(System.Label.VF_Easy_MassNACampaignMember_Message_NA_Required);
				isError = true;
				continue;
			}
			//NA_List__c naAcc = naAccMap.get(row.NAMember.NA_Account__c);
			//if(naAcc != null) {
			//	row.NAMember.Name = naAcc.Name;
				//TODO: 同步其它字段
			//}
			members.add(row.NAMember);
		}
		if(!isError) {
			upsert members;
			this.initMemberRows();
			return this.setController.cancel();
		} else {
			return null;
		}
	}
	
	public System.Pagereference add() {
		China_Campaign_NA_Member__c member = new China_Campaign_NA_Member__c();
		member.Campaign__c = this.CampaignId;
		MemberRow row = new MemberRow(this);
		row.Checked = false;
		row.NAMember = member;
		this.mRows.add(row);
		return null;
	}
	
	public System.Pagereference del() {
		this.SaveDisabled = false;
		List<China_Campaign_NA_Member__c> delMembers = new List<China_Campaign_NA_Member__c>();//在数据库中存在,待删除的记录
		List<MemberRow> afterDelRows = new List<MemberRow>();//保存留下来的行
		for(MemberRow row : this.mRows) {
			if(row.Checked) {
				if(row.NAMember != null && row.NAMember.ID != null) {
					delMembers.add(row.NAMember);
				}
			} else {
				afterDelRows.add(row);
			}
		}
		delete delMembers;
		this.mRows = afterDelRows;
		return null;
	}
	
	public System.Pagereference cancel() {
		return this.setController.cancel();
	}
}