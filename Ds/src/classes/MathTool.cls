public class MathTool {

	public static Decimal getMin(List<Decimal> decList){
			Decimal t=decList.get(0);
			for(Decimal dec:decList){
				if(dec<t)t=dec;
			}
			return t;
	}
	
	public static testMethod void currClsTest(){
		Decimal d1=0.1;
		Decimal d2=0.2;
		Decimal d3=0.3;
		List<Decimal> dl=new List<Decimal>();
		dl.add(d1);
		dl.add(d2);
		dl.add(d3);
		MathTool.getMin(dl);
	}
}