@isTest
private class ServiceRequestSurveyHandlerTest {

    static testMethod void singleRun() {
    	Case serviceRequest = UtilUnitTest.createChinaServiceRequest();
    	Account account = UtilUnitTest.createAccount();
    	Contact contact = UtilUnitTest.createContact(account);
    	system.debug(contact);
    	serviceRequest.ContactId =contact.Id;
    	serviceRequest.HF_Remote_Not_Success_Survey_Reason__c = 'test';
        serviceRequest.HF_Onsite_Not_Success_Survey_Reason__c = 'test';
        serviceRequest.HF_Spare_Not_Success_Survey_Reason__c = 'test';        
    	update serviceRequest;
    	serviceRequest = [select id,status,HF_Remote_Survey_Time__c,HF_Onsite_Survey_Time__c,
            HF_Spare_Survey_Time__c from Case where id =:serviceRequest.id];
    	System.assertNotEquals('Closed',serviceRequest.Status);
    	System.assertNotEquals(null,serviceRequest.HF_Remote_Survey_Time__c);
        System.assertNotEquals(null,serviceRequest.HF_Onsite_Survey_Time__c);
        System.assertNotEquals(null,serviceRequest.HF_Spare_Survey_Time__c);
    	
    	serviceRequest.HF_Remote_Survey_Answer3_china__c = 'test';
        serviceRequest.HF_Onsite_Survey_Answer2_china__c = 'test';
        serviceRequest.HF_Spare_Survey_Answer2_china__c = 'test';  
        serviceRequest.Last_Survey_Date__c =Datetime.now();
        contact.Survey_Date__c =serviceRequest.Last_Survey_Date__c;
    	update serviceRequest;
    	update contact;
    	
    	serviceRequest = [select id,status,HF_Remote_Survey_Time__c,HF_Onsite_Survey_Time__c,
            HF_Spare_Survey_Time__c from Case where id =:serviceRequest.id];
    	System.assertEquals('Closed',serviceRequest.Status);
    	System.assertNotEquals(null,serviceRequest.HF_Remote_Survey_Time__c);
        System.assertNotEquals(null,serviceRequest.HF_Onsite_Survey_Time__c);
        System.assertNotEquals(null,serviceRequest.HF_Spare_Survey_Time__c);
    	
    	
    }
    
    /*
    对应的验证规则被取消
    static testMethod void validationRuleTest(){//当Satisfactory Level原值不为空时；且回访各内容有更新时.禁止修改
    	Case serviceRequest = UtilUnitTest.createChinaServiceRequest();
    	
    	serviceRequest.HF_Remote_Survey_Answer3_china__c = 'test';
    	update serviceRequest;
    	serviceRequest = [select id,status,HF_Remote_Survey_Time__c from Case where id =:serviceRequest.id];
    	System.assertEquals('Closed',serviceRequest.Status);
    	
    	serviceRequest.HF_Remote_Survey_Answer3_china__c = 'test2';
    	try{
	    	update serviceRequest;
	    	System.assertEquals('should never run into this','回访已完成，不允许再更新回访内容');
    	}catch(Exception e){
    	
    	}
    	
    	
    }
    
    */
}