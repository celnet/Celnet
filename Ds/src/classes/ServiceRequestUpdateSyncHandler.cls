/*
*@Purpose:icare owner/group 或summary字段更新时,向EIP推送数据
*@Author:Brain Zhang
*@Create Date：2013-11
*/


public without sharing class ServiceRequestUpdateSyncHandler implements Triggers.Handler{//after update
	public static Boolean shouldRun = true;
	public void handle(){
		if(!ServiceRequestUpdateSyncHandler.ShouldRun){
			return ;
		}
		List<Id> caseIds = new List<Id>();
		for(Case c : (List<Case>)trigger.new){
			System.debug('now the c is:----------------------------' + c);
			System.debug('now the c subject is:----------------------------' + c.subject);
			System.debug('in the oldmap, the c is:----------------------------' + (Case)trigger.oldmap.get(c.id));
			System.debug('in the oldmap, the c subject is:----------------------------' + ((Case)trigger.oldmap.get(c.id)).subject);
			if(c.iCare_Number__c == null){
				continue;//只同步存在iCare_Number__c的字段
			}
			if(c.subject != ((Case)trigger.oldmap.get(c.id)).subject){//summary changed
				caseIds.add(c.Id);
				continue;
			}
			if(c.iCare_Group__c != ((Case)trigger.oldmap.get(c.id)).iCare_Group__c){//icare group changed
				caseIds.add(c.Id);
				continue;
			}
			if(c.iCare_Owner__c  != ((Case)trigger.oldmap.get(c.id)).iCare_Owner__c){//icare owner changed
				caseIds.add(c.Id);
				continue;
			}
			if(c.Employee_ID__c  != ((Case)trigger.oldmap.get(c.id)).Employee_ID__c){//icare owner changed
				caseIds.add(c.Id);
				continue;
			}
			if(c.Is_Overflowed__c !=((Case)trigger.oldmap.get(c.id)).Is_Overflowed__c){
				system.debug('hewei'+c.id+'-------------'+c.CaseNumber);
				caseIds.add(c.Id);
				continue;
			}
		}
			//run the sync method
		if(caseIds.size() > 0){//需要进行同步
			Profile currentUserProfile = [select id,Name from Profile where id = :Userinfo.getProfileId()];
			System.debug('the current profile:--------' + currentUserProfile.name);
			if(currentUserProfile.name.contains('Integration')){
				return;//如果是集成账号在更新此字段,那么直接返回, 不再次回写
			}
			
			//ServiceRequestUpdateSyncHandler.shouldRun = false;
			System.debug('send out once:------------------------------------');
			ServiceRequestSyncUtils.updateToEIP(caseIds);
		}		
	}
}