@isTest
private class MassEditChinaAMSalesTargetExtensionTest {
	static testMethod void test() {
    Sales_Target_Accomplishment__c target = new Sales_Target_Accomplishment__c();
    target.Name = 'test';
    target.Year__c = '2014';
    target.China_Representative_Office__c = '上海代表处';
    target.Account_Manager_Property__c = '商业';
    target.Industries_Responsible_for__c = '大企业系统部';
    target.China_Order_Target__c = 100;
    target.China_Income_Target__c = 100;
    target.RecordTypeId = CONSTANTS.SALESTARGETREGIONRECORDTYPE;
    insert target;
    
    List<Sales_Target_Accomplishment__c> targetList = [SELECT ID FROM Sales_Target_Accomplishment__c];
    System.assertEquals(1, targetList.size());
    ApexPages.StandardSetController setCon = new ApexPages.StandardSetController(targetList);
    setCon.setSelected(targetList);
    
    MassEditChinaAMSalesTargetExtension massCon = new MassEditChinaAMSalesTargetExtension(setCon);
    List<MassEditChinaAMSalesTargetExtension.TargetRow> selRows = massCon.SelectedTargetRows;
    System.assertEquals(1, selRows.size());
    
    ID userID = UserInfo.getUserId();
    selRows[0].target.OwnerId = userID;
    massCon.submit();
	}
}