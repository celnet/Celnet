/*
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-8-27
 * Description: Account的Handler，同步到EIP
 */
public class Lepus_OpportunityHandler implements Triggers.Handler{
    public static boolean isFirstRun = true;
    
    public void handle(){
        // 触发记录日志
        List<Lepus_Log__c> logList = new List<Lepus_Log__c>();
        List<Opportunity> triggerOppList = Lepus_HandlerUtil.retrieveRecordList();
        Set<Id> triggerOppIdSet = new Set<Id>();
        
        for(Opportunity opp : triggerOppList){
            if(Lepus_HandlerUtil.filterRecordType(opp, Opportunity.sobjecttype)){
                Lepus_Log__c log = new Lepus_Log__c();
				log.RecordId__c = opp.Id;
				log.LastModifiedDate__c = opp.LastModifiedDate;
				log.UniqueId__c = opp.Id + opp.LastModifiedDate.formatGMT('yyyy-MM-dd HH:mm:ss');
				log.InTrigger__c = true;
                log.Updated__c = true;
                logList.add(log);
                
                triggerOppIdSet.add(opp.Id);
            }
        }
        
        if(logList.size() > 0)
        upsert logList UniqueId__c;
        
        List<Id> opportunityIdList = new List<Id>();
        String action = Lepus_HandlerUtil.retrieveAction();
        List<Opportunity> opportunityList = Lepus_HandlerUtil.retrieveRecordList();
        Map<Id , Opportunity> map_opp = new Map<Id,Opportunity>();
        for(Opportunity opp : opportunityList){
            if(Lepus_HandlerUtil.filterRecordType(opp, Opportunity.sobjecttype)){
                
                opportunityIdList.add(opp.Id);
                map_opp.put(opp.Id , opp);
            }
        }
        
        boolean syncBusinessData = (Lepus_Data_Sync_Controller__c.getInstance('机会点业务数据') != null) && 
                                        Lepus_Data_Sync_Controller__c.getInstance('机会点业务数据').IsSync__c;
        boolean syncTeamMember = (Lepus_Data_Sync_Controller__c.getInstance('机会点团队') != null) && 
                                        Lepus_Data_Sync_Controller__c.getInstance('机会点团队').IsSync__c;
        boolean syncFieldUpdate = (Lepus_Data_Sync_Controller__c.getInstance('机会点字段更新') != null) && 
                                        Lepus_Data_Sync_Controller__c.getInstance('机会点字段更新').IsSync__c;
        
        if(syncBusinessData){
            if(opportunityIdList.size() == 1 && Limits.getFutureCalls() < Limits.getLimitFutureCalls()){
                if(syncTeamMember && action.toLowerCase().equals('insert')){
                    Lepus_FutureCallout.syncDataAndTeamMember(opportunityIdList[0], 'opportunity', '');
                } else {
                    try{
                    	Lepus_FutureCallout.syncData(opportunityIdList[0], action, 'opportunity');
                    } catch(Exception ae){
                        Lepus_SyncUtil.initQueue(map_opp, opportunityIdList, action, '业务数据同步', datetime.now());
                    }
                }
                
            } else if(opportunityIdList.size() > 0){
                Lepus_SyncUtil.initQueue(map_opp, opportunityIdList, action, '业务数据同步', datetime.now());
            }
        }
          
        if(syncTeamMember){
        	List<Id> syncOppIdList = new List<Id>();
        	
            if(action.toLowerCase().equals('update')){
                for(Id oppId : opportunityIdList){
                    if((trigger.newMap.get(oppId)).get('OwnerId') != (trigger.oldMap.get(oppId)).get('OwnerId')
                    || (trigger.newMap.get(oppId)).get('Name') != (trigger.oldMap.get(oppId)).get('Name')
                    || (trigger.newMap.get(oppId)).get('PRM_ID__c') != (trigger.oldMap.get(oppId)).get('PRM_ID__c')){
                        syncOppIdList.add(oppId);
                    }
                }
                
            } else if(action.toLowerCase().equals('delete')){
            	syncOppIdList = opportunityIdList;
            }
            
            if(syncOppIdList.size() == 1 && Limits.getFutureCalls() < Limits.getLimitFutureCalls()){
                Lepus_FutureCallout.syncTeamMember(syncOppIdList, 'opportunity', action);
            } else if(syncOppIdList.size() > 0){
                Lepus_SyncUtil.initQueue(map_opp, syncOppIdList, action, '团队成员同步', datetime.now());
            }
        }
          
        if(syncFieldUpdate && action.toLowerCase().equals('update')){
            if(opportunityIdList.size() == 1 && Limits.getFutureCalls() < Limits.getLimitFutureCalls()){
                Lepus_FutureCallout.syncFieldUpdate(opportunityIdList[0]);
            } else if(opportunityIdList.size() > 0){
                Lepus_SyncUtil.initQueue(map_opp, opportunityIdList, action, '字段更新同步', datetime.now());
            }
        }
    }
}