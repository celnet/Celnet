@isTest 
private class Lepus_OpportunityHandlerTest {
    static testmethod void insertUpdateTest(){
        User u = [Select id From User Where UserType = 'Standard' And isActive = true limit 1];
    
        Lepus_EIPCalloutServiceTest.setCustomSettings();
        Account acc1 = Lepus_EIPCalloutServiceTest.createAccount('acc1');
        insert acc1;
        
        Opportunity oppx = Lepus_EIPCalloutServiceTest.createOpportunity('oppx', acc1.Id);
        insert oppx;
        
        Lepus_OpportunityHandler.isFirstRun = true;
        oppx.OwnerId = u.Id;
        update oppx;
    }
    
    static testmethod void deleteTest(){
        Account accx = Lepus_EIPCalloutServiceTest.createAccount('acc1');
        insert accx;
        Lepus_OpportunityHandler.isFirstRun = false;
        Opportunity opp2 = Lepus_EIPCalloutServiceTest.createOpportunity('oppx', accx.Id);
        insert opp2;
        
        Lepus_OpportunityHandler.isFirstRun = true;
        delete opp2;
    }
}