/*
 * @Purpose : 中国区线索分发审批权限处理逻辑
 *				 1、线索分发时，自动更新状态并将Owner分配给相应的审批人
 *				 2、线索驳回时，自动更新状态并将Owner分配给之前的分发人
 * @Author : Steven
 * @Date : 2013-10-29
 */
public without sharing class ChinaLeadSharingHandler implements Triggers.Handler{ //after update
	public void handle() {
		Set<Id> submitIds = new Set<Id>();
		Set<Id> rejectedIds = new Set<Id>();
		for(Lead l : (List<Lead>)Trigger.new){
			if(l.RecordTypeId == CONSTANTS.CHINALEADRECORDTYPE) {
				String newStatus = l.Status;
				String oldStatus = ((Lead)Trigger.oldMap.get(l.id)).Status;
				//待分发 TO 待确认
				if(newStatus == CONSTANTS.CHINALEADSTATUSCONFIRM && oldStatus == CONSTANTS.CHINALEADSTATUSASSIGN) submitIds.add(l.id); 
				//待确认 TO 待分发
				if(newStatus == CONSTANTS.CHINALEADSTATUSASSIGN && oldStatus == CONSTANTS.CHINALEADSTATUSCONFIRM) rejectedIds.add(l.id);  
				//System.debug(oldStatus+' TO '+newStatus);
			}
		}
		//提交审批时Change lead Owner给当前审批人
		if (!submitIds.isEmpty()) ChinaLeadSharingHandler.runLaterSubmit(submitIds); 
		//审批驳回时ChangeOwner给审批前的Owner
		if (!rejectedIds.isEmpty()) runLaterRejected(rejectedIds); 
	}
	
	/**
	 * 提交给客户经理确认时自动change owner给相应的客户经理
	 */
	@future
	public static void runLaterSubmit(Set<id> ids){
		//查询出相关线索的审批流程
		List<ProcessInstance> instances = 
			[SELECT Id,TargetObjectId, (SELECT Id, ActorId,OriginalActorId ,ProcessInstanceId FROM Workitems)
			   FROM ProcessInstance 
			  where TargetObjectId in : ids and status = 'Pending'];
		//查询出当前审批人
		Map<id,Id> recordToApproversMap = new Map<id,Id>();
		for(ProcessInstance i : instances){
			if(i.Workitems != null && i.Workitems.size()>0) {
				Id newOwnerId = i.Workitems.get(0).ActorId;
				recordToApproversMap.put(i.TargetObjectId,newOwnerId);
			}
		}
		//ChangeOwner并记录之前的OwnerId
		List<Lead> leads = [SELECT Id, OwnerID,Can_Be_Edit__c, China_Lead_Distributor__c FROM Lead where Id in : ids];
		for(Lead l : leads){
			l.China_Lead_Distributor__c = l.OwnerId;
			l.Can_Be_Edit__c = !l.Can_Be_Edit__c; //add by steven 20131224 控制允许后台修改不允许前台修改
			l.OwnerId = recordToApproversMap.get(l.Id);
		}
		update leads;
	}
	
	/**
	 * 如果驳回，ChangeOwner给之前分发的销管
	 */
	private void runLaterRejected(Set<id> ids){
		List<Lead> leads = [SELECT Id, OwnerID, China_Lead_Distributor__c FROM Lead where Id in : ids];
		for(Lead l : leads){
			l.OwnerId = l.China_Lead_Distributor__c;
		}
		update leads;
	}
}