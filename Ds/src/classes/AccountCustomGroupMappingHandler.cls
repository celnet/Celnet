/**
 * @Purpose : 当CIS系统对客户信息作出变更,回写至Salesforce系统
 *					 1、如果customer group code字段有更新,一并更新customer group/sub industry/industry字段
 *					 2、如果省份字段有更新,那么一并更新代表处字段
 * @Author : Steven
 * @Date : 2014-07-01 update
 */
public without sharing class AccountCustomGroupMappingHandler  implements Triggers.Handler{//before update
	public Map<String,String> industryDictonary {get;set;}
	public Map<String,String> subIndustryDictonary {get;set;}
	public Map<String,String> customerGroupDictonary {get;set;}
	public Map<String,String> provinceDictonary {get;set;}
	public Profile currentUserProfile {get;set;}
	public Boolean codeMapHasInit = false;
	
	public void handle(){ 
    for(Account acc : (List<Account>)Trigger.new){
			if(acc.RecordTypeId == CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE){
				Account oldAcc = (Account)trigger.OldMap.Get(acc.Id);
				if(acc.Customer_Group_Code__c != oldAcc.Customer_Group_Code__c) {
					initNameCodeMap();
	    		String industry = industryDictonary.get(acc.Customer_Group_Code__c);
	    		String subIndustry = subIndustryDictonary.get(acc.Customer_Group_Code__c);
	    		String customerGroup = customerGroupDictonary.get(acc.Customer_Group_Code__c);
	    		if(customerGroup != null) acc.Customer_Group__c = customerGroup;
	    		if(subIndustry != null) acc.Sub_industry_HW__c = subIndustry;
	    		if(industry != null) acc.industry = industry;
				}
				if(acc.Province__c != oldAcc.Province__c) {
					initNameCodeMap();
					String representativeOffice = provinceDictonary.get(acc.Province__c);
	    		if(representativeOffice != null) acc.Representative_Office__c = representativeOffice;
				}
			}
    }
	}
	
	/**
	 * 初始化Name Code对应关系 
	 */
	private void initNameCodeMap() {
		if(this.codeMapHasInit) return;
		this.industryDictonary = new Map<String,String>();
		this.subIndustryDictonary = new Map<String,String>();
		this.customerGroupDictonary = new Map<String,String>();
		this.provinceDictonary = new Map<String,String>();
		//判断是否为集成账号所做操作
		Profile currentUserProfile = [select id,Name from Profile where id =:Userinfo.getProfileId()];
		if(currentUserProfile.name != 'Integration Profile') { 
			this.codeMapHasInit = true;
			return;
		}
		//获取CODE和NAME
		List<DataDictionary__c> dds = [select name__c, type__c, code__c from DataDictionary__c 
																		where Object__c= 'Account' and type__c in('industry','Sub-Industry','Customer Group','province')];
		for(DataDictionary__c d : dds){
			if(d.Type__c == 'Customer Group'){
				customerGroupDictonary.put(d.code__c,d.name__c);
			}else if(d.Type__c == 'Sub-Industry'){
				subIndustryDictonary.put(d.code__c,d.name__c);
			}else if(d.Type__c == 'industry'){
				industryDictonary.put(d.code__c,d.name__c);
			}else if(d.Type__c == 'province'){
				provinceDictonary.put(d.code__c,d.name__c);
			}
		}
		this.codeMapHasInit = true;
	}
	
}