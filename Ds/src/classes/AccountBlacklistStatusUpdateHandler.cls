/*
* @Purpose : it is the trigger handler, which records the history when the blacklist status is changed. 
*
* @Author : Jen Peng
* @Date : 		2013-4-29
*/
public without sharing class AccountBlacklistStatusUpdateHandler implements Triggers.Handler{
	public static Boolean isFirstRun = true;
	public void handle(){
		if(AccountBlacklistStatusUpdateHandler.isFirstRun){
			try{
				Map<Id,Account> oldMaps = new Map<Id,Account>();
				Map<Id,Blacklist_Check_History__c> bchMaps = new Map<Id,Blacklist_Check_History__c>();
				List<Blacklist_Check_History__c> newBch = new List<Blacklist_Check_History__c>();
				List<Blacklist_Check_History__c> tmpBch = new List<Blacklist_Check_History__c>();
				Set<Id> accountIds = new Set<Id>();
				for(Account a : (List<Account>)trigger.new){
					accountIds.add(a.id);
				}//it is modified by jen peng on 2013、10、10
				tmpBch=[select id,Account__c from Blacklist_Check_History__c where Blacklist_Status__c='Yellow' and isNew__c=true and Account__c in:accountIds];
				for(Blacklist_Check_History__c b : tmpBch){
					bchMaps.put(b.Account__c,b);
				}
				tmpBch.clear();
				for (Account acc : (List<Account>)trigger.old){
					oldMaps.put(acc.id,acc);
				}
				for (Account a : (List<Account>)trigger.new){
					if(a.blacklist_type__c!=((Account)trigger.oldMap.get(a.id)).blacklist_type__c
						||a.blacklist_comment__c!=((Account)trigger.oldMap.get(a.id)).blacklist_comment__c
						||a.isFollow__c!=((Account)trigger.oldMap.get(a.id)).isFollow__c){
							if(oldMaps.get(a.id)!=null && oldMaps.get(a.id).Blacklist_Status__c=='Yellow'&&!bchMaps.isEmpty()){
								//Yellow status,the Yellow checking history will be update.
								if(a.blacklist_type__c=='Tier I'){
									a.Blacklist_Status__c='Red';
									a.blacklist_lock__c = true;
								}else if(a.blacklist_type__c=='Tier II'){
									a.Blacklist_Status__c='Red';
									a.blacklist_lock__c = false;
								}else if(a.blacklist_type__c=='Pass'){
									a.Blacklist_Status__c='Green';
									a.isFollow__c=true;
									a.blacklist_lock__c = false;
								}else if(a.blacklist_type__c==''||a.blacklist_type__c==null){
									a.Blacklist_Status__c='';
									a.blacklist_lock__c = false;
								}else{
									a.Blacklist_Status__c='Yellow';
									a.blacklist_lock__c = false;
								}
								a.Last_Blacklist_Status__c=oldMaps.get(a.id).Blacklist_Status__c;
								a.Blacklist_Last_Type__c=oldMaps.get(a.id).blacklist_type__c;
								bchMaps.get(a.id).Reverse_flag__c=oldMaps.get(a.id).isFollow__c;
								bchMaps.get(a.id).Last_status__c=oldMaps.get(a.id).Blacklist_Status__c;
								bchMaps.get(a.id).Blacklist_Status__c=a.Blacklist_Status__c;
								bchMaps.get(a.id).blacklist_type__c=a.blacklist_type__c;
								bchMaps.get(a.id).status_date__c=system.now();
								bchMaps.get(a.id).comment__c=a.blacklist_comment__c;
							}else{
								// if status is not yellow, or others, it will be insert into history
								if(bchMaps.get(a.id)!=null && a.Blacklist_Status__c=='Yellow'){
									bchMaps.get(a.id).Reverse_flag__c=a.isFollow__c;
									bchMaps.get(a.id).Last_status__c=a.Blacklist_Status__c;
									bchMaps.get(a.id).Blacklist_Status__c=a.Blacklist_Status__c;
									bchMaps.get(a.id).blacklist_type__c=a.blacklist_type__c;
									bchMaps.get(a.id).status_date__c=system.now();
									bchMaps.get(a.id).comment__c=a.blacklist_comment__c;
								}else{
									if(a.Blacklist_Status__c=='Gray'&&a.blacklist_type__c == 'Pending'){
										a.blacklist_lock__c = false;
									}else{
										
										if(a.blacklist_type__c=='Tier I'){
											a.Blacklist_Status__c='Red';
											a.blacklist_lock__c = true;
										}else if(a.blacklist_type__c=='Tier II'){
											a.Blacklist_Status__c='Red';
											a.blacklist_lock__c = false;
										}else if(a.blacklist_type__c=='Pass'){
											a.Blacklist_Status__c='Green';
											a.blacklist_lock__c = false;
										}else if(a.blacklist_type__c==''||a.blacklist_type__c==null){
											a.Blacklist_Status__c='';
											a.blacklist_lock__c = false;
										}else{
											a.Blacklist_Status__c='Yellow';
											a.blacklist_lock__c = false;
										}
										a.Last_Blacklist_Status__c=oldMaps.get(a.id).Blacklist_Status__c;
										a.Blacklist_Last_Type__c=oldMaps.get(a.id).blacklist_type__c;
										Blacklist_Check_History__c bch1 = new Blacklist_Check_History__c();
										bch1.Reverse_flag__c=a.isFollow__c;
										bch1.Account__c=a.Id;
										bch1.status_date__c=system.now();
										bch1.Account_Country__c=a.Country_Code_HW__c;
										bch1.Last_status__c=oldMaps.get(a.id).Blacklist_Status__c;
										bch1.Blacklist_Status__c=a.Blacklist_Status__c;
										bch1.blacklist_type__c=a.blacklist_type__c;
										bch1.comment__c=a.blacklist_comment__c;
										newBch.add(bch1);
									
									}
										
								}
									
							}
						}
							
				}
				if(!newBch.isEmpty()){
					insert newBch;
				}
				if(!bchMaps.isEmpty()){
					update bchMaps.values();
				}
				
			}catch(Exception e){
			}
				
		}
	}
}