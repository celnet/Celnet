/**
 * @Purpose : 1、创建线索时，根据线索的孵化状态进行自动分发，如果该状态为空则不分发； before insert
 *            2、更新线索时，如果线索的孵化状态发生了变化，根据孵化状态自动分发线索的责任人； before update
 * @Author : Steven
 * @Date : 2014-06-16
 */
public without sharing class OverseaLeadDistributeHandler implements Triggers.Handler{ //before insert before update
	public Set<Id> inactiveOwnerIds {get;set;} //失效的线索分发责任人ID 
	public Map<String,Id> qualifiedLeadsOwnerMap {get;set;}  //线索分发责任人ID和区域的对应关系
	public Map<String,Id> unQualifiedLeadsOwnerMap {get;set;}  //线索分发责任人ID和区域的对应关系
	public Boolean hasInit {get;set;} 
	
	public void handle() {
		this.hasInit = false;
		for(Lead l : (List<Lead>)Trigger.new) {
			if(l.RecordTypeId == CONSTANTS.OVERSEALEADRECORDTYPE) {
				//Logic1: 创建线索时，根据线索的孵化状态进行自动分发，如果该状态为空则不分发（一般一线创建的线索状态为空）
				if(Trigger.isInsert) {		
					if(l.Create_Type__c == 'Qualified' || l.Create_Type__c == 'Unqualified') {
						if(!this.hasInit) initSalesManagerInfo();
						Id ownerId;
						if(l.Create_Type__c == 'Qualified') ownerId = this.qualifiedLeadsOwnerMap.get(l.Country__c);
						if(l.Create_Type__c == 'Unqualified') ownerId = this.unQualifiedLeadsOwnerMap.get(l.Country__c);
						//自动查找有效的线索自动分发责任人，如果查询不到,Owner默认为创建人
						if((ownerId != null) && (!this.inactiveOwnerIds.contains(ownerId))) {
							l.ownerId = ownerId;
						}
					}
				}
				//Logic2: 更新线索时，如果线索的孵化状态发生了变化，根据孵化状态自动分发线索的责任人； 
				if(Trigger.isUpdate) {
					String newType = l.Create_Type__c;
					String oldType = ((Lead)Trigger.oldMap.get(l.id)).Create_Type__c;	
					if(newType == null || newType == oldType) continue;
					if(newType == 'Qualified' || newType == 'Unqualified') {
						if(!this.hasInit) initSalesManagerInfo();
						Id ownerId;
						if(newType == 'Qualified') ownerId = this.qualifiedLeadsOwnerMap.get(l.Country__c);
						if(newType == 'Unqualified') ownerId = this.unQualifiedLeadsOwnerMap.get(l.Country__c);
						if((ownerId != null) && (!this.inactiveOwnerIds.contains(ownerId))) {
							l.ownerId = ownerId;
						}
					}
				} //end 如果是更新线索
			} //end 如果是海外线索
		} //end for 
	} //end handle 

	/**
	 * 初始化线索责任人MAP，仅在第一次执行该方法时加载
	 */
	private void initSalesManagerInfo() {
		Set<Id> ownerIds = new Set<Id>();
		this.inactiveOwnerIds = new Set<Id>();
		this.qualifiedLeadsOwnerMap = new Map<String,Id>();
		this.unQualifiedLeadsOwnerMap = new Map<String,Id>();
		this.hasInit = true;
		//Step1：查找出所有国家和线索分发Owner的对应关系
		List<Account_Link_Approver_Map__c> approvers = 
			[Select Country__c,Approver__c,type__c FROM Account_Link_Approver_Map__c 
				Where recordtypeid =:CONSTANTS.APPROVERMAPOVERSEALEADSOWNERRECORDTYPE];
		for(Account_Link_Approver_Map__c a : approvers){
			if(a.type__c == 'Qualified') this.qualifiedLeadsOwnerMap.put(a.Country__c,a.Approver__c);
			if(a.type__c == 'Unqualified') this.unQualifiedLeadsOwnerMap.put(a.Country__c,a.Approver__c);
			ownerIds.add(a.Approver__c);
		}
		//Step2：查找出所有失效的线索分发Owner
	  List<User> owners = [Select Id FROM User where Id =: ownerIds and IsActive = false];
	  for(User u : owners){
			this.inactiveOwnerIds.add(u.id);
		}
	}

}