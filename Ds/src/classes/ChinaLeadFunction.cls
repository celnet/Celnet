/**
 * @Purpose : 1、点击作废按钮将当前线索失效
 *				    2、点击关闭按钮将当前线索关闭
 *				    3、点击驳回按钮将当前线索驳回给销管
 *				    4、点击分发按钮将当前线索重新分发给其他客户经理
 *            5、点击确认按钮将当前线索更新成跟进中状态
 * @Author : Steven
 * @Date : 2014-07-21
 */
public class ChinaLeadFunction {
	public Lead chinaLead {get;set;}
	public Boolean isDistribute {get;set;} //用于控制界面是否显示手动分配的线索责任人字段
	public Boolean isConfirm {get;set;}    //用于控制界面是否显示线索跟进方式字段
	public Boolean isInvalid {get;set;}    //用于控制界面是否显示线索失效原因字段
	public Boolean isRejected {get;set;}   //用于控制界面是否显示线索驳回原因字段
	public Boolean canSave {get;set;}      //用于控制界面是否显示保存按钮
	public String operType{ 
		get{return operType;}
		set{operType=value;} 
	}
	
	/**
	 * 构造方法: 1、获取传入的LeadId、操作类型; 2、通过ID查询Lead关键字段信息；
	 */
	public ChinaLeadFunction(ApexPages.StandardController controller) { 
		Id chinaLeadId= ApexPages.currentPage().getParameters().get('id');
		isDistribute = false;
		isRejected = false;
		canSave = false;
		isInvalid = false;
		isConfirm = false;
		operType= ApexPages.currentPage().getParameters().get('type');
		if(chinaLeadId != null) {
			chinaLead = [Select id,OwnerId,Status_Changed__c,Follow_Up_Type__c,China_Lead_Invalid_Reason__c,
													Rejected_Reason__c,China_Representative_Office__c,Status,Name,Company,LeadSource,CreatedDate
										 from Lead where id=:chinaLeadId];
		}
	}
	
	/**
	 * 根据传入的操作类型，立即执行相应的操作方法
	 */
	public PageReference executeFunction(){
		PageReference result;
		if(operType=='closed') { 
			chinaLead.Status = CONSTANTS.CHINALEADSTATUSCLOSED;
		} else if(operType=='rejected') {
			chinaLead.Status = CONSTANTS.CHINALEADSTATUSASSIGN;
			isRejected = true;
			//将线索驳回给对应区域的生效销管账号，如果无销管则失败
			Boolean rejectSuccess = rejectToSalesManager('销管',chinaLead.China_Representative_Office__c); 
			if(rejectSuccess) canSave = true;
			return null;
		} else if(operType=='invalid') {
			chinaLead.Status = CONSTANTS.CHINALEADSTATUSINVALID;
			isInvalid = true;
			return null;
		} else if(operType=='distribute' || operType=='transmit') {
			chinaLead.Status = CONSTANTS.CHINALEADSTATUSCONFIRM;
			chinaLead.Follow_Up_Type__c = null;
			isDistribute = true;
			return null;
		} else if(operType=='confirm') {
			chinaLead.Status = CONSTANTS.CHINALEADSTATUSFOLLOW;
			isConfirm = true;
			return null;
		}
		try{
			chinaLead.Status_Changed__c = !chinaLead.Status_Changed__c;
			update(chinaLead);
			sendRemindMail();  //发送邮件提醒
		} catch(Exception e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,e.getMessage()));
			return null;
		}
		return returnPage(hasReadPermission());
	}
	
	/**
	 * 对线索进行重新分发、失效、确认后，保存线索的相关字段信息
	 */
	public PageReference save(){
		//如果是重新分发，且分发给了销管，设置线索的状态为待分发
		if(this.operType=='transmit' && isSalesManager(chinaLead.OwnerId)) {
			chinaLead.Status = CONSTANTS.CHINALEADSTATUSASSIGN;
		}
		try{
			chinaLead.Status_Changed__c = !chinaLead.Status_Changed__c;
			update(chinaLead); //更新线索
			sendRemindMail();  //发送邮件提醒
			return returnPage(hasReadPermission());
		}catch(Exception e){
			chinaLead.addError(e.getMessage());
			return null;
		}
	}

	/**
	 * Lead保存后如果用户有有查看权限跳转到明细界面，没有查看权限直接跳转到Home页面 
	 */
	private PageReference returnPage(Boolean hasReadPermission) { 
		if(hasReadPermission) { 
			return new ApexPages.StandardController(chinaLead).view();
		} else {
			PageReference pr = new PageReference('/00Q/o');
      pr.setRedirect(true);
			return pr;
		}
	}	
	
	/**
	 * 判断change owner后，当前用户是否还有查看权限
	 */
	private Boolean hasReadPermission() { 
		Boolean result = false;
		UserRecordAccess access = 
			[SELECT RecordId,HasReadAccess FROM UserRecordAccess WHERE UserId =:UserINfo.getUserId() AND RecordId =:chinaLead.id limit 1];
		if(access != null && access.HasReadAccess){
			result = true;
		}
		return result;
	}
	
	/**
	 * 从Map表中自动获取当前用户所在区域的销管
	 */
	private Boolean rejectToSalesManager(String approverType,String region){
		Boolean result = false;
		Id ApproverMapRecordType = [select id from RecordType where DeveloperName = 'Huawei_China_Approver'].id;
		List<Account_Link_Approver_Map__c> approvers = 
			[Select Approver__c FROM Account_Link_Approver_Map__c 
				where Representative_Office__c =:region  and RecordTypeId = :ApproverMapRecordType and Type__c =:approverType];
		if(approvers.size() > 0){
			User currentApprover = [Select IsActive, Name FROM User where Id = : approvers[0].Approver__c];
			if(!currentApprover.IsActive) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,
				'您所属区域的'+ approverType +'"'+currentApprover.Name+'"已经失效，请联系"'+System.Label.China_Sales_Dept_Contact+'"维护'));
			} else {
				this.chinaLead.OwnerId = approvers[0].Approver__c;
				result = true;
			}
		} else {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,
			'您的所属区域 "'+region + '"没有维护'+ approverType +'，请联系"'+System.Label.China_Sales_Dept_Contact+'"维护'));
		}
		return result;
	}
	
	/**
	 * 如果线索被客户经理重新分发给销管，则线索状态自动设置为待分发
	 */
	private Boolean isSalesManager(Id ownerId){
		Boolean result = false;
		User newOwner = [select Id,Profile.Name from user where id =: ownerId];
		if(newOwner.Profile.name.contains('(Huawei China)代表处销管')|| 
			 newOwner.Profile.name.contains('(Huawei China)系统部销管')){
			result = true;
		}
		return result;
	}
	
	/**
	 * 发送提醒邮件
	 */ 
	private void sendRemindMail() {
		List<Lead> leadList = new List<Lead>(); 
		String operation = '';
		if('distribute'==this.operType) {
			operation = '分发';
		} else if('transmit'==this.operType) {
			operation = '转发';
		} else if('rejected'==this.operType){
			operation = '驳回';
		} else {
			return;
		}
		User newOwner = [select id,name,email from user where id =:chinaLead.OwnerId];
		leadList.add(chinaLead);
		sendLeadRemindMail(UserINfo.getName(),newOwner,leadList,operation);
	}

	/**
	 * 发送线索提醒邮件
	 */ 
	public static void sendLeadRemindMail(String fromName,User newOwner,List<Lead> lList,String operation) {
		if(lList.size()==0 || newOwner==null || fromName==null || operation==null) return;
		try { 
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			List<string> toAddress = new List<string>();
			toAddress.add(newOwner.email);
			mail.setToAddresses(toAddress);
			mail.setSenderDisplayName('Salesforce Supporter');
			mail.setSubject('您收到待处理的线索，请及时登陆Salesforce处理');
			String strbody = getHtmlBody(fromName,newOwner.name,lList,operation);
			mail.setHtmlBody(strbody);
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		} catch(Exception e) {
			System.debug(e);
		}
	}

	/**
	 * 构造邮件主体 ，参数：发件人姓名，收件人姓名，线索列表，操作类型（转发、分发、驳回）
	 */ 
	public static String getHtmlBody(String fromName,String newOwnerName,List<Lead> lList,String operation) {
		String baseUrl = system.Url.getSalesforceBaseUrl().toExternalForm()+'/';
		String salesContactName = System.Label.China_Sales_Dept_Contact;
		String leadsString = '';
		String result = '';
		//Step1：线索遍历和分组
		for(Lead lead : lList) {
			leadsString += getHtmlLead(lead,newOwnerName,baseUrl);
		}
		//Step2：构造待处理线索邮件主体
		result = '<p>'+ newOwnerName +'，您好！</p>';
		result += '<p>&nbsp;&nbsp;“'+ fromName +'”将'+ lList.size() +'条线索'+ operation +'给您，请尽快登陆系统的“线索”模块处理。</p>';
		result += getHtmlTable(leadsString);
		result += '<p>&nbsp;&nbsp;如有操作疑问，请咨询本部门销售管理员。</p>';
		result += '<p>声明：按公司线索管理要求</p>'; 
		result += '<p>&nbsp;&nbsp;1.销管在1日内进行分发处理；</p>';
		result += '<p>&nbsp;&nbsp;2.销售人员在3日内进行确认处理，确认后的线索在7日内进行闭环处理。</p>';
		result += '<p>&nbsp;&nbsp;3.跟踪方式选择“自跟线索”的线索请在系统内转机会点或作废；跟踪方式选择“渠道跟踪”的线索请在系统内选定渠道分发，接收渠道在echannel线索模块内接收并做报备或作废处理，销售人员督促渠道及时反馈，在系统内进行闭环。</p>';
		return result;
	}
    
	/**
	 * 构造Html形式的机会点列表（带标题的） 
	 */ 
	public static String getHtmlTable(String htmlLeadList) {
		String result = '';
		if(htmlLeadList != null && htmlLeadList != '') {
			result += '<table style="font-family: Arial,微软雅黑;font-size:12px; border:1px solid #AAA;width:1100px;background: #EEE;border-spacing: 1px;text-align:center">';
			result +=   '<tbody>';
			result +=       '<tr style="background: #CCC;font-weight: bold;">';
			result +=           '<th style="width:10%">姓名</th>';
			result +=           '<th style="width:20%">客户名称</th>';
			result +=           '<th style="width:15%">区域/系统部</th>';
			result +=           '<th style="width:10%">线索来源</th>';
			result +=           '<th style="width:8%">线索阶段</th>';
			result +=           '<th style="width:17%">线索所有人</th>';
			result +=           '<th style="width:20%">创建日期</th>';
			result +=       '</tr>';
			result +=       htmlLeadList;
			result +=   '</tbody>';
			result += '</table>';
		}
		return result;
	}
    
	/**
	 * 构造Html表格格式的线索String
	 */ 
	public static String getHtmlLead(Lead lead,String ownerName,String baseUrl) {
		String result = '';
		if(lead != null) {
			result += '<tr style="background: #FFF;">';
			result +=   '<td><a href='+ baseUrl + lead.Id +'>' + lead.Name + '</a></td>';
			result +=   '<td>'+ lead.Company +'</td>';
			result +=   '<td>'+ lead.China_Representative_Office__c +'</td>';
			result +=   '<td>'+ lead.LeadSource +'</td>';
			result +=   '<td>'+ lead.Status +'</td>';
			result +=   '<td>'+ ownerName +'</td>';
			result +=   '<td>'+ lead.CreatedDate +'</td>';
			result += '</tr>';
		}
		return result;
	}
}