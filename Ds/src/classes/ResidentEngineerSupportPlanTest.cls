/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ResidentEngineerSupportPlanTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Account acc = UtilUnitTest.createAccountWithName('Test');
        Contact contact = UtilUnitTest.createContact(acc);
        CSS_Contract__c c = UtilUnitTest.createContract(acc);
        SFP__c sfp = UtilUnitTest.createSFP(c);
        SFP.Resident_Engineer_Support_Total__c=4;
        update sfp;
        Resident_Engineer_Support_Plan__c res = new Resident_Engineer_Support_Plan__c();
        res.SFP__c = sfp.Id;
        res.Service_Start_Time__c = system.today();        
        res.Service_Site__c ='test';
        res.Service_Customer_Demand__c='test';
        res.Customer_Contact__c = contact.Id;
        insert res;
    }
}