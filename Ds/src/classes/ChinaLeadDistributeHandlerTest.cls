/*
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-6-24
 * Description: Test ChinaLeadDistributeHandler
 */
@isTest
private class ChinaLeadDistributeHandlerTest {
	static testmethod void test(){
		User activeManagerUser = [Select Id From User Where Profile.Name like '%代表处客户经理%' And isActive = true limit 1];
		User inactiveManagerUser = [Select Id From User Where Profile.Name like '%代表处客户经理%' And isActive = false limit 1];
		
		System.runAs(activeManagerUser){
			Account_Link_Approver_Map__c am = new Account_Link_Approver_Map__c();
			am.Region__c = '北京市';
			am.Representative_Office__c = '北京代表处';
			am.Approver__c = UserInfo.getUserId();
			am.Type__c = '销管';
			insert am;
			
			Account acc = new Account();
            acc.Name = 'acc';
            acc.cis__c = 'TEST123';
            acc.RecordTypeId = CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE;
            acc.Region_HW__c='China';
            acc.Country_HW__c='China';
            acc.Country_Code_HW__c='CN';
            acc.Province__c = '北京市';
            acc.City_Huawei_China__c = '北京市';
            acc.Industry = '商业销售部';
            acc.blacklist_type__c='Pending';
            insert acc;
			
			Lead l = new Lead(Account_Name__c = acc.id,NA_Lead_Name__c = 'nalead',
	        	NA_Lead_Confirmation__c='未确认',lastname = 'lead',company = 'company', China_Representative_Office__c = '北京代表处');
		    l.recordtypeid = CONSTANTS.CHINALEADRECORDTYPE;
		    insert l;
			Lead l2 = [Select Id, Status From Lead];
	    	System.assertEquals(CONSTANTS.CHINALEADSTATUSFOLLOW, l2.Status);
	    
	    	l.NA_Lead_Name__c = 'nalead2';
	    	update l;
		}
	}
}