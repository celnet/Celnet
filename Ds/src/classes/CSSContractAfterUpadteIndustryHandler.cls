public without sharing class CSSContractAfterUpadteIndustryHandler implements Triggers.Handler{
	public void handle(){
		Set<id> cssContractIds = new Set<id>();		
		String cssRecordTypeId = 
        Schema.SObjectType.CSS_Contract__c.getRecordTypeInfosByName().get('HW-(Int CSS) Service Contract') == null ? 
        Schema.SObjectType.CSS_Contract__c.getRecordTypeInfosByName().get('海外服务合同').getRecordTypeId() :
        Schema.SObjectType.CSS_Contract__c.getRecordTypeInfosByName().get('HW-(Int CSS) Service Contract').getRecordTypeId();			
		for(CSS_Contract__c css : (List<CSS_Contract__c>)trigger.new){
			if((css.Industry__c !=((CSS_Contract__c)trigger.OldMap.Get(css.Id)).Industry__c||
				css.Sub_Industry__c !=((CSS_Contract__c)trigger.OldMap.Get(css.Id)).Sub_Industry__c||
				css.Office__c !=((CSS_Contract__c)trigger.OldMap.Get(css.Id)).Office__c)
				&& css.RecordTypeId!=cssRecordTypeId
			  ){			  	
				cssContractIds.add(css.Id);				
			}
		}		
		List<CSS_Contract__c> cssContractList = [SELECT Id,Industry__c,Sub_Industry__c,Office__c
  												 FROM CSS_Contract__c
  												 WHERE Id IN: cssContractIds];
		if(cssContractList.size()>0){
			List<SFP__c> relatedSFPList = [SELECT Id, Contract_No__c,Industry__c, Sub_Industry__c,Rep_Office_China__c
            							   FROM SFP__c WHERE Contract_No__c IN: cssContractIds];			
			/*Integer eaCount = [Select count()  FROM Installed_Asset__c
            										  WHERE Contracts_No__c IN: cssContractIds];   
            										  */         										   
			List<Contract_Product_PO__c> contractProList =[SELECT Id, Contract_No__c, Industry__c,Sub_Industry__c,
            										  Rep_Office_China__c
            										  FROM  Contract_Product_PO__c where Contract_No__c in :cssContractIds];          										  
			Map<Id,CSS_Contract__c> serviceContractMap = new Map<Id,CSS_Contract__c>([SELECT Id, Industry__c,Sub_Industry__c,Office__c
	  												 FROM CSS_Contract__c
	  												 WHERE Id IN: cssContractIds]);         										         							
		         							  
			
				if(relatedSFPList.size()>0){
					for (SFP__c sfp : relatedSFPList){						
							sfp.Industry__c = serviceContractMap.get(sfp.Contract_No__c).Industry__c;
							sfp.Sub_Industry__c=serviceContractMap.get(sfp.Contract_No__c).Sub_Industry__c;
							sfp.Rep_Office_China__c =serviceContractMap.get(sfp.Contract_No__c).Office__c;						
					}
				}				
             	if (contractProList.size() > 0){
             			for (Contract_Product_PO__c cp : contractProList){             				
             				cp.Industry__c = serviceContractMap.get(cp.Contract_No__c).Industry__c;
							cp.Sub_Industry__c=serviceContractMap.get(cp.Contract_No__c).Sub_Industry__c;
							cp.Rep_Office_China__c =serviceContractMap.get(cp.Contract_No__c).Office__c;						               					             				
             			}
             	}
			
			update relatedSFPList;			
			update contractProList;
			/*if(eaCount<5000&&eaCount>0){
				List<Installed_Asset__c> relatedEAList = [SELECT Id, Contracts_No__c, Industry__c,Sub_Industry__c,
            										  Rep_Office_China__c
            										  FROM Installed_Asset__c
            										  WHERE Contracts_No__c IN: cssContractIds]; 
				for (Installed_Asset__c ea : relatedEAList){             				
             				ea.Industry__c = serviceContractMap.get(ea.Contracts_No__c).Industry__c;
							ea.Sub_Industry__c=serviceContractMap.get(ea.Contracts_No__c).Sub_Industry__c;
							ea.Rep_Office_China__c =serviceContractMap.get(ea.Contracts_No__c).Office__c;						          					             				
             			}
				update relatedEAList;
			}else{	*/						
				UpdateContractEAIndustry_Batch eaBatch= new UpdateContractEAIndustry_Batch();  	
				eaBatch.query='SELECT Id, Contracts_No__c,Industry__c,Sub_Industry__c,Rep_Office_China__c FROM Installed_Asset__c WHERE Contracts_No__c IN :cssContractIds';
				eaBatch.serviceContractMap = serviceContractMap;
				eaBatch.cssContractIds = cssContractIds;
				Id batchId = Database.executeBatch(eaBatch,1000);
			
			//}
			  												 
		}  	
					
	}	
}