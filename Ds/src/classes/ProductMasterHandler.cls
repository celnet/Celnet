/****************************************************************************/
/* Name: ProductMasterHandler
/* Author: Jojo/Jorry
/* Date: 2012.6
/* Description: Opportunity product tree class
/****************************************************************************/
public with sharing class ProductMasterHandler{
    Product_HS__c root;
    public String rootId { get; set; }
    // all top level products
    public List<Product_HS__c> parentNodes { get; set; }
    // all sub level prodcuts
    public List<Product_HS__c> childNodes { get; set; }
    // all product id
    public String productId { get; set; }
    // all product url
    public String url { get; set; }
    // existed products
    public List<Project_Product__c> existProducts {get; set;}
    
    /* Class Construnction  */
    public ProductMasterHandler(ApexPages.StandardController controller) { 
        //childNodes = new List<Product_HS__c>();
        //rootId = controller.getId();
        
        // get all existed products of the opportunity.
        this.existProducts = [select Id, Lookup__c, Product_Name__c, Quantity__c, Sales_Price__c, CurrencyIsoCode, Product_level__c from Project_Product__c
                                    where Project_Name__c = :controller.getId()];
    }
    
    /* Get Existed Products Number */
    public String getitemNum() {
        return existProducts.size().format();
    }
         
    /* Custom Project Tree View Init  */
    /*public void init(){ 
        // Get all the parent nodes.
        parentNodes = [SELECT Id, Name, Parent_Product__c, Product_level__c FROM Product_HS__c WHERE Parent_Product__c='' AND RecordType.Name='Huawei Product' AND Active__c='Yes'];
        childNodes = new List<Product_HS__c>();
        for(Product_HS__c pp :parentNodes){
            ProductMasterHandler.getChildNodes(childNodes, pp.Id);
        }
        System.debug('childNodes.size===>' + childNodes.size());
    } */
    
    /* Get All Child Custom Project. */
    /*static void getChildNodes(List<Product_HS__c> childNodes, String id){
        List<Product_HS__c> listChild = [SELECT Id, Name, Parent_Product__c, Product_level__c FROM Product_HS__c WHERE Parent_Product__c=:id AND Active__c='Yes'];
        If(listChild != null && listChild.size()>0){
            for(Product_HS__c cp : listChild){
                childNodes.add(cp);
                ProductMasterHandler.getChildNodes(childNodes, cp.id);                
            }       
        }    
    } */
    
    
    /* Custom Project Tree View Init  */
    public void init(){ 
        // Get all the parent nodes.
        parentNodes = [SELECT Id, Name, Parent_Product__c, Product_level__c FROM Product_HS__c WHERE Parent_Product__c='' AND RecordType.Name='Huawei Product' AND Active__c='Yes'];
        childNodes = [SELECT Id, Name, Parent_Product__c, Product_level__c FROM Product_HS__c WHERE Parent_Product__c<>'' AND RecordType.Name='Huawei Product' AND Active__c='Yes'];
        System.debug('childNodes.size===>' + childNodes.size());
    } 
    
    
    public static TestMethod void PruductMasterHandler_UnitTest() {
        Opportunity opp = new Opportunity(name='Test Opp');
        ApexPages.StandardController controller = new ApexPages.StandardController(opp);

        ProductMasterHandler pmTest = new ProductMasterHandler(controller);
        pmTest.init();
        pmTest.getitemNum();
        
    }
    
}