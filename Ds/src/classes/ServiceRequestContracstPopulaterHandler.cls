/*
*@Purpose:rewrite the TigOnCase trigger,perform the same function:
*barcode字段有值时,自动填充对应的Contract_No__c和W_M_SFP__c字段
*只要有条码，以条码为准，如果没有条码，可以手动加
*@Author:Brain Zhang
*Create Date：2013-9
*/
public without sharing class ServiceRequestContracstPopulaterHandler implements Triggers.Handler{
        public static boolean shouldRun = true;
        public void handle(){
            if(!CommonConstant.serviceRequestTriggerShouldRun){
                return ;
            }
            if(!shouldRun){
                return ;
            }
            Set<Id> installAssetIds = new Set<Id> ();
            Set<Id> hwProductIds = new Set<Id>();
            Id sfpId;
            Id contractId;
            for(Case c:(List<Case>)Trigger.new){
                if(trigger.isUpdate){
                    if(c.Barcode__c!=null){
                        installAssetIds.add(c.Barcode__c);
                    }else {
                        c.W_M_SFP__c=null;
                        c.Contract_No__c=null;
                    }
                }
                if(trigger.isInsert){
                    if(c.Barcode__c!=null){
                        installAssetIds.add(c.Barcode__c);
                    }
                }           
            }           
            /**if(!hwProductIds.isEmpty()){
                Map<id,Product_HS__c> prMap = new Map<id,Product_HS__c>([select id,iCare_Group_Product__c from Product_HS__c
                where id in : hwProductIds]);
                for(Case c:(List<Case>)Trigger.new){
                    system.debug('++++++++++'+c.HW_Product__c);
                    system.debug(prMap.get(c.HW_Product__c).iCare_Group_Product__c);
                    if(prMap.get(c.HW_Product__c)!=null){           
                        c.iCare_Group_ID__c =prMap.get(c.HW_Product__c).iCare_Group_Product__c;
                    }else{
                        c.iCare_Group_ID__c=null;
                    }                   
                } 
            }**/
            if(!installAssetIds.isEmpty()){
                Map<id,Installed_Asset__c> iaMap= new Map<id,Installed_Asset__c>([select id,name,Contracts_No__c,
                    (Select Id,SFP__r.Contract_No__c,SFP__c,SFP_Status__c,SFP_Contract_NO__c From SFP_Asset__r where SFP__r.RecordType.DeveloperName like '%Warranty%' order by CreatedDate desc) 
                    from Installed_Asset__c 
                    where id in :installAssetIds]);
                for(Case c:(List<Case>)Trigger.new){  
                system.debug(c.status);                            
                        if(iaMap.get(c.Barcode__c) != null){
                            //c.Contract_No__c = iaMap.get(c.Barcode__c).Contracts_No__c;
                            if(iaMap.get(c.Barcode__c).SFP_Asset__r.size() > 0){
                                for(SFP_Asset__c r:iaMap.get(c.Barcode__c).SFP_Asset__r){
                                    if(r.SFP_Status__c=='Active'){
                                        sfpId = r.SFP__c;
                                        contractId = r.SFP__r.Contract_No__c;
                                        break;
                                    }
                                }
                                if(sfpId!=null){
                                    c.W_M_SFP__c =sfpId;
                                    c.Contract_No__c =contractId;
                                }else{
                                    c.W_M_SFP__c = iaMap.get(c.Barcode__c).SFP_Asset__r.get(0).SFP__c;
                                    c.Contract_No__c = iaMap.get(c.Barcode__c).SFP_Asset__r.get(0).SFP__r.Contract_No__c;
                                }                               
                            }else{
                                c.W_M_SFP__c=null;
                                c.Contract_No__c=null;
                            }
                        }                   
                }
            }
    }
}