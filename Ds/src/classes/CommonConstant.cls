/**************************************************************************************************
 * Name             : StatusforCode
 * Purpose          : 
 * Author           : 
 * Create Date      : 
 * Modify History   : 2012-11-14  Polaris Yu ******************************************************
 *					               添加了用于存放Region Dept相关触发器的执行情况的变量changedFromAccount
 *					     和changedFromContract
***************************************************************************************************/
public with sharing class CommonConstant {
     public static boolean isUpdate =false;
     
	public static boolean changedFromAccount = false;
	public static boolean changedFromContract = false;
	public static integer runTimes = 0;
	public static boolean serviceRequestTriggerShouldRun = true;
	public static boolean workOrderTriggerShouldRun = true;
	public static boolean accountTriggerShouldRun = true;
	public static boolean dealRegistrationTriggerShouldRun = true;
	
	
}