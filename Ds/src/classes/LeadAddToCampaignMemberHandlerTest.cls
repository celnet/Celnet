@isTest
private class LeadAddToCampaignMemberHandlerTest {

    static testMethod void noCampaign() {
      Account acc = UtilUnitTest.createHWChinaAccount('naaccount');
      Named_Account__c na = new Named_Account__c(Account_Name__c = acc.id,
    		Is_Named_Account__c = true);
    	insert na;
    	
      Lead l = new Lead(Account_Name__c = acc.id,NA_Lead_Name__c = 'nalead',lastname = 'lead',company = 'company',
      	LeadSource = '展会');
      l.recordtypeid = UtilUnitTest.getRecordTypeIdByName('Lead', 'Huawei China NA Lead');
      try{
	      insert l;
      }catch(DMLException e){
        System.debug('exception:-----' + e.getmessage());
      }
      Integer leadCount = [select count() from Lead];
    }
    
    static testMethod void withCampaign() {
      Account acc = UtilUnitTest.createHWChinaAccount('naaccount');
      Named_Account__c na = new Named_Account__c(Account_Name__c = acc.id,
    		Is_Named_Account__c = true);
    	insert na;
    	Campaign c = UtilUnitTest.createCampaign();
      Lead l = new Lead(Account_Name__c = acc.id,NA_Lead_Name__c = 'nalead',lastname = 'lead',company = 'company',
      	LeadSource = '展会' , campaign__c = c.id);
      l.recordtypeid = UtilUnitTest.getRecordTypeIdByName('Lead', 'Huawei China NA Lead');
      Test.startTest();
      insert l;
      Test.stopTest();
    }
}