@isTest
private class LeadConvertToOpportunityTest {

    static testMethod void convert() {
    	Account acc = UtilUnitTest.createHWChinaAccount('acc');
    	acc.Region_HW__c = 'test';
    	
    	
        Lead naLead = UtilUnitTest.createLead(acc);
        LeadConvertToOpportunity controller = new LeadConvertToOpportunity(new ApexPages.StandardController(naLead));
        System.assertEquals(true, controller.isDisabled);
        
    }
    
    static testMethod void convert2() {
    	Account acc = UtilUnitTest.createHWChinaAccount('acc');
    	acc.Region_HW__c = 'test';
      Lead naLead = UtilUnitTest.createLead(acc);
      naLead.NA_Lead_Confirmation__c = '确认有效';
      update naLead;
      LeadConvertToOpportunity controller = new LeadConvertToOpportunity(new ApexPages.StandardController(naLead));
      controller.initial();
      controller.newOpp.StageName = 'test';
      controller.newOpp.Opportunity_type__c='Direct Sales';//it is added on 2013/05/25 
      controller.newOpp.CloseDate = Date.today();
      controller.convert();
      
      Opportunity opp = [select name from Opportunity];
      System.assertEquals('nalead',opp.name);
      naLead = [select id,Status from Lead ];
      System.assertEquals('已转机会点',naLead.Status);
    }
}