/**
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-8-30
 * Description: Wisdom回写到Salesforce时，处理Opportunity控制字段的赋值
 */
public class Lepus_WisdomOpportunityWriteBackHandler implements Triggers.Handler{
    public static boolean isFirstRun = true;
    
    public void handle(){
        if(Lepus_WisdomOpportunityWriteBackHandler.isFirstRun){
            isFirstRun = false;
            
            if(UserInfo.getUserId() != '00590000002oT1VAAU' && !System.test.isRunningTest())
            return;
            
            //User u = [Select Id, Profile.Name From User Where Id =: UserInfo.getUserId()];
            //if(u.Profile.Name != 'Integration Profile for Lepus')
            //return;
            
            Lepus_WisdomOpportunityWriteBackHandler.updateControllingFields();
        }
    }
    
    public static void updateControllingFields(){
        
        // 中国区 Opportunity_Level_Code__c -> Opportunity_Level__c 从DataDictionary获取
        Map<String, String> opportunityLevelMap = new Map<String, String>();
        // 中国区 Opportunity_Type_Code__c -> Opportunity_Type__c 从DataDictionary获取
        Map<String, String> opportunityTypeMap = new Map<String, String>();
        
        // 海外 Country_Code__c -> Country__c 从DataDictionary获取
        map<String, String> countryMap = new map<String, String>();
        // 海外 Country__c -> Representative_Office__c
        map<String, String> repOfficeMap = new map<String, String>();
        // 海外 Representative_Office__c -> Region__c
        map<String, String> regionMap = new Map<String, String>();
        
        for(DataDictionary__c dd : [Select Id, Code__c, Name__c, Object__c, Type__c From DataDictionary__c Where Object__c = 'Opportunity']){
            if(dd.Type__c == 'Opportunity Level'){
                opportunityLevelMap.put(dd.Code__c, dd.Name__c);
            } else if(dd.Type__c == 'Opportunity Type'){
                opportunityTypeMap.put(dd.Code__c, dd.Name__c);
            } 
        }
        
        for(Field_Dependency__c fd : [Select Id, Controlling_Field__c, Controlling_Field_Value__c, Dependent_Field__c, 
                                            Dependent_Field_Value__c, Object_Type__c From Field_Dependency__c Where Object_Type__c = 'Opportunity']){
            if(fd.Dependent_Field__c == 'Country__c'){
                repOfficeMap.put(fd.Dependent_Field_Value__c, fd.Controlling_Field_Value__c);
            } else if(fd.Dependent_Field__c == 'Representative_Office__c'){
                regionMap.put(fd.Dependent_Field_Value__c, fd.Controlling_Field_Value__c);
            } else if(fd.Dependent_Field__c == 'Country_Code__c'){
                countryMap.put(fd.Dependent_Field_Value__c, fd.Controlling_Field_Value__c);
            } 
        }
        
        for(Opportunity opp : (List<Opportunity>)trigger.new){
            // 过滤记录类型
            if(!Lepus_HandlerUtil.filterRecordType(opp, Opportunity.sobjecttype))
            continue;
            
            String recordType = Lepus_HandlerUtil.getRecordType(opp, Opportunity.sobjecttype);
            
            if(recordType == '中国区'){
                String oldLevelCode;
                String oldLevel;
                String oldTypeCode;
                String oldType;
                
                if(trigger.isInsert){
                    oldLevelCode = null;
                    oldTypeCode = null;
                    oldLevel = null;
                    oldType = 'Certified Channel Value Sales';
                } else if(trigger.isUpdate){
                    oldLevelCode = (String)(trigger.oldMap.get(opp.Id).get('Opportunity_Level_Code__c'));
                    oldTypeCode = (String)(trigger.oldMap.get(opp.Id).get('Opportunity_Type_Code__c'));
                    oldLevel = (String)(trigger.oldMap.get(opp.Id).get('Opportunity_Level__c'));
                    oldType = (String)(trigger.oldMap.get(opp.Id).get('Opportunity_Type__c'));
                }
                
                if((opp.Opportunity_Level_Code__c != oldLevelCode) && (opp.Opportunity_Level__c == oldLevel)){
                    opp.Opportunity_Level__c = opportunityLevelMap.get(opp.Opportunity_Level_Code__c);
                }
                
                if((opp.Opportunity_Type_Code__c != oldTypeCode) && (opp.Opportunity_type__c == oldType)){
                    opp.Opportunity_type__c = opportunityTypeMap.get(opp.Opportunity_Type_Code__c);
                }
            } else if(recordType == '海外'){
                
                if(opp.Country_Code__c != null && countryMap.get(opp.Country_Code__c) != null ){
                    opp.Country__c = countryMap.get(opp.Country_Code__c);
                    opp.Representative_Office__c = repOfficeMap.get(opp.Country__c);
                    opp.Region__c = regionMap.get(opp.Representative_Office__c);
                }
                
            }
        }
    }
}