@isTest
private class GetICareUpdatedInfoTest {
	static testMethod void singleTest(){
		Case c = UtilUnitTest.createChinaServiceRequest();
		c.iCare_Number__c = '11';
		update c;
		Test.startTest();
	  Test.setMock(WebServiceMock.class, new EIPMockImpl());
	  EIPMockImpl.isManualGetMode = true;
	  GetICareUpdatedInfo controller = new GetICareUpdatedInfo(new ApexPages.StandardController(c));
	  controller.init();
	  Test.stopTest();
	  List<iCareLog__c> logs = [select id,ErrorTitle__c,ErrorDescription__c from iCareLog__c];
	  System.debug('the logs :-----------' + logs);
	  System.assertEquals(0,[select count() from iCareLog__c]);
	}
}