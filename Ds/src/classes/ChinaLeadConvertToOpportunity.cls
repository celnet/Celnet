/*
 * @Purpose : 中国区线索转换成机会点
 * @Author : Steven
 * @Date : 2013-10-29
 */
public without sharing class ChinaLeadConvertToOpportunity {
    public Opportunity newOpp {get;set;}
    public Lead currentLead {get;set;}
    public Account acc {get;set;}
    
    public ChinaLeadConvertToOpportunity(ApexPages.StandardController controller){
        init(controller.getId());
    }
    public ChinaLeadConvertToOpportunity(Id leadId){
        init(leadId);
    }
    
    private void init(Id leadId){
        currentLead = [select id, Account_Name__c, name, Status, Status_Changed__c, OwnerId, China_Participate_Space__c,
                                                    Expected_Project_Date__c, China_Main_Product__c , China_Project_Description__c
                                     from Lead where id = :leadId];  
        if(currentLead.Status != CONSTANTS.CHINALEADSTATUSFOLLOW){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'只有"跟进中"的线索才可以转换为机会点'));
        }
        try{
            acc = [select id, name, Industry,Industry__c, Sub_industry_HW__c, Customer_Group__c, Customer_Group_Code__c,
                                        Region_HW__c,Representative_Office__c, Country_HW__c, Country_Code_HW__c, Province__c, City_Huawei_China__c
                             from Account where id = :currentLead.Account_Name__c];
        }catch(QueryException e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'无法获取最终客户的信息，请确认选择了正确的最终客户'));
        }
        newOpp = new Opportunity();
    }
    
    private void initial(){
        DateTime dT = System.now();
                Date currentDate = date.newinstance(dT.year(), dT.month(), dT.day());
                Date closeDate = currentLead.Expected_Project_Date__c;
                closeDate = closeDate == null ? currentDate : closeDate;

        newOpp.AccountId = acc.id;
        newOpp.name = currentLead.name;
        newOpp.RecordTypeId = Constants.CHINAOPPORTUNITYRECORDTYPE;
        //newOpp.Lead__c = currentLead.id;

        newOpp.Participate_Space__c = currentLead.China_Participate_Space__c;
        newOpp.Opportunity_Progress__c = currentLead.China_Project_Description__c;
        newOpp.HW_Master_Product__c = currentLead.China_Main_Product__c;
        newOpp.CloseDate = closeDate;
        newOpp.Estimated_Shipment_Date__c = closeDate;
        newOpp.OwnerId = currentLead.OwnerId;
        newOpp.StageName = 'SS3 (Validating Opportunity)';
        newOpp.Master_Channer_Partner__c = '待补充';
        newOpp.Opportunity_Level__c = 'Ordinary Level';
        newOpp.Estimated_ContractSign_Amount__c = 0;
        
        newOpp.Region__c = acc.Region_HW__c;
        newOpp.Representative_Office__c = acc.Representative_Office__c;
        newOpp.Country__c = acc.Country_HW__c ;
        newOpp.Country_Code__c = acc.Country_Code_HW__c;
        newOpp.Province__c =  acc.Province__c;
        newOpp.City__c = acc.City_Huawei_China__c;

        newOpp.System_Department__c = acc.Industry; 
        newOpp.Industry__c = acc.Industry__c;
        newOpp.Sub_Industry__c = acc.Sub_industry_HW__c;
        newOpp.Customer_Group__c = acc.Customer_Group__c;
        newOpp.Customer_Group_Code__c = acc.Customer_Group_Code__c;
    }
    
    public PageReference convert(){
        if(ApexPages.hasMessages()) return null;
        try{
                initial();
            insert newOpp;
        }catch(Exception e){
            ApexPages.addMessages(e);//这里需要优化一下错误提示最好能显示在之前的lead对象中
            return null;
        }
        if(currentLead.status != CONSTANTS.CHINALEADSTATUSCLOSED){
            currentLead.Status_Changed__c = !currentLead.Status_Changed__c;
            currentLead.Convert_Date__c = DateTime.now();
            currentLead.China_Opportunity__c = newOpp.Id;
            update currentLead;
        }
        return new ApexPages.StandardController(newOpp).view();
    }
}