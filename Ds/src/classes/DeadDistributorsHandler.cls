/*
* @Purpose : 海外报备创建时给总代联系人发送邮件提醒；
* @Author : steven
* @Date :		2013-11-15
*/
public without sharing class DeadDistributorsHandler implements Triggers.Handler{//after insert
	public void handle() {
		for(Deal_Registration_Distributor__c distributor : (List<Deal_Registration_Distributor__c>)Trigger.new){
			String mail = distributor.Distributor_Contact_Email_Address__c;
			//如果需要发邮件，选择邮件模板并发送邮件
			if(distributor.Send_Remind_Mail__c=='YES' && mail!=null && mail!='') { 
				Id templateId = [select id from EmailTemplate where DeveloperName='Receipt_of_Huawei_Deal_Registration'].id;
				UtilEmail.sendMailByTemplate(new List<String>{mail},templateId,distributor.Deal_Registration__c);
			}
		}
	}
}