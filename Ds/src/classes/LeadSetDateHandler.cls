/*
*	在lead信息更新为确认有效时,更新确认时间
* 在workflow中,formula或criteria均查找不到NA_Lead_Confirmation__c,因此通过trigger实现
* 如果是客户经理创建,那么在创建记录时即设置此字段
*	
*/
public without sharing class LeadSetDateHandler  implements Triggers.Handler{//before update
	public void handle(){
		for(Lead l : (List<Lead>)Trigger.new){
			if(l.NA_Lead_Confirmation__c == '确认有效' && 
				((Lead)Trigger.oldMap.get(l.id)).NA_Lead_Confirmation__c != '确认有效'){
				l.Confirm_Date__c = DateTime.now();
			}
		}
	}
}