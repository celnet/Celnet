/*
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-8-27
 * Description: Account的Handler，同步到EIP
 */

public class Lepus_AccountHandler implements Triggers.Handler{
    public static boolean isFirstRun = true;
    
    public void handle(){
        
        // 触发记录日志
        List<Lepus_Log__c> logList = new List<Lepus_Log__c>();
        List<Account> triggerAccList = Lepus_HandlerUtil.retrieveRecordList();
        Set<Id> triggerAccIdSet = new Set<Id>();
        
        for(Account acc : triggerAccList){
            if(Lepus_HandlerUtil.filterRecordType(acc, Account.sobjecttype)){
                Lepus_Log__c log = new Lepus_Log__c();
                log.UniqueId__c = acc.Id + acc.LastModifiedDate.formatGMT('yyyy-MM-dd HH:mm:ss');
                log.Updated__c = true;
                log.RecordId__c = acc.Id;
                log.LastModifiedDate__c = acc.LastModifiedDate;
                log.InTrigger__c = true;
                logList.add(log);
                
                triggerAccIdSet.add(acc.Id);
            }
        }
        
        if(logList.size() > 0)
        upsert logList UniqueId__c;
        
        List<Id> accountIdList = new List<Id>();
        String action = Lepus_HandlerUtil.retrieveAction();
        List<Account> accountList = (List<Account>)Lepus_HandlerUtil.retrieveRecordList();
        Map<Id , Account> map_Account = new Map<Id ,Account>();
        
        for(Account acc : accountList){
            if(Lepus_HandlerUtil.filterRecordType(acc, Account.sobjecttype)){
                
                accountIdList.add(acc.Id);
                map_Account.put(acc.Id , acc);
            }
        }
        
        boolean syncBusinessData = (Lepus_Data_Sync_Controller__c.getInstance('客户业务数据') != null) && 
                                        Lepus_Data_Sync_Controller__c.getInstance('客户业务数据').IsSync__c;
        boolean syncTeamMember = (Lepus_Data_Sync_Controller__c.getInstance('客户团队') != null) && 
                                        Lepus_Data_Sync_Controller__c.getInstance('客户团队').IsSync__c;
        
        if(syncBusinessData){
            
            if(accountIdList.size() == 1 && Limits.getFutureCalls() < Limits.getLimitFutureCalls()){
                if(syncTeamMember && action.toLowerCase().equals('insert')){
                    Lepus_FutureCallout.syncDataAndTeamMember(accountIdList[0], 'account','');
                } else {
                    try{
                        Lepus_FutureCallout.syncData(accountIdList[0], action, 'account');
                    } catch (Exception ae){
                        Lepus_SyncUtil.initQueue(map_Account, accountIdList, action, '业务数据同步', datetime.now());
                    }
                }
            } else if(accountIdList.size() > 0){
                Lepus_SyncUtil.initQueue(map_Account, accountIdList, action, '业务数据同步', datetime.now());
            }
        }


        if(syncTeamMember){
        	List<Id> syncAccIdList = new List<Id>();
            if(action.toLowerCase().equals('update')){
                for(Id accId : accountIdList){
                    if((trigger.newMap.get(accId)).get('OwnerId') != (trigger.oldMap.get(accId)).get('OwnerId')
                        || (trigger.newMap.get(accId)).get('Name') != (trigger.oldMap.get(accId)).get('Name')){
                        syncAccIdList.add(accId);
                    }
                }
            } else if(action.toLowerCase().equals('delete')){
            	syncAccIdList = accountIdList;
            }
            
            if(syncAccIdList.size() == 1 && Limits.getFutureCalls() < Limits.getLimitFutureCalls()){
                Lepus_FutureCallout.syncTeamMember(syncAccIdList, 'account', action);
            } else if(syncAccIdList.size() > 0){
                Lepus_SyncUtil.initQueue(map_Account, syncAccIdList, action, '团队成员同步', datetime.now());
            }
        }
    }
}