/*
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-8-27
 * Description: 同步OpportunityProduct到EIP
 *              insert, delete时会触发TigOnProjectProduct.trigger 更新Opportunity 从而 触发 Opportunity.trigger进行同步
 *              update时 复制TigOnProjectProduct.trigger 逻辑，判断不更新Opportunity时 在这里同步
 */
public class Lepus_OpportunityProductHandler implements Triggers.Handler{
	public static boolean isFirstRun = true;
    public void handle(){
        if(Lepus_OpportunityProductHandler.isFirstRun){
            isFirstRun = false;
            if(trigger.isUpdate){
                if(Lepus_Data_Sync_Controller__c.getInstance('机会点业务数据') != null && Lepus_Data_Sync_Controller__c.getInstance('机会点业务数据').IsSync__c){
                	// 复制TigOnProjectProduct.trigger逻辑 
                	Set<Id> oppIdSet = new Set<Id>();
      	 			Set<Id> filterOppIdSet = new Set<Id>();
                	for (Project_Product__c newProduct : (List<Project_Product__c>)trigger.new) {
                		Project_Product__c oldProduct = (Project_Product__c)trigger.oldMap.get(newProduct.Id);
				        if(newProduct.Lookup__c != oldProduct.Lookup__c){
				        	filterOppIdSet.add(newProduct.Project_Name__c);
				        } 
				        oppIdSet.add(newProduct.Project_Name__c);
				    }
				    oppIdSet.removeAll(filterOppIdSet);
				    
				    List<Id> oppIdList = new List<Id>(oppIdSet);
				    map<Id, String> oppIdGlobalIDMap = new map<Id, String>();
				    
				    for(Opportunity opp : [Select Id, Global_Id__c From Opportunity Where Id IN: oppIdList]){
				    	oppIdGlobalIDMap.put(opp.Id, opp.Global_Id__c);
				    }
				    
				    if(oppIdList.size() == 1){
				    	try{
				    		Lepus_FutureCallout.syncData(oppIdList[0], 'update', 'opportunity');
				    	} catch(System.AsyncException ae){
				    		Lepus_SyncUtil.initQueue(oppIdList, oppIdGlobalIDMap, 'update', '业务数据同步', datetime.now());
				    	}
				    } else if(oppIdList.size() > 1){
				    	Lepus_SyncUtil.initQueue(oppIdList, oppIdGlobalIDMap, 'update', '业务数据同步', datetime.now());
				    }
                }
            }
        }
    }
}