/**
 * @Purpose : 1、非一线创建的中国区线索，系统根据线索的代表处信息自动分发给代表处销管；   before insert
 *            2、一线用户创建的中国区线索，系统自动将线索状态设置成跟进中，且Owner不发生变化； before insert
 *            3、待分发状态的线索，如果修改了线索的代表处信息，系统会自动分发给新的代表处销管；before update
 * @Author : Steven
 * @Date : 2014-06-16
 */
public without sharing class ChinaLeadDistributeHandler implements Triggers.Handler{ //before insert before update
	public Set<Id> ownerIds {get;set;}  //线索分发责任人ID
	public Set<Id> inactiveOwnerIds {get;set;} //失效的线索分发责任人ID 
	public Map<String,Id> ownerMap {get;set;}  //线索分发责任人ID和区域的对应关系
	public Boolean accManagerCreate {get;set;} //是否是一线创建的线索
	public Boolean hasInit {get;set;} 
	
	public void handle() {
		this.hasInit = false;
		for(Lead l : (List<Lead>)Trigger.new) {
			if(l.RecordTypeId == CONSTANTS.CHINALEADRECORDTYPE) {
				//Step1：创建线索时，根据是否一线用户创建，初始化线索状态
				if(Trigger.isInsert){
					l.China_Lead_Creator_Role__c = getCurrentUserProfile();
					if(this.accManagerCreate){
						l.Status = CONSTANTS.CHINALEADSTATUSFOLLOW;
					} else {
						l.Status = CONSTANTS.CHINALEADSTATUSASSIGN;
					}
				}
				//Step2：更新线索时，如果线索是待分发状态，且修改了区域信息，重新分配给对应区域销管
				if(Trigger.isUpdate){
					String newRegion = l.China_Representative_Office__c;
					String oldRegion = ((Lead)Trigger.oldMap.get(l.id)).China_Representative_Office__c;
					if((l.Status != CONSTANTS.CHINALEADSTATUSASSIGN) || (newRegion == oldRegion)) continue;
				}
				//Step3：根据线索的区域信息，自动查找线索分配的责任人
				if(!this.hasInit) initSalesManagerInfo();
				Id ownerId = this.ownerMap.get(l.China_Representative_Office__c);
				if((ownerId != null) && (!this.inactiveOwnerIds.contains(ownerId))) {
					l.ownerId = ownerId;
				}else{
					String salesContactName = System.Label.China_Sales_Dept_Contact;
					l.addError('没有查询到线索所属区域"'+ l.China_Representative_Office__c +'"的生效销管账号，线索无法自动分发，请联系"'+ salesContactName +'"进行维护');
				}
			} //如果是中国区线索
		} //for end
	} //handle end
	
	/**
	 * 初始化区域-销管对应关系MAP
	 */
	private void initSalesManagerInfo() {
		this.ownerIds = new Set<Id>();
		this.inactiveOwnerIds = new Set<Id>();
		this.ownerMap = new Map<String,Id>();
		this.hasInit = true;
		//Step1：查找出所有的区域销管MAP关系
		List<Account_Link_Approver_Map__c> approvers = 
			[Select Region__c,Representative_Office__c,Approver__c 
				 FROM Account_Link_Approver_Map__c Where Type__c = '销管' And recordtypeid =:CONSTANTS.APPROVERMAPCHINARECORDTYPE];
		for(Account_Link_Approver_Map__c a : approvers){
			this.ownerMap.put(a.Representative_Office__c,a.Approver__c);
			this.ownerIds.add(a.Approver__c);
		}
		//Step2：查找出所有失效的区域销管
	  List<User> owners = [Select Id FROM User where Id =: ownerIds and IsActive = false];
	  for(User u : owners){
			this.inactiveOwnerIds.add(u.id);
		}
	}
	
	/**
	 * 获取当前用户的简档名称，用于判断是MKT线索还是商业线索，及是否是一线创建的线索
	 */
	private String getCurrentUserProfile(){
		String result = 'Sales';
		this.accManagerCreate = false;
		//Step1：判断是否是一线创建的线索
		Profile currentUserProfile = [select id,Name from Profile where id = :Userinfo.getProfileId()];
		if(currentUserProfile.name.contains('代表处客户经理') || currentUserProfile.name.contains('系统部客户经理') || 
			 currentUserProfile.name.contains('代表处产品经理') || currentUserProfile.name.contains('系统部产品经理') || 
			 currentUserProfile.name.contains('代表处渠道经理') || currentUserProfile.name.contains('系统部渠道经理')){
			accManagerCreate = true;
		}
		//Step2：判断是MKT线索还是商业线索
		if(currentUserProfile.name.contains('(Huawei China)MKT')){
			result = 'MKT';
		} else if(currentUserProfile.name.contains('(Huawei China)商业销售部')
			      ||currentUserProfile.name.contains('(Huawei China)分销业务部')
			      ||currentUserProfile.name.contains('(Huawei China)中小企业业务部')){
			result = 'Commercial';
		}
		return result;
	}

}