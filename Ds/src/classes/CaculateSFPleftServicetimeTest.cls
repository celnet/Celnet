/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CaculateSFPleftServicetimeTest {

    static testMethod void myUnitTest() {
    	RecordType chinaRecordType = [select id from RecordType where isActive = true 
    		and developername like 'N_Health_Check%' and SobjectType = 'Work_Order__c' limit 1];
        Account acc = UtilUnitTest.createAccountWithName('Test');
        Contact contact = UtilUnitTest.createContact(acc);
        CSS_Contract__c c = UtilUnitTest.createContract(acc);
        SFP__c sfp = UtilUnitTest.createSFP(c);
        SFP.No_of_Health_Check_Service__c=2;
        update sfp;
        Case sr = UtilUnitTest.createCCRRequest(contact);
        Work_Order__c wo = UtilUnitTest.createOnSiteWorkOrder(acc, sr);
        wo.RecordTypeId = chinaRecordType.id;
        wo.Status__c='Working';
        update wo;
        wo.Status__c='Cancelled';
        update wo;
        wo.Status__c='Working';
        delete wo;
        
    }
}