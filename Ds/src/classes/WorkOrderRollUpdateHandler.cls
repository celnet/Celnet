/*
* @Purpose : 
* 新建、更新、删除时	更新关联SR的On Site Work Order Number字段
*	更新为SR下所有现场服务工单的数量
*
*
* @Author : Brain Zhang
* @Date :       2013-8
*
*
*/
public without sharing class WorkOrderRollUpdateHandler implements Triggers.Handler{
	public void handle(){
    Set<Id> caseIDs = new Set<Id>();
    if(trigger.isInsert || trigger.isUnDelete){
    	for(Work_Order__c w : (List<Work_Order__c>)trigger.new){
				if(w.RecordTypeDeveloperName__c.startsWith('N_On_Site') && w.Service_Request_No__c != null ){
					caseIDs.add(w.Service_Request_No__c);
				}
    	}
    }
		if(trigger.isUpdate ){
			for(Work_Order__c w : (List<Work_Order__c>)trigger.new){
				if(w.RecordTypeDeveloperName__c.startsWith('N_On_Site') &&
					(w.Service_Request_No__c != ((Work_Order__c)trigger.oldmap.get(w.id)).Service_Request_No__c
					||w.Number_of_SparePart__c != ((Work_Order__c)Trigger.oldmap.get(w.id)).Number_of_SparePart__c)
					){
					caseIDs.add(w.Service_Request_No__c);
					//一并加入原来的service request no
					caseIDs.add(((Work_Order__c)trigger.oldmap.get(w.id)).Service_Request_No__c);
				}
			}
        
		}
		if(trigger.isDelete){
       for(Work_Order__c w : (List<Work_Order__c>)trigger.old){
				if(w.RecordTypeDeveloperName__c.startsWith('N_On_Site') && w.Service_Request_No__c != null){
					caseIDs.add(w.Service_Request_No__c);
				}
			}
    }
    System.debug('WorkOrderRollUpdateHandler : the  caseIds:--------------' + caseIDs);
    if(caseIDs.size() > 0){
    	
    	UtilLREngine.Context ctx = new UtilLREngine.Context(Case.SobjectType, // parent object
                                            Work_Order__c.SobjectType,  // child object
                                            Schema.SObjectType.Work_Order__c.fields.Service_Request_No__c // relationship field name
                                            );    
    	ctx.add(
            new UtilLREngine.RollupSummaryField(
                                            Schema.SObjectType.Case.fields.On_Site_Work_Order_Number__c,
                                            Schema.SObjectType.Work_Order__c.fields.id,
                                            UtilLREngine.RollupOperation.Count 
                                       )); 
//计算SR下的工单备件数量
    	ctx.add(
            new UtilLREngine.RollupSummaryField(
                                            Schema.SObjectType.Case.fields.Spare_Amount__c,
                                            Schema.SObjectType.Work_Order__c.fields.Number_of_SparePart__c,
                                            UtilLREngine.RollupOperation.Sum 
                                       )); 

      List<Case> cases = (List<Case>)UtilLREngine.rollUp(ctx, caseIDs);
      System.debug('the cases:--------- ' + cases);
	    CommonConstant.changedFromAccount = true;//add this to ignore the other trigger
	    // ignore all the trigger handlers on service request
	    CommonConstant.serviceRequestTriggerShouldRun = false;
			update cases;
    }
	}

}