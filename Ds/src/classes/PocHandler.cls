/*
 * @Purpose : 海外POC更新时自动刷新机会点的POC,如果被机会点引用不允许删除
 * @Author : Steven
 * @Date : 2013-11-05
 */
public without sharing class PocHandler implements Triggers.Handler{ //before update before delete
	public static Boolean isFirstRun = true;
	public void handle() {
		if(!PocHandler.isFirstRun) return;//如果是被PocInOpportunityHandler触发直接返回
		PocInOpportunityHandler.isFirstRun = false;//禁止触发PocInOpportunityHandler
		//Type1：如果是修改机会点POC,先更新关联的客户POC
		if(Trigger.isUpdate){
			for(POC__c poc : (List<POC__c>)Trigger.new){
				//如果被机会点引用了同步刷新机会点上的POC
				if(poc.Opportunity__c != null) {
					List<POC_In_Opportunity__c> updatePocInOpp = 
						[Select name,Main_Products__c,POC_Completion_Date__c,POC_Start_Date__c,
										POC_Failed_Reason_Analysis__c,POC_Records__c,POC_Status__c
							 FROM POC_In_Opportunity__c where POC__c =: poc.Id];
					for(POC_In_Opportunity__c pocInOpp : updatePocInOpp){
						pocInOpp.New_POC_Name__c = poc.Name;
						pocInOpp.Main_Products__c = poc.Main_Products__c;
						pocInOpp.POC_Completion_Date__c = poc.POC_Completion_Date__c;
						pocInOpp.POC_Failed_Reason_Analysis__c = poc.POC_Failed_Reason_Analysis__c;
						pocInOpp.POC_Records__c = poc.POC_Records__c;
						pocInOpp.POC_Start_Date__c = poc.POC_Start_Date__c;
						pocInOpp.POC_Status__c = poc.POC_Status__c;
						update pocInOpp;
					}
				}
			}
		}
		//Type2：如果被机会点引用不允许删除POC
		if(Trigger.isDelete){
			for(POC__c delPoc : (List<POC__c>)trigger.old){
				List<POC_In_Opportunity__c> pocInOppList = [Select id FROM POC_In_Opportunity__c where POC__c =: delPoc.Id];
				if(pocInOppList.size()>0) {
					delPoc.addError('This POC has been used by opportunity.');
				}
			}
		}
	} // handle end
}