/**
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-06-17
 * Description: 创建Lead或者ChangeOwner后，共享给商业Leader
 */
public without sharing class ChinaLeadBusinessLeaderShareHandler implements Triggers.Handler{
	public void handle(){
		if(Trigger.isAfter){ 
			// Map<Lead的Id,Lead所有人的Id>
			Map<Id, Id> insertOwnerMap = new Map<Id, Id>();
			Map<Id, String> insertRegionMap = new Map<Id, String>();
			for(Lead l : (List<Lead>)trigger.new){
				if(Trigger.isUpdate){//仅修改了Owner或者新建线索时共享给商业leader
					Lead oldLead = (Lead)trigger.oldMap.get(l.id);
					if((oldLead.OwnerId == l.OwnerId) && (oldLead.China_Representative_Office__c == l.China_Representative_Office__c)) continue;
				}
				if(l.RecordTypeId == CONSTANTS.CHINALEADRECORDTYPE){
					insertOwnerMap.put(l.Id, l.OwnerId);
					insertRegionMap.put(l.Id, l.China_Representative_Office__c);
				}
			}
			if(insertOwnerMap.values().size()==0) return;
			this.shareLeadToBusinessLeader(insertOwnerMap, insertRegionMap);
		}
	}

	private void shareLeadToBusinessLeader(Map<Id, Id> ownerMap, Map<Id, String> regionMap){
		List<LeadShare> leadShareList = new List<LeadShare>();
		// 查找出关联用户不是Lead Owner的Special Role
		List<Special_Role__c> srList = 
			[Select Id, User__c,User__r.IsActive, Representative_Office__c From Special_Role__c 
				Where Role_Name__c = 'China_Business_Leader' 
				And Representative_Office__c IN: regionMap.values()];
		Set<Id> leadIdSet = ownerMap.keySet();
		
		for(Special_Role__c sr : srList){
			if(!sr.User__r.IsActive) continue;
			for(Id leadId : leadIdSet){
				if(sr.User__c != ownerMap.get(leadId) && sr.Representative_Office__c == regionMap.get(leadId)){
					leadShareList.add(new LeadShare(LeadId = leadId, UserOrGroupId = sr.User__c, LeadAccessLevel = 'Read'));
				}
			}
		}
		
		if(leadShareList.size() > 0){
			insert leadShareList;
		}
	}
}