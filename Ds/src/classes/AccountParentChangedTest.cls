@isTest
private class AccountParentChangedTest {
	
	static testMethod void save1(){//not na child add na parent
		Account acc = UtilUnitTest.createHWChinaAccount('acc');
		Named_Account__c na = new Named_Account__c(Account_Name__c = acc.id,
    		Is_Named_Account__c = true,Approved_Date__c = Date.today(),Named_Account_Character__c = 'character',
    		Named_Account_Level__c= 'level',Named_Account_Property__c = 'property' );
    insert na;
    acc = [select id,Is_Named_Account__c,NA_Approved_Date__c,Named_Account_Character__c,
    	Named_Account_Level__c,Named_Account_Property__c from Account where id = :acc.id];
    System.assertEquals(na.Is_Named_Account__c , acc.Is_Named_Account__c);
    System.assertEquals(na.Approved_Date__c , acc.NA_Approved_Date__c);
    System.assertEquals(na.Named_Account_Character__c , acc.Named_Account_Character__c);
    System.assertEquals(na.Named_Account_Level__c , acc.Named_Account_Level__c);
    System.assertEquals(na.Named_Account_Property__c , acc.Named_Account_Property__c);
    Account child = UtilUnitTest.createHWChinaAccount('child');
    
    Test.setCurrentPage(Page.AccountParentChanged);
    AccountParentChanged controller = new AccountParentChanged(new ApexPages.StandardController(child));
    controller.acc.parentid = acc.id;
    Test.startTest();
    controller.validation();
    controller.save();
    Test.stopTest();
    
    child = [select id,Is_Named_Account__c,NA_Approved_Date__c,Named_Account_Character__c,
    	Named_Account_Level__c,Named_Account_Property__c from Account where id = :child.id];
    System.assertEquals(na.Is_Named_Account__c , child.Is_Named_Account__c);
    System.assertEquals(na.Approved_Date__c , child.NA_Approved_Date__c);
    System.assertEquals(na.Named_Account_Character__c , child.Named_Account_Character__c);
    System.assertEquals(na.Named_Account_Level__c , child.Named_Account_Level__c);
    System.assertEquals(na.Named_Account_Property__c , child.Named_Account_Property__c);
     
	}
	
		static testMethod void save2(){
		Account acc = UtilUnitTest.createHWChinaAccount('acc');
		Named_Account__c na = new Named_Account__c(Account_Name__c = acc.id,
    		Is_Named_Account__c = true,Approved_Date__c = Date.today(),Named_Account_Character__c = 'character',
    		Named_Account_Level__c= 'level',Named_Account_Property__c = 'property' );
    insert na;
    acc = [select id,Is_Named_Account__c,NA_Approved_Date__c,Named_Account_Character__c,
    	Named_Account_Level__c,Named_Account_Property__c from Account where id = :acc.id];
    Account child = UtilUnitTest.createHWChinaAccount('child');
    child.Is_Named_Account__c = false;
    update child;
    Account child2 = UtilUnitTest.createHWChinaAccount('child2');
    Account child3 = UtilUnitTest.createHWChinaAccount('child3');
    Test.setCurrentPage(Page.AccountParentChanged);
    AccountParentChanged controller = new AccountParentChanged(new ApexPages.StandardController(child));
    controller.acc.parentid = acc.id;
    Test.startTest();
    controller.validation();
    controller.save();
    
    controller.acc.CIS__c = null;
    controller.validation();
    
    child2.ParentId = child.id;
    update child2;
    controller.validation();
    
    child3.ParentId = acc.id;
    update child3;
    controller.acc.parentid = child3.id;
    controller.validation();
    
    controller.acc.parentid = controller.acc.id;
    controller.validation();
    
    controller.acc.parentid = acc.id;
    List<User> approvers = [Select id FROM User where isactive = true and profileid = '00e10000000MKiC'];
		controller.request.Approver__c = approvers[0].id;
    controller.startApproval();
    
    Test.stopTest();      
	}
	
	static testMethod void save3(){
		Account acc = UtilUnitTest.createHWChinaAccount('acc');
		Named_Account__c na = new Named_Account__c(Account_Name__c = acc.id,
    		Is_Named_Account__c = true,Approved_Date__c = Date.today(),Named_Account_Character__c = 'character',
    		Named_Account_Level__c= 'level',Named_Account_Property__c = 'property' );
    insert na;
    acc = [select id,Is_Named_Account__c,NA_Approved_Date__c,Named_Account_Character__c,
    	Named_Account_Level__c,Named_Account_Property__c from Account where id = :acc.id];
    Account child = UtilUnitTest.createHWChinaAccount('child');
    child.Is_Named_Account__c = false;
    child.ParentId = acc.id;
    update child;
    Test.setCurrentPage(Page.AccountParentChanged);
    AccountParentChanged controller = new AccountParentChanged(new ApexPages.StandardController(child));
    controller.acc.parentid = null;
    Test.startTest();
    controller.validation();
    
    controller.acc.Inactive_NA__c = true;
    controller.validation();
    controller.acc.Inactive_NA__c = false;
    controller.validation();
    controller.acc.ParentId = acc.id;
    controller.validation();
    
    Test.stopTest();     
	}
	
	static testMethod void save4(){
		Account acc = UtilUnitTest.createHWChinaAccount('acc');
		Named_Account__c na = new Named_Account__c(Account_Name__c = acc.id,
    		Is_Named_Account__c = true,Approved_Date__c = Date.today(),Named_Account_Character__c = 'character',
    		Named_Account_Level__c= 'level',Named_Account_Property__c = 'property' );
    insert na;
    acc = [select id,Is_Named_Account__c,NA_Approved_Date__c,Named_Account_Character__c,
    	Named_Account_Level__c,Named_Account_Property__c from Account where id = :acc.id];
    Account child = UtilUnitTest.createHWChinaAccount('child');
    child.Is_Named_Account__c = false;
    update child;
    
    Opportunity opp = new Opportunity();
		opp.recordtypeid = CONSTANTS.CHINAOPPORTUNITYRECORDTYPE;
		opp.accountid = child.id;
		opp.name = 'testOpp';
		opp.closedate = Date.today();
		opp.stagename = 'SS4 (Developing Solution)';
		opp.Opportunity_type__c='Direct Sales';
		opp.Participate_Space__c = 1;
		insert opp;
    
    Test.setCurrentPage(Page.AccountParentChanged);
    AccountParentChanged controller = new AccountParentChanged(new ApexPages.StandardController(child));
    controller.acc.parentid = acc.id;
    Test.startTest();
    controller.validation();
    Test.stopTest();     
	}
	
}