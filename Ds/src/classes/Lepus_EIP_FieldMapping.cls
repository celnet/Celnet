/**
 * @Purpose : Salesforce字段与天兔字段对应
 * @Author : 孙达
 * @Date : 2014-07-29
 **/
public class Lepus_EIP_FieldMapping {
	//机会点字段Mapping
	public static List<fieldMap> getOppFieldMap(){
		List<fieldMap> list_fMap = new list<fieldMap>();
		list_fMap.add(new fieldMap('Id' , 'sfID'));
		list_fMap.add(new fieldMap('PRM_ID__c' , 'prmID'));
		list_fMap.add(new fieldMap('Global_ID__C' , 'sRowIdx'));
		list_fMap.add(new fieldMap('Name' , 'opportunityName'));
		list_fMap.add(new fieldMap('Opportunity_Number__c' , 'opportunityCodeSF'));
		list_fMap.add(new fieldMap('Opportunity_Level__c' , 'opportunityLevel'));
		list_fMap.add(new fieldMap('Opportunity_Level_Code__c' , 'opportunityLevelCode'));
		list_fMap.add(new fieldMap('Participate_Space__c' , 'opportunitySize'));
		list_fMap.add(new fieldMap('Win_Odds__c' , 'winProbability'));
		list_fMap.add(new fieldMap('Owner.Employee_ID__c' , 'hwOwnerName')); // relationship
		list_fMap.add(new fieldMap('Main_Competitor__c' , 'mainCompetitor'));
		list_fMap.add(new fieldMap('Other_Competitor__c' , 'otherCompetitor'));
		list_fMap.add(new fieldMap('CloseDate' , 'estimatedSignDate'));
		list_fMap.add(new fieldMap('Estimated_Shipment_Date__c' , 'estimatedShipmentDate'));
		list_fMap.add(new fieldMap('Estimated_ContractSign_Amount__c' , 'estimatedSignAmount'));
		list_fMap.add(new fieldMap('Opportunity_Progress__c' , 'huaweiProg'));
		list_fMap.add(new fieldMap('Estimated_PO_Amount__c' , 'estimatedPoAmount'));
		list_fMap.add(new fieldMap('Actual_PO_Amount__c' , 'actualPOAmount'));
		list_fMap.add(new fieldMap('Ati_Status__c' , 'atiStatus'));
		list_fMap.add(new fieldMap('CurrencyIsoCode' , 'currencyCode'));
		list_fMap.add(new fieldMap('Opportunity_type__c' , 'salesMode'));
		list_fMap.add(new fieldMap('Opportunity_Type_Code__c' , 'salesModeCode'));
		list_fMap.add(new fieldMap('StageName' , 'salesStage'));
		list_fMap.add(new fieldMap('NextStep' , 'nextStep'));
		list_fMap.add(new fieldMap('HW_Master_Product__c' , 'hwMasterProduct'));
		list_fMap.add(new fieldMap('Other_Products__c' , 'otherProduct'));
		list_fMap.add(new fieldMap('Master_Channer_Partner__c' , 'masterChannelPartner'));
		list_fMap.add(new fieldMap('City__c' , 'city'));
		list_fMap.add(new fieldMap('City_Code__c' , 'hwCityId'));
		list_fMap.add(new fieldMap('Province__c' , 'province'));
		list_fMap.add(new fieldMap('Province_Code__c' , 'provinceCode'));
		list_fMap.add(new fieldMap('Region__c' , 'hwRegionName'));
		list_fMap.add(new fieldMap('Representative_Office__c' , 'repOffice'));
		list_fMap.add(new fieldMap('Representative_Office_Code__c' , 'hwRepOfficeId'));
		list_fMap.add(new fieldMap('Country__c' , 'hwCountry'));
		list_fMap.add(new fieldMap('Country_Code__c' , 'hwCountryId'));
		list_fMap.add(new fieldMap('System_Department__c' , 'systemDepartment'));
		list_fMap.add(new fieldMap('Sub_Industry__c' , 'subIndustry'));
		list_fMap.add(new fieldMap('Customer_Group__c' , 'customerGroup'));
		list_fMap.add(new fieldMap('Customer_Group_Code__c' , 'customerGroupCode'));
		list_fMap.add(new fieldMap('Industry__c' , 'industry'));
		list_fMap.add(new fieldMap('Sub_Industry_E__c' , 'subIndustryE'));
		list_fMap.add(new fieldMap('Channel_Path__c' , 'channelPath'));
		list_fMap.add(new fieldMap('Primary_Channel_Partner__c' , 'vap'));
		list_fMap.add(new fieldMap('Distributor__c' , 'distributor'));
		list_fMap.add(new fieldMap('GP__c' , 'gp'));
		list_fMap.add(new fieldMap('Tier2_Partner__c' , 'tier2Partner'));
		list_fMap.add(new fieldMap('Carrier__c', 'carrier'));
		list_fMap.add(new fieldMap('AccountId' , 'accountId'));
		list_fMap.add(new fieldMap('Account.Name' , 'accountName')); // relationship
		list_fMap.add(new fieldMap('Project_Progress__c', 'opportunityProgress'));
		list_fMap.add(new fieldMap('Lost_Reason__c' , 'lostReason'));
		list_fMap.add(new fieldMap('Lost_Reason_Description__c' , 'lostReasonDescription'));
		list_fMap.add(new fieldMap('Winning_Deal_Vendor__c' , 'winVendor'));
		//list_fMap.add(new fieldMap('WinCom_Cisco__c' , 'winningDealVendor'));
		//list_fMap.add(new fieldMap('WinCom_EMC__c' , 'winningDealVendor'));
		//list_fMap.add(new fieldMap('WinCom_H3C__c' , 'winningDealVendor'));
		//list_fMap.add(new fieldMap('WinCom_HongShan__c' , 'winningDealVendor'));
		//list_fMap.add(new fieldMap('WinCom_Polycom__c' , 'winningDealVendor'));
		//list_fMap.add(new fieldMap('WinCom_ZhongXin__c' , 'winningDealVendor'));
		//list_fMap.add(new fieldMap('WinCom_DELL__c' , 'winningDealVendor'));
		//list_fMap.add(new fieldMap('WinCom_HP__c' , 'winningDealVendor'));
		list_fMap.add(new fieldMap('WinCom_Other__c' , 'winComOther'));
		list_fMap.add(new fieldMap('China_Competitor_Trends__c' , 'chinaCompetitorTrends'));
		list_fMap.add(new fieldMap('Project_Risk__c' , 'projectRisk'));
		list_fMap.add(new fieldMap('China_Following_Strategy__c' , 'chinaFollowingStrategy'));
		list_fMap.add(new fieldMap('Difficulties_and_Help_Type__c' , 'difficultType'));
		list_fMap.add(new fieldMap('Difficulties_and_Help__c' , 'difficultiesAndHelp'));
		list_fMap.add(new fieldMap('Difficulty_and_Help__c' , 'difficultyAndHelp'));
		list_fMap.add(new fieldMap('Project_Type_Display__c' , 'naProject'));
		list_fMap.add(new fieldMap('CreatedById' , 'createdBy'));
		list_fMap.add(new fieldMap('CREATEDDATE' , 'creationDate'));
		list_fMap.add(new fieldMap('LastModifiedById' , 'lastModifiedBy'));
		list_fMap.add(new fieldMap('LASTMODIFIEDDATE' , 'lastModifiedDate'));
		list_fMap.add(new fieldMap('RecordTypeId' , 'recordTypeId'));
		list_fMap.add(new fieldMap('OwnerId','ownerId'));
		
		list_fMap.add(new fieldMap('Bidding_date__c' , 'wsBiddingdatec'));
		list_fMap.add(new fieldMap('Bidding_Risk_Type__c' , 'wsBiddingRiskTypec'));
		list_fMap.add(new fieldMap('CampaignId' , 'wsCampaignId'));
		list_fMap.add(new fieldMap('China_Revenue_Confirm_Time__c' , 'wsChinaRevenueConfirmTimec'));
		list_fMap.add(new fieldMap('China_Revenue_Progress__c' , 'wsChinaRevenueProgressc'));
		list_fMap.add(new fieldMap('CQ__c' , 'wsCQc'));
		list_fMap.add(new fieldMap('CQ_Income_Risk__c' , 'wsCQIncomeRiskc'));
		list_fMap.add(new fieldMap('Estimated_End_User_Sign_Date__c' , 'wsEstimatedEndUserSignDatec'));
		list_fMap.add(new fieldMap('Estimated_PO_Date__c' , 'wsEstimatedPODatec'));
		list_fMap.add(new fieldMap('Forecast__c' , 'wsForecastc'));
		list_fMap.add(new fieldMap('Forecast_Revenue_Finally__c' , 'wsForecastRevenueFinallyc'));
		list_fMap.add(new fieldMap('ForecastCategory' , 'wsForecastCategory'));
		list_fMap.add(new fieldMap('Industry_HW__c','wsIndustryHWc'));
		list_fMap.add(new fieldMap('Invite_Bid_Date__c' , 'wsInviteBidDatec'));
		list_fMap.add(new fieldMap('Is_403_Project__c' , 'wsIs403Projectc'));
		list_fMap.add(new fieldMap('Lead__c' , 'wsLeadc'));
		list_fMap.add(new fieldMap('Market_Target__c' , 'wsMarketTargetc'));
		list_fMap.add(new fieldMap('Opportunity_Owner__c' , 'wsOpportunityOwnerc'));
		list_fMap.add(new fieldMap('Opportunity_Score__c' , 'wsOpportunityScorec'));
		list_fMap.add(new fieldMap('Opportunity_Type_new__c' , 'wsOpportunityTypenewc'));
		list_fMap.add(new fieldMap('PV__c' , 'wsPVc'));
		list_fMap.add(new fieldMap('Representative_Office_HW__c' , 'wsRepresentativeOfficeHWc'));
		list_fMap.add(new fieldMap('TCG_Name__c' , 'wsTCGNamec'));
		list_fMap.add(new fieldMap('To_Be_Deleted__c' , 'wsToBeDeletedc'));
		list_fMap.add(new fieldMap('To_Be_Project__c' , 'wsToBeProjectc'));
		list_fMap.add(new fieldMap('X403_Project_Amount__c' , 'wsX403ProjectAmountc'));
		list_fMap.add(new fieldMap('X403_Project_Type__c' , 'wsX403ProjectTypec'));
		
		return list_fMap;
	}
	//客户字段Mapping
	public static List<fieldMap> getAccFieldMap(){
		List<fieldMap> list_fMap = new list<fieldMap>();
		list_fMap.add(new fieldMap('ID' , 'sfID' ));
		//list_fMap.add(new fieldMap('ID' , 'accountId' ));
		list_fMap.add(new fieldMap('Global_ID__c' , 'sRowIdx' ));
		list_fMap.add(new fieldMap('Name' , 'accountName' ));
		list_fMap.add(new fieldMap('REGION_HW__C' , 'region' ));
		list_fMap.add(new fieldMap('REPRESENTATIVE_OFFICE__C' , 'repOffice' ));
		list_fMap.add(new fieldMap('COUNTRY_HW__C' , 'hwCountry' ));
		//list_fMap.add(new fieldMap('COUNTRY_HW__C' , 'country' ));
		list_fMap.add(new fieldMap('COUNTRY_Code_HW__C' , 'hwCountryId' ));
		list_fMap.add(new fieldMap('PROVINCE__C' , 'province' ));
		list_fMap.add(new fieldMap('CITY_HUAWEI_CHINA__C' , 'cityC' ));
		//list_fMap.add(new fieldMap('City_Code__c' , 'hwCityId')); // Salesforce中无此字段
		list_fMap.add(new fieldMap('IS_NAMED_ACCOUNT__C' , 'isNAAccount' ));
		list_fMap.add(new fieldMap('STATE_PROVINCE__C' , 'state' ));
		list_fMap.add(new fieldMap('CITY__C' , 'cityE' ));
		list_fMap.add(new fieldMap('IS_NAMED_ACCOUNT_E__C' , 'isNAAccountE' ));
		list_fMap.add(new fieldMap('IS_403_CUSTOMER__C' , 'is403Customer' ));
		list_fMap.add(new fieldMap('INACTIVE_NA__C' , 'isActive' ));
		list_fMap.add(new fieldMap('ACCOUNT_LEVEL__C' , 'accountLevel' ));
		list_fMap.add(new fieldMap('BLACKLIST_TYPE__C' , 'blackListType' ));
		list_fMap.add(new fieldMap('CIS__C' , 'accountNumber' ));
		list_fMap.add(new fieldMap('Parent.Name' , 'parentAccount' )); // relationship
		list_fMap.add(new fieldMap('PARENTID' , 'parentID' ));
		list_fMap.add(new fieldMap('NAMED_ACCOUNT_PROPERTY__C' , 'namedAccountProperty' ));
		list_fMap.add(new fieldMap('NAMED_ACCOUNT_LEVEL__C' , 'namedAccountLevel' ));
		list_fMap.add(new fieldMap('NAMED_ACCOUNT_CHARACTER__C' , 'namedAccountCharacter' ));
		list_fMap.add(new fieldMap('NAMED_ACCOUNT_CATEGORY__C' , 'namedAccountCategory' ));
		list_fMap.add(new fieldMap('NAMED_ACCOUNT_TYPE__C' , 'namedAccountType' ));
		list_fMap.add(new fieldMap('NAMED_ACCOUNT_STATUS__C' , 'namedAccountStatus' ));
		list_fMap.add(new fieldMap('industry' , 'industry' ));
		list_fMap.add(new fieldMap('INDUSTRY_Code__C' , 'industryCode' ));
		list_fMap.add(new fieldMap('SUB_INDUSTRY_HW__C' , 'subIndustry' ));
		list_fMap.add(new fieldMap('CUSTOMER_GROUP__C' , 'customerGroup' ));
		list_fMap.add(new fieldMap('CUSTOMER_GROUP_Code__C' , 'customerGroupCode' ));
		list_fMap.add(new fieldMap('STREET__C' , 'street' ));
		list_fMap.add(new fieldMap('PHONE' , 'phone' ));
		list_fMap.add(new fieldMap('FAX' , 'fax' ));
		list_fMap.add(new fieldMap('WEBSITE' , 'webSite' ));
		list_fMap.add(new fieldMap('Postal_Code__c' , 'postalCode' ));
		list_fMap.add(new fieldMap('Owner.Employee_ID__c' , 'accountOwner' )); // relationship
		list_fMap.add(new fieldMap('CREATEDBYID' , 'createdBy' ));
		list_fMap.add(new fieldMap('CREATEDDATE' , 'creationDate' ));
		list_fMap.add(new fieldMap('LASTMODIFIEDBYID' , 'lastModifiedBy' ));
		list_fMap.add(new fieldMap('LASTMODIFIEDDATE' , 'lastModifiedDate' ));
		list_fMap.add(new fieldMap('RECORDTYPEID' , 'recordTypeId' ));
		list_fMap.add(new fieldMap('OwnerId','ownerId'));
		list_fMap.add(new fieldMap('City_Code__c','hwCityId'));
		list_fMap.add(new fieldMap('Province_Code__c','provinceCode'));
		list_fMap.add(new fieldMap('Region_Code__c','regionCode'));
		list_fMap.add(new fieldMap('Representative_Code__c','repOfficeCode'));
		
		list_fMap.add(new fieldMap('Annual_Booking_Target__c' , 'wsAnnualBookingTargetc' ));
		list_fMap.add(new fieldMap('Annual_Revenue_Target__c' , 'wsAnnualRevenueTargetc' ));
		list_fMap.add(new fieldMap('CIS_Name__c' , 'wsCISNamec' ));
		list_fMap.add(new fieldMap('Commercial_Customer_Group__c' , 'wsCommercialCustomerGroupc' ));
		list_fMap.add(new fieldMap('Commercial_Customer_Level__c' , 'wsCommercialCustomerLevelc' ));
		list_fMap.add(new fieldMap('Commercial_Customer_Type__c' , 'wsCommercialCustomerTypec' ));
		list_fMap.add(new fieldMap('Customer_Type__c' , 'wsCustomerTypec' ));
		list_fMap.add(new fieldMap('Is_Parent_Account__c' , 'wsIsParentAccountc' ));
		list_fMap.add(new fieldMap('LastActivityDate' , 'wsLastActivityDate' ));
		//list_fMap.add(new fieldMap('NumberOfOpportunities__c' , 'wsNumberOfOpportunitiesc' ));
		list_fMap.add(new fieldMap('TCG_Name__c' , 'wsTCGNamec' ));
		
		return list_fMap;
	}
	
	//联系人字段Mapping
	public static List<fieldMap> getContactFieldMap(){
		List<fieldMap> list_fMap = new List<fieldMap>();
		list_fMap.add(new fieldMap('ID' , 'sfID' ));
		list_fMap.add(new fieldMap('Global_ID__C' , 'sRowIdx' ));
		list_fMap.add(new fieldMap('LASTNAME' , 'lastName' ));
		list_fMap.add(new fieldMap('FIRSTNAME' , 'firstName' ));
		list_fMap.add(new fieldMap('Salutation' , 'gender' ));
		list_fMap.add(new fieldMap('AccountId' , 'accountId' ));
		list_fMap.add(new fieldMap('Account.Name' , 'accountName' )); // relationship
		list_fMap.add(new fieldMap('TITLE' , 'position' ));
		list_fMap.add(new fieldMap('PHONE' , 'officeTel' ));
		list_fMap.add(new fieldMap('MOBILEPHONE' , 'mobileTel' ));
		list_fMap.add(new fieldMap('OtherPhone' , 'secondMobile' ));
		list_fMap.add(new fieldMap('EMAIL' , 'email' ));
		list_fMap.add(new fieldMap('BIRTHDATE' , 'birthday' ));
		list_fMap.add(new fieldMap('DESCRIPTION' , 'remark' ));
		list_fMap.add(new fieldMap('CREATEDBYID' , 'createdBy' ));
		list_fMap.add(new fieldMap('CreatedDate','creationDate'));
		list_fMap.add(new fieldMap('LastModifiedById' , 'lastModifiedBy' ));
		list_fMap.add(new fieldMap('LASTMODIFIEDDATE' , 'lastModifiedDate' ));
		list_fMap.add(new fieldMap('RECORDTYPEID' , 'recordTypeId' ));
		list_fMap.add(new fieldMap('OwnerId','ownerId'));
		
		list_fMap.add(new fieldMap('Account_Record_Type__c' , 'wsAccountRecordTypec' ));
		list_fMap.add(new fieldMap('Certified_HSCSP__c' , 'wsCertifiedHSCSPc' ));
		list_fMap.add(new fieldMap('Is_new__c' , 'wsIsnewc' ));
		list_fMap.add(new fieldMap('IsDeleted' , 'wsIsDeleted' ));
		list_fMap.add(new fieldMap('Reg__c' , 'wsRegc' ));
		list_fMap.add(new fieldMap('To_be_Deleted__c' , 'wsTobeDeletedc' ));
		list_fMap.add(new fieldMap('Trainer__c' , 'wsTrainerc' ));
		
		return list_fMap; 
	}
	
	public static List<fieldMap> getUserFieldMap(){
		List<fieldMap> list_fMap = new List<fieldMap>();
		list_fMap.add(new fieldMap('Id','id'));
		list_fMap.add(new fieldMap('LastName','lastName'));
		list_fMap.add(new fieldMap('FirstName','firstName'));
		list_fMap.add(new fieldMap('Alias','alias'));
		list_fMap.add(new fieldMap('Email','email'));
		list_fMap.add(new fieldMap('Username','userName'));
		list_fMap.add(new fieldMap('ProfileId','profileID'));
		// list_fMap.add(new fieldMap('Profile.Name','profileName')); // relationship
		list_fMap.add(new fieldMap('UserRoleId','roleID'));
		// list_fMap.add(new fieldMap('UserRole.Name','roleName')); // relationship
		list_fMap.add(new fieldMap('IsActive','isActive'));
		list_fMap.add(new fieldMap('Region_Office__c','region'));
		list_fMap.add(new fieldMap('Country__c','representative'));
		list_fMap.add(new fieldMap('FederationIdentifier','federationID'));
		list_fMap.add(new fieldMap('Employee_ID__c','employeeID'));
		list_fMap.add(new fieldMap('W3ACCOUNT__c','w3Account'));
		list_fMap.add(new fieldMap('User_Group__c','userGroup'));
		list_fMap.add(new fieldMap('Group__c','group'));
		list_fMap.add(new fieldMap('CreatedDate','createdDate'));
		list_fMap.add(new fieldMap('LastModifiedDate','lastModifiedDate'));
		list_fMap.add(new fieldMap('EnglishName__c','englishName'));
		list_fMap.add(new fieldMap('FullName__c','fullName'));
		/*  文档中特殊标记出来
		list_fMap.add(new fieldMap('CreatedById','createdBy'));
		list_fMap.add(new fieldMap('CurrencyIsoCode','currencyIsoCode'));
		list_fMap.add(new fieldMap('Fax','fax'));
		list_fMap.add(new fieldMap('InsertDate','insertDate'));
		list_fMap.add(new fieldMap('LastModifiedById','lastModifiedBy'));
		list_fMap.add(new fieldMap('MobilePhone','mobilePhone'));
		list_fMap.add(new fieldMap('Name','name'));
		list_fMap.add(new fieldMap('Phone','phone'));
		list_fMap.add(new fieldMap('Title','title'));
		list_fMap.add(new fieldMap('UserPreferencesOptOutOfTouch','userPreferencesOptOutOfTouch'));
		list_fMap.add(new fieldMap('UserGroup', 'userGroup'));
		*/
		list_fMap.add(new fieldMap('Account_Approver__c','wsAccountApproverc'));
		//list_fMap.add(new fieldMap('AccountId','wsAccountId')); // Partner用户用
		list_fMap.add(new fieldMap('CallCenterId','wsCallCenterId'));
		list_fMap.add(new fieldMap('Cancelled_Date__c','wsCancelledDatec'));
		list_fMap.add(new fieldMap('City','wsCity'));
		list_fMap.add(new fieldMap('CommunityNickname','wsCommunityNickname'));
		list_fMap.add(new fieldMap('CompanyName','wsCompanyName'));
		//list_fMap.add(new fieldMap('ContactId','wsContactId')); // Partner用户用
		list_fMap.add(new fieldMap('Country','wsCountry'));
		list_fMap.add(new fieldMap('CreatedDate','wsCreatedtime'));
		list_fMap.add(new fieldMap('CSS_TAC__c','wsCSSTACc'));
		list_fMap.add(new fieldMap('DefaultCurrencyIsoCode','wsDefaultCurrencyIsoCode'));
		list_fMap.add(new fieldMap('DelegatedApproverId','wsDelegatedApproverId'));
		list_fMap.add(new fieldMap('Department','wsDepartment'));
		list_fMap.add(new fieldMap('Division','wsDivision'));
		list_fMap.add(new fieldMap('EmailEncodingKey','wsEmailEncodingKey'));
		list_fMap.add(new fieldMap('EmployeeNumber','wsEmployeeNumber'));
		list_fMap.add(new fieldMap('Extension','wsExtension'));
		list_fMap.add(new fieldMap('ForecastEnabled','wsForecastEnabled'));
		list_fMap.add(new fieldMap('HW_Dept__c','wsHWDeptc'));
		list_fMap.add(new fieldMap('HW_Role__c','wsHWRolec'));
		list_fMap.add(new fieldMap('INA_Role__c','wsINARolec'));
		//list_fMap.add(new fieldMap('InsertDate','wsInsertDate')); // Salesforce无此字段
		list_fMap.add(new fieldMap('LanguageLocaleKey','wsLanguageLocaleKey'));
		list_fMap.add(new fieldMap('Last_Active_Date__c','wsLastActiveDatec'));
		list_fMap.add(new fieldMap('LastLoginDate','wsLastLoginDate'));
		list_fMap.add(new fieldMap('LastPasswordChangeDate','wsLastPasswordChangeDate'));
		list_fMap.add(new fieldMap('LocaleSidKey','wsLocaleSidKey'));
		list_fMap.add(new fieldMap('ManagerId','wsManagerId'));
		list_fMap.add(new fieldMap('OfflinePdaTrialExpirationDate','wsOfflinePdaTrialExpirationDate'));
		list_fMap.add(new fieldMap('OfflineTrialExpirationDate','wsOfflineTrialExpirationDate'));
		list_fMap.add(new fieldMap('PostalCode','wsPostalCode'));
		list_fMap.add(new fieldMap('Profile_Name__c','wsProfileNamec'));
		list_fMap.add(new fieldMap('ProfileId','wsProfileId'));
		list_fMap.add(new fieldMap('ReceivesAdminInfoEmails','wsReceivesAdminInfoEmails'));
		list_fMap.add(new fieldMap('ReceivesInfoEmails','wsReceivesInfoEmails'));
		list_fMap.add(new fieldMap('Region_Office__c','wsRegionOfficec'));
		list_fMap.add(new fieldMap('Remark__c','wsRemarkc'));
		list_fMap.add(new fieldMap('Role_Name__c','wsRoleNamec'));
		list_fMap.add(new fieldMap('service_channel_manager__c','wsservicechannelmanagerc'));
		list_fMap.add(new fieldMap('State','wsState'));
		list_fMap.add(new fieldMap('Street','wsStreet'));
		//list_fMap.add(new fieldMap('SystemModstamp','wsSystemModstamp')); // 系统最后修改时间，无必要传递
		list_fMap.add(new fieldMap('TimeZoneSidKey','wsTimeZoneSidKey'));
		list_fMap.add(new fieldMap('LastModifiedDate','wsUpdateDate'));
		list_fMap.add(new fieldMap('User_Group__c','wsUserGroupc'));
		// list_fMap.add(new fieldMap('User_Group','wsUserGroup')); // Salesforce无此字段
		list_fMap.add(new fieldMap('UserPermissionsAvantgoUser','wsUserPermissionsAvantgoUser'));
		list_fMap.add(new fieldMap('UserPermissionsCallCenterAutoLogin','wsUserPermissionsCallCenterAutoLogin'));
		list_fMap.add(new fieldMap('UserPermissionsInteractionUser','wsUserPermissionsInteractionUser'));
		list_fMap.add(new fieldMap('UserPermissionsMarketingUser','wsUserPermissionsMarketingUser'));
		list_fMap.add(new fieldMap('UserPermissionsMobileUser','wsUserPermissionsMobileUser'));
		list_fMap.add(new fieldMap('UserPermissionsOfflineUser','wsUserPermissionsOfflineUser'));
		list_fMap.add(new fieldMap('UserPermissionsSFContentUser','wsUserPermissionsSFContentUser'));
		list_fMap.add(new fieldMap('UserPreferencesActivityRemindersPopup','wsUserPreferencesActivityRemindersPopup'));
		list_fMap.add(new fieldMap('UserPreferencesApexPagesDeveloperMode','wsUserPreferencesApexPagesDeveloperMode'));
		list_fMap.add(new fieldMap('UserPreferencesEventRemindersCheckboxDefault','wsUserPreferencesEventRemindersCheckboxDefault'));
		list_fMap.add(new fieldMap('UserPreferencesHideCSNDesktopTask','wsUserPreferencesHideCSNDesktopTask'));
		list_fMap.add(new fieldMap('UserPreferencesHideCSNGetChatterMobileTask','wsUserPreferencesHideCSNGetChatterMobileTask'));
		//list_fMap.add(new fieldMap('UserPreferencesOptOutOfTouch','wsUserPreferencesOptOutOfTouch')); // Salesforce无此字段
		list_fMap.add(new fieldMap('UserPreferencesReminderSoundOff','wsUserPreferencesReminderSoundOff'));
		list_fMap.add(new fieldMap('UserPreferencesTaskRemindersCheckboxDefault','wsUserPreferencesTaskRemindersCheckboxDefault'));
		list_fMap.add(new fieldMap('UserRoleId','wsUserRoleId'));
		list_fMap.add(new fieldMap('UserType','wsUserType'));
		return list_fMap;
		
	}
	
	// 线索字段Mapping
	public static List<fieldMap> getLeadFieldMap(){
		List<fieldMap> list_fMap = new List<fieldMap>();
		list_fMap.add(new fieldMap('Id','sfID'));
		list_fMap.add(new fieldMap('Global_ID__c','sRowIdx'));
		list_fMap.add(new fieldMap('Campaign__c','campaign'));
		list_fMap.add(new fieldMap('Company','company'));
		list_fMap.add(new fieldMap('CreatedById','createdBy'));
		list_fMap.add(new fieldMap('CREATEDDATE','creationDate'));
		list_fMap.add(new fieldMap('Description','valueProposition'));
		list_fMap.add(new fieldMap('Email','email'));
		list_fMap.add(new fieldMap('Industry','industry'));
		list_fMap.add(new fieldMap('LastModifiedById','lastModifiedBy'));
		list_fMap.add(new fieldMap('LastModifiedDate','lastModifiedDate'));
		list_fMap.add(new fieldMap('Owner.Employee_ID__c','leadOwner')); // relationship
		list_fMap.add(new fieldMap('RecordTypeId','recordTypeId'));
		list_fMap.add(new fieldMap('LeadSource','leadSource'));
		list_fMap.add(new fieldMap('Status','leadStatus'));
		list_fMap.add(new fieldMap('LastName','lastName'));
		list_fMap.add(new fieldMap('FirstName','firstName'));
		list_fMap.add(new fieldMap('Phone','phone'));
		list_fMap.add(new fieldMap('Rating','winProbability'));
		list_fMap.add(new fieldMap('Expected_Project_Date__c','estimatedSignDate'));
		list_fMap.add(new fieldMap('Interested_Product__c','interestedProduct'));
		list_fMap.add(new fieldMap('Representative_Office__c','office'));
		list_fMap.add(new fieldMap('Region_HW__c','region'));
		list_fMap.add(new fieldMap('Country__c','country'));
		list_fMap.add(new fieldMap('Sub_industry__c','subIndustry'));
		list_fMap.add(new fieldMap('China_Representative_Office__c','repOffice'));
		list_fMap.add(new fieldMap('China_Province__c','province'));
		list_fMap.add(new fieldMap('China_City__c','city'));
		list_fMap.add(new fieldMap('China_Lead_Source_Dept__c','leadSourceDept'));
		list_fMap.add(new fieldMap('China_Lead_Activity_Name__c','activityName'));
		list_fMap.add(new fieldMap('China_Lead_Customer_Group__c','leadCustomerGroup'));
		list_fMap.add(new fieldMap('China_Project_Name__c','projectName'));
		list_fMap.add(new fieldMap('China_Participate_Space__c','leadSize'));
		list_fMap.add(new fieldMap('China_Main_Product__c','mainProduct'));
		list_fMap.add(new fieldMap('China_Project_Description__c','projectDescription'));
		list_fMap.add(new fieldMap('Account_Name__c','accountID'));
		//list_fMap.add(new fieldMap('China_Opportunity__c','opportunityID'));
		list_fMap.add(new fieldMap('China_Lead_Invalid_Reason__c','invalidReason'));
		list_fMap.add(new fieldMap('Create_Type__c','createType'));
		list_fMap.add(new fieldMap('Country_Code__c','countryId'));
		
		list_fMap.add(new fieldMap('Lead_Code__c','leadCode'));
		list_fMap.add(new fieldMap('Is_Named_Account__c','isNaAccount'));
		list_fMap.add(new fieldMap('OwnerId','ownerId'));
		
		list_fMap.add(new fieldMap('Follow_Up_Type__c','wsFollowUpTypec'));
		list_fMap.add(new fieldMap('IsDeleted','wsIsDeleted'));
		
		return list_fMap;
	}
	
	//字段Mapping封装
	public class fieldMap{
		public String SFField{get;set;}
		public String EIPField{get;set;}
		public fieldMap(String sField,String eField ){
			SFField = sField;
			EIPField = eField;
		}
	}
}