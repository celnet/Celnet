@isTest
private class WorkOrderFieldPopulaterTest {

   
    /*
    //以下两个字段未使用
    static testMethod void workHourTest(){//7.设置 service request上的work order字段:CSE_Work_Hour_HW__c/CSE_Work_Hour_SW__c
    	Account acc = UtilUnitTest.createAccount();
    	Case sr = UtilUnitTest.createChinaServiceRequest();
    	Work_Order__c wo = UtilUnitTest.createOnSiteWorkOrder(acc,sr);
    	RecordType onsiteSupportType = [select id from RecordType where isActive = true 
    		and name like '%On Site Support Work Order' and SobjectType = 'Work_Order__c' limit 1];
    	wo.RecordTypeId = onsiteSupportType.id;
    	wo.Work_Hour_Hardware_software__c = 1;
    	wo.Work_Hour_Value_added_Standalone__c = 3;
    	update wo;
    	sr = [select id,CSE_Work_Hour_SW__c,CSE_Work_Hour_HW__c from Case where id =:sr.id];
    	System.assertEquals(wo.Work_Hour_Value_added_Standalone__c,sr.CSE_Work_Hour_SW__c);
    	System.assertEquals(wo.Work_Hour_Hardware_software__c,sr.CSE_Work_Hour_HW__c);
    }
    */
    static testMethod void smsMessageTest(){//8.设置 SMS_Notification_Message__c字段
    	Account acc = UtilUnitTest.createAccount();
    	Case sr = UtilUnitTest.createChinaServiceRequest();
    	Work_Order__c wo = UtilUnitTest.createOnSiteWorkOrder(acc,sr);
    	wo = [select id,SMS_Notification_Message__c from Work_Order__c where id=:wo.id];
    	System.assertNotEquals(null,wo.SMS_Notification_Message__c);
    	
    }
    
    static testMethod void assetTest(){//9.设置Asset__c字段
    	Account acc = UtilUnitTest.createAccount();
    	Case sr = UtilUnitTest.createChinaServiceRequest();
    	CSS_Contract__c css = UtilUnitTest.createContract(acc);
    	Installed_Asset__c ia = UtilUnitTest.createAsset(css);
    	ia.Region__c = 'test';
    	update ia;
    	sr.Barcode__c = ia.id;
    	update sr;
    	Work_Order__c wo = UtilUnitTest.createOnSiteWorkOrder(acc,sr);
    	wo.Barcode__c = ia.id;
    	update wo;
    	wo = [select id,Asset__c from Work_Order__c where id=:wo.id];
    	System.assertNotEquals(null,wo.Asset__c);
    }
}