/**
 * @Purpose : 中国区报备关联的机会点的关键字段更新后记录到历史机会点表中供集成抓取
 * @Author : steven
 * @Date : 2014-09-09
 */
public class ChinaDealOpportunityUpdateHistory implements Triggers.Handler{ //after update
	public void handle(){
		//Step1：查询出所有的关键字段发生了变化的机会点ID
		Set<Id> updateOppIds = new Set<Id>();
		for(Opportunity o : (List<Opportunity>)trigger.new){
			if(o.RecordTypeId == CONSTANTS.CHINAOPPORTUNITYRECORDTYPE || 
				 o.recordtypeid == CONSTANTS.HUAWEICHINASERVICEOPPRECORDTYPE || 
				 o.RecordTypeId == CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE){
				Opportunity oldO = (Opportunity)Trigger.oldMap.get(o.id);
				if(o.Estimated_ContractSign_Amount__c != oldO.Estimated_ContractSign_Amount__c ||
					 o.StageName != oldO.StageName || o.CloseDate != oldO.CloseDate) {
					if(o.StageName == 'SS1 (Notifying Opportunity)' || 
						 o.StageName == 'SS2 (Identifying Opportunity)' || 
						 o.StageName == 'SS3 (Validating Opportunity)' ) {
						updateOppIds.add(o.id);
					}
				}
			}
		}
		if(updateOppIds.isEmpty()) return;
		
		//Step2：判断哪些机会点关联了Deal
		List<Deal_Registration__c> dealList = [select id,Related_Opportunity__c from Deal_Registration__c where Related_Opportunity__c in:updateOppIds];
		List<Data_Maintain_History__c> archiveList = new List<Data_Maintain_History__c>();
		Set<Id> dealOppIds = new Set<Id>();
		for(Deal_Registration__c deal : dealList) {
			if(!dealOppIds.contains(deal.Related_Opportunity__c)) {
				Data_Maintain_History__c dmh = new Data_Maintain_History__c();
				dmh.Record_ID__c = deal.Related_Opportunity__c;
				dmh.Operation_Type__c = 'Update';
				dmh.Data_Type__c = 'China Opportunity Team';
				archiveList.add(dmh);
			}
			dealOppIds.add(deal.Related_Opportunity__c);
		}
		
		//Step3：将关联了Deal的机会点ID记录下来供集成使用
		if(archiveList.size() > 0){
			insert archiveList;
		}
	}
}