@isTest
private class AccountPreventDeleteHandlerTest {
    static testMethod void deleteCheck(){//delete
    	Account acc = UtilUnitTest.createHWChinaAccount('acc');
			Account child = UtilUnitTest.createHWChinaAccount('child');
	    child.ParentId = acc.id;
	    update child;
			Opportunity o = new Opportunity(accountid = acc.id,
				     Name = 'test',StageName = 'Solution',CloseDate = Date.today());
				     o.Opportunity_type__c='Direct Sales';//it is added on 2013/05/25 
			insert o;
			
			try{
				delete acc;
			}catch(DmlException e){
			}
			acc = [select id from Account where id = :acc.id];
			System.assertNotEquals(null, acc);
    }
    
    static testMethod void deleteCheck2(){//add ToBeDeleted Flag
			Account acc = UtilUnitTest.createHWChinaAccount('acc');
			Account child = UtilUnitTest.createHWChinaAccount('child');
	    child.ParentId = acc.id;
	    update child;
			Opportunity o = new Opportunity(accountid = acc.id,recordtypeid = CONSTANTS.CHINAOPPORTUNITYRECORDTYPE,
				     Name = 'test',StageName = 'Solution',CloseDate = Date.today());
				     o.Opportunity_type__c='Direct Sales';//it is added on 2013/05/25 
			insert o;
			acc.To_Be_Deleted__c = true;
			try{
				update acc;
			}catch(DmlException e){
			}
			/*
			acc = [select To_Be_Deleted__c from Account where id = :acc.id];
			System.assertEquals(false,acc.To_Be_Deleted__c);
			System.assertEquals(2,ApexPages.getMessages().size());
			acc = UtilUnitTest.createHWChinaAccount('acc2');
			child = UtilUnitTest.createAccount();
	    child.ParentId = acc.id;
	    update child;
			o.accountid = acc.id;
			update o;
			acc.To_Be_Deleted__c = true;
			update acc;
			acc = [select To_Be_Deleted__c from Account where id = :acc.id];

			System.assertEquals(true,acc.To_Be_Deleted__c);
			*/
    }
}