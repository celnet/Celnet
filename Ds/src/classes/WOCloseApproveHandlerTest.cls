/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class WOCloseApproveHandlerTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
         // TO DO: implement unit test
        //User approver = UtilUnitTest.CreateUser('lijianguang', '(CSS)国内服务项目经理(SPM)', '(N) Tianjin Rep RSC', '00208604');
        Account acc = UtilUnitTest.createAccountWithName('Test');
        Case sr =UtilUnitTest.createChinaServiceRequest();                
        Spare_Plan_Approver_Maps__c spmap = new Spare_Plan_Approver_Maps__c();
        spmap.Type__c = 'Work Order';
        spmap.Approver__c='005900000019y4A';//approver.Id;//
        spmap.Rep_Office__c='天津市';   
        spmap.RecordTypeId=[select id from RecordType where DeveloperName ='Spare_Plan_Approver_Map'].id;        
        insert spmap;  
        Work_Order__c wo =UtilUnitTest.createOnSiteWorkOrder(acc, sr);
    }
}