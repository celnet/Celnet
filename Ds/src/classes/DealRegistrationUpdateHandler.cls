/*
* @Purpose : 1、海外报备审批通过/驳回时给总代联系人发送邮件提醒；
*            2、中国区报备关联机会点时根据机会点状态刷新报备状态
* @Author : steven
* @Date :		2013-11-15
*/
public without sharing class DealRegistrationUpdateHandler implements Triggers.Handler{//after update
	public static Boolean isFirstRun = true;
	public void handle() {
		if(!isFirstRun) return;
		for(Deal_Registration__c deal : (List<Deal_Registration__c>)Trigger.new){
			Deal_Registration__c oldDeal = (Deal_Registration__c)Trigger.oldMap.get(deal.id);
			
			//海外报备审批通过/驳回时给总代联系人发送邮件提醒；
			if(Trigger.isAfter && deal.RecordTypeId==CONSTANTS.HUAWEIOVERSEADEALRECORDTYPE && 
			   deal.Deal_Status__c != oldDeal.Deal_Status__c && (deal.Deal_Status__c == 'Registration Failed' || deal.Deal_Status__c == 'Open')) { 
				try{
					//获取当前报备所有总代的邮箱
					List<Deal_Registration_Distributor__c> partnerList = 
						[Select Distributor_Contact_Email_Address__c From Deal_Registration_Distributor__c 
							Where Deal_Registration__c =: deal.Id And Distributor_Contact_Email_Address__c <> null];
					List<String> partnerMails = new List<String>(); 
					for(Deal_Registration_Distributor__c partner : partnerList) {
						partnerMails.add(partner.Distributor_Contact_Email_Address__c);
					}
					//根据报备状态选择邮件模板并发送邮件
					Id templateId = null;
					if(deal.Deal_Status__c == 'Registration Failed') {
						templateId = [select id from EmailTemplate where DeveloperName='Deal_Been_Rejected_Oversea'].id;
					} else {
						templateId = [select id from EmailTemplate where DeveloperName='Deal_Been_Approved_Oversea'].id;
					}
					UtilEmail.sendMailByTemplate(partnerMails,templateId,deal.Id);
					isFirstRun = false;
				}catch(Exception e){
					System.debug(e);
				}
			}
			
		  //中国区报备关联机会点时根据机会点状态刷新报备状态
			if(Trigger.isBefore && deal.RecordTypeId==CONSTANTS.HUAWEICHINADEALRECORDTYPE) { 
				Boolean needUpdate = false;
				//关联了机会点的报备被审批通过
				if(deal.Has_Approved__c && !oldDeal.Has_Approved__c && deal.Related_Opportunity__c != null){
					needUpdate = true;
				}
				//审批通过的报备被关联了机会点
				if(deal.Has_Approved__c && deal.Related_Opportunity__c != null && deal.Related_Opportunity__c != oldDeal.Related_Opportunity__c){
					needUpdate = true;
				}
				if(needUpdate) {
					//查询机会点是什么状态
					Opportunity opp = [select id,stagename from Opportunity where id =: deal.Related_Opportunity__c];
					String stage = opp.stagename;
					if((stage == 'SS4 (Developing Solution)')||(stage == 'SS5 (Gaining Agreement)')||(stage == 'SS6 (Winning)')) {
						deal.Deal_Status__c = 'Open';
					} else if((stage == 'SS7 (Implementing)')||(stage == 'SS8 (Completed)')) {
						deal.Deal_Status__c = 'Closed';
					} else if((stage == 'SSA (Huawei No Bid)')||(stage == 'SSB (Customer Did Not Pursue)')||(stage == 'SSC (Lost to Competition)')){
						deal.Deal_Status__c = 'Cancelled';
					}
				}
			}
			
		}
	}
	
}