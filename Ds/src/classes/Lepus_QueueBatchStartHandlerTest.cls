@isTest
public class Lepus_QueueBatchStartHandlerTest{
    static testmethod void myUnitTest(){
        Lepus_Conflict_Batch_Future__c conflict1 = new Lepus_Conflict_Batch_Future__c();
        conflict1.Name = 'testx';
        conflict1.JobType__c = 'future';
        
        Lepus_Conflict_Batch_Future__c conflict2 = new Lepus_Conflict_Batch_Future__c();
        conflict2.Name = 'testfadsfa';
        conflict2.JobType__c = 'batch';
        
        insert new list<Lepus_Conflict_Batch_Future__c>{conflict1, conflict2};
        
        Lepus_QueueBatchStartHandler h = new Lepus_QueueBatchStartHandler();
        h.handle();
        
    }
}