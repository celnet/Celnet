/**
 * @Purpose : 删除后将记录保存到数据维护历史对象上
 * @Author : Steven
 * @Date : 2014-06-09
 */
public with sharing class OpportunityProductHandler implements Triggers.Handler{
  public static Boolean isFirstRun = true;   
	public void handle(){
		if(OpportunityProductHandler.isFirstRun){
			List<Data_Maintain_History__c> archiveList = new List<Data_Maintain_History__c>();
			for(Project_Product__c t : (List<Project_Product__c>)trigger.old){
				Opportunity opp = [Select Id,RecordTypeId From Opportunity Where Id =: t.Project_Name__c];
				if(opp == null)continue;
				if((opp.RecordTypeId == CONSTANTS.CHINAOPPORTUNITYRECORDTYPE) || (opp.RecordTypeId == CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE)){
					Data_Maintain_History__c dmh = new Data_Maintain_History__c();
					dmh.Record_ID__c = t.Id;
					dmh.Operation_Type__c = 'Delete';
					dmh.Data_Type__c = 'China Opportunity Product';
					archiveList.add(dmh);
				}
				if((opp.RecordTypeId == CONSTANTS.HUAWEIOVERSEAOPPRECORDTYPE) || (opp.RecordTypeId == CONSTANTS.HUAWEIOVERSEADEALOPPRECORDTYPE)){
					Data_Maintain_History__c dmh = new Data_Maintain_History__c();
					dmh.Record_ID__c = t.Id;
					dmh.Operation_Type__c = 'Delete';
					dmh.Data_Type__c = 'Oversea Opportunity Product';
					archiveList.add(dmh);
				}
			}
			if(archiveList.size() > 0){
				insert archiveList;
			}
			OpportunityProductHandler.isFirstRun = false;
		}
	}
}