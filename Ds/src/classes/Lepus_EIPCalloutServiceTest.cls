@isTest
public class Lepus_EIPCalloutServiceTest {
	static testmethod void myUnitTest(){
		// 设置 Custom Settings
		setCustomSettings();
		
		Account acc = createAccount('Acc1');
		insert acc;
		
		Lepus_AccountHandler.isFirstRun = true;
		
		User u = [Select Id, FederationIdentifier From User Where UserType = 'Standard' And IsActive = true limit 1];
		acc.OwnerId = u.id;
		update acc;
		
		list<Opportunity> oppList = new list<Opportunity>();
		
		u.FederationIdentifier = u.FederationIdentifier == null?'test0915':(u.FederationIdentifier + '0915');
		update u;
		
		OpportunityFieldHistory ofh = new OpportunityFieldHistory();
		ofh.OpportunityId = '006O0000002H4va';
		Lepus_EIPCalloutService.FieldUpdateXmlConcatenation(new list<OpportunityFieldHistory>{ofh});
		
		LeadHistory lh = new LeadHistory();
		lh.LeadId = '00QO0000002H4va';
		Lepus_EIPCalloutService.LeadHistoryXmlConcatenation(new list<LeadHistory>{lh});
		
	}
	
	static testmethod void myUnitTest2(){
		Account acc = createAccount('Acc1');
		insert acc;
		
		setCustomSettings();
		
		Opportunity opp = createOpportunity('Opp1', acc.Id);
		insert opp;
		
		Lepus_OpportunityHandler.isFirstRun = true;
		
		User u = [Select Id, FederationIdentifier From User Where UserType = 'Standard' And IsActive = true limit 1];
		
		opp.OwnerId = u.Id;
		update opp;
	}
	
	static testmethod void myUnitTest3(){
		Account acc = createAccount('Acc1');
		insert acc;
		
		setCustomSettings();
		
		Contact con = createContact(acc.Id);
		insert con;
		
		Lepus_ContactHandler.isFirstRun = true;
		
		con.FirstName = 'xxx';
		update con;
	}
	
	public static void setCustomSettings(){
		list<String> syncSettingNames = new list<String>{'机会点业务数据', '机会点团队', '机会点字段更新','线索业务数据','线索团队','线索字段更新','客户业务数据','客户团队','联系人业务数据','用户角色'};
		list<Lepus_Data_Sync_Controller__c> controllers = new list<Lepus_Data_Sync_Controller__c>();
		for(String str : syncSettingNames){
			Lepus_Data_Sync_Controller__c controller = new Lepus_Data_Sync_Controller__c();
			controller.Name = str;
			controller.IsSync__c = true;
			controllers.add(controller);
		}
		insert controllers;
	}
	
	public static Opportunity createOpportunity(String oppName, Id accId){
		Opportunity opp = new Opportunity();
		opp.Name = oppName;
		opp.AccountId = accId;
		opp.OwnerId = UserInfo.getUserId();
		opp.Opportunity_Level_Code__c = 'SubRegion';
		//opp.Opportunity_Level__c = 'Region Level';
		//opp.Opportunity_Type__c = 'Carrier Resales';
		opp.Opportunity_Type_Code__c = 'SM002';
		opp.China_Competitor_Trends__c = 'xxx';
		opp.Project_Risk__c = 'xxx';
		opp.China_Following_Strategy__c = 'xxx';
		opp.Master_Channer_Partner__c = 'xxx';
		opp.StageName = 'SS7 (Implementing)';
		opp.CloseDate = Date.today();
		opp.RecordTypeId = CONSTANTS.HUAWEIOVERSEAOPPRECORDTYPE;
		
		return opp;
	}
	
	public static Account createAccount(String accName){
		Account acc = new Account();
		acc.Name = accName;
		acc.RecordTypeId = CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE;
		acc.City_Huawei_China__c = '北京市';
		acc.OwnerId = UserInfo.getUserId();
		
		return acc;
	}
	
	public static Contact createContact(Id accId){
		Contact con = new Contact();
		con.RecordTypeId = CONSTANTS.HUAWEICHINACONTACTRECORDTYPE;
		con.AccountId = accId;
		con.FirstName = 'Chung';
		con.LastName = 'cola';
		
		return con;
	}
	
	public static Lead createLead(String leadName){
		Lead le = new Lead();
		le.RecordTypeId = CONSTANTS.CHINALEADRECORDTYPE;
		
		
		return le;
	}
}