/**
 * 
 */
@isTest
private class Lepus_ValidationDataBatchTest {

    static testMethod void SyncAccount() {
        //初始化SyncFailRecord__c
        List<SyncFailRecord__c> list_sfr = new List<SyncFailRecord__c>();
        SyncFailRecord__c sfr1 = new SyncFailRecord__c();
        sfr1.SyncType__c = '对账数据同步';
        sfr1.ObjectType__c = 'Account';
        sfr1.SyncTime__c = datetime.now().addDays(-1);
        list_sfr.add(sfr1);
        SyncFailRecord__c sfr2 = new SyncFailRecord__c();
        sfr2.SyncType__c = '对账数据同步';
        sfr2.ObjectType__c = 'Opportunity';
        sfr2.SyncTime__c = datetime.now().addDays(-1);
        list_sfr.add(sfr2);
        SyncFailRecord__c sfr3 = new SyncFailRecord__c();
        sfr3.SyncType__c = '对账数据同步';
        sfr3.ObjectType__c = 'Contact';
        sfr3.SyncTime__c = datetime.now().addDays(-1);
        list_sfr.add(sfr3);
        SyncFailRecord__c sfr4 = new SyncFailRecord__c();
        sfr4.SyncType__c = '对账数据同步';
        sfr4.ObjectType__c = 'Lead';
        sfr4.SyncTime__c = datetime.now().addDays(-1);
        list_sfr.add(sfr4);
        insert list_sfr;
        //初始化Custom setting
        Lepus_Data_Sync_Controller__c dsc = new Lepus_Data_Sync_Controller__c();
        dsc.Name = '客户业务数据';
        dsc.IsSync__c = false;
        insert dsc;
        Lepus_Data_Sync_Controller__c dsc2 = new Lepus_Data_Sync_Controller__c();
        dsc2.Name = '客户团队';
        dsc2.IsSync__c = false;
        insert dsc2;
        Lepus_Data_Sync_Controller__c dsc3 = new Lepus_Data_Sync_Controller__c();
        dsc3.Name = '对账数据';
        dsc3.IsSync__c = true;
        insert dsc3;
        //初始化客户
        Account acc = UtilUnitTest.createHWChinaAccount('testacc87');
        
        
        
        system.Test.startTest();
        Lepus_ValidationDataBatch vdb = new Lepus_ValidationDataBatch(Account.sobjecttype);
        //Lepus_EIPDataBatch edb = new Lepus_EIPDataBatch(new List<ID>{acc.Id},'Insert',Account.sobjecttype);
        Database.executeBatch(vdb, 1);
        system.test.stopTest();
    }
    static testMethod void SyncOpportunity() {
        //初始化SyncFailRecord__c
        List<SyncFailRecord__c> list_sfr = new List<SyncFailRecord__c>();
        SyncFailRecord__c sfr1 = new SyncFailRecord__c();
        sfr1.SyncType__c = '对账数据同步';
        sfr1.ObjectType__c = 'Account';
        sfr1.SyncTime__c = datetime.now().addDays(-1);
        list_sfr.add(sfr1);
        SyncFailRecord__c sfr2 = new SyncFailRecord__c();
        sfr2.SyncType__c = '对账数据同步';
        sfr2.ObjectType__c = 'Opportunity';
        sfr2.SyncTime__c = datetime.now().addDays(-1);
        list_sfr.add(sfr2);
        SyncFailRecord__c sfr3 = new SyncFailRecord__c();
        sfr3.SyncType__c = '对账数据同步';
        sfr3.ObjectType__c = 'Contact';
        sfr3.SyncTime__c = datetime.now().addDays(-1);
        list_sfr.add(sfr3);
        SyncFailRecord__c sfr4 = new SyncFailRecord__c();
        sfr4.SyncType__c = '对账数据同步';
        sfr4.ObjectType__c = 'Lead';
        sfr4.SyncTime__c = datetime.now().addDays(-1);
        list_sfr.add(sfr4);
        insert list_sfr;
        //初始化Custom setting
        List<Lepus_Data_Sync_Controller__c> ldsc = new List<Lepus_Data_Sync_Controller__c>();
        Lepus_Data_Sync_Controller__c dsc = new Lepus_Data_Sync_Controller__c();
        dsc.Name = '机会点业务数据';
        dsc.IsSync__c = false;
        Lepus_Data_Sync_Controller__c dsc2 = new Lepus_Data_Sync_Controller__c();
        dsc2.Name = '机会点团队';
        dsc2.IsSync__c = false;
        Lepus_Data_Sync_Controller__c dsc3 = new Lepus_Data_Sync_Controller__c();
        dsc3.Name = '对账数据';
        dsc3.IsSync__c = true;
        Lepus_Data_Sync_Controller__c dsc4 = new Lepus_Data_Sync_Controller__c();
        dsc4.Name = '机会点字段更新';
        dsc4.IsSync__c = false;
        Lepus_Data_Sync_Controller__c dsc5 = new Lepus_Data_Sync_Controller__c();
        dsc5.Name = '客户业务数据';
        dsc5.IsSync__c = false;
        Lepus_Data_Sync_Controller__c dsc6 = new Lepus_Data_Sync_Controller__c();
        dsc6.Name = '客户团队';
        dsc6.IsSync__c = false;
        ldsc.add(dsc);
        ldsc.add(dsc2);
        ldsc.add(dsc3);
        ldsc.add(dsc4);
        ldsc.add(dsc5);
        ldsc.add(dsc6);
        insert ldsc;
        //初始化机会点
        Account acc = UtilUnitTest.createHWChinaAccount('testacc87');
        UtilUnitTest.createOpportunity(acc);
        
        
        system.Test.startTest();
        Lepus_ValidationDataBatch vdb = new Lepus_ValidationDataBatch(Opportunity.sobjecttype);
        //Lepus_EIPDataBatch edb = new Lepus_EIPDataBatch(new List<ID>{acc.Id},'Insert',Account.sobjecttype);
        Database.executeBatch(vdb, 1);
        system.test.stopTest();
    }
    static testMethod void SyncContact() {
        //初始化SyncFailRecord__c
        List<SyncFailRecord__c> list_sfr = new List<SyncFailRecord__c>();
        SyncFailRecord__c sfr1 = new SyncFailRecord__c();
        sfr1.SyncType__c = '对账数据同步';
        sfr1.ObjectType__c = 'Account';
        sfr1.SyncTime__c = datetime.now().addDays(-1);
        list_sfr.add(sfr1);
        SyncFailRecord__c sfr2 = new SyncFailRecord__c();
        sfr2.SyncType__c = '对账数据同步';
        sfr2.ObjectType__c = 'Opportunity';
        sfr2.SyncTime__c = datetime.now().addDays(-1);
        list_sfr.add(sfr2);
        SyncFailRecord__c sfr3 = new SyncFailRecord__c();
        sfr3.SyncType__c = '对账数据同步';
        sfr3.ObjectType__c = 'Contact';
        sfr3.SyncTime__c = datetime.now().addDays(-1);
        list_sfr.add(sfr3);
        SyncFailRecord__c sfr4 = new SyncFailRecord__c();
        sfr4.SyncType__c = '对账数据同步';
        sfr4.ObjectType__c = 'Lead';
        sfr4.SyncTime__c = datetime.now().addDays(-1);
        list_sfr.add(sfr4);
        insert list_sfr;
        //初始化Custom setting
        List<Lepus_Data_Sync_Controller__c> ldsc = new List<Lepus_Data_Sync_Controller__c>();
        Lepus_Data_Sync_Controller__c dsc = new Lepus_Data_Sync_Controller__c();
        dsc.Name = '机会点业务数据';
        dsc.IsSync__c = false;
        Lepus_Data_Sync_Controller__c dsc2 = new Lepus_Data_Sync_Controller__c();
        dsc2.Name = '机会点团队';
        dsc2.IsSync__c = false;
        Lepus_Data_Sync_Controller__c dsc3 = new Lepus_Data_Sync_Controller__c();
        dsc3.Name = '对账数据';
        dsc3.IsSync__c = true;
        Lepus_Data_Sync_Controller__c dsc4 = new Lepus_Data_Sync_Controller__c();
        dsc4.Name = '机会点字段更新';
        dsc4.IsSync__c = false;
        Lepus_Data_Sync_Controller__c dsc5 = new Lepus_Data_Sync_Controller__c();
        dsc5.Name = '客户业务数据';
        dsc5.IsSync__c = false;
        Lepus_Data_Sync_Controller__c dsc6 = new Lepus_Data_Sync_Controller__c();
        dsc6.Name = '客户团队';
        dsc6.IsSync__c = false;
        Lepus_Data_Sync_Controller__c dsc7 = new Lepus_Data_Sync_Controller__c();
        dsc7.Name = '联系人业务数据';
        dsc7.IsSync__c = false;
        ldsc.add(dsc);
        ldsc.add(dsc2);
        ldsc.add(dsc3);
        ldsc.add(dsc4);
        ldsc.add(dsc5);
        ldsc.add(dsc6);
        ldsc.add(dsc7);
        insert ldsc;
        //初始化联系人
        Account acc = UtilUnitTest.createHWChinaAccount('testacc87');
        UtilUnitTest.createContact(acc);
        
        
        system.Test.startTest();
        Lepus_ValidationDataBatch vdb = new Lepus_ValidationDataBatch(Contact.sobjecttype);
        //Lepus_EIPDataBatch edb = new Lepus_EIPDataBatch(new List<ID>{acc.Id},'Insert',Account.sobjecttype);
        Database.executeBatch(vdb, 1);
        system.test.stopTest();
    }
    static testMethod void SyncLead() {
        //初始化SyncFailRecord__c
        List<SyncFailRecord__c> list_sfr = new List<SyncFailRecord__c>();
        SyncFailRecord__c sfr1 = new SyncFailRecord__c();
        sfr1.SyncType__c = '对账数据同步';
        sfr1.ObjectType__c = 'Account';
        sfr1.SyncTime__c = datetime.now().addDays(-1);
        list_sfr.add(sfr1);
        SyncFailRecord__c sfr2 = new SyncFailRecord__c();
        sfr2.SyncType__c = '对账数据同步';
        sfr2.ObjectType__c = 'Opportunity';
        sfr2.SyncTime__c = datetime.now().addDays(-1);
        list_sfr.add(sfr2);
        SyncFailRecord__c sfr3 = new SyncFailRecord__c();
        sfr3.SyncType__c = '对账数据同步';
        sfr3.ObjectType__c = 'Contact';
        sfr3.SyncTime__c = datetime.now().addDays(-1);
        list_sfr.add(sfr3);
        SyncFailRecord__c sfr4 = new SyncFailRecord__c();
        sfr4.SyncType__c = '对账数据同步';
        sfr4.ObjectType__c = 'Lead';
        sfr4.SyncTime__c = datetime.now().addDays(-1);
        list_sfr.add(sfr4);
        insert list_sfr;
        //初始化Custom setting
        List<Lepus_Data_Sync_Controller__c> ldsc = new List<Lepus_Data_Sync_Controller__c>();
        Lepus_Data_Sync_Controller__c dsc = new Lepus_Data_Sync_Controller__c();
        dsc.Name = '机会点业务数据';
        dsc.IsSync__c = false;
        Lepus_Data_Sync_Controller__c dsc2 = new Lepus_Data_Sync_Controller__c();
        dsc2.Name = '机会点团队';
        dsc2.IsSync__c = false;
        Lepus_Data_Sync_Controller__c dsc3 = new Lepus_Data_Sync_Controller__c();
        dsc3.Name = '对账数据';
        dsc3.IsSync__c = true;
        Lepus_Data_Sync_Controller__c dsc4 = new Lepus_Data_Sync_Controller__c();
        dsc4.Name = '机会点字段更新';
        dsc4.IsSync__c = false;
        Lepus_Data_Sync_Controller__c dsc5 = new Lepus_Data_Sync_Controller__c();
        dsc5.Name = '客户业务数据';
        dsc5.IsSync__c = false;
        Lepus_Data_Sync_Controller__c dsc6 = new Lepus_Data_Sync_Controller__c();
        dsc6.Name = '客户团队';
        dsc6.IsSync__c = false;
        Lepus_Data_Sync_Controller__c dsc7 = new Lepus_Data_Sync_Controller__c();
        dsc7.Name = '联系人业务数据';
        dsc7.IsSync__c = false;
        Lepus_Data_Sync_Controller__c dsc8 = new Lepus_Data_Sync_Controller__c();
        dsc8.Name = '线索业务数据';
        dsc8.IsSync__c = false;
        Lepus_Data_Sync_Controller__c dsc9 = new Lepus_Data_Sync_Controller__c();
        dsc9.Name = '线索团队';
        dsc9.IsSync__c = false;
        ldsc.add(dsc);
        ldsc.add(dsc2);
        ldsc.add(dsc3);
        ldsc.add(dsc4);
        ldsc.add(dsc5);
        ldsc.add(dsc6);
        ldsc.add(dsc7);
        ldsc.add(dsc8);
        ldsc.add(dsc9);
        insert ldsc;
        //初始化线索
        Account acc = UtilUnitTest.createHWChinaAccount('testacc87');
        insert new Lead(Account_Name__c = acc.id,NA_Lead_Name__c = 'nalead',
            NA_Lead_Confirmation__c='未确认',lastname = 'lead',company = 'company');
        
        system.Test.startTest();
        Lepus_ValidationDataBatch vdb = new Lepus_ValidationDataBatch(Contact.sobjecttype);
        //Lepus_EIPDataBatch edb = new Lepus_EIPDataBatch(new List<ID>{acc.Id},'Insert',Account.sobjecttype);
        Database.executeBatch(vdb, 1);
        system.test.stopTest();
    }
}