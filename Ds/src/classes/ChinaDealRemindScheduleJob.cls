/**
 * @Purpose : 1、报备审批通过后，要求渠道每两周更新一次进展，到期前三天系统给渠道发送邮件提醒；
 *            2、两周的计时周期从报备更新时点起重新计算，报备超过两周未更新，置为失效Invalid；
 *            注：以下报备字段有变化时才认为报备有刷新：项目名称、项目描述、预计签单金额（万元）、预计签单时间、
 *            最终客户名称、最终客户联系人、最终客户电话、最终客户邮箱、最终客户地址、报备的产品（关联列表）、竞争对手（关联列表）
 * @Author : steven
 * @Date : 2014-09-04
 */
global class ChinaDealRemindScheduleJob implements Schedulable,Database.Batchable<String>{
	public Map<String,List<Deal_Registration__c>> remindDealMap {get;set;} //用于分组保存每个联系人的待提醒报备清单
	public List<Deal_Registration__c> dealInvalidList {get;set;} //用于记录需要进行失效处理的报备

	public ChinaDealRemindScheduleJob() {
		this.remindDealMap = new Map<String,List<Deal_Registration__c>>();
		this.dealInvalidList = new List<Deal_Registration__c>();
	}   
    
	//ScheduleJob Execute
	global void execute(SchedulableContext SC){
		ChinaDealRemindScheduleJob remindJob = new ChinaDealRemindScheduleJob();
		remindJob.initNeedRemindDealMap();
		Database.executeBatch(remindJob,10);
	}
    
	//Batch Start
	public Iterable<String> start(Database.BatchableContext BC){
		List<String> emailList = new List<String>();
		for(String email : this.remindDealMap.keySet()) {
			emailList.add(email);
		}
		return emailList;
	}
	//Batch Execute
	public void execute(Database.BatchableContext BC, list<String> scope){
		try {
			for(String email : scope){
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
				mail.setToAddresses(new List<string>{email});
				mail.setSenderDisplayName('Salesforce Supporter');
				mail.setSubject('【提醒】项目报备即将失效，请及时更新');
				String strbody = htmlBodyConstructor(email); 
				mail.setHtmlBody(strbody);
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
			}
		} catch(Exception e) {
			System.debug(e);
		}
	}
	//Batch Finished
	public void finish(Database.BatchableContext BC){
	}
    
	/**
	 * 获取需要提醒的报备清单，并按提醒人进行分组
	 */
	public void initNeedRemindDealMap() {
		//Step1: 查询出所有审批通过的中国区报备数据
		List<Deal_Registration__c> dealList = 
			[select id,Name,Expiring_Date__c,Deal_Status__c,login_contact__r.Email from Deal_Registration__c 
				where Deal_Status__c = 'Open' and RecordTypeId =: CONSTANTS.HUAWEICHINADEALRECORDTYPE]; 
		//Step2: 分别记录到期的报备 和 还有3天就要到期的报备
		for(Deal_Registration__c deal : dealList) {
			//Step2-1: 已到期的报备需要进行失效处理
			if(deal.Expiring_Date__c <= system.today()) {
				deal.Deal_Status__c = 'Invalid';
				this.dealInvalidList.add(deal);
			}
			//Step2-2: 即将到期的报备分组进行邮件提醒
			if(deal.Expiring_Date__c == (system.today()+3)) {
				String email = deal.login_contact__r.Email;
				if(email == null || email.trim() == '') continue; 
				if(remindDealMap.get(email) == null) {
					//Step2-2-1: 如果MAP表中不存在该用户，插入该用户，并添加报备
		      List<Deal_Registration__c> dList = new List<Deal_Registration__c>();
		      dList.add(deal);
		      this.remindDealMap.put(email,dList);
				} else {
					//Step2-2-2: 如果MAP表中已存在该用户，直接添加报备
		      List<Deal_Registration__c> dList = this.remindDealMap.get(email);
		      dList.add(deal);
				}
			}
		}
		//Step3: 失效所有已到期的报备
		if(this.dealInvalidList.size()>0) {
			try {
				update this.dealInvalidList;
			} catch(Exception e) {
				system.debug(e.getMessage());
			}
		}
	}

	/**
	 * 构造邮件主体 (注意String长度有限制，一般情况下不会超出)
	 */ 
	private String htmlBodyConstructor(String email) {
	String result = '';
	String dealListString = '';
	String baseUrl = system.Url.getSalesforceBaseUrl().toExternalForm()+'/';
		//Step1：获取当前老板相关的报备全集
		List<Deal_Registration__c> dList = remindDealMap.get(email);
		//Step2：将需要提醒的报备转换成Html
		for(Deal_Registration__c deal : dList) {
			dealListString += getHtmlDeal(deal);
		}
		//Step3：构造提醒邮件主体
		result  = '<p>尊敬的先生/女士，您好</p>';
		result += '<p>&nbsp;&nbsp;为了维护您的相关权益，请尽快更新以下项目的相关信息，否则项目报备将在三天后失效，谢谢。</p>';
		result += '<table style="font-family: Arial,微软雅黑;font-size:14px; border:1px solid #AAA;width:800px;background: #EEE;border-spacing: 1px;text-align:center">';
		result +=   '<tbody>';
		result +=     '<tr style="background: #CCC;font-weight: bold;height:20px;">';
		result +=       '<th style="width:45%">项目报备名称</th>';
		result +=       '<th style="width:25%">项目报备状态</th>';
		result +=       '<th style="width:30%">项目报备失效日期</th>';
		result +=     '</tr>';
		result +=     dealListString;
		result +=   '</tbody>';
		result += '</table>';
		result += '<br><p>&nbsp;&nbsp;华为企业业务中国区</p>';
		result += '<p>&nbsp;&nbsp;该邮件为系统自动发送，请勿回复。</p>';
		return result;
	}

	/** 
	 * 构造Html格式的报备String
	 */ 
	private String getHtmlDeal(Deal_Registration__c deal) {
		String result = '';
		if(deal != null) {
			String expiringDate = String.valueOf(deal.Expiring_Date__c);
			result += '<tr style="background: #FFF;">';
			result +=   '<td>'+ deal.Name +'</td>';
			result +=   '<td>审批通过</td>';
			result +=   '<td>'+ expiringDate.substring(0,10) +'</td>';
			result += '</tr>';
		}
		return result;
	}
    
}