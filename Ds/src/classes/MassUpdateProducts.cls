/**************************************************************************************************
 * Name             : MassUpdateProducts 
 * Object           : Project_Product__c
 * Requirement      : Mass update project product
 * Purpose          : 
 * Author           : Iverson gao
 * Create Date      : 06/11/2012
 * Modify History   : 
***************************************************************************************************/
public class MassUpdateProducts{
    List<Project_Product__c> products= null;
    Opportunity opp= null;
    Id oid= null;   
      
    //ApexPages.StandardSetController setCon;   
    //Constructor of This class
    public MassUpdateProducts(ApexPages.StandardSetController controller) {
          //setCon = controller;     
          oid= ApexPages.currentPage().getParameters().get('id');    
          System.debug('@@@-->'+oid);             
          products = (List<Project_Product__c>)[Select Total_Price__c,Quantity__c,Product_Name__c,Project_Name__c,Sales_Price__c 
                                                ,Id,CurrencyIsoCode From Project_Product__c where Project_Name__c =:oid];    
          System.debug('@###-->'+products.size());      
          //opp = [Select Id,Name From Opportunity Where Id =: oid];                         
    }     
    //click save button do update,return currentpage.
    public PageReference savePP() {
          System.debug('@###-->'+products.size());
          //update selected;
          //return (new ApexPages.StandardController(opp)).view();
          return null;
    }  
    public static testmethod void Test_MassUpdateProducts(){
          MassUpdateProducts mass = new MassupdateProducts(null);
          mass.savePP();
    }
}