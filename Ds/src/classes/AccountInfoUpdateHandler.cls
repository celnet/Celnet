/*
 * 当客户的行业和归属信息更改被审批通过后，trigger刷新相应属性
 * @Author : steven 
 * @Date : 20130820
 */
public without sharing class AccountInfoUpdateHandler implements Triggers.Handler{ //after update
	public void handle() {
		Boolean shouldSendExceptionMail = false;
		Boolean newAccIsNA = false;
		String errorMsg = '';
		for(Account_Info_Update_Request__c r : (List<Account_Info_Update_Request__c>)Trigger.new){
			if(r.IsApproved__c && !((Account_Info_Update_Request__c)Trigger.oldMap.get(r.id)).IsApproved__c){ //after approved
				Account acc = new Account(id = r.account__c);
				
				List<Account> newAccs = [select id,Is_Named_Account__c from Account where id = :r.account__c];
				for(Account newAcc : newAccs){
					newAccIsNA = newAcc.Is_Named_Account__c;
				}
				
				//Logic:NA can not be update
				if(newAccIsNA) {
					if(!r.Is_Named_Account__c) return; //not NA when submitted
				}
				acc.Name = r.Account_Name__c; //update by steven 20131223
				acc.Representative_Office__c = r.Representative_Office__c;
				acc.Province__c = r.Province__c;
				acc.City_Huawei_China__c = r.City__c;
				acc.Industry = r.Industry__c;
				acc.Sub_industry_HW__c = r.Sub_Industry__c;
				acc.Customer_Group__c = r.Customer_Group__c;
				acc.Customer_Group_Code__c = r.Customer_Group_Code__c;
				//Logic:update non NA Account 
				try{
					update acc;
				}catch(Exception e){
					//if has error, send mail to the owner
					user recordOwner = [select id,Email from User where id = :r.OwnerId];
					String mailContent = e.getMessage();
					List<Account> accs = [select id,name,Is_Named_Account__c from Account where id = :r.account__c];
					Account updateAcc = null;
					for(Account a : accs){
						updateAcc = a;
					}
					mailContent += '<br/>客户为: <a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + updateAcc.id + '">' + updateAcc.name + '</a>';
					
					EmailSender.sendHTMLMail('更新客户信息失败',mailContent ,
							new String[] {Userinfo.getUserEmail(),recordOwner.email});
				} //end try 
			} //end approved
		}
	}
}