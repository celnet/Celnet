/*
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-6-24
 * Description: Test CampaignFindApproverOversea
 */
@isTest
private class CampaignFindapproverOverseaTest {
    static testMethod void test() {
    	User u = [Select Id,Region_Office__c From User Where Id =: UserInfo.getUserId()];
        Account_Link_Approver_Map__c am = new Account_Link_Approver_Map__c();
        am.Region__c = u.Region_Office__c;
        am.RecordTypeId = [select id from RecordType where DeveloperName = 'HW_Campaign_Approver'].id;
        am.Approver__c = u.Id;
        insert am;
        
        Campaign cam = UtilUnitTest.createCampaign();
        cam.RecordTypeId = CONSTANTS.OVERSEACAMPAIGNRECORDTYPE;
        cam.Status = 'Request for Approval';
        update cam;
        
        Campaign cam2 = [Select Id, Regional_Approver__c From Campaign];
        
        System.assertEquals(cam2.Regional_Approver__c, UserInfo.getUserId());
    }
}