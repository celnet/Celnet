/*
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-8-27
 * Description: 
 */
public class Lepus_ContactHandler implements Triggers.Handler{
	public static boolean isFirstRun = true;
    public void handle(){
    	
    	// 触发记录日志
		List<Lepus_Log__c> logList = new List<Lepus_Log__c>();
		List<Contact> triggerconList = Lepus_HandlerUtil.retrieveRecordList();
		for(Contact con : triggerconList){
			if(Lepus_HandlerUtil.filterRecordType(con, Contact.sobjecttype)){
				Lepus_Log__c log = new Lepus_Log__c();
				log.RecordId__c = con.Id;
				log.LastModifiedDate__c = con.LastModifiedDate;
				log.UniqueId__c = con.Id + con.LastModifiedDate.formatGMT('yyyy-MM-dd HH:mm:ss');
				log.Updated__c = true;
				logList.add(log);
			}
		}
		
		if(logList.size() > 0)
		upsert logList UniqueId__c;
    	
        if((Lepus_Data_Sync_Controller__c.getInstance('联系人业务数据') != null) && Lepus_Data_Sync_Controller__c.getInstance('联系人业务数据').IsSync__c){
        	List<Id> contactIdList = new List<Id>();
			String action = Lepus_HandlerUtil.retrieveAction();
			List<Contact> contactList = Lepus_HandlerUtil.retrieveRecordList();
			Map<ID , Contact> map_Contact = new Map<ID,Contact>();
			
			for(Contact con : contactList){
				if(Lepus_HandlerUtil.filterRecordType(con, Contact.sobjecttype)){
					
					contactIdList.add(con.Id);
					map_Contact.put(con.Id , con);
				}
			}
			
			if(contactIdList.size() == 1){
				Lepus_FutureCallout.syncData(contactIdList[0], action, 'contact');
			} else if(contactIdList.size() > 1){
				Lepus_SyncUtil.initQueue(map_Contact, contactIdList, action, '业务数据同步', datetime.now());
			}
        }
    }
}