public without sharing class WorkOrderAddProductHandler  implements Triggers.Handler{
	public void handle(){
		List<Id> sfpIdList=new List<Id>();
		Map<Id,Id> woIdSFPMap=new Map<Id,Id>();//woid,sfpid //每个WO对应的SFP Id
		List<SFP_Product__c> onsiteSfpproduct =new List<SFP_Product__c>();//现场工单的SFP产品
		for(Work_Order__c wo:(List<Work_Order__c>)Trigger.new){
			if(wo.SFP_No__c!=null && !wo.RecordTypeDeveloperName__c.contains('N_On_Site')){
				woIdSFPMap.put(wo.id,wo.SFP_No__c);
				sfpIdList.add(wo.SFP_No__c);
			}
			//现场工单，只取其中一条产品
			if(wo.SFP_No__c!=null && wo.RecordTypeDeveloperName__c.contains('N_On_Site')){
				onsiteSfpproduct=[Select s.SFP__c
					, s.Contract_Product_PO__r.Id, s.Contract_Product_PO__c 
					, s.Contract_Product_PO__r.Quantity__c, s.Quantity__c
					From SFP_Product__c s
					where s.SFP__c = : wo.SFP_No__c];					
			}
			if(onsiteSfpproduct.size()>0){
				Work_Order_Product__c onsiteProduct =new Work_Order_Product__c(
							 Contract_Product_PO__c=onsiteSfpproduct[0].Contract_Product_PO__c
							,Work_Order__c=wo.id
							,Quantity__c=onsiteSfpproduct[0].Quantity__c
					);
					insert onsiteProduct;
			}				
		}
		if(sfpIdList.size() > 0){
			List<SFP_Product__c> sfppductList=[Select s.SFP__c
				, s.Contract_Product_PO__r.Id, s.Contract_Product_PO__c 
				, s.Contract_Product_PO__r.Quantity__c, s.Quantity__c
				From SFP_Product__c s
				where s.SFP__c in :sfpIdList]; //WO对应的所有的SFP_Product__c
			List<Work_Order_Product__c> wopList=new List<Work_Order_Product__c>();			
			for(Work_Order__c wo:(List<Work_Order__c>)Trigger.new){
				for(SFP_Product__c sfpp:sfppductList){
					if(sfpp.SFP__c==wo.SFP_No__c){
						wopList.add(
							new Work_Order_Product__c(
								 Contract_Product_PO__c=sfpp.Contract_Product_PO__c
								,Work_Order__c=wo.id
								,Quantity__c=sfpp.Quantity__c
						));
					}
				}					
			}
			insert wopList;
			
		}
	}
}