/**
 * @Purpose : 
 * @Author : Steven
 * @Date : 2014-07-21
 */
public class ChinaSuperNAFunction {
	public China_Strategy_NA__c superNA {get; set;}

	public ChinaSuperNAFunction(ApexPages.StandardController controller) { 
		this.superNA = [select id,NA_Child__c from China_Strategy_NA__c where id=:controller.getId()];
	}
	
	public PageReference save(){
		try{
			Account updateAcc = new Account(id=superNA.NA_Child__c,China_Strategy_NA__c=superNA.Id);
			update(updateAcc); 
			return new ApexPages.StandardController(superNA).view();
		}catch(Exception e){
			return null;
		}
	}

}