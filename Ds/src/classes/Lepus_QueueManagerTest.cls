@isTest
private class Lepus_QueueManagerTest {
	static testmethod void myUnitTest(){
		Account acc = Lepus_EIPCalloutServiceTest.createAccount('dfad');
		insert acc;
		
		Opportunity opp = Lepus_EIPCalloutServicetest.createOpportunity('dasfa', acc.Id);
		insert opp;
		
		Contact con = Lepus_EIPCalloutServiceTest.createContact(acc.Id);
		insert con;
		
		Lead l = new Lead();
		l.LastName = 'dfa';
		l.Company = 'daf';
		insert l;
		
		Lepus_QueueManager manager = new Lepus_QueueManager();
		Lepus_Queue__c qu1 = manager.initQueue('dfadf', acc.Id, 'insert', '业务数据同步', datetime.now());
		Lepus_Queue__c qu2 = manager.initQueue('dfassa', opp.Id, 'insert', '业务数据同步', datetime.now());
		Lepus_Queue__c qu3 = manager.initQueue('dfassa', con.Id, 'insert', '业务数据同步', datetime.now());
		Lepus_Queue__c qu4 = manager.initQueue('dfassa', l.Id, 'insert', '业务数据同步', datetime.now());
		
		
		manager.EnQueue(new List<Lepus_Queue__c>{qu1, qu2, qu3, qu4});
		
		
		Lepus_QueueManager.HaveUntreated();
		Lepus_QueueManager.OutQueue(new List<Id>{qu1.Id, qu2.Id, qu3.Id, qu4.Id});
	}
}