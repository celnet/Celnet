/*
* @Purpose : 
* 针对中国区记录:
* 当Satisfactory Level首次被更新不为空时	
*										更新状态为Closed
*										更新回访时间=now()	
*	当“未成功回访原因”更新时	
*										更新回访时间=now()	
*
*
* @Author : Brain Zhang
* @Date :       2013-8
*
* @Revision: 2013-11
*
*  细化回访类型,逻辑不变
* (HF)Remote Survey Answer3(china)远程回访满意度
* (HF)Onsite Survey Answer2(china)现场服务回访满意度
* (HF)Spare Survey Answer2(china)备件回访满意度   
* (HF)Remote Survey Time远程回访时间
* (HF)Onsite Survey Time现场回访时间
* (HF)Spare Survey Time	备件回访时间
*
*/
public without sharing class ServiceRequestSurveyHandler implements Triggers.Handler{
	public static boolean shouldRun = true;
	public void handle(){//beforeupdate
		if(!CommonConstant.serviceRequestTriggerShouldRun){
			return ;
		}
		if(!shouldRun){
			return;//执行前进行判断,避免循环调用
		}
		for (Case c : (List<Case>)trigger.new){//before update
			if(c.Record_Type_Name__c.startsWith('China') ){  
				if(c.HF_Remote_Survey_Answer3_china__c != null &&
					((Case)trigger.oldmap.get(c.id)).HF_Remote_Survey_Answer3_china__c ==null){//远程回访满意度首次被更新不为空，再次更新的限制由VR实现
					c.Status = 'Closed';//更新状态为Closed
					c.HF_Remote_Survey_Time__c = Datetime.now();//设置远程回访时间为当前时间
				}
				if(c.HF_Onsite_Survey_Answer2_china__c != null &&
					((Case)trigger.oldmap.get(c.id)).HF_Onsite_Survey_Answer2_china__c ==null){//现场回访满意度首次被更新不为空，再次更新的限制由VR实现
					c.Status = 'Closed';//更新状态为Closed
					c.HF_Onsite_Survey_Time__c = Datetime.now();//设置现场回访时间为当前时间
				}
				if(c.HF_Spare_Survey_Answer2_china__c != null &&
					((Case)trigger.oldmap.get(c.id)).HF_Spare_Survey_Answer2_china__c ==null){//备件回访满意度首次被更新不为空，再次更新的限制由VR实现
					c.Status = 'Closed';//更新状态为Closed
					c.HF_Spare_Survey_Time__c = Datetime.now();//设置备件回访时间为当前时间
				}
			}
			if(c.Record_Type_Name__c.startsWith('China') ){
				if(c.HF_Remote_Not_Success_Survey_Reason__c != 
					((Case)trigger.oldmap.get(c.id)).HF_Remote_Not_Success_Survey_Reason__c ){//更新未回访原因
					c.HF_Remote_Survey_Time__c = Datetime.now();//设置远程回访时间为当前时间
				}
				if(c.HF_Onsite_Not_Success_Survey_Reason__c != 
					((Case)trigger.oldmap.get(c.id)).HF_Onsite_Not_Success_Survey_Reason__c ){//更新未回访原因
					c.HF_Onsite_Survey_Time__c = Datetime.now();//设置现场回访时间为当前时间
				}
				if(c.HF_Spare_Not_Success_Survey_Reason__c != 
					((Case)trigger.oldmap.get(c.id)).HF_Spare_Not_Success_Survey_Reason__c ){//更新未回访原因
					c.HF_Spare_Survey_Time__c = Datetime.now();//设置备件回访时间为当前时间
				}
			}
		}
	}
}