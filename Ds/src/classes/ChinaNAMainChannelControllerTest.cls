/*
 * Author: steven
 * Date: 2014-5-27
 * Description: test ChinaNAMainChannel
 */
@isTest
private class ChinaNAMainChannelControllerTest {
	static testMethod void myUnitTest() {
		Account acc = UtilUnitTest.createHWChinaAccount('acc');
		Account_Competitor_Situation__c mainOBJ = new Account_Competitor_Situation__c();
		//设置当前页面
		PageReference pageRef1 = new PageReference('/' + acc.Id);
		Test.setCurrentPage(pageRef1);
		//传递参数
		ApexPages.currentPage().getParameters().put('accId',acc.Id);
		//创建controller
		ApexPages.StandardController setCon1 = new ApexPages.StandardController(mainOBJ);
		ChinaNAMainChannelController massCon = new ChinaNAMainChannelController(setCon1);
		
		massCon.initMainChannel();
		
		//测试添加功能
		massCon.product = '数通';
		massCon.addMainChannel();
		massCon.product = '存储';
		massCon.addMainChannel();
		massCon.product = '智真&视讯';
		massCon.addMainChannel();
		
		//测试删除功能
		massCon.deleteMainChannel();
		
		//测试保存功能
		massCon.saveMainChannel();
		
		//测试已经存在的主对象
		ApexPages.StandardController setCon2 = new ApexPages.StandardController(massCon.mainChannelExpand);
		ChinaNAMainChannelController massCon2 = new ChinaNAMainChannelController(setCon1);
		
		massCon2.cancel();
		massCon2.editMainChannel();
	}
}