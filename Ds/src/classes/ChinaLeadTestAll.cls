/**
 * 中国区线索的测试方法
 */
@isTest
private class ChinaLeadTestAll {
    
    static testMethod void chinaLeadCreateAndUpdateTest() {
        Account_Link_Approver_Map__c approver = new Account_Link_Approver_Map__c();
        approver.RecordTypeId = CONSTANTS.APPROVERMAPCHINARECORDTYPE;
        approver.Approver__c = Userinfo.getUserId();
        approver.Representative_Office__c = '北京代表处';
        approver.Type__c = '销管';
        insert approver;

    Lead chinaLead = new Lead();
    chinaLead.RecordTypeId = CONSTANTS.CHINALEADRECORDTYPE;
    chinaLead.Company = '中国区测试公司';
    chinaLead.LastName = '中国区测试名称';
    chinaLead.Phone = '123';
    chinaLead.China_Representative_Office__c = '北京代表处';
    chinaLead.China_Province__c = '北京市';
    //chinaLead.China_Project_Description__c = '项目概述测试';
    chinaLead.China_Lead_Source_Dept__c = 'MKT';
    chinaLead.LeadSource = '公司展会';
    chinaLead.Status = '待分发';
    insert chinaLead;
    
    chinaLead.Status = '待确认1';
    update chinaLead;
    
    //chinaLead.Status = '待分发';
    //update chinaLead;
    }
    
    static TestMethod void chinaLeadInvaildTest(){
        Account_Link_Approver_Map__c approver = new Account_Link_Approver_Map__c();
        approver.RecordTypeId = CONSTANTS.APPROVERMAPCHINARECORDTYPE;
        approver.Approver__c = Userinfo.getUserId();
        approver.Representative_Office__c = '北京代表处';
        approver.Type__c = '销管';
        insert approver;
        
        Lead chinaLead = new Lead();
        chinaLead.RecordTypeId = CONSTANTS.CHINALEADRECORDTYPE;
    chinaLead.Company = '中国区测试公司w';
    chinaLead.LastName = '中国区测试名称w';
    chinaLead.Phone = '123';
    chinaLead.China_Representative_Office__c = '北京代表处';
    chinaLead.China_Province__c = '北京市';
   // chinaLead.China_Project_Description__c = '项目概述测试';
    chinaLead.China_Lead_Source_Dept__c = 'MKT';
    chinaLead.LeadSource = '公司展会';
    chinaLead.Status = '待分发';
    chinaLead.China_Lead_Invalid_Reason__c = 'test';
    insert chinaLead;
    
        ChinaLeadToInvalid cti = new ChinaLeadToInvalid();
        cti.currId = chinaLead.id;
        cti.operType = 'invalid';
      cti.init(); 
    }
    
    static TestMethod void chinaLeadConvertTest(){
        Account acc = new Account();
        acc.Name = 'TEST001';
        acc.cis__c = 'TEST123';
        acc.RecordTypeId = CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE;
        acc.Region_HW__c='China';
        acc.Representative_Office__c='系统部总部'; 
        acc.Country_HW__c='China';
        acc.Country_Code_HW__c='CN';
        acc.Province__c = '北京市';
        acc.City_Huawei_China__c = '北京市';
        acc.Industry = '大企业系统部';
        acc.Sub_industry_HW__c='大企业二部（石油化工、燃气、煤炭）';
        acc.Customer_Group__c = '能源-煤炭';
        acc.Customer_Group_Code__c = '7E';
        acc.Is_Named_Account__c = true;
        acc.blacklist_type__c='Pending';
        insert acc;
        
        Account_Link_Approver_Map__c approver = new Account_Link_Approver_Map__c();
        approver.RecordTypeId = CONSTANTS.APPROVERMAPCHINARECORDTYPE;
        approver.Approver__c = Userinfo.getUserId();
        approver.Representative_Office__c = '北京代表处';
        approver.Type__c = '销管';
        insert approver;
            
        Lead chinaLead = new Lead();
        chinaLead.RecordTypeId = CONSTANTS.CHINALEADRECORDTYPE;
    chinaLead.Company = '中国区测试公司';
    chinaLead.LastName = '中国区测试名称';
    chinaLead.Phone = '123';
    chinaLead.China_Representative_Office__c = '北京代表处';
    chinaLead.China_Province__c = '北京市';
    //chinaLead.China_Project_Description__c = '项目概述测试';
    chinaLead.China_Lead_Source_Dept__c = 'MKT';
    chinaLead.LeadSource = '公司展会';
    chinaLead.Status = '跟进中';
    chinaLead.Account_Name__c = acc.Id;
    insert chinaLead;

        ChinaLeadConvertToOpportunity cto = new ChinaLeadConvertToOpportunity(chinaLead.Id); 
      cto.convert(); 
    }
    
    
}