@isTest
private class AccountCustomGroupMappingHandlerTest {

    static testMethod void change() {
    	Account acc = UtilUnitTest.createHWChinaAccount('TEST001');
    	acc = [select id,Customer_Group_Code__c,Customer_Group__c,Sub_industry_HW__c,industry, Representative_Office__c ,Province__c from Account where id =:acc.id];
    	System.assertEquals(null, acc.Province__c);
    	System.assertEquals(null, acc.Representative_Office__c);
    	System.assertEquals(null, acc.industry);
    	System.assertEquals(null, acc.Sub_industry_HW__c);
    	System.assertEquals(null, acc.Customer_Group__c);
    	System.assertEquals(null, acc.Customer_Group_Code__c);
    	Profile p = UtilUnitTest.getProfile('Integration Profile');
    	User u = UtilUnitTest.newUserWithoutInsert(p.id);
      UtilUnitTest.dataDictonaryPrepare();
      Test.startTest();
      System.runas(u){
      	acc.Customer_Group_Code__c = 'testCode';
      	acc.Province__c = 'testCode';
      	update acc;
      }
      Test.stopTest();
      acc = [select id,Customer_Group_Code__c,Customer_Group__c,Sub_industry_HW__c,industry,
    		Representative_Office__c ,Province__c from Account where id =:acc.id];
    	System.assertEquals('testCode', acc.Province__c);
    	System.assertEquals('testProvince', acc.Representative_Office__c);
    	System.assertEquals('testIndustry', acc.industry);
    	System.assertEquals('testSubIndustry', acc.Sub_industry_HW__c);
    	System.assertEquals('testCustomerGroup', acc.Customer_Group__c);
    	System.assertEquals('testCode', acc.Customer_Group_Code__c);
      
    }
}