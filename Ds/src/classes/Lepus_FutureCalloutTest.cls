@isTest
private class Lepus_FutureCalloutTest {
	static testmethod void myUnitTest(){
		Account acc = Lepus_EIPCalloutServiceTest.createAccount('acc');
		insert acc;
		
		AccountTeamMember atm = new AccountTeamMember();
		atm.AccountId = acc.Id;
		atm.UserId = UserInfo.getUserId();
		insert atm;
		
		Lepus_FutureCallout.syncData(acc.Id, 'insert', 'Account');
		Lepus_FutureCallout.syncDataAndTeamMember(acc.Id, 'Account', '');
		
		Opportunity opp = Lepus_EIPCalloutServiceTest.createOpportunity('xxx', acc.Id);
		insert opp;
		
		Lepus_FutureCallout.syncFieldUpdate(opp.Id);
		
		Lead l = new Lead();
		l.LastName = 'x';
		l.Company = 'dfads';
		insert l;
		
		Lepus_FutureCallout.syncLeadHistory(new list<Id>{l.Id});
		Lepus_FutureCallout.syncTeamMember(new list<Id>{acc.Id}, 'Account', '');
		Lepus_FutureCallout.syncUser(new list<Id>{UserInfo.getUserId()}, 'update');
		
		OpportunityFieldHistory ofh = new OpportunityFieldHistory();
		ofh.OpportunityId = opp.Id;
		Lepus_FutureCallout.syncFieldUpdateCallout(opp.Id, opp, new List<OpportunityFieldHistory>{ofh});
		
		LeadHistory lh = new LeadHistory();
		lh.LeadId = l.Id;
		Lepus_FutureCallout.syncLeadHistoryCallout(new List<Id>{l.Id}, new list<LeadHistory>{lh});
	}
}