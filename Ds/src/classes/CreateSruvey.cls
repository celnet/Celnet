public class CreateSruvey {
    
    //private String id;
    //private String soTypeName; 
    
    public String getPath(){
    	return '/'+id;
    }
    public String msg{
    	get{return msg;}
    	set{msg=value;}
    }
    public String id{
    	get{return id;}
    	set{id=value;}
    }
    public String tp{
    	get{return tp;}
    	set{tp=value;}
    }
    public PageReference init(){
        //String tp=''; 
        id=ApexPages.currentPage().getParameters().get('id');
        tp=ApexPages.currentPage().getParameters().get('tp');
        //if(tp.equals('wo'))
        //id=System.currentPageReference().getParameters().get('sid');
        //soTypeName=EdObject.getSObjectTypeNameFromId(id);
        //if(soTypeName!=null){
        PageReference pr=create();
        /**
        String url='';
        if(tp!=null){
            	msg='';
	    		ApexPages.Message myMsg = null;
	            if(tp.equals('wo')){
	                createWorkOrderSurvey();
	                //url='/apex/MassEditWOSurvey?retURL=%2F'+id+'&wrapMassAction=1&scontrolCaching=1&id='+id+'&...';
	                url='/apex/MassEditWOSurvey?scontrolCaching=1&id='+id;
	            //}else if(soTypeName.equals('case')){
	            }else if(tp.equals('cs')){
	                createCaseSurvey();
	                url='/apex/MassEditSRSurvey?scontrolCaching=1&id='+id;
	            }
	                if(normal){
	                	pr=new PageReference(url);
	                	pr.setRedirect(true); 
	                }else{
			    		myMsg=new ApexPages.Message(ApexPages.Severity.ERROR, msg);
			    		ApexPages.addMessage(myMsg);
	                }
        }
        */
        return pr;
    }
    
    public PageReference create(){
    	normal=false;
        String url='';
        PageReference pr=null;
        if(tp!=null){
            //soTypeName=soTypeName.toLowerCase(); 
            //if(soTypeName.equals('work_order__c')){
            //try{
            	msg='';
	    		ApexPages.Message myMsg = null;
	            if(tp.equals('wo')){
	                createWorkOrderSurvey();
	                //url='/apex/MassEditWOSurvey?retURL=%2F'+id+'&wrapMassAction=1&scontrolCaching=1&id='+id+'&...';
	                url='/apex/MassEditWOSurvey?scontrolCaching=1&id='+id;
	            //}else if(soTypeName.equals('case')){
	            }else if(tp.equals('cs')){
	                createCaseSurvey();
	                url='/apex/MassEditSRSurvey?scontrolCaching=1&id='+id;
	            }
	                if(normal){
	                	pr=new PageReference(url);
	                	pr.setRedirect(true); 
	                }else{
			    		myMsg=new ApexPages.Message(ApexPages.Severity.ERROR, msg);
			    		ApexPages.addMessage(myMsg);
	                }
            //}catch(Exception e){
	    	//	ApexPages.Message myMsg = null;
	    	//	myMsg=new ApexPages.Message(ApexPages.Severity.ERROR, '批量创建失败 :'+e.getMessage());
	    	//	ApexPages.addMessage(myMsg); 
            //}
        }
        return pr;
    }
    
    public String getId(){
        return id;
    }
    
    //public String getSoTypeName(){
    //    return soTypeName;//'为 '+soTypeName+' 创建Survey';
    //}
    
    public Boolean normal{
    	get{return normal;}
    	set{normal=value;}
    }
    
    public void createWorkOrderSurvey(){
            Work_Order__c wo=[select id,name,RecordTypeId,RecordType.Name,Status__c 
            ,Engineer_Setoff_Time__c, Engineer_Return_Time__c
            from Work_Order__c where id=:id];
            
            if(wo==null){
            	msg='创建回访的工单不存在，或刚好被删除';
            	return;
            }
            if(wo.RecordType==null || wo.RecordType.Name==null){
            	msg='此工单记录类型不存在，不可创建回访';
            	return ;
            }
            if(wo.Engineer_Setoff_Time__c > wo.Engineer_Return_Time__c){
            	msg='Engineer Setoff Time must less then Engineer Return Time';
            	return;
            }
            
            List<RecordType> tmpRTList=[Select r.SobjectType, r.Name, r.Id 
                From RecordType r 
                where (r.name='Work Order Survey' or r.name='Training Survey') and r.sobjectType='Work_Order_Survey__c'];

            Map<String,Id> nameRTMap=new Map<String,Id>();
            for(RecordType rt:tmpRTList){
                if(rt.Name.indexOf('Training')>=0){
                    nameRTMap.put('TrainSurveyId',rt.id);
                }else{
                    nameRTMap.put('WOSurveyId',rt.id);
                }
            }
            //[Select c.Work_Order_No__c, c.Type__c, c.RecordTypeId, c.Name, c.Id From Customer_Survey__c c]
                
            Id tmpId=null;  
            if(wo.RecordType.Name.indexOf('Training')>=0){
                tmpId=nameRTMap.get('TrainSurveyId'); 
            }else{
                tmpId=nameRTMap.get('WOSurveyId'); 
            }
            
            if(tmpId==null){
            	msg='记录类型不能为null';
            	return; 
            }
            List<Work_Order_Survey__c> t=[Select w.Work_Order_No__c, w.Id From Work_Order_Survey__c w
            	where w.Work_Order_No__c=:wo.Id];
            if(t!=null && t.size()>0){
            	msg='该工单已经创建过回访，不可执行批量创建';
            	return;
            }
            List<Work_Order_Survey__c> customerSuryList=new List<Work_Order_Survey__c>();
            Work_Order_Survey__c cs=null;
            if(wo.RecordType.Name.indexOf('Training')>=0){
                cs=new Work_Order_Survey__c(
                         Work_Order_No__c=wo.Id
                        ,Type__c='01. Have you gain any aids out of the course to work?'
                        ,group__c='课程评价'
                        ,RecordTypeId=tmpId
                    );
                customerSuryList.add(cs);
                cs=new Work_Order_Survey__c(
                         Work_Order_No__c=wo.Id
                        ,Type__c='02. Is the course clear or not?'
                        ,group__c='课程评价'
                        ,RecordTypeId=tmpId
                    );
                customerSuryList.add(cs);
                cs=new Work_Order_Survey__c(
                         Work_Order_No__c=wo.Id
                        ,Type__c='03. Do you think the content is adequate and the structure is integrated?'
                        ,group__c='课程评价'
                        ,RecordTypeId=tmpId
                    );
                customerSuryList.add(cs);
                cs=new Work_Order_Survey__c(
                         Work_Order_No__c=wo.Id
                        ,Type__c='04. Do you think the case-study or practice is helpful?'
                        ,group__c='课程评价'
                        ,RecordTypeId=tmpId
                    );
                customerSuryList.add(cs);
                cs=new Work_Order_Survey__c(
                         Work_Order_No__c=wo.Id
                        ,Type__c='05. Does the course give the points?'
                        ,group__c='教师评价'
                        ,RecordTypeId=tmpId
                    );
                customerSuryList.add(cs);
                cs=new Work_Order_Survey__c(
                         Work_Order_No__c=wo.Id
                        ,Type__c='06. Are the training methods and teaching techniques well-organized or not during the course?'
                        ,group__c='教师评价'
                        ,RecordTypeId=tmpId
                    );
                customerSuryList.add(cs);
                cs=new Work_Order_Survey__c(
                         Work_Order_No__c=wo.Id
                        ,Type__c='07. Does the instructor have a good expression and live speaking?'
                        ,group__c='教师评价'
                        ,RecordTypeId=tmpId
                    );
                customerSuryList.add(cs);
                cs=new Work_Order_Survey__c(
                         Work_Order_No__c=wo.Id
                        ,Type__c='08. Do you think the time and schedule are well-organized or not?'
                        ,group__c='教师评价'
                        ,RecordTypeId=tmpId
                    );
                customerSuryList.add(cs);
                cs=new Work_Order_Survey__c(
                         Work_Order_No__c=wo.Id
                        ,Type__c='09. Do you think the course is impressive or not?'
                        ,group__c='教师评价' 
                        ,RecordTypeId=tmpId
                    );
                customerSuryList.add(cs);
                cs=new Work_Order_Survey__c(
                         Work_Order_No__c=wo.Id
                        ,Type__c='10. How do you rate the instructor’s communication and contribution to solve doubts?'
                        ,group__c='教师评价'
                        ,RecordTypeId=tmpId
                    );
                customerSuryList.add(cs);
                cs=new Work_Order_Survey__c(
                         Work_Order_No__c=wo.Id
                        ,Type__c='11. Does the training content meet my expectations?'
                        ,group__c='总体评价'
                        ,RecordTypeId=tmpId
                    );
                customerSuryList.add(cs);
                cs=new Work_Order_Survey__c(
                         Work_Order_No__c=wo.Id
                        ,Type__c='12. Your comments about what you have learned from the course?'
                        ,Description__c='课程的哪些部分对您最有价值？学完本课程您认为有哪些具体收获？'
                        ,RecordTypeId=tmpId
                    );
                customerSuryList.add(cs);
                cs=new Work_Order_Survey__c(
                         Work_Order_No__c=wo.Id
                        ,Type__c='13. Your comments about what more you want to learn?'
                        ,Description__c='在该课程中你还想学到哪些内容？'
                        ,RecordTypeId=tmpId
                    );
                customerSuryList.add(cs);
                cs=new Work_Order_Survey__c(
                         Work_Order_No__c=wo.Id
                        ,Type__c='14. Your comments about others?'
                        ,Description__c='对教师、课程等其他方面的具体建议？'
                        ,RecordTypeId=tmpId
                    );
                customerSuryList.add(cs);
            }else{
                /////////////////////////
                cs=new Work_Order_Survey__c(
                         Work_Order_No__c=wo.Id
                        ,Type__c='1.Response time'
                        ,Description__c='1.请问您对工程师的响应时间评价如何？'
                        ,RecordTypeId=tmpId
                    );
                customerSuryList.add(cs);
                
                cs=new Work_Order_Survey__c(
                         Work_Order_No__c=wo.Id
                        ,Type__c='2.Technical level' 
                        ,Description__c='2.请问您对工程师的技术水平评价如何？'
                        ,RecordTypeId=tmpId
                    );
                customerSuryList.add(cs);
                
                cs=new Work_Order_Survey__c(
                         Work_Order_No__c=wo.Id
                        ,Type__c='3.Service attitude' 
                        ,Description__c='3.请问您对工程师的服务态度评价如何？'
                        ,RecordTypeId=tmpId
                    );
                customerSuryList.add(cs);
                
                cs=new Work_Order_Survey__c(
                         Work_Order_No__c=wo.Id
                        ,Type__c='4.Suggestion'
                        ,Description__c='4.请问您觉得我司服务还有哪些需要改进的地方？'
                        ,RecordTypeId=tmpId
                    );
                customerSuryList.add(cs);
                
                cs=new Work_Order_Survey__c(
                         Work_Order_No__c=wo.Id
                        ,Type__c='5.Product Survey'
                        ,Description__c='5. 请问您对我司产品质量评价如何？（如可操作性、稳定性、可维护性等）'
                        ,RecordTypeId=tmpId
                    );
                customerSuryList.add(cs);
            }
            try{
            	if(customerSuryList!=null && customerSuryList.size()>0)insert(customerSuryList);
            	normal=true;
            }catch(Exception e){
            	normal=false;
            	msg='创建工单回访失败，可能相应工单记录的数据不合法，或不完整，或不符合正常规则...'+e;
            }
    }
    
    public void createCaseSurvey(){
            Case c=[select Id,Status from Case where Id=:id];
            if(c==null){
            	msg='请求回访不存在或刚好被删除...';
            	return;
            }
            List<Service_Request_Survey__c> t=[Select s.SR_No__c, s.Id From Service_Request_Survey__c s
            	where s.SR_No__c=:c.Id];
            if(t!=null && t.size()>0){
            	msg='该服务请求已经创建回访,不可再执行批量创建....';
            	return;
            }
            
            //if(c==null || c.Status!='Pending for Survey')return;
            List<Service_Request_Survey__c> customerSuryList=new List<Service_Request_Survey__c>();
            Service_Request_Survey__c cs=null;
                cs=new Service_Request_Survey__c(
                         SR_No__c=c.Id
                        ,Type__c='1.Response time'
                        ,Description__c='1.请问您对工程师的响应时间评价如何？'
                        //,RecordTypeId=tmpId
                    );
                customerSuryList.add(cs);
                
                cs=new Service_Request_Survey__c(
                         SR_No__c=c.Id
                        ,Type__c='2.Technical level'
                        ,Description__c='2.请问您对工程师的技术水平评价如何？'
                        //,RecordTypeId=tmpId
                    );
                customerSuryList.add(cs);
                
                cs=new Service_Request_Survey__c(
                         SR_No__c=c.Id
                        ,Type__c='3.Service attitude'
                        ,Description__c='3.请问您对工程师的服务态度评价如何？'
                        //,RecordTypeId=tmpId
                    );
                customerSuryList.add(cs); 
                
                cs=new Service_Request_Survey__c(
                         SR_No__c=c.Id
                        ,Type__c='4.Suggestion'
                        ,Description__c='4.请问您觉得我司服务还有哪些需要改进的地方？'
                        ,Suggestion__c=' '
                    );
                customerSuryList.add(cs);
                
                cs=new Service_Request_Survey__c(
                         SR_No__c=c.Id
                        ,Type__c='5.Product Survey'
                        ,Description__c='5.请问您对我司产品质量评价如何？（如可操作性、稳定性、可维护性等）'
                        //,RecordTypeId=tmpId
                    );
                customerSuryList.add(cs);
                
            if(customerSuryList!=null && customerSuryList.size()>0)
            try{
            	insert(customerSuryList);
            	normal=true;
            }catch(Exception e){
            	normal=false;
            	msg='创建请求回访失败，可能相应服务请求记录的数据不合法，或不完整，或不符合正常规则...';
            }
    }
    
    public static TestMethod void currClsTest(){
    	//测试前提:
    	//保证记录中有以下记录:
    	//1、Work_Order对象中，一条记录的记录类型为Training...，一条不为Training....
    	//2、Case中最少有一条记录
    	//('012900000000OISAA2','012900000000OITAA2')(training,)
            List<RecordType> tmpRTList=[Select r.SobjectType, r.Name, r.Id 
                From RecordType r 
                where (r.name='Work Order Survey' or r.name='Training Survey') and r.sobjectType='Work_Order_Survey__c'
                limit 20];
    	
            Map<String,Id> nameRTMap=new Map<String,Id>();
            for(RecordType rt:tmpRTList){
                if(rt.Name.indexOf('Training')>=0){
                    nameRTMap.put('TrainSurveyId',rt.id);
                }else{
                    nameRTMap.put('WOSurveyId',rt.id);
                }
            }
        String woId=''; 
    	CreateSruvey cs1=null;
    	Id tmpId=null;
    	
    	tmpId=nameRTMap.get('WOSurveyId');
    	System.debug('tmpId1='+tmpId);
    	
    	Case c0 = UtilUnitTest.createChinaServiceRequest();
    	
    	 List<RecordType> rts = [select id,name from RecordType 
    	 	where sobjecttype = 'Work_Order__c' and 
    	 		(name = 'Training Work Order' or name = '(N) On Site Support Work Order') ];
    	 Account acc = UtilUnitTest.createAccount();
    	 for(RecordType rt : rts){
    	 	if(rt.name == 'Training Work Order'){
		    	Work_Order__c wo1=new Work_Order__c(RecordTypeId = rt.id,Account_Name__c = acc.id,Service_Request_No__c = c0.id) ;
		    	insert wo1;
    	 	}else{
		    	Work_Order__c wo1=new Work_Order__c(RecordTypeId = rt.id,Account_Name__c = acc.id,Service_Request_No__c = c0.id) ;
		    	insert wo1;
    	 	}
    	 }
    	Work_Order__c wo=null;
    	List<Work_Order__c> woList=[select id,name,RecordTypeId,RecordType.Name,Status__c 
            ,Engineer_Setoff_Time__c, Engineer_Return_Time__c
            from Work_Order__c limit 20];
        if(woList==null || woList.size()<1){
        	wo=[select id,name,RecordTypeId,RecordType.Name,Status__c 
            ,Engineer_Setoff_Time__c, Engineer_Return_Time__c
            from Work_Order__c limit 1];
            wo.RecordTypeId=tmpId;
            update(wo);
        }else{
        	wo=woList.get(0);
        }
        woId=wo.Id;
    	cs1=new CreateSruvey();
    	cs1.id=woId;
    	cs1.tp='wo';
    	cs1.create();
    	Boolean bb=cs1.normal;
    	String ss=cs1.msg;    	
    	
    	tmpId=nameRTMap.get('TrainSurveyId'); 
    	System.debug('tmpId2='+tmpId);
    	woList=[select id,name,RecordTypeId,RecordType.Name,Status__c 
            ,Engineer_Setoff_Time__c, Engineer_Return_Time__c
            from Work_Order__c limit 20];
        if(woList==null || woList.size()<1){
        	wo=[select id,name,RecordTypeId,RecordType.Name,Status__c 
            ,Engineer_Setoff_Time__c, Engineer_Return_Time__c
            from Work_Order__c limit 1];
            wo.RecordTypeId=tmpId;
            update(wo);
        }else{
        	wo=woList.get(0);
        }
        woId=wo.Id;
    	cs1=new CreateSruvey();
    	cs1.id=woId;
    	cs1.tp='wo';
    	cs1.create();
    	
    	Case c=[select Id,Status from Case limit 1];
    	String csId=c.id;
    	CreateSruvey cs2=new CreateSruvey();
    	cs2.id=csId;
    	cs2.tp='cs';
    	//cs2.init();
    	cs2.create();
    	 
    }
}