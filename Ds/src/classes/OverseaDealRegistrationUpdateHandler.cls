/**
 * @Purpose : (海外)报备审批通过/驳回时给总代联系人发送邮件提醒;
 * @Author : steven
 * @Date :   2013-11-15
 */
public without sharing class OverseaDealRegistrationUpdateHandler implements Triggers.Handler{ //after update
	public static Boolean isFirstRun = true;
	public void handle() {
		if(!isFirstRun) return;
		for(Deal_Registration__c deal : (List<Deal_Registration__c>)Trigger.new){
			Deal_Registration__c oldDeal = (Deal_Registration__c)Trigger.oldMap.get(deal.id);
			if(deal.RecordTypeId==CONSTANTS.HUAWEIOVERSEADEALRECORDTYPE && 
			   deal.Deal_Status__c != oldDeal.Deal_Status__c && (deal.Deal_Status__c == 'Registration Failed' || deal.Deal_Status__c == 'Open')) { 
				try{
					//Step1: 获取当前报备所有总代的邮箱
					List<Deal_Registration_Distributor__c> partnerList = 
						[Select Distributor_Contact_Email_Address__c From Deal_Registration_Distributor__c 
							Where Deal_Registration__c =: deal.Id And Distributor_Contact_Email_Address__c <> null];
					List<String> partnerMails = new List<String>(); 
					for(Deal_Registration_Distributor__c partner : partnerList) {
						partnerMails.add(partner.Distributor_Contact_Email_Address__c);
					}
					//Step2: 根据报备状态选择邮件模板并发送邮件
					Id templateId = null;
					if(deal.Deal_Status__c == 'Registration Failed') {
						templateId = [select id from EmailTemplate where DeveloperName='Deal_Been_Rejected_Oversea'].id;
					} else {
						templateId = [select id from EmailTemplate where DeveloperName='Deal_Been_Approved_Oversea'].id;
					}
					UtilEmail.sendMailByTemplate(partnerMails,templateId,deal.Id);
					isFirstRun = false;
				}catch(Exception e){
					System.debug(e);
				}
			}
		}
	}
	
}