/*
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-5-16
 * Description: test MassMaIncomeForecastExtension and MassMaOrderForecastExtension
 */
 
@isTest
private class MassMaForecastExtensionTest{
    static Sales_Meeting__c sm1;
    static Id iafProductRecordTypeId;
    static Id oafIndustryRecordTypeId;
    static Income_And_Forecast__c[] iaf1List;
    static Order_And_Forecast__c[] oaf1List;
    
    static void prepareData(){
        Sales_Meeting__c[] sms = new Sales_Meeting__c[]
        {
            new Sales_Meeting__c(Year__c = '2014',Month__c = '6月',Representative_Office__c = '北京代表处')
        };
        insert sms;
        sm1 = [Select Id From Sales_Meeting__c Where Representative_Office__c = '北京代表处'];
        
        RecordType[] iafRecordTypes = [Select Id,Name From RecordType Where SobjectType = 'Income_And_Forecast__c' And IsActive = true];
        for(RecordType rt : iafRecordTypes){
            if(rt.Name == 'Income And Forecast Record Type(Product)'){
                iafProductRecordTypeId = rt.Id;
            }
        }
        
        RecordType[] oafRecordTypes = [Select Name,Id From RecordType Where SobjectType = 'Order_And_Forecast__c' And IsActive = true];
        for(RecordType rt : oafRecordTypes){
            if(rt.Name == 'Order And Forecast(Industry)'){
                oafIndustryRecordTypeId = rt.Id;
            }
        }
        
        Income_And_Forecast__c[] iafs1 = new Income_And_Forecast__c[]{
            new Income_And_Forecast__c(RecordTypeId = iafProductRecordTypeId,Sales_Meeting__c = sm1.Id, Is_Important_Region__c = '是', Product__c = '存储')
        };
        insert iafs1;
        iaf1List = [Select Id From Income_And_Forecast__c Where Sales_Meeting__c =: sm1.Id]; 
        
        Order_And_Forecast__c[] oafs1 = new Order_And_Forecast__c[]{
            new Order_And_Forecast__c(RecordTypeId = oafIndustryRecordTypeId,Sales_Meeting__c = sm1.Id, Is_Important_Region__c = '是', Industry__c = '交通系统部')
        };
        insert oafs1;
        oaf1List = [Select Id From Order_And_Forecast__c Where Sales_Meeting__c =: sm1.Id]; 
    }
    
    static testmethod void testIncome(){
        prepareData();
        
        // 执行测试
        PageReference pageRef1 = new PageReference('/' + sm1.Id);
        Test.setCurrentPage(pageRef1); 
        ApexPages.currentPage().getParameters().put('Id',sm1.Id);
        
        ApexPages.Standardcontroller con = new ApexPages.Standardcontroller(new Account());
        ApexPages.StandardSetController setCon1 = new ApexPages.StandardSetController(iaf1List);
        MassMaIncomeForecastExtension massCon = new MassMaIncomeForecastExtension(setCon1);
        
        massCon.saveIncomeForecast();
        String nextPage1 = massCon.cancel().getUrl();
        
        // 验证结果
        System.assertEquals(nextPage1, '/' + sm1.Id);
    }
    
    static testmethod void testOrder()
    {
        prepareData();
        
        // 执行测试
        PageReference pageRef1 = new PageReference('/' + sm1.Id);
        Test.setCurrentPage(pageRef1);
        ApexPages.currentPage().getParameters().put('Id',sm1.Id);
        
        ApexPages.StandardSetController setCon1 = new ApexPages.StandardSetController(oaf1List);
        MassMaOrderForecastExtension massCon = new MassMaOrderForecastExtension(setCon1);
        
        massCon.saveOrderForecast();
        String nextPage1 = massCon.cancel().getUrl();
        
        //验证结果
        System.assertEquals(nextPage1, '/' + sm1.Id);
    }
}