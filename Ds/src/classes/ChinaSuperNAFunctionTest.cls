@isTest
private class ChinaSuperNAFunctionTest {
	static testMethod void myUnitTest() {
		Account acc = new Account();
    acc.Name = 'TEST001';
    acc.cis__c = 'TEST123';
    acc.RecordTypeId = CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE;
    acc.Region_HW__c='China';
    acc.Representative_Office__c='系统部总部'; 
    acc.Country_HW__c='China';
    acc.Country_Code_HW__c='CN';
    acc.Province__c = '北京市';
    acc.City_Huawei_China__c = '北京市';
    acc.Industry = '大企业系统部';
    acc.Sub_industry_HW__c='大企业二部（石油化工、燃气、煤炭）';
    acc.Customer_Group__c = '能源-煤炭';
    acc.Customer_Group_Code__c = '7E';
    acc.Is_Named_Account__c = true;
    acc.blacklist_type__c='Pending';
    insert acc;
    
		China_Strategy_NA__c superNA = new China_Strategy_NA__c(name='test',NA_Child__c=acc.id);
		insert superNA;

		ChinaSuperNAFunction sna = new ChinaSuperNAFunction(new ApexPages.StandardController(superNA)); 
		sna.save(); 
	}
	
}