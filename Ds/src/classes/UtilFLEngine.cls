/**
	FLEngine("F"ormula "L"ike Engine) : 
	Sometimes we need to populate a field from the corresponding value in a parent field,
	but can't use formula,such as picklist or need this field to perform criteria-sharing.
	This class simplifies formula like the population like a formula.
*/
public without sharing class UtilFLEngine {
	static String SOQL_TEMPLATE = 'SELECT {0} FROM {1} {2} ';
	
	///TODO:WHEN THE PARENT IS CLEARED,SHOULD THE FIELD ON CHILD BE CLEARED TOO? 
	
	
	//we cant use the [select id,parent__r.id from child ] method,because when there's in the before insert trigger,the value is null
	//might implement the util class when needed
	public static Sobject[] populater(Context ctx,List<sObject> children){
		Set<id> parentIds = new Set<id>();
    for (sObject c : children) {
    	parentIds.add((Id)c.get(ctx.lookupField.getName()));
    }	
        
    String soqlProjection = ' ' ;
   	for (lookupField rsf : ctx.fieldsToLookup) {
    		// create aggreate projection with alias for easy fetching via AggregateResult class
    		// i.e. SUM(Amount) Amount
    		soqlProjection +=  rsf.master.getName() + ' ,';//add the 'X' to avoid the situation: count(id) id
    	}
    	soqlProjection = soqlProjection.substring(0,soqlProjection.length()-1);
    	String detailTblName = ctx.parent.getDescribe().getName(); 
    	String whereClause = ' where id in :parentIds';
    	String soql = String.format(SOQL_TEMPLATE, new String[]{soqlProjection, detailTblName, whereClause});
    	System.debug('the current soql:-----' + soql);
			Map<id,sobject> parentsMap = new Map<id,sobject>(database.query(soql));
			for(sObject c : children){
				sObject parent = parentsMap.get((Id)c.get(ctx.lookupField.getName()));
				if(parent != null){
					for(lookupField rsf : ctx.fieldsToLookup){
						c.put(rsf.detail.getName(),parent.get(rsf.master.getName()));
					}
				}else{//需要根据传入参数,判断是否将child上对应值设为空
					
				}
			}
			
			
			return children;
	}
	
	
	public class LookupField {
        public Schema.Describefieldresult master;
        public Schema.Describefieldresult detail;        
        
        public LookupField(Schema.Describefieldresult m, 
                                         Schema.Describefieldresult d) {
            this.master = m;
            this.detail = d;
        }   
        
    }
	
	 /**
    	Context having all the information about the update to be done. 
    	Please note : This class encapsulates many update fields .
    */
	public class Context {
	    // Master Sobject Type
	    public Schema.Sobjecttype parent;
	    // Child/Details Sobject Type
	    public Schema.Sobjecttype child;
	    // Lookup field on Child/Detail Sobject
	    public Schema.Describefieldresult lookupField;
	    // various fields to rollup on
	    public List<LookupField> fieldsToLookup;

		// Where clause or filters to apply while aggregating detail records
		public String detailWhereClause;		    

	    public Context(Schema.Sobjecttype m, Schema.Sobjecttype d, 
                           Schema.Describefieldresult lf) {
			this(m, d, lf, '');                           	
        }
        
	    public Context(Schema.Sobjecttype m, Schema.Sobjecttype d, 
                           Schema.Describefieldresult lf, String detailWhereClause) {
	        this.parent = m;
	        this.child = d;
	        this.lookupField = lf;
	        this.detailWhereClause = detailWhereClause;
	        this.fieldsToLookup = new List<LookupField>();
	    }

	    /**
	    	Adds new rollup summary fields to the context
	    */
	    public void add(LookupField fld) {
	        this.fieldsToLookup.add(fld);
	    }
	}    
}