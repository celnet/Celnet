/**
 * @Purpose : 客户行业属性变更同步刷新机会点的行业信息 http://10.24.52.52/svn/SF/tssandbox/src/triggers/TigOnAccount.trigger
 *            update 添加记录类型过滤，仅针对海外最终客户
 * @Author : BLS
 * @Date :   2014-07-01 Steven Update
 */
public without sharing class AccountUpdateIndustryHandler  implements Triggers.Handler{//after update
	public static Boolean isFirstRun = true;
	public void handle() {
		if(AccountUpdateIndustryHandler.isFirstRun){
			Set<Id> accIds = new Set<Id>();
			//Step1:记录所有行业更新的客户数据
			for(Account newAcc : (List<Account>)trigger.new) {
				if(newAcc.RecordTypeId != CONSTANTS.HUAWEIOVERSEACUSTOMERRECORDTYPE) continue;
				if(newAcc.Industry != ((Account)trigger.oldMap.get(newAcc.Id)).Industry) {
					accIds.add(newAcc.Id);
				}
			}
			if (accIds.size() == 0) return;
			
			//Step2:一次查询出所有涉及的机会点数据
			List<Opportunity> opps = new List<Opportunity>();
			for (Opportunity opp : [SELECT Industry__c, Account.Industry FROM Opportunity WHERE AccountId IN :accIds]) {
				opp.Industry__c = opp.Account.Industry;
				opps.add(opp);
			}
			
			//Step3:一次更新所有的涉及的机会点数据
			try {
				update opps;
			} catch(DMLException e) {trigger.new[0].addError(e.getDMLMessage(0));
				System.debug('AccountUpdateIndustryHandler Exception: ' + e);
			}
			AccountUpdateIndustryHandler.isFirstRun = false;
		}
	}
}