/**
 * @Purpose : (中国区)报备提交时文本记录报备提交人的姓名及所属经销商名称
 * @Author : steven
 * @Date :   2014-09-03
 */
public without sharing class ChinaDealRegistrationInsertHandler implements Triggers.Handler{ //before insert
	public void handle() {
		//Step1: 记录所有待刷新的报备联系人ID
		Set<Id> contactIds = new Set<Id>();
		for(Deal_Registration__c deal : (List<Deal_Registration__c>)Trigger.new){
			if(deal.RecordTypeId==CONSTANTS.HUAWEICHINADEALRECORDTYPE){
				if(deal.login_contact__c != null) contactIds.add(deal.login_contact__c);
			}
		}
		if(contactIds.isEmpty()) return;
		system.debug('-----ChinaDealRegistrationInsertHandler----');
		
		//Step2: 一次查询所有报备联系人信息
		Map<id,Contact> contactMap = new Map<id,Contact>([Select Id,FirstName,LastName,Account.Name From Contact Where Id In : contactIds]);
		
		//Step3: 根据报备联系人信息刷新报备相关字段
		for(Deal_Registration__c deal : (List<Deal_Registration__c>)Trigger.new){
			if(deal.RecordTypeId==CONSTANTS.HUAWEICHINADEALRECORDTYPE){
				if(deal.login_contact__c != null) {
					Contact ct = contactMap.get(deal.login_contact__c);
					if(ct!=null) {
						deal.Submitted_by_Name_Text__c = ct.FirstName + ' ' + ct.LastName;
						deal.Submitted_Partner_Name_Text__c = ct.Account.Name;
					}
				}
			}
		}
	}
}