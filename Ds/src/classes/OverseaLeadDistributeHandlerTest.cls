/*
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-6-24
 * Description: Test OverseaLeadDistributeHandler
 */
@isTest
private class OverseaLeadDistributeHandlerTest {
	static testmethod void test(){
		Account_Link_Approver_Map__c am = new Account_Link_Approver_Map__c();
		am.Country__c = 'Germany';
		am.Approver__c = UserInfo.getUserId();
		am.RecordTypeId = CONSTANTS.APPROVERMAPOVERSEALEADSOWNERRECORDTYPE;
		insert am;
		
		Account acc = UtilUnitTest.createAccount('acc');
		
		Lead l = new Lead();
		l.Country__c = 'Germany';
		l.RecordTypeId = CONSTANTS.OVERSEALEADRECORDTYPE;
		l.LastName = 'test';
		l.Company = 'test';
		insert l;
		
		Lead l2 = [Select id, OwnerId From Lead];
		
		System.assertEquals(UserInfo.getUserId(), l2.OwnerId);
	}
}