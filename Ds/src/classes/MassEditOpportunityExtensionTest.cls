/*
Author: tommy.liu@celnet.com.cn
Created On: 2014-5-15
Function: Test MassEditOpportunityExtension
 */
@isTest
private class MassEditOpportunityExtensionTest 
{
    static testMethod void test() 
    {
    	Account acc = UtilUnitTest.createHWChinaAccount('test account');
    	Opportunity[] opps = new Opportunity[] {
    		new Opportunity(AccountId = acc.Id, Name='test opp 1', CloseDate = Date.newInstance(2014, 5, 30), StageName = 'Project Verification', Master_Channer_Partner__c = 'MCP1'),
    		new Opportunity(AccountId = acc.Id, Name='test opp 2', CloseDate = Date.newInstance(2014, 5, 30), StageName = 'Project Verification', Master_Channer_Partner__c = 'MCP1'),
    		new Opportunity(AccountId = acc.Id, Name='test opp 3', CloseDate = Date.newInstance(2014, 5, 30), StageName = 'Project Verification', Master_Channer_Partner__c = 'MCP1')
    		};
    	insert opps;
    	
    	
      List<Opportunity> oppList = [SELECT Name, StageName FROM Opportunity];
      System.assertEquals(3, oppList.size());
			ApexPages.StandardSetController setCon = new ApexPages.StandardSetController(oppList);
			setCon.setSelected(oppList);
			MassEditOpportunityExtension massCon = new MassEditOpportunityExtension(setCon);
			
			List<Schema.FieldSetMember> additionFieldList = SObjectType.Opportunity.FieldSets.Mass_Edit_Addition.getFields();
			List<Schema.FieldSetMember> addition2FieldList = SObjectType.Opportunity.FieldSets.Mass_Edit_Addition2.getFields();
			List<SelectOption> additionFieldOptionList = massCon.getAdditionFieldOptions();
			List<SelectOption> addition2FieldOptionList = massCon.getAddition2FieldOptions();
			Integer additionFieldOptionSize = massCon.getOptionSize();
			Integer addition2FieldOptionSize = massCon.getOption2Size();
			System.assertEquals(additionFieldList.size(), additionFieldOptionSize);
			System.assertEquals(additionFieldList.size(), additionFieldOptionList.size());
			System.assertEquals(addition2FieldList.size(), addition2FieldOptionSize);
			System.assertEquals(addition2FieldList.size(), addition2FieldOptionList.size());
			
			String selectFieldStr = '';
			for(SelectOption option: additionFieldOptionList)
			{
				selectFieldStr += option.getValue() + ',';
			}
			massCon.SelectedFieldStr = selectFieldStr;
			massCon.setSelectedFields();
			List<MassEditOpportunityExtension.Field> selectedFieldList = massCon.getSelectedFields();
			System.assertEquals(additionFieldList.size(), selectedFieldList.size());
			selectFieldStr = '';
			for(SelectOption option: addition2FieldOptionList)
			{
				selectFieldStr += option.getValue() + ',';
			}
			massCon.SelectedFieldStr = selectFieldStr;
			massCon.setSelectedFields();
			selectedFieldList = massCon.getSelectedFields();
			System.assertEquals(addition2FieldList.size(), selectedFieldList.size());
			
			List<MassEditOpportunityExtension.OppRow> oppRows = massCon.getSelectedOppRows();
			System.assertEquals(3, oppRows.size());
    }
}