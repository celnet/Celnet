@isTest
private class Lepus_FailureReSyncScheduleJobTest {
	static testmethod void myUnitTest(){
		Account acc = Lepus_EIPCalloutServiceTest.createAccount('test');
		insert acc;
		
		AccountTeamMember atm = new AccountTeamMember();
		atm.UserId = UserInfo.getUserId();
		atm.AccountId = acc.id;
		insert atm;
		
		Opportunity opp = Lepus_EIPCalloutServiceTest.createOpportunity('dfads', acc.Id);
		insert opp;
		
		Lead l = new Lead();
		l.LastName = 'xcd';
		l.Company = 'fad';
		insert l;
		
		Contact con = Lepus_EIPCalloutServiceTest.createContact(acc.Id);
		insert con;
		
		createSyncFailRecord(acc.Id, 'insert', '业务数据同步', '');
		
		Lepus_FailureReSyncScheduleJob job = new Lepus_FailureReSyncScheduleJob();
		job.execute(null);
	}
	
	static testmethod void myUnitTest2(){
		Account acc = Lepus_EIPCalloutServiceTest.createAccount('test');
		insert acc;
		
		AccountTeamMember atm = new AccountTeamMember();
		atm.UserId = UserInfo.getUserId();
		atm.AccountId = acc.id;
		insert atm;
		
		Opportunity opp = Lepus_EIPCalloutServiceTest.createOpportunity('dfads', acc.Id);
		insert opp;
		
		Lead l = new Lead();
		l.LastName = 'xcd';
		l.Company = 'fad';
		insert l;
		
		Contact con = Lepus_EIPCalloutServiceTest.createContact(acc.Id);
		insert con;
		
		createSyncFailRecord(opp.Id, 'insert', '业务数据同步', '');
		
		Lepus_FailureReSyncScheduleJob job = new Lepus_FailureReSyncScheduleJob();
		job.execute(null);
	}
	
	static testmethod void myUnitTest3(){
		Account acc = Lepus_EIPCalloutServiceTest.createAccount('test');
		insert acc;
		
		AccountTeamMember atm = new AccountTeamMember();
		atm.UserId = UserInfo.getUserId();
		atm.AccountId = acc.id;
		insert atm;
		
		Opportunity opp = Lepus_EIPCalloutServiceTest.createOpportunity('dfads', acc.Id);
		insert opp;
		
		Lead l = new Lead();
		l.LastName = 'xcd';
		l.Company = 'fad';
		insert l;
		
		Contact con = Lepus_EIPCalloutServiceTest.createContact(acc.Id);
		insert con;
		
		createSyncFailRecord(l.Id, 'insert', '业务数据同步', '');
		
		Lepus_FailureReSyncScheduleJob job = new Lepus_FailureReSyncScheduleJob();
		job.execute(null);
	}
	
	static testmethod void myUnitTest4(){
		Account acc = Lepus_EIPCalloutServiceTest.createAccount('test');
		insert acc;
		
		AccountTeamMember atm = new AccountTeamMember();
		atm.UserId = UserInfo.getUserId();
		atm.AccountId = acc.id;
		insert atm;
		
		Opportunity opp = Lepus_EIPCalloutServiceTest.createOpportunity('dfads', acc.Id);
		insert opp;
		
		Lead l = new Lead();
		l.LastName = 'xcd';
		l.Company = 'fad';
		insert l;
		
		Contact con = Lepus_EIPCalloutServiceTest.createContact(acc.Id);
		insert con;
		
		createSyncFailRecord(con.Id, 'insert', '业务数据同步', '');
		
		Lepus_FailureReSyncScheduleJob job = new Lepus_FailureReSyncScheduleJob();
		job.execute(null);
	}
	
	static testmethod void myUnitTestFieldUpdate(){
		Account acc = Lepus_EIPCalloutServiceTest.createAccount('testdfa');
		insert acc;
		
		AccountTeamMember atm = new AccountTeamMember();
		atm.UserId = UserInfo.getUserId();
		atm.AccountId = acc.id;
		insert atm;
		
		Opportunity opp = Lepus_EIPCalloutServiceTest.createOpportunity('dfads', acc.Id);
		insert opp;
		
		createSyncFailRecord(opp.Id, 'insert', '字段更新同步', '');
		
		Lepus_FailureReSyncScheduleJob job = new Lepus_FailureReSyncScheduleJob();
		job.execute(null);
	}
	
	static testmethod void myUnitTestUserRole(){
		
		createSyncFailRecord(UserInfo.getUserId(), 'update', '用户角色同步', '');
		Lepus_FailureReSyncScheduleJob job = new Lepus_FailureReSyncScheduleJob();
		job.execute(null);
	}
	
	static testmethod void myUnitTestLeadUpdate(){
		createSyncFailRecord('ddd', 'update', '线索更新同步', 'xxxx');
		Lepus_FailureReSyncScheduleJob job = new Lepus_FailureReSyncScheduleJob();
		job.execute(null);
	}
	
	static testmethod void myUnitTestTeamMemberSync(){
		Account acc = Lepus_EIPCalloutServiceTest.createAccount('test');
		insert acc;
		
		AccountTeamMember atm = new AccountTeamMember();
		atm.UserId = UserInfo.getUserId();
		atm.AccountId = acc.id;
		insert atm;
		
		AccountTeamMember atm2 = new AccountTeamMember();
		atm2.UserId = UserInfo.getUserId();
		atm2.AccountId = acc.id;
		insert atm2;
		createSyncFailRecord(acc.Id, 'update', '团队成员同步', 'xxxx');
		Lepus_FailureReSyncScheduleJob job = new Lepus_FailureReSyncScheduleJob();
		job.execute(null);
	}
	
	static testmethod void myUnitTestTeamMemberSyncOpp(){
		Account acc = Lepus_EIPCalloutServiceTest.createAccount('test');
		insert acc;
		
		Opportunity opp = Lepus_EIPCalloutServiceTest.createOpportunity('Opp', acc.Id);
		insert opp;
		OpportunityTeamMember otm = new OpportunityTeamMember();
		otm.UserId = UserInfo.getUserId();
		otm.OpportunityId = opp.id;
		insert otm;
		
		createSyncFailRecord(opp.Id, 'update', '团队成员同步', 'xxxx');
		Lepus_FailureReSyncScheduleJob job = new Lepus_FailureReSyncScheduleJob();
		job.execute(null);
	}
	
	static testmethod void myUnitTestTeamMemberSyncLead(){
		Lead l = new Lead();
		l.OwnerId = UserInfo.getUserId();
		l.LastName = 'dfa';
		l.Company = 'dfa';
		insert l;
		
		createSyncFailRecord(l.Id, 'update', '团队成员同步', 'xxxx');
		Lepus_FailureReSyncScheduleJob job = new Lepus_FailureReSyncScheduleJob();
		job.execute(null);
	}
	
	static void createSyncFailRecord(String recordId, String action, String syncType, String xmlString){
		SyncFailRecord__c sfr = new SyncFailRecord__c();
		sfr.ReSync_Success__c = false;
		sfr.SyncRecordId__c = recordId;
		sfr.SyncAction__c = action;
		sfr.SyncType__c = syncType;
		sfr.SyncTime__c = Datetime.now();
		sfr.XML_String__c = xmlString;
		insert sfr;
	}
}