/*
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-8-27
 * Description: 处理要同步的数据工具类
 */
public class Lepus_SyncUtil {
	public static Sobject querySobject(Id recordId, Schema.Sobjecttype sobjecttype){
		List<Sobject> sobjList = querySobjects(new Set<Id>{recordId}, sobjecttype);
		if(sobjList.size() > 0){
			return sobjList[0];
		} else {
			return null;
		}
	}
	
	public static List<Sobject> querySobjects(Set<Id> recordIds, Schema.Sobjecttype sobjecttype){
        Set<Id> sidSet = recordIds;
        String query;
        query = accessibleField(sobjecttype);
        query += (' Where Id IN: sidSet all rows');
        List<sObject> sobjList = Database.query(query);
        return sobjList;
    }
    
    //根据ID集合获取ID 记录的Map，用于团队成员拼接
    public static Map<Id, sObject> querySobjectsToMap(Set<Id> idSet, Schema.Sobjecttype sobjecttype){
        List<sObject> list_Obj = querySobjects(idSet, sobjecttype);
        Map<Id, sObject> map_Obj = new Map<Id, sObject>();
        for(sObject so : list_Obj){
            map_Obj.put(so.Id, so);
        }
        return map_Obj;
    }
    
    //根据对象拼接全字段的查询语句
    private static String accessibleField(Schema.Sobjecttype objType){
    	// 组合字段无法查询，排除掉
    	Set<String> compositeFields = new Set<String>{'billingaddress','shippingaddress','address'};
        
        String sQuery = 'Select ';
        
        Map<String, Schema.SobjectField> map_fields = new Map<String, Schema.SobjectField>();
        
        //根据对象类型，获取字段集合
        map_fields = objType.getDescribe().fields.getMap();
        
        //拼接字段到SOQL语句
        for(String sFieldApiName : map_fields.keySet()){
            // 排除组合字段
            if(compositeFields.contains(sFieldApiName)){
            	continue;
            }
            sQuery += sFieldApiName ;
            sQuery += ',';
        }
        
        sQuery += 'LastModifiedBy.Employee_ID__c,';
        sQuery += 'LastModifiedBy.W3ACCOUNT__c,';
        sQuery += 'LastModifiedBy.FullName__c,';
        sQuery += 'LastModifiedBy.EnglishName__c,';
        sQuery += 'CreatedBy.Employee_ID__c,';
        sQuery += 'CreatedBy.W3ACCOUNT__c,';
        sQuery += 'CreatedBy.FullName__c,';
        sQuery += 'CreatedBy.EnglishName__c,';
        
        //添加关系类型的查找，比如account.name。todo..
        if(objType != User.sobjecttype){
        	sQuery += 'RecordType.DeveloperName,' ;
        } else {
        	sQuery += 'Profile.Name,';
        	sQuery += 'UserRole.Name,';
        }
        
        // Lead的Owner关联到User或者Group，需另外单独查询， User无Owner
        if(objType != Lead.sobjecttype && objType != User.sobjecttype){
        	sQuery += 'Owner.Employee_ID__c,' ;
        	sQuery += 'Owner.W3ACCOUNT__c,' ;
        	sQuery += 'Owner.FullName__c,';
        }
        
        if(objType == Opportunity.sobjecttype){
            sQuery += 'Account.Name,' ;
            sQuery += '(Select p.Id, p.Sales_Price__c, p.Quantity__c, p.Lookup__r.Name, p.Lookup__c, p.Project_Name__c From Project_Product__r p Where IsDeleted = false),';
        } else if(objType == Account.sobjecttype){
            sQuery += 'Parent.Name,' ;
        } else if(objType == Lead.sobjecttype){
        	sQuery += 'Account_Name__r.Name,';
        	sQuery += 'China_Opportunity__r.Name,';
        	sQuery += 'China_Opportunity__r.Global_ID__c,';
        	sQuery += 'ConvertedOpportunity.Name,';
        	sQuery += 'ConvertedOpportunity.Global_ID__c,';
        } else if(objType == Contact.sobjecttype){
        	sQuery += 'Account.Name,';
        }
        sQuery = sQuery.substring(0, sQuery.length()-1 );
        sQuery += ' From '+objType.getDescribe().getName();
        return sQuery;
    }
    
    // 查询LeadHistory
    public static list<LeadHistory> queryLeadHistories(List<Id> leadIds){
    	list<Lead> leadList = querySobjects(new Set<Id>(leadIds), Lead.Sobjecttype);
    	map<Id, Datetime> lastModifiedDateMap = new map<Id,Datetime>();
    	
    	// 最早的lastmodifieddate
    	Datetime lastModifiedTime;
    	for(Lead l : leadList){
    		if(lastModifiedTime == null || lastModifiedTime > l.LastModifiedDate){
    			lastModifiedTime = l.LastModifiedDate;
    		}
    		
    		lastModifiedDateMap.put(l.Id, l.LastModifiedDate);
    	}
    	
		list<LeadHistory> lhList = [Select OldValue, NewValue, LeadId, IsDeleted, Id, Field, CreatedDate, CreatedById, CreatedBy.Name, 
										 Lead.Global_ID__c, Lead.Lead_Code__c From LeadHistory 
    									Where LeadId IN: leadIds And CreatedDate >=: lastModifiedTime And Field = 'Status'];
    	
    	list<LeadHistory> syncLeadHistoryList = new list<LeadHistory>();
    	
    	for(LeadHistory lh : lhList){
    		if(lh.CreatedDate >= lastModifiedDateMap.get(lh.LeadId)){
    			syncLeadHistoryList.add(lh);
    		}
    	}
    	
    	return syncLeadHistoryList;
    }
    
    // 查询OpportunityFieldHistory
    public static List<OpportunityFieldHistory> queryOppFieldHistories(ID oppId , Datetime dTime){
        List<OpportunityFieldHistory> list_oppFieldHis = new List<OpportunityFieldHistory>();
        Datetime dt;
        for(OpportunityFieldHistory ofh : [Select o.OpportunityId, o.OldValue, o.NewValue, o.IsDeleted, Opportunity.LastModifiedBy.W3ACCOUNT__c, Opportunity.LastModifiedBy.Employee_ID__c,
                o.Id, o.Field, o.CreatedDate, o.CreatedById, o.CreatedBy.Name, o.Opportunity.Global_ID__c, Opportunity.LastModifiedDate
                From OpportunityFieldHistory o Where OpportunityId =: oppId And CreatedDate >=: dTime And Field != null And Field != 'created' And Field != 'To_Be_Deleted__c' order by CreatedDate]){
        	dt = (dt==null?ofh.CreatedDate:dt);
        	if(ofh.CreatedDate==dt){
        		list_oppFieldHis.add(ofh);
        	}
        }
        return list_oppFieldHis;
    }
    
    public static Schema.Sobjecttype retrieveSobjectType(String objType){
    	Schema.Sobjecttype sobjecttype;
    	if(objType.toLowerCase().equals('account')){
    		sobjecttype = Account.Sobjecttype;
    	} else if( objType.toLowerCase().equals('opportunity')){
    		sobjecttype = Opportunity.Sobjecttype;
    	} else if( objType.toLowerCase().equals('contact')){
    		sobjecttype = Contact.Sobjecttype;
    	} else if( objType.toLowerCase().equals('lead')){
    		sobjecttype = Lead.Sobjecttype;
    	} else if( objType.toLowerCase().equals('user')){
    		sobjecttype = User.Sobjecttype;
    	}
    	return sobjecttype;
    }
    
    /**
     * 同步多条入Lepus_Queue
     **/
    public static void initQueue(map<Id, sobject> sobjMap, list<Id> recordIds, String action, String syncType, Datetime syncTime){
    	Lepus_QueueManager queueManager = new Lepus_QueueManager();
		List<Lepus_Queue__c> list_queue = new List<Lepus_Queue__c>();
		//初始化队列信息
		for(Id recordId : recordIds){
			Lepus_Queue__c queue = queueManager.initQueue((String)sobjMap.get(recordId).get('Global_ID__c'),recordId,action,syncType,syncTime);
			list_queue.add(queue);
		}
		//插入队列
		queueManager.EnQueue(list_queue);
    }
    
    public static void initQueue(list<Id> recordIds, map<Id, String> idGlobalIdMap, String action, String syncType, Datetime syncTime){
    	Lepus_QueueManager queueManager = new Lepus_QueueManager();
		List<Lepus_Queue__c> list_queue = new List<Lepus_Queue__c>();
		//初始化队列信息
		for(Id recordId : recordIds){
			Lepus_Queue__c queue = queueManager.initQueue(idGlobalIdMap.get(recordId),recordId,action,syncType,syncTime);
			list_queue.add(queue);
		}
		//插入队列
		queueManager.EnQueue(list_queue);
    }
    
    /**
     * 获取TeamMember
     **/
    public static Map<Id, List<Lepus_EIPMember>> retrieveTeamMembers(List<Sobject> sobjList, Schema.Sobjecttype sobjecttype){
      Map<Id, List<Lepus_EIPMember>> memberMap = new Map<Id, List<Lepus_EIPMember>>();
      
      for(Sobject sobj : sobjList){
         List<Lepus_EIPMember> memberList = new List<Lepus_EIPMember>();
         List<Lepus_EIPMember> ownerMemberList = retrieveOwnerTeamMembers(sobj, sobjecttype);
         memberList.addAll(ownerMemberList);
         
         if(sobjecttype == Opportunity.sobjecttype){
         	List<Lepus_EIPMember> opportunityMemberList = retrieveOpportunityTeamMembers((Opportunity)sobj);
         	memberList.addAll(opportunityMemberList);
         } else if(sobjecttype == Account.sobjecttype){
         	List<Lepus_EIPMember> accountMemberList = retrieveAccountTeamMembers((Account)sobj);
         	memberList.addAll(accountMemberList);
         }
         
         memberMap.put((Id)sobj.get('ID'), memberList);
      }
      return memberMap;
   }
   
   private static List<Lepus_EIPMember> retrieveOwnerTeamMembers(Sobject sobj, Schema.Sobjecttype sobjecttype){
   	  List<Lepus_EIPMember> memberList = new List<Lepus_EIPMember>();
   	  for(User u : [Select Id, Name, Employee_ID__c, Email, W3ACCOUNT__c, EnglishName__c, FullName__c From User Where Id =: (Id)sobj.get('OwnerId')]){
   	  	Lepus_EIPMember member = new Lepus_EIPMember();
   	  	member.role = 'owner';
   	  	member.userAcc = u.W3ACCOUNT__c;
   	  	member.userEmail = u.Email;
   	  	member.username = u.FullName__c;//u.EnglishName__c + ' ' + u.Employee_ID__c; 
   	  	member.userCustom1 = 'A';
   	  	
   	  	if(sobjecttype == Opportunity.sobjecttype){
   	  		member.wsOpportunityAccessLevel = 'Owner';
			member.wsOpportunityId = (Id)sobj.get('Id');
   	  	} else if(sobjecttype == Account.sobjecttype){
   	  		member.wsAccountAccessLevel = 'Owner';
			member.wsAccountId = (Id)sobj.get('Id');
   	  	}
   	  	
		//member.wsInsertDate = String.valueOf(otm.CreatedDate.getTime());
		member.wsIsDeleted = String.valueOf(false);
		//member.wsSystemModStamp = String.valueOf(otm.SystemModstamp.getTime());
		//member.wsUpdateDate = String.valueOf(otm.LastModifiedDate.getTime());
		member.wsUserId = u.Id;
		
   	  	memberList.add(member);
   	  }
   	  
      return memberList;
   }
   
   // 获取OpportunityTeamMember, for OpportunityTeamMember.trigger
	private static List<Lepus_EIPMember> retrieveOpportunityTeamMembers(Opportunity opp){
		List<Lepus_EIPMember> memberList = new List<Lepus_EIPMember>();
		for(OpportunityTeamMember otm : [Select OpportunityAccessLevel, CreatedDate, IsDeleted, LastModifiedDate, SystemModstamp, TeamMemberRole, OpportunityId, 
      			UserId, User.Name, User.Employee_ID__c, User.Email, User.W3ACCOUNT__c, User.EnglishName__c, User.FullName__c, User.UserRole.Name  From OpportunityTeamMember Where OpportunityId =: opp.Id]){
			Lepus_EIPMember member = new Lepus_EIPMember();
			//member.action = action;
			String roleName = otm.User.UserRole.Name;
			member.role = assignRole(roleName);
			
			member.userCustom1 = 'A';
			member.userCustom2 = convertMemberRole(roleName);
			
			member.userAcc = otm.User.W3ACCOUNT__c;
			member.userEmail = otm.User.Email;
			member.username = otm.User.FullName__c;//otm.User.EnglishName__c + ' ' + otm.User.Employee_Id__c;
			
			
			member.wsOpportunityAccessLevel = otm.OpportunityAccessLevel;
			member.wsOpportunityId = otm.OpportunityId;
			member.wsInsertDate = String.valueOf(otm.CreatedDate.getTime());
			member.wsIsDeleted = String.valueOf(otm.IsDeleted);
			member.wsSystemModStamp = String.valueOf(otm.SystemModstamp.getTime());
			member.wsUpdateDate = String.valueOf(otm.LastModifiedDate.getTime());
			member.wsUserId = otm.UserId;
			
			memberList.add(member);
		}
		
		return memberList;
	}
	
   // 获取AccountTeamMember
   private static List<Lepus_EIPMember> retrieveAccountTeamMembers(Account acc){
      List<Lepus_EIPMember> memberList = new List<Lepus_EIPMember>();
      for(AccountTeamMember atm : [Select AccountAccessLevel, CreatedDate, IsDeleted, LastModifiedDate, SystemModstamp, TeamMemberRole, 
										AccountId, UserId, User.FullName__c, User.Name, User.Employee_ID__c, User.Email, User.W3ACCOUNT__c, User.EnglishName__c, User.UserRole.Name  
										From AccountTeamMember Where AccountId =: acc.Id]){
         Lepus_EIPMember member = new Lepus_EIPMember();
         // member.action = action;
         member.userAcc = atm.User.W3ACCOUNT__c;
      	 member.username = atm.User.FullName__c;//atm.User.EnglishName__c + ' ' + atm.User.Employee_ID__c;
      	 member.userEmail = atm.User.Email;
      	 
      	 String roleName = atm.User.UserRole.Name;
         member.role = assignRole(roleName);
         
         member.userCustom1 = 'A';
         member.userCustom2 = convertMemberRole(roleName);
         
         member.wsAccountAccessLevel = atm.AccountAccessLevel;
		 member.wsAccountId = atm.AccountId;
		 member.wsInsertDate = String.valueOf(atm.CreatedDate.getTime());
		 member.wsIsDeleted = String.valueOf(atm.IsDeleted);
		 member.wsSystemModStamp = String.valueOf(atm.SystemModstamp.getTime());
		 member.wsUpdateDate = String.valueOf(atm.LastModifiedDate.getTime());
		 member.wsUserId = atm.UserId;
         
         memberList.add(member);
      }
      
      return memberList;
   }
   
   // 转化Role
	private static String convertMemberRole(String roleName){
		String eipRole;
		if(roleName == null){
			return '';
		}
		
		if(roleName.contains('Account Manager')){
	  	 	eipRole = 'Account Responsible';
	  	 } else if (roleName.contains('Product Manager')){
	  	 	eipRole = 'Solution Responsible';
	  	 } else if (roleName.contains('Channel Manager')){
	  	 	eipRole = 'Channel Responsible';
	  	 } else if (roleName.contains('Service Manager')){
	  	 	eipRole = 'Fulfillment Responsible';
	  	 } else {
	  	 	eipRole = '';
	  	 }
	  	 
	  	 return eipRole;
	}
	
	private static String assignRole(String roleName){
		String role;
		
		if(roleName == null){
			return 'member';
		}
		
		if(roleName.contains('Account Manager') || roleName.contains('Product Manager') || roleName.contains('Channel Manager') || roleName.contains('Service Manager')){
			role = 'admin';
		} else {
			role = 'member';
		}
		
		return role;
	}
}