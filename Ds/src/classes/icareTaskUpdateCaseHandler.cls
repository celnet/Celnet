/*
*@Purpose:逻辑：当新建、更新时，根据iCare Task的Type值，对问题单Case中的以下字段赋值

含义	Case字段	取值
二线开始工作时间	PSE_Actual_Start_Time__c	Type为SR Escalate to L2的iCare Task的Actual_Start_Date__c
二线结束时间	HPE_Actual_Time__c	Type为SR Escalate to L1的iCare Task的实际完成时间Actual_End_Date__c
三线开始工作时间	R_D_Start_Time__c	Type为SR Escalate to L3的iCare Task的Actual_Start_Date__c
三线结束时间	R_D_Actual_Time__c	Type为SR Escalate to L3的iCare Task的Actual_End_Date__c


iCare Task的Type字段可取值如下：
SR Escalate to L2
SR Escalate to L3
Collect Information
Customer Suspending SR From L1
Customer Suspending SR From L2
Customer Suspending SR From L3

当前处理工程师取值逻辑：
1、如果当前状态为L2-Work in Progress或L2-Solution Review，取L2 task的assignee
2、如果当前状态为L3-Work in Progress，取L3 task的assignee（RPMM)
3、否则为SR owner  （该条通过workflow，根据status来实现)

*@Author:Brain Zhang
*Create Date：2013-9
*/
public without sharing class icareTaskUpdateCaseHandler implements Triggers.Handler{
	public void handle(){
		List<id> caseIds = new List<id>();
		for(iCare_Task__c t : (List<iCare_Task__c>)Trigger.new){
			caseIds.add(t.Service_Request__c);
		}
		Map<id,Case> caseMap = New Map<id,Case>([select Id,Status,Handling_Engineer__c from Case where Id in : caseIds]);
		Map<id,Case> toUpdateMap = new Map<id,Case> ();
		for(iCare_Task__c t : (List<iCare_Task__c>)Trigger.new){
			Case c = caseMap.get(t.Service_Request__c);
			Boolean hasChanged = false;
			if(t.Type__c != null && t.Type__c.trim() == 'SR Escalate to L2'){//Type为SR Escalate to L2
					hasChanged = true;//需要更新
					//二线开始工作时间	PSE_Actual_Start_Time__c	Type为SR Escalate to L2的iCare Task的Actual_Start_Date__c
					c.PSE_Actual_Start_Time__c = t.Actual_Start_Date__c;
					c.HPE_Actual_Time__c = t.Actual_End_Date__c;
					c.PSE_Planned_Start_Time__c = t.Planned_Start_Date__c;
					c.PSE_Planned_End_Time__c = t.Planned_End_Date__c;
					c.iCare_L1_To_L2_Time__c = t.iCare_Creation_Date__c;
					//根据问题单的状态Status和iCareTask的工程师Assignee来获取当前处理工程师Handling_Engineer__c
					if(c.Status =='L2-Work in Progress' || c.Status == 'L2-Solution Review')
					{
						c.Handling_Engineer__c = t.Assignee__c;
					}
					
				}else if(t.Type__c != null && t.Type__c.trim() == 'SR Escalate to L3'){//Type为SR Escalate to L3
					hasChanged = true;//需要更新
					//三线开始工作时间	R_D_Start_Time__c	Type为SR Escalate to L3的iCare Task的Actual_Start_Date__c
					c.R_D_Start_Time__c = t.Actual_Start_Date__c;
					
					//三线结束时间	R_D_Actual_Time__c	Type为SR Escalate to L3的iCare Task的Actual_End_Date__c
					c.R_D_Actual_Time__c = t.Actual_End_Date__c;
					c.RD_Planned_Start_Time__c = t.Planned_Start_Date__c;
					c.RD_Planned_End_Time__c = t.Planned_End_Date__c;
					c.iCare_L2_To_L3_Time__c = t.iCare_Creation_Date__c;
					
					if(c.Status =='L3-Work In Progress')
					{
						c.Handling_Engineer__c = t.Assignee__c;
					}
					
				}
				if(hasChanged){
					toUpdateMap.put(c.id,c);
				}
		}
		if(!toUpdateMap.isEmpty()){//不为空时,更新相关case
			try{
				update toUpdateMap.Values();
			}catch(Exception e){//improve the exception handling
			
			}
		}
	}

}