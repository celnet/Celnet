public without sharing class ContactDelete implements Triggers.Handler{
	public void handle(){
		set <ID> ContactId = new set<ID>();	
		Integer sfp =0;
		Integer wo =0;
		Integer contract =0;		
		if(trigger.isDelete){
			for(Contact contact :(list<Contact>)trigger.old){						
				//ContactId.add(contact.id);
				sfp=[select count() from SFP__c where Contact_Person__c=:contact.id];
				wo=[select count() from Work_Order__c where Contact_Person__c=:contact.id or Engineer_In_Charge__c=:contact.id];
				contract=[select count() from CSS_Contract__c where Contact_Person__c=:contact.id];
				if(sfp>0||wo>0||contract>0){
					contact.addError('该联系人已经关联服务合同/SFP/工单,不允许删除');
				}
			}
		}		
	}
}