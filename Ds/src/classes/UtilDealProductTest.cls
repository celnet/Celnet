@isTest
private class UtilDealProductTest {
  static testMethod void myUnitTest() {
    Deal_Registration__c deal = new Deal_Registration__c();
    deal.Name = 'testUtilDealProduct01';
    insert deal;
        
    Product_HS__c ps1 = new Product_HS__c();
    ps1.NAME = 'test企业IT';
    ps1.PRODUCT_LEVEL__C = '1';
    ps1.Level_1__c = 'test企业IT';
    insert ps1;
    
    Product_HS__c ps2 = new Product_HS__c();
    ps2.NAME = 'test企业网络';
    ps2.PRODUCT_LEVEL__C = '1';
    ps2.Level_1__c = 'test企业网络';
    insert ps2;
    
    Product_HS__c ps3 = new Product_HS__c();
    ps3.NAME = 'testUC&C';
    ps3.PRODUCT_LEVEL__C = '1';
    ps3.Level_1__c = 'testUC&C';
    insert ps3;

    Deal_Registration_Product__c product1 = new Deal_Registration_Product__c (
      Deal_Registration__c = deal.id, Lookup_Product_Master__c = ps1.Id);
    insert product1;

    Deal_Registration_Product__c product2 = new Deal_Registration_Product__c (
      Deal_Registration__c = deal.id, Lookup_Product_Master__c = ps2.Id);
    insert product2;
    
    product2.Lookup_Product_Master__c = ps3.Id;
    update(product2);
    
    delete product1;
  }
}