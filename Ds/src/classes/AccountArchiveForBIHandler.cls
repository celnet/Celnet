/**
 * @Purpose : 删除后将记录保存到数据维护历史对象上
 * @Author : kejun 20140609
 * @Date : 2014-06-09 update by Steven
 */
public with sharing class AccountArchiveForBIHandler implements Triggers.Handler{
    public static Boolean isFirstRun = true;
    public void handle(){
        if(AccountArchiveForBIHandler.isFirstRun){
            List<Data_Maintain_History__c> archiveList = new List<Data_Maintain_History__c>();
            for(Account t : (List<Account>)trigger.old){
                if(t.RecordTypeId == CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE){
                    Data_Maintain_History__c dmh = new Data_Maintain_History__c();
                    dmh.Record_ID__c = t.Id;
                    dmh.Operation_Type__c = 'Delete';
                    dmh.Data_Type__c = 'China Account';
                    archiveList.add(dmh);
                    /*
                    List<AccountTeamMember> atmList = new List<AccountTeamMember>();
                    atmList = [Select Id From AccountTeamMember Where AccountId =: t.Id ALL ROWS];
                    for(AccountTeamMember atm : atmList){
                        Data_Maintain_History__c dmh1 = new Data_Maintain_History__c();
                        dmh1.Record_ID__c = atm.Id;
                        dmh1.Operation_Type__c = 'Delete';
                        dmh1.Data_Type__c = 'China Account Team';
                        archiveList.add(dmh1);
                    } 
                    */
                }
                if(t.RecordTypeId == CONSTANTS.HUAWEIOVERSEACUSTOMERRECORDTYPE){
                    Data_Maintain_History__c dmh = new Data_Maintain_History__c();
                    dmh.Record_ID__c = t.Id;
                    dmh.Operation_Type__c = 'Delete';
                    dmh.Data_Type__c = 'Oversea Account';
                    archiveList.add(dmh);
                }
            }
            if(archiveList.size() > 0){
                insert archiveList;
            }
            AccountArchiveForBIHandler.isFirstRun = false;
        }
    }
}