/*
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-5-12
 * Description: mass edit order and forecast
 */
public class MassMaOrderForecastExtension {
    // sales meeting info
    private Id salesMeetingId;
    public String salesMeetingOffice{get;set;}
    public String salesMeetingName{get;set;}
    private boolean isImportantRegion;
    
    // order and forecast
    private List<Order_And_Forecast__c> orderAndForecastList;
    public List<Order_And_Forecast__c> productOafList{get;set;}
    public List<Order_And_Forecast__c> industryOafList{get;set;}

    // picklist value
    private List<String> industryList;
    private List<String> productList;

    public MassMaOrderForecastExtension(ApexPages.StandardSetController setCon) {
        this.salesMeetingId = ApexPages.currentPage().getParameters().get('id');
        if(this.salesMeetingId != null) {
            this.retrieveSalesmeetingInfo();
            this.retrieveOrderAndForecast();
            this.retrievePicklistValue();
            this.initOrderAndForecast();
        }
    }
    
    private void retrieveSalesmeetingInfo(){
        Sales_Meeting__c sm = [Select Id,Name,Representative_Office__c From Sales_Meeting__c Where Id =: this.salesMeetingId];
        this.salesMeetingOffice = sm.Representative_Office__c;
        this.salesMeetingName = sm.Name;
        if(salesMeetingOffice == '北京代表处' || salesMeetingOffice == '广州代表处' || salesMeetingOffice == '杭州代表处') {
            this.isImportantRegion = true;
        } else {
            this.isImportantRegion = false;
        }
    }
    
    private void retrieveOrderAndForecast(){
        this.orderAndForecastList = new List<Order_And_Forecast__c>();
        this.orderAndForecastList = 
            [SELECT Name,Id,Sales_Meeting__c,RecordTypeId,RecordType.Name,Sales_Meeting__r.Name,Industry__c, 
                            Sales_Meeting__r.Representative_Office__c, Is_Important_Region__c,Unique_Name__c,
                            Product__c,Forecast_First_Month__c,Forecast_Next_Quarter__c,Forecast_Second_Month__c,
                            Forecast_Third_Month__c,Forecast_this_quarter__c,Forecast_This_Quarter_HQ_Distribute__c, Q1_Target__c, H1_Target__c, Annual_Target__c
                 FROM Order_And_Forecast__c 
                Where Sales_Meeting__c =: this.salesMeetingId];
    }
    
    private void retrievePicklistValue(){
        // 产品列表
        this.productList = new List<String>();
        Schema.Describefieldresult productFieldResult = Order_And_Forecast__c.Product__c.getDescribe();
        List<Schema.Picklistentry> pv = productFieldResult.getPicklistValues();
        for(Schema.Picklistentry p : pv){
            this.productList.add(p.getValue());
        }
        
        // 行业列表
        this.industryList = new List<String>();
        Schema.Describefieldresult industryFieldResult = Order_And_Forecast__c.Industry__c.getDescribe();
        List<Schema.Picklistentry> iv = industryFieldResult.getPicklistValues();
        for(Schema.Picklistentry i : iv){
            // 移除不需要的picklist值
            if(this.isImportantRegion){
                if(i.getValue() != '媒体与资讯系统部'){
                    this.industryList.add(i.getValue());
                }
            } else {
                if(i.getValue() != '媒资（除三大ISP）' && i.getValue() != '媒资（三大ISP）'){
                    this.industryList.add(i.getValue());
                }
            }
        }
    }
    
    /**
     * 初始化待显示的产品和行业订货列表
     */
    private void initOrderAndForecast(){
        this.industryOafList = new List<Order_And_Forecast__c>();
        this.productOafList = new List<Order_And_Forecast__c>();
        
        // 生成所有记录
        Map<String, Order_And_Forecast__c> productOafMap = new Map<String, Order_And_Forecast__c>();
        for(String str : this.productList){
            productOafMap.put(str, this.addProductLine(str));
        }
        Map<String, Order_And_Forecast__c> industryOafMap = new Map<String, Order_And_Forecast__c>();
        for(String str : this.industryList){
            industryOafMap.put(str, this.addIndustryLine(str));
        }
        
        // 用系统已有记录替换对应的记录
        for(Order_And_Forecast__c oaf : this.orderAndForecastList){
            if(oaf.RecordType.Name == 'Order And Forecast(Industry)'){
                industryOafMap.put(oaf.Industry__c, this.avoidNull(oaf));
            } else if(oaf.RecordType.Name == 'Order And Forecast(Product)'){
                productOafMap.put(oaf.Product__c, this.avoidNull(oaf));
            }
        }
        
        // 按照picklist顺序显示
        for(String str : this.industryList){
            this.industryOafList.add(industryOafMap.get(str));
        }
        for(String str : this.productList){
            this.productOafList.add(productOafMap.get(str));
        }
    }
    
    private Order_And_Forecast__c avoidNull(Order_And_Forecast__c oaf){
    	oaf.Forecast_Next_Quarter__c = oaf.Forecast_Next_Quarter__c == null?0:oaf.Forecast_Next_Quarter__c;
        oaf.Forecast_this_quarter__c = oaf.Forecast_this_quarter__c == null?0:oaf.Forecast_this_quarter__c;
        oaf.Forecast_This_Quarter_HQ_Distribute__c = oaf.Forecast_This_Quarter_HQ_Distribute__c == null?0:oaf.Forecast_This_Quarter_HQ_Distribute__c;
        oaf.Forecast_First_Month__c = oaf.Forecast_First_Month__c == null?0:oaf.Forecast_First_Month__c;
        oaf.Forecast_Second_Month__c = oaf.Forecast_Second_Month__c == null?0:oaf.Forecast_Second_Month__c;
        oaf.Forecast_Third_Month__c = oaf.Forecast_Third_Month__c == null?0:oaf.Forecast_Third_Month__c;
        oaf.Q1_Target__c = oaf.Q1_Target__c == null?0:oaf.Q1_Target__c;
        oaf.H1_Target__c = oaf.H1_Target__c == null?0:oaf.H1_Target__c;
        oaf.Annual_Target__c = oaf.Annual_Target__c == null?0:oaf.Annual_Target__c;
    	return oaf;
    }
    
    private Order_And_Forecast__c addProductLine(String pName) {
        Order_And_Forecast__c oaf = new Order_And_Forecast__c();
        oaf.RecordTypeId = CONSTANTS.SALESMEETINGORDERPRODUCT;
        oaf.Sales_Meeting__c = salesMeetingId;
        oaf.Forecast_Next_Quarter__c = 0;
        oaf.Forecast_this_quarter__c = 0;
        oaf.Forecast_This_Quarter_HQ_Distribute__c = 0;
        oaf.Q1_Target__c = 0;
        oaf.H1_Target__c = 0;
        oaf.Annual_Target__c = 0;
        oaf.Product__c = pName;
        return oaf;
    }
    
    private Order_And_Forecast__c addIndustryLine(String iName) {
        Order_And_Forecast__c oaf = new Order_And_Forecast__c();
        oaf.RecordTypeId = CONSTANTS.SALESMEETINGORDERINDUSTRY;
        oaf.Sales_Meeting__c = salesMeetingId;
        oaf.Is_Important_Region__c = this.isImportantRegion?'是':'否';
        oaf.Forecast_First_Month__c = 0;
        oaf.Forecast_Next_Quarter__c = 0;
        oaf.Forecast_Second_Month__c = 0;
        oaf.Forecast_Third_Month__c = 0;
        oaf.Forecast_This_Quarter_HQ_Distribute__c = 0;
        oaf.Q1_Target__c = 0;
        oaf.H1_Target__c = 0;
        oaf.Annual_Target__c = 0;
        oaf.Industry__c = iName;
        return oaf;
    }

    /**
     * 保存所有的订货预测行记录（用于编辑界面点击保存按钮）
     */
    public PageReference saveOrderForecast() {
        List<Order_And_Forecast__c> orderForecastListSave = new List<Order_And_Forecast__c>();
        try {
            for(Order_And_Forecast__c ioaf : this.industryOafList) orderForecastListSave.add(ioaf);
            for(Order_And_Forecast__c poaf : this.productOafList) orderForecastListSave.add(poaf);
            if(orderForecastListSave.size()>0) {
                upsert orderForecastListSave;
            }
            PageReference pr = new PageReference('/' + this.salesMeetingId);
            pr.setRedirect(true);
            return pr;
        } catch(Exception e) {
            ApexPages.addMessages(e);
            return null;
        }
    }

    /**
     * 返回销售例会明细界面
     */
    public PageReference cancel() {
        PageReference pr = new PageReference('/' + this.salesMeetingId);
        pr.setRedirect(true);
        return pr;
    }
}