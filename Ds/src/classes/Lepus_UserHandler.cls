/**
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-8-29
 * Description: 同步用户角色到EIP
 */
public class Lepus_UserHandler implements Triggers.Handler{
	public static boolean isFirstRun = true;
	public void handle(){
		if(Lepus_UserHandler.isFirstRun){
			isFirstRun = false;
			
			List<User> userList = Lepus_HandlerUtil.retrieveRecordList();
			List<Id> userIdList = new List<Id>();
			for(User u : userList){
				userIdList.add(u.Id);
			}
			
			
			if((Lepus_Data_Sync_Controller__c.getInstance('用户角色') != null) && Lepus_Data_Sync_Controller__c.getInstance('用户角色').IsSync__c){
				if(trigger.isInsert){
					Lepus_FutureCallout.syncUser(userIdList, 'insert');
				} else if(trigger.isUpdate){
					List<Id> syncUserIdList = new List<Id>();
					for(User u : userList){
						// 下面字段更新后，同步推送更新
						if((trigger.newMap.get(u.Id)).get('ProfileId') != (trigger.oldMap.get(u.Id)).get('ProfileId')
							||(trigger.newMap.get(u.Id)).get('UserRoleId') != (trigger.oldMap.get(u.Id)).get('UserRoleId')
							||(trigger.newMap.get(u.Id)).get('IsActive') != (trigger.oldMap.get(u.Id)).get('IsActive')
							||(trigger.newMap.get(u.Id)).get('REGION_Office__c') != (trigger.oldMap.get(u.Id)).get('REGION_Office__c')
							||(trigger.newMap.get(u.Id)).get('Country__c') != (trigger.oldMap.get(u.Id)).get('Country__c')
							||(trigger.newMap.get(u.Id)).get('W3ACCOUNT__c') != (trigger.oldMap.get(u.Id)).get('W3ACCOUNT__c')
							||(trigger.newMap.get(u.Id)).get('FederationIdentifier') != (trigger.oldMap.get(u.Id)).get('FederationIdentifier')
							||(trigger.newMap.get(u.Id)).get('Employee_ID__c') != (trigger.oldMap.get(u.Id)).get('Employee_ID__c')
							||(trigger.newMap.get(u.Id)).get('EnglishName__c') != (trigger.oldMap.get(u.Id)).get('EnglishName__c')
							||(trigger.newMap.get(u.Id)).get('FullName__c') != (trigger.oldMap.get(u.Id)).get('FullName__c')){
							syncUserIdList.add(u.Id);
						}
					}
					if(syncUserIdList.size() > 0){
						Lepus_FutureCallout.syncUser(syncUserIdList, 'update');
					}
				}
			}
		}
	}
}