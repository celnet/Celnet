public without sharing class LeadAssignNAOwnerHandler implements Triggers.Handler{//before insert
	public void handle() {
		/*
		
		(Huawei China)MKT管理员
(Huawei China)MKT营销经理
(Huawei China)销售管理策略经理
(Huawei China)销售管理策略主管
  创建记录时需要做分发 .
  如果是MKT用户创建记录,并且线索来源为空,自动设置来源为展会
		
		*/
		Profile currentUserProfile = [select id,Name from Profile where id = :Userinfo.getProfileId()];
		if(!currentUserProfile.name.contains('MKT') && (!currentUserProfile.name.contains('销售管理策略'))){
			return;
		}
		
		//MKT用户创建NA LEAD判断
		Set<String> regions = new Set<String>();
		for(Lead l : (List<Lead>)Trigger.new){
			if('Huawei_China_NA_Lead' == l.RecordTypeDeveloperName__c){//检查是否为NA LEAD
				regions.add(l.HW_China_Lead__c);//需要创建vr 来限制MKT创建NA LEAD时该字段必填
			}
		}
		List<Leads_Distribute_Map__c> regionMap = [select Leads_Owner__c,Region_Name__c , Leads_Owner__r.Email 
			from Leads_Distribute_Map__c where Region_Name__c in :regions];
		Map<String,Leads_Distribute_Map__c> ownerMap = new Map<String,Leads_Distribute_Map__c>();
			//region name,Leads_Distribute_Map__c
		for(Leads_Distribute_Map__c d : regionMap){
			ownerMap.put(d.Region_Name__c,d);
		}
		for(Lead l : (List<Lead>)Trigger.new){
			if('Huawei_China_NA_Lead' == l.RecordTypeDeveloperName__c){
				if(currentUserProfile.name.contains('MKT') && l.LeadSource == null){
					l.LeadSource = '展会';//MKT用户导入时,自动设置来源为展会
				}
				//是否要加入判断,如果lead对应的campaign__c上有值,那么设置来源为展会?
				if((ownerMap.get(l.HW_China_Lead__c) != null )&&
					( ownerMap.get(l.HW_China_Lead__c).Leads_Owner__c != null)){
					l.ownerId = ownerMap.get(l.HW_China_Lead__c).Leads_Owner__c;
					System.debug('current ownerid :------' + l.OwnerId);
				}else{
					l.addError('未查找到对应的负责人,请检查数据');
				}
			}
		}
		
		//邮件通知由LeadAssignmentNotification定期发送
	}
}