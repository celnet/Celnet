/*
* 控制Account的跳转,创建的是TCG,转到专门的vf页面.否则转回标准页面
* Steven 2013-12
*/
public with sharing class AccountRedirect {
	private ApexPages.StandardController controller;
	public String retURL {get; set;}
	public String rType {get; set;}
	public String cancelURL {get; set;}
	public String ent {get; set;}
	private String originURL {get;set;}

	public AccountRedirect(ApexPages.StandardController controller) {
		this.controller = controller;
		retURL = ApexPages.currentPage().getParameters().get('retURL');
		rType = ApexPages.currentPage().getParameters().get('RecordType');
		cancelURL = ApexPages.currentPage().getParameters().get('cancelURL');
		ent = ApexPages.currentPage().getParameters().get('ent');
		originURL = ApexPages.currentPage().getUrl();
	}
	
	public PageReference redirect() {
		PageReference returnURL;
		String TCGRecordTypeId = CONSTANTS.HUAWEICHINATCGRECORDTYPE;
		if(rType == TCGRecordTypeId.substring(0,15)) {
			returnURL = Page.ChinaNewTCG;  
		} else {
			returnURL = new PageReference('/001/e');
			returnURL.getParameters().put('nooverride', '1');
		}
		returnURL.getParameters().put('retURL', retURL);
		returnURL.getParameters().put('RecordType', rType);
		returnURL.getParameters().put('cancelURL', cancelURL);
		returnURL.getParameters().put('ent', ent);
		returnURL.setRedirect(true);
		return returnURL;
	}

}