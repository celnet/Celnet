/**
 * if the parentid field changed ,change the HQ_Parent_Account__c as well
 * Author           : BLS
 * Create Date      : 2013/3
 *     
	update :
	业务要求子客户的行业信息同父客户保持一致，因此父子客户挂靠完成的时候需要刷新子客户的行业信息
	steven 20130605
	update :
	业务要求子客户的“区域/系统部”字段继承父客户，父子客户挂靠完成的时候需要去刷新所有子客户的区域/系统部信息
	steven 20130729
 */ 
public without sharing class AccountUpdateRootHandler implements Triggers.Handler{//before update
	public static Boolean isFirstRun = true;
	public void handle() {
		if(AccountUpdateRootHandler.isFirstRun){
			for (Account acc :  (List<Account>)trigger.new){
        		if (acc.RecordTypeId == CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE){
           			//父客户发生变化 
					if (acc.ParentId != ((Account)trigger.OldMap.Get(acc.Id)).ParentId) {
						if (acc.ParentId == null){
							//父客户修改为空，修改根节点父客户为空
							acc.HQ_Parent_Account__c = null;
						}else{
							//修改根节点父客户
							acc.HQ_Parent_Account__c = UtilAccount.GetRootParentAccount(acc.parentId);
							UpdateAccountIndustryInfoByParent(acc); //add by steven 20130605
						}
						//所有子客户的根节点父客户要修改为本节点的根客户
						//此功能在AccountAfterUpdateHandler中实现,便于手动调整.如果需要在当前handler中处理,取消下面一行的注释即可.
				     	//UtilAccount.UpdateAllHQParentAccount(acc);
					}
				}
			}
		}
	}
	
	//add by steven 20130605 start
    //根据父客户的行业和客户群信息,更新当前account的行业和客户群信息
    private void UpdateAccountIndustryInfoByParent(Account current){
    	Account acc = [select id,name,industry,Sub_industry_HW__c,Customer_Group__c,Customer_Group_Code__c
    						 ,Region_HW__c,Representative_Office__c,Province__c,City_Huawei_China__c//add by steven 20130729
	    				 from Account where id =: current.ParentId];
 		current.industry = acc.industry;
 		current.Sub_industry_HW__c = acc.Sub_industry_HW__c;
 		current.Customer_Group__c = acc.Customer_Group__c;
 		current.Customer_Group_Code__c = acc.Customer_Group_Code__c;
 		
 		
 		current.Region_HW__c = acc.Region_HW__c;//add by steven 20130729
 		current.Representative_Office__c = acc.Representative_Office__c;//add by steven 20130729
 		if(acc.Representative_Office__c != '系统部总部') {
 			current.Province__c = acc.Province__c;
 		}
 		//current.City_Huawei_China__c = acc.City_Huawei_China__c;//add by steven 20130729
    }
    //add by steven 20130605 end
}