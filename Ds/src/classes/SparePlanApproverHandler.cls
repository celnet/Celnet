public without sharing class SparePlanApproverHandler implements Triggers.Handler{
	public void handle(){
			String sparePlanMapRecordTypeName ='Spare_Plan_Approver_Map';
			String mapType ='Spare Plan';
			String productLine ='';
			list<Spare_Plan_Approver_Maps__c> sparePlanApprover = new list<Spare_Plan_Approver_Maps__c> ();
			list<SFP_Product__c> sfpProduct = new list<SFP_Product__c>();
			for(Spare_Plan__c spc :(list<Spare_Plan__c>) trigger.new){
				if(spc.Status__c=='Waiting for Approval'&&((Spare_Plan__c)trigger.oldMap.get(spc.id)).Status__c!='Waiting for Approval'){					
					Id mapRecordTypeId = [select id from RecordType where DeveloperName =: sparePlanMapRecordTypeName].id;
					sfpProduct=[select Contract_Product_PO__r.Product_Family__c from SFP_Product__c where SFP__c = :spc.SFP__c limit 1];
					if(sfpProduct.size()>0){
						productLine =sfpProduct[0].Contract_Product_PO__r.Product_Family__c;
					}else productLine='';
					//productLine =[select Contract_Product_PO__r.Product_Family__c from SFP_Product__c where SFP__c = :spc.SFP__c limit 1].Contract_Product_PO__r.Product_Family__c;
					if(spc.Contract_Key_Accounts__c!=null&&spc.Contract_Key_Accounts__c!='非大客户'){
						sparePlanApprover =
						[select id,Rep_Office__c,Approver__c,Type__c from Spare_Plan_Approver_Maps__c
						where RecordTypeId= :mapRecordTypeId and Rep_Office__c= :spc.Contract_Key_Accounts__c
						and Type__c =:mapType];
					}else if(spc.Contract_Rep_Office__c!=null&&spc.Contract_Rep_Office__c!=''&&productLine!=null&&productLine!=''){
						sparePlanApprover =
						[select id,Rep_Office__c,Approver__c,Type__c from Spare_Plan_Approver_Maps__c
						where RecordTypeId= :mapRecordTypeId and Rep_Office__c= :spc.Contract_Rep_Office__c
						and Product_Line__c = :productLine and Type__c =:mapType];
					}else{
						sparePlanApprover =
						[select id,Rep_Office__c,Approver__c,Type__c from Spare_Plan_Approver_Maps__c
						where RecordTypeId= :mapRecordTypeId and Rep_Office__c= '机关'
						and Type__c =:mapType];
					}										
					if(sparePlanApprover.size()>0){
						User currentApprover =[Select IsActive, Name FROM User where Id = : sparePlanApprover[0].Approver__c];
						if(!currentApprover.IsActive){
							spc.addError('备件计划审批人 "' + currentApprover.Name + '" 失效，请联系管理员');
							return;
						}else{
							spc.Plan_Approver__c =sparePlanApprover[0].Approver__c;						
						}
					}else{
						spc.addError(spc.Contract_Rep_Office__c+spc.Contract_Key_Accounts__c + '没有维护备件计划审批人, 请联系管理员');
					}			
				}
			}
		}
}