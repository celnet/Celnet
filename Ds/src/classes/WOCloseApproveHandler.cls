public without sharing class WOCloseApproveHandler implements Triggers.Handler{
	public void handle(){				
		String woMapRecordTypeName ='Onsite_WO_Approver_Map';
		String mapType ='Work Order';
		String productLine ='';
		list <Spare_Plan_Approver_Maps__c> woApprover =new list<Spare_Plan_Approver_Maps__c>();//审批人的ID
		list <Work_Order_Product__c> woProduct = new list <Work_Order_Product__c>();
		for(Work_Order__c wo :(list<Work_Order__c>) trigger.new){
			if(!wo.RecordTypeDeveloperName__c.contains('HW_Int_CSS')&& !wo.RecordTypeDeveloperName__c.contains('Int')&&!wo.RecordTypeDeveloperName__c.contains('N_Exception')&&wo.Status__c=='Waiting for Approval'
				&&((Work_Order__c)trigger.oldMap.get(wo.id)).Status__c!='Waiting for Approval'){
				Id mapRecordTypeId = [select id from RecordType where DeveloperName =: woMapRecordTypeName].id;
			
			woProduct = [select Contract_Product_PO__r.Product_Family__c from Work_Order_Product__c
							where Work_Order__c = :wo.id limit 1];
			if(woProduct.size()> 0){
				productLine = woProduct[0].Contract_Product_PO__r.Product_Family__c;
			}else productLine='';
			if((wo.Office__c!=null&&wo.Office__c!='')&&(productLine!=''&&productLine!=null)){
				woApprover =
				[select id,Rep_Office__c,Approver__c,Type__c from Spare_Plan_Approver_Maps__c
				where RecordTypeId= :mapRecordTypeId and Rep_Office__c= :wo.Office__c
				and Product_Line__c =:productLine and Type__c =:mapType];
			}else if((wo.Office__c!=null&&wo.Office__c!='')&&(productLine==''||productLine==null)){
				woApprover =
				[select id,Rep_Office__c,Approver__c,Type__c from Spare_Plan_Approver_Maps__c
				where RecordTypeId= :mapRecordTypeId and Rep_Office__c= :wo.Office__c
				and Product_Line__c ='ALL' and Type__c =:mapType];
			}else if(wo.Office__c==null||wo.Office__c==''){
				woApprover =[select id,Rep_Office__c,Approver__c,Type__c from Spare_Plan_Approver_Maps__c
				where RecordTypeId= :mapRecordTypeId and Rep_Office__c= '机关'
				and Type__c =:mapType];
			}
			system.debug('hewei++++++'+productLine+wo.Office__c);
			if(woApprover.size()>0){
						User currentApprover =[Select IsActive, Name FROM User where Id = : woApprover[0].Approver__c];
						if(!currentApprover.IsActive){
							wo.addError(wo.Office__c+productLine+'的工单审批人 "' + currentApprover.Name + '" 失效，请联系管理员');
							return;
						}else{
							wo.Close_Approver__c =woApprover[0].Approver__c;						
						}
					}else{
						wo.addError(wo.Office__c +productLine+'没有维护工单审批人, 请联系管理员');
					}		
			}			
		}
	}
}