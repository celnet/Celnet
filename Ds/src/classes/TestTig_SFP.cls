/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestTig_SFP {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        CSS_Contract__c cc = new CSS_Contract__c(name='123456789012345678',
    		Contract_Signed_Date__c = DAte.today().addDays(-1),Frame_Contract_NO__c = '123456789012345678');
    		insert cc;
        SFP__c esfp = new SFP__c(Contract_No__c = cc.id);
        insert esfp;
       	List<SFP__c> sfplist=[select id,name,status__c from SFP__c where status__c != 'Cancelled' and status__c != 'Closed' limit 1];
    	
    	SFP__c sfp0=new SFP__c();
		sfp0=sfplist[0].clone(false); 
		insert(sfp0);
    	
    	for(SFP__c sfp : sfplist){
    		sfp.status__c= 'Cancelled';
    	}
    	
    	try{
    		update(sfplist);
    	}catch(Exception e){}

    }
}