global without sharing class LeadConfirmNotification implements Schedulable{
	public static String CRON_EXP = '0 0 22 * * ?';
   															//Seconds Minutes Hours Day_of_month Month Day_of_week 
   global void execute(SchedulableContext ctx) {
			List<Id> userIds = LeadConfirmNotification.queryUsers();
		  LeadConfirmNotification.sendEmail(userIds);
   }
   
   public static List<Id> queryUsers(){
   	AggregateResult[] groupedResults = [select ownerid,count(id) from lead 
				where NA_Lead_Confirmation__c = '未确认'
				 and RecordTypeDeveloperName__c = 'Huawei_China_NA_Lead' group by ownerid];
	    Map<id,Integer> numMap = new Map<id,Integer> ();//user id,owned lead nums
			for(AggregateResult gr : groupedResults){
				numMap.put((String)gr.get('ownerid'),(Integer)gr.get('expr0'));//parse aggregate result
			}
			List<Id> userIds = new List<Id>();
			for(Id i : numMap.keySet()){
				if(numMap.get(i) > 0){//该用户名下拥有未确认的NA线索,需要发邮件通知
					userIds.add(i);
				}
			}
			return userIds;
   } 
   
   public static void sendEmail(List<Id> userIds){
   	
			EmailTemplate et = [select id,developername from EmailTemplate where developername = 'UnConfirmed_NA_Lead'];
			Messaging.MassEmailMessage mail = new Messaging.MassEmailMessage();
			mail.setTargetObjectIds(userIds);
			mail.setTemplateID(et.id);
			Messaging.sendEmail(new Messaging.Email[] { mail } , false);
   }
      
}