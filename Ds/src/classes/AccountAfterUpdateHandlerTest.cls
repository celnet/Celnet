@isTest
private class AccountAfterUpdateHandlerTest {

    static testMethod void updateCustomerGroupCode() {
    	Account acc = UtilUnitTest.createHWChinaAccount('testacc');
    	acc.Region_HW__c = 'testr';
			acc.Representative_Office__c = 'testro';
			acc.Country_HW__c = 'testch';
			acc.Country_Code_HW__c = 'testcch';
			acc.Industry__c = 'testi';
			acc.Customer_Group__c = 'testcg';
			acc.Sub_industry_HW__c = 'testsih';
			acc.Industry = 'testi';
			acc.City_Huawei_China__c = 'testchc';
			acc.Customer_Group_Code__c = 'testcgc';
			acc.Province__c = 'testp';
			update acc;
			
			Opportunity opp = new Opportunity(accountid = acc.id,recordtypeid = CONSTANTS.CHINAOPPORTUNITYRECORDTYPE,closedate = Date.today(),
				name = 'testOpp' ,stagename = 'test');
				opp.Opportunity_type__c='Direct Sales';//it is added on 2013/05/25 
			insert opp;
			Opportunity opp2 = new Opportunity(accountid = acc.id,recordtypeid = CONSTANTS.CHINAOPPORTUNITYRECORDTYPE,closedate = Date.today(),
				name = 'testOpp' ,stagename = 'SSA (Huawei No Bid)',Lost_Reason__c  = 'test',Lost_Reason_Description__c = 'test', WinCom_Other__c = 'ts');
				opp2.Opportunity_type__c='Direct Sales';//it is added on 2013/05/25 
			insert opp2;
			
			Test.startTest();
			acc.Customer_Group_Code__c = 'code';
			update acc;
			Test.stopTest();
			opp= [select id,Customer_Group_Code__c from Opportunity where id = :opp.id];
			System.assertEquals('code',opp.Customer_Group_Code__c);
			opp2= [select id,Customer_Group_Code__c,IsClosed from Opportunity where id = :opp2.id];
			System.assertEquals('testcgc',opp2.Customer_Group_Code__c);
			
			acc.City_Huawei_China__c = 'city';
			update acc;
			opp= [select id,City__c from Opportunity where id = :opp.id];
			System.assertEquals('city',opp.City__c);
			opp2= [select id,City__c from Opportunity where id = :opp2.id];
			System.assertEquals('testchc',opp2.City__c);
			
			
    }
}