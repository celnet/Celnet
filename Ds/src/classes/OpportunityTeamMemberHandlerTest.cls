@isTest
private class OpportunityTeamMemberHandlerTest {
	static testMethod void myUnitTest() {
		Opportunity opp = OpportunityTeamMemberHandlerTest.createHWChinaOpportunity('TestChinaOpportunity');
		OpportunityTeamMember member = new OpportunityTeamMember();
		member.opportunityid = opp.id;
		member.UserId = Userinfo.getUserId();
		member.TeamMemberRole = 'test';
		insert member;
		delete member;
	}
	
	public static Opportunity createHWChinaOpportunity(String name) {
		Account acc = UtilUnitTest.createHWChinaAccount('TEST001');
		Opportunity opp = new Opportunity();
		opp.recordtypeid = CONSTANTS.CHINAOPPORTUNITYRECORDTYPE;
		opp.accountid = acc.id;
		opp.name = 'testOpp';
		opp.closedate = Date.today();
		opp.stagename = 'SSX (Developing Solution)';
		opp.Opportunity_type__c='Direct Sales';
		opp.Participate_Space__c = 1;
		opp.Project_Progress__c = 'test';
		insert opp;
		return opp;
	}
}