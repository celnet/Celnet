/*
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-8-27
 * Description: 处理触发trigger的数据的工具类
 */
public class Lepus_HandlerUtil {
	
	// 获取触发trigger的记录List
	public static List<Sobject> retrieveRecordList(){
		List<Sobject> recordList = new List<Sobject>();
		if(trigger.isInsert){
			recordList = trigger.new;
		} else if(trigger.isUpdate){
			recordList = trigger.new;
		} else if(trigger.isDelete){
			recordList = trigger.old;
		}
		return recordList;
	}
	
	// 获取触发的Action
	public static String retrieveAction(){
		String action;
		if(trigger.isInsert){
			action = 'insert';
		} else if(trigger.isUpdate){
			action = 'update';
		} else if(trigger.isDelete){
			action = 'delete';
		}
		return action;
	}
	
	// 过滤记录类型
	public static boolean filterRecordType(Sobject sobj, Schema.Sobjecttype sobjecttype){
		Id recordTypeId = (Id)sobj.get('RecordTypeId');
		Boolean isAccepted = false;
		if(sobjecttype == Opportunity.sobjecttype){
			isAccepted = (recordTypeId == CONSTANTS.CHINAOPPORTUNITYRECORDTYPE || recordTypeId == CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE || 
         					recordTypeId == CONSTANTS.HUAWEIOVERSEAOPPRECORDTYPE || recordTypeId == CONSTANTS.HUAWEIOVERSEADEALOPPRECORDTYPE);
		} else if(sobjecttype == Account.sobjecttype){
			isAccepted = (recordTypeId == CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE || recordTypeId == CONSTANTS.HUAWEIOVERSEACUSTOMERRECORDTYPE);
		} else if(sobjecttype == Contact.sobjecttype){
			isAccepted = (recordTypeId == CONSTANTS.HUAWEICHINACONTACTRECORDTYPE || recordTypeId == CONSTANTS.HUAWEIOVERSEACONTACTRECORDTYPE);
		} else if(sobjecttype == Lead.sobjecttype){
			isAccepted = (recordTypeId == CONSTANTS.CHINALEADRECORDTYPE || recordTypeId == CONSTANTS.OVERSEALEADRECORDTYPE);
		}
		
		return isAccepted;
	}
	
	public static String getRecordType(Sobject sobj, Schema.Sobjecttype sobjecttype){
		Id recordTypeId = (Id)sobj.get('RecordTypeId');
		String recordType = '';
		if(recordTypeId == CONSTANTS.CHINAOPPORTUNITYRECORDTYPE || recordTypeId == CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE
			|| recordTypeId == CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE
			|| recordTypeId == CONSTANTS.HUAWEICHINACONTACTRECORDTYPE
			|| recordTypeId == CONSTANTS.CHINALEADRECORDTYPE){
			recordType = '中国区';
		}
		
		if(recordTypeId == CONSTANTS.HUAWEIOVERSEAOPPRECORDTYPE || recordTypeId == CONSTANTS.HUAWEIOVERSEADEALOPPRECORDTYPE
			|| recordTypeId == CONSTANTS.HUAWEIOVERSEACUSTOMERRECORDTYPE
			|| recordTypeId == CONSTANTS.HUAWEIOVERSEACONTACTRECORDTYPE
			|| recordTypeId == CONSTANTS.OVERSEALEADRECORDTYPE){
			recordType = '海外';
		}
		
		return recordType;
	}
}