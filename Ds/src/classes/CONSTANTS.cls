/**
 * @Purpose : 保存所有的常用常量信息
 * @Author : Steven
 * @Date : 2014-08-20 Refactor
 */ 
public without sharing class CONSTANTS {
	public static final String CISENDPOINT = 'https://szxap03-in.huawei.com/public/accounts/accountEntry.jhtml';
	public static final String CISEMPTYSTRING = '-';
	public static final String CISEMPLOYEE= '00105534';
    
	//客户记录类型
	public static final String HUAWEICHINACUSTOMERRECORDTYPE = 
		Schema.SObjectType.Account.getRecordTypeInfosByName().get('Huawei China Customer') == null ? 
		Schema.SObjectType.Account.getRecordTypeInfosByName().get('中国区客户').getRecordTypeId() :
		Schema.SObjectType.Account.getRecordTypeInfosByName().get('Huawei China Customer').getRecordTypeId();
	public static final String HUAWEIOVERSEACUSTOMERRECORDTYPE = 
		Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
	public static final String HUAWEICHINATCGRECORDTYPE =
		Schema.SObjectType.Account.getRecordTypeInfosByName().get('Huawei China TCG') == null ? 
		Schema.SObjectType.Account.getRecordTypeInfosByName().get('中国区商业目标客户群').getRecordTypeId() :
		Schema.SObjectType.Account.getRecordTypeInfosByName().get('Huawei China TCG').getRecordTypeId();
	public static final String HUAWEICHINAPARTNERRECORDTYPE =
		Schema.SObjectType.Account.getRecordTypeInfosByName().get('Huawei China PRM Channel Partner') == null ? 
		Schema.SObjectType.Account.getRecordTypeInfosByName().get('中国区渠道合作伙伴').getRecordTypeId() :
		Schema.SObjectType.Account.getRecordTypeInfosByName().get('Huawei China PRM Channel Partner').getRecordTypeId();
        
	//线索记录类型
	public static final String CHINALEADRECORDTYPE = 
		Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Huawei China Lead') == null ? 
		Schema.SObjectType.Lead.getRecordTypeInfosByName().get('中国区线索').getRecordTypeId() :
		Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Huawei China Lead').getRecordTypeId();
	public static final String OVERSEALEADRECORDTYPE = 
		Schema.SObjectType.Lead.getRecordTypeInfosByName().get('HW Lead') == null ? 
		Schema.SObjectType.Lead.getRecordTypeInfosByName().get('海外线索').getRecordTypeId() :
		Schema.SObjectType.Lead.getRecordTypeInfosByName().get('HW Lead').getRecordTypeId();
	public static final String NALEADRECORDTYPE = 
		Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Huawei China NA Lead') == null ? 
		Schema.SObjectType.Lead.getRecordTypeInfosByName().get('中国区NA客户线索').getRecordTypeId() :
		Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Huawei China NA Lead').getRecordTypeId();
        
	//机会点记录类型
	public static final String CHINAOPPORTUNITYRECORDTYPE = 
		Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Huawei China Opportunity') == null ? 
		Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('中国区机会点').getRecordTypeId() :
		Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Huawei China Opportunity').getRecordTypeId();
	public static final String HUAWEICHINADEALOPPRECORDTYPE = 
		Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Huawei China Deal Registration') == null ? 
		Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('中国区项目报备').getRecordTypeId() :
		Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Huawei China Deal Registration').getRecordTypeId(); 
	public static final String HUAWEICHINASERVICEOPPRECORDTYPE = 
		Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Huawei China Service Opportunity') == null ? 
		Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('中国区服务机会点').getRecordTypeId() :
		Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Huawei China Service Opportunity').getRecordTypeId(); 
	public static final String HUAWEIOVERSEAOPPRECORDTYPE = 
		Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Enterprise Business Opportunity') == null ? 
		Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('海外机会点').getRecordTypeId() :
		Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Enterprise Business Opportunity').getRecordTypeId(); 
	public static final String HUAWEIOVERSEADEALOPPRECORDTYPE = 
		Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Enterprise Business Deal Registration') == null ? 
		Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('海外项目报备').getRecordTypeId() :
		Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Enterprise Business Deal Registration').getRecordTypeId(); 
	    
	//项目报备记录类型
	public static final String HUAWEICHINADEALRECORDTYPE = 
		Schema.SObjectType.Deal_Registration__c.getRecordTypeInfosByName().get('Huawei Deal Registration-China') == null ? 
		Schema.SObjectType.Deal_Registration__c.getRecordTypeInfosByName().get('中国区项目报备').getRecordTypeId() :
		Schema.SObjectType.Deal_Registration__c.getRecordTypeInfosByName().get('Huawei Deal Registration-China').getRecordTypeId();
	public static final String HUAWEIOVERSEADEALRECORDTYPE = 
		Schema.SObjectType.Deal_Registration__c.getRecordTypeInfosByName().get('Huawei Deal Registration-Oversea') == null ? 
		Schema.SObjectType.Deal_Registration__c.getRecordTypeInfosByName().get('海外项目报备').getRecordTypeId() :
		Schema.SObjectType.Deal_Registration__c.getRecordTypeInfosByName().get('Huawei Deal Registration-Oversea').getRecordTypeId();
    
	//联系人记录类型
	public static final String HUAWEICHINACONTACTRECORDTYPE = 
		Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Huawei China Contact') == null ? 
		Schema.SObjectType.Contact.getRecordTypeInfosByName().get('中国区客户联系人').getRecordTypeId() :
		Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Huawei China Contact').getRecordTypeId();
	public static final String HUAWEIOVERSEACONTACTRECORDTYPE = 
		Schema.SObjectType.Contact.getRecordTypeInfosByName().get('(HW) Contact') == null ? 
		Schema.SObjectType.Contact.getRecordTypeInfosByName().get('海外客户联系人').getRecordTypeId() :
		Schema.SObjectType.Contact.getRecordTypeInfosByName().get('(HW) Contact').getRecordTypeId();
    
	//审批人表记录类型
	public static final String APPROVERMAPCHINARECORDTYPE = 
		Schema.SObjectType.Account_Link_Approver_Map__c.getRecordTypeInfosByName().get('Huawei China Approver').getRecordTypeId(); 
	public static final String APPROVERMAPCAMPAIGNRECORDTYPE = 
		Schema.SObjectType.Account_Link_Approver_Map__c.getRecordTypeInfosByName().get('HW Campaign Approver').getRecordTypeId(); 
	public static final String APPROVERMAPOVERSEALEADSOWNERRECORDTYPE = 
		Schema.SObjectType.Account_Link_Approver_Map__c.getRecordTypeInfosByName().get('HW Oversea Leads Owner').getRecordTypeId(); 
        
	//销售目标记录类型
	public static final String SALESTARGETMANAGERECORDTYPE = 
		Schema.SObjectType.Sales_Target_Accomplishment__c.getRecordTypeInfosByName().get('China Account Manager') == null ? 
		Schema.SObjectType.Sales_Target_Accomplishment__c.getRecordTypeInfosByName().get('中国区客户经理NA销售目标').getRecordTypeId() :
		Schema.SObjectType.Sales_Target_Accomplishment__c.getRecordTypeInfosByName().get('China Account Manager').getRecordTypeId(); 
	public static final String SALESTARGETCHINANARECORDTYPE = 
		Schema.SObjectType.Sales_Target_Accomplishment__c.getRecordTypeInfosByName().get('China Named Account') == null ? 
		Schema.SObjectType.Sales_Target_Accomplishment__c.getRecordTypeInfosByName().get('中国区NA客户销售目标').getRecordTypeId() :
		Schema.SObjectType.Sales_Target_Accomplishment__c.getRecordTypeInfosByName().get('China Named Account').getRecordTypeId();
	public static final String SALESTARGETREGIONRECORDTYPE = 
		Schema.SObjectType.Sales_Target_Accomplishment__c.getRecordTypeInfosByName().get('China Representative Office') == null ? 
		Schema.SObjectType.Sales_Target_Accomplishment__c.getRecordTypeInfosByName().get('中国区客户经理销售目标').getRecordTypeId() :
		Schema.SObjectType.Sales_Target_Accomplishment__c.getRecordTypeInfosByName().get('China Representative Office').getRecordTypeId();
	public static final String SALESTARGETCHINATCGRECORDTYPE = 
		Schema.SObjectType.Sales_Target_Accomplishment__c.getRecordTypeInfosByName().get('China TCG') == null ? 
		Schema.SObjectType.Sales_Target_Accomplishment__c.getRecordTypeInfosByName().get('中国区TCG销售目标').getRecordTypeId() :
		Schema.SObjectType.Sales_Target_Accomplishment__c.getRecordTypeInfosByName().get('China TCG').getRecordTypeId();    

	//销售例会订收记录类型
	public static final String SALESMEETINGORDERPRODUCT =
		Schema.SObjectType.Order_And_Forecast__c.getRecordTypeInfosByName().get('Order And Forecast(Product)') == null ? 
		Schema.SObjectType.Order_And_Forecast__c.getRecordTypeInfosByName().get('订货及预测(产品)').getRecordTypeId() :
		Schema.SObjectType.Order_And_Forecast__c.getRecordTypeInfosByName().get('Order And Forecast(Product)').getRecordTypeId();
	public static final String SALESMEETINGORDERINDUSTRY =
		Schema.SObjectType.Order_And_Forecast__c.getRecordTypeInfosByName().get('Order And Forecast(Industry)') == null ? 
		Schema.SObjectType.Order_And_Forecast__c.getRecordTypeInfosByName().get('订货及预测(行业)').getRecordTypeId() :
		Schema.SObjectType.Order_And_Forecast__c.getRecordTypeInfosByName().get('Order And Forecast(Industry)').getRecordTypeId();
    public static final String SALESMEETINGINCOMEPRODUCT =
		Schema.SObjectType.Income_And_Forecast__c.getRecordTypeInfosByName().get('Income And Forecast Record Type(Product)') == null ? 
		Schema.SObjectType.Income_And_Forecast__c.getRecordTypeInfosByName().get('收入及预测(产品)').getRecordTypeId() :
		Schema.SObjectType.Income_And_Forecast__c.getRecordTypeInfosByName().get('Income And Forecast Record Type(Product)').getRecordTypeId();
	public static final String SALESMEETINGINCOMEINDUSTRY =
		Schema.SObjectType.Income_And_Forecast__c.getRecordTypeInfosByName().get('Income And Forecast Record Type(Industry)') == null ? 
		Schema.SObjectType.Income_And_Forecast__c.getRecordTypeInfosByName().get('收入及预测(行业)').getRecordTypeId() :
		Schema.SObjectType.Income_And_Forecast__c.getRecordTypeInfosByName().get('Income And Forecast Record Type(Industry)').getRecordTypeId();
 
	//市场活动记录类型
	public static final String OVERSEACAMPAIGNRECORDTYPE = 
		Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('HW Campaign') == null ? 
		Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('海外市场活动').getRecordTypeId() :
		Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('HW Campaign').getRecordTypeId();
		
	//中国区父子客户挂靠申请类型
	public static final String CHINAACCOUNTLINKCREATE =
		Schema.SObjectType.Account_Link_Request__c.getRecordTypeInfosByName().get('Account Link Create').getRecordTypeId();
	public static final String CHINAACCOUNTLINKREMOVE =
		Schema.SObjectType.Account_Link_Request__c.getRecordTypeInfosByName().get('Account Link Remove').getRecordTypeId();
			
	//审批状态
	public static final String CHINALEADSTATUSCONFIRM = '待确认';
	public static final String CHINALEADSTATUSFOLLOW = '跟进中';
	public static final String CHINALEADSTATUSASSIGN = '待分发';
	public static final String CHINALEADSTATUSINVALID = '已作废';
	public static final String CHINALEADSTATUSCLOSED = '已关闭';
        
}