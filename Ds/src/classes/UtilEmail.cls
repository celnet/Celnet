/*
* @Purpose : 邮件公用方法类
* @Author : steven
* @Date :		2013-11-15
*/
public without sharing class UtilEmail {
  /**
	 * 根据邮件模板发送邮件
	 */
	public static void sendMailByTemplate(List<String> receiverMails,Id templateId,Id templateObjectId) {
		try{
			// Pick a dummy Contact
			Contact c = [select id,Email from Contact 
										where email<>null 
										  and createddate > 2013-11-01T00:00:00Z 
										  and createddate < 2013-11-02T00:00:00Z limit 1];
			// Construct the list of emails we want to send
			List<Messaging.SingleEmailMessage> lstMsgs = new List<Messaging.SingleEmailMessage>();
			Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
			msg.setTemplateId(templateId);
			msg.setWhatId(templateObjectId);
			msg.setTargetObjectId(c.id); //dummy Contact
			msg.setToAddresses(receiverMails);
			lstMsgs.add(msg);
			// Send the emails in a transaction, then roll it back
			Savepoint sp = Database.setSavepoint();
			Messaging.sendEmail(lstMsgs);
			Database.rollback(sp);
			// For each SingleEmailMessage that was just populated by the sendEmail() method, copy its
			// contents to a new SingleEmailMessage. Then send those new messages.
			List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();
			for (Messaging.SingleEmailMessage email : lstMsgs) {
			   Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
			   emailToSend.setCcAddresses(email.getToAddresses());
			   emailToSend.setPlainTextBody(email.getPlainTextBody());
			   emailToSend.setHTMLBody(email.getHTMLBody());
			   emailToSend.setSubject(email.getSubject());
			   lstMsgsToSend.add(emailToSend);
			}
			Messaging.sendEmail(lstMsgsToSend);
		}catch(Exception e){
			System.debug('SendMail Error#################' + e);
		}
	}

}