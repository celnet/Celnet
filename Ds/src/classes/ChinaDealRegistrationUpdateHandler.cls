/**
 * @Purpose : 1、(中国区)报备关联机会点后被激活，激活后经销商至少每14天要更新一次，否则报备失效;
 *            2、(中国区)报备转/关联机会点后，将报备设置成Open状态;
 *            3、(中国区)Open状态的报备被经销商更新后，自动刷新失效时间;
 * @Author : steven
 * @Date :   2014-09-03
 */
public without sharing class ChinaDealRegistrationUpdateHandler implements Triggers.Handler{//before update
	public void handle() {
		List<Data_Maintain_History__c> archiveList = new List<Data_Maintain_History__c>();
		for(Deal_Registration__c deal : (List<Deal_Registration__c>)Trigger.new){
			if(deal.RecordTypeId==CONSTANTS.HUAWEICHINADEALRECORDTYPE){
				system.debug('-----ChinaDealRegistrationUpdateHandler----');
				Deal_Registration__c oldDeal = (Deal_Registration__c)Trigger.oldMap.get(deal.id);
				//Logic1: Open状态的报备更新后，刷新失效时间（经销商在eChannel刷新报备）
				if(deal.Deal_Status__c == 'Open' && keyFieldIsUpdate(deal,oldDeal)){
					if(deal.Expiring_Date__c != null) {//机会点关闭后该时间会被清空，关闭后不再失效
						deal.Expiring_Date__c = system.today()+14;
					}
				}
				//Logic2: 报备关联了机会点（即报备审批通过）
				if(deal.Related_Opportunity__c != null && oldDeal.Related_Opportunity__c == null){
					if(deal.Deal_Status__c == 'Waiting For Confirmation'){
						Opportunity openOpportunity = getOpenOpportunity(deal.Related_Opportunity__c);
						if(openOpportunity == null) {
							deal.addError('不允许关联已关闭的机会点');
						} else {
							deal.Expiring_Date__c = system.today()+14;
							deal.Deal_Status__c = 'Open';
							deal.end_customer_lookup__c = openOpportunity.accountId;
							deal.Qualify_Date__c = system.today();//记录审批通过时间
							//向PRM集成数据表中写入机会点ID
							Data_Maintain_History__c history = new Data_Maintain_History__c();
							history.Data_Type__c = 'China Opportunity Team';
							history.Operation_Type__c = 'Update';
							history.Record_ID__c = deal.Related_Opportunity__c;
							archiveList.add(history);
						}
					} else {
						deal.addError('只有“待提交审批”和“待确认”状态的报备可以选择关联一个机会点');
					}
				}
				//Logic3: 报备更新了关联的机会点（目前不允许修改，通过校验规则控制）
				if(deal.Related_Opportunity__c != null && oldDeal.Related_Opportunity__c != null && 
					 deal.Related_Opportunity__c != oldDeal.Related_Opportunity__c){
					/*
					if(deal.Deal_Status__c == 'Open'){
						Opportunity oldDealOpenOpp = getOpenOpportunity(oldDeal.Related_Opportunity__c);
						if(oldDealOpenOpp == null) {
							deal.addError('报备关联的机会点已关闭，不允许再修改');
						}
						Opportunity newDealOpenOpp = getOpenOpportunity(deal.Related_Opportunity__c);
						if(newDealOpenOpp == null) {
							deal.addError('不允许关联已关闭的机会点');
						}
					} else {
						deal.addError('只有“审批通过”状态的报备可以修改关联的机会点');
					}
					*/
				}
				//Logic4: 报备取消关联了机会点     （目前不允许取消，通过校验规则控制）
				if(deal.Related_Opportunity__c == null && oldDeal.Related_Opportunity__c != null){
					/*
					deal.addError('报备已关联的机会点不能删除，只能修改');
					*/
				}
				//Logic5: 报备做了分发操作，记录分发时间 （点击分发按钮，或者直接Change Owner）
				if(deal.OwnerId != oldDeal.OwnerId){
					if(deal.Deal_Status__c == 'Waiting For Confirmation'){
						deal.Submited_Time__c = system.now();
					}
				}
				//Logic6: 驳回的报备记录驳回时间
				if(deal.Deal_Status__c != oldDeal.Deal_Status__c){
					if(deal.Deal_Status__c == 'Registration Failed'){
						deal.Qualify_Date__c = system.today();
					}
				}
				//Logic7: 驳回的报备重新从Echannel同步到SF （重置状态字段）
				if(deal.Deal_Status__c != oldDeal.Deal_Status__c){
					if(deal.Deal_Status__c == 'Waiting For Confirmation'){
						deal.Submited_Time__c = null;
						deal.Expiring_Date__c = null;
						deal.Related_Opportunity__c = null;
						deal.end_customer_lookup__c = null;
						deal.Qualify_Date__c = null;
					}
				}
				//Logic8: 如果报备提交人发送了变化
				if(deal.login_contact__c != null && (deal.login_contact__c != oldDeal.login_contact__c)){
					List<Contact> ctList = [Select Id,FirstName,LastName,Account.Name From Contact Where Id =: deal.login_contact__c];
					if(ctList.size()>0) {
						Contact ct = ctList.get(0);
						deal.Submitted_by_Name_Text__c = ct.FirstName + ' ' + ct.LastName;
						deal.Submitted_Partner_Name_Text__c = ct.Account.Name;
					}
				}
				
			}
		} //end for
		if(archiveList.size()>0) insert(archiveList);
	} //end handler
	
	/**
	 * 校验Deal的关键字段是否被经销商更新了
	 */
	private Boolean keyFieldIsUpdate(Deal_Registration__c deal,Deal_Registration__c oldDeal) {
		if(deal.Deal_Competitor_Update_Time__c != oldDeal.Deal_Competitor_Update_Time__c) return true;
		if(deal.Deal_Product_Update_Time__c != oldDeal.Deal_Product_Update_Time__c) return true;
		if(deal.Name != oldDeal.Name) return true;
		if(deal.Estimated_Contracting_Date__c != oldDeal.Estimated_Contracting_Date__c) return true;
		if(deal.Equipment_Value__c != oldDeal.Equipment_Value__c) return true;
		if(deal.Deal_Value__c != oldDeal.Deal_Value__c) return true;
		if(deal.End_Customer_Company_Name__c != oldDeal.End_Customer_Company_Name__c) return true;
		if(deal.End_Customer_Contact_Email_Adress__c != oldDeal.End_Customer_Contact_Email_Adress__c) return true;
		if(deal.End_Customer_Contact_Last_Name__c != oldDeal.End_Customer_Contact_Last_Name__c) return true;
		if(deal.End_Customer_Contact_Work_Phone__c != oldDeal.End_Customer_Contact_Work_Phone__c) return true;
		if(deal.End_Customer_Address__c != oldDeal.End_Customer_Address__c) return true;
		if(deal.Deal_Description__c != oldDeal.Deal_Description__c) return true;
		return false;
	}
	
	/**
	 * 获取未关闭的机会点
	 */
	private Opportunity getOpenOpportunity(Id oppId) {
		List<Opportunity> oppList = [select id,AccountId from Opportunity where isClosed = false and id =: oppId];
		if(oppList.size()>0) {
			return oppList.get(0);
		}
		return null;
	}
	
}