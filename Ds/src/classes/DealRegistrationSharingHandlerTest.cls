@isTest
private class DealRegistrationSharingHandlerTest {

    static testMethod void testShare() {
        Deal_Registration__c deal = new Deal_Registration__c(name = 'test');
        insert deal;
        Test.startTest();
        deal.SubmittedToApproval__c = true;
        update deal;
        Test.stopTest();
    }
    
     static testMethod void share2() {
    	Profile p = UtilUnitTest.getProfile('(Huawei China)代表处产品经理');
    	User u = UtilUnitTest.newUserWithoutInsert(p.id);
      insert u;
      Deal_Registration__c dr = new Deal_Registration__c(Deal_Status__c='Waiting For Confirmation',	
      	recordtypeid = CONSTANTS.HUAWEICHINADEALRECORDTYPE);
      insert dr;
      Approval.ProcessSubmitRequest req = 
            new Approval.ProcessSubmitRequest();
      req.setObjectId(dr.id);
      req.setNextApproverIds(new List<id>{u.id});
      Test.startTest();
      Approval.ProcessResult result = Approval.process(req);
      System.assertEquals(true,result.isSuccess());
      Test.stopTest();
      	 UserRecordAccess access = [SELECT RecordId, HasReadAccess, HasEditAccess
         FROM UserRecordAccess
         WHERE UserId = :UserINfo.getUserId()
         AND RecordId = :dr.id limit 1];
         System.assertEquals(true,access.HasReadAccess);
      
    }
}