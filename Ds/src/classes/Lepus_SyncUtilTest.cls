@isTest
private class Lepus_SyncUtilTest {
    static testmethod void myUnitTest(){
        Account acc = Lepus_EIPCalloutServiceTest.createAccount('acc');
        insert acc;
        
        Lepus_SyncUtil.querySobject(acc.Id, Account.sobjecttype);
        Lepus_SyncUtil.querySobjectsToMap(new Set<Id>{acc.Id}, Account.sobjecttype);
        
        Opportunity opp = Lepus_EIPCalloutServiceTest.createOpportunity('oppx', acc.Id);
        insert opp;
        
        Lead l = new Lead();
        l.LastName = 'test';
        l.Company = 'test';
        insert l;
        
        Contact con = Lepus_EIPCalloutServiceTest.createContact(acc.Id);
        insert con;
        
        Lepus_SyncUtil.querySobject(opp.Id, Opportunity.sobjecttype);
        Lepus_SyncUtil.querySobject(l.Id, Lead.sobjecttype);
        Lepus_SyncUtil.querySobject(con.Id, Contact.sobjecttype);
        Lepus_SyncUtil.querySobject(UserInfo.getUserId(), User.sobjecttype);
        Lepus_SyncUtil.queryLeadHistories(new list<Id>{l.Id});
        Lepus_SyncUtil.queryOppFieldHistories(opp.Id, datetime.now());
        Lepus_SyncUtil.retrieveSobjectType('Account');
        Lepus_SyncUtil.retrieveSobjectType('User');
        Lepus_SyncUtil.retrieveSobjectType('Opportunity');
        Lepus_SyncUtil.retrieveSobjectType('Lead');
        Lepus_SyncUtil.retrieveSobjectType('Contact');
        
        map<Id, String> idGlobalIdMap = new map<Id, String>();
        idGlobalIdMap.put(opp.Id, 'testoppa');
        
        Lepus_SyncUtil.initQueue(new list<Id>{opp.Id}, idGlobalIdMap, 'insert', '业务数据同步', datetime.now());
        
        
    }
    
    static testmethod void testmethod2(){
        Account acc = Lepus_EIPCalloutServiceTest.createAccount('acc');
        insert acc;
        
        Opportunity opp = Lepus_EIPCalloutServiceTest.createOpportunity('oppx', acc.Id);
        insert opp;
        
        map<Id, sobject> sobjMap = new map<Id, sobject>();
        sobjMap.put(opp.Id, opp);
        
        Lepus_SyncUtil.initQueue(sobjMap, new list<Id>{opp.Id}, 'insert', '业务数据同步', datetime.now());
        
        OpportunityTeamMember otm = new OpportunityTeamMember();
        otm.OpportunityId = opp.Id;
        otm.UserId = UserInfo.getUserId();
        insert otm;
        
        Lepus_SyncUtil.retrieveTeamMembers(new list<Opportunity>{opp}, Opportunity.sobjecttype);
    }
}