/*
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-5-12
 * Description: mass edit income and forecast
 */
public class MassMaIncomeForecastExtension 
{
    // salesmeeting info
    private Id salesMeetingId;
    public String salesMeetingOffice{get;set;}
    public String salesMeetingName{get;set;}
    private boolean isImportantRegion;
    
    // income and forecast
    private List<Income_And_Forecast__c> incomeAndForecastList;
    public List<Income_And_Forecast__c> productIafList{get;set;}
    public List<Income_And_Forecast__c> industryIafList{get;set;}
    
    // picklist value
    private List<String> industryList;
    private List<String> productList;
    
    public MassMaIncomeForecastExtension(ApexPages.StandardSetController setCon){
        this.salesMeetingId = ApexPages.currentPage().getParameters().get('id');
        if(this.salesMeetingId != null){
            this.retrieveSalesMeetingInfo();
            this.retrieveIncomeAndForecast();
            this.retrievePicklistValue();
            this.initIncomeAndForecast();
        }
    }
    
    private void retrieveSalesMeetingInfo(){
        Sales_Meeting__c sm = [Select Id, Name, Representative_Office__c From Sales_Meeting__c Where Id =: this.salesMeetingId];
        this.salesMeetingOffice = sm.Representative_Office__c;
        this.salesMeetingName = sm.Name;
        
        if(this.salesMeetingOffice == '北京代表处' || this.salesMeetingOffice == '广州代表处' || this.salesMeetingOffice == '杭州代表处'){
            this.isImportantRegion = true;
        } else {
            this.isImportantRegion = false;
        }
    }
    
    private void retrieveIncomeAndForecast(){
        this.incomeAndForecastList = new List<Income_And_Forecast__c>();
        this.incomeAndForecastList = [SELECT Name, Id, Sales_Meeting__c, RecordTypeId, RecordType.Name, Sales_Meeting__r.Name,
                                        Sales_Meeting__r.Representative_Office__c, Industry__c, Is_Important_Region__c,
                                        Product__c, Stock_Overall__c, Stock_finished__c, Incremental_Finished__c, 
                                        Stock_Not_Finished__c, Incremental_Not_Finished__c, Forecast_This_Quarter_HQ_Distribute__c,
                                        Forecast_Next_Quarter__c, Unique_Name__c, Q1_Target__c, H1_Target__c, Annual_Target__c
                                     FROM  Income_And_Forecast__c 
                                     Where  Sales_Meeting__c =: this.salesMeetingId];
    }
    
    private void retrievePicklistValue(){
        // 产品列表
        this.productList = new List<String>();
        Schema.DescribeFieldResult productFieldResult = Income_And_Forecast__c.Product__c.getDescribe();
        List<Schema.PicklistEntry> pv = productFieldResult.getPicklistValues();
        for(Schema.Picklistentry f : pv){
            productList.add(f.getValue());
        }
        
        // 行业列表
        this.industryList = new List<String>();
        Schema.Describefieldresult industryFieldResult = Income_And_Forecast__c.Industry__c.getDescribe();
        List<Schema.Picklistentry> iv = industryFieldResult.getPicklistValues();
        for(Schema.Picklistentry f : iv){
            // 移除不需要的picklist值
            if(this.isImportantRegion){
                if(f.getValue() != '媒体与资讯系统部') {
                    this.industryList.add(f.getValue());
                }
            } else {
                if(f.getValue() != '媒资（除三大ISP）' && f.getValue() != '媒资（三大ISP）') {
                    this.industryList.add(f.getValue());
                }
            }
        }
    }
    
    private void initIncomeAndForecast(){
        this.productIafList = new List<Income_And_Forecast__c>();
        this.industryIafList = new List<Income_And_Forecast__c>();
        
        // 生成所有记录
        Map<String, Income_And_Forecast__c> iafIndustryMap = new Map<String, Income_And_Forecast__c>();
        for(String str : this.industryList){
            iafIndustryMap.put(str, this.newIncomeAndForecast(str, ''));
        }
        Map<String, Income_And_Forecast__c> iafProductMap = new Map<String, Income_And_Forecast__c>();
        for(String str : this.productList){
            iafProductMap.put(str, this.newIncomeAndForecast('', str));
        }
        
        // 用系统存在的记录替换对应的记录
        for(Income_And_Forecast__c iaf : this.incomeAndForecastList){
            if(iaf.RecordType.Name == 'Income And Forecast Record Type(Product)'){
                iafProductMap.put(iaf.Product__c, this.avoidNull(iaf));
            }else if(iaf.RecordType.Name == 'Income And Forecast Record Type(Industry)'){
                iafIndustryMap.put(iaf.Industry__c, this.avoidNull(iaf));
            }
        }
        
        // 按照picklist顺序显示
        for(String str : this.industryList){
            this.industryIafList.add(iafIndustryMap.get(str));
        }
        for(String str : this.productList){
            this.productIafList.add(iafProductMap.get(str));
        }
    }
    
    private Income_And_Forecast__c avoidNull(Income_And_Forecast__c iaf){
    	iaf.Stock_Overall__c = iaf.Stock_Overall__c == null?0:iaf.Stock_Overall__c;
        iaf.Stock_finished__c = iaf.Stock_finished__c == null?0:iaf.Stock_finished__c;
        iaf.Incremental_Finished__c = iaf.Incremental_Finished__c == null?0:iaf.Incremental_Finished__c;
        iaf.Stock_Not_Finished__c = iaf.Stock_Not_Finished__c == null?0:iaf.Stock_Not_Finished__c;
        iaf.Incremental_Not_Finished__c = iaf.Incremental_Not_Finished__c == null?0:iaf.Incremental_Not_Finished__c;
        iaf.Forecast_This_Quarter_HQ_Distribute__c = iaf.Forecast_This_Quarter_HQ_Distribute__c == null?0:iaf.Forecast_This_Quarter_HQ_Distribute__c;
        iaf.Forecast_Next_Quarter__c = iaf.Forecast_Next_Quarter__c == null?0:iaf.Forecast_Next_Quarter__c;
        iaf.Q1_Target__c = iaf.Q1_Target__c == null?0:iaf.Q1_Target__c;
        iaf.H1_Target__c = iaf.H1_Target__c == null?0:iaf.H1_Target__c;
        iaf.Annual_Target__c = iaf.Annual_Target__c == null?0:iaf.Annual_Target__c;
        return iaf;
    }
    
    private Income_And_Forecast__c newIncomeAndForecast(String industry, String product){
        Income_And_Forecast__c iaf = new Income_And_Forecast__c();
        iaf.Stock_Overall__c = 0;
        iaf.Stock_finished__c = 0;
        iaf.Incremental_Finished__c = 0;
        iaf.Stock_Not_Finished__c = 0;
        iaf.Incremental_Not_Finished__c = 0;
        iaf.Forecast_This_Quarter_HQ_Distribute__c = 0;
        iaf.Forecast_Next_Quarter__c = 0;
        iaf.Q1_Target__c = 0;
        iaf.H1_Target__c = 0;
        iaf.Annual_Target__c = 0;
        iaf.Sales_Meeting__c = this.salesMeetingId;
        if(industry != ''){
            iaf.Industry__c = industry;
            iaf.Is_Important_Region__c = this.isImportantRegion?'是':'否';
            iaf.RecordTypeId = CONSTANTS.SALESMEETINGINCOMEINDUSTRY;
        } else if (product != '') {
            iaf.Product__c = product;
            iaf.RecordTypeId = CONSTANTS.SALESMEETINGINCOMEPRODUCT;
        }
        return iaf;
    }
    
    /**
     * 返回销售例会明细界面
     */
    public PageReference cancel(){
        PageReference pr = new PageReference('/' + this.salesMeetingId);
        pr.setRedirect(true);
        return pr;
    }
    
    /**
     * 保存所有的收入预测行记录（用于编辑界面点击保存按钮）
     */
    public PageReference saveIncomeForecast(){
        List<Income_And_Forecast__c> incomeForecastListSave = new List<Income_And_Forecast__c>();
        try {
            incomeForecastListSave.addAll(this.industryIafList);
            incomeForecastListSave.addAll(this.productIafList);

            if(incomeForecastListSave.size()>0) {
                upsert incomeForecastListSave;
            }
            PageReference pr = new PageReference('/' + this.salesMeetingId);
            pr.setRedirect(true);
            return pr;
        } catch(DMLException e) {
            ApexPages.addMessages(e);
            return null;
        }
    }
}