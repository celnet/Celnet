/**
 * @Purpose : （海外）机会点根据阶段的变化记录OI和OO
 * @Author : Steven
 * @Date : 2014-08-29 Refactor
 */
public without sharing class OverseaOpportunityUpdateOIInfo implements Triggers.Handler{ //before update
	public void handle(){
		//Step1: 记录所有待刷新的机会点ID
		Set<Id> userIds = new Set<Id>(); 
		Set<Id> oppIds = new Set<Id>(); 
		for(Opportunity opt : (List<Opportunity>)Trigger.New) {
			if(opt.RecordTypeId == CONSTANTS.HUAWEIOVERSEAOPPRECORDTYPE || 
				 opt.RecordTypeId == CONSTANTS.HUAWEIOVERSEADEALOPPRECORDTYPE){ 
				Opportunity oldOpt = (Opportunity)trigger.oldMap.get(opt.id);
				if(opt.StageName != oldOpt.StageName) {
					userIds.add(opt.OwnerId);
					oppIds.Add(opt.Id);
				}
			}
		}
		if(oppIds.isEmpty()) return;
		system.debug('-----OverseaOpportunityUpdateOIInfo----');
		
		//Step2: 一次查询所有数据
		Map<Id,User> idToUserMap = new Map<Id,User>([SELECT Id, Name FROM User WHERE Id IN :userIds]);  
		Set<String> oldStage = new set<String>();
		oldStage.add('SS1 (Notifying Opportunity)');
		oldStage.add('SS2 (Identifying Opportunity)');
		oldStage.add('SS3 (Validating Opportunity)');
		Set<String> newStage = new set<String>();
		newStage.add('SS4 (Developing Solution)');
		newStage.add('SS5 (Gaining Agreement)');
		newStage.add('SS6 (Winning)');
		newStage.add('SS7 (Implementing)');
		newStage.add('SS8 (Completed)');
		//Step3: 根据机会点状态变化来更新OI OO
		for(Opportunity opp : (List<Opportunity>)Trigger.New) {
			if(oppIds.contains(opp.id)) {
				//get old opp map
				Opportunity oldOpp = (Opportunity)trigger.oldMap.get(opp.id);
				User user = idToUserMap.get(opp.OwnerId);
				//when update stage in SS1-SS3,OI can feldom change that.
				if(oldStage.contains(opp.StageName) && user.id == opp.OwnerId && opp.TestTriggerChangeOIOnStageUpdate__c==false){
					opp.Opportunity_Identifier__c = user.Name;                                                   
				}
				//when update stage in SS1-SS3 to SS4-SS8,OI will once change never to do that.当阶段从SS4以上变更为SS1的时候，原本的OI也不再改变
				if(oldStage.contains(oldOpp.StageName) && newStage.contains(opp.StageName) && user.id == opp.OwnerId && opp.TestTriggerChangeOIOnStageUpdate__c==false){
					opp.Opportunity_Identifier__c = user.Name;
					opp.TestTriggerChangeOIOnStageUpdate__c = true;
				}
				//当ss4在回到ss4以下，oo还是继续改变
				if(newStage.contains(opp.StageName) && user.id == opp.OwnerId){
					opp.Opportunity_Owner__c = user.Name;
					opp.TestTriggerChangeOOOnStageUpdate__c = true;  
				}
				//再一次改回SS4以上，oo继续变
				if(oldStage.contains(opp.StageName) && user.Id == opp.OwnerId && opp.TestTriggerChangeOOOnStageUpdate__c==true){
					opp.Opportunity_Owner__c = user.Name;
				}
			}
		} //end for
 	}

}