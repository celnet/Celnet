/*
* @Purpose : 当经销商认证信息过期时，自动取消TCG关联的经销商
* @Author : steven
* @Date : 2014-03-03
*/
global class ChinaTCGPartnerRefreshScheduleJob implements Schedulable,Database.Batchable<sObject>{
	public Set<Id> partnerIDs {get;set;} 
	public String query;
	 
	//Constructor
	public ChinaTCGPartnerRefreshScheduleJob() {
		partnerIDs = new Set<Id>();
   	//查询出所有当天过期的数据
		List<TCG_Partner_Certification__c> pcList = [select id,OC_Name__c from TCG_Partner_Certification__c where Overdue_Time__c = 1];
		for(TCG_Partner_Certification__c pc : pcList) {
			partnerIDs.add(pc.OC_Name__c);
		}
	}   
	
	//ScheduleJob Execute
	global void execute(SchedulableContext SC){
		String partners = '';
		if(partnerIDs.size()==0) return;
		for(Id i : partnerIDs){
			if(partners == '') partners = partners + '\''+ i +'\'';
			else partners = partners + ',\''+ i +'\'';
		}
		ChinaTCGPartnerRefreshScheduleJob e = new ChinaTCGPartnerRefreshScheduleJob();
		e.query='select id from Account where id in ('+ partners +')';
		Id batchId = Database.executeBatch(e,5);
	}
    
	//Batch Start
	public Database.QueryLocator start(Database.BatchableContext BC){
		return Database.getQueryLocator(query);   
	}
	//Batch Execute
	public void execute(Database.BatchableContext BC, list<sObject> scope){
		try {
			Set<id> pIDs = new Set<id>();
			//获取本批次所有需要处理的经销商ID
			for(sObject s : scope){
				Account p = (Account)s;
				pIDs.add(p.id);
			}
			//根据经销商关联的认证信息去创建TCG关联的经销商列表
			TCGPartnerCertificationHandler.refreshTCGRelatedPartnerImmediately(pIDs);
		} catch(Exception e) {
			System.debug(e);
		}
	}
	//Batch Finished
	public void finish(Database.BatchableContext BC){
	}
	
}