/**
 * @Purpose : 删除后将记录保存到数据维护历史对象上
 * @Author : kejun 20140609
 * @Date : 2014-06-09 update by Steven
 */
public class OpportunityArchiveForBIHandler implements Triggers.Handler{
    public void handle(){
        List<Data_Maintain_History__c> archiveList = new List<Data_Maintain_History__c>();
        for(Opportunity o : (List<Opportunity>)trigger.old){
            if(o.RecordTypeId == CONSTANTS.CHINAOPPORTUNITYRECORDTYPE || 
            	 o.recordtypeid == CONSTANTS.HUAWEICHINASERVICEOPPRECORDTYPE || 
            	 o.RecordTypeId == CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE){
                Data_Maintain_History__c dmh = new Data_Maintain_History__c();
                dmh.Record_ID__c = o.Id;
                dmh.Operation_Type__c = 'Delete';
                dmh.Data_Type__c = 'China Opportunity';
                archiveList.add(dmh);
                
                List<OpportunityTeamMember> otmList = new List<OpportunityTeamMember>();
                otmList = [Select id From OpportunityTeamMember Where OpportunityId =: o.Id];
                for(OpportunityTeamMember otm : otmList){
                    Data_Maintain_History__c dmh2 = new Data_Maintain_History__c();
                    dmh2.Record_ID__c = otm.Id;
                    dmh2.Operation_Type__c = 'Delete';
                    dmh2.Data_Type__c = 'China Opportunity Team (BI)';
                    archiveList.add(dmh2);
                }
                
                List<Project_Product__c> ppcList = new List<Project_Product__c>();
                ppcList = [Select id From Project_Product__c Where Project_Name__c =: o.Id];
                for(Project_Product__c ppc : ppcList){
                    Data_Maintain_History__c dmh3 = new Data_Maintain_History__c();
                    dmh3.Record_ID__c = ppc.Id;
                    dmh3.Operation_Type__c = 'Delete';
                    dmh3.Data_Type__c = 'China Opportunity Product';
                    archiveList.add(dmh3);
                }
            }
            if(o.RecordTypeId == CONSTANTS.HUAWEIOVERSEAOPPRECORDTYPE || 
            	 o.RecordTypeId == CONSTANTS.HUAWEIOVERSEADEALOPPRECORDTYPE){
                Data_Maintain_History__c dmh = new Data_Maintain_History__c();
                dmh.Record_ID__c = o.Id;
                dmh.Operation_Type__c = 'Delete';
                dmh.Data_Type__c = 'Oversea Opportunity';
                archiveList.add(dmh);
                
                List<OpportunityTeamMember> otmList = new List<OpportunityTeamMember>();
                otmList = [Select id From OpportunityTeamMember Where OpportunityId =: o.Id];
                for(OpportunityTeamMember otm : otmList){
                    Data_Maintain_History__c dmh2 = new Data_Maintain_History__c();
                    dmh2.Record_ID__c = otm.Id;
                    dmh2.Operation_Type__c = 'Delete';
                    dmh2.Data_Type__c = 'Oversea Opportunity Team';
                    archiveList.add(dmh2);
                }
                
                List<Project_Product__c> ppcList = new List<Project_Product__c>();
                ppcList = [Select id From Project_Product__c Where Project_Name__c =: o.Id];
                for(Project_Product__c ppc : ppcList){
                    Data_Maintain_History__c dmh3 = new Data_Maintain_History__c();
                    dmh3.Record_ID__c = ppc.Id;
                    dmh3.Operation_Type__c = 'Delete';
                    dmh3.Data_Type__c = 'Oversea Opportunity Product';
                    archiveList.add(dmh3);
                }
            }
        }
        if(archiveList.size() > 0){
            insert archiveList;
        }
    }
}