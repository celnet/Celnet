@isTest
private class AccountUpdateRootHandlerTest {
	static testMethod void multipleLevel() {
		Account parent = UtilUnitTest.createHWChinaAccount('p');
		Named_Account__c na = UtilUnitTest.createActiveNamedAccount(parent);
		Account acc = UtilUnitTest.createHWChinaAccount('a');
		acc.ParentId = parent.id;
		update acc;
		acc.ParentId = null;
		update acc;
		acc.ParentId = parent.id;
		update acc;
		Map<id,Account> accMap = new Map<id,Account>([select id, HQ_Parent_Account__c from Account]);
		System.assertEquals(null,accMap.get(parent.id).HQ_Parent_Account__c);
		System.assertEquals(parent.id,accMap.get(acc.id).HQ_Parent_Account__c);
	}
}