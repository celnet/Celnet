public without sharing class SalesTargetSharingHandler  implements Triggers.Handler{//after insert ,after update 
	public void handle(){
		//Logic1:对于中国区客户经理的NA客户销售目标，需要共享给指定的客户经理
		if(Trigger.isInsert){
			List<Sales_Target_Accomplishment__Share> sharingRecords = new List<Sales_Target_Accomplishment__Share> ();
			for(Sales_Target_Accomplishment__c s : (List<Sales_Target_Accomplishment__c>)Trigger.new){
				if(s.recordtypeid == CONSTANTS.SALESTARGETMANAGERECORDTYPE){//记录类型为中国区客户经理的NA客户销售目标
					Sales_Target_Accomplishment__Share r = new Sales_Target_Accomplishment__Share();
					r.UserOrGroupId = s.Account_Manager__c;
					r.ParentId = s.id;
					r.AccessLevel = 'Read';
					r.RowCause = Schema.Sales_Target_Accomplishment__Share.rowCause.Share_to_AccountManager__c;
					sharingRecords.add(r);
				}
			}
			insert sharingRecords;
		}
		
		//Logic2:对于中国区客户经理的NA客户销售目标、NA客户销售目标、中国区TCG的销售目标，新创建的记录应该自动changeOwner给客户Owner
		if(Trigger.isInsert){
			Set<Id> targetIds = new Set<Id>();
			Set<Id> accIds = new Set<Id>();
			for(Sales_Target_Accomplishment__c s : (List<Sales_Target_Accomplishment__c>)Trigger.new){
				if(s.recordtypeid == CONSTANTS.SALESTARGETMANAGERECORDTYPE || 
					 s.recordtypeid == CONSTANTS.SALESTARGETCHINANARECORDTYPE){					 						
					targetIds.add(s.id);
					accIds.add(s.China_Account_Name__c);
				}
				//add by steven 20140127 start
				if(s.recordtypeid == CONSTANTS.SALESTARGETCHINATCGRECORDTYPE){ 
					targetIds.add(s.id);
					accIds.add(s.China_TCG_Name__c);
				}
				//add by steven 20140127 end 
			}
			if (targetIds.isEmpty()) return;
			//查询并保存相关客户的Owner属性
		  Map<Id,Id> accOwnerIdMap = new Map<Id,Id>();
		  List<Account> accountList = [select id, OwnerId from Account where id in :accIds];
		  for(Account acc : accountList){
		  	accOwnerIdMap.put(acc.id,acc.OwnerId);
		  }
			//刷新Target的Owner
			List<Sales_Target_Accomplishment__c> targetUpdateList = 
				[select id, OwnerId, China_Account_Name__c, China_TCG_Name__c,recordtypeid  //edit by steven 20140127
				   from Sales_Target_Accomplishment__c where id in :targetIds and Inactive__c=false];							
			for(Sales_Target_Accomplishment__c target : targetUpdateList){
				//edit by steven 20140127 start
				if(target.recordtypeid == CONSTANTS.SALESTARGETCHINATCGRECORDTYPE){ 
					target.OwnerId = accOwnerIdMap.get(target.China_TCG_Name__c);
				} else {
					target.OwnerId = accOwnerIdMap.get(target.China_Account_Name__c);
				}
				//edit by steven 20140127 end 
		  }
			
			update targetUpdateList;
		}
		
		//Logic3:对于中国区客户经理的NA客户销售目标，如果允许修改客户经理，必须将原先的共享规则删掉，再增加新的共享
		if(Trigger.isUpdate){
		}
	}
}