/*
* @Purpose : 
* 新建、更新、删除时	更新关联SFP的Product Name字段
*	
*
* @Author : Brain Zhang
* @Date :       2013-11
*
*
*/
public without sharing class SFPProductNameRollUpHandler implements Triggers.Handler{
	public void handle(){
		Set<Id> SFPIDs = new Set<Id>();
    if(trigger.isInsert || trigger.isUnDelete){
    	for(SFP_Product__c w : (List<SFP_Product__c>)trigger.new){
					SFPIDs.add(w.SFP__c);
    	}
    }
		if(trigger.isUpdate ){
			for(SFP_Product__c w : (List<SFP_Product__c>)trigger.new){
				if((w.SFP__c != ((SFP_Product__c)trigger.oldmap.get(w.id)).SFP__c
					||w.Contract_Product_PO__c != ((SFP_Product__c)Trigger.oldmap.get(w.id)).Contract_Product_PO__c)){//SFP或contract product变化
					SFPIDs.add(w.SFP__c);
					//一并加入原来的service request no
					SFPIDs.add(((SFP_Product__c)trigger.oldmap.get(w.id)).SFP__c);
				}
			}
        
		}
		if(trigger.isDelete){
       for(SFP_Product__c w : (List<SFP_Product__c>)trigger.old){
					SFPIDs.add(w.SFP__c);
			}
    }
    if(SFPIDs.size() > 0){//更新其中所有记录的product name
    	List<SFP__c> sfps = [select id,Product_Name__c,Product_Line__c, (Select Contract_Product_PO__r.name,Contract_Product_PO__r.Product_Family__c From SFP_Product__r limit 5) //只读取前五个产品
    		from SFP__c where id in:SFPIDs];
    	for(SFP__c s : sfps){
    		s.Product_Name__c = '';
    		s.Product_Line__c = '';
    		for(SFP_Product__c p : s.SFP_Product__r){
    			s.Product_Name__c += p.Contract_Product_PO__r.name;
    			s.Product_Name__c += ';';
    			s.Product_Line__c +=p.Contract_Product_PO__r.Product_Family__c;
    			s.Product_Line__c +=';';
    		}
   			s.Product_Name__c = s.Product_Name__c.removeEnd(';');//remove the final ;
   			s.Product_Line__c = s.Product_Line__c.removeEnd(';');
    	}
    	//should ignore all the sfp trigger
    	update sfps;
    }
	}
	
}