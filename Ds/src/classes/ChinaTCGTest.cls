@isTest
private	class ChinaTCGTest {
	static testMethod void createTCG(){
		Account acc = createChinaTCG();
		ChinaTCGExtension controller = new ChinaTCGExtension(new ApexPages.StandardController(acc));
		try{
			controller.save();
		}catch(Exception e){}
	}
	
	static testMethod void redirectTCG(){
		Account acc = createChinaTCG();
		AccountRedirect controller = new AccountRedirect(new ApexPages.StandardController(acc));
		try{
			controller.redirect();
		}catch(Exception e){}
	}
	
	static testMethod void specialRoleTest(){
		Special_Role__c sr = new Special_Role__c();
		sr.Role_Name__c = 'China_Business_Leader';
		sr.User__c = Userinfo.getUserId();
		insert sr;
	}
	
	static testMethod void tcgHandler1(){
			Account tcg1 = createChinaTCG1();
			
			Account acc1 = createCustomer1();
			Account acc2 = createCustomer2();
			
			Opportunity opp = createOpportunity(acc1.id);

			createChinaTCG2();
			updateChinaTCG01(tcg1);
			updateChinaTCG02(tcg1);
			
			updateCustomer1(acc1);
			updateOpportunity(opp,acc2.Id);
	}
	
	static testMethod void tcgHandler2(){
			Account tcg1 = createChinaTCG1();

			Account partner = createPartner();
			createDatadictionary();
			createPartnerCertification(partner.id);

			executeTCGScheduleJob();
	}
	
	public static Account createChinaTCG() {
    Account acc = new Account();
    acc.RecordTypeId = CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE;
    acc.Name = 'tcg';
    acc.TCG_Main_Industry__c = '医疗卫生';
    acc.TCG_Industry__c = '商业-医疗卫生-三甲医院';
    acc.TCG_Level__c = 'Level 1';
    acc.TCG_Representative_Office__c = '北京代表处';
    acc.TCG_City_1__c ='北京市';
    insert acc; 
    return acc;
  }
	public static Account createChinaTCG1() {
    Account tcg = new Account();
    tcg.RecordTypeId = CONSTANTS.HUAWEICHINATCGRECORDTYPE;
    tcg.Name = 'tcg1';
    tcg.TCG_Main_Industry__c = '医疗卫生';
    tcg.TCG_Industry__c = '商业-医疗卫生-三甲医院';
    tcg.TCG_Level__c = 'Level 1';
    tcg.TCG_Representative_Office__c = '北京代表处';
    tcg.TCG_City_Display__c ='北京市';
    insert tcg; 
    return tcg;
  }
	public static Account createChinaTCG2() {
    Account tcg = new Account();
    tcg.RecordTypeId = CONSTANTS.HUAWEICHINATCGRECORDTYPE;
    tcg.Name = 'tcg2';
    tcg.TCG_Main_Industry__c = '医疗卫生';
    tcg.TCG_Industry__c = '商业-医疗卫生-公共卫生系统';
    tcg.TCG_Level__c = 'Level 1';
    tcg.TCG_Representative_Office__c = '北京代表处';
    tcg.TCG_City_Display__c ='北京市';
    insert tcg; 
    return tcg;
  }
  public static void updateChinaTCG01(Account tcg) {
    tcg.TCG_Main_Industry__c = '政府综合';
    tcg.TCG_Industry__c = '商业-政府综合-财税保障';
    update tcg; 
  }
  public static void updateChinaTCG02(Account tcg) {
    tcg.TCG_Main_Industry__c = '医疗卫生';
    tcg.TCG_Industry__c = '商业-医疗卫生-三甲医院';
    update tcg; 
  }
  public static Account createCustomer1() {
    Account acc = new Account();
    acc.RecordTypeId = CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE;
    acc.Name = 'TestCustomer1';
    acc.Region_HW__c = '中国区';
    acc.Representative_Office__c = '北京代表处';
    acc.Province__c = '北京市';
    acc.City_Huawei_China__c = '北京市';
    acc.Industry = '商业销售部';
    acc.Sub_industry_HW__c = '中小企业业务部';
    acc.Customer_Group__c = '商业-医疗卫生-三甲医院';
    acc.Customer_Group_Code__c = 'CG10661';
    insert acc;
    return acc;
  }
  public static Account createCustomer2() {
    Account acc = new Account();
    acc.RecordTypeId = CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE;
    acc.Name = 'TestCustomer2';
    acc.Region_HW__c = '中国区';
    acc.Representative_Office__c = '北京代表处';
    acc.Province__c = '北京市';
    acc.City_Huawei_China__c = '北京市';
    acc.Industry = '商业销售部';
    acc.Sub_industry_HW__c = '中小企业业务部';
    acc.Customer_Group__c = '商业-医疗卫生-公共卫生系统';
    acc.Customer_Group_Code__c = 'CG10663';
    insert acc;
    return acc;
  }
  public static void updateCustomer1(Account acc) {
    acc.Customer_Group__c = '商业-政府综合-财税保障';
    acc.Customer_Group_Code__c = 'CG10668';
    update acc;
  }
  public static Account createPartner() {
    Account partner = new Account();
    partner.RecordTypeId = CONSTANTS.HUAWEICHINAPARTNERRECORDTYPE;
    partner.Name = 'TestPartner';
    insert partner; 
    return partner;
  }
  public static void createDatadictionary() {
    List<DataDictionary__c> ddlist = new List<DataDictionary__c>();
    ddlist.add(new DataDictionary__c(Name__c = '北京市',Code__c = 'bj',Object__c = 'Opportunity',Type__c = 'City'));
    ddlist.add(new DataDictionary__c(Name__c = '商业-医疗卫生-三甲医院',Code__c = '3jyy',Object__c = 'Account',Type__c = 'Customer Group'));
    ddlist.add(new DataDictionary__c(Name__c = '医疗卫生',Code__c = 'ylws',Object__c = 'Account',Type__c = 'Commercial Industry'));
    ddlist.add(new DataDictionary__c(Name__c = '北京代表处',Code__c = 'bjdbc',Object__c = 'Opportunity',Type__c = 'Representative Office'));
    insert ddlist; 
  }
  public static void createPartnerCertification(Id partnerId) {
    TCG_Partner_Certification__c oc = new TCG_Partner_Certification__c();
    oc.OC_Name__c = partnerId;
    oc.Certification_Start_Date__c = Date.today();
    oc.Certification_End_Date__c = Date.today();
    oc.Certification_City_Code__c = 'bj';
    oc.Certification_Sub_Industry_Code__c = '3jyy';
    oc.Certification_Industry_Code__c = 'ylws';
    oc.Representative_Office_Code__c = 'bjdbc';
    insert oc; 
  }
  public static Opportunity createOpportunity(Id accId) {
    Opportunity opp = new Opportunity();
    opp.RecordTypeId = CONSTANTS.CHINAOPPORTUNITYRECORDTYPE;
    opp.AccountId = accId;
    opp.closedate = Date.today();
		opp.name = 'testOpp';
		opp.stagename = 'test';
		opp.Opportunity_type__c='Direct Sales';
    insert opp;
		return opp;
  }
  public static void updateOpportunity(Opportunity opp,Id accId) {
    opp.AccountId = accId;
    update opp; 
  }
  
  public static void executeTCGScheduleJob(){
		ChinaTCGPartnerRefreshScheduleJob e = new ChinaTCGPartnerRefreshScheduleJob();
		Set<Id> partnerIDs = e.partnerIDs;
		String partners = '';
		if(partnerIDs.size()==0) return;
		for(Id i : partnerIDs){
			if(partners == '') partners = partners + '\''+ i +'\'';
			else partners = partners + ',\''+ i +'\'';
		}
		e.query='select id from Account where id in ('+ partners +')';
		Id batchId = Database.executeBatch(e,5);
	}
}