public without sharing class OneTimeServicePlan implements Triggers.Handler{
	public void handle(){
		list <Once_Time_Plan__c> alloneTime = new list <Once_Time_Plan__c>();
		list <SFP__c> sfp = new list <SFP__c>();		
		set <ID> oneTimeID = new set<ID>();	
		set <ID> erroroneTimeID = new set<ID>();	
		Map <id,id> sfponeTimeMap = new Map <id,id>();		
		if(trigger.isInsert || trigger.isUnDelete||trigger.isUpdate){
			for(Once_Time_Plan__c oneTime :(list<Once_Time_Plan__c>)trigger.new){					
				oneTimeID.add(oneTime.id);
				sfponeTimeMap.put(oneTime.id,oneTime.SFP__c);					
			}
		}
		if(trigger.isDelete){
			for(Once_Time_Plan__c oneTime :(list<Once_Time_Plan__c>)trigger.old){						
				oneTimeID.add(oneTime.id);
				sfponeTimeMap.put(oneTime.id,oneTime.SFP__c);				
			}
		}
		system.debug('hewei1111111111111'+oneTimeID);
		
		if(oneTimeID.size()>0){
				for(ID id :oneTimeID){
					Once_Time_Plan__c oneTime = new Once_Time_Plan__c();
					alloneTime =[select id,Sum_Service_Amount__c from Once_Time_Plan__c 
						where SFP__c = :sfponeTimeMap.get(id) ];
					sfp = [select id,No_of_On_Site_Service__c,No_of_On_Site_Service_Left__c,No_of_On_Site_Service_Used__c from SFP__c 
						where id =:sfponeTimeMap.get(id)];									
					if(alloneTime.size()>0){	
						Integer sumServiceAmount = 0;			
						for(Once_Time_Plan__c h :alloneTime){
							sumServiceAmount +=Integer.valueOf(h.Sum_Service_Amount__c);
							if(h.id == id) oneTime = h;
						}						
							//if(sfp[0].Key_Event_On_Duty_Total__c!=null){
								if(sumServiceAmount >sfp[0].No_of_On_Site_Service__c ||sfp[0].No_of_On_Site_Service__c==null){
									addErrorInfo(id);
									return;
								}								
						//}
						sfp[0].No_of_On_Site_Service_Used__c=sumServiceAmount;
						sfp[0].No_of_On_Site_Service_Left__c =sfp[0].No_of_On_Site_Service__c - sumServiceAmount;										
						
					}	else{sfp[0].No_of_On_Site_Service_Left__c=sfp[0].No_of_On_Site_Service__c;}	
						update sfp;
			}
		}

	}
	
	private void addErrorInfo(Id oneTimeId){
		for(Once_Time_Plan__c oneTime :(list<Once_Time_Plan__c>)trigger.new){					
			if(oneTimeId == oneTime.id )oneTime.addError('单次服务时长不能大于SFP单次服务总时长');
		}
	}		
}