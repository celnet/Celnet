/*
*updated by brain zhang,only to pass the test,didn't go through logic
*modified date : 2013-3-14
*/
public class UpgradeToRD {
    public String currId{ 
        get{return currId;}
        set{currId=value;} 
    } 
     public UpgradeToRD(ApexPages.StandardController controller) { 
        currId= ApexPages.currentPage().getParameters().get('id');
    }
    public UpgradeToRD() { 
        currId= ApexPages.currentPage().getParameters().get('id');
    }
    public Boolean haveError{ 
        get{return haveError;} 
        set{haveError=value;}
    }  
    public String getId(){
        return currId;  
    } 
    public void init1()
    {
        String usrId=UserInfo.getUserId();
        Id profileId=UserInfo.getProfileId();  
        
        Profile pf=[select id,Name from Profile where id = :profileId];
        //if(pf==null)return;
        if(pf.Name.indexOf('HPE')<0 && pf.Name.indexOf('PSE')<0 && pf.Name.indexOf('Administrator')<0){ 
            haveError=true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '简档不符，无法上升研发(Just for HPE/PSE profile.)')); 
            return;     
        }
        Case cs=[Select c.id
            ,c.Owner.Id
            ,c.Owner.ProfileId
            ,c.Num_of_Upgrade__c
            ,c.To_R_D__c
            ,c.Status
            ,c.Closed__c
            ,c.R_D_Start_Time__c
            from Case c  
            limit 1];
        if(cs==null)return;
        if(cs.Status=='Upgrade to R&D' || cs.Status=='R&D Implementation'){haveError=true; 
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '已上升研发处理(The case has been upgrade to R&D.)')); 
            return;
        } 
        if(cs.Closed__c=='True'){haveError=true; 
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '该问题已关闭，不能上升研发(The case is closed, cant upgrade.)')); 
            return;
        }
        else {
        //  cs.To_R_D__c='True';
        //  cs.Num_of_Upgrade__c=cs.Num_of_Upgrade__c+1;
            cs.Status='Waiting for upgrade';
        //  cs.R_D_Start_Time__c=datetime.now();
            update(cs);
        }       
    }   
    
    public void init(){ 
        
        String usrId=UserInfo.getUserId();
        Id profileId=UserInfo.getProfileId();  
        
        Profile pf=[select id,Name from Profile where id = :profileId];
        //if(pf==null)return;
        if(pf.Name.indexOf('HPE')<0 && pf.Name.indexOf('PSE')<0 && pf.Name.indexOf('Administrator')<0){haveError=true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '简档不符，无法上升研发(Just for HPE/PSE profile.)')); 
            return;
        }       
        Case cs=[Select c.id
            ,c.Owner.Id
            ,c.Owner.ProfileId
            ,c.Num_of_Upgrade__c
            ,c.To_R_D__c
            ,c.Status
            ,c.Closed__c
            ,c.R_D_Start_Time__c
            from Case c  
            where c.id=:currId];
        if(cs==null)return;
        if(cs.Status=='Upgrade to R&D' || cs.Status=='R&D Implementation'){haveError=true; 
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '已上升研发处理(The case has been upgrade to R&D.)')); 
            return;
        } 
        if(cs.Closed__c=='True'){haveError=true; 
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '该问题已关闭，不能上升研发(The case is closed, cant upgrade.)')); 
            return;
        }
        else {
            //cs.To_R_D__c='True';
            //cs.Num_of_Upgrade__c=cs.Num_of_Upgrade__c+1;
            cs.Status='Waiting for upgrade';
            //cs.R_D_Start_Time__c=datetime.now();
            update(cs);
        }       
    }
    
    public void submit(){       
        
        Case cs=[Select 
        Id, 
        Problem_Priority__c,
        Type,
        Fault_Type__c,
        Region__c, 
        Office__c,
        PBI_Product__c, 
        L1_Presenter__c,
        L1_Presenter_s_Phone_Number__c, 
        Servere_accident_level__c, 
        AccountId,
        Resolve_Due_Date_Time__c,
        Subject,
        Description,
        Occurred_Time__c, 
        ConfirmedKeyOffice__c,
        Key_Office_Name__c,
        Owner_Phone__c,
        Status,
        Num_of_Upgrade__c,
        Closed__c
        from Case where id=:currId];
                
        if(cs.Problem_Priority__c==null || cs.Type==null || cs.Fault_Type__c==null ||
        cs.Region__c==null || cs.Office__c==null || cs.PBI_Product__c==null || 
        cs.L1_Presenter__c==null || cs.L1_Presenter_s_Phone_Number__c==null ||
        cs.AccountId==null || cs.Resolve_Due_Date_Time__c==null || cs.Subject==null ||
        cs.Description==null || cs.Occurred_Time__c==null || cs.ConfirmedKeyOffice__c==null ||
        cs.PBI_Product__c==null){haveError=true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '信息不完整，无法提交，请先编辑补充完整信息')); 
            return;
        }
        
        if(cs.ConfirmedKeyOffice__c=='Yes' && cs.Key_Office_Name__c==null){haveError=true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '请填写重点局名称(Key Office Name is required.)')); 
            return;
        }
        
        if(cs.Owner_Phone__c==null) {haveError=true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Owner电话信息不存在，请先到个人信息页面填写该信息(Owner phone is required, please fill it in your personal infromation)')); 
            return;
        }
        if(cs.Problem_Priority__c=='Critical' && cs.Servere_accident_level__c==null){
            haveError=true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '请填写Servere accident level(Servere accident level is required.)')); 
            return;
        }
        if(cs.Status=='Upgrade to R&D' || cs.Status=='R&D Implementation'){haveError=true; 
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '已上升研发处理(The case has been upgrade to R&D.)')); 
            return;
        } 
        if(cs.Closed__c=='True'){haveError=true; 
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '该问题已关闭，不能上升研发(The case is closed, cant upgrade.)')); 
            return;
        }
        else{
            cs.To_R_D__c='True';
            cs.Num_of_Upgrade__c=cs.Num_of_Upgrade__c+1;
            cs.Status='Upgrade to R&D';
        //  cs.R_D_Start_Time__c=datetime.now();
            update(cs);
        }
        
    }
        
}