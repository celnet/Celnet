/*
 * Author: sunny.sun@celnet.com.cn
 * Date: 2014-8-27
 * Description: 启用batch
 */
public class Lepus_QueueBatchStartHandler implements Triggers.Handler{
	public static boolean isFirstRun = true;
    public void handle(){
        if(Lepus_QueueBatchStartHandler.isFirstRun){
        	isFirstRun = false;
        	
        	List<String> list_Status = new List<String>{'Queued','Processing','Preparing'}; 
        	
        	if([Select Id From AsyncApexJob a Where Status in: list_Status And (ApexClass.Name = 'Lepus_QueueBatch' Or ApexClass.Name = 'Lepus_FailureReSyncScheduleJob') limit 1].size() == 0){
        		try{
        			Lepus_QueueBatch qb = new Lepus_QueueBatch();
					database.executeBatch(qb, 1);
        		} catch (Exception e){
        			System.debug(e.getMessage());
        		}
        	}
        }
    }
}