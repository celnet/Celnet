/*
 * @Purpose : 海外机会点POC创建时自动创建一个客户的POC,更新时自动刷新客户的POC,删除时自动解除关联
 * @Author : Steven
 * @Date : 2013-11-04
 */
public without sharing class PocInOpportunityHandler implements Triggers.Handler{ //before insert before update before delete
	public static Boolean isFirstRun = true;
	public void handle() {
System.debug('PocInOpportunityHandler begin:::::');
		if(!PocInOpportunityHandler.isFirstRun) return;//如果是被PocHandler触发直接返回
		PocHandler.isFirstRun = false;//禁止触发PocHandler
		//Type1：如果是新创建的机会点POC,先创建一个客户的POC
		if(Trigger.isInsert){
			for(POC_In_Opportunity__c pocInopp : (List<POC_In_Opportunity__c>)Trigger.new){
				//判断是否选择了POC，如果选择了POC以选择的为准，并更新机会点的POC相关字段
				if(pocInopp.POC__c != null) {
					List<POC__c> pocList = 
						[Select name,Main_Products__c,POC_Completion_Date__c,POC_Failed_Reason_Analysis__c,Opportunity__c,
										POC_Records__c,POC_Start_Date__c,POC_Status__c FROM POC__c where id =: pocInopp.POC__c];
					for(POC__c p : pocList){
						//判断POC是否已经被引用
						if(p.Opportunity__c != null){
							//pocInopp.addError('This POC has been used by Opportunity:'+p.Opportunity__c);
							Opportunity opp = [select id, Name from Opportunity where id = :p.Opportunity__c limit 1];
							pocInopp.addError('This POC has been used by Opportunity: ' + opp.Name);
							return;
						}
						//更新机会点POC界面显示的相关字段的值
						pocInopp.New_POC_Name__c = p.name;
						pocInopp.Main_Products__c = p.Main_Products__c;
						pocInopp.POC_Completion_Date__c = p.POC_Completion_Date__c;
						pocInopp.POC_Failed_Reason_Analysis__c = p.POC_Failed_Reason_Analysis__c;
						pocInopp.POC_Records__c = p.POC_Records__c;
						pocInopp.POC_Start_Date__c = p.POC_Start_Date__c;
						pocInopp.POC_Status__c = p.POC_Status__c;
						//更新POC的机会点字段
						p.Opportunity__c = pocInopp.Opportunity__c;
						update p;
					}
				} else {
				//如果没有选择POC，根据填写的信息插入一个客户POC，并更新机会点POC字段
					//检查必填字段是否都填写了
					if(checkRequiredField(pocInopp))continue;
					//插入一条POC
					POC__c newPoc = new POC__c();
					newPoc.name = pocInopp.New_POC_Name__c;
					newPoc.Account__c = pocInopp.Account__c;
					newPoc.Opportunity__c = pocInopp.Opportunity__c;
					newPoc.Main_Products__c = pocInopp.Main_Products__c;
					newPoc.POC_Completion_Date__c = pocInopp.POC_Completion_Date__c;
					newPoc.POC_Failed_Reason_Analysis__c = pocInopp.POC_Failed_Reason_Analysis__c;
					newPoc.POC_Records__c = pocInopp.POC_Records__c;
					newPoc.POC_Start_Date__c = pocInopp.POC_Start_Date__c;
					newPoc.POC_Status__c = pocInopp.POC_Status__c;
					insert newPoc;
					//更新机会点POC界面显示的机会点字段值
					pocInopp.POC__c = newPoc.id;
				}
			}
		}
		//Type2：如果是修改机会点POC,先更新关联的客户POC
		if(Trigger.isUpdate){
			for(POC_In_Opportunity__c pocInopp : (List<POC_In_Opportunity__c>)Trigger.new){
				//POC字段不允许为空
				if(pocInopp.POC__c == null) { 
					pocInopp.addError('POC can not be null.');
					continue;
				}
				//检查必填字段是否都填写了
				if(checkRequiredField(pocInopp)) {
					continue;
				}
				//检查是否更新了备注信息
				String newPocRecords = pocInopp.POC_Records__c;
				String oldPocRecords = ((POC_In_Opportunity__c)Trigger.oldMap.get(pocInopp.id)).POC_Records__c;
				if(newPocRecords == null || newPocRecords == '' || newPocRecords == oldPocRecords) {
					pocInopp.addError('POC records must be updated when you update the POC.');
					continue;
				}
				List<POC__c> updatePocList = [Select id,name,Main_Products__c,POC_Completion_Date__c,Opportunity__c,
																				 POC_Failed_Reason_Analysis__c,POC_Records__c,POC_Start_Date__c,POC_Status__c
														  			FROM POC__c where id =: pocInopp.POC__c];
				for(POC__c updatePoc : updatePocList){
					updatePoc.name = pocInopp.New_POC_Name__c;
					updatePoc.Opportunity__c = pocInopp.Opportunity__c;
					updatePoc.Main_Products__c = pocInopp.Main_Products__c;
					updatePoc.POC_Completion_Date__c = pocInopp.POC_Completion_Date__c;
					updatePoc.POC_Failed_Reason_Analysis__c = pocInopp.POC_Failed_Reason_Analysis__c;
					updatePoc.POC_Records__c = pocInopp.POC_Records__c;
					updatePoc.POC_Start_Date__c = pocInopp.POC_Start_Date__c;
					updatePoc.POC_Status__c = pocInopp.POC_Status__c;
					update updatePoc;
				}
			}
		}
		//Type3：如果是删除机会点POC,先更新关联的客户POC，将关联的机会点字段置空
		if(Trigger.isDelete){
			for(POC_In_Opportunity__c delPocInopp : (List<POC_In_Opportunity__c>)trigger.old){
				List<POC__c> deletePocList = [Select id,Opportunity__c FROM POC__c where id =: delPocInopp.POC__c];
				for(POC__c poc : deletePocList){
					poc.Opportunity__c = null;
					update poc;
				}
			}
		}
	} // handle end
	
	private boolean checkRequiredField(POC_In_Opportunity__c pocInopp) {
		boolean hasError = false;
		if(pocInopp.New_POC_Name__c == null || pocInopp.New_POC_Name__c == '' ||
			 pocInopp.Main_Products__c == null || pocInopp.Main_Products__c == '' ||
			 pocInopp.POC_Completion_Date__c == null || pocInopp.POC_Start_Date__c == null || 
			 pocInopp.POC_Status__c == null) {
			pocInopp.addError('"New POC Name","POC Status","Main Products","POC Start Date" and "POC Completion Date" can not be null.');
			hasError = true;
		} 
		return hasError;
	}
	
}