/**
 * The controller of corresponding AccountNAChanged.page ,used in the change na button on account
 * The user need confirm on some situation,so can't update the parent field in page layout directly
 *
 * Author           : BLS
 * Create Date      : 2013/3
 * 
 *	@Update
 *	加入判断对于NA父子客户，子客户的属性不允许修改      Brain
 *	2013-4
 */   
public without sharing class AccountNAChanged {
	public Account account {get;set;}
	public Boolean ableToModify {public get;private set;}//关联父客户时不允许修改IsNAAcc属性
	public Boolean needConfirm {get;set;}//判断是否需要提示用户
	public Named_Account__c na {get;set;}
	public Boolean hasNAParent {get;set;}//对于NA父子客户，子客户的属性不允许修改
	
	public AccountNAChanged(ApexPages.StandardController stdCon){
		this.account = [select id,Is_Named_Account__c,Named_Account_Level__c,Parent.Is_Named_Account__c,
			ParentId,Named_Account_Property__c,NA_Approved_Date__c,name,Named_Account__r.Is_Named_Account__c,
			Named_Account_Character__c,Is_403_Customer__c from Account where Id = : stdCon.getId()];
		ableToModify = true;
		hasNAParent = false;
		if(account.ParentId != null){
			ableToModify = false;//此时不允许修改is na属性,只能修改其他信息
			if( account.Parent.Is_Named_Account__c ){//对于NA父子客户，子客户的属性不允许修改
				hasNAParent = true;
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 
    	 		System.Label.No_Privilege));
			}
		}
		Integer childrenNum = [select count() from Account where ParentId = : stdCon.getId()];
		needConfirm = false;
		if(childrenNum > 0){
			needConfirm = true;
		}
		na = new Named_Account__c(Account_Name__c = account.id,Approved_Date__c = Date.today(),
			Is_Named_Account__c = account.Is_Named_Account__c,
			Named_Account_Level__c = account.Named_Account_Level__c,
			Named_Account_Character__c = account.Named_Account_Character__c,
			Is_403_Customer__c = account.Is_403_Customer__c,
			Named_Account_Property__c = account.Named_Account_Property__c,
			Record_Description__c = 'User created');//初始化页面中的输入内容
	}
	
	public PageReference save(){
		try{
			na.Is_Named_Account__c = account.Is_Named_Account__c;
			na.Named_Account_Character__c = account.Named_Account_Character__c;
			na.Is_403_Customer__c = account.Is_403_Customer__c;
			na.Named_Account_Property__c = account.Named_Account_Property__c;
			na.Named_Account_Level__c = account.Named_Account_Level__c;
			na.Approved_Date__c = account.NA_Approved_Date__c;
			insert na;//insert na instead of update the fields on account directly,so we can keep track of the na history
			//the trigger on na will update account
			return new ApexPages.StandardController(account).view();
		}catch(Exception e){
			ApexPages.addMessages(e);
			return null;
		}	
	}
	
	
}