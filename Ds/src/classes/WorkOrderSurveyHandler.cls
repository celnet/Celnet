/*
* @Purpose : 
* 针对中国区记录:
* 当Overall Satisfactory首次被更新不为空时	
*										更新状态为Closed
*										更新回访时间=now()	
*	当“未成功回访原因”更新时	
*										更新回访时间=now()	
*
*
* @Author : Brain Zhang
* @Date :       2013-8
*
*
*/
public without sharing class WorkOrderSurveyHandler implements Triggers.Handler{
	public void handle(){
		Map<id,case> caseMap = new Map<id,case>();
		for (Work_Order__c w : (List<Work_Order__c>)trigger.new){//before update
			if(w.RecordTypeDeveloperName__c.startsWith('N_On_Site') && w.Overall_Satisfactory__c != null && 
				((Work_Order__c)trigger.oldmap.get(w.id)).Overall_Satisfactory__c ==null){//首次被更新不为空，再次更新的限制由	VR实现
				w.Status__c = 'Closed';
				w.Survey_Time__c = Datetime.now();
				if((w.Service_Request_No__c != null) &&
					 (caseMap.get(w.Service_Request_No__c) != null)){
					caseMap.get(w.Service_Request_No__c).Satisfactory_Level1_China__c = w.Overall_Satisfactory__c;//更新对应SR的满意度与Work Order相同
				}else if((w.Service_Request_No__c != null) &&
					 (caseMap.get(w.Service_Request_No__c) == null)){
					caseMap.put(w.Service_Request_No__c,
						new case(id=w.Service_Request_No__c,Satisfactory_Level1_China__c = w.Overall_Satisfactory__c));
				}
			}
			if(w.RecordTypeDeveloperName__c.startsWith('N_On_Site') && 
				(w.Survey_Rejected_Reason__c != ((Work_Order__c)trigger.oldmap.get(w.id)).Survey_Rejected_Reason__c)){//更新未回访原因
				w.Survey_Time__c = Datetime.now();
			}
		}
		/*
		2013-9
		按争航邮件要求:work order下的这两个操作先 暂时 注释掉，写上说明 ，不要删除，后续确认后根据需要再定。
		取消该段注释,会触发该功能:
		当Overall Satisfactory首次被更新不为空时
			3、	更新对应SR状态为Closed
			4、	更新对应SR的满意度与Work Order相同   
		
		取消注释时,请修改WorkOrderSurveyHandlerTest中的updateSRTest方法.以保证对应的测试代码通过
		
		if(!caseMap.isEmpty()){
			BypassVR__c customSetting = BypassVR__c.getInstance();
			customSetting.BypassVR__c = true;//use this to avoid the validation rule that forbid update the 
			insert customSetting;//service request's statisfactory field when there's work order
			System.debug('the current casemap:--------' + caseMap.values());
			update caseMap.values();
			delete customSetting;
		}
		*/
	}
}