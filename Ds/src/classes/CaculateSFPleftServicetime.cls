public without sharing class CaculateSFPleftServicetime implements Triggers.Handler{
	public void handle(){
		set <ID> woIds = new set<ID>();	
		set <ID> sfpIds = new set<ID>();				
		//插入和撤销删除时
		if((trigger.isInsert || trigger.isUnDelete)){
			for(Work_Order__c wo :(list<Work_Order__c>)trigger.new){					
				if((wo.RecordTypeDeveloperName__c.contains('Health_Check'))&&wo.SFP_No__c!=null
					&&wo.Status__c!='Cancelled')
					{
						woIds.add(wo.id);
						sfpIds.add(wo.SFP_No__c);
					}						
			}
		}
		//状态更新为取消
		if(trigger.isUpdate){			
			for(Work_Order__c wo :(list<Work_Order__c>)trigger.new){					
				if((wo.RecordTypeDeveloperName__c.contains('Health_Check'))&&wo.SFP_No__c!=null
					&&wo.Status__c=='Cancelled'&& wo.Status__c!=((Work_Order__c)trigger.oldMap.get(wo.id)).Status__c)
					{
						woIds.add(wo.id);
						sfpIds.add(wo.SFP_No__c);	
					}						
			}
		}
		//删除非取消状态的工单时
		if(trigger.isDelete){
			for(Work_Order__c wo :(list<Work_Order__c>)trigger.old){					
				if((wo.RecordTypeDeveloperName__c.contains('Health_Check'))&&wo.SFP_No__c!=null
					&& wo.Status__c!='Cancelled')
					{
						woIds.add(wo.id);
						sfpIds.add(wo.SFP_No__c);	
					}						
			}
		}		
		List<Work_Order__c> woLists =[select SFP_No__c from Work_Order__c where id in : woIds and Status__c!='Cancelled'];
		List<SFP__c> sfpLists =[select id,No_of_Health_Check_Service__c,No_of_Health_Check_Service_Used__c,No_of_Health_Check_Service_Left__c
				 	from SFP__c where id in :sfpIds];
		if(sfpLists.size()>0){			
			for(SFP__c sfp :sfpLists){
				integer heathWo =0;
				if(woLists.size()>0){
					for(Work_Order__c wo :woLists ){
						if(wo.SFP_No__c == sfp.Id){
							heathWo+=1;
						}
					}
				}else{
					heathWo=0;
				}
				
				sfp.No_of_Health_Check_Service_Used__c =heathWo;				
				update sfp;
				
			}
		}	
					 	
	}
}