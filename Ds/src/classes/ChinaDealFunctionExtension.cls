/**
 * @Purpose : 1、点击分发按钮将报备分发给指定的人
 *				    2、点击驳回按钮将当前报备驳回
 * @Author : Steven
 * @Date : 2014-09-03
 */
public class ChinaDealFunctionExtension {
	public Deal_Registration__c chinaDeal {get;set;}
	public Boolean isDistribute {get;set;} //用于控制界面是否显示手动分配的报备责任人字段
	public Boolean isRejected {get;set;}   //用于控制界面是否显示报备驳回原因字段

	public ChinaDealFunctionExtension(ApexPages.StandardController controller) { 
		//Step1: 通过传入的ID查询报备关键字段信息；
		Id chinaDealId = ApexPages.currentPage().getParameters().get('id');
		if(chinaDealId != null) {
			this.chinaDeal = 
				[Select id,OwnerId,Deal_Status__c, Rejected_Reason__c from Deal_Registration__c where id=:chinaDealId];
		}
		//Step2: 根据传入的操作类型，初始化界面显示
		String operType = ApexPages.currentPage().getParameters().get('type');
		this.isDistribute = false;
		this.isRejected = false;
		if(operType=='rejected') {
			chinaDeal.Deal_Status__c = 'Registration Failed';
			this.isRejected = true;
		} else if(operType=='distribute') {
			chinaDeal.OwnerId = null;
			this.isDistribute = true;
		}
	}
	
	/**
	 * 保存分发或失效的报备信息
	 */
	public PageReference save(){
		try{
			update(chinaDeal); 
			return new ApexPages.StandardController(chinaDeal).view();
		}catch(Exception e){
			return null;
		}
	}
	
	/**
	 * 返回报备页面
	 */
	public PageReference cancel(){
		return new ApexPages.StandardController(chinaDeal).view();
	}
}