public without sharing class AccoutUpdateCSScontractIndustryHandler  implements Triggers.Handler{//after update
	public static Boolean isFirstRun = true;
	public void handle() {
		if(AccoutUpdateCSScontractIndustryHandler.isFirstRun){
			Set<Id> accIds = new Set<Id>();
        for (Account newAcc : (List<Account>)trigger.new) {
            if (newAcc.Industry != ((Account)trigger.oldMap.get(newAcc.Id)).Industry
            ||newAcc.Sub_industry_HW__c != ((Account)trigger.oldMap.get(newAcc.Id)).Sub_industry_HW__c ) {
                accIds.add(newAcc.Id);
            }
        }
        system.debug('hewei++++++++++++++++++++++++'+accIds.size()); 
        if (accIds.size() == 0) {
            return;
        }                     
        list<CSS_Contract__c> cssContracts =new list<CSS_Contract__c>();
        for (CSS_Contract__c cssContract : [SELECT Industry__c,Sub_Industry__c, Account_Name__r.Industry,
        						Account_Name__r.Sub_industry_HW__c 
                                FROM CSS_Contract__c 
                                WHERE Account_Name__c IN :accIds ]) {
            cssContract.Industry__c = cssContract.Account_Name__r.Industry;
            cssContract.Sub_Industry__c = cssContract.Account_Name__r.Sub_industry_HW__c;
            cssContracts.add(cssContract);
        }              
        try {
            System.debug('cssContracts==============>' + cssContracts);            
            update cssContracts;
        }
        catch (DMLException e) {trigger.new[0].addError(e.getDMLMessage(0));
            System.debug('AccoutUpdateCSScontractIndustryHandler Exception: ' + e);
        }
			AccoutUpdateCSScontractIndustryHandler.isFirstRun = false;//just need to run once
		}
	}

}