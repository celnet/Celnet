/*
*
*	 控制LEAD的跳转,如果为MKT用户,转到专门的vf页面.否则转回标准页面
* @Author Brain
* 2013-4
*/
public with sharing class LeadRedirect {
	
	private ApexPages.StandardController controller;
	public String retURL {get; set;}
	public String saveNewURL {get; set;}
	public String rType {get; set;}
	public String cancelURL {get; set;}
	public String ent {get; set;}
	public String confirmationToken {get; set;}
	public String accountID {get; set;}
	public String contactID {get; set;}
	private String originURL {get;set;}
	
	public LeadRedirect(ApexPages.StandardController controller) {
	
		this.controller = controller;
	
		retURL = ApexPages.currentPage().getParameters().get('retURL');
		rType = ApexPages.currentPage().getParameters().get('RecordType');
		cancelURL = ApexPages.currentPage().getParameters().get('cancelURL');
		ent = ApexPages.currentPage().getParameters().get('ent');
		originURL = ApexPages.currentPage().getUrl();
	}
	
	public PageReference redirect() {
	
		PageReference returnURL;
		String naLeadRecordTypeId = CONSTANTS.NALEADRECORDTYPE;
		System.debug('the naLeadRecordTypeId:-----' + naLeadRecordTypeId.substring(0,15));
		Profile currentUserProfile = [select id,Name from Profile where id = :Userinfo.getProfileId()];
		IF(rType == naLeadRecordTypeId.substring(0,15)) {
			
			returnURL = Page.LeadNewNA;  
			returnURL.getParameters().put('retURL', retURL);
			returnURL.getParameters().put('RecordType', rType);
			returnURL.getParameters().put('cancelURL', cancelURL);
			returnURL.getParameters().put('ent', ent);
	
		}
		ELSE {
			 returnURL = new PageReference('/00Q/e');
					returnURL.getParameters().put('retURL', retURL);
			returnURL.getParameters().put('RecordType', rType);
			returnURL.getParameters().put('cancelURL', cancelURL);
			returnURL.getParameters().put('ent', ent);
			returnURL.getParameters().put('nooverride', '1');
		}
	
	/*
		IF (accountID != null){
	
			returnURL.getParameters().put('def_account_id', accountID);
	
		}
	
		IF (contactID != null){
	
			returnURL.getParameters().put('def_contact_id', contactID);
	
		}
	*/
		returnURL.setRedirect(true);
		return returnURL;
	
	}

}