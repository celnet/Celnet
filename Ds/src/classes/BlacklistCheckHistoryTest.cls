/**
 * The test class for BlacklistCheckHistory.trigger
 * Author           : Jen Peng
 * Create Date      : 2013/5
 *     
 */ 
@isTest
private class BlacklistCheckHistoryTest {

    static testMethod void addNewHistoryTest() {
        // TO DO: implement unit test
        UtilUnitTest.createAccount('test001');
    	Account acc = [select id,name from Account where name='test001'];
    	acc.blacklist_type__c='Tier I';
    	update acc;
        Blacklist_Check_History__c b = [select id,Account__c,isNew__c from Blacklist_Check_History__c where Account__c=:acc.id];
        system.assertEquals(b.isNew__c, true);
    }
}