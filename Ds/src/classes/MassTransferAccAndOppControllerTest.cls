/*
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-6-24
 * Description: 测试MassTransferAccAndOppController
 */
@isTest
public with sharing class MassTransferAccAndOppControllerTest {
	static testmethod void test(){
		List<Opportunity> oppList = new List<Opportunity>();
		for(Integer i= 0;i<4;i++){
			Account acc1 = UtilUnitTest.createHWChinaAccount('acc'+i);
			Opportunity opp = new Opportunity(AccountId = acc1.Id, Name = 'TEST Opportunity' + i,
                                          StageName = 'SS1 (Notifying Opportunity)',
                                          CloseDate = Date.today(), RecordTypeId = CONSTANTS.CHINAOPPORTUNITYRECORDTYPE,
                                          Opportunity_Level__c = '', Master_Channer_Partner__c = 'test');
            oppList.add(opp);
		}
		insert oppList;
        
		MassTransferAccAndOppController controller = new MassTransferAccAndOppController();
		controller.getRepresentativeOfficeOptions();
		controller.query();
		controller.selectedRepresentativeOffice = 'null';
		controller.query();
		controller.paginator.first();
		System.assertEquals(4,controller.paginator.recordCount);
		controller.awList[0].newowner.OwnerId = UserInfo.getUserId();
		controller.save();
		
		controller.selected = 'opportunity';
		controller.query();
		controller.paginator.first();
		System.assertEquals(4,controller.paginator.recordCount);
		controller.owList[0].newowner.OwnerId = UserInfo.getUserId();
		controller.save();
		
	}
}