@isTest(SeeAllData = true)
private class PocInOpportunityHandlerTest {
	static testMethod void myUnitTest() {
		Opportunity opp = [select id,AccountId from Opportunity where RecordTypeId = '01290000000d7M6' limit 1];
		POC_In_Opportunity__c pocInopp = new POC_In_Opportunity__c();
		pocInopp.New_POC_Name__c = 'test';
		pocInopp.Main_Products__c = 'test';
		pocInopp.POC_Completion_Date__c = System.today();
		pocInopp.POC_Failed_Reason_Analysis__c = 'test';
		pocInopp.POC_Records__c = 'test';
		pocInopp.POC_Start_Date__c = System.today();
		pocInopp.POC_Status__c = 'test';
		pocInopp.Opportunity__c = opp.id;
		pocInopp.Account__c = opp.AccountId ;
		
		POC__c poc = new POC__c();
		poc.name = 'test1';
		poc.Main_Products__c = 'test1';
		poc.POC_Completion_Date__c = System.today();
		poc.POC_Failed_Reason_Analysis__c = 'test1';
		poc.POC_Records__c = 'test1';
		poc.POC_Start_Date__c = System.today();
		poc.POC_Status__c = 'test1';
		poc.Account__c = opp.AccountId ;

		POC_In_Opportunity__c pocInopp1 = new POC_In_Opportunity__c();
		pocInopp1.Opportunity__c = opp.id;
		pocInopp1.Account__c = opp.AccountId ;
		
		Test.startTest();
		insert poc;
 		insert pocInopp;
 		pocInopp.POC_Records__c = 'testupdate';
 		update pocInopp;
 		delete pocInopp;
 		pocInopp1.POC__c = poc.id;
 		insert pocInopp1;
    Test.stopTest();
	}
}