/**
 * @Purpose : 1、检查修改后的中国区最终客户CIS编码是否重复，如果重复给客户的Owner发送邮件提醒；
 *            2、检查修改后的海外最终客户CIS编码是否重复，如果重复弹出错误提示；
 * @Author :  steven
 * @Date :		2014-07-18 update
 */
public without sharing class AccountCisRepeatRemindHandler implements Triggers.Handler{ //after update after insert
	public static Boolean isFirstRun = true;
	public void handle() {
		if(AccountCisRepeatRemindHandler.isFirstRun){
			isFirstRun = false;
			Set<id> cisChangedChinaAccIds = new Set<id>();
			Set<id> cisChangedOverseaAccIds = new Set<id>();
			for(Account acc : (List<Account>)trigger.new){
				//Step1-1：记录下中国区CIS编码发生变化的最终客户ID
				if(Trigger.isUpdate && acc.RecordTypeId == CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE && 
				  (acc.CIS__c != null) && (acc.CIS__c != '') && (acc.CIS__c != ((Account)trigger.oldMap.get(acc.id)).CIS__c)){
					cisChangedChinaAccIds.add(acc.id);
				}
				//Step1-2：记录下海外CIS编码发生变化的最终客户ID
				if(Trigger.isUpdate && acc.RecordTypeId == CONSTANTS.HUAWEIOVERSEACUSTOMERRECORDTYPE && 
				  (acc.CIS__c != null) && (acc.CIS__c != '') && (acc.CIS__c != ((Account)trigger.oldMap.get(acc.id)).CIS__c)){
					cisChangedOverseaAccIds.add(acc.id);
				}
				//Step1-3：记录下海外创建CIS编码不为空的最终客户ID
				if(Trigger.isInsert && acc.RecordTypeId == CONSTANTS.HUAWEIOVERSEACUSTOMERRECORDTYPE && 
				  (acc.CIS__c != null) && (acc.CIS__c != '')){
					cisChangedOverseaAccIds.add(acc.id);
				}
			}
			//Step2-1：中国区CIS编码重复不允许修改同时发邮件提醒
			if(!cisChangedChinaAccIds.isEmpty()) {
				for(Id currentAccId : cisChangedChinaAccIds){
					Account acc = (Account)trigger.newMap.get(currentAccId);
					Account accOld = (Account)trigger.oldMap.get(currentAccId);
					List<Account> accExistList = 
						[Select Name From Account Where CIS__c =:acc.CIS__c and id <>:currentAccId and RecordTypeId =:CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE];
					if(accExistList.size()>0){
						sendRemindMail(currentAccId,accExistList.get(0).Name,accOld.CIS__c); 
					}
				}
			}
			//Step2-2：海外客户CIS编码重复不允许修改同时弹出错误提示信息
			if(!cisChangedOverseaAccIds.isEmpty()) {
				for(Id currentAccId : cisChangedOverseaAccIds){
					Account acc = (Account)trigger.newMap.get(currentAccId);
					List<Account> accExistList = 
						[Select Name From Account Where CIS__c =:acc.CIS__c and id <>:currentAccId and RecordTypeId =:CONSTANTS.HUAWEIOVERSEACUSTOMERRECORDTYPE];
					if(accExistList.size()>0){
						String accName = accExistList.get(0).name;
						acc.addError('CIS code "'+ acc.CIS__c +'" has been used by account "'+accName+'"');
					}
				}
			}
			
		}
	}
	/**
	 * 发送CIS编码重复的提醒邮件
	 */
	private void sendRemindMail(String accId,String accExistName,String oldAccCis){
		try {
			Account accCurrent = [Select id,Name,CIS__c,Owner.Email From Account Where id =:accId];
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			List<string> toAddress = new List<string>{accCurrent.Owner.Email};
			mail.setToAddresses(toAddress);
			mail.setSenderDisplayName('Salesforce Supporter');
			mail.setSubject('客户关联CIS编码失败，该CIS编码已被引用');
			String strbody = '<p>您好：</p>';
			strbody += '<p>您申请的中国区客户"'+accCurrent.Name+'"关联CIS编码"'+accCurrent.CIS__c+'"失败，原因是Salesforce系统中已有客户"'+accExistName+'"使用了该CIS编码。</p>';
			strbody += '<p>请核对Salesforce中客户信息和所申请客户信息后再做相关数据处理。</p>';
			strbody += system.Url.getSalesforceBaseUrl().toExternalForm()+'/'+accId;
			mail.setHtmlBody(strbody);
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
			accCurrent.CIS__c=oldAccCis;
			update accCurrent;
		} catch(Exception e) {
			System.debug(e);
		}
	}
}