/*
* @Purpose : it is for email opps owner on the blacklist project 
*
* @Author : Jen Peng
* @Date : 		2013-5-23
*/
global class EmailOppsOwnersBatch implements Database.Batchable<sObject>,Database.Stateful{
	public Set<id> ownerids;
	public Map<id,Set<Account>> accIdMaps;
	public boolean isLocked;
	public String query;
	public Set<id> accountids;
	public List<Messaging.SendEmailResult> results{get;set;}
	
	global EmailOppsOwnersBatch(){
		ownerids= new Set<id>();
		accountids=new Set<id>();
		accIdMaps = new Map<id,Set<Account>>();
	}
	global Database.QueryLocator start(Database.BatchableContext bc){
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC, list<sObject> scope){
		for(sObject s : scope){
			Opportunity opp = (Opportunity)s;
			try{
				Account acc = [select Name, Id,blacklist_type__c,Blacklist_Status__c,Blacklist_Last_Type__c,Last_Blacklist_Status__c from Account where id=:opp.AccountId];
				ownerids.add(opp.OwnerId);
				accIdMaps.put(opp.OwnerId,new Set<Account>{acc});
			}catch(Exception e){
			
			}
			
		}
	}
	
	global void finish(Database.BatchableContext BC){
		List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
			for(Id i:ownerids){
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
				mail.setSaveAsActivity(false);
				mail.setTargetObjectId(i);
				String em = 'Dear Mr/Ms<br/>';
				if(isLocked){
					if(accIdMaps.get(i)!=null){
						for(Account ac:accIdMaps.get(i)){
							em+='Your Account '+ac.Name+' falls within the category of  Tier I Export Control Blacklist, with which the company is forbidden to enter into transaction according to our export control policy. Therefore, this account has been automatically locked  by the System, and any activities of the account will be terminated. You may track the information of the account through the following link: <br>';
							em+=system.Url.getSalesforceBaseUrl().toExternalForm()+'/'+ac.id;
							mail.setHtmlBody(em);
							mails.add(mail);
						}
					}
					
				}else{
					if(accIdMaps.get(i)!=null){
						
						for(Account ac:accIdMaps.get(i)){
							if(ac.blacklist_type__c=='Tier II' && ac.Blacklist_Last_Type__c=='Tier I'){
								em+='Your Account '+ac.Name+' is changed from the category of  Tier I Export Control Blacklist toTier II Export Control Blacklist , you MAY continue to deal with this account on the normal course of business; however, any transaction with this account will be subject to export control compliance review by EBG Legal Department. You may track the information of the account through the following link: <br>';
								em+=system.Url.getSalesforceBaseUrl().toExternalForm()+'/'+ac.id+'<br/>';
								em+='Should you have any question regarding the compliance review, please contact Ms. Yu XU (00205657).';
							}else if(ac.blacklist_type__c=='Pass' && ac.Blacklist_Last_Type__c=='Tier I'){
								em+='Your Account '+ac.Name+'  is removed from Export Control Blacklist now.  You can continue to deal with this account on the normal course of business. You may track the information of the account through the following link: <br>';
								em+=system.Url.getSalesforceBaseUrl().toExternalForm()+'/'+ac.id;
							}else if(ac.blacklist_type__c=='Tier II' && ac.Blacklist_Last_Type__c=='Pass'){
								em+='Your Account '+ac.Name+' falls within the category of  Tier II Export Control Blacklist. You may track the information of the account through the following link: <br>';
								em+=system.Url.getSalesforceBaseUrl().toExternalForm()+'/'+ac.id;
							}else if(ac.blacklist_type__c=='Pass' && ac.Blacklist_Last_Type__c=='Tier II'&&ac.Last_Blacklist_Status__c=='Red'){
								em+='Your Account '+ac.Name+'   is removed from Export Control Blacklist now.  You can continue to deal with this account on the normal course of business. You may track the information of the account through the following link: <br>';
								em+=system.Url.getSalesforceBaseUrl().toExternalForm()+'/'+ac.id;
							}else if(ac.blacklist_type__c=='Tier II' && ac.Last_Blacklist_Status__c=='Yellow' && ac.Blacklist_Last_Type__c=='Pending'){
								em+='Your Account '+ac.Name+' falls within the category of  Tier II Export Control Blacklist. You may track the information of the account through the following link: <br>';
								em+=system.Url.getSalesforceBaseUrl().toExternalForm()+'/'+ac.id;
							}
							mail.setHtmlBody(em);
							mails.add(mail);
						}
					}
					
				}
							
			}
			results=Messaging.sendEmail(mails);
			
			String con='';
			for(Id i:accountids){
				con+='\''+i+'\',';
			}
			con = '('+con.substring(0,con.length()-1)+')';
			EmailCasesOwnerBatch ec = new EmailCasesOwnerBatch();
			ec.isLocked=isLocked;
			ec.accountids=accountids;
			ec.query='Select OwnerId,AccountId From Case where AccountId in '+con;
			Id batchId = Database.executeBatch(ec,200);
			
	}
}