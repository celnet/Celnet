/*
	it is created for testing the  BlacklistHumanCheck controller on the blacklist project
	and it is created by jen peng on 2013-05-17;
*/
@isTest
private class BlacklistHumanCheckTest {

    static testMethod void blacklistHumanCheckTest() {
        // TO DO: implement unit test
        List<BlackList__c> bls=new List<BlackList__c>();
        //bls=UtilUnitTest.createBlackLists(0,20, 'blacklist', 'XX', 'new');
        bls=UtilUnitTest.createBlackLists(0,150, 'blacklist', 'XX', 'new');
        List<Account> accs = new List<Account>();
        //accs=UtilUnitTest.createAccounts(0,50,'blacklist');
        accs=UtilUnitTest.createAccounts(0,200,'blacklist');
        Test.startTest();		
         
       	BlacklistCheckBatch bcb = new BlacklistCheckBatch();
		bcb.query='select id,Name,Blacklist_Account_Alias__c,Country_Code_HW__c,Blacklist_Status__c,Last_Blacklist_Status__c from Account where Blacklist_Status__c=\'Gray\'';
		bcb.isChain=false;//it is added by jen on 2013.07.24
		//Id batchId = Database.executeBatch(bcb,50);
		Id batchId = Database.executeBatch(bcb,200);
		
		Test.stopTest();
		PageReference checkPage = Page.BlacklistHumanCheck;
		Test.setCurrentPageReference(checkPage);
		BlacklistHumanCheck bhc = new BlacklistHumanCheck();
		//system.assertEquals(bhc.getWrapps().size(), 5);
		system.assertEquals(bhc.getWrapps().size(), 50);
		system.assertEquals(bhc.pageCount,3);
		system.assertEquals(bhc.hasNext,true);
		bhc.next();
		system.assertEquals(bhc.hasPrevious,true);
		bhc.previous();
		system.assertEquals(bhc.hasPrevious,false);
		bhc.next();
		bhc.first();
		system.assertEquals(bhc.hasPrevious,false);
		system.assertEquals(bhc.hasNext,true);
		bhc.last();
		system.assertEquals(bhc.hasPrevious,true);
		system.assertEquals(bhc.hasNext,false);
		bhc.currentPageNumber=2;
		bhc.changeCurrentPageNumber();
		system.assertEquals(bhc.hasPrevious,true);
		system.assertEquals(bhc.hasNext,true);
		bhc.pageSize=Integer.valueOf(bhc.getItems().get(1).getValue());
		bhc.changePageSize();
		system.assertEquals(bhc.getWrapps().size(), 10);
		bhc.kw='blacklist_0';
		bhc.runSearch();
		system.assertEquals(bhc.getWrapps().size(), 1);
		bhc.showAll();
		system.assertEquals(bhc.getWrapps().size(), 50);
		bhc.getWrapps().get(0).acc.blacklist_type__c='Tier I';
		bhc.save();
		List<Account> a = [select id from Account where blacklist_type__c='Tier I'];
		system.assertEquals(1, a.size());
		PageReference pageCancel = bhc.cancel();
		system.assertEquals('/home/home.jsp', pageCancel.getUrl());

		
    }
}