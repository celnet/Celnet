public without sharing class OpportunityTriggerHandler {
	//更新opportunity的区域，国家，国家代码字段值
    public static void UpdateRegionCountry(List<opportunity> listOpportunities) {
		//获取Opportunity两种Record Type的Id
	   	String r1 = 'Enterprise_Business_Opportunity';
	   	String r2 = 'Enterprise_Business_Deal_Registration';
		//Id optyRecordType1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(r1).getRecordTypeId();
		//Id optyRecordType2 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(r2).getRecordTypeId();

	   	Set<Id> accountIds = new Set<Id>();
		for(Opportunity opt : listOpportunities){
	        if(opt.AccountId != null){
	        	accountIds.Add(opt.AccountId);
	        }
		}
		
		
		//Exit if no account Ids Ian Huang 2013-08-09
		if (accountIds.isEmpty()) return;

		
	    Map<Id, Account> mAccount = new Map<Id, Account>([Select Id, Region_HW__c, Representative_Office__c, Country_HW__c, Country_Code_HW__c 
	                                                    from Account where Id in:accountIds]);

		for (Opportunity opt : listOpportunities){
	        if(opt.AccountId != null){
	        	Account acc = mAccount.get(opt.AccountId);
	        	if(acc != null && (opt.RecordTypeDeveloperName__c == r1 || opt.RecordTypeDeveloperName__c == r2)){
	       			if(opt.Region__c == null){
	       				opt.Region__c = acc.Region_HW__c;
	       			}
	       			if(opt.Representative_Office__c == null){
	       				opt.Representative_Office__c = acc.Representative_Office__c;
	       			}
	       			if(opt.Country__c == null ){
	       				opt.Country__c = acc.Country_HW__c;
	       				opt.Country_Code__c = acc.Country_Code_HW__c;
	       			}
	        	}
	        }
		}
    }
    
    //更新opportunity相关的Deal_Registration中Deal Status字段值
    public static void UpdateDealRegStatus(List<opportunity> listOpportunities, Map<Id, opportunity>mapNewOpty, Map<Id, opportunity>mapOldOpty) {
        List<Opportunity> optlist = new List<Opportunity>();
        List<Deal_Registration__c> drUpdateList = new List<Deal_Registration__c>();

		for (Opportunity opt : [Select o.Id, o.AccountId, o.StageName, 
			(Select Deal_Status__c, LastModifyDateTime_Trigger__c From Deal_Registrations__r) 
								From Opportunity o
								Where Id In : listOpportunities]) {
			Deal_Registration__c[] drList = opt.Deal_Registrations__r;
			String StageName = opt.StageName;
            if (StageName == 'SSA (Huawei No Bid)' || StageName=='SSB (Customer Did Not Pursue)' || StageName=='SSC (Lost to Competition)'){
            	mapNewOpty.get(opt.id).NextStep = drList.size().format();
                for(Deal_Registration__c dr : drlist){
                    If(dr.Deal_Status__c != 'Abolished'){
                        dr.Deal_Status__c = 'Cancelled';
                        drUpdateList.Add(dr);
                    }
                }
            }

            if (StageName == 'SS7 (Implementing)' || StageName == 'SS8 (Completed)'){
                for(Deal_Registration__c dr : drlist){
                    If(dr.Deal_Status__c != 'Abolished'){
                        dr.Deal_Status__c = 'Closed';
                        
                        if (opt.StageName != mapOldOpty.Get(opt.Id).StageName ){
                        	dr.LastModifyDateTime_Trigger__c = system.now();
                        }
                        
                        drUpdateList.Add(dr);
                    }
                }
            }
            
            //Modify by czh 2013-2-8
            //如果opt.StageName更新为SS4及后续状态时，更新所有Deal
           	if (trigger.isUpdate){
			    if(  
			   		(StageName == 'SS4 (Developing Solution)' || StageName == 'SS5 (Gaining Agreement)' || StageName == 'SS6 (Winning)')
			        && opt.StageName != mapOldOpty.Get(opt.Id).StageName       
			        ){
			        for(Deal_Registration__c dr : drlist){
		                dr.LastModifyDateTime_Trigger__c = system.now();
		                drUpdateList.Add(dr);
			        }
				}
           	}
		}
		
	    if (drUpdateList.Size() > 0){
	        try {
	            update drUpdateList;
	        }catch (DMLException e) {
	            listOpportunities[0].addError(e.getDMLMessage(0));
	            System.debug('UpdateDealRegStatus. DMLException: ' + e.getDMLMessage(0));
	        }
	    }		    	
    }
    
	public static void ChangeOI(List<opportunity> listOpportunities, Map<Id, opportunity>mapNewOpty, Map<Id, opportunity>mapOldOpty) {
			//old stage set
			Set<String> stageSeta = new set<String>();
			stageSeta.add('SS1 (Notifying Opportunity)');
			stageSeta.add('SS2 (Identifying Opportunity)');
			stageSeta.add('SS3 (Validating Opportunity)');
			//new stage set
			Set<String> stageSetb = new set<String>();
			stageSetb.add('SS4 (Developing Solution)');
			stageSetb.add('SS5 (Gaining Agreement)');
			stageSetb.add('SS6 (Winning)');
			stageSetb.add('SS7 (Implementing)');
			stageSetb.add('SS8 (Completed)');

   //Ony retrieve opportunity owners Ian Huang 2013-08-09    
Set<Id> userIds = new Set<Id>();    
for (Opportunity opp : listOpportunities)       
 userIds.add(opp.OwnerId);       
 Map<Id,User> idToUserMap = new Map<Id,User>([SELECT Id, Name FROM User WHERE Id IN :userIds]);                
User user;

	        for(Opportunity opp : listOpportunities) {
	            //get old opp map
	            Opportunity oldOpp = mapOldOpty.get(opp.Id);
	            user = idToUserMap.get(opp.OwnerId);
	            //when update stage in SS1-SS3,OI can feldom change that.
	            System.debug('@1@######old OI :'+opp.Opportunity_Identifier__c+'old CheckboxValue:'+opp.TestTriggerChangeOIOnStageUpdate__c);
	            if(stageSeta.contains(opp.StageName) && user.id == opp.OwnerId && opp.TestTriggerChangeOIOnStageUpdate__c==false){
	                opp.Opportunity_Identifier__c = user.Name;
	                //opp.TestTriggerChangeOIOnStageUpdate__c = true;                                                    
	            } 
	            System.debug('@1@######New OI :'+opp.Opportunity_Identifier__c+'new CheckboxValue:'+opp.TestTriggerChangeOIOnStageUpdate__c);
	            System.debug('@2@######old OI :'+opp.Opportunity_Identifier__c+'old CheckboxValue:'+opp.TestTriggerChangeOIOnStageUpdate__c);
	            //when update stage in SS1-SS3 to SS4-SS8,OI will once change never to do that.当阶段从SS4以上变更为SS1的时候，原本的OI也不再改变
	            if(stageSeta.contains(oldOpp.StageName) && stageSetb.contains(opp.StageName) && user.id == opp.OwnerId && opp.TestTriggerChangeOIOnStageUpdate__c==false){
	                opp.Opportunity_Identifier__c = user.Name;
	                opp.TestTriggerChangeOIOnStageUpdate__c = true;
	            } 
	            System.debug('@2@######old OI :'+opp.Opportunity_Identifier__c+'old CheckboxValue:'+opp.TestTriggerChangeOIOnStageUpdate__c);
	            /*System.debug('@3@######old OI :'+opp.Opportunity_Identifier__c+'old CheckboxValue:'+opp.TestTriggerChangeOIOnStageUpdate__c);
	            //when update stage in SS4-SS8 to SS1-SS3,OI is never to change .
	            if(stageSetb.contains(oldOpp.StageName) && stageSeta.contains(opp.StageName) && user.id == opp.OwnerId && opp.TestTriggerChangeOIOnStageUpdate__c==true){
	                        opp.Opportunity_Identifier__c = user.Name;
	                        opp.TestTriggerChangeOIOnStageUpdate__c=true;
	            }    */                        
	            System.debug('@3@######old OI :'+opp.Opportunity_Identifier__c+'old CheckboxValue:'+opp.TestTriggerChangeOIOnStageUpdate__c);
	            //当ss4在回到ss4以下，oo还是继续改变
	            if(stageSetb.contains(opp.StageName) && user.id == opp.OwnerId){
					opp.Opportunity_Owner__c = user.Name;
					opp.TestTriggerChangeOOOnStageUpdate__c = true;  
	            }     
	            //再一次改回SS4以上，oo继续变
	            if(stageSeta.contains(opp.StageName) && user.Id == opp.OwnerId && opp.TestTriggerChangeOOOnStageUpdate__c==true){opp.Opportunity_Owner__c = user.Name;}     
	            System.debug('@4@######old OI :'+opp.Opportunity_Identifier__c+'old CheckboxValue:'+opp.TestTriggerChangeOIOnStageUpdate__c);                                 
	        }
 	}
 	
	//为了接口程序需要Representative Office， Province， City， System Department， Sub-Industry, Opportunity Level, Opportunity Attribution，Opportunity Type，Lead Source
	//这9个字段的 Code，将此9字段的值翻译成对应的Code
	//只针对Huawei China Deal Registration， Huawei China Opportunity 两种记录类型
 	public static void UpdateDictCode(List<opportunity> listOpportunities, Map<Id, opportunity>mapNewOpty, Map<Id, opportunity>mapOldOpty) {
 		Boolean bChanged = true;						//判断值是否更新
 		/*
 		if (trigger.isUpdate){
	 		for (Opportunity opt : listOpportunities){
	 			Opportunity oldOpt = mapOldOpty.get(opt.id);
	 			if(opt.Representative_Office__c == oldOpt.Representative_Office__c 
	 				&& opt.Province__c == oldOpt.Province__c 
	 				&& opt.City__c == oldOpt.City__c 
	 				&& opt.System_Department__c == oldOpt.System_Department__c 
	 				&& opt.Sub_Industry__c == oldOpt.Sub_Industry__c 
	 				&& opt.Opportunity_Level__c == oldOpt.Opportunity_Level__c 
	 				&& opt.Opportunity_Attribution__c == oldOpt.Opportunity_Attribution__c 
	 				&& opt.LeadSource == oldOpt.LeadSource 
	 				&& opt.Opportunity_Type__c == oldOpt.Opportunity_Type__c 
	 				){
	 					bChanged = false;
	 					break;
	 				}
	 		}
 		}
 		*/
		if (bChanged){ 		
			String r1 = 'Huawei_China_Deal_Registration';	//中国区项目报备
			String r2 = 'Huawei_China_Opportunity';			//中国区机会点
			/*
	 		if (UserInfo.getLanguage() == 'zh_CN'){
				r1 = '中国区项目报备';
				r2 = '中国区机会点';
	 		}
			Id optyRecordType1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(r1).getRecordTypeId();
			Id optyRecordType2 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(r2).getRecordTypeId();
			*/
	
			//名称与代码对应表
			Map<String, String> mapDict = new Map<String, String>();
			List<DataDictionary__c> dds = [select name__c, type__c, code__c from DataDictionary__c where Object__c= 'Opportunity'];
			for(DataDictionary__c dd : dds){
				mapDict.put(dd.type__c + '__' + dd.name__c, dd.code__c);
			}
	
			for (Opportunity opt : listOpportunities){
				if (opt.RecordTypeDeveloperName__c == r1 || opt.RecordTypeDeveloperName__c == r2){
					if (opt.Representative_Office__c != null){
						opt.Representative_Office_Code__c = mapDict.get('Representative Office__' + opt.Representative_Office__c);
					}else{
						opt.Representative_Office_Code__c = null;
					}
					if (opt.Province__c != null){
						opt.Province_Code__c = mapDict.get('Province__' + opt.Province__c);
					}else{
						opt.Province_Code__c = null;
					}
					if (opt.City__c != null){
						opt.City_Code__c = mapDict.get('City__' + opt.City__c);
					}else{
						opt.City_Code__c = null;
					}
					if (opt.System_Department__c != null){
						opt.System_Department_Code__c = mapDict.get('System Department__' + opt.System_Department__c);
					}else{
						opt.System_Department_Code__c = null;
					}
					if (opt.Sub_Industry__c != null){
						opt.Sub_Industry_Code__c = mapDict.get('Sub-Industry__' + opt.Sub_Industry__c);
					}else{
						opt.Sub_Industry_Code__c = null;
					}				
					if (opt.Opportunity_Level__c != null){
						opt.Opportunity_Level_Code__c = mapDict.get('Opportunity Level__' + opt.Opportunity_Level__c);
					}else{
						opt.Opportunity_Level_Code__c = null;
					}				
					/*
					* Use formula to get the code
					if (opt.Opportunity_Attribution__c != null){
						opt.Opportunity_Attribution_Code__c = mapDict.get('Opportunity Attribution__' + opt.Opportunity_Attribution__c);
					}
					else{
						opt.Opportunity_Attribution_Code__c = null;
					}
					*/				
					if (opt.LeadSource != null){
						opt.Lead_Source_Code__c = mapDict.get('Lead Source__' + opt.LeadSource);
					}else{
						opt.Lead_Source_Code__c = null;
					}				
					if (opt.Opportunity_Type__c != null){
						opt.Opportunity_Type_Code__c = mapDict.get('Opportunity Type__' + opt.Opportunity_Type__c);
					}else{
						opt.Opportunity_Type_Code__c = null;
					}
				}
			}
		}
 	}
 	/*此处无意义，新创建opportunity时，workflow会为此字段赋值
 	//新建opportunity时，设置Opportunity Owner为空
 	public static void SetOpportunityOwnerToNull(List<opportunity> listOpportunities){
        for(Opportunity opp : listOpportunities){
           opp.Opportunity_Owner__c = null;
        }
    }
    */
    
    public static void updateNALeadStatus(List<opportunity> listOpportunities){//after insert
    	List<Lead> naLeads = new List<Lead>();
    	for(Opportunity o : listOpportunities){
    		if(o.Lead__c != null){
    			Lead l = new Lead(id = o.Lead__c);
    			l.Status = '已转机会点';
    			naLeads.add(l);
    		}
    	}
    	update naLeads;
    }
}