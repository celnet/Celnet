@isTest
private class SalesTargetSharingHandlerTest {

    static testMethod void sharingTestNew() {
    	Account acc = new Account();
			acc.Name = 'TEST001';
			acc.cis__c = 'TEST123';
			acc.RecordTypeId = CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE;
			acc.Region_HW__c='China';
			acc.Representative_Office__c='系统部总部'; 
			acc.Country_HW__c='China';
			acc.Country_Code_HW__c='CN';
			acc.Province__c = '北京市';
			acc.City_Huawei_China__c = '北京市';
			acc.Industry = '大企业系统部';
			acc.Sub_industry_HW__c='大企业二部（石油化工、燃气、煤炭）';
			acc.Customer_Group__c = '能源-煤炭';
			acc.Customer_Group_Code__c = '7E';
			acc.Is_Named_Account__c = true;
			acc.blacklist_type__c='Pending';
			insert acc;
			
			Sales_Target_Accomplishment__c record = new Sales_Target_Accomplishment__c();
			record.RecordTypeId = CONSTANTS.SALESTARGETCHINANARECORDTYPE;
			record.Year__c = '2013';
			record.China_Account_Name__c = acc.id;
			record.China_Income_Target__c = 0;
			record.China_Order_Target__c = 0;
			insert record;
    }
    	
    /*
    static testMethod void sharingTest() {
        User u;
         User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
    System.runAs ( thisUser ) {
        Profile p = UtilUnitTest.getProfile('(Huawei China)代表处客户经理');
        UserRole role = UtilUnitTest.getRoleFromDeveloperName('Huawei_China92');//(Huawei China)上海代表处客户经理
        
        u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id,UserroleId = role.id,
      TimeZoneSidKey='America/Los_Angeles', UserName='mkt@huawei.testorg.com');
      insert u;
    }
        Sales_Target_Accomplishment__c record = new Sales_Target_Accomplishment__c(
            recordtypeid = CONSTANTS.SALESTARGETMANAGERECORDTYPE,Account_Manager__c = u.id);
        Test.startTest();
        insert record;
        UserRecordAccess recordAccess = [select recordid,HasReadAccess, HasEditAccess,
            MaxAccessLevel
         FROM UserRecordAccess where RecordId = :record.id and userid = :u.id];
      System.assertEquals(true,recordAccess.HasReadAccess);
      
      
        //UserRole salesManagerRole = [select id from Role where name = '(Huawei China)上海代表处客户经理'];
        List<user> salesManagers = [select id from User where  UserRole.Name = '(Huawei China)上海代表处销管' and IsActive = true];
        for(User s : salesManagers){
            System.debug('the manager user:---' + s);
            UserRecordAccess r = [select recordid,HasReadAccess, HasEditAccess,
            MaxAccessLevel
         FROM UserRecordAccess where RecordId = :record.id and userid = :s.id];
         //System.assertEquals('Edit',r.MaxAccessLevel);
         System.assertEquals(true,r.HasEditAccess);
        //Sales_Target_Accomplishment__Share re = [select AccessLevel,RowCause from  Sales_Target_Accomplishment__Share
          //   where  ParentId = :record.id and UserOrGroupId = :s.id];
        //System.debug('sharng:---' + re);
        //System.assertEquals('Edit',re.AccessLevel);
        }
        Test.stopTest();
    }*/
}