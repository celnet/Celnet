public class StddInstalledAsset {
    
   /*************************************************************************
    *
    *  2013-08-06
    *  Ian Huang
    *  1. retrieving all SFPs attached to current and parent EAs
    *  2. SOQL optimizations
    *  3. Tree optimizations
    *
    **************************************************************************/

    private String id;
    private transient List<Installed_Asset__c>          allInstalledAssets;
    private transient Map<Id, Installed_Asset__c>       parentInstalledAssetMap;
    private transient Map<Id, List<Installed_Asset__c>> childInstalledAssetsMap;
    public Id currentId ;
    public String currentName;
    // isOverseas: Judging whether current user is an overseas user
    public boolean isOverseas {get; set;}
    
    //overseasProList: used to store overseas profile information
    List<RecordType> overseasRTList = [SELECT Id
                                       FROM   RecordType
                                       WHERE  Name like 'HW-(Int CSS)%'
                                       AND    SobjectType =: 'Installed_Asset__c'];
    
    public transient Installed_Asset__c currentInstalledAsset {get;private set;}
    
    public StddInstalledAsset(ApexPages.StandardController controller) {
    	currentId = controller.getId();
    	  currentInstalledAsset = [SELECT  Id,(Select CreatedById, CreatedDate, Field, OldValue, NewValue From Histories),
                        Name,
                        FRU__c,
                        Spare_property__c,
                        Hard_disk_retention_service__c,
                        Service_quote_items__c,
                        OwnerId,                                          
                        RecordTypeId,
                        Account_Name__c,
		                City__c,
		                Other_City__c,
		                Region__c,
		                SubRegion__c,
		                Street__c,
		                Postal_Code__c,
		                Shipping_Date__c,
		                W_M_Start_Date__c,
		                Configuration_Information__c,
		                Product_Family__c,
		                Quantity__c,
		                BOM_Description__c,
		                BOM_Code__c,
		                Product_Version1__c, 
		                Contracts_No__c,
		                Filed__c,
		                Device__c,
                        
                        Solution_Device__c,
                        Solution_Device__r.Solution_Device__c,
		                Solution_Device__r.Id,
		                Solution_Device__r.Name,
		                
		                Product_Name1__c,
		                Product_Name__r.Name,
		                Product_Name__r.Version__c,
		                Product_Name__r.Product_Family__c,
                        //Service Requests
                        (SELECT Id,
                                Barcode__c, 
                                Type,
                                Status,
                                Owner.Name,
                                OwnerId,
                                CaseNumber
                         FROM   Service_Requests__r),
                        //Work Order Asset
                        (SELECT Id, 
                                Work_Order_No__c,
                                Work_Order_No__r.Id,
		                        Work_Order_No__r.Name,
		                        Work_Order_No__r.Type__c,
		                        Work_Order_No__r.Status__c,
		                        Work_Order_No__r.RecordType.Name,
		                        Work_Order_No__r.Owner.Name,
		                        Work_Order_No__r.Engineer_In_Charge__r.Name,
		                        Installed_Asset__c,
		                        Region__c,
		                        Sub_region__c,
		                        City__c
                         FROM Work_Order_Asset__r),
                        //CSS Contract 
                        Contracts_No__r.Id,
                        Contracts_No__r.Name,
                        Contracts_No__r.Type__c,
                        Contracts_No__r.Status__c,
                        Contracts_No__r.Contrac_No__c, 
                        Contracts_No__r.Contract_Signed_Date__c,
                        Contracts_No__r.Contract_Group__c
                 FROM   Installed_Asset__c
                 WHERE  Id = :controller.getId()];
    	 currentName = currentInstalledAsset.name;
        // OverSeasProIdSet: used to store the Ids of specific profile
        Set<Id> overseasRTIdSet = new Set<Id>();
                
        for (RecordType rt : overseasRTList){
            overseasRTIdSet.add(rt.Id);
        }
        //set isOverseas user based on EA record type visibility
        isOverseas = overseasRTIdSet.contains(currentInstalledAsset.RecordTypeId);
	      WorkOrderList = new List<Work_Order__c>();
	      for (Work_Order_Asset__c woa :currentInstalledAsset.Work_Order_Asset__r) {
                	if (woa.Work_Order_No__r != null){
                	    workOrderList.add(woa.Work_Order_No__r);
                	}                		
        		}
        
       
    }
    
    public String getIciUrl(){
        String ici = currentInstalledAsset.Configuration_Information__c;
        if (ici==null) ici = '';
        String u = EncodingUtil.urlEncode(ici,'UTF-8');
        return u;
    }
    
    public Set<Id> installedAssetIds;
    public void init(){
        /*
        *  改变以下代码注释方式会显示全部条码或仅直属上级条码
        */
        //searchToTop(currentInstalledAsset);//仅显示直属上级条码
        Id rootId = getRoot(currentInstalledAsset.id);//显示全部条码
        root = [select id,name from Installed_Asset__c where id =:rootId];//显示全部条码
        buildList(rootId);//显示全部条码
        
        /*
        *以上部分控制具体的显示形式,全部条码或仅直属上级条码
        */
                List<SFP_Asset__c> sfpAssets = [SELECT Id,  
								           	           SFP__c,
								           	           SFP__r.Id,
								                       SFP__r.Name,
								                       SFP__r.Type__c,
								                       SFP__r.RecordType.Name,
								                       SFP__r.Status__c,
												       SFP__r.Service_Package__c,
												       SFP__r.Service_Package__r.Name,
												       SFP__r.Service_Package__r.id,
												       SFP__r.SFP_No_1_12__c,
												       SFP__r.Contract_No__c,
												       SFP__r.Warranty_Start_Date__c,
												       SFP__r.Warranty_End_Date__c,
												       SFP__r.Maintenance_End_Date__c,
												       SFP__r.Maintenance_Start_Date__c,
												       SFP__r.Contract_No__r.Name
								    	        FROM   SFP_Asset__c
								    	        WHERE  Install_Asset__c IN :installedAssetIds 
								    	        and SFP__r.RecordType.developerName like '%Warranty_and_Maintenance%'];
                SFPList = new List<SFP__c>();
                Set<SFP__c> sfps = new Set<SFP__c>();
                for (SFP_Asset__c sa : sfpAssets){
                	sfps.add(sa.SFP__r);
                }
                SFPList.addAll(sfps);	    
    }
    
    
    public transient List<Work_Order__c> WorkOrderList {get;private set;}

    
    //returns all SFPs attached to current and its parent EAs
    public transient List<SFP__c> SFPList {get;private set;}
    
    public Account Account{
        get {
        	if (Account == null) {
                 Account= [SELECT   Id,
                                    Name,
			                        Type,
			                        ParentId,
			                        Parent.Name,
			                        Industry__c,
			                        Website,
			                        Sub_Region__c,
			                        Sub_Industry__c,
			                        Service_Level__c,
			                        Region__c,
			                        Rating__c,
			                        Phone,
			                        Fax,
			                        ERP_Account_Number__c,
			                        Approval_Status__c,
			                        Owner.Name,
			                        (SELECT Id, Name, AccountId FROM Contacts)
                           FROM     Account 
                           WHERE    Id = :currentInstalledAsset.Account_Name__c];
        	
        	}
        	return Account;
        }
        private set;
    }
    
    public List<Contact> getAccContactList(){
        return Account.Contacts;
    }
    
    public String getParentName(){
        if (Account.Parent != null)
        	return Account.Parent.Name;
        return '';
    }
    
    /*
    public PageReference editIA() {
        String url='/'+currentId+'/e?retURL='+currentId;
        PageReference pr=new PageReference(url);
        pr.setRedirect(true);
        return pr;
    }
    
    public PageReference deleteIA(){
        String url='/setup/own/deleteredirect.jsp?delID='+currentId+'&amp;retURL=%2Fhome%2Fhome.jsp';
        PageReference pr=new PageReference(url);
        pr.setRedirect(true);
        return pr;
    }
    
    public PageReference cloneIA(){
        String url='/'+currentInstalledAsset.id+'/e?clone=1&retURL=%2F'+currentId;
        PageReference pr=new PageReference(url);
        pr.setRedirect(true);
        return pr;
    }
    */
    public PageReference newServiceRequest(){
        String iaName=EncodingUtil.urlEncode(currentName,'UTF-8');
        
        String url='/setup/ui/recordtypeselect.jsp?CF00NO0000000Nkww='+iaName+'&ent=Case&retURL=%2F500%2Fo&save_new_url=%2F500%2Fe%3FretURL%3D%252F500%252Fo';
        PageReference pr=new PageReference(url);
        pr.setRedirect(true);
        return pr;
    }
    
    public PageReference newContact(){
        String url='/setup/ui/recordtypeselect.jsp?ent=Contact&retURL=%2F003%2Fo&save_new_url=%2F003%2Fe%3FretURL%3D%252F003%252Fo';
        PageReference pr=new PageReference(url);
        pr.setRedirect(true);
        return pr;
    }
    
    
    public List<Installed_Asset__c> rootAndChilds{get;set;} 
    
    public Installed_Asset__c root{get;set;} 

     //查找根节点Install Asset的Id
    public Id getRoot(Id theId) {
        Id s;
        Id sParentId;
        installedAssetIds = new Set<Id>{theId};
        sParentId = theId;
        while (true) {
            Installed_Asset__c a = [SELECT Id, Name, 
                                Solution_Device__r.id, 
                                Solution_Device__r.Solution_Device__r.id, 
                                Solution_Device__r.Solution_Device__r.Solution_Device__r.id,
                                Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.id,
                                Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.id
                         FROM   Installed_Asset__c
                         WHERE  Id = :sParentId];
            installedAssetIds.add(a.id);
            if(a.Solution_Device__r.Id != null) {
                s = a.Solution_Device__r.Id;
                installedAssetIds.add(s);
                if(a.Solution_Device__r.Solution_Device__c != null) {
                    s = a.Solution_Device__r.Solution_Device__r.Id;
		                installedAssetIds.add(s);
                    if(a.Solution_Device__r.Solution_Device__r.Solution_Device__r != null) {
                        s = a.Solution_Device__r.Solution_Device__r.Solution_Device__r.Id;
				                installedAssetIds.add(s);
                        if(a.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r != null) {
                            s = a.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.Id;
						                installedAssetIds.add(s);
                            if(a.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r != null) {
                                s = a.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.Id;
							                installedAssetIds.add(s);
                            }
                        }
                    }
                }
            }
            if (s != null && s!= sParentId){
                sParentId = s;
            }
            else{
                break;
            }
        }
        return sParentId;
    }
    
    public void buildList(Id rootId){
    	Map<id, Installed_Asset__c> iaMap = new Map<id,Installed_Asset__c>([SELECT Id, Name,
                     Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.Id  
              FROM   Installed_Asset__c 
              WHERE  id =:rootId 
              or     Solution_Device__r.Id = :rootId 
              OR     Solution_Device__r.Solution_Device__r.id = :rootId
              OR     Solution_Device__r.Solution_Device__r.Solution_Device__r.id = :rootId
              OR     Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.id = :rootId
              OR     Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.Id = :rootId ]);
      
      Set<Id> childIds = new Set<id>();//all children ids
      List<Installed_Asset__c> ias = iaMap.values();
       do {
            childIds.clear();
            for (Installed_Asset__c a : ias){
                if (a.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.Id != null){
	                childIds.add(a.Id);
                }
            }
            	
            
            if (childIds.isEmpty()) {
            	break;
            }
            
            ias = [SELECT Id, Name,
                     Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.Id  
              FROM   Installed_Asset__c 
              WHERE  id =:rootId 
              or     Solution_Device__r.Id = :rootId 
              OR     Solution_Device__r.Solution_Device__r.id = :rootId
              OR     Solution_Device__r.Solution_Device__r.Solution_Device__r.id = :rootId
              OR     Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.id = :rootId
              OR     Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.Id IN :childIds ];
            iaMap.putAll(ias);
        } while (!ias.isEmpty());
        rootAndChilds = iaMap.values();
        rootAndChilds.sort();
    }
    
    //给定当前记录,查询所有直接父项
    public void searchToTop(Installed_Asset__c current) {
    	Map<id, Installed_Asset__c> iaMap = new Map<id,Installed_Asset__c>();
        Installed_Asset__c rootRecord = current;
        Installed_Asset__c s;
        Installed_Asset__c sParent;
        installedAssetIds = new Set<Id>{current.id};
        sParent = current;
        while (true) {
            Installed_Asset__c a = [SELECT Id, Name, 
                                Solution_Device__r.id, 
                                Solution_Device__r.name, 
                                Solution_Device__r.Solution_Device__r.id, 
                                Solution_Device__r.Solution_Device__r.name, 
                                Solution_Device__r.Solution_Device__r.Solution_Device__r.id,
                                Solution_Device__r.Solution_Device__r.Solution_Device__r.name,
                                Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.id,
                                Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.name,
                                Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.id,
                                Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.name
                         FROM   Installed_Asset__c
                         WHERE  Id = :sParent.id];
             installedAssetIds.add(a.id);
            iaMap.put(a.id,a);
            if(a.Solution_Device__r.Id != null) {
              s = new Installed_Asset__c(id=a.Solution_Device__r.Id ,name = a.Solution_Device__r.name,
              	Solution_Device__c = a.Solution_Device__r.Solution_Device__r.id );
            	iaMap.put(s.Id, s);
                installedAssetIds.add(s.id);
                if(a.Solution_Device__r.Solution_Device__c != null) {
                     s = new Installed_Asset__c(id=a.Solution_Device__r.Solution_Device__r.Id ,
                     	name = a.Solution_Device__r.Solution_Device__r.name,
              	Solution_Device__c = a.Solution_Device__r.Solution_Device__r.Solution_Device__r.id );
            	iaMap.put(s.Id, s);
                installedAssetIds.add(s.id);
                    if(a.Solution_Device__r.Solution_Device__r.Solution_Device__r != null) {
                         s = new Installed_Asset__c(id=a.Solution_Device__r.Solution_Device__r.Solution_Device__r.Id ,
                         	name = a.Solution_Device__r.Solution_Device__r.Solution_Device__r.name,
              	Solution_Device__c = a.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.id );
            	iaMap.put(s.Id, s);
                installedAssetIds.add(s.id);
                        if(a.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r != null) {
                             s = new Installed_Asset__c(id=a.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.Id ,
                             name = a.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.name,
              	Solution_Device__c = a.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.id );
            	iaMap.put(s.Id, s);
                installedAssetIds.add(s.id);
                  if(a.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r != null) {
                     s = new Installed_Asset__c(id=a.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.Id ,
                     name = a.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.Solution_Device__r.name );
            	iaMap.put(s.Id, s);
                installedAssetIds.add(s.id);
                            }
                        }
                    }
                }
            }
            if (s != null && s!= sParent){
                sParent = s;
            }
            else{
                break;
            }
        }
         rootAndChilds = iaMap.values();
         root = sParent;
    }
        
}