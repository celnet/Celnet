global  class AutoCreateSFPTask_Batch implements Database.Batchable<Id>,Database.Stateful{
	public Set<Id> sfpIds;
	public Map<Id,Id>sfpTaskMap;
	
	
	global Iterable<Id> start(Database.BatchableContext bc)
	{
		List<Id>sfpId=new List<Id>();
		for(ID id:sfpIds){
			sfpId.add(id);
		}
		return sfpId;
	}

	global void execute(Database.BatchableContext BC, list<Id> scope)
	{
		try
		{
			//Task task = new Task();
			for(Id sfp:scope){
				Task task = new Task();				
				task.Subject='主动服务（健康检查/驻场/事件值守/单次服务）计划任务提醒';
				task.Description='请尽快提交：主动服务（健康检查/驻场/事件值守/单次服务）的任务计划';
				task.Status='Not Started';
				task.WhatId=sfp;
				task.ActivityDate=System.today();
				task.IsReminderSet=true;
				task.ReminderDateTime=System.today();
				task.OwnerId = sfpTaskMap.get(sfp);
				Database.DMLOptions dmlo = new Database.DMLOptions(); 
				dmlo.EmailHeader.triggerUserEmail = true; 
				database.insert(task, dmlo);
				//system.debug('hewei'+task.id);
			}
		}
		catch(Exception e)	
		{
			
		}		
		
	}
	
	global void finish(Database.BatchableContext BC)
	{
		System.debug('-----------finish------');
	}
}