/*
 * Author: Sunny.sun@celnet.com.cn
 * Date: 2014-8-27
 * Description: Lepus队列处理功能类
 */
public class Lepus_QueueService {
	public Boolean QueueHandle(Lepus_Queue__c Queue){
		Boolean syncSuccess = true;
		
		//数据同步
		try{
			if(Queue.SyncType__c == '业务数据同步'){
				syncSuccess = BusinessDataSync(Queue);
			}else if(Queue.SyncType__c == '字段更新同步'){
				syncSuccess = FieldUpdateSync(Queue);
			}else if(Queue.SyncType__c == '团队成员同步'){
				syncSuccess = syncTeamMember(Queue);
			} else if(Queue.SyncType__c == '线索字段更新'){
				leadHistorySync(Queue);
			} else {
				syncSuccess = BusinessDataSync(Queue);
			}
			
		}catch(exception e){
			Lepus_FailureHandler.handleBusinessDataFailure(Queue.RecordId__c, 'Exception Type:'+e.getTypeName()+' Exception Msg:'+e.getMessage(), Queue.Action__c, Queue.ObjectType__c);
		}
		
		//删除队列
		if(Queue.Id != null)
		Lepus_QueueManager.OutQueue(new List<ID>{Queue.Id});
		
		return syncSuccess;
	}
	//业务数据同步
	private Boolean BusinessDataSync(Lepus_Queue__c Queue){
		String sXml = BusinessDataXml(Queue);
		try{
			Lepus_WSDL_Info.tGetResponse response = Lepus_EIPCalloutService.sendToEIP(sXml, 'isaleslepus_lepusglobal_sync', Queue.Global_ID__c, 'CI0000194284');
			if( response != null && response.ResultStatus == 'true'){
				// 如果为insert操作并且对象为Account，Opportunity，Lead，则同步团队成员
				if(Queue.Action__c == 'insert' && (Queue.ObjectType__c == 'Account' || Queue.ObjectType__c == 'Opportunity' || Queue.ObjectType__c == 'Lead')){
					syncTeamMember(Queue);
				}
			} else{
        		Lepus_FailureHandler.handleBusinessDataFailure(Queue.RecordId__c, response == null?'no response':response.Error, Queue.Action__c, Queue.ObjectType__c);
        		return false;
        	}
		}catch(exception e){
			Lepus_FailureHandler.handleBusinessDataFailure(Queue.RecordId__c, 'Exception Type:'+e.getTypeName()+' Exception Msg:'+e.getMessage(), Queue.Action__c, Queue.ObjectType__c);
			return false ;
		}
		return true;
	}
	
	// 同步团队成员
	public boolean syncTeamMember(Lepus_Queue__c Queue){
		try{
			Schema.Sobjecttype sobjecttype = Lepus_SyncUtil.retrieveSobjectType(Queue.ObjectType__c);
			List<Sobject> sobjList = Lepus_SyncUtil.querySobjects(new Set<Id>{Queue.RecordId__c}, sobjecttype);
			Map<Id, List<Lepus_EIPMember>> memberMap = Lepus_SyncUtil.retrieveTeamMembers(sobjList, sobjecttype);
			String action = Queue.Action__c == 'delete'?'delete':'';
			String tXml = Lepus_EIPCalloutService.memberXmlConcatenation(memberMap, sobjecttype, action);
			
			Lepus_WSDL_Info.tGetResponse tResponse = Lepus_EIPCalloutService.sendToEIP(tXml, 'mchat_bizdatasync', '', 'CI0000194284');
	        
	        if(!(tResponse != null && tResponse.ResultStatus == 'true')){
	        	Lepus_FailureHandler.handleTeamMemberFailure(new List<Id>{Queue.RecordId__c}, tResponse == null?'no response':tResponse.Error, '');
	        	return false;
	    	} 
		} catch(Exception e){
			Lepus_FailureHandler.handleTeamMemberFailure(new List<Id>{Queue.RecordId__c}, 'Exception Type:'+e.getTypeName()+' Exception Msg:'+e.getMessage(), '');
			return false;
		}
		return true;
	}
	
	// 线索历史同步
	public void leadHistorySync(Lepus_Queue__c Queue){
		list<LeadHistory> leadHistories = Lepus_SyncUtil.queryLeadHistories(new List<Id>{Queue.RecordId__c});
        if(leadHistories.size() == 0)
        return;
        Lepus_FutureCallout.syncLeadHistoryCallout(new List<Id>{Queue.RecordId__c}, leadHistories);
	}
	
	//字段更新同步
	private Boolean FieldUpdateSync(Lepus_Queue__c Queue){
		Boolean success = true;
		List<OpportunityFieldHistory> list_oppFieldHistory = Lepus_SyncUtil.queryOppFieldHistories(Queue.RecordId__c, Queue.SyncTime__c);
		if(list_oppFieldHistory.size() == 0){
			return  success;
		}
		String sXML = Lepus_EIPCalloutService.FieldUpdateXmlConcatenation(list_oppFieldHistory);
		success = fieldUpdateCallout(sXML, Queue);
		
		return success;
	}
	
	public Boolean fieldUpdateCallout(String sXml, Lepus_Queue__c Queue){
		Boolean success = true;
		try{
			Lepus_WSDL_Info.tGetResponse response = Lepus_EIPCalloutService.sendToEIP(sXML, 'lepusmanage_field_update', Queue.Global_ID__c, 'CI0000194284');
			if(!(response != null && response.ResultStatus == 'true')){
        		Lepus_FailureHandler.handleFieldUpdateFailure(Queue.RecordId__c, Queue.SyncTime__c, response == null?'no response':response.Error);
        		success = false;
        	}
		}catch(exception e){
			Lepus_FailureHandler.handleFieldUpdateFailure(Queue.RecordId__c, Queue.SyncTime__c, 'Exception Type:'+e.getTypeName()+' Exception Msg:'+e.getMessage());
			success = false ;
		}
		
		return success;
	}
	
	//业务数据拼接XML
	private String BusinessDataXml(Lepus_Queue__c Queue){
		String sXML ='';
		
		Schema.Sobjecttype sObjType = Lepus_SyncUtil.retrieveSobjectType(Queue.ObjectType__c);
		sObject sObj = Lepus_SyncUtil.querySobject(Queue.RecordId__c, sObjType);
		
		sXML = Lepus_EIPCalloutService.DataSyncXmlConcatenation(sobj, String.valueOf(sObjType), Queue.Action__c);
		return sXML;
	}
}