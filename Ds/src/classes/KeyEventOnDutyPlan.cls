public without sharing class KeyEventOnDutyPlan implements Triggers.Handler{
	public void handle(){
		list <Key_Event_On_Duty_Plan__c> allKeyEvent = new list <Key_Event_On_Duty_Plan__c>();
		list <SFP__c> sfp = new list <SFP__c>();		
		set <ID> keyEventID = new set<ID>();	
		set <ID> errorkeyEventID = new set<ID>();	
		Map <id,id> sfpkeyEventMap = new Map <id,id>();		
		if(trigger.isInsert || trigger.isUnDelete||trigger.isUpdate){
			for(Key_Event_On_Duty_Plan__c keyEvent :(list<Key_Event_On_Duty_Plan__c>)trigger.new){					
				keyEventID.add(keyEvent.id);
				sfpkeyEventMap.put(keyEvent.id,keyEvent.SFP__c);					
			}
		}
		if(trigger.isDelete){
			for(Key_Event_On_Duty_Plan__c keyEvent :(list<Key_Event_On_Duty_Plan__c>)trigger.old){						
				keyEventID.add(keyEvent.id);
				sfpkeyEventMap.put(keyEvent.id,keyEvent.SFP__c);				
			}
		}
		
		if(keyEventID.size()>0){
				for(ID id :keyEventID){
					Key_Event_On_Duty_Plan__c keyEvent = new Key_Event_On_Duty_Plan__c();
					allKeyEvent =[select id,Sum_Service_Amount__c from Key_Event_On_Duty_Plan__c 
						where SFP__c = :sfpkeyEventMap.get(id) ];
					sfp = [select id,Key_Event_On_Duty_Total__c,Key_Event_On_Duty_Used__c from SFP__c 
						where id =:sfpkeyEventMap.get(id)];									
					if(allKeyEvent.size()>0){	
						Integer sumServiceAmount = 0;			
						for(Key_Event_On_Duty_Plan__c h :allKeyEvent){
							sumServiceAmount +=Integer.valueOf(h.Sum_Service_Amount__c);
							if(h.id == id) keyEvent = h;
						}						
							//if(sfp[0].Key_Event_On_Duty_Total__c!=null){
								if(sumServiceAmount >sfp[0].Key_Event_On_Duty_Total__c ||sfp[0].Key_Event_On_Duty_Total__c==null){
									addErrorInfo(id);
									return;
								}								
						//}
						sfp[0].Key_Event_On_Duty_Used__c = sumServiceAmount;										
						
					}	else{sfp[0].Key_Event_On_Duty_Used__c=0;}	
						update sfp;
			}
		}

	}
	
	private void addErrorInfo(Id KeyEventId){
		for(Key_Event_On_Duty_Plan__c keyEvent :(list<Key_Event_On_Duty_Plan__c>)trigger.new){					
			if(KeyEventId == keyEvent.id )keyEvent.addError('事件值守时长不能大于SFP事件值守总时长');
		}
	}		
}