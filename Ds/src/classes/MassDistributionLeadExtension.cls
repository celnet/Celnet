/**
 * @Purpose : 批量分发中国区线索
 * @Author : Tommy
 * @Date : 2014-07-16 update by Steven
 */
public with sharing class MassDistributionLeadExtension {
	//构造Lead行的展示类
	public class LeadRow {
		private String result; //分发结果标识，Success，Ignore，Failed
		public Lead lead {get; set;}
		public String Message {get; set;}
		public String ResultLabel {
			get {
				if(this.result == 'Success') return System.Label.VF_Easy_MassDistributionLead_Result_Success;
				if(this.result == 'Ignore')  return System.Label.VF_Easy_MassDistributionLead_Result_Ignore;
				if(this.result == 'Failed')  return System.Label.VF_Easy_MassDistributionLead_Result_Failed;
				return null;
			}
		}
		public Boolean Distributeable {
			get {
				if(this.lead == null) return false;
				if(this.lead.OwnerID != UserINfo.getUserId()) return false;
				if(this.lead.Status == '待分发') return true;
				if(this.lead.Status == '待确认') return true;
				if(this.lead.Status == '跟进中') return true;
				return false;
			}
		}
	}
	private List<Lead> selLeads; //视图界面选择的批量分发Leads
	private List<LeadRow> selLeadRows; //批量编辑界面显示行
	private Map<ID, LeadRow> selLeadRowMap;
	public Boolean ShowSubmit {get; set;} //是否显示提交按钮
	public Boolean IsSubmitted {get; set;} //根据是否已经提交判断是否显示状态信息列
	public String distributeInfo{get;set;} //线索分发提示信息内容
	public boolean displayDistributeInfo{get;set;} //是否显示分发提示信息
	public List<LeadRow> SelectedLeadRows {
		get {
			return this.selLeadRows;
		}
	}
	//邮件提醒相关
	public Map<Id,List<Lead>> remindLeadMap {get;set;} //用于分组保存每个用户的待提醒线索信息
	public Set<Id> allUserIdSet {get;set;}
	public Map<Id,User> allUserMap {get;set;}
	
	/**
	 * 构造方法初始化相关参数
	 */
	public MassDistributionLeadExtension(ApexPages.StandardSetController setController) {
  	this.showSubmit = true;
  	this.IsSubmitted = false;
  	this.selLeadRows = new List<LeadRow>();
  	this.selLeadRowMap = new Map<ID, LeadRow>();
  	this.selLeads = (Lead[])setController.getSelected();
  	this.allUserIdSet = new Set<Id>();
		this.allUserMap = new Map<Id,User>();
  	this.remindLeadMap = new Map<Id,List<Lead>>();
 		this.initLeadRows();
 		this.showBeforeMessage();
 		this.allUserIdSet.add(UserInfo.getUserId());
	}
	
	/**
	 * 根据选择的lead，初始化批量编辑界面显示的lead字段值
	 */
	private void initLeadRows() {
		//Step1：获取所有选择的线索的ID
		List<ID> selectedIds = new List<ID>();
		for(Integer i = 0; i < this.selLeads.size(); i++ ) {
			selectedIds.add(this.selLeads[i].Id);
		}
		//Step2：根据ID查询出Lead界面显示的字段值
  	Set<String> fieldNameSet = new Set<String>();
		fieldNameSet.add('Name');
		fieldNameSet.add('Status');
		fieldNameSet.add('OwnerId');
		fieldNameSet.add('LeadSource');
		fieldNameSet.add('CreatedDate');
		fieldNameSet.add('Distribute_To__c');
		fieldNameSet.add('Status_Changed__c');
		for(Schema.FieldSetMember field : SObjectType.Lead.FieldSets.Mass_Distribution.getFields()) {
			fieldNameSet.add(field.FieldPath);
		}
		String soql = 'Select Id';
		for(String fieldName : fieldNameSet) {
			soql += ', ' + fieldName;
		}
		soql += ' FROM Lead Where ID In: selectedIds';
		this.selLeads = (Lead[])Database.query(soql);
		//Step3：根据查询出的lead数据拼装LeadRows和LeadRowMap
  	for(Lead l: this.selLeads) {
  		LeadRow row = new LeadRow();
  		l.Distribute_To__c = null;//清空历史分发人
  		row.lead = l;
  		this.selLeadRows.add(row);
  		this.selLeadRowMap.put(row.lead.Id, row);
  	}
	}
	
	/**
	 * 根据选择的lead，初始化批量编辑界面显示的提示信息
	 */
	private void showBeforeMessage() {
		this.displayDistributeInfo = false;
		Integer totel = this.selLeadRows.size();
		Integer canbeDistributed = 0;
		for(LeadRow row : this.selLeadRows) {
			if(row.Distributeable) canbeDistributed ++;
		}
		//Logic1:如果没有勾选任何线索
		if(totel == 0) {
			ApexPages.Message wMsg = new ApexPages.Message(ApexPages.Severity.WARNING , System.Label.VF_Easy_Message_No_Selected_Record);
			ApexPages.addMessage(wMsg);
			this.showSubmit = false;
		} else {
			//Logic2:如果勾选的线索没有可以分发的
			if(canbeDistributed == 0) {
				ApexPages.Message wMsg = new ApexPages.Message(ApexPages.Severity.WARNING , System.Label.VF_Easy_MassDistributionLead_Message_No_Selected_Distribute_Record);
				ApexPages.addMessage(wMsg);
				this.showSubmit = false;
			//Logic3:如果勾选的线索全都可以分发
			} else if (totel == canbeDistributed) {
				String label1 = System.Label.VF_Easy_MassDistributionLead_Message_Selected_Report1;
				if(label1 != null) {
					label1 = label1.replace('{!totel}', totel.format());
				}
				this.distributeInfo = label1;
				this.displayDistributeInfo = true;
				this.showSubmit = true;
			//Logic4:如果勾选的线索部分可以分发
			} else  {
				String label2 = System.Label.VF_Easy_MassDistributionLead_Message_Selected_Report2;
				if(label2 != null) {
					label2 = label2.replace('{!totel}', totel.format());
					label2 = label2.replace('{!canbeDistributed}', canbeDistributed.format());
				}
				this.distributeInfo = label2;
				this.displayDistributeInfo = true;
				this.showSubmit = true;
			}
		}
	}
	
	/**
	 * Lead批量分发后，根据Lead的分发状态，构造界面提示信息
	 */
	private void showAfterMessage() {
		this.displayDistributeInfo = false;
		Integer success = 0;
		Integer failed = 0;
		Integer ignore = 0;
		for(LeadRow row : this.selLeadRows) {
			if(row.Result == 'Success') success ++;
			if(row.Result == 'Ignore') ignore ++;
			if(row.Result == 'Failed') failed ++;
		}
		String labelSuc = System.Label.VF_Easy_MassDistributionLead_Message_Submit_Report;
		if(labelSuc != null) {
			labelSuc = labelSuc.replace('{!success}', success.format());
			labelSuc = labelSuc.replace('{!ignore}', ignore.format());
			labelSuc = labelSuc.replace('{!failed}', failed.format());
		}
		ApexPages.Severity sev = ApexPages.Severity.INFO;
		if(failed > 0) sev = ApexPages.Severity.Error;
		ApexPages.Message wMsg = new ApexPages.Message(sev, labelSuc);
		ApexPages.addMessage(wMsg);
	}
	
	public PageReference submit() {
		List<Lead> updateLeads = new List<Lead>();
		List<LeadRow> submitRows = new List<LeadRow>();
		//Step1: 判断提交的线索哪些需要进行分发
		for(LeadRow row: selLeadRows) {
  		if(!row.Distributeable) {
				row.Result = 'Ignore';
				row.Message = System.Label.VF_Easy_MassDistributionLead_Message_Non_Distribute_Stage;
				continue;
			}
			if(row.lead.Distribute_To__c == null) {
				row.Result = 'Failed';
				row.Message = System.Label.VF_Easy_MassDistributionLead_Message_Non_Specified_Distribute_To;
				continue;
			}
			allUserIdSet.add(row.lead.Distribute_To__c);
			submitRows.add(row);
  	}
  	this.getAllUserInfo();
  	
		//Step2: 逐个分发Lead并记录状态和邮件提醒
		for(LeadRow lr : submitRows) {
			String oldLeadStatus = lr.lead.Status;
			try {
				lr.lead.ownerId = lr.lead.Distribute_To__c;
				lr.lead.Status = getLeadNewStatusByOwnerProfile(lr.lead.ownerId);
				lr.lead.Status_Changed__c = !lr.lead.Status_Changed__c;
				update lr.lead;
				lr.Result = 'Success';
				addTORemindLeadMap(lr.lead);//将需要邮件提醒的线索加入到待提醒清单
			} catch(Exception e) {
				lr.lead.Status = oldLeadStatus;
				lr.Result = 'Failed';
				lr.Message = e.getMessage();
			} 
		}
		this.sendMailRemindNewOwner();
  	this.showAfterMessage();
  	this.showSubmit = false;
  	this.IsSubmitted = true;
		return null;//完成后停留在页面显示每个行的结果
	}
	
	/**
	 * 一次查询出所有涉及的用户信息
	 */
	private void getAllUserInfo(){
		if(this.allUserIdSet.size()==0)return;
		this.allUserMap = new Map<id,User>([select Id,Name,Email,Profile.Name from user where id in: allUserIdSet]);
	}
	
	/**
	 * 一次查询出所有涉及的用户信息
	 */
	private String getLeadNewStatusByOwnerProfile(Id newOwnerId){
		String result = CONSTANTS.CHINALEADSTATUSCONFIRM;
		User newOwner = this.allUserMap.get(newOwnerId);
		if(newOwner.Profile.name.contains('(Huawei China)代表处销管')|| 
			 newOwner.Profile.name.contains('(Huawei China)系统部销管')){
			result = CONSTANTS.CHINALEADSTATUSASSIGN;
		}
		return result;
	}
	
	/**
	 * 初始化分发后需要邮件提醒的线索清单
	 */
	private void addTORemindLeadMap(Lead remindLead){
		if(remindLead == null)return;
		if(this.remindLeadMap.get(remindLead.OwnerId) == null) {
			//Logic1-1:如果MAP表中不存在该用户，插入该用户，并添加线索
			List<Lead> oList = new List<Lead>();
			oList.add(remindLead);
			this.remindLeadMap.put(remindLead.OwnerId,oList);
		} else {
			//Logic1-2:如果MAP表中已存在该用户，直接添加线索
			List<Lead> oList = this.remindLeadMap.get(remindLead.OwnerId);
			oList.add(remindLead);
		}
	}
	
	/**
	 * 发送提醒邮件
	 */ 
	private void sendMailRemindNewOwner() {
		Set<Id> remindUserIds = this.remindLeadMap.keySet();
		User operationUser = this.allUserMap.get(UserINfo.getUserId());
		String operation = '转发';
		if(operationUser.Profile.name.contains('(Huawei China)代表处销管')|| 
			 operationUser.Profile.name.contains('(Huawei China)系统部销管')){
			operation = '分发';
		}
		//循环发邮件提醒每个Owner
		for(Id remindUserId : remindUserIds){
			List<Lead> leadList = this.remindLeadMap.get(remindUserId);
			User newOwner = allUserMap.get(remindUserId);
			ChinaLeadFunction.sendLeadRemindMail(operationUser.Name,newOwner,leadList,operation);
		}
	}

}