/**
 * @Purpose : (中国区/海外)对于Open的机会点，NA属性随客户变化；
 *						(中国区/海外)对于Closed的机会点，记住关闭时客户的NA属性，不再变化；
 * @Author : Steven
 * @Date : 2014-08-29 Refactor
 */
public without sharing class OpportunityUpdateStageHandler implements Triggers.Handler{ //before insert before update
	public void handle(){
		Set<Id> accountIds = new Set<Id>();
		Set<Id> updateOppIds = new Set<Id>();
		//Step1: 记录所有涉及的机会点的客户ID
		for(Opportunity opp : (List<Opportunity>)Trigger.new){
			if(opp.recordtypeid == CONSTANTS.CHINAOPPORTUNITYRECORDTYPE || 
			   opp.recordtypeid == CONSTANTS.HUAWEICHINASERVICEOPPRECORDTYPE || 
			   opp.recordtypeid == CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE){
				//Step1-1: 如果是创建机会点，机会点为关闭状态，记录客户ID
				if(Trigger.isInsert && opp.IsClosed){ 
					accountIds.add(opp.accountid);
				}
				//Step1-2: 如果是更新机会点，机会点的关闭状态发生了变化，记录客户ID
				if(Trigger.isUpdate){
					Opportunity oldOpp = (Opportunity)Trigger.oldMap.get(opp.id);
					if(opp.IsClosed != oldOpp.IsClosed) {
						accountIds.add(opp.accountid);
						updateOppIds.add(opp.id);
					}
				}
			}
		}
		if(accountIds.size()==0) return;
		system.debug('-----OpportunityUpdateStageHandler----');
		
		//Step2: 一次查询出所有涉及的机会点客户信息
		Map<id,Account> accountMap = 
			new Map<id,Account>([select id,is_named_account__c,Is_Named_Account_E__c from Account where id in: accountIds]);

		//Step3：分别更新中国区和海外机会点的NA类型
		for(Opportunity opp : (List<Opportunity>)Trigger.New) {
			if(opp.recordtypeid == CONSTANTS.CHINAOPPORTUNITYRECORDTYPE || 
			   opp.recordtypeid == CONSTANTS.HUAWEICHINASERVICEOPPRECORDTYPE || 
			   opp.recordtypeid == CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE ||
			   opp.RecordTypeId == CONSTANTS.HUAWEIOVERSEAOPPRECORDTYPE || 
				 opp.RecordTypeId == CONSTANTS.HUAWEIOVERSEADEALOPPRECORDTYPE){ 
				//Step3-1: 如果是创建机会点，机会点为关闭状态，记录客户ID
				if(Trigger.isInsert && opp.IsClosed){ 
					Account acc = accountMap.get(opp.accountid);
					opp.Project_Type_Record__c = getOppNAType(acc,true);
				}
				//Step3-2: 如果是更新机会点，机会点的关闭状态发生了变化，记录客户ID
				if(Trigger.isUpdate && updateOppIds.contains(opp.id)){
					Account acc = accountMap.get(opp.accountid);
					if(opp.IsClosed)  opp.Project_Type_Record__c = getOppNAType(acc,true);
					if(!opp.IsClosed) opp.Project_Type_Record__c = null;
				}
			}
			if(opp.RecordTypeId == CONSTANTS.HUAWEIOVERSEAOPPRECORDTYPE || 
				 opp.RecordTypeId == CONSTANTS.HUAWEIOVERSEADEALOPPRECORDTYPE){ 
				//Step3-3: 如果是创建机会点，机会点为关闭状态，记录客户ID
				if(Trigger.isInsert && opp.IsClosed){ 
					Account acc = accountMap.get(opp.accountid);
					opp.Project_Type_Record__c = getOppNAType(acc,false);
				}
				//Step3-4: 如果是更新机会点，机会点的关闭状态发生了变化，记录客户ID
				if(Trigger.isUpdate && updateOppIds.contains(opp.id)){
					Account acc = accountMap.get(opp.accountid);
					if(opp.IsClosed)  opp.Project_Type_Record__c = getOppNAType(acc,false);
					if(!opp.IsClosed) opp.Project_Type_Record__c = null;
				}
			}
		}
	    	
	}
	
	/**
	 * 根据客户的NA属性获取机会点NA类型
	 */
	private String getOppNAType(Account acc,Boolean isChina) {
		String result = 'No';
		if(isChina && acc.is_named_account__c) {
			result = 'Yes';
		}
		if(!isChina && acc.Is_Named_Account_E__c == 'YES') {
			result = 'Yes';
		}
		return result;
	}

}