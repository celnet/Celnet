/**
 * @Purpose : (中国区)当机会点阶段发生变化时，更新报备的最后修改时间，便于增量集成同步
 *						(中国区)当报备关联的机会点赢单或丢单时，且报备状态为Open（报备审批通过）的，清空掉失效日期；
 * @Author : Steven
 * @Date : 2014-08-29 Refactor
 */
public without sharing class ChinaOpportunityUpdateDealStatus implements Triggers.Handler{ //before update
	public void handle(){
		//Step1: 记录所有待刷新的机会点ID
		Set<Id> oppIds = new Set<Id>();
		for(Opportunity opt : (List<Opportunity>)Trigger.New){
			if(opt.recordtypeid == CONSTANTS.CHINAOPPORTUNITYRECORDTYPE || 
				 opt.recordtypeid == CONSTANTS.HUAWEICHINASERVICEOPPRECORDTYPE || 
				 opt.recordtypeid == CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE){
				Opportunity oldOpt = (Opportunity)trigger.oldMap.get(opt.id);
				if(opt.StageName != oldOpt.StageName) {
					oppIds.Add(opt.Id);
				}
			}
		}
		if(oppIds.isEmpty()) return;
		system.debug('-----ChinaOpportunityUpdateDealStatus----');
		
		//Step2: 一次查询出所有机会点关联的报备信息
		List<Opportunity> oppWithDRList = 
			[Select o.Id,(Select Deal_Status__c, Expiring_Date__c From Deal_Registrations__r) From Opportunity o Where Id In : oppIds];
		Map<id,list<Deal_Registration__c>> drMap = new Map<id,list<Deal_Registration__c>>();
		for (Opportunity opp : oppWithDRList){
			drMap.put(opp.id,opp.Deal_Registrations__r);
		}
		//Step3：记录下需要更新的报备记录
		List<Deal_Registration__c> drUpdateList = new List<Deal_Registration__c>();
		for(Opportunity opt : (List<Opportunity>)Trigger.New) {
			if(oppIds.contains(opt.id)) {
				for(Deal_Registration__c dr : drMap.get(opt.id)){
					//如果机会点关闭了,且报备状态为Open,清空掉报备的失效日期
					if(opt.IsClosed && dr.Deal_Status__c == 'Open'){
						dr.Expiring_Date__c = null;
					}
					drUpdateList.Add(dr);
				}
			}
		}
		//Step4：刷新需要更新的报备记录
		if (drUpdateList.Size() > 0){
			try {
				update drUpdateList;
			}catch (DMLException e) {
				Trigger.New[0].addError(e.getDMLMessage(0));
			}
		}		    	
	}

}