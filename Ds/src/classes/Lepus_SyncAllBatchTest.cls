@isTest
private class Lepus_SyncAllBatchTest {
	static testmethod void myUnitTest(){
		Lead l = new Lead();
		l.LastName = 'test';
		l.Company = 'test';
		l.RecordTypeId = CONSTANTS.OVERSEALEADRECORDTYPE;
		insert l;
		
		Lepus_SyncAllBatch ls = new Lepus_SyncAllBatch('Lead');
		Database.executeBatch(ls, 10);
	}
}