/**
 * @Purpose : 当线索上的客户字段第一次赋值时，根据客户上的Is Named Account字段赋值线索上的Is Named Account字段。
 * @Author : sunda
 * @Date :   2014-08-29
 */
public class Lepus_SetIsNamedAccountHandler implements Triggers.Handler{
	public void handle() {
		List<ID> list_AccountId = new List<ID>();
		for(Lead objLead : (List<Lead>)trigger.new){
			if(trigger.isInsert && objLead.Account_Name__c != null){
				list_AccountId.add(objLead.Account_Name__c);
			}else if(trigger.isUpdate && objLead.Account_Name__c != null && trigger.oldMap.get(objLead.Id).get('Account_Name__c')==null){
				list_AccountId.add(objLead.Account_Name__c);
			}
		}
		if(list_AccountId.size() > 0 ){
			Map<ID,Account> map_Acc = new Map<ID , Account>([select id,Is_Named_Account__c From Account Where Id in: list_AccountId]);
			for(Lead objLead : (List<Lead>)trigger.new){
				if(map_Acc.containsKey(objLead.Account_Name__c)){
					objLead.Is_Named_Account__c = map_Acc.get(objLead.Account_Name__c).Is_Named_Account__c;
				}
			}
		}
	}
}