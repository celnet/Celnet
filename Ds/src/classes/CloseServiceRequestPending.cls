public with sharing class CloseServiceRequestPending {
	public Case sr {get;set;}
	public SR_Pending__c srp {get;set;}
	public boolean isSaveButtonVisible {get; set;}		//保存按钮是否显示
	public boolean isEditVisible {get; set;}			//编辑界面是否显示

	public CloseServiceRequestPending(ApexPages.StandardController stdCon){
		this.sr = [select id, CaseNumber from Case where Id = : stdCon.getId()];
		if ([select count() from SR_Pending__c where Service_Request__c =: this.sr.id and Status__c='Open'] > 0){
			srp = [select Pending_Reason__c, Description__c, CreatedDate from SR_Pending__c where Service_Request__c =: this.sr.id and Status__c='Open' order by createddate desc limit 1];
			isSaveButtonVisible = true;
			isEditVisible = true;
		}else{
			//没有需要关闭的记录
			sr.addError('No open service request pending!');
			isSaveButtonVisible = false;
			isEditVisible = false;
		}

	    UserRecordAccess access ;
	    try{
	        access = [SELECT RecordId, HasReadAccess, HasEditAccess
						 FROM UserRecordAccess
						 WHERE UserId = :UserINfo.getUserId()
						 AND RecordId = :stdCon.getId() limit 1];
	    }catch(QueryException e){
	    }
		if(access == null || !access.HasEditAccess){
			//无编辑权限，提示
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,
			'hasediteaccess:---' + System.Label.No_Privilege));
		}
	}

    public PageReference save(){
		try{
			srp.Status__c = 'Closed';
			srp.End_Time__c = System.now();
			update srp;
			return new ApexPages.StandardController(sr).view();
		}catch(Exception e){
			ApexPages.addMessages(e);
			System.debug('the exception e:---' + e.getMessage());
			return null;
		}	
    }
}