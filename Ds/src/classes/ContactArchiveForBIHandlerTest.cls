@isTest
private class ContactArchiveForBIHandlerTest {
    static testMethod void deleteOverseaContact() {
    	Account acc = UtilUnitTest.createHWChinaAccount('testacc');
    	Contact contact = new Contact();
    	contact.RecordTypeId = CONSTANTS.HUAWEIOVERSEACONTACTRECORDTYPE;
    	contact.LastName = 'test';
    	contact.Email = '1@1.1';
    	contact.AccountId = acc.id;
			insert contact;
			delete contact;
    }
}