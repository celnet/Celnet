/*
	it is created for testing the blacklist batch controller on the blacklist project
	and it is created by jen peng on 2013-05-16;
*/
@isTest
private class BlacklistBatchTest {
	
	
    static testMethod void blacklistCheckBatchTest() {
        // TO DO: implement unit test
        List<BlackList__c> bls=new List<BlackList__c>();
        bls=UtilUnitTest.createBlackLists(0,20, 'blacklist', 'XX', 'new');
        List<Account> accs = new List<Account>();
        accs=UtilUnitTest.createAccounts(0,50,'blacklist');
        
       	Test.startTest();		
         
       	BlacklistCheckBatch bcb = new BlacklistCheckBatch();
		bcb.query='select id,Name,Blacklist_Account_Alias__c,blacklist_type__c,Country_Code_HW__c,Blacklist_Status__c,Last_Blacklist_Status__c from Account where Blacklist_Status__c=\'Gray\'';
		Id batchId = Database.executeBatch(bcb,50);
		
		String sch = '0 0 0 10 2 ? 2022';
		 String jobId = System.schedule('testBasicScheduledApex',
      							sch, 
         						new BlacklistCheckBatch());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];
         
         System.assertEquals(sch,ct.CronExpression);
		 System.assertEquals('2022-02-10 00:00:00', 
         String.valueOf(ct.NextFireTime));			
       	Test.stopTest();
       	    
       
       	System.AssertEquals(
           database.countquery('SELECT COUNT()'
              +' FROM Account WHERE Blacklist_Status__c=\'Yellow\''),
           20); 
           
        System.AssertEquals(
           database.countquery('SELECT COUNT()'
              +' FROM Account WHERE Blacklist_Status__c=\'Green\''),
           30);
       	
    }
    
    static testMethod void blacklistIncrementalBatchTest(){
    	List<BlackList__c> bls=new List<BlackList__c>();
    	List<BlackList__c> newbls=new List<BlackList__c>();
        bls=UtilUnitTest.createBlackLists(0,30, 'blacklist', 'XX', 'old');
        newbls=UtilUnitTest.createBlackLists(30,40, 'blacklist', 'XX', 'new');
        List<Account> accs = new List<Account>();
        UtilUnitTest.createAccounts(0,50,'blacklist');
        accs=[select id,name,Blacklist_Status__c,blacklist_type__c from Account];
        for(Account a:accs){
        	a.blacklist_type__c='Pass';
        	a.Blacklist_Status__c='Green';
        }
        update accs;
        
        Test.startTest();		
        
       	BlacklistIncrementalCheckBatch bicb = new BlacklistIncrementalCheckBatch();
		bicb.query='select id,Name,Blacklist_Account_Alias__c,blacklist_type__c,Country_Code_HW__c,Blacklist_Status__c from Account where Blacklist_Status__c=\'Green\'';
		Id batchId = Database.executeBatch(bicb,50);
		
		String sch = '0 0 0 10 2 ? 2022';
		 String jobId = System.schedule('testBasicScheduledApex',
      							sch, 
         						new BlacklistIncrementalCheckBatch());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];
         
         System.assertEquals(sch,ct.CronExpression);
		 System.assertEquals('2022-02-10 00:00:00', 
         String.valueOf(ct.NextFireTime));	
       	Test.stopTest();
       	
       	System.AssertEquals(
           database.countquery('SELECT COUNT()'
              +' FROM Account WHERE Blacklist_Status__c=\'Yellow\''),
           10); 
          
        System.AssertEquals(
           database.countquery('SELECT COUNT()'
              +' FROM Account WHERE Blacklist_Status__c=\'Green\''),
           40);
    }
    
    static testMethod void blacklistDecrementalBatchTest(){
    	List<BlackList__c> bls=new List<BlackList__c>();
        bls=UtilUnitTest.createBlackLists(0,30, 'blacklist', 'XX', 'new');
        for(BlackList__c b:bls){
        	b.status__c='del';
        }
        update bls;
        List<Account> accs = new List<Account>();
        UtilUnitTest.createAccounts(0,50,'blacklist');
        accs=[select id,name,Blacklist_Status__c,blacklist_type__c from Account];
        for(Account a:accs){
        	a.blacklist_type__c='Tier I';
        	a.Blacklist_Status__c='Red';
        }
        update accs;
        
        Test.startTest();		
        
       	BlacklistDecrementalCheckBatch bdcb = new BlacklistDecrementalCheckBatch();
		bdcb.query='select id,Name,Blacklist_Account_Alias__c,blacklist_type__c,Country_Code_HW__c,Blacklist_Status__c from Account where Blacklist_Status__c=\'Red\'';
		Id batchId = Database.executeBatch(bdcb,50);
		
		 String sch = '0 0 0 10 2 ? 2022';
		 String jobId = System.schedule('testBasicScheduledApex',
      							sch, 
         						new BlacklistDecrementalCheckBatch());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];
         
         System.assertEquals(sch,ct.CronExpression);
		 System.assertEquals('2022-02-10 00:00:00', 
         String.valueOf(ct.NextFireTime));	
       	Test.stopTest();
       	
       	System.AssertEquals(
           database.countquery('SELECT COUNT()'
              +' FROM Account WHERE Blacklist_Status__c=\'Red\''),
           50); 
          
    }
    
    
    static testMethod void blacklistStatusChangedBatchTest(){
    	List<BlackList__c> bls=new List<BlackList__c>();
        bls=UtilUnitTest.createBlackLists(0,30, 'blacklist', 'XX', 'new');
        
        Test.startTest();		
        
       	BlacklistStatusChangedBatch bscb = new BlacklistStatusChangedBatch();
		bscb.query='select id,Name,status__c from BlackList__c where status__c<>\'del\' OR status__c<>\'old\'';
		Id batchId = Database.executeBatch(bscb,30);
		
		String sch = '0 0 0 10 2 ? 2022';
		 String jobId = System.schedule('testBasicScheduledApex',
      							sch, 
         						new BlacklistStatusChangedBatch());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];
         
         System.assertEquals(sch,ct.CronExpression);	
         System.assertEquals('2022-02-10 00:00:00', 
         String.valueOf(ct.NextFireTime));
       	Test.stopTest();
       	
       	System.AssertEquals(
           database.countquery('SELECT COUNT()'
              +' FROM BlackList__c WHERE status__c=\'old\''),
           30); 
          
    }
}