/**************************************************************************************************
 * Name             : AllUnitTest
 * Object           : Account, Opportunity
 * Requirement      : NONE
 * Purpose          : Test all related Trigger and Class 
 * Author           : Tulipe Lee
 * Create Date      : 05/22/2012
 * Modify History   : 
 * 
***************************************************************************************************/
@isTest
private class AllUnitTest {
    // Test TigProjectProduct
    public static testMethod void testTigProjectProduct() {
        Account acc = UtilUnitTest.createAccount();
        Opportunity opp = UtilUnitTest.createOpportunity(acc);
        String level1 = 'Test';
        Product_HS__c product1 = UtilUnitTest.createProductMaster(level1);
        Project_Product__c lineItem = UtilUnitTest.createProjectProduct(opp, product1);
        
        // When new Project Product is created, the Opportunity Level1 of OPP should be changed
        System.assertEquals([SELECT Opportunity_Level1__c 
                             FROM Opportunity
                             WHERE Id = :opp.Id].Opportunity_Level1__c, level1 + ';');
        
        // Create a new Product Master
        String level2 = 'Test2';
        Product_HS__c product2 = UtilUnitTest.createProductMaster(level2);
        
        // After the Product Master of Project Product is changed, Opportunity Level1 
        //  of opp should be changed to new Level of new Product Master
        lineItem.Lookup__c = product2.Id;
        update lineItem;
        opp = [SELECT Opportunity_Level1__c FROM Opportunity WHERE Id = :opp.Id];
        System.assertEquals(opp.Opportunity_Level1__c, level2 + ';');
        
        // After the Project Product is deleted, Opportunity Level1 of opp should be cleared
        delete lineItem;
        opp = [SELECT Opportunity_Level1__c FROM Opportunity WHERE Id = :opp.Id];
        System.assertEquals(opp.Opportunity_Level1__c, null);
    }
    // Test TigOnProductMaster
    public static testMethod void testTigOnProductMaster() {
        Account acc = UtilUnitTest.createAccount();
        Opportunity opp = UtilUnitTest.createOpportunity(acc);
        String level = 'Test';
        Product_HS__c product = UtilUnitTest.createProductMaster(level);
        Project_Product__c lineItem = UtilUnitTest.createProjectProduct(opp, product);
        
        // After the Level of Product Master, the corresponding Opportunity Level1 should be changed
        String level1 = 'TEST1';
        product.Level_1__c = level1;
        update product;
        opp = [SELECT Opportunity_Level1__c FROM Opportunity WHERE Id = :opp.Id];
        System.assertEquals(opp.Opportunity_Level1__c, level1 + ';');
    }

    // Test TigOnAccount
    public static testMethod void testTigOnAccount() {
        Account acc = UtilUnitTest.createAccount('acc');
        Opportunity opp = UtilUnitTest.createOpportunity(acc);
        
        acc.Industry = 'Large Enterprise';
        update acc;
        
        // After Account Industry is changed, related Opportunity Industry 
        //   should be changed to the same on with Account
        opp = [SELECT Industry__c FROM Opportunity WHERE Id = :opp.Id LIMIT 1];
        System.assertEquals(opp.Industry__c, 'Large Enterprise');// annotation by czh
    }
    
    // Test TigOnSalesTeam                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              m
    public static testMethod void testTigOnSalesTeam() {
        Account acc = UtilUnitTest.createAccount();
        Opportunity opp = UtilUnitTest.createOpportunity(acc);
        User usr = [SELECT Id 
                    FROM User 
                    WHERE (Profile.Name = '(CSS)国内总部响应工程师(HRE)' or Profile.Name = '(CSS)国内总部响应工程师(CSE)')
                    AND IsActive = true LIMIT 1];
        Sales_Team__c team = UtilUnitTest.createSalesTeam(opp, usr);
        
        // Insert Action Test
        System.assertEquals([SELECT COUNT() 
                             FROM OpportunityShare 
                             WHERE UserOrGroupId = :usr.Id 
                             AND OpportunityId = :opp.Id], 1);
        
        // Update Action Test          
        team.Opportunity_Access_Level__c = 'Read Only';
        update team;
        OpportunityShare share = [SELECT OpportunityAccessLevel 
                                  FROM OpportunityShare 
                                  WHERE UserOrGroupId = :usr.Id
                                  AND OpportunityId = :opp.Id LIMIT 1];
        System.assertEquals(share.OpportunityAccessLevel, 'Read');
        
        // Delete Action Test
        delete team;
        System.assertEquals([SELECT COUNT() 
                             FROM OpportunityShare 
                             WHERE UserOrGroupId = :usr.Id 
                             AND OpportunityId = :opp.Id], 0); 
    }    
}