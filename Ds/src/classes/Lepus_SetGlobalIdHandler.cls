/**
 * Author : sunda
 * Date : 2014.8.15
 * Des : Set Global Id Field(Account,Opportunity,Contact,Lead)
 **/
public class Lepus_SetGlobalIdHandler implements Triggers.Handler{
    public void handle(){
        
        if(trigger.isAfter && trigger.isInsert){
            list<Sobject> list_obj = new list<Sobject>();
            for(Sobject obj : trigger.new){
                if(obj.get('Global_ID__c') == null ){
                    Sobject sobj = ((ID)obj.get('ID')).getSobjectType().newSobject();
                    sobj.put('ID',obj.get('ID'));
                    sobj.put('Global_ID__c',String.valueOf(obj.get('ID')).toUpperCase());
                    list_obj.add(sobj);
                }
            }
            update list_obj;
        }
        
    }
}