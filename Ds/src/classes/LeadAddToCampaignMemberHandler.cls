public without sharing class LeadAddToCampaignMemberHandler implements Triggers.Handler{
		//after insert,because we need the lead id
		
	public void handle() {
		List<CampaignMember> members = new List<CampaignMember>();
		
		Set<String> regions = new Set<String>();
		for(Lead l : (List<Lead>)Trigger.new){
			if('Huawei_China_NA_Lead' == l.RecordTypeDeveloperName__c){//Record type 为 NA Lead
				if(l.LeadSource == '展会' && l.campaign__c == null){
					l.adderror('来源为展会的NA线索必须指定市场活动');
				}else{
					if(l.campaign__c != null){
						CampaignMember m = new CampaignMember(CampaignId = l.campaign__c,LeadId = l.id);
						members.add(m);
					}
				}
			}
		}
		insert members;
	}
}