@isTest
private class Lepus_QueueBatchTest {
	static testmethod void myUnitTest(){
		Account acc = Lepus_EIPCalloutServiceTest.createAccount('dfadsa');
		insert acc;
		
		Lepus_QueueManager manager = new Lepus_QueueManager();
		Lepus_Queue__c qu = manager.initQueue('fdaf', acc.Id, 'insert', '业务数据同步', datetime.now());
		//manager.EnQueue(new List<Lepus_Queue__c>{qu});
		
		Lepus_QueueBatch batch = new Lepus_QueueBatch();
		batch.start(null);
		batch.execute(null, new List<Lepus_Queue__c>{qu});
		batch.finish(null);
		
		//Database.executeBatch(batch, 1);
	}
}