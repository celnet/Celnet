@isTest
private class SFPProductNameRollUpHandlerTest {
	 static testMethod void singleTest() {
      Account acc = UtilUnitTest.createAccount();
      CSS_Contract__c contract = UtilUnitTest.createContract(acc);
      SFP__c sfp = UtilUnitTest.createSFP(contract);
      Product_HS__c hs = UtilUnitTest.createProductMaster('level');
    	Contract_Product_PO__c cpp = UtilUnitTest.createContractProduct(contract,hs);
    	cpp = [select id,name from Contract_Product_PO__c where id =:cpp.id];
      sfp = [select id,Product_Name__c from SFP__c ];
    	System.assertEquals(null,sfp.Product_Name__c);
    	
			SFP_Product__c sfpp = UtilUnitTest.createSFPProduct(sfp,cpp);//insert    	
    	sfp = [select id,Product_Name__c from SFP__c ];
    	System.assertEquals(cpp.name,sfp.Product_Name__c);
    	
    	SFP_Product__c sfpp2 = UtilUnitTest.createSFPProduct(sfp,cpp);//insert
    	sfp = [select id,Product_Name__c from SFP__c ];
    	System.assertEquals(cpp.name+';'+cpp.name,sfp.Product_Name__c);
    	
    	delete sfpp2;//delete
    	sfp = [select id,Product_Name__c from SFP__c ];
    	System.assertEquals(cpp.name,sfp.Product_Name__c);
    	
    	undelete sfpp2;//undelete
    	sfp = [select id,Product_Name__c from SFP__c ];
    	System.assertEquals(cpp.name+';'+cpp.name,sfp.Product_Name__c);
    	
    	//doesnt allow reparent sfp/contract product,so no need to update the sfp__c field on sfp product
    	/*
    	Contract_Product_PO__c cpp2 = UtilUnitTest.createContractProduct(contract,hs);
    	cpp2.name = 'xyz';
    	update cpp2;
    	cpp2 = [select id,name from Contract_Product_PO__c where id =:cpp2.id];
    	sfpp2.Contract_Product_PO__c = cpp2.id;
    	update sfpp2;
    	sfp = [select id,Product_Name__c from SFP__c ];
    	System.assertEquals(true,sfp.Product_Name__c.contains(cpp.name));
    	System.assertEquals(true,sfp.Product_Name__c.contains(cpp2.name));
    	System.assertEquals(true,sfp.Product_Name__c.contains(';'));
    	System.assertEquals(cpp.name.length() + cpp2.name.length() + ';'.length(),sfp.Product_Name__c.length());
    	*/
    }
}