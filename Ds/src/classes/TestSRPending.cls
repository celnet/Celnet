/**
 * Purpose          : Test Create SR Pending, Close SR Pending
 * Author           : BLS
 * Date				: 2013-3-19
 **/
@isTest
private class TestSRPending {
    static testMethod void SRPending() {
		Account acc = UtilUnitTest.createHWChinaAccount('bbb');
		Contact co = New Contact(AccountId=acc.Id, LastName='Zhang', Email='zhang@test.com');
		insert co;
		Case sr = UtilUnitTest.createChinaServiceRequest();	
    	
    	//create new sr pending test
	    ServiceRequestPending controller = new ServiceRequestPending(new ApexPages.StandardController(sr));
	    Test.setCurrentPage(Page.CreateSRPending);
	    System.assertEquals(true, controller.isEditVisible);

	    controller.srp.Pending_Reason__c = 'test';
	    controller.srp.Description__c = 'test';
	    controller.save();

	    controller = new ServiceRequestPending(new ApexPages.StandardController(sr));
	    Test.setCurrentPage(Page.CreateSRPending);
	    System.assertEquals(false, controller.isEditVisible);
	    
	    sr = [select id, Restore_Due_Date_Time__c, Resolve_Due_Date_Time__c from case where id=:sr.id];
	    SR_Pending__c srp = [select id, SR_Due_Restore_Time__c, SR_Due_Resolve_Time__c from SR_Pending__c where Service_Request__c=:sr.id order by CreatedDate desc limit 1];
	    System.assertEquals(srp.SR_Due_Restore_Time__c, sr.Restore_Due_Date_Time__c);
	    System.assertEquals(srp.SR_Due_Resolve_Time__c, sr.Resolve_Due_Date_Time__c);
	    
		//Close sr pending test
	    CloseServiceRequestPending controller2 = new CloseServiceRequestPending(new ApexPages.StandardController(sr));
	    Test.setCurrentPage(Page.CloseSRPending);
	    System.assertEquals(true, controller2.isEditVisible);
	    controller2.save();
	    srp = [select id, Status__c from SR_Pending__c where Service_Request__c=:sr.id order by CreatedDate desc limit 1];
	    System.assertEquals('Closed', srp.Status__c);

	    controller2 = new CloseServiceRequestPending(new ApexPages.StandardController(sr));
	    Test.setCurrentPage(Page.CloseSRPending);
	    System.assertEquals(false, controller2.isEditVisible);
	}
}