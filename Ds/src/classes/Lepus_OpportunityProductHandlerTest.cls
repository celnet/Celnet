@isTest
private class Lepus_OpportunityProductHandlerTest {
    static testmethod void myUnitTest(){
        
        Account acc1 = UtilUnitTest.createAccount();
        
        Opportunity opp = Lepus_EIPCalloutSErviceTest.createOpportunity('xmlda', acc1.Id);
        opp.Region__c = 'xxx';
        opp.Representative_Office__c = 'daf';
        opp.Country__c = 'dfa';
        insert opp;
        
        Product_HS__c ph = new Product_HS__c();
        insert ph;
        
        Product_HS__c ph2 = new Product_HS__c();
        insert ph2;
        
        Project_Product__c pp = new Project_Product__c();
        pp.Project_Name__c = opp.id;
        pp.Lookup__c = ph.Id;
        insert pp;
        
        Project_Product__c pp2 = new Project_Product__c();
        pp2.Project_Name__c = opp.Id;
        pp2.Lookup__c = ph2.Id;
        insert pp2;
        
        pp2.Lookup__c = ph.Id;
        
        Lepus_EIPCalloutServiceTest.setCustomSettings();
        
        pp.Lookup__c = ph2.Id;
        update  new list<Project_Product__c>{pp,pp2};
    }
}