@isTest
private class WorkOrderOwnerCheckHandlerTest {

     static testMethod void ownerValidateTest(){//2.国内、国际的用户不能互转，即国内用户不能转给国际用户，国际用户不能转给国内用户
    	Account acc = UtilUnitTest.createAccount();
    	Case sr = UtilUnitTest.createChinaServiceRequest();
    	Work_Order__c wo = UtilUnitTest.createOnSiteWorkOrder(acc,sr);
    	User intUser = UtilUnitTest.queryUser( ' UserRole.Name = \'HW-(Int CSS)America - CCR\'' );
    	wo.ownerId = intUser.id;
    	update wo;
    	User nUser = UtilUnitTest.queryUser( ' UserRole.Name =  \'(N) Call Center Rep(CCR)\'' );
    	wo.ownerId = nUser.id;
    	try{
	    	update wo;
	    	System.assertEquals('should not enter here' , '国际用户不能转给国内用户');
    	}catch(DMLException e){
    		System.assertEquals(true,e.getMessage().contains('国际用户不能转给国内用户'));
    	}
    }
    
     
    static testMethod void externalValidateTest(){//3.当前用户角色不包含SD或External时，新的Owner用户的角色名称中不能包含External
    	Account acc = UtilUnitTest.createAccount();
    	Case sr = UtilUnitTest.createChinaServiceRequest();
    	Work_Order__c wo = UtilUnitTest.createOnSiteWorkOrder(acc,sr);
    	User intUser = UtilUnitTest.queryUser( ' UserRole.Name = \'HW-(Int CSS)America - CCR\'' );
    	wo.ownerId = intUser.id;
    	update wo;
    	User externalUser = UtilUnitTest.queryUser( ' UserRole.Name like \'%External%\'' );
    	system.runAs(intUser){
	    	wo.ownerId = externalUser.id;
	    	try{
		    	update wo;
		    	System.assertEquals('should not enter here' , '只有SD和外部用户可以将工单派给外部用户');
	    	}catch(DMLException e){
	    		System.assertEquals(true,e.getMessage().contains('只有SD和外部用户可以将工单派给外部用户'));
	    	}
    	}
    }
    
    static testMethod void mobileValidateTest(){//4.owner字段变化时重设Owner_Mobile__c
    	Account acc = UtilUnitTest.createAccount();
    	Case sr = UtilUnitTest.createChinaServiceRequest();
    	Work_Order__c wo = UtilUnitTest.createOnSiteWorkOrder(acc,sr);
    	wo = [select id,Owner_Mobile__c from Work_Order__c where id=:wo.id];
    	System.assertEquals(null,wo.Owner_Mobile__c);
    	User intUser = UtilUnitTest.queryUser( ' UserRole.Name = \'HW-(Int CSS)America - CCR\'' );
    	intUser.MobilePhone = '1234567';
    	update intUser;
    	wo.ownerId = intUser.id;
    	update wo;
    	wo = [select id,Owner_Mobile__c from Work_Order__c where id=:wo.id];
    	System.assertEquals(intUser.MobilePhone,wo.Owner_Mobile__c);
    }
    
    static testMethod void SPMRegionTest(){//5.owner字段变化时,如果是SPM/TM/RSC用户,重设SPM_Region__c字段
    	Account acc = UtilUnitTest.createAccount();
    	Case sr = UtilUnitTest.createChinaServiceRequest();
    	Work_Order__c wo = UtilUnitTest.createOnSiteWorkOrder(acc,sr);
    	wo = [select id,SPM_Region__c from Work_Order__c where id=:wo.id];
    	System.assertEquals(null,wo.SPM_Region__c);
    	User tmUser = UtilUnitTest.queryUser( ' Profile.Name like \'%TM%\' ' );
    	tmUser.Region_Office__c = 'test';
    	update tmUser;
    	wo.ownerId = tmUser.id;
    	update wo;
    	wo = [select id,SPM_Region__c from Work_Order__c where id=:wo.id];
    	System.assertEquals(tmUser.Region_Office__c,wo.SPM_Region__c);
    	
    }
    
    static testMethod void RSCRegionTest(){//6.owner字段变化时,如果是RSC用户,重设RSC_Region__c字段
    	Account acc = UtilUnitTest.createAccount();
    	Case sr = UtilUnitTest.createChinaServiceRequest();
    	Work_Order__c wo = UtilUnitTest.createOnSiteWorkOrder(acc,sr);
    	wo = [select id,RSC_Region__c from Work_Order__c where id=:wo.id];
    	System.assertEquals(null,wo.RSC_Region__c);
    	User rscUser = UtilUnitTest.queryUser( ' Profile.Name like \'%RSC%\'' );
    	rscUser.Region_Office__c = 'test';
    	update rscUser;
    	wo.ownerId = rscUser.id;
    	update wo;
    	wo = [select id,RSC_Region__c from Work_Order__c where id=:wo.id];
    	System.assertEquals(rscUser.Region_Office__c,wo.RSC_Region__c);
    }
    
}