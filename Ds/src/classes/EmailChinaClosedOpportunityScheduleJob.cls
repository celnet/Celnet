/**
 * @Purpose : 中国区激活金额小于预计签单金额50%的发送提醒
 * @Author : steven
 * @Date : 2014-08-28
 */
global class EmailChinaClosedOpportunityScheduleJob implements Schedulable,Database.Batchable<Id>{    
	public Map<Id,List<Opportunity>> remindOppMap {get;set;} //用于分组保存每个销管的待提醒机会点
	public Map<String,Id> salesManagerMap {get;set;}         //用于记录区域-销管ID对应关系
	public Map<Id,String> salesManagerEmailMap {get;set;}    //用于记录销管ID-销管Email对应关系
	public Map<Id,String> salesManagerNameMap {get;set;}     //用于记录销管ID-销管Name对应关系
	public List<id> batchUserIdList {get;set;}  

	public EmailChinaClosedOpportunityScheduleJob() {  
		this.remindOppMap = new Map<Id,List<Opportunity>>(); 
		this.salesManagerMap = new Map<String,Id>(); 
		this.salesManagerEmailMap = new Map<Id,String>(); 
		this.salesManagerNameMap = new Map<Id,String>(); 
		this.batchUserIdList = new List<id>();
	}
	
	//ScheduleJob Execute
	global void execute(SchedulableContext SC){
		EmailChinaClosedOpportunityScheduleJob e = new EmailChinaClosedOpportunityScheduleJob();
		e.initSalesManagerMap();
		e.getNeedRemindOpportunity();
		for(Id userId : e.remindOppMap.keySet()) {
			e.batchUserIdList.add(userId);
		}
		Id batchId = Database.executeBatch(e,10);
	}

	//Batch Start
	global Iterable<Id> start(Database.BatchableContext BC){
		return this.batchUserIdList;   
	}
	//Batch Execute
	global void execute(Database.BatchableContext BC, list<Id> scope){
		try {
			for(Id userId : scope) {
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
				List<string> toAddress = new List<string>();
				toAddress.add(salesManagerEmailMap.get(userId));
				mail.setToAddresses(toAddress);
				mail.setSenderDisplayName('Salesforce Supporter');
				mail.setSubject('您所在区域/系统部存在部分激活的机会点');
				String strbody = htmlBodyConstructor(userId); 
				mail.setHtmlBody(strbody);
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
			}
		} catch(Exception e) {
			System.debug(e);
		}
	}
	//Batch Finished
	global void finish(Database.BatchableContext BC){
	}
	
	/**
	 * 初始化区域-销管对应关系MAP
	 */
	public void initSalesManagerMap() {
		//Step1：获取代表处销管清单
		List<Account_Link_Approver_Map__c> approvers = 
			[Select Representative_Office__c,Approver__c,Approver__r.IsActive,Approver__r.Email,Approver__r.Name
				 FROM Account_Link_Approver_Map__c 
				Where Type__c = '销管' And recordtypeid =:CONSTANTS.APPROVERMAPCHINARECORDTYPE];
		//Step2：根据代表处销管清单初始化相关MAP
		for(Account_Link_Approver_Map__c a : approvers){
			if(!a.Approver__r.IsActive) continue;
			this.salesManagerMap.put(a.Representative_Office__c,a.Approver__c);
			this.salesManagerEmailMap.put(a.Approver__c,a.Approver__r.Email);
			this.salesManagerNameMap.put(a.Approver__c,a.Approver__r.Name);
		}
	}
	
	/**
	 * 获取需要提醒的机会点清单，并按代表处分组
	 */
	public void getNeedRemindOpportunity() {
		//Step1：查询出中国区所有关闭的机会点
		List<Opportunity> oppList = 
			[Select Id,Name,Opportunity_Number__c,Estimated_ContractSign_Amount__c,Account.Representative_Office__c,Sum_Of_Contract_Amount_RMB__c 
				 From Opportunity 
				Where China_Need_Remind_Opportunity__c = true 
					And (Recordtypeid =:CONSTANTS.CHINAOPPORTUNITYRECORDTYPE or Recordtypeid =:CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE)];
		//Step2：遍历机会点列表，并按照销管ID进行分组
		for(Opportunity opp : oppList) {
			//Step2-1:根据代表处获取销管ID
			Id salesManagerId = this.salesManagerMap.get(opp.Account.Representative_Office__c);
			if(salesManagerId == null) continue;
			//Step2-2:根据代表处销管ID对机会点进行分组
			if(remindOppMap.get(salesManagerId) == null) {
				//Step2-2-1:如果MAP表中不存在该用户，插入该用户，并添加机会点
				List<Opportunity> oList = new List<Opportunity>();
				oList.add(opp);
				remindOppMap.put(salesManagerId,oList);
			} else {
				//Step2-2-2:如果MAP表中已存在该用户，直接添加机会点
				List<Opportunity> oList = remindOppMap.get(salesManagerId);
				oList.add(opp);
			}
		}
	}

	/**
	 * 构造邮件主体 (注意String长度有限制，一般情况下不会超出)
	 */ 
	private String htmlBodyConstructor(Id userId) {
    String result = '';
    String oppListString = '';
    String baseUrl = system.Url.getSalesforceBaseUrl().toExternalForm()+'/';
		//Step1：获取User相关的机会点全集
		String userName = salesManagerNameMap.get(userId);
		List<Opportunity> oList = remindOppMap.get(userId);
		//Step2：将需要提醒的机会点转换成Html
		for(Opportunity opp : oList) {
			oppListString += getHtmlOpp(opp,userName,baseUrl);
		}
		//Step3：构造提醒邮件主体
		result  = '<p>尊敬的'+ userName +'，您好</p>';
		result += '<p>&nbsp;&nbsp;您所在区域/系统部以下项目疑似部分激活（累计激活金额低于预签金额的50%），请参考并审核：</p>';
		result += getHtmlTable(oppListString);
		result += '<p>&nbsp;&nbsp;如果您对以上信息有任何疑问，请与销售业务部联系。</p>';
		result += '<p>salesforce.com</p>';
		return result;
	}
	
	/**
	 * 构造Html形式的机会点列表
	 */ 
	private String getHtmlTable(String htmlOppList) {
		String result = '';
		if(htmlOppList != null && htmlOppList != '') {
			result += '<table style="font-family: Arial,微软雅黑;font-size:14px; border:1px solid #AAA;width:900px;background: #EEE;border-spacing: 1px;text-align:center">';
			result +=   '<tbody>';
			result +=     '<tr style="background: #CCC;font-weight: bold;height:20px;">';
			result +=       '<th style="width:35%">机会点名称</th>';
			result +=       '<th style="width:25%">机会点编码</th>';
			result +=       '<th style="width:20%">预签金额(万元)</th>';
			result +=       '<th style="width:20%">累计激活金额（万元）</th>';
			result +=     '</tr>';
			result +=     htmlOppList;
			result +=   '</tbody>';
			result += '</table>';
		}
		return result;
	}
	
	/**
	 * 构造Html格式的机会点String
	 */ 
	private String getHtmlOpp(Opportunity opp,String userName,String baseUrl) {
		String result = '';
		Decimal contractAmount = opp.Sum_Of_Contract_Amount_RMB__c/10000;
		if(opp != null) {
			result += '<tr style="background: #FFF;">';
			result +=   '<td><a href='+ baseUrl + opp.Id +'>' + opp.Name + '</a></td>';
			result +=   '<td>'+ opp.Opportunity_Number__c +'</td>';
			result +=   '<td>'+ opp.Estimated_ContractSign_Amount__c +'</td>';
			result +=   '<td>'+ contractAmount.setScale(2) +'</td>';
			result += '</tr>';
		}
		return result;
	}
}