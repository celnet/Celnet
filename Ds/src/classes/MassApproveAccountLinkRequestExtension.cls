/*
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-6-14
 * Description: 批量提交审批
 */
public with sharing class MassApproveAccountLinkRequestExtension {
    private ApexPages.StandardSetController setController;
    private Set<Id> selectedAccountLinkRequestIds;
    private Set<Id> pendingAccountLinkRequestIds;
    public List<AccountLinkRequestWrapper> selectedAccountLinkRequests{get;set;}
    public String requestsInfo{get;set;}
    public boolean displayRequestsInfo{get;set;}
    public boolean displayProcessButtons{get;set;}
    public List<SelectOption> getOperationOptions(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('unprocessed',System.Label.VF_Easy_MassApproveAccountLinkRequest_Message_Pending));
        options.add(new SelectOption('Approve',System.Label.VF_Easy_MassApproveAccountLinkRequest_Message_Approve));
        options.add(new SelectOption('Reject',System.Label.VF_Easy_MassApproveAccountLinkRequest_Message_Reject));
        return options;
    }
    
    public class AccountLinkRequestWrapper{
        public Account_Link_Request__c accountLinkRequest{get;set;}
        public boolean isPending{get;set;}
        public String comment{get;set;}
        public String operation{get;set;}
        public AccountLinkRequestWrapper(Account_Link_Request__c request, boolean pending){
            accountLinkRequest = request;
            isPending = pending;
        }
    }
    
    public MassApproveAccountLinkRequestExtension(ApexPages.StandardSetController setController){
        this.setController = setController;
        this.retrieveSelectedAccountLinkRequests();
    }
    
    private void retrieveSelectedAccountLinkRequests(){
        this.selectedAccountLinkRequests = new List<AccountLinkRequestWrapper>();
        this.selectedAccountLinkRequestIds = new Set<Id>();
        this.pendingAccountLinkRequestIds = new Set<Id>();
        List<Account_Link_Request__c> links = new List<Account_Link_Request__c>();
        links = (List<Account_Link_Request__c>)this.setController.getSelected();
        Integer total = 0;
        Integer pending = 0;
        
        for(Account_Link_Request__c alr : (List<Account_Link_Request__c>) this.setController.getSelected()){
            this.selectedAccountLinkRequestIds.add(alr.Id);
        }
        
        links = [Select HasSubmitted__c, Id, Name,Approve_Status__c,Process_End__c , OwnerId, Approver__c, child__c, parent__c, Account_Link_Reason__c From Account_Link_Request__c 
            Where Id IN: this.selectedAccountLinkRequestIds];
        
        for(Account_Link_Request__c alr : links){
            total++;
            if(alr.HasSubmitted__c == true && alr.Process_End__c == false){
                this.selectedAccountLinkRequests.add(new AccountLinkRequestWrapper(alr,true));
                this.pendingAccountLinkRequestIds.add(alr.Id);
                pending++;
            }else{
                this.selectedAccountLinkRequests.add(new AccountLinkRequestWrapper(alr,false));
            }
        }
        
        this.displayProcessButtons = pending == 0?false:true;
        
        this.generateMessage(total, pending);
    }
    
    private void generateMessage(Integer total, Integer pending){
        this.displayRequestsInfo = false;
        if(pending == 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING , System.Label.VF_Easy_MassApproveAccountLinkRequest_Message_No_Selected_Record));
        }else {
        	String infoLabel = System.Label.VF_Easy_MassApproveAccountLinkRequest_Message_Info;
        	if(infoLabel != null){
        		infoLabel = infoLabel.replace('{!total}',total.format());
        		infoLabel = infoLabel.replace('{!pending}',pending.format());
        	}
            this.requestsInfo = infoLabel; //'您选择的 ' + total + ' 条记录中, 有 ' + pending + ' 条记录待处理，请选择"批准"或"拒绝"后提交, 其它记录将会被忽略';
            this.displayRequestsInfo = true;
        }
    }
    
    public System.Pagereference submit(){
        
        //Approval.Processresult result;
        List<Approval.Processworkitemrequest> reqList = new List<Approval.Processworkitemrequest>();
        
        List<ProcessInstanceWorkitem> piwList = [Select Id, ProcessInstanceId, ProcessInstance.TargetObjectId From ProcessInstanceWorkitem 
                Where ProcessInstance.TargetObjectId IN: this.pendingAccountLinkRequestIds];
        for(ProcessInstanceWorkitem piw : piwList){
            Approval.Processworkitemrequest req;
            req = new Approval.Processworkitemrequest();
            req.setWorkitemId(piw.Id);
            for(AccountLinkRequestWrapper arw : this.selectedAccountLinkRequests){
                if(arw.accountLinkRequest.Id == piw.ProcessInstance.TargetObjectId){
                    req.setComments(arw.comment);
                    if(arw.operation != 'unprocessed'){
                        req.setAction(arw.operation);
                        reqList.add(req);
                        
                    }
                    break;
                }
            }
            
        }
        
        Approval.process(reqList);
        return setController.save();
    }
}