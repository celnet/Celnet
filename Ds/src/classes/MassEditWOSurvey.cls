public with sharing class MassEditWOSurvey {

    Work_Order__c wo=null;
    List<Work_Order_Survey__c> srses=null;
    Id cid=null;
    
    
    public MassEditWOSurvey(ApexPages.StandardController controller) {
        cid= ApexPages.currentPage().getParameters().get('id');

        srses = (List<Work_Order_Survey__c>)[Select w.Type__c, w.Suggestion__c
                                , w.Satisfactory_Level__c, w.Name, w.Id, w.Description__c 
                                , w.Work_Order_No__c
                                , w.RecordTypeId
                                , w.RecordType.Name
                                , w.Score__c                                
                                , w.Aggregated_Score__c
                                From Work_Order_Survey__c w
                                where w.Work_Order_No__c =: cid]; 
        //controller.setSelected(srses);
        wo=[select id from Work_Order__c where id=:cid]; 
        
        List<String> sorts=new List<String>();
        Map<String,Work_Order_Survey__c> isrsMap=new Map<String,Work_Order_Survey__c>();
        for(Work_Order_Survey__c w:srses){
            String t=w.Type__c;
            if(t.indexOf('.')==1)t='0'+t;
            sorts.add(t);
            isrsMap.put(t,w);
        }
        sorts.sort();
        srses.clear();
        for(String str:sorts){
            srses.add(isrsMap.get(str));
        }
    }
    
    
    public List<Work_Order_Survey__c> getNewselected(){
        return srses;
    }
    
    public PageReference save() {
        /**
        
        Map<Id,Service_Request_Survey__c> isMap=new Map<Id,Service_Request_Survey__c>();
        List<Service_Request_Survey__c> s=new List<Service_Request_Survey__c>();
        for(Service_Request_Survey__c srs:srses){
            if(isMap.containsKey(srs.id))continue;
            isMap.put(srs.id,srs);
        }
        */
        update srses;
        return ApexPages.currentPage();
        //return (new ApexPages.StandardController(wo)).view();
    }
    
    public PageReference cancle() {
       return (new ApexPages.StandardController(wo)).view();
    }    
    
    public static testMethod void currClsTest(){
        CSS_Contract__c c0 = new CSS_Contract__c (name = '12345678901234');
       	insert c0;
				Account acc = UtilUnitTest.createHWChinaAccount('t');
				Case c = UtilUnitTest.createChinaServiceRequest();
      Work_Order__c wo2= new Work_Order__c(status__c='Working',Account_Name__c = acc.id,
      	Contract_No__c = c0.id,Service_Request_No__c = c.id);
      insert wo2;
        Work_Order__c wo=[select id from Work_Order__c limit 1];
        ApexPages.currentPage().getParameters().put('id', wo.id);
        MassEditWOSurvey mes=new MassEditWOSurvey(null);
        mes.getNewselected();
        mes.save();
        mes.cancle();
    }
}