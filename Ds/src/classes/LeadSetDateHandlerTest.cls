@isTest
private class LeadSetDateHandlerTest {

    static testMethod void confirmDateTest() {
    	Account acc = UtilUnitTest.createHWChinaAccount('acc');
    	Lead currentLead = UtilUnitTest.createLead(acc);
    	
    	currentLead = [select id,NA_Lead_Confirmation__c,Confirm_Date__c from Lead where id = :currentLead.id];
    	System.assertEquals('未确认',currentLead.NA_Lead_Confirmation__c);
    	System.assertEquals(null,currentLead.Confirm_Date__c);
    	
    	currentLead.NA_Lead_Confirmation__c = '确认有效';
    	update currentLead;
    	
    	currentLead = [select id,NA_Lead_Confirmation__c,Confirm_Date__c from Lead where id = :currentLead.id];
    }
}