/*
 * Author: Sunny.sun@celnet.com.cn
 * Date: 2014-8-27
 * Description: Lepus队列管理
 */
public class Lepus_QueueManager {
	public void EnQueue(List<Lepus_Queue__c> list_Queue){
		system.debug('Queue Info:'+List_Queue);
		insert list_Queue;
	}
	public Lepus_Queue__c initQueue(String GlobalId,ID RecordId,String sAction,String sSyncType,Datetime dTime){
		Lepus_Queue__c Queue = new Lepus_Queue__c();
		Queue.RecordId__c = RecordId;
		Queue.Action__c = sAction;
		Queue.SyncType__c = sSyncType;
		Queue.SyncTime__c = dTime;
		Queue.RecordGlobalId__c = GlobalId;
		//Queue.SyncType__c
		if(String.valueOf(RecordId).startsWith(Account.sObjectType.getDescribe().getKeyPrefix())){
			Queue.ObjectType__c = 'Account';
		}else if(String.valueOf(RecordId).startsWith(Opportunity.sObjectType.getDescribe().getKeyPrefix())){
			Queue.ObjectType__c = 'Opportunity';
		}else if(String.valueOf(RecordId).startsWith(User.sObjectType.getDescribe().getKeyPrefix())){
			Queue.ObjectType__c = 'User';
		}else if(String.valueOf(RecordId).startsWith(Lead.sObjectType.getDescribe().getKeyPrefix())){
			Queue.ObjectType__c = 'Lead';
		}else if(String.valueOf(RecordId).startsWith(Contact.sObjectType.getDescribe().getKeyPrefix())){
			Queue.ObjectType__c = 'Contact';
		}
		return Queue;
	}
	public static void OutQueue(List<ID> list_QueueIds){
		List<Lepus_Queue__c> list_Queue = new List<Lepus_Queue__c>();
		for(ID qId : list_QueueIds){
			Lepus_Queue__c queue = new Lepus_Queue__c(ID=qId);
			list_Queue.add(queue);
		}
		delete list_Queue;
	}
	public static Boolean HaveUntreated(){
		Boolean haveUntreated = false;
		if([Select Id From Lepus_Queue__c limit 1].size() > 0)
		haveUntreated=true;
		return haveUntreated;
	}
}