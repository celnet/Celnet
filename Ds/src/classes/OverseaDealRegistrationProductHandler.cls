/**
 * @Purpose : 海外报备产品创建刷新报备上的涉及的一级产品名称字段
 * @Author : steven
 * @Date :   2014-09-05
 */
public without sharing class OverseaDealRegistrationProductHandler implements Triggers.Handler{ //after insert,after update,after delete
	public void handle() {
		Set<Id> dealIds = new Set<Id>();  
		//Step1: 对于创建报备产品，记录下报备ID
		if(trigger.isInsert) {
			for (Deal_Registration_Product__c product : (List<Deal_Registration_Product__c>)trigger.new) {
				dealIds.add(product.Deal_Registration__c);
			}
		}
		//Step2: 对于删除报备产品，记录下报备ID
		if(trigger.isDelete) {
			for (Deal_Registration_Product__c product : (List<Deal_Registration_Product__c>)trigger.old) {  
				dealIds.add(product.Deal_Registration__c);
			}
		}
		//Step3: 对于修改报备产品，如果一级产品信息发生了更改，记录下报备ID
		if(trigger.isUpdate) {
			for(Deal_Registration_Product__c newProduct : (List<Deal_Registration_Product__c>)trigger.new) {
				Deal_Registration_Product__c oldProduct = (Deal_Registration_Product__c) trigger.oldMap.get(newProduct.Id);
				if (oldProduct.Lookup_Product_Master__c != newProduct.Lookup_Product_Master__c) {
					dealIds.add(newProduct.Deal_Registration__c);
				}
			}
		}
		//Step4: 刷新报备的一级产品名称字段
		if (dealIds.size() > 0) {
			system.debug('---------OverseaDealRegistrationProductHandler--------');
			updateDealLevel1ProductName(dealIds);
		}
	}

	/**
	 * 根据报备产品信息刷新（海外）报备的一级产品名称字段
	 */
	private void updateDealLevel1ProductName(Set<Id> dealIds) {
		List<Deal_Registration__c> deals = 
			[SELECT Id, Deal_Product_Level1__c,
							(SELECT Id, Deal_Registration__c, Lookup_Product_Master__r.Level_1__c FROM Deal_Registration_Products__r)
				 FROM Deal_Registration__c WHERE Id IN : dealIds AND recordTypeID =: CONSTANTS.HUAWEIOVERSEADEALRECORDTYPE];
				
		Map<Id, Deal_Registration__c> dealMap = new Map<Id, Deal_Registration__c>(); 
		for(Deal_Registration__c deal : deals) {
			String dealLevel = '';
			for(Deal_Registration_Product__c product : deal.Deal_Registration_Products__r) {
        if(product.Lookup_Product_Master__r.Level_1__c != null && 
        	 product.Lookup_Product_Master__r.Level_1__c != '' && 
        	 !dealLevel.contains(product.Lookup_Product_Master__r.Level_1__c)) {  
          dealLevel += product.Lookup_Product_Master__r.Level_1__c + '/';
        }
      }
			if(dealLevel.length()>0) {
				dealLevel=dealLevel.substring(0,dealLevel.length()-1);
			}
      deal.Deal_Product_Level1__c = dealLevel;
      dealMap.put(deal.Id, deal);
    }
    
    try {
    	List<Deal_Registration__c> dlist = dealMap.values();
      if(dlist!=null && dlist.size()>0) update dlist;
    } catch (DMLException e) {
      if(trigger.isDelete) {
        trigger.old[0].addError(e.getDMLMessage(0));
      } else {
        trigger.new[0].addError(e.getDMLMessage(0));
      }
      System.debug('UtilDealProduct.processDealLevel Exception: ' + e);
    }
  }

}