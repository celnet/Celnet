/*
* @Purpose : 给中国区超期/即将超期的机会点的Owner发送邮件提醒；
* @Author : steven
* @Date :       2013-11-13
*/
global class EmailChinaOverdueOpportunityScheduleJob implements Schedulable,Database.Batchable<sObject>{    
    public Map<Id,List<Opportunity>> remindOppMap {get;set;} //用于分组保存每个用户的待提醒机会点信息
    public Set<Id> ownerIdSet {get;set;} //保存所有待提醒用户的ID
    public Map<Id,String> ownerEmailMap {get;set;}
    public Map<Id,String> ownerNameMap {get;set;} 
    public String query;
     
    //Constructor
    public EmailChinaOverdueOpportunityScheduleJob() {
        remindOppMap = new Map<Id,List<Opportunity>>();
        ownerIdSet = new Set<Id>();
        ownerEmailMap = new Map<Id,String>();
        ownerNameMap = new Map<Id,String>();
    }   
    
    //ScheduleJob Execute
    global void execute(SchedulableContext SC){
        EmailChinaOverdueOpportunityScheduleJob e = new EmailChinaOverdueOpportunityScheduleJob();
        e.query='SELECT id From User WHERE Isactive = true';
        Id batchId = Database.executeBatch(e,10);
    }
    
    //Batch Start
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);   
    }
    
    //Batch Execute
    global void execute(Database.BatchableContext BC, list<sObject> scope){
        Set<Id> batchOwnerId = new Set<Id>();
        try {
            for(sObject s : scope){
                batchOwnerId.add(((User)s).Id);
            }
            List<Opportunity> oppList = 
                [Select id,Name,Account.Name,Estimated_ContractSign_Amount__c,CloseDate,StageName,CreatedDate,
                                China_Overdue_Status__c,OwnerId,Owner.Email,Owner.Name,owner.IsActive From Opportunity
                    Where (China_Overdue_Status__c = '已超期' or China_Overdue_Status__c = '即将超期') AND Ownerid in:batchOwnerId];
            //遍历机会点列表，并按照用户ID进行分组
            for(Opportunity opp : oppList) {
                ownerIdSet.add(opp.OwnerId);
                if(remindOppMap.get(opp.OwnerId) == null) {
                    //Logic1-1:如果MAP表中不存在该用户，插入该用户，并添加机会点
                    List<Opportunity> oList = new List<Opportunity>();
                    oList.add(opp);
                    remindOppMap.put(opp.OwnerId,oList);
                    ownerEmailMap.put(opp.OwnerId,opp.owner.Email);
                    ownerNameMap.put(opp.OwnerId,opp.owner.Name);
                } else {
                    //Logic1-2:如果MAP表中已存在该用户，直接添加机会点
                    List<Opportunity> oList = remindOppMap.get(opp.OwnerId);
                    oList.add(opp);
                }
            }
            sendRemindMail(); //发送提醒邮件
        } catch(Exception e) {
            System.debug(e);
        }
    }
    
    //Batch Finished
    global void finish(Database.BatchableContext BC){
    }
    
    /**
     * 发送提醒邮件
     */ 
    private void sendRemindMail() {
        try { 
            for(Id ownerId : ownerIdSet) { 
                //Messaging.reserveSingleEmailCapacity(1);
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                List<string> toAddress = new List<string>();
                toAddress.add(ownerEmailMap.get(ownerId));
                mail.setToAddresses(toAddress);
                mail.setSenderDisplayName('Salesforce Supporter');
                mail.setSubject('您有已超期或即将超期的机会点，请及时处理');
                String strbody = htmlBodyConstructor(ownerId);
                mail.setHtmlBody(strbody);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
        } catch(Exception e) {
            System.debug(e);
        }
    }

    /**
     * 构造邮件主体 (注意String长度有限制，一般情况下不会超出)
     */ 
    private String htmlBodyConstructor(Id ownerId) {
        String result = '';
        String overdueList = '';
        String willOverdueList = '';
        String baseUrl = system.Url.getSalesforceBaseUrl().toExternalForm()+'/';
        //Step1：获取Owner相关的机会点全集
        String ownerName = ownerNameMap.get(ownerId);
        List<Opportunity> oList = remindOppMap.get(ownerId);
        //Step2：机会点遍历和分组
        for(Opportunity opp : oList) {
            if(opp.China_Overdue_Status__c == '已超期') overdueList += getHtmlOpp(opp,ownerName,baseUrl);
            if(opp.China_Overdue_Status__c == '即将超期') willOverdueList += getHtmlOpp(opp,ownerName,baseUrl);
        }
        result = '<p>'+ ownerName +'先生/小姐，您好！</p>';
        //Step3：构造已超期的机会点列表
        if(overdueList != '') {
            result += '<p>您有如下 Salesforce 机会点已成为超期机会点，请及时刷新：</p>';
            result += getHtmlTable(overdueList);
        }
        //Step4：构造即将超期的机会点列表
        if(willOverdueList != '') {
            result += '<p>您有如下Salesforce 机会点将在一周内成为超期机会点，请重点关注：</p>';
            result += getHtmlTable(willOverdueList);
        }
        //result += '<p>如果您有任何疑问，请与您的管理员联系：henry.heyi@huawei.com</p>';//delete by lyx 20131119
        result += '<p>十分感谢！</p>';
        result += '<p>salesforce.com</p>';
        return result;
    }
    /**
     * 构造Html形式的机会点列表
     */ 
    private String getHtmlTable(String htmlOppList) {
        String result = '';
        if(htmlOppList != null && htmlOppList != '') {
            result += '<table style="font-family: Arial,微软雅黑;font-size:12px; border:1px solid #AAA;width:1100px;background: #EEE;border-spacing: 1px;text-align:center">';
            result +=   '<tbody>';
            result +=       '<tr style="background: #CCC;font-weight: bold;">';
            result +=           '<th style="width:20%">机会点名称</th>';
            result +=           '<th style="width:20%">客户名</th>';
            result +=           '<th>预签金额(万元)</th>';
            result +=           '<th>预签日期</th>';
            result +=           '<th style="width:20%">SS7状态</th>';
            result +=           '<th>机会点所有人</th>';
            result +=           '<th>创建日期</th>';
            result +=       '</tr>';
            result +=       htmlOppList;
            result +=   '</tbody>';
            result += '</table>';
        }
        return result;
    }
    /**
     * 构造Html格式的机会点String
     */ 
    private String getHtmlOpp(Opportunity opp,String ownerName,String baseUrl) {
        String result = '';
        if(opp != null) {
            result += '<tr style="background: #FFF;">';
            result +=   '<td><a href='+ baseUrl + opp.Id +'>' + opp.Name + '</a></td>';
            result +=   '<td>'+ opp.Account.Name +'</td>';
            result +=   '<td>'+ opp.Estimated_ContractSign_Amount__c +'</td>';
            result +=   '<td>'+ String.valueOf(opp.CloseDate) +'</td>';
            result +=   '<td>'+ opp.StageName +'</td>';
            result +=   '<td>'+ ownerName +'</td>';
            result +=   '<td>'+ opp.CreatedDate +'</td>';
            result += '</tr>';
        }
        return result;
    }
}