/*
* @Purpose : 1、OC认证信息创建、修改或删除时要刷新TCG关联的经销商列表
*						 2、OC填写时系统帮自动填写所有未填写项
* @Author : Steven
* @Date : 2014-02-26
*/
public without sharing class TCGPartnerCertificationHandler implements Triggers.Handler,Database.Batchable<sObject>{ //fires before update & insert   after update & insert & delete
	public String query;
	public Map<String,String> officeMap {get;set;}
	public Map<String,String> cityMap {get;set;}
	public Map<String,String> industryMap {get;set;}
	public Map<String,String> subIndustryMap {get;set;}
	public Boolean codeMapHasInit = false;
	
	public void handle(){
		//Step1:插入或更新前，自动刷新所有的未填写的字段
		if((Trigger.isBefore) && (Trigger.isInsert || Trigger.isUpdate)){
			for(TCG_Partner_Certification__c pc : (List<TCG_Partner_Certification__c>)trigger.new){
				
				//如果是PRM同步进来的根据PRM传入的CODE获取城市行业代表处字段名称  add by steven 2014-06-19
				if(pc.From_PRM__c != 'No'){
					initNameCodeMap();
					pc.Certification_City__c = cityMap.get(pc.Certification_City_Code__c);
					if(pc.Certification_City__c == null || pc.Certification_City__c == ''){
						pc.addError('没有匹配出有效的城市信息');
						continue;
					}
					pc.Representative_Office__c = officeMap.get(pc.Representative_Office_Code__c);
					if(pc.Representative_Office__c == null || pc.Representative_Office__c == ''){
						pc.addError('没有匹配出有效的区域/系统部信息');
						continue;
					}
					pc.Certification_Industry__c = null;
					if(pc.Certification_Industry_Code__c != null) {
						List<String> industryCodeList = pc.Certification_Industry_Code__c.split(';');
						for(String industryCode : industryCodeList) {
							String industry = industryMap.get(industryCode.trim());
							if(industry != null && industry != '') {
								if(pc.Certification_Industry__c != null && pc.Certification_Industry__c != '') {
									pc.Certification_Industry__c = pc.Certification_Industry__c + ';' + industry;
								} else {
									pc.Certification_Industry__c = industry;
								}
							}
						}
					}
					pc.Certification_Sub_Industry__c = null;
					if(pc.Certification_Sub_Industry_Code__c != null) {
						List<String> subIndustryCodeList = pc.Certification_Sub_Industry_Code__c.split(';');
						for(String subIndustryCode : subIndustryCodeList) {
							String subIndustry = subIndustryMap.get(subIndustryCode.trim());
							if(subIndustry != null && subIndustry != '') {
								if(pc.Certification_Sub_Industry__c != null && pc.Certification_Sub_Industry__c != '') {
									pc.Certification_Sub_Industry__c = pc.Certification_Sub_Industry__c + ';' + subIndustry;
								} else {
									pc.Certification_Sub_Industry__c = subIndustry;
								}
							}
						}
					}
				}
				
				//Logic1:当只选择了城市时，保存时系统帮自动选择所有的商业细分行业和子行业
				if((pc.Certification_Industry__c == null) && (pc.Certification_Sub_Industry__c == null)) {
					pc.Certification_Industry__c = '政府综合;医疗卫生;教育;酒店楼宇;中小企业';
					pc.Certification_Sub_Industry__c  = '商业-政府综合-财税保障;商业-政府综合-党务政务;商业-政府综合-公检法司;商业-政府综合-综合及其他;';
					pc.Certification_Sub_Industry__c += '商业-医疗卫生-三甲医院;商业-医疗卫生-非三甲医院（含民营）;商业-医疗卫生-公共卫生系统;';
					pc.Certification_Sub_Industry__c += '商业-教育-高教其他（含民办）;商业-教育-科研院所;商业-教育-普教;商业-教育-社会培训机构;商业-教育-图书馆、文化; 商业-教育-职教、中职（含民办）;';
					pc.Certification_Sub_Industry__c += '商业-酒店楼宇-场馆; 商业-酒店楼宇-景区; 商业-酒店楼宇-酒店; 商业-酒店楼宇-智能楼宇（含房地产）;';
					pc.Certification_Sub_Industry__c += '商业-中小企业-传媒互联网; 商业-中小企业-交通物流; 商业-中小企业-能源; 商业-中小企业-软件和服务外包; 商业-中小企业-商贸连锁; 商业-中小企业-制造业; 商业-中小企业-其他';
				} else {
					//Logic2:如果选择了行业，但没有选择对应的子行业，保存时系统帮自动选择该行业对应的所有子行业
					if(pc.Certification_Industry__c != null) {
						List<String> industryList = pc.Certification_Industry__c.split(';');
						String subIndustry = pc.Certification_Sub_Industry__c;
						if(subIndustry==null) subIndustry = '';
						for(String industry : industryList) {
							if(!subIndustry.contains(industry.trim())) {
								if(industry=='政府综合') subIndustry += ';商业-政府综合-财税保障;商业-政府综合-党务政务;商业-政府综合-公检法司;商业-政府综合-综合及其他';
								if(industry=='医疗卫生') subIndustry += ';商业-医疗卫生-三甲医院;商业-医疗卫生-非三甲医院（含民营）;商业-医疗卫生-公共卫生系统';
								if(industry=='教育')     subIndustry += ';商业-教育-高教其他（含民办）;商业-教育-科研院所;商业-教育-普教;商业-教育-社会培训机构;商业-教育-图书馆、文化; 商业-教育-职教、中职（含民办）';
								if(industry=='酒店楼宇') subIndustry += ';商业-酒店楼宇-场馆; 商业-酒店楼宇-景区; 商业-酒店楼宇-酒店; 商业-酒店楼宇-智能楼宇（含房地产）';
								if(industry=='中小企业') subIndustry += ';商业-中小企业-传媒互联网;商业-中小企业-交通物流;商业-中小企业-能源;商业-中小企业-软件和服务外包;商业-中小企业-商贸连锁;商业-中小企业-制造业;商业-中小企业-其他';
							}
						}
						if(subIndustry.startsWith(';')) subIndustry = subIndustry.substring(1);
						pc.Certification_Sub_Industry__c = subIndustry;
					} 
					//Logic3:如果选择了子行业，但没有选择对应的行业，保存时系统帮自动选择对应的行业
					if(pc.Certification_Sub_Industry__c != null) {
						String industry = pc.Certification_Industry__c;
						String subIndustry = pc.Certification_Sub_Industry__c;
						if(industry==null) industry = '';
						if(!industry.contains('政府综合') && subIndustry.contains('政府综合')) industry += ';政府综合';
						if(!industry.contains('医疗卫生') && subIndustry.contains('医疗卫生')) industry += ';医疗卫生';
						if(!industry.contains('教育')     && subIndustry.contains('教育'))     industry += ';教育';
						if(!industry.contains('酒店楼宇') && subIndustry.contains('酒店楼宇')) industry += ';酒店楼宇';
						if(!industry.contains('中小企业') && subIndustry.contains('中小企业')) industry += ';中小企业';
						if(industry.startsWith(';')) industry = industry.substring(1);
						pc.Certification_Industry__c = industry;
					}
				} //end logic2/3
			} //end for
		} //end Step1
		
		//Step2:插入、更新、删除后，自动刷新TCG关联的经销商列表
		if(Trigger.isAfter){
			Set<Id> partnerIDs = new Set<Id>();
			List<TCG_Partner_Certification__c> pcList = 
				Trigger.isDelete ? (List<TCG_Partner_Certification__c>)trigger.old : (List<TCG_Partner_Certification__c>)trigger.new;
			for(TCG_Partner_Certification__c pc : pcList){
				partnerIDs.add(pc.OC_Name__c);
			}
			refreshTCGRelatedPartner(partnerIDs);
		}
	}
	
	/**
	 * 初始化Name Code对应关系 
	 */
	private void initNameCodeMap() {
		//根据PRM传入的CODE获取城市行业代表处字段名称  add by steven 2014-06-19
		if(codeMapHasInit) return;
		this.officeMap = new Map<String,String>();
		this.cityMap = new Map<String,String>();
		this.industryMap = new Map<String,String>();
		this.subIndustryMap = new Map<String,String>();
		List<DataDictionary__c> ddList = 
			[select Name__c,Code__c,type__c,Object__c from DataDictionary__c 
				where type__c = 'Representative Office' or type__c = 'City' or type__c = 'Commercial Industry' or type__c = 'Customer Group'];
		for(DataDictionary__c dd : ddList) {
			if(dd.Type__c=='Representative Office' && dd.Object__c == 'Opportunity') 
				this.officeMap.put(dd.Code__c, dd.Name__c);
			if(dd.Type__c=='City' && dd.Object__c == 'Opportunity') 
				this.cityMap.put(dd.Code__c, dd.Name__c);
			if(dd.Type__c=='Commercial Industry' && dd.Object__c == 'Account') 
				this.industryMap.put(dd.Code__c, dd.Name__c);
			if(dd.Type__c=='Customer Group' && dd.Object__c == 'Account') 
				this.subIndustryMap.put(dd.Code__c, dd.Name__c);
		}
		this.codeMapHasInit = true;
	}

	/**
	 * 刷新TCG关联的经销商列表 
	 */
	private void refreshTCGRelatedPartner(Set<Id> partnerIDs) {
		//如果小于5个经销商采用直接处理模式
		if(partnerIDs.size()<5) {
			refreshTCGRelatedPartnerImmediately(partnerIDs);
		} else {
		//如果大于5个经销商采用batch模式
			String partners = '';
			for(Id i : partnerIDs){
				if(partners == '') partners = partners + '\''+ i +'\'';
				else partners = partners + ',\''+ i +'\'';
			}
			TCGPartnerCertificationHandler e = new TCGPartnerCertificationHandler();
			e.query='select id from Account where id in ('+ partners +')';
			Id batchId = Database.executeBatch(e,5);
		}
	}
    
	//Batch Start
	public Database.QueryLocator start(Database.BatchableContext BC){
		return Database.getQueryLocator(query);   
	}
	//Batch Execute
	public void execute(Database.BatchableContext BC, list<sObject> scope){
		try {
			Set<id> partnerIDs = new Set<id>();
			//获取本批次所有需要处理的经销商ID
			for(sObject s : scope){
				Account p = (Account)s;
				partnerIDs.add(p.id);
			}
			//根据经销商关联的认证信息去创建TCG关联的经销商列表
			refreshTCGRelatedPartnerImmediately(partnerIDs);
		} catch(Exception e) {
			System.debug(e);
		}
	}
	//Batch Finished
	public void finish(Database.BatchableContext BC){
	}

	/**
	 * 立即刷新TCG关联的经销商列表
	 */
	public static void refreshTCGRelatedPartnerImmediately(Set<id> partnerIDs) {
		Set<Id> oCPartnerIds = new Set<Id>();//只要有任意一条生效的认证都是优选渠道
		try {
			//Step1：查询并删除所有TCG关联的经销商
			List<TCG_Related_Partner__c> oldOCList = [select id from TCG_Related_Partner__c where OC_Name__c in: partnerIDs];
			if(oldOCList.size()>0) {
				delete oldOCList;
			}
			
			//Step2：根据经销商最新的认证信息更新TCG的关联经销商列表
			List<TCG_Related_Partner__c> newOCList = new List<TCG_Related_Partner__c>();
			//1、一次查询出所有涉及到的经销商的有效的认证信息
			List<TCG_Partner_Certification__c> pcList = 
				[select id,OC_Name__c,Certification_City__c,Certification_Sub_Industry__c 
					 from TCG_Partner_Certification__c where OC_Name__c in: partnerIds AND Overdue__c = false];
			//2、分别处理批次中的各个经销商
			for(Id partnerId : partnerIDs) {
				//3、分别处理该经销商的每条认证信息
				for(TCG_Partner_Certification__c pc : pcList) {
					//4、查询出当前认证信息涉及的生效的TCG有哪些
					if(pc.OC_Name__c != partnerId) continue;
					oCPartnerIds.add(partnerId);
					String certificationSubIndustry = ChinaTCGUpdateHandler.getSOQLParameter(pc.Certification_Sub_Industry__c);
					String soql = 'SELECT Id FROM Account'+  
											  ' WHERE recordtypeid = \'' + CONSTANTS.HUAWEICHINATCGRECORDTYPE + '\''+
											    ' AND (TCG_City_1__c includes (\''+ pc.Certification_City__c +'\') OR'+
											         ' TCG_City_2__c includes (\''+ pc.Certification_City__c +'\') OR'+
											         ' TCG_City_3__c includes (\''+ pc.Certification_City__c +'\'))'+
											 	  ' AND TCG_Industry__c includes ('+ certificationSubIndustry +') '+
											 	  ' AND Inactive__c = false ';
					List<Account> tcgList = (List<Account>)Database.query(soql);
					Set<id> tcgIds = new Set<id>();
					//5、创建待处理的TCG关联经销商
					for(Account tcg : tcgList) {
						//6、如果因其他认证已创建，不重复创建
						if(!tcgIds.contains(tcg.id)) { 
							TCG_Related_Partner__c newOC = new TCG_Related_Partner__c();
							newOC.TCG_Name__c = tcg.Id;
							newOC.OC_Name__c = partnerId;
							newOCList.add(newOC);
							tcgIds.add(tcg.id);
						}
					}
				}
			}
			//7、插入保存这些TCG关联的经销商清单
			if(newOCList.size()>0) {
				insert newOCList;
			}
			//8、标示哪些渠道是有生效认证的渠道
			List<Account> partnerList = [select id,is_oc__c from account where id in:partnerIDs];
			for(Account p : partnerList) {
				if(oCPartnerIds.contains(p.Id)) p.is_oc__c = true;
				else p.is_oc__c = false;
			}
			update partnerList;
		} catch(Exception e) {
			System.debug(e);
		}
	}	
}