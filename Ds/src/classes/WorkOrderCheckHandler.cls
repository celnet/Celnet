/*
*@Purpose:新建非例外工单时检查Service_Request_No__c 或Contract_No__c或SFP不为空
*@Author:Brain Zhang
*Create Date：2013-9
*/
public without sharing class WorkOrderCheckHandler implements Triggers.Handler{
	public void handle(){
		 for(Work_Order__c wo:(List<Work_Order__c>)Trigger.new){
       if(wo.Contract_No__c==null && wo.Service_Request_No__c==null && wo.SFP_No__c == null &&
       		wo.work_order_type__c != '(Int) Exception Service' && wo.work_order_type__c != '(N) Exception Service' ){
          wo.addError('Please create work orders under SFP or Service Request.(请在SFP或服务请求下创建工单)');
          //考虑加入代码,禁止以后的handler继续执行
       }
     }
	}
}