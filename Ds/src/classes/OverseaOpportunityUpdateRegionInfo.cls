/**
 * @Purpose : （海外）机会点如果没选区域属性，创建和更新时会根据客户上的相关信息刷新机会点属性
 *            （海外）机会点创建时或更新了机会点客户，会根据客户上的相关信息刷新机会点属性
 * @Author : Steven
 * @Date : 2014-08-29 Refactor
 */
public without sharing class OverseaOpportunityUpdateRegionInfo implements Triggers.Handler{ //before insert,before update
	public void handle(){
		Set<Id> accIds = new Set<Id>();
		//Step1：获取所有涉及的Opportunity AccountId
		for(Opportunity opt : (List<Opportunity>)Trigger.New){
			if(opt.RecordTypeId == CONSTANTS.HUAWEIOVERSEAOPPRECORDTYPE || 
				 opt.RecordTypeId == CONSTANTS.HUAWEIOVERSEADEALOPPRECORDTYPE){ 
				//Step1-1：如果是创建机会点，根据客户属性刷新机会点行业属性
				if(Trigger.isInsert){ 
					accIds.add(opt.accountid);
				}
				//Step1-2：如果是更改了机会点的客户，刷新机会点行业属性
				if(Trigger.isUpdate){ 
					if(opt.accountid != ((Opportunity)Trigger.oldMap.get(opt.id)).accountid) {
						accIds.add(opt.accountid);
					}
				}
				//Step1-3：如果区域属性为空，根据客户信息刷新区域属性
				if((opt.Region__c == null)||(opt.Representative_Office__c == null)||(opt.Country__c == null)) {
					accIds.Add(opt.AccountId);
				}
			}
		}
		if (accIds.isEmpty()) return;
		system.debug('-----OverseaOpportunityUpdateRegionInfo----');
		
		//Step2：一次获取所有涉及的AccountInfo
		Map<Id, Account> accMap = new Map<Id, Account>([
			Select Id,Industry, Region_HW__c, Representative_Office__c, 
						 Country_HW__c, Country_Code_HW__c,Sub_industry_HW__c 
				from Account where Id in:accIds]);
			
		//Step3：根据客户的属性刷机会点属性
		for (Opportunity opt : (List<Opportunity>)Trigger.New){
			if(opt.RecordTypeId == CONSTANTS.HUAWEIOVERSEAOPPRECORDTYPE || 
				 opt.RecordTypeId == CONSTANTS.HUAWEIOVERSEADEALOPPRECORDTYPE){ 
				//Step3-1：如果创建和更新时未填写行业属性，根据客户属性自动刷新
				if((opt.Region__c == null)||(opt.Representative_Office__c == null)||(opt.Country__c == null)) {
					Account acc = accMap.get(opt.AccountId);
					if(opt.Region__c == null){
						opt.Region__c = acc.Region_HW__c;
					}
					if(opt.Representative_Office__c == null){
						opt.Representative_Office__c = acc.Representative_Office__c;
					}
					if(opt.Country__c == null ){
						opt.Country__c = acc.Country_HW__c;
						opt.Country_Code__c = acc.Country_Code_HW__c;
					}
				}
				//Step3-2：创建或更新了机会点客户，刷新机会点行业属性
				if(Trigger.isUpdate){ 
					if(opt.accountid == ((Opportunity)Trigger.oldMap.get(opt.id)).accountid) continue;
				}
				Account acc = accMap.get(opt.AccountId);
				opt.Industry__c = acc.Industry;
				opt.Sub_Industry_E__c = acc.Sub_industry_HW__c;
			} //end if recordtype
		} //end step3
	}

}