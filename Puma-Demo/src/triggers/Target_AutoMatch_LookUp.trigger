/*Author:Leo
 *Date:2014-4-11
 *function:当插入Target__c时自动根据Date_Import__c和Store_Code__c匹配lookup字段
 *		   Date__c、Store__c
 * 2014-4-28 Sunny:不需要lookup到date
 */

trigger Target_AutoMatch_LookUp on Target__c (before insert) {
	
	Set<string> set_Store = new Set<string>();
	Map<string,Id> map_Store = new Map<string,Id>();
	//获取需要匹配的Target__c 
	for(Target__c tar : trigger.new)
	{
		if(tar.Store__c == null || tar.Store__c == '')
		{
			set_Store.add(tar.Store_Code__c);
		}
	}
	
	//如果门店需要匹配lookup
	if(set_Store.size()!=0)
	{
		List<Store__c> tempstore = [Select Store_Code__c, Id From Store__c where Store_Code__c in :set_Store];
		if(tempstore.size()==0)
		{
			return;
		}
		for(Store__c s : tempstore)
		{
			map_Store.put(s.Store_Code__c,s.Id);
		}
		for(Target__c tar : trigger.new)
		{
			if(tar.Store__c !=null)
			{
				continue;
			}
			if(map_Store.containsKey(tar.Store_Code__c))
			{
				tar.Store__c = map_Store.get(tar.Store_Code__c);
			}
		}
	}
}