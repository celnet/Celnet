/*Author:Leo
 *Date:2014-4-11
 *function:当插入Raw_Data__c时自动根据Billing_Doc__c和Store_Code__c匹配lookup字段
 *		   Billing__c、Store__c
 * 2014-4-28 Sunny修改 1.不需要在lookup到billing 2.修改lookup到store的逻辑
 * 
 */
trigger RawData_AutoMatch_LookUp on Raw_Data__c (before insert) 
{
	Set<String> list_StoreCode = new Set<String>();
	Map<String , ID> map_Store = new Map<String , ID>();
	
	for(Raw_Data__c rawData : trigger.new)
	{
		if(rawData.Store_Code__c != null)
		{
			list_StoreCode.add(rawData.Store_Code__c);
		}
	}
	if(list_StoreCode.size() > 0)
	{
		for(Store__c s : [Select Store_Code__c,Id From Store__c Where Store_Code__c in: list_StoreCode])
		{
			map_Store.put(s.Store_Code__c , s.Id);
		}
		for(Raw_Data__c rawData : trigger.new)
		{
			if(map_Store.containsKey(rawData.Store_Code__c))
			{
				rawData.Store__c = map_Store.get(rawData.Store_Code__c);
			}
		}
	}
	
}