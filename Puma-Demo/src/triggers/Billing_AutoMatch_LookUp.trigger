/**
 * 作者：Sunny
 * 自动匹配lookup关系
 **/
trigger Billing_AutoMatch_LookUp on Billing__c (before insert) {
	Set<String> set_StoreCode = new Set<String>();
	for(Billing__c b : trigger.new)
	{
		if(b.Store_Code__c != null)
		{
			set_StoreCode.add(b.Store_Code__c);
		}
	}
	if(set_StoreCode.size() > 0)
	{
		Map<String , ID> map_StoreID = new Map<String , ID>();
		for(Store__c s : [Select id,Store_Code__c from Store__c where Store_Code__c in: set_StoreCode])
		{
			map_StoreID.put(s.Store_Code__c,s.id);
		}
		for(Billing__c b : trigger.new)
		{
			if(map_StoreID.containsKey(b.Store_Code__c))
			{
				b.Store__c = map_StoreID.get(b.Store_Code__c);
			}
		}
	}
}