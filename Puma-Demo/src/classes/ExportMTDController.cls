public class ExportMTDController {
	public string xmlheader {get;set;}
  public string endfile{get;set;}
  public Date ChDate{get;set;}
  public Integer ThisYears{get;set;} 
 public Integer LastYears{get;set;} 
 public String ExcelName{get;set;} 
 public list<Daily_Sales__c> listWeekly_Dashboard{get;set;}

public ExportMTDController()
{
	listWeekly_Dashboard = new list<Daily_Sales__c>();
	list<Daily_Sales__c> list_FOCEC = new list<Daily_Sales__c>();
	list<Daily_Sales__c> list_FOCSC = new list<Daily_Sales__c>();
	list<Daily_Sales__c> list_FOCNC = new list<Daily_Sales__c>();
	list<Daily_Sales__c> list_FPSEC = new list<Daily_Sales__c>();
	list<Daily_Sales__c> list_FPSSC = new list<Daily_Sales__c>();
	list<Daily_Sales__c> list_FPSNC = new list<Daily_Sales__c>();
	
	xmlheader ='<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?>';
		endfile = '</Workbook>';
		Date SeDate = Date.valueOf(ApexPages.currentPage().getParameters().get('SeDate')); 
	System.debug('#######'+SeDate);
		ExcelName = String.valueOf(SeDate)+'SUM(EXC_VAT)';
	for(Daily_Sales__c Dai: [Select d.vs_LY_MTD__c, d.vs_LY_MTD_Net__c, d.VS_LY_Traffic_MTD__c, d.VS_LY_No_of_Tx_MTD__c, 
	d.Unit_TTL__c, d.Unit_FW__c, d.Unit_APP__c, d.Unit_ACC__c, d.UPT_vs_Full_Month_KPI_Target__c, d.UPT_target_MTD__c, 
	d.UPT__c, d.UPT_MTD__c, d.Traffic_This_Year__c, d.Traffic_This_Year_MTD__c, d.Traffic_Last_Year__c, 
	d.Traffic_Last_Year_MTD__c, d.Target__c, d.Target_MTD__c, d.SystemModstamp, d.Store__c, d.Store__r.Region_Acronym__c,d.Store_Type__c, 
	d.Sales_vs_LY_Daily__c, d.Sales_vs_LY_Daily_Net__c, d.Sales_Unit_MTD__c, d.Sales_Target_This_Year_New__c, 
	d.Sales_Target_Last_Year__c, d.Sales_FW__c, d.Sales_FW_Net__c, d.Sales_Amt_MTD__c, d.Sales_APP__c, 
	d.Sales_APP_Net__c, d.Sales_ACC__c, Store__r.Store_Type__c,d.Sales_ACC_Net__c, d.OwnerId, d.Original_Retail_Price_Amount__c, 
	d.Original_Retail_Price_Amount_MTD__c, d.No_of_Tx_This_Year__c, d.No_of_Tx_This_Year_MTD__c, 
	d.No_of_Tx_Last_Year__c, d.No_of_Tx_Last_Year_MTD__c, d.Name, d.Multi_doc__c, d.Multi__c, d.Monthly_Target__c, 
	d.MTD_Traffic_growth__c, d.MTD_Traffic_COMP_MTD__c, d.MTD_Target__c, d.MTD_Sales_Vs_MTD_Target__c, 
	d.MTD_NS_COMP_MTD__c, d.MD_MTD__c, d.Id, d.GP_TTL__c, d.GP_TTL_Rate__c, d.GP_MTD__c, d.GP_FW__c, 
	d.GP_FW_Rate__c, d.GP_Amount_MTD__c, d.GP_APP__c, d.GP_APP_Rate__c, d.GP_ACC__c, d.GP_ACC_Rate__c, 
	d.For_Export__c, d.For_Export_City__c, d.FTW_MTD__c, d.FTW_MTD_M__c, d.Division_TTL__c, d.Division_TTL_Net__c, 
	d.Division_FW__c, d.Division_FW_Net__c, d.Division_APP__c, d.Division_APP_Net__c, d.Division_ACC__c, 
	d.Division_ACC_Net__c, d.Date2__c, d.Daily_Sales_Type__c, d.Daily_Sales_External_ID__c, d.CreatedDate, 
	d.CreatedById, d.Conversion_vs_Full_Month_KPI_Target__c, d.Conversion_target_MTD__c, d.Conversion_MTD__c, 
	d.Conv_Rate__c, d.Comp_growth__c, d.Billing__c, d.Actual_Sales_Vs_Target__c, d.Actual_Sales_This_Year__c, 
	d.Actual_Sales_This_Year_Net__c, d.Actual_Sales_This_Year_MTD__c, d.Actual_Sales_This_Year_MTD_Net__c, 
	d.Actual_Sales_Net_Vs_Target__c, d.Actual_Sales_Last_Year__c, d.Actual_Sales_Last_Year_Net__c, 
	d.Actual_Sales_Last_Year_MTD__c, d.Actual_Sales_Last_Year_MTD_Net__c, d.Achieve_Vs_Monthly_Target__c, 
	d.ATV__c, d.ATV_Net__c, d.ATV_MTD__c, d.ASP_vs_Full_Month_KPI_Target__c, d.ASP_target_MTD__c, d.ASP_MTD__c, 
	d.APP_MTD__c, d.APP_MTD_M__c, d.ACC_MTD__c, Store__r.Store_Code__c,Store__r.Name,d.ACC_MTD_M__c From Daily_Sales__c d 
	Where d.Date2__c =: SeDate and d.Store__c !=null])
	{
		System.debug('#######2222'+Dai.Store__r.Store_Type__c);
		System.debug('#######222211'+Dai.Store__r.Region_Acronym__c);
		if(Dai.Store__r.Store_Type__c == 'FOC')
		{
			System.debug('#######FPC');
			if(Dai.Store__r.Region_Acronym__c == 'EC')
			{
				System.debug('#######EC');
				list_FOCEC.add(Dai);
			}
			else if(Dai.Store__r.Region_Acronym__c == 'SC')
			{
				System.debug('#######SC');
				list_FOCSC.add(Dai);
			}
			else if(Dai.Store__r.Region_Acronym__c == 'NC')
			{
				System.debug('#######NC');
				list_FOCNC.add(Dai);
			}
		}
		else if(Dai.Store__r.Store_Type__c == 'FPS')
		{
			System.debug('#######FPS');
			if(Dai.Store__r.Region_Acronym__c == 'EC')
			{
				System.debug('#######EC');
				list_FPSEC.add(Dai);
			}
			else if(Dai.Store__r.Region_Acronym__c == 'SC')
			{
				System.debug('#######SC');
				list_FPSSC.add(Dai);
			}
			else if(Dai.Store__r.Region_Acronym__c == 'NC')
			{
				System.debug('#######NC');
				list_FPSNC.add(Dai); 
			} 
		}
	}
	List<Daily_Sales__c> list_All = new List<Daily_Sales__c>();
	List<Daily_Sales__c> list_FOC = new List<Daily_Sales__c>();
	list_FOC.addAll(list_FOCEC);
	list_FOC.addAll(list_FOCSC);
	list_FOC.addAll(list_FOCNC);
	
	List<Daily_Sales__c> list_FPS = new List<Daily_Sales__c>();
	list_FPS.addAll(list_FPSEC);
	list_FPS.addAll(list_FPSSC);
	list_FPS.addAll(list_FPSNC);
	
	list_All.addAll(list_FOC);
	list_All.addAll(list_FPS);
	
	listWeekly_Dashboard.addAll(list_FOCEC);
	listWeekly_Dashboard.add(GetTotal(list_FOCEC,'FOC','EC'));
	listWeekly_Dashboard.addAll(list_FOCSC);
	listWeekly_Dashboard.add(GetTotal(list_FOCSC,'FOC','SC'));
	listWeekly_Dashboard.addAll(list_FOCNC);
	listWeekly_Dashboard.add(GetTotal(list_FOCNC,'FOC','NC'));
	listWeekly_Dashboard.add(GetTotal(list_FOC,'FOC',''));
	
	listWeekly_Dashboard.addAll(list_FPSEC);
	listWeekly_Dashboard.add(GetTotal(list_FPSEC,'FPS','EC'));
	listWeekly_Dashboard.addAll(list_FPSSC);
	listWeekly_Dashboard.add(GetTotal(list_FPSSC,'FPS','SC'));
	listWeekly_Dashboard.addAll(list_FPSNC);
	listWeekly_Dashboard.add(GetTotal(list_FPSNC,'FPS','NC'));
	listWeekly_Dashboard.add(GetTotal(list_FPS,'FPS',''));
	
	listWeekly_Dashboard.add(GetTotal(list_All,'All',''));
}
	public Daily_Sales__c GetTotal(list<Daily_Sales__c> listRank,String TypeS,String Type)
	{
		Daily_Sales__c DailyS = new Daily_Sales__c();
		DailyS.FTW_MTD__c = 0;
		DailyS.APP_MTD__c = 0; 
		DailyS.ACC_MTD__c = 0;
		
		DailyS.Target_MTD__c = 0;
		DailyS.Actual_Sales_Last_Year_MTD_Net__c = 0;
		DailyS.Original_Retail_Price_Amount_MTD__c = 0;
		
		
		DailyS.GP_Amount_MTD__c = 0;
		DailyS.No_of_Tx_This_Year_MTD__c = 0;
		DailyS.Traffic_This_Year_MTD__c = 0;
		DailyS.Sales_Unit_MTD__c = 0;
		
		DailyS.For_Export_City__c = TypeS;
		DailyS.For_Export__c = Type;
		
		DailyS.Conversion_target_MTD__c = 0;
		DailyS.ASP_target_MTD__c = 0;
		DailyS.UPT_target_MTD__c = 0;
		DailyS.Traffic_Last_Year_MTD__c = 0;
	for(Daily_Sales__c Dail :listRank)
	{
		
		
		if(Dail.FTW_MTD__c != null)
		{
			DailyS.FTW_MTD__c += Dail.FTW_MTD__c;
		}
		if(Dail.APP_MTD__c != null)
		{
			DailyS.APP_MTD__c += Dail.APP_MTD__c; 
		}
		if(Dail.ACC_MTD__c != null)
		{
			DailyS.ACC_MTD__c += Dail.ACC_MTD__c;
		}
		if(Dail.Target_MTD__c != null)
		{
			DailyS.Target_MTD__c += Dail.Target_MTD__c;
		}
		if(Dail.Actual_Sales_Last_Year_MTD_Net__c != null)
		{
			DailyS.Actual_Sales_Last_Year_MTD_Net__c += Dail.Actual_Sales_Last_Year_MTD_Net__c;
		}
		if(Dail.Original_Retail_Price_Amount_MTD__c != null)
		{
			DailyS.Original_Retail_Price_Amount_MTD__c += Dail.Original_Retail_Price_Amount_MTD__c;
		}
		
		if(Dail.GP_Amount_MTD__c != null)
		{
			DailyS.GP_Amount_MTD__c += Dail.GP_Amount_MTD__c;
		}
		
		
		if(Dail.Traffic_This_Year_MTD__c != null)
		{
			DailyS.Traffic_This_Year_MTD__c += Dail.Traffic_This_Year_MTD__c;
		}
		
		if(Dail.Sales_Unit_MTD__c != null)
		{
			DailyS.Sales_Unit_MTD__c += Dail.Sales_Unit_MTD__c;
		}
		
		if(Dail.No_of_Tx_This_Year_MTD__c != null)
		{
			DailyS.No_of_Tx_This_Year_MTD__c += Dail.No_of_Tx_This_Year_MTD__c;
		}
		
		
		if(Dail.Conversion_target_MTD__c != null)
		{
			DailyS.Conversion_target_MTD__c += Dail.Conversion_target_MTD__c;
		}
		
		if(Dail.ASP_target_MTD__c != null)
		{
			DailyS.ASP_target_MTD__c += Dail.ASP_target_MTD__c;
		}
		
		if(Dail.UPT_target_MTD__c != null)
		{
			DailyS.UPT_target_MTD__c += Dail.UPT_target_MTD__c;
		}
		
		if(Dail.Traffic_Last_Year_MTD__c != null)
		{
			DailyS.Traffic_Last_Year_MTD__c += Dail.Traffic_Last_Year_MTD__c;
		}
		
		
	}
	return DailyS;
	}
}