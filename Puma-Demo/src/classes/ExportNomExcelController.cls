public class ExportNomExcelController {
	public string xmlheader {get;set;}
  public string endfile{get;set;}
  public Date ChDate{get;set;}
  public Integer ThisYears{get;set;}
 public Integer LastYears{get;set;}
 public String ExcelName{get;set;}
 public list<Weekly_Dashboard__c> listWeekly_Dashboard{get;set;}
 	public class ShowExcle
    {
    	public String ShowName{get;set;}
    	public List<Daily_Sales__c> ShowList{get;set;}
    }
public ExportNomExcelController()
{
	xmlheader ='<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?>';
		endfile = '</Workbook>';
		Date SeDate = Date.valueOf(ApexPages.currentPage().getParameters().get('SeDate')); 

		ExcelName = String.valueOf(SeDate)+'SUM(EXC_VAT)';
	listWeekly_Dashboard = [Select 
    							Id,
    							Order__c,
    							Net_Sales__c,
						    	Target_Sales__c,
						    	Sales_Achievement_Rate__c,
						    	ACC_Sales_Percent__c,
						    	APP_Sales_Percent__c,
						    	FTW_Sales_Percent__c,
						    	FTW_Sales__c,
						    	APP_Sales__c,
						    	ACC_Sales__c,
						    	ATV__c,
						    	CR__c,
						    	Target_CR__c,
						    	CR_Achievement_Rate__c,
						    	UPT__c,
						    	Target_UPT__c,
						    	UPT_Achievement_Rate__c,
						    	ASP__c,
						    	Target_ASP__c,
						    	ASP_Achievement_Rate__c,
						    	MD__c,
						    	MTD_NS_COMP__c,
						    	MTD_Traffic_COMP__c,
						    	GP__c,
						    	Feedback__c,
						    	WeeklyDashboard_External_ID__c,
						    	Weekly_Dashboard_Type__c,
						    	Region__r.Name,
						    	Store_Type__c,
						    	Store__c,
						    	Store__r.Name,
						 		Store__r.Store_Code__c,
						 		Store__r.Region__r.Name,
						 		Store__r.Store_Type__c,
						 		Store_Type_VF__c,
						 		Region_VF__c
			    			From 
			    				Weekly_Dashboard__c 
			    			Where 
			    				Week_Start_Date__c <=: Date.valueOf(SeDate) 
			    			And 
			    				Week_End_Date__c >=: Date.valueOf(SeDate)
			    			Order by All_Order__c asc, Store_Type__c asc,Order__c desc,Region__r.Name asc,Region_Order__c desc, Store__r.Store_Code__c asc];
}
}