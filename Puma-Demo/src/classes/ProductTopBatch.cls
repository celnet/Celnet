/*
 * Author: Steven
 * Date: 2014-4-22
 * Description: 根据给定日期统计Raw Data的利润，销售额，销量生成Product Top，维度为门店，产品，日期
 * 2014-05-22 Sunny不使用Product__c字段进行汇总，而使用Article code进行汇总，汇总之后，按照article code查找product，并且将product字段赋值。
 */
global class ProductTopBatch implements Database.Batchable<sObject>, Database.Stateful 
{
    // ProductTop的计算日期
    global Date runDate;

    // 生成的ProductTop
    global List<Product_Top__c> ptList;
    
    global ProductTopBatch(Date d)
    {
        runDate = d;
        ptList = new List<Product_Top__c>();
    }
    
    global Database.Querylocator start(Database.BatchableContext BC)
    { 
        return Database.getQueryLocator([Select Id From Store__c]);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        // 遍历所有门店
        for(Store__c s : (List<Store__c>) scope)
        {
            System.debug('门店>>>>' + s.Id);
            
            // 从Raw Data中获取每个产品(SKU)当天每个门店的SalesAmt, SalesQty, GPAmt
            AggregateResult[] arList = [Select 
                                            Article_Code__c,
                                            SUM(GP_Amt__c) ga, 
                                            SUM(Sales_Amt__c) sa, 
                                            SUM(Sales_Qty__c) sq
                                        From 
                                            Raw_Data__c 
                                        Where
                                            Store__c =:s.Id
                                        And 
                                            Transaction_Date__c =: runDate
                                        Group by
                                            Article_Code__c];
            
            System.debug('RawData统计的SKU' + arList.size());
            //获取所有的article code
            Map<String , ID> map_ArticleProduct = new Map<String , ID>();
            Set<String> set_Article = new Set<String>();
            for(AggregateResult ar : arList){
                set_Article.add((String)ar.get('Article_Code__c'));
            }
            for(Product__c p:[Select id,Article_Code__c From Product__c Where Article_Code__c in: set_Article order by Launch_Month__c]){
                map_ArticleProduct.put(p.Article_Code__c , p.Id);
            }
            // 遍历每一个产品的记录
            for(AggregateResult ar : arList)
            {
                Product_Top__c pt = new Product_Top__c();
                
                //pt.Product__c = (Id)ar.get('Product__c');
                String sArticleCode = (String)ar.get('Article_Code__c');
                pt.Product__c = map_ArticleProduct.get(sArticleCode);
                pt.Store__c = s.Id;
                pt.Date2__c = runDate;
                pt.Sales_Amount__c = (Double)ar.get('sa');
                pt.Sales_Quantity__c = (Double)ar.get('sq');
                pt.GP_Amount__c = (Double)ar.get('ga');
                pt.Product_Top_External_ID__c = s.Id + '-' + pt.Product__c + '-' + runDate;

                if(pt.Product__c != null)
                {
                    ptList.add(pt);
                }
            }
        }
    }
    
    global void finish(Database.BatchableContext BC)
    {
        System.debug('产生的ProductTop条数' + ptList.size());
        if(ptList.size() > 0)
        {
            upsert ptList Product_Top_External_ID__c;
        }   
        
        WeeklyDashboardBatch wdb = new WeeklyDashboardBatch(runDate);
        Database.executeBatch(wdb, 1);
    }
}