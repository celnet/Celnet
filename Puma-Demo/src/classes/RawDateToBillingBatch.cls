global class RawDateToBillingBatch implements Database.Batchable<sObject>
{
    global Date RunDate;
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        if(RunDate == null)
        {
            RunDate = Date.today();
        }
        return Database.getQueryLocator([Select r.UOM__c, r.Transaction_Date__c, r.SystemModstamp, r.Store__c, r.Store_Type__c, 
        r.Store_Desc__c, r.Store_Code__c, r.Std_Unit_Cost__c, r.Std_Retail_Price__c, r.Std_Retail_Amt__c, r.Size__c, r.Season__c, 
        r.Sales_Qty__c, r.Sales_Amt__c, r.SKU_No__c, r.Rounded_Sales_Amt__c, r.RBU__c, r.Product__c, r.Product_Type__c, 
        r.Product_Type2__c, r.Posting_Date__c, r.OwnerId, r.Original_Retail_Price__c, r.Original_Retail_Amt__c, r.Net_Sales_Amt__c, 
        r.Name, r.Line_Name__c, r.Launch_Month__c, r.Item_No__c, r.IsDeleted, r.Inv_No__c, r.Id, r.Gender__c, r.GP_Rate__c, 
        r.GP_Amt__c, r.Division__c, r.Discount_Rate__c, r.Description__c, r.Cty_Division__c, r.Concept__c, r.Color__c, 
        r.Black_Inline__c, r.Billing__c,r.Staff_Doc__c, r.Billing_Doc__c, r.Article_Code__c From Raw_Data__c r where r.Posting_Date__c =: RunDate]); 
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        Map<String,Map<String,String>> Map_DocAndNum= new Map<String,Map<String,String>>();
        Set<String> Set_Doc = new Set<String>();
        Set<String> Set_StDoc = new Set<String>();
        Double AmtSun = 0;
        for(sObject sObj : scope)
        {
            Raw_Data__c Raw = (Raw_Data__c)sObj;
            Set_Doc.add(Raw.Billing_Doc__c);
            Set_StDoc.add(Raw.Staff_Doc__c);
            //Raw.Sales_Amt__c;
            //Raw.Sales_Qty__c;
            Map<String,String> Map_DateAndNum = new Map<String,String>();
            if(Map_DocAndNum.containsKey(Raw.Billing_Doc__c))
            {
                Map_DateAndNum = Map_DocAndNum.get(Raw.Billing_Doc__c);
                
                AmtSun = 0;
                if(Map_DateAndNum.containsKey('Sales_Amt'))
                {
                    AmtSun = Double.valueOf(Map_DateAndNum.get('Sales_Amt'));
                }
                AmtSun +=  Raw.Sales_Amt__c;
                Map_DateAndNum.put('Sales_Amt',String.valueOf(AmtSun));
                
                AmtSun = 0;
                if(Map_DateAndNum.containsKey('Sales_Qty'))
                {
                    AmtSun = Double.valueOf(Map_DateAndNum.get('Sales_Qty'));
                }
                AmtSun +=  Raw.Sales_Qty__c;
                Map_DateAndNum.put('Sales_Qty',String.valueOf(AmtSun));
                
                
                AmtSun = 0;
                if(Map_DateAndNum.containsKey('GP_Amt'))
                {
                    AmtSun = Double.valueOf(Map_DateAndNum.get('GP_Amt'));
                }
                AmtSun +=  Raw.GP_Amt__c;
                Map_DateAndNum.put('GP_Amt',String.valueOf(AmtSun));
                
                AmtSun = 0;
                if(Map_DateAndNum.containsKey('Net_Sales_Amt'))
                {
                    AmtSun = Double.valueOf(Map_DateAndNum.get('Net_Sales_Amt'));
                }
                AmtSun +=  Raw.Net_Sales_Amt__c;
                Map_DateAndNum.put('Net_Sales_Amt',String.valueOf(AmtSun));
            }
            else
            {
                AmtSun = 0;
                AmtSun +=  Raw.Sales_Amt__c;
                Map_DateAndNum.put('Sales_Amt',String.valueOf(AmtSun));
                
                AmtSun = 0;
                AmtSun +=  Raw.Sales_Qty__c;
                Map_DateAndNum.put('Sales_Qty',String.valueOf(AmtSun));
                
                AmtSun = 0;
                AmtSun +=  Raw.GP_Amt__c;
                Map_DateAndNum.put('GP_Amt',String.valueOf(AmtSun));
                
                AmtSun = 0;
                AmtSun +=  Raw.Net_Sales_Amt__c;
                Map_DateAndNum.put('Net_Sales_Amt',String.valueOf(AmtSun));
                
                if(Raw.Posting_Date__c != null)
                {
                    Map_DateAndNum.put('Date',String.valueOf(Raw.Posting_Date__c));
                }
                if(Raw.Store_Code__c != null)
                {
                    Map_DateAndNum.put('Store_Code',String.valueOf(Raw.Store_Code__c)); 
                }
                if(Raw.Staff_Doc__c != null)
                {
                    Map_DateAndNum.put('Staff_Doc',String.valueOf(Raw.Staff_Doc__c)); 
                }
                /*if(Raw.Store_Code__c != null)
                {
                    Map_DateAndNum.put('Store_Code',String.valueOf(Raw.Store_Code__c)); 
                }*/
            }
            Map_DocAndNum.put(Raw.Billing_Doc__c,Map_DateAndNum);
                //Sales_Amt__c
            //Sum_Qty__c
        }
        Map<String,Double> MapSum_QtyOld = new Map<String,Double>();
        Map<String,Double> MapNet_Sales_AmtOld = new Map<String,Double>();
        Map<String,Double> MapSales_AmtOld = new Map<String,Double>();
        Map<String,Double> MapReturnOld = new Map<String,Double>();
        For(Billing__c Bill:[Select Name__c ,Sum_Qty__c, Net_Sales_Amt__c , Sales_Amt__c, Return__c
        From Billing__c where Name__c IN: Set_Doc ])
        {
            MapSum_QtyOld.put(Bill.Name__c,Bill.Sum_Qty__c);
            MapNet_Sales_AmtOld.put(Bill.Name__c,Bill.Net_Sales_Amt__c);
            MapSales_AmtOld.put(Bill.Name__c,Bill.Sales_Amt__c);
            MapReturnOld.put(Bill.Name__c,Bill.Return__c);
        }
        
        //Map<String,String> Map_StIDAndStCod = new Map<String,String>();
        //for(Store__c St:[Select Id ,Store_Code__c From Store__c])
        //{
            //Map_StIDAndStCod.put(St.Store_Code__c,St.Id);
        //}
        Map<String,String> Map_TaIDAndTaCod = new Map<String,String>();
        for(Staff__c St:[Select Id ,Name__c From Staff__c where Name__c IN: Set_StDoc])
        {
            Map_TaIDAndTaCod.put(St.Name__c,St.Id);
        }
        list<Billing__c> list_Bill = new list<Billing__c>();
        for(String BillingName:Map_DocAndNum.keySet())
        {
            Billing__c Bil = new Billing__c();
            Bil.Name__c = BillingName;
            Bil.DateImport__c =  Date.valueOf(Map_DocAndNum.get(BillingName).get('Date'));
            if(MapSales_AmtOld.containsKey(BillingName))
            {
                Bil.Sales_Amt__c = Double.valueOf(Map_DocAndNum.get(BillingName).get('Sales_Amt'))+MapSales_AmtOld.get(BillingName);
            }
            else
            {
                Bil.Sales_Amt__c = Double.valueOf(Map_DocAndNum.get(BillingName).get('Sales_Amt'));
            }
            if(MapSum_QtyOld.containsKey(BillingName))
            {
                Bil.Sum_Qty__c = Double.valueOf(Map_DocAndNum.get(BillingName).get('Sales_Qty'))+MapSum_QtyOld.get(BillingName);
            }
            else
            {
                Bil.Sum_Qty__c = Double.valueOf(Map_DocAndNum.get(BillingName).get('Sales_Qty'));
            }
            if(MapReturnOld.containsKey(BillingName))
            {
                Bil.Return__c = Double.valueOf(Map_DocAndNum.get(BillingName).get('GP_Amt'))+MapReturnOld.get(BillingName);
            }
            else
            {
                Bil.Return__c = Double.valueOf(Map_DocAndNum.get(BillingName).get('GP_Amt'));
            }
            if(MapNet_Sales_AmtOld.containsKey(BillingName))
            {
                Bil.Net_Sales_Amt__c = Double.valueOf(Map_DocAndNum.get(BillingName).get('Net_Sales_Amt'))+MapNet_Sales_AmtOld.get(BillingName);
            }
            else
            {
                Bil.Net_Sales_Amt__c = Double.valueOf(Map_DocAndNum.get(BillingName).get('Net_Sales_Amt'));
            }
            
            
            if(Map_TaIDAndTaCod.containsKey(Map_DocAndNum.get(BillingName).get('Staff_Doc')))
            {
                Bil.Staff__c = Map_TaIDAndTaCod.get(Map_DocAndNum.get(BillingName).get('Staff_Doc'));
            }
            Bil.Store_Code__c = Map_DocAndNum.get(BillingName).get('Store_Code');
            
            /*if(Map_StIDAndStCod.containsKey(Bil.Store_Code__c))
            {
                Bil.Store__c = Map_StIDAndStCod.get(Bil.Store_Code__c);
            }*/
            /*if(Map_TaIDAndTaCod.containsKey(Bil.Store_Code__c))
            {
                Bil.Store__c = Map_StIDAndStCod.get(Bil.Store_Code__c);
            }
            */
            list_Bill.add(Bil);
        }
        upsert list_Bill Name__c; 
       
    }
    /*public Map<String,Double> AddNumByMap(String KeyN,Map<String,Double> Map_DateAndNum)
    {
        Map<String,Double> Map_NewDateAndNum = new Map<String,Double>();
        if(Map_DateAndNum.containsKey('Sales_Amt'))
        {
            AmtSun = Map_DateAndNum.get('Sales_Amt');
        }
        AmtSun +=  Raw.Sales_Amt__c;
        Map_NewDateAndNum.put('Sales_Amt',AmtSun);
        return Map_NewDateAndNum;
    }*/
    global void finish(Database.BatchableContext BC)
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
 
        String repBody = '您好: <br><br>';
        repBody += 'Daily Sales报表计算已经完成，您可以登陆Salesforce系统查看、导出相关报表。<br>';
        repBody += '祝您工作愉快 <br>';
        repBody += '__________________________________________________ <br>';
        repBody += '本邮件由Salesforce系统产生，请勿回复。<br>';
        repBody += '如有任何疑问或者要求，请联系系统管理人员。<br>';
        User u = [select Email from User where id =:UserInfo.getUserId()];
        String emailAddress = String.ValueOf(u.Email);
        String[] repAddress =new string[]{emailAddress};
        mail.setToAddresses(repAddress);
        mail.setHtmlBody(repBody);
        mail.setSubject('Daily Sales报表计算完成');
        mail.setSaveAsActivity(false);//存为活动
        mail.setSenderDisplayName('Salesforce');
        mail.setCharset('UTF-8');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
       
        list<Raw_Data__c> list_Raw_Data = new list<Raw_Data__c>();
        Map<String,String> MapBiNameAndId = new Map<String,String>();
        
        if(RunDate == null)
        {
            RunDate = Date.today();
        }
        for(Billing__c Bi:[Select Id,Name__c from Billing__c where DateImport__c =:RunDate]) 
        {
            MapBiNameAndId.put(Bi.Name__c,Bi.Id);
        }
        for(Raw_Data__c Ra:[Select Id,Billing_Doc__c from Raw_Data__c where Posting_Date__c =:RunDate])
        {
            if(MapBiNameAndId.containsKey(Ra.Billing_Doc__c))
            {
                Raw_Data__c Raw = new Raw_Data__c();
                Raw.Id = Ra.Id;
                Raw.Billing__c = MapBiNameAndId.get(Ra.Billing_Doc__c);
                list_Raw_Data.add(Raw);
            }
        }
        if(list_Raw_Data.size() > 0)
        {
            update list_Raw_Data;
        }
        
    }
}