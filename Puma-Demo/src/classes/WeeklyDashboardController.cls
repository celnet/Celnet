/*
 * Author: Steven
 * Date: 2014-4-27
 * Description: controller
 */
public class WeeklyDashboardController {
    // 用于选项列表
    public List<Store__c> storeList{get;set;}
    
    public String selectedYear{get;set;}
    
    public String selectDate{get;set;}
    public List<SelectOption> years{get;set;}
    public String selectedWeek{get;set;}
    public List<SelectOption> weeks{get;set;}
    public String selectedStore{get;set;}
    public List<SelectOption> stores{get;set;}
    public String selectedRegion{get;set;}
    public List<SelectOption> regions{get;set;}
    public String selectedStoreType{get;set;}
    public List<SelectOption> storeTypes{get;set;}
    
    // 控制页面显示
    public boolean editingFeedback{get;set;}
    public boolean displayingFeedback{get;set;}
    public String displayPDF{get;set;}
    public boolean displaySelections{get;set;}
    public String displayReport{get;set;}
    public String displayDashboard{get;set;}
    public String displayPDFDashboard{get;set;}
    
    // dashboard
    public Weekly_Dashboard__c wd{get;set;}
    public List<Weekly_Dashboard__c> wdAllList{get;set;}
    public String title{get;set;}
    public String week{get;set;}
    
    // dashboard显示的图表
    public String salesAchvChartUrl{get;set;}
    public String salesDivChartUrl{get;set;}
    public String CRChartUrl{get;set;}
    public String UPTChartUrl{get;set;}
    public String ASPChartUrl{get;set;}
    
    public WDChart wdc{get;set;}
    
    // 用于pdf
    public List<WDWrapper> wdwList{get;set;}
    
    public class WDWrapper
    {
        public Weekly_Dashboard__c weekD{get;set;}
        public String dTitle{get;set;}
        public String salesAchvChartUrl{get;set;}
        public String salesDivChartUrl{get;set;}
        public String CRChartUrl{get;set;}
        public String UPTChartUrl{get;set;}
        public String ASPChartUrl{get;set;} 
        public String feedback{get;set;}
    }
    
    public class WDChart
    {
        public Double salesTarget{get;set;}
        public Double salesNet{get;set;}
        public Integer salesMetric1{get;set;}
        public Integer salesMetric2{get;set;}
        public Integer salesMetric3{get;set;}
        public Integer salesMetric4{get;set;}
        public Integer salesMetric5{get;set;}
        public Integer salesMetric6{get;set;}
        public Double salesAPP{get;set;}
        public Double salesACC{get;set;}
        public Double salesFTW{get;set;}
        public Double CRTarget{get;set;}
        public Double CR{get;set;}
        public Double CRMetric1{get;set;}
        public Double CRMetric2{get;set;}
        public Double CRMetric3{get;set;}
        public Double UPTTarget{get;set;}
        public Double UPT{get;set;}
        public Double UPTMetric1{get;set;}
        public Double UPTMetric2{get;set;}
        public Double UPTMetric3{get;set;}
        public Double UPTMetric4{get;set;}
        public Double ASPTarget{get;set;}
        public Double ASP{get;set;}
        public Double ASPMetric1{get;set;}
        public Double ASPMetric2{get;set;}
        public Double ASPMetric3{get;set;}
        public Double ASPMetric4{get;set;}
        public Double ASPMetric5{get;set;}
    }
    
    public WeeklyDashboardController(){
        this.storeList = [Select Id, Name, Store_Type__c, Region__c, Region__r.Name From Store__c];
        
        this.stores = new List<SelectOption>();
        this.regions = new List<SelectOption>();
        this.storeTypes = new List<SelectOption>();
        
        this.years = new List<SelectOption>();
        this.weeks = new List<SelectOption>();
        
        this.assignStoreTypeSelectOptions();
        this.assignYearSelectOptions();
        
        this.displayPDF = '';
        this.displaySelections = true;
        this.displayingFeedback = true;
        this.editingFeedback = false;
        this.displayPDFDashboard = 'display:none';
        
        this.wdc = new WDChart();
        
        this.generateDashboard();
        this.queryAllWeeklyDashboard();
        selectDate = this.selectedWeek;
        
    }
    
    public void generateDashboard()
    {
        selectDate = this.selectedWeek;
        this.queryAllWeeklyDashboard();
        this.displayDashboard = '';
        this.displayReport = 'display:none';
        this.displayPDFDashboard = 'display:none';
        
        this.wd = this.queryWeeklyDashboard();
        if(wd == null)
        {
            wd = new Weekly_Dashboard__c();
        }
        /*
        if(this.wd.Feedback__c == null)
        {
            this.wd.Feedback__c = '';
        }
        */
        this.assignTitle();
        
        this.salesAchvChartUrl = this.getSalesAchvUrl(wd);
        this.salesDivChartUrl = this.getSalesDivUrl(wd);
        this.CRChartUrl = this.getCRUrl(wd);
        this.UPTChartUrl = this.getUPTUrl(wd);
        this.ASPChartUrl = this.getASPUrl(wd);
        selectDate = this.selectedWeek;
    }
    
    public void generateAllDashboard()
    {
        this.wdwList = new List<WDWrapper>();
        selectDate = this.selectedWeek;
        List<Weekly_Dashboard__c> weekDashList = new List<Weekly_Dashboard__c>();
        weekDashList = [Select 
                            Id,
                            Net_Sales__c,
                            Target_Sales__c,
                            Sales_Achievement_Rate__c,
                            ACC_Sales_Percent__c,
                            APP_Sales_Percent__c,
                            FTW_Sales_Percent__c,
                            CR__c,
                            Target_CR__c,
                            CR_Achievement_Rate__c,
                            UPT__c,
                            Target_UPT__c,
                            UPT_Achievement_Rate__c,
                            ASP__c,
                            Target_ASP__c,
                            ASP_Achievement_Rate__c,
                            MD__c,
                            MTD_NS_COMP__c,
                            MTD_Traffic_COMP__c,
                            GP__c,
                            Feedback__c,
                            Target_ASP_Net__c,
                            Target_Sales_Net__c,
                            Store_Type__c,
                            Region__r.Name,
                            Store__c,
                            Store__r.Name,
                            Weekly_Dashboard_Type__c,
                            WeeklyDashboard_External_ID__c
                        From 
                            Weekly_Dashboard__c 
                        Where 
                            Week_Start_Date__c <=: Date.valueOf(this.selectedWeek) 
                        And 
                            Week_End_Date__c >=: Date.valueOf(this.selectedWeek)
                        Order by All_Order__c desc, Store_Type__c desc,Order__c desc,Region__r.Name asc,Region_Order__c desc, Store__c desc];
        
        for(Weekly_Dashboard__c w : weekDashList)
        {
            WDWrapper wdw = new WDWrapper();
            wdw.weekD = w;
            wdw.dTitle = this.assignTotalTitle(w);
            wdw.ASPChartUrl = this.getASPUrl(w);
            wdw.UPTChartUrl = this.getUPTUrl(w);
            wdw.CRChartUrl = this.getCRUrl(w);
            wdw.salesAchvChartUrl = this.getSalesAchvUrl(w);
            wdw.salesDivChartUrl = this.getSalesDivUrl(w);
            wdw.feedback = this.manualNewLine(w.Feedback__c, 15);
            
            wdwList.add(wdw);
        }
        selectDate = this.selectedWeek;
    }
    
    public String getSalesAchvUrl(Weekly_Dashboard__c wd)
    {
        WDChart wdc = new WDChart();
        
        if(wd.Net_Sales__c == null)
        {
            wd.Net_Sales__c = 0;
        }
        
        if(wd.Target_Sales_Net__c > wd.Net_Sales__c)
        {
            wdc.salesTarget = 80 + (Integer)(10 * Math.random());
            if(wd.Target_Sales_Net__c != null && wd.Net_Sales__c != null && wd.Target_Sales_Net__c != 0)
            wdc.salesNet = 80 * wd.Net_Sales__c / wd.Target_Sales_Net__c;
            wdc.salesMetric1 = 0;
            if((Integer) (wd.Target_Sales_Net__c * 0.12 * 2 / 50000) != 0)
            {
                
                wdc.salesMetric2 = (Integer) (wd.Target_Sales_Net__c * 0.12 / 50000) * 10;
                wdc.salesMetric3 = (Integer) (wd.Target_Sales_Net__c * 0.12 * 2 / 50000) * 10;
                wdc.salesMetric4 = (Integer) (wd.Target_Sales_Net__c * 0.12 * 3 / 50000) * 10;
                wdc.salesMetric5 = (Integer) (wd.Target_Sales_Net__c * 0.12 * 4 / 50000) * 10;
                wdc.salesMetric6 = (Integer) (wd.Target_Sales_Net__c * 0.12 / 10000) * 10;
            }
            else if((Integer) (wd.Target_Sales_Net__c * 0.12 * 2 / 5000) != 0)
            {
                wdc.salesMetric2 = (Integer) (wd.Target_Sales_Net__c * 0.12 / 5000) * 10;
                wdc.salesMetric3 = (Integer) (wd.Target_Sales_Net__c * 0.12 * 2 / 5000) * 10;
                wdc.salesMetric4 = (Integer) (wd.Target_Sales_Net__c * 0.12 * 3 / 5000) * 10;
                wdc.salesMetric5 = (Integer) (wd.Target_Sales_Net__c * 0.12 * 4 / 5000) * 10;
                wdc.salesMetric6 = (Integer) (wd.Target_Sales_Net__c * 0.12 / 1000) * 10;
            }
            else
            {
                wdc.salesMetric2 = (Integer) (wd.Target_Sales_Net__c * 0.12 / 500) * 10;
                wdc.salesMetric3 = (Integer) (wd.Target_Sales_Net__c * 0.12 * 2 / 500) * 10;
                wdc.salesMetric4 = (Integer) (wd.Target_Sales_Net__c * 0.12 * 3 / 500) * 10;
                wdc.salesMetric5 = (Integer) (wd.Target_Sales_Net__c * 0.12 * 4 / 500) * 10;
                wdc.salesMetric6 = (Integer) (wd.Target_Sales_Net__c * 0.12 / 100) * 10;
            }
        }
        else
        {
            wdc.salesNet = 80 + (Integer)(10 * Math.random());
            if(wd.Target_Sales_Net__c != null && wd.Net_Sales__c != null && wd.Net_Sales__c != 0)
            wdc.salesTarget = 80 * wd.Target_Sales_Net__c / wd.Net_Sales__c;
            wdc.salesMetric1 = 0;
            if((Integer) (wd.Net_Sales__c * 0.12 * 2 / 50000) != 0)
            {
                wdc.salesMetric2 = (Integer) (wd.Net_Sales__c * 0.12 / 50000) * 10;
                wdc.salesMetric3 = (Integer) (wd.Net_Sales__c * 0.12 * 2 / 50000) * 10;
                wdc.salesMetric4 = (Integer) (wd.Net_Sales__c * 0.12 * 3 / 50000) * 10;
                wdc.salesMetric5 = (Integer) (wd.Net_Sales__c * 0.12 * 4 / 50000) * 10;
                wdc.salesMetric6 = (Integer) (wd.Net_Sales__c * 0.12 / 10000) * 10;
            }
            else if((Integer) (wd.Net_Sales__c * 0.12 * 2 / 5000) != 0)
            {
                wdc.salesMetric2 = (Integer) (wd.Net_Sales__c * 0.12 / 5000) * 10;
                wdc.salesMetric3 = (Integer) (wd.Net_Sales__c * 0.12 * 2 / 5000) * 10;
                wdc.salesMetric4 = (Integer) (wd.Net_Sales__c * 0.12 * 3 / 5000) * 10;
                wdc.salesMetric5 = (Integer) (wd.Net_Sales__c * 0.12 * 4 / 5000) * 10;
                wdc.salesMetric6 = (Integer) (wd.Net_Sales__c * 0.12 / 1000) * 10;
            }
            else
            {
                wdc.salesMetric2 = (Integer) (wd.Net_Sales__c * 0.12 / 500) * 10;
                wdc.salesMetric3 = (Integer) (wd.Net_Sales__c * 0.12 * 2 / 500) * 10;
                wdc.salesMetric4 = (Integer) (wd.Net_Sales__c * 0.12 * 3 / 500) * 10;
                wdc.salesMetric5 = (Integer) (wd.Net_Sales__c * 0.12 * 4 / 500) * 10;
                wdc.salesMetric6 = (Integer) (wd.Net_Sales__c * 0.12 / 100) * 10;
            }
        }
        
        String salesAchvUrl = 'http://chart.apis.google.com/chart?chs=400x200&chd=t:'+ wdc.salesTarget + ',' + wdc.salesNet + '&cht=bvs&chco=346EA8|009933&chxt=x,y&chxl=0:|Target|Net%20Sales|1:|' + wdc.salesMetric1 + '|' + wdc.salesMetric2 + '|' + wdc.salesMetric3 + '|' + wdc.salesMetric4 + '|' + wdc.salesMetric5 + '|' + wdc.salesMetric6 + '&chbh=140,40';
        selectDate = this.selectedWeek;
        return salesAchvUrl;
    }
    
    public String getSalesDivUrl(Weekly_Dashboard__c wd)
    {
        WDChart wdc = new WDChart();
        
        wdc.salesAPP = wd.APP_Sales_Percent__c;
        wdc.salesACC = wd.ACC_Sales_Percent__c;
        wdc.salesFTW = wd.FTW_Sales_Percent__c;
        
        String salesDivUrl = 'http://chart.apis.google.com/chart?chs=400x200&chd=t:' + wdc.salesFTW + ',' + wdc.salesAPP + ',' + wdc.salesACC + '&cht=p&chco=346EA8,009933,CC0000&chl=FTW|APP|ACC';
        return salesDivUrl;
    }
    
    public String getCRUrl(Weekly_Dashboard__c wd)
    {
        WDChart wdc = new WDChart();
        
        if(wd.CR__c != null && wd.Target_CR__c != null)
        {
            if(wd.CR__c > wd.Target_CR__c)
            {
                wdc.CR = 80 + (10 * Math.random());
                if(wd.Target_CR__c != null && wd.CR__c != null && wd.CR__c != 0)
                wdc.CRTarget = 80 * wd.Target_CR__c / wd.CR__c;
                wdc.CRMetric1 = 0;
                wdc.CRMetric2 = (Integer)((wd.CR__c * 12 / 2) / 10);
                wdc.CRMetric3 = (Integer)((wd.CR__c * 12) / 10);
            }
            else
            {
                wdc.CRTarget = 80 + (10 * Math.random());
                if(wd.Target_CR__c != null && wd.CR__c != null && wd.Target_CR__c != 0)
                wdc.CR = 80 * wd.CR__c / wd.Target_CR__c;
                wdc.CRMetric1 = 0;
                wdc.CRMetric2 = (Integer)((wd.Target_CR__c * 12 / 2) / 10);
                wdc.CRMetric3 = (Integer)((wd.Target_CR__c * 12) / 10);
            }
        }
        
        String CRUrl = 'http://chart.apis.google.com/chart?chs=400x200&chd=t:' + wdc.CR + ',' + wdc.CRTarget + '&cht=bhs&chco=346EA8|009933&chxt=x,y&chxl=0:|' + wdc.CRMetric1 + '%|' + wdc.CRMetric2 + '%|' + wdc.CRMetric3 + '%|1:|WK CR|Target&chbh=70,20';
        return CRUrl;
    }
    
    public String getUPTUrl(Weekly_Dashboard__c wd)
    {
        WDChart wdc = new WDChart();
        
        if(wd.UPT__c != null && wd.Target_UPT__c != null)
        {
            if(wd.UPT__c > wd.Target_UPT__c)
            {
                wdc.UPT = 89 + (10 * Math.random());
                if(wd.Target_UPT__c != null && wd.UPT__c != null && wd.UPT__c != 0)
                wdc.UPTTarget = 80 * wd.Target_UPT__c / wd.UPT__c;
                wdc.UPTMetric1 = 0;
                wdc.UPTMetric2 = (Integer)(((wd.UPT__c * 12 / 3) / 100) * 100);
                wdc.UPTMetric2 = wdc.UPTMetric2 / 10;
                wdc.UPTMetric3 = (Integer)(((wd.UPT__c * 12 * 2 / 3) / 100) * 100);
                wdc.UPTMetric3 = wdc.UPTMetric3 / 10;
                wdc.UPTMetric4 = (Integer)(((wd.UPT__c * 12) / 100) * 100);
                wdc.UPTMetric4 = wdc.UPTMetric4 / 10;
            }
            else
            {
                wdc.UPTTarget = 89 + (10 * Math.random());
                if(wd.Target_UPT__c != null && wd.UPT__c != null && wd.Target_UPT__c != 0)
                wdc.UPT = 89 * wd.UPT__c / wd.Target_UPT__c;
                wdc.UPTMetric1 = 0;
                wdc.UPTMetric2 = (Integer)(((wd.Target_UPT__c * 12 / 3) / 100) * 100);
                wdc.UPTMetric2 = wdc.UPTMetric2 / 10;
                wdc.UPTMetric3 = (Integer)(((wd.Target_UPT__c * 12 * 2 / 3) / 100) * 100);
                wdc.UPTMetric3 = wdc.UPTMetric3 / 10;
                wdc.UPTMetric4 = (Integer)(((wd.Target_UPT__c * 12) / 100) * 100);
                wdc.UPTMetric4 = wdc.UPTMetric4 / 10;
            }
        }
        
        String UPTUrl = 'http://chart.apis.google.com/chart?chs=400x200&chd=t:' + wdc.UPT + ',' + wdc.UPTTarget + '&cht=bhs&chco=346EA8|009933&chxt=x,y&chxl=0:|' + wdc.UPTMetric1 + '|' + wdc.UPTMetric2 + '|' + wdc.UPTMetric3 + '|' + wdc.UPTMetric4 + '|1:|WK UPT|Target&chbh=70,20';
        return UPTUrl;
    }
    
    public String getASPUrl(Weekly_Dashboard__c wd)
    {
        WDChart wdc = new WDChart();
        
        if(wd.ASP__c != null && wd.Target_ASP_Net__c != null)
        {
            if(wd.ASP__c > wd.Target_ASP_Net__c)
            {
                wdc.ASP = 80 + (10 * Math.random());
                if(wd.Target_ASP_Net__c != null && wd.ASP__c != null || wd.ASP__c != 0)
                wdc.ASPTarget = 80 * wd.Target_ASP_Net__c / wd.ASP__c;
                wdc.ASPMetric1 = 0;
                wdc.ASPMetric2 = (Integer)(((wd.ASP__c * 12 / 4) / 100) );
                wdc.ASPMetric2 = wdc.ASPMetric2 * 10;
                wdc.ASPMetric3 = (Integer)(((wd.ASP__c * 12 * 2 / 4) / 100) );
                wdc.ASPMetric3 = wdc.ASPMetric3 * 10;
                wdc.ASPMetric4 = (Integer)(((wd.ASP__c * 12 * 3 / 4) / 100));
                wdc.ASPMetric4 = wdc.ASPMetric4 * 10;
                wdc.ASPMetric5 = (Integer)(((wd.ASP__c * 12) / 100) );
                wdc.ASPMetric5 = wdc.ASPMetric5 * 10;
            }
            else
            {
                wdc.ASPTarget = 80 + (10 * Math.random());
                if(wd.ASP__c != null && wd.Target_ASP_Net__c != null && wd.Target_ASP_Net__c != 0)
                wdc.ASP = 80 * wd.ASP__c / wd.Target_ASP_Net__c;
                wdc.ASPMetric1 = 0;
                wdc.ASPMetric2 = (Integer)(((wd.Target_ASP_Net__c * 12 / 4) / 100) );
                wdc.ASPMetric2 = wdc.ASPMetric2 * 10;
                wdc.ASPMetric3 = (Integer)(((wd.Target_ASP_Net__c * 12 * 2 / 4) / 100) );
                wdc.ASPMetric3 = wdc.ASPMetric3 * 10;
                wdc.ASPMetric4 = (Integer)(((wd.Target_ASP_Net__c * 12 * 3 / 4) / 100) );
                wdc.ASPMetric4 = wdc.ASPMetric4 * 10;
                wdc.ASPMetric5 = (Integer)(((wd.Target_ASP_Net__c * 12) / 100) );
                wdc.ASPMetric5 = wdc.ASPMetric5 * 10;
            }
        }
        
        String ASPUrl = 'http://chart.apis.google.com/chart?chs=400x200&chd=t:' + wdc.ASP + ',' + wdc.ASPTarget + '&cht=bhs&chco=346EA8|009933&chxt=x,y&chxl=0:|' + wdc.ASPMetric1 + '|' + wdc.ASPMetric2 + '|' + wdc.ASPMetric3 + '|' + wdc.ASPMetric4 + '|' + wdc.ASPMetric5 + '|1:|WK ASP|Target&chbh=70,20';
        return ASPUrl;
    }
    
    // 转为pdf
    public void generateAllDashboardPDF()
    {
        this.generateAllDashboard();
        this.displayPDF = 'pdf';
        this.displaySelections = false;
        this.displayReport = 'display:none';
        this.displayDashboard = 'display:none';
        this.displayPDFDashboard = '';
    }
    
    // 生成报表
    public void generateReport()
    {
        this.queryAllWeeklyDashboard();
        this.displayReport = '';
        this.displayDashboard = 'display:none';
        
        Date d = Date.valueOf(this.selectedWeek);
        selectDate = String.valueOf(d);
        Integer weekNumber = (Date.newInstance(d.year(), 1, 1).toStartOfWeek().addDays(1).daysBetween(d.toStartOfWeek().addDays(1)) / 7);
        Integer startMonth = d.toStartOfWeek().addDays(1).month();
        Integer startDay = d.toStartOfWeek().addDays(1).day();
        Integer endMonth = d.toStartOfWeek().addDays(7).month();
        Integer endDay = d.toStartOfWeek().addDays(7).day();
        
        this.week = '第' + weekNumber + '周' + ' ' + startMonth + '.' + startDay + '-' + endMonth + '.' + endDay;
    }
    
    // 查询出所有Dashboard，用于Report
    public void queryAllWeeklyDashboard()
    {
        wdAllList = new List<Weekly_Dashboard__c>();
        wdAllList = [Select 
                                Id,
                                Order__c,
                                Net_Sales__c,
                                Target_Sales__c,
                                Sales_Achievement_Rate__c,
                                ACC_Sales_Percent__c,
                                APP_Sales_Percent__c,
                                FTW_Sales_Percent__c,
                                FTW_Sales__c,
                                APP_Sales__c,
                                ACC_Sales__c,
                                ATV__c,
                                CR__c,
                                Target_CR__c,
                                CR_Achievement_Rate__c,
                                UPT__c,
                                Target_UPT__c,
                                UPT_Achievement_Rate__c,
                                ASP__c,
                                Target_ASP__c,
                                ASP_Achievement_Rate__c,
                                MD__c,
                                MTD_NS_COMP__c,
                                MTD_Traffic_COMP__c,
                                GP__c,
                                Feedback__c,
                                Target_ASP_Net__c,
                                Target_Sales_Net__c,
                                WeeklyDashboard_External_ID__c,
                                Weekly_Dashboard_Type__c,
                                Region__r.Name,
                                Store_Type__c,
                                Store__c,
                                Store__r.Name,
                                Store__r.Store_Code__c,
                                Store__r.Region__r.Name,
                                Store__r.Store_Type__c,
                                Store_Type_VF__c,
                                Region_VF__c
                            From 
                                Weekly_Dashboard__c 
                            Where 
                                Week_Start_Date__c <=: Date.valueOf(this.selectedWeek) 
                            And 
                                Week_End_Date__c >=: Date.valueOf(this.selectedWeek)
                            Order by All_Order__c asc, Store_Type__c asc,Order__c desc,Region__r.Name asc,Region_Order__c desc, Store__r.Store_Code__c asc];
        String firstStoreType = '';
        for(Weekly_Dashboard__c w1 : wdAllList)
        {
            if(w1.Store_Type__c != null)
            {
                firstStoreType = w1.Store_Type__c;
                w1.Store_Type_VF__c = w1.Store_Type__c;
                break;
            }
        }
        
        for(Weekly_Dashboard__c w2 : wdAllList)
        {
            if(w2.Store_Type__c != null && w2.Store_Type__c != firstStoreType)
            {
                w2.Store_Type_VF__c = w2.Store_Type__c;
                break;
            }
        }
    }
    
    // 查询出当周的所选项的Dashboard
    public Weekly_Dashboard__c queryWeeklyDashboard()
    {
        List<Weekly_Dashboard__c> wdList = new List<Weekly_Dashboard__c>();
        if(this.selectedStore == '全部')
        {
            if(this.selectedRegion == '全部')
            {
                wdList = [Select 
                                Id,
                                Net_Sales__c,
                                Target_Sales__c,
                                Sales_Achievement_Rate__c,
                                ACC_Sales_Percent__c,
                                APP_Sales_Percent__c,
                                FTW_Sales_Percent__c,
                                CR__c,
                                Target_CR__c,
                                CR_Achievement_Rate__c,
                                UPT__c,
                                Target_UPT__c,
                                UPT_Achievement_Rate__c,
                                ASP__c,
                                Target_ASP__c,
                                ASP_Achievement_Rate__c,
                                MD__c,
                                MTD_NS_COMP__c,
                                MTD_Traffic_COMP__c,
                                GP__c,
                                Target_ASP_Net__c,
                                Target_Sales_Net__c,
                                Feedback__c,
                                WeeklyDashboard_External_ID__c
                            From 
                                Weekly_Dashboard__c 
                            Where 
                                Week_Start_Date__c <=: Date.valueOf(this.selectedWeek) 
                            And 
                                Week_End_Date__c >=: Date.valueOf(this.selectedWeek)
                            And
                                Weekly_Dashboard_Type__c = 'Total'
                            And 
                                Store_Type__c =: this.selectedStoreType];
            }
            else
            {
                wdList = [Select 
                                Id,
                                Net_Sales__c,
                                Target_Sales__c,
                                Sales_Achievement_Rate__c,
                                ACC_Sales_Percent__c,
                                APP_Sales_Percent__c,
                                FTW_Sales_Percent__c,
                                CR__c,
                                Target_CR__c,
                                CR_Achievement_Rate__c,
                                UPT__c,
                                Target_UPT__c,
                                UPT_Achievement_Rate__c,
                                ASP__c,
                                Target_ASP__c,
                                ASP_Achievement_Rate__c,
                                MD__c,
                                MTD_NS_COMP__c,
                                MTD_Traffic_COMP__c,
                                GP__c,
                                Feedback__c,
                                Target_ASP_Net__c,
                                Target_Sales_Net__c,
                                WeeklyDashboard_External_ID__c
                            From 
                                Weekly_Dashboard__c 
                            Where 
                                Week_Start_Date__c <=: Date.valueOf(this.selectedWeek) 
                            And 
                                Week_End_Date__c >=: Date.valueOf(this.selectedWeek)
                            And
                                Weekly_Dashboard_Type__c = 'Region'
                            And 
                                Store_Type__c =: this.selectedStoreType
                            And 
                                Region__r.Name =: this.selectedRegion];
            }
        }
        else
        {
            wdList = [Select 
                            Id,
                            Net_Sales__c,
                            Target_Sales__c,
                            Sales_Achievement_Rate__c,
                            ACC_Sales_Percent__c,
                            APP_Sales_Percent__c,
                            FTW_Sales_Percent__c,
                            CR__c,
                            Target_CR__c,
                            CR_Achievement_Rate__c,
                            UPT__c,
                            Target_UPT__c,
                            UPT_Achievement_Rate__c,
                            ASP__c,
                            Target_ASP__c,
                            ASP_Achievement_Rate__c,
                            MD__c,
                            MTD_NS_COMP__c,
                            MTD_Traffic_COMP__c,
                            GP__c,
                            Feedback__c,
                            Target_ASP_Net__c,
                            Target_Sales_Net__c,
                            WeeklyDashboard_External_ID__c
                        From 
                            Weekly_Dashboard__c 
                        Where 
                            Week_Start_Date__c <=: Date.valueOf(this.selectedWeek) 
                        And 
                            Week_End_Date__c >=: Date.valueOf(this.selectedWeek)
                        And
                            Weekly_Dashboard_Type__c = 'Store'
                        And
                            Store__r.Name =: this.selectedStore];
            }
        
        if(wdList.size() > 0)
        {
            return wdList[0];
        }
        else
        {
            return null;
        }
    }
    
    // 控制Feedback编辑
    public void editFeedback()
    {
        this.editingFeedback = true;
        this.displayingFeedback = false;
    }
    
    public void saveFeedback()
    {
        if(wd != null)
        {
            update wd;
        }
        this.displayingFeedback = true;
        this.editingFeedback = false;
    }
    
    public void cancelEditFeedback()
    {
        this.displayingFeedback = true;
        this.editingFeedback = false;
    }
    
    // 选项列表赋值
    public void assignYearSelectOptions()
    {
        this.years.clear();
        this.years.add(new SelectOption(Date.today().addYears(-1).year() + '',Date.today().addYears(-1).year() + ''));
        this.years.add(new SelectOption(Date.today().year() + '',Date.today().year() + ''));
        this.selectedYear = Date.today().year() + '';
        this.assignWeekSelectOptions();
    }
    
    public void assignWeekSelectOptions()
    {
        this.weeks.clear();
        Date d;
        if(Date.valueOf(this.selectedYear + '-1-1').toStartOfWeek().addDays(1).year() == Integer.valueOf(this.selectedYear))
        {
            d = Date.valueOf(this.selectedYear + '-1-1');
        }
        else
        {
            d = Date.valueOf(this.selectedYear + '-1-1').toStartOfWeek().addDays(8);
        }
        
        String week = '';
        Integer weekNumber = 0;
        Integer startMonth = 0;
        Integer startDay = 0;
        Integer endMonth = 0;
        Integer endDay = 0;
        
        for(Integer i = 7; d.year() == Integer.valueOf(this.selectedYear);)
        {
            weekNumber = (Date.newInstance(d.year(), 1, 1).toStartOfWeek().addDays(1).daysBetween(d.toStartOfWeek().addDays(1)) / 7);
            startMonth = d.toStartOfWeek().addDays(1).month();
            startDay = d.toStartOfWeek().addDays(1).day();
            endMonth = d.toStartOfWeek().addDays(7).month();
            endDay = d.toStartOfWeek().addDays(7).day();
            String weekString = '第' + weekNumber + '周' + ' ' + startMonth + '.' + startDay + '-' + endMonth + '.' + endDay;
            this.weeks.add(new SelectOption(this.selectedYear + '-' + startMonth + '-' + startDay, weekString));
            if(d.toStartOfWeek() == Date.today().toStartOfWeek())
            {
                this.selectedWeek = this.selectedYear + '-' + startMonth + '-' + startDay;
            }
            d = d.addDays(7);
        }
    }
    
    public void assignStoreTypeSelectOptions()
    {
        this.storeTypes.clear();
        Set<String> storeTypeSet = new Set<String>();
        
        for(Store__c s : this.storeList)
        {
            storeTypeSet.add(s.Store_Type__c);
        }
        
        for(String st : storeTypeSet)
        {
            if(st != null)
            {
                this.storeTypes.add(new selectOption(st,st));
            }
        }
        
        this.selectedStoreType = storeTypes[0].getValue();
        this.assignRegionSelectOptions();
    }
    
    public void assignRegionSelectOptions()
    {
        this.regions.clear();
        Set<String> regionSet = new Set<String>();
 
        for(Store__c s : this.storeList)
        {
            if(s.Store_Type__c == this.selectedStoreType && s.Region__r.Name != null)
            {
                regionSet.add(s.Region__r.Name);    
            }
        }
        
        this.regions.add(new SelectOption('全部','全部'));
        system.debug('>>><>'+regionSet);
        for(String r : regionSet)
        {
            this.regions.add(new SelectOption(r,r));
        }
        
        this.selectedRegion = this.regions[0].getValue();
        this.assignStoreSelectOptions();
    }
    
    public void assignStoreSelectOptions()
    {
        this.stores.clear();
        Set<String> storeSet = new Set<String>();
        
        for(Store__c s : storeList)
        {
            if(s.Store_Type__c == this.selectedStoreType && s.Region__r.Name == this.selectedRegion)
            {
                storeSet.add(s.Name);
            }
            
            if(s.Store_Type__c == this.selectedStoreType && this.selectedRegion == '全部')
            {
                storeSet.add(s.Name);
            }
        }
        
        this.stores.add(new SelectOption('全部','全部'));
        
        for(String s : storeSet)
        {
            this.stores.add(new SelectOption(s,s));
        }
        this.selectedStore = this.stores[0].getValue();
    }
    
    public String assignTotalTitle(Weekly_Dashboard__c w)
    {
        String t = '';
        
        Integer weekNumber = (Date.newInstance(Date.valueOf(this.selectedWeek).year(), 1, 1).toStartOfWeek().daysBetween(Date.valueOf(this.selectedWeek).toStartOfWeek()) / 7);
        
        Integer weekStartMonth = Date.valueOf(this.selectedWeek).toStartOfWeek().addDays(1).month();
        Integer weekEndMonth = Date.valueOf(this.selectedWeek).toStartOfWeek().addDays(7).month();
        
        String startMonth = this.monthNumberToString(weekStartMonth);
        String endMonth = this.monthNumberToString(weekEndMonth);
        
        Integer weekStartDay = Date.valueOf(this.selectedWeek).toStartOfWeek().addDays(1).day();
        Integer weekEndDay = Date.valueOf(this.selectedWeek).toStartOfWeek().addDays(7).day();
        
        if(w.Weekly_Dashboard_Type__c == 'All')
        {
            
        }
        if(w.Weekly_Dashboard_Type__c == 'Store')
        {
            t = w.Store__r.Name + ' Week' + weekNumber + ' ' + weekStartDay + ' ' + startMonth + '-' + weekEndDay + ' ' + endMonth; 
        }
        else
        {
            if(w.Weekly_Dashboard_Type__c == 'Region')
            {
                t = w.Store_Type__c + ' ' +  w.Region__r.Name + ' Week' + weekNumber + ' ' + weekStartDay + ' ' + startMonth + '-' + weekEndDay + ' ' + endMonth;
            }
            else if(w.Weekly_Dashboard_Type__c == 'Total')
            {
                t = w.Store_Type__c + ' Total Week' + weekNumber + ' ' + weekStartDay + ' ' + startMonth + '-' + weekEndDay + ' ' + endMonth;
            }
            else if(w.Weekly_Dashboard_Type__c == 'All')
            {
                t = 'All  Week' + weekNumber + ' ' + weekStartDay + ' ' + startMonth + '-' + weekEndDay + ' ' + endMonth;
            }
        }
        
        return t;
    }
    
    public void assignTitle()
    {
        Integer weekNumber = (Date.newInstance(Date.valueOf(this.selectedWeek).year(), 1, 1).toStartOfWeek().daysBetween(Date.valueOf(this.selectedWeek).toStartOfWeek()) / 7);
        
        Integer weekStartMonth = Date.valueOf(this.selectedWeek).toStartOfWeek().addDays(1).month();
        Integer weekEndMonth = Date.valueOf(this.selectedWeek).toStartOfWeek().addDays(7).month();
        
        String startMonth = this.monthNumberToString(weekStartMonth);
        String endMonth = this.monthNumberToString(weekEndMonth);
        
        Integer weekStartDay = Date.valueOf(this.selectedWeek).toStartOfWeek().addDays(1).day();
        Integer weekEndDay = Date.valueOf(this.selectedWeek).toStartOfWeek().addDays(7).day();
        
        if(this.selectedStore == '全部')
        {
            if(this.selectedRegion == '全部')
            {
                this.title = this.selectedStoreType + ' Total Week' + weekNumber + ' ' + weekStartDay + ' ' + startMonth + '-' + weekEndDay + ' ' + endMonth;
            }
            else
            {
                this.title = this.selectedStoreType + ' ' + this.selectedRegion + ' Week' + weekNumber + ' ' + weekStartDay + ' ' + startMonth + '-' + weekEndDay + ' ' + endMonth;
            }
        }
        else
        {
            this.title = this.selectedStore + ' Week' + weekNumber + ' ' + weekStartDay + ' ' + startMonth + '-' + weekEndDay + ' ' + endMonth; 
        }
    } 
    
    public String monthNumberToString(Integer m)
    {
        String month = '';
        if(m == 1)
        {
            month = 'Jan';
        }
        else if(m == 2)
        {
            month = 'Feb';
        }
        else if(m == 3)
        {
            month = 'Wed';
        }
        else if(m == 4)
        {
            month = 'Thu';
        }
        else if(m == 5)
        {
            month = 'May';
        }
        else if(m == 6)
        {
            month = 'Jun';
        }
        else if(m == 7)
        {
            month = 'Jul';
        }
        else if(m == 8)
        {
            month = 'Aug';
        }
        else if(m == 9)
        {
            month = 'Sep';
        }
        else if(m == 10)
        {
            month = 'Oct';
        }
        else if(m == 11)
        {
            month = 'Nov';
        }
        else if(m == 12)
        {
            month = 'Dec';
        }
        return month;
    }
    
    public String manualNewLine(String longString, Integer numberPerLine)
    {
        String newLinedString = '';
        if(longString != null)
        {
            newLinedString = longString.left(numberPerLine);
            if(longString.length() > numberPerLine)
            {
                String remainString = longString.substring(numberPerLine + 1);
                while(remainString.length() > numberPerLine)
                {
                    newLinedString += '\n';
                    newLinedString += remainString.left(numberPerLine);
                    remainString = remainString.substring(numberPerLine + 1);
                }
                
                newLinedString += '\n';
                newLinedString += remainString;
            }
        }
        return newLinedString;
    }
    public PageReference ExportExcelNom()
    {
        //window.open('/,'newwindow','height=100,width=200,top=150,left=250,toolbar=no,menubar=no,scrollbars=yes, resizable=yes,location=no, status=no');
     
        return new PageReference('/apex/ExportNomExcel?SeDate='+this.selectedWeek);
    }
}