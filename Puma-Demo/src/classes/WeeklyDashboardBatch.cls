/*
 * Author: Steven
 * Date: 2014-4-27
 * Description: 计算WeeklyDashboard
 *     2014-6-3 update: 修改用于在VF页面显示的字段的赋值逻辑，当值为null时，不取null
 */
global class WeeklyDashboardBatch implements Database.Batchable<sObject>, Database.Stateful
{
    global Date weekStartDate;
    global Date weekEndDate;
    global String weekNumber;
    global Date dat;
    String query;
    global List<Target__c> targetList;
    global List<Daily_Sales__c> dsList;
    
    // <store type, weeklydashboard>
    global Map<String, Weekly_Dashboard__c> totalWD;
    // <store type + region, weeklydashboard>
    global Map<String, Weekly_Dashboard__c> regionWD;
    // <store, weeklydashboard>
    global Map<String, Weekly_Dashboard__c> storeWD;
    // 
    global Weekly_Dashboard__c allWD;
    
    global WeeklyDashboardBatch(Date d)
    {
        this.dat = d;
        this.assignWeekInfo(d);
        this.targetList = [Select 
                                Id,
                                Store__c,
                                Store__r.Region__c,
                                Store__r.Store_Type__c,
                                UPT__c, 
                                Sales_Volume__c,
                                Conversion_Rate__c, ASP__c, Date_Import__c 
                            From Target__c 
                            Where Date_Import__c >=: this.weekStartDate 
                            And Date_Import__c <=: this.weekEndDate];
        this.dsList = [Select 
                                Id,
                                Sales_ACC_Net__c,
                                Sales_FW_Net__c, 
                                Sales_APP_Net__c, 
                                Actual_Sales_This_Year_Net__c,
                                No_of_Tx_This_Year__c, 
                                Traffic_This_Year__c, 
                                Unit_TTL__c, 
                                Original_Retail_Price_Amount__c,
                                Actual_Sales_This_Year_MTD_Net__c, 
                                Actual_Sales_Last_Year_MTD_Net__c,
                                Traffic_This_Year_MTD__c,
                                Traffic_Last_Year_MTD__c,
                                GP_TTL__c,
                                Store__r.Store_Type__c, 
                                Store__r.Region__c, 
                                Store__c, 
                                Date2__c
                            From 
                                Daily_Sales__c 
                            Where 
                                Date2__c >=: this.weekStartDate 
                            And 
                                Date2__c <=: this.weekEndDate];
        this.totalWD = new Map<String, Weekly_Dashboard__c>();
        this.regionWD = new Map<String, Weekly_Dashboard__c>();
        this.storeWD = new Map<String, Weekly_Dashboard__c>();
        this.allWD = new Weekly_Dashboard__c();
        allWD = new Weekly_Dashboard__c();
        allWD.Week_Start_Date__c = this.weekStartDate;
        allWD.Week_End_Date__c = this.weekEndDate;
        allWD.Week_Number__c = this.weekNumber;
        allWD.Weekly_Dashboard_Type__c = 'All';
        allWD.WeeklyDashboard_External_ID__c = this.weekNumber + '-All';
        allWD.Store_Type_VF__c = 'All';
        allWD.All_Order__c = 2;
    }
    
    global Database.Querylocator start(Database.BatchableContext BC)
    { 
        return Database.getQueryLocator([Select Id, Store_Type__c, Region__c, Region__r.Name From Store__c]);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        for(Store__c st : (List<Store__c>) scope)
        {
            String storeTypeRegion = st.Store_Type__c + '' + st.Region__c;
            
            Weekly_Dashboard__c tWD;
            Weekly_Dashboard__c rWD;
            Weekly_Dashboard__c sWD;
            
            if(this.totalWD.get(st.Store_Type__c) == null)
            {
                tWD = new Weekly_Dashboard__c();
                tWD.Week_Start_Date__c = this.weekStartDate;
                tWD.Week_End_Date__c = this.weekEndDate;
                tWD.Week_Number__c = this.weekNumber;
                tWD.Weekly_Dashboard_Type__c = 'Total';
                tWD.Store_Type__c = st.Store_Type__c;
                tWD.WeeklyDashboard_External_ID__c = this.weekNumber + '-Total-' + st.Store_Type__c;
                tWD.Store_Type_VF__c = (st.Store_Type__c==null?'':st.Store_Type__c) + 'Total';
                tWD.Order__c = 1;
                tWD.All_Order__c = 1;
            }
            else
            {
                tWD = this.totalWD.get(st.Store_Type__c);
            }
            
            if(this.regionWD.get(storeTypeRegion) == null)
            {
                rWD = new Weekly_Dashboard__c();
                rWD.Week_Start_Date__c = this.weekStartDate;
                rWD.Week_End_Date__c = this.weekEndDate;
                rWD.Week_Number__c = this.weekNumber;
                rWD.Weekly_Dashboard_Type__c = 'Region';
                rWD.Store_Type__c = st.Store_Type__c;
                rWD.Region__c = st.Region__c;
                rWD.WeeklyDashboard_External_ID__c = this.weekNumber + '-Region-' + st.Region__c + '-' + st.Store_Type__c;
                if(st.Region__c != null) {
                   rWD.Region_VF__c = st.Region__r.Name + 'Total';
                } else {
                    rWD.Region_VF__c = 'Total';
                }
                rWD.Order__c = 2;
                rWD.Region_Order__c = 1;
                rWD.All_Order__c = 1;
            }
            else
            {
                rWD = this.regionWD.get(storeTypeRegion);
            }
            
            if(this.storeWD.get(st.Id) == null)
            {
                sWD = new Weekly_Dashboard__c();
                sWD.Week_Start_Date__c = this.weekStartDate;
                sWD.Week_End_Date__c = this.weekEndDate;
                sWD.Week_Number__c = this.weekNumber;
                sWD.Weekly_Dashboard_Type__c = 'Store';
                sWD.Store_Type__c = st.Store_Type__c;
                sWD.Region__c = st.Region__c;
                sWD.Store__c = st.Id;
                sWD.WeeklyDashboard_External_ID__c = this.weekNumber + '-Store-' + st.Id;
                sWD.Order__c = 2;
                sWD.All_Order__c = 1;
            }
            else
            {
                sWD = this.storeWD.get(st.Id);
            }
            
            for(Daily_Sales__c ds : this.dsList)
            {
                if(ds.Store__c == st.Id)
                {
                    this.sumSales(sWD, ds);
                    this.sumSales(tWD, ds);
                    this.sumSales(rWD, ds);
                    this.sumSales(allWD, ds);
                    
                    if(ds.Date2__c == this.weekEndDate)
                    {
                        sWD.MTD_Sales_Last_Year__c = ds.Actual_Sales_Last_Year_MTD_Net__c;
                        sWD.MTD_Sales_This_Year__c = ds.Actual_Sales_This_Year_MTD_Net__c;
                        sWD.MTD_Traffic_Last_Year__c = ds.Traffic_Last_Year_MTD__c;
                        sWD.MTD_Traffic_This_Year__c = ds.Traffic_This_Year_MTD__c;
                        
                        this.sumMTDSales(tWD, ds);
                        this.sumMTDSales(rWD, ds);
                        this.sumMTDSales(allWD,ds);
                    }
                }
            }
            
            for(Target__c t : targetList)
            {
                if(t.Store__c == st.Id)
                {
                    this.sumTargets(sWD, t);
                    this.sumTargets(tWD, t);
                    this.sumTargets(rWD, t);
                    this.sumTargets(allWD, t);
                }
            }
            
            this.totalWD.put(st.Store_Type__c,tWD);
            this.regionWD.put(storeTypeRegion,rWD);
            this.storeWD.put(st.Id,sWD);
        }
    }
    
    global void finish(Database.BatchableContext BC)
    {
        List<Weekly_Dashboard__c> wdList = new List<Weekly_Dashboard__c>();
        
        if(this.totalWD.size() > 0){
            //wdList.addAll(this.totalWD.values());
        }
        
        if(this.storeWD.size() > 0){
            wdList.addAll(this.storeWD.values());
        }
        
        if(this.regionWD.size() > 0){
            //wdList.addAll(this.regionWD.values());
        }
        
        if(this.allWD != null){
            //wdList.add(this.allWD);
        }
        
        if(wdList.size() > 0){
            upsert wdList WeeklyDashboard_External_ID__c;
        }
        DailySalesMTD rdub = new DailySalesMTD();
		rdub.runDate = dat;
		Database.executeBatch(rdub,20);
		
       RawDateToBillingBatch rbatch = new RawDateToBillingBatch();
       rbatch.RunDate = dat;
       Database.executeBatch(rbatch);
    }
    
    private void sumSales(Weekly_Dashboard__c wd, Daily_Sales__c ds)
    {
        if(ds.Sales_ACC_Net__c == null)
        {
            ds.Sales_ACC_Net__c = 0;    
        }
        
        if(ds.Sales_APP_Net__c == null)
        {
            ds.Sales_APP_Net__c = 0;
        }
        
        if(ds.Sales_FW_Net__c == null)
        {
            ds.Sales_FW_Net__c = 0;
        }
        
        if(ds.Traffic_This_Year__c == null)
        {
            ds.Traffic_This_Year__c = 0;
        }
        
        if(ds.Original_Retail_Price_Amount__c == null)
        {
            ds.Original_Retail_Price_Amount__c = 0;
        }
        
        if(ds.No_of_Tx_This_Year__c == null)
        {
            ds.No_of_Tx_This_Year__c = 0;
        }
        
        /////////
        
        if(wd.ACC_Sales__c == null)
        {
            wd.ACC_Sales__c = ds.Sales_ACC_Net__c;
        }
        else
        {
            wd.ACC_Sales__c += ds.Sales_ACC_Net__c;
        }
        
        if(wd.APP_Sales__c == null)
        {
            wd.APP_Sales__c = ds.Sales_APP_Net__c;
        }
        else
        {
            //System.debug('>>>>>>' + ds.Sales_APP_Net__c);
            wd.APP_Sales__c += ds.Sales_APP_Net__c;
        }
        
        if(wd.FTW_Sales__c == null)
        {
            wd.FTW_Sales__c = ds.Sales_FW_Net__c;
        }
        else
        {
            wd.FTW_Sales__c += ds.Sales_FW_Net__c;
        }
        
        if(ds.Actual_Sales_This_Year_Net__c != null)
        {
            if(wd.Net_Sales__c == null)
            {
                wd.Net_Sales__c = ds.Actual_Sales_This_Year_Net__c;
            }
            else
            {
                wd.Net_Sales__c += ds.Actual_Sales_This_Year_Net__c;
            }
        }
        
        if(wd.No_of_Tx_This_Year__c == null)
        {
            wd.No_of_Tx_This_Year__c = ds.No_of_Tx_This_Year__c;
        }
        else
        {
            wd.No_of_Tx_This_Year__c += ds.No_of_Tx_This_Year__c;
        }
        
        if(wd.Traffic_This_Year__c == null)
        {
            wd.Traffic_This_Year__c = ds.Traffic_This_Year__c;
        }
        else
        {
            wd.Traffic_This_Year__c += ds.Traffic_This_Year__c;
        }
        
        if(ds.Unit_TTL__c != null)
        {
            if(wd.Sales_Unit__c == null)
            {
                wd.Sales_Unit__c = ds.Unit_TTL__c;
            }
            else
            {
                wd.Sales_Unit__c += ds.Unit_TTL__c;
            }
        }
        
        if(wd.Original_Retail_Price__c == null)
        {
            wd.Original_Retail_Price__c = ds.Original_Retail_Price_Amount__c;
        }
        else
        {
            wd.Original_Retail_Price__c += ds.Original_Retail_Price_Amount__c;
        }
        
        if(ds.GP_TTL__c != null)
        {
            if(wd.GP_Amount__c == null)
            {
                wd.GP_Amount__c = ds.GP_TTL__c;
            }
            else
            {
                wd.GP_Amount__c += ds.GP_TTL__c;
            }
        }
    }
    
    private void sumMTDSales(Weekly_Dashboard__c wd, Daily_Sales__c ds)
    {
        if(ds.Actual_Sales_Last_Year_MTD_Net__c == null)
        {
            ds.Actual_Sales_Last_Year_MTD_Net__c = 0;
        }
        
        if(ds.Actual_Sales_This_Year_MTD_Net__c == null)
        {
            ds.Actual_Sales_This_Year_MTD_Net__c = 0;
        }
        
        if(ds.Traffic_Last_Year_MTD__c == null)
        {
            ds.Traffic_Last_Year_MTD__c = 0;
        }
        
        if(ds.Traffic_This_Year_MTD__c == null)
        {
            ds.Traffic_This_Year_MTD__c = 0;
        }
        
        if(wd.MTD_Sales_Last_Year__c == null)
        {
            wd.MTD_Sales_Last_Year__c = ds.Actual_Sales_Last_Year_MTD_Net__c;
        }
        else
        {
            wd.MTD_Sales_Last_Year__c += ds.Actual_Sales_Last_Year_MTD_Net__c;
        }
        
        if(wd.MTD_Sales_This_Year__c == null)
        {
            wd.MTD_Sales_This_Year__c = ds.Actual_Sales_This_Year_MTD_Net__c;
        }
        else
        {
            wd.MTD_Sales_This_Year__c += ds.Actual_Sales_This_Year_MTD_Net__c;
        }
        
        if(wd.MTD_Traffic_Last_Year__c == null)
        {
            wd.MTD_Traffic_Last_Year__c = ds.Traffic_Last_Year_MTD__c;
        }
        else
        {
            wd.MTD_Traffic_Last_Year__c += ds.Traffic_Last_Year_MTD__c;
        }
        
        if(wd.MTD_Traffic_This_Year__c == null)
        {
            wd.MTD_Traffic_This_Year__c = ds.Traffic_This_Year_MTD__c;
        }
        else
        {
            wd.MTD_Traffic_This_Year__c += ds.Traffic_This_Year_MTD__c;
        }
    }
    
    // 
    private void sumTargets(Weekly_Dashboard__c wd, Target__c t){
        if(t.ASP__c == null){
            t.ASP__c = 0;   
        }
        
        if(t.Conversion_Rate__c == null){
            t.Conversion_Rate__c = 0;
        }
        
        if(t.UPT__c == null){
            t.UPT__c = 0;
        }
        
        if(t.Sales_Volume__c == null){
            t.Sales_Volume__c = 0;
        }
        
        //////
        if(wd.Target_ASP_Number__c == null){
            wd.Target_ASP_Number__c = 1;
            wd.Target_ASP_Total__c = t.ASP__c;
        }else{
            wd.Target_ASP_Number__c += 1;
            wd.Target_ASP_Total__c += t.ASP__c;
        }
        
        if(wd.Target_CR_Number__c == null){
            wd.Target_CR_Number__c = 1;
            wd.Target_CR_Total__c = t.Conversion_Rate__c;
        } else {
            wd.Target_CR_Number__c += 1;
            wd.Target_CR_Total__c += t.Conversion_Rate__c;
        }
        
        if(wd.Target_UPT_Number__c == null)
        {
        	System.debug('>>>>>>>>>' + t.Date_Import__c);
            wd.Target_UPT_Number__c = 1;
            wd.Target_UPT_Total__c = t.UPT__c;
            System.debug('>>>>>number' + wd.Target_UPT_Total__c + '>>>>upt' + t.UPT__c);
            System.debug('>>>>total' + wd.Target_UPT_Number__c);
        }
        else
        {
        	System.debug('>>>>>>>>>houlaijinru' + t.Date_Import__c);
            wd.Target_UPT_Number__c += 1;
            wd.Target_UPT_Total__c += t.UPT__c;
            System.debug('>>>>>>>>>total' + wd.Target_UPT_Total__c + '>>>>upt' + t.UPT__c);
            System.debug('>>>>>>>number' + wd.Target_UPT_Number__c);
        }
        
        if(wd.Target_Sales__c == null)
        {
            wd.Target_Sales__c = t.Sales_Volume__c;
        }
        else
        {
            wd.Target_Sales__c += t.Sales_Volume__c;
        }
    }
    
    // 周信息
    private void assignWeekInfo(Date d)
    {
        this.weekStartDate = d.toStartOfWeek().addDays(1);
        this.weekEndDate = d.toStartOfWeek().addDays(7);
        if(Date.newInstance(d.year(), 1, 1).toStartOfWeek().daysBetween(d.toStartOfWeek()) / 7 <= 9)
        {
            this.weekNumber = d.year() + '0' + (Date.newInstance(d.year(), 1, 1).toStartOfWeek().addDays(1).daysBetween(d.toStartOfWeek().addDays(1)) / 7);
        }
        else
        {
            this.weekNumber = d.year() + '' + (Date.newInstance(d.year(), 1, 1).toStartOfWeek().addDays(1).daysBetween(d.toStartOfWeek().addDays(1)) / 7);
        }
    }
}