global class DailySalesMTD implements Database.Batchable<sObject>
{
	global Date runDate;
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		
		
		return Database.getQueryLocator([Select Id From Store__c]);
	}
	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		Map<String,Daily_Sales__c> Map_StorAndDai= new Map<String,Daily_Sales__c>();
		Set<ID> Set_StorID  = new Set<ID>();
		for(sObject sObj : scope)
		 {
		 	Store__c St = (Store__c)sObj;
		 	Set_StorID.add(St.Id);
		 } 
		 for(Daily_Sales__c Da:[Select d.vs_LY_MTD__c, d.vs_LY_MTD_Net__c, d.VS_LY_Traffic_MTD__c, 
		 d.VS_LY_No_of_Tx_MTD__c, d.Unit_TTL__c, d.Unit_FW__c, d.Unit_APP__c, d.Unit_ACC__c, 
		 d.UPT_vs_Full_Month_KPI_Target__c, d.UPT_target_MTD__c, d.UPT__c, d.Traffic_This_Year__c, 
		 d.Traffic_This_Year_MTD__c, d.Traffic_Last_Year__c, d.Traffic_Last_Year_MTD__c, d.Target__c, 
		 d.Target_MTD__c, d.SystemModstamp, d.Store__c, d.Store_Type__c, d.Sales_vs_LY_Daily__c, 
		 d.Sales_vs_LY_Daily_Net__c, d.Sales_Target_This_Year_New__c, d.Sales_Target_Last_Year__c, 
		 d.Sales_FW__c, d.Sales_FW_Net__c, d.Sales_Amt_MTD__c, d.Sales_APP__c, d.Sales_APP_Net__c, 
		 d.Sales_ACC__c, d.Sales_ACC_Net__c, d.OwnerId, d.Original_Retail_Price_Amount__c, d.No_of_Tx_This_Year__c, 
		 d.No_of_Tx_This_Year_MTD__c, d.No_of_Tx_Last_Year__c, d.No_of_Tx_Last_Year_MTD__c, d.Name, 
		 d.Multi_doc__c, d.Multi__c, d.Monthly_Target__c, d.MTD_Traffic_growth__c, 
		 d.MTD_Target__c, d.MTD_Sales_Vs_MTD_Target__c, d.LastViewedDate, 
		 d.LastReferencedDate, d.LastModifiedDate, d.LastModifiedById, d.IsDeleted, d.Id, d.GP_TTL__c, 
		 d.GP_TTL_Rate__c, d.GP_FW__c, d.GP_FW_Rate__c, d.GP_APP__c, d.GP_APP_Rate__c, 
		 d.GP_ACC__c, d.GP_ACC_Rate__c, d.For_Export__c, d.For_Export_City__c, d.FTW_MTD__c, d.FTW_MTD_M__c, 
		 d.Division_TTL__c, d.Division_TTL_Net__c, d.Division_FW__c, d.Division_FW_Net__c, d.Division_APP__c, 
		 d.Division_APP_Net__c, d.Division_ACC__c, d.Division_ACC_Net__c, d.Date2__c, d.Daily_Sales_Type__c, 
		 d.Daily_Sales_External_ID__c, d.CreatedDate, d.CreatedById, d.Conversion_vs_Full_Month_KPI_Target__c, 
		 d.Conversion_target_MTD__c,  d.Conv_Rate__c, d.Comp_growth__c, d.Billing__c, 
		 d.Actual_Sales_Vs_Target__c, d.Actual_Sales_This_Year__c, d.Actual_Sales_This_Year_Net__c, d.Actual_Sales_This_Year_MTD__c, 
		 d.Actual_Sales_This_Year_MTD_Net__c, d.Actual_Sales_Net_Vs_Target__c, d.Actual_Sales_Last_Year__c, 
		 d.Actual_Sales_Last_Year_Net__c, d.Actual_Sales_Last_Year_MTD__c, d.Actual_Sales_Last_Year_MTD_Net__c, 
		 d.Achieve_Vs_Monthly_Target__c, d.ATV__c, d.ATV_Net__c,  d.ASP_vs_Full_Month_KPI_Target__c,d.Target__r.UPT__c,d.Target__r.ASP__c,d.Target__r.Conversion_Rate__c,
		 d.ASP_target_MTD__c, d.APP_MTD__c, d.APP_MTD_M__c, d.ACC_MTD__c, d.ACC_MTD_M__c From Daily_Sales__c d where d.Store__c IN:Set_StorID
		 AND d.Date2__c >=:runDate.toStartOfMonth() AND d.Date2__c <=: runDate])
		 {
		 	
		 		Daily_Sales__c Dai = new Daily_Sales__c();
		 		//Dai.Sales_Amt_MTD__c = 0;
				Dai.Target_MTD__c = 0;
				Dai.FTW_MTD__c = 0;
				Dai.APP_MTD__c = 0;
				Dai.ACC_MTD__c = 0;
				//Dai.FTW_MTD_M__c = 0;
				//Dai.APP_MTD_M__c = 0;
				//Dai.ACC_MTD_M__c = 0;

				System.debug('#####运行几次');
				Dai.GP_Amount_MTD__c = 0;
				Dai.Conversion_vs_Full_Month_KPI_Target__c = 0;
				Dai.ASP_vs_Full_Month_KPI_Target__c = 0;
				Dai.UPT_vs_Full_Month_KPI_Target__c = 0;
				Dai.Conversion_target_MTD__c = 0;
				Dai.ASP_target_MTD__c = 0;
				Dai.UPT_target_MTD__c = 0;
				Dai.Sales_Unit_MTD__c = 0;
				Dai.Original_Retail_Price_Amount_MTD__c = 0;
		 		if(Map_StorAndDai.containsKey(Da.Store__c))
		 		{
		 			System.debug('#########赋值一次');
		 			Dai = Map_StorAndDai.get(Da.Store__c);
		 		}
		 		//Dai.Sales_Amt_MTD__c = 0;
				//Dai.Target_MTD__c += 0;
				if(Da.Sales_FW_Net__c != null)
				{
					Dai.FTW_MTD__c += Da.Sales_FW_Net__c;
				}
				if(Da.Sales_APP_Net__c != null)
				{
					Dai.APP_MTD__c += Da.Sales_APP_Net__c;
				}
				
				if(Da.Sales_ACC_Net__c != null)
				{
					Dai.ACC_MTD__c += Da.Sales_ACC_Net__c;
				}
				//Dai.FTW_MTD_M__c = 0;
				//Dai.APP_MTD_M__c = 0;
				//Dai.ACC_MTD_M__c = 0;
				
				//去年比例Dai.MTD_NS_COMP_MTD__c = 0;
				//去年Traffic 的比例Dai.MTD_Traffic_COMP_MTD__c = 0;
				//Original_Retail_Price_Amount__c Dai.MD_MTD__c = 0;!!!!!!!!
				if(Da.Original_Retail_Price_Amount__c != null)
				{
					Dai.Original_Retail_Price_Amount_MTD__c += Da.Original_Retail_Price_Amount__c;
				}
				//GP壁纸Dai.GP_MTD__c = 0;
				if(Da.GP_TTL__c != null)
				{
					Dai.GP_Amount_MTD__c += Da.GP_TTL__c;
				}
				//比例Dai.Conversion_MTD__c = Traffic_This_Year_MTD__c;
				if(Da.Unit_TTL__c != null) 
				{
					Dai.Sales_Unit_MTD__c += Da.Unit_TTL__c;
				}
				if(Da.Date2__c == runDate)	
				{
					System.debug('#####唯一值'+Da.Daily_Sales_External_ID__c);
					Dai.Daily_Sales_External_ID__c = Da.Daily_Sales_External_ID__c;
				}
				
				//Dai.ASP_MTD__c = 0;
				//Dai.UPT_MTD__c = 0;No_of_Tx_This_Year_MTD__c
				//Dai.ATV_MTD__c = 0;
				if(Da.Target__r.UPT__c != null)
				{
					Dai.ASP_target_MTD__c += Da.Target__r.UPT__c;
				}
				if(Da.Target__r.ASP__c != null)
				{
					Dai.UPT_target_MTD__c += Da.Target__r.UPT__c;
				}
				if(Da.Target__r.Conversion_Rate__c != null)
				{
					Dai.Conversion_target_MTD__c += Da.Target__r.Conversion_Rate__c;
				}
				/*
				Dai.Conversion_vs_Full_Month_KPI_Target__c = 0;
				Dai.ASP_vs_Full_Month_KPI_Target__c = 0;
				Dai.UPT_vs_Full_Month_KPI_Target__c = 0;
				
				Dai.Conversion_target_MTD__c = 0;
				Dai.ASP_target_MTD__c = 0;
				Dai.UPT_target_MTD__c = 0;
				*/
				
				Map_StorAndDai.put(Da.Store__c,Dai);
		 }
		 list<Daily_Sales__c> list_Daily_Sales = new list<Daily_Sales__c>();
		 for(String StorId:Map_StorAndDai.KeySet())
		 {
		 	if(Map_StorAndDai.get(StorId).Daily_Sales_External_ID__c != null)
		 	list_Daily_Sales.add(Map_StorAndDai.get(StorId));
		 }
		 if(list_Daily_Sales.size() > 0)
		 {
		 	upsert list_Daily_Sales Daily_Sales_External_ID__c;
		 }
	}
	global void finish(Database.BatchableContext BC)
	{
		 
	}
}