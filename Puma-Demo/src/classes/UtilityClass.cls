/**
 * 作者 : Sunny
 * 描述：工具类
 * 1.根据日期获取是否周末
 * 2.根据日期获取日期所在周为本年第几周
 **/
public class UtilityClass 
{
	public static String getWeekOfYear(Date dat)
	{
        //输入日期 -1 ，为防止周日的情况
        dat = dat.addDays(-1);
        //输入日期的周一，来计算是本年第几周
        dat = dat.toStartOfWeek().addDays(1);
        Integer i = dat.dayOfYear();
        double dbl = double.valueOf(i)/7;
        Integer j = Integer.valueOf(dbl);
        return (dbl - j)>0?String.valueOf(j+1):String.valueOf(j);
    }
    public static String getWeek(Date dat)
    {
    	Integer iWeek = dat.toStartOfWeek().daysBetween(dat);
    	return iWeek==0?'7':String.valueOf(iWeek);
    }
}