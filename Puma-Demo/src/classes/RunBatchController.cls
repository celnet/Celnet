public with sharing class RunBatchController 
{
	// 取DailySales的日期字段Date2__c
	public Daily_Sales__c d{get;set;}
	public Daily_Sales__c DExport{get;set;}
	public RunBatchController()
	{
		d = new Daily_Sales__c();
		DExport = new Daily_Sales__c();
		d.Date2__c = Date.today();
		DExport.Date2__c = date.today();
	}
	public void Run()
	{
		if(d.Date2__c == null)
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '请先选择日期');         
	   		ApexPages.addMessage(msg);
		}
		else
		{
			DailySalesMonthlyNewBatch dsmnb = new DailySalesMonthlyNewBatch(d.Date2__c.toStartOfMonth(),d.Date2__c);
			Database.executeBatch(dsmnb,1);
			// DailySalesMonthlyBatch dsb = new DailySalesMonthlyBatch(d.Date2__c);
			// Database.executeBatch(dsb,1);
		}
		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, '已提交系统进行计算，请稍等一段时间进行查看。');         
	   	ApexPages.addMessage(msg);
	}
	public void ExprtE()
	{
		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, '正在导出数据请稍等。');         
	   	ApexPages.addMessage(msg);
	}
}