/* Author:Tobe
 * Date:2014-4-30
 * Description:当Escalation解决以后，记录不允许被修改，当Open变为Closed的时候，自动给Close Date赋值
 */
trigger Escalation_Update_Handle on Escalation__c (before update) 
{
	for(Escalation__c newEscalation : trigger.new)
	{
		Escalation__c oldEscalation = trigger.oldMap.get(newEscalation.Id);
		if(oldEscalation.Status__c =='Closed')
		{
			newEscalation.addError('Records can not be modified!');
		}
		else if(oldEscalation.Status__c =='Open' && newEscalation.Status__c =='Closed')
		{
			newEscalation.Close_Date__c = Date.today();
		}
	}
}