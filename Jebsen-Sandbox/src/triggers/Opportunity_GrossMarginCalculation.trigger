/**
 * Author: Steven
 * Date: 2014-8-31
 * Description: 更新产品对应的Forecast记录上的GP字段
 * 计算GP上的字段
 */
trigger Opportunity_GrossMarginCalculation on Gross_Margin_Calculation__c (before insert,before update,after insert, after update,after Delete) {
    // 更新Forecast
    if((trigger.isAfter && (trigger.isInsert || trigger.isUpdate))){
        map<String, Decimal> gpMap = new map<String, Decimal>();
        map<String, Id> gpIdMap = new map<String, Id>();
        map<String,Date> gpDateMap=new map<String,Date>();//和创建日期的对应关系
        set<String> productArticles = new Set<String>();
        System.debug(trigger.new+'trigger.new****************');
        for(Gross_Margin_Calculation__c gmc : trigger.new){
            productArticles.add(gmc.Product_Article__c);
            gpMap.put(gmc.Product_Article__c, gmc.GP__c);
            gpIdMap.put(gmc.Product_Article__c, gmc.Opportunity__c);
            gpDateMap.put(gmc.Product_Article__c,gmc.Date__c);
        }
        
        System.debug(gpMap.size());
        
        list<Forecast__c> updateForecasts = new list<Forecast__c>();
        
        for(Forecast__c f : [Select Id, Year__c,GP__c,CreatedDate,Product_Article__c, Opportunity__c From Forecast__c Where Product_Article__c IN: productArticles]){  
            if(gpMap.get(f.Product_Article__c) != null && gpMap.get(f.Product_Article__c) != f.GP__c && gpIdMap.get(f.Product_Article__c) != null && gpIdMap.get(f.Product_Article__c) == f.Opportunity__c
                  &&Integer.valueOf(f.Year__c)>=gpDateMap.get(f.Product_Article__c).year()){
               f.GP__c = gpMap.get(f.Product_Article__c);
                updateForecasts.add(f);
            }
        }
        
        if(updateForecasts.size() > 0){
            update updateForecasts;
        }
    }
    if(trigger.isAfter&&trigger.isDelete)
    {
         map<String, Decimal> gpMap = new map<String, Decimal>();
        map<String, Id> gpIdMap = new map<String, Id>();
        map<String,Date> gpDateMap=new map<String,Date>();//和创建日期的对应关系
        set<String> productArticles = new Set<String>();
        for(Gross_Margin_Calculation__c gmc : trigger.old){
            productArticles.add(gmc.Product_Article__c);
            gpMap.put(gmc.Product_Article__c, gmc.GP__c);
            gpIdMap.put(gmc.Product_Article__c, gmc.Opportunity__c);
            gpDateMap.put(gmc.Product_Article__c,gmc.Date__c);
        }
        
        System.debug(gpMap.size());
        
        list<Forecast__c> updateForecasts = new list<Forecast__c>();
        
        for(Forecast__c f : [Select Id, Year__c,GP__c,CreatedDate,Product_Article__c, Opportunity__c From Forecast__c Where Product_Article__c IN: productArticles]){  
             if(gpMap.get(f.Product_Article__c) != null && gpMap.get(f.Product_Article__c) == f.GP__c && gpIdMap.get(f.Product_Article__c) != null && gpIdMap.get(f.Product_Article__c) == f.Opportunity__c
                  &&Integer.valueOf(f.Year__c)>=gpDateMap.get(f.Product_Article__c).year())
            {
            	System.debug('*&&&&&&&&&&');
                f.GP__c =0;
                updateForecasts.add(f); 
            }
        }
        
        if(updateForecasts.size() > 0){
            update updateForecasts;
        }
    }
    //计算GP上的字段
    if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate)){
        Map<String , GrossMarginCalculation_Parameter__c> map_FastenerSea = new Map<String , GrossMarginCalculation_Parameter__c>();
        for(GrossMarginCalculation_Parameter__c p : GrossMarginCalculation_Parameter__c.getAll().values()){
            if(p.Parameter_Type__c == 'Fastener(Sea)'){
                map_FastenerSea.put(p.Name , p);
            }
        }
        for(Gross_Margin_Calculation__c gmc : trigger.new){
            if(gmc.Product_Type__c == 'Fastener'){
                if(gmc.Transport__c == 'Nuts Sea Transport'){
                    //海运赋值参数
                    gmc.Inbound_Freight__c = map_FastenerSea.get('Inbound Freight').Parameter__c;
                    gmc.Operation_charge_per_month__c = map_FastenerSea.get('Operation charge per month').Parameter__c;
                    gmc.Weight_per_PCS_KG_Num__c = 15 / gmc.Quantity_Carton__c;
                    //海运计算
                    //Inbound logistic
                    gmc.Inbound_Logistic_Cost__c = gmc.Inbound_Freight__c/38/60/gmc.Quantity_Carton__c;
                    //Outbound logistic
                    gmc.Outbound_Logistic_cost__c = gmc.Destination_Value__c * gmc.Weight_per_PCS_KG_Num__c ;
                    //Inbound Insurance
                    gmc.Inbound_Insurance_num__c = gmc.Purchase__c * 0.0004;
                    //Customs Duty
                    gmc.Customs_Duty_Num__c = (gmc.Purchase__c + gmc.Inbound_Insurance_num__c + gmc.Inbound_Logistic_Cost__c) * gmc.Duty__c / 100;
                    //Logistic
                    gmc.Logistic_Num__c = gmc.Inbound_Logistic_Cost__c+gmc.Outbound_Logistic_cost__c+gmc.Inbound_Insurance_num__c+gmc.Customs_Duty_Num__c;
                    //Warehousing
                    gmc.Inventory__c = gmc.Operation_charge_per_month__c/40/60/gmc.Quantity_Carton__c;
                    //Total Inventory Cost
                    gmc.Total_Inventory_Cost__c = gmc.Purchase__c + gmc.Logistic_Num__c + gmc.Inventory__c;
                    //Price without Tax
                    gmc.Price_with_Tax__c = gmc.Total_Inventory_Cost__c / (1 - gmc.GP__c/100);
                    //Output VAT
                    gmc.Tax__c = gmc.Total_Inventory_Cost__c / (1 - gmc.GP__c/100)*0.17;
                }else if(gmc.Transport__c == 'Nuts Air Transport'){
                    //空运计算
                    //Inbound Insurance
                    gmc.Inbound_Insurance_num__c = gmc.Purchase__c * 0.0004;
                    //Customs Duty
                    System.debug('AAAAAA'+gmc.Purchase__c+'  '+gmc.Inbound_Insurance_num__c+'  '+gmc.Inbound_Logistic_Cost__c+'  '+gmc.Duty__c);
                    gmc.Customs_Duty_Num__c = (gmc.Purchase__c + gmc.Inbound_Insurance_num__c + gmc.Inbound_Logistic_Cost__c) * gmc.Duty__c / 100;
                    //Logistic
                    gmc.Logistic_Num__c = gmc.Inbound_Logistic_Cost__c+gmc.Outbound_Logistic_cost__c+gmc.Inbound_Insurance_num__c+gmc.Customs_Duty_Num__c;
                    //Total Inventory Cost
                    gmc.Total_Inventory_Cost__c = gmc.Purchase__c + gmc.Logistic_Num__c + gmc.Inventory__c;
                    //Price without Tax
                    gmc.Price_with_Tax__c = gmc.Total_Inventory_Cost__c / (1 - gmc.GP__c/100);
                    //Output VAT
                    gmc.Tax__c = gmc.Total_Inventory_Cost__c / (1 - gmc.GP__c/100)*0.17;
                }
            }else if(gmc.Product_Type__c == 'Feeder or tooling'){
                //Feeder计算
                //Inbound Insurance
                gmc.Inbound_Insurance_num__c = gmc.Purchase__c * 0.0004;
                //Customs Duty
                gmc.Customs_Duty_Num__c = (gmc.Purchase__c + gmc.Inbound_Insurance_num__c + gmc.Inbound_Logistic_Cost__c) * 0.08;
                //Logistic
                gmc.Logistic_Num__c = gmc.Inbound_Logistic_Cost__c+gmc.Outbound_Logistic_cost__c+gmc.Inbound_Insurance_num__c+gmc.Customs_Duty_Num__c;
                //Total Inventory Cost
                gmc.Total_Inventory_Cost__c = gmc.Purchase__c + gmc.Logistic_Num__c + gmc.Inventory__c;
                //Price without Tax
                gmc.Price_with_Tax__c = gmc.Total_Inventory_Cost__c / (1 - gmc.GP__c/100);
                //Output VAT
                gmc.Tax__c = gmc.Total_Inventory_Cost__c / (1 - gmc.GP__c/100)*0.17;
            }
        }
    }
}