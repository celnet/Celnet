/*
 * 作者：Ziyue
 * 时间：2013-10-25
 * 功能：将
*/
trigger Escalation_AutoUser on Escalation__c (before insert) 
{
	set<ID> oppIds = new set<ID>();
	for(Escalation__c es:trigger.new)
	{
		oppIds.add(es.Opportunity__c);
	}
	map<ID,Opportunity> map_opp = new map<ID,Opportunity>([select id,OwnerId,Owner.ManagerId,Account.OwnerId from Opportunity where id in:oppIds]);
	for(Escalation__c es:trigger.new)
	{
		es.Account_Owner__c = map_opp.get(es.Opportunity__c).Account.OwnerId;
		if(map_opp.get(es.Opportunity__c).Owner.ManagerId != null)
		{
			es.Owner_Manager__c = map_opp.get(es.Opportunity__c).Owner.ManagerId;
		}
	}
}