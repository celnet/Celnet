/*
 * 作者：Ziyue
 * 时间：2013-10-14
 * 功能：某一类型的附件上传后，将对应业务机会上该类型的标识字段打勾
*/
trigger Document_upload_AutoUpdOpportunity on Document_upload__c (after delete, after insert) 
{
    //业务机会ID
    set<ID> set_oppIds = new set<ID>();
    if(trigger.isInsert)
    {
        for(Document_upload__c doc : trigger.new)
        {
            if(doc.Opportunity__c !=  null)
            {
                set_oppIds.add(doc.Opportunity__c);
            }
        }
    }
    else
    {
        for(Document_upload__c doc : trigger.old)
        {
            if(doc.Opportunity__c !=  null)
            {
                set_oppIds.add(doc.Opportunity__c);
            }
        }
    }
    if(set_oppIds.size() == 0)
    {
        return;
    }
    list<Opportunity> list_opp =[Select Samples_demand_plan_has_been_uploaded__c, 
                                Samples_Purchase_Order_has_been_uploaded__c, 
                                Quotation_to_customer_has_been_uploaded__c, 
                                Quotation_to_Jebsen_has_been_uploaded__c,RFQ_has_been_uploaded__c, 
                                Questionnaire_has_been_uploaded__c, PO_to_Profil_has_been_uploaded__c, 
                                Overall_solution_has_been_uploaded__c, Frame_contract_has_been_uploaded__c, 
                                Automated_solution_has_been_uploaded__c, 
                                (Select Document_Type__c, DocumentID__c, Document_Name__c, Opportunity__c 
                                From Document_Upload__r) From Opportunity where id in:set_oppIds];
    if(list_opp != null && list_opp.size() > 0)
    {
        for(Opportunity opp : list_opp)
        {
            opp.Samples_demand_plan_has_been_uploaded__c = false;
            opp.Samples_Purchase_Order_has_been_uploaded__c = false;
            opp.Quotation_to_customer_has_been_uploaded__c = false;
            opp.Quotation_to_Jebsen_has_been_uploaded__c = false;
            opp.Questionnaire_has_been_uploaded__c = false;
            opp.PO_to_Profil_has_been_uploaded__c = false;
            opp.Overall_solution_has_been_uploaded__c = false;
            opp.Frame_contract_has_been_uploaded__c = false;
            opp.PPAP_Documents_has_been_uploaded__c = false;
            opp.Automated_solution_has_been_uploaded__c = false;
            opp.RFQ_has_been_uploaded__c = false;
            opp.Concept_Design_has_been_uploaded__c = false;
            opp.X2D_Drawings_has_been_uploaded__c = false;
            opp.Cost_Calculation_has_been_uploaded__c = false;
            if(opp.Document_Upload__r == null)
            {
                continue;
            }
            for(Document_upload__c doc : opp.Document_Upload__r)
            {
                if(doc.Document_Type__c == null)
                {
                    continue;
                }
                string typeAPI='';
                for(string str : doc.Document_Type__c.split(' '))
                {
                    if(typeAPI == '')
                    {
                        typeAPI = str;
                    }
                    else
                    {
                        typeAPI+=('_'+str);
                    }
                }
                if(typeAPI.startsWith('2'))
                    typeAPI = 'X'+typeAPI;
                typeAPI+='_has_been_uploaded__c';
                opp.put(typeAPI,true);
                /*
                if(doc.Document_Type__c == 'Automated solution')
                {
                    opp.Automated_solution_has_been_uploaded__c = true;
                }
                else if(doc.Document_Type__c == 'Frame contract')
                {
                    opp.Frame_contract_has_been_uploaded__c = true;
                }
                else if(doc.Document_Type__c == 'Overall solution')
                {
                    opp.Overall_solution_has_been_uploaded__c = true;
                }
                else if(doc.Document_Type__c == 'PO to Profil')
                {
                    opp.PO_to_Profil_has_been_uploaded__c = true;
                }
                else if(doc.Document_Type__c == 'Questionnaire')
                {
                    opp.Questionnaire_has_been_uploaded__c = true;
                }
                else if(doc.Document_Type__c == 'Quotation to customer')
                {
                    opp.Quotation_to_customer_has_been_uploaded__c = true;
                }
                else if(doc.Document_Type__c == 'Quotation to Jebsen')
                {
                    opp.Quotation_to_Jebsen_has_been_uploaded__c = true;
                }
                else if(doc.Document_Type__c == 'RFQ')
                {
                    opp.RFQ_has_been_uploaded__c = true;
                }
                else if(doc.Document_Name__c == 'Samples demand plan')
                {
                    opp.Samples_demand_plan_has_been_upload__c = true;
                }
                else if(doc.Document_Name__c == 'Samples Purchase Order')
                {
                    opp.Samples_Purchase_Order_has_been_uploaded__c = true;
                }*/
            }
        }
        update list_opp;
    }                         
}