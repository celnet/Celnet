/*Author:Leo
 *Date:2014-4-10
 *function:当业务机会下面一条escalation被删除的时候，需要将最新的escalation type赋值到所属业务机会的recently escalation type
 */
trigger Escalation_Auto_SetOpportunity on Escalation__c (after delete) {
    //被删除的Escalation__c所属业务机会的id集合
    Set<Id> set_Opportunity_Id = new Set<Id>();
    Map<Id,List<Escalation__c>> map_newest_escalation = new Map<Id,List<Escalation__c>>();
    for(Escalation__c esc : trigger.old)
    {
        set_Opportunity_Id.add(esc.Opportunity__c);
    }
    //获取被删除的Escalation__c所属业务机会
    List<Opportunity> list_toupdate_opp = [Select id, Recently_Escalation_Type__c from Opportunity where id = :set_Opportunity_Id];
    //根据拿到的业务机会id集合得到所有的Escalation__c
    //List<Escalation__c> list_esc = [Select Opportunity__c, Id, Escalation_Type__c, CreatedDate From Escalation__c where id in :set_deletedEscalation_Id];
    //获取该业务机会下最新的类型escalation
    List<Escalation__c> list_newest_escalation = [Select Opportunity__c, Id, Escalation_Type__c, CreatedDate 
                                                  From Escalation__c 
                                                  where Opportunity__c in :set_Opportunity_Id
                                                  order by CreatedDate desc limit 1];
    //若Escalation_Have不为空，将查询的结果封装到map
    if(list_newest_escalation.size() > 0)
    {
        for(Escalation__c esc : list_newest_escalation)
        {
            List<Escalation__c> tempList = new List<Escalation__c>();
            if(map_newest_escalation.containsKey(esc.Opportunity__c))
            {
                //如果map中有该业务机会的id,则把本次遍历的Escalation__c插入到所属list
                tempList = map_newest_escalation.get(esc.Opportunity__c);
                tempList.add(esc);
            }
            else
            {
                //如果map中没有该业务机会的id,则把本次遍历的Escalation__c插入到list，然后把id和list插入到map
                tempList.add(esc);
                map_newest_escalation.put(esc.Opportunity__c,tempList);
            }
        }
    }
    //如果业务机会下删除一条escalation后仍然有escalation记录,且最新的记录的type不为 Escalation Solve,
        //需要将这条记录的type插入到对应的业务机会下的Recently_Escalation_Type__c
        //如果该业务机会下一条记录被删除后，没有escalation记录，或者最新的escalation记录的type为Escalation Solve,
            //需要将该业务机会的Recently_Escalation_Type__c设置为空
    for(Opportunity opp : list_toupdate_opp)
    {
        if(map_newest_escalation.containsKey(opp.Id))
        {
            //如果业务机会下删除一条escalation后仍然有escalation记录
            Escalation__c esc = map_newest_escalation.get(opp.Id)[0];
            if(esc.Escalation_Type__c != 'Escalation Solve')
            {
                //若最新的记录的type不为 Escalation Solve
                opp.Recently_Escalation_Type__c = esc.Escalation_Type__c;
            }
            else
                {
                    opp.Recently_Escalation_Type__c = '';
                }
        }
        else
        {
            opp.Recently_Escalation_Type__c = '';
        }
        
    }
    update list_toupdate_opp;
}