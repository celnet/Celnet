/*Author:Leo
 *Date:2014-3-31
 *Function:当Opp上的Recently Escalation Type字段被修改时，
 *               自动生成一条Escalation列表记录。
 */
trigger Opportunity_AutoCreat_Escalation on Opportunity (after update) {
    if(trigger.isUpdate)
    {
        //要更新新的Escalation__c集合
        List<Escalation__c> list_escalation = new List<Escalation__c>();
        //更新的业务机会的id集合
        Set<Id> set_Opp_Id = new Set<Id>();
        //要更新的Escalation__c所属opp的集合
        Map<Id,List<Escalation__c>> map_escalation = new Map<Id,List<Escalation__c>>();
        //获取更新的业务机会的id集合
        for(Opportunity opp: trigger.new)
        {
            if(opp.Recently_Escalation_Type__c != trigger.oldMap.get(opp.id).Recently_Escalation_Type__c)
            set_Opp_Id.add(opp.Id);
        }
        if(set_Opp_Id.size()==0)
        {
            return;
        }
        //获取所有被更新的业务机会下的escalation
        List<Escalation__c> Escalation_Have = [Select Escalation_Type__c, Opportunity__c, CreatedDate 
                                               From Escalation__c 
                                               where Opportunity__c in :set_Opp_Id
                                               order by CreatedDate desc limit 1];
        //若Escalation_Have不为空，将查询的结果封装到map
        if(Escalation_Have.size() > 0)
        {
            for(Escalation__c esc : Escalation_Have)
            {
                List<Escalation__c> tempList = new List<Escalation__c>();
                if(map_escalation.containsKey(esc.Opportunity__c))
                {
                    //如果map中有该业务机会的id,则把本次遍历的Escalation__c插入到所属list
                    tempList = map_escalation.get(esc.Opportunity__c);
                    tempList.add(esc);
                }
                else
                {
                    //如果map中没有该业务机会的id,则把本次遍历的Escalation__c插入到list，然后把id和list插入到map
                    tempList.add(esc);
                    map_escalation.put(esc.Opportunity__c,tempList);
                }
            }
        }
        for(Opportunity opp : trigger.new)
        {
            
            if(map_escalation.containsKey(opp.Id))
            {
                //处理业务机会下有escalation的情况 
                boolean isupdate = true;
                //如果map中有这个业务机会
                //根据new集合的id获取到其下的escalation集合
                List<Escalation__c> list_temp_Escalation = map_escalation.get(opp.Id);
                //遍历该集合
                for(Escalation__c esc : list_temp_Escalation)
                {
                    system.debug('******Escalation__c***************' + esc);
                    if(esc.Escalation_Type__c == 'Escalation Solve' && opp.Recently_Escalation_Type__c==null)
                    {
                        system.debug('******进入了判定***************');
                        //如果该业务机会下最新的escalation记录为escalation solve，且Recently_Escalation_Type__c为空则不插入新的escalation
                        isupdate = false;
                    }
                    if(esc.Escalation_Type__c == opp.Recently_Escalation_Type__c)
                    {
                        //如果该业务机会下有最新的escalation记录，并且type不为Escalation Solve，
                        //且该条记录的type与更新后的opp的Recently_Escalation_Type__c一样，则不插入记录
                        isupdate = false;
                    }
                }
                system.debug('**************isupdate***********' + isupdate);
                if(!isupdate)
                {
                    continue;
                }
            }
            else
                if(opp.Recently_Escalation_Type__c==null || opp.Recently_Escalation_Type__c=='')
                {
                    //如果map中没有这个业务机会，也就是说该业务机会下的escalation没有数据，如果此时opp的type为空
                    continue;
                }
            
            //当业务机会下的Recently_Escalation_Type__c字段由空变为有，或者由有变为空时
            Opportunity tempOpp = trigger.oldMap.get(opp.id);
            if( opp.Recently_Escalation_Type__c != tempOpp.Recently_Escalation_Type__c)
            {
                Escalation__c tempEscalation = new Escalation__c();
                if((opp.Recently_Escalation_Type__c==null || opp.Recently_Escalation_Type__c==''))
                {//如果由有值变为无值
                    tempEscalation.Escalation_Type__c ='Escalation Solve';
                    System.debug('*************' + tempEscalation.Escalation_Type__c);
                }
                else
                {
                    //如果由无值变为有值时
                    tempEscalation.Escalation_Type__c = opp.Recently_Escalation_Type__c;
                }
                tempEscalation.Opportunity__c = opp.Id;
                list_escalation.add(tempEscalation);
            }
            
        }
        if(list_escalation.size()>0)
        insert list_escalation;
    }
    
}