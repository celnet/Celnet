/**
 * Author: Steven
 * Date: 2014-8-31
 * Description: 插入或者更新的时候去GP对象找对应的产品记录，赋GP字段值
 *              更新Opportunity上的字段
 */
trigger Opportunity_Forecast on Forecast__c (before insert, before update, after insert, after update, after delete) {
	// 更新GP字段
	if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate)){
		set<String> productArticles = new set<String>();
		Set<String> OppIdSet=new Set<String>();
		for(Forecast__c f : trigger.new){
			productArticles.add(f.Product_Article__c);
			OppIdSet.add(f.Opportunity__c);
		}
		map<String, Id> gpIdMap = new map<String, Id>();
		
		// 查询GP对象
		Map<String, Decimal> gpMap = new Map<String, Decimal>();
		for(Gross_Margin_Calculation__c gp: [Select Id, GP__c, Product_Article__c, Opportunity__c,Date__c,CreatedDate From Gross_Margin_Calculation__c where Product_Article__c IN: productArticles and Opportunity__c In:OppIdSet Order by CreatedDate asc]){
			gpMap.put(gp.Product_Article__c+gp.Opportunity__c, gp.GP__c);
			//gpIdMap.put(gp.Product_Article__c, gp.Opportunity__c);
		}
		
		
		System.debug(gpMap+'gpMap&&&&&&&&&&');
		for(Forecast__c f : trigger.new){
			if(gpMap.get(f.Product_Article__c+f.Opportunity__c) != null && gpMap.get(f.Product_Article__c+f.Opportunity__c) != f.GP__c ){
				f.GP__c = gpMap.get(f.Product_Article__c+f.Opportunity__c);
				System.debug(gpMap.get(f.Product_Article__c)+'gpMap************************');
			}
			System.debug(f.GP__c+'gp***************');
		}
	}
	
	// 更新Opportunity上的GP字段
	if(trigger.isAfter){
		map<Id, Decimal> oppGpMap = new map<Id, Decimal>();
		map<Id, Integer> oppGpNumberMap = new map<Id, Integer>();
		
		list<Forecast__c> forecastList = new list<Forecast__c>();
		if(trigger.isInsert || trigger.isUpdate){
			forecastList = trigger.new;
		} else if(trigger.isDelete) {
			forecastList = trigger.old;
		}
		
		set<Id> oppIds = new set<Id>();
		for(Forecast__c f : forecastList){
			oppIds.add(f.Opportunity__c);
		}
		
		list<Forecast__c> allForecastList = [Select Id, GP__c, Opportunity__c From Forecast__c Where Opportunity__c IN: oppIds];
		
		for(Forecast__c f : allForecastList){
			if(oppGpMap.get(f.Opportunity__c) != null){
				oppGpMap.put(f.Opportunity__c, oppGpMap.get(f.Opportunity__c) + (f.GP__c == null?0:f.GP__c));
			} else {
				oppGpMap.put(f.Opportunity__c, (f.GP__c == null?0:f.GP__c));
			}
			
			if(oppGpNumberMap.get(f.Opportunity__c) != null){
				oppGpNumberMap.put(f.Opportunity__c, oppGpNumberMap.get(f.Opportunity__c) + 1);
			} else {
				oppGpNumberMap.put(f.Opportunity__c,1);
			}
		}
		
		list<Opportunity> updateOppList = new list<Opportunity>();
		//Sunny：汇总opp上的Annual Revenue
		Map<ID , Decimal> MAP_OppRevenue = new Map<ID , Decimal>();
		for(Forecast__c fc:[Select Id,	Q1__c,Q2__c,Q3__c,	Q4__c,Opportunity__c From Forecast__c Where Opportunity__c in: oppGpMap.keySet()]){
			Decimal d = 0 ;
			if(MAP_OppRevenue.containsKey(fc.Opportunity__c)){
				d = MAP_OppRevenue.get(fc.Opportunity__c);
			}
			d += (fc.Q1__c==null?0:fc.Q1__c);
			d += (fc.Q2__c==null?0:fc.Q2__c);
			d += (fc.Q3__c==null?0:fc.Q3__c);
			d += (fc.Q4__c==null?0:fc.Q4__c);
			MAP_OppRevenue.put(fc.Opportunity__c , d);
		}
		for(Id i : oppGpMap.keySet()){
			Opportunity opp = new Opportunity();
			if(i != null){
				opp.Id = i;
				opp.Gross_Profit__c = (oppGpMap.get(i) / oppGpNumberMap.get(i));
				opp.Annual_Revenue__c = MAP_OppRevenue.get(i);
				updateOppList.add(opp);
			}
		}
		
		if(updateOppList.size() > 0)
		update updateOppList;
	}
}