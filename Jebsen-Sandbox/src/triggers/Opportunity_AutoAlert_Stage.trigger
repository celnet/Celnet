/*Author；Leo
 *Date:2014-4-14
 *function:当人为去改动Stage，且Stage=Win或者Lose的时候，若Close Date大于或者小于Close Data
 			提示错误“Close Date should be today”。
 */
trigger Opportunity_AutoAlert_Stage on Opportunity (before update) {
	for(Opportunity newopp : trigger.new)
	{
		Opportunity oldopp = trigger.oldMap.get(newopp.Id);
		string tt = formatDate(date.today());
		system.debug('***************' + tt);
		if(newopp.StageName == oldopp.StageName)
		{
			continue;
		}
		if(newopp.StageName != 'Implementation(Win)' && newopp.StageName!='Lost Order')
		{
			continue;
		}
		if(formatDate(newopp.CloseDate) != formatDate(date.today()))
		{
			newopp.addError('Close Date should be TODAY');
		}
	}
	public string formatDate(Datetime dt)
    {
    	string result = '';
    	string year = string.valueOf((dt.year()));
    	string month = string.valueOf((dt.month()));
    	string day = string.valueof(dt.day());
    	result = year + '-' + month +  '-'+ day;
    	return result;
    }
}