/* Author:Tobe
 * Date:2014-4-29
 * Description:当Stage=win时自动将Competitor赋值到Competitor Name字段上
 */
trigger AutoFillOutCompetitorName on Opportunity (before update) 
{
	set<ID> set_OppId = new set<ID>();
	for(Opportunity opp : trigger.new)
    {
        if(opp.StageName != 'Implementation(Win)' || (opp.Decision_Competitor__c != null && opp.Decision_Competitor__c !=''))
        {
        	continue;
        }
        set_OppId.add(opp.id);
   	}
   	map<ID,string> map_Opp_CompetitorName = new map<ID,string>();
   	for(Opportunity opp :[Select 
   							id,
   							(Select CompetitorName From OpportunityCompetitors order by CompetitorName asc) 
   						  From Opportunity o])
   	{
		string competitorName='';
		for(OpportunityCompetitor  oc : opp.OpportunityCompetitors)
		{
			competitorName+=oc.CompetitorName+',';
		}
		if(competitorName != '')
		{
			competitorName = competitorName.removeEnd(',');
		}
		map_Opp_CompetitorName.put(opp.id,competitorName);
   	}
   	for(Opportunity opp : trigger.new)
    {
        if(opp.StageName != 'Implementation(Win)' || (opp.Decision_Competitor__c != null && opp.Decision_Competitor__c !=''))
        {
        	continue;
        }
        if(map_Opp_CompetitorName.containsKey(opp.Id))
        {
        	opp.Decision_Competitor__c = map_Opp_CompetitorName.get(opp.Id);
        }
   	}
}