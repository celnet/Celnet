/*
 * 作者：Ziyue
 * 时间：2013-10-14
 * 描述：当将阶段改为下一个阶段时，判断上一阶段对应必填字段是否都填写，如果没有则提示填写。
 * Edit By Tobe 2014-4-29 增加Decision部分验证规则
*/
trigger Opportunity_ChangeStage_Validate on Opportunity (before update) 
{
    Map<String, Schema.SobjectField> FsMap = Schema.SObjectType.Opportunity.fields.getMap();
    //Label:API
    map<string,string> LabeltoName = new map<string,string>();
    for (String s: FsMap.keySet()) 
    {
        Schema.SobjectField sObjField = FsMap.get(s);
        LabeltoName.put(sObjField.getDescribe().getLabel(),s);
    }
        
    //阶段：必填字段API集合
    map<string,set<string>> map_Requred = new map<string,set<string>>();
    //Achievement
    Schema.DescribeFieldResult Achievement = Opportunity.Achievement_Requred__c.getDescribe();
    set<string> set_Achievement = new set<string>();
    for(Schema.Picklistentry p : Achievement.getPicklistValues())
    {
        set_Achievement.add(p.getValue());
    }   
    map_Requred.put('Implementation(Win)',set_Achievement);
    
    //AOA
    Schema.DescribeFieldResult AOA = Opportunity.AOA_Requred__c.getDescribe();
    set<string> set_AOA = new set<string>();
    for(Schema.Picklistentry p : AOA.getPicklistValues())
    {
        set_AOA.add(p.getValue());
    }   
    map_Requred.put('Assessment of Alternatives',set_AOA);
    
    //Awareness of needs
    Schema.DescribeFieldResult AON = Opportunity.AON_Requred__c.getDescribe();
    set<string> set_AON = new set<string>();
    for(Schema.Picklistentry p : AON.getPicklistValues())
    {
        set_AON.add(p.getValue());
    }   
    map_Requred.put('Awareness of Needs',set_AON);
    
     //AOR
    Schema.DescribeFieldResult AOR = Opportunity.AOR_Requred__c.getDescribe();
    set<string> set_AOR = new set<string>();
    for(Schema.Picklistentry p : AOR.getPicklistValues())
    {
        set_AOR.add(p.getValue());
    }   
    map_Requred.put('Alleviation of Risk',set_AOR);
    
     //Decision
    Schema.DescribeFieldResult Decision = Opportunity.Decision_Requred__c.getDescribe();
    set<string> set_Decision = new set<string>();
    for(Schema.Picklistentry p : Decision.getPicklistValues())
    {
        set_Decision.add(p.getValue());
    }   
    map_Requred.put('Decision(ContractSigned/PaymentReceived)',set_Decision);
    
    //阶段：必填字段API集合
    //map<string,set<string>> map_Requred 
    set<ID> AOA_oppIds = new set<ID>();
    set<ID> AOR_oppIds = new set<ID>();
    for(Opportunity opp : trigger.new)
    {
        Opportunity old = trigger.oldMap.get(opp.id);
        /*Edit By Tobe 2014-4-29*/
        if(opp.StageName == 'Lost Order')
        {
        	
            if(opp.Decision_Result__c == null)
            {
                opp.Decision_Result__c.addError('Please fill out the result.');
            }
            if(opp.Decision_Lost_Type__c == null)
            {
            	opp.Decision_Lost_Type__c.addError('Please fill out the lost type.');
            }
            if(opp.Main_reason_for_winning_or_losing__c == null)
            {
                opp.Main_reason_for_winning_or_losing__c.addError('Please fill out the main reason.');
            }
            if(opp.Main_reason_for_winning_or_losing__c =='Other' && 
            (opp.Other_reason_for_winning_or_losing__c == null || opp.Other_reason_for_winning_or_losing__c==''))
            {
                opp.Other_reason_for_winning_or_losing__c.addError('Please fill out the Other reason.');
            }
            if(opp.Decision_Lost_Type__c=='Lose To Competitor' && opp.Decision_Competitor__c==null)
            {
            	opp.Decision_Competitor__c.addError('Please fill out the competitor name.');
            }
            system.debug('****************'+opp.Main_reason_for_winning_or_losing__c+'***'+opp.Other_reason_for_winning_or_losing__c);
            continue;
        }
        if(opp.StageName == 'Implementation(Win)')
        {
        	
            if(opp.Decision_Result__c == null)
            {
                opp.Decision_Result__c.addError('Please fill out the result.');
            }
            if(opp.Main_reason_for_winning_or_losing__c == null)
            {
                opp.Main_reason_for_winning_or_losing__c.addError('Please fill out the main reason.');
            }
            if(opp.Main_reason_for_winning_or_losing__c =='Other' && 
			(opp.Other_reason_for_winning_or_losing__c == null || opp.Other_reason_for_winning_or_losing__c==''))
            {
                opp.Other_reason_for_winning_or_losing__c.addError('Please fill out the Other reason.');
            }
            continue;
        }
        if(old.StageName == opp.StageName)
        {
            continue;
        }
        
        if(opp.IsAllowAcrossStage__c)//2013-11-19允许跨阶段后，不再对必填字段进行判断
        {
            return;
        }
        //改为Implementation(Win)阶段时，要将Implementation(Win)对应必填字段都填写完整，否则提示报错
        //从Lost Order阶段改为其他阶段时，必须允许跨阶段才可修改，否则提示不允许跨阶段
        ///判断是否允许跨阶段修改
        if(!opp.IsAllowAcrossStage__c && (old.StageName == 'Lost Order' || (  old.StageName == 'Awareness of Needs' && opp.StageName != 'Assessment of Alternatives' && opp.StageName != 'Lost Order') || (old.StageName == 'Assessment of Alternatives' && opp.StageName != 'Binding Offer/Quotation Made(AOR)'  && opp.StageName != 'Lost Order')|| (old.StageName == 'Binding Offer/Quotation Made(AOR)' && opp.StageName != 'Alleviation of Risk'  && opp.StageName != 'Lost Order')|| (old.StageName == 'Alleviation of Risk' && opp.StageName != 'Decision(ContractSigned/PaymentReceived)'  && opp.StageName != 'Lost Order')|| (old.StageName == 'Decision(ContractSigned/PaymentReceived)' && opp.StageName != 'Implementation(Win)' && opp.StageName != 'Lost Order')))
        {
            opp.addError('This opportunity is not allowed to across stage');
            return;
        }
        string Requred;
        if(opp.StageName == 'Implementation(Win)')
        {
            Requred = getError(opp,opp.StageName);
        }
        ///判断上一阶段的必填字段是否都填写完全
        if(old.StageName != 'Alleviation of Risk' && old.StageName != 'Assessment of Alternatives')
        {
            string Requred2 = getError(opp,old.StageName);
            if(Requred == null || Requred == '')
            {
                Requred = Requred2;
            }
            else
            {
                Requred += (','+Requred2);
            }
        }
        else
        {
            if(old.StageName == 'Alleviation of Risk')
            {
                AOR_oppIds.add(opp.id);
            }
            else
            {
                AOA_oppIds.add(opp.id);
            }
        }
        if(Requred != null && Requred != '')
        {
            opp.addError('Stage:'+old.StageName+' of required field：'+Requred+' have not filled out.');
            if(old.StageName != 'Alleviation of Risk' && old.StageName != 'Assessment of Alternatives')
            {
                return;
            }
        }
    }
    if(AOR_oppIds.size() == 0 && AOA_oppIds.size() == 0)
    {
        return;
    }
    if(AOR_oppIds.size() > 0)
    {
        for(Opportunity opp : [select AOR_Requred__c,Quotation_to_customer_has_been_uploaded__c,Quotation_to_Jebsen_has_been_uploaded__c,(Select Id From Risk_Tracker__r) from Opportunity where id in: AOR_oppIds])
        {
            if(opp.Risk_Tracker__r != null && opp.Risk_Tracker__r.size() > 0 && opp.Quotation_to_Jebsen_has_been_uploaded__c == true)
            {
                continue;
            }
            else
            {
                string errorInfo='';
                for(string requred :map_Requred.get('Alleviation of Risk'))
                {
                    if(requred.contains('Quotation') && Boolean.valueOf(opp.get(LabeltoName.get(requred))) == false)
                    {
                        errorInfo+='Quotation to Jebsen has not been uploaded;';
                    }
                }
                if(opp.Risk_Tracker__r == null || opp.Risk_Tracker__r.size() == 0)
                {
                    if(errorInfo != null && errorInfo != '')
                    {
                        errorInfo += ' Alleviation of risk information has not yet been completed.';
                    }
                    else
                    {
                        errorInfo = ' Alleviation of risk information has not yet been completed.';
                    }
                }
                trigger.new[0].addError(errorInfo);
                return;
            }
        } 
    }
    if(AOA_oppIds.size() > 0)
    {
        for(Opportunity opp : [select AOA_Requred__c,RFQ_has_been_uploaded__c,  Automated_solution_has_been_uploaded__c,Overall_solution_has_been_uploaded__c,
                              (Select Id From Value_Map_Strategies__r) from Opportunity where id in: AOA_oppIds])
        {
            if(opp.Value_Map_Strategies__r != null && opp.Value_Map_Strategies__r.size() > 0 
            && opp.RFQ_has_been_uploaded__c == true && opp.Overall_solution_has_been_uploaded__c == true 
            && opp.Automated_solution_has_been_uploaded__c == true)
            {
                continue;
            }
            else
            {
                string errorInfo='';
                for(string requred :map_Requred.get('Assessment of Alternatives'))
                {
                    if((requred.contains('RFQ') || requred.contains('Overall') || requred.contains('Automated')) && Boolean.valueOf(opp.get(LabeltoName.get(requred))) == false)
                    {
                        if(requred.contains('RFQ'))
                        {
                            errorInfo+='RFQ to Jebsen has not been uploaded;';
                        }
                        else if(requred.contains('Overall'))
                        {
                            errorInfo+='Overall Solution to Jebsen has not been uploaded;';
                        }
                        else if(requred.contains('Automated'))
                        {
                            errorInfo+='Automated Solution to Jebsen has not been uploaded;';
                        }
                    }
                }
                if(opp.Value_Map_Strategies__r == null || opp.Value_Map_Strategies__r.size() == 0)
                {
                    if(errorInfo != null && errorInfo != '')
                    {
                        errorInfo += ' Customer progress list has not  been created.';
                    }
                    else
                    {
                        errorInfo = ' Customer progress list has not  been created.';
                    }
                }
                trigger.new[0].addError(errorInfo);
                return;
            }
        } 
    }
    public string getError(Opportunity opp,string stageName)
    {
        set<string> fieldNames = new set<string>();
        string Requred='';
        if(map_Requred != null && map_Requred.containsKey(stageName))
        {
            for(string f : map_Requred.get(stageName))
            {
                if((!LabeltoName.get(f).contains('uploaded')&&opp.get(LabeltoName.get(f)) == null) || (LabeltoName.get(f).contains('uploaded') && Boolean.valueOf(opp.get(LabeltoName.get(f))) == false))
                {
                    if(!LabeltoName.get(f).contains('uploaded'))
                    {
                        fieldNames.add(f);
                    }
                    if(Requred == '')
                    {
                        Requred = f;
                    }
                    else
                    {
                        Requred += (','+f);
                    }
                }
            }
        }
        if(fieldNames.size() > 0)
        {
            FieldAddError(opp,fieldNames);
        }
        return Requred;
    }
    public void FieldAddError(Opportunity opp,set<string> fieldNames)
    {
        /*if(fieldNames.contains('Questionnaire'))
        {
            opp.Questionnaire_has_been_uploaded__c.addError('This field is required');
        }
        if(fieldNames.contains('Automated solution'))
        {
            opp.Automated_solution_has_been_uploaded__c.addError('This field is required');
        }
        if(fieldNames.contains('Quotation to Jebsen'))
        {
            opp.Quotation_to_Jebsen_has_been_uploaded__c.addError('This field is required');
        }
        if(fieldNames.contains('Samples demand plan'))
        {
            opp.Samples_demand_plan_has_been_uploaded__c.addError('This field is required');
        }
        if(fieldNames.contains('Frame contract'))
        {
            opp.Frame_contract_has_been_uploaded__c.addError('This field is required');
        }
        if(fieldNames.contains('RFQ'))
        {
            opp.RFQ_has_been_uploaded__c.addError('This field is required');
        }
        if(fieldNames.contains('Overall solution'))
        {
            opp.Overall_solution_has_been_uploaded__c.addError('This field is required');
        }
        if(fieldNames.contains('Quotation to customer'))
        {
            opp.Quotation_to_customer_has_been_uploaded__c.addError('This field is required');
        }
        if(fieldNames.contains('Samples Purchase Order'))
        {
            opp.Samples_Purchase_Order_has_been_uploaded__c.addError('This field is required');
        }
        if(fieldNames.contains('PO to Profil'))
        {
            opp.PO_to_Profil_has_been_uploaded__c.addError('This field is required');
        }*/
        if(fieldNames.contains('Customer Requirements Description'))
        {
            opp.Customer_Requirements_Description__c.addError('This field is required');
        }
        if(fieldNames.contains('Main reason for winning or losing'))
        {
            opp.Main_reason_for_winning_or_losing__c.addError('This field is required');
        }
        if(fieldNames.contains('Other reason for winning or losing'))
        {
            opp.Other_reason_for_winning_or_losing__c.addError('This field is required');
        }
        if(fieldNames.contains('Actual Volume'))
        {
            opp.Actual_Volume__c.addError('This field is required');
        }
        if(fieldNames.contains('Actual Amount'))
        {
            opp.Actual_Amount__c.addError('This field is required');
        }
        if(fieldNames.contains('Actual Close Date'))
        {
            opp.Actual_Close_Date__c.addError('This field is required');
        }
        if(fieldNames.contains('Summary and Experience'))
        {
            opp.Summary_and_Experience__c.addError('This field is required');
        }
    }
}