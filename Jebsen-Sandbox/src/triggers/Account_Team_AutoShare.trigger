/*
 * 作者：ziyue
 * 时间：2013-10-18
 * 功能：根据Account_Team__c记录来进行数据的共享
 *       Account_Access__c:Read Only\Read/Write
 *       Activities_Access__c:Read Only (related Activities) -----不做共享操作
                              Read Only (all Activities)
 *       Contact_Access__c:Read Only (related Contact) -----不做共享操作 
                           Read Only (all Contact)
                           Read/Write (related Contact) -----不做共享操作
                           Read/Write (all Contact)
 *       Opportunity_Access__c:Read Only (related opportunity) -----不做共享操作，该选项意思是将业务机会共享给业务机会小组，因为业务机会本身就是共享给小组的，因此无需做任何操作
                               Read Only (all opportunity)
                               Read/Write (related opportunity) -----不做共享操作
                               Read/Write (all opportunity)
*/
trigger Account_Team_AutoShare on Account_Team__c (before insert,before delete) 
{
	//客户ID：客户小组人员ID:权限
	map<ID,map<ID,string>> map_acc_Members = new map<ID,map<ID,string>>();
	//客户共享
	list<AccountShare> list_AccShare = new list<AccountShare>();
	set<ID> UserIds = new set<ID>();
	if(trigger.isInsert)
	{//Select a.UserOrGroupId, a.RowCause, a.OpportunityAccessLevel, a.ContactAccessLevel, a.CaseAccessLevel, a.AccountId, a.AccountAccessLevel From AccountShare a
		for(Account_Team__c team : trigger.new)
		{
			if(team.Account__c != null && team.Team_Member__c != null && team.AccountOwnerId__c != team.Team_Member__c)
			{
				AccountShare accShare = new AccountShare();
				accShare.UserOrGroupId = team.Team_Member__c;
				accShare.AccountId = team.Account__c;
				if(team.Opportunity_Access__c == 'Read Only (all opportunity)')
				{
					accShare.OpportunityAccessLevel = 'Read';
				}
				else if(team.Opportunity_Access__c == 'Read/Write (all opportunity)')
				{
					accShare.OpportunityAccessLevel = 'Edit';
				}
				else
				{
					accShare.OpportunityAccessLevel = 'None';
				}
				if(team.Account_Access__c == 'Read/Write')
				{
					accShare.AccountAccessLevel = 'Edit';
				}
				else
				{
					accShare.AccountAccessLevel = 'Read';
				}
				list_AccShare.add(accShare);
				/*if(team.Contact_Access__c == 'Read Only (all Contact)')
				{
					accShare.ContactAccessLevel = 'Read';
				}
				else if(team.Contact_Access__c == 'Read/Write (all Contact)')
				{
					accShare.ContactAccessLevel = 'Edit';
				}
				else
				{
					accShare.ContactAccessLevel = 'None';
				}*/
				if(map_acc_Members.containsKey(team.Account__c))
				{
					map_acc_Members.get(team.Account__c).put(team.Team_Member__c,team.Contact_Access__c);
				}
				else
				{
					map<ID,string> memberIds = new map<ID,string>();
					memberIds.put(team.Team_Member__c,team.Contact_Access__c);
					map_acc_Members.put(team.Account__c,memberIds);
				}
			}
		}
	}
	else
	{
		for(Account_Team__c team : trigger.old)
		{
			if(team.Account__c != null && team.Team_Member__c != null && team.AccountOwnerId__c != team.Team_Member__c)
			{
				UserIds.add(team.Team_Member__c);
				if(map_acc_Members.containsKey(team.Account__c))
				{
					map_acc_Members.get(team.Account__c).put(team.Team_Member__c,team.Contact_Access__c);
				}
				else
				{
					map<ID,string> memberIds = new map<ID,string>();
					memberIds.put(team.Team_Member__c,team.Contact_Access__c);
					map_acc_Members.put(team.Account__c,memberIds);
				}
			}
		}
	}
	if(map_acc_Members.size() == 0)
	{
		return;
	}
	list<ContactShare> in_ContShare = new list<ContactShare>();
	if(trigger.isInsert)
	{
		for(Account acc:[Select id,(Select Id, AccountId From Contacts) From Account where id in:map_acc_Members.keySet()])
		{
			map<ID,string> map_cont = map_acc_Members.get(acc.id);
			for(ID UserId : map_cont.keySet())
			{
				if(map_cont.get(UserId) != 'Read Only (all Contact)' && map_cont.get(UserId) != 'Read/Write (all Contact)')
				{
					continue;
				}
				for(Contact cont : acc.Contacts)
				{//Select c.UserOrGroupId, c.RowCause, c.ContactId, c.ContactAccessLevel From ContactShare c
					ContactShare contShare = new ContactShare();
					contShare.UserOrGroupId = UserId;
					contShare.ContactId = cont.id;
					if(map_cont.get(UserId) == 'Read Only (all Contact)')
					{
						contShare.ContactAccessLevel = 'Read';
					}
					else if(map_cont.get(UserId) == 'Read/Write (all Contact)')
					{
						contShare.ContactAccessLevel = 'Edit';
					}
					in_ContShare.add(contShare);
				}
			}
		}
		if(list_AccShare.size() > 0)
		{
			insert list_AccShare;
		}
		if(in_ContShare.size() > 0)
		{
			insert in_ContShare;
		}
	}
	else
	{
		list<AccountShare> Del_AccShare = [select id from AccountShare where AccountId in:map_acc_Members.keySet() and UserOrGroupId in:UserIds and RowCause!='Owner'];
		list<ContactShare> Del_contShare = [select id from ContactShare where Contact.AccountId in:map_acc_Members.keySet() and UserOrGroupId in:UserIds and RowCause!='Owner'];
		if(Del_AccShare != null && Del_AccShare.size() > 0)
		{
			delete Del_AccShare;
		}
		if(Del_contShare != null && Del_contShare.size() > 0)
		{
			delete Del_contShare;
		}
	}
}