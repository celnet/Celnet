/**
 创建人: Betsy
 创建时间:2013-10-12
 功能:创建Opportunity 时，把当前用户的经理 赋给 Submiter_Manager
             更新所有人时，把更新后的经理赋给 Submiter_Manager
*/
trigger Opportunity_Update_Manage on Opportunity (before insert, before update)
{
	Set<Id> setId = new Set<Id>();
	for(Opportunity opp : trigger.new)
	{
		if(trigger.isInsert)
		{
			setId.add(UserInfo.getUserId());
		}
		else
		{
			setId.add(opp.OwnerId);
		}
	}
	map<Id,User> map_user=new map<Id,User>([select id,ManagerId from User where id in:setId]);
	for(Opportunity opp : trigger.new)
	{
		if(trigger.isInsert)
		{
			opp.Submiter_Manager__c = map_user.get(UserInfo.getUserId()).ManagerId ;
		}
		else
		{
			opp.Submiter_Manager__c = map_user.get(opp.OwnerId).ManagerId;
		}
	}
}