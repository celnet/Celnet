/*
 * 作者：Ziyue
 * 时间：2013-10-14
 * 描述：在opportunity上添加一个按钮，点击按钮弹出新页面，在页面中填写Escalation Type 与 Escalation Remark
*/
public class New_EscalationController 
{
    public Escalation__c Escalation{get;set;}
    public Opportunity opp{get;set;}
    public string status{get;set;}
    public string url{get;set;}
    public New_EscalationController(ApexPages.StandardController controller)
    {
        ID oppId = ApexPages.currentPage().getParameters().get('oppId');
        url='/'+oppId;
        Escalation = new Escalation__c();
        Escalation.Opportunity__c = oppId;
        opp = [select id,StageName
         from Opportunity where id =:oppId];
    }
    public void Hold()
    {
        Escalation.Status__c = 'Open';
        Escalation.Opportunity_Stage__c = opp.StageName;
        insert Escalation;
        status='success';
    }
}