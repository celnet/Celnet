/**
 * 作者：Ziyue
 * 时间：2013-10-29
 * 功能：class.Event_SendMeetingMinutesWS测试类
 */
@isTest
private class Test_Event_SendMeetingMinutesWS 
{
    static testMethod void myUnitTest() 
    {
        Account acc = new Account();
        acc.Name = 'acc';
        insert acc;
        Contact cont = new Contact();
        cont.LastName = 'ont';
        cont.FirstName = 'c';
        cont.Email = '123456789@qq.com';
        cont.AccountId = acc.id;
        insert cont;
        Lead led = new Lead();
        led.LastName = 'ed';
        led.FirstName = 'l';
        led.Email = '987654321@qq.com';
        led.Company = 'Company';
        insert led;
        Opportunity opp = new Opportunity();
        opp.Name = 'opp';
        opp.AccountId = acc.id;
        opp.StageName = 'AOA';
        opp.CloseDate = Date.today().addMonths(1);
        insert opp;
        Event eve = new Event();
        eve.Subject = 'Subject';
        eve.WhatId = opp.id;
        eve.StartDateTime = DateTime.now();
        eve.EndDateTime = DateTime.now().addDays(1);
        eve.Description = 'Description';
        eve.Meeting_Minutes__c = 'Meeting Minutes';
        insert eve;
        EventRelation er = new EventRelation();
        er.EventId = eve.id;
        er.RelationId = cont.id;
        insert er;
        
        EventRelation er1 = new EventRelation();
        er1.EventId = eve.id;
        er1.RelationId = led.id;
        insert er1;
        string result = Event_SendMeetingMinutesWS.SendMeetingMinutes(eve.id);
        system.assertEquals('Successfully sent!', result);
    }
}