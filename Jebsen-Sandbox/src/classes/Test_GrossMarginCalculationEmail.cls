@isTest
public class Test_GrossMarginCalculationEmail{
    
    static testmethod void unitTest(){
        
        Opportunity opp = new Opportunity();
        opp.Name = 'opp';
        opp.CloseDate = Date.today();
        opp.StageName = 'Awareness of Needs';
        opp.Probability = 10;
        insert opp;

        
        Gross_Margin_Calculation__c gmc1 = new Gross_Margin_Calculation__c(Total_Inventory_Cost__c=12,Opportunity__c = opp.Id,Transport__c='Nuts Sea Transport', Purchase__c = 33.40,Tax__c = 34.13, Price_with_Tax__c = 34.13, Inbound_Logistic_Cost__c = 43.43, Outbound_Logistic_cost__c = 41.44, Inventory__c = 34.44, Date__c = date.today(),Logistic_Num__c=23);
        Gross_Margin_Calculation__c gmc2 = new Gross_Margin_Calculation__c(Total_Inventory_Cost__c=12,Opportunity__c = opp.Id,Transport__c='Nuts Sea Transport', Purchase__c = 33.39,Tax__c = 34.13, Price_with_Tax__c = 34.13, Inbound_Logistic_Cost__c = 43.43, Outbound_Logistic_cost__c = 41.44, Inventory__c = 34.44, Date__c = date.today(),Logistic_Num__c=23);
        
        insert gmc1;
        insert gmc2;
        
        Gross_Margin_Calculation__c gmc3 = [Select Id, Product_Type__c, Product_Article__c,Logistic_Num__c,Transport__c, Destination__c, GP__c, Date__c, Purchase__c, Inventory__c, Tax__c, Price_with_Tax__c, Total_Inventory_Cost__c  From Gross_Margin_Calculation__c where Purchase__c = 33.40];
        Gross_Margin_Calculation__c gmc4 = [Select Id, Product_Type__c, Product_Article__c,Logistic_Num__c,Transport__c, Destination__c, GP__c, Date__c, Purchase__c, Inventory__c, Tax__c, Price_with_Tax__c,Total_Inventory_Cost__c From Gross_Margin_Calculation__c where Purchase__c = 33.39];
        
        //GrossMarginCalculationController.GPWrapper gpw1 = new GrossMarginCalculationController.GPWrapper(false,0,new Gross_Margin_Calculation__c(Opportunity__c = opp.Id, Purchase__c = 33.39,Tax__c = 34.13, Price_with_Tax__c = 34.13, Inbound_Logistic_Cost__c = 43.43, Outbound_Logistic_cost__c = 41.44, Inventory__c = 34.44));
        //GrossMarginCalculationController.GPWrapper gpw2 = new GrossMarginCalculationController.GPWrapper(false,1,new Gross_Margin_Calculation__c(Opportunity__c = opp.Id, Purchase__c = 33.39,Tax__c = 34.13, Price_with_Tax__c = 34.13, Inbound_Logistic_Cost__c = 43.43, Outbound_Logistic_cost__c = 41.44, Inventory__c = 34.44));
        GrossMarginCalculationController.GPWrapper gpw3 = new GrossMarginCalculationController.GPWrapper(false, 0, gmc3);
        list<GrossMarginCalculationController.GPWrapper> fastener = new list<GrossMarginCalculationController.GPWrapper>();
        //fastener.add(gpw1);
        //fastener.add(gpw2);
        fastener.add(gpw3);
        
        //GrossMarginCalculationController.GPWrapper gpw4 = new GrossMarginCalculationController.GPWrapper(false,0,new Gross_Margin_Calculation__c(Opportunity__c = opp.Id, Purchase__c = 33.39,Tax__c = 34.13, Price_with_Tax__c = 34.13, Inbound_Logistic_Cost__c = 43.43, Outbound_Logistic_cost__c = 41.44, Inventory__c = 34.44));
        //GrossMarginCalculationController.GPWrapper gpw5 = new GrossMarginCalculationController.GPWrapper(false,2,new Gross_Margin_Calculation__c(Opportunity__c = opp.Id, Purchase__c = 33.39,Tax__c = 34.13, Price_with_Tax__c = 34.13, Inbound_Logistic_Cost__c = 43.43, Outbound_Logistic_cost__c = 41.44, Inventory__c = 34.44));
        GrossMarginCalculationController.GPWrapper gpw6 = new GrossMarginCalculationController.GPWrapper(false,0,gmc4);
        list<GrossMarginCalculationController.GPWrapper> feeder = new list<GrossMarginCalculationController.GPWrapper>();
        //feeder.add(gpw4);
        //feeder.add(gpw5);
        feeder.add(gpw6);
        
        GrossMarginCalculationEmail.concatenateEmailHtmlBody(fastener, feeder, opp);
    }
}