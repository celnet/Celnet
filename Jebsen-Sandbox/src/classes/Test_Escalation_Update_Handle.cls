/**
 * Test Class Of Escalation_Update_Handle
 */
@isTest
private class Test_Escalation_Update_Handle {

    static testMethod void myUnitTest() {
        Opportunity opp = new Opportunity();
        opp.Name = 'opp';
        opp.StageName = 'Awareness of Needs';
        opp.CloseDate = Date.today().addMonths(1);
        insert opp;
        Escalation__c Escalation = new Escalation__c();
        Escalation.Status__c = 'Open';
        Escalation.Deadline__c = Date.today();
        Escalation.Opportunity__c = opp.id;
        insert Escalation;
        Escalation.Status__c='Closed';
        update Escalation;
        try
        {
        	Escalation.Status__c='Closed';
       		update Escalation;
        }
        catch(exception e){}
    }
}