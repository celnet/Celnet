/*
 * 作者：Ziyue
 * 时间：2013-10-11
 * 功能：上传文件
*/
public class AOA_AttachmentUploadController 
{
	public Attachment document {get; set;}
	public string Type{get;set;}
	public list<Document_upload__c> list_docUp{get;set;}
	public ID oppId{get;set;}
	public ID DelId{get;set;}
	public list<string> DocTypes{get;set;}
	public string url{get;set;}
	public string status{get;set;}
	public Opportunity opp{get;set;}
  	public list<SelectOption> DocumentTypes
	{
		get;set;
	}
	public Boolean IsRFQ{get;set;}
	public Boolean IsAutomated_solution{get;set;}
	public Boolean IsOverall_solution{get;set;}
	public Boolean IsEdit{get;set;}
	public List<AOAdocument> list_aoaDoc{get;set;}
	private List<String> list_TCGDocField = new List<String>{'RFQ','Concept Design','2D Drawings','Cost Calculation'};
	private List<String> list_ProfilDocField = new List<String>{'RFQ','Automated solution','Overall solution'};
	private Map<String , String> map_DocField {
		get{
			Map<String , String> map_t = new Map<String , String>();
			map_t.put('RFQ','RFQ_has_been_uploaded__c');
			map_t.put('Concept Design','Concept_Design_has_been_uploaded__c');
			map_t.put('2D Drawings','X2D_Drawings_has_been_uploaded__c');
			map_t.put('Cost Calculation','Cost_Calculation_has_been_uploaded__c');
			map_t.put('Automated solution','Automated_solution_has_been_uploaded__c');
			map_t.put('Overall solution','Overall_solution_has_been_uploaded__c');
			return map_t;
		}
		set;
	}
	
	public AOA_AttachmentUploadController(ApexPages.StandardController controller)
	{
		IsEdit = PubMethod.IsEdit(UserInfo.getUserRoleId());
		Schema.DescribeFieldResult F = Opportunity.AOA_Requred__c.getDescribe();
        set<string> options = new set<string>();
        for(Schema.Picklistentry p : F.getPicklistValues())
        {
            if(!p.isActive())
            {
                continue;
            }
            options.add(p.getValue());
        }
        IsRFQ =  options.contains('RFQ')?true:false;
        IsAutomated_solution =  options.contains('Automated solution')?true:false;
        IsOverall_solution =  options.contains('Overall solution')?true:false;
        
		document = new Attachment();
		oppId = controller.getId();
		this.initOpportunity();
		initDocTypes();
	}
	private void initOpportunity(){
		opp = [select id,RFQ_has_been_uploaded__c,Automated_solution_has_been_uploaded__c,
			Overall_solution_has_been_uploaded__c,RecordTypeId,RecordType.DeveloperName,
			X2D_Drawings_has_been_uploaded__c,Concept_Design_has_been_uploaded__c,Cost_Calculation_has_been_uploaded__c 
			from Opportunity where id=:oppId];
		url = '/'+oppId;   
		documentStatus();
	}
	//初始化文档类型
	private void initDocTypes(){
		//页面文档类型选项
		list<SelectOption> options = new list<SelectOption>();
		//查询文档类型
		DocTypes = new list<string>();
		if(opp.RecordType.DeveloperName == 'JTCG_pipeline'){
			//选项
			for(String s : list_TCGDocField){
				options.add(new SelectOption(s,s));
			}
			//文档类型
			DocTypes.addAll(list_TCGDocField);
		}else if(opp.RecordType.DeveloperName == 'Profil_Pipeline'){
			//选项
			for(String s : list_ProfilDocField){
				options.add(new SelectOption(s,s));
			}
			//文档类型
			DocTypes.addAll(list_ProfilDocField);
		}
		DocumentTypes = options;
		
		SelectDoc();
	}
	private void documentStatus(){
		list_aoaDoc = new List<AOAdocument>();
		if(opp.RecordType.DeveloperName == 'JTCG_pipeline'){
			for(String s : list_TCGDocField){
				Boolean blnHasUploaded = boolean.valueOf(opp.get(map_DocField.get(s)));
				AOAdocument aoaD = new AOAdocument(blnHasUploaded,true,s);
				list_aoaDoc.add(aoaD);
			}
			/*
			AOAdocument aoaRFQ = new AOAdocument(opp.RFQ_has_been_uploaded__c,true,'RFQ');
			list_aoaDoc.add(aoaRFQ);
			AOAdocument aoaCD = new AOAdocument(opp.Concept_Design_has_been_uploaded__c,true,'Concept Design');
			list_aoaDoc.add(aoaCD);
			AOAdocument aoa2D = new AOAdocument(opp.X2D_Drawings_has_been_uploaded__c,true,'2D Drawings');
			list_aoaDoc.add(aoa2D);
			AOAdocument aoaCC = new AOAdocument(opp.Cost_Calculation_has_been_uploaded__c,true,'Cost Calculation');
			list_aoaDoc.add(aoaCC);
			*/
		}else if(opp.RecordType.DeveloperName == 'Profil_Pipeline'){
			for(String s : list_ProfilDocField){
				Boolean blnHasUploaded = boolean.valueOf(opp.get(map_DocField.get(s)));
				AOAdocument aoaD = new AOAdocument(blnHasUploaded,false,s);
				list_aoaDoc.add(aoaD);
			}
			/*
			AOAdocument aoaRFQ = new AOAdocument(opp.RFQ_has_been_uploaded__c,false,'RFQ');
			list_aoaDoc.add(aoaRFQ);
			AOAdocument aoaAS = new AOAdocument(opp.Automated_solution_has_been_uploaded__c,false,'Automated solution');
			list_aoaDoc.add(aoaAS);
			AOAdocument aoaOS = new AOAdocument(opp.Overall_solution_has_been_uploaded__c,false,'Overall solution');
			list_aoaDoc.add(aoaOS);
			*/
		}
	}
	public void SelectDoc()
	{ 
		list_docUp = [select CreatedDate,CreatedBy.Name,Opportunity__c,id,DocumentID__c,Document_Name__c,
		             Document_Type__c,Name__c from Document_upload__c 
		             where Opportunity__c=:oppId and Document_Type__c in: DocTypes];
		if(list_docUp == null)  
		{
			list_docUp = new list<Document_upload__c>();
		}  
	}
	public void doSave() 
	{
		Document_upload__c doc_Up = new Document_upload__c();
		doc_Up.Opportunity__c = oppId;
		doc_Up.Document_Name__c = document.Name;
		doc_Up.Document_Type__c = Type;
		insert doc_Up;
		document.ParentId = doc_Up.id;
		insert document;
		doc_Up.DocumentID__c = document.Id;
		update doc_Up;
		doc_Up = [select CreatedDate,CreatedBy.Name,Opportunity__c,id,DocumentID__c,Document_Name__c,
		         Document_Type__c,Name__c from Document_upload__c where id=:doc_Up.id];
		list_docUp.add(doc_Up);
		document = new Attachment();
		status = 'success';
		opp.put(map_DocField.get(Type) , true);
		/*
		if(Type == 'RFQ')
		{
			opp.RFQ_has_been_uploaded__c = true;
		}
		else if(Type == 'Automated solution')
		{
			opp.Automated_solution_has_been_uploaded__c = true;
		}
		else
		{
			opp.Overall_solution_has_been_uploaded__c = true;
		}
		*/
		update opp;
		documentStatus();
	}
	//删除页面中点击删除的那一行记录
	public void Del()
	{
		//Document_upload__c Del_Doc = [select id from Document_upload__c where id=:DelId];
		Attachment attach = [select id from Attachment where ParentId=:DelId];
		delete attach;
		//delete Del_Doc;
		for(Document_upload__c doc : list_docUp)
		{
			if(doc.id == DelId)
			{
				delete doc;
			}
		}
		SelectDoc();
		status = 'success';
		//opp = [select id,RFQ_has_been_uploaded__c,Automated_solution_has_been_uploaded__c,Overall_solution_has_been_uploaded__c from Opportunity where id=:oppId];
		initOpportunity();
	}
	public class AOAdocument{
		public Boolean IsUpload{get;set;}
		public String DocumentName{get;set;}
		public Boolean IsRequired{get;set;}
		public AOAdocument(Boolean upload,Boolean req,String sName){
			IsUpload = upload;
			DocumentName = sName;
			IsRequired = req;
		}
	}
}