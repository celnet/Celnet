public class Account_Team_BatchAddController 
{
	public class Account_Team
	{
		public Integer Index{get;set;}
		public Account_Team__c AccTeam{get;set;}
	}
	public list<Account_Team> list_Account_Team{get;set;}
	public Integer Index{get;set;}//删除记录的下标
	public ID AccId{get;set;}
	public string status{get;set;}
	/*public list<SelectOption> getOptions()
    {
        Schema.DescribeFieldResult F = Account_Team__c.Account_Access__c.getDescribe();
        List<SelectOption> options = new List<SelectOption>();
        for(Schema.Picklistentry p : F.getPicklistValues())
        {
            options.add(new SelectOption(p.getLabel(),p.getValue()));
        }
        return options;
    }*/
   // public Boolean IsEdit{get;set;}
	public Account_Team_BatchAddController(ApexPages.StandardController controller)
	{
		//IsEdit = PubMethod.IsEdit(UserInfo.getUserRoleId());
		AccId = ApexPages.currentPage().getParameters().get('AccId');
		list_Account_Team = new list<Account_Team>();
        for(Integer i = 0;i < 5;i++)
        {
        	Account_Team accTeam = new Account_Team();
			accTeam.Index = list_Account_Team.size();
			Account_Team__c VMstrate = new Account_Team__c();
			VMstrate.Account__c = AccId;
			accTeam.AccTeam = VMstrate;
			list_Account_Team.add(accTeam);
        }
	}
	//删除操作
	public void Del()
	{
		if(Index>=0)
		{
			for(Integer i = 0 ; i<  list_Account_Team.size() ; i ++)
			{
				if(list_Account_Team[i].Index == Index)
				{
					if(list_Account_Team[i].AccTeam.id != null)
					{
						delete list_Account_Team[i].AccTeam;
					}
					list_Account_Team.remove(i);
					return;
				}
			}
		}
	}
	//添加记录操作
	public void Add()
	{
		Account_Team strate = new Account_Team();
		strate.Index = list_Account_Team.size();
		Account_Team__c VMstrate = new Account_Team__c();
		VMstrate.Account__c = AccId;
		strate.AccTeam = VMstrate;
		list_Account_Team.add(strate);
	}
	//保存记录操作
	public void Hold()
	{
		status = 'success';
		list<Account_Team__c> In_Account_Team = new list<Account_Team__c>();
		if(list_Account_Team != null && list_Account_Team.size() > 0)
		{
			for(Account_Team strate : list_Account_Team)
			{
				if(strate.AccTeam.Team_Member__c != null && strate.AccTeam.Account__c != null )
				{
					In_Account_Team.add(strate.AccTeam);
				}
			}
		}
		if(In_Account_Team.size() > 0)
		{
			insert In_Account_Team;
		}
	}
}