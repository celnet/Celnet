/**
 * Test Trigger Opportunity_GrossMarginCalculation
 */
@isTest (SeeAllData=true)
private class Test_Opportunity_GrossMarginCalculation {
	static testMethod void myUnitTest() {
		List<Gross_Margin_Calculation__c> list_gmc = new List<Gross_Margin_Calculation__c>();
		Gross_Margin_Calculation__c gmc1 = new Gross_Margin_Calculation__c();
		gmc1.Product_Article__c = 'EBF0616A08A';
		gmc1.Quantity_Carton__c = 3000;
		gmc1.Product_Type__c = 'Fastener';
		gmc1.Destination_Value__c = 0.5;
		gmc1.Destination__c = 'Shanghai';
		gmc1.Transport__c = 'Nuts Sea Transport';
		gmc1.Date__c = date.today();
		gmc1.Purchase__c = 100;
		gmc1.Duty__c = 8;
		gmc1.GP__c = 35;
		list_gmc.add(gmc1);
		Gross_Margin_Calculation__c gmc2 = new Gross_Margin_Calculation__c();
		gmc2.Product_Article__c = 'EBF0616A08A';
		gmc2.Quantity_Carton__c = 3000;
		gmc2.Product_Type__c = 'Fastener';
		gmc2.Destination_Value__c = 0.5;
		gmc2.Destination__c = 'Shanghai';
		gmc2.Transport__c = 'Nuts Air Transport';
		gmc2.Date__c = date.today();
		gmc2.Purchase__c = 100;
		gmc2.Duty__c = 8;
		gmc2.GP__c = 35;
		gmc2.Outbound_Logistic_cost__c = 84;
		gmc2.Inbound_Logistic_Cost__c = 23;
		gmc2.Inventory__c = 0.3;
		list_gmc.add(gmc2);
		Gross_Margin_Calculation__c gmc3 = new Gross_Margin_Calculation__c();
		gmc3.Product_Article__c = 'EBF0616A08A';
		gmc3.Quantity_Carton__c = 3000;
		gmc3.Product_Type__c = 'Feeder or tooling';
		gmc3.Destination_Value__c = 0.5;
		gmc3.Destination__c = 'Shanghai';
		//gmc3.Transport__c = 'Nuts Air Transport';
		gmc3.Date__c = date.today();
		gmc3.Purchase__c = 100;
		gmc3.GP__c = 35;
		gmc3.Outbound_Logistic_cost__c = 84;
		gmc3.Inbound_Logistic_Cost__c = 23;
		gmc3.Inventory__c = 0.3;
		list_gmc.add(gmc3);

		System.Test.startTest();
		insert list_gmc;
		delete gmc3;
		System.Test.stopTest();
	}
}