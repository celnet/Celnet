/**
 * 作者：Ziyue
 * 时间：2013-10-17
 * 功能：trigger.Document_upload_AutoUpdOpportunity测试类
 */
@isTest
private class Test_Document_upload_AutoUpdOpportunity 
{
    static testMethod void myUnitTest() 
    {
        Opportunity opp = new Opportunity();
        opp.CloseDate = Date.today().addDays(10);
        opp.StageName = 'AOA';
        opp.Name = 'opp';
        insert opp;
        Document_upload__c doc = new Document_upload__c();
        doc.Document_Name__c = 'doc';
        doc.Opportunity__c = opp.id;
        doc.Document_Type__c = 'Automated solution';
        insert doc;
        Document_upload__c doc2 = new Document_upload__c();
        doc2.Document_Name__c = 'doc2';
        doc2.Opportunity__c = opp.id;
        insert doc2;
        delete doc2;
    }
}