/**
 * Sunny
 * Test ForecastAccountExtension
 */
@isTest
private class Test_ForecastAccountExtension {

    static testMethod void myUnitTest() {
        Opportunity opp = new Opportunity();
        opp.Name = 'opp';
        opp.CloseDate = Date.today();
        opp.StageName = 'Awareness of Needs';
        opp.Probability = 10;
        insert opp;
        Account acc = new Account();
        acc.Name = 'acc';
        insert acc;
        
        Forecast_Account__c f = new Forecast_Account__c();
        f.Q1__c = 321;
        f.Q2__c = 32;
        f.Q3__c = 44;
        f.Q4__c = 51;
        //f.Opportunity__c = opp.Id;
        f.Account__c = acc.Id;
        f.Year__c = String.valueOf(Date.today().year());
        insert f;
        Forecast_Account__c f1 = new Forecast_Account__c();
        f1.Q1__c = 321;
        f1.Q2__c = 32;
        f1.Q3__c = 44;
        f1.Q4__c = 51;
        //f.Opportunity__c = opp.Id;
        f1.Account__c = acc.Id;
        f1.Year__c = String.valueOf(date.today().addYears(-1).year());
        insert f1;
        
        //ApexPages.currentPage().getParameters().put('oppId',opp.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        ForecastAccountExtension extension = new ForecastAccountExtension(controller);
        list<SelectOption> lso = extension.years;
        extension.addLine();
        extension.Index = 1;
        extension.selectedYear = String.valueOf(Date.today().year());
        extension.deleteRecord();  
        extension.saveRecords();
        extension.editRecord();
        extension.selectedYear = String.valueOf(Date.today().addYears(1).year());
        extension.refreshRecords();
        
    }
}