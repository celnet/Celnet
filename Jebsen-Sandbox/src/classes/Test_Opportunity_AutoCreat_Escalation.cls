/*Author:Leo
 *Date:2014-3-31
 *Function:test Opportunity_AutoCreat_Escalation
 */
@isTest
private class Test_Opportunity_AutoCreat_Escalation {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Account acc = new Account();
        acc.Name = 'AUDI';
        insert acc;
        Opportunity opp = new Opportunity();
        opp.Name = 'AUDI2 opportunity';
        opp.AccountId = acc.id;
        opp.CloseDate = Date.today();
        opp.StageName = 'Awareness of Needs';
        insert opp;
        opp.Name = 'AUDI2';
        update opp;
        opp.Recently_Escalation_Type__c = 'others';
        update opp;
        opp.Recently_Escalation_Type__c = 'resource request';
        update opp;
        opp.Recently_Escalation_Type__c = null;
        update opp;
        opp.Recently_Escalation_Type__c = 'resource request';
        update opp;
        List<Escalation__c> esc = [Select Opportunity__c, Opportunity__r.Name, CreatedDate, Escalation_Type__c 
        						   From Escalation__c 
        						   order by CreatedDate limit 1];
        delete esc;
        esc = [Select Opportunity__c, Opportunity__r.Name, CreatedDate, Escalation_Type__c 
        						   From Escalation__c 
        						   order by CreatedDate limit 1];
        delete esc;
        esc = [Select Opportunity__c, Opportunity__r.Name, CreatedDate, Escalation_Type__c 
        						   From Escalation__c 
        						   order by CreatedDate limit 1];
        delete esc;
        esc = [Select Opportunity__c, Opportunity__r.Name, CreatedDate, Escalation_Type__c 
        						   From Escalation__c 
        						   order by CreatedDate limit 1];
        delete esc;
        
    }
}