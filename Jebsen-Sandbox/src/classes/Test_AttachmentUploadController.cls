/**
 * 作者：Ziyue
 * 时间：2013-10-22
 * 功能：上传文件控制类的测试类
 */
@isTest
private class Test_AttachmentUploadController 
{
    static testMethod void myUnitTest() 
    {
        Opportunity opp = new Opportunity();
        opp.Name = 'opp';
        opp.CloseDate = Date.today();
        opp.StageName = 'AOA';
        insert opp;
        ApexPages.StandardController controller = new ApexPages.StandardController(opp);
        Achievement_AttachmentUploadController attachcontr = new Achievement_AttachmentUploadController(controller);
        attachcontr.document.Name = 'acc';
        attachcontr.document.Body = Blob.valueOf('acc');
        attachcontr.doSave();
        attachcontr.DelId = attachcontr.list_docUp[0].id;
        attachcontr.Del();
        
        AOA_AttachmentUploadController AOA_contr = new AOA_AttachmentUploadController(controller);
        AOA_contr.document.Name = 'acc';
        AOA_contr.document.Body = Blob.valueOf('acc');
        AOA_contr.doSave();
        AOA_contr.DelId = AOA_contr.list_docUp[0].id;
        AOA_contr.Del();
        
        AON_AttachmentUploadController AON_contr = new AON_AttachmentUploadController(controller);
        AON_contr.document.Name = 'acc';
        AON_contr.document.Body = Blob.valueOf('acc');
        AON_contr.doSave();
        AON_contr.DelId = AON_contr.list_docUp[0].id;
        AON_contr.Del();
        
        AOR_AttachmentUploadController AOR_contr = new AOR_AttachmentUploadController(controller);
        AOR_contr.document.Name = 'acc';
        AOR_contr.document.Body = Blob.valueOf('acc');
        AOR_contr.doSave();
        AOR_contr.DelId = AOR_contr.list_docUp[0].id;
        AOR_contr.Del();
        
        Decision_AttachmentUploadController Dec_contr = new Decision_AttachmentUploadController(controller);
        Dec_contr.document.Name = 'acc';
        Dec_contr.document.Body = Blob.valueOf('acc');
        Dec_contr.doSave();
        Dec_contr.DelId = Dec_contr.list_docUp[0].id;
        Dec_contr.Del();
    }
}