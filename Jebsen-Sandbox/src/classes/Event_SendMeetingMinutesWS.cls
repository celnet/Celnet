/*
 * 作者：Ziyue
 * 时间：2013-10-29
 * 功能：点击按钮会发送邮件给 最大的老大和邀请人员  内容：Meeting Minutes内容与相关业务机会、时间 等
*/
global class Event_SendMeetingMinutesWS 
{
	webservice static string SendMeetingMinutes(ID EventId)
	{
		Event ev = [Select id,IsSend_MeetingMinutes__c,WhoId,Meeting_Minutes__c, WhatId,What.Name, Subject, StartDateTime, EndDateTime, 
		           Description, ActivityDate,(Select RelationId,Relation.Email, EventId,Relation.LastName, 
	           					Status, RespondedDate, Response From EventRelations ) From Event where id =:EventId];
	    //最大的老大
		list<User> CEO = [select id,Email from User where UserRole.Name = 'JAT_Director'];
		string[] emails = new string[]{};
		if(CEO != null && CEO.size() > 0 && CEO[0].Email != null)
		{
			emails.add(CEO[0].Email);
		}       					
	    set<ID> contIds = new set<ID>();
	    set<ID> leadIds = new set<ID>();
	    set<ID> userIds = new set<ID>();
	    if(ev.EventRelations != null && ev.EventRelations.size() > 0)
	    {
	    	for(EventRelation er : ev.EventRelations)
	    	{
	    		string objectType = er.RelationId.getSObjectType().getDescribe().getName();
	    		if(objectType == 'Contact')
	    		{
	    			contIds.add(er.RelationId);
	    		}
	    		else if(objectType == 'Lead')
	    		{
	    			leadIds.add(er.RelationId);
	    		}
	    		else 
	    		{
	    			userIds.add(er.RelationId);
	    		}
	    	}
	    }
	    if(contIds.size() > 0)
	    {
	    	for(Contact cont : [select Email from Contact where Email != null and id in:contIds])
	    	{
	    		emails.add(cont.Email);
	    	}
	    }
		if(leadIds.size() > 0)
		{
			for(Lead led : [select Email from Lead where Email != null and id in: leadIds])
			{
				emails.add(led.Email);
			}
		}
		if(userIds.size() > 0)
		{
			for(User u : [select Email from User where Email != null and id in: userIds])
			{
				emails.add(u.Email);
			}
		}
		if(emails.size() == 0)
		{
			return 'No mail can be sent.';
		}
		string repBody = 'Dear <br><br>';
		repBody += 'There is a meeting will be held. <br>';
		repBody += 'Meeting theme: '+ev.Subject+'<br>';
		repBody += 'Meeting start time: '+ev.StartDateTime+'<br>';
		repBody += 'Meeting end time: '+ev.EndDateTime+'<br>';
		repBody += 'Meeting Content: '+ev.Description+'<br>';
		repBody += 'Meeting Minutes: '+ev.Meeting_Minutes__c+'<br>';
		if(ev.WhatId.getSObjectType().getDescribe().getName() == 'Opportunity')
		{
			repBody += 'Opportunity: '+ev.What.Name+'<br>';
		}
		List<Messaging.Singleemailmessage> mailList = new List<Messaging.Singleemailmessage>();
		Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
		mail.setToAddresses(emails);
		mail.setHtmlBody(repBody);
		mail.setSubject('Meeting Minutes');
		mail.setSaveAsActivity(false);//存为活动
		mail.setBccSender(false);
		mail.setReplyTo('noreply@salesforce.com');
		mail.setSenderDisplayName('Salesforce');
		//mail.setTargetObjectId(ev.WhatId);
		mailList.add(mail);
		try
		{
			Messaging.sendEmail(mailList);	
		}
		catch(Exception exc)
		{
			return exc.getMessage();
		}
		ev.IsSend_MeetingMinutes__c = true;
		update ev;
		return 'Successfully sent!';
	}
}