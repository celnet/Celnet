/**
 * 作者：Ziyue
 * 时间：2013-10-22
 * 描述：class.Opportunity_ReportController测试类
 */
@isTest
private class Test_Opportunity_ReportController 
{
    static testMethod void myUnitTest() 
    {
    	Account acc = new Account();
    	acc.Name = 'acc';
    	insert acc;
    	Contact cont = new Contact();
    	cont.AccountId = acc.id;
    	cont.FirstName = 'c';
    	cont.LastName = 'ont';
    	insert cont;
        Opportunity opp = new Opportunity();
        opp.AccountId = acc.id;
        opp.Name = 'opp';
        opp.CloseDate = Date.today();
        opp.StageName = 'AOA';
        insert opp;
        opp.StageName = 'AON';
        opp.Product_Volume__c = 100;
        update opp;
        OpportunityTeamMember teamMember = new OpportunityTeamMember();
        teamMember.OpportunityId = opp.id;
        teamMember.UserId = UserInfo.getUserId();
        teamMember.TeamMemberRole = 'ACT Commissioner';
        insert teamMember;
        OpportunityCompetitor competitor = new OpportunityCompetitor();
        competitor.CompetitorName = 'competitor';
        competitor.OpportunityId = opp.id;
        competitor.Strengths = 'strengths';
        competitor.Weaknesses = 'weaknesses';
        insert competitor;
        
        Order_Amount_Per_Year__c oapy = new Order_Amount_Per_Year__c();
        oapy.Account__c = acc.id;
        oapy.Amount__c = 100;
        insert oapy;
        Contact_Role__c role = new Contact_Role__c();
        role.Contact__c = cont.id;
        role.Opportunity__c = opp.id;
        insert role;
        Value_Map_Strategies__c strate1 = new Value_Map_Strategies__c();
        strate1.Opportunity__c = opp.id;
        strate1.Importance_to_Customer__c = 'H';
        strate1.Competitive_Standing__c = 'M';
        strate1.Value_Options__c = 'Value Winner';
        strate1.Value_Criteria__c = 'price';
        insert strate1;
        
        Value_Map_Strategies__c strate2 = new Value_Map_Strategies__c();
        strate2.Opportunity__c = opp.id;
        strate2.Importance_to_Customer__c = 'M';
        strate2.Competitive_Standing__c = 'H';
        strate2.Value_Options__c = 'Value Killer';
        strate2.Value_Criteria__c = 'design';
        insert strate2;
        
        Value_Map_Strategies__c strate3 = new Value_Map_Strategies__c();
        strate3.Opportunity__c = opp.id;
        strate3.Importance_to_Customer__c = 'H';
        strate3.Competitive_Standing__c = 'L';
        strate3.Value_Options__c = 'Value Sleeper';
        strate3.Value_Criteria__c = 'quality';
        insert strate3;
        
        Value_Map_Strategies__c strate4 = new Value_Map_Strategies__c();
        strate4.Opportunity__c = opp.id;
        strate4.Importance_to_Customer__c = 'M';
        strate4.Competitive_Standing__c = 'M';
        strate4.Value_Options__c = 'Value Other';
        strate4.Value_Criteria__c = 'lead time';
        insert strate4;
        
        Risk_Tracker__c traker = new Risk_Tracker__c();
        traker.Opportunity__c = opp.id;
        traker.Risk_Type__c = 'Company';
        traker.What_risks_or_uncertainties_are_there__c	= 'Strategic';
        insert traker;
        
        Risk_Tracker__c traker1 = new Risk_Tracker__c();
        traker1.Opportunity__c = opp.id;
        traker1.Risk_Type__c = 'Company';
        traker1.What_risks_or_uncertainties_are_there__c = 'Tactical';
        insert traker1;
        
        Risk_Tracker__c traker2 = new Risk_Tracker__c();
        traker2.Opportunity__c = opp.id;
        traker2.Risk_Type__c = 'Individual';
        traker2.What_risks_or_uncertainties_are_there__c = 'Political';
        insert traker2;
        
        Risk_Tracker__c traker3 = new Risk_Tracker__c();
        traker3.Opportunity__c = opp.id;
        traker3.Risk_Type__c = 'Individual';
        traker3.What_risks_or_uncertainties_are_there__c = 'Individual';
        insert traker3;
        
        Imlpementation_Plan__c plan = new Imlpementation_Plan__c();
        plan.Type__c = 'Promises';
        plan.Opportunity__c = opp.id;
        insert plan;
        
        Imlpementation_Plan__c plan1 = new Imlpementation_Plan__c();
        plan1.Type__c = 'Commitments';
        plan1.Opportunity__c = opp.id;
        insert plan1;
        
        Imlpementation_Plan__c plan2 = new Imlpementation_Plan__c();
        plan2.Type__c = 'Issues';
        plan2.Opportunity__c = opp.id;
        insert plan2;
        
        Imlpementation_Plan__c plan3 = new Imlpementation_Plan__c();
        plan3.Type__c = 'Other';
        plan3.Opportunity__c = opp.id;
        insert plan3;
        ApexPages.currentPage().getParameters().put('oppId',opp.id);
        Opportunity_ReportController report = new Opportunity_ReportController();   
        string value=report.ActualCloseDate;
        value=report.CloseDate;
        value=report.thisDt;
        value=report.LastModifiedOn;
        value=report.CeatedOn;
        // list<Opportunity_ReportController.OpportunityHistories> history=report.list_OpportunityHistories;
        
    }
}