/*
 * 作者：Ziyue
 * 时间：2013-11-1
 * 功能：更换业务机会的阶段页面。当跨阶段报错时，提交审批按钮显示出来，点击提交审批按钮时，弹出提交审批页面
*/
public class Opportunity_ChangeStageController 
{
	public Opportunity opp{get;set;}
	public string StartProbability{get;set;}
	public string EndProbability{get;set;}
	public ID oppId{get;set;}
	public Boolean IsDisabled{get;set;}
	public string status{get;set;}
	public string URL{get;set;}
	public Opportunity_ChangeStageController(ApexPages.StandardController controller)
	{
		IsDisabled = true;
		oppId = ApexPages.currentPage().getParameters().get('oppId');
		URL = '/apex/Opportunity_Stage?id='+oppId;
		opp = [select id,StageName,	Probability from Opportunity where id =: oppId];
	}
	public void Hold()
	{
		if(StartProbability != null && EndProbability != null && StartProbability != '' && EndProbability != '' && opp.Probability != null)
		{
			if(opp.Probability < Double.valueOf(StartProbability) || opp.Probability > Double.valueOf(EndProbability))
			{
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING , 'Probability must be completed within the scope of Probability.');  
         		ApexPages.addMessage(msg);
         		return;
			}
		}
		try
		{
			update opp;
		}
		catch(Exception exc)
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING , exc.getMessage());  
         	ApexPages.addMessage(msg);
         	if(exc.getMessage().contains('This opportunity is not allowed to across stage'))
         	{
         		IsDisabled = false;
         	}
         	return;
		}
		status = 'success';
	}
}