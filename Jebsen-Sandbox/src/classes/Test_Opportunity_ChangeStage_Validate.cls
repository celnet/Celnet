/**
 * 作者：Ziyue
 * 时间：2013-10-14
 * 描述：trigger.Opportunity_ChangeStage_Validate测试类
 */
@isTest
private class Test_Opportunity_ChangeStage_Validate 
{
    static testMethod void myUnitTest() 
    {
        Opportunity opp = new Opportunity();
        opp.Name = 'opp';
        opp.StageName = 'Awareness of Needs';
        opp.CloseDate = Date.today().addMonths(1);
        insert opp;
        try
        {
	        opp.StageName = 'Binding Offer/Quotation Made(AOR)';
	        update opp;
        }
        catch(Exception exc){}
        try
        {
	        opp.StageName = 'Assessment of Alternatives';
	        update opp;
        }
        catch(Exception exc){}
        opp.StageName = 'Awareness of Needs';
        update opp;
        opp.StageName = 'Lost Order';
        try
        {
        	update opp;
        }
        catch(Exception exc){}
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'opp1';
        opp1.StageName = 'Alleviation of Risk';
        opp1.CloseDate = Date.today().addMonths(1);
        insert opp1;
        try
        {
	        opp1.StageName = 'Decision(ContractSigned/PaymentReceived)';
	        update opp1;
        }
        catch(Exception exc){}
        
        Opportunity opp2 = new Opportunity();
        opp2.Name = 'opp2';
        opp2.StageName = 'Assessment of Alternatives';
        opp2.CloseDate = Date.today().addMonths(1);
        insert opp2;
        try
        {
	        opp2.StageName = 'Binding Offer/Quotation Made(AOR)';
	        update opp2;
        }
        catch(Exception exc){}
    }
}