/*
 * 作者：Ziyue
 * 时间：2013-9-25
 * 描述：业务机会页面上的AOR模块页面
*/
public class Risk_TrackerController 
{
	public list<Risk_Tracker__c> list_Tracker{get;set;}
	public ID OppId{get;set;}
	public Boolean IsDisabled{get;set;}//是否无用
	public Boolean IsAccess{get;set;}//控制Approve和Retake按钮的可见性
	public PubMethod pub{get;set;}
	public Boolean IsEdit{get;set;}
	public Risk_TrackerController(ApexPages.StandardController controller)
	{
		IsEdit = PubMethod.IsEdit(UserInfo.getUserRoleId());
		pub = new PubMethod();
		Opportunity opp = [select Owner.UserRoleId from Opportunity where id =: controller.getId()];
		set<ID> HighRole = pub.getHighRoleName(opp.Owner.UserRoleId);
		if(HighRole.contains(UserInfo.getUserRoleId()))
		{
			IsAccess = true;
		}
		else
		{
			IsAccess = false;
		}
		IsDisabled = true;
		//Risk Type:what risks or uncertainties are there
		//map<string,list<string>> map_RiskType = new map<string,list<string>>();
		set<string> Company = new set<string>();
		Company.add('Strategic');
		Company.add('Tactical');
		//map_RiskType.put('Company',Company);
		set<string> Individual = new set<string>();
		Individual.add('Political');
		Individual.add('Individual');
		//map_RiskType.put('Individual',Individual);
		
		OppId = controller.getId();
		list_Tracker = new list<Risk_Tracker__c>();
		list<Risk_Tracker__c> list_rt = [select id,OwnerId,Cure_Plan_B__c,Prevention_Plan_A__c,	Risk_Type__c,
		                                What_risks_or_uncertainties_are_there__c from Risk_Tracker__c 
		                                where Opportunity__c =: OppId order by Risk_Type__c,
		                                What_risks_or_uncertainties_are_there__c Asc];
		
		
		//Risk Type:what risks or uncertainties are there:Risk_Tracker
		map<string,Risk_Tracker__c> map_rt = new map<string,Risk_Tracker__c>();
		if(list_rt != null && list_rt.size() > 0)
		{
			for(Risk_Tracker__c rt : list_rt)
			{
				map_rt.put(rt.What_risks_or_uncertainties_are_there__c,rt);
			}
		}
		
		Schema.DescribeFieldResult F = Risk_Tracker__c.What_risks_or_uncertainties_are_there__c.getDescribe();
        for(Schema.Picklistentry p : F.getPicklistValues())
        {
        	if(!p.isActive())
        	{
        		continue;
        	}
        	if(map_rt.containsKey(p.getValue()))
        	{
        		list_Tracker.add(map_rt.get(p.getValue()));
        	}
        	else
        	{
        		Risk_Tracker__c rt = new Risk_Tracker__c();
        		rt.Opportunity__c = OppId;
        		rt.What_risks_or_uncertainties_are_there__c = p.getValue();
        		if(Company.contains(p.getValue()))
        		{
        			rt.Risk_Type__c = 'Company';
        		}
        		else
        		{
        			rt.Risk_Type__c = 'Individual';
        		}
        		list_Tracker.add(rt);
        	}
        }	
	}
	public void Upd()
	{
		IsDisabled = false;
	}
	public void Hold()
	{
		IsDisabled = true;
		list<Risk_Tracker__c> In_rt = new list<Risk_Tracker__c>();
		list<Risk_Tracker__c> upd_rt = new list<Risk_Tracker__c>();
		for(Risk_Tracker__c rt : list_Tracker)
		{
			if(rt.id == null)
			{
				In_rt.add(rt);
			}
			else
			{
				upd_rt.add(rt);
			}
		}
		if(In_rt.size() > 0)
		{
			insert In_rt;
		/*	set<ID> rtIds = new set<ID>();
			for(Risk_Tracker__c rt : In_rt)
			{
				rtIds.add(rt.id);
			}
			//将新建进去的记录重新查询一遍，将需要的信息都查询出来
			map<ID,Risk_Tracker__c> map_rt = new map<ID,Risk_Tracker__c>([select id,OwnerId,Cure_Plan_B__c,Prevention_Plan_A__c,	Risk_Type__c,
										                                What_risks_or_uncertainties_are_there__c from Risk_Tracker__c 
										                                where id in: rtIds ]);
			for(Risk_Tracker__c rt : list_Tracker)		
			{
				if(map_rt.containsKey(rt.id))
				{
					rt = map_rt.get(rt.id);
				}
			}					                                
			*/							                                
		}
		if(upd_rt.size() > 0)
		{
			update upd_rt;
		}
		
	}
	/*
	public void Approve()
	{
		list<Risk_Tracker__Share> RT_Shares = new list<Risk_Tracker__Share>();
		for(OpportunityTeamMember team : [Select UserId, TeamMemberRole, OpportunityId, OpportunityAccessLevel, Id 
		                                  From OpportunityTeamMember where OpportunityId =: OppId])
		{
			if(team.UserId != null)
			{
				for(Risk_Tracker__c st:list_Tracker)
				{
					if(st.id != null  && st.OwnerId != team.UserId)
					{
						Risk_Tracker__Share speShare = new Risk_Tracker__Share();
						speShare.ParentId = st.id;
						if(team.OpportunityAccessLevel == 'Read Only')
						{
							speShare.AccessLevel = 'Read';
						}
						else
						{
							speShare.AccessLevel = 'Edit';
						}
						speShare.UserOrGroupId = team.UserId;
						RT_Shares.add(speShare);
					}
				}
			}
		}
		if(RT_Shares.size() > 0)
		{
			insert RT_Shares;
		}
	}
	public void Retake()
	{
		set<ID> set_MemberIds = new set<ID>();
		set<ID> set_speIds = new set<ID>();
		for(OpportunityTeamMember team : [Select UserId, TeamMemberRole, OpportunityId, OpportunityAccessLevel, Id 
		                                  From OpportunityTeamMember where OpportunityId =: OppId])
		{
			if(team.UserId != null)
			{
				for(Risk_Tracker__c st:list_Tracker)
				{
					if(st.id != null  && st.OwnerId != team.UserId)
					{
						set_MemberIds.add(team.UserId);
						set_speIds.add(st.id);
					}
				}
			}
		}
		if(set_MemberIds != null && set_speIds != null)
		{
			list<Risk_Tracker__Share> list_Share = [select id from Risk_Tracker__Share 
			                                       where UserOrGroupId in:set_MemberIds and ParentId in: set_speIds];
			if(list_Share != null && list_Share.size() > 0)
			{
				delete list_Share;
			}
		}
	}*/
	
}