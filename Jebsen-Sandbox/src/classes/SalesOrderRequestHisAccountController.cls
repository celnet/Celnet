/*
 * 作者：Sunny
 * 时间：2014-8-29
 * 功能：Sales order request历史
*/
public class SalesOrderRequestHisAccountController {
	private ID accId;
	public List<HisLineItem> list_HislineItem{get;set;}
	private List<Sales_Order_Request__c> list_hisData =new List<Sales_Order_Request__c>();
	public class HisLineItem{
		public String SO_Date{get;set;}
		public String Archived_Date{get;set;}
		public Double Total_Amount{get;set;}
		public String DetailLineId{get;set;}
		public Sales_Order_Request__c sor{get;set;}
		public List<Sales_Order_Request__c> list_detail{get;set;}
	}
	public SalesOrderRequestHisAccountController(ApexPages.StandardController controller){
		accId = controller.getId();
		this.initSalesOrderList();
	}
	private void initSalesOrderList(){
		for(Sales_Order_Request__c so : [Select s.Submitted__c, s.Archived__c, s.Account__c, s.Unit_List_Price__c, s.Total_Amount_whit_Tax_HK__c, 
				s.Tax_Amount_CNY__c, s.Quantity__c, s.Qty_Box__c, s.Product_Article__c, s.Name, s.Id, s.Description__c, s.Date__c, 
				s.Customers_Drawing_Number__c, s.Customer_Schedule_Agreement_Number__c, s.Comments__c, s.ArchivedDate__c 
				From Sales_Order_Request__c s Where Account__c =: accId And Archived__c = true order by createddate]){
			list_hisData.add(so);
		}
		//initTotal();
		//initShowControl();
		initHisList();
	}
	private void initHisList(){
		list_HislineItem = new List<HisLineItem>();
		Map<Date , List<Sales_Order_Request__c>> map_HisSO = new Map<Date , List<Sales_Order_Request__c>>();
		system.debug(list_hisData.size());
		for(Sales_Order_Request__c so : this.list_hisData){
			List<Sales_Order_Request__c> list_so = new List<Sales_Order_Request__c>();
			if(map_HisSO.containsKey(so.Date__c)){
				list_so = map_HisSO.get(so.Date__c);
			}
			list_so.add(so);
			map_HisSO.put(so.Date__c , list_so);
		}
		this.initHisLineItem(map_HisSO);
	}
	private void initHisLineItem(Map<Date , List<Sales_Order_Request__c>> map_HisSO){
		for(Date d : map_HisSO.keySet()){
			HisLineItem hisItem = new HisLineItem();
			hisItem.list_detail = map_HisSO.get(d);
			hisItem.SO_Date = d.year()+'-'+d.month()+'-'+d.day();
			hisItem.Total_Amount = 0 ;
			hisItem.DetailLineId = 'DetailLine'+list_HislineItem.size();
			hisItem.sor = new Sales_Order_Request__c();
			hisItem.sor.UtilField__c = 0 ;
			for(Sales_Order_Request__c so : hisItem.list_detail){
				if(so.ArchivedDate__c != null)
				hisItem.Archived_Date = so.ArchivedDate__c.year()+'-'+so.ArchivedDate__c.month()+'-'+so.ArchivedDate__c.day();
				hisItem.Total_Amount += so.Tax_Amount_CNY__c ;
				hisItem.sor.UtilField__c += so.Total_Amount_whit_Tax_HK__c;
			}
			list_HislineItem.add(hisItem);
		}
	}
}