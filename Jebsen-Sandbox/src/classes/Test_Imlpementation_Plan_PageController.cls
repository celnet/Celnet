/**
 * 作者：Ziyue
 * 时间：2013-10-29
 * 功能：class.Imlpementation_Plan_PageController测试类
 */
@isTest
private class Test_Imlpementation_Plan_PageController 
{
    static testMethod void myUnitTest() 
    {
        Opportunity opp = new Opportunity();
        opp.Name = 'opp';
        opp.StageName = 'AOA';
        opp.CloseDate = Date.today().addMonths(1);
        insert opp;
        ApexPages.StandardController controller = new ApexPages.StandardController(opp);
        Imlpementation_Plan_PageController plan_Page = new Imlpementation_Plan_PageController(controller);
        plan_Page.Add();
        plan_Page.Add();
        plan_Page.Index = plan_Page.list_Strategies.size() - 1;
        plan_Page.Del();
        plan_Page.Index = plan_Page.list_Strategies.size() - 1;
        plan_Page.Upd();
        plan_Page.Hold();
        
        Imlpementation_Plan_PageController plan_Page1 = new Imlpementation_Plan_PageController(controller);
        plan_Page1.getOptions();
        plan_Page1.Add();
        plan_Page1.Add();
        plan_Page1.Index = plan_Page.list_Strategies.size() - 1;
        plan_Page1.Del();
        plan_Page1.Index = plan_Page.list_Strategies.size() - 1;
        plan_Page1.Upd();
        plan_Page1.Hold();
    }
}