/*
 * 作者：Sunny
 * 时间：2014-8-29
 * 功能：Sales order request录入及发货
*/
public class SalesOrderRequestAccountController {
	private ID accId;
	private Account acc;
	public List<LineItem> list_lineItem{get;set;}
	public List<HisLineItem> list_HislineItem{get;set;}
	public Integer Index{get;set;}
	public Boolean ShowEdit{get;set;}
	public Boolean ShowArchive{get;set;}
	public Sales_Order_Request__c TotalQtyAmount{get;set;}
	public Sales_Order_Request__c TotalQtyAmount2{get;set;}
	//public Double TotalQty{get;set;}
	//public Double TotalAmount{get;set;}
	public String pageMsg{get;set;}
	private Set<String> list_email = new Set<String>();
	private List<User> list_Shipper = new list<User>();
	private Id accOwnerId; 
	private List<Sales_Order_Request__c> list_hisData =new List<Sales_Order_Request__c>();
	public SalesOrderRequestAccountController(ApexPages.StandardController controller){
		accId = controller.getId();
		initSalesOrderList();
	}
	public void iAdd(){
		pageMsg = '';
		list_lineItem.add(new LineItem(true,list_lineItem.size(),new Sales_Order_Request__c(Account__c=accId,Date__c=date.today())));
	}
	public void iEdit(){
		pageMsg = '';
		if(Index>=0)
        {
            for(Integer i = 0 ; i<  list_lineItem.size() ; i ++)
            {
                if(list_lineItem[i].Index == Index)
                {
                    if(list_lineItem[i].SalesOrder.id != null)
                    {
                        list_lineItem[i].IsEdit = true;
                        //delete list_Strategies[i].Strategie;
                    }
                    //list_Strategies.remove(i);
                    return;
                }
            }
        }
	}
	public void iDel(){
		pageMsg = '';
		if(Index>=0)
        {
            for(Integer i = 0 ; i<  list_lineItem.size() ; i ++)
            {
                if(list_lineItem[i].Index == Index)
                {
                    if(list_lineItem[i].SalesOrder.id != null)
                    {
                        //list_lineItem[i].IsEdit = true;
                        delete list_lineItem[i].SalesOrder;
                    }
                    list_lineItem.remove(i);
                    break;
                }
            }
            initTotal();
        }
	}
	private void initTotal(){
		this.TotalQtyAmount = new Sales_Order_Request__c();
		this.TotalQtyAmount2 = new Sales_Order_Request__c();
		//this.TotalQty=0;
		this.TotalQtyAmount.Quantity__c=0;
		this.TotalQtyAmount.UtilField__c=0;
		this.TotalQtyAmount2.UtilField__c=0;
		//this.TotalAmount=0;
		if(list_lineItem != null && list_lineItem.size() > 0)
		for(LineItem li : list_lineItem){
			this.TotalQtyAmount.Quantity__c += (li.SalesOrder.Quantity__c==null?0:li.SalesOrder.Quantity__c);
			this.TotalQtyAmount.UtilField__c += (li.SalesOrder.Tax_Amount_CNY__c==null?0:li.SalesOrder.Tax_Amount_CNY__c);
			this.TotalQtyAmount2.UtilField__c += (li.SalesOrder.Total_Amount_whit_Tax_HK__c==null?0:li.SalesOrder.Total_Amount_whit_Tax_HK__c);
		}
	}
	public void iSave(){
		pageMsg = '';
		list<Sales_Order_Request__c> In_SalesOrder = new list<Sales_Order_Request__c>();
        list<Sales_Order_Request__c> Upd_SalesOrder = new list<Sales_Order_Request__c>();
        if(list_lineItem != null && list_lineItem.size() > 0)
        {
        	for(LineItem li : list_lineItem){
        		if(li.SalesOrder.Product_Article__c == null || 
        			li.SalesOrder.Quantity__c == null || 
        			li.SalesOrder.Unit_List_Price__c == null 
        			){
        			pageMsg = 'Please enter the required fields.';
        			return ;
        		}
        	}
            for(LineItem li : list_lineItem)
            {
                li.IsEdit = false;//You must enter a value
                if(li.SalesOrder.id != null)
                {
                    Upd_SalesOrder.add(li.SalesOrder);
                }
                else
                {
                    In_SalesOrder.add(li.SalesOrder);
                }
            }
        }
        if(In_SalesOrder.size() > 0)
        {
            insert In_SalesOrder;
        }
        if(Upd_SalesOrder.size() > 0)
        {
            update Upd_SalesOrder;
        }
        initSalesOrderList();
	}
	public void iArchive(){
		list<Sales_Order_Request__c> Upd_SalesOrder = new list<Sales_Order_Request__c>();
		if(list_lineItem != null && list_lineItem.size() > 0)
        {
            for(LineItem li : list_lineItem)
            {
                li.IsEdit = false;
                li.SalesOrder.Archived__c = true;
                li.SalesOrder.ArchivedDate__c = date.today();
                Upd_SalesOrder.add(li.SalesOrder);
            }
        }
        if(Upd_SalesOrder.size() > 0)
        {
            update Upd_SalesOrder;
            this.insertHis(Upd_SalesOrder);
        }
        initSalesOrderList();
        SendArchive();
	}
	private void insertHis(List<Sales_Order_Request__c> list_SalesOrder){
		List<Sales_Order_Request_History__c> list_soHis = new List<Sales_Order_Request_History__c>();
		List<Sales_Order_Request_History_Detail__c> list_soHisDetail = new List<Sales_Order_Request_History_Detail__c>();
		Set<String> set_Key = new Set<String>();
		for(Sales_Order_Request__c so : list_SalesOrder){
			String key = so.Account__c+''+so.Date__c.year()+so.Date__c.month()+so.Date__c.day();
			if(!set_Key.contains(key)){
				Sales_Order_Request_History__c soHis = new Sales_Order_Request_History__c();
				soHis.Unique_Key__c = key;
				soHis.Account__c = so.Account__c;
				soHis.Date__c = so.Date__c;
				soHis.ArchivedDate__c = so.ArchivedDate__c;
				list_soHis.add(soHis);
				set_Key.add(key);
			}
		}
		upsert list_soHis Unique_Key__c;
		for(Sales_Order_Request__c so : list_SalesOrder){
			String key = so.Account__c+''+so.Date__c.year()+so.Date__c.month()+so.Date__c.day();
			Sales_Order_Request_History_Detail__c soHisDetail = new Sales_Order_Request_History_Detail__c();
			soHisDetail.Comments__c = so.Comments__c;
			soHisDetail.Product_Article__c = so.Product_Article__c;
			soHisDetail.Customers_Drawing_Number__c = so.Customers_Drawing_Number__c;
			soHisDetail.Customer_Schedule_Agreement_Number__c= so.Customer_Schedule_Agreement_Number__c;
			soHisDetail.Description__c = so.Description__c;
			soHisDetail.Quantity__c = so.Quantity__c;
			soHisDetail.Unit_Price__c = so.Unit_List_Price__c;
			soHisDetail.Exchange_Rate__c = so.Exchange_Rate__c;
			soHisDetail.Sales_Order_Request_History__r = new Sales_Order_Request_History__c(Unique_Key__c = key);
			list_soHisDetail.add(soHisDetail);
		}
		insert list_soHisDetail;
	}
	public void copyLastSo(){
		pageMsg = '';
		if(list_HislineItem==null || list_HislineItem.size() ==0){
			pageMsg = 'Thiere is no so history.';
			return ;
		}
		List<Sales_Order_Request__c> list_LastSo = new List<Sales_Order_Request__c>();
		Date d = date.today();
		for(HisLineItem liHis : list_HislineItem){
			if(d >= Date.valueOf(liHis.SO_Date)){
				list_LastSo = liHis.list_detail;
			}
		}
		for(Sales_Order_Request__c so : list_LastSo){
			Sales_Order_Request__c soNew = new Sales_Order_Request__c();
			soNew.Date__c = date.today();
			soNew.Product_Article__c = so.Product_Article__c;
			soNew.Comments__c = so.Comments__c;
			soNew.Customer_Schedule_Agreement_Number__c = so.Customer_Schedule_Agreement_Number__c;
			soNew.Customers_Drawing_Number__c = so.Customers_Drawing_Number__c;
			soNew.Description__c = so.Description__c;
			soNew.Account__c = so.Account__c;
			soNew.Qty_Box__c = so.Qty_Box__c;
			soNew.Quantity__c = so.Quantity__c;
			soNew.Unit_List_Price__c = so.Unit_List_Price__c;
			list_lineItem.add(new LineItem(true,list_lineItem.size(),soNew));
		}
	}
	private void SendRequestMail(){
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		List<String> list_address = new List<String>();
		list_address.addAll(list_email);
		mail.setToAddresses(list_address);
		system.debug('mail address:'+list_address);
		//mail.setReplyTo('lyxpxf@126.com');
		//mail.setSenderDisplayName('Salesforce System');
		mail.setOrgWideEmailAddressId([Select o.IsAllowAllProfiles, o.Id, o.DisplayName, o.Address From OrgWideEmailAddress o where DisplayName = 'Jebsen Salesforce'].Id);
		//mail.setSubject('Sales Order Request');
		EmailTemplate et = [SELECT id FROM EmailTemplate WHERE DeveloperName = 'Confirm_Sales_Order_Request'];
		mail.setTemplateId(et.id);
		system.debug('target users:'+list_Shipper);
		if(this.list_Shipper.size() > 0)
		mail.setTargetObjectId(list_Shipper[0].Id);
		//system.debug('target user!:'+list_Shipper[0].Id);
		mail.setWhatId(accId);
		mail.setSaveAsActivity(false);
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
	private void SendArchive(){
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		//mail.setToAddresses(list_address);
		//mail.setSenderDisplayName('Salesforce.com');
		mail.setOrgWideEmailAddressId([Select o.IsAllowAllProfiles, o.Id, o.DisplayName, o.Address From OrgWideEmailAddress o where DisplayName = 'Jebsen Salesforce'].Id);
		mail.setSubject('Sales Order Archived');
		//EmailTemplate et = [SELECT id FROM EmailTemplate WHERE DeveloperName = 'Sales_Order_Archived'];
		//mail.setTemplateId(et.id);
		String sBody = 'Dear, <br/>';
		sBody += 'Your sales order ';
		sBody += acc.Name;
		sBody += ' is being processed and archived. <br/>';
		sBody += 'You will be informed when order is delivered.<br/><br/>';
		sBody += 'Thanks,<br/><br/>';
		sBody += 'Salesforce.com';
		//mail.setPlainTextBody(sBody);
		mail.setHtmlBody(sBody);
		mail.setTargetObjectId(acc.OwnerId);
		//mail.setWhatId(accId);
		mail.setSaveAsActivity(false);
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
	public void iRequest(){
		pageMsg = '';
		list<Sales_Order_Request__c> Upd_SalesOrder = new list<Sales_Order_Request__c>();
		if(list_lineItem != null && list_lineItem.size() > 0)
        {
            for(LineItem li : list_lineItem)
            {
            	if(li.IsEdit){
            		pageMsg = 'Please save first.';
            		return;
            	}
                li.IsEdit = false;
                li.SalesOrder.Submitted__c = true;
                Upd_SalesOrder.add(li.SalesOrder);
            }
        }
        if(Upd_SalesOrder.size() > 0)
        {
            update Upd_SalesOrder;
        }
        initSalesOrderList();
        SendRequestMail();
	}
	private void initSalesOrderList(){
		list_lineItem = new list<LineItem>();
		for(Sales_Order_Request__c so : [Select s.Submitted__c, s.Archived__c, s.Account__c, s.Unit_List_Price__c, s.Total_Amount_whit_Tax_HK__c, 
				s.Tax_Amount_CNY__c, s.Quantity__c, s.Qty_Box__c, s.Product_Article__c, s.Name, s.Id, s.Description__c, s.Date__c, 
				s.Customers_Drawing_Number__c, s.Customer_Schedule_Agreement_Number__c, s.Comments__c, s.ArchivedDate__c, s.Exchange_Rate__c 
				From Sales_Order_Request__c s Where Account__c =: accId order by createddate]){
			if(!so.Archived__c){
				list_lineItem.add(new LineItem(false,list_lineItem.size(),so));
			}else{
				list_hisData.add(so);
			}
		}
		initTotal();
		initShowControl();
		initHisList();
	}
	private void initHisList(){
		list_HislineItem = new List<HisLineItem>();
		Map<Date , List<Sales_Order_Request__c>> map_HisSO = new Map<Date , List<Sales_Order_Request__c>>();
		system.debug(list_hisData.size());
		for(Sales_Order_Request__c so : this.list_hisData){
			List<Sales_Order_Request__c> list_so = new List<Sales_Order_Request__c>();
			if(map_HisSO.containsKey(so.Date__c)){
				list_so = map_HisSO.get(so.Date__c);
			}
			list_so.add(so);
			map_HisSO.put(so.Date__c , list_so);
		}
		this.initHisLineItem(map_HisSO);
	}
	
	private void initHisLineItem(Map<Date , List<Sales_Order_Request__c>> map_HisSO){
		for(Date d : map_HisSO.keySet()){
			HisLineItem hisItem = new HisLineItem();
			hisItem.list_detail = map_HisSO.get(d);
			hisItem.SO_Date = d.year()+'-'+d.month()+'-'+d.day();
			hisItem.Total_Amount = 0 ;
			hisItem.DetailLineId = 'DetailLine'+list_HislineItem.size();
			for(Sales_Order_Request__c so : hisItem.list_detail){
				if(so.ArchivedDate__c != null)
				hisItem.Archived_Date = so.ArchivedDate__c.year()+'-'+so.ArchivedDate__c.month()+'-'+so.ArchivedDate__c.day();
				hisItem.Total_Amount += so.Tax_Amount_CNY__c ;
			}
			list_HislineItem.add(hisItem);
		}
	}
	
	private void initShowControl(){
		Boolean Submitted = false;
		
		for(MailBox__c mailBox : MailBox__c.getAll().values()){
			if(mailBox.Name.startsWith('Processed&Archive')){
				this.list_email.add(mailBox.Email__c);
			}
		}
		system.debug('mail box :'+this.list_email);
		Boolean IsShipper =false;
		list_Shipper = [Select Id,Email From User Where Email in: this.list_email];
		for(User u : list_Shipper){
			if(u.Id == UserInfo.getUserId())
			IsShipper = true;
			list_email.remove(u.Email);
		}
		acc = [Select Id,OwnerId,Name,KAM_Profil__c From Account Where Id =: accId];
		Boolean IsOwner = acc.KAM_Profil__c==UserInfo.getUserId()?true:false;
		if(list_lineItem.size() >0 && list_lineItem[0].SalesOrder.Submitted__c){
			Submitted = true;
		}
		this.ShowEdit = IsOwner?true:false;
		this.ShowEdit = ShowEdit&&(Submitted?false:true);
		this.ShowArchive = (IsShipper&&Submitted)?true:false;
	}
	
	public class LineItem{
		public Boolean IsEdit{get;set;}
        public Integer Index{get;set;}
        public Sales_Order_Request__c SalesOrder{get;set;}
        public Decimal TaxAmount{get{Decimal d = SalesOrder.Tax_Amount_CNY__c;return d;}set;}
        public Decimal UnitPrice{get{Decimal d = SalesOrder.Unit_List_Price__c;return d;}set;}
        public LineItem(Boolean e,Integer i,Sales_Order_Request__c so){
        	IsEdit = e ;
        	Index = i;
        	SalesOrder = so;
        }
	}
	public class HisLineItem{
		public String SO_Date{get;set;}
		public String Archived_Date{get;set;}
		public Double Total_Amount{get;set;}
		public String DetailLineId{get;set;}
		public List<Sales_Order_Request__c> list_detail{get;set;}
	}
}