/**
 * 作者：Ziyue
 * 时间：2013-9-27
 * 功能：
 */
@isTest
private class Test_Sweet_Point_Exp_The_Value_HyCont 
{
    static testMethod void myUnitTest() 
    {
        Account acc = new Account();
        acc.Name = 'acc';
        insert acc;
        
        Account_Team__c team = new Account_Team__c();
        team.Account__c = acc.id;
        team.Team_Member__c = UserInfo.getUserId();
        insert team;
        
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        Sweet_Point_Exp_The_Value_HyController hy = new Sweet_Point_Exp_The_Value_HyController(controller);
        hy.Upd();
        hy.Hold();
        hy.Approve();
        hy.Retake();
        
        /*Sweet_Point_Exp_The_Value_HyController hy1 = new Sweet_Point_Exp_The_Value_HyController(controller);
        hy1.Upd();
        hy1.Hold();
        hy1.Approve();
        hy1.Retake();
    	*/
    }
}