/**
 * Test SalesOrderRequestController
 */
@isTest (SeeAllData=true)
private class Test_SalesOrderRequestController {

    static testMethod void myUnitTest() {
        Opportunity opp = new Opportunity();
		opp.Name = 'Test opp';
		opp.StageName = 'test';
		opp.CloseDate = date.today().addMonths(2);
		opp.Purchase_scope__c = 'Fastener Parts;Mold';
		insert opp;
		Account acc = new Account();
        acc.Name = 'acc';
        insert acc;
		
		system.Test.startTest();
		Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(acc);
		SalesOrderRequestAccountController sorc = new SalesOrderRequestAccountController(controller);
		sorc.iAdd();
		sorc.list_lineItem[0].SalesOrder.Product_Article__c = 'test';
		sorc.list_lineItem[0].SalesOrder.Quantity__c = 234;
		sorc.list_lineItem[0].SalesOrder.Unit_List_Price__c = 12;
		sorc.list_lineItem[0].SalesOrder.Date__c = date.today();
		sorc.iSave();
		sorc.Index = 0 ;
		sorc.iEdit();
		sorc.list_lineItem[0].SalesOrder.Unit_List_Price__c = 12.3;
		sorc.iSave();
		sorc.iRequest();
		sorc.iArchive();
		sorc.copyLastSo();
		
		Apexpages.Standardcontroller controller2 = new Apexpages.Standardcontroller(acc);
		SalesOrderRequestHisAccountController sorhc = new SalesOrderRequestHisAccountController(controller2);
		
		
		sorc.iDel();
		Sales_Order_Request__c sor = new Sales_Order_Request__c();
		
		system.Test.stopTest();
    }
}