/*
 * 作者：Ziyue
 * 时间：2013-9-17
 * 功能：价值假设工具
*/
public class Sweet_Point_Exp_The_Value_HyController 
{
    public list<SPE> list_spe{get;set;}
    public Boolean IsRead{get;set;}
    public Boolean IsAccess{get;set;}//控制Approve和Retake按钮的可见性
    public PubMethod pub{get;set;}
    public ID AccId;
    public Boolean IsEdit{get;set;}//控制 Save和Edit按钮的可见性
    public string IsShare{get;set;}
    public string AccoutOwnId;
    public list<Sweet_Point_Explore__c> list_insertSw;
    private String sUserType ;//TCG,ACD,Manager,登录用户类型，1.TCG销售、2.ACD销售、3.GM/Manager/Admin
    private ApexPages.StandardController controller;
    public String SelectedTeamType{get;set;}
    public Boolean ShowSelect{get;set;}
    public list<SelectOption> TeamTypes{
        get{
            list<SelectOption> options = new list<SelectOption>();
            options.add(new SelectOption('Profil','Profil'));
            options.add(new SelectOption('Jebsen-TCG','Jebsen-TCG'));
            return options;
        }
        private set;
    }
    public Sweet_Point_Exp_The_Value_HyController(ApexPages.StandardController controller)
    {
        this.controller = controller;
        this.initCurrentUser();
        pub = new PubMethod();
        list_insertSw = new list<Sweet_Point_Explore__c>();
        
        AccId = controller.getId();
        
            Account acc = [select id,OwnerId,Owner.UserRoleId,KAM_Jebsen_TCG__c,KAM_Profil__c from Account where id =: AccId];
        AccoutOwnId = Acc.OwnerId;
        
        set<ID> HighRole = pub.getHighRoleName(acc.Owner.UserRoleId);
        if(HighRole.contains(UserInfo.getUserRoleId()) && acc.OwnerId != UserInfo.getUserId())
        {
            IsAccess = true;
        }
        else
        {
            IsAccess = false;
        }
        
        //if(UserInfo.getUserId() == acc.OwnerId)
        if(acc.KAM_Profil__c == UserInfo.getUserId() || acc.KAM_Jebsen_TCG__c == UserInfo.getUserId() )
        {
            IsEdit = true;
        }
        else
        {
            IsEdit = false;
        }
        IsRead = true;
        this.ChangeType();
    }
    public void ChangeType(){
        this.searchRecords(SelectedTeamType);
    }
    private void searchRecords(String sType){
        String sTeamType ;
        if(sUserType == 'ACD'){
            sTeamType = 'Profil';
        }else if(sUserType == 'TCG'){
            sTeamType = 'Jebsen-TCG';
        }else if(sType != null){
            sTeamType = sType;
        }

        list_spe = new list<SPE>();
        list<Sweet_Point_Explore__c> list_spes = [Select IsShare__c,OwnerId,Related_Account__c, RecordTypeId, Name, Key_Success_Factors_for_sales_Marketing__c, 
                    Key_Success_Factors_for_Production__c, Key_Factors_for_R_D__c, Key_Factors_for_Purchasing__c, 
                    Key_Factors_for_Maintenance__c, Key_Factors_for_IT_Engineering_Operation__c, 
                    Key_Factors_for_Finance__c, Jebsen_s_Key_Advantage__c, Id, Team_Type__c 
                    From Sweet_Point_Explore__c where Related_Account__c=:controller.getId() 
                    and RecordType.DeveloperName='The_Value_Hypothesis'
                    and Team_Type__c =: sTeamType];
        //Jebsen_s_Key_Advantage__c:Sweet_Point_Explore__c记录
        map<string,Sweet_Point_Explore__c> map_spe = new map<string,Sweet_Point_Explore__c>();
        if(list_spes != null && list_spes.size() > 0)
        {
            if(list_spes[0].IsShare__c)
            {
                IsShare = 'Is Shared';
            }
            else
            {
                IsShare = 'Is Retake';
            }
            for(Sweet_Point_Explore__c spe : list_spes)
            {
                if(!map_spe.containsKey(spe.Jebsen_s_Key_Advantage__c))
                {
                    map_spe.put(spe.Jebsen_s_Key_Advantage__c,spe);
                }
            }
        }
        else
        {
            IsShare = 'None';
        }
        RecordType rt =[select id from RecordType where SobjectType='Sweet_Point_Explore__c' and DeveloperName='The_Value_Hypothesis']; 
        Schema.DescribeFieldResult F = Sweet_Point_Explore__c.Jebsen_s_Key_Advantage__c.getDescribe();
        
        for(Schema.PicklistEntry e:F.getPicklistValues())
        {
            SPE sp = new SPE();
            Sweet_Point_Explore__c spe;
            if(map_spe.containsKey(e.getValue()))
            {
                spe = map_spe.get(e.getValue());
            }
            else
            {
                
                spe = new Sweet_Point_Explore__c();
                spe.Team_Type__c = sTeamType;
                spe.RecordTypeId = rt.id;
                spe.Jebsen_s_Key_Advantage__c = e.getValue();
                if(e.getValue() == 'Company')
                {
                    spe.Key_Factors_for_Purchasing__c = 'Offer high quality goods';//Purchasing
                    spe.Key_Factors_for_R_D__c = 'Profil supports professionally';//R&D
                    spe.Key_Factors_for_IT_Engineering_Operation__c = '';//Engineering / Operations
                    spe.Key_Success_Factors_for_Production__c = '';//Production
                    spe.Key_Factors_for_Finance__c = 'More than 100 years company, offer payment term';//Finance
                    spe.Key_Factors_for_Maintenance__c = '';//Maintenance
                    spe.Key_Success_Factors_for_sales_Marketing__c = '';//Sales/Marketing
                }
                else if(e.getValue() == 'Products/Service')
                {
                    spe.Key_Factors_for_Purchasing__c = 'Local service';
                    spe.Key_Factors_for_R_D__c = '';
                    spe.Key_Factors_for_IT_Engineering_Operation__c = 'Logistic Hub in Shanghai';
                    spe.Key_Success_Factors_for_Production__c = 'Local service';
                    spe.Key_Factors_for_Finance__c = 'RMB transaction';
                    spe.Key_Factors_for_Maintenance__c = 'Good quality goods';
                    spe.Key_Success_Factors_for_sales_Marketing__c = '';
                }
                else if(e.getValue() == 'People')
                {
                    spe.Key_Factors_for_Purchasing__c = 'Local support';
                    spe.Key_Factors_for_R_D__c = '';
                    spe.Key_Factors_for_IT_Engineering_Operation__c = 'Local support';
                    spe.Key_Success_Factors_for_Production__c = 'Local support';
                    spe.Key_Factors_for_Finance__c = '';
                    spe.Key_Factors_for_Maintenance__c = '';
                    spe.Key_Success_Factors_for_sales_Marketing__c = '';
                }
                else if(e.getValue() == 'Others')
                {
                    spe.Key_Factors_for_Purchasing__c = '';
                    spe.Key_Factors_for_R_D__c = '';
                    spe.Key_Factors_for_IT_Engineering_Operation__c = '';
                    spe.Key_Success_Factors_for_Production__c = '';
                    spe.Key_Factors_for_Finance__c = '';
                    spe.Key_Factors_for_Maintenance__c = '';
                    spe.Key_Success_Factors_for_sales_Marketing__c = '';
                }
                else
                {
                    spe.Key_Factors_for_Purchasing__c = '';
                    spe.Key_Factors_for_R_D__c = '';
                    spe.Key_Factors_for_IT_Engineering_Operation__c = '';
                    spe.Key_Success_Factors_for_Production__c = '';
                    spe.Key_Factors_for_Finance__c = '';
                    spe.Key_Factors_for_Maintenance__c = '';
                    spe.Key_Success_Factors_for_sales_Marketing__c = '';
                }
                //TCG不需要默认值
                if(sTeamType == 'Jebsen-TCG'){
                    spe.Key_Factors_for_Purchasing__c = '';
                    spe.Key_Factors_for_R_D__c = '';
                    spe.Key_Factors_for_IT_Engineering_Operation__c = '';
                    spe.Key_Success_Factors_for_Production__c = '';
                    spe.Key_Factors_for_Finance__c = '';
                    spe.Key_Factors_for_Maintenance__c = '';
                    spe.Key_Success_Factors_for_sales_Marketing__c = '';
                }
            
                spe.Related_Account__c = AccId;
                list_insertSw.add(spe);
                //spe.Jebsen_s_Key_Advantage__c = '1234555';
                
            }
            
            
            spe.Related_Account__c = AccId;
            sp.Jebsen_s_Key_Advantage = e.getValue();
            sp.sweet = spe;
            list_spe.add(sp);
        }
    }
    private void initCurrentUser(){
        this.ShowSelect = false;
        this.sUserType = 'ACD';
        User u = [Select Id,UserRole.Name,Profile.Name From User Where Id =: Userinfo.getUserId()];
        String sUserRoleName = u.UserRole.Name;
        if(sUserRoleName=='Jebsen TCG Sales' || sUserRoleName=='Jebsen TCG Manager'){
            this.sUserType = 'TCG';
        }
        if(sUserRoleName=='ACD Sales'){
            this.sUserType = 'ACD';
        }
        if(sUserRoleName=='ACD Sales Manager' || sUserRoleName=='JAT General Manager' || u.Profile.Name == 'System Administrator'){
            this.sUserType = 'Manager';
            this.SelectedTeamType = 'Profil';
            this.ShowSelect = true;
        }
    }
    public void Hold()
    {
        IsRead = true;
        list<Sweet_Point_Explore__c> upd_spe = new list<Sweet_Point_Explore__c>();
        list<Sweet_Point_Explore__c> ind_spe = new list<Sweet_Point_Explore__c>();
        for(SPE sp:list_spe)
        {
            if(sp.sweet.id == null)
            {
                ind_spe.add(sp.sweet);
            }
            else
            {
                upd_spe.add(sp.sweet);
            }
        }
        if(upd_spe.size() > 0)
        {
            update upd_spe;
        }
        if(ind_spe.size() > 0)
        {
            insert ind_spe;
        }
        set<ID> speIds = new set<ID>();
        for(Sweet_Point_Explore__c sp:ind_spe)
        {
            speIds.add(sp.id);
        }
        map<ID,Sweet_Point_Explore__c> map_spe = new map<ID,Sweet_Point_Explore__c>([Select IsShare__c,OwnerId,Related_Account__c, RecordTypeId, Name, Key_Success_Factors_for_sales_Marketing__c, 
                                                Key_Success_Factors_for_Production__c, Key_Factors_for_R_D__c, Key_Factors_for_Purchasing__c, 
                                                Key_Factors_for_Maintenance__c, Key_Factors_for_IT_Engineering_Operation__c, 
                                                Key_Factors_for_Finance__c, Jebsen_s_Key_Advantage__c, Id 
                                                From Sweet_Point_Explore__c where id in:speIds]);
        for(SPE sp:list_spe)
        {
            if(map_spe.containsKey(sp.sweet.id))
            {
                sp.sweet = map_spe.get(sp.sweet.id);
            }
        }            
    }
    public void Upd()
    {
        IsRead = false;
    }
    /*
    点击Approve按钮，The value hypthesis部分的所有内容都将共享给account team内的成员以只读的权限。
    此时状态变更为is shared
    */
    public void Approve()
    {//Select s.UserOrGroupId, s.RowCause, s.ParentId, s.AccessLevel From Sweet_Point_Explore__Share s
        if(list_insertSw.size() > 0)
        {
            list<Sweet_Point_Explore__c> list_inse = new list<Sweet_Point_Explore__c>();
            list<Sweet_Point_Explore__c> list_upse = new list<Sweet_Point_Explore__c>();
            for(Sweet_Point_Explore__c Sw : list_insertSw)
            {
                Sw.OwnerId = AccoutOwnId;
                if(Sw.Id == null)
                {
                    list_inse.add(Sw);
                }
                else
                {
                    list_upse.add(Sw);
                }
                
                //list_inse.add(Sw);
            }
            if(list_inse.size() > 0)
            {
                insert list_inse;   
            }
            if(list_upse.size() > 0)
            {
                update list_upse;
            }
            list_insertSw.clear();
        }
        list<Sweet_Point_Explore__c> upd_spe = new list<Sweet_Point_Explore__c>();
        Set<ID> set_speID = new Set<ID>();
        list<Sweet_Point_Explore__Share> list_Share = new list<Sweet_Point_Explore__Share>();
        //for(Account_Team__c team : [select id,Team_Member__c,Account__c from Account_Team__c where Account__c =: AccId])
        for(User u :[select id from User where UserRole.Name ='Profil Manager' or UserRole.Name ='Profil Engineer'])
        {
            //if(team.Team_Member__c != null)
            //{
                for(SPE sp:list_spe)
                {
                    sp.sweet.IsShare__c = true;
                    if(sp.sweet.id == null)
                    {
                        continue;
                    }
                    if(!set_speID.contains(sp.sweet.id)){
                        system.debug('sharing??:'+sp.sweet);
                        upd_spe.add(sp.sweet);
                        set_speID.add(sp.sweet.id);
                    }
                    if(sp.sweet.OwnerId != u.id)
                    {
                        Sweet_Point_Explore__Share speShare = new Sweet_Point_Explore__Share();
                        speShare.ParentId = sp.sweet.id;
                        speShare.AccessLevel = 'Read';
                        speShare.UserOrGroupId = u.id;//team.Team_Member__c;
                        list_Share.add(speShare);
                    }
                }
            //}
        }
        if(list_Share.size() > 0)
        {
            insert list_Share;
        }
        if(upd_spe.size() > 0)
        {
        system.debug('sharing??:'+upd_spe);
            update upd_spe;
        }
        IsShare = 'Is Shared';
    }
    /*
    点击retake按钮，The value hypthesis部分的所有内容都将只共享给account owner 和其直属上级（含系统管理员）。此时状态变更为is retaked
    */
    public void Retake()
    {
        if(list_insertSw.size() > 0)
        {
            list<Sweet_Point_Explore__c> list_inse = new list<Sweet_Point_Explore__c>();
            for(Sweet_Point_Explore__c Sw : list_insertSw)
            {
                Sw.OwnerId = AccoutOwnId;
                list_inse.add(Sw);
            }
            insert list_inse;
            list_insertSw.clear();
        }
        set<ID> set_MemberIds = new set<ID>();
        set<ID> set_speIds = new set<ID>();
        Set<ID> set_speID = new Set<ID>();
        list<Sweet_Point_Explore__c> upd_spe = new list<Sweet_Point_Explore__c>();
        //for(Account_Team__c team : [select id,Team_Member__c,Account__c from Account_Team__c where Account__c =: AccId])
        for(User u :[select id from User where UserRole.Name ='Profil Manager' or UserRole.Name ='Profil Engineer'])
        {
            for(SPE sp:list_spe)
            {
                sp.sweet.IsShare__c = false;
                if(sp.sweet.id == null)
                {
                    continue;
                }
                if(!set_speID.contains(sp.sweet.id)){
                    system.debug('sharing??:'+sp.sweet);
                    upd_spe.add(sp.sweet);
                    set_speID.add(sp.sweet.id);
                }
                //upd_spe.add(sp.sweet);
                if(sp.sweet.OwnerId != u.id)
                {
                    set_MemberIds.add(u.id);
                    set_speIds.add(sp.sweet.id);
                }
            }
        }
        if(set_MemberIds != null && set_speIds != null)
        {
            list<Sweet_Point_Explore__Share> list_Share = [select id from Sweet_Point_Explore__Share 
                                                           where UserOrGroupId in:set_MemberIds and ParentId in: set_speIds];
            if(list_Share != null && list_Share.size() > 0)
            {
                delete list_Share;
            }
        }
        if(upd_spe.size() > 0)
        {
            update upd_spe;
        }
        IsShare = 'Is Retake';
    }
    public class SPE
    {
        public string Jebsen_s_Key_Advantage{get;set;}
        public Sweet_Point_Explore__c sweet{get;set;}
    }
}