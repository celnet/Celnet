/*
 * 作者：Ziyue
 * 时间：2013-10-11
 * 功能：上传文件
*/
public class Achievement_AttachmentUploadController 
{
    public Attachment document {get; set;}
    public string Type{get;set;}
    public list<Document_upload__c> list_docUp{get;set;}
    public ID oppId{get;set;}
    public ID DelId{get;set;}
    public list<string> DocTypes{get;set;}
    public string url{get;set;}
    public string status{get;set;}
    public Opportunity opp{get;set;}
  /*public list<SelectOption> getTypes()
    {
        
        list<SelectOption> options = new list<SelectOption>();
        options.add(new SelectOption('Automated solution','Automated solution'));
        options.add(new SelectOption('Frame contract','Frame contract'));
        
        options.add(new SelectOption('Overall solution','Overall solution'));
        options.add(new SelectOption('PO to Profil','PO to Profil'));
        
        options.add(new SelectOption('Questionnaire','Questionnaire'));
        options.add(new SelectOption('Quotation to customer','Quotation to customer'));
        
        options.add(new SelectOption('Quotation to Jebsen','Quotation to Jebsen'));
        options.add(new SelectOption('RFQ','RFQ'));
        
        options.add(new SelectOption('Samples demand plan','Samples demand plan'));
        options.add(new SelectOption('Samples Purchase Order','Samples Purchase Order'));
        return options;
    }*/
    public Boolean IsSamples_demand_plan{get;set;}
    public Boolean IsSamples_Purchase_Order{get;set;}
    public Boolean IsFrame_contract{get;set;}
    public Boolean IsPPAP_Documents{get;set;}
    public Boolean IsPO_to_Profil{get;set;}
    public Boolean IsEdit{get;set;}
    public Achievement_AttachmentUploadController(ApexPages.StandardController controller)
    {//Achievement_Requred__c
        IsEdit = PubMethod.IsEdit(UserInfo.getUserRoleId());
        Schema.DescribeFieldResult F = Opportunity.Achievement_Requred__c.getDescribe();
        set<string> options = new set<string>();
        for(Schema.Picklistentry p : F.getPicklistValues())
        {
            if(!p.isActive())
            {
                continue;
            }
            options.add(p.getValue());
        }
        IsSamples_demand_plan =  options.contains('Samples demand plan')?true:false;
        IsSamples_Purchase_Order =  options.contains('Samples Purchase Order')?true:false;
        IsFrame_contract =  options.contains('Frame contract')?true:false;
        IsPPAP_Documents =  options.contains('PPAP Documents')?true:false;
        IsPO_to_Profil =  options.contains('PO to Profil')?true:false;
        DocTypes = new list<string>();
        DocTypes.add('Samples demand plan');
        DocTypes.add('Samples Purchase Order');
        DocTypes.add('Frame contract');
        DocTypes.add('PPAP Documents');
        DocTypes.add('PO to Profil');
        document = new Attachment();
        oppId = controller.getId();
        opp = [select PO_to_Profil_has_been_uploaded__c,Frame_contract_has_been_uploaded__c,PPAP_Documents_has_been_uploaded__c,
              Samples_demand_plan_has_been_uploaded__c,Samples_Purchase_Order_has_been_uploaded__c 
              from Opportunity where id=:oppId];
        url = '/'+oppId;
        SelectDoc();
        if(list_docUp == null)  
        {
            list_docUp = new list<Document_upload__c>();
        }           
    }
    public void SelectDoc()
    {
        list_docUp = [select CreatedDate,CreatedBy.Name,Opportunity__c,id,DocumentID__c,Document_Name__c,
                     Document_Type__c,Name__c from Document_upload__c 
                     where Opportunity__c=:oppId and Document_Type__c in: DocTypes];
    }
    public void doSave() 
    {
        Document_upload__c doc_Up = new Document_upload__c();
        doc_Up.Opportunity__c = oppId;
        doc_Up.Document_Name__c = document.Name;
        doc_Up.Document_Type__c = Type;
        insert doc_Up;
        document.ParentId = doc_Up.id;
        insert document;
        doc_Up.DocumentID__c = document.Id;
        update doc_Up;
        doc_Up = [select CreatedDate,CreatedBy.Name,Opportunity__c,id,DocumentID__c,Document_Name__c,
                 Document_Type__c,Name__c from Document_upload__c where id=:doc_Up.id];
        list_docUp.add(doc_Up);
        document = new Attachment();
        status = 'success';
        /*
        DocTypes = new list<string>();
        DocTypes.add('Samples demand plan');
        DocTypes.add('Samples Purchase Order');
        DocTypes.add('Frame contract');
        DocTypes.add('PO to Profil');
        document = new Attachment();
        oppId = controller.getId();
        opp = [select PO_to_Profil_has_been_uploaded__c,Frame_contract_has_been_uploaded__c,PPAP_Documents_has_been_uploaded__c,
              Samples_demand_plan_has_been_uploaded__c,Samples_Purchase_Order_has_been_uploaded__c 
              from Opportunity where id=:oppId];
        */
        if(Type == 'Samples demand plan')
        {
            opp.Samples_demand_plan_has_been_uploaded__c = true;
        }
        else if(Type == 'Samples Purchase Order')
        {
            opp.Samples_Purchase_Order_has_been_uploaded__c = true;
        }
        else if(Type == 'Frame contract')
        {
            opp.Frame_contract_has_been_uploaded__c = true;
        }
        else if(Type == 'PPAP Documents')
        {
            opp.PPAP_Documents_has_been_uploaded__c = true;
        }
        else if(Type == 'PO to Profil')
        {
            opp.PO_to_Profil_has_been_uploaded__c = true;
        }
        update opp;
    }
    //删除页面中点击删除的那一行记录
    public void Del()
    {
        //Document_upload__c Del_Doc = [select id from Document_upload__c where id=:DelId];
        Attachment attach = [select id from Attachment where ParentId=:DelId];
        delete attach;
        //delete Del_Doc;
        for(Document_upload__c doc : list_docUp)
        {
            if(doc.id == DelId)
            {
                delete doc;
            }
        }
        SelectDoc();
        status = 'success';
        opp = [select PO_to_Profil_has_been_uploaded__c,Frame_contract_has_been_uploaded__c,PPAP_Documents_has_been_uploaded__c,
              Samples_demand_plan_has_been_uploaded__c,Samples_Purchase_Order_has_been_uploaded__c 
              from Opportunity where id=:oppId];
    }
}