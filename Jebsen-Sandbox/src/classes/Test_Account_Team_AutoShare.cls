/**
 * 作者：Ziyue
 * 时间：2013-10-18
 * 功能：trigger.Test_Account_Team_AutoShare测试类
 */
@isTest
private class Test_Account_Team_AutoShare 
{
    static testMethod void myUnitTest() 
    {
    	//公开课销售简档
        list<Profile> PublicPro = [select Id from Profile where Name='系统管理员' or Name='System Administrator' limit 1];
        list<User> user = [select LanguageLocaleKey,Alias,TimeZoneSidKey,LocaleSidKey,EmailEncodingKey from User where IsActive=true limit 1];
        User TUser = new User();
        TUser.Username='RepSu@123.com';
        TUser.LastName='RepSu';
        TUser.Email='RepSu@123.com';
        TUser.Alias=user[0].Alias;
        TUser.TimeZoneSidKey=user[0].TimeZoneSidKey;
        if(PublicPro!=null&&PublicPro.size()>0)
        {
            TUser.ProfileId=PublicPro[0].Id;
        }
        else
        {
            TUser.ProfileId=UserInfo.getProfileId();
        }
        
        TUser.LocaleSidKey=user[0].LocaleSidKey;
        TUser.LanguageLocaleKey=user[0].LanguageLocaleKey;
        TUser.EmailEncodingKey=user[0].EmailEncodingKey;
        TUser.CommunityNickname='RepSu';
        TUser.MobilePhone='12345678912';
        //PublicUser.UserRoleId = RepUserRole.Id ;
        TUser.IsActive = true;
        insert TUser;
        
        Account acc = new Account();
        acc.Name = 'acc';
        insert acc;
        Contact cont = new Contact();
        cont.FirstName = 'ct';
        cont.LastName = 'ont';
        cont.AccountId = acc.id;
        Opportunity opp = new Opportunity();
        opp.Name = 'opp';
        opp.AccountId = acc.id;
        opp.CloseDate = Date.today().addMonths(2);
        opp.StageName = 'AOA';
        insert opp;
        Account_Team__c team = new Account_Team__c();
        team.Account__c = acc.id;
        team.Account_Access__c = 'Read/Write';
        team.Contact_Access__c = 'Read/Write (all Contact)';
        team.Opportunity_Access__c = 'Read/Write (all opportunity)';
        team.Team_Member__c = TUser.id;
        team.Team_Role__c = 'ACT Commissioner';
        insert team;
        
        Account_Team__c team1 = new Account_Team__c();
        team1.Account__c = acc.id;
        team1.Account_Access__c = 'Read Only';
        team1.Contact_Access__c = 'Read Only (all Contact)';
        team1.Opportunity_Access__c = 'Read Only (all opportunity)';
        team1.Team_Member__c = TUser.id;
        team1.Team_Role__c = 'ACT Commissioner';
        insert team1;
        
        Account_Team__c team2 = new Account_Team__c();
        team2.Account__c = acc.id;
        team2.Account_Access__c = 'Read Only';
        team2.Contact_Access__c = 'Read Only (related Contact)';
        team2.Opportunity_Access__c = 'Read Only (related opportunity)';
        team2.Team_Member__c = TUser.id;
        team2.Team_Role__c = 'ACT Commissioner';
        insert team2;
        delete team2;
        
        
        
    }
}