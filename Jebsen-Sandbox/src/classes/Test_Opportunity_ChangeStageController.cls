/**
 * 作者：Ziyue
 * 时间：2013-11-1
 * 功能：class.Opportunity_ChangeStageController测试类
 */
@isTest
private class Test_Opportunity_ChangeStageController 
{
    static testMethod void myUnitTest() 
    {
        Opportunity opp = new Opportunity();
        opp.Name = 'opp';
        opp.CloseDate = Date.today();
        opp.StageName = 'Awareness of Needs';
        opp.Probability = 10;
        insert opp;
        ApexPages.currentPage().getParameters().put('oppId',opp.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(opp);
        Opportunity_ChangeStageController changeStage = new Opportunity_ChangeStageController(controller);
        changeStage.StartProbability='10';
        changeStage.EndProbability='20';
        changeStage.Hold();
        changeStage.opp.Probability = 30;
        changeStage.Hold();
        changeStage.opp.StageName = 'Alleviation of Risk';
        changeStage.Hold();
        
        
        Opportunity_SubmitApprovalController submitContr = new Opportunity_SubmitApprovalController(controller);
        submitContr.Submit();
    }
}