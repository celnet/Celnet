/**
 * 作者：Ziyue
 * 时间：2013-10-22
 * 描述：class.New_EscalationController测试类
 */
@isTest
private class Test_New_EscalationController 
{
    static testMethod void myUnitTest() 
    {
    	Opportunity opp = new Opportunity();
    	opp.Name = 'opp';
    	opp.CloseDate = Date.today();
    	opp.StageName = 'AOA';
    	insert opp;
        ApexPages.currentPage().getParameters().put('oppId',opp.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(opp);
        New_EscalationController escalation = new New_EscalationController(controller);
        escalation.Escalation.Deadline__c = date.today();
        escalation.Hold();
    }
}