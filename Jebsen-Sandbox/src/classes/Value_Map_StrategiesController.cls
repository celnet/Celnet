/*
 * 作者：Ziyue
 * 时间：2013-9-25
 * 功能：业务机会页面上的AOA模块页面
*/
public class Value_Map_StrategiesController 
{
    public class Strategies
    {
        public Boolean IsEdit{get;set;}
        public Integer Index{get;set;}
        public Value_Map_Strategies__c Strategie{get;set;}
    }
    public list<Strategies> list_Strategies{get;set;}
    public ID OppId{get;set;}
    public Integer Index{get;set;}//删除记录的下标
    public Boolean IsAccess{get;set;}//控制Approve和Retake按钮的可见性
	public PubMethod pub{get;set;}
	public string IsShare{get;set;}
	public Boolean IsEdit{get;set;}
    public list<SelectOption> getCriterias()
    {
        Schema.DescribeFieldResult F = Value_Map_Strategies__c.Value_Criteria__c.getDescribe();
        List<SelectOption> options = new List<SelectOption>();
        for(Schema.Picklistentry p : F.getPicklistValues())
        {
            if(!p.isActive())
            {
                continue;
            }
            options.add(new SelectOption(p.getLabel(),p.getValue()));
        }
        return options;
    }
    public list<SelectOption> getOptions()
    {
        Schema.DescribeFieldResult F = Value_Map_Strategies__c.Value_Options__c.getDescribe();
        List<SelectOption> options = new List<SelectOption>();
        for(Schema.Picklistentry p : F.getPicklistValues())
        {
            if(!p.isActive())
            {
                continue;
            }
            options.add(new SelectOption(p.getLabel(),p.getValue()));
        }
        return options;
    }
    public list<SelectOption> getImportance_to_Customer()
    {
        Schema.DescribeFieldResult F = Value_Map_Strategies__c.Importance_to_Customer__c.getDescribe();
        List<SelectOption> options = new List<SelectOption>();
        for(Schema.Picklistentry p : F.getPicklistValues())
        {
            if(!p.isActive())
            {
                continue;
            }
            options.add(new SelectOption(p.getLabel(),p.getValue()));
        }
        return options;
    }
    public list<SelectOption> getCompetitive_Standing()
    {
        Schema.DescribeFieldResult F = Value_Map_Strategies__c.Competitive_Standing__c.getDescribe();
        List<SelectOption> options = new List<SelectOption>();
        for(Schema.Picklistentry p : F.getPicklistValues())
        {
            if(!p.isActive())
            {
                continue;
            }
            options.add(new SelectOption(p.getLabel(),p.getValue()));
        }
        return options;
    }
    public Value_Map_StrategiesController(ApexPages.StandardController controller)
    {
    	IsEdit = PubMethod.IsEdit(UserInfo.getUserRoleId());
        OppId = controller.getId();
        pub = new PubMethod();
		Opportunity opp = [select Owner.UserRoleId from Opportunity where id =: OppId];
		set<ID> HighRole = pub.getHighRoleName(opp.Owner.UserRoleId);
		if(HighRole.contains(UserInfo.getUserRoleId()))
		{
			IsAccess = true;
		}
		else
		{
			IsAccess = false;
		}
        list_Strategies = new list<Strategies>();
        list<Value_Map_Strategies__c> list_Value_Map_Strategies = [select IsShare__c,id,Action_Plan__c,Competitive_Standing__c,Importance_to_Customer__c,
                          Value_Criteria__c,OwnerId,
                          Value_Options__c,Opportunity__c from Value_Map_Strategies__c 
                          where Opportunity__c=:OppId];
                          
        if(list_Value_Map_Strategies == null || list_Value_Map_Strategies.size() == 0)  
        {
        	IsShare = 'Is Retake';
            Schema.DescribeFieldResult F = Value_Map_Strategies__c.Value_Options__c.getDescribe();
            for(Schema.Picklistentry p : F.getPicklistValues())
            {
                if(!p.isActive() || p.getValue() == 'Value Other')
                {
                    continue;
                }
                Strategies strate = new Strategies();
                strate.IsEdit = true;
                strate.Index = list_Strategies.size();
                Value_Map_Strategies__c VMstrate = new Value_Map_Strategies__c();
                VMstrate.Value_Options__c = p.getValue();
                VMstrate.Opportunity__c = OppId;
                strate.Strategie = VMstrate;
                list_Strategies.add(strate);
            }
        }
        else
        {
        	if(list_Value_Map_Strategies[0].IsShare__c)
        	{
        		IsShare = 'Is Shared';
        	}
        	else
        	{
        		IsShare = 'Is Retake';
        	}
            for(Value_Map_Strategies__c vm : list_Value_Map_Strategies)
            {
                Strategies strate = new Strategies();
                strate.IsEdit = false;
                strate.Index = list_Strategies.size();
                strate.Strategie = vm;
                list_Strategies.add(strate);
            }
        }
    }
    //删除操作
    public void Del()
    {
        if(Index>=0)
        {
            for(Integer i = 0 ; i<  list_Strategies.size() ; i ++)
            {
                if(list_Strategies[i].Index == Index)
                {
                    if(list_Strategies[i].Strategie.id != null)
                    {
                        delete list_Strategies[i].Strategie;
                    }
                    list_Strategies.remove(i);
                    return;
                }
            }
        }
    }
    //编辑操作
    public void Upd()
    {
        if(Index>=0)
        {
            for(Integer i = 0 ; i<  list_Strategies.size() ; i ++)
            {
                if(list_Strategies[i].Index == Index)
                {
                    if(list_Strategies[i].Strategie.id != null)
                    {
                        list_Strategies[i].IsEdit = true;
                        //delete list_Strategies[i].Strategie;
                    }
                    //list_Strategies.remove(i);
                    return;
                }
            }
        }
        
    }
    //添加记录操作
    public void Add()
    {
        Strategies strate = new Strategies();
        strate.IsEdit = true;
        strate.Index = list_Strategies.size();
        Value_Map_Strategies__c VMstrate = new Value_Map_Strategies__c();
        VMstrate.Opportunity__c = OppId;
        strate.Strategie = VMstrate;
        list_Strategies.add(strate);
    }
    //保存记录操作
    public void Hold()
    {
        list<Value_Map_Strategies__c> In_Strategies = new list<Value_Map_Strategies__c>();
        list<Value_Map_Strategies__c> Upd_Strategies = new list<Value_Map_Strategies__c>();
        if(list_Strategies != null && list_Strategies.size() > 0)
        {
            for(Strategies strate : list_Strategies)
            {
                strate.IsEdit = false;
                if(strate.Strategie.id != null)
                {
                    Upd_Strategies.add(strate.Strategie);
                }
                else
                {
                    In_Strategies.add(strate.Strategie);
                }
            }
        }
        if(Upd_Strategies.size() > 0)
        {
            update Upd_Strategies;
        }
        if(In_Strategies.size() > 0)
        {
            insert In_Strategies;
        }
    }
/*
	public void Approve()
	{
		list<Value_Map_Strategies__c> Upd_Strategies = new list<Value_Map_Strategies__c>();
		IsShare = 'Is Shared';
		list<Value_Map_Strategies__Share> RT_Shares = new list<Value_Map_Strategies__Share>();
		for(OpportunityTeamMember team : [Select UserId, TeamMemberRole, OpportunityId, OpportunityAccessLevel, Id 
		                                  From OpportunityTeamMember where OpportunityId =: OppId])
		{
			if(team.UserId != null)
			{
				for(Strategies st:list_Strategies)
				{
					st.Strategie.IsShare__c = true;
					if(st.Strategie.id == null)
					{
						continue;
					}
					Upd_Strategies.add(st.Strategie);
					if(st.Strategie.OwnerId != team.UserId)
					{
						Value_Map_Strategies__Share speShare = new Value_Map_Strategies__Share();
						speShare.ParentId = st.Strategie.id;
						if(team.OpportunityAccessLevel == 'Read Only')
						{
							speShare.AccessLevel = 'Read';
						}
						else
						{
							speShare.AccessLevel = 'Edit';
						}
						speShare.UserOrGroupId = team.UserId;
						RT_Shares.add(speShare);
					}
				}
			}
		}
		if(Upd_Strategies.size() > 0)
		{
			update Upd_Strategies;
		}
		if(RT_Shares.size() > 0)
		{
			insert RT_Shares;
		}
	}

	public void Retake()
	{
		IsShare = 'Is Retake';
		set<ID> set_MemberIds = new set<ID>();
		set<ID> set_speIds = new set<ID>();
		list<Value_Map_Strategies__c> Upd_Strategies = new list<Value_Map_Strategies__c>();
		for(OpportunityTeamMember team : [Select UserId, TeamMemberRole, OpportunityId, OpportunityAccessLevel, Id 
		                                  From OpportunityTeamMember where OpportunityId =: OppId])
		{
			if(team.UserId != null)
			{
				for(Strategies st:list_Strategies)
				{
					st.Strategie.IsShare__c = false;
					if(st.Strategie.id == null)
					{
						continue;
					}
					Upd_Strategies.add(st.Strategie);
					if(st.Strategie.id != null  && st.Strategie.OwnerId != team.UserId)
					{
						set_MemberIds.add(team.UserId);
						set_speIds.add(st.Strategie.id);
					}
				}
			}
		}
		if(set_MemberIds != null && set_speIds != null)
		{
			list<Value_Map_Strategies__Share> list_Share = [select id from Value_Map_Strategies__Share 
			                                       where UserOrGroupId in:set_MemberIds and ParentId in: set_speIds];
			if(list_Share != null && list_Share.size() > 0)
			{
				delete list_Share;
			}
		}
		if(Upd_Strategies.size() > 0)
		{
			update Upd_Strategies;
		}
	}*/
}