/**
 * 作者：Ziyue
 * 时间：2013-9-27
 * 功能：
 */
@isTest
private class Test_Sweet_Point_Exp_Key_Suc_FacContr 
{
    static testMethod void myUnitTest() 
    {
        Account acc = new Account();
        acc.Name = 'acc';
        insert acc;
        
        Account_Team__c team = new Account_Team__c();
        team.Account__c = acc.id;
        team.Team_Member__c = UserInfo.getUserId();
        insert team;
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        Sweet_Point_Exp_Key_Suc_FacController fac = new Sweet_Point_Exp_Key_Suc_FacController(controller);
        fac.Upd();
        fac.Hold();
        fac.Approve();
        fac.Retake();
        
        Sweet_Point_Exp_Key_Suc_FacController fac1 = new Sweet_Point_Exp_Key_Suc_FacController(controller);
        fac1.Upd();
        fac1.Hold();
        fac1.Approve();
        fac1.Retake();
    }
}