/**
 * 作者：Ziyue
 * 时间：2013-9-27
 * 功能：
 */
@isTest
private class Test_Risk_TrackerController 
{
    static testMethod void myUnitTest() 
    {
        Opportunity opp = new Opportunity();
        opp.Name = 'opp';
        opp.StageName = '已赢单';
        opp.CloseDate = Date.today();
        insert opp;
        ApexPages.StandardController controller = new ApexPages.StandardController(opp);
        Risk_TrackerController tracker = new Risk_TrackerController(controller);
        tracker.Upd();
        tracker.Hold();
        Risk_TrackerController tracker2 = new Risk_TrackerController(controller);
        tracker2.Upd();
        tracker2.Hold();
    }
}