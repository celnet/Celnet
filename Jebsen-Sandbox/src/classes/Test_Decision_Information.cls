/*Author:Leo
 *Date:2014-4-1	
 *Function:test decision_information
 */
@isTest
private class Test_Decision_Information {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Account acc = new Account();
        acc.Name = 'AUDI';
        insert acc;
        Opportunity opp = new Opportunity();
        opp.Name = 'AUDI2 opportunity';
        opp.AccountId = acc.id;
        opp.CloseDate = Date.today();
        opp.StageName = 'Lost Order';
        opp.Recently_Escalation_Type__c = 'resource request';
        insert opp;
        ApexPages.StandardController sa = new ApexPages.StandardController(opp);
        Decision_Information di = new Decision_Information(sa);
        di.save();
        di.Cancel();
        di.Edit();
    }
}