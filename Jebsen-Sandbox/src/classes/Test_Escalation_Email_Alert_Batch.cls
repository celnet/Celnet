/*Author:Leo
 *Date:2014-4-10
 *Function:Test Escalation_Email_Alert_Batch
 */
@isTest
private class Test_Escalation_Email_Alert_Batch {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        //插入联系人
        
        //插入业务机会
        Account acc = new Account();
        acc.Name = 'AUDI';
        insert acc;
        Opportunity opp = new Opportunity();
        opp.Name = 'AUDI2 opportunity';
        opp.AccountId = acc.id;
        opp.CloseDate = Date.today();
        opp.StageName = 'Lost Order';
        opp.Amount = 2000000;
        insert opp;
        opp.Recently_Escalation_Type__c = 'others';
        opp.Main_reason_for_winning_or_losing__c = 'test';
        opp.Decision_Lost_Type__c = 'test';
        opp.Decision_Result__c = 'test';
        update opp;
        Escalation__c es = new Escalation__c();
        es.Opportunity__c = opp.id;
        es.Escalation_Type__c = 'Test';
        es.Deadline__c = Date.today();
        insert es;
        //插入Escalation
        //执行batch
        Escalation_Email_Alert_Batch eeab = new Escalation_Email_Alert_Batch();
        eeab.daysBetween = 7;
        Database.executeBatch(eeab,1);
        opp.Amount = 20000000;
        update opp;
        eeab = new Escalation_Email_Alert_Batch();
        eeab.daysBetween = 7;
        opp.Amount = 2000;
        update opp;
        Database.executeBatch(eeab,1);
        eeab.daysBetween = -1;
        opp.Amount = 2000;
        update opp;
        Database.executeBatch(eeab,1);
        
    }
}