/**
 * Test GrossMarginCalculationController
 */
@isTest (SeeAllData=true)
private class Test_GrossMarginCalculationController {

    static testMethod void myUnitTest() {
		Opportunity opp = new Opportunity();
		opp.Name = 'Test opp';
		opp.StageName = 'test';
		opp.CloseDate = date.today().addMonths(2);
		opp.Purchase_scope__c = 'Fastener Parts;Mold';
		insert opp;
		
		system.Test.startTest();
		Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(opp);
		GrossMarginCalculationController gmcc = new GrossMarginCalculationController(controller);
		gmcc.iFastenerAdd();
		gmcc.list_FastenerGP[0].gp.Product_Article__c = 'EBF0625A05A';
		gmcc.list_FastenerGP[0].gp.Destination__c = 'Shanghai';
		gmcc.list_FastenerGP[0].gp.Purchase__c = 134;
		gmcc.list_FastenerGP[0].gp.GP__c = 24.5;
		gmcc.Index = 0 ;
		gmcc.iSubmit();
		gmcc.iFastenerEdit();
		gmcc.iSave();
		gmcc.iFastenerDel();
		
		gmcc.iFeederAdd();
		gmcc.iFeederNewDel();
		gmcc.iFeederAdd();
		gmcc.list_FeederGPNew[0].gp.Product_Article__c = 'AVCD23';
		gmcc.list_FeederGPNew[0].gp.Purchase__c = 134;
		gmcc.list_FeederGPNew[0].gp.Inbound_Logistic_Cost__c = 234;
		gmcc.list_FeederGPNew[0].gp.Outbound_Logistic_cost__c = 123;
		gmcc.list_FeederGPNew[0].gp.Inventory__c = 123;
		gmcc.list_FeederGPNew[0].gp.GP__c = 24.5;
		gmcc.Index = 0 ;
		gmcc.iSubmit();
		//gmcc.iFeederEdit();
		gmcc.iSave();
		try{
		gmcc.iSubmit();
		gmcc.iSubmit();
		}catch(exception e){
			
		}
		gmcc.iReject();
		gmcc.iRejectConfirm();
		gmcc.iFeederDel();
		
		
		system.Test.stopTest();
    }
}