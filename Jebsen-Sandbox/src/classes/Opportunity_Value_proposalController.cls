/*
 * 作者：Ziyue
 * 时间：2013-9-27
 * 功能：业务机会Value proposal
*/
public class Opportunity_Value_proposalController 
{
	public Boolean Disabled{get;set;}
	public Opportunity opp{get;set;}
	public Boolean IsCapabilities{get;set;}//Capabilities__c是否必填
	public Boolean IsImpact{get;set;}//Impact_in_Year_1__c是否必填
	public Boolean IsIndividual{get;set;}//Individual__c是否必填
	public Boolean IsPolitical{get;set;}//Political__c是否必填
	public Boolean IsStrategic{get;set;}//Strategic__c是否必填
	public Boolean IsTactical{get;set;}//Tactical__c是否必填
	public Boolean IsProposed_Price_of_Solution{get;set;}//Proposed_Price_of_Solution__c是否必填
	public Boolean IsValue_Created_by_Solution_in_Year_1{get;set;}//Value_Created_by_Solution_in_Year_1__c是否必填
	public Boolean IsTotal_Implementation_Cost{get;set;}//Total_Implementation_Cost__c是否必填
	public Boolean IsInvestment{get;set;}//Investment__c是否必填
	public Boolean IsEffort{get;set;}//Effort__c是否必填
	public Boolean IsTime{get;set;}//Time__c是否必填
	public Boolean IsSolution_Implementation_Costs{get;set;}//Solution_Implementation_Costs__c是否必填
	public Boolean IsUSD_EUR_RMB{get;set;}//Total_Impact_in_USD_EUR_RMB__c是否必填
	public Boolean IsEdit{get;set;}
	public Opportunity_Value_proposalController(ApexPages.Standardcontroller controller)
	{
		IsEdit = PubMethod.IsEdit(UserInfo.getUserRoleId());
		Disabled=true;
		opp = (Opportunity)controller.getRecord();
		Schema.DescribeFieldResult F = Opportunity.AON_Requred__c.getDescribe();
        set<string> options = new set<string>();
        for(Schema.Picklistentry p : F.getPicklistValues())
        {
            if(!p.isActive())
            {
                continue;
            }
            options.add(p.getValue());
        }
        IsCapabilities = options.contains('Capabilities')?true:false;
        IsImpact = options.contains('Impact in Year 1')?true:false;
        IsIndividual = options.contains('Individual')?true:false;
        IsPolitical = options.contains('Political')?true:false;
        IsStrategic = options.contains('Strategic')?true:false;
        IsTactical = options.contains('Tactical')?true:false;
        IsProposed_Price_of_Solution = options.contains('Proposed Price of Solution')?true:false;
        IsValue_Created_by_Solution_in_Year_1 = options.contains('Value Created by Solution in Year 1')?true:false;
        IsTotal_Implementation_Cost = options.contains('Total Implementation Cost')?true:false;
        IsInvestment = options.contains('Investment')?true:false;
        IsEffort = options.contains('Effort')?true:false;
        IsTime = options.contains('Time')?true:false;
        IsSolution_Implementation_Costs = options.contains('Solution Implementation Costs')?true:false;
        IsUSD_EUR_RMB = options.contains('Total Impact in USD/EUR/RMB')?true:false;
        
	}
	public void Upd()
	{
		Disabled=false;
	}
	public void Hold()
	{
		Disabled=true;
		update opp;
	}
}