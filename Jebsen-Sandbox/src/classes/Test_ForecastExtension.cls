/**
 * Author: Steven
 * Date: 2014-9-9
 * Test Forecast Extension
 */
@isTest
private class Test_ForecastExtension {

    static testMethod void myUnitTest() {
        Opportunity opp = new Opportunity();
        opp.Name = 'opp';
        opp.CloseDate = Date.today();
        opp.StageName = 'Awareness of Needs';
        opp.Probability = 10;
        insert opp;
        
        Forecast__c f = new Forecast__c();
        f.Q1__c = 321;
        f.Q2__c = 32;
        f.Q3__c = 44;
        f.Q4__c = 51;
        f.Opportunity__c = opp.Id;
        f.Year__c = String.valueOf(Date.today().year());
        insert f;
        
        ApexPages.currentPage().getParameters().put('oppId',opp.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(opp);
        ForecastExtension extension = new ForecastExtension(controller);
        list<SelectOption> lso = extension.years;
        extension.addLine();
        extension.Index = 1;
        extension.deleteRecord();  
        extension.saveRecords();
        extension.editRecord();
        extension.selectedYear = String.valueOf(Date.today().addYears(1).year());
        extension.refreshRecords();
              
    }
}