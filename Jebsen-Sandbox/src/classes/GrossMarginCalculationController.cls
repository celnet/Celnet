public without sharing class GrossMarginCalculationController {
    public Boolean isFastener{get;set;}
    public Boolean isFeeder{get;set;}
    
    public Double ExchangeRate{get;set;}
    public Double CapitalCost{get;set;}
    
    private Opportunity opp ;
    private User ApprovalUser ; 
    private Boolean UserConfirm = false;
    
    public Integer Index{get;set;}
    public String pageMsg{get;set;}
    public String pageFeederMsg{get;set;}
    public String pageRejectMsg{get;set;}
    public boolean RejectShow{get;set;}
    public boolean RejectConfirmShow{get;set;}
    public boolean ShowEdit{get;set;}
    public boolean saveBtn{get;set;}
    public String RejectComments{get;set;}
    public String RejectArticle{get;set;}
    public Gross_Margin_Calculation__c RejectDate{get;set;}
    
    public List<GPWrapper> list_FastenerGP{get;set;}
    public List<GPWrapper> list_FeederGP{get;set;}
    public List<GPWrapper> list_FeederGPNew{get;set;}
    
    public GrossMarginCalculationController(ApexPages.StandardController controller){
        isFastener=false;
        isFeeder=false;
        saveBtn = true;
        RejectDate = new Gross_Margin_Calculation__c();
        this.initOpportunity(controller.getId());
        this.initGPParameter();
        this.initShowControl();
    }
    public void iFastenerAdd(){
        pageMsg = '';
        Gross_Margin_Calculation__c newGP = new Gross_Margin_Calculation__c();
        newGP.Product_Type__c = 'Fastener';
        newGP.Transport__c='Nuts Sea Transport';
        newGP.Date__c =  date.today();
        newGP.Opportunity__c = opp.Id;
        newGP.Exchange_Rate__c = this.ExchangeRate;
        newGP.Capital_Cost__c = this.CapitalCost;
        GPWrapper gpw = new GPWrapper(true, list_FastenerGP.size() , newGP);
        list_FastenerGP.add(gpw);
    }
    public void changetrans()
    {}
    public void iFeederAdd(){
        pageFeederMsg = '';
        Gross_Margin_Calculation__c newGP = new Gross_Margin_Calculation__c();
        newGP.Product_Type__c = 'Feeder or tooling';
        newGP.Date__c =  date.today();
        newGP.Opportunity__c = opp.Id;
        newGP.Exchange_Rate__c = this.ExchangeRate;
        newGP.Capital_Cost__c = this.CapitalCost;
        GPWrapper gpw = new GPWrapper(true, list_FeederGP.size() , newGP);
        list_FeederGPNew.add(gpw);
    }
    public void iFastenerEdit(){
        pageMsg = '';
        if(Index>=0)
        {
            for(Integer i = 0 ; i<  list_FastenerGP.size() ; i ++)
            {
                if(list_FastenerGP[i].Index == Index)
                {
                    if(list_FastenerGP[i].gp.id != null)
                    {
                        list_FastenerGP[i].IsEdit = true;
                        //delete list_Strategies[i].Strategie;
                    }
                    //list_Strategies.remove(i);
                    return;
                }
            }
        }
    }
    public void iFastenerDel(){
        pageMsg = '';
        if(Index>=0)
        {
            for(Integer i = 0 ; i< list_FastenerGP.size() ; i ++)
            {
                if(list_FastenerGP[i].Index == Index)
                {
                    if(list_FastenerGP[i].gp.id != null)
                    {
                        //list_lineItem[i].IsEdit = true;
                        delete list_FastenerGP[i].gp;
                    }
                    list_FastenerGP.remove(i);
                    break;
                }
            }
        }
    }
    public void iFeederDel(){
        pageFeederMsg = '';
        if(Index>=0)
        {
            for(Integer i = 0 ; i< list_FeederGP.size() ; i ++)
            {
                if(list_FeederGP[i].Index == Index)
                {
                    if(list_FeederGP[i].gp.id != null)
                    {
                        delete list_FeederGP[i].gp;
                    }
                    list_FeederGP.remove(i);
                    break;
                }
            }
        }
    }
    public void iFeederNewDel(){
        pageFeederMsg = '';
        if(Index>=0)
        {
            for(Integer i = 0 ; i< list_FeederGPNew.size() ; i ++)
            {
                if(list_FeederGPNew[i].Index == Index)
                {
                    if(list_FeederGPNew[i].gp.id != null)
                    {
                        delete list_FeederGPNew[i].gp;
                    }
                    list_FeederGPNew.remove(i);
                    break;
                }
            }
        }
    }
    public void iSubmit(){
        pageMsg = '';
        pageFeederMsg = '';
        for(GPWrapper li : list_FastenerGP)
        {
            if(li.IsEdit){
                pageMsg = 'Please save first.';
                return;
            }
        }
        for(GPWrapper li : list_FeederGP)
        {
            if(li.IsEdit){
                pageFeederMsg = 'Please save first.';
                return;
            }
        }
        List<Gross_Margin_Calculation__c> list_gmc = new List<Gross_Margin_Calculation__c>();
        List<GPWrapper> list_Fastener = new List<GPWrapper>();
        List<GPWrapper> list_Feeder = new List<GPWrapper>();
        for(GPWrapper li : list_FastenerGP){
            if(!li.gp.Submitted__c){
                list_gmc.add(li.gp);
                list_Fastener.add(li);
            }
        }
        for(GPWrapper li : list_FeederGP){
            if(!li.gp.Submitted__c){
                list_gmc.add(li.gp);
                list_Feeder.add(li);
            }
        }
        if(list_gmc.size() > 0){
            datetime dt = Datetime.now();
            Boolean needSendMail = false;
            for(Gross_Margin_Calculation__c gmc : list_gmc){
                gmc.Submitted__c = true;
                //gmc.Submit_Date__c = date.today();
                gmc.Submit_Time__c = dt;
                if(gmc.GP__c < 20){
                    needSendMail = true;
                }
            }
            update list_gmc;
            if(needSendMail){
                String sHtmlBody = GrossMarginCalculationEmail.concatenateEmailHtmlBody(list_Fastener, list_Feeder, this.opp);
                this.SubmitMail(sHtmlBody);
            }
        }else{
            pageMsg = 'Please do not resubmit.';
            pageFeederMsg = 'Please do not resubmit.';
            return;
        }
        initOpportunity(opp.Id);
    }
    public void iReject(){
        this.RejectShow = false;
        this.RejectConfirmShow = true;
        this.pageRejectMsg = '';
    }
    public void iRejectConfirm(){
        Set<ID> set_UpIds = new Set<ID>();
        system.debug(this.RejectArticle+' ***** '+this.RejectDate.Date__c);
        this.RejectArticle = this.RejectArticle==''?null:this.RejectArticle;
        if(this.RejectArticle != null && this.RejectDate.Date__c != null){
            for(Gross_Margin_Calculation__c gmc : opp.Gross_Margin_Calculation__r){
                if(this.RejectArticle.contains(gmc.Product_Article__c) && gmc.Date__c == this.RejectDate.Date__c){
                    set_UpIds.add(gmc.Id);
                }
            }
        }else if(this.RejectArticle == null && this.RejectDate.Date__c != null){
            for(Gross_Margin_Calculation__c gmc : opp.Gross_Margin_Calculation__r){
                if(gmc.Date__c == this.RejectDate.Date__c){
                    set_UpIds.add(gmc.Id);
                }
            }
        }else if(this.RejectArticle != null && this.RejectDate.Date__c == null){
            for(Gross_Margin_Calculation__c gmc : opp.Gross_Margin_Calculation__r){
                if(this.RejectArticle.contains(gmc.Product_Article__c)){
                    set_UpIds.add(gmc.Id);
                }
            }
        }else{
            DateTime newSubmitTime ;
            for(Gross_Margin_Calculation__c gmc : opp.Gross_Margin_Calculation__r){
                if(gmc.Submitted__c && gmc.Submit_Time__c != null){
                    newSubmitTime = newSubmitTime==null?gmc.Submit_Time__c:newSubmitTime;
                    newSubmitTime  = newSubmitTime<gmc.Submit_Time__c?gmc.Submit_Time__c:newSubmitTime;
                }
            }
            for(Gross_Margin_Calculation__c gmc : opp.Gross_Margin_Calculation__r){
                if(gmc.Submitted__c && gmc.Submit_Time__c == newSubmitTime){
                    set_UpIds.add(gmc.Id);
                }
            }
        }
        if(set_UpIds.size() > 0){
            List<Gross_Margin_Calculation__c> list_gmc = new List<Gross_Margin_Calculation__c>();
            for(ID gmcId : set_UpIds){
                Gross_Margin_Calculation__c gmc = new Gross_Margin_Calculation__c();
                gmc.Id = gmcId;
                gmc.Submitted__c = false;
                //gmc.Submit_Date__c = null;
                gmc.Submit_Time__c=null;
                gmc.Sent__c = false;
                list_gmc.add(gmc);
            }
            update list_gmc;
            pageMsg = 'Rejected Successfully.';
            pageFeederMsg = 'Rejected Successfully.';
            rejectMail();
        }else{
            pageRejectMsg = 'Please check reject date and reject article.';
            return;
        }
    }
    private void rejectMail(){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //mail.setToAddresses(list_address);
        //mail.setSenderDisplayName('Salesforce.com');
        mail.setOrgWideEmailAddressId([Select o.IsAllowAllProfiles, o.Id, o.DisplayName, o.Address From OrgWideEmailAddress o where DisplayName = 'Jebsen Salesforce'].Id);
        mail.setSubject('GP calculation request Rejected');
        //EmailTemplate et = [SELECT id FROM EmailTemplate WHERE DeveloperName = 'Sales_Order_Archived'];
        //mail.setTemplateId(et.id);
        String sBody = 'Dear, <br/>';
        sBody += 'Your GP calculation request of ';
        sBody += opp.Account.Name;
        sBody += '+';
        sBody += opp.Name;
        sBody += ' has been rejected. <br/>';
        if(this.RejectComments != null){
            sBody+=('The reason is:'+this.RejectComments+'<br/>');
        }
        if(this.RejectDate.Date__c != null){
            sBody+=('The reject date is:'+this.RejectDate.Date__c.Year()+'-'+this.RejectDate.Date__c.Month()+'-'+this.RejectDate.Date__c.Day()+'<br/>');
        }
        if(this.RejectArticle != null){
            sBody+=('The reject article is:'+this.RejectArticle+'<br/>');
        }
        sBody += 'Pls log in Salesforce to update  your calculation or contact your supervisor for advise.<br/>';
        
        sBody += ('Click <a href="'+URL.getSalesforceBaseUrl().getHost()+'/'+opp.Id +'">website here.</a><br/><br/>');
        sBody += 'Thanks,<br/><br/>';
        sBody += 'Salesforce.com';
        //mail.setPlainTextBody(sBody);
        mail.setHtmlBody(sBody);
        mail.setTargetObjectId(opp.OwnerId);
        //mail.setWhatId(oppId);
        mail.setSaveAsActivity(false);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        this.RejectConfirmShow = false;
    }
    private void SubmitMail(String sHtmlBody){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //mail.setToAddresses(list_address);
        //mail.setSenderDisplayName('Salesforce.com');
        //mail.setSenderDisplayName('Salesforce.com');
       	mail.setOrgWideEmailAddressId([Select o.IsAllowAllProfiles, o.Id, o.DisplayName, o.Address From OrgWideEmailAddress o where DisplayName = 'Jebsen Salesforce'].Id);
        mail.setSubject('Reminder: GP Calculation LESS TAHN 20% ');
        //mail.setSubject('Sales Order Request');
        //EmailTemplate et = [SELECT id FROM EmailTemplate WHERE DeveloperName = 'New_Quotation_Request'];
        //mail.setTemplateId(et.id);
        mail.setHtmlBody(sHtmlBody);
        mail.setTargetObjectId(ApprovalUser.Id);
        //mail.setWhatId(opp.Id);
        mail.setSaveAsActivity(false);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        pageMsg = 'Submitted successfully.';
        pageFeederMsg = 'Submitted successfully.';
    }
    public void iSaveConfirm(){
        this.UserConfirm = true;
        this.iSave();
        this.saveBtn = true;
    }
    public void iSave(){
        pageMsg = '';
        pageFeederMsg = '';
        list<Gross_Margin_Calculation__c> In_gp = new list<Gross_Margin_Calculation__c>();
        list<Gross_Margin_Calculation__c> Upd_gp = new list<Gross_Margin_Calculation__c>();
        
        boolean haveFastenerMail = false;
        boolean haveFeederMail = false;
        if(list_FastenerGP != null && list_FastenerGP.size() > 0)
        {
            for(GPWrapper li : list_FastenerGP){
                if(li.gp.Product_Type__c == null || 
                    li.gp.Product_Article__c == null || 
                    li.gp.Purchase__c == null || 
                    li.gp.GP__c == null 
                    ){
                    pageMsg = 'Please enter the required fields.';
                    return ;
                }
            }
            for(GPWrapper li : list_FastenerGP)
            {
                system.debug('Dessssss:'+li.gp.Destination__c);
                if(li.gp.Destination__c != null){
                    li.gp.Destination_Value__c = map_Destination.get(li.gp.Destination__c);
                }
                system.debug('DessssssV:'+li.gp.Destination_Value__c);
                if(map_QC.containsKey(li.gp.Product_Article__c)){
                    li.gp.Quantity_Carton__c = map_QC.get(li.gp.Product_Article__c);
                    li.gp.Duty__c=map_QCDuty.get(li.gp.Product_Article__c);
                }else{
                    li.gp.Product_Article__c.addError('Invalid value.');
                    pageMsg = 'The product article is not exist. Please contact your system administrator to add in the system.';
                    return ;
                }
                if(li.gp.Transport__c=='Nuts Sea Transport')
                {
                    li.gp.Inbound_Freight__c=map_GrossMarginCalculation_Parameter.get('Inbound Freight');
                    li.gp.Operation_charge_per_month__c=map_GrossMarginCalculation_Parameter.get('Operation charge per month');
                }
                if(li.gp.id == null && li.gp.GP__c < 20){
                    haveFastenerMail = true;
                }
                //li.IsEdit = false;
                if(li.gp.id == null)
                {
                    In_gp.add(li.gp);
                }
                
            }
        }
        if(list_FeederGPNew != null && list_FeederGPNew.size() > 0)
        {
            for(GPWrapper li : list_FeederGPNew){
                if(li.gp.Product_Type__c == null || 
                    li.gp.Product_Article__c == null || 
                    li.gp.Purchase__c == null || 
                    li.gp.GP__c == null || 
                    li.gp.Inbound_Logistic_Cost__c == null ||
                    li.gp.Outbound_Logistic_cost__c == null
                    ){
                    pageFeederMsg = 'Please enter the required fields.';
                    return ;
                }
            }
            for(GPWrapper li : list_FeederGPNew)
            {
                if(li.gp.GP__c < 20){
                    haveFeederMail = true;
                }
                //li.IsEdit = false;
                if(li.gp.id == null)
                {
                    In_gp.add(li.gp);
                }
            }
        }
        //若存在
        if(!this.UserConfirm && (haveFastenerMail || haveFeederMail)){
            if(haveFastenerMail){
                pageMsg = 'GP is less than 20%. Once you saved, an email will send to your superior.';
            }
            if(haveFeederMail){
                pageFeederMsg = 'GP is less than 20%. Once you saved, an email will send to your superior.';   
            }
            this.saveBtn = false;
            return ;
        }
        if(list_FastenerGP != null && list_FastenerGP.size() > 0)
        {
            for(GPWrapper li : list_FastenerGP){
                li.IsEdit = false;
            }
        }
        if(list_FeederGPNew != null && list_FeederGPNew.size() > 0)
        {
            for(GPWrapper li : list_FeederGPNew){
                li.IsEdit = false;
            }
        }
        if(In_gp.size() > 0)
        {
            insert In_gp;
        }
        //发送邮件
        if(this.UserConfirm && (haveFastenerMail || haveFeederMail)){
            List<GPWrapper> list_FastenerMail = new List<GPWrapper>();
            List<GPWrapper> list_FeederMail = new List<GPWrapper>();
            List<id> list_gmcid = new List<id>();
            for(Gross_Margin_Calculation__c gmc : In_gp){
                list_gmcid.add(gmc.Id);
            }
            for(Gross_Margin_Calculation__c gmc : [Select  g.Tax__c, g.Quantity_Carton__c, g.Purchase__c, g.Product_Type__c, g.Date__c,g.Logistic_Num__c,
                g.Product_Article__c, g.Price_with_Tax__c, g.Outbound_Logistic_cost__c, g.Opportunity__c,
                g.Destination__c, g.Destination_Value__c ,   
                g.Operation_charge_per_month__c, g.Inventory__c, g.Inbound_Logistic_Cost__c,g.Total_Inventory_Cost__c,
                g.Inbound_Freight__c, g.GP__c, g.Exchange_Rate__c, g.Submit_Time__c, g.Transport__c,
                g.Capital_Cost__c,  g.Submitted__c, g.Sent__c 
                From Gross_Margin_Calculation__c g where id in: list_gmcid]){
                if(gmc.GP__c < 20){
                    if(gmc.Product_Type__c == 'Fastener'){
                        GPWrapper li = new GPWrapper(false, list_FastenerMail.size() , gmc);
                        list_FastenerMail.add(li);
                    }else if(gmc.Product_Type__c == 'Feeder or tooling'){
                        GPWrapper li = new GPWrapper(false,list_FeederMail.size() , gmc);
                        list_FeederMail.add(li);
                    }
                }
            }
            system.debug(list_FastenerMail);
            String sHtmlBody = GrossMarginCalculationEmail.concatenateEmailHtmlBody(list_FastenerMail, list_FeederMail, this.opp);
            this.SubmitMail(sHtmlBody);
        }
        initOpportunity(opp.Id);
        this.UserConfirm = false;
    }
    private void initOpportunity(ID oppId){
        opp = [Select Id,Purchase_scope__c, Name , Account.Name, Ownerid, Owner.Name, 
                (Select  g.Tax__c, g.Quantity_Carton__c, g.Purchase__c, g.Product_Type__c, g.Date__c,g.Logistic_Num__c,
                g.Product_Article__c, g.Price_with_Tax__c, g.Outbound_Logistic_cost__c, g.Opportunity__c,
                g.Destination__c, g.Destination_Value__c ,   
                g.Operation_charge_per_month__c, g.Inventory__c, g.Inbound_Logistic_Cost__c,g.Total_Inventory_Cost__c,
                g.Inbound_Freight__c, g.GP__c, g.Exchange_Rate__c, g.Submit_Time__c, g.Transport__c,
                g.Capital_Cost__c,  g.Submitted__c, g.Sent__c 
                From Gross_Margin_Calculation__r g order by CreatedDate) 
                From Opportunity Where Id =: oppId];
        if(opp.Purchase_scope__c.contains('Fastener Parts') || opp.Purchase_scope__c.contains('Other'))
        isFastener = true;
        if(opp.Purchase_scope__c.contains('Mold') || opp.Purchase_scope__c.contains('Equipment'))
        isFeeder = true;
        initGPWrapper(opp.Gross_Margin_Calculation__r);
    }
    private void initShowControl(){
        List<String> list_email = new list<String>();
        for(MailBox__c mailBox : MailBox__c.getAll().values()){
            if(mailBox.Name.startsWith('GP Approver')){
                list_email.add(mailBox.Email__c);
            }
        }
        List<User> lu = [Select Id,Email From user where email in: list_email and IsActive = true limit 1];
        if(lu.size() == 0)
        this.ApprovalUser = [Select Id,Email From User Where UserRole.Name = 'Sales Manager' limit 1];
        else
        this.ApprovalUser = lu[0];
        this.RejectShow = this.ApprovalUser.Id==UserInfo.getUserId()?true:false;
        this.ShowEdit = this.RejectShow?false:true;
        if(ShowEdit){
            this.saveBtn = true;
        }
    }
    private void initGPWrapper(List<Gross_Margin_Calculation__c> list_gp){
        list_FastenerGP = new List<GPWrapper>();
        list_FeederGP = new List<GPWrapper>();
        list_FeederGPNew = new List<GPWrapper>();
        for(Gross_Margin_Calculation__c gp:list_gp){
            if(gp.Product_Type__c == 'Fastener'){
                GPWrapper gpw = new GPWrapper(false, list_FastenerGP.size() , gp);
                list_FastenerGP.add(gpw);
            }else if(gp.Product_Type__c == 'Feeder or tooling'){
                GPWrapper gpw = new GPWrapper(false, list_FeederGP.size() , gp);
                list_FeederGP.add(gpw);
            }
        }
    }
    private void initGPParameter(){
        Map<String , Double> map_FastenerParameter = new Map<String , Double>();
        for(Destination__c Dest : Destination__c.getAll().values()){
            map_Destination.put(Dest.Name , Dest.Outbound_Price_KG__c);
        }
        
        for(QuantityCarton__c qc : QuantityCarton__c.getAll().values()){
            map_QC.put(qc.Name , qc.Pcs_box__c);
            map_QCDuty.put(qc.Name,qc.Duty__c);
        }
        for(GrossMarginCalculation_Parameter__c gc:GrossMarginCalculation_Parameter__c.getAll().values())
        {
            map_GrossMarginCalculation_Parameter.put(gc.Name,gc.Parameter__c);
        }
    }
    private Map<String , Double> map_Destination = new Map<String , Double>();
    private Map<String , Double> map_QC = new Map<String , Double>();
    private Map<String,double> map_GrossMarginCalculation_Parameter=new Map<String,Double>();
    private Map<String,Double> map_QCDuty=new Map<String,Double>();
    public class GPWrapper{
        public Boolean IsEdit{get;set;}
        //public Boolean PorudctTypeEdit{get;set;}
        public Integer Index{get;set;}
        public Gross_Margin_Calculation__c gp{get;set;}
        public GPWrapper(Boolean e,Integer i,Gross_Margin_Calculation__c g){
            IsEdit = e ;
            //PorudctTypeEdit = pe;
            Index = i;
            gp = g;
        }
    }
}