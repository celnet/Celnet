/*
 * 作者：Ziyue
 * 时间：2013-9-17
 * 功能：客户部门关键成功因素页面的控制类
 * 2014-12-16，Sunny 拆分两个不同部门使用
*/
public class Sweet_Point_Exp_Key_Suc_FacController 
{
	public list<Sweet_Point_Explore__c> list_spes{get;set;}
	public list<Sweet_Point_Explore__c> list_spesShow{get;set;}
	public Boolean IsRead{get;set;}
	public Boolean IsAccess{get;set;}//控制Approve和Retake按钮的可见性
	public PubMethod pub{get;set;}
	public ID AccId;
	public set<ID> HighRole;
	public Boolean IsEdit{get;set;}//控制 Save和Edit按钮的可见性
	public string IsShare{get;set;}
	private String sUserType ;//TCG,ACD,Manager,登录用户类型，1.TCG销售、2.ACD销售、3.GM/Manager/Admin
	private ApexPages.StandardController controller;
	public String SelectedTeamType{get;set;}
	public Boolean ShowSelect{get;set;}
	public list<SelectOption> TeamTypes{
        get{
            list<SelectOption> options = new list<SelectOption>();
            options.add(new SelectOption('Profil','Profil'));
            options.add(new SelectOption('Jebsen-TCG','Jebsen-TCG'));
            return options;
        }
        private set;
    }
	public Sweet_Point_Exp_Key_Suc_FacController(ApexPages.StandardController controller)
	{
		this.controller = controller;
		this.initCurrentUser();
		pub = new PubMethod();
		AccId = controller.getId();
		Account acc = [select id,OwnerId,Owner.UserRoleId,KAM_Jebsen_TCG__c,KAM_Profil__c from Account where id =: AccId];
		HighRole = pub.getHighRoleName(acc.Owner.UserRoleId);
		if(HighRole.contains(UserInfo.getUserRoleId()) && acc.OwnerId != UserInfo.getUserId())
		{
			IsAccess = true;
		}
		else
		{
			IsAccess = false;
		}
		//Account Team中的销售可以编辑
		List<String> list_teamRole = new List<String>{'Sales','Key Account Manager','Project Manager'};
		Set<ID> set_userIds = new Set<ID>();
		for(Account_Team__c at:[Select Id,Team_Role__c,Team_Type__c,Team_Member__c from Account_Team__c 
			where Account__c =: AccId and Team_Role__c in: list_teamRole]){
			set_userIds.add(at.Team_Member__c);
		}
		//if(UserInfo.getUserId() == acc.OwnerId)
		if(acc.KAM_Profil__c == UserInfo.getUserId() || acc.KAM_Jebsen_TCG__c == UserInfo.getUserId() )
		{
			IsEdit = true;
		}
		else
		{
			IsEdit = false;
		}
		IsRead = true;
		this.ChangeType();
	}
	public void ChangeType(){
		this.searchRecords(SelectedTeamType);
	}
	private void searchRecords(String sType){
		String sTeamType ;
		if(sUserType == 'ACD'){
			sTeamType = 'Profil';
		}else if(sUserType == 'TCG'){
			sTeamType = 'Jebsen-TCG';
		}else if(sType != null){
			sTeamType = sType;
		}

		list_spes = new list<Sweet_Point_Explore__c>();
		list_spes = [Select IsShare__c,OwnerId,Related_Account__c, RecordTypeId, Name, Key_Success_Factors_for_sales_Marketing__c, 
		            Key_Success_Factors_for_Production__c, Key_Factors_for_R_D__c, Key_Factors_for_Purchasing__c, 
		            Key_Factors_for_Maintenance__c, Key_Factors_for_IT_Engineering_Operation__c, 
		            Key_Factors_for_Finance__c, Jebsen_s_Key_Advantage__c, Id, Team_Type__c 
		            From Sweet_Point_Explore__c where Related_Account__c=:this.controller.getId() 
		            and RecordType.DeveloperName='Customer_Value_Chain'
		            and Team_Type__c =: sTeamType];
		RecordType rt =[select id from RecordType where SobjectType='Sweet_Point_Explore__c' and DeveloperName='Customer_Value_Chain'];           
	    if(list_spes == null || list_spes.size() == 0)
	    {
	    	IsShare = 'None';
	    	Sweet_Point_Explore__c spe = new Sweet_Point_Explore__c();
	    	spe.Related_Account__c = controller.getId();
	    	spe.RecordTypeId = rt.id;
	    	spe.Team_Type__c = sTeamType;
	    	list_spes.add(spe);
	    }
	    else
	    {
	    	if(list_spes[0].IsShare__c)
	    	{
	    		IsShare = 'Is Shared';
	    	}
	    	else
	    	{
	    		IsShare = 'Is Retake';
	    	}
	    }
	}
	private void initCurrentUser(){
		this.ShowSelect = false;
		this.sUserType = 'ACD';
		User u = [Select Id,UserRole.Name,Profile.Name From User Where Id =: Userinfo.getUserId()];
		String sUserRoleName = u.UserRole.Name;
		if(sUserRoleName=='Jebsen TCG Sales' || sUserRoleName=='Jebsen TCG Manager'){
			this.sUserType = 'TCG';
		}
		if(sUserRoleName=='ACD Sales'){
			this.sUserType = 'ACD';
		}
		if(sUserRoleName=='ACD Sales Manager' || sUserRoleName=='JAT General Manager' || u.Profile.Name == 'System Administrator'){
			this.sUserType = 'Manager';
			this.SelectedTeamType = 'Profil';
			this.ShowSelect = true;
		}
	}
	public void Hold()
	{
		IsRead = true;
		upsert list_spes;
		list_spes = [Select IsShare__c,OwnerId,Related_Account__c, RecordTypeId, Name, Key_Success_Factors_for_sales_Marketing__c, 
		            Key_Success_Factors_for_Production__c, Key_Factors_for_R_D__c, Key_Factors_for_Purchasing__c, 
		            Key_Factors_for_Maintenance__c, Key_Factors_for_IT_Engineering_Operation__c, 
		            Key_Factors_for_Finance__c, Jebsen_s_Key_Advantage__c, Id 
		            From Sweet_Point_Explore__c where id =:list_spes[0].id];
	}
	public void Upd()
	{
		IsRead = false;
	}
	/*
	点击Approve按钮，customer value chain部分的所有内容都将共享给account team内的成员以只读的权限。
	此时状态变更为is shared
	*/
	public void Approve()
	{//Select s.UserOrGroupId, s.RowCause, s.ParentId, s.AccessLevel From Sweet_Point_Explore__Share s
		if(list_spes[0].id == null)
		{
			return;
		}
		list<Sweet_Point_Explore__Share> list_Share = new list<Sweet_Point_Explore__Share>();
		//for(Account_Team__c team : [select id,Team_Member__c,Account__c from Account_Team__c where Account__c =: AccId])
		for(User u :[select id from User where UserRole.Name ='Profil_Manager' or UserRole.Name ='Profil_Engineer'])
		{
			//if(team.Team_Member__c != null && list_spes[0].OwnerId != team.Team_Member__c)
			if(u.id != list_spes[0].OwnerId)
			{
				Sweet_Point_Explore__Share speShare = new Sweet_Point_Explore__Share();
				speShare.ParentId = list_spes[0].id;
				speShare.AccessLevel = 'Read';
				speShare.UserOrGroupId = u.id;//team.Team_Member__c;
				list_Share.add(speShare);
			}
		}
		if(list_Share.size() > 0)
		{
			insert list_Share;
		}
		IsShare = 'Is Shared';
		list_spes[0].IsShare__c = true;
		update list_spes[0];	
	}
	/*
	点击retake按钮，customer value chain部分的所有内容都将只共享给account owner 和其直属上级（含系统管理员）。此时状态变更为is retaked
	即：收回上述权限
	*/
	public void Retake()
	{
		if(list_spes[0].id == null)
		{
			return;
		}
		set<ID> TeamID = new set<ID>();
		//for(Account_Team__c team : [select id,Team_Member__c,Account__c from Account_Team__c where Account__c =: AccId])
		for(User u :[select id from User where UserRole.Name ='Profil_Manager' or UserRole.Name ='Profil_Engineer'])
		{
			//if(team.Team_Member__c != null && list_spes[0].OwnerId != team.Team_Member__c && list_spes[0].id != null)
			if(list_spes[0].OwnerId != u.id && list_spes[0].id != null)
			{
				TeamID.add(u.id);
			}
		}
		if(list_spes[0].id != null)
		{
			list<Sweet_Point_Explore__Share> list_Share = [select id from Sweet_Point_Explore__Share 
			                                              where UserOrGroupId in:TeamID and ParentId=:list_spes[0].id];
			if(list_Share != null && list_Share.size() > 0)
			{
				delete list_Share;
			}
		}
		IsShare = 'Is Retake';
		list_spes[0].IsShare__c = false;	
		update list_spes[0];
	}
}