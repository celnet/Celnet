/*Author:Leo
 *Date:2014-4-14
 *function:Test_Opportunity_AutoAlert_Stage
 */
@isTest
private class Test_Opportunity_AutoAlert_Stage {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Account acc = new Account();
        acc.Name = 'AUDI';
        insert acc;
        Opportunity opp = new Opportunity();
        opp.Name = 'AUDI2 opportunity';
        opp.AccountId = acc.id;
        opp.CloseDate = Date.today().addDays(-1);
        opp.StageName = 'Alleviation of Risk';
        insert opp;
        Opportunity oppp = [select Name,StageName,CloseDate from opportunity where name = :opp.Name];
        oppp.StageName = 'Lost Order';
        system.debug('****************************'+oppp.CloseDate);
        update opp;
    }
}