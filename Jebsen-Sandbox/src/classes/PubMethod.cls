/*
 * 作者：子越
 * 时间：2013-10-17
 * 描述：公共方法
*/
public class PubMethod 
{
	//根据角色ID判断是否有编辑权限
	public static Boolean IsEdit(ID RoleID)
	{
		//没有编辑权限的角色名集合
		set<string> Roles = new set<string>();
		Roles.add('Profil Manager');
		Roles.add('Profil Engineer');
 		if(RoleID != null)
		{
			UserRole ur = [select id,Name from UserRole where id=:RoleID];
			if(Roles.contains(ur.Name))
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			return true;
		}
	}
	//通过一个角色ID获取该角色的所有上级角色ID
	public set<ID> getHighRoleName(ID roleId)
	{
		set<ID> set_roleId = new set<ID>();
		set_roleId.add(roleId);
		return getHighRoleId(set_roleId);
	}
	//通过一个角色ID获取该角色的所有下级角色ID
	public set<ID> getLowRoleName(ID roleId)
	{
		set<ID> set_roleId = new set<ID>();
		set_roleId.add(roleId);
		return getLowRoleId(set_roleId);
	}
	public set<ID> getHighRoleId(set<ID> RoleIds)
	{
		Integer roleSize = RoleIds.size();
		list<UserRole> list_Roles = [Select ParentRoleId From UserRole where Id in:RoleIds];
		Boolean HaveParent = false;
		for(UserRole role : list_Roles)
		{
			if(role.ParentRoleId != null)
			{
				HaveParent = true;
				RoleIds.add(role.ParentRoleId);
			}
		}
		if(roleSize != RoleIds.size())
		{
			return getHighRoleId(RoleIds);
		}
		else
		{
			return RoleIds;
		}
	}
	public set<ID> getLowRoleId(set<ID> RoleIds)
	{
		Integer roleSize = RoleIds.size();
		list<UserRole> list_Roles = [select id from UserRole where ParentRoleId in: RoleIds];
		if(list_Roles != null && list_Roles.size() > 0)
		{
			for(UserRole role : list_Roles)
			{
				RoleIds.add(role.id);
			}
			if(RoleIds.size() == roleSize)
			{
				return RoleIds;
			}
			else
			{
				return getLowRoleId(RoleIds);
			}
		}
		else
		{
			return RoleIds;
		}
	}
}