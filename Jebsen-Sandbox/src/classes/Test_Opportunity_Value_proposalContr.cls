/**
 * 作者：Ziyue
 * 时间：2013-9-27
 * 功能：
 */
@isTest
private class Test_Opportunity_Value_proposalContr 
{
    static testMethod void myUnitTest() 
    {
        Opportunity opp = new Opportunity();
        opp.Name = 'opp';
        opp.StageName = '已赢单';
        opp.CloseDate = Date.today();
        insert opp;
        ApexPages.StandardController controller = new ApexPages.StandardController(opp);
        Opportunity_Value_proposalController proposal = new Opportunity_Value_proposalController(controller);
        proposal.Upd();
        proposal.Hold();
    }
}