/*
 * 作者：Ziyue
 * 时间：2013-10-31
 * 功能：点击opportunity中提交审批按钮时弹出该页面，填写提交审批的comments
*/
public class Opportunity_SubmitApprovalController 
{
	public ID OppId{get;set;}
	public string Comments{get;set;}//提交审批的备注信息
	public string URL{get;set;}
	public string status{get;set;}
	public Opportunity_SubmitApprovalController(ApexPages.StandardController controller)
	{
		OppId = ApexPages.currentPage().getParameters().get('oppId');
		URL = '/'+OppId;
	}
	public void Submit()
	{
		Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setObjectId(OppId);
        req.setComments(Comments);
        try
        {
        	Approval.ProcessResult result = Approval.process(req);
        }
        catch(Exception exc)
        {
        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING  , 
        	'This record does not meet the entry criteria or initial submitters of any active approval processes. Please contact your administrator for assistance.');  
         	ApexPages.addMessage(msg);
         	return;
        }
        status = 'success';
	}
}