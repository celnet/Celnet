/**
 * Author: Steven
 * Date: 2014-8-30
 * Description: Account Sales Order Request 邮件模版Controller
 */
public class Account_EmailRequestController {
	public Id accountId{get;set;}
	public String accountUrl{
		get{
			return URL.getSalesforceBaseUrl().toExternalForm() + '/' + accountId;
		}
		private set;
	}
	
	public List<LineItem> list_lineItem{
		get{
			if(list_lineItem != null){
				return list_lineItem;
			} else {
				return initSalesOrderList();
			}
		}
		private set;
	}
	
	
	public Sales_Order_Request__c TotalQtyAmount{get;set;}
	public Sales_Order_Request__c TotalQtyAmount2{get;set;}
	
	private void initTotal(){
		this.TotalQtyAmount = new Sales_Order_Request__c();
		this.TotalQtyAmount2 = new Sales_Order_Request__c();
		//this.TotalQty=0;
		this.TotalQtyAmount.Quantity__c=0;
		this.TotalQtyAmount.UtilField__c=0;
		this.TotalQtyAmount2.UtilField__c=0;
		//this.TotalAmount=0;
		if(list_lineItem != null && list_lineItem.size() > 0)
		for(LineItem li : list_lineItem){
			this.TotalQtyAmount.Quantity__c += (li.SalesOrder.Quantity__c==null?0:li.SalesOrder.Quantity__c);
			this.TotalQtyAmount.UtilField__c += (li.SalesOrder.Tax_Amount_CNY__c==null?0:li.SalesOrder.Tax_Amount_CNY__c);
			this.TotalQtyAmount2.UtilField__c += (li.SalesOrder.Total_Amount_whit_Tax_HK__c==null?0:li.SalesOrder.Total_Amount_whit_Tax_HK__c);
		}
	}
	
	private list<LineItem> initSalesOrderList(){
		list_lineItem = new list<LineItem>();
		for(Sales_Order_Request__c so : [Select s.Submitted__c, s.Archived__c, s.Account__c, s.Unit_List_Price__c, s.Total_Amount_whit_Tax_HK__c, 
				s.Tax_Amount_CNY__c, s.Quantity__c, s.Qty_Box__c, s.Product_Article__c, s.Name, s.Id, s.Description__c, s.Date__c, 
				s.Customers_Drawing_Number__c, s.Customer_Schedule_Agreement_Number__c, s.Comments__c, s.ArchivedDate__c 
				From Sales_Order_Request__c s Where Account__c =: accountId order by createddate]){
			if(!so.Archived__c){
				list_lineItem.add(new LineItem(so));
			}
		}
		initTotal();
		return list_lineItem;
	}
	public class LineItem{
        public Sales_Order_Request__c SalesOrder{get;set;}
        public Decimal TaxAmount{get{Decimal d = SalesOrder.Tax_Amount_CNY__c;return d;}set;}
        public Decimal UnitPrice{get{Decimal d = SalesOrder.Unit_List_Price__c;return d;}set;}
        public LineItem(Sales_Order_Request__c so){
        	SalesOrder = so;
        }
	}
}