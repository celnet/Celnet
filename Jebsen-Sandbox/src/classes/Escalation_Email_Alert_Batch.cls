/*Author:Leo
 *Date:2014-4-9	
 *Function:当Escalation变红时，若Escalation的金额单笔超过200万，直接发送邮件给Opp Owner、Manager（Sun Minbo）、系统管理员（Zoe），Arnie。
 *		         否则，每天定时（早上10:00或者12:00）发送邮件给Opp Owner、Manager（Sun Minbo）、系统管理员（Zoe），若一个星期后仍然没有解决，需要发邮件给上述三个人和Arnie。
 * 
 */
global class Escalation_Email_Alert_Batch implements Database.Batchable<sObject>, Database.Stateful{
	global final string query = 'Select Id, Opportunity__r.Name, Opportunity__r.Amount,Opportunity__r.Owner.Email,Opportunity__r.Owner.Name,Escalation_Type__c,Deadline__c,Opportunity_Stage__c,Status__c, CreatedDate, Escalation_Remark__c From Escalation__c Where Status__c =\'Open\'';
	List<Escalation__c> list_OverAmount = new List<Escalation__c>();
	List<Escalation__c> list_OverAmountOverWeek = new List<Escalation__c>();
	List<Escalation__c> list_NotOverWeek = new List<Escalation__c>();//未超过200w且一星期内未解决的escalation所属opportunity集合
	List<Escalation__c> list_OverWeek = new List<Escalation__c>();//未超过200w且超过一星期未解决的escalation所属opportunity集合
	Boolean isException = false;
	public Integer daysBetween = 7;
	public decimal criticalAmount=2000000;
	String msg;
	//获取系统管理员、Manager、Arnie邮箱
	User Sales_Manager = [Select UserRole.Name, UserRoleId, Id, Email From User where UserRole.Name =:'Sales Manager' limit 1];
	string Sales_Manager_Email = Sales_Manager.Email;
	User JAT_General_Manager = [Select UserRole.Name, UserRoleId, Id, Email From User where UserRole.Name =:'JAT General Manager' limit 1];
	string JAT_General_ManagerEmail = JAT_General_Manager.Email;
	User admin = [Select UserRole.Name, UserRoleId, Id, Email From User where UserRole.Name =:'Project Coordinator' limit 1];
	string admin_email = admin.Email;
	//batch查询符合条件的opp
	global Escalation_Email_Alert_Batch()
	{
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC,List<sObject> scope)
	{
		try
		{
			//将需要发送给三个人的和四个人的分别封装
			for(SObject s : scope)
			{
				Escalation__c getCreateDateEscalation = (Escalation__c)s;
				if(getCreateDateEscalation.Opportunity__r.Amount==null)
				{
					continue; 
				}
				system.debug('********test**********'+getCreateDateEscalation.Opportunity__r.Amount);
				system.debug('********test**********'+criticalAmount);
				system.debug('********test**********'+getCreateDateEscalation.CreatedDate.date().daysBetween(Date.today()));
				system.debug('********test**********'+daysBetween);
				if(getCreateDateEscalation.Opportunity__r.Amount > criticalAmount)
				{
					
					if(getCreateDateEscalation.CreatedDate.date().daysBetween(Date.today()) > daysBetween)
					{
						
						list_OverAmountOverWeek.add(getCreateDateEscalation);
						system.debug('********加入到了超过一星期的list**********');
					}
					else
					{
						list_OverAmount.add(getCreateDateEscalation);
						system.debug('********加入到了未超过一星期的list**********');
					}
				}
				else
				{
					if(getCreateDateEscalation.CreatedDate.date().daysBetween(Date.today()) > daysBetween)
					{
						
						list_OverWeek.add(getCreateDateEscalation);
						system.debug('********加入到了超过一星期的list**********');
					}
					else
					{
						list_NotOverWeek.add(getCreateDateEscalation);
						system.debug('********加入到了未超过一星期的list**********');
					}
				}
			}
			system.debug('********list_OverAmount**********' + list_OverAmount.size());
			system.debug('********list_OverWeek**********' + list_OverWeek.size());
			system.debug('********list_NotOverWeek**********' + list_NotOverWeek.size());
			//发送邮件
			//超过200w的
			for(Escalation__c esc : list_OverAmount)
			{
				sendEmailAlert(esc,false,true);
				system.debug('********超过200w的list执行了**********');
			}
			for(Escalation__c esc : list_OverAmountOverWeek)
			{
				sendEmailAlert(esc,true,true);
				system.debug('********超过200w且超过一周的list执行了**********');
			}
			//未过一星期的
			for(Escalation__c esc : list_NotOverWeek)
			{
				sendEmailAlert(esc,false,false);
				system.debug('********未一星期的list执行了**********');
			}
			//超过一星期的
			for(Escalation__c esc : list_OverWeek)
			{
				sendEmailAlert(esc,true,false);
				system.debug('********超过一星期的list执行了**********');
			}
		}
		catch(Exception ex) 
		{
			isException=true;
			msg=ex.getMessage();
		}
	}
	
	global void finish(Database.BatchableContext BC)
    {
    
    }
    
    public void sendEmailAlert(Escalation__c esca,Boolean IsOverWeek, Boolean IsOverAmount)
    {
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		String repBody = '';
		string ownerEmail = esca.Opportunity__r.Owner.Email; 
		String[] repAddress;
		string notOverWeekEmail = 'Dear, <br>'+'An Escalation record of ' + esca.Opportunity__r.Name 
								   			  +' by ' + esca.Opportunity__r.Owner.Name + ' created on '
						 					  + formatDate(esca.CreatedDate) + ' is not solved yet.'
						 					  +'<br><br>Escalation Amount:HKD ' + esca.Opportunity__r.Amount.format()
						 					  + '<br>Escalation Status:' + esca.Status__c 
						 					  + '<br>Escalation Type:' + esca.Escalation_Type__c 
						 					  + '<br>Escalation Deadline:' + esca.Deadline__c 
						 					  + '<br>Escalation Stage:' + esca.Opportunity_Stage__c 
						 					  + '<br>Escalation Remark:' + esca.Escalation_Remark__c
						 					  + '<br><br>Thanks<br><br>Salesforce';
						 					  
		string overWeekEmail = 'Dear, <br>'+'An Escalation record of ' + esca.Opportunity__r.Name 
										   +' by ' + esca.Opportunity__r.Owner.Name + ' created on '
					 					   + formatDate(esca.CreatedDate) + ' is not solved yet and more than a week.'
					 					   +'<br><br>Escalation Amount:HKD ' + esca.Opportunity__r.Amount.format()
					 					   + '<br>Escalation Status:' + esca.Status__c 
					 					   + '<br>Escalation Type:' + esca.Escalation_Type__c 
					 					   + '<br>Escalation Deadline:' + esca.Deadline__c 
					 					   + '<br>Escalation Stage:' + esca.Opportunity_Stage__c 
					 					   + '<br>Escalation Remark:' + esca.Escalation_Remark__c
					 					   + '<br><br>Thanks<br><br>Salesforce';
		//设置邮件内容
		//如果是超过200w的
		if(IsOverAmount && !IsOverWeek) 
		{
			repBody = notOverWeekEmail;
			repAddress =new string[]{ownerEmail,Sales_Manager_Email,JAT_General_ManagerEmail,admin_email};
			//repAddress =new string[]{Sales_Manager_Email,JAT_General_ManagerEmail};				   
		}
		else if(IsOverAmount && IsOverWeek)
		{
			repBody = overWeekEmail;
			repAddress =new string[]{ownerEmail,Sales_Manager_Email,JAT_General_ManagerEmail,admin_email};
		}
		//如果是不超过200w并且没有超过一星期的
		else if((!IsOverAmount) && (!IsOverWeek))
		{
			repBody += notOverWeekEmail;
			repAddress =new string[]{ownerEmail,Sales_Manager_Email,admin_email};
			//repAddress =new string[]{Sales_Manager_Email};	
		}
		//如果是超过一星期的
		else if((!IsOverAmount) && IsOverWeek)
		{
			 repBody += overWeekEmail;
			 repAddress =new string[]{ownerEmail,Sales_Manager_Email,JAT_General_ManagerEmail,admin_email};
			//repAddress =new string[]{Sales_Manager_Email,JAT_General_ManagerEmail};	
		}
		
		//设置收件人
       // String[] repAddress =new string[]{ownerEmail,Sales_Manager_Email,JAT_General_ManagerEmail,admin_email,'zoexie@jebsen.com'};
        mail.setToAddresses(repAddress);
	    mail.setHtmlBody(repBody);
	    mail.setSubject('Escalation Email Alert');
	    mail.setSenderDisplayName('Salesforce');
	//    system.debug('**************emailAddress****************'+emailAddress);
		if(ownerEmail != null || Sales_Manager_Email!= null || JAT_General_ManagerEmail!= null || admin_email!= null)
	    {
	        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	    } 
    }
    
    public string formatDate(Datetime dt)
    {
    	string result = '';
    	string year = string.valueOf((dt.year()));
    	string month = string.valueOf((dt.month()));
    	string day = string.valueof(dt.day());
    	result = year + '-' + month +  '-'+ day;
    	return result;
    }
    
}