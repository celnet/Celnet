/**
 * 作者：Ziyue
 * 时间：2013-10-22
 * 功能：clss.Account_Team_BatchAddController测试类
 */
@isTest
private class Test_Account_Team_BatchAddController 
{
    static testMethod void myUnitTest() 
    {
        Account acc = new Account();
        acc.Name = 'acc';
        insert acc;
        ApexPages.currentPage().getParameters().put('AccId',acc.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        Account_Team_BatchAddController batch = new Account_Team_BatchAddController(controller);
        batch.Add();
        batch.Hold();
        batch.Index = 1;
        batch.Del();
    }
}