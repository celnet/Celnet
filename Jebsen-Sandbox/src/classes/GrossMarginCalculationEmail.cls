public class GrossMarginCalculationEmail {
    public static String concatenateEmailHtmlBody(list<GrossMarginCalculationController.GPWrapper> list_FastenerGP, list<GrossMarginCalculationController.GPWrapper> list_FeederGP, Opportunity opp){
        String body = '';
        
        body += '<body style="font-family:Calibri">';
        body += '<p style="font-weight:bold;">Dear,</p>';
        body += '<p>You get a new GP calculation. GP calculation is less than 20%. Detail information is as followed:</p>';
        body += '</p>';
        body += '<p>Customer: ';
        body += opp.Account.Name;
        body += ' + ';
        body += opp.Name;
        body += '</p>';
        
        if(list_FastenerGP != null && list_FastenerGP.size() > 0){
            body += concatenateFastenerTable(list_FastenerGP);
            body += '<br/>';
        }
        
        if(list_FeederGP != null && list_FeederGP.size() > 0){
            body += concatenateFeederTable(list_FeederGP);
        }
        
        body += ' Only if you think this is not acceptable, please log in the Salesforce to reject it, click <a href="';
        
        String address = URL.getSalesforceBaseUrl().toExternalForm() + '/' + opp.Id;
        
        body += address;
        body += '">website here</a>.</p>';
        
        body += '<p>Thank you,</p>';
        body += '<p style="font-weight:bold;">Salesforce.com</p>';
        body += '</body>';
        
        return body;
    }
    
    private static String concatenateFastenerTable(list<GrossMarginCalculationController.GPWrapper> list_FastenerGP){
        String body = '';
        body += '<table class="list" cellspacing="0" cellpadding="0" border="0" width="100%" style="border: solid thin #B4B4B4;">';
        
        body += '<tr class="headerRow" style="background: #F2F3F3; align: center;">';
        body += '<td colspan="11" class="headerRow" align="center">';
        body += 'Gross Margin Calculation(Fastener)';
        body += '</td>';
        body += '</tr>';
        
        body += '<tr class="headerRow" style="background: #F2F3F3; align:center;">';
        body += concatenateTHeaderTdNode('rowspan="2"','&nbsp;Date');
        body += concatenateTHeaderTdNode('rowspan="2"','&nbsp;Transport');
        body += concatenateTHeaderTdNode('rowspan="2"','&nbsp;Product Article');
        body += concatenateTHeaderTdNode('rowspan="2"','&nbsp;Destination');
        body += concatenateTHeaderTdNode('colspan="3"','&nbsp;Net Value Cost');
        body += concatenateTHeaderTdNode('rowspan="2"','&nbsp;Total Inventory Cost');
        body += concatenateTHeaderTdNode('rowspan="2"','&nbsp;GP');
        body += concatenateTHeaderTdNode('rowspan="2"','&nbsp;Output VAT');
        body += concatenateTHeaderTdNode('rowspan="2"','&nbsp; Price without Tax');
        body += '</tr>';
        
        body += '<tr class="headerRow" style="background: #F2F3F3; align: center;">';
        body += concatenateTHeaderTdNode('','&nbsp;Purchase');
        body += concatenateTHeaderTdNode('','&nbsp;Logistic');
        body += concatenateTHeaderTdNode('','&nbsp;Warehousing');
        body += '</tr>';
        
        for(GrossMarginCalculationController.GPWrapper item : list_FastenerGP){
            body += '<tr>';
            body += concatenateTBodyTdNode(item.gp.Date__c.Year()+'-'+item.gp.Date__c.Month()+'-'+item.gp.Date__c.Day(), '1', '10%');
            body += concatenateTBodyTdNode(item.gp.Transport__c, '0', '6%');
            body += concatenateTBodyTdNode(item.gp.Product_Article__c, '1', '10%');
            body += concatenateTBodyTdNode(item.gp.Destination__c, '1','10%');
            body += concatenateTBodyTdNode(String.valueOf(item.gp.Purchase__c.setScale(4,system.RoundingMode.HALF_UP)), '1', '10%');
            body += concatenateTBodyTdNode(String.valueOf(item.gp.Logistic_Num__c.setScale(4,system.RoundingMode.HALF_UP)), '1', '10%');
            body += concatenateTBodyTdNode(String.valueOf(item.gp.Inventory__c.setScale(4,system.RoundingMode.HALF_UP)), '1', '10%');
            body += concatenateTBodyTdNode(String.valueof(item.gp.Total_Inventory_Cost__c.setScale(4,system.RoundingMode.HALF_UP)), '1', '7%');
            body += concatenateTBodyTdNode(item.gp.GP__c + '%', '1', '7%');
            body += concatenateTBodyTdNode(String.valueOf(item.gp.Tax__c.setScale(4,system.RoundingMode.HALF_UP)), '1', '10%');
            body += concatenateTBodyTdNode(String.valueOf(item.gp.Price_with_Tax__c.setScale(4,system.RoundingMode.HALF_UP)), '1', '8%');
            body += '</tr>';
        }
        
        body += '</table>';
        return body;
    }
    
    private static String concatenateFeederTable(list<GrossMarginCalculationController.GPWrapper> list_FeederGP){
        String body = '';
        body += '<table class="list" cellspacing="0" cellpadding="0" border="0" width="100%" style="border: solid thin #B4B4B4;">';
        body += '<tr class="headerRow" style="background: #F2F3F3; align: center;">';
        body += '<td colspan="9" class="headerRow" align="center">Gross Margin Calculation(Feeder)</td>';
        body += '</tr>';
        
        body += '<tr class="headerRow" style="background: #F2F3F3; align: center;">';
        
        body += concatenateTHeaderTdNode('rowspan="2"', '&nbsp;Date');
        body += concatenateTHeaderTdNode('rowspan="2"', '&nbsp;Product Article');
        body += concatenateTHeaderTdNode('colspan="3"', '&nbsp;Net Value Cost');
        body += concatenateTHeaderTdNode('rowspan="2"','&nbsp;Total Inventory Cost');
        body += concatenateTHeaderTdNode('rowspan="2"', '&nbsp;GP');
        body += concatenateTHeaderTdNode('rowspan="2"', '&nbsp;Output VAT');
        body += concatenateTHeaderTdNode('rowspan="2"', '&nbsp;Price without Tax');
        body += '</tr>';
        
        body += '<tr class="headerRow"  style="background:#F2F3F3;align:center;">';
        body += concatenateTHeaderTdNode('rowspan="1"','&nbsp;Purchase');
        body += concatenateTHeaderTdNode('colspan="1"','&nbsp;Logistic');
        body += concatenateTHeaderTdNode('rowspan="1"','&nbsp;Warehousing');
        body += '</tr>';
        /*
        body += '<tr class="headerRow" style="background: #F2F3F3; align: center;">';
        body += concatenateTHeaderTdNode('', '&nbsp;Inbound logistic');
        body += concatenateTHeaderTdNode('', '&nbsp;Outbound logistic');
        body += '</tr>';
        */
        for(GrossMarginCalculationController.GPWrapper item : list_FeederGP){
            body += '<tr>';
            body += concatenateTBodyTdNode(item.gp.Date__c.Year()+'-'+item.gp.Date__c.Month()+'-'+item.gp.Date__c.Day(), '1', '10%');
            body += concatenateTBodyTdNode(item.gp.Product_Article__c, '1', '9%');
            //body += concatenateTBodyTdNode(String.valueOf(item.gp.Inbound_Logistic_Cost__c.setScale(2,system.RoundingMode.HALF_UP)), '1', '9%');
            //body += concatenateTBodyTdNode(String.valueOf(item.gp.Warehousing_Cost__c.setScale(2,system.RoundingMode.HALF_UP)), '1', '9%');
            //body += concatenateTBodyTdNode(String.valueOf(item.gp.Outbound_Logistic_cost__c.setScale(2,system.RoundingMode.HALF_UP)), '1', '9%');
            body += concatenateTBodyTdNode(String.valueOf(item.gp.Purchase__c.setScale(2,system.RoundingMode.HALF_UP)), '1', '9%');
            body += concatenateTBodyTdNode(String.valueOf(item.gp.Logistic_Num__c.setScale(2,system.RoundingMode.HALF_UP)), '1', '9%');
            body += concatenateTBodyTdNode(String.valueOf(item.gp.Inventory__c), '1', '9%');
            body += concatenateTBodyTdNode(String.valueof(item.gp.Total_Inventory_Cost__c.setScale(4,system.RoundingMode.HALF_UP)), '1', '9%');
            body += concatenateTBodyTdNode(String.valueOf(item.gp.GP__c) + '%', '1', '9%');
            body += concatenateTBodyTdNode(String.valueOf(item.gp.Tax__c.setScale(2,system.RoundingMode.HALF_UP)), '1', '8%');
            body += concatenateTBodyTdNode(String.valueOf(item.gp.Price_with_Tax__c.setScale(2,system.RoundingMode.HALF_UP)), '1', '8%');
            body += '</tr>';
        }
        
        body += '</table>';
        return body;
    }
    
    private static String concatenateTHeaderTdNode(String attribute, String value){
        String td = '';
        td += '<td class="headerRow" align="center" ';
        td += attribute;
        td += ' style="border-top: solid thin #B4B4B4; border-left-color: #B4B4B4;">';
        td += value;
        td += '</td>';
        return td;
    }
    
    private static String concatenateTBodyTdNode(String value, String paddingRight, String width){
        String td = '';
        td += '<td class="dataCell" style="padding-left: 1; padding-right: ';
        td += paddingRight;
        td += '; border-left-color: #B4B4B4; border-top-color: #B4B4B4; width: ';
        td += width;
        td += '">';
        td += value;
        td += '</td>';
        return td;
    }
}