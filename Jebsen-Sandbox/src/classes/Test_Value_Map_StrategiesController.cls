/**
 * 作者：Ziyue
 * 时间：2013-9-27
 * 功能：
 */
@isTest
private class Test_Value_Map_StrategiesController 
{
    static testMethod void myUnitTest() 
    {
        Opportunity opp = new Opportunity();
        opp.Name = 'opp';
        opp.StageName = '已赢单';
        opp.CloseDate = Date.today();
        insert opp;
        ApexPages.StandardController controller = new ApexPages.StandardController(opp);
        Value_Map_StrategiesController strate = new Value_Map_StrategiesController(controller);
        strate.getCompetitive_Standing();
        strate.getImportance_to_Customer();
        strate.getOptions();
        strate.Add();
        strate.Add();
        strate.Index = 1;
        strate.Del();
        strate.Upd();
        strate.Hold();
    }
}