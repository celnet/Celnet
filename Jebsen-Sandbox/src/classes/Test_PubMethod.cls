/**
 * 作者：Ziyue
 * 时间：2013-10-22
 * 描述：class.PubMethod测试类
 */
@isTest
private class Test_PubMethod 
{
    static testMethod void myUnitTest() 
    {
        PubMethod method = new PubMethod();
        method.getHighRoleName(UserInfo.getUserRoleId());
        method.getLowRoleName(UserInfo.getUserRoleId());
    }
}