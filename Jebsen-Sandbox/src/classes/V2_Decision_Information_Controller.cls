/*Author:Leo
 *Date:2014-4-1
 *Function:V2_Decision_Information_Controller visual force page extensions
 */
public with sharing class V2_Decision_Information_Controller {
	public Opportunity Opp{get;set;}
	public V2_Decision_Information_Controller(ApexPages.standardController controller)
	{
		//获取当前opportunity的id并将相关信息赋值给opp
		Opp = (Opportunity)controller.getRecord();
		opp = [Select id, Other_reason_for_winning_or_losing__c, Main_reason_for_winning_or_losing__c, Decision_Result__c, Decision_Lost_Type__c, Decision_Competitor__c 
			   From Opportunity where id=:Opp.Id];
	}
	
	public void SaveOpp()
	{
		if(Opp.Decision_Lost_Type__c != 'Lose To Competitor')
		Opp.Decision_Competitor__c =null;
		update Opp;
	}
	
	
}