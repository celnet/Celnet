/**
 * Test Class of Auto FillOutCompetitor Name
 */
@isTest
private class Test_AutoFillOutCompetitorName {

    static testMethod void myUnitTest() {
        Opportunity opp = new Opportunity();
        opp.Name = 'opp';
        opp.StageName = 'Awareness of Needs';
        opp.CloseDate = Date.today().addMonths(1);
        insert opp;
        try
        {
	        opp.StageName = 'Implementation(Win)';
	        update opp;
        }
        catch(Exception exc){}
    }
}