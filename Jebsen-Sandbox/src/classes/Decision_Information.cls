/*Author:Leo
 *Date:2014-4-1
 *Function:Decision_Information visual force page extensions
 */
public with sharing class Decision_Information {
	public Boolean IsDisabled{get;set;}{IsDisabled = true;}//是否无用
	public Opportunity opp {get;set;}{opp = new Opportunity();}
	Opportunity temp_opp;
	
	public void Edit()
	{
		IsDisabled = false;
	}
	
	public void Cancel()
	{
		IsDisabled = true;
		if(temp_opp.id != null)
		{
			opp = [Select id, Other_reason_for_winning_or_losing__c, Main_reason_for_winning_or_losing__c, Decision_Result__c, Decision_Lost_Type__c, Decision_Competitor__c 
			   From Opportunity where id=:temp_opp.Id];
		}
	}
	
	public Decision_Information(ApexPages.standardController controller)
	{
		//获取当前opportunity的id并将相关信息赋值给opp
		temp_opp = (Opportunity)controller.getRecord();
		opp = [Select id, Other_reason_for_winning_or_losing__c, Main_reason_for_winning_or_losing__c, Decision_Result__c, Decision_Lost_Type__c, Decision_Competitor__c 
			   From Opportunity where id=:temp_opp.Id];
	}
	
	public void save()
	{
		Opportunity update_opp = [Select id, Other_reason_for_winning_or_losing__c, Main_reason_for_winning_or_losing__c, Decision_Result__c, Decision_Lost_Type__c, Decision_Competitor__c 
			   From Opportunity where id=:temp_opp.Id];
		update_opp.Main_reason_for_winning_or_losing__c = opp.Main_reason_for_winning_or_losing__c;
		update_opp.Other_reason_for_winning_or_losing__c = opp.Other_reason_for_winning_or_losing__c;
		update_opp.Decision_Competitor__c = opp.Decision_Competitor__c;
		update_opp.Decision_Lost_Type__c = opp.Decision_Lost_Type__c;
		update_opp.Decision_Result__c = opp.Decision_Result__c;
		update update_opp;
	}
	
	
}