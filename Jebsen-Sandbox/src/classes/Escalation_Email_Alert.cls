/*Author:Leo
 *Date:2014-4-9	
 *Function:当Escalation变红时，若Escalation的金额单笔超过200万，直接发送邮件给Opp Owner、Manager（Sun Minbo）、系统管理员（Zoe），Arnie。
 *		         否则，每天定时（早上10:00或者12:00）发送邮件给Opp Owner、Manager（Sun Minbo）、系统管理员（Zoe），若一个星期后仍然没有解决，需要发邮件给上述三个人和Arnie。
 *
 */
global class Escalation_Email_Alert implements Schedulable{
	global void execute(SchedulableContext sc) {
      Escalation_Email_Alert_Batch eeab = new Escalation_Email_Alert_Batch(); 
      database.executebatch(eeab,1); 
   }
	
}