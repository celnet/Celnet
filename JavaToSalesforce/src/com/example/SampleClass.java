package com.example;

import com.sforce.soap.enterprise.Connector;
import com.sforce.soap.enterprise.DeleteResult;
import com.sforce.soap.enterprise.EnterpriseConnection;
import com.sforce.soap.enterprise.Error;
import com.sforce.soap.enterprise.QueryResult;
import com.sforce.soap.enterprise.SaveResult;
import com.sforce.soap.enterprise.sobject.Account;
import com.sforce.soap.enterprise.sobject.Contact;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;


public class SampleClass {
	static final String USERNAME = "steven@heroku.com";
	static final String PASSWORD = "online2014";
	
	static EnterpriseConnection connection;
	
	public static void main(String[] args){
		ConnectorConfig config = new ConnectorConfig();
		config.setUsername(USERNAME);
		config.setPassword(PASSWORD);
		// config.setTraceMessage(true);
		
		try{
			connection = Connector.newConnection(config);
			
			// display some current settings
			System.out.println("Auth EndPoint: " + config.getAuthEndpoint());
			System.out.println("Service EndPoint: " + config.getServiceEndpoint());
			System.out.print("Username: " + config.getUsername());
			System.out.println("SessionId: " + config.getSessionId());
			
			// run the different examples
			queryContacts();
			createAccounts();
			updateAccounts();
			
			
		} catch (ConnectionException e1){
			e1.printStackTrace();
		}
		
	}
	
	private static void queryContacts(){
		System.out.println("Querying for the 5 newest Contacts...");
		
		try{
			// query for the 5 newest contacts
			QueryResult queryResults = connection.query("SELECT Id, FirstName, LastName, Account.Name "
					+ "FROM Contact WHERE AccountId != NULL ORDER BY CreatedDate DESC LIMIT 5");
			if(queryResults.getSize() > 0){
				for (int i = 0; i< queryResults.getRecords().length;i++){
					// cast the SObject to a strongly-typed Contact
					Contact c = (Contact)queryResults.getRecords()[i];
					System.out.println("Id: " + c.getId() + " - Name: " + c.getFirstName() + " " + c.getLastName() + 
							" - Account: " + c.getAccount().getName());
				}
			}
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	// create 5 test Accounts
	private static void createAccounts(){
		System.out.println("Creating 5 new test Accounts...");
		Account[] records = new Account[5];
		
		try{
			// create 5 test accounts
			for(int i = 0; i<5;i++){
				Account a = new Account();
				a.setName("Test Account " + i);
				a.setActive__c("Yes");
				records[i] = a;
			}
			
			// create the records in Salesforce.com
			SaveResult[] saveResults = connection.create(records);
			
			// check the returned results for any errors
			for(int i = 0; i< saveResults.length;i++){
				if(saveResults[i].isSuccess()){
					System.out.println(i + ". Successfully created record - Id: " + saveResults[i].getId());
				} else {
					Error[] errors = saveResults[i].getErrors();
					for(int j = 0; j < errors.length; j++){
						System.out.println("ERROR creating record: " + errors[j].getMessage());
					}
				}
			}
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	// updates the 5 newly created Accounts
	private static void updateAccounts(){
		System.out.println("Update the 5 new test Accounts...");
		Account[] records = new Account[5];
		
		try {
			QueryResult queryResults = connection.query("SELECT Id, Name FROM Account ORDER BY " + "CreatedDate DESC LIMIT 5");
			if(queryResults.getSize() > 0){
				for( int i = 0; i<queryResults.getRecords().length;i++){
					// cast the SObject to a strongly-typed Account
					Account a = (Account)queryResults.getRecords()[i];
					
					System.out.println("Updating Id: " + a.getId() + " - Name: " + a.getName());
					
					// modify the name of the Account
					a.setName(a.getName().replace("UPDATED", ""));
					//a.setActive__c("");
					
					String[] fieldsToNull = new String[2];
					fieldsToNull[0] = "Active__c";
					fieldsToNull[1] = "AccountSource";
					
					a.setFieldsToNull(fieldsToNull);
					records[i] = a;
				}
			}
			
			// update the records in Salesforce.com
			SaveResult[] saveResults = connection.update(records);
			
			// check the returned results for any errors
			for (int i = 0; i< saveResults.length; i++){
				if(saveResults[i].isSuccess()){
					System.out.println(i + ". Successfully updated record - Id: " + saveResults[i].getId());
				} else {
					Error[] errors = saveResults[i].getErrors();
					for( int j = 0; j< errors.length; j++){
						System.out.println("ERROR updating record: " + errors[j].getMessage());
					}
				}
			}
			
		} catch(Exception e){
			e.printStackTrace();
		}
	}
}
