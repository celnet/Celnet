trigger TigOnWorkOrder_AutoAddWorkOrderProduct on Work_Order__c (after insert) {
    //1.新建work order product
    if(Trigger.isInsert){
        List<RecordType> rtList=[Select r.SobjectType, r.Name, r.Id 
                    From RecordType r 
                    where r.SObjectType='Work_Order__c' and (not r.name like '%on site%')]; 
        Map<Id,RecordType> rtIdMap=new Map<Id,RecordType>();
        for(RecordType rt:rtList){
            rtIdMap.put(rt.id,rt);
        }
        Map<Id,Work_Order__c> woIdWOMap=new Map<Id,Work_Order__c>();
        List<Work_Order__c> woList=new List<Work_Order__c>();
        
        for(Work_Order__c wo:Trigger.new){
            //woIdWOMap.put(wo.id,wo);
            //wo.addError('测试:wo.id='+wo.id);
            if(wo.RecordTypeId!=null){
                if(rtIdMap.containsKey(wo.RecordTypeId)){
                    woList.add(wo);
                }
            }
        }
        /**
        List<Work_Order__c> woListTmp=[select id,name,SFP_No__c,RecordTypeId,RecordType.Id 
            from Work_Order__c 
            where RecordType.Id in:rtList];//获取所有要修改的WO
        for(Work_Order__c wo:woListTmp){
            if(woIdWOMap.containsKey(wo.id)){
                woList.add(wo);
            }
        }
        */
        /**
        if(woList!=null)
            Trigger.new[0].addError('测试:woList size='+woList.size());
        else
            Trigger.new[0].addError('测试:woList=null');
        */
        Map<Id,Id> woIdSFPMap=new Map<Id,Id>();//woid,sfpid //每个WO对应的SFP Id
        List<Id> sfpIdList=new List<Id>();
        for(Work_Order__c wo:woList){
            if(wo.SFP_No__c!=null){
                woIdSFPMap.put(wo.id,wo.SFP_No__c);
                sfpIdList.add(wo.SFP_No__c);
            }
        }
        
        List<SFP_Product__c> sfpProductList=[Select s.SFP__c
            , s.Contract_Product_PO__r.Id, s.Contract_Product_PO__c 
            , s.Contract_Product_PO__r.Quantity__c, s.Quantity__c
            From SFP_Product__c s
            where s.SFP__c in :sfpIdList]; //WO对应的所有的SFP_Product__c
        Map<Id,List<SFP_Product__c>> woIdSFPProListMap=new Map<Id,List<SFP_Product__c>>();//woid, 
        List<SFP_Product__c> sfpListForWO=null;//临时单个WO对应的SFP_Product__c队列
        
        for(Work_Order__c wo:woList){
            if(woIdSFPProListMap.containsKey(wo.id))
                sfpListForWO=woIdSFPProListMap.get(wo.id);
            else
                sfpListForWO=new List<SFP_Product__c>();
            for(SFP_Product__c sfpPro:sfpProductList){
                if(sfpPro.SFP__c==wo.SFP_No__c){
                    sfpListForWO.add(sfpPro);
                }
            }
            woIdSFPProListMap.put(wo.id,sfpListForWO);//设置单个WO相应的SFP_Product__c队列
        }
        
        List<Work_Order_Product__c> wopList=new List<Work_Order_Product__c>();
        for(Work_Order__c wo:Trigger.new){
            if(woIdSFPMap.containsKey(wo.id)){//适合/需要修改的WO
                //Select w.Work_Order__c, w.Quantity__c, w.Contract_Product_PO__c From Work_Order_Product__c w
                sfpListForWO=woIdSFPProListMap.get(wo.id);
                for(SFP_Product__c sfpPro:sfpListForWO){
                    wopList.add(
                        new Work_Order_Product__c(
                             Contract_Product_PO__c=sfpPro.Contract_Product_PO__r.Id
                            ,Work_Order__c=wo.id
                            ,Quantity__c=sfpPro.Quantity__c
                    ));
                }
            }
        }
        if(wopList!=null && wopList.size()>0)insert(wopList);
    }
}