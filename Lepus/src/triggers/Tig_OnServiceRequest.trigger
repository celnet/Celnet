/**************************************************************************************************
 * Name             : Tig_OnServiceRequest
 * Object           : Case
 * Requirement      : 
 * Purpose          : 
 * Author           : Iverson gao
 * Create Date      : 07/12/2012
 * Modify History   : 
 * 2013-1-30 Polaris Yu
 *      1. Replace accIdList with accIdSet to avoid duplicates
 *      2. Add try...catch to make the error messages easier to read
***************************************************************************************************/ 
trigger Tig_OnServiceRequest on Case(before insert, after update){
    Set<Id> accIdSet = new Set<Id>();
    Map<Id, Account> accMap = new Map<Id, Account>();
    
    // New Service Request
    if(trigger.isBefore && trigger.isInsert && !CommonConstant.changedFromAccount){
        accIdSet = new Set<Id>();
        accMap = new Map<Id, Account>();
        
        for(Case sr : trigger.new){
            accIdSet.add(sr.AccountId);
        }
        accMap = new Map<Id, Account>([SELECT Id, Region_Dept__c FROM Account WHERE Id IN: accIdSet]);
        
        // Get region dept information from related account
        if (accMap.size() > 0){
            for (Case sr : trigger.new){
                sr.Region_Dept__c = accMap.get(sr.AccountId).Region_Dept__c;
            }
        }
    }
    
    // Update Sevice Request
    if(trigger.isAfter && trigger.isUpdate && !CommonConstant.changedFromAccount){
        accIdSet = new Set<Id>();
        accMap = new Map<Id, Account>();
        Set<Id> newSRIdSet = new Set<Id>();
        Map<Id, String> oldAccMap = new Map<Id, String>();
        
        // Used in the comparison of the old and new related Account on Service Request
        for (Case oldSR : trigger.old){
            oldAccMap.put(oldSR.Id, oldSR.AccountId);
        }
        for (Case sr : trigger.new){
            // Judge whether the related account is changed
            if (sr.AccountId != oldAccMap.get(sr.Id)){
                accIdSet.add(sr.AccountId);
                newSRIdSet.add(sr.Id);
            }
        }
        
        List<Case> newSRList = [SELECT Id, AccountId, Region_Dept__c FROM Case
                                WHERE Id IN: newSRIdSet];
        
        if (newSRList.size() > 0){
            accMap = new Map<Id, Account>([SELECT Id, Region_Dept__c FROM Account
                                           WHERE Id IN: accIdSet]);
            
            // Store the related work orders which will be updated later
            List<Work_Order__c> relatedWOList = [SELECT Id, Service_Request_No__c, Account_Name__c,
                                                 Region_Dept__c FROM Work_Order__c
                                                 WHERE Service_Request_No__c IN: newSRIdSet];
            
            // Update the region dept information of service request and related work orders
            if (accMap.size() > 0){
                Set<Id> woIdSet = new Set<Id>();
                
                for (Case sr : newSRList){
                    sr.Region_Dept__c = accMap.get(sr.AccountId).Region_Dept__c;
                               
                    if (relatedWOList.size() > 0){
                        for (Work_Order__c wo : relatedWOList){
                            if (wo.Service_Request_No__c == sr.Id){
                                woIdSet.add(wo.Id);
                                wo.Account_Name__c = sr.AccountId;
                                wo.Region_Dept__c = sr.Region_Dept__c;
                            }
                        }
                    }
                }
                try{
                    update relatedWOList;
                }
                catch(DMLException e){
                    trigger.new[0].addError(e.getDMLMessage(0));
                }
            }
            try{
                update newSRList;
            }
            catch(DMLException e){
                trigger.new[0].addError(e.getDMLMessage(0));
            }
        }
    }
    System.debug('in the Tig_OnServiceRequest method:--------------' + Datetime.now().getTime());
}