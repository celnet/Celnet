/*
根据工单上的服务区域，自动匹配关单审批人
*@Author:He Wei
*Create Date：2014-4-21
*/

trigger WOCloseApprove on Work_Order__c (before update) {
	new Triggers()
    .bind(Triggers.Evt.beforeupdate,new WOCloseApproveHandler())    
    .manage();
}