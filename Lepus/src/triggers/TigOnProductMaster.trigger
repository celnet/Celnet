/**************************************************************************************************
 * Name             : TigOnProductMaster
 * Object           : Opportunity, Project_Product__c, Product_HS__c
 * Requirement      : Cretiera Base Sharing Rule based on Opportunity
 * Purpose          : When Product Master Level is changed, so we need to update the 
 *                    Opportunity Level of correspoding Opportunities
 * Author           : Mouse Liu
 * Create Date      : 05/16/2012
 * Modify History   : 
 * 
***************************************************************************************************/
trigger TigOnProductMaster on Product_HS__c (after update) {
    if (trigger.isAfter && trigger.isUpdate) {
        List<Product_HS__c> products = [SELECT Id, Level_1__c,
                                           (SELECT Project_Name__c
                                            FROM Project_Product__r)
                                        FROM Product_HS__c
                                        WHERE Id IN :trigger.new];
        
        // Exclude the Product Master which Level_1__c has no change,
        //     and then get the Ids of the related Opportunity
        Set<Id> oppIds = new Set<Id>();
        for (Product_HS__c newProductMaster : products) {
            Product_HS__c oldProductMaster = trigger.oldMap.get(newProductMaster.Id);
            if (newProductMaster.Level_1__c != oldProductMaster.Level_1__c) {
                for (Project_Product__c p : newProductMaster.Project_Product__r) {
                    oppIds.add(p.Project_Name__c);
                }
            }
        }
        
        // We need to follow the logic of Project Product trigger
        //     after the Project Product or Product Master "after action" is finished,
        //     just recalculate the Opportunity Level of the corresponding Opportunities
        if (oppIds.size() > 0) {
            UtilProjectProduct.processOpportunityLevel(oppIds);
        }
    }
}