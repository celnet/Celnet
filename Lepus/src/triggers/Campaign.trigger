trigger Campaign on Campaign (before update) {
	new Triggers()
	.bind(Triggers.Evt.beforeupdate,new CampaignFindApproverOversea())
	.manage();
}

	/*
	for (Campaign campaign : (List<Campaign>) trigger.new) {
		system.debug('testtest');
		if (campaign.Status == 'Request for Approval' 
				&& ((Campaign)trigger.Oldmap.get(campaign.id)).Status != 'Request for Approval') {
			String sApproverMapRecordTypeName = 'HW_Campaign_Approver';
			Id ApproverMapRecordType = [select id from RecordType where DeveloperName =: sApproverMapRecordTypeName].id;
	        User currentUser = [Select Id, Name,Region_Office__c FROM User where Id = : UserINfo.getUserId()];
	        List<Account_Link_Approver_Map__c> approvers = [Select Approver__c FROM Account_Link_Approver_Map__c 
	                                                         where Representative_Office__c = : currentUser.Region_Office__c
	                                                         and RecordTypeId = :ApproverMapRecordType];
	        if(approvers.size() > 0){
	            User currentApprover = [Select IsActive, Name FROM User where Id = : approvers[0].Approver__c];
	            if(!currentApprover.IsActive) {
					campaign.addError('The approver "' + currentApprover.Name + '" is inactive, please contact the administrator.');
	            } else {
	                campaign.Regional_Approver__c = approvers[0].Approver__c;
	            }
	        } else {
				campaign.addError('No approver maintenance for your region "'+currentUser.Region_Office__c + '", please contact the administrator.');
	        }
		}
	}
	*/