/**************************************************************************************************
 * Name             : TigOnSalesTeam
 * Object           : Account, Opportunity
 * Requirement      : When creating Opportunity Sales Team, system send email to the new memeber
 * Purpose          : 1. Create a custom object [Sales Team]
 *                    2. When creating the new team member on the custom object, sync with standard
 *                       object and copy the email of user to the custom field [Team Email]
 *                    3. When deleting and update, sync the two object records
 *                    4. Create a workflow to send the email alert
 * Author           : Tulipe Lee
 * Create Date      : 05/19/2012
 * Modify History   : 
 * 
***************************************************************************************************/
trigger TigOnSalesTeam on Sales_Team__c (after delete, after insert, after update, before insert) {
    // Before Insert, we need to copy the email of User field
    //     to Team Email field, so that Email Alert can use it.
    if (trigger.isInsert && trigger.isBefore) {
        UtilSalesTeam.updateTeamEmailAfterInsert();
    }
    
    // Create the OpportunityTeamMember according to the custom Sales Team records
    if (trigger.isInsert && trigger.isAfter) {
        UtilSalesTeam.createOpportunityTeamMember();
    }
    
    // When updating the Opportunity Access Level field of custom Sales Team records
    //  update the corresponding OpportunityShare records
    if (trigger.isUpdate && trigger.isAfter) {
        UtilSalesTeam.updateOpportunityTeamMember();
    }
    
    // When deleting the custom Sales Team records, delete the corresponding OpportunityTeamMember
    if (trigger.isDelete && trigger.isAfter) {
        UtilSalesTeam.deleteOpportunityTeamMember();
    }
}