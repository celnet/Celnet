/**
 * @Purpose : The account trigger 
 * @Author : BLS
 * @Date : 2014-07-01 Steven Refactor
 */ 
trigger Lead on Lead (before insert,after insert,before update, after update, after delete) {
    new Triggers()
    	.bind(Triggers.Evt.beforeinsert, new Lepus_WisdomLeadWriteBackHandler())
    	.bind(Triggers.Evt.beforeupdate, new Lepus_WisdomLeadWriteBackHandler())
    
        .bind(Triggers.Evt.afterinsert,new Lepus_LeadHandler()) // 同步到EIP
        .bind(Triggers.Evt.afterupdate,new Lepus_LeadHandler()) // 同步到EIP
        .bind(Triggers.Evt.afterdelete,new Lepus_LeadHandler()) // 同步到EIP
        
        .bind(Triggers.Evt.afterinsert,new Lepus_SetGlobalIdHandler()) // add by sunda 20140815 设置Global ID 字段
        
        .bind(Triggers.Evt.beforeinsert,new OverseaLeadDistributeHandler()) //(海外)导入的线索根据LDR培育状态，自动分发给对应区域的接口人
        .bind(Triggers.Evt.beforeupdate,new OverseaLeadDistributeHandler()) //(海外)线索的LDR培育状态发生变化，自动分发给对应区域的接口人
        
        .bind(Triggers.Evt.beforeinsert,new ChinaLeadDistributeHandler()) //(中国区)非一线创建的线索，自动分发给对应区域销管
        .bind(Triggers.Evt.beforeupdate,new ChinaLeadDistributeHandler()) //(中国区)待分发线索如果修改了区域，自动分发给对应区域销管

        .bind(Triggers.Evt.beforeupdate,new ChinaLeadBeforeUpdateHandler()) //(中国区)跟进中的线索关闭或转机会点等处理逻辑
    
        .bind(Triggers.Evt.beforeinsert,new Lepus_SetIsNamedAccountHandler()) // add by sunda 20140904 设置Is Named Account 字段
        .bind(Triggers.Evt.beforeupdate,new Lepus_SetIsNamedAccountHandler()) // add by sunda 20140904 设置Is Named Account 字段
    
        .bind(Triggers.Evt.afterinsert, new ChinaLeadBusinessLeaderShareHandler()) //(中国区)线索创建时共享给商业leader
        .bind(Triggers.Evt.afterupdate, new ChinaLeadBusinessLeaderShareHandler()) //(中国区)线索如果修改了Owner共享给商业leader
    
    	.bind(Triggers.Evt.afterdelete, new LeadArchiveHandler()) // archive 
        .manage();
}