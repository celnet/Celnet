/**
 * The trigger on namedaccount,the inserted named account record doesn't allow edit,so only handle the 
 * before insert and after insert situation
 * Author           : BLS
 * Create Date      : 2013/3
 *     
 */               
trigger NamedAccount on Named_Account__c (before insert, after insert){
	new Triggers()
			//set the Record_Description__c field if it is null
		.bind(Triggers.Evt.beforeinsert,new NamedAccountBeforeInsertHandler())
		.bind(Triggers.Evt.afterinsert,new NamedAccountAfterInsertHandler())//update the account's na related fields
		.manage();
}