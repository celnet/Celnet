trigger Contact on Contact (after delete, after insert, after update, before update) {
    new Triggers()
        .bind(Triggers.Evt.afterinsert,new Lepus_SetGlobalIdHandler()) // add by sunda 20140815 设置Global ID 字段
    
        .bind(Triggers.Evt.afterdelete, new ContactArchiveForBIHandler()) //(ALL)删除后将记录保存到数据维护历史对象上，供BI抓取
        
        .bind(Triggers.Evt.afterinsert, new Lepus_ContactHandler()) // 同步到EIP
        .bind(Triggers.Evt.afterupdate, new Lepus_ContactHandler()) // 同步到EIP
        .bind(Triggers.Evt.afterdelete, new Lepus_ContactHandler()) // 同步到EIP
        
        .manage();
}