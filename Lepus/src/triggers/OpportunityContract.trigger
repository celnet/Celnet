trigger OpportunityContract on Opportunity_Contract__c (after insert) {
	new Triggers()
		.bind(Triggers.Evt.afterinsert, new Lepus_OpportunityContractHandler()) // add by kejun 2014-9-3 Contract创建时发送线索同步到EIP
		
		.manage();
}