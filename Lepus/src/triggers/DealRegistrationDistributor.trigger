/*
* @Purpose : 海外报备创建时给总代联系人发送邮件提醒；
* @Author : steven
* @Date :		2013-11-15
*/
trigger DealRegistrationDistributor on Deal_Registration_Distributor__c (after insert) {
	new Triggers()
		.bind(Triggers.Evt.afterinsert,new DeadDistributorsHandler())
		.manage(); 
}