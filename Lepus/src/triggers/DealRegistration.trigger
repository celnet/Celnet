trigger DealRegistration on Deal_Registration__c (after update,before update) {
    new Triggers()
        .bind(Triggers.Evt.afterupdate,new DealRegistrationSharingHandler())
        .bind(Triggers.Evt.afterupdate,new DealRegistrationUpdateHandler())
        .bind(Triggers.Evt.beforeupdate,new DealRegistrationUpdateHandler())
        .manage(); 
}