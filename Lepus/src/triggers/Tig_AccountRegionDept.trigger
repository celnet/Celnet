/**********************************************************************/
/* Tig_AccountRegionDept
/* Author: jojo
/* Date: 2012/7/12
/* Description: 
/*    After updating account's [Region_Dept__c], all sub objects' 
/*    [Region_Dept__c] will also be updated.
/* Modify History:
/* 2013-1-30 Polaris Yu
/*	   1. Add criteria: is overseas account; region dept is changed
/*	   2. Replace curAccIdSet with curAccRegionDeptMap.keySet()
/*	   3. Add try...catch to make the error messages easier to read
/**********************************************************************/
trigger Tig_AccountRegionDept on Account (after insert, after update) {
	Map<Id, String> curAccRegionDeptMap = new Map<Id, String>();
	
    if(trigger.isAfter && trigger.isUpdate){
    	CommonConstant.changedFromAccount = true;
        for (Account acc : trigger.new){
        	Account oldAcc = trigger.oldMap.get(acc.Id);
        	// Judge whether the account is an overseas account and whether its
        	//   region dept has been changed
        	if (acc.RecordType_Name__c.contains('HW-(Int CSS)')
        		&& acc.Region_Dept__c != oldAcc.Region_Dept__c){
        		curAccRegionDeptMap.put(acc.Id, acc.Region_Dept__c);
        	}
        }
        
        
        //Sync Contact
        List<Contact> relatedContactList = [SELECT Id, Region_Dept__c, AccountId
        									FROM Contact
        									WHERE AccountId
        									IN: curAccRegionDeptMap.keySet()];
        if (relatedContactList.size() > 0){
        	for (Contact contact : relatedContactList){
        		contact.Region_Dept__c = curAccRegionDeptMap.get(contact.AccountId);
        	}
        	try{
        		update relatedContactList;
        	}
        	catch(DMLException e){
        		trigger.new[0].addError(e.getDMLMessage(0));
        	}
        }
        
        //Sync Service Contract
        List<CSS_Contract__c> relatedContractList = [SELECT Id, Region_Dept__c, Account_Name__c
        											 FROM CSS_Contract__c
        											 WHERE Account_Name__c
        											 IN: curAccRegionDeptMap.keySet()];
        Set<Id> relatedContractIdSet = new Set<Id>();
        Map<Id, String> contractRegionMap = new Map<Id, String>();
        if (relatedContractList.size() > 0){
        	for (CSS_Contract__c contract : relatedContractList){
        		contract.Region_Dept__c = curAccRegionDeptMap.get(contract.Account_Name__c);
        		relatedContractIdSet.add(contract.Id);
        		contractRegionMap.put(contract.Id, contract.Region_Dept__c);
        	}
        	try{
        		update relatedContractList;
        	}
        	catch(DMLException e){
        		trigger.new[0].addError(e.getDMLMessage(0));
        	}
        }
        
        //Sync SFP
        List<SFP__c> relatedSFPList = [SELECT Id, Region_Dept__c, Account_Name__c
        							   FROM SFP__c
        							   WHERE Account_Name__c
        							   IN: curAccRegionDeptMap.keySet()];
        if (relatedSFPList.size() > 0){
        	for (SFP__c sfp : relatedSFPList){
        		sfp.Region_Dept__c = curAccRegionDeptMap.get(sfp.Account_Name__c);
        	}
        	try{
        		update relatedSFPList;
        	}
        	catch(DMLException e){
        		trigger.new[0].addError(e.getDMLMessage(0));
        	}
        }
        
        //Sync Service Request
        List<Case> relatedSRList = [SELECT Id, Region_Dept__c, AccountId FROM Case
        							WHERE AccountId IN: curAccRegionDeptMap.keySet()];
        							
        List<Case> relatedSRListNew = new List<Case>();
        if (relatedSRList.size() > 0){
        	for (case sr : relatedSRList){
        		relatedSRListNew.add(new Case(id=sr.id, Region_Dept__c=curAccRegionDeptMap.get(sr.AccountId)));
        	}
        	try{
        		update relatedSRListNew;
        	}
        	catch(DMLException e){
        		trigger.new[0].addError(e.getDMLMessage(0));
        	}
        }
        
        //Sync Work Order
        List<Work_Order__c> relatedWOList = [SELECT Id, Region_Dept__c, Account_Name__c
        									 FROM Work_Order__c
        									 WHERE Account_Name__c
        									 IN: curAccRegionDeptMap.keySet()];
        if (relatedWOList.size() > 0){
        	for (Work_Order__c wo : relatedWOList){
        		wo.Region_Dept__c = curAccRegionDeptMap.get(wo.Account_Name__c);
        	}
        	try{
        		update relatedWOList;
        	}
        	catch(DMLException e){
        		trigger.new[0].addError(e.getDMLMessage(0));
        	}
        }
        
        //Sync Contract order
        List<Contract_Product_PO__c> relatedCPOLIst = [SELECT Id, Region_Dept__c, Contract_No__c
        											   FROM Contract_Product_PO__c
        											   WHERE Contract_No__c IN: relatedContractIdSet];
        if (relatedCPOList.size() > 0){
        	for (Contract_Product_PO__c cpo : relatedCPOList){
        		cpo.Region_Dept__c = contractRegionMap.get(cpo.Contract_No__c);
        	}
        	try{
        		update relatedCPOList;
        	}
        	catch(DMLException e){
        		trigger.new[0].addError(e.getDMLMessage(0));
        	}
        }
        
        //Sync Equipment Archive
        List<Installed_Asset__c> relatedEAList = [SELECT Id, Region_Dept__c, Account_Name__c,
        										  Contracts_No__r.Account_Name__c
        										  FROM Installed_Asset__c
        										  WHERE Account_Name__c
        										  IN: curAccRegionDeptMap.keySet()];
        if (relatedEAList.size() > 0){
        	for (Installed_Asset__c ea : relatedEAList){
        		if (ea.Contracts_No__r.Account_Name__c != ea.Account_Name__c){
        			trigger.new[0].addError('\nError on EA(' + ea.Id + '): The Equipment Archive and'
        				+ ' the Service Contract is not associated with the same Account!');
        		}
        		else{
        			ea.Region_Dept__c = curAccRegionDeptMap.get(ea.Account_Name__c);
        		}
        	}
        }
        try{
        	update relatedEAList;
        }
        catch(DMLException e){
        	trigger.new[0].addError(e.getDMLMessage(0));
        }
	}
}