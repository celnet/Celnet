trigger PocInOpportunity on POC_In_Opportunity__c (before insert,before update,before delete) {
	new Triggers()
		.bind(Triggers.Evt.beforeinsert,new PocInOpportunityHandler())
		.bind(Triggers.Evt.beforeupdate,new PocInOpportunityHandler())
		.bind(Triggers.Evt.beforedelete,new PocInOpportunityHandler())
		.manage();    
}