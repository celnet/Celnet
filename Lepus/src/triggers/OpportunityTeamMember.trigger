trigger OpportunityTeamMember on OpportunityTeamMember (after delete, after insert, after update) {
    new Triggers()
        .bind(Triggers.Evt.afterinsert,new OpportunityTeamMemberHandler())
        .bind(Triggers.Evt.afterupdate,new OpportunityTeamMemberHandler())
        .bind(Triggers.Evt.afterdelete,new OpportunityTeamMemberHandler())
        .bind(Triggers.Evt.afterdelete,new OpportunityTeamMemberArchiveForBIHandler())
        
        .bind(Triggers.Evt.afterinsert,new Lepus_OpportunityTeamMemberHandler())  // add by kejun 2014-8-29 同步到EIP
        .bind(Triggers.Evt.afterupdate,new Lepus_OpportunityTeamMemberHandler())  // add by kejun 2014-8-29 同步到EIP
        .bind(Triggers.Evt.afterdelete,new Lepus_OpportunityTeamMemberHandler())  // add by kejun 2014-8-29 同步到EIP
        
        .manage(); 
}