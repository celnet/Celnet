/**************************************************************************************************
 * Name             : Tig_OnWorkOrder
 * Object           : Work_Order__c
 * Requirement      : 
 * Purpose          : 
 * Author           : Iverson gao
 * Create Date      : 07/12/2012
 * Modify History   : 
***************************************************************************************************/ 
trigger Tig_OnWorkOrder on Work_Order__c (before insert,before update) {
      if(trigger.isBefore){
             Work_Order__c woc = trigger.new[0];   
             //List<SFP__c> sfps = [Select Contract_No__c From SFP__c where Id =: woc.SFP_No__c];             
             //SFP__c sfp = sfps[0];
             //CSS_Contract__c con = [Select Account_Name__c From CSS_Contract__c where Id =:sfp.Contract_No__c];
             List<Account> accList = [Select Region_Dept__c From Account where Id =:woc.Account_Name__c];
             if( accList.size()>0 ){
                 Account acc = accList[0];
                 woc.Region_Dept__c = acc.Region_Dept__c;    
             }               
      }   
}
//this trigger seems to populate the region dept field from the account ,need to rewrite