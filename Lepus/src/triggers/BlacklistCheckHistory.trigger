/**
 * The BlacklistCheckHistory trigger
 * Author           : BLS
 * Create Date      : 2013/5
 *     
 */  
trigger BlacklistCheckHistory on Blacklist_Check_History__c (before insert) {
	new Triggers()
	.bind(Triggers.Evt.beforeinsert,new BlacklistCheckHistoryIsNewInsertHandler())
	.manage();
}