/*
根据驻场计划上的Sum_Service_Amount__c字段来计算SFP下所有计划的已使用服务时间
*@Author:He Wei
*Create Date：2014-3-13
*/
trigger ResidentEngineerSupportPlan on Resident_Engineer_Support_Plan__c (after delete, after insert, after undelete, 
after update) {
	new Triggers()
	    .bind(Triggers.Evt.afterupdate,new ResidentEngineerSupportPlan())
	    .bind(Triggers.Evt.afterinsert,new ResidentEngineerSupportPlan())
	    .bind(Triggers.Evt.afterundelete,new ResidentEngineerSupportPlan())
	    .bind(Triggers.Evt.afterdelete,new ResidentEngineerSupportPlan())
	    .manage();
}