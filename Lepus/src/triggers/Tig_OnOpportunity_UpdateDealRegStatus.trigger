trigger Tig_OnOpportunity_UpdateDealRegStatus on Opportunity (before insert, before update) {
    If(trigger.isBefore){
        List<Opportunity> optlist = new List<Opportunity>();
    //  List<Deal_Registration__c> drlist=new List<Deal_Registration__c>();
        
        for ( Opportunity opt : trigger.new ){
            optlist.add(opt);               
        }
            
        for(Opportunity opt:optlist){
            If(opt.StageName=='SSA (Huawei No Bid)' || opt.StageName=='SSB (Customer Did Not Pursue)' 
            || opt.StageName=='SSC (Lost to Competition)'  ){
                List<Deal_Registration__c> drlist=new List<Deal_Registration__c>();
                drlist=[select 
                id,
                deal_status__c 
                from Deal_Registration__c 
                where Related_Opportunity__c=:opt.Id];
                opt.NextStep=drlist.size().format();
                for(Deal_Registration__c dr:drlist){
                    If(dr.Deal_Status__c!='Abolished'){
                        dr.Deal_Status__c='Cancelled';
                        update(dr);
                    }
                }
            }
             if(opt.StageName=='SS7 (Implementing)' || opt.StageName=='SS8 (Completed)' ){
                List<Deal_Registration__c> drlist2=new List<Deal_Registration__c>();
                drlist2=[select 
                id,
                deal_status__c 
                from Deal_Registration__c 
                where Related_Opportunity__c=:opt.Id];
                for(Deal_Registration__c dr:drlist2){
                    If(dr.Deal_Status__c!='Abolished'){
                        dr.Deal_Status__c='Closed';
                        update(dr);
                    }
                }
            }
            
            /*Modify by czh 2013-2-8*/
            /*如果opt.StageName更新为SS4及后续状态时，更新所有Deal*/
           if(
                (opt.StageName=='SS4 (Developing Solution)' || opt.StageName=='SS5 (Gaining Agreement)'
                ||opt.StageName=='SS6 (Winning)'||opt.StageName=='SS7 (Implementing)'
                ||opt.StageName=='SS8 (Completed)' ) 
                && trigger.isUpdate && opt.StageName != trigger.OldMap.Get(opt.Id).StageName       
                )
               
            {
                List<Deal_Registration__c> drlist3=new List<Deal_Registration__c>();
                drlist3=[select 
                id,
                deal_status__c 
                from Deal_Registration__c 
                where Related_Opportunity__c=:opt.Id];
                for(Deal_Registration__c dr:drlist3){
                        dr.LastModifyDateTime_Trigger__c=System.now();
                        update(dr);
                }            
            }//end update deal for ss3->ss4

        }
        
    }

}