trigger TigOnSFP_AutoUpdateWO on SFP__c (after update) {//2009.12.29 before update
     
    if(Trigger.isUpdate){
        List<Id> sfpIdList=new List<Id>();
        Map<Id,SFP__c> sfpIdsfpMap=new Map<Id,SFP__c>();
        
        for(SFP__c sfp:Trigger.new){
            sfpIdList.add(sfp.id);
            sfpIdsfpMap.put(sfp.id,sfp);
        }
        
       
                /**
        List<Work_Order__c> woList=[Select w.Status__c, w.Contract_No__c From Work_Order__c w
                where w.Contract_No__c in :cccIdList and w.Status__c!='Cancelled'];
                **/
        List<Work_Order__c> woListTmp=[Select w.Status__c, w.SFP_No__c,w.Engineer_Setoff_Time__c, w.Engineer_Return_Time__c
                From Work_Order__c w
                where w.SFP_No__c in :sfpIdList 
                    and w.Status__c!='Cancelled'];
        List<Work_Order__c> woList=new List<Work_Order__c>();
        for(Work_Order__c wo:woListTmp){
            /**
            if(wo.Engineer_Setoff_Time__c!=null && 
                wo.Engineer_Return_Time__c!=null &&
                wo.Engineer_Setoff_Time__c < wo.Engineer_Return_Time__c){
                woList.add(wo);
            }
            */
            if(wo.Status__c!='Closed' && wo.Status__c!='Cancelled')woList.add(wo);
        }
                
        if(Trigger.isAfter){
                        
            if(woList!=null && woList.size()>0){
                SFP__c sfp;
                for(Work_Order__c wo:woList){
                    sfp=sfpIdsfpMap.get(wo.SFP_No__c);
                    if(sfp.Status__c=='Cancelled')wo.Status__c='Cancelled'; 
                }
                try{
                    if(woList!=null && woList.size()>0 && sfp.Status__c=='Cancelled')update(woList);
                }catch(Exception e){}
            }
        }      
            
    }
    
   
    
}