trigger TigOnWorkOrder on Work_Order__c (after insert,after update,before insert,before update,after delete) {
    if(Trigger.isAfter){
        //** 用于更新SFP .3.services left.
        Set<Id> woSFPNoSet=new Set<Id>();
        
	    if(Trigger.isInsert){ 
	        for(Work_Order__c wo:Trigger.new){
	            woSFPNoSet.add(wo.SFP_No__c);
	        } 
	    }else if(Trigger.isDelete){
	        for(Work_Order__c wo:Trigger.old){
	            woSFPNoSet.add(wo.SFP_No__c);
	        }
	        
	     
	    }
    }
        
        //1.新建非例外工单时检查Service_Request_No__c 或Contract_No__c不为空，增加对SFP的校验
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            for(Work_Order__c wo:Trigger.new){
                if(wo.Contract_No__c==null && wo.Service_Request_No__c==null && wo.work_order_type__c != '(Int) Exception Service' && wo.work_order_type__c != '(N) Exception Service' ){
                    wo.addError('Please create work orders under SFP or Service Request.(请在SFP或服务请求下创建工单)');
                }
            }
        }
    }
    
    if(Trigger.isBefore){ 
        Map<Id,Work_Order__c> wo_iacIdMap=new Map<Id,Work_Order__c>();//<id=wo.Barcode__c>
        Map<Id,Work_Order__c> wo_contactIdMap=new Map<Id,Work_Order__c>();//id=contact.id
        Map<Id,Work_Order__c> wo_accountIdMap=new Map<Id,Work_Order__c>();//id=account.id
        
        Map<Id,Work_Order__c> newWOIdWOMap=new Map<Id,Work_Order__c>();
        Map<Id,Work_Order__c> oldWOIdWOMap=new Map<Id,Work_Order__c>();
        Set<Id> uIdSet=new Set<Id>();
        if(Trigger.isUpdate){
            for(Work_Order__c wo:Trigger.old){
                oldWOIdWOMap.put(wo.id,wo);
                uIdSet.add(wo.OwnerId);
            }
        }         
        List<Id> cssIdList=new List<Id>();
        for(Work_Order__c wo:Trigger.new){
            wo_iacIdMap.put(wo.Barcode__c,wo);
            wo_contactIdMap.put(wo.Contact_Person__c,wo);
            wo_accountIdMap.put(wo.Account_Name__c,wo); 
            
            newWOIdWOMap.put(wo.id,wo);
            uIdSet.add(wo.OwnerId);
            if(Trigger.isInsert) cssIdList.add(wo.Contract_No__c);
        }
        
        //当前用户角色不包含CFS或External时，新的Owner用户的角色名称中不能包含External
        //https://cs5.salesforce.com/a0aO0000000CavI
        if(Trigger.isUpdate){
            uIdSet.add(UserInfo.getUserId());//加入当前用户ID
            List<User> uList=[select u.id,u.name,u.Region_Office__c,u.MobilePhone
                ,u.UserRole.Name,u.UserRoleId 
                ,u.Profile.Name, u.ProfileId
                from User u 
                where u.id in:uIdSet];
            Map<Id,User> uIduMap=new Map<Id,User>(); 
            for(User u:uList){
                uIduMap.put(u.id,u);
            }
            
            for(Work_Order__c wo:Trigger.new){
                Work_Order__c oldWo=oldWOIdWOMap.get(wo.id);
                Work_Order__c newWo=newWOIdWOMap.get(wo.id);
                if(oldWo==null || newWo==null)continue;
                if(oldWo.OwnerId==null || newWo.OwnerId==null)continue;
                User newUser=uIduMap.get(newWo.OwnerId);
                User oldUser=uIduMap.get(oldWo.OwnerId);
                //2.国内、国际的用户不能互转，即国内用户不能转给国际用户，国际用户不能转给国内用户
                //https://cs5.salesforce.com/a0aO0000000CawV
                if(oldUser!=null && oldUser.UserRole!=null && newUser!=null && newUser.UserRole!=null){
                	String msg='';
                	if(oldUser.UserRole.Name.indexOf('Int')>-1 && newUser.UserRole.Name.indexOf('(N)')>-1){
                		msg='国际用户不能转给国内用户';
                	}
                	if(oldUser.UserRole.Name.indexOf('(N)')>-1 && newUser.UserRole.Name.indexOf('Int')>-1){
                		msg='国内用户不能转给国际用户';
                	}
                	if(!msg.equals(''))
                		wo.addError(msg);
                }
                
                //3.当前用户角色不包含SD或External时，新的Owner用户的角色名称中不能包含External
                //https://cs5.salesforce.com/a0aO0000000CavI
                User currUser=uIduMap.get(UserInfo.getUserId());
                System.debug('the currUser:---' + currUser + ' ,UserRole:' + currUser.UserRole);
                System.debug('the newUser:---' + newUser + ' ,UserRole:' + newUser.UserRole);
                System.debug('the oldUser:---' + oldUser + ' ,UserRole:' + oldUser.UserRole);
                if(currUser!=null && currUser.UserRole!=null && 
                   newUser!=null && newUser.UserRole!=null &&
                   oldUser!=null && newUser.Id!=oldUser.Id ){ 
                   	//wo.addError('测试：如果显示此1，则第一条件（Owner改变）满足');
                   	//continue;
                    if( currUser.UserRole.Name.indexOf('SD')<0 &&
                        currUser.UserRole.Name.indexOf('External')<0 && 
                        newUser.UserRole.Name.indexOf('External')>0 ){
                   		//wo.addError('测试：如果显示此2，则第二条件（当前用户角色为CFS或External，并且新用户角色为External）满足');
                   		//continue;
                   		System.debug('should enter here:--------------the add error part');
                        wo.addError('Only SD and external users can assign Work Order to external users. 只有SD和外部用户可以将工单派给外部用户。');
                    }
                }//else wo.addError('测试：如果显示此3，Owner 没改变');
                
                //4.设置Owner_Mobile__c
                if(oldWo.OwnerId!=newWo.OwnerId  ){
                    wo.Owner_Mobile__c=newUser.MobilePhone;
                }
                //5.SPM_Region__c 和 RSC REGION 并行，增加RSC profile 判断.查看SPM报表的使用情况
                if(oldWo.OwnerId!=newWo.OwnerId && newUser.ProfileId!=null && (newUser.Profile.Name.indexOf('SPM')>0 ||newUser.Profile.Name.indexOf('TM')>0 )){
                    wo.SPM_Region__c=newUser.Region_Office__c; 
                    
                    /*
                    wo.addError('原用户Id:'+oldWo.OwnerId+'、新用户Id:'+newWo.OwnerId+'='+newUser.Id
                        +'、简档:'+newUser.Profile.Name
                        +'、新用户的newUser.Region_Office__c='+newUser.Region_Office__c
                        +'、改后的wo.SPM_Region__c='+wo.SPM_Region__c
                        );
                    */
                }
                
                // Update RSC_Region__c
                //	 Polaris Yu 2012-7-17
                //6.RSC REGION 赋值
                if(oldWo.OwnerId!=newWo.OwnerId && newUser.ProfileId!=null &&
                   (newUser.Profile.Name.indexOf('RSC')>0) ){
                    wo.RSC_Region__c = newUser.Region_Office__c; 
                }
            }
        }
        
        String sms='';
        Work_Order__c woContact=null;
        List<Contact> contactList=[select id,name,phone,MobilePhone 
            from Contact 
            where id in :wo_contactIdMap.keySet()];
        Map<Id,Contact> contactMap=new Map<Id,Contact>();
        
        List<Account> accountList=[select id,name from Account where id in :wo_accountIdMap.keySet()];
        Map<Id,Account> accountMap=new Map<Id,Account>();
        
        for(Contact c:contactList){
            contactMap.put(c.id,c);
        }
        for(Account acc:accountList){
            accountMap.put(acc.id,acc);
        }
        
        //change the installation address of worker order=the address from contract
        //w.Intallation_Sub_Region__c, w.Installation_City__c, w.Regions__c
        List<RecordType> rtList0=[Select r.SobjectType, r.Name, r.Id 
            From RecordType r 
            where r.SObjectType='Work_Order__c'];
        List<RecordType> rtList=new List<RecordType>();
        List<RecordType> rtList2=new List<RecordType>();
        for(RecordType rt:rtList0){
            if(rt.name.indexOf('Installation Work Order')>=0){
                rtList.add(rt);
            }
            if(rt.name.indexOf('On Site Support Work Order')>=0){
                rtList2.add(rt);
            }
        }
        //Select w.Work_Hour_Value_added_Standalone__c, w.Work_Hour_Hardware_software__c
        //, w.Total_Work_Hour__c, w.Service_Request_No__c 
        //From Work_Order__c w
        Map<Id,Work_Order__c> caseIdWOMap=new Map<Id,Work_Order__c>();//id=case.id
        List<Case> willUpdateCaseList=new List<Case>();
        for(Work_Order__c wo:Trigger.new){
            for(RecordType rt:rtList2){
                if(rt.id!=wo.RecordTypeId)continue;
                if(wo.Total_Work_Hour__c!=0){
                    caseIdWOMap.put(wo.Service_Request_No__c,wo);
                }
            }
        }
        willUpdateCaseList=[select id,CSE_Work_Hour_HW__c,CSE_Work_Hour_SW__c 
            from Case 
            where id in :caseIdWOMap.keySet()];
        for(Case c:willUpdateCaseList){
            Work_Order__c wo=caseIdWOMap.get(c.id);
            //7.设置 service request上的work order字段
            c.CSE_Work_Hour_HW__c=wo.Work_Hour_Hardware_software__c ;
            c.CSE_Work_Hour_SW__c=wo.Work_Hour_Value_added_Standalone__c ;
        }
        //update(willUpdateCaseList);
        
        //change the installation address of worker order=the address from contract
        //w.Intallation_Sub_Region__c, w.Installation_City__c, w.Regions__c 
         
        for(Work_Order__c wo:Trigger.new){
            Account acc=accountMap.get(wo.Account_Name__c);
            Contact c=contactMap.get(wo.Contact_Person__c);
            String accName=acc.Name==null?' ':acc.Name;
            String cName='',cMobilePhone='',cPhone='', ContractNo='';
            ContractNo=wo.Contract_No_ESPM__c;
            if(c!=null){            	
                if(c.Name!=null)cName=c.Name;
                if(c.MobilePhone!=null)cMobilePhone=','+c.MobilePhone;
                if(c.Phone!=null)cPhone=','+ c.Phone;
            }
            //8. SMS_Notification_Message__c
            if(wo.SMS_Notification_Message__c==null)wo.SMS_Notification_Message__c='ContractNo:'+ContractNo+','+accName+','+cName+cMobilePhone +cPhone;
        }
        
        List<Installed_Asset__c> woiacList=[Select i.SubRegion__c, i.Street__c, i.Region__c, i.Other_City__c, i.Id, i.City__c 
            From Installed_Asset__c i
            where i.Id in :wo_iacIdMap.keySet()];
        Work_Order__c woiac=null;
        for(Installed_Asset__c iac:woiacList){
		        String combineAddress='';
            combineAddress='';
            if(iac.Region__c!=null)combineAddress+=iac.Region__c +' ';
            if(iac.SubRegion__c!=null)combineAddress+=iac.SubRegion__c +' ';
            if(iac.City__c!=null)combineAddress+=iac.City__c +' ';
            if(iac.Other_City__c!=null)combineAddress+=iac.Other_City__c +' ';
            if(iac.Street__c!=null)combineAddress+=iac.Street__c;
            woiac=wo_iacIdMap.get(iac.Id);
            for(Work_Order__c wo:Trigger.new){
                if(woiac.id!=wo.id)continue;
                //9.asset field
                wo.Asset__c=combineAddress;
            }
        }       
    }
}