/*
*@Author:Brain Zhang
*Create Date：2013-9
*/
trigger iCareTask on iCare_Task__c (after insert, after update) {
	new Triggers()
			.bind(Triggers.Evt.afterinsert,new icareTaskUpdateCaseHandler()) //根据iCare Task的Type值，对问题单Case中相关时间字段赋值
			.bind(Triggers.Evt.afterupdate,new icareTaskUpdateCaseHandler()) //根据iCare Task的Type值，对问题单Case中相关时间字段赋值
			.manage();    
}