trigger TigOnContractProductPO on Contract_Product_PO__c (after update,after insert,after delete) {

	Set<Id> cccIdSet=new Set<Id>();
	if(Trigger.old!=null){
		for(Contract_Product_PO__c cpp:Trigger.old){
			if(cpp.Contract_No__c!=null){
				cccIdSet.add(cpp.Contract_No__c);
			}			
		}
	}
	if(Trigger.new!=null){
		for(Contract_Product_PO__c cpp:Trigger.new){
			if(cpp.Contract_No__c!=null){
				cccIdSet.add(cpp.Contract_No__c);
			}			
		}
	}
	if(cccIdSet!=null){
		List<CSS_Contract__c> cccList=[Select c.Total_Contract_PO__c, c.Id 
						From CSS_Contract__c c
						where c.Id in :cccIdSet];
		Map<Id,CSS_Contract__c> cccIdcccMap=new Map<Id,CSS_Contract__c>();
		for(CSS_Contract__c ccc:cccList){
			cccIdcccMap.put(ccc.Id,ccc);
		}	
		List<Contract_Product_PO__c> cppList=[Select c.Total_PO_Amount__c, c.Contract_No__c 
						From Contract_Product_PO__c c
						where c.Contract_No__c in :cccIdSet];
		Map<Id,List<Contract_Product_PO__c>> cccIdcppListMap=new Map<Id,List<Contract_Product_PO__c>>();
		for(Contract_Product_PO__c cpp:cppList){
			List<Contract_Product_PO__c> tmpCppList=new List<Contract_Product_PO__c>();
			if(cccIdcppListMap.containsKey(cpp.Contract_No__c))
				tmpCppList=cccIdcppListMap.get(cpp.Contract_No__c);
			
			tmpCppList.add(cpp);
			cccIdcppListMap.put(cpp.Contract_No__c,tmpCppList);
		}
		
		///List<CSS_Contract__c> cccUpdateList=new List<CSS_Contract__c>();
		for(CSS_Contract__c ccc:cccList){
		
		/**
		for(Contract_Product_PO__c cpp:Trigger.new){
			if(!cccIdcccMap.containsKey(cpp.Contract_No__c))continue;
			CSS_Contract__c ccc=cccIdcccMap.get(cpp.Contract_No__c);
			if(ccc==null)continue;
		*/	
			Decimal amount=0.0;
			if(!cccIdcppListMap.containsKey(ccc.Id)){
				ccc.Total_Contract_PO__c=amount;
				//cccUpdateList.add(ccc);
				continue;
			}	
			List<Contract_Product_PO__c> tmpCppList=cccIdcppListMap.get(ccc.Id);
			for(Contract_Product_PO__c cppc:tmpCppList){
				if(cppc.Total_PO_Amount__c!=null)amount+=cppc.Total_PO_Amount__c;
			}
			ccc.Total_Contract_PO__c=amount;
			//cccUpdateList.add(ccc);
			tmpCppList.clear(); 
		} 
		//if(cccUpdateList!=null && cccUpdateList.size()>0)update(cccUpdateList);
		if(cccList!=null && cccList.size()>0)update(cccList);
	}
}