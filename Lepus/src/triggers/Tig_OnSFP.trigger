/**************************************************************************************************
 * Name             : Tig_OnSFP
 * Object           : Account
 * Requirement      : SFP__c
 * Purpose          : 
 * Author           : Iverson gao
 * Create Date      : 07/12/2012
 * Modify History   : 
 * 2013-1-30 Polaris Yu
 * 		1. Delete debugMsg to simplify the codes
 *		2. Replace accIdList,newContractIdList with accIdSet,newContractIdSet to avoid duplicates
***************************************************************************************************/ 
trigger Tig_OnSFP on SFP__c (Before insert, Before update) {
    if(trigger.isBefore
       && !(CommonConstant.changedFromAccount || CommonConstant.changedFromContract)){
    	Set<Id> accIdSet = new Set<Id>();
		Map<Id, Account> accMap = new Map<Id, Account>();
    	
    	// New SFP
    	if(trigger.isInsert){
			accIdSet = new Set<Id>();
      		accMap = new Map<Id, Account>();
      		for(SFP__c sfp : trigger.new){
      			accIdSet.add(sfp.Account_Name__c);
      		}
			accMap = new Map<Id, Account>([SELECT Id, Region_Dept__c FROM Account
										   WHERE Id IN: accIdSet]);
			
			if (accMap.size() > 0){
				for (SFP__C sfp : trigger.new){
					sfp.Region_Dept__c = 
         				accMap.get(sfp.Account_Name__c).Region_Dept__c;
             	}
			}
		}
		
		// Update SFP
		if(trigger.isUpdate){
			accIdSet = new Set<Id>();
      		accMap = new Map<Id, Account>();
      		List<SFP__c> newSFPList = new List<SFP__c>();
      		Set<Id> newContractIdSet = new Set<Id>();
      		Map<Id, String> oldContractMap = new Map<Id, String>();
      		
      		// Used in the comparison of the old and new related service contract on SFP
      		for (SFP__c sfp : trigger.old){
      			oldContractMap.put(sfp.Id, sfp.Contract_No__c);
      		}
      		for (SFP__c sfp : trigger.new){
      			// Judge whether the related service contract is changed
      			if(sfp.Contract_No__c != oldContractMap.get(sfp.Id)){
      				newSFPList.add(sfp);
      				newContractIdSet.add(sfp.Contract_No__c);
      			}
      		}
      		
      		Map<Id, CSS_Contract__c> newContractMap = 
      			new Map<Id, CSS_Contract__c>([SELECT Id, Name, Account_Name__c, Region_Dept__c,
      										  Account_Name__r.Name
      										  FROM CSS_Contract__c
      										  WHERE Id IN: newContractIdSet]);
      		
      		for (SFP__c sfp : newSFPList){
      			sfp.Account_Name__c = newContractMap.get(sfp.Contract_No__c).Account_Name__c;
      			sfp.Region_Dept__c = newContractMap.get(sfp.Contract_No__c).Region_Dept__c;
      		}
      		
		}
    }
}