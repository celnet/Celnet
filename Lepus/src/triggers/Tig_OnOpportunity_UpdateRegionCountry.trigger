trigger Tig_OnOpportunity_UpdateRegionCountry on Opportunity (before insert, before update) {

   if(trigger.isBefore){    
   	 String r1='Enterprise Business Opportunity'.toLowerCase();
   	 String r2='Enterprise Business Deal Registration'.toLowerCase();
   	 List<Opportunity> optlist = new List<Opportunity>();
   	 List<Id> recordtypeidlist= new list<Id>();
   	  for ( Opportunity opt : trigger.new ){
               optlist.add(opt);
               recordtypeidlist.add(opt.RecordTypeId);
           }   	
    for(Opportunity opt:optlist){
        RecordType rt=[select id,name from RecordType where id=:opt.RecordTypeId]; 
        if(opt.AccountId!=null){       	
       
        	Account acc=[select id, Region_HW__c,Representative_Office__c,Country_HW__c,Country_Code_HW__c from Account where id=:opt.AccountId];
  
        	if(acc!=null && rt!=null &&( rt.name.toLowerCase().equals(r1) || rt.name.toLowerCase().equals(r2)) ){
 
       			if(opt.Region__c == null) opt.Region__c=acc.Region_HW__c;
       	
       			if(opt.Representative_Office__c== null) opt.Representative_Office__c=acc.Representative_Office__c ;       	
       			if(opt.Country__c == null ){
       				opt.Country__c=acc.Country_HW__c; 
       				opt.Country_Code__c=acc.Country_Code_HW__c;
       			}
        	}    
        }
   	}
  }
   	
}