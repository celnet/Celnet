/**
 * @Purpose : The account trigger 
 * @Author : BLS
 * @Date :   2014-07-01 Steven Refactor
 */     
trigger Account on Account (before insert,after insert, after update, before delete, before update, after delete) {
    
    new Triggers()
        .bind(Triggers.Evt.beforeinsert, new Lepus_WisdomAccountWriteBackHandler()) // add by kejun 20140901 Wisdom回写到Salesforce控制字段赋值
        .bind(Triggers.Evt.beforeupdate, new Lepus_WisdomAccountWriteBackHandler()) // add by kejun 20140901 Wisdom回写到Salesforce控制字段赋值
        
        .bind(Triggers.Evt.afterinsert, new Lepus_AccountHandler()) // add by kejun 20140820 同步到EIP
        .bind(Triggers.Evt.afterupdate, new Lepus_AccountHandler()) // add by kejun 20140820 同步到EIP
        .bind(Triggers.Evt.afterdelete, new Lepus_AccountHandler()) // add by kejun 20140820 同步到EIP
        
        .bind(Triggers.Evt.afterinsert, new Lepus_SetGlobalIdHandler()) // add by sunda 20140815 设置Global ID 字段
        
        .bind(Triggers.Evt.beforeupdate,new AccountCustomGroupMappingHandler()) //(中国区销售)CIS系统更新时,同步刷新SF的代表处行业客户群字段 
        .bind(Triggers.Evt.beforeupdate,new AccountPreventDeleteHandler())      //(中国区销售)删除客户前,检查是否有子客户和机会点
        .bind(Triggers.Evt.beforeupdate,new AccountParentChangedHandler())      //(中国区销售)父客户变化时,更新子客户的NA记录及关键属性
        .bind(Triggers.Evt.afterupdate, new AccountNAChangedHandler())          //(中国区销售)NA属性变化时,批量更新子客户的NA记录
        .bind(Triggers.Evt.beforeupdate,new AccountBeforeUpdateHandler())       //(中国区销售)客户下面有未关闭的机会点不允许失效
                
        .bind(Triggers.Evt.beforeupdate,new AccountBlacklistStatusUpdateHandler()) //(ALL)黑名单状态刷新
        .bind(Triggers.Evt.beforeinsert,new AccountBlacklistStatusInsertHandler()) //(ALL)黑名单状态插入
        .bind(Triggers.Evt.afterupdate, new AccountEmailInformHandler())           //(ALL)黑名单状态改变发邮件提醒
        
        .bind(Triggers.Evt.afterupdate, new AccountAfterUpdateHandler())      //(中国区销售)父客户关键属性变化刷新子客户及机会点
        .bind(Triggers.Evt.afterupdate, new AccountUpdateRegionDeptHandler()) //(海外服务)客户区域发生变化更新
        .bind(Triggers.Evt.afterupdate, new AccountUpdateIndustryHandler())   //(海外销售)客户行业变化了同步刷新机会点的行业
        .bind(Triggers.Evt.beforedelete,new AccountPreventDeleteHandler())    //(中国区销售)删除account前检查是否存在子客户或机会点
        .bind(Triggers.Evt.afterinsert, new AccountCisRepeatRemindHandler())  //(ALL)创建海外最终客户CIS编码重复性校验
        .bind(Triggers.Evt.afterupdate, new AccountCisRepeatRemindHandler())  //(ALL)中国区/海外最终客户CIS编码重复性校验
         
        .bind(Triggers.Evt.afterupdate, new AccountUpdateSalesTargetHandler())//(中国区销售)当客户的Owner变化时，更新所有生效的的销售目标Owner
        
        .bind(Triggers.Evt.afterinsert, new AccountTCGUpdateHandler()) //(中国区销售)自动刷新TCG
        .bind(Triggers.Evt.afterupdate, new AccountTCGUpdateHandler()) //(中国区销售)自动刷新TCG
        .bind(Triggers.Evt.beforeinsert,new ChinaTCGUpdateHandler())   //(中国区销售)自动更新TCG关联列表
        .bind(Triggers.Evt.afterinsert, new ChinaTCGUpdateHandler())   //(中国区销售)自动更新TCG关联列表
        .bind(Triggers.Evt.beforeupdate,new ChinaTCGUpdateHandler())   //(中国区销售)自动更新TCG关联列表
        .bind(Triggers.Evt.afterupdate, new ChinaTCGUpdateHandler())   //(中国区销售)自动更新TCG关联列表
        
        .bind(Triggers.Evt.afterdelete, new AccountArchiveForBIHandler()) //(ALL)删除后将记录保存到数据维护历史对象上，供BI抓取
        
        .bind(Triggers.Evt.afterupdate,new AccoutUpdateCSScontractIndustryHandler()) //(服务) 何微20140912创建
        
        .manage();
    
    
        
        
        
}