trigger TigOnCase on Case (before insert, before update) {//
    if(Trigger.isBefore){ 
        List<Case> caseList=new List<Case>();
        List<Id> iaCaseIdList=new List<Id>();
        Map<id,id> CaseIaMap=new Map<id,id>(); //zhengjing 2011-11-16
        Map<Id,Case> caseOldMap=new Map<Id,Case>(),caseNewMap=new Map<Id,Case>();
        List<Id> ownerIdList=new List<Id>(); 
        
        if(Trigger.isUpdate){  
            for(Case c:Trigger.old){
                caseOldMap.put(c.id,c);
                 System.debug('the old ownerid is:---------' + c.OwnerId);
                ownerIdList.add(c.OwnerId); 
            }
        }
        for(Case c:Trigger.new){//Case=>ServiceRequest
      //      //Warranty and Maintenance SFP ,SFP维保记录类型
            caseList.add(c);
            System.debug('the current ownerid is:---------' + c.OwnerId);
            ownerIdList.add(c.OwnerId);
            caseNewMap.put(c.id,c);
            if(c.Barcode__c==null)continue;
            String cid=c.Barcode__c;
            iaCaseIdList.add(cid);
            CaseIaMap.put(c.id,cid);   //zhengjing 2011-11-16
        } 
        
        List<User> owners=[Select u.Id,u.EmployeeNumber,u.Name,u.Profile.Name, u.Profile.Id, u.ProfileId 
            From User u
            where u.Id in :ownerIdList];
        Map<Id,User> ownerMap=new Map<Id,User>();
        for(User u:owners){
            ownerMap.put(u.id,u); 
        }
      
        List<Installed_Asset__c> iacList=[select id,name,Contracts_No__c from Installed_Asset__c where id in :iaCaseIdList];
        List<Id> iacContractNoList=new List<Id>();
        map<id,id> IaCcMap=new map<id,id>();  //zhengjing 2011-11-16
        for(Installed_Asset__c iac:iacList){
            iacContractNoList.add(iac.Contracts_No__c);
            IaCcMap.put(iac.id,iac.Contracts_No__c);  //zhengjing 2011-11-16
        }

   
        List<CSS_Contract__c> cccList=[select id,name from CSS_Contract__c where id in :iacContractNoList];
        List<Id> cccIdList=new List<Id>();
        for(CSS_Contract__c ccc:cccList){
            cccIdList.add(ccc.id);
        }
   
   //zhengjing 2011-11-16  
        Map<id,id> CaseCcMap=new Map<id,id>();
        for(Case c:Trigger.new){
            id ia=CaseIaMap.get(c.id); //获取case对应的Installed Asset
            id cc=IaCcMap.get(ia); // 获取Installed Asset对应的CSS Contract
     //     id ccid=CcIdMap.get(cc); //获取CSS Contract对应的Id
            CaseCcMap.put(c.id,cc);       //建立case和CSS Contract name的对应关系    
        }
    //zhengjing 2011-11-16  
   
        List<Id> iaIdList=new List<Id>();//id=barcode=ia.id       
        for(Case c:Trigger.new){
            c.Contract_No__c=CaseCcMap.get(c.id);  //zhengjing 2011-11-16
            iaIdList.add(c.Barcode__c);
        } 
        Map<Id,SFP_Asset__c> iaIdSFPAMap=new Map<Id,SFP_Asset__c>();//id=ia.id
        List<SFP_Asset__c> tmpSFPAList=[Select s.SFP__c,s.SFP__r.id, s.SFP_Status__c
                                , s.Name, s.Install_Asset__c, s.Id 
                                From SFP_Asset__c s
                                where s.Install_Asset__c in :iaIdList and s.SFP_Status__c='Active' and  s.SFP__r.RecordType.DeveloperName like '%Warranty%' ];
                                //根据业务需求，只带出维保SFP，且不论是否激活。原需求是要求带出激活的SFP
       for(SFP_Asset__c sfpa:tmpSFPAList){
            iaIdSFPAMap.put(sfpa.Install_Asset__c,sfpa); 
        }
        for(Case c:Trigger.new){
            SFP_Asset__c sfpa=iaIdSFPAMap.get(c.Barcode__c);
            if(sfpa!=null && sfpa.SFP__r!=null)c.W_M_SFP__c=sfpa.SFP__r.id;
            //c.addError('测试：'+c.W_M_SFP__c);
        }
        
        //Case的owner只能是CCR、HRE(CSE)或HPE(PSE)（检查Owner的简档）
        // https://cs5.salesforce.com/a0aO0000000Cavc
        if(Trigger.isUpdate){
        for(Case c:Trigger.new){
            String msg='';
            Boolean errNote=false;
            String idSObjectType='';
            Case caseOld=caseOldMap.get(c.id);
            Case caseNew=caseNewMap.get(c.id);
            User oldUser=null,newUser=null;
            if(caseNew.OwnerId!=null){
                idSObjectType=EdObject.getSObjectTypeNameFromId(caseNew.OwnerId);
                if(idSObjectType!=null && idSObjectType.toUpperCase().equals('GROUP')){
                    continue;
                }else{
                    newUser=caseNew==null?null:ownerMap.get(caseNew.OwnerId);
                }
            }
            if(caseOld!=null && caseOld.OwnerId!=null){
                idSObjectType=EdObject.getSObjectTypeNameFromId(caseOld.OwnerId);
                if(idSObjectType!=null && idSObjectType.toUpperCase().equals('GROUP')){
                    //continue;
                }else{
                    oldUser=caseOld==null?null:ownerMap.get(caseOld.OwnerId);
                }
            }
            msg='类型:'+idSObjectType+c.OwnerId;
                
                if(oldUser!=null && oldUser.Profile!=null){
                    if( oldUser.Profile.Name.indexOf('CCR')>0 ||
                        oldUser.Profile.Name.indexOf('HRE')>0 ||
                        oldUser.Profile.Name.indexOf('HPE')>0 ||
                        oldUser.Profile.Name.indexOf('CSE')>0 ||    //Polaris Yu 2012-7-17
                        oldUser.Profile.Name.indexOf('PSE')>0 ||
                        oldUser.Profile.Name.indexOf('管理员')>0 ||
                        oldUser.Profile.Name.indexOf('System Administrator')>=0 ) {
                        errNote=false; 
                        msg+=',1点';
                    }else{
                        errNote=false; 
                        msg+=',2点';
                    }
                }
                if(newUser!=null && newUser.Profile!=null){
                    if( newUser.Profile.Name.indexOf('CCR')>0 ||
                        newUser.Profile.Name.indexOf('HRE')>0 ||
                        newUser.Profile.Name.indexOf('HPE')>0 ||
                        newUser.Profile.Name.indexOf('CSE')>0 ||
                        newUser.Profile.Name.indexOf('PSE')>0 ||
                        newUser.Profile.Name.indexOf('管理员')>0 ||
                        newUser.Profile.Name.indexOf('System Administrator')>=0 
                        ) {
                        errNote=errNote || false; 
                        msg+=',3点';
                    }else{
                        errNote=errNote || true; 
                        msg+=',4点';
                    }
                }else{
                    errNote=errNote || true; 
                }
            if(errNote){//newUser.Profile.Name+','+errNote+','+msg+','+
                c.addError('Only CCR, HRE(CSE) and HPE(PSE) can be the service request\'s owner. 只有CCR、HRE(CSE)、HPE(PSE)和管理员可以为服务请求的所有人。'
                    //+'newUser.Profile.Name:'+newUser.Profile.Name
                    //+',oldUser.Profile.Name:'+(oldUser==null?'原所有人空':(oldUser.Profile==null?'简档空':oldUser.Profile.Name))
                    //+'msg:'+msg
                    );
            }
        }
        }
        if(Trigger.isUpdate){
            for(Case c:Trigger.new){
                Case caseOld=caseOldMap.get(c.id);
                Case caseNew=caseNewMap.get(c.id);
                User oldUser=ownerMap.get(caseOld.OwnerId);
                User newUser=caseNew==null?null:ownerMap.get(caseNew.OwnerId);
                System.debug('the caseOld:-----------' + caseOld);
                System.debug('the caseNew:-----------' + caseNew);
                System.debug('the oldUser:-----------' + oldUser);
                System.debug('the newUser:-----------' + newUser);
                /**暂停 HPE Actual Time 
                if(oldUser!=null && oldUser.Profile.Name.indexOf('HPE')>0){
                    if(oldUser.id!=newUser.Id || c.Status=='Pending for Survey')
                        c.HPE_Actual_Time__c=System.now();
                } 
                **/
                
                // Polaris Yu, 2012-7-23, Added Profile 'CSE' and 'PSE'
                if(newUser!=null && ( newUser.Profile.Name.indexOf('HRE')>0 
                   || newUser.Profile.Name.indexOf('CSE')>0)){
                    if(oldUser!=null && oldUser.id!=newUser.Id)
                        c.HRE_Start_Time__c=System.now();
                }
                if( (newUser!=null && ( newUser.Profile.Name.indexOf('HPE')>0
                    || newUser.Profile.Name.indexOf('PSE')>0 )) 
                    && (oldUser!=null && ( oldUser.Profile.Name.indexOf('HPE')<0
                    || oldUser.Profile.Name.indexOf('PSE')<0))){
                    if(oldUser.id!=newUser.Id)
                        c.HPE_Start_Time__c=System.now();
                }
                //原来当Owner从HRE/CSE转给HPE时，更新HPE Start Time，
                //现在增加同时更新HRE Actual Time
                //https://cs5.salesforce.com/a0aO0000000CaxT/e?retURL=%2Fa0aO0000000CaxT
                if( (newUser!=null && ( newUser.Profile.Name.indexOf('HPE')>0
                                        || newUser.Profile.Name.indexOf('PSE')>0 ))
                    && ( oldUser!=null && ( oldUser.Profile.Name.indexOf('HRE')>0
                                            || oldUser.Profile.Name.indexOf('CSE')>0 ))){
                    c.HRE_Actual_Time__c=System.now(); 
                  }
            }
        }
        if(Trigger.isUpdate){
            //Owner_Employee_Id__c
            Set<Id> ownerIdSet2=new Set<Id>();//id=c.ownerid
            for(Case c:Trigger.new){
                ownerIdSet2.add(c.OwnerId);
                //ownerIdSet2.add(c.Owner__c);
            }
            for(Case c:Trigger.old){
                ownerIdSet2.add(c.OwnerId);
                //ownerIdSet2.add(c.Owner__c);
            }
            List<User> userList=[select id,name,EmployeeNumber,Employee_ID__c from User where id in :ownerIdSet2];
            //trigger.new[0].addError('测试:userList size:'+userList.size());
            Map<Id,User> userIdUserMap=new Map<Id,User>();
            for(User u:userList){
                userIdUserMap.put(u.id,u);
            }
            for(Case c:Trigger.new){
                User usr=userIdUserMap.get(c.OwnerId);
                    //c.addError('测试:usr:'+usr.id);
                if(usr!=null){
                    //c.Owner_Employee_Id__c=usr.Employee_ID__c;//usr.EmployeeNumber;
                    c.Owner__c=c.OwnerId;
                    //c.addError('测试:UserInfo.getUserId():'+UserInfo.getUserId());
                }
            }
        }
        
        for(Case c:Trigger.new){
            Decimal res=0;
            
            //修改Overall_SLA_Goal_Hr__c:SLA Goal of Response Time (Min) / 60 + SLA Goal of Restore Time (Hr) + SLA Goal of Resolve Time (Day)*24
            Decimal rp_HR=c.SLA_Goal_of_Response_Time__c==null?0:c.SLA_Goal_of_Response_Time__c/60;
            Decimal rt_HR=c.SLA_Goal_of_Restore_Time__c==null?0:c.SLA_Goal_of_Restore_Time__c;
            Decimal rs_HR=c.SLA_Goal_of_Resolve_Time__c==null?0:c.SLA_Goal_of_Resolve_Time__c*24;
            
            res=rp_HR+rt_HR+rs_HR;
            //c.Overall_SLA_Goal_Hr__c=res;//因为提到不需要,所以灰掉
            
            if((c.Type=='Online' 
                && c.Response_Performance__c!=null 
                && c.Restore_Performance__c!=null
                && c.Resolve_Performance__c!=null
               // && c.Overall_SLA_Performance_Hr__c!=null
                )||(
                c.Type!='Online' 
                && c.Response_Performance__c!=null 
                && c.Resolve_Performance__c!=null
                )
                ){
                //IF((Response Time Win/Lost + Restore Time Win/Lost + Resolution time win/lost) / 3<1,0,1)  
                Decimal rp_wl=c.Response_Performance__c==null?0:c.Response_Performance__c;
                Decimal rt_wl=c.Restore_Performance__c==null?0:c.Restore_Performance__c;
                Decimal rs_wl=c.Resolve_Performance__c==null?0:c.Resolve_Performance__c;
                if(c!=null && c.Type!=null && !c.Type.toLowerCase().equals('online')) res=(rp_wl+rs_wl)/2;
                else res=(rp_wl+rt_wl+rs_wl)/3;
                if(res<100){
                   res=0;
                }else{
                   res=100;
                }
                c.Overall_SLA_Performance_Hr__c=res;
            }
            //将指定字段修改为最小值
            //MIN( Response_SLA_By_Type_Min__c , Response_SLA_By_Customer_Lvl_Min__c , Response_SLA_by_SFP_Min__c )
            Decimal dec=0;
            List<Decimal> decList=new List<Decimal>();
            if(c.Response_SLA_By_Type_Min__c!=null)decList.add(c.Response_SLA_By_Type_Min__c);
            if(c.Response_SLA_By_Customer_Lvl_Min__c!=null)decList.add(c.Response_SLA_By_Customer_Lvl_Min__c);
            if(c.Response_SLA_by_SFP_Min__c!=null)decList.add(c.Response_SLA_by_SFP_Min__c);
            if(!decList.isEmpty()) c.SLA_Goal_of_Response_Time__c=MathTool.getMin(decList);
            else c.SLA_Goal_of_Response_Time__c=null;
            decList.clear();
            
            //MIN( Restore_SLA_By_Type_Hr__c , Restore_SLA_By_Customer_Lvl_Hr__c , Restore_SLA_by_SFP_Hr__c )
            if(c.Restore_SLA_By_Type_Hr__c!=null)decList.add(c.Restore_SLA_By_Type_Hr__c);
            if(c.Restore_SLA_By_Customer_Lvl_Hr__c!=null)decList.add(c.Restore_SLA_By_Customer_Lvl_Hr__c);
            if(c.Restore_SLA_by_SFP_Hr__c!=null)decList.add(c.Restore_SLA_by_SFP_Hr__c);
            if(!decList.isEmpty())c.SLA_Goal_of_Restore_Time__c=MathTool.getMin(decList);
            else c.SLA_Goal_of_Restore_Time__c=null;
            decList.clear();
            
            //MIN( Resolve_SLA_By_Type_Day__c , Resolve_SLA_By_Customer_Lvl_Day__c , Resolve_SLA_by_SFP_Day__c )
            if(c.Resolve_SLA_By_Type_Day__c!=null)decList.add(c.Resolve_SLA_By_Type_Day__c);
            if(c.Resolve_SLA_By_Customer_Lvl_Day__c!=null)decList.add(c.Resolve_SLA_By_Customer_Lvl_Day__c);
            if(c.Resolve_SLA_by_SFP_Day__c!=null)decList.add(c.Resolve_SLA_by_SFP_Day__c);
            if(!decList.isEmpty()) c.SLA_Goal_of_Resolve_Time__c=MathTool.getMin(decList);
            else c.SLA_Goal_of_Resolve_Time__c=null;
            decList.clear();
        } 
    }
    System.debug('in the TigOnCase method:--------------' + Datetime.now().getTime());
}