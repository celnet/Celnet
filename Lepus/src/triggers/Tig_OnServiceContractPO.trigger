/**************************************************************************************************
 * Name             : Tig_OnServiceContractPO
 * Object           : Contract_Product_PO__c
 * Requirement      : 
 * Purpose          : 
 * Author           : Iverson gao
 * Create Date      : 07/12/2012
 * Modify History   : 
***************************************************************************************************/ 
trigger Tig_OnServiceContractPO on Contract_Product_PO__c (before insert) {
	if(trigger.isBefore && trigger.isInsert && trigger.new.size() > 0){
		List<Contract_Product_PO__c> cpoList = new List<Contract_Product_PO__c>();
		Set<Id> relatedContractIdSet = new Set<Id>();
		for (Contract_Product_PO__c cpo : trigger.new){
			relatedContractIdSet.add(cpo.Contract_No__c);
		}
		Map<Id, CSS_Contract__c> regionDeptMap =
			new Map<Id, CSS_Contract__c>([SELECT Id, Region_Dept__c FROM CSS_Contract__c
										  WHERE Id IN: relatedContractIdSet]);
		System.debug('\n=========>\nThe size of ' + 'regionDeptMap is: ' + regionDeptMap.size() + '.');
					 
		if (regionDeptMap.size() > 0){
			for (Contract_Product_PO__c cpo : trigger.new){
				cpo.Region_Dept__c = regionDeptMap.get(cpo.Contract_No__c).Region_Dept__c;
			}
		}
	}   
}