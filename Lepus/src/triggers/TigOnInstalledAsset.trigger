trigger TigOnInstalledAsset on Installed_Asset__c (before insert) {//ok+

	/**
	for(Installed_Asset__c ia:Trigger.new){
		CSS_Contract__c ccc=[select id,name,Account_Name__c from CSS_Contract__c where id=:ia.Contracts_No__c limit 1];
		ia.Account_Name__c=ccc.Account_Name__c;
	}
	**/
	if(Trigger.isInsert){
		Map<Id,CSS_Contract__c> cccMap=new Map<Id,CSS_Contract__c>();
		
		List<Id> contractIdSet=new List<Id>();
		for (Integer i = 0; i < Trigger.new.size(); i++){
			if(Trigger.new[i].Contracts_No__c!=null)contractIdSet.add(Trigger.new[i].Contracts_No__c);
		}
		
		List<CSS_Contract__c> cccList=[select c.id,c.name,c.Account_Name__c
			,c.Sub_Region__c, c.Street__c, c.Region__c, c.Postal_Code__c, c.Other_City__c, c.City__c
			from CSS_Contract__c c where c.id in :contractIdSet];
		for(CSS_Contract__c c:cccList){
			if(cccMap.containsKey(c.id))continue;
			cccMap.put(c.id,c);
		}
		
		for(Installed_Asset__c i:Trigger.new){
			if(!cccMap.containsKey(i.Contracts_No__c))continue;
			CSS_Contract__c ccc=cccMap.get(i.Contracts_No__c);
			if(ccc==null)continue;
			i.Account_Name__c=ccc.Account_Name__c;
			//updated by czh 2013-12-27
			/*
			i.SubRegion__c=ccc.Sub_Region__c;
			i.Street__c=ccc.Street__c;
			i.Region__c=ccc.Region__c;
			i.Postal_Code__c=ccc.Postal_Code__c;
			i.Other_City__c=ccc.Other_City__c;
			i.City__c=ccc.City__c;
			*/
		}
	}
}