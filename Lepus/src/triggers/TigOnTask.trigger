trigger TigOnTask on Task (before insert, before update) {

	List<Id> userIdList=new List<Id>();//id=tk.OwnerId
	Map<Id,User> userMap=new Map<Id,User>();//id=user.id
	
	List<Id> contactIdList=new List<Id>();//id=tk.OwnerId
	Map<Id,Contact> contactMap=new Map<Id,Contact>();//id=contact.id
	
	List<Id> pdcIdList=new List<Id>();//id=tk.What
	
	List<Id> RecordtypeIdList= new List<Id>();//id=tk.recordtype
	
	for(Task tk:Trigger.new){
		userIdList.add(tk.OwnerId);
		contactIdList.add(tk.WhoId);
		pdcIdList.add(tk.whatId);
		RecordtypeIdList.add(tk.RecordTypeId);
	}
	for(Id id:RecordtypeIdList ){
	if(id != '012900000000OIP' ){
	
	List<User> userList=[select id,name,Employee_ID__c from User where id in :userIdList];
	for(User u:userList){
		userMap.put(u.id,u);
	}
	
	List<Contact> contactList=[select c.id,c.name,c.MobilePhone,c.Account.Name from Contact c where c.id in :contactIdList];
	for(Contact c:contactList){
		contactMap.put(c.id,c);
	}
	
	Map<Id,Product_Details__c> pdcMap=new Map<Id,Product_Details__c>();//id=pdc.id
	List<Product_Details__c> pdcList=[Select p.Product_Name__c, p.Product_Family__c, p.Id 
		From Product_Details__c p 
		where p.id in :pdcIdList];
	for(Product_Details__c pdc:pdcList){
		pdcMap.put(pdc.id,pdc);
	}	
		
		
	for(Task tk:Trigger.new){
		
		User usr=null;
		Contact ctt=null;
		Product_Details__c pdc=null;
		if(tk.OwnerId!=null)usr=userMap.get(tk.OwnerId);
		if(tk.WhoId!=null){
			ctt=contactMap.get(tk.WhoId);
			String whoTypeName=EdObject.getSObjectTypeNameFromId(tk.WhoId);
			if(whoTypeName!=null && whoTypeName.toLowerCase().equals('contact')){
				tk.Account_Name__c=ctt.Account.Name;
			}
		}
		if(Trigger.isInsert){
			//只在创建任务时，更新CCR In Charge为Owner用户，Owner改变时CCR In Charge不更新
			//https://cs5.salesforce.com/a0aO0000000Cavr
			if(usr!=null && usr.name!=null)tk.CCR_in_Charge__c=usr.name;
		}
		//if(usr!=null && usr.Employee_ID__c!=null)tk.OwnerId=usr.Employee_ID__c;//usr.name;
		if(usr!=null)tk.OwnerId=usr.Id;//usr.name;
		if(ctt!=null && ctt.MobilePhone!=null)tk.Mobile__c=ctt.MobilePhone;
		if(tk.WhatId!=null)pdc=pdcMap.get(tk.WhatId);
		if(pdc!=null ){
			tk.Product_Detail__c=pdc.Product_Name__c;
			//tk.Product_Family__c=pdc.Product_Family__c;
		}

	}
	}
	}
	
}