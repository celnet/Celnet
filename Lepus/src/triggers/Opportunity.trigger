trigger Opportunity on Opportunity (after insert, after update, before insert, before update, after delete) {
 
  new Triggers()
    
    .bind(Triggers.Evt.beforeinsert, new Lepus_WisdomOpportunityWriteBackHandler()) // 从Wisdom 回写到Salesforce给控制字段赋值
    .bind(Triggers.Evt.beforeupdate, new Lepus_WisdomOpportunityWriteBackHandler()) // 从Wisdom 回写到Salesforce给控制字段赋值
    
    .bind(Triggers.Evt.afterinsert,new Lepus_SetGlobalIdHandler())            //(ALL)设置Global ID 字段

    .bind(Triggers.Evt.beforeinsert,new OpportunityFieldsPopulater())         //(中国区)根据客户的归属/行业/TCG信息刷新机会点的相应属性
    .bind(Triggers.Evt.beforeupdate,new OpportunityFieldsPopulater())         //(中国区)根据客户的归属/行业/TCG信息刷新机会点的相应属性

    .bind(Triggers.Evt.beforeinsert,new OverseaOpportunityUpdateRegionInfo()) //(海外)根据客户上的相关信息刷新机会点属性
    .bind(Triggers.Evt.beforeupdate,new OverseaOpportunityUpdateRegionInfo()) //(海外)根据客户上的相关信息刷新机会点属性

    .bind(Triggers.Evt.beforeinsert,new ChinaOpportunityUpdateDictCode())     //(中国区)将机会点的9个字段翻译成对应的CODE(PRM集成需要)
    .bind(Triggers.Evt.beforeupdate,new ChinaOpportunityUpdateDictCode())     //(中国区)将机会点的9个字段翻译成对应的CODE(PRM集成需要)

    .bind(Triggers.Evt.beforeupdate,new OverseaOpportunityUpdateDealStatus()) //(海外)机会点状态改变时同时刷新关联的报备
    .bind(Triggers.Evt.beforeupdate,new ChinaOpportunityUpdateDealStatus())   //(中国区)机会点状态改变时同时刷新关联的报备
    
    .bind(Triggers.Evt.beforeinsert,new OpportunityUpdateStageHandler())      //(ALL)机会点关闭状态改变时刷新机会点NA类型
    .bind(Triggers.Evt.beforeupdate,new OpportunityUpdateStageHandler())      //(ALL)机会点关闭状态改变时刷新机会点NA类型

    .bind(Triggers.Evt.beforeupdate,new OverseaOpportunityUpdateOIInfo())     //(海外)机会点根据阶段的变化记录OI和OO

    .bind(Triggers.Evt.afterdelete, new OpportunityArchiveForBIHandler())     //(ALL)删除时把记录保存到数据维护历史 供BI抓取
    .bind(Triggers.Evt.afterupdate, new ChinaDealOpportunityUpdateHistory())  //(中国区)更新报备关联的机会点的关键字段时记录ID供PRM抓取

    .bind(Triggers.Evt.afterinsert, new Lepus_OpportunityHandler())           //(ALL)创建时同步数据到EIP
    .bind(Triggers.Evt.afterupdate, new Lepus_OpportunityHandler())           //(ALL)创建时同步数据到EIP
    .bind(Triggers.Evt.afterdelete, new Lepus_OpportunityHandler())           //(ALL)创建时同步数据到EIP
    
    .bind(Triggers.Evt.afterinsert, new ChinaSpecialOpportunityHistory())     //(中国区)容灾解决方案的机会点创建时同步到PRM
    .bind(Triggers.Evt.afterupdate, new ChinaSpecialOpportunityHistory())     //(中国区)机会点容灾解决方案发生了改变同步到PRM

    .manage();
}