/*
* @Purpose: Test EditCaseWithoutConflictCheck extension controller
*
* @Author: Tommy Liu(tommyliu@celnet.com.cn)
* @Date: 2014-2-12
*/
@isTest
private class EditCaseWithoutConflictCheckTest  
{
    static testMethod void testSaveComments() 
    {
        Case sampleCase = new Case();
        sampleCase.status = 'Handing';
        sampleCase.subject = 'this is a sample case';
        insert sampleCase;
        
        sampleCase.subject = 'this case was modifed';
        ApexPages.StandardController sc = new ApexPages.StandardController(sampleCase);
        EditCaseWithoutConflictCheck editCaseExtension = new EditCaseWithoutConflictCheck(sc);
        editCaseExtension.comments = 'this is case comment1';
        System.PageReference pf = editCaseExtension.save();
        
        sampleCase = [Select Id, Subject, (Select Id, CommentBody From CaseComments) From Case Where Id=: sampleCase.ID];
        System.assert(pf != null);
        System.assertEquals('this case was modifed', sampleCase.Subject);
        System.assertEquals(1, sampleCase.CaseComments.size());
        System.assertEquals('this is case comment1', sampleCase.CaseComments[0].CommentBody);
    }
    
    //测试： 避免页面的旧值修改了数据库的新值，在用户打开页面过程中后台的中间件可能已经将值更新
    static testMethod void testSaveType() 
    {
        Case sampleCase = new Case();
        sampleCase.status = 'Handing';
        sampleCase.subject = 'this is a sample case';
        sampleCase.Type = 'Fault';
        sampleCase.Problem_Priority__c = 'Minor';
        insert sampleCase;
        
        //模拟用户打开界面
        sampleCase.subject = 'this case was modifed';
        ApexPages.StandardController sc = new ApexPages.StandardController(sampleCase);
        EditCaseWithoutConflictCheck editCaseExtension = new EditCaseWithoutConflictCheck(sc);
        Case theCase = (Case)sc.getRecord();
        theCase.Type = 'Fault';
        theCase.Problem_Priority__c = 'Minor';
        
        //模拟中间件更新
        Case upCase = new Case();
        upCase.Id = sampleCase.Id;
        upCase.Type = 'Engineering';
        upCase.Problem_Priority__c = 'Major';
        update upCase;
        
				//模拟用户保存页面
        System.PageReference pf = editCaseExtension.save();
        
        sampleCase = [Select Id, Subject, Problem_Priority__c, Type From Case Where Id=: sampleCase.ID];
        System.assertEquals('this case was modifed', sampleCase.Subject);
        System.assertEquals('Engineering', sampleCase.Type);
        System.assertEquals('Major', sampleCase.Problem_Priority__c);
    }
}