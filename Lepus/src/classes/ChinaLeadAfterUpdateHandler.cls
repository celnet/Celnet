/**
 * @Purpose : 1、当手动关联机会点且没有最终客户：自动关联机会点的最终客户、自动将线索设置为关闭状态、确认机会点没有被其他线索关联
 *            2、当手动关联机会点维护了最终客户：判断机会点的最终客户是否和目前客户一致、其他同上
 *				    3、当修改关联机会点：一般用户没有该权限，提示不允许修改（在校验规则中做）；如果修改同上面逻辑，且清掉之前机会点的线索字段的值
 *				    4、当解除关联机会点：一般用户没有该权限，提示不允许修改（在校验规则中做）；如果解除后清空机会点上的线索字段的值
 *						5、对于“待分发”或者“待确认”的线索，如果选择了线索跟进方式，就认为线索已经被确认，线索状态自动更新成“跟进中”状态；
 * @Author : Steven
 * @Date : 2013-10-29
 */
public without sharing class ChinaLeadAfterUpdateHandler implements Triggers.Handler{ //after update
	public static Boolean isFirstRun = true;
	public void handle() {
		if(!ChinaLeadAfterUpdateHandler.isFirstRun) return;
		Lead updateLead = new Lead();
		Opportunity updateOldOpp = new Opportunity();
		Opportunity updateNewOpp = new Opportunity();
		//考虑到这些场景处理的数据量很小，不会有大数量导入场景，直接在这里逐条处理
		for(Lead l : (List<Lead>)Trigger.new){
			if(l.RecordTypeId == CONSTANTS.CHINALEADRECORDTYPE) {
				Id newRelatedOpp = l.China_Opportunity__c;
				Id oldRelatedOpp = ((Lead)Trigger.oldMap.get(l.id)).China_Opportunity__c;
				//Logic1: 关联一个新机会点
				if(oldRelatedOpp == null && newRelatedOpp != null) {
					updateLead = [SELECT Id,STATUS,Follow_Up_Type__c,Status_Changed__c,Account_Name__c,China_Opportunity__c FROM Lead where Id =: l.Id];
					updateNewOpp = [SELECT Id, Lead__c,Lead__r.Name, AccountId FROM Opportunity where Id =: newRelatedOpp];
					if(updateLead.Follow_Up_Type__c==null) {
						l.addError('必须选择一个"线索跟进方式"');
					} else if(updateNewOpp.Lead__c != null) {
						l.addError('该机会点已被线索"'+updateNewOpp.Lead__r.Name+'"关联');
					} else if(l.Account_Name__c != null && l.Account_Name__c != updateNewOpp.AccountId){
						l.addError('线索的最终客户和机会点的最终客户不一致');
					} else {
						updateLead.Status = CONSTANTS.CHINALEADSTATUSCLOSED;
						updateLead.Account_Name__c = updateNewOpp.AccountId;
						updateLead.Status_Changed__c = !updateLead.Status_Changed__c;
						updateNewOpp.Lead__c = updateLead.Id;
						update updateLead;
						update updateNewOpp;
					}
				}
				//Logic2: 删除关联的机会点(普通用户没有该权限，通过校验规则控制)
				if(oldRelatedOpp != null && newRelatedOpp == null) {
					updateOldOpp = [SELECT Id, Lead__c FROM Opportunity where Id =: oldRelatedOpp];
					updateOldOpp.Lead__c = null;
					update updateOldOpp;
				}
				//Logic3: 修改关联的机会点(普通用户没有该权限，通过校验规则控制)
				if(oldRelatedOpp != null && newRelatedOpp != null && oldRelatedOpp != newRelatedOpp) {
					updateOldOpp = [SELECT Id, Lead__c FROM Opportunity where Id =: oldRelatedOpp];
					updateNewOpp = [SELECT Id, Lead__c, AccountId FROM Opportunity where Id =: newRelatedOpp];
					if(updateNewOpp.Lead__c != null) {
						l.addError('该机会点已被其他线索关联LeadId:'+updateNewOpp.Lead__c);
					} else if(l.Account_Name__c != null && l.Account_Name__c != updateNewOpp.AccountId){
						l.addError('线索的最终客户和机会点的最终客户不一致');
					} else {
						updateOldOpp.Lead__c = null;
						updateNewOpp.Lead__c = l.Id;
						update updateOldOpp;
						update updateNewOpp;
					}
				}
				//Logic4: 对于“待分发”或者“待确认”的线索，如果选择了线索跟进方式，就认为线索已经被确认，线索状态自动更新成“跟进中”状态；
				String newFollowUpType = l.Follow_Up_Type__c;
				String oldFollowUpType = ((Lead)Trigger.oldMap.get(l.id)).Follow_Up_Type__c;
				if(l.Status == CONSTANTS.CHINALEADSTATUSASSIGN || l.Status == CONSTANTS.CHINALEADSTATUSCONFIRM) {
					if((oldFollowUpType == null || oldFollowUpType == '') && (newFollowUpType != null && newFollowUpType != '')){
						updateLead = [SELECT Id,STATUS,Status_Changed__c FROM Lead where Id =: l.Id];
						updateLead.Status = CONSTANTS.CHINALEADSTATUSFOLLOW;
						updateLead.Status_Changed__c = !updateLead.Status_Changed__c;
						update updateLead;
					}
				}
				
			}
		}
	} // handle end

}