/*
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-8-27
 * Description: 
 */
public class Lepus_ContactHandler implements Triggers.Handler{
	public static boolean isFirstRun = true;
    public void handle(){
    	
        if((Lepus_Data_Sync_Controller__c.getInstance('联系人业务数据') != null) && Lepus_Data_Sync_Controller__c.getInstance('联系人业务数据').IsSync__c){
        	List<Lepus_Log__c> logList = new List<Lepus_Log__c>();
        	
        	List<Id> contactIdList = new List<Id>();
			String action = Lepus_HandlerUtil.retrieveAction();
			List<Contact> contactList = Lepus_HandlerUtil.retrieveRecordList();
			Map<ID , Contact> map_Contact = new Map<ID,Contact>();
			
			for(Contact con : contactList){
				if(Lepus_HandlerUtil.filterRecordType(con, Contact.sobjecttype)){
					Lepus_Log__c log = new Lepus_Log__c();
					log.RecordId__c = con.Id;
					log.LastModifiedDate__c = con.LastModifiedDate;
					log.UniqueId__c = con.Id + con.LastModifiedDate.formatGMT('yyyy-MM-dd HH:mm:ss');
					log.InHandler__c = true;
					logList.add(log);
					
					contactIdList.add(con.Id);
					map_Contact.put(con.Id , con);
				}
			}
			
			if(logList.size() > 0)
			upsert logList UniqueId__c;
			
			if(contactIdList.size() == 1){
				Lepus_FutureCallout.syncData(contactIdList[0], action, 'contact');
				
				List<Lepus_Log__c> logList2 = new List<Lepus_Log__c>();
				
				for(Contact con : map_Contact.values()){
					Lepus_Log__c log = new Lepus_Log__c();
					log.UniqueId__c = con.Id + con.LastModifiedDate.formatGMT('yyyy-MM-dd HH:mm:ss');
					log.InFuture__c = true;
					logList2.add(log);
				}
				
				upsert logList2 UniqueId__c;
				
			} else if(contactIdList.size() > 1){
				Lepus_SyncUtil.initQueue(map_Contact, contactIdList, action, '业务数据同步', datetime.now());
				
				List<Lepus_Log__c> logList3 = new List<Lepus_Log__c>();
				
				for(Contact con : map_Contact.values()){
					Lepus_Log__c log = new Lepus_Log__c();
					log.UniqueId__c = con.Id + con.LastModifiedDate.formatGMT('yyyy-MM-dd HH:mm:ss');
					log.InQueue__c = true;
					logList3.add(log);
				}
				
				upsert logList3 UniqueId__c;
			}
        }
    }
}