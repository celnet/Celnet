public with sharing class TestTig_WorkOrder {
    public static testMethod void myUnitTest() {
      EdObject.currClsTest();
      MathTool.currClsTest();
      Account acc = UtilUnitTest.createHWChinaAccount('t');
      Case c = UtilUnitTest.createChinaServiceRequest();
      Work_Order__c wo= new Work_Order__c(status__c='Working',Account_Name__c = acc.id,Service_Request_No__c = c.id);
      insert wo;
      wo=[Select w.Work_Order_Type__c, w.Work_Hour_Value_added_Standalone__c, w.Work_Hour_Hardware_software__c, w.Work_Description__c, w.Warranty_or_Maintanence__c, w.WO_Running_No__c, w.Type__c, w.Training_duration_day__c, w.Training_Venue__c, w.Training_Survey_Score_SUM__c, w.Training_Survey_Score_AVG__c, w.Training_Survey_Count__c, w.Training_Supervisor__c, w.Training_Request_Date__c, w.Training_Name__c, w.Training_Manager__c, w.Training_Description__c, w.Training_Content__c, w.Training_Completion_Date__c, w.Trainer__c, w.Total_Work_Hour__c, w.Total_Work_Hour_Value_added_Standalone__c, w.Total_Work_Hour_Hardware_Software__c, w.Technical_Support_Summary__c, w.SystemModstamp, w.Suspend__c, w.Suspend_Reason__c, w.Surveyed__c, w.Survey_Score_SUM__c, w.Survey_Score_AVG__c, w.Survey_Rejected_Reason__c, w.Survey_Not_Required__c, w.Survey_Not_Required_Reason__c, w.Survey_Method__c, w.Survey_Count__c, w.Survey_Completed__c, w.Sub_Region__c, w.Sub_Region1__c, w.Street__c, w.Street1__c, w.Status__c, w.Spare_Part_Arrival_Time__c, w.Spare_Part_Arrival_On_Time__c, w.Solution_Attachment__c, w.Solution_Attachment_Path__c, w.Service_Start_Time__c, w.Service_Request_No__c, w.Service_Provider__c, w.Service_Provider1__c, w.Service_Party__c, w.Service_Package__c, w.Service_Entitlement__c, w.Service_Entitlement1__c, w.Service_Description_and_Remarks__c, w.Service_Completion_Time__c, w.Service_Branch__c, w.Service_Branch1__c, w.SR_Owner_Email__c, w.SPM_Region__c, w.SMS_Notification_Message__c, w.SLA_Restore_Time__c, w.SLA_Response_Time__c, w.SLA_Reminder_Time__c, w.SLA_Recover_Time__c, w.SLA_Performance__c, w.SLA_Name__c, w.SLA_Goal__c, w.SLA_Due_Time__c, w.SFP_No__c, w.Requirement_Solution__c, w.Requirement_Solution1__c, w.Requested_Resolution_Time__c, w.Requested_Resolution_Time_SR__c, w.Requested_On_Site_Time__c, w.Requested_Arrival_Time__c, w.Report_No__c, w.Report_Filed_Submitted__c, w.Replacement_of_Spare_Part__c, w.Remarks__c, w.Regions__c, w.Region__c, w.Region1__c, w.Record_Type_Name__c, w.RecordTypeId, w.Project_Manager__c, w.Project_Manager_Contact_No__c, w.Product_Name__c, w.Problem_Solved__c, w.Postal_Code__c, w.Postal_Code1__c, w.Owner__c, w.Owner_Mobile__c, w.OwnerId, w.Overall_Satisfactory__c, w.Outsource_Onsite_Service__c, w.Other_City__c, w.Other_City1__c, w.Onsite_Service_Advice__c, w.No_of_Trainee__c, w.Name, w.Main_Distributor__c, w.LastModifiedDate, w.LastModifiedById, w.LastActivityDate, w.IsDeleted, w.Intallation_Sub_Region__c, w.Installed_Asset_Filed_Updated__c, w.Installation_Street__c, w.Installation_Postal_Code__c, w.Installation_Other_City__c, w.Installation_Method__c, w.Installation_City__c, w.Initial_Test_Date__c, w.Id, w.Gold_Silver_Distributor__c, w.Final_Test_Date__c, w.Fault_Type__c, w.Engineer_Setoff_Time__c, w.Engineer_Return_Time__c, w.Engineer_In_Charge__c, w.End_Distributor__c, w.ERP_Account_Number__c, w.CurrencyIsoCode, w.CreatedDate, w.CreatedById, w.Coordinator_Name__c, w.Coordinator_Contact_No__c, w.Contract_Status__c, w.Contract_No__c, w.Contact_Phone__c, w.Contact_Person__c, w.Contact_Mobile_No__c, w.Configuration_Detail__c, w.Configuration_Detail_Path__c, w.Closed_Date__c, w.Click_to_View_Solution__c, w.Click_to_View_Solution1__c, w.Click_to_View_Configuration__c, w.Click_to_View_Configuration_Detail__c, w.City__c, w.City1__c, w.Barcode_not_in_system__c, w.Barcode__c, w.Authorized_Training_Organization__c, w.Asset__c, w.Application_of_Part_Required__c, w.Additional_Service_Term__c, w.Accurate_Spare_Part_Fault_Diagnosis__c, w.Account_Name__c, w.Account_Manager__c, w.Account_Manager_Phone_Number__c, w.Account_Manager_Contact_No__c, w.Account_Manager1__c 
      From Work_Order__c w where w.status__c='Working'
      limit 1];
      
      Work_Order__c wo0=new Work_Order__c();     
	  wo0=wo.clone(false);
      insert(wo0);	
	 
      
   /*  
      Work_Order_Asset__c woa=[Select w.Work_Order_No__c, w.WO_Sub_Region__c, w.WO_Street__c, w.WO_Region__c, w.WO_Postal_Code__c, w.WO_Other_City__c, w.WO_No__c, w.WO_City__c, w.SystemModstamp, w.Sub_Region__c, w.Street__c, w.SLA_Restore_Time__c, w.SLA_Response_Time__c, w.SLA_Recover_Time__c, w.SLA_Name__c, w.Region__c, w.Quantity__c, w.Product_Name__c, w.Postal_Code__c, w.Other_City__c, w.Name, w.LastModifiedDate, w.LastModifiedById, w.LastActivityDate, w.IsDeleted, w.Installed_Asset__c, w.Id, w.CurrencyIsoCode, w.CreatedDate, w.CreatedById, w.City__c, w.Actual_Work_Hour__c From Work_Order_Asset__c w
        limit 1];
      Work_Order_Asset__c woa0=new Work_Order_Asset__c();
      woa0=woa.clone(false);
      insert(woa0);
   */ 
  /* 20130110:临时取消
      Task t=[Select t.WhoId, t.WhatId, t.Valid_Request__c, t.SystemModstamp, t.Subject, t.Status, t.Source__c, t.Service_Type__c, t.Service_ID__c, t.Request_Summary__c, t.Request_Result__c, t.Request_Detail__c, t.ReminderDateTime, t.Region__c, t.RecurrenceType, t.RecurrenceTimeZoneSidKey, t.RecurrenceStartDateOnly, t.RecurrenceMonthOfYear, t.RecurrenceInterval, t.RecurrenceInstance, t.RecurrenceEndDateOnly, t.RecurrenceDayOfWeekMask, t.RecurrenceDayOfMonth, t.RecurrenceActivityId, t.RecordTypeId, t.Product_Family__c, t.Product_Detail__c, t.Priority, t.OwnerId, t.Office__c, t.Mobile__c, t.LastModifiedDate, t.LastModifiedById, t.IsReminderSet, t.IsRecurrence, t.IsDeleted, t.IsClosed, t.IsArchived, t.Id, t.Description, t.CurrencyIsoCode, t.CreatedDate, t.CreatedById, t.CallType, t.CallObject, t.CallDurationInSeconds, t.CallDisposition, t.CCR_in_Charge__c, t.ActivityDate, t.Account_Name__c, t.AccountId From Task t
        where t.Status='Not Started' 
         limit 1];
      Task t0=new Task();
      t0=t.clone(false);
      Profile pf=[select id,Name from Profile where Name like '%Huawei%' limit 1];
	  User u = [select Id,profileId,IsActive from User where IsActive=true and profileId = :pf.id limit 1];
	  System.runAs(u){
		System.debug('Current User: ' + UserInfo.getUserName());
	    System.debug('Current Profile: ' + UserInfo.getProfileId());
      	insert(t0);
	  }
  */    
    }

}