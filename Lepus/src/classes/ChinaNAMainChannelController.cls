/**
* 功能:批量维护NA Partner
* 作者:Steven
* 日期:2014-04-30 
*/
public with sharing class ChinaNAMainChannelController {
  public ID accountId {get;set;}
  public String accountName {get;set;}
  public ID mcExpandId {get;set;}
    public String product {get;set;} 
    public boolean canEdit {get;set;} 
  public Account_Competitor_Situation__c mainChannelExpand {get;set;} //NC拓展主对象
    public list<China_Main_Channel__c> mcListDelete {get;set;} //待删除清单
    public list<China_Main_Channel__c> srNCList {get; set;} //数通NC清单
    public list<China_Main_Channel__c> stNCList {get; set;} //存储NC清单
    public list<China_Main_Channel__c> tvNCList {get; set;} //智真NC清单
    
    /**
     * 构造方法，初始化相关数据
     */
    public ChinaNAMainChannelController(ApexPages.StandardController controller) {
        this.accountId=ApexPages.currentPage().getParameters().get('accId'); 
        this.mcExpandId=ApexPages.currentPage().getParameters().get('ID'); 
        this.srNCList = new list<China_Main_Channel__c>();
        this.stNCList = new list<China_Main_Channel__c>();
        this.tvNCList = new list<China_Main_Channel__c>();
        this.mcListDelete = new list<China_Main_Channel__c>();
        this.canEdit = true;
        
        //A0:获取客户的NC拓展主对象
        List<Account_Competitor_Situation__c> mainChannelExpandList = new List<Account_Competitor_Situation__c>();
        if(this.mcExpandId != null) { //点标准编辑按钮进入
            mainChannelExpandList = [select ID,Account__c,Account__r.Name,Switch_Router_Competitor__c,Storage_Competitor__c,TP_VC_Competitor__c
                                                                 from Account_Competitor_Situation__c where id =: mcExpandId];
        } else { //点批量编辑按钮进入
            mainChannelExpandList = [select ID,Account__c,Account__r.Name,Switch_Router_Competitor__c,Storage_Competitor__c,TP_VC_Competitor__c
                                                                 from Account_Competitor_Situation__c where Account__c =: accountId];
        }
        
        //A1:如果有，取第一条(正常情况下只允许有一条)
        if(mainChannelExpandList.size()>0){
            this.mainChannelExpand = mainChannelExpandList.get(0);
        this.mcExpandId = mainChannelExpand.id;
        this.accountId = mainChannelExpand.Account__c;
        this.accountName = mainChannelExpand.Account__r.Name;
        //读取当前拓展的所有的NC数据
            list<China_Main_Channel__c> listMC = new list<China_Main_Channel__c>(
                [select id,Product__c,Main_Partner_Type__c,Partner_Name__c,Partner_Name_Input__c,IsDelete__c,Partner_Name_Display__c
                     from China_Main_Channel__c 
                    where Account_Competitor_Situation__c  =: mcExpandId]);
            //将所有的的NC数据添加到界面显示的List中
            for(China_Main_Channel__c mc : listMC) {
                //this.mainChannelList.add(mc);
                if('数通' == mc.Product__c) srNCList.add(mc);
                if('存储' == mc.Product__c) stNCList.add(mc);
                if('智真&视讯' == mc.Product__c) tvNCList.add(mc);
            }
        //A2:如果没有，创建一条
        } else {
            this.mainChannelExpand = new Account_Competitor_Situation__c(Account__c = accountId);
            Account acc = [select id,name from account where id =:accountId];
            if(acc != null) this.accountName = acc.Name;
            //insert mainChannelExpand; 不允许在构造方法中创建进行DML操作
        }
        checkAuthority();
    }
    
    /**
     * 构造方法执行完成后，如果判断没有主记录就创建一条（用于点击批量编辑按钮后自动判断执行）
     */
    public void initMainChannel(){
        if(!this.canEdit) return;
        if(this.mainChannelExpand.id == null) insert this.mainChannelExpand;
        this.mcExpandId = this.mainChannelExpand.id;
    }
    
    /**
     * 添加NC行（用于编辑界面点击添加行按钮）
     */
    public void addMainChannel() {
        if(!this.canEdit) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, System.Label.No_Privilege));
            return;
        }
        //创建一条新的NC,类型默认为我司合作的NA主流渠道
        China_Main_Channel__c mc = new China_Main_Channel__c();
        mc.Account_Competitor_Situation__c = mcExpandId;
        mc.Main_Partner_Type__c = '我司合作的NA主流渠道';
        mc.Product__c = this.product;
        //根据产品类型添加到不同的NC列表中
        if('数通' == this.product) {
            srNCList.add(mc);
        } else if('存储' == this.product) {
            stNCList.add(mc);
        } else if('智真&视讯' == this.product) {
            tvNCList.add(mc);
        } else ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, '当前产品信息"'+ this.product +'"不正确'));
    }

    /**
     * 删除NC行（用于编辑界面点击删除行按钮）
     */
    public void deleteMainChannel() {
        if(!this.canEdit) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, System.Label.No_Privilege));
            return;
        }
        for(Integer i=0; i<srNCList.size(); i++) { //删除数通NC
            if(!srNCList[i].IsDelete__c) continue;
            if(srNCList[i].id!=null) mcListDelete.add(srNCList[i]); //对于已保存的NC数据，添加到待删除列表
            srNCList.remove(i); //删除前台展示的数据
        }
        for(Integer i=0; i<stNCList.size(); i++) { //删除存储NC
            if(!stNCList[i].IsDelete__c) continue;
            if(stNCList[i].id!=null) mcListDelete.add(stNCList[i]); //对于已保存的NC数据，添加到待删除列表
            stNCList.remove(i); //删除前台展示的数据
        }
        for(Integer i=0; i<tvNCList.size(); i++) { //删除智真NC
            if(!tvNCList[i].IsDelete__c) continue;
            if(tvNCList[i].id!=null) mcListDelete.add(tvNCList[i]); //对于已保存的NC数据，添加到待删除列表
            tvNCList.remove(i); //删除前台展示的数据
        }
    }

    /**
     * 保存MainChannel及NC数据（用于编辑界面点击保存按钮）
     */
    public PageReference saveMainChannel() {
        List<China_Main_Channel__c> mainChannelListSave = new List<China_Main_Channel__c>();
        for(China_Main_Channel__c mc : srNCList) mainChannelListSave.add(mc);
        for(China_Main_Channel__c mc : stNCList) mainChannelListSave.add(mc);
        for(China_Main_Channel__c mc : tvNCList) mainChannelListSave.add(mc);
        for(China_Main_Channel__c mc : mainChannelListSave) {
            if('我司合作的NA主流渠道' == mc.Main_Partner_Type__c) mc.Partner_Name_Input__c = '';
            if('竞争对手的NA主流渠道' == mc.Main_Partner_Type__c) mc.Partner_Name__c = null;
        }
        try {
            //保存MainChannel主数据
            update mainChannelExpand;
            //删除已保存的NC数据 
            delete mcListDelete; 
            mcListDelete.clear(); 
            //保存NC列表数据
            if(mainChannelListSave.size()>0) {
                upsert mainChannelListSave;
            }
        } catch(Exception e) {
            System.debug(e);      
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, '保存数据发生错误'));
        }
        return new ApexPages.StandardController(mainChannelExpand).view();
    }

    /**
     * 维护MainChannel及NC数据（用于明细信息界面点击编辑按钮）
     */
    public PageReference editMainChannel() {
        return new ApexPages.StandardController(mainChannelExpand).edit();
    }
    
    /**
     * 返回销售例会明细界面
     */
    public PageReference cancel() {
        PageReference pr = new PageReference('/' + this.accountId);
        pr.setRedirect(true);
        return pr;
    }

    /**
     * 判断当前用户是否有客户编辑权限
     */
    private void checkAuthority(){
        UserRecordAccess access ;
        try{
            access = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = :UserINfo.getUserId() AND RecordId = :accountId limit 1];
        }catch(QueryException e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '无法获取用户对当前客户的权限信息'));
            this.canEdit = false;
        }
        if(access == null || !access.HasEditAccess){
            this.canEdit = false;
        }
    }
}