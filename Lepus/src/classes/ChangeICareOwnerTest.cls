/*
* @Purpose: Test ChangICareOwner extension controller
*
* @Author: Tommy Liu(tommyliu@celnet.com.cn)
* @Date: 2014-2-26
*/
@isTest
private class ChangeICareOwnerTest 
{
     //测试 ChangeICareOwner强制提交这些值，即使 为空也要强制覆盖到数据库
    static testMethod void testChangeICareOwner() 
    {
        Case sampleCase = new Case();
        sampleCase.iCare_Owner__c = null;
        sampleCase.Employee_ID__c = '12345678';
        sampleCase.subject = 'this is a sample case';

        insert sampleCase;
        
        //模拟用户打开界面
        sampleCase.subject = 'this case was modified by page';
        ApexPages.StandardController sc = new ApexPages.StandardController(sampleCase);
        ChangeICareOwner editCaseExtension = new ChangeICareOwner(sc);
        Case theCase = (Case)sc.getRecord();
        theCase.Employee_ID__c = null;
        
        //模拟中间件更新
        Case upCase = new Case();
        upCase.Id = sampleCase.Id;
        upCase.iCare_Owner__c = UserInfo.getUserId();
        upCase.Employee_ID__c = '88888888';
        upCase.subject = 'this is a sample case modified';
        update upCase;
        
				//模拟用户保存页面
        System.PageReference pf = editCaseExtension.save();
        
        sampleCase = [Select Id, Subject, iCare_Owner__c, Employee_ID__c From Case Where Id=: sampleCase.ID];
        System.assert(pf != null);
        System.assertEquals('this case was modified by page', sampleCase.Subject);
        System.assertEquals(null, sampleCase.iCare_Owner__c);
        System.assertEquals(null, sampleCase.Employee_ID__c);
    }
}