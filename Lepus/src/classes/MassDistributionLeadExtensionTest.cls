/*
Author: tommy.liu@celnet.com.cn
Created On: 2014-5-15
Function: Test MassDistributionLeadExtension
 */
@isTest
private class MassDistributionLeadExtensionTest 
{
    static testMethod void test() 
    {
        //Account acc = UtilUnitTest.createHWChinaAccount('test account');
    
        //ID leadRtId = [Select ID, Name From RecordType Where DeveloperName = 'Huawei_China_Lead' And SobjectType = 'Lead'].ID;
        Lead[] leads = new Lead[] {
            new Lead(LastName='test lead 1', Status='待分发', Company='Huawei'),
            new Lead(LastName='test lead 2', Status='待分发', Company='Huawei'),
            new Lead(LastName='test lead 3', Status='已作废', Company='Huawei')
            };
        insert leads;
        
      List<Lead> leadList = [SELECT ID, Name, Status, Distribute_To__c FROM Lead];
      System.assertEquals(3, leadList.size());
            ApexPages.StandardSetController setCon = new ApexPages.StandardSetController(leadList);
            setCon.setSelected(leadList);
            MassDistributionLeadExtension massCon = new MassDistributionLeadExtension(setCon);
            List<MassDistributionLeadExtension.LeadRow> selRows = massCon.SelectedLeadRows;
            System.assertEquals(3, selRows.size());
            
            /* 提示信息 从 message处 改为放到title
            String label2 = System.Label.VF_Easy_MassDistributionLead_Message_Selected_Report2;
            if(label2 != null)
            {
                label2 = label2.replace('{!totel}', '3');
                label2 = label2.replace('{!canbeDistributed}', '2');
            }
            ApexPages.Message[] msgs = ApexPages.getMessages();
            system.assert(msgs.size() > 0);
            ApexPages.Message beforMsg = msgs[0];
            system.assertEquals(label2, beforMsg.getSummary());
            system.assert(massCon.ShowSubmit);
            system.assert(!massCon.IsSubmitted);
            */
            ID userID = UserInfo.getUserId();
            selRows[0].lead.Distribute_To__c = userID;
            selRows[1].lead.Distribute_To__c = userID;
            selRows[2].lead.Distribute_To__c = userID;
            massCon.submit();
            String labelSuc = System.Label.VF_Easy_MassDistributionLead_Message_Submit_Report;
            if(labelSuc != null)
            {
                labelSuc = labelSuc.replace('{!success}', '0');
                labelSuc = labelSuc.replace('{!ignore}', '1');
                labelSuc = labelSuc.replace('{!failed}', '2');
            }
            
            ApexPages.Message[] msgsR = ApexPages.getMessages();
            system.assert(msgsR.size() > 0);
            ApexPages.Message afterMsg = msgsR[0];
    }
}