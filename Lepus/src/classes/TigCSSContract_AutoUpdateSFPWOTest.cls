/**
* Write a new test class to replace TestTig_CSSContract
*@Author:Brain Zhang
*Create Date：2013-9
 */
@isTest
private class TigCSSContract_AutoUpdateSFPWOTest {

    static testMethod void positiveTest() {
    	Account acc = UtilUnitTest.createAccount();
    	CSS_Contract__c cc = UtilUnitTest.createContract(acc);
    	SFP__c sfp = UtilUnitTest.createSFP(cc);
    	Case sr = UtilUnitTest.createChinaServiceRequest();
    	Work_Order__c wo = UtilUnitTest.createOnSiteWorkOrder(acc, sr);
    	wo.Contract_No__c = cc.id;
    	update wo;
    	sfp = [select id,Status__c from SFP__c where id=:sfp.id];
    	wo = [select id,Status__c from Work_Order__c where id=:wo.id];
    	System.assertNotEquals('Cancelled',sfp.Status__c);
    	System.assertNotEquals('Cancelled',wo.Status__c);
    	
    	cc.Status__c = 'Cancelled';
    	update cc;
    	sfp = [select id,Status__c from SFP__c where id=:sfp.id];
    	wo = [select id,Status__c from Work_Order__c where id=:wo.id];
    	System.assertEquals('Cancelled',sfp.Status__c);
    	System.assertEquals('Cancelled',wo.Status__c);
    	
    }
}