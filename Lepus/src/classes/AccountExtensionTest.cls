@isTest
private	class AccountExtensionTest {
	static testMethod void create(){
		Account acc = UtilUnitTest.createHWChinaAccount('acc');
		AccountExtension controller = new AccountExtension(
        	new ApexPages.StandardController(acc));
		System.assertNotEquals(null,controller.cr);

		try{
			controller.createcr();
		}catch(Exception e){}
		List<Customer_Relationship__c> crs = [select id,Company_Visit__c from Customer_Relationship__c];
		System.assertEquals(0,crs.size());
		
		controller.cr.Company_Visit__c = true;
		controller.createcr();
		Customer_Relationship__c cr = [select id,Company_Visit__c from Customer_Relationship__c ];
		System.assertEquals(true,cr.Company_Visit__c);

		acc = [select id,Company_Visit__c from Account where id = :acc.id];
		System.assertEquals(1,acc.Company_Visit__c);
	}
}