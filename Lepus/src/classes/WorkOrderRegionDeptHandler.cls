public without sharing class WorkOrderRegionDeptHandler implements Triggers.Handler{
	public void handle(){//before insert ,before update
		List<Work_Order__c> wos = new List<Work_Order__c>();
		if(Trigger.isInsert){
			for(Work_Order__c w : (List<Work_Order__c>)Trigger.new){
				if(w.Account_Name__c != null){
					wos.add(w);
				}
			}
		}
		if(Trigger.isUpdate){
			for(Work_Order__c w : (List<Work_Order__c>)Trigger.new){
				if(w.Account_Name__c != null){//SHOULD ASK TO GET THE REQUIREMENTS CLEAR
					wos.add(w);
				}
			}
		}
			if(wos.size() > 0){
				UtilFLEngine.Context ctx = new UtilFLEngine.Context(Account.SobjectType, // parent object
                                            Work_Order__c.SobjectType,  // child object
                                            Schema.SObjectType.Work_Order__c.fields.Account_Name__c // relationship field name
                                            );     
    	ctx.add(
            new UtilFLEngine.LookupField(
                                            Schema.SObjectType.Account.fields.Region_Dept__c,
                                            Schema.SObjectType.Work_Order__c.fields.Region_Dept__c
                                       )); 
     	UtilFLEngine.populater(ctx, wos);
     	System.debug('the current new list:---' + trigger.new); 
		}
	}
}