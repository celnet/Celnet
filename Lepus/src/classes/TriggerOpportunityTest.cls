/**
 * Purpose          : Test Opportunity Trigger
 * Author           : BLS
 * Date				: 2013-3-13
 */
@isTest
private class TriggerOpportunityTest {
  static testMethod void OpportunityTest() {
  	/*
		Account acc = UtilUnitTest.createHWChinaAccount('bbb');
		String sRecordTypeName = 'Huawei_China_Opportunity';
		Id optyRecordType = [select id from RecordType where DeveloperName =: sRecordTypeName].id;
		
		Opportunity opp = UtilUnitTest.createOpportunity(acc);
		opp.Representative_Office__c = '广州代表处';
		opp.Province__c = '广东省';
		opp.City__c = '珠海市';
		opp.System_Department__c = '大企业系统部';
		opp.Sub_Industry__c = '大企业一部（二级运营商）';
		opp.Opportunity_Level__c = 'Ordinary Level';
		opp.Opportunity_Attribution__c = '系统部';
		opp.LeadSource = 'Account Sales Team';
		opp.Opportunity_Type__c = 'Direct Sales';
		System.debug('run into here:---------------------1');
		update opp;
		
		Deal_Registration__c dr = new Deal_Registration__c();
		dr.Related_Opportunity__c = opp.id;
		dr.Name = 'Test Deal Registration';
		dr.Deal_Status__c = 'Open';
		insert dr;

		Product_HS__c hs = UtilUnitTest.createProductMaster('level0');
		Project_Product__c pp = UtilUnitTest.createProjectProduct(opp,hs);
		pp.Quantity__c = 3;
		pp.Sales_Price__c = 7;
		update pp;
		opp.StageName = 'SS4 (Developing Solution)';
		System.debug('run into here:---------------------2');
		update opp;
		
		opp.StageName = 'SSA (Huawei No Bid)';
		opp.Lost_Reason__c = 'Bid resources not available';
		opp.Lost_Reason_Description__c = 'test';
		opp.WinCom_Other__c = 'test';
		System.debug('run into here:---------------------3');
		update opp;

		opp.RecordTypeId = optyRecordType;
		System.debug('run into here:---------------------4');
		update opp;

		dr = [select Deal_Status__c from Deal_Registration__c where id =: dr.id];
		System.assertEquals('Cancelled', dr.Deal_Status__c);
		
		Account acc2 = UtilUnitTest.createHWChinaAccount('aaa');
		Opportunity opp2 = UtilUnitTest.createOpportunity(acc2);
		opp2.RecordTypeId = optyRecordType;
		Deal_Registration__c dr2 = new Deal_Registration__c();
		dr2.Related_Opportunity__c = opp2.id;
		dr2.Name = 'Test Deal Registration2';
		insert dr2;
		
		opp2.StageName = 'SS8 (Completed)';
		opp2.Actual_PO_Amount__c = 100;
		System.debug('run into here:---------------------5');
		update opp2;
		dr2 = [select Deal_Status__c from Deal_Registration__c where id =: dr2.id];
		System.assertEquals('Closed', dr2.Deal_Status__c);
    */
    }
}