/**
 * @Purpose : 记录进行审批时，根据审批步骤的变化，同步到W3待办
 * @Author : sunda
 * @Date :   2014-08-29
 */
public class Lepus_SetW3AccountHandler implements Triggers.Handler{
	public void handle() {
		for(User u : (List<User>)trigger.new){
			if(u.FederationIdentifier == null){
				u.W3ACCOUNT__c = null;
			}
		}
	}
}