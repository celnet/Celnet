/*
 * 提交审批时，通过Map对象查找对应区域的审批人
 * @Author: LYX
 * @Date: 20131108
 */
public with sharing class CampaignFindApproverOversea implements Triggers.Handler{ //before insert
	public void handle() {
		for (Campaign campaign : (List<Campaign>) trigger.new) {
			if(campaign.RecordTypeId == CONSTANTS.OVERSEACAMPAIGNRECORDTYPE) {
				if (campaign.Status == 'Request for Approval' && ((Campaign)trigger.Oldmap.get(campaign.id)).Status != 'Request for Approval') {
					String sApproverMapRecordTypeName = 'HW_Campaign_Approver';
					Id ApproverMapRecordType = [select id from RecordType where DeveloperName =: sApproverMapRecordTypeName].id;
					User currentUser = [Select Id, Name,Region_Office__c FROM User where Id = : UserINfo.getUserId()];
					List<Account_Link_Approver_Map__c> approvers = [Select Approver__c FROM Account_Link_Approver_Map__c 
					                                                 where Region__c = : currentUser.Region_Office__c
					                                                 and RecordTypeId = :ApproverMapRecordType];
					if(approvers.size() > 0){
						User currentApprover = [Select IsActive, Name FROM User where Id = : approvers[0].Approver__c];
						if(!currentApprover.IsActive) {
							campaign.addError('The approver "' + currentApprover.Name + '" is inactive, please contact the administrator.');
						} else {
							campaign.Regional_Approver__c = approvers[0].Approver__c;
						}
					} else {
						campaign.addError('No approver maintenance for your region "'+currentUser.Region_Office__c + '", please contact the administrator.');
					}
				}
			}
		}
	}
}