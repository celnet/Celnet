/*
* @Purpose : 报备的邮件提醒：报备审批通过后，要求渠道每两周更新一次进展，到期前三天系统给渠道发送邮件提醒，两周的计时周期从报备更新时点起重新计算；
如报备关闭（签单）或取消（丢单）不再发送邮件提醒；
报备超过两周未更新，置为失效Invalid；
提醒邮件模板：（标题+内容）----（项目报备失效提醒！＋您好！为了维护您的相关权益，请尽快更新“项目名称”项目的相关信息，否则，项目报备将在三天后失效，谢谢！）
注：以下报备字段有变化时才认为报备有刷新：项目名称、项目描述、预计签单金额（万元）、预计签单时间、最终客户名称、最终客户联系人、最终客户电话、最终客户邮箱、最终客户地址、报备的产品（关联列表）、竞争对手（关联列表）
     
  方案：
     //通过工作流实现
     1、报备审批通过后自动将值设置成14 
     2、关键字段更新后自动将值设置成14（审批通过、未关闭、未取消、未失效）
     //代码实现
     1、查询出所有审批通过的，未关闭或取消或失效的中国区报备数据
     2、将这些报备数据的倒计时全部减1
     3、对于小于等于3的发送邮件提醒
     4、对于小于0的，失效报备
* @Author : steven
* @Date : 2014-03-31
*/
global class ChinaDealRemindScheduleJob implements Schedulable,Database.Batchable<sObject>{
    public String query;

    public ChinaDealRemindScheduleJob() {
    }   
    
    //ScheduleJob Execute 取出所有的11天未更新的
    global void execute(SchedulableContext SC){
			//1、查询出所有审批通过的，未关闭或取消或失效的中国区报备数据
			List<Deal_Registration__c> pcList = 
				[select id,Email_Remind_Count_Down__c,Deal_Status__c from Deal_Registration__c 
					where Deal_Status__c = 'Open' 
						and Email_Remind_Count_Down__c <> null
						and RecordTypeId =: CONSTANTS.HUAWEICHINADEALRECORDTYPE]; 
			//2、将这些报备数据的倒计时全部减1
			//4、对于小于0的，失效报备
			for(Deal_Registration__c d : pcList) {
				if(d.Email_Remind_Count_Down__c != null) d.Email_Remind_Count_Down__c = d.Email_Remind_Count_Down__c - 1;
				if(d.Email_Remind_Count_Down__c == 0) d.Deal_Status__c = 'Invalid';
			}
			update pcList;
			
			ChinaDealRemindScheduleJob e = new ChinaDealRemindScheduleJob();
			e.query='select id,name,Email_Remind_Count_Down__c,login_contact__r.Email '+
			          'from Deal_Registration__c '+ 
			         'where Deal_Status__c = \'Open\' ' + 
			           'and Email_Remind_Count_Down__c = 3 '+
			         	 'and RecordTypeId =\''+ CONSTANTS.HUAWEICHINADEALRECORDTYPE + '\'';
			Id batchId = Database.executeBatch(e,10);
		}
    
    //Batch Start
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);   
    }
    //Batch Execute
    public void execute(Database.BatchableContext BC, list<sObject> scope){
			try {
				Set<id> pIDs = new Set<id>();
				//获取本批次所有需要处理的经销商ID
				for(sObject s : scope){
					//3、对于小于等于3的发送邮件提醒
					Deal_Registration__c deal = (Deal_Registration__c) s;
					Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
					List<string> toAddress = new List<string>();
					toAddress.add(deal.login_contact__r.Email);
					mail.setToAddresses(toAddress);
					mail.setSenderDisplayName('Salesforce Supporter');
					mail.setSubject('【提醒】项目报备失效提醒');
					String strbody = '<p>您好！为了维护您的相关权益，请尽快更新\''+ deal.Name +'\'项目的相关信息，否则，项目报备将在三天后失效，谢谢！</p>';
					mail.setHtmlBody(strbody);
					Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
				}
			} catch(Exception e) {
				System.debug(e);
			}
		}
    //Batch Finished
    public void finish(Database.BatchableContext BC){
    }
    
}