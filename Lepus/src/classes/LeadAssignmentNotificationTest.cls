@isTest
private class LeadAssignmentNotificationTest {

    static testMethod void queryTest() {
    	UtilUnitTest.createLeadDistributeMap();
    	Account acc = UtilUnitTest.createAccount();
    	Lead l = UtilUnitTest.createLead(acc);
    	LeadAssignmentNotification controller = new LeadAssignmentNotification();
    	List<Id> userIds = LeadAssignmentNotification.queryUsers();
    	System.assertEquals(1,userIds.size());
    	System.assertEquals(UserInfo.getUserId(),userIds[0]);
    	LeadAssignmentNotification.sendEmail(userIds);
    }
    
    static testMethod void scheduleTest(){
    	UtilUnitTest.createLeadDistributeMap();
    	Account acc = UtilUnitTest.createAccount();
    	Lead l = UtilUnitTest.createLead(acc);
    	Test.startTest();
    	
    	String jobId = System.schedule('TestLeadAssignmentNotification',
     		LeadAssignmentNotification.CRON_EXP, 
         new LeadAssignmentNotification());
    	 CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];

      System.assertEquals(LeadAssignmentNotification.CRON_EXP, 
         ct.CronExpression);
    	
    	Test.stopTest();
    }
}