@isTest
private class AccountCisRepeatRemindHandlerTest {
	static testMethod void myUnitTest() {
		Account acc = UtilUnitTest.createHWChinaAccount('TEST001');
		Account acc2 = UtilUnitTest.createHWChinaAccount('TEST002');
		acc.CIS__c = 'CIS123456';
		update acc;
		acc2.CIS__c = 'CIS123456';
		update acc2;
	}
}