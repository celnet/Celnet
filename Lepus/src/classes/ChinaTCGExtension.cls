/*
* @Purpose : TCG创建或更新时，自动刷新其关联的客户、认证渠道、销售目标
* @Author : Steven
* @Date : 2014-01-26
*/
public with sharing class ChinaTCGExtension {
	public Account chinaTCG {get;set;}
	public Boolean isCity1 {get;set;}   //根据区域信息控制城市字段是否可见
	public Boolean isCity2 {get;set;}   //根据区域信息控制城市字段是否可见
	public Boolean isCity3 {get;set;}   //根据区域信息控制城市字段是否可见
	public Boolean canSave {get;set;}   //防止多次点击Save按钮
	
	/**
	 * 构造方法初始化TCG
	 */ 
	public ChinaTCGExtension(ApexPages.StandardController controller){
		canSave = true;
		isCity1 = true;
		isCity2 = true;
		isCity3 = true;
		User currentUser = [select Profile_Name__c,Country__c from User where id =: Userinfo.getUserId()];		
		//判断是创建还是修改TCG
		if(controller.getId() == null) {
			chinaTCG = new Account(recordtypeid = CONSTANTS.HUAWEICHINATCGRECORDTYPE);
		} else {
			chinaTCG = [select id,Name,TCG_Level__c,TCG_Representative_Office__c,TCG_Industry__c,
												 TCG_Main_Industry__c, TCG_City_1__c,TCG_City_2__c,TCG_City_3__c,TCG_City_Display__c
				 						from account where id =: controller.getId()];
		}
		//根据当前用户的区域信息，控制哪个CITY字段显示
		String cityCode = ChinaTCGUpdateHandler.getUsedCity(currentUser.Country__c);
		if(cityCode == '1') {
			isCity2 = false; isCity3 = false;
		} else if(cityCode == '2') {
			isCity1 = false; isCity3 = false;
		} else if(cityCode == '3') {
			isCity1 = false; isCity2 = false;
		}
		
		//如果是一线用户，根据用户信息中的归属信息初始化代表处值
		if(currentUser.Profile_Name__c.contains('(Huawei China)代表处销管')||
			 currentUser.Profile_Name__c.contains('(Huawei China)代表处客户经理')){
	    chinaTCG.TCG_Representative_Office__c = currentUser.Country__c;
		}
	}
	
	/**
	 * 创建一个或修改保存TCG
	 */ 
	public PageReference save(){
		setCityDisplayValue();
		//如果验证未通过，提示错误信息  
		List<ApexPages.Message> messages = this.validation();
		if(messages.size() > 0){
			for(ApexPages.Message m : messages) ApexPages.addMessage(m);
			return null;
		}
		//创建新的TCG或更新已有的TCG
		try{
			if(chinaTCG.id != null){
				update chinaTCG;
			} else {
				insert chinaTCG;
			}
		}catch(Exception e){
			ApexPages.addMessages(e);
			return null;
		}
		return new ApexPages.StandardController(chinaTCG).view();
	}
	
	/**
	 * 有的City字段不在界面上显示，在保存前初始化城市显示字段值
	 */
	private void setCityDisplayValue(){
		if(!isCity1) chinaTCG.TCG_City_1__c = null;
		if(!isCity2) chinaTCG.TCG_City_2__c = null;
		if(!isCity3) chinaTCG.TCG_City_3__c = null;
		if(isCity1 && chinaTCG.TCG_City_1__c != null && chinaTCG.TCG_City_1__c != '') {
			chinaTCG.TCG_City_Display__c = chinaTCG.TCG_City_1__c;
		} else if(isCity2 && chinaTCG.TCG_City_2__c != null && chinaTCG.TCG_City_2__c != '') {
			chinaTCG.TCG_City_Display__c = chinaTCG.TCG_City_2__c;
		} else if(isCity3 && chinaTCG.TCG_City_3__c != null && chinaTCG.TCG_City_3__c != '') {
			chinaTCG.TCG_City_Display__c = chinaTCG.TCG_City_3__c;
		}
	}
	
	/**
	 * 验证是否符合业务规则，返回错误信息
	 */
	private List<ApexPages.Message> validation(){
		List<ApexPages.Message> messages = new List<ApexPages.Message>();
		//如果和其他TCG有覆盖，提示错误信息
		String vDuplicate = ChinaTCGUpdateHandler.validateDuplicate(chinaTCG);
		if(vDuplicate!='') {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING,vDuplicate);
			messages.add(msg);
		}
		//判断行业和子行业是否符合规范
		String vIndustry = ChinaTCGUpdateHandler.validateTCGRequestField(chinaTCG);
		if(vIndustry!='') {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING,vIndustry);
			messages.add(msg);
		}
		return messages;
	}
}