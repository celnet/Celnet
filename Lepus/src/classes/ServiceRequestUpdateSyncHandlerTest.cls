@isTest
private class ServiceRequestUpdateSyncHandlerTest {
	static testMethod void notFireTest(){//if the integration user change the content,not sync 
		Case c = UtilUnitTest.createChinaServiceRequest();
    c.subject = 'test';
    update c;//wont fire the callout becuase the iCare_Number__c  is null
    
    String whereClause = ' Profile.Name  like \'%Integration%\' ';
    User u = UtilUnitTest.queryUser(whereClause);
    System.runas(u){
    	c.iCare_Number__c = '11';
    	c.subject = 'ok';
    	update c;//wont fire because it's an integration user
    }
		
		System.assertEquals(0,[select count() from iCareLog__c]);
		c.Subject = 'try the exception';
		Test.startTest();
		update c;    
		Test.stopTest();
		System.assertEquals(1,[select count() from iCareLog__c]);//the callout wont success
		
	}
	
	static testMethod void fireTest(){
		Case c = UtilUnitTest.createChinaServiceRequest();
		c.subject = 'test';
		c.iCare_Number__c = '11';
		Test.startTest();
	  Test.setMock(WebServiceMock.class, new EIPMockImpl());
	  EIPMockImpl.isBatchMode = true;
	  EIPMockImpl.isUpdateMode = true;
	  update c;
	  Test.stopTest();
	  //System.assertEquals(0,[select count() from iCareLog__c]);
	}
}