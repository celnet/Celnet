@isTest
private class Lepus_WisdomOpportunityWriteBackTest {
    static testmethod void myUnitTest(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Integration Profile for Lepus']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser_x@testorg.com');
        //User u = [Select Id From User Where Profile.Name = 'Integration Profile for Lepus'];
        
        insertDataDictionary('Opportunity Level','RepOffice','Representative Office Level');
        insertDataDictionary('Opportunity Type','SM002','Carrier Resales');
        
        insertFieldDependency('Country__c', 'Cameroon', 'Country_Code__c', 'CM');
        insertFieldDependency('Representative_Office__c', 'Cameroon', 'Country__c', 'Cameroon');
        insertFieldDependency('Region__c', 'West Africa Region', 'Representative_Office__c', 'Cameroon');
        
        
        
        Account acc = new Account();
        acc.Name = 'xxx9153';
        acc.RecordTypeId = CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE;
        acc.City_Huawei_China__c = '北京市';
        acc.OwnerId = UserInfo.getUserId();
        acc.Customer_Group_Code__c = '79';
        
        insert acc;
System.runAs(u){
        Opportunity opp = new Opportunity();
        opp.Name = 'xxxx9152';
        opp.RecordTypeId = CONSTANTS.CHINAOPPORTUNITYRECORDTYPE;
        opp.OwnerId = UserInfo.getUserId();
        opp.AccountId = acc.Id;
        opp.Opportunity_Level_Code__c = 'SubRegion';
        opp.Opportunity_Type_Code__c = 'SM004';
        opp.China_Competitor_Trends__c = 'xxx';
        opp.Project_Risk__c = 'xxx';
        opp.China_Following_Strategy__c = 'xxx';
        opp.Master_Channer_Partner__c = 'xxx';
        opp.StageName = 'SS3 (Validating Opportunity)';
        opp.CloseDate = Date.today();
        
        
            insert opp;
        
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'xxxxdfad';
        opp1.RecordTypeId = CONSTANTS.HUAWEIOVERSEAOPPRECORDTYPE;
        opp1.OwnerId = UserInfo.getUserId();
        opp1.AccountId = acc.Id;
        opp1.China_Competitor_Trends__c = 'xxx';
        opp1.Project_Risk__c = 'xxx';
        opp1.China_Following_Strategy__c = 'xxx';
        opp1.Master_Channer_Partner__c = 'xxx';
        opp1.StageName = 'SS3 (Validating Opportunity)';
        opp1.CloseDate = Date.today();
        opp1.Country_Code__c = 'CM';
        Lepus_WisdomOpportunityWriteBackHandler.isFirstRun = true;
        
        
            insert opp1;
        }
    }
    
    static void insertDataDictionary(String fieldType, String fcode, String fvalue){
        DataDictionary__c dd = new DataDictionary__c();
        dd.Code__c = fcode;
        dd.Object__c = 'Opportunity';
        dd.Name__c = fvalue;
        dd.Type__c = fieldType;
        insert dd;
    }
    
    static void insertFieldDependency(String cf, String cv, String df, String dv){
        Field_Dependency__c fd5 = new Field_Dependency__c();
        fd5.Controlling_Field__c = cf;
        fd5.Controlling_Field_Value__c = cv;
        fd5.Dependent_Field__c = df;
        fd5.Dependent_Field_Value__c = dv;
        fd5.Object_Type__c = 'Opportunity';
        
        insert fd5;
    }
}