/*
* @Purpose : 客户创建或者商业客户群更新时自动刷新其关联的TCG,如果有关联机会点同时刷新机会点的TCG信息
* @Author : Steven
* @Date : 2014-01-26
*/
public without sharing class AccountTCGUpdateHandler implements Triggers.Handler,Database.Batchable<sObject>{ //fires after update & Insert
	public static Boolean isFirstRun = true;
	public String query;
	public void handle(){
		if(AccountTCGUpdateHandler.isFirstRun){
			Set<Id> updateAccIds = new Set<Id>();
			for(Account acc : (List<Account>)trigger.new){
				//只处理中国区最终客户
				if(acc.RecordTypeId == CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE){
					//如果是创建
					if(Trigger.isInsert){
						if(acc.Industry == '商业销售部') updateAccIds.add(acc.id);
					}
					//如果是修改
					if(Trigger.isUpdate){ //如果考虑CIS码在这里添加
						Account oldAcc = (Account) trigger.OldMap.Get(acc.Id);
						if((acc.Customer_Group__c != oldAcc.Customer_Group__c) && (acc.Industry == '商业销售部' || oldAcc.Industry == '商业销售部')){
							updateAccIds.add(acc.id);
						}
						if((acc.Industry == '商业销售部') && (acc.City_Huawei_China__c != oldAcc.City_Huawei_China__c)){
							updateAccIds.add(acc.id);
						}
					}
				}
			}
			//如果有待更新的客户，则执行刷新TCG方法
			if (!updateAccIds.isEmpty()) {
				refreshAccountTCGInfo(updateAccIds);
				isFirstRun = false;
			}
		}
	}
	
  /**
	 * 刷新TCG关联的经销商列表 
	 */
	private void refreshAccountTCGInfo(Set<Id> updateAccIds) {
		//如果只有10个客户采用直接处理模式
		if(updateAccIds.size()<=10) {
			List<Account> accList = [select id,Industry,Customer_Group__c,City_Huawei_China__c,Industry__c,TCG_Name__c 
																 from Account where id =:updateAccIds];
			for(Account acc :accList) {
				refreshAccountTCGInfoImmediately(acc);
			}
		} else {
			//如果大于10个客户采用batch模式
			String accids = '';
			for(Id i : updateAccIds){
				if(accids == '') accids = accids + '\''+ i +'\'';
				else accids = accids + ',\''+ i +'\'';
			}
			AccountTCGUpdateHandler e = new AccountTCGUpdateHandler();
			e.query='select id,Industry,Customer_Group__c,City_Huawei_China__c,Industry__c,TCG_Name__c from Account where id in ('+ accids +')';
			Id batchId = Database.executeBatch(e,10);
		}
	}

	//Batch Start
	public Database.QueryLocator start(Database.BatchableContext BC){
		return Database.getQueryLocator(query);   
	}
	//Batch Execute
	public void execute(Database.BatchableContext BC, list<sObject> scope){
		try {
			for(sObject s : scope){
				Account acc = (Account)s;
				refreshAccountTCGInfoImmediately(acc);
			}
		} catch(Exception e) {
			System.debug(e);
		}
	}
	//Batch Finished
	public void finish(Database.BatchableContext BC){
	}
	
	/**
	 * 立即刷新客户的TCG信息
	 */
	private void refreshAccountTCGInfoImmediately(Account a) {
		try {
			String soql = 'SELECT Id FROM Account '+  
										 'WHERE recordtypeid = \''+ CONSTANTS.HUAWEICHINATCGRECORDTYPE +'\' '+
										 	 'AND TCG_Industry__c includes (\''+ a.Customer_Group__c +'\') '+
										 	 'AND TCG_City_Display__c like \'%'+ a.City_Huawei_China__c +'%\' '+
										 	 'AND Inactive__c = false ';
			List<Account> tcgList = (List<Account>)Database.query(soql);
			a.TCG_Name__c = (tcgList.size() > 0) ? ((Account)tcgList.get(0)).id : null;
			update a;
			
			//刷新相关机会点的TCG信息
			OpportunityUpdateTCGInfo.isFirstRun = false;
			List<Opportunity> oppList = [Select id,TCG_Name__c From Opportunity Where accountid =:a.id and Is_Closed__c = false];
			for(Opportunity opp : oppList) {
				opp.TCG_Name__c = a.TCG_Name__c;
			}
			if(oppList.size()>0){ 
				update oppList;
			}
		} catch(Exception e) {
			System.debug(e);
		}
	}
}