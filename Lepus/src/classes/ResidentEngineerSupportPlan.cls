public without sharing class ResidentEngineerSupportPlan implements Triggers.Handler{
	public void handle(){
			list <Resident_Engineer_Support_Plan__c> allResident = new list <Resident_Engineer_Support_Plan__c>();
			list <SFP__c> sfp = new list <SFP__c>();
			set <ID> residentID = new set<ID>();	
			set <ID> errorresidentID = new set<ID>();	
			Map <id,id> sfpresidentMap = new Map <id,id>();		
			if(trigger.isInsert || trigger.isUnDelete||trigger.isUpdate){
				for(Resident_Engineer_Support_Plan__c resident :(list<Resident_Engineer_Support_Plan__c>)trigger.new){								
					residentID.add(resident.id);
					sfpresidentMap.put(resident.id,resident.SFP__c);					
				}
			}
			if(trigger.isDelete){
				for(Resident_Engineer_Support_Plan__c resident :(list<Resident_Engineer_Support_Plan__c>)trigger.old){							
					residentID.add(resident.id);
					sfpresidentMap.put(resident.id,resident.SFP__c);					
				}
			}
			if(residentID.size()>0){
				for(ID id :residentID){
					Resident_Engineer_Support_Plan__c Resident = new Resident_Engineer_Support_Plan__c();
					sfp = [select id,Resident_Engineer_Support_Total__c,Resident_Engineer_Support_Used__c from SFP__c 
						where id =:sfpresidentMap.get(id)];					
					allResident =
					[select id,Sum_Service_Amount__c from Resident_Engineer_Support_Plan__c 
						where SFP__c = :sfpresidentMap.get(id)];
					if(allResident.size()>0){	
						Integer sumServiceAmount = 0;			
						for(Resident_Engineer_Support_Plan__c h :allResident){
							sumServiceAmount +=Integer.valueOf(h.Sum_Service_Amount__c);
							if(h.id == id) Resident = h;
						}						
							if(sfp[0].Resident_Engineer_Support_Total__c!=null){								
								if(sumServiceAmount >sfp[0].Resident_Engineer_Support_Total__c ){
									addErrorInfo(id);
									return;
								}								
						}
						sfp[0].Resident_Engineer_Support_Used__c = sumServiceAmount;																
					}else 	sfp[0].Resident_Engineer_Support_Used__c=0;	
					update sfp;
			}
		}

	}
	
	private void addErrorInfo(Id ResidentId){
		for(Resident_Engineer_Support_Plan__c resident :(list<Resident_Engineer_Support_Plan__c>)trigger.new){					
			if(ResidentId == resident.id )Resident.addError('驻场服务时长不能大于SFP驻场服务总时长');
		}
	}
}