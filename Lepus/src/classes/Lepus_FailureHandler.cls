/**
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-8-29
 * Description: 同步失败时把失败记录存到SyncFailRecord和SyncFailDetail对象上
 */
public class Lepus_FailureHandler {
    
    // 处理业务数据同步失败
    public static void handleBusinessDataFailure(Id recordId, String errorMessage, String action, String objType){
        Schema.Sobjecttype sobjecttype = Lepus_SyncUtil.retrieveSobjectType(objType);
        handleBusinessDataFailure(recordId, errorMessage, action, sobjecttype);
    }
    
    // 处理业务数据同步失败
    public static void handleBusinessDataFailure(Id recordId, String errorMessage, String action, Schema.Sobjecttype sobjecttype){
        SyncFailRecord__c record = new SyncFailRecord__c();
        
        if(!action.toLowerCase().equals('delete')){
            if(sobjectType == Account.sobjecttype){
                record.Account__c = recordId;
            } else if(sobjectType == Opportunity.sobjecttype){
                record.Opportunity__c = recordId;
            } else if(sobjectType == Contact.sobjecttype){
                record.Contact__c = recordId;
            } else if(sobjectType == Lead.sobjecttype){
                record.Lead__c = recordId;
            } 
        }
        
        record.ObjectType__c = sobjectType.getDescribe().getName();
        record.SyncStatus__c = '同步失败';
        record.SyncTime__c = Datetime.now();
        record.SyncAction__c = action;
        record.SyncRecordID__c = recordId;
        record.SyncType__c = '业务数据同步';
        insert record;
        
        insertSyncFailDetail(record.Id, errorMessage);
    }
    
    // 处理字段更新同步失败
    public static void handleFieldUpdateFailure(Id OppId, Datetime dTime, String errorMessage){
        SyncFailRecord__c record = new SyncFailRecord__c();
        record.SyncStatus__c = '同步失败';
        record.SyncTime__c = dTime;
        record.SyncRecordID__c = OppId;
        record.SyncType__c = '字段更新同步';
        record.ObjectType__c = 'Opportunity';
        insert record;
        
        insertSyncFailDetail(record.Id, errorMessage);
    }
    
    // 处理线索字段更新同步
    public static void handleLeadHistoryFailure(List<Id> recordIds, String xml, String errorMessage){
        String ids = convertToCommaSeparatedString(recordids);
        
        SyncFailRecord__c record = new SyncFailRecord__c();
        record.SyncStatus__c = '同步失败';
        record.SyncTime__c = Datetime.now();
        record.SyncRecordID__c = ids;
        record.XML_String__c = xml;
        record.SyncType__c = '线索更新同步';
        record.ObjectType__c = 'Lead';
        insert record;
        
        insertSyncFailDetail(record.Id, errorMessage);
    }
    
    // 处理团队成员同步失败
    public static void handleTeamMemberFailure(List<Id> recordIds, String errorMessage, String action){
        String ids = convertToCommaSeparatedString(recordIds);
        
        SyncFailRecord__c record = new SyncFailRecord__c();
        record.SyncStatus__c = '同步失败';
        record.SyncTime__c = Datetime.now();
        record.SyncRecordID__c = ids;
        record.SyncType__c = '团队成员同步';
        record.SyncAction__c = action;
        
        insert record;
        
        insertSyncFailDetail(record.Id, errorMessage);
    }
    
    // 处理用户同步失败
    public static void handleUserFailure(List<Id> userIds, String errorMessage){
        String ids = convertToCommaSeparatedString(userIds);
        
        SyncFailRecord__c record = new SyncFailRecord__c();
        record.ObjectType__c = 'User';
        record.SyncStatus__c = '同步失败';
        record.SyncTime__c = Datetime.now();
        record.SyncRecordID__c = ids;
        record.SyncType__c = '用户角色同步';
        
        insert record;
        
        insertSyncFailDetail(record.Id, errorMessage);
    }
    
    // 处理对账数据同步失败
    public static void handleValidationFailure(String errorMessage ,Datetime errorDatetime , Schema.Sobjecttype sobjectType){
        SyncFailRecord__c record = new SyncFailRecord__c();
        
        record.ObjectType__c = sobjectType.getDescribe().getName();
        record.SyncStatus__c = '同步失败';
        record.SyncTime__c = errorDatetime;
        record.SyncType__c = '对账数据同步';
        
        insert record;
    }
    
    private static void insertSyncFailDetail(Id syncFailRecordId, String errorMessage){
        SyncFailDetail__c detail = new SyncFailDetail__c();
        detail.SyncStatus__c = '同步失败';
        detail.SyncTime__c = Datetime.now();
        detail.FailCause__c = errorMessage;
        detail.SyncFailRecord__c = syncFailRecordId;
        insert detail;
    }
    
    private static String convertToCommaSeparatedString(List<Id> recordIds){
        String ids = '';
        for(Id uid : recordIds){
            ids += uId;
            ids += ',';
        }
        ids = ids.substring(0, ids.length() - 1);
        return ids;
    }
    
    public static void handleReSyncFailure(Id syncFailRecordId, String errorMessage){
        // 发送邮件
        /*
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        User u = [Select Id, Email From User Where Id =: UserInfo.getUserId()];
        String[] toAddresses = new String[] {u.Email}; 
        mail.setToAddresses(toAddresses);
        mail.setSenderDisplayName('Salesforce Support');
        mail.setSubject('同步到EIP失败 ');
        mail.setBccSender(false);
        mail.setHtmlBody('同步到EIP失败<p>'+ '查看失败记录 ：<a href=' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + syncFailRecordId + '>请点此处</a>');
        
        try{
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        } catch(EmailException e){
            System.debug('Send email failed: ' + e.getMessage());
        }
        */
        // 插入Detail
        insertSyncFailDetail(syncFailRecordId, errorMessage);
    }
    
    public static String retrieveExceptionMessage(Exception e){
        String errorMessage = e.getTypeName() + ': ' + e.getMessage() + '. ' + 'Stack trace: ' + e.getStackTraceString() + '. ';
        return errorMessage;
    }
}