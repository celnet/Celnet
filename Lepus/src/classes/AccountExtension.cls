/*
	允许用户在Account详细信息页面录入五项关键客户关系
* @Author : Brain Zhang
* @Date : 		2013-4
*/
public with sharing class AccountExtension {
	public Boolean shouldRedirect {get; private set;}
	public String redirectUrl {get; private set;}
	public Id accId {get;set;}
	public Customer_Relationship__c cr {get;set;}
	public String message {get;private set;}
	public Boolean ableToSave{get;private set;}
	
	public TCG_Key_Activity__c tcgKA{get;set;}
	public Boolean tcgLv1{get; set;}
	public Boolean tcgLv2{get; set;}
	
	public AccountExtension(ApexPages.StandardController controller){
		shouldRedirect = false;
		redirectUrl = controller.view().getUrl();
		accId = controller.getId();
		tcgLv1 = false;
		tcgLv2 = false;
		//判断是TCG还是一般最终客户
		Account acc = [Select id,recordTypeId,TCG_Level__c from Account where id =:accId];
		if(acc.RecordTypeId == CONSTANTS.HUAWEICHINATCGRECORDTYPE){
			tcgKA = new TCG_Key_Activity__c(Account_Name__c = accId);
			if(acc.TCG_Level__c == 'Level 1') tcgLv1 = true;
			if(acc.TCG_Level__c == 'Level 2') tcgLv2 = true;
		} else {
			cr = new Customer_Relationship__c(Account_Name__c = accId);
		}
		ableToSave = Account.sObjectType.getDescribe().createable;
		
		//销管不允许创建TCG5大行为
		if(tcgLv1 || tcgLv2) { 
			User currentUser = [select Profile_Name__c from User where id =: Userinfo.getUserId()];
			if(currentUser.Profile_Name__c.contains('代表处销管')) ableToSave = false;
		}
	}

	public PageReference createcr(){
		if(tcgLv1 || tcgLv2) { //Logic1:对于TCG
			if(!tcgKA.Customer_Management__c && !tcgKA.OC_Coverage__c && 
				 !tcgKA.Marketing_Management__c && !tcgKA.OC_Customer_Management__c && 
				 !tcgKA.OC_Channel_Management__c && !tcgKA.OC_Marketing_Management__c && 
				 !tcgKA.Benchmarking_Management__c && !tcgKA.Pipeline_Management__c ){
				message = '至少需要选择一项才能保存';
				return null;
			}
		} else { //Logic2：对于最终客户
			if(!cr.Company_Visit__c && !cr.High_Level_Exchange__c && 
				 !cr.Important_Exposition_In__c && !cr.Strategy_Research__c && 
				 !cr.Showcase_Investigation__c){
				message = '至少需要选择一项才能保存';
				return null;
			}
		}
		//保存五大关键行为
		try{
			if(tcgLv1 || tcgLv2) { 
				insert tcgKA;
			} else {
				insert cr;
			}
		}catch(Exception e ){
			system.debug(e.getMessage());
			message = e.getMessage();
			if(tcgLv1 || tcgLv2) message = '您没有TCG的编辑权限，无法创建该TCG的关键动作';
			return null;//如果出现异常,直接返回
		}
		shouldRedirect = true;
		return null;
	}
}