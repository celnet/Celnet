@isTest
private class DealRegConvertToOppTest {
    private static Deal_Registration__c dealRegistration;
    
    static testMethod void currClsTest(){
        setupTestData();
        
        Test.startTest();
        
        Deal_Registration__c newdr = new Deal_Registration__c();
        newdr = dealRegistration.clone();
        insert newdr;
        DealRegConvertToOpp dto = new DealRegConvertToOpp();
        dto.currId = newdr.Id;
        dto.convert();
        
        Test.stopTest();
    }
    
    private static void setupTestData() {
        dealRegistration = new Deal_Registration__c();
        
        dealRegistration.Name               = 'Testing Deal Registration';
        dealRegistration.Deal_status__c     = 'Open';
        //Sales_stage__c = '';
        dealRegistration.Win_probability__c = 80;
        dealRegistration.Estimated_contracting_date__c = Date.today().addDays(1);
        dealRegistration.Deal_description__c           = 'Test';
        dealRegistration.End_customer_lookup__c        = UtilUnitTest.createAccount().Id;
        //Reseller__r.Id ,    
        //Reseller_contact__r.Id,       
        dealRegistration.Expiring_date__c = Date.today().addMonths(3);
        //related_opportunity__c,
        
        insert dealRegistration;
    }
}