/**
 * Test the corresponding AccountNAChangedHandler class
 *
 * Author           : BLS
 * Create Date      : 2013/3
 *     
 */  
@isTest
private class AccountNAChangedHandlerTest {

   /**
	*如果IsNAAcc属性变化了，且当前客户关联了子客户
	*系统将会将子客户的NA标签一同修改
	*/
    static testMethod void naChanged() {//
    	String RecordTypeId = UtilUnitTest.getRecordTypeIdByName('Account',
    		 'Huawei China Customer');
        Account grandParent = UtilUnitTest.createHWChinaAccount('grandparent');
        Account parent = UtilUnitTest.createHWChinaAccount('parent');
        parent.ParentId = grandParent.Id;
        update parent;
        Account child = UtilUnitTest.createHWChinaAccount('child');
        child.ParentId = parent.Id;
        update child;
        Account son = UtilUnitTest.createHWChinaAccount('son');
        son.ParentId = child.id;
        update son;

        Account grandson = UtilUnitTest.createHWChinaAccount('grandson');
        grandson.ParentId = son.id;
        update grandson;

				Test.startTest();
        Named_Account__c newNA = UtilUnitTest.createActiveNamedAccount(grandParent);
	
        parent = [select id,Is_Named_Account__c,Named_Account_Character__c,
        	Named_Account_Level__c,Named_Account_Property__c,NA_Approved_Date__c from Account where id = :parent.id];
        System.assertEquals(true,parent.Is_Named_Account__c);
        System.assertEquals('levelY',parent.Named_Account_Level__c);
        System.assertEquals('propertyY',parent.Named_Account_Property__c);
        System.assertEquals(Date.today(),parent.NA_Approved_Date__c);
        System.assertEquals('characterY',parent.Named_Account_Character__c);
        
        child = [select id,Is_Named_Account__c,Named_Account_Character__c,
        	Named_Account_Level__c,Named_Account_Property__c,NA_Approved_Date__c from Account where id = :child.id];
        System.assertEquals(true,child.Is_Named_Account__c);
        System.assertEquals('levelY',child.Named_Account_Level__c);
        System.assertEquals('propertyY',child.Named_Account_Property__c);
        System.assertEquals(Date.today(),child.NA_Approved_Date__c);
        System.assertEquals('characterY',child.Named_Account_Character__c);
        
        son = [select id,Is_Named_Account__c,Named_Account_Character__c,
        	Named_Account_Level__c,Named_Account_Property__c,NA_Approved_Date__c from Account where id = :son.id];
        System.assertEquals(true,son.Is_Named_Account__c);
        System.assertEquals('levelY',son.Named_Account_Level__c);
        System.assertEquals('propertyY',son.Named_Account_Property__c);
        System.assertEquals(Date.today(),son.NA_Approved_Date__c);
        System.assertEquals('characterY',son.Named_Account_Character__c);
        
        grandson = [select id,Is_Named_Account__c,Named_Account_Character__c,
        	Named_Account_Level__c,Named_Account_Property__c,NA_Approved_Date__c from Account where id = :grandson.id];
        System.assertEquals(true,grandson.Is_Named_Account__c);
        System.assertEquals('levelY',grandson.Named_Account_Level__c);
        System.assertEquals('propertyY',grandson.Named_Account_Property__c);
        System.assertEquals(Date.today(),grandson.NA_Approved_Date__c);
        System.assertEquals('characterY',grandson.Named_Account_Character__c);
        
        
        
        
    }
    
}