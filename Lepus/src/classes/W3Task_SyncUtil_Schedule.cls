public class W3Task_SyncUtil_Schedule {
	public static W3Task_Info.W3Task initW3Task(ProcessInstanceWorkitem WorkItem , Map<ID, SObject> map_obj , Map<ID , User> map_User, Map<ID,ProcessInstance> map_PI){
			W3Task_Info.W3Task w3task = new W3Task_Info.W3Task();
			w3task.appName = 'Salesforce';
			w3task.appURL = 'https://huawei.my.salesforce.com/home/home.jsp';
			w3task.type_x = 'J2EE';
			w3task.taskDesc = initTaskDesc(map_PI.get(WorkItem.ProcessInstanceId).TargetObjectId , map_obj);
			w3task.taskURL = 'https://'+URL.getSalesforceBaseUrl().getHost()+'/p/process/ProcessInstanceWorkitemWizardStageManager?id='+WorkItem.Id;
			w3Task.taskUUID = W3Task_SyncUtil_Schedule.initTaskUUID(WorkItem.ActorId , WorkItem , map_obj , map_PI , map_User) ;
			
			w3Task.time_x = WorkItem.SystemModstamp.format('yyyy-MM-dd hh:mm:ss');
			w3Task.handler = map_User.get(WorkItem.ActorId ).EnglishName__c+' '+map_User.get(WorkItem.ActorId ).Employee_ID__c;
			w3Task.applicant = map_User.get(map_PI.get(WorkItem.ProcessInstanceId).CreatedById).EnglishName__c+' '+map_User.get(map_PI.get(WorkItem.ProcessInstanceId).CreatedById).Employee_ID__c;
			w3Task.reserve3 = 'https://'+URL.getSalesforceBaseUrl().getHost()+'/'+map_PI.get(WorkItem.ProcessInstanceId).TargetObjectId;
			//使用reserve8记录ProcessInstanceID、reserve9记录WorkItemID或者StepId，用于异常处理
			w3Task.reserve8 = WorkItem.ProcessInstanceId;
			w3Task.reserve9 = WorkItem.Id;

			return w3Task;
	}

	public static String initTaskDesc(ID objId , Map<ID , SObject> map_obj){
		String sDesc = '';
		if(String.valueOf(objId).startsWith(Account_Link_Request__c.sObjectType.getDescribe().getKeyPrefix())){
			sDesc += ('[NA父子关系挂靠]['+map_obj.get(objId).get('Name')+']请审批NA父子关系挂靠申请');
		}else if(String.valueOf(objId).startsWith(Deal_Registration__c.sObjectType.getDescribe().getKeyPrefix())){
			Deal_Registration__c dr = (Deal_Registration__c)map_obj.get(objId);
			if(dr.RecordType.DeveloperName=='Huawei_Deal_Registration_Oversea'){ 
				sDesc += ('[Deal Registration]['+dr.Name+']Please appove the Deal Registration application.');
			}else if(dr.RecordType.DeveloperName=='Huawei_Deal_Registration_China'){
				sDesc += ('[渠道报备]['+dr.Name+']请审批渠道报备申请。');
			}
		}else if(String.valueOf(objId).startsWith(Campaign.sObjectType.getDescribe().getKeyPrefix())){
			Campaign cam = (Campaign)map_obj.get(objId);
			if(cam.RecordType.DeveloperName=='HW_Campaign'){
				sDesc += ('[Campaign]['+cam.Name+']Please appove the Campaign application.');
			}else if(cam.RecordType.DeveloperName=='Huawei_China_Campaign'){
				sDesc += ('[市场活动]['+cam.Name+']请审批市场活动申请。');
			}
		}
		return sDesc;
	}
	public static String initTaskUUID(ID userid,ProcessInstanceWorkitem workitem , Map<ID , SObject> map_obj , Map<ID , ProcessInstance> map_PI, Map<ID , User> map_User){
		String sUUID = '';
		sUUID += 'Salesforce_';
		sUUID += workitem.ProcessInstanceId+'_';
		ProcessInstance pi = map_PI.get(workitem.ProcessInstanceId);
		sUUID += pi.Steps.size();
		//sUUID += '_';
		//sUUID += map_User.get(userid).Employee_ID__c;
		return sUUID.toLowerCase();
	}
	
	public static Map<ID , sObject> initObject(set<ID> list_objId){
		Map<ID , SObject> map_obj = new Map<ID , SObject>();
		for(Account_Link_Request__c alr:[Select Id,Name From Account_Link_Request__c Where Id in: list_objId]){
			map_obj.put(alr.Id , (sobject)alr);
		}
		for(Campaign cam:[Select Id,Name,RecordTypeId,RecordType.DeveloperName From Campaign Where Id in: list_objId]){
			map_obj.put(cam.Id , (sobject)cam);
		}
		for(Deal_Registration__c dealReg:[Select Id,Name,RecordTypeId,RecordType.DeveloperName From Deal_Registration__c Where Id in: list_objId]){
			map_obj.put(dealReg.Id , (sobject)dealReg);
		}
		return map_obj;
	}
	
	public static Map<ID , User> initUser(){
		Map<ID , User> map_User = new Map<ID , User>([Select Id,Name,W3ACCOUNT__c,EnglishName__c,Employee_ID__c From User where isactive =true]);
		return map_User;
	}
	
	public static Map<ID , ProcessInstance> initProcessInstance(Set<ID> ProcessIdSet){
		//获取每个对象的最新审批流程，通过自查的方式
		Map<ID , ProcessInstance> map_objProcessInstance = new Map<ID , ProcessInstance>();
		for(ProcessInstance pi : [Select p.Status, TargetObjectId, p.CreatedDate, p.CreatedById, 
				(Select Id, OriginalActorId, ActorId, CreatedDate, SystemModstamp, ProcessInstanceId, ProcessInstance.CreatedById 
						From Workitems), 
				(Select Id, StepStatus, OriginalActorId, ActorId, StepNodeId, CreatedDate, SystemModstamp, ProcessInstanceId, ProcessInstance.CreatedById 
						From Steps Order by CreatedDate desc) 
			From ProcessInstance p Where id in: ProcessIdSet order by CreatedDate desc ]){
			if(!map_objProcessInstance.containsKey(pi.ID))
				map_objProcessInstance.put(pi.Id ,pi);
		}
		return map_objProcessInstance;
	}
	
	public static void W3Task_Util(List<ProcessInstanceWorkItem> wiList,List<ProcessInstanceStep> stepList,Set<Id> ProcessInstanceId)
	{
		Set<Id> ObjIdSet=new Set<Id>();
		List<W3Task_Info.W3Task> list_AddW3Task = new List<W3Task_Info.W3Task>();
		List<W3Task_Info.W3Task> list_DelW3Task = new List<W3Task_Info.W3Task>();
		
		List<ProcessInstance> tempPiList=[select TargetObjectId from ProcessInstance where id in:ProcessInstanceId];
		for(ProcessInstance pi:tempPiList)
		{
			ObjIdSet.add(pi.TargetObjectId);
		}
		Map<ID,User> map_User = initUser();
		Map<ID, SObject> map_obj = initObject(ObjIdSet);
		Map<ID , ProcessInstance> map_processInstance = initProcessInstance(ProcessInstanceId);
		for(ProcessInstanceWorkItem wi:wiList)
		{
			list_AddW3Task.add(initW3Task(wi,map_obj,map_User,map_processInstance));
		} 
		
		for(ProcessInstanceStep step : stepList)
		{			
			list_DelW3Task.add(W3Task_SyncUtil_Schedule.initDelW3Task(step, map_obj,map_User,map_processInstance));
		}
		List<W3_Task_Sync_Fail__c> list_sf = new List<W3_Task_Sync_Fail__c>();
		for(W3Task_Info.W3Task task : list_AddW3Task){
			W3_Task_Sync_Fail__c syncFail = W3Task_SyncUtil_Schedule.SendToW3(task , 'add' , '');
			if(syncFail != null)
			list_sf.add(syncFail);
		}
		for(W3Task_Info.W3Task task : list_DelW3Task){
			W3_Task_Sync_Fail__c syncFail = W3Task_SyncUtil_Schedule.SendToW3(task , 'del' , '');
			if(syncFail != null)
			list_sf.add(syncFail);
		}
		insert list_sf;
	}
	
	public static W3Task_Info.W3Task initDelW3Task(ProcessInstanceStep Step , Map<ID , SObject> map_obj , Map<ID , User> map_User , Map<ID , ProcessInstance> map_PI){
			W3Task_Info.W3Task w3task = new W3Task_Info.W3Task();
			w3Task.taskUUID = W3Task_SyncUtil_Schedule.initDelTaskUUID(Step.ActorId, Step , map_obj , map_PI , map_User) ;
			//使用reserve8记录ProcessInstanceID、reserve9记录WorkItemID或者StepId，用于异常处理
			w3Task.reserve8 = Step.ProcessInstanceId;
			w3Task.reserve9 = Step.Id;  

		return w3task; 
	}
	
	public static String initDelTaskUUID(ID userid,ProcessInstanceStep step , Map<ID , SObject> map_obj , Map<ID , ProcessInstance> map_PI, Map<ID , User> map_User){
		String sUUID = '';
		sUUID += 'Salesforce_';
		sUUID += step.ProcessInstanceId+'_';
		Integer i = 0;
		ProcessInstance pi = map_PI.get(step.ProcessInstanceId);
		for(ProcessInstanceStep s : pi.steps){
			if(s.CreatedDate < step.CreatedDate){
				i++;
			}
		}
		sUUID += i;
		//sUUID += '_';
		//sUUID += map_User.get(userid).Employee_ID__c;
		return sUUID.toLowerCase();
	}
	
	public static W3_Task_Sync_Fail__c SendToW3(W3Task_Info.W3Task task , String sAction , String sEncoded){
		W3Task_Service.SalesForce_W3_HTTPSPort taskToW3 = new W3Task_Service.SalesForce_W3_HTTPSPort();
		taskToW3.timeout_x = W3TASK_CONSTANTS.TIME_OUT;
		taskToW3.PartyInfo = new W3Task_Partyinfo.PartyInfo_element();
		W3Task_Partyinfo.party pFrom = new W3Task_Partyinfo.party();
		pFrom.name = W3TASK_CONSTANTS.SOAP_FROM_NAME;
		W3Task_Partyinfo.party pTo = new W3Task_Partyinfo.party();
		pTo.name = W3TASK_CONSTANTS.SOAP_TO_NAME;
		taskToW3.PartyInfo.from_x = pFrom;
		taskToW3.PartyInfo.to = pTo;
		taskToW3.PartyInfo.operationID = 'SalesForce/W3/'+(sAction=='add'?'createTask4W3':'deleteTask4W3');
		taskToW3.PartyInfo.operationType = 'SyncRequestResponse';
		taskToW3.PartyInfo.transactionID = task.taskUUID+datetime.now().format('YYYY-MM-DD HH:MM:SS');
		system.debug('Task info:'+task);
		System.debug('TaskToW3:'+taskToW3);
		Boolean success ;
		W3_Task_Sync_Fail__c syncFail ;
		try{
			if(sAction == 'add'){
				success = taskToW3.createTask4W3(task, sEncoded);
				system.debug('Create W3:' + task);
				system.debug('Create UUID:' + task.taskUUID + ' '+task.taskDesc);
				system.debug('Create result:===' + success);
			}else if(sAction == 'del'){
				success = taskToW3.deleteTask4W3(task.taskUUID, sEncoded);
				system.debug('Delete w3:' + task);
				system.debug('Delete UUID:' + task.taskUUID + ' '+task.taskDesc);
				system.debug('Delete result:===' + success);
			}
			
			if(!success)
			syncFail = W3Task_SyncUtil_Schedule.SaveSyncFail(task,sAction,'API Return False');
		}catch(exception e){
			system.debug('get exception :'+e.getMessage());
			syncFail = W3Task_SyncUtil_Schedule.SaveSyncFail(task,sAction,e.getMessage());
		}
		system.debug('Sync result:'+success);
		return syncFail;
	}
	public static W3_Task_Sync_Fail__c SaveSyncFail(W3Task_Info.W3Task task , String sAction ,String errorMessage ){
			W3_Task_Sync_Fail__c syncFail = new W3_Task_Sync_Fail__c();
			syncFail.SyncStatus__c = 'Open';
			syncFail.appName__c = task.appName;
			syncFail.appURL__c = task.appURL;
			syncFail.type_x__c = task.type_x;
			syncFail.taskDesc__c = task.taskDesc;
			syncFail.taskURL__c = task.taskURL;
			syncFail.taskUUID__c = task.taskUUID;
			syncFail.time_x__c = task.time_x;
			syncFail.handler__c = task.handler;
			syncFail.applicant__c = task.applicant;
			syncFail.reserve3__c = task.reserve3;
			syncFail.ActionType__c = sAction;
			syncFail.ProcessInstanceId__c = task.reserve8;
			syncFail.WorkItem_Step_Id__c = task.reserve9;
			syncFail.ErrorMessage__c = errorMessage; 
			//insert(syncFail);
			return syncFail;
	}
}