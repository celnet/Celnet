/*
*回访状态更新时,更新最后回访时间到相关的Account
*@Author:hewei
*Create Date：2014-7-8
*/
public without sharing class ServiceRequestUpdateSurveyDateToAccount implements Triggers.Handler{
		public static boolean shouldRun = true;
		public void handle(){//before insert,before update
			if(!CommonConstant.serviceRequestTriggerShouldRun){
				return ;
			}
			if(!shouldRun){
				return ;
			}
			Contact contact = new Contact();
			for (Case c : (List<Case>)trigger.new){
				if(c.Record_Type_Name__c.startsWith('China') ){
					if((c.HF_Remote_Survey_Answer3_china__c != null &&
					((Case)trigger.oldmap.get(c.id)).HF_Remote_Survey_Answer3_china__c ==null)||(c.HF_Onsite_Survey_Answer2_china__c != null &&
					((Case)trigger.oldmap.get(c.id)).HF_Onsite_Survey_Answer2_china__c ==null)||(c.HF_Spare_Survey_Answer2_china__c != null &&
					((Case)trigger.oldmap.get(c.id)).HF_Spare_Survey_Answer2_china__c ==null)){
						c.Last_Survey_Date__c =Datetime.now();//任何一个回访后，记录最后回访时间
						contact =[select id, Survey_Date__c from Contact where id =:c.ContactId];
						contact.Survey_Date__c=c.Last_Survey_Date__c;
						update contact;		
					}
				}
			}
		}
}