public with sharing class AccountIntegration {
	private final Id accountId;
	public Account a{get;set;}
	public String result{get;set;} 
	public String url {get;set;} // CIS的URL
	public RequestBody container {get;private set;}
	public Boolean isUpdate {get;private set;} //是修改客户信息操作还是申请CIS编码操作
	public Boolean isChinese {get;private set;}
	public Boolean shouldCommit {get;private set;}  //是否提交到CIS

	public Account_Info_Update_Request__c request {get;set;} //Salesforce相关字段修改申请
	public Boolean isNamedAccount {get;private set;}//控制界面相关字段的编辑属性
	public Boolean isSystemDept {get;private set;}//控制界面相关字段的编辑属性
	public Boolean ableToModify {get;private set;}  //是否有修改权限
	public Boolean cisInfoUpdate {get;private set;} //CIS的相关字段是否被更新
	public Boolean sfInfoUpdate {get;private set;}  //Salesforce待审批字段是否被更新
	public Boolean cityInfoUpdate {get;private set;}  //Salesforce的省份城市是否被更新

	public AccountIntegration(ApexPages.StandardController stdCon){
		//Step1: 初始化状态标识字段
		initFlagValue(); 
		//Step2: 获取客户ID
		accountId = stdCon.getId();
		//Step3: 加载客户信息 
		loadAccInfo();   
		//Step4: 权限校验
		checkPrivilege();
		//Step5: 将当前客户信息展示到页面
		container = new RequestBody(a); 
		//Step6: 创建并初始化一个客户信息修改申请
		if(isUpdate) {
			initAccountUpdateRequest();  
		}
	}
	
	/**
	 * 初始化状态标识字段
	 */
	private void initFlagValue() {
		isUpdate = false;
		isChinese = false;
		shouldCommit = false;
		isNamedAccount = false;
		isSystemDept = false;
		ableToModify = true;
		cisInfoUpdate = true;
		sfInfoUpdate = true;
		cityInfoUpdate = true;
		//instead of read from apex class,read from custom setting
		url = CISENDPOINT__c.getInstance('url').url__c;
	}
	
	/**
	 * 根据客户ID加载客户信息
	 */
	private void loadAccInfo() {
		Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.Account.fields.getMap();
		String queryString = 'select ';
		for(String s : fieldMap.keyset()){
			queryString += s + ',';
		}
		queryString = queryString.substring(0,queryString.length() -1);
		queryString += ' from Account where id = :accountId';
		a = Database.query(queryString);
	}
	
	/**
	 * 根据Account信息及权限信息设置状态标识字段
	 */
	private void checkPrivilege() {
		//Logic1: 如果是NA客户，不允许修改区域和行业信息
		if(a.Is_Named_Account__c){
			isNamedAccount = true;
		}
		//Logic2: 如果客户归属是系统部总部，允许修改省份信息
		//if(a.Representative_Office__c == '系统部总部'){
			isSystemDept = true;
		//}
		//Logic3: 根据是否有CIS编码判断是新增还是修改
		if(a.CIS__c != null){
			isUpdate = true;
		}
		//Logic4: 判断是否中国区客户
		if(a.RecordTypeDeveloperName__c.contains('China')){
			isChinese = true;
		}
		//Logic5: 判断当前用户是否有权限进行修改
		UserRecordAccess access ;
		try{
			access = [SELECT RecordId, HasReadAccess, HasEditAccess FROM UserRecordAccess
								WHERE UserId = :UserINfo.getUserId() AND RecordId = :a.id limit 1];
		}catch(QueryException e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '无法获取用户对当前客户的权限信息'));
		}
		if(access == null || !access.HasEditAccess){
			ableToModify = false;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, System.Label.No_Privilege));
		}
	}
    	
	/**
	 * 创建一个客户信息修改申请，同时记录客户相关字段的的历史信息
	 */
	private void initAccountUpdateRequest() {
		request = new Account_Info_Update_Request__c();
		request.account__c = a.id;
		request.Account_Name_Old__c = a.Name;
		request.Representative_Office_Old__c = a.Representative_Office__c;
		request.Province_Old__c = a.Province__c;
		request.City_Old__c = a.City_Huawei_China__c;
		request.Industry_Old__c = a.Industry;
		request.Sub_industry_Old__c = a.Sub_industry_HW__c;
		request.Customer_Group_Old__c = a.Customer_Group__c;
		request.Customer_Group_Code_Old__c = a.Customer_Group_Code__c;
		request.Is_Named_Account__c = a.Is_Named_Account__c;  
	}
	
	/**
	 * 提交客户信息修改申请
	 * 任何信息做过修改都会发起CIS审批流程，如更新了归属和行业则发起SF审批流程
	 */
	public PageReference submit(){
		PageReference result = null;
		String nameOld = container.param2;       //记录历史值 update 20140114
		String streetOld = container.param8;     //记录历史值
		String phoneOld = container.param9;      //记录历史值
		String postalCodeOld = container.param10;//记录历史值
		String cisTeamOld = container.param15;   //记录历史值
		checkUpdate();
		if(!cisInfoUpdate && !sfInfoUpdate) {
			//Logic1: 如果什么信息都没有更新，提示错误
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '没有修改任何客户信息,请修改后再提交.'));
		} else {
			if(a.Representative_Office__c.equals('系统部总部') && a.Industry.equals('商业销售部')) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '若系统部为"商业销售部"，区域/系统部不允许为"系统部总部"'));
				return result; 
			}
			//update by Steven 2014-02-15 start
			if(a.Representative_Office__c.equals('系统部总部') && a.Industry.equals('运营商业务支持部')) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '若系统部为"运营商业务支持部"，区域/系统部不允许为"系统部总部"'));
				return result; 
			}
			if(!a.Is_Named_Account__c && !a.Industry.equals('商业销售部') && !a.Industry.equals('运营商业务支持部')) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '非NA客户的"系统部"只能选择"商业销售部"或"运营商业务支持部"'));
				return result; 
			}
			//update by Steven 2014-02-15 end
			//Logic3: 修改了归属和行业相关信息，发起SF审批
			if(sfInfoUpdate){	
				if(checkHasRequest()) {     //判断是否已经有申请记录
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '存在待审批的客户信息修改记录，请勿重复提交Salesforce修改申请.'));
				} else {
					result = startApproval(); //跳转到SF待审批界面
				}
			}
			//Logic4: 修改了任何客户相关信息， 发起CIS审批
			if(cisInfoUpdate){
				result = null;       //跳转到CIS申请界面
				shouldCommit = true; //允许发起CIS申请
				container = new RequestBody(a);//set new value
			}
			//Logic2: 仅更新了省份城市，直接刷新客户属性
			if(!sfInfoUpdate && cityInfoUpdate) {
				a.AccInfoUpdate__c = !a.AccInfoUpdate__c; //跳过不允许修改的校验规则
				a.Name = nameOld;								 //update by steven 20131223
				a.Street__c = streetOld;         //不直接刷新该字段
				a.Phone = phoneOld;              //不直接刷新该字段
				a.Postal_Code__c = postalCodeOld;//不直接刷新该字段
				a.CIS_Team__c = cisTeamOld;      //不直接刷新该字段
				update a;
			}
		}
		return result;
	}
    
	/**
	 * 检查客户的相关属性是否有更新
	 */
	private void checkUpdate() {
		//CIS相关的字段只有7个（客户名称、营业地址、邮政编码、电话、CIS Team、省份、城市）
		String cisFieldOld = container.param2 + container.param8 + container.param9 + container.param10 + container.param15 + container.param5 + container.param6;
		String cisFieldNew = parmHandler(a.name) + parmHandler(a.Street__c) + parmHandler(a.Phone) + parmHandler(a.Postal_Code__c) + parmHandler(a.CIS_Team__c) + parmHandler(a.Province__c) + parmHandler(a.City_Huawei_China__c);
		String sfFieldOld = parmHandler(request.Account_Name_Old__c) + parmHandler(request.Representative_Office_Old__c) + parmHandler(request.Customer_Group_Code_Old__c);		
		String sfFieldNew = parmHandler(a.name) + parmHandler(a.Representative_Office__c) + parmHandler(a.Customer_Group_Code__c);
		String cityFieldOld = parmHandler(request.Province_Old__c) + parmHandler(request.City_Old__c);		//update by steven 20131223
		String cityFieldNew = parmHandler(a.Province__c) + parmHandler(a.City_Huawei_China__c);//update by steven 20131223
		//Logic1: 检查CIS审批关键字段是否有更新（有更新则提交CIS申请）
		if(cisFieldOld.equals(cisFieldNew)) {
			cisInfoUpdate = false;
		} else {
			cisInfoUpdate = true; //reset flag
		}
		//Logic2: 检查SF审批关键字段是否有更新（有更新则提交Salesforce申请）
		if(sfFieldOld.equals(sfFieldNew)) {
			sfInfoUpdate = false;
		} else {
			sfInfoUpdate = true; //reset flag
		}
		//Logic3: 检查省份城市字段是否有更新
		if(cityFieldOld.equals(cityFieldNew)) {
			cityInfoUpdate = false;
		} else {
			cityInfoUpdate = true; //reset flag
		}
	}

	/**
	 * 保存Request并发起审批流程
	 */
	private PageReference startApproval(){
		//Step1: Create an account update request
		request.account__c = a.id;
		request.Account_Name__c = a.Name;
		request.Representative_Office__c = a.Representative_Office__c;
		request.Province__c = a.Province__c;
		request.City__c = a.City_Huawei_China__c;
		request.Industry__c = a.Industry;
		request.Sub_industry__c = a.Sub_industry_HW__c;
		request.Customer_Group__c = a.Customer_Group__c;
		request.Customer_Group_Code__c = a.Customer_Group_Code__c;
		insert request;
		//Step2: Submit the approval request
		Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
		req.setObjectId(request.id);
		Approval.ProcessResult result = Approval.process(req);
		
		return new ApexPages.StandardController(request).view();
	}

	/**
	 * 检查客户下是已存在提交的修改申请
	 */
	private Boolean checkHasRequest(){
		Boolean result = false;
		List<Account_Info_Update_Request__c> oldRequests = [Select id FROM Account_Info_Update_Request__c 
																												where Process_End__c = false and account__c = : a.id];
		if(oldRequests.size() > 0){
			result = true;
		}
		return result;
	}
    
	//wrapper class
	public class RequestBody{
		public User currentUser {get;set;}
		public User owner {get;set;}
		public Account a {get;set;}
		public String param0 {get;set;}
		public String param1 {get;set;}
		public String param2 {get;set;}
		public String param3 {get;set;}
		public String param4 {get;set;}
		public String param5 {get;set;}
		public String param6 {get;set;}
		public String param7 {get;set;}
		public String param8 {get;set;}
		public String param9 {get;set;}
		public String param10 {get;set;}
		public String param11 {get;set;}
		public String param12 {get;set;}
		public String param13 {get;set;}
		public String param14 {get;set;}
		public String param15 {get;set;}
		public String param16 {get;set;}
		public String param17 {get;set;}
		public String param18 {get;set;}
		public String method {get;set;}
		public String workid {get;set;}
		public String username {get;set;}
		public String useremail {get;set;}
		public String mobile {get;set;}
		public String workphone {get;set;}

		public RequestBody(Account acc){
			this.a = acc;
			List<User> users = [select id,Employee_ID__c,Name,Phone,MobilePhone,email from User 
													where id =: a.OwnerId or id =: UserInfo.getUserId()];
			for(User u : users){
				if(u.id == UserInfo.getUserId()){
					this.currentUser = u;
				}
				if(u.id == a.OwnerId){
					this.owner = u;
				}
			}
			this.param0 = a.id;
			this.param1 = a.CIS__c;
			this.param2 = a.name;
			this.param3 = a.RecordTypeDeveloperName__c.contains('China') ? a.name : a.Local_Name__c; //中国区LocalName和Name保持一致
			this.param4 = a.Country_HW__c;
			this.param5 = a.Province__c;
			this.param6 = parmHandler(a.City_Huawei_China__c);
			this.param7 = '-'; //parmHandler(a.ShippingCountry + a.ShippingPostalCode + a.ShippingState +a.ShippingStreet + a.ShippingCity);
			this.param8 = parmHandler(a.Street__c); 
			this.param9 = parmHandler(a.Phone);
			this.param10 = parmHandler(a.Postal_Code__c);
			this.param11 = a.Customer_Group_Code__c; //a.CIS__c == null ? a.Customer_Group_Code__c : '^'; //更新时不传客户群到CIS
			this.param12 = 'China';
			this.param13 = 'China';
			this.param14 = '安捷信';
			this.param15 = parmHandler(a.CIS_Team__c);
			this.param16 = CONSTANTS.CISEMPLOYEE;
			this.param17 = owner.name;
			this.param18 = owner.Employee_ID__c;
			this.method = a.CIS__c == null ? 'create' : 'update';
			this.workid = currentUser.Employee_ID__c; 
			this.username = currentUser.name;
			this.useremail = currentUser.email;
			this.mobile = parmHandler(currentUser.mobilePhone);
			this.workphone = currentUser.Phone == null ? this.mobile : currentUser.Phone; 
		}
	} 
    
	private static String parmHandler(String str){
		if(str == null){
			return CONSTANTS.CISEMPTYSTRING;
		}else{
			return str;
		}
	}
}