/**************************************************************************************************
 * Name             : UtilNamedAccount
 * Object           : Named_Account__c
 * Requirement      :   
 * Purpose          : 
 * Author           : BLS
 * Create Date      : 2013/3/5
 * Modify History   : 
 ***************************************************************************************************/ 
public without sharing class UtilNamedAccount {
    //
    public static List<Named_Account__c> createNAsFromParentAccount(Named_Account__c na){
    	System.debug('enter the createNAsFromParentAccount method------------');
    	System.debug('na is :------' + na);
    	List<Account> accs = [select id from Account where parentid =:na.Account_Name__c];//查找第一层子客户
    	Set<Account> childrenAccounts = new  Set<Account>();
    	childrenAccounts.addAll(accs);
    	while(accs.size() > 0){
    		accs = [select id from Account where parentid in : accs];
    		if(0 == accs.size()){//未查找到任何子记录
    			break;
    		}
	    	childrenAccounts.addAll(accs);
    	}
    	List<Named_Account__c> toCreateNAs = new List<Named_Account__c>();
    	for(Account a : childrenAccounts){
    		Named_Account__c newNa = na.clone(false,true,false,false);
    		newNa.Account_Name__c = a.id;
    		toCreateNAs.add(newNa);
    	}
    	System.debug('going to create :---------------' + toCreateNAs);
    	return toCreateNAs;
    }
    
    //构建Named_Account__c对象
    public static Named_Account__c NewNamedAccount(Id AccountId, Boolean Is_Named_Account, String Named_Account_Property, 
                                                    String Named_Account_Level, String Named_Account_Character, 
                                                    Date Approved_Date, Boolean Active, Boolean Is_System_Create, 
                                                    String Record_Description,Boolean Is_403_Customer){
        
        Named_Account__c na = new Named_Account__c();
        na.Account_Name__c = AccountId;
        na.Is_Named_Account__c = Is_Named_Account;
        na.Named_Account_Property__c = Named_Account_Property;
        na.Named_Account_Level__c = Named_Account_Level;
        na.Named_Account_Character__c = Named_Account_Character;
        na.Is_403_Customer__c = Is_403_Customer;
        na.Approved_Date__c = Approved_Date;
        na.Active__c = Active;
        na.Is_System_Create__c = Is_System_Create;
        na.Record_Description__c = Record_Description;

        return na;
    }
    
    public static Named_Account__c newNotNA(Id AccountId){
    	 Named_Account__c na = new Named_Account__c();
    	 na.Account_Name__c = AccountId;
        na.Is_Named_Account__c = false;
        na.Named_Account_Property__c = null;
        na.Named_Account_Level__c = null;
        na.Is_403_Customer__c = false;
        na.Named_Account_Character__c = null;
        na.Approved_Date__c = null;
        na.Active__c = true;
        return na;
    }
    
    /*  
    		Refactor the methods to UtilAccount.cls
    
    //更新Account的根节点父客户
    public static void UpdateHQParentAccount(Id ParentId, Id HQParentAccountId){
	    list <Account> AccountList = [select id, HQ_Parent_Account__c
	    				                    from Account 
	                                        where ParentId =: ParentId];
	    //失效旧的Name Account
	    for(Account acc : AccountList){
	    	acc.HQ_Parent_Account__c = HQParentAccountId;
	    	UpdateHQParentAccount(acc.id, HQParentAccountId);
	    }
	    update AccountList;
    }
    
    //给定account id和其根父节点id,一次性更新其下所有子节点的根父节点
    public static void UpdateAllHQParentAccount(Id ParentId, Id HQParentAccountId){
    	list <Account> AccountList = [select id, HQ_Parent_Account__c,parent.parent.parent.parent.parent.id  
	    				                    from Account 
	        where ParentId =: ParentId or parent.parent.id =:parentid or parent.parent.parent.id = :parentid
	     or parent.parent.parent.parent.id = :parentid or parent.parent.parent.parent.parent.id = :parentid];//取到五层
	    System.debug('the AccountList :-----' + AccountList);
	    Set<id> childIds = new Set<id>();
	    for(Account a : AccountList){
	    	try{
		    	if(a.parent.parent.parent.parent.parent.id != null){
		    		childIds.add(a.id);
		    	}
	    	}catch(Exception e){}//do nothing ,just avoid null point exception
	    }
		    List<Account> acc = [select id, HQ_Parent_Account__c,parent.parent.parent.parent.parent.id  
	    				                    from Account 
	        where ParentId in :childIds or parent.parent.id in :childIds or parent.parent.parent.id in :childIds
	     or parent.parent.parent.parent.id in :childIds or parent.parent.parent.parent.parent.id in :childIds];
	     	if(acc.size() > 0){
	     		AccountList.addAll(acc);//先只取十层的数据,待完善
	     	}
	     Set<Account> accs = new Set<Account>();
	     accs.addall(AccountList);
	     AccountList.clear();
	     AccountList.addAll(accs);
	     for(Account a : AccountList){
	     	a.HQ_Parent_Account__c =  HQParentAccountId;
	     }
	     System.debug('the child list:------' + AccountList);
	     update AccountList;
    }
    
    
    */
    
}