@isTest
private class WorkOrderChangeOwnerHandlerTest {

    static testMethod void notSDUserTest() {
    	Account acc = UtilUnitTest.createAccount();
    	Case c = UtilUnitTest.createChinaServiceRequest();
    	Work_Order__c wo =  UtilUnitTest.createOnSiteWorkOrder(acc,c);
    	String whereClause = ' (not UserRole.DeveloperName like \'%sd%\') and (not UserRole.DeveloperName like \'%External%\') and  UserRoleid != null and  id != ' + '\'' +userinfo.getuserid()  + '\'' ;
	    User notSDUser = UtilUnitTest.queryUser(whereClause);
	    wo.ownerid = notSDUser.id;
	    CommonConstant.changedFromAccount = false;
	    update wo;
    	whereClause = ' UserRole.DeveloperName like \'%External%\' and  UserRoleid != null and  id != ' + '\'' +userinfo.getuserid()  + '\'' ;
	    User externalUser = UtilUnitTest.queryUser(whereClause);
	    System.assertNotEquals(userinfo.getuserid(),externalUser.id);
	    System.runAs(notSDUser){
		    wo.ownerid = externalUser.id;
		    try{
			    update wo;
			    System.assertEquals('should not enter here','只有SD和外部用户可以将工单派给外部用户');
		    }catch(Exception e){
		    
		    }
	    }
    }
}