public class EdEmail {
		
		List<Messaging.Sendemailresult> sendInfo=null;
    	public EdEmail(){}
    	
    	Messaging.SingleEmailMessage mail=new Messaging.SingleEmailMessage();
    	public List<String> sendToEmailAddrList{
    		get{
    			return sendToEmailAddrList;
    		}
    		set{sendToEmailAddrList=value;}
    	}
    	public String sendToEmailAddr{
    		get{return sendToEmailAddr.trim();}
    		set{
    			sendToEmailAddr=value; 
    			if(sendToEmailAddrList==null)
    				sendToEmailAddrList=new String[]{};
    			if(sendToEmailAddr.indexOf(';')>0)
    				for(String addr:sendToEmailAddr.split(';'))
    					sendToEmailAddrList.add(addr.trim());
    			else
    				sendToEmailAddrList.add(sendToEmailAddr.trim());
    		}
    	} 
    	public String replyTo{// Specify the address used when the recipients reply to the email.
    		get{return replyTo;}
    		set{replyTo=value;}
    	}
    	public String subject{
    		get{return subject;}
    		set{subject=value;}
    	}
    	public String senderDisplayName{
    		get{return senderDisplayName;}
    		set{senderDisplayName=value;}
    	}
    	public String plainTextBody{
    		get{return plainTextBody;}
    		set{plainTextBody=value;}
    	}
    	public String htmlBody{
    		get{return htmlBody;}
    		set{htmlBody=value;}
    	}
    	
    	public void sendEmail(){
    		mail.setSaveAsActivity(true);
    		mail.setSubject(subject);
    		mail.setToAddresses(sendToEmailAddrList); 
    		mail.setReplyTo(replyTo); 
    		mail.setSenderDisplayName(senderDisplayName);
    		mail.setBccSender(false);
    		mail.setUseSignature(false);
    		mail.setPlainTextBody(plainTextBody);
    		mail.setHtmlBody(htmlBody);
    		
    		sendInfo = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail}); 
    		
    	}
    	
    	public List<Messaging.Sendemailresult> getSendResult(){
    		if(sendInfo==null) return null;
		    if (!sendInfo[0].isSuccess()){
		        
		    }
    		return sendInfo;
    	}
    	
    public static TestMethod void clsTest(){
    	String TO='ken.yuxy@gmail.com';
    		EdEmail mail=new EdEmail();
    		mail.sendToEmailAddr=TO;
    		mail.senderDisplayName='Ken,DN';
    		mail.replyTo='k_kin2003@hotmail.com';
    		mail.subject='来自SF的推送短信发送测试...';
    		String smsContent='subject：test class，body：<br><br>短信内容发送测试,收到此邮件请回邮至 ken.yuxy@gmail.com';
    		mail.htmlBody=smsContent;
    		mail.sendEmail();
    		mail.getSendResult();
    }
}