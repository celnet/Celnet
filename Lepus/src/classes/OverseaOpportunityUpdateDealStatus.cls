/**
 * @Purpose : (海外)更新机会点是刷新相关的Deal_Registration中Deal Status字段值
 * @Author : Steven
 * @Date : 2014-08-29 Refactor
 */
public without sharing class OverseaOpportunityUpdateDealStatus implements Triggers.Handler{ //before update
    public void handle(){
        //Step1: 记录所有待刷新的机会点ID
        Set<Id> oppIds = new Set<Id>();
        for(Opportunity opt : (List<Opportunity>)Trigger.New){
            if(opt.RecordTypeId == CONSTANTS.HUAWEIOVERSEAOPPRECORDTYPE || 
                 opt.RecordTypeId == CONSTANTS.HUAWEIOVERSEADEALOPPRECORDTYPE){ 
                Opportunity oldOpt = (Opportunity)trigger.oldMap.get(opt.id);
                if(opt.StageName != oldOpt.StageName) {
                    oppIds.Add(opt.Id);
                }
            }
        }
        if(oppIds.isEmpty()) return;
        system.debug('-----OverseaOpportunityUpdateDealStatus----');
        
        //Step2: 一次查询出所有机会点关联的报备信息
        List<Opportunity> oppWithDRList = 
            [Select o.Id,(Select Deal_Status__c, LastModifyDateTime_Trigger__c From Deal_Registrations__r) From Opportunity o Where Id In : oppIds];
        Map<id,list<Deal_Registration__c>> drMap = new Map<id,list<Deal_Registration__c>>();
        for (Opportunity opp : oppWithDRList){
            drMap.put(opp.id,opp.Deal_Registrations__r);
        }
        //Step3：记录下需要更新的报备记录
        List<Deal_Registration__c> drUpdateList = new List<Deal_Registration__c>();
        for(Opportunity opt : (List<Opportunity>)Trigger.New) {
            if(oppIds.contains(opt.id)) {
                List<Deal_Registration__c> drList = drMap.get(opt.id);
                //Step3-1：丢单的机会点
                if(opt.StageName == 'SSA (Huawei No Bid)' || opt.StageName == 'SSB (Customer Did Not Pursue)' || opt.StageName == 'SSC (Lost to Competition)'){
                    for(Deal_Registration__c dr : drList){
                        if(dr.Deal_Status__c != 'Invalid'){
                            dr.Deal_Status__c = 'Cancelled';
                            drUpdateList.Add(dr);
                        }
                    }
                }
                //Step3-2：赢单的机会点
                if(opt.StageName == 'SS7 (Implementing)' || opt.StageName == 'SS8 (Completed)'){
                    for(Deal_Registration__c dr : drlist){
                        if(dr.Deal_Status__c != 'Invalid'){
                            dr.Deal_Status__c = 'Closed';         
                            dr.LastModifyDateTime_Trigger__c = system.now();
                            drUpdateList.Add(dr);
                        }
                    }
                }
                //Step3-3：跟进中的机会点
                if(opt.StageName == 'SS4 (Developing Solution)' || opt.StageName == 'SS5 (Gaining Agreement)' || opt.StageName == 'SS6 (Winning)'){
                    for(Deal_Registration__c dr : drlist){
                        dr.LastModifyDateTime_Trigger__c = system.now();
                        drUpdateList.Add(dr);
                    }
                }
            }
        }
        //Step4：刷新需要更新的报备记录
        if (drUpdateList.Size() > 0){
            try {
                update drUpdateList;
            }catch (DMLException e) {
                Trigger.New[0].addError(e.getDMLMessage(0));
            }
        }               
    }

}