@isTest
private class AccountParentChangedHandlerTest {
	static testMethod void notNAParent(){
		Account parent = UtilUnitTest.createHWChinaAccount('p');
		Account child = UtilUnitTest.createHWChinaAccount('c');
		child.ParentId = parent.id;
		update child;
		System.assertEquals(0,[select count() from Named_Account__c]);
	}
	
  //非NA客户关联到NA父客户
	static testMethod void NAParent1(){
		Account parent = UtilUnitTest.createHWChinaAccount('p');
		Named_Account__c na = UtilUnitTest.createActiveNamedAccount(parent);
		Account child = UtilUnitTest.createHWChinaAccount('c');
		child.ParentId = parent.id;
		Test.startTest();
		update child;
		Test.stopTest();
		child = [select id,Is_Named_Account__c,Named_Account__c,Named_Account_Character__c,
		    Named_Account_Property__c,Named_Account_Level__c from Account where id = :child.id];
		Named_Account__c otherNA = [select id ,Named_Account_Character__c,Named_Account_Property__c,Is_Named_Account__c,
		    Named_Account_Level__c from Named_Account__c where id != :na.id  LIMIT 1];
		    
  	System.assertNotEquals(null, child.Named_Account__c);
    System.assertEquals(true,child.Is_Named_Account__c);    
    System.assertEquals('characterY',child.Named_Account_Character__c);    
    System.assertEquals('propertyY',child.Named_Account_Property__c);    
    System.assertEquals('levelY',child.Named_Account_Level__c);    
    System.assertEquals('characterY',otherNA.Named_Account_Character__c);
    System.assertEquals('propertyY',otherNA.Named_Account_Property__c);
    System.assertEquals('levelY',otherNA.Named_Account_Level__c);
    System.assertEquals(true,otherNA.Is_Named_Account__c);
	}

	//解除与NA父客户的关联,将所有子客户设为非NA客户
	static testMethod void NAParentRemoved(){
		Account parent = UtilUnitTest.createHWChinaAccount('p');
		Named_Account__c na = UtilUnitTest.createActiveNamedAccount(parent);
    parent = [select id,Named_Account__c,Is_Named_Account__c from Account where id =:parent.id];
    System.assertEquals(na.id,parent.Named_Account__c);
    System.assertEquals(true,parent.Is_Named_Account__c);
        
    Account child = UtilUnitTest.createHWChinaAccount('c');
    child.ParentId = parent.Id;
    update child;
    child = [select id,Named_Account__c,Is_Named_Account__c from Account where id =:child.id];
    System.assertEquals(true,child.Is_Named_Account__c);

    Test.startTest();
    child.ParentId = null;
    update child;
    Test.stopTest();
        
		child = [select id,Is_Named_Account__c,Named_Account__c from Account where id = :child.id];
		System.assertEquals(false,child.Is_Named_Account__c); 
	}
}