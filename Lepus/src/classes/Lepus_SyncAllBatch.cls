global class Lepus_SyncAllBatch implements Database.Batchable<Sobject>, Database.Stateful, Database.AllowsCallouts{
    global String objType;
    global String syncType;
    global String queryString;
    
    global Lepus_SyncAllBatch(String objectType){
        this.objType = objectType;
        
        if(objType == 'Opportunity'){
            queryString = 'Select Id From Opportunity Where RecordTypeId = \'';
            queryString += CONSTANTS.CHINAOPPORTUNITYRECORDTYPE;
            queryString += '\' or RecordTypeId = \'';
            queryString += CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE;
            queryString += '\' or RecordTypeId = \'';
            queryString += CONSTANTS.HUAWEIOVERSEADEALOPPRECORDTYPE;
            queryString += '\' or RecordTypeId = \'';
            queryString += CONSTANTS.HUAWEIOVERSEAOPPRECORDTYPE;
            queryString += '\'';
        } else if(objType == 'Lead'){
            queryString = 'Select Id From Lead Where RecordTypeId = \'';
            queryString += CONSTANTS.OVERSEALEADRECORDTYPE;
            queryString += '\' or RecordTypeId = \'';
            queryString += CONSTANTS.CHINALEADRECORDTYPE;
            queryString += '\'';
        }
    }
    
    global Database.querylocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(queryString);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        for(sObject s : scope){
            Lepus_Queue__c qu = new Lepus_Queue__c();
            qu.RecordId__c = (Id)s.get('Id');
            qu.RecordGlobalId__c = (Id)s.get('Id');
            qu.SyncType__c = '团队成员同步';
            qu.SyncTime__c = Datetime.now();
            qu.ObjectType__c = this.objType;
            qu.Action__c = 'update';
            
            Lepus_QueueService service = new Lepus_QueueService();
            service.QueueHandle(qu);
            
	    }
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
}