public class EdHistory {

    /* Properties of the class */
    public datetime historydt { get; private set; }
    public boolean ispublic   { get; private set; }
    public string actorname   { get; private set; }
    public string historyType { get; private set; }
    public string to          { get; private set; }
    public string fr          { get; private set; }
    
    /* Class constructor */
    public EdHistory(Datetime d, boolean p, String actor, String ht, String f, String t) {
        historydt   = d;
        historydate = d.format();
        ispublic    = p;
        actorname   = actor;
        historyType = ht;
        fr          = f;
        to          = t;
    }
    
    /* Formatting methods utilized primarily by the CaseHistoryPrint Visualforce page*/
    public string historydate { get; set; }
    public string dtmonthyr   { get { return historydt.format('MMMMM yyyy'); } }
    public string dttime      { get { return historydt.format('h:mm a');} }
    public string dtdayfmt    { get { return historydt.format('d - EEEE'); } }
    public integer dtmonth    { get { return historydt.month();} }
    public integer dtyear     { get { return historydt.year();} }
    public integer dtday      { get { return historydt.day();} }
    
    public static TestMethod void currClsTest(){
    	EdHistory eh=new EdHistory(System.now(),true,'ken','ht','f','t');
    	eh.historydate='';
    	String s=eh.dtmonthyr;
    	s=eh.dttime;
    	s=eh.dtdayfmt ;
    	Integer i=eh.dtmonth;
    	i=eh.dtyear;
    	i=eh.dtday;
    }
}