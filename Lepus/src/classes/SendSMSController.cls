public class SendSMSController {
	
	private static final String TO='hwcrmsms@huawei.com';
	private String currId;
	private String toIdPath;
    public SendSMSController(ApexPages.StandardController controller) {

    }  
	String msg='';
	public Boolean smsTypeIsNull{
		get{return smsTypeIsNull;}
		set{smsTypeIsNull=value;}
	}
	//短信以邮件形式到固定地址：hscrmsms@huaweisymantec.com
	//格式：subject：手机号，body：短信内容
	public void init(){
		
	    ApexPages.Message myMsg = null;
    	currId= ApexPages.currentPage().getParameters().get('id');
    	Case cs=[Select c.SMS_Type__c, c.SMS_Message__c,c.Contact.MobilePhone From Case c where c.id=:currId];
    	String msg='';
    	if(cs.SMS_Type__c==null){
    		toIdPath='/'+currId;
    		smsTypeIsNull=true;
    		msg='返回';
    		myMsg=new ApexPages.Message(ApexPages.Severity.ERROR, '发送失败:请确保已经选择SMS Type.'
    			+'<br><br><br><a href="'+getPath()+'" onclick="history.go(-1);return false;">返回</a> ');
    		ApexPages.addMessage(myMsg); 
    	}else{ 
    		smsTypeIsNull=false;
    		EdEmail mail=new EdEmail();
    		mail.sendToEmailAddr=TO;
    		mail.subject=cs.Contact.MobilePhone;
    		String smsContent=cs.SMS_Message__c;
    		mail.htmlBody=smsContent;
    		mail.sendEmail();
    		toIdPath='"/'+currId+'"';
    		
    	}
	}
    
    public String getMsg(){
    	return msg;
    }
    
    public String getPath(){
    	return toIdPath;
    } 
    
	public static testMethod void currClsTest(){
		EdEmail.clsTest();
		Case ist_case= UtilUnitTest.createChinaServiceRequest();
		ist_case.SMS_Type__c='More Information Request';
		update ist_case;
		
		Case ist_case2 = UtilUnitTest.createChinaServiceRequest();
		
		Case cs0=[Select c.SMS_Type__c, c.SMS_Message__c,c.Contact.MobilePhone From Case c where c.SMS_Type__c!=null limit 1];
		ApexPages.currentPage().getParameters().put('id',cs0.id);
		SendSMSController ssc=new SendSMSController(null);
		ssc.init();
		ssc.getMsg();
		ssc.getPath();
		Boolean b=ssc.smsTypeIsNull;
		
		Case cs=[Select c.SMS_Type__c, c.SMS_Message__c,c.Contact.MobilePhone From Case c where c.SMS_Type__c=null limit 1];
		ApexPages.currentPage().getParameters().put('id',cs.id);
		ssc=new SendSMSController(null);
		ssc.init();
		ssc.getMsg();
		ssc.getPath();
		b=ssc.smsTypeIsNull;
	}
}