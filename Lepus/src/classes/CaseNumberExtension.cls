// Retrieve & save iCare number by calling iCare web service
// Ian Huang
// 2013-08-13

public with sharing class CaseNumberExtension {
	
	private Id caseId;
	public Case serviceRequest{get;private set;}
	
	public CaseNumberExtension(ApexPages.StandardController stdController) {
		caseId = stdController.getId();
	}
	
	public void init() {
		serviceRequest = ServiceRequestSyncUtils.syncToEIP(caseId);
	}
	
}