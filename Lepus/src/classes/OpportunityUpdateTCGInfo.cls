/*
* @Purpose : 1、中国区机会点创建时刷新机会点的TCG信息
*						 2、中国区机会点客户更改时刷新机会点的TCG信息
*            fires before update & insert 
* @Author : Steven
* @Date : 2014-03-03
*/
public without sharing class OpportunityUpdateTCGInfo implements Triggers.Handler{
	public static Boolean isFirstRun = true;
	public void handle(){
		if(OpportunityUpdateTCGInfo.isFirstRun){
			Set<Id> accIds = new Set<Id>(); //涉及到的客户ID
			Map<Id,Id> accTCGMap = new Map<Id,Id>(); //客户和TCG的对应表
			//获取涉及的所有客户
			for(Opportunity opp : (List<Opportunity>)trigger.new){
				if(opp.RecordTypeId == CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE ||
				 	 opp.RecordTypeId == CONSTANTS.CHINAOPPORTUNITYRECORDTYPE ){
					accIds.add(opp.AccountId);
				}
			}
			if(accIds.size()==0) return;
			
			//获取涉及客户的TCG信息
			List<Account> accList = [select id,TCG_Name__c from account where id in:accIds];
			for(Account acc : accList) {
				accTCGMap.put(acc.id,acc.TCG_Name__c);
			}
	
			//Step1: 对于未关闭的机会点在插入前的更新TCG信息
			if(Trigger.isInsert){
				for(Opportunity opp : (List<Opportunity>)trigger.new){
					if(opp.RecordTypeId == CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE ||
					 	 opp.RecordTypeId == CONSTANTS.CHINAOPPORTUNITYRECORDTYPE ){
						if(!opp.Is_Closed__c){
							opp.TCG_Name__c = accTCGMap.get(opp.AccountId);
						}
					}
				}
			}
			//Step2:对于未关闭的机会点在更改前的更新TCG信息
			if(Trigger.isUpdate){
				for(Opportunity opp : (List<Opportunity>)trigger.new){
					if(opp.RecordTypeId == CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE ||
					 	 opp.RecordTypeId == CONSTANTS.CHINAOPPORTUNITYRECORDTYPE ){
						//如果客户被更改，且未关闭
						if(opp.AccountId != ((Opportunity)trigger.OldMap.Get(opp.Id)).AccountId && !opp.Is_Closed__c){
							opp.TCG_Name__c = accTCGMap.get(opp.AccountId);
						}
					}
				}
			}
		}
		
	}

}