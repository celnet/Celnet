public class Lepus_ReSync {
    public static List<Lepus_Log__c> logList;
    public static Map<Id,String> idGlobalIdMap;
    
    public static void reSync(){
        if((Lepus_Data_Sync_Controller__c.getInstance('客户业务数据') != null) && 
                                            Lepus_Data_Sync_Controller__c.getInstance('客户业务数据').IsSync__c)
        reSyncAccount();
        
        if((Lepus_Data_Sync_Controller__c.getInstance('机会点业务数据') != null) && 
                                            Lepus_Data_Sync_Controller__c.getInstance('机会点业务数据').IsSync__c)
        reSyncOpportunity();
        
        if((Lepus_Data_Sync_Controller__c.getInstance('线索业务数据') != null) && 
                                            Lepus_Data_Sync_Controller__c.getInstance('线索业务数据').IsSync__c)
        reSyncLead();
        
        if((Lepus_Data_Sync_Controller__c.getInstance('联系人业务数据') != null) && 
                                            Lepus_Data_Sync_Controller__c.getInstance('联系人业务数据').IsSync__c)                         
        reSyncContact();
        
        deleteHistoryLogs();
    }
    
    public static void deleteHistoryLogs(){
         delete [Select Id From Lepus_Log__c Where LastModifiedDate__c <: Datetime.now().addHours(-72) limit 10000];
    }
    
    public static void reSyncOpportunity(){
        
        logList = [Select id, LastModifiedDate__c, RecordId__c from Lepus_Log__c
                                        where LastModifiedDate__c >: Datetime.now().addMinutes(-16) 
                                        and RecordId__c like '006%' limit 1000];
        
        List<Opportunity> oppList = [Select id, createddate, lastmodifieddate, global_id__c from Opportunity 
                                        where LastModifiedDate >: Datetime.now().addMinutes(-16)
                                        and (RecordTypeId =: CONSTANTS.CHINAOPPORTUNITYRECORDTYPE
                                        or RecordTypeId =: CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE
                                        or RecordTypeId =: CONSTANTS.HUAWEIOVERSEAOPPRECORDTYPE
                                        or RecordTypeId =: CONSTANTS.HUAWEIOVERSEADEALOPPRECORDTYPE) limit 1000];
        
        Set<Opportunity> unsyncedOpps = new Set<Opportunity>(oppList);
        Set<Opportunity> syncedOpps = new Set<Opportunity>();
        idGlobalIdMap = new Map<Id, String>();
        
        for(Opportunity opp : oppList){
            idGlobalIdMap.put(opp.Id, opp.Global_ID__c);
            for(Lepus_Log__c log : logList){
                if(log.RecordId__c == opp.Id && log.LastModifiedDate__c == opp.LastModifiedDate){
                    syncedOpps.add(opp);
                }
            }
        }
        
        unsyncedOpps.removeAll(syncedOpps);
        
        List<Id> insertOppIds = new List<Id>();
        List<Id> updateOppIds = new List<Id>();
        
        for(Opportunity opp : unsyncedOpps){
            if(opp.LastModifiedDate <= opp.CreatedDate.addSeconds(3)){
                insertOppIds.add(opp.Id);
            } else {
                updateOppIds.add(opp.Id);
            }
        }
        
        if(insertOppIds.size() > 0){
            Lepus_SyncUtil.initQueue(insertOppIds, idGlobalIdMap, 'insert', '业务数据同步', datetime.now());
            Lepus_SyncUtil.initQueue(insertOppIds, idGlobalIdMap, 'update', '字段更新同步', datetime.now());
        }
        
        if(updateOppIds.size() > 0){
            Lepus_SyncUtil.initQueue(updateOppIds, idGlobalIdMap, 'update', '业务数据同步', datetime.now());
            Lepus_SyncUtil.initQueue(updateOppIds, idGlobalIdMap, 'update', '团队成员同步', datetime.now());
            Lepus_SyncUtil.initQueue(updateOppIds, idGlobalIdMap, 'update', '字段更新同步', datetime.now());
        }
    }
    
    public static void reSyncAccount(){
        logList = [Select id, LastModifiedDate__c, RecordId__c from Lepus_Log__c
                                        where LastModifiedDate__c >: Datetime.now().addMinutes(-16) 
                                        and RecordId__c like '001%' limit 1000];
        
        List<Account> accList = [Select id, createddate, lastmodifieddate, global_id__c from Account 
                                        where LastModifiedDate >: Datetime.now().addMinutes(-16)
                                        and (RecordTypeId =: CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE
                                        or RecordTypeId =: CONSTANTS.HUAWEIOVERSEACUSTOMERRECORDTYPE) limit 1000];
        
        Set<Account> unsyncedAccs = new Set<Account>(accList);
        Set<Account> syncedAccs = new Set<Account>();
        idGlobalIdMap = new Map<Id, String>();
        
        for(Account acc : accList){
            idGlobalIdMap.put(acc.Id, acc.Global_ID__c);
            for(Lepus_Log__c log : logList){
                if(log.RecordId__c == acc.Id && log.LastModifiedDate__c == acc.LastModifiedDate){
                    syncedAccs.add(acc);
                }
            }
        }
        
        unsyncedAccs.removeAll(syncedAccs);
        
        List<Id> insertAccIds = new List<Id>();
        List<Id> updateAccIds = new List<Id>();
        
        for(Account acc : unsyncedAccs){
            if(acc.LastModifiedDate <= acc.CreatedDate.addSeconds(3)){
                insertAccIds.add(acc.Id);
            } else {
                updateAccIds.add(acc.Id);
            }
        }
        
        if(insertAccIds.size() > 0){
            Lepus_SyncUtil.initQueue(insertAccIds, idGlobalIdMap, 'insert', '业务数据同步', datetime.now());
        }
        
        if(updateAccIds.size() > 0){
            Lepus_SyncUtil.initQueue(updateAccIds, idGlobalIdMap, 'update', '业务数据同步', datetime.now());
            Lepus_SyncUtil.initQueue(updateAccIds, idGlobalIdMap, 'update', '团队成员同步', datetime.now());
        }
    }
    
    public static void reSyncLead(){
        logList = [Select id, LastModifiedDate__c, RecordId__c from Lepus_Log__c
                                        where LastModifiedDate__c >: Datetime.now().addMinutes(-16) 
                                        and RecordId__c like '00Q%' limit 1000];
        
        List<Lead> leadList = [Select id, createddate, lastmodifieddate, global_id__c from Lead 
                                        where LastModifiedDate >: Datetime.now().addMinutes(-16)
                                        and (RecordTypeId =: CONSTANTS.CHINALEADRECORDTYPE
                                        or RecordTypeId =: CONSTANTS.OVERSEALEADRECORDTYPE) limit 1000];
        
        Set<Lead> unsyncedLeads = new Set<Lead>(leadList);
        Set<Lead> syncedLeads = new Set<Lead>();
        idGlobalIdMap = new Map<Id, String>();
        
        for(Lead lead : leadList){
            idGlobalIdMap.put(lead.Id, lead.Global_ID__c);
            for(Lepus_Log__c log : logList){
                if(log.RecordId__c == lead.Id && log.LastModifiedDate__c == lead.LastModifiedDate){
                    syncedLeads.add(lead);
                }
            }
        }
        
        unsyncedLeads.removeAll(syncedLeads);
        
        List<Id> insertLeadIds = new List<Id>();
        List<Id> updateLeadIds = new List<Id>();
        
        for(Lead lead : unsyncedLeads){
            if(lead.LastModifiedDate <= lead.CreatedDate.addSeconds(3)){
                insertLeadIds.add(lead.Id);
            } else {
                updateLeadIds.add(lead.Id);
            }
        }
        
        if(insertLeadIds.size() > 0){
            Lepus_SyncUtil.initQueue(insertLeadIds, idGlobalIdMap, 'insert', '业务数据同步', datetime.now());
        }
        
        if(updateLeadIds.size() > 0){
            Lepus_SyncUtil.initQueue(updateLeadIds, idGlobalIdMap, 'update', '业务数据同步', datetime.now());
            Lepus_SyncUtil.initQueue(updateLeadIds, idGlobalIdMap, 'update', '团队成员同步', datetime.now());
        }
    }
    
    public static void reSyncContact(){
        logList = [Select id, LastModifiedDate__c, RecordId__c from Lepus_Log__c
                                        where LastModifiedDate__c >: Datetime.now().addMinutes(-16) 
                                        and RecordId__c like '003%' limit 1000];
        
        List<Contact> conList = [Select id, createddate, lastmodifieddate, global_id__c from Contact 
                                        where LastModifiedDate >: Datetime.now().addMinutes(-16)
                                        and (RecordTypeId =: CONSTANTS.HUAWEICHINACONTACTRECORDTYPE
                                        or RecordTypeId =: CONSTANTS.HUAWEIOVERSEACONTACTRECORDTYPE) limit 1000];
        
        Set<Contact> unsyncedCons = new Set<Contact>(conList);
        Set<Contact> syncedCons = new Set<Contact>();
        idGlobalIdMap = new Map<Id, String>();
        
        for(Contact con : conList){
            idGlobalIdMap.put(con.Id, con.Global_ID__c);
            for(Lepus_Log__c log : logList){
                if(log.RecordId__c == con.Id && log.LastModifiedDate__c == con.LastModifiedDate){
                    syncedCons.add(con);
                }
            }
        }
        
        unsyncedCons.removeAll(syncedCons);
        
        List<Id> insertConIds = new List<Id>();
        List<Id> updateConIds = new List<Id>();
        
        for(Contact con : unsyncedCons){
            if(con.LastModifiedDate <= con.CreatedDate.addSeconds(3)){
                insertConIds.add(con.Id);
            } else {
                updateConIds.add(con.Id);
            }
        }
        
        if(insertConIds.size() > 0){
            Lepus_SyncUtil.initQueue(insertConIds, idGlobalIdMap, 'insert', '业务数据同步', datetime.now());
        }
        
        if(updateConIds.size() > 0){
            Lepus_SyncUtil.initQueue(updateConIds, idGlobalIdMap, 'update', '业务数据同步', datetime.now());
        }
    }
}