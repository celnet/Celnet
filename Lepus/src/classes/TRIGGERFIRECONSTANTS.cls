/*
* @Purpose : 
* 作为变量记录trigger是否需要执行
* @Author : Brain Zhang
* @Date :       2013-11
*
*
*/
public without sharing class TRIGGERFIRECONSTANTS {
	public static boolean accountTriggerShouldRun = true; 
	public static boolean serviceRequestUpdateSyncHandlerShouldRun = true;
}