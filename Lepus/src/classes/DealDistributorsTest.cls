@isTest(SeeAllData = true)
private class DealDistributorsTest {
    static testMethod void myUnitTest() {
    Deal_Registration__c deal = [Select id From Deal_Registration__c 
      Where Deal_Status__c = 'Waiting For Confirmation' and RecordTypeId = '01290000000p2AA' limit 1];	
    Account partner = [Select id From Account Where RecordTypeId = '01290000000p2AZ' limit 1];	
    Contact contact = [Select id From Contact Where recordtypeid ='01290000000p2Aa' limit 1];	
    Deal_Registration_Distributor__c distributor = new Deal_Registration_Distributor__c();
    distributor.Deal_Registration__c = deal.id;
    distributor.Distributor__c = partner.id;
    distributor.Distributor_Contact__c = contact.id;
    Test.startTest();
    insert distributor;
    Test.stopTest();
	}
}