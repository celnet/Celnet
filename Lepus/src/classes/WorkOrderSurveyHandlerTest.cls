@isTest
private class WorkOrderSurveyHandlerTest {

    static testMethod void singleTest() {
    	Account acc = UtilUnitTest.createAccount();
    	Case sr = UtilUnitTest.createChinaServiceRequest(); 
    	Work_Order__c w = UtilUnitTest.createOnSiteWorkOrder(acc,sr);
    	w.Survey_Rejected_Reason__c = 'test';
    	update w;
    	w = [select id,Survey_Time__c,Status__c from Work_Order__c where id =:w.id];
    	System.assertNotEquals(null,w.Survey_Time__c);
    	System.assertNotEquals('Closed',w.Status__c);
    	
    	w.Overall_Satisfactory__c ='test';
    	update w;
    	w = [select id,Survey_Time__c,Status__c from Work_Order__c where id =:w.id];
    	System.assertNotEquals(null,w.Survey_Time__c);
    	System.assertEquals('Closed',w.Status__c);
    }
    
    static testMethod void updateSRTest(){//	更新对应SR状态为Closed,更新对应SR的满意度与Work Order相同
    	System.debug('begin the test method--------------------');
      Account acc = UtilUnitTest.createAccount();
    	Case sr = UtilUnitTest.createChinaServiceRequest(); 
    	sr=[select id,Satisfactory_Level1_China__c ,status from case where id =:sr.id];
    	System.assertNotEquals('Closed',sr.status);
    	Work_Order__c w = UtilUnitTest.createOnSiteWorkOrder(acc,sr);
    	Work_Order__c w2 = UtilUnitTest.createOnSiteWorkOrder(acc,sr);
    	w.Overall_Satisfactory__c ='test';
    	w2.Overall_Satisfactory__c ='test';
    	System.debug('enter this section:---------------');
    	update new Work_Order__c[] {w,w2};
    	
    	sr=[select id,Satisfactory_Level1_China__c ,status from case where id =:sr.id];
    	//System.assertNotEquals('Closed',sr.status);
    	//System.assertEquals('test',sr.Satisfactory_Level1_China__c);
    	
    	/*因相关逻辑已被注释,以上验证无法通过.如果重新打开,请注释以下测试代码,并将上面的注释取消
    	*/
    	System.assertNotEquals(null,sr.status);
    	System.assertEquals(null,sr.Satisfactory_Level1_China__c);
    }
    
    /*
    static testMethod void vrTest(){//当Satisfactory Level原值不为空时；且回访各内容有更新时.禁止修改
    	Account acc = UtilUnitTest.createAccount();
    	Case sr = UtilUnitTest.createChinaServiceRequest();
    	Work_Order__c w = UtilUnitTest.createOnSiteWorkOrder(acc,sr);
    	
    	w.Overall_Satisfactory__c ='test';
    	update w;
    	w = [select id,Survey_Time__c,Status__c from Work_Order__c where id =:w.id];
    	System.assertNotEquals(null,w.Survey_Time__c);
    	System.assertEquals('Closed',w.Status__c);
    	
    	w.Overall_Satisfactory__c ='test2';
    	try{
	    	update w;
	    	System.assertEquals('should never run into this','回访已完成，不允许再更新回访内容');
    	}catch(Exception e){
    	
    	}
    	
    	
    }
    
    */
}