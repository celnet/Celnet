@isTest
private class ServiceRequestOwnerChangedHandlerTest {
		static testMethod void notRun(){
				Case serviceRequest = UtilUnitTest.createChinaServiceRequest();
				CommonConstant.serviceRequestTriggerShouldRun = false;
				update serviceRequest;
		}
		
		static testMethod void notRun2(){
				Case serviceRequest = UtilUnitTest.createChinaServiceRequest();
				ServiceRequestOwnerChangedHandler.shouldRun = false;
				update serviceRequest;
		}

    static testMethod void updateNotApplicableUserTest(){
    	Case serviceRequest = UtilUnitTest.createChinaServiceRequest();
    	String whereClause = ' ( not Profile.Name  like \'%HRE%\' ) and ( not Profile.Name  like \'%HPE%\') and (not Profile.Name  like \'%PSE%\') and (not Profile.Name  like \'%CSE%\')  and (not Profile.Name  like \'System%\') ';
    	User notApplicableUser = UtilUnitTest.queryUser(whereClause);
    	serviceRequest.ownerid = notApplicableUser.id;
    	try{
	    	update serviceRequest;
	    	System.assertEquals('should not enter here','the user is not applicable');
    	}catch(DMLException e){
    		//should enter here;
    	}
    	
    	serviceRequest = [select id,ownerid from Case where id =:serviceRequest.id];
    	System.assertEquals(serviceRequest.ownerid,Userinfo.getuserid());
    	
    }
    
    static testMethod void updateApplicableUserTest(){
    		Case serviceRequest = UtilUnitTest.createChinaServiceRequest();
    		String whereClause = ' ( Profile.Name  like \'%HRE%\' or Profile.Name  like \'%CSE%\' ) and id !='
    			 + '\'' +userinfo.getuserid()  + '\'';
	    	User CSEUser = UtilUnitTest.queryUser(whereClause);
	    	System.debug('the queryed user:------' + CSEUser);
	    	System.assertNotEquals(userinfo.getuserid(),CSEUser.id);
	    	serviceRequest.ownerid = CSEUser.id;
	    	System.debug('the current service request:---' + serviceRequest);
    		update serviceRequest;
    		serviceRequest = [select id,ownerid,HRE_Start_Time__c from Case where id =:serviceRequest.id];
    		System.assertEquals(serviceRequest.ownerid,CSEUser.id);
    		System.assertNotEquals(null,serviceRequest.HRE_Start_Time__c);
    		
    		whereClause = ' ( Profile.Name  like \'%HPE%\' or Profile.Name  like \'%PSE%\' ) and id !='
    			 + '\'' +userinfo.getuserid()  + '\'';
	    	User PSEUser = UtilUnitTest.queryUser(whereClause);
	    	serviceRequest.ownerid = PSEUser.id;
	    	System.debug('the current service request:---' + serviceRequest);
    		update serviceRequest;
    		serviceRequest = [select id,ownerid,HPE_Start_Time__c,HRE_Actual_Time__c from Case where id =:serviceRequest.id];
    		System.assertEquals(serviceRequest.ownerid,PSEUser.id);
    		System.assertNotEquals(null,serviceRequest.HPE_Start_Time__c);
    		System.assertNotEquals(null,serviceRequest.HRE_Actual_Time__c);
    }
}