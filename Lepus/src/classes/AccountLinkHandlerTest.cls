@isTest
private class AccountLinkHandlerTest {

    static testMethod void approved() {
    		Account parent = UtilUnitTest.createHWChinaAccount('p');
        Named_Account__c na = UtilUnitTest.createActiveNamedAccount(parent);
        parent = [select id,Named_Account__c,Is_Named_Account__c from Account where id =:parent.id];
        System.assertEquals(na.id,parent.Named_Account__c);
        System.assertEquals(true,parent.Is_Named_Account__c);
        
        Account child = UtilUnitTest.createHWChinaAccount('c');
        Account_Link_Request__c request = new Account_Link_Request__c(child__c = child.id,parent__c = parent.id);
        insert request;
        Test.startTest();
        request.IsApproved__c = true;
        update request;
        Test.stopTest();
        child = [select id,Named_Account__c,Is_Named_Account__c from Account where id =:child.id];
        System.assertEquals(true,child.Is_Named_Account__c);
    }
    
    static testMethod void failure() {//associate na account to non-na parent 
    		Account parent = UtilUnitTest.createHWChinaAccount('p');
        
        Account child = UtilUnitTest.createHWChinaAccount('c');
        Named_Account__c na = UtilUnitTest.createActiveNamedAccount(child);
        Account_Link_Request__c request = new Account_Link_Request__c(child__c = child.id,parent__c = parent.id);
        try{
	        insert request;
        }catch(DMLException e){
        	//should enter here
        }
    }
}