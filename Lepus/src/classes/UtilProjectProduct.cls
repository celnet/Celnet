/**************************************************************************************************
 * Name             : Utility for TigProjectProduct
 * Object           : Opportunity, Project_Product__c, Product_Master__c
 * Requirement      : Cretiera Base Sharing Rule based on Opportunity, 
 *                    so we need to syn the industry on account to related opportunities
 * Purpose          : Update the Opportunity Level field of all related opportunities
 * Author           : Mouse Liu
 * Create Date      : 05/16/2012
 * Modify History   : 
 * 
***************************************************************************************************/
public class UtilProjectProduct { 
    // Update the Opportunity Level field of all related opportunities
    public static void processProjectProductChange(List<Project_Product__c> productTrigger) {
        if (trigger.isExecuting && trigger.isAfter) {
            Set<Id> oppIds = new Set<Id>();
            // If insert or delete action, we just get the related Opportunities
            //     and recalculate the Opportunity Level of them.
            if (trigger.isDelete || trigger.isInsert) {
                for (Project_Product__c product : productTrigger) {
                    oppIds.add(product.Project_Name__c);
                }
            }
            // If update action, we need to execude the project products which
            //     Product Master has no change.
            else if (trigger.isUpdate) {
                for (Project_Product__c newProduct : productTrigger) {
                    Project_Product__c oldProduct = (Project_Product__c) trigger.oldMap.get(newProduct.Id);
                    if (oldProduct.Lookup__c != newProduct.Lookup__c) {
                        oppIds.add(newProduct.Project_Name__c);
                    }
                }
                
                // If no records which Product Master is changed, so just exit
                if (oppIds.size() == 0) {
                    return;
                }
            }
            
            // According to the oppIds, get the opporutnities and the related project product,
            //     and recalculate the Opportunity Level
            if (oppIds.size() > 0) {
               processOpportunityLevel(oppIds);
            }
        }
    }
    
    public static void processOpportunityLevel(Set<Id> oppIds) { 
        List<Opportunity> opps = [SELECT Id, Opportunity_Level1__c,
                                    (SELECT Id, Project_Name__c, Lookup__r.Level_1__c
                                     FROM Project_Product__r)
                                  FROM Opportunity
                                  WHERE Id IN :oppIds];
                     
        Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>();                                 
        for (Opportunity opp : opps) {
            // Initiate oppLevel
            String oppLevel = '';
            
            // Populate the oppLevel with the product master Level value
            for (Project_Product__c product : opp.Project_Product__r) {
                if (product.Lookup__r.Level_1__c != null && product.Lookup__r.Level_1__c != ''
                        && !oppLevel.contains(product.Lookup__r.Level_1__c)) {
                    oppLevel += product.Lookup__r.Level_1__c + ';';
                }
            }
            
            // Set the Opportunity Level
            opp.Opportunity_Level1__c = oppLevel;
            oppMap.put(opp.Id, opp);
        }
        
        try {
            update oppMap.values();
        }
        catch (DMLException e) {
            if (trigger.isDelete) {
               trigger.old[0].addError(e.getDMLMessage(0));
            }
            else {
                trigger.new[0].addError(e.getDMLMessage(0));
            }
            System.debug('UtilProjectProduct.processOpportunityLevel Exception: ' + e);
        }
    }
}