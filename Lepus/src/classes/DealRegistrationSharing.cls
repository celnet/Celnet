public without sharing class DealRegistrationSharing {
    public ApexPages.StandardController controller {get;set;}
    public DealRegistrationSharing(ApexPages.StandardController stdCon){
         
        controller = stdCon;
        
    }
    
        public PageReference redirect(){
        	System.debug('the record id:------' + controller.getId());
            UserRecordAccess access = [SELECT RecordId, HasReadAccess, HasEditAccess
         FROM UserRecordAccess
         WHERE UserId = :UserINfo.getUserId()
         AND RecordId = :controller.getId() limit 1];
       if(!access.HasReadAccess){//当前用户无读权限
       	//检查当前用户是否为approver,如果是,则赋予edit权限
            List<ProcessInstance> instances = [SELECT Id,TargetObjectId, 
                (SELECT Id, ActorId,OriginalActorId ,ProcessInstanceId FROM Workitems)
                    FROM ProcessInstance where TargetObjectId =:controller.getId()];
            Set<String> approverIDs = new Set<String>();
        for(ProcessInstance i : instances){
            System.debug('the i:-------' + i);
            for(ProcessInstanceWorkitem m : i.Workitems){
            	System.debug('the m :------' + m);
                approverIds.add(m.ActorId);
                approverIds.add(m.OriginalActorId);
            }
        }
        System.debug('the exist approverids:------' + approverIds);
        System.debug('current user id :------' + UserINfo.getUserId());
        if(approverIds.contains(UserINfo.getUserId())){
            Deal_Registration__Share shareRecord = new Deal_Registration__Share(ParentId = controller.getId(),
                    UserOrGroupId = UserINfo.getUserId(),AccessLevel  = 'Edit',
                    RowCause = Schema.Deal_Registration__Share.rowCause.Share_to_approver__c);
            insert shareRecord;
            
        }
       }
       PageReference p = controller.view();
       p.getParameters().put('nooverride', '1');
        return p;
        }
}