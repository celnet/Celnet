/*
 * @Purpose : 中国区线索失效和关闭逻辑
 *				 1、点击失效按钮将当前线索失效
 *				 2、点击关闭按钮将当前线索关闭
 * @Author : Steven
 * @Date : 2013-10-29
 */
public class ChinaLeadToInvalid {
	public String currId{ 
		get{return currId;}
		set{currId=value;} 
	} 
	public String operType{ 
		get{return operType;}
		set{operType=value;} 
	}
   
	public ChinaLeadToInvalid(ApexPages.StandardController controller) { 
		currId= ApexPages.currentPage().getParameters().get('id');
		operType= ApexPages.currentPage().getParameters().get('type');
	}

	public ChinaLeadToInvalid() { 
		currId= ApexPages.currentPage().getParameters().get('id');
		operType= ApexPages.currentPage().getParameters().get('type');
	}
  
	public Boolean haveError { 
		get{return haveError;} 
		set{haveError=value;}
	}  
   
	public String getId() {
		return currId;  
	} 
   
	public void init(){ 
		Lead l = [Select Status,Status_Changed__c from Lead where id=:currId and recordtypeid =: CONSTANTS.CHINALEADRECORDTYPE];
		if(l != null) {
			if(operType=='closed') { 
				l.Status = CONSTANTS.CHINALEADSTATUSCLOSED;
			} else if(operType=='invalid') {
				l.Status = CONSTANTS.CHINALEADSTATUSINVALID;
			}
			l.Status_Changed__c = !l.Status_Changed__c;
			update(l);
		} else {
			l.addError('没有查到对应的中国区线索，请确认线索的类型是否为中国区线索');
		}
	}
}