/**
 * @Purpose A: 1、中国区最终客户的归属及行业信息变化了要同步刷新子客户和机会点(已关闭的机会点不需要再刷新);
 *             2、关键信息包括：区域、代表处、省份、城市、系统部、子系统部、客户群、客户群编码;
 *             3、适用于Huawei China Deal Registration 和 Huawei China Opportunity 两种记录类型；
 *             4、涉及到两个地方，一个是直接创建机会点，第二个是Lead转换的机会点页面；
 * @Author : Steven
 * @Date :   2014-07-01 Update
 */
public without sharing class AccountAfterUpdateHandler implements Triggers.Handler{ //fires after update
	public static Boolean isFirstRun = true;
	public void handle(){
		if(AccountAfterUpdateHandler.isFirstRun){
			Set<Id> updateAccIds = new Set<Id>();
			//Step1: 中国区最终客户关键字段发生变化记录下ID (含父子客户)
			for (Account acc : (List<Account>)trigger.new){
				if (acc.RecordTypeId == CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE){
					Account oldAcc = (Account)trigger.OldMap.Get(acc.Id);
					if((acc.Customer_Group_Code__c != oldAcc.Customer_Group_Code__c)||
						 (acc.Representative_Office__c != oldAcc.Representative_Office__c)||
						 (acc.City_Huawei_China__c != oldAcc.City_Huawei_China__c)||
						 (acc.Province__c != oldAcc.Province__c)){
						updateAccIds.add(acc.id);
					}
				}
			}
			if(updateAccIds.isEmpty()) return;
      
      //Step2: 一次查询出所有涉及的客户信息（含父子客户）
			Map<id,Account> allAccMap = new Map<id,Account>([select id,ParentId,industry,Sub_industry_HW__c,Customer_Group__c,Customer_Group_Code__c,
	    				  																							Region_HW__c,Representative_Office__c,Province__c,City_Huawei_China__c
	    		 																							 from Account where ParentId in:updateAccIds or id in:updateAccIds]);
			//Step3: 一次查询出所有涉及的机会点信息（父子客户所有机会点）
			List<Account> accWithOpps = [select id, (select id, Region__c, Representative_Office__c, Province__c, City__c, System_Department__c, Sub_Industry__c, Customer_Group__c, Customer_Group_Code__c
											 													 from Opportunities where IsClosed = false and (recordtypeid =:CONSTANTS.CHINAOPPORTUNITYRECORDTYPE or recordtypeid =:CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE))
																		 from Account where id in :allAccMap.keySet()];
			Map<id,list<Opportunity>> allOppMap = new Map<id,list<Opportunity>>();
			for (Account acc : accWithOpps){
				allOppMap.put(acc.id,acc.Opportunities);
			}
			
			//Step4：刷新子客户和机会点的归属行业信息
			Map<id,Account> updateAccMap = new Map<id,Account>();
			Map<id,Opportunity> updateOppMap = new Map<id,Opportunity>();
			for(Account currentAcc : (List<Account>)trigger.new){
				if(updateAccIds.contains(currentAcc.Id)){
					List<Opportunity> currentOppList = allOppMap.get(currentAcc.Id);
					if(currentOppList == null) currentOppList = new List<Opportunity>();
					//Step4-1:更新当前客户所有子客户的行业及归属信息
					for(Account child : allAccMap.values()){
						if(currentAcc.id == child.ParentId){
							child.HQ_Parent_Account__c = currentAcc.ParentId;
							//acc.City_Huawei_China__c = currentAcc.City_Huawei_China__c;
							child.industry = currentAcc.industry;
							child.Sub_industry_HW__c = currentAcc.Sub_industry_HW__c;
							child.Customer_Group__c = currentAcc.Customer_Group__c;
							child.Customer_Group_Code__c = currentAcc.Customer_Group_Code__c;
							child.Region_HW__c = currentAcc.Region_HW__c;
							child.Representative_Office__c = currentAcc.Representative_Office__c;
							if(child.Representative_Office__c != '系统部总部') {
								child.Province__c = currentAcc.Province__c;
							}
							//Step4-1-1:将子客户添加进带刷新列表
							updateAccMap.put(child.id,child);
							//Step4-1-2:将子客户的机会点添加到待刷新列表
							List<Opportunity> childOpps = allOppMap.get(child.Id);
							for(Opportunity childOpp : childOpps){
								childOpp.City__c = child.City_Huawei_China__c;
								currentOppList.add(childOpp);
							}
						}
					}
					//Step4-2:根据客户属性刷新机会点的归属和行业属性
					for(Opportunity o : currentOppList){
						o.Customer_Group_Code__c = currentAcc.Customer_Group_Code__c;
						o.Customer_Group__c = currentAcc.Customer_Group__c;
						o.Sub_Industry__c = currentAcc.Sub_industry_HW__c;
						o.System_Department__c = currentAcc.Industry;
						if(currentAcc.Representative_Office__c == '系统部总部') {
							o.City__c = null;
							o.Province__c = '中国';//代表处选择系统部总部，省份任然是各个省份，但是传到机会点的时候只传中国
						} else {
							o.Province__c = currentAcc.Province__c;
							if(currentAcc.id == o.AccountId) o.City__c = currentAcc.City_Huawei_China__c; //仅刷当前客户的城市信息，子客户的不刷
						}
						o.Representative_Office__c = currentAcc.Representative_Office__c;
						o.Region__c = currentAcc.Region_HW__c;
						updateOppMap.put(o.id,o);
					}
				} // end of current account
			} // end of trigger.new
			
			TRIGGERFIRECONSTANTS.accountTriggerShouldRun = false;//提前禁止子客户上trigger的执行
	    update updateAccMap.values();
	    TRIGGERFIRECONSTANTS.accountTriggerShouldRun = true;//允许子客户上trigger的执行
	    update updateOppMap.values();
			//AccountAfterUpdateHandler.isFirstRun = false;
		}
	}
}