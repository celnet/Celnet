/**************************************************************************************************
 * Name             : TigOnSalesTeam
 * Object           : Account, Opportunity
 * Requirement      : When creating Opportunity Sales Team, system send email to the new memeber
 * Purpose          : 1. Create a custom object [Sales Team]
 *                    2. When creating the new team member on the custom object, sync with standard
 *                       object and copy the email of user to the custom field [Team Email]
 *                    3. When deleting and update, sync the two object records
 *                    4. Create a workflow to send the email alert
 * Author           : Tulipe Lee
 * Create Date      : 05/19/2012
 * Modify History   : 
 * 
***************************************************************************************************/
public with sharing class UtilSalesTeam {
    // Copy email of user field to Team Email field
    public static void updateTeamEmailAfterInsert() {
        if (trigger.isExecuting && trigger.isBefore && trigger.isInsert) {
            // Get userIds in Sales Team
            Set<Id> userIds = new Set<Id>();
            for (SObject so : trigger.new) {
                Sales_Team__c team = (Sales_Team__c) so;
                if (team.User__c != null) {
                    userIds.add(team.User__c);
                }
            }
            
            // Query email of the Sales Team Member
            Map<Id, User> userMap = new Map<Id, User>([SELECT Id, Email FROM User WHERE Id IN :userIds]);           
            
            // Assign the email of user field to Team Email field
            for (SObject so : trigger.new) {
                Sales_Team__c team = (Sales_Team__c) so;
                team.Team_Email__c = userMap.get(team.User__c).Email;
            }
        }
    }
    
    // Sync the Sales Team with OpportunityTeamMember
    public static void createOpportunityTeamMember() {
        if (trigger.isExecuting && trigger.isAfter && trigger.isInsert) {
            // Collections for used in updating OpportunityShare
            Set<Id> userIds = new Set<Id>();
            Set<Id> oppIds = new Set<Id>();
            Map<String, String> userIdOppId2AccessLevelMap = new Map<String, String>();
            
            List<OpportunityTeamMember> members = new List<OpportunityTeamMember>();
            List<OpportunityShare> shares = new List<OpportunityShare>();
            for (SObject so : trigger.new) {
                Sales_Team__c team = (Sales_Team__c) so;
                // Create the OpportunityTeamMember, because the OpportunityAccessLevel in OpportunityTeamMember 
                // is not writeable, so we need to create another OpportunityShare to achieve the share functionality
                OpportunityTeamMember member = new OpportunityTeamMember(UserId = team.User__c,
                                                                         TeamMemberRole = team.Team_Role__c,
                                                                         OpportunityId = team.Opportunity__c);

                members.add(member);
                
                // Populate the three collections for used in updating OpportunityShare
                userIds.add(team.User__c);
                oppIds.add(team.Opportunity__c);
                userIdOppId2AccessLevelMap.put(String.valueOf(team.User__c) + String.valueOf(team.Opportunity__c), 
                                               team.Opportunity_Access_Level__c);
            }
            
            // Create OpportunityTeamMembers
            try {
                insert members;
            }
            catch (DMLException e) {
            	String errorMessage = e.getDMLMessage(0);
            	if (errorMessage.contains('insufficient access rights on cross-reference id')) {
            		trigger.new[0].addError('You have no permission to add sales team.');
            	}
            	else {
            		trigger.new[0].addError(errorMessage);
            	}
                System.debug('Util.createOpportunityTeamMember DMLException: ' + e.getDMLMessage(0));
            }
            
            // Update the OpportunityAccessLevel of OpportunityShare record
            List<OpportunityShare> osList = [SELECT UserOrGroupId, RowCause, OpportunityId, OpportunityAccessLevel, Id
                                             FROM OpportunityShare
                                             WHERE UserOrGroupId IN :userIds
                                             AND OpportunityId IN :oppIds
                                             AND RowCause = 'Team'];
                                             
            System.debug(osList);
            for (OpportunityShare os : osList) {
                String accessLevel = userIdOppId2AccessLevelMap.get(String.valueOf(os.UserOrGroupId) + 
                                                                    String.valueOf(os.OpportunityId));
                accessLevel = (accessLevel == 'Read Only' ? 'Read' : 'Edit');
                os.OpportunityAccessLevel = accessLevel;
            }
            
            try {
                update osList;
            }
            catch (DMLException e) {
            	trigger.new[0].addError(e.getDMLMessage(0));
                System.debug('Util.createOpportunityTeamMember DMLException: ' + e.getDMLMessage(0));
            }

        }
    }

    // If the Sales Team Opportunity Access Level or Team Role is changed,
    //      sync the Sales Team with OpportunityTeamMember
    public static void updateOpportunityTeamMember() {
        if (trigger.isExecuting && trigger.isUpdate && trigger.isAfter) {
            // Get userIds and oppIds of changed Sales Team Records
            Set<Id> userIds = new Set<Id>();
            Set<Id> oppIds = new Set<Id>();
            Map<String, String> userIdOppId2AccessLevelMap = new Map<String, String>();
            for (SObject so : trigger.new) {
                Sales_Team__c newTeam = (Sales_Team__c) so;
                Sales_Team__c oldTeam = (Sales_Team__c) trigger.oldMap.get(so.Id);
                // Populate the three collections for used in updating OpportunityShare
                if (oldTeam.Opportunity_Access_Level__c != newTeam.Opportunity_Access_Level__c
                      || oldTeam.Team_Role__c != newTeam.Team_Role__c) {
                    userIds.add(newTeam.User__c);
                    oppIds.add(newTeam.Opportunity__c);
                    userIdOppId2AccessLevelMap.put(String.valueOf(newTeam.User__c) + String.valueOf(newTeam.Opportunity__c), 
                       newTeam.Opportunity_Access_Level__c);
                }
            }
            
            // Update the OpportunityAccessLevel of OpportunityShare record
            List<OpportunityShare> osList = [SELECT UserOrGroupId, RowCause, OpportunityId, OpportunityAccessLevel, Id
                                             FROM OpportunityShare
                                             WHERE UserOrGroupId IN :userIds
                                             AND OpportunityId IN :oppIds];
                                             
            // Update the list according to custom Sales Team
            for (OpportunityShare os : osList) {
                String accessLevel = userIdOppId2AccessLevelMap.get(String.valueOf(os.UserOrGroupId) + 
                                                                    String.valueOf(os.OpportunityId));
                if (accessLevel == 'Read Only') {
                    os.OpportunityAccessLevel = 'Read';
                }
                else if (accessLevel == 'Read/Write') {
                    os.OpportunityAccessLevel = 'Edit';
                }
            }
            
            try {
                update osList;
            }
            catch (DMLException e) {
            	String errorMessage = e.getDMLMessage(0);
            	if (errorMessage.contains('insufficient access rights on cross-reference id')) {
            		trigger.new[0].addError('You have no permission to update sales team.');
            	}
            	else {
            		trigger.new[0].addError(errorMessage);
            	}
                System.debug('Util.updateOpportunityTeamMember DMLException: ' + e.getDMLMessage(0));
            }
        }
    }
    
    // After deleting the Sales Team record, just delete the corresponding OpportunityTeamMember
    public static void deleteOpportunityTeamMember() {
        if (trigger.isExecuting && trigger.isAfter && trigger.isDelete) {
            // Collections for used in deleting OpportunityTeamMember
            Set<Id> userIds = new Set<Id>();
            Set<Id> oppIds = new Set<Id>();
            
            // Populate userIds and oppIds
            for (SObject so : trigger.old) {
                Sales_Team__c team = (Sales_Team__c) so;
                userIds.add(team.User__c);
                oppIds.add(team.Opportunity__c);
            }
            
            // Delete the OpportunityTeamMember according to the deleted Sales Team
            try {
                delete [SELECT Id FROM OpportunityTeamMember WHERE UserId IN :userIds AND OpportunityId IN :oppIds];
            }
            catch (DMLException e) {
                trigger.old[0].addError(e.getDMLMessage(0));
                System.debug('Util.deleteOpportunityTeamMember DMLException: ' + e.getDMLMessage(0));
            }
        }
    }
}