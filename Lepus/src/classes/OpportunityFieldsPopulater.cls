/**
 * @Purpose : 1 机会点归属及行业字段默认取自客户的相关属性，客户的相关属性修改时同步刷新机会点的相关属性；
 *            2 字段包括：区域、代表处、省份、城市、系统部、子系统部、客户群、客户群编码、TCG信息；
 *            3 机会点关闭后，以上属性不再发生变化；
 * @Author : Steven
 * @Date : 2014-08-20 Refactor
 */
public without sharing class OpportunityFieldsPopulater implements Triggers.Handler{ //before insert,before update
    public void handle(){
        Set<Id> accountIds = new Set<Id>();
        Set<Id> updateOppIds = new Set<Id>();
        //Step1: 记录所有涉及的机会点的客户ID
        for(Opportunity opp : (List<Opportunity>)Trigger.new){
            if(opp.recordtypeid == CONSTANTS.CHINAOPPORTUNITYRECORDTYPE || 
               opp.recordtypeid == CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE){
                //Step1-1: 如果是创建机会点，根据客户属性刷新机会点属性
                if(Trigger.isInsert){
                    accountIds.add(opp.accountid);
                    if(opp.IsClosed) opp.Opportunity_Closed_Date__c = system.today();
                }
                //Step1-2: 如果是更新机会点，客户没有发生变化则不进行更新
                if(Trigger.isUpdate){ 
                    Opportunity oldOpp = (Opportunity)Trigger.oldMap.get(opp.id);
                    //Step1-2-1: 机会点客户发生了变化，且机会点未关闭或者在本次操作中关闭
                    if(opp.accountid != oldOpp.accountid) {
                        if((!opp.IsClosed) || (opp.IsClosed && !oldOpp.IsClosed)) {
                            accountIds.add(opp.accountid);
                            updateOppIds.add(opp.Id);
                        }
                    }
                    //Step1-2-2: 机会点从关闭状态变成未关闭
                    if(!opp.IsClosed && oldOpp.IsClosed) {
                        accountIds.add(opp.accountid);
                        updateOppIds.add(opp.Id);
                    }
                    if(opp.IsClosed && !oldOpp.IsClosed) opp.Opportunity_Closed_Date__c = system.today();
                    if(!opp.IsClosed && oldOpp.IsClosed) opp.Opportunity_Closed_Date__c = null;
                }
            }
        }
        if(accountIds.size()==0) return;
        system.debug('-----OpportunityFieldsPopulater----');
        
        //Step2: 一次查询出所有涉及的机会点客户信息
        Map<id,Account> accountMap = new Map<id,Account>([
            select id,TCG_Name__c,Region_HW__c,Representative_Office__c,Country_HW__c,Country_Code_HW__c,Industry__c,
                         Customer_Group__c,Sub_industry_HW__c,Industry,City_Huawei_China__c,Customer_Group_Code__c,Province__c 
                from Account where id in: accountIds]);
                
        //Step3: 根据客户信息设置机会点的相关属性  
        for(Opportunity opp : (List<Opportunity>)Trigger.new){
            if(opp.recordtypeid == CONSTANTS.CHINAOPPORTUNITYRECORDTYPE || 
               opp.recordtypeid == CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE){
              //只处理新创建的机会点，或者非关闭状态的机会点修改了客户
                if((Trigger.isInsert) || (Trigger.isUpdate && updateOppIds.contains(opp.id))){ 
                    Account acc = accountMap.get(opp.accountid);
                    opp.Region__c = acc.Region_HW__c;
                    opp.Representative_Office__c = acc.Representative_Office__c;
                    opp.Country__c = acc.Country_HW__c;
                    opp.Country_Code__c = acc.Country_Code_HW__c;
                    opp.Industry__c = acc.Industry__c;
                    opp.Customer_Group__c = acc.Customer_Group__c;
                    opp.Sub_Industry__c = acc.Sub_industry_HW__c;
                    opp.System_Department__c = acc.Industry;
                    opp.Customer_Group_Code__c = acc.Customer_Group_Code__c;
                    opp.TCG_Name__c = acc.TCG_Name__c;
                    //代表处选择系统部总部，省份任然是各个省份，但是传到机会点的时候只传中国
                    if(acc.Representative_Office__c == '系统部总部') {
                        opp.Province__c = '中国';
                        opp.City__c = null;
                    } else {
                        opp.Province__c = acc.Province__c;
                        opp.City__c = acc.City_Huawei_China__c;
                    }
                }
            }
        }// end of step3
        
    } //end of handle function

}