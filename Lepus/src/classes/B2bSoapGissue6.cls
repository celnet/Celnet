//Generated by wsdl2apex

public class B2bSoapGissue6 {
    public class tSRStatusReply {
        public String FLAG;
        public String DESCRIPTION;
        public String AUX1;
        public String AUX2;
        public String AUX3;
        private String[] FLAG_type_info = new String[]{'FLAG','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR',null,'1','1','false'};
        private String[] DESCRIPTION_type_info = new String[]{'DESCRIPTION','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR',null,'0','1','false'};
        private String[] AUX1_type_info = new String[]{'AUX1','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR',null,'0','1','false'};
        private String[] AUX2_type_info = new String[]{'AUX2','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR',null,'0','1','false'};
        private String[] AUX3_type_info = new String[]{'AUX3','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR','false','false'};
        private String[] field_order_type_info = new String[]{'FLAG','DESCRIPTION','AUX1','AUX2','AUX3'};
    }
    public class RQ_element {
        public B2bSoapGissue6.tSRStatusGet[] SRStatusGet;
        private String[] SRStatusGet_type_info = new String[]{'SRStatusGet','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR',null,'1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR','false','false'};
        private String[] field_order_type_info = new String[]{'SRStatusGet'};
    }
    public class RS_element {
        public B2bSoapGissue6.tSRStatusReply[] SRStatusReply;
        private String[] SRStatusReply_type_info = new String[]{'SRStatusReply','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR',null,'1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR','false','false'};
        private String[] field_order_type_info = new String[]{'SRStatusReply'};
    }
    public class tSRStatusGet {
        public String SF_NUMBER;
        public String ICARE_NUMBER;
        public String Salesforce_SR_ID;
        public String RecordType_Developer_Name;
        public String Actual_Restore_Time;
        public String iCare_Group_Name;
        public String iCare_Owner_Employee_No;
        public String Prod_Offer_Code;
        public String AUX1;
        public String AUX2;
        public String AUX3;
        private String[] SF_NUMBER_type_info = new String[]{'SF_NUMBER','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR',null,'0','1','false'};
        private String[] ICARE_NUMBER_type_info = new String[]{'ICARE_NUMBER','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR',null,'0','1','false'};
        private String[] Salesforce_SR_ID_type_info = new String[]{'Salesforce_SR_ID','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR',null,'0','1','false'};
        private String[] RecordType_Developer_Name_type_info = new String[]{'RecordType_Developer_Name','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR',null,'0','1','false'};
        private String[] Actual_Restore_Time_type_info = new String[]{'Actual_Restore_Time','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR',null,'0','1','false'};
        private String[] iCare_Group_Name_type_info = new String[]{'iCare_Group_Name','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR',null,'0','1','false'};
        private String[] iCare_Owner_Employee_No_type_info = new String[]{'iCare_Owner_Employee_No','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR',null,'0','1','false'};
        private String[] Prod_Offer_Code_type_info = new String[]{'Prod_Offer_Code','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR',null,'0','1','false'};
        private String[] AUX1_type_info = new String[]{'AUX1','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR',null,'0','1','false'};
        private String[] AUX2_type_info = new String[]{'AUX2','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR',null,'0','1','false'};
        private String[] AUX3_type_info = new String[]{'AUX3','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR','false','false'};
        private String[] field_order_type_info = new String[]{'SF_NUMBER','ICARE_NUMBER','Salesforce_SR_ID','RecordType_Developer_Name','Actual_Restore_Time','iCare_Group_Name','iCare_Owner_Employee_No','Prod_Offer_Code','AUX1','AUX2','AUX3'};
    }
}