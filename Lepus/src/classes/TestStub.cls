@isTest
private class TestStub {

    static testMethod void testG() {
        B2bSoapGissue6.tSRStatusReply x = new B2bSoapGissue6.tSRStatusReply();
        B2bSoapGissue6.tSRStatusGet y = new B2bSoapGissue6.tSRStatusGet();
        B2bSoapGissue6.RQ_element e = new B2bSoapGissue6.RQ_element();
        B2bSoapGissue6.RS_element f = new B2bSoapGissue6.RS_element();
    }
    
    static testMethod void testS(){
    	SoapIssues6.IssueReq_x1_HTTPSPort x = new SoapIssues6.IssueReq_x1_HTTPSPort();
        SoapIssues6.RQ_element q = new SoapIssues6.RQ_element();
        SoapIssues6.tIssueRQ r = new SoapIssues6.tIssueRQ();
    }
    
    static testMethod void testSS(){
    	B2bSoapSissue6.tIssueRQ q = new B2bSoapSissue6.tIssueRQ();
        B2bSoapSissue6.tIssueRS s = new B2bSoapSissue6.tIssueRS();
    }
    
    static testMethod void testU(){
    	B2bSoapUissue6.tUpdateIcareSR r = new B2bSoapUissue6.tUpdateIcareSR();
        B2bSoapUissue6.RQ_element rq = new B2bSoapUissue6.RQ_element();
        B2bSoapUissue6.RS_element rs = new B2bSoapUissue6.RS_element();
        B2bSoapUissue6.tUpdateIcareSRReply rr = new B2bSoapUissue6.tUpdateIcareSRReply();
    }
}