public with sharing class TestTig_InstalledAssed {

    public static testMethod void myUnitTest() {
    	EdObject.currClsTest();
    	MathTool.currClsTest();
      Installed_Asset__c asset = new Installed_Asset__c(Barcode1__c = 'test' ,BOM_Code__c = '12345678',
      	SubRegion__c = 'test',ownerid = UserInfo.getUserId()); 	
      insert asset;
       	Installed_Asset__c i=[Select i.SystemModstamp, i.SubRegion__c, i.Street__c, 
       		i.Solution_Device__c, i.Shipping_Date__c, i.Region__c, i.Quantity__c, 
       		i.Product_Version1__c, i.Product_Name1__c, i.Product_Line__c, i.Product_Family__c, 
       		i.Owner.Id, i.OwnerId, i.Other_City__c, i.Name, i.LastModifiedDate, 
       		i.LastModifiedById, i.Install_Asset_Count__c, 
       		i.Id, i.Filed__c, i.Device__c, 
       		i.Contracts_No__c, i.Configuration_Information__c, i.Click_to_View__c, i.City__c, 
       		i.Barcode1__c, i.BOM_Description__c, i.BOM_Code__c, i.Account_Name__c 
       		From Installed_Asset__c i
       		Where i.Barcode1__c!=null and i.SubRegion__c!=null and i.OwnerId!=null
       		limit 1];
		Installed_Asset__c i0=new Installed_Asset__c();
		User u=[select id,name from User limit 1];
		System.debug('i.OwnerId=/////////////'+i.OwnerId+',或'+u.Id);
		i0=i.clone(false);
			i0.OwnerId=u.id;
       		i0.Name='Eeploy0011111122';    
       		i0.Barcode1__c='Eeploy0011111122'; 
       		i0.BOM_Code__c='03Eeploy'; 
       	try{
			insert(i0);
       	}catch(Exception e){
       		System.debug('////////////////////>:'+e);
       	}
		if(true)return;
		if(i!=null){
			i0.SubRegion__c=i.SubRegion__c; 
			i0.Street__c=i.Street__c; 
       		i0.Solution_Device__c=i.Solution_Device__c; 
       		i0.Shipping_Date__c=i.Shipping_Date__c; 
       		i0.Region__c=i.Region__c; 
       		i0.Quantity__c=i.Quantity__c; 
       		i0.Product_Version1__c=i.Product_Version1__c; 
       		i0.Product_Name1__c=i.Product_Name1__c; 
       		i0.Product_Line__c=i.Product_Line__c; 
       		i0.Product_Family__c=i.Product_Family__c; 
       		i0.Postal_Code__c=i.Postal_Code__c;  
       		i0.Other_City__c=i.Other_City__c; 
       		//i0.Install_Asset_Count__c=i.Install_Asset_Count__c;  
       		i0.Filed__c=i.Filed__c; 
       		i0.Device__c=i.Device__c;    
       		i0.Contracts_No__c=i.Contracts_No__c; 
       		i0.Configuration_Information__c=i.Configuration_Information__c; 
       		//i0.Click_to_View__c=i.Click_to_View__c; 
       		i0.City__c=i.City__c; 
       		i0.Name='Eeploy0011111122';    
       		i0.Barcode1__c='Eeploy0011111122'; 
       		i0.BOM_Code__c='03Eeploy'; 
       		i0.BOM_Description__c='ddeesscct'; 
       		i0.Account_Name__c=i.Account_Name__c;
       		insert(i0);
		}
    }
}