@isTest
private class Lepus_QueueServiceTest {
    static testmethod void myUnitTest(){
        Lepus_QueueService service = new Lepus_QueueService();
        Lepus_QueueManager manager = new Lepus_QueueManager();
        Account acc = Lepus_EIPCalloutServiceTest.createAccount('fdfa');
        insert acc;
        Lepus_Queue__c Queue = manager.initQueue('fdasfasf', acc.Id, 'insert', '业务数据同步', datetime.now());
        service.QueueHandle(Queue);
        service.syncTeamMember(Queue);
        service.fieldUpdateCallout('fadsfads', Queue);
        Opportunity opp = Lepus_EIPCalloutServiceTest.createOpportunity('djsafsd', acc.Id);
        insert opp;
        Lepus_Queue__c Queue2 = manager.initQueue('fdasfasf', opp.Id, 'insert', '字段更新同步', datetime.now());
        service.QueueHandle(Queue2);
    }
}