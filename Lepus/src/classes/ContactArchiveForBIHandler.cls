/**
 * @Purpose : 删除后将记录保存到数据维护历史对象上
 * @Author : Steven
 * @Date : 2014-06-09
 */
public with sharing class ContactArchiveForBIHandler implements Triggers.Handler{
    public static Boolean isFirstRun = true;
    public void handle(){
        if(ContactArchiveForBIHandler.isFirstRun){
            List<Data_Maintain_History__c> archiveList = new List<Data_Maintain_History__c>();
            for(Contact t : (List<Contact>)trigger.old){
                if(t.RecordTypeId == CONSTANTS.HUAWEIOVERSEACONTACTRECORDTYPE){
                    Data_Maintain_History__c dmh = new Data_Maintain_History__c();
                    dmh.Record_ID__c = t.Id;
                    dmh.Operation_Type__c = 'Delete';
                    dmh.Data_Type__c = 'Oversea Contact';
                    archiveList.add(dmh);
                }
                
                if(t.RecordTypeId == CONSTANTS.HUAWEICHINACONTACTRECORDTYPE){
                	Data_Maintain_History__c dmh = new Data_Maintain_History__c();
                    dmh.Record_ID__c = t.Id;
                    dmh.Operation_Type__c = 'Delete';
                    dmh.Data_Type__c = 'China Contact';
                    archiveList.add(dmh);
                }
                
            }
            if(archiveList.size() > 0){
                insert archiveList;
            }
            ContactArchiveForBIHandler.isFirstRun = false;
        }
    }
}