/**
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-8-29
 * Description: 定时从SyncFailRecord获取同步失败记录再次同步
 */

global class Lepus_FailureReSyncScheduleJob implements Schedulable, Database.Batchable<Sobject>, Database.AllowsCallouts{
    global void execute(SchedulableContext sc){
    	List<String> list_Status = new List<String>{'Queued','Processing','Preparing'}; 
    	
    	// 确保lepus的queuebatch和failureresyncbatch加起来只有一个实例在运行
    	if([Select Id From AsyncApexJob a Where Status in: list_Status And (ApexClass.Name = 'Lepus_QueueBatch' Or ApexClass.Name = 'Lepus_FailureReSyncScheduleJob') And JobType != 'ScheduledApex'].size() == 0){
    		Lepus_QueueBatch qb = new Lepus_QueueBatch();
			database.executeBatch(qb, 1);
    	} 
    	
    	Lepus_ReSync.reSync();
    	Lepus_AccountTeamMemberScheduleJob.syncAccountTeamMembers();
    }
     
    global Database.Querylocator start(Database.BatchableContext bc){
        // 获取同步失败记录
        return Database.getQueryLocator([Select Id, SyncType__c, SyncTime__c, SyncTimes__c, SyncStatus__c, SyncRecordID__c, SyncAction__c, 
                                                        User__c, Opportunity__c, Lead__c, Contact__c, Account__c, XML_String__c From SyncFailRecord__c 
                                                Where ReSync_Success__c = false ]);
    }
     
    global void execute(Database.BatchableContext bc, List<Sobject> scope){
        for(SyncFailRecord__c record : (List<SyncFailRecord__c>)scope){ 
            if(record.SyncRecordID__c == null)
            continue;
            
            if(record.SyncType__c == '业务数据同步'){
                if(record.SyncRecordID__c.startsWith(Account.sobjecttype.getDescribe().getKeyPrefix())){
                    this.reSyncBusinessData(record.SyncRecordID__c, record.SyncAction__c, 'Account', record.Id, record.SyncTime__c);
                } else if(record.SyncRecordID__c.startsWith(Contact.sobjecttype.getDescribe().getKeyPrefix())){
                    this.reSyncBusinessData(record.SyncRecordID__c, record.SyncAction__c, 'Contact', record.Id, record.SyncTime__c);
                } else if(record.SyncRecordID__c.startsWith(Lead.sobjecttype.getDescribe().getKeyPrefix())){
                    this.reSyncBusinessData(record.SyncRecordID__c, record.SyncAction__c, 'Lead', record.Id, record.SyncTime__c);
                } else if(record.SyncRecordID__c.startsWith(Opportunity.sobjecttype.getDescribe().getKeyPrefix())){
                    this.reSyncBusinessData(record.SyncRecordID__c, record.SyncAction__c, 'Opportunity', record.Id, record.SyncTime__c);
                }
            } else if(record.SyncType__c == '字段更新同步'){
                this.reSyncFieldUpdate(record.SyncRecordID__c, record.SyncTime__c, record.Id);
            } else if(record.SyncType__c == '团队成员同步'){
                List<Id> ids = record.SyncRecordID__c.split(',', -2);
                if(record.SyncRecordID__c.startsWith(Account.sobjecttype.getDescribe().getKeyPrefix())){
                    this.reSyncTeamMember(ids, Account.Sobjecttype, record.Id, record.SyncAction__c);
                } else if(record.SyncRecordID__c.startsWith(Lead.sobjecttype.getDescribe().getKeyPrefix())){
                    this.reSyncTeamMember(ids, Lead.Sobjecttype, record.Id, record.SyncAction__c);
                } else if(record.SyncRecordID__c.startsWith(Opportunity.sobjecttype.getDescribe().getKeyPrefix())){
                    this.reSyncTeamMember(ids, Opportunity.Sobjecttype, record.Id, record.SyncAction__c);
                }
            } else if(record.SyncType__c == '用户角色同步'){
                this.reSyncUser(record.SyncRecordID__c, record.Id, record.SyncTime__c);
            } else if(record.SyncType__c == '线索更新同步'){
                this.reSyncLeadHistory(record.XML_String__c, record.Id);
            }
        }
    }
    
    global void finish(Database.BatchableContext bc){
        List<String> list_Status = new List<String>{'Queued','Processing','Preparing'}; 
        boolean norunning = ([Select Id From AsyncApexJob a Where Status in: list_Status And ApexClass.Name = 'Lepus_QueueBatch' limit 1].size() == 0);
        
        //检查Lepus Queue中是否有未处理记录
		if(Lepus_QueueManager.HaveUntreated() && norunning){
			//启用新的Queue Batch
			Lepus_QueueBatch qb = new Lepus_QueueBatch();
			database.executeBatch(qb, 1);
		} 
    }
    
    private void reSyncBusinessData(String recordId, String action, String objType, Id failRecordId, Datetime syncFailTime){
        Schema.Sobjecttype sobjecttype = Lepus_SyncUtil.retrieveSobjectType(objType);
        Sobject sobj = Lepus_SyncUtil.querySobject(recordId, sobjecttype);
        
        if(sobj == null || (((Datetime)sobj.get('LastModifiedDate'))) > syncFailTime){
        	// 数据已更新并再次同步，标记此错误记录为删除
            update new SyncFailRecord__c(Id = failRecordId, ReSync_Success__c = true);
        	return;
        }
        
        Lepus_Log__c log = new Lepus_Log__c();
        log.UniqueId__c = ((String)sobj.get('Id') + String.valueOf(sobj.get('LastModifiedDate')));
        
        String xml;
        
        try{
            xml = Lepus_EIPCalloutService.DataSyncXmlConcatenation(sobj, objType, action);
            Lepus_WSDL_Info.tGetResponse response = Lepus_EIPCalloutService.sendToEIP(xml,'isaleslepus_lepusglobal_sync',(Id)sobj.get('Id'), 'CI0000194284');
            
            
            if(response != null && response.ResultStatus == 'true'){
                update new SyncFailRecord__c(Id = failRecordId, ReSync_Success__c = true);
                
                // 如果是插入操作并且是Account，Opportunity，Lead，需要同步团队成员数据
                if(action == 'insert' && (sobjecttype == Account.sobjecttype || sobjecttype == Opportunity.sobjecttype || sobjecttype == Lead.sobjecttype)){
                    Lepus_FutureCallout.syncTeamMemberOnly(new List<Id>{recordId}, objType, 'insert');
                }
                
                log.Sent_Success__c = true;
                
               	
                
            } else {
                // 再次同步出现异常
                Lepus_FailureHandler.handleReSyncFailure(failRecordId, response == null?'no response':response.Error);
                
                log.Sent_Success__c = false;
                log.Sent_Failure_Cause__c = (response == null?'no response':response.Error);
                
            }
        } catch(Exception e){
            // 再次同步出现异常
            Lepus_FailureHandler.handleReSyncFailure(failRecordId, e.getMessage());
            
            log.Sent_Success__c = false;
            log.Sent_Failure_Cause__c = e.getMessage();
        }
        
        log.BusinessData_XML__c = xml;
        upsert log UniqueId__c;
    }
    
    private void reSyncFieldUpdate(Id oppId, Datetime dTime, Id failRecordId){
        List<OpportunityFieldHistory> list_oppFieldHis = Lepus_SyncUtil.queryOppFieldHistories(oppId, dTime);
        if(list_oppFieldHis.size() > 0){
            String sXml = Lepus_EIPCalloutService.FieldUpdateXmlConcatenation(list_oppFieldHis);
            try{
                Lepus_WSDL_Info.tGetResponse response = Lepus_EIPCalloutService.sendToEIP(sXml, 'lepusmanage_field_update' , oppId , 'CI0000194284');
                if(response != null && response.ResultStatus == 'true'){
                    delete new SyncFailRecord__c(Id = failRecordId);
                }else {
                    Lepus_FailureHandler.handleReSyncFailure(failRecordId, response == null?'no response':response.Error);
                }
            } catch (Exception e){
                Lepus_FailureHandler.handleReSyncFailure(failRecordId, e.getMessage());
            }
        }
    }
    
    private void reSyncTeamMember(List<Id> recordIdList, Schema.sobjecttype sobjecttype, Id failRecordId, String action){
        try{
            // 错误记录中的所有user
            List<Sobject> sobjList = Lepus_SyncUtil.querySobjects(new Set<Id>(recordIdList), sobjecttype);
            Map<Id, List<Lepus_EIPMember>> memberMap = Lepus_SyncUtil.retrieveTeamMembers(sobjList, sobjecttype);
            String xml = Lepus_EIPCalloutService.memberXmlConcatenation(memberMap, sobjecttype, action);
            
            Lepus_WSDL_Info.tGetResponse response = Lepus_EIPCalloutService.sendToEIP(xml, 'mchat_bizdatasync', '', 'CI0000194284');
            if(response != null && response.ResultStatus == 'true'){
                // 同步成功，标记此错误记录为删除
                update new SyncFailRecord__c(Id = failRecordId, ReSync_Success__c = true);
            } else {
                // 再次同步失败
                Lepus_FailureHandler.handleReSyncFailure(failRecordId, response == null?'no response':response.Error);
            }
            
        } catch(Exception e){
            // 再次同步出现异常
            Lepus_FailureHandler.handleReSyncFailure(failRecordId, e.getMessage());
        }
    }
    
    private void reSyncUser(String recordIds, Id failRecordId, Datetime syncFailTime){
        try{
            // 错误记录中的所有user
            List<Id> userIds = recordIds.split(',', -2);
            List<User> userList = Lepus_SyncUtil.querySobjects(new Set<Id>(userIds), User.sobjecttype);
            
            // 错误记录中的最近一小时未更新user需要再次同步，其余不同步
            List<User> reSyncUserList = new List<user>();
            for(User u : userList){
            if(u.LastModifiedDate < syncFailTime){
                    reSyncUserList.add(u);
                }
            }
            
            if(reSyncUserList.size() > 0){
                String xml = Lepus_EIPCalloutService.UserSyncXMLConcatenation(reSyncUserList, 'insert');
                Lepus_WSDL_Info.tGetResponse response = Lepus_EIPCalloutService.sendToEIP(xml, 'salesforcelepus_userrole_sync', '', 'CI0000194284');
                
                if(response != null && response.ResultStatus == 'true'){
                    // 同步成功，标记此错误记录为删除
                    update new SyncFailRecord__c(Id = failRecordId, ReSync_Success__c = true);
                } else {
                    // 再次同步失败
                    Lepus_FailureHandler.handleReSyncFailure(failRecordId, response == null?'no response':response.Error);
                }
            } else {
                // 错误记录中的user都已经被更新并再次同步，标记错误记录为删除
                update new SyncFailRecord__c(Id = failRecordId, ReSync_Success__c = true);
            }
        } catch(Exception e){
            // 再次同步出现异常
            Lepus_FailureHandler.handleReSyncFailure(failRecordId, e.getMessage());
        }
    }
    
    private void reSyncLeadHistory(String xml, Id failRecordId){
        try{
            Lepus_WSDL_Info.tGetResponse response = Lepus_EIPCalloutService.sendToEIP(xml, 'salesforcelepus_userrole_sync', '', 'CI0000194284');
            if(response!=null && response.ResultStatus == 'true'){
                update new SyncFailRecord__c(Id = failRecordId, ReSync_Success__c = true);
            } else {
                Lepus_FailureHandler.handleReSyncFailure(failRecordId, response == null?'no response':response.Error);
            }
        } catch(Exception e){
            Lepus_FailureHandler.handleReSyncFailure(failRecordId, e.getMessage());
        }
    }
}