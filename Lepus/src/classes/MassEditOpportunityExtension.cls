/* 
Author: tommy.liu@celnet.com.cn
Created On: 2014-5-12
Function: Mass edit Opp
*/
public with sharing class MassEditOpportunityExtension 
{
	public class OppRow
	{
		public Opportunity Opp {get; set;}
		public UserRecordAccess Access {get; set;}
		public Boolean Editable
		{
			get
			{
				if(this.Access != null && this.Access.HasEditAccess)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		public Boolean ReadOnly 
		{
			get
			{
				return !this.Editable;
			}
		}
	}
	
	public class Field
	{
		private MassEditOpportunityExtension function;
		public Schema.FieldSetMember FieldSetMember {get; set;}
		public Easy_Mass_Field_Attributes__c FieldAttributes 
		{
			get
			{
				if(FieldSetMember == null)
				{
					return null;
				}
				return this.function.FieldAttrMap.get(FieldSetMember.FieldPath);
			}
		}
		
		public Field(MassEditOpportunityExtension function)
		{
			this.function = function;
		}
	}
	
	private ApexPages.StandardSetController setController;
	private List<OppRow> selectedOppRows;
	private List<Opportunity> editableOpps;
	private Set<String> fieldNameSet;
	private Map<String, Schema.FieldSetMember> additionFieldSetMemberMap;
	public String SelectedFieldStr {get; set;}
	public Boolean HasMessage{get; set;}//表示是否在页面上显示Message区域,前端JS根据此计算调整列表的高度
	public Map<String, Easy_Mass_Field_Attributes__c> FieldAttrMap;
	
	
	public MassEditOpportunityExtension(ApexPages.StandardSetController setController) 
	{
		this.HasMessage = false;
    this.setController = setController;
    this.loadFieldAttribues();
    this.initAdditionFieldSetMemberMap();
   	this.initFieldNameSet();
    this.initSelectedOppRows();
    this.showBeforeMessage();
	}
	
	private void initAdditionFieldSetMemberMap()
	{
		this.additionFieldSetMemberMap = new Map<String, Schema.FieldSetMember>();
    for(Schema.FieldSetMember field: SObjectType.Opportunity.FieldSets.Mass_Edit_Addition.getFields())
    {
    	additionFieldSetMemberMap.put(field.FieldPath, field);
    }
    for(Schema.FieldSetMember field: SObjectType.Opportunity.FieldSets.Mass_Edit_Addition2.getFields())
    {
    	additionFieldSetMemberMap.put(field.FieldPath, field);
    }
	}
	
	private void loadFieldAttribues()
	{
		this.fieldAttrMap = new Map<String, Easy_Mass_Field_Attributes__c>();
		for(Easy_Mass_Field_Attributes__c att : [Select ID, Name, Field__c, Function__c, Required__c From Easy_Mass_Field_Attributes__c Where Function__c = 'MassEditOpportunity'])
		{
			fieldAttrMap.put(att.Field__c, att);
		}
	}

  private void showBeforeMessage()
	{
		Integer totel = this.selectedOppRows.size();
		if(totel == 0)
		{
			ApexPages.Message wMsg = new ApexPages.Message(ApexPages.Severity.WARNING , '您没有选择任何记录, 请回到列表视图中选择记录');
			ApexPages.addMessage(wMsg);
			this.HasMessage = true;
		}
	}
	
	private void initFieldNameSet()
	{
		this.fieldNameSet = new Set<String>();//用Set避免字段重复出现在Select列表中
		this.fieldNameSet.add('Name');
		for(Schema.FieldSetMember field : SObjectType.Opportunity.FieldSets.Mass_Edit_default.getFields())
		{
			this.fieldNameSet.add(field.FieldPath);
		}
		for(Schema.FieldSetMember field : SObjectType.Opportunity.FieldSets.Mass_Edit_Addition.getFields())
		{
			this.fieldNameSet.add(field.FieldPath);
		}
		for(Schema.FieldSetMember field : SObjectType.Opportunity.FieldSets.Mass_Edit_Addition2.getFields())
		{
			this.fieldNameSet.add(field.FieldPath);
		}
	}
	
	//修复错误: SObject row was retrieved via SOQL without querying the requested field
	private void initSelectedOppRows()
	{
		String soql = 'Select Id';
		for(String fieldName : fieldNameSet)
		{
			soql += ', ' + fieldName;
		}
		soql += ' FROM Opportunity Where ID In: selectedIds';
		
		SObject[] selectedObjs = setController.getSelected();
		List<ID> selectedIds = new List<ID>();
		for(Integer i = 0; i < selectedObjs.size(); i++ )
		{
			selectedIds.add(selectedObjs[i].Id);
		}
		SObject[] selObjs = Database.query(soql);
		Map<ID, UserRecordAccess> recordAccessMap = new Map<ID, UserRecordAccess>();
		for(UserRecordAccess access : [SELECT
																			RecordId, 
																			HasReadAccess, 
																				HasEditAccess 
																		FROM UserRecordAccess 
																		WHERE UserId =: UserInfo.getUserId()
																		AND RecordId In: selectedIds])
			{
				recordAccessMap.put(access.RecordId, access);
			}
			
			this.selectedOppRows = new List<OppRow>();
			for(Sobject selObj : selObjs)
			{
				OppRow row = new OppRow();
				row.Opp = (Opportunity)selObj;
				row.Access = recordAccessMap.get(selObj.Id);
				this.selectedOppRows.add(row);
			}
			
			this.editableOpps = new List<Opportunity>();
			for(OppRow row: this.selectedOppRows)
			{
				if(row.Editable)
				{
					this.EditableOpps.add(row.Opp);
				}
			}
			this.setController.setSelected(this.editableOpps);//利用标准控制器保存记录,避免写DML
	}
	
	public List<OppRow> getSelectedOppRows()
	{
		return this.selectedOppRows;
	}
	
	public List<Field> getDefaultFields()
	{
		List<Field> fList = new List<Field>();
		for(Schema.FieldSetMember fieldM : SObjectType.Opportunity.FieldSets.Mass_Edit_Default.getFields())
		{
	    Field f = new Field(this);
	    f.FieldSetMember = fieldM;
	    fList.add(f);
		}
		return fList;
	}
	
	public List<Field> getSelectedFields()
	{
		List<Field> selectedFields = new List<Field>();
		if(this.SelectedFieldStr != null)
		{
			String[] selFieldNames = this.SelectedFieldStr.split(',', 0);
			for(String fName: selFieldNames)
			{
				if(fName != '')
				{
					Schema.FieldSetMember fielSetMb = this.additionFieldSetMemberMap.get(fName);
					if(fielSetMb != null)
					{
						Field f = new Field(this);
						f.FieldSetMember = fielSetMb;
						selectedFields.add(f);
					}
				}
			}
		}
		return selectedFields;
	}
	
	public PageReference setSelectedFields()
	{
		return null;
	}
	
	public List<SelectOption> getAdditionFieldOptions()
	{
		List<SelectOption> options = new List<SelectOption>();
		for(Schema.FieldSetMember field: SObjectType.Opportunity.FieldSets.Mass_Edit_Addition.getFields())
		{
	    options.add(new SelectOption(field.FieldPath,field.Label));
		}
		return options;
	}
	
	public List<SelectOption> getAddition2FieldOptions()
	{
		List<SelectOption> options = new List<SelectOption>();
		for(Schema.FieldSetMember field: SObjectType.Opportunity.FieldSets.Mass_Edit_Addition2.getFields())
		{
	    options.add(new SelectOption(field.FieldPath,field.Label));
		}
		return options;
	}
	
	public Integer getOptionSize()
	{
		return SObjectType.Opportunity.FieldSets.Mass_Edit_Addition.getFields().size();
	}
	public Integer getOption2Size()
	{
		return SObjectType.Opportunity.FieldSets.Mass_Edit_Addition2.getFields().size();
	}
}