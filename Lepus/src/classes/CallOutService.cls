/**
 * @Purpose : 发送消息到EIP接口(测试)
 * @Author : 孙达
 * @Date : 2014-07-29
 **/
public class CallOutService {
	//发送联系人信息
	//参数：联系人ID ; 操作(create、update、delete)
	//@future (callout=true)
	public static void sendContactInfo(ID contactId , String sAction){
		//Step1: 准备数据
		//查询联系人信息，此处仅查询少量字段作为测试内容
		Contact oContact = [Select Id,Name,AccountId,Birthdate,Gender__c,Phone,Email,LastModifiedDate 
				From Contact Where Id =: contactId];
		//将联系人信息转换为JSON格式字符串
		String sContactJson = JSON.serialize(oContact);
		system.debug('Contact Json Info:'+sContactJson);
		//拼接XML
		String xml = UtilCallOut.DataSyncXmlConcatenation(sContactJson , 'Contact' , 
				oContact.LastModifiedDate.format('MM/dd/yyyy HH:mm:ss'), String.valueOf(oContact.Id));
		system.debug('xml info:'+xml);
		//Step2:将信息发送给EIP接口
		sendToEIP(xml);
	}
	//发送联系人信息
	//参数：联系人ID
	//@future (callout=true)
	public static void sendLeadInfo(ID leadId){
		//Step1: 准备数据
		//查询联系人信息，此处仅查询少量字段作为测试内容
		Lead oLead = [Select ID,AnnualRevenue,Campaign__c,Company,CreatedById,Description,Email,Fax,HasOptedOutOfEmail,
				Industry,LastModifiedById,LastModifiedDate,CurrencyIsoCode,OwnerId,RecordTypeId,LeadSource,
				Status,MobilePhone,Name,NumberOfEmployees,Phone,Rating,Title,Website,Country__c,Customer_Group__c,
				Expected_Project_Date__c,HW_China_Lead__c,Inactive__c,Interested_Product__c,Potential_Revenue__c,
				Purchase_Objective__c,Representative_Office__c,Region_HW__c,Remark__c,Sub_industry__c,
				To_Be_Deleted__c,China_Representative_Office__c,China_Province__c,China_City__c,HasOptedOutOfFax,
				China_Lead_Source_Dept__c,China_Lead_Activity_Name__c,China_Lead_Customer_Group__c,
				China_Project_Name__c,China_Participate_Space__c,China_Main_Product__c,China_Project_Description__c,
				Account_Name__c,China_Opportunity__c,Follow_Up_Type__c,China_Lead_Invalid_Reason__c 
				From Lead Where Id =: leadId];
		//将联系人信息转换为JSON格式字符串
		String sContactJson = JSON.serialize(oLead);
		system.debug('Lead Json Info:'+sContactJson);
		//拼接XML
		String xml = UtilCallOut.DataSyncXmlConcatenation(sContactJson , 'Lead' , 
				oLead.LastModifiedDate.format('MM/dd/yyyy HH:mm:ss'), String.valueOf(oLead.Id));
		system.debug('Lead xml info:'+xml);
		//Step2:将信息发送给EIP接口
		sendToEIP(xml);
	}
	
	private static void sendToEIP(String sXml){
		Http h = new Http();
		Httprequest hReq = new Httprequest();
		Httpresponse hRes;
		hReq.setBody(sXml);
		hReq.setEndpoint('');
		hReq.setMethod('GET');
		hReq.setTimeout(100);
		try{
			hRes = h.send(hReq);
		}catch(Exception e){
			//Step3 结果处理
			//todo
			system.debug('send fail,message:'+e.getMessage());
		}
	}
	
}