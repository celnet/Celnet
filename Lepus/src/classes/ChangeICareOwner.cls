/*
* @Purpose: Hard save to database as well as inluded null values, not matter what values has been updated by backgroud processes
*
* @Author: Tommy Liu(tommyliu@celnet.com.cn)
* @Date: 2014-2-25
*/
public class ChangeICareOwner 
{
	private final ApexPages.StandardController stdController;
	
	public ChangeICareOwner(ApexPages.StandardController stdController) 
	{
		this.stdController = stdController;
	}
	
	public System.PageReference save()//override "save" action of std controller
	{
		System.PageReference pf = null;
		try
		{
			//强制提交这些值，即使 为空也要强制覆盖到数据库
			Case cs = (Case)this.stdController.getRecord();
			cs.iCare_Owner__c = cs.iCare_Owner__c;
			cs.Employee_ID__c = cs.Employee_ID__c;
			cs.iCare_Group__c = cs.iCare_Group__c;
			cs.Subject = cs.Subject;
			pf = this.stdController.save();
		}
		catch(DmlException ex)
		{
    	ApexPages.addMessages(ex);
    	pf = null;
    }
    return pf;
	}
}