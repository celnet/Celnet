@isTest
private class Lepus_LeadHandlerTest {
	static testmethod void myUnitTest(){
		User u = [Select Id From User Where UserType = 'Standard' and isactive = true limit 1];
		
		Lepus_EIPCalloutServiceTest.setCustomSettings();
		
		Lead l = new Lead();
		l.LastName = 'xxx';
		l.Company = 'xfdfd';
		l.RecordTypeId = CONSTANTS.OVERSEALEADRECORDTYPE;
		l.Status = 'Follow-up';
		l.OwnerId = UserInfo.getUserId();
		insert l;
		
		Lepus_LeadHandler.isFirstRun = true;
		l.OwnerId = u.Id;
		update l;
	
	}
	
	static testmethod void deleteTest(){
		
		Lead l = new Lead();
		l.LastName = 'xxx';
		l.Company = 'xfdfd';
		l.RecordTypeId = CONSTANTS.OVERSEALEADRECORDTYPE;
		l.Status = 'Follow-up';
		l.OwnerId = UserInfo.getUserId();
		
		Lepus_LeadHandler.isFirstRun = false;
		insert l;
		
		Lepus_EIPCalloutServiceTest.setCustomSettings();
		
		Lepus_LeadHandler.isFirstRun = true;
		delete l;
	}
}