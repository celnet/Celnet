public with sharing class MassEditSRSurvey {
    
    string Language = UserInfo.getLanguage();
    Case c = new Case();
    List<Service_Request_Survey__c> srses = new List<Service_Request_Survey__c>();
    private String cid { get; set; }
    String msg = null;
    public String getPath(){
        return '/' + cid;
    }
    PageReference pr = null;
    
    public MassEditSRSurvey(ApexPages.StandardController controller) {
        cid= ApexPages.currentPage().getParameters().get('id');
        if ( ApexPages.currentPage().getParameters().get( 'act' ) == 'new' ){
            createCaseSurvey();
            if ( msg != null ){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, msg));
            }
        }
        else {
            srses = (List<Service_Request_Survey__c>)[Select s.Type__c, s.Suggestion__c, s.Satisfactory_Level__c
                                                      , s.SR_No__c, s.Name, s.Id, Description__c
                                                      From Service_Request_Survey__c s
                                                      WHERE s.SR_No__c =: cid];
            //controller.setSelected(srses);
            c = [SELECT id FROM Case WHERE id =: cid]; 
        
            List<String> sorts = new List<String>();
            Map<String, Service_Request_Survey__c> isrsMap = new Map<String, Service_Request_Survey__c>();
            //for(Integer i=0;i<srses.size();i++){
            for(Service_Request_Survey__c s : srses){
                //Service_Request_Survey__c s=srses.get(i);
                String t = s.Type__c;
                if(t.indexOf('.') == 1)t='0'+t;
                //sorts.add(s.Type__c);
                sorts.add(t);
                //isrsMap.put(s.Type__c,s);
                isrsMap.put(t,s);
            }
            
            sorts.sort();
            srses.clear();
            for(String str : sorts){
                //if(str.substring(0,1)=='0')str=str.substring(1);
                srses.add(isrsMap.get(str));
            }
        }
    }
    
    public void createCaseSurvey(){
        Case c = new Case();
        try{
            c = [ SELECT Id, Status
                  FROM Case
                  WHERE Id =: cid ];
        }
        catch(Exception e){
            msg = Label.Warn_Service_Request_Does_Not_Exist;
            return;
        }
        List<Service_Request_Survey__c> t = [ SELECT s.SR_No__c, s.Id
                                              FROM Service_Request_Survey__c s
                                              WHERE s.SR_No__c =: c.Id ];
        System.debug( '\nSize of SRSurvey' + t.size() );
        if( t != null && t.size() > 0){
            msg = Label.Warn_Surveys_Already_Created;
            return;
        }
        
        List<Service_Request_Survey__c> customerSuryList = new List<Service_Request_Survey__c>();
        List<Service_Request_Survey__c> customerSuryList_2 = new List<Service_Request_Survey__c>();
        Service_Request_Survey__c cs = null;
        cs = new Service_Request_Survey__c( SR_No__c = c.Id
                                            ,Type__c = '1.Response time'
                                            ,Description__c = '1.How do you feel about our '
                                                              + 'engineer\'s response time?');
        customerSuryList.add( cs );
        cs = new Service_Request_Survey__c( SR_No__c = c.Id
                                            ,Type__c = '2.Technical level'
                                            ,Description__c = '2.How do you feel about our '
                                                              + 'engineer\'s technical skill?');
        customerSuryList.add( cs );
        cs = new Service_Request_Survey__c( SR_No__c = c.Id
                                            ,Type__c = '3.Service attitude'
                                            ,Description__c = '3.How do you feel about our '
                                                              + 'engineer\'s service?');
        customerSuryList.add( cs ); 
        cs = new Service_Request_Survey__c( SR_No__c = c.Id
                                            ,Type__c = '4.Suggestion'
                                            ,Description__c = '4.What else should we improve about '
                                                              + 'our service do you think?'
                                            ,Suggestion__c = ' ' );
        customerSuryList.add( cs );
        cs = new Service_Request_Survey__c( SR_No__c = c.Id
                                            ,Type__c = '5.Product Survey'
                                            ,Description__c = '5.How do you feel about our product?'
                                            + '(Such as operability, stability and maintainability)');
        customerSuryList.add( cs );
            
        if( customerSuryList != null && customerSuryList.size() > 0 ){
            try{
                srses = customerSuryList;
            }
            catch( Exception e ){
                msg = Label.Warn_Invalid_Data_for_Survey;
            }
        }
    }


    public List<Service_Request_Survey__c> getNewselected(){
        return srses;
    }
    
    public PageReference save() {
        if ( ApexPages.currentPage().getParameters().get( 'act' ) == 'new' ){
            insert srses;
        }
        else {
            update srses;
        }
        PageReference pr = null;
        pr = new PageReference( getPath() );
        pr.setRedirect( true );
        return pr;
        //return ApexPages.currentPage();
        //return (new ApexPages.StandardController(c)).view();
    }
    
    public PageReference cancel() {
       //return (new ApexPages.StandardController(c)).view();
        PageReference pr = null;
        pr = new PageReference( getPath() );
        pr.setRedirect( true );
        return pr;
    }

     public static String getRecordTypeIdByName(String sObjectType, String recordTypeName){
        Schema.SobjectType d = Schema.getGlobalDescribe().get(sObjectType);
        Map<String, Schema.RecordTypeInfo> rtMapByName = d.getDescribe().getRecordTypeInfosByName();
        Schema.RecordTypeInfo rtByName = rtMapByName.get(recordTypeName);
        return rtByName.getRecordTypeId();
     }

    public static testMethod void currClsTest(){
        //New account for test
        String recordtypeId = getRecordTypeIdByName('Account', 'Global Customer');
        Account acc = new Account(Name = 'Test Account',CurrencyIsoCode ='USD',
                                 Is_Ultimate_Parent__c = true,Global_Account_Manager__c = '005900000016UWc',
                                 Region__c ='India',Industry = 'Transportation',
                                 RecordTypeId = recordtypeId,
                                 blacklist_type__c='Pending'
                                 );       
        insert acc;
        
        //New contact for test
        recordtypeId = getRecordTypeIdByName('Contact', 'HW-(Int CSS) Contact');
        Contact con = new Contact(LastName = 'TEST Contact',
                                  AccountId =acc.Id,                                  
                                  MailingState='china',
                                  MailingCity='shanghai',
                                  MailingStreet='caoyang road',
                                  MailingPostalCode ='200000',
                                  Phone='14789714199',
                                  RecordTypeId =recordtypeId
                                  );
        insert con;
        
        //New service request for test
        recordtypeId = getRecordTypeIdByName('Case', 'HW-(Int CSS) External Post Sales-CCR');
        Case cas = UtilUnitTest.createChinaServiceRequest();
        cas.Status='Waiting for response';
        cas.AccountId=acc.Id;
        cas.Customer_Type__c='Huawei';
        cas.ContactId=con.Id;
        update cas;
        
        ApexPages.currentPage().getParameters().put('id', cas.Id);
        MassEditSRSurvey mes=new MassEditSRSurvey(null);
        mes.getNewselected();
        mes.save();
        mes.cancel();
        
        ApexPages.currentPage().getParameters().put('id', cas.Id);
        ApexPages.currentPage().getParameters().put('act', 'new');
        MassEditSRSurvey mes2=new MassEditSRSurvey(null);
        mes2.getNewselected();
        mes2.save();
        mes2.cancel();
    }
}