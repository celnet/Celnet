/**
 * update the account's na related fields after the record insert
 * Author           : BLS
 * Create Date      : 2013/3
 *     
 */ 
public without sharing class NamedAccountAfterInsertHandler implements Triggers.Handler {
	public static Boolean isFirstRun = true;
	public void handle() {
		if(NamedAccountAfterInsertHandler.isFirstRun){
		        //客户的Id集合
		        Set<Id> AccountIds = new Set<Id>();
		        for(Named_Account__c na : (List<Named_Account__c>)trigger.new) {
		            AccountIds.Add(na.Account_Name__c);
		        }
		        //客户List
		        Map<Id, Account> accountMap = new Map<Id, Account>([Select Id, ParentId, Is_Named_Account__c,
		        	Named_Account_Character__c,Is_403_Customer__c,
		        	Named_Account_Level__c,Named_Account_Property__c,NA_Approved_Date__c 
		        	from Account where Id in:AccountIds]);
		
		        for(Named_Account__c na : (List<Named_Account__c>)trigger.new) {
		        	accountMap.get(na.Account_Name__c).Named_Account__c = na.id;
		        	accountMap.get(na.Account_Name__c).Is_Named_Account__c = na.Is_Named_Account__c;
		        	accountMap.get(na.Account_Name__c).Named_Account_Character__c = na.Named_Account_Character__c;
		        	accountMap.get(na.Account_Name__c).Named_Account_Level__c = na.Named_Account_Level__c;
		        	accountMap.get(na.Account_Name__c).Named_Account_Property__c = na.Named_Account_Property__c;
		        	accountMap.get(na.Account_Name__c).NA_Approved_Date__c = na.Approved_Date__c;
		        	accountMap.get(na.Account_Name__c).Is_403_Customer__c = na.Is_403_Customer__c; 
		        }
		        update accountMap.values();
		      }
			//NamedAccountAfterInsertHandler.isFirstRun = false;
		}
}