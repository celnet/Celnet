public with sharing class LeadExtension {
        public Lead naLead {get;set;}
        public String status {get;set;}
        public Boolean isAccountManager {get;set;}
        public String companyName {get;set;}
        
        public LeadExtension(ApexPages.StandardController controller){
                Id naLeadRecordTypeId = CONSTANTS.NALEADRECORDTYPE;
                
                naLead = new Lead(recordtypeid = naLeadRecordTypeId);
                
                isAccountManager = false;
                //客户经理创建的NA线索“确认有效”字段默认为“是”，该字段值不可修改；
                Profile currentUserProfile = [select id,Name from Profile where id = :Userinfo.getProfileId()];
                if(currentUserProfile.name.contains('客户经理')){
                        naLead.NA_Lead_Confirmation__c = '确认有效';
                        naLead.Confirm_Date__c = DateTime.now();//直接初始化确认日期
                        isAccountManager = true;
                }
                if(currentUserProfile.name.contains('MKT')){
                        naLead.LeadSource = '展会';
                }
                
                
                if(Apexpages.currentPage().getParameters().get('accid') != null){
                        naLead.Account_Name__c = Apexpages.currentPage().getParameters().get('accid');
                        Account acc = [select id,name,Region_HW__c,Sub_Region__c,Representative_Office__c,
                                Industry,Sub_industry_HW__c,Customer_Group__c
                                from Account where id = :naLead.Account_Name__c];
                        naLead.Company = acc.name;
                        companyName = acc.name;
                }
                
                if(Apexpages.currentPage().getParameters().get('retURL') != null){
                        try{
                        naLead.Account_Name__c = Apexpages.currentPage().getParameters().get('retURL').substring(1,16);
                                Account acc = [select id,name,Region_HW__c,Sub_Region__c,Representative_Office__c,
                                        Industry,Sub_industry_HW__c,Customer_Group__c
                                        from Account where id = :naLead.Account_Name__c];
                                naLead.Company = acc.name;
                                companyName = acc.name;
                        }catch(Exception e){}
                }
        }
        
        public PageReference save(){
                if(naLead.Account_Name__c != null){
                        
                        Account acc = [select id,name,Region_HW__c,Sub_Region__c,Representative_Office__c,
                                Industry,Sub_industry_HW__c,Customer_Group__c
                                from Account where id = :naLead.Account_Name__c];
                        naLead.Company = acc.name;
                }
                /*
                nalead.Region_HW__c = acc.Region_HW__c;
                naLead.Country__c = acc.Representative_Office__c;
                naLead.Industry = acc.Industry;
                naLead.Sub_industry__c = acc.Sub_industry_HW__c;
                naLead.Customer_Group__c = acc.Customer_Group__c;
                */
                
                List<ApexPages.Message> messages = this.validation();
                if(messages.size() > 0){
                        for(ApexPages.Message m : messages){
                                ApexPages.addMessage(m);
                        }
                        return null;
                }
                try{
                        insert naLead;
                }catch(Exception e){
                        ApexPages.addMessages(e);
                        return null;
                }
                
                return new ApexPages.StandardController(naLead).view();
        }
        
        public PageReference savenew(){
                PageReference p = save();
                String url = Apexpages.currentPage().getUrl();
                p = new PageReference(url);
                p.setRedirect(true);
                return p;
        }
        
        private List<ApexPages.Message> validation(){
                System.debug('enter the validation method-----------------');
                List<ApexPages.Message> messages = new List<ApexPages.Message>();
                /*
                if(naLead.Account_Name__c == null){
                        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING,'请选择NA客户');
                }
                if(naLead.NA_Lead_Name__c == null){
                        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING,'请输入线索名称');
                }
                if(naLead.lastname == null){
                        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING,'请输入联系人姓名');
                }
                */
                
                return messages;
        }
}