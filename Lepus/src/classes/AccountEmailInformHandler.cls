/*
* @Purpose : it is the trigger handler for Sending the email to the Opps or case or contract owner when
* the Account falls in the blacklist and locked or not, or when the Account is removed from the blacklist.
*
* @Author : Jen Peng
* @Date : 		2013-5-20
*/
public without sharing class AccountEmailInformHandler implements Triggers.Handler{
	public static Boolean isFirstRun = true;
	public void handle() {
		system.debug('isFirstRun============================='+AccountEmailInformHandler.isFirstRun);
		
		if(AccountEmailInformHandler.isFirstRun){
			system.debug('isFirstRun============================='+AccountEmailInformHandler.isFirstRun);
			Set<Id> accountLockedIds = new Set<Id>();
			Set<Id> accountUnLockedIds = new Set<Id>();
			for(Account acc : (List<Account>)trigger.new){
				if(acc.blacklist_lock__c != ((Account)trigger.oldMap.get(acc.id)).blacklist_lock__c&&acc.blacklist_lock__c==true){//account被锁或者被解锁时
					accountLockedIds.add(acc.id);
					
				}else if(acc.blacklist_lock__c != ((Account)trigger.oldMap.get(acc.id)).blacklist_lock__c&&acc.blacklist_lock__c==false){
					accountUnLockedIds.add(acc.id);
				}else if(acc.blacklist_lock__c==false
						&& acc.blacklist_type__c!=((Account)trigger.oldMap.get(acc.id)).blacklist_type__c
						&& !(acc.blacklist_type__c=='Pass'&&((Account)trigger.oldMap.get(acc.id)).blacklist_type__c=='Pending')
						&& !(acc.blacklist_type__c=='Pending'&&((Account)trigger.oldMap.get(acc.id)).blacklist_type__c=='Pass')
						&& !(acc.blacklist_type__c==''&& acc.Blacklist_Status__c=='')){
					accountUnLockedIds.add(acc.id);
				}
			}
			//system.debug('accountLocked is here==================='+accountLockedIds);
			//system.debug('accountUnLockedIds is here==================='+accountUnLockedIds);
	
			if(!accountLockedIds.isEmpty()){
				EmailOppsOwnersBatch eo = new EmailOppsOwnersBatch();
				eo.isLocked=true;
				eo.accountids=accountLockedIds;
				String con='';
				for(Id i:accountLockedIds){
					con+='\''+i+'\',';
				}
				con = '('+con.substring(0,con.length()-1)+')';
				eo.query='Select OwnerId,AccountId From Opportunity where AccountId in '+con;
				Id batchId = Database.executeBatch(eo,200);
				//AccountEmailInformHandler.emailInform(accountLockedIds, true);
			}
			
			if(!accountUnLockedIds.isEmpty()){
				EmailOppsOwnersBatch eo = new EmailOppsOwnersBatch();
				eo.isLocked=false;
				eo.accountids=accountUnLockedIds;
				String con='';
				for(Id i:accountUnLockedIds){
					con+='\''+i+'\',';
				}
				con = '('+con.substring(0,con.length()-1)+')';
				eo.query='Select OwnerId,AccountId From Opportunity where AccountId in '+con;
				Id batchId = Database.executeBatch(eo,200);
				//AccountEmailInformHandler.emailInform(accountUnLockedIds, false);
			}
		}
		
	}

}