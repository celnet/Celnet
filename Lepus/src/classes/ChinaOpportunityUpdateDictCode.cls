/**
 * @Purpose : （中国区）机会点PRM集成需要将机会点的9个字段翻译成对应的CODE
 *            涉及：Huawei China Deal Registration； Huawei China Opportunity； Huawei China Service Opportunity 三种类型
 *            Representative Office， Province， City， System Department， Sub-Industry
 *            Opportunity Level, Opportunity Attribution，Opportunity Type，Lead Source
 * @Author : Steven
 * @Date : 2014-08-29 Refactor
 */
public without sharing class ChinaOpportunityUpdateDictCode implements Triggers.Handler{ //before insert,before update
    public void handle(){
        //Step1：获取所有涉及的OpportunityId
        Set<Id> accountIds = new Set<Id>();
        Set<Id> changedOppIds = new Set<Id>();
        for(Opportunity opt : (List<Opportunity>)Trigger.New){
            if(opt.recordtypeid == CONSTANTS.CHINAOPPORTUNITYRECORDTYPE || 
                 opt.recordtypeid == CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE){
                //Step1-1：如果是创建机会点，需要更新Code
                if(Trigger.isInsert){ 
                    accountIds.add(opt.accountid);
                }
                //Step1-2：如果是更新机会点，关键字段没变化不进行更新
                if(Trigger.isUpdate){ 
                    Opportunity oldOpt = (Opportunity)Trigger.oldMap.get(opt.id);
                    if(oppHasChanged(opt,oldOpt)) {
                        accountIds.add(opt.accountid);
                        changedOppIds.add(opt.id);
                    }
                }
            }
        }
        if(accountIds.isEmpty()) return;
        system.debug('-----ChinaOpportunityUpdateDictCode----');
        
        //Step2: 一次查询出机会点的CODE数据
        Map<String, String> mapDict = new Map<String, String>();
        List<DataDictionary__c> dds = [select name__c, type__c, code__c from DataDictionary__c where Object__c= 'Opportunity'];
        for(DataDictionary__c dd : dds){
            mapDict.put(dd.type__c + '__' + dd.name__c, dd.code__c);
        }
        
        //Step3：为机会点更新CODE
        for (Opportunity opt : (List<Opportunity>)Trigger.New){
            if(opt.recordtypeid == CONSTANTS.CHINAOPPORTUNITYRECORDTYPE || 
                 opt.recordtypeid == CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE){
                //Step3-1：如果是更新且关键字段没有变化，跳过
                if(Trigger.isUpdate && !changedOppIds.contains(opt.id)) continue;
                //Step3-2：如果是新增或关键字段发生变化的更新，刷新CODE
                opt.Representative_Office_Code__c = 
                    (opt.Representative_Office__c != null) ? mapDict.get('Representative Office__' + opt.Representative_Office__c) : null;
                opt.Province_Code__c = 
                    (opt.Province__c != null) ? mapDict.get('Province__' + opt.Province__c) : null;
                opt.City_Code__c = 
                    (opt.City__c != null) ? mapDict.get('City__' + opt.City__c) : null;
                opt.System_Department_Code__c = 
                    (opt.System_Department__c != null) ? mapDict.get('System Department__' + opt.System_Department__c) : null;
                opt.Sub_Industry_Code__c = 
                    (opt.Sub_Industry__c != null) ? mapDict.get('Sub-Industry__' + opt.Sub_Industry__c) : null;
                opt.Opportunity_Level_Code__c = 
                    (opt.Opportunity_Level__c != null) ? mapDict.get('Opportunity Level__' + opt.Opportunity_Level__c) : null;
                opt.Lead_Source_Code__c = 
                    (opt.LeadSource != null) ? mapDict.get('Lead Source__' + opt.LeadSource) : null;
                opt.Opportunity_Type_Code__c = 
                    (opt.Opportunity_Type__c != null) ? mapDict.get('Opportunity Type__' + opt.Opportunity_Type__c) : null;
            }
        }
    }

    /**
     * 判断机会点关键字段是否有更新
     */
    private Boolean oppHasChanged(Opportunity newOpp,Opportunity oldOpp){
        if(valueHasChanged(newOpp.Representative_Office__c,oldOpp.Representative_Office__c)) return true;
        if(valueHasChanged(newOpp.Province__c,oldOpp.Province__c)) return true;
        if(valueHasChanged(newOpp.City__c,oldOpp.City__c)) return true;
        if(valueHasChanged(newOpp.System_Department__c,oldOpp.System_Department__c)) return true;
        if(valueHasChanged(newOpp.Sub_Industry__c,oldOpp.Sub_Industry__c)) return true;
        if(valueHasChanged(newOpp.Opportunity_Level__c,oldOpp.Opportunity_Level__c)) return true;
        if(valueHasChanged(newOpp.LeadSource,oldOpp.LeadSource)) return true;
        if(valueHasChanged(newOpp.Opportunity_Type__c,oldOpp.Opportunity_Type__c)) return true;
        return false;
    }
    
    /**
     * 判断新旧属性值是否一致
     */
    private Boolean valueHasChanged(String newValue,String oldvalue){
        if((newValue == null)&&(oldValue == null)) {
            return false;
        } else if((newValue != null)&&(oldValue == null)) {
            return true;
        } else if((newValue == null)&&(oldValue != null)) {
            return true;
        } else if(newValue == oldValue) {
            return false;
        } else if(newValue != oldValue) {
            return true;
        }
        return false;
    }

}