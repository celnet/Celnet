/*
* @Purpose : it is the trigger handler,which will assign the account blacklist status be Gray
*  when the account record is created. 
*
* @Author : Jen Peng
* @Date : 		2013-5-4
*/
public without sharing class AccountBlacklistStatusInsertHandler implements Triggers.Handler{
	public void handle(){
		
		for (Account a : (List<Account>)trigger.new){
			a.Blacklist_Status__c='Gray';
			a.Last_Blacklist_Status__c=a.Blacklist_Status__c;
			a.blacklist_type__c='Pending';
			a.Blacklist_Last_Type__c=a.blacklist_type__c;
		}
		//system.debug('trigger insert account is here======================='+trigger.new);
		
	}
}