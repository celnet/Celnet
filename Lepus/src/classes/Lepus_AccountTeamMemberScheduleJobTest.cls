@isTest
private class Lepus_AccountTeamMemberScheduleJobTest {
	static testmethod void myUnitTest(){
		Lepus_EIPCalloutServiceTest.setCustomSettings();
		
		Account acc = Lepus_EIPCalloutServiceTest.createAccount('accx');
		insert acc;
		
		AccountTeamMember atm = new AccountTeamMember();
		atm.AccountId = acc.Id;
		atm.UserId = UserInfo.getUserId();
		insert atm;
		
		Lepus_AccountTeamMemberScheduleJob.syncAccountTeamMembers();
	}
}