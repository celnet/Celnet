/*
* @Purpose : 
* 在项目报备进行审批的时候,将该记录的编辑权限分配给对应的审批人
*
*
* @Author : Brain Zhang
*
*
*/
public without sharing class DealRegistrationSharingHandler implements Triggers.Handler{//after update
	
	public void handle() {
		Set<Id> ids = new Set<Id>();
		for(Deal_Registration__c d : (List<Deal_Registration__c>)Trigger.new){
			if(d.SubmittedToApproval__c && !((Deal_Registration__c)Trigger.oldMap.get(d.id)).SubmittedToApproval__c){
				ids.add(d.id);
			}
		}
		if(ids.isEmpty()){//如果没有变化,直接返回
			return ;
		}
		DealRegistrationSharingHandler.runLater(ids);
		
	}
	
	@future
	public static void runLater(Set<id> ids){//TargetObjectId
		List<ProcessInstance> instances = [SELECT Id,TargetObjectId, (SELECT Id, ActorId,OriginalActorId ,ProcessInstanceId FROM Workitems)
			FROM ProcessInstance where TargetObjectId in : ids and status = 'Pending'];
		Map<id,Set<Id>> recordToApproversMap = new Map<id,Set<Id>>();//deal registration id,approvers that need to share
		for(ProcessInstance i : instances){
			Set<Id> approverIDs = new Set<Id>();
			for(ProcessInstanceWorkitem m : i.Workitems){
				approverIds.add(m.ActorId);
				approverIds.add(m.OriginalActorId);
			}
			recordToApproversMap.put(i.TargetObjectId,approverIds);
		}
		List<Deal_Registration__Share> shareRecords = new List<Deal_Registration__Share>();
		for(Id i : recordToApproversMap.keySet()){
			Set<Id> approverIDs = recordToApproversMap.get(i);
			for(Id a : approverIDs){
				Deal_Registration__Share sharingRecord = new Deal_Registration__Share(ParentId = i,
					UserOrGroupId = a,AccessLevel  = 'Edit',RowCause = Schema.Deal_Registration__Share.rowCause.Share_to_approver__c);
				shareRecords.add(sharingRecord);
			}	
		}
		
		insert shareRecords;
	}
}