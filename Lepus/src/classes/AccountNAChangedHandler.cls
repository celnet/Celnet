/**
 * @Purpose : 1、父客户的NA属性变化，同步刷新所有子客户的NA属性
 *            2、NA属性的任何变化都会创建一条独立的Named Account记录
 * @Author : Steven
 * @Date :   2014-07-01 Update
 */
public without sharing class AccountNAChangedHandler implements Triggers.Handler{ //after update
	public static Boolean isFirstRun = true; 
	public void handle() {
		if(AccountNAChangedHandler.isFirstRun){
			for(Account acc : (List<Account>)trigger.new){
				if (acc.RecordTypeId == CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE){
					if(acc.Named_Account__c != ((Account)trigger.oldMap.get(acc.id)).Named_Account__c){
						AccountNAChangedHandler.isFirstRun = false;//not allow current code fire on the children accounts anymore
						if(acc.Is_Parent_Account__c == 'YES') { //只处理父客户变化的场景
							UtilAccount.UpdateChildrenNA(acc,'NA changed and system created'); //调用工具类来批量处理子客户na信息
						}
					} //end of Name_Account change
				} // end of HUAWEICHINACUSTOMERRECORDTYPE
			} // end of trigger new
		} // end of first run 
	} // end of handle
}