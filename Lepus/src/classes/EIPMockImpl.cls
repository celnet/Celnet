@isTest
public class EIPMockImpl implements WebServiceMock { 
	 public static boolean shouldThrowException = false;
	 public static boolean isBatchMode = false;
	 public static boolean isUpdateMode = false;
	 public static boolean isManualGetMode = false;//用户手动获取
   public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       if(EIPMockImpl.shouldThrowException){
       	throw new CalloutMockException(EIPSYNCCONSTANTS.VALIDATIONMSG);
       }
       if(isManualGetMode){
       	System.debug('enter this line:-----------');
       	B2bSoapGissue6.RS_element rs = new B2bSoapGissue6.RS_element();
        B2bSoapGissue6.tSRStatusReply respElement = new B2bSoapGissue6.tSRStatusReply();
        respElement.FLAG = 'true';
        rs.SRStatusReply = new B2bSoapGissue6.tSRStatusReply[]{respElement};
        response.put('response_x', rs); 
       	return;
       }
       if(!isBatchMode){
	       B2bSoapSissue6.tIssueRS respElement = new B2bSoapSissue6.tIssueRS();
	       respElement.ICareSR_Number = '123456';
	       respElement.ICARE_SYNC_STATUS = 'S';//iCare Sync Status 可能返回两个值，E表示错误，S表示成功
	       respElement.ICARE_ACK = 'success';
	       response.put('response_x', respElement); 
       }else{
       	System.debug('isUpdateMode:--------------' + isUpdateMode);
       		if(!isUpdateMode){//新建单条记录
		        SoapIssues6.RS_element rs = new SoapIssues6.RS_element();
		        SoapIssues6.tIssueRS respElement = new SoapIssues6.tIssueRS();
			       respElement.ICareSR_Number = '123456';
			       respElement.ICARE_SYNC_STATUS = 'S';//iCare Sync Status 可能返回两个值，E表示错误，S表示成功
			       respElement.ICARE_ACK = 'success';
		        rs.IssueRS = new SoapIssues6.tIssueRS[]{respElement};
		       	response.put('response_x', rs); 
       		}else{
       			B2bSoapUissue6.RS_element rs = new B2bSoapUissue6.RS_element();
       			B2bSoapUissue6.tUpdateIcareSRReply respElement = new B2bSoapUissue6.tUpdateIcareSRReply();
       			respElement.FLAG = 'true';
       			rs.UpdateIcareSRReply = new B2bSoapUissue6.tUpdateIcareSRReply[]{respElement};
       			response.put('response_x', rs); 
       		}
       }
   }
   
   public class CalloutMockException extends Exception {} 
}