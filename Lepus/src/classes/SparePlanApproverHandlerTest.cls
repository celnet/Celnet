/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SparePlanApproverHandlerTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        //User approver = UtilUnitTest.CreateUser('gaoyang', '(CSS)国内外部现场服务工程师(FSE)', '(N) Changchun Rep FSE', '00197371');
        Account acc = UtilUnitTest.createAccountWithName('Test');
        Contact contact = UtilUnitTest.createContact(acc);
        CSS_Contract__c c = UtilUnitTest.createContract(acc);
        c.Region_Dept_Picklist__c ='中国地区部';
        c.Office__c='长春代表处';
        update c;        
        Spare_Plan_Approver_Maps__c spmap = new Spare_Plan_Approver_Maps__c();
        spmap.Type__c = 'Spare Plan';
        spmap.Approver__c='005900000019xxj';
        spmap.Rep_Office__c='长春代表处';   
        spmap.RecordTypeId=[select id from RecordType where DeveloperName ='Spare_Plan_Approver_Map'].id;        
        insert spmap;  
        SFP__c sfp = UtilUnitTest.createSFP(c);         
        Spare_Plan__c sp = new Spare_Plan__c();
        sp.SFP__c = sfp.Id;  
        sp.Status__c ='Draft';      
        insert sp;
    }
}