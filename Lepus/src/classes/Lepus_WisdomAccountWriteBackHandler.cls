/**
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-8-30
 * Description: Wisdom回写到Salesforce时，处理Account控制字段的赋值
 */
public class Lepus_WisdomAccountWriteBackHandler implements Triggers.Handler{
    public static boolean isFirstRun = true;
    
    public void handle(){
        if(Lepus_WisdomAccountWriteBackHandler.isFirstRun){
            isFirstRun = false;
            
            if(UserInfo.getUserId() != '00590000003DBRhAAO' && !System.test.isRunningTest())
            return;
            
            //User u = [Select Id, Profile.Name From User Where Id =: UserInfo.getUserId()];
            //if(u.Profile.Name != 'Integration Profile for Lepus' && !System.test.isRunningTest())
            //return;
            
            Lepus_WisdomAccountWriteBackHandler.updateControllingFields();
        }
    }

   public static void updateControllingFields(){
    
        // 中国区 Customer_Group_Code__c -> Customer_Group__c, 从DataDictionary获取
        Map<String, String> customergroupMap = new Map<String, String>();
        // 中国区 Customer_Group__c -> Sub_Industry_Hw__c 
        Map<String, String> subIndustryHwMap = new Map<String, String>();
        // 中国区 Sub_Industry_Hw__c -> Industry 
        Map<String, String> chinaIndustryMap = new Map<String, String>();
        
        // 中国区 City_Code__c -> City_Huawei_China__c, 从DataDictionary获取
        Map<String, String> cityMap = new Map<String, String>();
        // 中国区 City_Huawei_China__c -> Province__c
        Map<String, String> provinceMap = new Map<String, String>();
        // 中国区 Province__c -> Representative_Office__c
        Map<String, String> repOfficeMap = new Map<String, String>();
        // 中国区 Representative_Office__c -> Region_Hw__c
        //Map<String, String> chinaRegionMap = new Map<String, String>();
        
        // 海外 Industry_Code__c -> Industry
        Map<String, String> overseaIndustryMap = new Map<String, String>();
        
        // 海外 Country_Code_HW__c -> Country_HW__c
        Map<String, String> countryMap = new Map<String, String>(); 
        // 海外 Country_HW__c -> Representative_Office__c
        Map<String, String> overseaRepOfficeMap = new Map<String, String>();
        // 海外 Representative_Office__c -> Region_Hw__c
        Map<String, String> regionHwMap = new Map<String, String>();
        
        
        for(DataDictionary__c dd : [Select Id, Code__c, Name__c, Object__c, Type__c From DataDictionary__c]){
            if(dd.Type__c == 'Customer Group' && dd.Object__c == 'Account'){
                customergroupMap.put(dd.Code__c, dd.Name__c);
            } else if(dd.Type__c == 'City' && dd.Code__c != '#N/A'){
                cityMap.put(dd.Code__c, dd.Name__c);
            }
        }
        
        for(Field_Dependency__c fd : [Select Id, Controlling_Field__c, Controlling_Field_Value__c, Dependent_Field__c, 
                                            Dependent_Field_Value__c, Object_Type__c From Field_Dependency__c Where Object_Type__c = 'Account']){
            if(fd.Dependent_Field__c == 'Customer_Group__c'){
                subIndustryHwMap.put(fd.Dependent_Field_Value__c, fd.Controlling_Field_Value__c);
            } else if(fd.Dependent_Field__c == 'Sub_Industry_Hw__c'){
                chinaIndustryMap.put(fd.Dependent_Field_Value__c, fd.Controlling_Field_Value__c);
            } else if(fd.Dependent_Field__c == 'City_Huawei_China__c'){
                provinceMap.put(fd.Dependent_Field_Value__c, fd.Controlling_Field_Value__c);
            } else if(fd.Dependent_Field__c == 'Province__c'  && fd.Controlling_Field_Value__c != '系统部总部' ){
                repOfficeMap.put(fd.Dependent_Field_Value__c, fd.Controlling_Field_Value__c);
            } else if(fd.Dependent_Field__c == 'Representative_Office__c'){
                regionHwMap.put(fd.Dependent_Field_Value__c, fd.Controlling_Field_Value__c);
            } else if(fd.Dependent_Field__c == 'Country_HW__c'){
                overseaRepOfficeMap.put(fd.Dependent_Field_Value__c, fd.Controlling_Field_Value__c);
            } else if(fd.Dependent_Field__c == 'Country_Code_HW__c'){
                countryMap.put(fd.Dependent_Field_Value__c, fd.Controlling_Field_Value__c);
            } else if(fd.Dependent_Field__c == 'Industry_Code__c'){
                overseaIndustryMap.put(fd.Dependent_Field_Value__c, fd.Controlling_Field_Value__c);
            }
        }
        
        for(Account acc : (List<Account>)trigger.new){
            // 过滤记录类型
            if(!Lepus_HandlerUtil.filterRecordType(acc, Account.sobjecttype))
            continue;
            
            // 如果CIS编码不为空，则不归集
            if(acc.CIS__c != null)
            continue;
            
            String recordType = Lepus_HandlerUtil.getRecordType(acc, Account.sobjecttype);
            
            if(recordType == '中国区'){
                
                if(acc.Customer_Group_Code__c != null && customergroupMap.get(acc.Customer_Group_Code__c) != null){   
                    acc.Customer_Group__c = customergroupMap.get(acc.Customer_Group_Code__c);
                    acc.Sub_industry_HW__c = subIndustryHwMap.get(acc.Customer_Group__c);
                    acc.Industry = chinaIndustryMap.get(acc.Sub_industry_HW__c);
                }
                
                
                if(acc.City_Code__c != null && cityMap.get(acc.City_Code__c) != null){
                    acc.City_Huawei_China__c = cityMap.get(acc.City_Code__c);
                    acc.Province__c = provinceMap.get(acc.City_Huawei_China__c);
                    acc.Representative_Office__c = repOfficeMap.get(acc.Province__c);
                    acc.Region_HW__c = regionHwMap.get(acc.Representative_Office__c);
                }
                
            } else if(recordType == '海外'){
                
                if(acc.Industry_Code__c != null && overseaIndustryMap.get(acc.Industry_Code__c) != null){
                    acc.Industry = overseaIndustryMap.get(acc.Industry_Code__c);
                }
                if(acc.Country_Code_HW__c != null && countryMap.get(acc.Country_Code_HW__c) != null){ 
                    acc.Country_HW__c = countryMap.get(acc.Country_Code_HW__c);
                    acc.Representative_Office__c = overseaRepOfficeMap.get(acc.Country_HW__c);
                    acc.Region_HW__c = regionHwMap.get(acc.Representative_Office__c);
                }
            }
        }
    }
}