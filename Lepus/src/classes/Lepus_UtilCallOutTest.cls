/**
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-9-15
 * Description: test Lepus_UtilCallOut
 */
@isTest
private class Lepus_UtilCallOutTest {
	static testmethod void myUnitTest(){
		// test concatenateLeadHistoryXml
		LeadHistory lh = new LeadHistory();
		lh.LeadId = '00QO0000002H4va';
		Lepus_UtilCallout.concatenateLeadHistoryXml(new list<LeadHistory>{lh}, '', '', '', '', '', '');
		
		// test concatenateUserXml
		String query = accessibleField(User.Sobjecttype);
		query += ' Where Id = \'' + UserInfo.getUserId() + '\'';
		User u = Database.query(query);
		Lepus_UtilCallout.concatenateUserXml(new List<User>{u}, '', '', '', '', '', '');
		
		// test ValidationDataToJson
		Lead l = new Lead();
		l.LastName = 'xxx';
		l.Company = 'xfdfd';
		l.RecordTypeId = CONSTANTS.OVERSEALEADRECORDTYPE;
		l.Status = 'Follow-up';
		insert l;
		
		String leadQuery = accessibleField(Lead.sobjecttype);
		leadQuery += ' Where Id = \'' + l.Id + '\'';
		
		Lead lx = Database.query(leadQuery);
		Lepus_UtilCallout.ValidationDataToJson(new list<Lead>{lx}, 'Lead');
		
		// test FieldUpdateToJson
		OpportunityFieldHistory ofh = new OpportunityFieldHistory();
		ofh.OpportunityId = '006O0000002H4va';
		Lepus_UtilCallout.FieldUpdateToJson(new list<OpportunityFieldHistory>{ofh});
		
		// test concatenateTeamMemberXml
		Account acc1 = createAccount('acc1');
		Account acc2 = createAccount('acc2');
		
		insert new list<Account>{acc1,acc2};
		
		String accQuery = accessibleField(Account.Sobjecttype);
		accQuery += ' Where Id = \'' + acc1.Id + '\'';
		accQuery += ' Or Id = \'' + acc2.Id + '\'';
		
		list<Account> accList = Database.query(accQuery);
		Lepus_EIPMember em11 = new Lepus_EIPMember();
		Lepus_EIPMember em12 = new Lepus_EIPMember();
		
		Lepus_EIPMember em21 = new Lepus_EIPMember();
		Lepus_EIPMember em22 = new Lepus_EIPMember();
		
		map<id, list<Lepus_EIPMember>> idmembermap = new map<Id, list<Lepus_EIPMember>>();
		idmembermap.put(acc1.Id, new list<Lepus_EIPMember>{em11, em12});
		idmembermap.put(acc2.Id, new list<Lepus_EIPMember>{em21, em22});
		
		map<id, Sobject> idobjmap = new map<Id, Sobject>();
		idobjmap.put(accList[0].Id, accList[0]);
		idobjmap.put(accList[1].Id, accList[1]);
		
		Lepus_UtilCallout.concatenateTeamMemberXml(idmembermap, idobjmap, 'Account', '');
		
		// test concatenateBusinessDataJson
		Opportunity opp = createOpportunity('xmlda', acc1.Id);
		insert opp;
		
		Product_HS__c ph = new Product_HS__c();
		insert ph;
		
		Project_Product__c pp = new Project_Product__c();
		pp.Project_Name__c = opp.id;
		pp.Lookup__c = ph.Id;
		insert pp;
		
		String oppQuery = accessibleField(Opportunity.sobjecttype);
		oppQuery += ' Where Id = \'' + opp.Id + '\'';
		Opportunity oppx = Database.query(oppQuery);
		
		Lepus_UtilCallout.concatenateBusinessDataJson(oppx, 'Opportunity');
		Lepus_UtilCallout.concatenateBusinessDataJson(acc1, 'Account');
		
		Contact con = createContact(acc1.Id);
		insert con;
		
		Lepus_UtilCallout.concatenateBusinessDataJson(lx, 'Lead');
		Lepus_UtilCallout.concatenateBusinessDataJson(con, 'Contact');
	}
	
	static String accessibleField(Schema.Sobjecttype objType){
    	// 组合字段无法查询，排除掉
    	Set<String> compositeFields = new Set<String>{'billingaddress','shippingaddress','address'};
        
        String sQuery = 'Select ';
        
        Map<String, Schema.SobjectField> map_fields = new Map<String, Schema.SobjectField>();
        
        //根据对象类型，获取字段集合
        map_fields = objType.getDescribe().fields.getMap();
        
        //拼接字段到SOQL语句
        for(String sFieldApiName : map_fields.keySet()){
            // 排除组合字段
            if(compositeFields.contains(sFieldApiName)){
            	continue;
            }
            sQuery += sFieldApiName ;
            sQuery += ',';
        }
        
        sQuery += 'LastModifiedBy.Employee_ID__c,';
        sQuery += 'LastModifiedBy.W3ACCOUNT__c,';
        sQuery += 'LastModifiedBy.FullName__c,';
        sQuery += 'LastModifiedBy.EnglishName__c,';
        sQuery += 'CreatedBy.Employee_ID__c,';
        sQuery += 'CreatedBy.W3ACCOUNT__c,';
        sQuery += 'CreatedBy.FullName__c,';
        sQuery += 'CreatedBy.EnglishName__c,';
        
        //添加关系类型的查找，比如account.name。todo..
        if(objType != User.sobjecttype){
        	sQuery += 'RecordType.DeveloperName,' ;
        } else {
        	sQuery += 'Profile.Name,';
        	sQuery += 'UserRole.Name,';
        }
        
        // Lead的Owner关联到User或者Group，需另外单独查询， User无Owner
        if(objType != Lead.sobjecttype && objType != User.sobjecttype){
        	sQuery += 'Owner.Employee_ID__c,' ;
        	sQuery += 'Owner.W3ACCOUNT__c,' ;
        	sQuery += 'Owner.FullName__c,';
        }
        
        if(objType == Opportunity.sobjecttype){
            sQuery += 'Account.Name,' ;
            sQuery += '(Select p.Id, p.Sales_Price__c, p.Quantity__c, p.Lookup__r.Name, p.Lookup__c, p.Project_Name__c From Project_Product__r p),';
        } else if(objType == Account.sobjecttype){
            sQuery += 'Parent.Name,' ;
        } else if(objType == Lead.sobjecttype){
        	sQuery += 'Account_Name__r.Name,';
        	sQuery += 'China_Opportunity__r.Name,';
        } else if(objType == Contact.sobjecttype){
        	sQuery += 'Account.Name,';
        }
        sQuery = sQuery.substring(0, sQuery.length()-1 );
        sQuery += ' From '+objType.getDescribe().getName();
        return sQuery;
    }
    
    static Account createAccount(String accName){
		Account acc = new Account();
		acc.Name = accName;
		acc.RecordTypeId = CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE;
		acc.City_Huawei_China__c = '北京市';
		acc.OwnerId = UserInfo.getUserId();
		
		return acc;
	}
	
	static Opportunity createOpportunity(String oppName, Id accId){
		Opportunity opp = new Opportunity();
		opp.Name = oppName;
		opp.AccountId = accId;
		opp.OwnerId = UserInfo.getUserId();
		opp.Opportunity_Level_Code__c = 'SubRegion';
		//opp.Opportunity_Level__c = 'Region Level';
		//opp.Opportunity_Type__c = 'Carrier Resales';
		opp.Opportunity_Type_Code__c = 'SM002';
		opp.China_Competitor_Trends__c = 'xxx';
		opp.Project_Risk__c = 'xxx';
		opp.China_Following_Strategy__c = 'xxx';
		opp.Master_Channer_Partner__c = 'xxx';
		opp.StageName = 'SS7 (Implementing)';
		opp.CloseDate = Date.today();
		opp.RecordTypeId = CONSTANTS.HUAWEIOVERSEAOPPRECORDTYPE;
		
		opp.WinCom_Cisco__c = true;
		opp.WinCom_EMC__c = true;
		opp.WinCom_H3C__c = true;
		opp.WinCom_HongShan__c = true;
		opp.WinCom_Polycom__c = true;
		opp.WinCom_ZhongXin__c = true;
		opp.WinCom_DELL__c = true;
		opp.WinCom_HP__c = true;
		
		return opp;
	}
	
	static Contact createContact(Id accId){
		Contact con = new Contact();
		con.RecordTypeId = CONSTANTS.HUAWEICHINACONTACTRECORDTYPE;
		con.AccountId = accId;
		con.FirstName = 'Chung';
		con.LastName = 'cola';
		
		return con;
	}
}