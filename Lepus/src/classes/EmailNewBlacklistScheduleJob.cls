/*
* @Purpose : this schedule job is for emailing to the Region Admin and EGB Sales Management Manager
* when there new Account records in the Blacklist.
*
* @Author : Jen Peng
* @Date : 		2013-5-9
*/
global class EmailNewBlacklistScheduleJob implements Schedulable{
	
	public Set<String> regionAdminEmails{get;set;}//for no duplicate regionAdminEmails
	public Set<String> ebgSalesMgrEmails{get;set;}//for no duplicate ebgSalesMgrEmails
	public Set<String> regions{get;set;}//for no duplicate regions
	public List<Account> redAccounts{get;set;} 
	public List<Blacklist_Check_History__c> bchs{get;set;}
	public List<User> users{get;set;}
	public List<Messaging.SendEmailResult> results{get;set;} 
	public List<Report> reports{get;set;}
	
	public EmailNewBlacklistScheduleJob(){
		try{
			regionAdminEmails=new Set<String>();
			ebgSalesMgrEmails=new Set<String>();
			regions=new Set<String>();
			redAccounts=new List<Account>();
			bchs=new List<Blacklist_Check_History__c>();
			users=new List<User>();
			Set<Id> accIds = new Set<Id>();
			reports = new List<Report>();
			reports = [Select r.Name, r.Id From Report r where r.Name='Blacklist Account(Current Month)'];
			bchs=[Select Account__c,Blacklist_Status__c From Blacklist_Check_History__c 
			      where status_date__c>=TODAY and status_date__c<TOMORROW and Blacklist_Status__c='Red'];
			if(!bchs.isEmpty()){
				for(Blacklist_Check_History__c b:bchs){
					accIds.add(b.Account__c);
				}
				bchs.clear();
				redAccounts=[Select id,Region_HW__c from Account where id in:accIds];
				for(Account a:redAccounts){
					regions.add(a.Region_HW__c);//get no duplicate regions
				}
				redAccounts.clear();
				String query = 'Select Id,Name,Role_Name__c,Email,IsActive From User where';
				String cons='';
				for(String r:regions){
					cons+=' Role_Name__c = \'HW '+r+' Region Admin\' or Role_Name__c = \'HW '+r+' Admin\' or';
				}
				system.debug('cons===================================='+cons);
				regions.clear();
				cons=cons.substring(0,cons.length()-3);
				query+=cons;
				users=Database.query(query);
				for(User u:users){
					if(u.IsActive){
						regionAdminEmails.add(u.Email);//get no duplicate regionAdminEmails
					}
				}
				users.clear();
				if(!regionAdminEmails.isEmpty()){
					String query1 = 'Select Id,Name,Role_Name__c,Email,IsActive From User where Role_Name__c=\'HW EBG Sales Management Manager\'';
					users=Database.query(query1);
					for(User u:users){
						if(u.IsActive){
							ebgSalesMgrEmails.add(u.Email);//get no duplicate ebgSalesMgrEmails
						}
					}
					users.clear();
				}
				//system.debug('regionAdminEmails is here========================='+regionAdminEmails);
				//system.debug('ebgSalesMgrEmails is here========================='+ebgSalesMgrEmails);
			}      
		}catch(Exception e){
			System.debug('==============opooos~!');
		}
			
	}
	
	public void sendEmail(){
		try{
			Integer n = ebgSalesMgrEmails.size()+regionAdminEmails.size();			
			if(n>0){
				Messaging.reserveSingleEmailCapacity(n);
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
				List<string> toAddress = new List<string>();
				if(regionAdminEmails.size()>0){
					for(String s:regionAdminEmails){
						toAddress.add(s);
					}
				}
				if(ebgSalesMgrEmails.size()>0){
					for(String s:ebgSalesMgrEmails){
						toAddress.add(s);
					}
				}
				mail.setToAddresses(toAddress);
				mail.setSenderDisplayName('Important No-reply email from Salesforce System');
				mail.setSubject('There some accounts are in the blacklist now~!');
				String strbody = '<p>Dear Mr/Ms:</p>';
				strbody += '<p>For your information, changes of their repecective Export Control Blacklist categories have occured to the following accounts:</p>';
				strbody += system.Url.getSalesforceBaseUrl().toExternalForm()+'/'+reports.get(0).id;
				mail.setHtmlBody(strbody);
				results=Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
				
			}
			
		}catch(Exception e){
			System.debug('=======================Email opooos~!');
		}
	}
	global void execute(SchedulableContext SC){
		EmailNewBlacklistScheduleJob e = new EmailNewBlacklistScheduleJob();
		e.sendEmail();
	}
}