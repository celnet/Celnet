public with sharing class OpportunityTeamMemberArchiveForBIHandler implements Triggers.Handler{
    public void handle(){
        Set<Id> oppIds = new Set<Id>();
        Set<Id> chinaOppIds = new Set<Id>();
        for(OpportunityTeamMember member : (List<OpportunityTeamMember>)trigger.old){
            oppIds.add(member.OpportunityId);
        }
        
        List<Opportunity> oppList = new List<Opportunity>();
        oppList = [Select Id From Opportunity Where Id IN: oppIds 
            And (RecordTypeId =: CONSTANTS.CHINAOPPORTUNITYRECORDTYPE OR RecordTypeId =: CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE)];
        for(Opportunity opp : oppList){
            chinaOppIds.add(opp.Id);
        }
        if(chinaOppIds.isEmpty()) return;
        List<Data_Maintain_History__c> archiveList = new List<Data_Maintain_History__c>();
        for(OpportunityTeamMember otm : (List<OpportunityTeamMember>) trigger.old){
            if(chinaOppIds.contains(otm.OpportunityId)){
                Data_Maintain_History__c archive = new Data_Maintain_History__c();
                archive.Record_ID__c = otm.Id;
                archive.Operation_Type__c = 'Delete';
                archive.Data_Type__c = 'China Opportunity Team(BI)';
                archiveList.add(archive);
            }
        }
        if(archiveList.size() > 0){
            insert archiveList;
        }
    }
}