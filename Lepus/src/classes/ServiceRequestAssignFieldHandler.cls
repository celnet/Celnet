/*
*@Purpose:rewrite the TigOnCase trigger,perform the same function:
*owner更新时,重设owner__c字段,因该字段牵涉面较广,因此未改由formula实现
*@Author:Brain Zhang
*Create Date：2013-9
*/
public without sharing class ServiceRequestAssignFieldHandler  implements Triggers.Handler {
		public static boolean shouldRun = true;
		public void handle(){//before insert,before update
			if(!CommonConstant.serviceRequestTriggerShouldRun){
				return ;
			}
			if(!shouldRun){
				return ;
			}

		Set<id> userIds = new Set<id>();
		for(Case c:(List<Case>)Trigger.new){
				userIds.add(c.OwnerId);
		}
		if(!userIds.isEmpty()){
			Map<Id,User> ownerMap = new Map<Id,User>([Select u.Id,u.EmployeeNumber,u.Name,u.Profile.Name, u.Profile.Id, u.ProfileId 
            From User u
            where u.Id in :userIds]);
			
			for(Case c : (List<Case>)Trigger.new){
				
				if(ownerMap.get(c.ownerid) != null)
				{
				System.debug('------------owner profile'+c.owner.Profile);
				c.owner__c = c.ownerid;
				}
			}
		}
		}
}