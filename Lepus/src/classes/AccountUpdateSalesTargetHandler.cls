/**
 * @Purpose : 当客户不再是NA父客户时，失效对应的销售目标；
 *						当TCG失效掉时，失效对应的销售目标；
 *						当客户和TCG的Owner发生变化时，更新所有的销售目标Owner；
 * @Author : Steven
 * @Date : 2013-10-23
 */
public without sharing class AccountUpdateSalesTargetHandler implements Triggers.Handler{ //fires after update
	public void handle(){
		Set<Id> naIds = new Set<Id>(); //NA父客户的ID
		Set<Id> inactiveNAIds = new Set<Id>(); //变成非NA父客户的ID
		Set<Id> tcgIds = new Set<Id>(); //TCG的ID
		Set<Id> inactiveTCGIds = new Set<Id>(); //失效的TCG ID
		Map<id,Id> ownerIdMap = new  Map<id,Id>(); //客户ID与OwnerID对应关系
		//查询出所有发生变化的客户Owner
		for (Account acc : (List<Account>)trigger.new){
	    if (acc.RecordTypeId == CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE){
	    	//Type1: 如果中国区NA父客户的Owner发生了改变
	      if(acc.OwnerId != ((Account)trigger.OldMap.Get(acc.Id)).OwnerId && acc.Is_Parent_Account__c=='YES'){
		  		naIds.add(acc.id);
		  		if(!ownerIdMap.containsKey(acc.id)) {
		  			ownerIdMap.put(acc.id,acc.OwnerId);
		  		}
	      }
	      //Type2: 如果中国区NA父客户变成其他类型客户
	      if(acc.Is_Parent_Account__c=='NO' && ((Account)trigger.OldMap.Get(acc.Id)).Is_Parent_Account__c=='YES'){
		  		inactiveNAIds.add(acc.id);
		  		if(!ownerIdMap.containsKey(acc.id)) {
		  			ownerIdMap.put(acc.id,acc.OwnerId);
		  		}
	      }
	    }
	    if (acc.RecordTypeId == CONSTANTS.HUAWEICHINATCGRECORDTYPE){
	    	//Type3: 如果中国区TCG的Owner发生了改变
	      if(acc.OwnerId != ((Account)trigger.OldMap.Get(acc.Id)).OwnerId){
		  		tcgIds.add(acc.id);
		  		if(!ownerIdMap.containsKey(acc.id)) {
	  			ownerIdMap.put(acc.id,acc.OwnerId);
		  		}
	      }
	      //Type4: 如果中国区TCG失效了
	      if(acc.Inactive__c && !((Account)trigger.OldMap.Get(acc.Id)).Inactive__c){
		  		inactiveTCGIds.add(acc.id);
		  		if(!ownerIdMap.containsKey(acc.id)) {
		  			ownerIdMap.put(acc.id,acc.OwnerId);
		  		}
	      }
	    }
		}
		
		//如果客户Owner没有发生变化则返回
		if (naIds.isEmpty() && inactiveNAIds.isEmpty() && tcgIds.isEmpty() && inactiveTCGIds.isEmpty()) return;
		
		//自动更新关联的销售目标
		autoUpdateSalesTarget(naIds,inactiveNAIds,tcgIds,inactiveTCGIds,ownerIdMap);
	}
	
	/**
	 * 自动更新关联的销售目标
	 */
	@future
	public static void autoUpdateSalesTarget(Set<Id> naIds,Set<Id> inactiveNAIds,Set<Id> tcgIds,Set<Id> inactiveTCGIds,Map<id,Id> ownerIdMap){
		//将所有客户的Target全部查询出来
		List<Sales_Target_Accomplishment__c> targetList = 
			[select id,OwnerId,China_Account_Name__c, China_TCG_Name__c,recordtypeid 
			   from Sales_Target_Accomplishment__c 
				where (recordtypeid =: CONSTANTS.SALESTARGETMANAGERECORDTYPE OR 
							 recordtypeid =: CONSTANTS.SALESTARGETCHINATCGRECORDTYPE OR 
							 recordtypeid =: CONSTANTS.SALESTARGETCHINANARECORDTYPE) 
					and Inactive__c = false
						and (China_Account_Name__c in :naIds OR 
						     China_Account_Name__c in :inactiveNAIds OR
						     China_TCG_Name__c in :tcgIds OR
						     China_TCG_Name__c in :inactiveTCGIds )];
		
		//根据客户Owner更新所有生效的Target的Owner
		for (Sales_Target_Accomplishment__c target : targetList){
			//如果是TCG的目标
			if(target.recordtypeid == CONSTANTS.SALESTARGETCHINATCGRECORDTYPE) {
				target.OwnerId = ownerIdMap.get(target.China_TCG_Name__c);
				if(inactiveTCGIds.contains(target.China_TCG_Name__c)) {
					target.Inactive__c = true;
					target.Inactive_Reason__c = '中国区TCG已经失效，系统自动失效TCG的目标';
				}
			//如果是NA客户的目标
			} else {
				target.OwnerId = ownerIdMap.get(target.China_Account_Name__c);
				if(inactiveNAIds.contains(target.China_Account_Name__c)) {
					target.Inactive__c = true;
					target.Inactive_Reason__c = '客户不再是NA父客户，系统自动失效NA客户目标';
				}
			}
		}
		update targetList;
	}
}