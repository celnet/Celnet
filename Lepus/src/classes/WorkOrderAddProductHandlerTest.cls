@isTest
private class WorkOrderAddProductHandlerTest {

    static testMethod void workOrderProductsTest(){//10.根据SFP_Product__c记录创建对应的Work_Order_Product__c记录
    	Account acc = UtilUnitTest.createAccount();
    	Case sr = UtilUnitTest.createChinaServiceRequest();
    	CSS_Contract__c cc = UtilUnitTest.createContract(acc);
    	SFP__c sfp = UtilUnitTest.createSFP(cc);
    	Product_HS__c product = UtilUnitTest.createProductMaster('test');
    	Contract_Product_PO__c cpp = UtilUnitTest.createContractProduct(cc,product);
    	SFP_Product__c sfpp = UtilUnitTest.createSFPProduct(sfp, cpp);
    	RecordType notOnSiteType = [select id from RecordType where isActive = true 
    		and (not developername like '%Health%') and SobjectType = 'Work_Order__c' limit 1];
    	Work_Order__c wo = new Work_Order__c(Account_Name__c = acc.id,Service_Request_No__c = sr.id,recordtypeid = notOnSiteType.id,
    		SFP_No__c = sfp.id);
    	System.debug('going to insert the wo---------------');
    	insert wo;
    	System.assertNotEquals(null,[select id from Work_Order_Product__c]);
    }
}