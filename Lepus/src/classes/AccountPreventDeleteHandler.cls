/**
 * @Purpose : 判断中国区客户是否有关联的子客户或机会点，如果有不允许删除
 * @Author : Steven
 * @Date :   2014-07-01 Update
 */    
public without sharing class AccountPreventDeleteHandler implements Triggers.Handler{//before update,before delete 
	public void handle() {
		Set<id> accIds = new Set<id>();
		//Step1：记录下待删除的客户ID
		if(Trigger.isUpdate){
			for(Account acc : (List<Account>)trigger.new){
				if(acc.RecordTypeId == CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE){
					Account oldAcc = (Account)trigger.oldMap.get(acc.id);
					if(acc.To_Be_Deleted__c && !oldAcc.To_Be_Deleted__c) accIds.add(acc.id);
				}
			}
		}
		if(Trigger.isDelete){
			for(Account acc : (List<Account>)trigger.old){
				if(acc.RecordTypeId == CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE){
					accIds.add(acc.id);
				}
			}
		}
    if (accIds.isEmpty()) return;

		//Step2：查询出客户下关联的子客户和机会点数量
		AggregateResult[] groupedAccResults = [SELECT parentid,count(id) FROM Account where parentid in:accIds group by parentid];
		AggregateResult[] groupedOppResults = [SELECT accountId,count(id) FROM opportunity where accountId in:accIds group by accountId];
		Map<id,Integer> accNumMap = new Map<id,Integer> ();
		Map<id,Integer> oppNumMap = new Map<id,Integer> ();
		for(AggregateResult gr : groupedAccResults){
			accNumMap.put((String)gr.get('parentid'),(Integer)gr.get('expr0'));
		}
		for(AggregateResult gr : groupedOppResults){
			oppNumMap.put((String)gr.get('accountId'),(Integer)gr.get('expr0'));
		}
		
		//Step3：判断客户是否可以删除
		List<Account> accs = Trigger.isDelete ? (List<Account>)trigger.old : (List<Account>)trigger.new;
		for(Account acc : accs){
			if(accNumMap.get(acc.id) > 0){
				acc.addError('此客户有子客户，不允许删除');
			}
			if(oppNumMap.get(acc.id) > 0){
				acc.addError('此客户有关联的机会点，不允许删除');
			}
		}
		
	}
}