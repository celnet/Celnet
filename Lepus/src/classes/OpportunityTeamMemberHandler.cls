/*
* @Purpose : 中国区机会点团队成员修改后刷新机会点上的一个时间字段；
* @Author : steven
* @Date :		2013-11-30
* update 2014-02-17 中国区机会点团队维护后自动记录团队维护历史  机会点团队成员不会同时删除和新增
*/
public with sharing class OpportunityTeamMemberHandler implements Triggers.Handler{ 
	public void handle(){
		List<Data_Maintain_History__c> historyList = new List<Data_Maintain_History__c>();
		Set<Id> chinaOppIds  = new Set<id>();
		Set<id> oppIds = new Set<id>();
		
		//查询出所有涉及到的中国区机会点
		if(Trigger.isInsert || Trigger.isUpdate) {
			for(OpportunityTeamMember member : (List<OpportunityTeamMember>)trigger.new){
				oppIds.add(member.OpportunityId);
			}
		}
		if(trigger.isDelete) {
			for(OpportunityTeamMember member : (List<OpportunityTeamMember>)trigger.old){
				oppIds.add(member.OpportunityId);
			}
		}
		List<Opportunity> opps = [Select id From Opportunity Where id IN:oppIds 
			AND (RecordTypeId =:CONSTANTS.CHINAOPPORTUNITYRECORDTYPE OR RecordTypeId =:CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE)
			AND StageName <> 'SS1 (Notifying Opportunity)' 
			AND StageName <> 'SS2 (Identifying Opportunity)' 
			AND StageName <> 'SS3 (Validating Opportunity)'];
		for(Opportunity o : opps) {
			chinaOppIds.add(o.Id);
		}
		if(chinaOppIds.isEmpty()) return;
		
		if(Trigger.isInsert) {
			for(OpportunityTeamMember member : (List<OpportunityTeamMember>)trigger.new){
				if(chinaOppIds.contains(member.OpportunityId)) {
					Data_Maintain_History__c history = new Data_Maintain_History__c();
					history.Data_Type__c = 'China Opportunity Team';
					history.Operation_Type__c = 'Create';
					history.Record_ID__c = member.OpportunityId;
					historyList.add(history);
					chinaOppIds.remove(member.OpportunityId); //同一个机会点加入一条记录就可以了，仅用于判断机会点团队有无刷新
				}
			}
		}
		if(Trigger.isUpdate) {
			for(OpportunityTeamMember member : (List<OpportunityTeamMember>)trigger.new){
				if(chinaOppIds.contains(member.OpportunityId)) {
					Data_Maintain_History__c history = new Data_Maintain_History__c();
					history.Data_Type__c = 'China Opportunity Team';
					history.Operation_Type__c = 'Update';
					history.Record_ID__c = member.OpportunityId;
					historyList.add(history);
					chinaOppIds.remove(member.OpportunityId); //同一个机会点加入一条记录就可以了，仅用于判断机会点团队有无刷新
				}
			}
		}
		if(trigger.isDelete) {
			for(OpportunityTeamMember member : (List<OpportunityTeamMember>)trigger.old){
				if(chinaOppIds.contains(member.OpportunityId)) {
					Data_Maintain_History__c history = new Data_Maintain_History__c();
					history.Data_Type__c = 'China Opportunity Team';
					history.Operation_Type__c = 'Delete';
					history.Record_ID__c = member.OpportunityId;
					historyList.add(history);
					chinaOppIds.remove(member.OpportunityId); //同一个机会点加入一条记录就可以了，仅用于判断机会点团队有无刷新
				}
			}
		}

		insert historyList;
	}
	
	/*
	public void handle(){
		Set<id> oppIds = new Set<id>();
		if(Trigger.isInsert || Trigger.isUpdate) {
			for(OpportunityTeamMember member : (List<OpportunityTeamMember>)trigger.new){
				oppIds.add(member.OpportunityId);
			}
		}
		if(trigger.isDelete) {
			for(OpportunityTeamMember member : (List<OpportunityTeamMember>)trigger.old){
				oppIds.add(member.OpportunityId);
			}
		}
		List<Opportunity> opps = [Select id,Team_Member_Update_Time__c From Opportunity Where id IN:oppIds AND 
			(RecordTypeId =:CONSTANTS.CHINAOPPORTUNITYRECORDTYPE OR RecordTypeId =:CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE)];
		for(Opportunity opp : opps) {
			try {
				opp.Team_Member_Update_Time__c = System.now();
				update opp;  
			} catch(Exception e) {
				if(Trigger.isInsert || Trigger.isUpdate) {
					for(OpportunityTeamMember member : (List<OpportunityTeamMember>)trigger.new){
						if(member.OpportunityId == opp.id) member.addError('当前机会点基本信息存在必填项未填，请先完善后再维护机会点小组');
					}
				}
				if(trigger.isDelete) {
					for(OpportunityTeamMember member : (List<OpportunityTeamMember>)trigger.old){
						if(member.OpportunityId == opp.id) member.addError('当前机会点基本信息存在必填项未填，请先完善后再维护机会点小组');
					}
				}
			}
		}
	}
	*/
}