/*
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-6-24
 * Description: Test MassApproveAccountLinkRequest
 */
@isTest
private class MassApproveAccountLinkRequestTest {
	testmethod static void test(){
		Account acc1 = UtilUnitTest.createHWChinaAccount('acc1');
		Account acc2 = UtilUnitTest.createHWChinaNAAccount('acc2');
		
		/*
		List<Account_Link_Request__c> alrList1 = new List<Account_Link_Request__c>();
		
		for(Integer i = 0;i<100;i++){
			Account_Link_Request__c alr1 = new Account_Link_Request__c();
			alr1.child__c = acc1.Id;
			alr1.parent__c = acc2.Id;
			alr1.Approver__c = UserInfo.getUserId();
			alrList1.add(alr1);
		}
		insert alrList1;
		*/
		
		Account_Link_Request__c alr1 = new Account_Link_Request__c();
		alr1.child__c = acc1.Id;
		alr1.parent__c = acc2.Id;
		alr1.Approver__c = UserInfo.getUserId();
		
		Account_Link_Request__c alr2 = new Account_Link_Request__c();
		alr2.child__c = acc1.Id;
		alr2.parent__c = acc2.Id;
		alr2.Approver__c = UserInfo.getUserId();
		
		Account_Link_Request__c alr3 = new Account_Link_Request__c();
		alr3.child__c = acc1.Id;
		alr3.parent__c = acc2.Id;
		alr3.Approver__c = UserInfo.getUserId();
		
		insert new List<Account_Link_Request__c>{alr1,alr2,alr3};
		
		/*
		for(Account_Link_Request__c alr : [Select Id From Account_Link_Request__c]){
			Approval.Processsubmitrequest req1 = new Approval.Processsubmitrequest();
			req1.setObjectId(alr.Id);
			Approval.Processresult result = Approval.process(req1);
		}
		*/
		
		Approval.Processsubmitrequest req1 = new Approval.Processsubmitrequest();
		req1.setObjectId(alr1.Id);
		Approval.Processresult result = Approval.process(req1);
		
		System.assert(result.isSuccess());
		System.assertEquals('Pending', result.getInstanceStatus(), 'Instance Status' + result.getInstanceStatus());
		
		Approval.Processsubmitrequest req2 = new Approval.Processsubmitrequest();
		req2.setObjectId(alr2.Id);
		Approval.Processresult result2 = Approval.process(req2);
		
		Approval.Processsubmitrequest req3 = new Approval.Processsubmitrequest();
		req3.setObjectId(alr3.Id);
		Approval.Processresult result3 = Approval.process(req3);
		
		
		/*
		Approval.Processsubmitrequest req2 = new Approval.Processsubmitrequest();
		req2.setObjectId(alr3.Id);
		Approval.Processresult result2 = Approval.process(req2);
		List<Id> itemIds = result2.getNewWorkitemIds();
		Approval.Processworkitemrequest req3 = new Approval.Processworkitemrequest();
		req3.setComments('Approving request');
		req3.setAction('Approve');
		req3.setWorkitemId(itemIds.get(0));
		Approval.Processresult result3 = Approval.process(req3);
		List<Id> item2Ids = result3.getNewWorkitemIds();
		Approval.Processworkitemrequest req4 = new Approval.Processworkitemrequest();
		req4.setComments('Approving request');
		req4.setAction('Approve');
		req4.setWorkitemId(item2Ids.get(0));
		Approval.Processresult result4 = Approval.process(req4);
		*/
		
		List<Account_Link_Request__c> alrList = [Select Id From Account_Link_Request__c];
		System.assertEquals(3,alrList.size());
		ApexPages.Standardsetcontroller setController = new ApexPages.Standardsetcontroller(alrList);
		setController.setSelected(alrList);
		System.assertEquals(3,setController.getSelected().size());
		MassApproveAccountLinkRequestExtension massApprove = new MassApproveAccountLinkRequestExtension(setController);
		massapprove.getOperationOptions();
		/*
		for(Integer i = 0;i<100;i++){
			massApprove.selectedAccountLinkRequests[i].operation ='Approve';
		}
		*/
		massApprove.selectedAccountLinkRequests[0].operation ='unprocessed';
		massApprove.selectedAccountLinkRequests[1].operation = 'Approve';
		massApprove.selectedAccountLinkRequests[2].operation = 'Reject';
		massApprove.submit();
		
	}
}