@isTest
private class Lepus_WisdomLeadWriteBackHandlerTest {
	static testmethod void myUnitTest(){
		Profile p = [SELECT Id FROM Profile WHERE Name='Integration Profile for Lepus']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser_x@testorg.com');
		
		insertFieldDependency('Country_HW__c', 'Uzbekistan', 'Country_Code_HW__c', 'UZ');
		insertFieldDependency('Representative_Office__c', 'Uzbekistan', 'Country_HW__c', 'Uzbekistan');
		insertFieldDependency('Region_Hw__c', 'Central Asia Region', 'Representative_Office__c', 'Uzbekistan');
		
		
		Lead l = new lead();
		l.RecordTypeId = CONSTANTS.OVERSEALEADRECORDTYPE;
		l.LastName = 'test';
		l.Company = 'test';
		l.country_code__c = 'UZ';
		System.runAs(u){
			insert l;
		}
		
	}
	
	static void insertFieldDependency(String cf, String cv, String df, String dv){
		Field_Dependency__c fd5 = new Field_Dependency__c();
		fd5.Controlling_Field__c = cf;
		fd5.Controlling_Field_Value__c = cv;
		fd5.Dependent_Field__c = df;
		fd5.Dependent_Field_Value__c = dv;
		fd5.Object_Type__c = 'Account';
		
		insert fd5;
	}
}