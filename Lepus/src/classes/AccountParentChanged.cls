/**
 * The controller of corresponding AccountParentChanged.page ,used in the change parent account button on account
 * The user need confirm on some situation,so can't update the parent field in page layout directly
 * Author           : BLS
 * Create Date      : 2013/3
 *     
 */  
public without sharing class AccountParentChanged {
   public Account acc {get;set;}
   public Boolean notNA {get;set;}//非NA客户
   public Boolean needConfirm {get;set;}//加入transient关键字以保证该字段每次重新加载时都会更新
   public Account_Link_Request__c request {get;set;}
   public String originParentId {get;set;}
   public Boolean ableToModify {get;private set;}
   public Boolean isAccountLinkCanceled {get; set;}
   
   public AccountParentChanged(ApexPages.StandardController stdCon){
     this.acc = [select id,Is_Named_Account__c,ParentId ,name,ownerid,CIS__c,Is_Parent_Account__c,Inactive_NA__c,Representative_Office__c,Province__c,City_Huawei_China__c,Industry,Sub_industry_HW__c,Customer_Group__c,Customer_Group_Code__c
                        from Account where Id = : stdCon.getId()];
                notNA = true;
     originParentId = acc.ParentId;
                if(acc.Is_Named_Account__c){//na客户
                        notNA = false;
                }
                UserRecordAccess access ;
     ableToModify = true;
     try{
         access = [SELECT RecordId, HasReadAccess, HasEditAccess
         FROM UserRecordAccess
         WHERE UserId = :UserINfo.getUserId()
         AND RecordId = :stdCon.getId() limit 1];
                }catch(QueryException e){
                }
     if(access == null || !access.HasEditAccess){
        ableToModify = false;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 
         System.Label.No_Privilege));//判断当前用户是否有权限进行修改
        }
     request = new Account_Link_Request__c();
     needConfirm = false;
     isAccountLinkCanceled = false;
   }
     
     public PageReference startApproval(){
         request.child__c = acc.id;
         //System.debug('parent id:-----' + acc.ParentId);
         request.parent__c = acc.Parentid;
         insert request;
         Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
         req.setObjectId(request.id);
         //req.setNextApproverIds(new List<Id>{request.Approver__c});
         // Submit the approval request for the account
         Approval.ProcessResult result = Approval.process(req);
       
         return new ApexPages.StandardController(request).view();
     }
        
     public PageReference validation(){
        isAccountLinkCanceled = false;
        //失效的NA不允许提交靠
        if(acc.Inactive_NA__c){  
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'失效的NA客户，不允许提交挂靠申请'));
            return null;
        }
        //父客户未做调整，请修改后再提交
        if(originParentId == acc.ParentId){
            //return new ApexPages.StandardController(acc).view();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'父客户未做调整，请修改后再提交'));
            return null;
        }
        //当前客户存在审批中的挂靠申请，不允许再次提交
        if(checkHasRequest()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'当前客户存在审批中的挂靠申请，不允许再次提交'));
            return null;
        }
        
        if(acc.ParentId == null){
            Boolean canRemove = true;
            canRemove = false;//只有销售管理策略经理和客户Owner和客户团队成员可以解除同父客户的关联关系
            if(acc.ownerid == UserINfo.getUserId()){
                canRemove = true;
            }
            List<UserRole> roles = [select DeveloperName  from UserRole where id =:UserInfo.getUserRoleId()];
            if(roles.size() > 0 && roles[0].DeveloperName == 'Huawei_China1'){
                canRemove = true;
            }
            if(roles.size() > 0 && roles[0].DeveloperName == 'Huawei_China2'){
                canRemove = true;
            }
            List<AccountTeamMember> members = [Select UserId, AccountId From AccountTeamMember where AccountId = :acc.id 
                and UserId =:UserINfo.getUserId() ];
            if(members.size() > 0){
                canRemove = true;
            }
            
            if(!canRemove){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,
                     '只有销售管理策略经理/客户Owner/客户团队成员可以解除同父客户的关联关系'));
                return null;
            }else{
                //Add by lyx 20131021, start
                //解除同父客户的关联关系时，区域/系统部、省份、城市、客户群及行业信息必填
                if(request.Industry__c == null 
                || request.Sub_Industry__c == null 
                || request.Customer_Group__c == null 
                || request.Customer_Group_Code__c == null 
                || request.Representative_Office__c == null 
                || request.Province__c == null 
                || request.City__c == null){
                    isAccountLinkCanceled = true;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,
                        '解除同父客户的关联关系时，区域/系统部、省份、城市、客户群及行业信息必填'));
                    return null;
                }
                //当前客户存在审批中的客户信息修改申请，不允许解除同父客户的关联关系
                if(checkHasAccountUpdateRequest())
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'当前客户存在审批中的客户信息修改申请，不允许解除同父客户的关联关系'));
                    return null;
                }
                //Add by lyx 20131021, end.
                //Modify by lyx 20131016, 解除挂靠关系时查出当前用户所在区域的销管，用于发送邮件通知销管。
                //return save();
                if(autoFetchSalesManager()) {
                    Account originParent = [select name from Account where Id = : originParentId];
                    acc.Origin_Parent_Name__c = originParent.name;
                    //return save();
                    return saveAndStartApproval();//Modify by lyx 20131021, 解除父子关系，同时创建客户信息修改申请
                }
                else{
                    return null;
                }
                //Modify by lyx 20131016, end.
            }   
        }
        if(acc.ParentId == acc.id){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'不能关联到客户自身'));
            return null;
        }
        
        //add by steven 20130617 start
        if(acc.Is_Parent_Account__c == 'YES'){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'不允许将NA父客户挂靠到另外一个NA客户下'));
            return null;
        }
        //add by steven 20130617 end 
        
        try{
            Account parent = [select id,Is_Named_Account__c,ParentId from Account where id = :acc.ParentId];
            //System.debug('the parent:------' + parent);
            if(parent.Is_Named_Account__c){
                
                //add by steven 20130617 start
                if(parent.ParentId != null){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'挂靠层级关系不允许超过二级,该父客户上面还有根父客户'));
                    return null;
                }
                List<Account> accChild = [Select id From Account where ParentId = :acc.id];
                if(accChild.size() > 0){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'挂靠层级关系不允许超过二级,该客户下关联有孙客户'));
                    return null;
                }
                //add by steven 20130617 end
                
                if(acc.CIS__c == null){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'子客户挂靠NA父客户必须先申请CIS编码'));
                    return null;
                }

                //add by steven 20130729 start
                //校验该客户下的SS7以前状态的机会点中可参与空间、项目进展是否为空，如果为空，不允许提交挂靠申请
                List<Opportunity> oppList = [ select id,name from opportunity
                                               where (Project_Progress__c = null or Participate_Space__c = null ) 
                                                 and StageName in ('SS1 (Notifying Opportunity)','SS2 (Identifying Opportunity)','SS3 (Validating Opportunity)','SS4 (Developing Solution)','SS5 (Gaining Agreement)','SS6 (Winning)') 
                                                 and accountId =:acc.Id];

                if(oppList.size()>0) {
                    String names = '';
                    for(Opportunity op:oppList) {
                        names = names + op.name + ',';
                    }
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'该客户下存在SS7以前状态机会点('+ names +')的可参与空间、项目进展为空，请维护后再提交!'));
                    return null;
                }
                //add by steven 20130729 end
                
                //modify by steven 20130729 start
                //自动获取审批人
                if(autoFetchApprover()) {
                    return startApproval();
                }
                //needConfirm = true;
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'关联到NA父客户需要发起审批流程'));
                //modify by steven 20130729 end
                return null;
            }else{
                //modify by steven 20130821 start
                //商业客户不允许挂靠父子关系
                //return save();
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'商业客户不允许挂靠父子关系'));
                return null;
                //modify by steven 20130821 end
            }
        }catch(QueryException e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'父客户: 找不到匹配目标。'));
            return null;
        }
    }
     
    public PageReference save(){
        try{
            update acc;
            return new ApexPages.StandardController(acc).view();
        }catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }
    }
    
    /**
     * 从Map表中自动获取当前用户所在区域的审批人
     *  add by steven 20130729
     */
    private Boolean autoFetchApprover(){
        Boolean result = false;
        String sApproverMapRecordTypeName = 'Huawei_China_Approver';
        Id ApproverMapRecordType = [select id from RecordType where DeveloperName =: sApproverMapRecordTypeName].id;
        User currentUser = [Select Id, Name,Country__c FROM User where Id = : UserINfo.getUserId()];
        List<Account_Link_Approver_Map__c> approvers = [Select Approver__c FROM Account_Link_Approver_Map__c 
                                                         where Representative_Office__c = : currentUser.Country__c 
                                                         and RecordTypeId = :ApproverMapRecordType 
                                                         and Type__c = '主管'];//Add by lyx 20131016, 增加维护销管信息，按Type字段区分。
        if(approvers.size() > 0){
            User currentApprover = [Select IsActive, Name FROM User where Id = : approvers[0].Approver__c];
            if(!currentApprover.IsActive) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'您所属区域的审批人 " '+currentApprover.Name+' " 已经失效，请联系销业龙思宇维护!'));
            } else {
                request.Approver__c = approvers[0].Approver__c;
                result = true;
            }
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'您的所属区域 " '+currentUser.Country__c + ' " 没有维护相应的审批人，请联系销业龙思宇维护!'));
        }
        return result;
    }
    
    /**
     * 检查客户下是否已经提交挂靠申请
     *  add by steven 20130829
     */
    private Boolean checkHasRequest(){
        Boolean result = false;
        List<Account_Link_Request__c> oldRequests = [Select id FROM Account_Link_Request__c 
                                                                                                    where Process_End__c = false and child__c = : acc.id];
        if(oldRequests.size() > 0){
            result = true;
                }
        return result;
    }
        
    /**
     * 从Map表中自动获取当前用户所在区域的销管
     *  add by lyx 20131016
     */
    private Boolean autoFetchSalesManager()
    {
        Boolean result = false;
        String sApproverMapRecordTypeName = 'Huawei_China_Approver';
        Id ApproverMapRecordType = [select id from RecordType where DeveloperName =: sApproverMapRecordTypeName].id;
        User currentUser = [Select Id, Name,Country__c FROM User where Id = : UserINfo.getUserId()];
        List<Account_Link_Approver_Map__c> approvers = [Select Approver__c FROM Account_Link_Approver_Map__c 
                                                         where Representative_Office__c = : currentUser.Country__c 
                                                         and RecordTypeId = :ApproverMapRecordType 
                                                         and Type__c = '销管'];
        if(approvers.size() > 0){
            User currentApprover = [Select IsActive, Name FROM User where Id = : approvers[0].Approver__c];
            if(!currentApprover.IsActive) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'您所属区域的销管 " '+currentApprover.Name+' " 已经失效，请联系销业龙思宇维护!'));
            } else {
                acc.Sales_Manager__c = approvers[0].Approver__c;
                result = true;
            }
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'您的所属区域 " '+currentUser.Country__c + ' " 没有维护相应的销管，请联系销业龙思宇维护!'));
        }
        return result;
    }
    
    /**
     * 检查客户下是否有已经提交的客户信息修改申请
     *  add by lyx 20131021
     */
    private Boolean checkHasAccountUpdateRequest(){
        Boolean result = false;
        List<Account_Info_Update_Request__c> oldRequests = [Select id FROM Account_Info_Update_Request__c 
                                                            where Process_End__c = false and account__c = : acc.id];
        if(oldRequests.size() > 0){
            result = true;
        }
        return result;
    }
        
    /**
     * 解除父子关系，同时创建客户信息修改申请
     *  add by lyx 20131021
     */
    public PageReference saveAndStartApproval(){
        Account_Info_Update_Request__c r = new Account_Info_Update_Request__c();
        r.Account__c = acc.id;
        r.Account_Name_old__c = acc.Name;
        r.Representative_Office_Old__c = acc.Representative_Office__c;
        r.Province_Old__c = acc.Province__c;
        r.City_Old__c = acc.City_Huawei_China__c;
        r.Industry_Old__c = acc.Industry;
        r.Sub_Industry_Old__c = acc.Sub_industry_HW__c;
        r.Customer_Group_Old__c = acc.Customer_Group__c;
        r.Customer_Group_Code_Old__c = acc.Customer_Group_Code__c;
        r.Is_Named_Account__c = false;
        r.Account_Name__c = acc.Name;
        r.Representative_Office__c = request.Representative_Office__c;
        r.Province__c = request.Province__c;
        r.City__c = request.City__c;
        r.Industry__c = request.Industry__c;
        r.Sub_Industry__c = request.Sub_Industry__c;
        r.Customer_Group__c = request.Customer_Group__c;
        r.Customer_Group_Code__c = request.Customer_Group_Code__c;
        try{
            update acc;
            insert r;
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            req.setObjectId(r.id);
            Approval.ProcessResult result = Approval.process(req);
            return new ApexPages.StandardController(r).view();
        }catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }
    }
}