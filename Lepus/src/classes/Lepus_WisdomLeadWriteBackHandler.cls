public class Lepus_WisdomLeadWriteBackHandler implements Triggers.Handler{
    public static boolean isFirstRun = true;
    public void Handle(){
        if(Lepus_WisdomLeadWriteBackHandler.isFirstRun){
            isFirstRun = false;
            
            if(UserInfo.getUserId() != '00590000003DBRhAAO' && !System.test.isRunningTest())
            return;
            
            //User u = [Select Id, Profile.Name From User Where Id =: UserInfo.getUserId()];
            //if(u.Profile.Name != 'Integration Profile for Lepus')
            //return;
            
            Lepus_WisdomLeadWriteBackHandler.updateControllingFields();
        }
    }
    

   public static void updateControllingFields(){
        
        // 海外 Country_Code_HW__c -> Country_HW__c
        Map<String, String> countryMap = new Map<String, String>(); 
        // 海外 Country_HW__c -> Representative_Office__c
        Map<String, String> overseaRepOfficeMap = new Map<String, String>();
        // 海外 Representative_Office__c -> Region_Hw__c
        Map<String, String> regionHwMap = new Map<String, String>();
        
        for(Field_Dependency__c fd : [Select Id, Controlling_Field__c, Controlling_Field_Value__c, Dependent_Field__c, 
                                            Dependent_Field_Value__c, Object_Type__c From Field_Dependency__c Where Object_Type__c = 'Account']){
            if(fd.Dependent_Field__c == 'Representative_Office__c'){
                regionHwMap.put(fd.Dependent_Field_Value__c, fd.Controlling_Field_Value__c);
            } else if(fd.Dependent_Field__c == 'Country_HW__c'){
                overseaRepOfficeMap.put(fd.Dependent_Field_Value__c, fd.Controlling_Field_Value__c);
            } else if(fd.Dependent_Field__c == 'Country_Code_HW__c'){
                countryMap.put(fd.Dependent_Field_Value__c, fd.Controlling_Field_Value__c);
            }
        }
        
        for(Lead l : (List<Lead>)trigger.new){
            if(!Lepus_HandlerUtil.filterRecordType(l, Lead.sobjecttype))
            continue;
            
            String recordType = Lepus_HandlerUtil.getRecordType(l, Lead.sobjecttype);
            if(recordType == '海外'){
                
                if(l.Country_Code__c != null && countryMap.get(l.Country_Code__c) != null){ 
                    l.Country__c = countryMap.get(l.Country_Code__c);
                    l.Representative_Office__c = overseaRepOfficeMap.get(l.Country__c);
                    l.Region_HW__c = regionHwMap.get(l.Representative_Office__c);
                }
            }
        }
    }
}