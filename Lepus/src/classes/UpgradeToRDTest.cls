/*
* Only remove the original currClsTest to a seperate test class,so the test method wont touch the existing data
*
*/
@isTest
private class UpgradeToRDTest {

    static testMethod void currClsTest() {
          EdObject.currClsTest();
        EdField.clsTest();
        EdEmail.clsTest();
        //简档更为HPE简档     (CSS) HQ Product Engineer(HPE)
        //Profile pf=[select id,Name from Profile where Name like '%(CSS)国内总部服务产品工程师(HPE)%' limit 1];
        String whereClause = ' ( Profile.Name  like \'%(CSS)国内总部服务产品工程师(HPE)%\' or Profile.Name  like \'%(CSS)国内总部服务产品工程师(PSE)%\' ) ';
        User u = UtilUnitTest.queryUser(whereClause);
 //       ApexPages.StandardController sc = new ApexPages.StandardController(Case); 
        System.debug('/////User ID:'+u.id);
//          System.debug('Current User: ' + UserInfo.getUserName());
//          System.debug('Current Profile: ' + UserInfo.getProfileId());
				//Product_HS__c p = [select id from Product_HS__c where Offering_Code__c != null limit 1];
				Case cs = UtilUnitTest.createChinaServiceRequest();
				cs.Status ='New' ;
				cs.ownerid = u.id;
				cs.HW_Product__c ='a0G90000008EXDEEA4';
				cs.PSE_Actual_Start_Time__c=system.now();
				update cs;
        cs=[Select c.id
            ,c.Owner.Id
            ,c.Owner.ProfileId
            from Case c where Closed__c='False' and id=:cs.id limit 1];     
        
        //https://c.cs5.visual.force.com/apex/VPUpdateCase?scontrolCaching=1&id=500O0000000lR83
        //'apex/UpgradeToRD?id='+cs.Id+'&scontrolCaching=1'
        System.runAs(u){
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId());
            PageReference pr=new PageReference('apex/UpgradeToRD?id='+cs.id+'&scontrolCaching=1');
            pr.setRedirect(true);
            Test.setCurrentPage(pr);                        
            UpgradeToRD up = new UpgradeToRD();  
//          up.getId();
            up.currId=ApexPages.currentPage().getParameters().get('id');        
            up.init1();
//          up.submit(); 
            Boolean b=up.haveError;         
//          String iid=up.currId;           
        }   
        
        Profile pf1=[select id,Name from Profile where Name  like '%HRE%' or name like '%CSE%' limit 1];
        //UtilUnitTest.createUser(pf1);
        User u3 = [select Id,profileId,IsActive from User where IsActive=true limit 1];
        User u1 = new User(Alias = 'newUser', Email='newuser@testorg.com', Employee_ID__c = '12345678',
         EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
         LocaleSidKey='en_US', ProfileId = pf1.Id, Account_Approver__c = u3.id,
         TimeZoneSidKey='America/Los_Angeles', UserName='newuser@hw.com');
        System.runAs(u1){
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId());
            PageReference pr=new PageReference('apex/UpgradeToRD?id='+cs.id);
            pr.setRedirect(false);
            Test.setCurrentPage(pr);             
            UpgradeToRD up = new UpgradeToRD(); 
            up.getId();
            up.currId=cs.id;            
            up.init1();
    //      up.submit();
            Boolean b=up.haveError;         
            String iid=up.currId;           
        }    
    }
}