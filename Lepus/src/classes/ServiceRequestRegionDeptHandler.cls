/*
*@Purpose:rewrite the Tig_OnServiceRequest trigger,perform the same function:
*在新建或更新客户时,一并更新自身及work order上的 region dept字段
*
*@Author:Brain Zhang
*Create Date：2013-9
*/
public without sharing class ServiceRequestRegionDeptHandler implements Triggers.Handler{
	public static boolean shouldRun = true;
	public void handle(){
		if(!CommonConstant.serviceRequestTriggerShouldRun){
			return ;
		}
		if(!shouldRun){
			return ;////执行前进行判断,避免循环调用
		}
			if(Trigger.isInsert){//before insert
				Set<Id> accIdSet = new Set<Id>();
	      for(Case c : (List<Case>)trigger.new){
	        accIdSet.add(c.AccountId);
	      }
	      if(accIdSet.isEmpty()){//如果无必要进行搜索,直接返回
	      	return ;
	      }
				Map<Id, Account> accMap = new Map<Id, Account>([SELECT Id, Region_Dept__c FROM Account 
					WHERE Id IN: accIdSet]);
				for(Case c : (List<Case>)trigger.new){
					if(accMap.get(c.AccountId) != null){
						c.Region_Dept__c = accMap.get(c.AccountId).Region_Dept__c;//设置Region_Dept__c字段为对应客户的Region_Dept__c值
					}
				}
			}else if(Trigger.isUpdate){//before update
				Set<Id> accIdSet = new Set<Id>();
				Set<Id> newSRIdSet = new Set<Id>();
				for(Case c : (List<Case>)trigger.new){
					if(c.AccountId != ((Case)trigger.oldmap.get(c.id)).AccountId){//只在客户字段变化时继续处理
						accIdSet.add(c.accountId);
						newSRIdSet.add(c.id);
					}
				}
				if(accIdSet.size() > 0){
					Map<Id, Account> accMap = new Map<Id, Account>([SELECT Id, Region_Dept__c FROM Account 
						WHERE Id IN: accIdSet]);
					List<Work_Order__c> relatedWOList = [SELECT Id, Service_Request_No__c, Account_Name__c,
	                  Region_Dept__c FROM Work_Order__c
	                        WHERE Service_Request_No__c IN: newSRIdSet];
					for(Case c : (List<Case>)trigger.new){
						if(c.AccountId == ((Case)trigger.oldmap.get(c.id)).AccountId){//只处理变化的情况
							continue;
						}
						if(accMap.get(c.AccountId) == null){//加入为空判断
							continue;
						}
						c.Region_Dept__c = accMap.get(c.AccountId).Region_Dept__c;//设置Region_Dept__c字段为对应客户的Region_Dept__c值
						 for (Work_Order__c wo : relatedWOList){
	              if (wo.Service_Request_No__c == c.Id){//设置 work order子记录的相关值
	                      wo.Account_Name__c = c.AccountId;
	                      wo.Region_Dept__c = c.Region_Dept__c;
	                   }
	                }
				}
				CommonConstant.workOrderTriggerShouldRun = false;//禁止相关workorder trigger的触发
				update relatedWOList;
				}	
			}
	}
}