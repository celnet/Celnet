/*
*@Purpose:rewrite the TigOnCase trigger,perform the same function:
*根据owner的变化自动设置相关时间值
*@Author:Brain Zhang
*Create Date：2013-9
*/
public without sharing class ServiceRequestOwnerChangedHandler implements Triggers.Handler {
		public static boolean shouldRun = true;
		public void handle(){//before insert,before update
		if(!CommonConstant.serviceRequestTriggerShouldRun){
			return ;
		}
		if(!shouldRun){
			return ;
		}
		Set<id> userIds = new Set<id>();
		for(Case c:(List<Case>)Trigger.new){
			if(c.OwnerId != ((Case)Trigger.oldMap.get(c.id)).OwnerId){//owner is changed
				userIds.add(c.OwnerId);
				userIds.add(((Case)Trigger.oldMap.get(c.id)).OwnerId);
			}
		}
		if(!userIds.isEmpty()){
			Map<Id,User> ownerMap = new Map<Id,User>([Select u.Id,u.EmployeeNumber,u.Name,u.Profile.Name, u.Profile.Id, u.ProfileId 
            From User u
            where u.Id in :userIds]);
      for(Case c:(List<Case>)Trigger.new){
      	if(c.OwnerId != ((Case)Trigger.oldMap.get(c.id)).OwnerId && ownerMap.get(c.ownerid) != null){//owner is changed
      		System.debug('the ownerMap.get(c.ownerid):--------' + ownerMap.get(c.ownerid));
      		if( !ownerMap.get(c.ownerid).Profile.Name.contains('CCR') &&
      			!ownerMap.get(c.ownerid).Profile.Name.contains('HRE') &&
      			!ownerMap.get(c.ownerid).Profile.Name.contains('HPE') &&
      			!ownerMap.get(c.ownerid).Profile.Name.contains('CSE') &&
      			!ownerMap.get(c.ownerid).Profile.Name.contains('PSE') &&
      			!ownerMap.get(c.ownerid).Profile.Name.contains('管理员') &&
      			!ownerMap.get(c.ownerid).Profile.Name.contains('System Administrator')){
      			c.addError('Only CCR, HRE(CSE) and HPE(PSE) can be the service request\'s owner. 只有CCR、HRE(CSE)、HPE(PSE)和管理员可以为服务请求的所有人。');
      		}else{
      		User originUser = ownerMap.get(((Case)Trigger.oldMap.get(c.id)).ownerid);
      		System.debug('the profile name in trigger:---------------' + ownerMap.get(c.ownerid).Profile.Name);
	      		if((ownerMap.get(c.ownerid).Profile.Name.contains('HRE') ||
	      				ownerMap.get(c.ownerid).Profile.Name.contains('CSE'))
	      				&&(originUser == null || (!originUser.Profile.Name.contains('HRE') && !originUser.Profile.Name.contains('CSE')))
	      				){
	      			 c.HRE_Start_Time__c=System.now();
	      		}
	      		if((ownerMap.get(c.ownerid).Profile.Name.contains('HPE') ||//HPE或PSE用户第一次作为case owner
	      				ownerMap.get(c.ownerid).Profile.Name.contains('PSE') )
	      				&&(originUser == null || (!originUser.Profile.Name.contains('HPE') && !originUser.Profile.Name.contains('PSE')))
	      				 ){
	      			c.HPE_Start_Time__c=System.now();
	      		}
	      		if((ownerMap.get(c.ownerid).Profile.Name.contains('HPE') ||//HPE或PSE用户第一次作为case owner
	      				ownerMap.get(c.ownerid).Profile.Name.contains('PSE') ) && originUser!=null && (
	      					(originUser.Profile.Name.contains('HRE') ||
	      						originUser.Profile.Name.contains('CSE')
	      				))){
	      			if(c.HRE_Actual_Time__c == null){//只在字段为空时才填充该字段,避免覆盖icare回传值
		      			c.HRE_Actual_Time__c=System.now(); 
	      			}
	      		}
      		}
      	}
      }//for
		}
	}
}