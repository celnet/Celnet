/*
* @Purpose: Handle case comments fake field
*
* @Author: Tommy Liu(tommyliu@celnet.com.cn)
* @Date: 2014-2-11
*/
public class EditCaseWithoutConflictCheck 
{
	private final ApexPages.StandardController stdController;
	public String comments {get; set;}
	
	public EditCaseWithoutConflictCheck(ApexPages.StandardController stdController) 
	{
		this.stdController = stdController;
	}
	
	public System.PageReference save()//override "save" action of std controller
	{
		System.PageReference pf = null;
		try
		{
			//避免页面的旧值修改了数据库的新值，在用户打开页面过程中后台的中间件可能已经将值更新
			Case newCs = [Select Id, Problem_Priority__c, Type From Case Where Id =: stdController.getId()];
			Case cs = (Case)this.stdController.getRecord();
			Boolean isNeedWaring = false;
			String flist = '';
			if(cs.Type != newCs.Type)
			{
				flist += Schema.SobjectType.Case.Fields.Type.Label + ', ';
				isNeedWaring = true;
			}
			if(cs.Problem_Priority__c != newCs.Problem_Priority__c)
			{
				flist += Schema.SobjectType.Case.Fields.Problem_Priority__c.Label + ', ';
				isNeedWaring = true;
			}
			if(isNeedWaring)
			{
				ApexPages.Message wMsg = new ApexPages.Message(ApexPages.Severity.WARNING , System.Label.VF_CaseEdit_Label_FieldUpdatedWarning + flist);
				ApexPages.addMessage(wMsg);
			}
			cs.Problem_Priority__c = newCs.Problem_Priority__c;
			cs.Type = newCs.Type;
			
			pf = this.stdController.save();
			if(isNeedWaring)
			{
				pf = null;//遇到警告，不跳转页面
			}
			//save case comments
			if(this.comments != null && this.comments != '')
			{ 
				CaseComment cc = new CaseComment();
				cc.ParentId = this.stdController.getId();
				cc.IsPublished = false;
				cc.CommentBody = this.comments;
				insert cc;
			}
			this.comments = null;
		}
		catch(DmlException ex)
		{
    	ApexPages.addMessages(ex);
    	pf = null;
    }
    return pf;
    //return null;
	}
}