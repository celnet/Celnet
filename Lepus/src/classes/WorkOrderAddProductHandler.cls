public without sharing class WorkOrderAddProductHandler  implements Triggers.Handler{
	public void handle(){
		List<Id> sfpIdList=new List<Id>();
		Map<Id,Id> woIdSFPMap=new Map<Id,Id>();//woid,sfpid //每个WO对应的SFP Id
		for(Work_Order__c wo:(List<Work_Order__c>)Trigger.new){
			if(wo.SFP_No__c!=null){
				woIdSFPMap.put(wo.id,wo.SFP_No__c);
				sfpIdList.add(wo.SFP_No__c);
			}
		}
		if(sfpIdList.size() > 0){
			List<SFP_Product__c> sfppductList=[Select s.SFP__c
				, s.Contract_Product_PO__r.Id, s.Contract_Product_PO__c 
				, s.Contract_Product_PO__r.Quantity__c, s.Quantity__c
				From SFP_Product__c s
				where s.SFP__c in :sfpIdList]; //WO对应的所有的SFP_Product__c
			List<Work_Order_Product__c> wopList=new List<Work_Order_Product__c>();
			for(Work_Order__c wo:(List<Work_Order__c>)Trigger.new){
				for(SFP_Product__c sfpp:sfppductList){
					if(sfpp.SFP__c==wo.SFP_No__c){
						wopList.add(
							new Work_Order_Product__c(
								 Contract_Product_PO__c=sfpp.Contract_Product_PO__c
								,Work_Order__c=wo.id
								,Quantity__c=sfpp.Quantity__c
						));
					}
				}
			}
			insert wopList;
		}
	}
}