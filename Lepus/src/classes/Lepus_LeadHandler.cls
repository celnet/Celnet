/*
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-8-27
 * Description: 同步到EIP
 */
public class Lepus_LeadHandler implements Triggers.Handler{
	public static boolean isFirstRun = true;
    public void handle(){
    	
    	// 触发记录日志
    	List<Lepus_Log__c> logList = new List<Lepus_Log__c>();
		
        List<Id> leadIdList = new List<Id>();
		String action = Lepus_HandlerUtil.retrieveAction();
		List<Lead> leadList = Lepus_HandlerUtil.retrieveRecordList();
		Map<Id , Lead> map_lead = new Map<Id,Lead>();
		
		for(Lead l : leadList){
			if(Lepus_HandlerUtil.filterRecordType(l, Lead.sobjecttype)){
				Lepus_Log__c log = new Lepus_Log__c();
				log.RecordId__c = l.Id;
				log.LastModifiedDate__c = l.LastModifiedDate;
				log.UniqueId__c = l.Id + l.LastModifiedDate.formatGMT('yyyy-MM-dd HH:mm:ss');
				log.InHandler__c = true;
				logList.add(log);
				
				leadIdList.add(l.Id);
				map_lead.put(l.Id , l);
			}
		}
		
		if(logList.size() > 0)
		upsert logList UniqueId__c;
			
		boolean syncBusinessData = (Lepus_Data_Sync_Controller__c.getInstance('线索业务数据') != null) && 
										Lepus_Data_Sync_Controller__c.getInstance('线索业务数据').IsSync__c;
        boolean syncTeamMember = (Lepus_Data_Sync_Controller__c.getInstance('线索团队') != null) && 
        								Lepus_Data_Sync_Controller__c.getInstance('线索团队').IsSync__c;
        boolean syncLeadHistory = (Lepus_Data_Sync_Controller__c.getInstance('线索字段更新') != null) &&
        							    Lepus_Data_Sync_Controller__c.getInstance('线索字段更新').IsSync__c;

        if(syncBusinessData){
        	if(leadIdList.size() == 1 && Limits.getFutureCalls() < Limits.getLimitFutureCalls()){
        		if(syncTeamMember && action.toLowerCase().equals('insert')){
        			Lepus_FutureCallout.syncDataAndTeamMember(leadIdList[0], 'lead', '');
        		} else {
        			Lepus_FutureCallout.syncData(leadIdList[0], action, 'lead');
        		}
        		
        		List<Lepus_Log__c> logList2 = new List<Lepus_Log__c>();
        			
    			for(Lead l : map_lead.values()){
    				Lepus_Log__c log = new Lepus_Log__c();
    				log.UniqueId__c = l.Id + l.LastModifiedDate.formatGMT('yyyy-MM-dd HH:mm:ss');
    				log.InFuture__c = true;
    				logList2.add(log);
    			}
    			
    			upsert logList2 UniqueId__c;
    			
			} else if(leadIdList.size() > 0){
				Lepus_SyncUtil.initQueue(map_lead, leadIdList, action, '业务数据同步', datetime.now());
				
				List<Lepus_Log__c> logList3 = new List<Lepus_Log__c>();
        			
    			for(Lead l : map_lead.values()){
    				Lepus_Log__c log = new Lepus_Log__c();
    				log.UniqueId__c = l.Id + l.LastModifiedDate.formatGMT('yyyy-MM-dd HH:mm:ss');
    				log.InQueue__c = true;
    				logList3.add(log);
    			}
    			
    			upsert logList3 UniqueId__c;
    			
			}
        }
            
        if(syncTeamMember){
        	List<Id> syncLeadIdList = new List<Id>();
        	
        	if(action.toLowerCase().equals('update')){
        		for(Id leadId : leadIdList){
        			if((trigger.newMap.get(leadId)).get('OwnerId') != (trigger.oldMap.get(leadId)).get('OwnerId')){
						syncLeadIdList.add(leadId);
					} else if((trigger.newMap.get(leadId)).get('LastName') != (trigger.oldMap.get(leadId)).get('LastName')){
						syncLeadIdList.add(leadId);
					} else if((trigger.newMap.get(leadId)).get('FirstName') != (trigger.oldMap.get(leadId)).get('FirstName')){
						syncLeadIdList.add(leadId);
					} else if((trigger.newMap.get(leadId)).get('Company') != (trigger.oldMap.get(leadId)).get('Company')){
						syncLeadIdList.add(leadId);
					}
        		}
        	}else if(action.toLowerCase().equals('delete')){
        		syncLeadIdList = leadIdList;
            } 
            
            if(syncLeadIdList.size() == 1 && Limits.getFutureCalls() < Limits.getLimitFutureCalls()){
    			Lepus_FutureCallout.syncTeamMember(syncLeadIdList, 'lead', action);
    		} else if(syncLeadIdList.size() > 0){
    			Lepus_SyncUtil.initQueue(map_lead, syncLeadIdList, action, '团队成员同步', datetime.now());
    		}
        } 
            
            
        if(syncLeadHistory && action.toLowerCase().equals('update')){
        	List<Id> syncLeadIdList = new List<Id>();
        	for(Id leadId : leadIdList){
        		if((trigger.newMap.get(leadId)).get('Status') != (trigger.oldMap.get(leadId)).get('Status')){
        			syncLeadIdList.add(leadId);
        		}
        	}
        	if(syncLeadIdList.size() == 1 && Limits.getFutureCalls() < Limits.getLimitFutureCalls()){
        		Lepus_FutureCallout.syncLeadHistory(leadIdList);
        	} else if(syncLeadIdList.size() > 0){
        		Lepus_SyncUtil.initQueue(map_lead, leadIdList, action, '线索字段更新', datetime.now());
        	}
        }
    }
}