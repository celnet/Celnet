public without sharing class W3Task_ProcessApproverPermissionHandler 
{

	public static void AddPermissionForApprover(List<ProcessInstanceWorkitem> tempwiList)
	{
		//仅处理报备的记录
		String idPrefix_deal = Deal_Registration__c.sObjectType.getDescribe().getKeyPrefix();
		List<Deal_Registration__Share> listDealShare = new List<Deal_Registration__Share>();
		system.debug(tempwiList);
		for(ProcessInstanceWorkitem wi : tempwiList)
		{
			if((((String)(wi.ProcessInstance.TargetObjectId)).contains(idPrefix_deal)))//仅处理Deal的审批
			{
				Deal_Registration__Share dealShare = new Deal_Registration__Share(
					ParentId = wi.ProcessInstance.TargetObjectId,UserOrGroupId = wi.ActorId,
					AccessLevel  = 'Edit',RowCause=Schema.Deal_Registration__Share.rowCause.Share_to_approver__c);
					system.debug(dealShare);
					system.debug('==========');
				listDealShare.add(dealShare);
			}	
		}
		if(listDealShare.size()>0)
		insert listDealShare;
	}
}