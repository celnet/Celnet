@isTest
private class Lepus_WisdomAccountWriteBackHandlerTest {
	static testmethod void myUnitTest(){
		Profile p = [SELECT Id FROM Profile WHERE Name='Integration Profile for Lepus']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser_x@testorg.com');
		
		// 准备picklist数据
		DataDictionary__c dd = new DataDictionary__c();
		dd.Code__c = '79';
		dd.Object__c = 'Account';
		dd.Name__c = '能源-石油（天然气）';
		dd.Type__c = 'Customer Group';
		insert dd;
		
		Field_Dependency__c fd = new Field_Dependency__c();
		fd.Controlling_Field__c = 'Sub_Industry_Hw__c';
		fd.Controlling_Field_Value__c = '大企业二部（石油化工、燃气、煤炭）';
		fd.Dependent_Field__c = 'Customer_Group__c';
		fd.Dependent_Field_Value__c = '能源-石油（天然气）';
		fd.Object_Type__c = 'Account';
		
		insert fd;
		
		Field_Dependency__c fd2 = new Field_Dependency__c();
		fd2.Controlling_Field__c = 'Industry';
		fd2.Controlling_Field_Value__c = '大企业系统部';
		fd2.Dependent_Field__c = 'Sub_Industry_Hw__c';
		fd2.Dependent_Field_Value__c = '大企业二部（石油化工、燃气、煤炭）';
		fd2.Object_Type__c = 'Account';
		
		insert fd2;
		
		Field_Dependency__c fd3 = new Field_Dependency__c();
		fd3.Controlling_Field__c = 'Province__c';
		fd3.Controlling_Field_Value__c = '北京市';
		fd3.Dependent_Field__c = 'City_Huawei_China__c';
		fd3.Dependent_Field_Value__c = '北京市';
		fd3.Object_Type__c = 'Account';
		
		insert fd3;
		
		Field_Dependency__c fd4 = new Field_Dependency__c();
		fd4.Controlling_Field__c = 'Representative_Office__c';
		fd4.Controlling_Field_Value__c = '北京代表处';
		fd4.Dependent_Field__c = 'Province__c';
		fd4.Dependent_Field_Value__c = '北京市';
		fd4.Object_Type__c = 'Account';
		
		insert fd4;
		
		Field_Dependency__c fd5 = new Field_Dependency__c();
		fd5.Controlling_Field__c = 'Region_Hw__c';
		fd5.Controlling_Field_Value__c = 'China';
		fd5.Dependent_Field__c = 'Representative_Office__c';
		fd5.Dependent_Field_Value__c = '北京代表处';
		fd5.Object_Type__c = 'Account';
		
		insert fd5;
		
		insertFieldDependency('City_Huawei_China__c', '北京市', 'City_Code__c', '100000');
		
		insertFieldDependency('Industry', 'SMB', 'Industry_Code__c', 'CG2011');
		insertFieldDependency('Country_HW__c', 'Uzbekistan', 'Country_Code_HW__c', 'UZ');
		insertFieldDependency('Representative_Office__c', 'Uzbekistan', 'Country_HW__c', 'Uzbekistan');
		insertFieldDependency('Region_Hw__c', 'Central Asia Region', 'Representative_Office__c', 'Uzbekistan');
		
		
		// 中国区Account
		Account acc = new Account();
		acc.Name = 'xxxfadsddfasd';
		acc.RecordTypeId = CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE;
		//acc.City_Huawei_China__c = '北京市';
		acc.city_code__c = '100000';
		acc.OwnerId = UserInfo.getUserId();
		acc.Customer_Group_Code__c = '79';
		
		insert acc;
		
		
		// 海外 Account
		Account acc2 = new Account();
		acc2.Name = 'testingoversea9152';
		acc2.RecordTypeId = CONSTANTS.HUAWEIOVERSEACUSTOMERRECORDTYPE;
		acc2.Industry_Code__c = 'CG2011';
		acc2.OwnerId = UserInfo.getUserId();
		acc2.Country_Code_HW__c = 'UZ';
		Lepus_WisdomAccountWriteBackHandler.isFirstRun = true;
		insert acc2;
		
	}
	
	static void insertFieldDependency(String cf, String cv, String df, String dv){
		Field_Dependency__c fd5 = new Field_Dependency__c();
		fd5.Controlling_Field__c = cf;
		fd5.Controlling_Field_Value__c = cv;
		fd5.Dependent_Field__c = df;
		fd5.Dependent_Field_Value__c = dv;
		fd5.Object_Type__c = 'Account';
		
		insert fd5;
	}
}