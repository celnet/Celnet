/**
 * @Purpose : 发送消息到EIP接口相关方法
 * @Author : 孙达
 * @Date : 2014-07-31
 **/
public class Lepus_EIPCalloutService {
    //业务数据推送拼接XML
    public static String DataSyncXmlConcatenation(Sobject sobj, String objType, String sAction){
        //根据记录获取Json
        String sJson = '';
        if(!sAction.toLowerCase().equals('delete')){
            sJson = Lepus_UtilCallOut.concatenateBusinessDataJson(sobj , objType);
        }
        //接口配置信息
        String iAppName = 'Salesforce';
        String iType = 'incremental';
        if(sAction.toLowerCase().equals('delete')){
            iType = 'incremental_Delete';
        }
        String iAction = '';
        if(sAction.toLowerCase().equals('insert')){
            iAction = 'C';
        }else if(sAction.toLowerCase().equals('update')){
            iAction = 'U';
        }
        String iModule = objType+'_Profile';
        String iSRowIdx = (String)sobj.get('Id');
        if(objType.toLowerCase().equals('opportunity') || objType.toLowerCase().equals('account')
            || objType.toLowerCase().equals('lead') || objType.toLowerCase().equals('contact')){
            iSRowIdx = String.valueOf(sobj.get('Global_ID__c'));
        }
        
        if(sAction.toLowerCase().equals('delete')){
        	iSRowIdx = (String)sobj.get('Id');
        }
        
        //Obj.get('Global_ID__c');
        String iLastUpdateTime ;
        if(!sAction.toLowerCase().equals('delete')){
            iLastUpdateTime = Datetime.valueOf(String.valueOf(sobj.get('LastModifiedDate'))).format('MM/dd/yyyy HH:mm:ss');
        }else{
            iLastUpdateTime = datetime.now().format('MM/dd/yyyy HH:mm:ss');
        }
            
        //拼接XML
        String xml = Lepus_UtilCallOut.concatenateXml(iAppName, iType, iModule, iSRowIdx, iLastUpdateTime, sJson , iAction);
        return xml;
    }
    
    //字段更新推送拼接XML
    public static String FieldUpdateXmlConcatenation(List<OpportunityFieldHistory> list_oppFieldHistory){
        String sJson = Lepus_UtilCallOut.FieldUpdateToJson(list_oppFieldHistory);
        //接口参数
        String iAppName = 'Salesforce';
        String iType = 'FieldStatus_Sync';
        String iModule = 'Opportunity_Update';
        String iSRowIdx = '';
        if(list_oppFieldHistory[0].OpportunityId != null){
            iSRowIdx = list_oppFieldHistory[0].Opportunity.Global_ID__c;
        } 
        String iLastUpdateTime = '';
        if(list_oppFieldHistory[0].OpportunityId != null && list_oppFieldHistory[0].Opportunity.LastModifiedDate != null){
            iLastUpdateTime = Datetime.valueOf(String.valueOf(list_oppFieldHistory[0].Opportunity.LastModifiedDate)).format('MM/dd/yyyy HH:mm:ss');
        }
        String xml = Lepus_UtilCallOut.concatenateXml(iAppName, iType, iModule, iSRowIdx, iLastUpdateTime, sJson, '');
        return xml;
    }
    
    // 用户角色推送拼接XML
    public static String UserSyncXMLConcatenation(List<User> userList, String action){
        // 接口参数
        String iAppName = 'Salesforce';
        String iType = 'Incremental';
        
        String iAction = '';
        if(action == 'insert'){
            iAction = 'C';
        } else if(action == 'update'){
            iAction = 'U';
        }
        
        String iModule = 'User_Profile';
        String iSRowIdx = '';
        String iLastUpdateTime = '';
        String xml = Lepus_UtilCallOut.concatenateUserXml(userList, iAppName, iType, iAction, iModule, iSRowIdx, iLastUpdateTime);
        return xml;
    }
    
    //线索更新推送
    public static String LeadHistoryXmlConcatenation(list<LeadHistory> leadHistories){
        // 接口参数
        String iAppName = 'Salesforce';
        String iType = 'Incremental';
        String iCreateUpdate = 'C';
        String iModule = 'LeadFieldUpdateHistory_Profile';
        String iSRowIdx = '';
        String iLastUpdateTime = '';
        String xml = Lepus_UtilCallOut.concatenateLeadHistoryXml(leadHistories, iAppName, iType, iCreateUpdate, iModule, iSRowIdx, iLastUpdateTime);
        return xml;
    }
    
    //团队成员更新推送拼接XML
    public static String memberXmlConcatenation(Map<Id, List<Lepus_EIPMember>> idMemberMap, Schema.Sobjecttype sobjecttype, String action){
        Map<Id, sObject> idObjMap = Lepus_SyncUtil.querySobjectsToMap(idMemberMap.keySet(), sobjecttype);
        
        String xml = Lepus_UtilCallOut.concatenateTeamMemberXml(idMemberMap, idObjMap, sobjecttype.getDescribe().getName(), action);
        return xml;
    }
    
    //发送数据
    public static Lepus_WSDL_Info.tGetResponse sendToEIP(String sXml , String dataType , String objId , String sClientCode){
        Lepus_WSDL_Info.tGetResponse Res;
        
        Lepus_WSDL_Connect.ARTCommon_ReceiveMessage_HTTPSPort hp = new Lepus_WSDL_Connect.ARTCommon_ReceiveMessage_HTTPSPort();
        
        hp.PartyInfo = new Lepus_WSDL_PartyInfo.PartyInfo_element();
        
        Lepus_WSDL_PartyInfo.party pInfoFrom = new Lepus_WSDL_PartyInfo.party();
        pInfoFrom.name = 'ARTCommon';
        Lepus_WSDL_PartyInfo.party pInfoTo = new Lepus_WSDL_PartyInfo.party();
        pInfoTo.name = 'Huawei';
        
        hp.PartyInfo.from_x = pInfoFrom;
        hp.PartyInfo.to = pInfoTo;
        hp.PartyInfo.operationID = 'ARTCommon/ReceiveMessage/ARTWS_Outbound';
        hp.PartyInfo.operationType = 'syncRequestResponse';
        hp.PartyInfo.transactionID = objId;
        
        Lepus_WSDL_Info.tNVPairList ple = new Lepus_WSDL_Info.tNVPairList();
        
        Lepus_WSDL_Info.tNVPair pe = new Lepus_WSDL_Info.tNVPair();
        pe.name = '';
        pe.value='';
        
        ple.NVPair = pe;
        
        system.debug('XML Info in callout function:'+sXml);
        
        Res = hp.ARTWS_Outbound(sClientCode, dataType, 'v1.0', objId, '', '', '', ple, sXml);
        
        system.debug(Res);
        return Res;
    }
}