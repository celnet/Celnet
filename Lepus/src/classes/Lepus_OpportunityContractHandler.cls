/**
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-9-3
 * Description: Contract创建时发送线索同步到EIP
 */
public class Lepus_OpportunityContractHandler implements Triggers.Handler{
    public static boolean isFirstRun = true;
    public void handle(){
        if(!(trigger.isAfter && trigger.isInsert))
        return;
        
        if(Lepus_OpportunityContractHandler.isFirstRun){
            isFirstRun = false;

            boolean syncLeadHistory = (Lepus_Data_Sync_Controller__c.getInstance('线索字段更新') != null) &&
                                            Lepus_Data_Sync_Controller__c.getInstance('线索字段更新').IsSync__c;          
            
            if(!syncLeadHistory)
            return;
            
            Set<Id> oppIds = new Set<Id>();
            for(Opportunity_Contract__c contract : (List<Opportunity_Contract__c>)trigger.new){
                oppIds.add(contract.Opportunity__c);
            }
            
            List<Id> leadIds = new List<Id>();
            List<Opportunity> oppList = [Select Id, Lead__c, (Select Id From Opportunity_Contracts_Opportunity__r) From Opportunity Where Id IN: oppIds];
            for(Opportunity opp : oppList){
                if(opp.Opportunity_Contracts_Opportunity__r != null && opp.Opportunity_Contracts_Opportunity__r.size() == 1 && opp.Lead__c != null){
                    leadIds.add(opp.Lead__c);
                }
            }
            
            if(leadIds.size() > 0)
            Lepus_Futurecallout.syncLeadHistory(leadIds);
        }
    }
}