@isTest
private class OpportunityFieldsPopulaterTest {

    static testMethod void populateField() {//correct record type 
    	Account acc = UtilUnitTest.createHWChinaAccount('TEST');
    	acc.Region_HW__c = 'testr';
			acc.Representative_Office__c = 'testro';
			acc.Country_HW__c = 'testch';
			acc.Country_Code_HW__c = 'testcch';
			acc.Industry__c = 'testi';
			acc.Customer_Group__c = 'testcg';
			acc.Sub_industry_HW__c = 'testsih';
			acc.Industry = 'testi';
			acc.City_Huawei_China__c = 'testchc';
			acc.Customer_Group_Code__c = 'testcgc';
			acc.Province__c = 'testp';
			acc.blacklist_type__c='Pending';//it is added on 2013/05/25
			update acc;
			
			Opportunity opp = new Opportunity(accountid = acc.id,recordtypeid = CONSTANTS.CHINAOPPORTUNITYRECORDTYPE,closedate = Date.today(),
				name = 'testOpp' ,stagename = 'test',Opportunity_type__c='Direct Sales');
			insert opp;
			
			opp = [select id,Region__c, Representative_Office__c , Country__c  , Country_Code__c , Industry__c , 
				Customer_Group__c , Sub_Industry__c , System_Department__c , City__c , Customer_Group_Code__c , Province__c  
				from Opportunity where id = :opp.id];
			System.assertEquals('testr',opp.Region__c);
			System.assertEquals('testro',opp.Representative_Office__c);
			System.assertEquals('testch',opp.Country__c);
			System.assertEquals('testcch',opp.Country_Code__c);
			System.assertEquals('testi',opp.Industry__c);
			System.assertEquals('testcg',opp.Customer_Group__c);
			System.assertEquals('testsih',opp.Sub_Industry__c);
			System.assertEquals('testi',opp.System_Department__c);
			System.assertEquals('testchc',opp.City__c);
			System.assertEquals('testcgc',opp.Customer_Group_Code__c);
			System.assertEquals('testp',opp.Province__c);
    }
    
    static testMethod void populateFieldInUpdate() {//change the account on update
    	Account a = UtilUnitTest.createHWChinaAccount('TEST');
    	
    	Account acc = UtilUnitTest.createHWChinaAccount('TEST2');
    	acc.Region_HW__c = 'testr';
			acc.Representative_Office__c = 'testro';
			acc.Country_HW__c = 'testch';
			acc.Country_Code_HW__c = 'testcch';
			acc.Industry__c = 'testi';
			acc.Customer_Group__c = 'testcg';
			acc.Sub_industry_HW__c = 'testsih';
			acc.Industry = 'testi';
			acc.City_Huawei_China__c = 'testchc';
			acc.Customer_Group_Code__c = 'testcgc';
			acc.Province__c = 'testp';
			update acc;
			
			Opportunity opp = new Opportunity(accountid = a.id,recordtypeid = CONSTANTS.CHINAOPPORTUNITYRECORDTYPE,closedate = Date.today(),
				name = 'testOpp' ,stagename = 'test',Opportunity_type__c='Direct Sales');
			insert opp;
			
			Test.startTest();
			opp.accountid = acc.id;
			update opp;
			Test.stopTest();
			
			opp = [select id,Region__c, Representative_Office__c , Country__c  , Country_Code__c , Industry__c , 
				Customer_Group__c , Sub_Industry__c , System_Department__c , City__c , Customer_Group_Code__c , Province__c  
				from Opportunity where id = :opp.id];
			System.assertEquals('testr',opp.Region__c);
			System.assertEquals('testro',opp.Representative_Office__c);
			System.assertEquals('testch',opp.Country__c);
			System.assertEquals('testcch',opp.Country_Code__c);
			System.assertEquals('testi',opp.Industry__c);
			System.assertEquals('testcg',opp.Customer_Group__c);
			System.assertEquals('testsih',opp.Sub_Industry__c);
			System.assertEquals('testi',opp.System_Department__c);
			System.assertEquals('testchc',opp.City__c);
			System.assertEquals('testcgc',opp.Customer_Group_Code__c);
			System.assertEquals('testp',opp.Province__c);
    }
    
}