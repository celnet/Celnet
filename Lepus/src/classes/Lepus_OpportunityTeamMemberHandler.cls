/**
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-8-30
 * Description: 同步机会点团队成员到EIP
 */
public class Lepus_OpportunityTeamMemberHandler implements Triggers.Handler{
    public static boolean isFirstRun = true;
    public void handle(){
        if(Lepus_OpportunityTeamMemberHandler.isFirstRun){
            isFirstRun = false;
            
            if((Lepus_Data_Sync_Controller__c.getInstance('机会点团队') != null) && Lepus_Data_Sync_Controller__c.getInstance('机会点团队').IsSync__c){
            	List<OpportunityTeamMember> otmList = Lepus_HandlerUtil.retrieveRecordList();
            	List<Id> opportunityIds = new List<id>();
            	Map<Id, String> idGlobalIdMap = new Map<Id, String>();
            	
            	Set<Id> oppIds = new Set<Id>();
				for(OpportunityTeamMember otm : otmList){
					oppIds.add(otm.OpportunityId);
				}
				for(Opportunity opp: [Select Id, Global_Id__c, RecordTypeId From Opportunity Where Id IN: oppIds]){
					if(Lepus_HandlerUtil.filterRecordType(opp, Opportunity.sobjecttype)){
						opportunityIds.add(opp.Id);
						idGlobalIdMap.put(opp.Id, opp.Global_ID__c);
					}				
				}
            	
            	if(opportunityIds.size() == 1){
            		try{
            			Lepus_FutureCallout.syncTeamMember(opportunityIds, 'opportunity', '');
            		} catch(System.AsyncException ae){
            			Lepus_SyncUtil.initQueue(opportunityIds, idGlobalIdMap, '', '团队成员同步', datetime.now());
            		}
            	} else if(opportunityIds.size() > 1){
            		Lepus_SyncUtil.initQueue(opportunityIds, idGlobalIdMap, '','团队成员同步',datetime.now());
            	}
            }
        }
    }
}