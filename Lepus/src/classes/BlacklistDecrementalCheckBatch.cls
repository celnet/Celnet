/*
* @Purpose : it is for Decremental blacklist checking in Red Accounts 
*
* @Author : Jen Peng
* @Date : 		2013-5-4
*/
global class BlacklistDecrementalCheckBatch implements Database.Batchable<sObject>,Schedulable,Database.Stateful{
	public String query;
	public List<Account> decAcc;
	public List<Blacklist_Check_History__c> histories;
	public Integer counter;
	public Set<String> fawuEmails{get;set;}//for no duplicate fawu emails
	public List<Messaging.SendEmailResult> results{get;set;} 
	public List<User> users{get;set;}
	public Map<Id,Account> accMap{get;set;}
	public boolean isChain{get;set;}//it is added by jen on 2013-07-24
	
	global BlacklistDecrementalCheckBatch(){
		decAcc=new List<Account>();
		counter=0;
		isChain=true;//it is added by jen on 2013-07-24
		fawuEmails=new Set<String>();
		users=new List<User>();
		accMap=new Map<Id,Account>();
		histories=new List<Blacklist_Check_History__c>();
		try{
			users=[Select Id,Name,Profile.Name,Email,IsActive From User where Profile.Name='(Huawei) HQ Legal Affairs Mgmt'];
			for(User u:users){
				if(u.IsActive){
					fawuEmails.add(u.Email);
				}
			}
		}catch(Exception e){}
	}
	global Database.QueryLocator start(Database.BatchableContext bc){
		AccountEmailInformHandler.isFirstRun=false;	
		AccountAfterUpdateHandler.isFirstRun=false;
		AccountNAChangedHandler.isFirstRun=false; 
		AccountUpdateRootHandler.isFirstRun=false;
		AccountUpdateRegionDeptHandler.isFirstRun=false;
		AccountUpdateIndustryHandler.isFirstRun=false; 
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC, list<sObject> scope) {
		decAcc.clear();
		histories.clear();
		List<BlackList__c> blacklists = new List<BlackList__c>();
		Map<String,BlackList__c> Acc2BlacklistMap = new Map<String,BlackList__c>();
		
		String blacklistQuery='select id,code__c,country__c,street1__c,street2__c,city__c,state__c,Blacklist_Alias__c from BlackList__c where status__c=\'del\'';
		String cond = '';
		for(sObject s : scope){
			Account acc = (Account)s;
			
			String AccName = acc.Blacklist_Account_Alias__c;
			String Country = acc.Country_Code_HW__c;
			if(Country==null||Country==''){
				cond += ' Blacklist_Alias__c = \''+AccName+'\' OR';

			}else{
				cond +=' (Blacklist_Alias__c = \''+AccName+'\' and (country__c=\''+Country+'\' or country__c=\'XX\' or country__c=\'\')) OR';

			}
		}
		blacklistQuery += ' and ('+cond.substring(0,cond.length()-3)+')';
		
		try{
			blacklists=Database.query(blacklistQuery);
		}catch(Exception e){
		
		}
		
		for(BlackList__c b:blacklists){
			Acc2BlacklistMap.put(b.Blacklist_Alias__c,b);
		}
		
		for(sObject s : scope){
			Account acc = (Account)s;
			Blacklist_Check_History__c bch = new Blacklist_Check_History__c();
			
			if(Acc2BlacklistMap.get(acc.Blacklist_Account_Alias__c)!=null){
				counter++;
				accMap.put(acc.id,acc);
				decAcc.add(acc);
				bch.Account__c=acc.Id;
				bch.BlackList__c=Acc2BlacklistMap.get(acc.Blacklist_Account_Alias__c).id;
				bch.status_date__c=system.now();
				bch.Account_Country__c=acc.Country_Code_HW__c;
				bch.blacklist_type__c=acc.blacklist_type__c;
				bch.Last_status__c=bch.Blacklist_Status__c;
				bch.Blacklist_Status__c=acc.Blacklist_Status__c;
				bch.isRemoved__c=true;
				histories.add(bch);
			}
		}
		
		AccountEmailInformHandler.isFirstRun=false;	
		AccountAfterUpdateHandler.isFirstRun=false;
		AccountNAChangedHandler.isFirstRun=false; 
		AccountUpdateRootHandler.isFirstRun=false;
		AccountUpdateRegionDeptHandler.isFirstRun=false;
		AccountUpdateIndustryHandler.isFirstRun=false; 
		if(!histories.isEmpty()){
			insert histories;
			system.debug('histories==================================='+histories);
		}
	}
	
	global void finish(Database.BatchableContext BC){
		Integer n = fawuEmails.size();	
		system.debug('fawuEmails========================'+fawuEmails);	
		system.debug('n========================'+n);
		system.debug('counter========================'+counter);
		if(counter>0 && n>0){
			Messaging.reserveSingleEmailCapacity(n);
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			List<string> toAddress = new List<string>();
			if(fawuEmails.size()>0){
				for(String s:fawuEmails){
					toAddress.add(s);
				}
			}
			mail.setToAddresses(toAddress);
			mail.setSenderDisplayName('Important No-reply email from Salesforce System');
			mail.setSubject('There some accounts are removed from Export Control Blacklist!');
			String strbody = '<p>Dear Mr/Ms:</p>';
			strbody += '<p>There some accounts are removed from Export Control Blacklist !Please check by following link:</p>';
			for(Id i:accMap.keySet()){
				strbody +=accMap.get(i).Name +' '+ system.Url.getSalesforceBaseUrl().toExternalForm()+'/'+accMap.get(i).id+'<br/>';
			}

			mail.setHtmlBody(strbody);
			system.debug('mail========================'+mail);
			results=Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
			system.debug('results======================================='+results);
		}
		if(isChain){//it is added by jen on 2013-07-24
			BlacklistStatusChangedBatch bscb = new BlacklistStatusChangedBatch();
			bscb.query='select id,Name,status__c from BlackList__c where status__c<>\'del\'';
			Id batchId = Database.executeBatch(bscb,50);
		}
			
	}
	
	global void execute(SchedulableContext SC){
		BlacklistDecrementalCheckBatch bdcb = new BlacklistDecrementalCheckBatch();
		bdcb.query='select id,Name,Blacklist_Account_Alias__c,Country_Code_HW__c,Blacklist_Status__c,blacklist_type__c from Account where Blacklist_Status__c=\'Red\'';
		Id batchId = Database.executeBatch(bdcb,50);
	}
}