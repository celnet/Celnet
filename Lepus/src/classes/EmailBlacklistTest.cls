/*
	it is created for testing the email controller on the blacklist project
	and it is created by jen peng on 2013-05-15;
*/
@isTest
private class EmailBlacklistTest {
    static testMethod void EmailNewBlacklistMethod() {
        Test.startTest();
        	
        	Account account = UtilUnitTest.createAccount('test20130515');
        	Account a = [select id,name,Blacklist_Status__c,blacklist_type__c from Account where name='test20130515'];
        	system.assertEquals(a.blacklist_type__c,'Pending');
      		system.assertEquals(a.Blacklist_Status__c,'Gray'); 
      		a.blacklist_type__c='Tier I';
      		update a;
      		Account a1 = [select id,Blacklist_Status__c,blacklist_type__c,name,blacklist_lock__c from Account where name='test20130515']; 
      		system.assertEquals(a1.blacklist_lock__c,true);
      		system.assertEquals(a1.Blacklist_Status__c,'Red');
      		Date startDate = System.today();
			Date endDate = System.today()+1;
      		Blacklist_Check_History__c bch =[Select Account__c,Blacklist_Status__c,isNew__c,Last_status__c From Blacklist_Check_History__c 
			      where Blacklist_Status__c='Red' and Account__c=:a1.id limit 1];
			      //where status_date__c>=:startDate and status_date__c<:endDate and Blacklist_Status__c='Red' and Account__c=:a1.id limit 1];
			system.assertEquals(bch.Blacklist_Status__c,'Red');
			system.assertEquals(bch.isNew__c,true);
			system.assertEquals(bch.Last_status__c,'Gray');

			EmailNewBlacklistScheduleJob e = new EmailNewBlacklistScheduleJob();
			e.bchs.add(bch);
			e.sendEmail();
			system.assertEquals(e.results,null);
			
        Test.stopTest();
    }
    
    static testMethod void EmailReleasedBlacklistMethod() {
        // TO DO: implement unit test
        Test.startTest();
        	
        	Account account = UtilUnitTest.createAccount('test20130515');
        	Account a = [select id,name,Blacklist_Status__c,blacklist_type__c from Account where name='test20130515'];
        	system.assertEquals(a.blacklist_type__c,'Pending');
      		system.assertEquals(a.Blacklist_Status__c,'Gray'); 
      		a.blacklist_type__c='Tier I';
      		update a;
      		Account a1 = [select id,Blacklist_Status__c,blacklist_type__c,name,blacklist_lock__c from Account where name='test20130515']; 
      		system.assertEquals(a1.blacklist_lock__c,true);
      		system.assertEquals(a1.Blacklist_Status__c,'Red');
      		a1.blacklist_type__c='Pass';
      		update a1;
      		Account a2 = [select id,Blacklist_Status__c,blacklist_type__c,name,blacklist_lock__c from Account where name='test20130515']; 
      		system.assertEquals(a2.blacklist_lock__c,false);
      		system.assertEquals(a2.Blacklist_Status__c,'Green');
      		Date startDate = System.today();
			Date endDate = System.today()+1;
      		Blacklist_Check_History__c bch =[Select Account__c,Blacklist_Status__c,isNew__c,Last_status__c From Blacklist_Check_History__c 
			      where Blacklist_Status__c='Green' and Account__c=:a2.id limit 1];
			      //where status_date__c>=:startDate and status_date__c<:endDate and Blacklist_Status__c='Green' and Account__c=:a2.id limit 1];
			system.assertEquals(bch.Blacklist_Status__c,'Green');
			system.assertEquals(bch.isNew__c,true);
			system.assertEquals(bch.Last_status__c,'Red');

			EmailReleaseBlacklistScheduleJob e = new EmailReleaseBlacklistScheduleJob();
			e.bchs.add(bch);
			e.sendEmail();
			system.assertEquals(e.results,null);
			 	
        Test.stopTest();
    }
    
    static testMethod void EmailTierI2TierIIBlacklistMethod() {
        // TO DO: implement unit test
        Test.startTest();
        	
        	Account account = UtilUnitTest.createAccount('test20130515');
        	Account a = [select id,name,Blacklist_Status__c,blacklist_type__c from Account where name='test20130515'];
        	system.assertEquals(a.blacklist_type__c,'Pending');
      		system.assertEquals(a.Blacklist_Status__c,'Gray'); 
      		a.blacklist_type__c='Tier I';
      		update a;
      		Account a1 = [select id,Blacklist_Status__c,blacklist_type__c,name,blacklist_lock__c from Account where name='test20130515']; 
      		system.assertEquals(a1.blacklist_lock__c,true);
      		system.assertEquals(a1.Blacklist_Status__c,'Red');
      		a1.blacklist_type__c='Tier II';
      		update a1;
      		Account a2 = [select id,Blacklist_Status__c,blacklist_type__c,name,blacklist_lock__c from Account where name='test20130515']; 
      		system.assertEquals(a2.blacklist_lock__c,false);
      		system.assertEquals(a2.Blacklist_Status__c,'Red');
      		Date startDate = System.today();
			Date endDate = System.today()+1;
      		Blacklist_Check_History__c bch =[Select Account__c,Blacklist_Status__c,isNew__c,Last_status__c From Blacklist_Check_History__c 
			      where Blacklist_Status__c='Red' and Last_status__c='Red' and blacklist_type__c='Tier II' and Account__c=:a2.id limit 1];
			      //where status_date__c>=:startDate and status_date__c<:endDate and Blacklist_Status__c='Red' and Last_status__c='Red' and blacklist_type__c='Tier II' and Account__c=:a2.id limit 1];
			system.assertEquals(bch.Blacklist_Status__c,'Red');
			system.assertEquals(bch.isNew__c,true);
			system.assertEquals(bch.Last_status__c,'Red');

			EmailBlacklistTierI2TierIIScheduleJob e = new EmailBlacklistTierI2TierIIScheduleJob();
			e.bchs.add(bch);
			e.sendEmail();
			system.assertEquals(e.results,null);
			
        Test.stopTest();
    }
    
    static testMethod void EmailTierII2TierIBlacklistMethod() {
        // TO DO: implement unit test
        Test.startTest();
        	
        	Account account = UtilUnitTest.createAccount('test20130515');
        	Account a = [select id,name,Blacklist_Status__c,blacklist_type__c from Account where name='test20130515'];
        	system.assertEquals(a.blacklist_type__c,'Pending');
      		system.assertEquals(a.Blacklist_Status__c,'Gray'); 
      		a.blacklist_type__c='Tier I';
      		update a;
      		Account a1 = [select id,Blacklist_Status__c,blacklist_type__c,name,blacklist_lock__c from Account where name='test20130515']; 
      		system.assertEquals(a1.blacklist_lock__c,true);
      		system.assertEquals(a1.Blacklist_Status__c,'Red');
      		a1.blacklist_type__c='Tier II';
      		update a1;
      		Account a2 = [select id,Blacklist_Status__c,blacklist_type__c,name,blacklist_lock__c from Account where name='test20130515']; 
      		system.assertEquals(a2.blacklist_lock__c,false);
      		system.assertEquals(a2.Blacklist_Status__c,'Red');
      		a2.blacklist_type__c='Tier I';
      		update a2;
      		Account a3 = [select id,Blacklist_Status__c,blacklist_type__c,name,blacklist_lock__c from Account where name='test20130515']; 
      		system.assertEquals(a3.blacklist_lock__c,true);
      		system.assertEquals(a3.Blacklist_Status__c,'Red');
      		Date startDate = System.today();
			Date endDate = System.today()+1;
      		Blacklist_Check_History__c bch =[Select Account__c,Blacklist_Status__c,isNew__c,Last_status__c From Blacklist_Check_History__c
      			  where Blacklist_Status__c='Red' and Last_status__c='Red' and blacklist_type__c='Tier I' and isNew__c=true and Account__c=:a3.id limit 1]; 
			     // where status_date__c>=:startDate and status_date__c<:endDate and Blacklist_Status__c='Red' and Last_status__c='Red' and blacklist_type__c='Tier I' and isNew__c=true and Account__c=:a3.id limit 1];
			system.assertEquals(bch.Blacklist_Status__c,'Red');
			system.assertEquals(bch.isNew__c,true);
			system.assertEquals(bch.Last_status__c,'Red');

			EmailBlacklistTierII2TierIScheduleJob e = new EmailBlacklistTierII2TierIScheduleJob();
			e.bchs.add(bch);
			e.sendEmail();
			system.assertEquals(e.results,null);
			 	
        Test.stopTest();
    }
}