public class InstallAssetHistoryCon {
	private EdObject eo=new EdObject('Installed_Asset__c');
	public InstallAssetHistoryCon(String ia_Id){
		this.iaId=iaId;
	}
	List<Installed_Asset__History> iaHistorys=new List<Installed_Asset__History>();

    public EdHistory[] getHistories() {
        iaHistorys=[Select ParentId, OldValue, NewValue, Id, CreatedDate, CreatedById 
        	, CreatedBy.Name, CreatedBy.Username, Field
        	From Installed_Asset__History 
        	where ParentId=: iaId ORDER BY CreatedDate ASC];
        EdHistory[] histories = new EdHistory[]{};
        // EdHistory(Datetime d, boolean p, String actor, String ht, String f, String t)
        // 				时间,	 是否published,	操作人,			历史类型,	原值,	新值	
        for (Installed_Asset__History iah:iaHistorys) {
        	String label='';
        	Schema.DescribeFieldResult sd=null;
        	if(iah.Field!=null)sd=eo.getFieldResult(iah.Field);
        	if(sd!=null)label=sd.getLabel();
        	addHistory(histories, new EdHistory(iah.createdDate, true, iah.createdBy.name, label + ' Change', String.valueOf(iah.oldValue), String.valueOf(iah.newValue))); 
        }
        return histories;
    }
    private void addHistory(EdHistory[] histories, EdHistory newHistory) {
        Integer position = histories.size();
        for (Integer i = 0; i < histories.size(); i++) {
            if (newHistory.historydt < histories[i].historydt) {
                position = i;
                break;
            }
        }
        if (position == histories.size()) {
            histories.add(newHistory);
        } else {
            histories.add(position, newHistory);
        }
    }
    private Id iaId {
        get { 
            if(ApexPages.currentPage().getparameters().get('id') != null) {
                iaId = ApexPages.currentPage().getparameters().get('id');
            }
            return iaId;
        }
        set { 
            if(value != null) iaId = value;
        }
    }
    
    public static TestMethod void currClsTest(){
    	
    	EdObject.currClsTest();
    	EdHistory.currClsTest();
    	 RecordType rt = [select id from RecordType where SobjectType = 'Installed_Asset__c' and name like 'HW-(Int CSS)%' limit 1];
        Installed_Asset__c asset = new Installed_Asset__c(RecordTypeId = rt.id,BOM_Code__c = '12345678');
        insert asset;
    	Installed_Asset__History iahSO=[Select ParentId, OldValue, NewValue, Id, CreatedDate, CreatedById 
        	, CreatedBy.Name, CreatedBy.Username, Field
        	From Installed_Asset__History 
        	limit 1];
    	System.debug('History IA ID:'+iahSO.id+', IA ID=['+iahSO.parentId+']');
    	Installed_Asset__c iac=[select id,name,filed__c,Street__c from Installed_Asset__c where id=:iahSO.ParentId limit 1];
    	System.debug('IA ID=['+iac.id+']');
    	InstallAssetHistoryCon iah=new InstallAssetHistoryCon(iac.id+'');
    	iah.getHistories();
    }
}