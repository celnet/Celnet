@isTest
private class Lepus_SetIsNamedAccountHandlerTest {
	static testmethod void myUnitTest(){
		Account acc = Lepus_EIPCalloutServiceTest.createAccount('accx');
		acc.Is_Named_Account__c = true;
		insert acc;
		
		Lead l = new Lead();
		l.Account_Name__c = acc.id;
		l.LastName = 'xxx';
		l.Company = 'xx';
		insert l;
	}
}