@isTest
private class DealRegistrationSharingTest {

    static testMethod void share() {
    	Profile p = UtilUnitTest.getProfile('(Huawei China)代表处产品经理');
    	User u = UtilUnitTest.newUserWithoutInsert(p.id);
      insert u;
      Deal_Registration__c dr = new Deal_Registration__c(Deal_Status__c='Waiting For Confirmation',	
      	recordtypeid = CONSTANTS.HUAWEICHINADEALRECORDTYPE);
      dr.SubmittedToApproval__c = true;//use this to aovid fire the trigger when submit approval
      insert dr;
      Approval.ProcessSubmitRequest req = 
            new Approval.ProcessSubmitRequest();
      req.setObjectId(dr.id);
      req.setNextApproverIds(new List<id>{u.id});
      
      Test.startTest();
      Approval.ProcessResult result = Approval.process(req);
      Test.stopTest();
      System.assertEquals(true,result.isSuccess());
      
      	 UserRecordAccess access = [SELECT RecordId, HasReadAccess, HasEditAccess
         FROM UserRecordAccess
         WHERE UserId = :u.id
         AND RecordId = :dr.id limit 1];
         System.assertEquals(false,access.HasReadAccess);
      System.runAs(u){
      	DealRegistrationSharing controller = new DealRegistrationSharing(
      		new ApexPages.StandardController(dr));
      	controller.redirect();
      }
      
      
       access = [SELECT RecordId, HasReadAccess, HasEditAccess
         FROM UserRecordAccess
         WHERE UserId = :u.id
         AND RecordId = :dr.id limit 1];
         System.assertEquals(true,access.HasReadAccess);
      
    }
}