global class UpdateContractEA_Batch  implements Database.Batchable<sObject>,Database.Stateful
{
	public String query;
	public Map<Id, Account> accMap;
	public Map<Id,CSS_Contract__c> serviceContractMap;
	public Set<Id> newContractIdSet;
	
	global Database.QueryLocator start(Database.BatchableContext bc)
	{
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, list<sObject> scope)
	{
		try
		{
			list<Installed_Asset__c> eaList = (list<Installed_Asset__c>)scope;
			for(Installed_Asset__c ea : eaList)
			{
				ea.Account_Name__c = serviceContractMap.get(ea.Contracts_No__c).Account_Name__c;
				ea.Region_Dept__c = accMap.get(serviceContractMap.get(ea.Contracts_No__c).Account_Name__c).Region_Dept__c;
			}
			System.debug('-----------update ealist------- '+eaList.size());
			update eaList;
		}
		catch(Exception e)	
		{
			
		}		
		
	}
	
	global void finish(Database.BatchableContext BC)
	{
		System.debug('-----------finish------');
	}
}