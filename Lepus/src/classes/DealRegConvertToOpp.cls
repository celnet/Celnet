/**
 * Update by Steven 20140118 
 * Purpose: reduce the number of queries
 */
public without sharing class DealRegConvertToOpp {
    public String currId{ 
        get{return currId;}
        set{currId=value;} 
    } 
    public String OppId{ //全局变量，用于记录转换后的机会点id
        get{return OppId;}
        set{OppId=value;} 
    } 
    public DealRegConvertToOpp(ApexPages.StandardController controller) { 
        currId= ApexPages.currentPage().getParameters().get('id');
    }
    public DealRegConvertToOpp() { 
        currId= ApexPages.currentPage().getParameters().get('id');
    }
    public Boolean haveError{ 
        get{return haveError;} 
        set{haveError=value;}
    }  
    public String getId(){
        return currId;  
    } 
    
    public PageReference convert(){
    Deal_Registration__c dr=
        [select id, RecordTypeId, name, deal_status__c, sales_stage__c, win_probability__c, end_customer_lookup__r.id,
                    estimated_contracting_date__c, deal_description__c, expiring_date__c , RecordType.DeveloperName ,
                    Reseller__r.Id ,Reseller__r.Name ,  reseller_contact__r.Id, owner.id, End_Customer_lookup__r.Representative_Office__c
             from Deal_Registration__c where id=:currId];
        //盈率百分比换算
    String winOdd = string.valueOf(dr.Win_Probability__c);
    if(winOdd.length()==3) {
        winOdd = winOdd.substring(0,1);
    } else if(winOdd.length()==4) {
        winOdd = winOdd.substring(0,2)+'%';
    } else if(winOdd.length()==5) {
        winOdd = winOdd.substring(0,3)+'%';
    }
    //创建机会点
    Opportunity opp=new Opportunity();
    opp.OwnerId=Userinfo.getUserId();
    opp.Name=dr.Name;
    opp.StageName='SS3 (Validating Opportunity)';
    opp.Win_Odds__c=winOdd;
    opp.CloseDate=dr.Estimated_Contracting_Date__c;
    opp.Description=dr.Deal_Description__c;
    opp.AccountId=dr.end_customer_lookup__c;
    opp.Primary_Channel_Partner__c=dr.Reseller__c;
    opp.Master_Channer_Partner__c=dr.Reseller__r.Name;
    opp.Primary_Channel_Partner_Contact__c=dr.Reseller_Contact__c;
    opp.Deal_Registration_Expiration_Date__c=dr.Expiring_Date__c;
    opp.Representative_Office__c = dr.End_Customer_lookup__r.Representative_Office__c ;
    if (dr.RecordTypeId == CONSTANTS.HUAWEICHINADEALRECORDTYPE){
    opp.RecordTypeId=CONSTANTS.HUAWEICHINADEALOPPRECORDTYPE;
    opp.Opportunity_Progress__c = '报备已转换成机会点';
    opp.participate_space__c = 0;
    } else if (dr.RecordTypeId == CONSTANTS.HUAWEIOVERSEADEALRECORDTYPE){
        opp.RecordTypeId=CONSTANTS.HUAWEIOVERSEADEALOPPRECORDTYPE;
    }
    List<Deal_Registration_Distributor__c> drdList = 
        [select Distributor__r.id, Distributor_Contact__r.id
                 from Deal_Registration_Distributor__c 
                where Distributor__r.id<>'' and Deal_Registration__r.id=:dr.Id];
    if(!drdList.isEmpty()) {
        opp.Distributor__c = drdList[0].Distributor__c;
        opp.Distributor_Contact__c = drdList[0].Distributor_Contact__c;
    }
        insert(opp);
        dr.related_opportunity__c=opp.Id;        
        update(dr);
        
        //插入机会点的关联列表
        insertRelateListLater(dr);
        OppId=opp.Id;
        
        
       return new ApexPages.StandardController(opp).view();
    }
    
  /**
     * 插入机会点的关联列表
     */
    private void insertRelateListLater(Deal_Registration__c dr) {
        //自动插入报备竞争对手列表
        insertCompetitors(dr.id,dr.related_opportunity__c);
        //自动插入报备产品列表
        insertProduct(dr.id,dr.related_opportunity__c);
        //自动插入报备总代列表
        insertDistributor(dr.id,dr.related_opportunity__c);
        //自动插入机会点团队成员
        if(dr.RecordTypeId == CONSTANTS.HUAWEICHINADEALRECORDTYPE){
            insertOppTeamMember(dr.OwnerId,dr.related_opportunity__c);
    }
    }
    /**
     * 插入报备竞争对手列表
     */
    @future
    public static void insertCompetitors(Id drId,Id oppId){
    List<Project_Competitors__c> oppcList = new List<Project_Competitors__c>();
    List<Deal_Registration_Competitor__c> drcList = 
        [select Deal_Registration__r.id, 
                            Competitive_Product__c, 
                            Competitive_Vendor__c, 
                            Competitive_Vendor_lookup__r.id
                 from Deal_Registration_Competitor__c 
                where Competitive_Vendor_lookup__r.id<>'' 
                  and Deal_Registration__r.id=:drId ]; 
    for(Deal_Registration_Competitor__c drc:drcList){
        Project_Competitors__c oppc = new Project_Competitors__c();
        oppc.Competitor_Name__c = drc.Competitive_Vendor_lookup__c;
        oppc.Product_Name__c = drc.Competitive_Product__c;
        oppc.Project_Name__c = oppId;
        oppcList.add(oppc);
    }
    if(!oppcList.isEmpty()) insert oppcList;
    }
    /**
     * 插入报备产品列表
     */
    @future
    public static void insertProduct(Id drId,Id oppId){
    List<Project_Product__c> opppList = new List<Project_Product__c>();
    List<Deal_Registration_Product__c> drplist = 
        [select Lookup_Product_Master__r.id,
                        Deal_Registration__r.id,
                        Equipment_Amount__c,
                        Service_Amount__c
             from Deal_Registration_Product__c 
          where Lookup_Product_Master__r.id<>'' 
            and Deal_Registration__r.id=:drId];
    for(Deal_Registration_Product__c drp:drplist){
        Project_Product__c oppp = new Project_Product__c();
        oppp.Project_Name__c = oppId;
        oppp.Lookup__c=drp.Lookup_Product_Master__c;
        drp.Equipment_Amount__c = drp.Equipment_Amount__c == null ? 0 : drp.Equipment_Amount__c;
        drp.Service_Amount__c = drp.Service_Amount__c == null ? 0 : drp.Service_Amount__c;
        oppp.Sales_Price__c=drp.Equipment_Amount__c+drp.Service_Amount__c;
        oppp.Quantity__c=1;
        opppList.add(oppp);
    }
        if(!opppList.isEmpty()) insert opppList;
    }
    /**
     * 插入报备总代列表
     */
    @future
    public static void insertDistributor(Id drId,Id oppId){
    List<Related_Channel_Partner__c> oppdList = new List<Related_Channel_Partner__c>();
    List<Deal_Registration_Distributor__c> drdlist = 
        [select Distributor__r.id,
                      Distributor_Contact__r.id
             from Deal_Registration_Distributor__c
            where Distributor__r.id<>'' 
                and Deal_Registration__r.id=:drId];
    for(Deal_Registration_Distributor__c drd:drdlist){
        Related_Channel_Partner__c oppd=new Related_Channel_Partner__c();
        oppd.Channel_Partner_Name__c=drd.Distributor__c;
        oppd.Project_Name__c = oppId;
        oppdList.add(oppd);
    }
    if(!drdlist.isEmpty()) insert oppdList;
    }
    /**
     * 插入默认的机会点团队成员
     */
    @future
    private static void insertOppTeamMember(Id ownerId,Id oppId){
        OpportunityTeamMember oppMember = new OpportunityTeamMember();
        oppMember.OpportunityId = oppId;
        oppMember.UserId = ownerId;
        oppMember.TeamMemberRole = 'Channel Manager';
        insert oppMember;
    }
  
}