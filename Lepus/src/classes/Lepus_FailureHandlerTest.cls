/**
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-8-30
 * Description: Lepus_FailureHandler 测试类
 */
@isTest
private class Lepus_FailureHandlerTest {

    static testMethod void myUnitTest() {
        Account acc = UtilUnitTest.createHWChinaAccount('testAcc');
        Opportunity opp = UtilUnitTest.createOpportunity(acc);
        User u = [Select Id From User limit 1];
        
        Lepus_FailureHandler.handleBusinessDataFailure(acc.Id, 'IOException: Time out', 'delete', 'Account');
        Lepus_FailureHandler.handleFieldUpdateFailure(opp.Id, Datetime.now(), 'Cannot config DB');
        Lepus_FailureHandler.handleTeamMemberFailure(new List<Id>{opp.Id}, 'Pending uncommitted operation', '');
        Lepus_FailureHandler.handleUserFailure(new List<Id>{u.Id}, 'SOQLException');
        
        SyncFailRecord__c sfr = [Select Id From SyncFailRecord__c limit 1];
        Lepus_FailureHandler.handleReSyncFailure(sfr.Id, 'Error');
        
        Lead l = new Lead();
        l.LastName = 'dfa';
        l.Company = 'dafs';
        insert l;
        
        Lepus_FailureHandler.handleLeadHistoryFailure(new list<Id>{l.Id}, 'xxx', 'xxx');
        Lepus_FailureHandler.handleValidationFailure('Error', Datetime.now(), Opportunity.sobjectType);
    }
}