/**************************************************************************************************
 * Name             : UtilUnitTest
 * Object           : Account, Opportunity
 * Requirement      : NONE
 * Purpose          : Prepare test data
 * Author           : Tulipe Lee
 * Create Date      : 05/22/2012
 * Modify History   : 
 *                  2013-3  Brain Zhang
 *                  Add some more methods
 *                  2013-5 Jen Peng
 *                  Add some more methods   
 * 
***************************************************************************************************/
@isTest
public class UtilUnitTest {
    //Create Profile 2013-05-15
    public static Profile CreateProfile(String n){
        Profile p =[select id,Name from Profile where Name=:n];
        return p;
    }
    
    //Create user 2013-05-15
    public static UserRole CreateRole(String n){
        UserRole ur = [select id,Name from UserRole where Name=:n];
        return ur;
    }
    //Create user 2013-5-15
    public static User CreateUser(String uname,String pname, String rname,String employeeId){
        Profile p = UtilUnitTest.CreateProfile(pname);
        UserRole ur = UtilUnitTest.CreateRole(rname);
        User u = new User(LastName=uname,Alias=uname.substring(0,3),Email=uname+'@123.com'
                        ,Username=uname+'@123.com.dev',CommunityNickname=uname+'nick',ProfileId=p.id,UserRoleId=ur.id
                        ,User_Group__c='Nation',Group__c='other',Region_Office__c='HQ',Employee_ID__c=employeeId
                        ,Last_Active_Date__c=system.today(),EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US'
                        ,LocaleSidKey='en_US',TimeZoneSidKey='Asia/Shanghai');
        insert u;
        return u;
    }
    // Create Account 2013-5-15
    public static Account createAccount(String n){
        String recordtypeId = getRecordTypeIdByName('Account', 'Customer');
        Account acc = new Account();
        acc.Name = n;
        acc.RecordTypeId=recordtypeId;
        acc.Region_HW__c='West Europe Region';
        acc.Representative_Office__c='Germany';
        acc.Country_HW__c='Germany';
        acc.Industry = 'Finance';
        acc.Country_Code_HW__c='DE';
        acc.Industry_Code__c='CG2012';
        acc.blacklist_type__c='Pending';
        insert acc;
        return acc;
    }
    
    // Create Account2
    public static Account createAccountWithName(String accName) {
        String recordtypeId = getRecordTypeIdByName('Account', 'Customer(HS)');
        Account acc = new Account(Name = accName, Type = 'International',
                                  CurrencyIsoCode = 'USD', RecordTypeId = recordtypeId,
                                  Industry = 'Finance',
                                  blacklist_type__c='Pending');
        insert acc; 
        return acc;
    }
    
    // Create amount of Account records 2013-5-16
    public static List<Account> createAccounts(Integer startIndex,Integer endIndex,String name){
        String recordtypeId = getRecordTypeIdByName('Account', 'Customer');
        List<Account> accs = new List<Account>();
        for(Integer i=startIndex;i<endIndex;i++){
            Account acc = new Account();
            acc.Name = name+'_'+i;
            acc.RecordTypeId=recordtypeId;
            acc.Region_HW__c='West Europe Region';
            acc.Representative_Office__c='Germany';
            acc.Country_HW__c='Germany';
            acc.Industry = 'Finance';
            acc.Country_Code_HW__c='DE';
            acc.Industry_Code__c='CG2012';
            acc.blacklist_type__c='Pending';
            accs.add(acc);
        }   
        insert accs;
        return accs;
    }
    
    // Create amount of BlackList__c records 2013-5-16
    public static List<BlackList__c> createBlackLists(Integer startIndex,Integer endIndex,String name,String country,String status){
        List<BlackList__c> bls=new List<BlackList__c>();
        for(Integer i=startIndex;i<endIndex;i++){
            BlackList__c b = new BlackList__c();
            b.Name=name+'_'+i;
            b.country__c=country;
            b.status__c=status;
            b.IDNum__c='bId_'+i;
            bls.add(b);
        }
        insert bls;
        return bls;
    }
    // Create Account
    public static Account createAccount() {
        String recordtypeId = getRecordTypeIdByName('Account', 'Customer(HS)');
        Account acc = new Account(Name = 'TEST Account____', Type = 'International',
                                  CurrencyIsoCode = 'USD', RecordTypeId = recordtypeId,
                                  Industry = 'Finance',
                                  blacklist_type__c='Pending');
        insert acc; 
        return acc;
    }
    
    public static Account createHWChinaAccount(String name) {
        Account acc = new Account(Name = name, RecordTypeId = CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE,
                                  blacklist_type__c='Pending' );
        insert acc; 
        return acc;
    }
    // Create Opportunity According to Account
    public static Opportunity createOpportunity(Account acc) {
        String recordtypeId = getRecordTypeIdByName('Opportunity', 'Enterprise Business Opportunity');
        Opportunity opp = new Opportunity(AccountId = acc.Id, Name = 'TEST Opportunity',
                                          StageName = 'SS1 (Notifying Opportunity)',
                                          CloseDate = Date.today(), RecordTypeId = recordtypeId,
                                          Opportunity_Level__c = '');
        
        insert opp;
        return opp;
    }
    
    public static Opportunity createVolumeOpportunity(Account acc){
         String recordtypeId = getRecordTypeIdByName('Opportunity', 'C&AP Volume Project');
        Opportunity opp = new Opportunity(AccountId = acc.Id, Name = 'TEST',Primary_Channel_Partner__c = acc.id,
                                          StageName = 'x',
                                          CloseDate = Date.today(), RecordTypeId = recordtypeId,
                                          Opportunity_Level__c = '');
        insert opp;
        Product_HS__c hs = UtilUnitTest.createProductMaster('l');
                Project_Product__c pp = UtilUnitTest.createProjectProduct(opp,hs);
                pp.Quantity__c = 3;
                pp.Sales_Price__c = 7;
                update pp;
                opp.StageName = 'SS4 (Developing Solution)';
                update opp;
        return opp;
    }
    
    // Create Product Master
    public static Product_HS__c createProductMaster(String level) {
        String recordTypeId = getRecordTypeIdByName('Product_HS__c', 'Huawei Product');
        Product_HS__c product = new Product_HS__c(RecordTypeId = recordTypeId, Name = 'Test Product ' + level,
                                                  Level_1__c = level, Active__c = 'Yes',
                                                  Product_Level__c = '1', 
                                                  External_Id__c = String.valueOf(System.now()));
        
        insert product;
        return product;
    }
    
    // Create Project Product
    public static Project_Product__c createProjectProduct(Opportunity opp, Product_HS__c product) {
        Project_Product__c lineItem = new Project_Product__c();
        lineItem.Name = 'Test Project Product';
        lineItem.Project_Name__c = opp.Id;
        lineItem.Quantity__c = 20;
        lineItem.Sales_Price__c = 10;
        lineItem.Lookup__c = product.Id;
        
        insert lineItem;
        return lineItem;
    }
    
    public static Deal_Registration_Product__c createDealProduct(Deal_Registration__c deal,Product_HS__c product){
        Deal_Registration_Product__c drp = new Deal_Registration_Product__c(Deal_Registration__c = deal.id,
            Lookup_Product_Master__c = product.id); 
        insert drp;
        return drp;
    }
    
    // Create Sales Team
    public static Sales_Team__c createSalesTeam(Opportunity opp, User usr) {
        Sales_Team__c team = new Sales_Team__c(Opportunity__c = opp.Id, Opportunity_Access_Level__c = 'Raad/Write',
                                               User__c = usr.Id, Team_Role__c = 'Executive Sponsor');
        
        insert team;
        return team;
    }
    
    // Gets the object record type id according to the recordType name.
    public static String getRecordTypeIdByName(String sObjectType, String recordTypeName){
        Schema.SobjectType d = Schema.getGlobalDescribe().get(sObjectType);
        Map<String, Schema.RecordTypeInfo> rtMapByName = d.getDescribe().getRecordTypeInfosByName();
        Schema.RecordTypeInfo rtByName = rtMapByName.get(recordTypeName);
        return rtByName.getRecordTypeId();
    }
    
    /**
    *
    *The below methods are added by Brain Zhang
    *
    */
    //Added By Brain Zhang ,2013-3-5
    public static Named_Account__c createActiveNamedAccount(Account a){
        Named_Account__c na = new Named_Account__c(Account_Name__c = a.id,
            Is_Named_Account__c = true,Named_Account_Character__c = 'characterY',
            Named_Account_Level__c = 'levelY',Approved_Date__c = Date.today(),Named_Account_Property__c = 'propertyY');
        insert na;
        return na;
    }
    
    public static Named_Account__c createInActiveNamedAccount(Account a){
        Named_Account__c na = new Named_Account__c(Account_Name__c = a.id,
            Is_Named_Account__c = false,Named_Account_Character__c = 'characterN',
            Named_Account_Level__c = 'levelN',Approved_Date__c = Date.today(),Named_Account_Property__c = 'propertyN');
        insert na;
        return na;
    }
    
    public static Profile getProfile(String name){
        Profile p = [select id,name from Profile where name = :name];
        return p;
    }
    
     public static UserRole getRoleFromDeveloperName(String name){
        UserRole p = [select id,name from UserRole where developername = :name];
        return p;
    }
    
    
    public static Campaign createCampaign(){
        Campaign c = new Campaign(name = 'c',Company_Brand_Fund__c = 1); 
        insert c;
        return c;
    }
    
    public static Lead createLead(Account acc){
      Lead l = new Lead(Account_Name__c = acc.id,NA_Lead_Name__c = 'nalead',
            NA_Lead_Confirmation__c='未确认',lastname = 'lead',company = 'company');
      l.recordtypeid = UtilUnitTest.getRecordTypeIdByName('Lead', 'Huawei China NA Lead');
      insert l;
      return l;
    }

    public static Account createHWChinaNAAccount(String name) {
            Account acc = new Account();
            acc.Name = name;
            acc.cis__c = 'TEST123';
            acc.RecordTypeId = CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE;
            acc.Region_HW__c='China';
            acc.Representative_Office__c='系统部总部'; 
            acc.Country_HW__c='China';
            acc.Country_Code_HW__c='CN';
            acc.Province__c = '北京市';
            acc.City_Huawei_China__c = '北京市';
            acc.Industry = '大企业系统部';
            acc.Sub_industry_HW__c='大企业二部（石油化工、燃气、煤炭）';
            acc.Customer_Group__c = '能源-煤炭';
            acc.Customer_Group_Code__c = '7E';
            acc.Is_Named_Account__c = true;
            acc.blacklist_type__c='Pending';
            insert acc;
            return acc;
    }
    
    public static void dataDictonaryPrepare(){
        
        DataDictionary__c dd0 = new DataDictionary__c(Object__c = 'Account',Type__c = 'Province',
            Code__c = 'testCode',Name__c = 'testProvince'); 
        DataDictionary__c dd1 = new DataDictionary__c(Object__c = 'Account',Type__c = 'Sub-Industry',
            Code__c = 'testCode',Name__c = 'testSubIndustry'); 
        DataDictionary__c dd2 = new DataDictionary__c(Object__c = 'Account',Type__c = 'Customer Group',
            Code__c = 'testCode',Name__c = 'testCustomerGroup'); 
        DataDictionary__c dd3 = new DataDictionary__c(Object__c = 'Account',Type__c = 'industry',
            Code__c = 'testCode',Name__c = 'testIndustry'); 
        
        List<DataDictionary__c> dds = new List<DataDictionary__c>{dd0,dd1,dd2,dd3}; 
        insert dds;
    }
    
    public static Leads_Distribute_Map__c createLeadDistributeMap(){
        Leads_Distribute_Map__c record = new Leads_Distribute_Map__c(Leads_Owner__c = Userinfo.getUserId(),
        Region_Name__c = 'test');
      insert record;
      return record;
    }
    
    public static Case createChinaServiceRequest(){
        RecordType chinaRecordType = [select id from RecordType where isActive = true 
            and developername like 'China_Internal_Post_Sales_Technical%' and SobjectType = 'case' limit 1];
        Case c = new Case(recordtypeid = chinaRecordType.id,iCare_Owner__c = Userinfo.getUserId());
        insert c;
        return c;
    }
    
    public static Case createCCRRequest(Contact con){
        RecordType chinaRecordType = [select id from RecordType where isActive = true 
            and developername = 'China_External_Post_Sales_CCR' and SobjectType = 'case' limit 1];
        Case c = new Case(recordtypeid = chinaRecordType.id,iCare_Owner__c = Userinfo.getUserId(),contactid = con.id);
        insert c;
        return c;
    }
    
    public static Work_Order__c createOnSiteWorkOrder(Account acc,Case sr){
        RecordType chinaRecordType = [select id from RecordType where isActive = true 
            and developername like 'N_On_Site%' and SobjectType = 'Work_Order__c' limit 1];
        Work_Order__c wo = new Work_Order__c(Account_Name__c = acc.id,Service_Request_No__c = sr.id,recordTypeId = chinaRecordType.id);
        insert wo;
        return wo;
    }
    
    public static Spare_Part__c createSparePart(Work_Order__c wo){
        Spare_Part__c sp = new Spare_Part__c(Work_Order__c = wo.id);
        insert sp;
        return sp;
    }
    
    public static CSS_Contract__c createContract(Account acc){
        CSS_Contract__c c = new CSS_Contract__c(Account_Name__c = acc.id,name='12345678901234');
        insert c;
        return c;
    }
    
    public static SFP__c createSFP(CSS_Contract__c c){
        RecordType warrantyRecordType = [select id from RecordType where isActive = true 
            and developername like '%Warranty%' and SobjectType = 'SFP__c' limit 1];
        SFP__c sfp = new SFP__c(Contract_No__c = c.id,Account_Name__c = c.Account_Name__c
            ,Status__c ='Active',recordtypeid = warrantyRecordType.id);
        insert sfp;
        return sfp;
    }
    
    public static Contract_Product_PO__c createContractProduct(CSS_Contract__c cc,Product_HS__c product){
        Contract_Product_PO__c cpp = new Contract_Product_PO__c(Contract_No__c = cc.id,name='test',
            Product_Search__c = product.id,Quantity__c =7);
        insert cpp;
        return cpp;
    }
    
    public static SFP_Product__c createSFPProduct(SFP__c sfp,Contract_Product_PO__c cpp){
        SFP_Product__c sfpp = new SFP_Product__c(SFP__c = sfp.id,Contract_Product_PO__c = cpp.id);
        insert sfpp;
        return sfpp;
    }
    
    public static Installed_Asset__c createAsset(CSS_Contract__c c){
        Installed_Asset__c asset = new Installed_Asset__c(Contracts_No__c = c.id,BOM_Code__c='12345678');
        asset.Account_Name__c = c.Account_Name__c;
        insert asset;
        return asset;
    }
    
    public static SFP_Asset__c createSFPAsset(Installed_Asset__c asset,SFP__c sfp){
        SFP_Asset__c sfpA = new SFP_Asset__c(Install_Asset__c = asset.id,SFP__c = sfp.id);
        insert sfpA;
        return sfpA;
    } 
    
    public static Contact createContact(Account acc){
        Contact c = new Contact(accountid = acc.id,lastname = 'testcontact',mobilephone ='123');
        insert c;
        return c;
    }
    
    public static User queryUser(String whereClause){
        String queryString = ' select id,Profile.Name,UserRole.DeveloperName,UserRole.Name  from User where isActive = true and '
         + whereClause + ' limit 1';
        User u = database.query(queryString);
            return u;
        }
        
        public static User newUserWithoutInsert(Id profileId){
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = profileId,  Employee_ID__c ='12324567',
      TimeZoneSidKey='America/Los_Angeles', UserName='mkt@huawei.testorg.com');
      return u;
        }
        
        public static Deal_Registration__c createDealRegistration(){
            Deal_Registration__c dr = new Deal_Registration__c();
            insert dr;
            return dr;
        }
    
}