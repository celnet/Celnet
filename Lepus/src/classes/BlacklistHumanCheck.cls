/*
* @Purpose :it is blacklist human check controller. every yellow Account record is checked by human.
* if it is pass,Account will be green; if no, Account will be mark Tier I/Tier II. In Tier I, the Account
* will be locked.  at last,every record will be record in checking history
*
* @Author : Jen Peng
* @Date : 		2013-5-2
*/
public without sharing class BlacklistHumanCheck {
	//for pagination
	public Integer currentPageNumber {get; set;}
    public Integer pageSize {get; set;}
	public Integer recordCount {get; set;}
	//for search bar
	public String kw {get;set;}
	public String searchQuery{get;set;}
	//for basic data
	public List<Account> accounts{get;set;}
	public List<Blacklist_Check_History__c> bchs{get;set;}
	public Map<Integer,WrappBlacklist> lsBlacklist{get;set;}
	public Map<Id,Blacklist_Check_History__c> bchMaps = new Map<Id,Blacklist_Check_History__c>();
	public Map<Integer,WrappBlacklist> mapWrappBlst {get;set;}
	
	//pagination methods
	public Integer pageCount {
        get {
            return Math.mod(recordCount, pageSize) == 0 ?
                     recordCount / pageSize : recordCount / pageSize + 1;
        }
        set;
    }
    
    public boolean hasNext {
        get {
        	if(currentPageNumber != pageCount){
        		return true;
        	}else{
        		return false;
        	}
            
        }
        set;
    }
    
    public boolean hasPrevious {
        get {
        	if(currentPageNumber != 1){
        		return true;
        	}else{
        		return false;
        	}
        }
        set;
    }
    
    public List<SelectOption> getItems(){
        List<SelectOption> options = new List<SelectOption>();
        //options.add(new SelectOption('1','1'));
        options.add(new SelectOption('5','5'));
        options.add(new SelectOption('10','10'));
        options.add(new SelectOption('20','20'));
        options.add(new SelectOption('50','50'));
        options.add(new SelectOption('100','100'));
        return options;
    }
    
    public void first() {
        currentPageNumber = 1;
        for(Integer i:lsBlacklist.keySet()){
        	mapWrappBlst.put(i,lsBlacklist.get(i));
        }
        populateBlacklistWrappers();
    }
    
    public PageReference next() {	
        currentPageNumber++;
        for(Integer i:lsBlacklist.keySet()){
        	mapWrappBlst.put(i,lsBlacklist.get(i));
        }
        populateBlacklistWrappers();
        return null;
   	}
   	
   	public void previous() {
        currentPageNumber--;
        for(Integer i:lsBlacklist.keySet()){
        	mapWrappBlst.put(i,lsBlacklist.get(i));
        }
        populateBlacklistWrappers();
    }
    
    public void last() {
        currentPageNumber = pageCount;
        for(Integer i:lsBlacklist.keySet()){
        	mapWrappBlst.put(i,lsBlacklist.get(i));
        }
        populateBlacklistWrappers();
    }
    
    public void changePageSize() {
        currentPageNumber = 1;
        for(Integer i:lsBlacklist.keySet()){
        	mapWrappBlst.put(i,lsBlacklist.get(i));
        }
        populateBlacklistWrappers();
    }
    
    public void changeCurrentPageNumber() {
        if (currentPageNumber > pageCount) {
            currentPageNumber = 1;
        }
        for(Integer i:lsBlacklist.keySet()){
        	mapWrappBlst.put(i,lsBlacklist.get(i));
        }       
        populateBlacklistWrappers();
    }
    //BlacklistHumanCheck constructor
	public BlacklistHumanCheck(){
		//initial value
		//pageSize = 5;
		pageSize = 50;
    	currentPageNumber = 1;
    	lsBlacklist = new Map<Integer,WrappBlacklist>();
    	mapWrappBlst = new Map<Integer,WrappBlacklist>();
		try{
			
			bchs=[select id,Account__c,Account__r.Name,BlackList__r.Name,blacklist_country__c,blacklist_code__c, 
			     blacklist_street__c,blacklist_state__c,blacklist_city__c
			     from Blacklist_Check_History__c where Blacklist_Status__c='Yellow'];
			for(Blacklist_Check_History__c b : bchs){
				bchMaps.put(b.Account__c,b);
			}
			bchs.clear();
			string query='select id,Name,RecordType_Name__c,Country_Code_HW__c,Blacklist_Status__c,Rep_Office__c,blacklist_type__c,isFollow__c,blacklist_comment__c,Blacklist_Status_img__c  from Account where Blacklist_Status__c=\'Yellow\'';
			accounts=Database.query(query);
			Integer cnt = 0;
			for(Account a:accounts){
				mapWrappBlst.put(cnt++,new WrappBlacklist(bchMaps,a));
			}
			accounts.clear();
			
		}catch(Exception e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Ooops!'));
		}
		populateBlacklistWrappers();
	}
	//saving function for save the checked records
	public PageReference save(){
		try{
			List<Account> carryAcc = new List<Account>();

			for(WrappBlacklist wbl : mapWrappBlst.values()){
				Account tmpAcc = new Account();
				tmpAcc=wbl.acc;
				tmpAcc.isFollow__c=wbl.isFollow;
				tmpAcc.blacklist_comment__c=wbl.comment;
				carryAcc.add(tmpAcc);
			}
			system.debug('carryAcc is here====================='+carryAcc);			
			update carryAcc;
			mapWrappBlst.clear();
			carryAcc.clear();
			bchs=[select id,Account__c,Account__r.Name,BlackList__r.Name,blacklist_country__c,blacklist_code__c, 
			     blacklist_street__c,blacklist_state__c,blacklist_city__c
			     from Blacklist_Check_History__c where Blacklist_Status__c='Yellow' and isNew__c=true];
			for(Blacklist_Check_History__c b : bchs){
				bchMaps.put(b.Account__c,b);
			}
			bchs.clear();
			string query='select id,Name,RecordType_Name__c,Country_Code_HW__c,Blacklist_Status__c,blacklist_type__c,Rep_Office__c,isFollow__c,blacklist_comment__c,Blacklist_Status_img__c  from Account where Blacklist_Status__c=\'Yellow\'';
			accounts=Database.query(query);
			Integer cnt = 0;
			for(Account a:accounts){
				mapWrappBlst.put(cnt++,new WrappBlacklist(bchMaps,a));
			}
			accounts.clear();
			populateBlacklistWrappers();
		}catch(Exception e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '无法保存该数据'));
			return null;
		}
		return null;
	}
	//Cancel function means wanna quit
	public PageReference cancel(){
		PageReference page = new PageReference('/home/home.jsp');
		return page;
		
	}
	// the result after runing search function
	public PageReference runSearch(){
		try{
			//pageSize = 5;
			pageSize = 50;
        	currentPageNumber = 1;
        	string query='select id,Name,RecordType_Name__c,Country_Code_HW__c,Blacklist_Status__c,blacklist_type__c,Rep_Office__c,isFollow__c,blacklist_comment__c,Blacklist_Status_img__c  from Account where Blacklist_Status__c=\'Yellow\'';
			if(kw==null||kw==''){			
				accounts.clear();
				accounts=Database.query(query);
			}else{
				query+=' and Name like \'%'+kw+'%\'';
				accounts.clear();
				accounts=Database.query(query);
			}
			mapWrappBlst.clear();
			Integer cnt = 0;
			for(Account a:accounts){
				mapWrappBlst.put(cnt++,new WrappBlacklist(bchMaps,a));
			}
			accounts.clear();
			
				
		}catch(Exception e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Ooops!'));
		}
		populateBlacklistWrappers();
		return null;
	}
	//reset the page after runing search function
	public PageReference showAll(){
		try{
			pageSize = 50;
        	currentPageNumber = 1;
        	string query='select id,Name,RecordType_Name__c,Country_Code_HW__c,Blacklist_Status__c,blacklist_type__c,Rep_Office__c,isFollow__c,blacklist_comment__c,Blacklist_Status_img__c  from Account where Blacklist_Status__c=\'Yellow\'';			
			accounts.clear();
			accounts=Database.query(query);
			Integer cnt = 0;
			mapWrappBlst.clear();
			for(Account a:accounts){
				mapWrappBlst.put(cnt++,new WrappBlacklist(bchMaps,a));
			}
			accounts.clear();
				
		}catch(Exception e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Ooops!没有查到数据'));
		}
		populateBlacklistWrappers();
		return null;
	}
	//populateAccountWrappers shows the page records 
	public void populateBlacklistWrappers(){
		recordCount = mapWrappBlst.size();
		Integer startNumber = 0;
    	Integer endNumber = 0;
    	if (currentPageNumber == pageCount) {
        	startNumber = (currentPageNumber - 1) * pageSize;
        	endNumber = recordCount;
    	}else {
        	startNumber = (currentPageNumber - 1) * pageSize;
        	endNumber = currentPageNumber * pageSize;
    	}
    	lsBlacklist.clear();
    	if(mapWrappBlst.size() > 0){
    		for(Integer i = startNumber; i < endNumber; i++){
				lsBlacklist.put(i,mapWrappBlst.get(i));
			}
    	}
	}
	
	public List<WrappBlacklist> getWrapps(){		
		return lsBlacklist.values();
	}
	//the inner class help wrapped the Yellow account and its blacklist check history
	class WrappBlacklist{
		public boolean checkRec{get;set;}
		public boolean isFollow{get;set;}
		public String comment{get;set;}
		public Account acc{get;set;}
		public Blacklist_Check_History__c bch{get;set;}
		
		public WrappBlacklist(Map<Id,Blacklist_Check_History__c> m,Account a){
			checkRec=false;
			isFollow=false;
			acc=a;
			bch=m.get(acc.id);
		}
	}
}