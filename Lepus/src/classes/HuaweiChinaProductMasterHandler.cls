public with sharing class HuaweiChinaProductMasterHandler {
    Product_HS__c root;
    
    public String rootId { get; set; }
    // all top level products
    public List<Product_HS__c> parentNodes { get; set; }
    // all sub level prodcuts
    public List<Product_HS__c> childNodes { get; set; }
    // all product id
    public String productId { get; set; }
    // all product url
    public String url { get; set; }
    // existed products
    public List<Project_Product__c> existProducts {get; set;}
    
    /* Class Construnction  */
    public HuaweiChinaProductMasterHandler(ApexPages.StandardController controller) { 
       
        // get all existed products of the opportunity.
        this.existProducts = [select Id, Lookup__c, Product_Name__c, Quantity__c, Sales_Price__c, CurrencyIsoCode, Product_level__c from Project_Product__c
                                    where Project_Name__c = :controller.getId()];
    }
    
    /* Get Existed Products Number */
    public String getitemNum() {
        return existProducts.size().format();
    }
    
    /* Custom Project Tree View Init  */
    public void init(){ 
        // Get all the parent nodes.
        parentNodes = [SELECT Id, Name, Parent_Product__c, Product_level__c FROM Product_HS__c WHERE Parent_Product__c='' AND RecordType.Name='Huawei China Product' AND Active__c='Yes'];
        childNodes = [SELECT Id, Name, Parent_Product__c, Product_level__c FROM Product_HS__c WHERE Parent_Product__c<>'' AND RecordType.Name='Huawei China Product' AND Active__c='Yes'];
        System.debug('childNodes.size===>' + childNodes.size());
    } 
    
    public static TestMethod void HuaweiChinaPruductMasterHandler_UnitTest() {
        Opportunity opp = new Opportunity(name='Test Opp');
        ApexPages.StandardController controller = new ApexPages.StandardController(opp);

        HuaweiChinaProductMasterHandler pmTest = new HuaweiChinaProductMasterHandler(controller);
        pmTest.init();
        pmTest.getitemNum();
        
    }

}