/**
 * The test class for account.trigger
 * Author           : BLS
 * Create Date      : 2013/3
 *     
 */ 
@isTest
private class TriggerAccountTests {

    //it is add method on 2013-05-23 for testing the AccountEmailInformHandler.cls begin
    static testMethod void AccountEmailTest(){
    	test.startTest();
    		UtilUnitTest.createAccount('test001');
    		Account acc = [select id,name from Account where name='test001'];
    		acc.RecordTypeId=UtilUnitTest.getRecordTypeIdByName('Account', 'HW-(Int CSS) Customer');
    		update acc;
    		
    		Contact cont = new Contact();
    		cont.LastName='testContact';
    		cont.AccountId=acc.id;
    		cont.RecordTypeId=UtilUnitTest.getRecordTypeIdByName('Contact','HW-(Int CSS) Contact');
    		insert cont;
    		
    		Case c = new Case();
    		c.RecordTypeId=UtilUnitTest.getRecordTypeIdByName('Case','HW-(Int CSS) Internal Post Sales-CCR');
    		c.Type='Engineering';
    		c.Status='Waiting for response';
    		c.AccountId=acc.id;
    		c.ContactId=cont.id;
    		c.Customer_Type__c='Internal Engineer';
    		insert c;
    		
    		CSS_Contract__c css = new CSS_Contract__c();
    		css.Name='12345678910234';
    		css.Project_Name__c='project test';
    		css.Contract_Type__c='Sales Contract';
    		css.Account_Name__c=acc.id;
    		css.Contact_Person__c=cont.id;
    		css.Contract_Signed_Date__c=system.today();
    		insert css;
    		Opportunity opp = UtilUnitTest.createOpportunity(acc);
    		acc.blacklist_type__c='Tier I';
    		update acc;
    		Account ac = [select id,name,blacklist_lock__c from Account where name='test001'];
    		system.assertEquals(ac.blacklist_lock__c, true);
    		ac.blacklist_type__c='Tier II';
    		update ac;
    		Account ac1 = [select id,name,blacklist_lock__c from Account where name='test001'];
    		system.assertEquals(ac1.blacklist_lock__c, false);
    		ac1.blacklist_type__c='Pass';
    		update ac1;
    		Account ac2 = [select id,name,blacklist_lock__c from Account where name='test001'];
    		system.assertEquals(ac1.blacklist_lock__c, false);
    		
    	test.stopTest();
    }
    
    static testMethod void deleteCheck(){//delete
    	Account acc = UtilUnitTest.createHWChinaAccount('acc');
    	UtilUnitTest.createOpportunity(acc);
    	Account child = UtilUnitTest.createHWChinaAccount('child');
    	child.ParentId = acc.id;
    	update child;
			Opportunity o = new Opportunity(accountid = acc.id,
			     Name = 'test',StageName = 'Solution',CloseDate = Date.today(),Opportunity_type__c='Direct Sales');
			insert o;
    	try{
    		delete acc;
    	}catch(Exception e){
    	}
    	System.assertEquals(2,[select count() from Account]);
    }
    
    
    static testMethod void addChild(){
    	Account grandParent = UtilUnitTest.createHWChinaAccount('grandparent');
        Account parent = UtilUnitTest.createHWChinaAccount('parent');

        Account child = UtilUnitTest.createHWChinaAccount('child');
        child.ParentId = parent.id;
        System.debug('before update child');
        update child;
        Account son = UtilUnitTest.createHWChinaAccount('son');
        son.ParentId = child.id;
        System.debug('before update son');
        update son;

        parent.ParentId = grandParent.id;
        System.debug('going to update ------');
        update parent;
    }
    
     static testMethod void addOneChild(){
        Account parent = UtilUnitTest.createHWChinaAccount('parent');
        Named_Account__c na = UtilUnitTest.createActiveNamedAccount(parent);
        Account child = UtilUnitTest.createHWChinaAccount('child');
        child.ParentId = parent.id;
        update child;
        child = [select id,HQ_Parent_Account__c from Account where id = :child.id];
        System.assertEquals(parent.id, child.HQ_Parent_Account__c);
    }
    
     static testMethod void cascadeInsert(){
    
        Account grandParent = UtilUnitTest.createHWChinaAccount('grandparent');
        update grandParent;
        
        Account parent = UtilUnitTest.createHWChinaAccount('p');
        parent.ParentId = grandParent.Id;
        update parent;

        Account child = UtilUnitTest.createHWChinaAccount('c');
        child.ParentId = parent.id;
        update child;
        
        Account son = UtilUnitTest.createHWChinaAccount('s');
        son.ParentId = child.id;
        update son;
        son = [select id,Is_Named_Account__c,Named_Account__c from Account where id = :child.id];
        System.assertEquals(null,son.Named_Account__c);  
        Test.startTest();
        Named_Account__c newNA = UtilUnitTest.createActiveNamedAccount(grandParent);
        grandParent.Named_Account__c = newNA.id;
        update grandParent;
        Test.stopTest();
        son = [select id,Is_Named_Account__c,Named_Account__c from Account where id = :child.id];
        System.assertNotEquals(newNA.id,son.Named_Account__c);        
        
     }
     
     /*
     static testMethod void testRootSetting(){
     	 Account root = UtilUnitTest.createHWChinaAccount('r');
     	 root = [select id,HQ_Parent_Account__c from Account where id = :root.id];
     	 System.assertEquals(null,root.HQ_Parent_Account__c);
     	 Account parent = UtilUnitTest.createHWChinaAccount('p');
     	 Account child = UtilUnitTest.createHWChinaAccount('c');
     	 Account grandchild = UtilUnitTest.createHWChinaAccount('g');
     	 child.ParentId = parent.id;
     	 grandchild.ParentId = child.id;
     	 update new Account[]{child,grandchild};
     	 Test.startTest();
     	 parent.ParentId = root.id;
     	 update parent;
     	 Test.stopTest();
     	 grandchild = [select id,HQ_Parent_Account__c from Account where id = :grandchild.id];
     	 System.assertEquals(root.id,grandchild.HQ_Parent_Account__c);
     }
     */
    
}