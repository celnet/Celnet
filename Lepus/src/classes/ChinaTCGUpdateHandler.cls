/*
* @Purpose : 1、TCG创建或更新后，自动刷新其关联的商业客户
*						 2、TCG创建或更新后，自动刷新其关联认证渠道
*						 3、TCG创建或更新后，自动创建或更新销售目标  
*            fires before update & insert    after update & insert   
* @Author : Steven
* @Date : 2014-02-28
*/
public without sharing class ChinaTCGUpdateHandler implements Triggers.Handler{
	public static Boolean isFirstRun = true;
	public void handle(){
		if(ChinaTCGUpdateHandler.isFirstRun){
			//Step1: 新增或更新前的操作
			if(Trigger.isBefore){
				Boolean hasAuth = false;
				for(Account tcg : (List<Account>)trigger.new){
					if(tcg.RecordTypeId == CONSTANTS.HUAWEICHINATCGRECORDTYPE){
						//校验该填写的是否都填写了
						String vRequestField = validateTCGRequestField(tcg);
						if(vRequestField == '') {
							//生成TCG名称
							tcg.Name = createTCGName(tcg,Trigger.isUpdate);
							//生成城市信息
							String cityCode = getUsedCity(tcg.TCG_Representative_Office__c);
							if(cityCode == '1') {
								tcg.TCG_City_1__c = tcg.TCG_City_Display__c;
							} else if(cityCode == '2') {
								tcg.TCG_City_2__c = tcg.TCG_City_Display__c;
							} else if(cityCode == '3') {
								tcg.TCG_City_3__c = tcg.TCG_City_Display__c;
							} else {
								tcg.addError('找不到当前代表处"'+ tcg.TCG_Representative_Office__c +'"对应的城市字段');
							}
						} else {
							tcg.addError(vRequestField);
						}
					}
				}
			}
			//Step2: 新增或修改后的操作
			if(Trigger.isAfter){
				Set<Id> tcgInsertIds = new Set<Id>();
				Set<Id> tcgInactiveIds = new Set<Id>();
				Set<Id> tcgUpdateIds = new Set<Id>();
				Map<id,String> tcgRegionMap= new Map<id,String>();
				Map<id,String> tcgOwnerMap= new Map<id,String>();
				for(Account tcg : (List<Account>)trigger.new){
					if(tcg.RecordTypeId == CONSTANTS.HUAWEICHINATCGRECORDTYPE){
						//如果是有效状态的TCG
						if(!tcg.Inactive__c){
							if(Trigger.isInsert) {
								tcgInsertIds.add(tcg.id);
								tcgRegionMap.put(tcg.id,tcg.TCG_Representative_Office__c);
								tcgOwnerMap.put(tcg.id,tcg.OwnerId);
							}
							if(Trigger.isUpdate) { 
								Account oldTcg = (Account)trigger.OldMap.Get(tcg.Id);
								//当owner改变了，share给商业leader
								if(tcg.OwnerId != oldTcg.OwnerId) shareTCGToBusinessLeaderWhenChangeOwner(oldTcg.OwnerId, tcg.Id);
								//如果城市和行业信息没有变化不重新关联客户和经销商
								if((tcg.TCG_City_Display__c == oldTcg.TCG_City_Display__c)&&(tcg.TCG_Industry__c == oldTcg.TCG_Industry__c)){
									continue;
								}
								tcgUpdateIds.add(tcg.Id);
							}
							//校验TCG覆盖内容是否有重复,如果没有重复，为该TCG自动添加客户和添加经销商,如果有重复 报错 
							String vDuplicate = validateDuplicate(tcg);
							if(vDuplicate=='') {
								createTCGMemberImmediately(tcg.TCG_City_Display__c,tcg.TCG_Industry__c,tcg.Id);
								createTCGPartnerImmediately(tcg.TCG_City_Display__c,tcg.TCG_Industry__c,tcg.Id);
							} else {
								tcg.addError(vDuplicate);
							}
						//如果是失效操作，记录下失效的TCG ID
						} else if(Trigger.isUpdate && !((Account)trigger.OldMap.Get(tcg.Id)).Inactive__c){
							tcgInactiveIds.add(tcg.id);
						}
						isFirstRun = false; //不允许重复执行
					}
				}
				//如果是新增TCG,创建TCG目标
				if(tcgInsertIds.size()>0) {
					createTCGTargetImmediately(tcgInsertIds);
					shareTCGToBusinessLeader(tcgInsertIds,tcgRegionMap,tcgOwnerMap);
				}
				//如果更新了TCG,更新目标的名称
				if(tcgUpdateIds.size()>0) updateTCGTargetImmediately(tcgUpdateIds);
				//如果是失效TCG,删除关联的客户渠道机会点
				if(tcgInactiveIds.size()>0) deleteTCGRelateInfoImmediately(tcgInactiveIds);
			}
		}
	}
	
	/**
	 * 生成TCG名称
	 */
	private String createTCGName(Account tcg,Boolean isUpdate){
		String tempName = 'TCG-' + tcg.TCG_Representative_Office__c + '-' + tcg.TCG_City_Display__c + '-' + tcg.TCG_Level__c;
		String prefix = 'TCG-' + tcg.TCG_Representative_Office__c;
		if(isUpdate) {//如果是更新
			Account accOld = [select name from account where id=:tcg.id];
			String accName = accOld.name;
			//更换了代表处才更新后缀编号
			if(accName.contains(prefix)) {
				tempName = tempName + accName.substring(accName.length()-4);
			} else {
				tempName = tempName + getSuffix(prefix);
			}
		} else {//如果是添加
			tempName = tempName + getSuffix(prefix);
		}
		return tempName;
	}
	
	/**
	 * 获取某一个代表处中TCG的最大编号
	 */
	private String getSuffix(String tcgName){
		Integer maxIndex = 0;
		tcgName = '%' + tcgName + '%';
		List<Account> accList = [select name from account where name like:tcgName];
		for(Account acc:accList){
			try {
				String accName = acc.name;
				String oldSuffix = accName.substring(accName.length()-3);
				Integer index = Integer.valueOf(oldSuffix);
				if(maxIndex < index) maxIndex = index;
			}catch(Exception e ){
			}
		}
		String newSuffix = String.valueOf(maxIndex+1);
		if(newSuffix.length() == 1) newSuffix = '0' + newSuffix;
		if(newSuffix.length() == 2) newSuffix = '0' + newSuffix;
		newSuffix = '-' + newSuffix;
		return newSuffix;
	}
		
	/**
	 * 验证必填信息是否规范填写
	 */
	public static String validateTCGRequestField(Account tcg) {
		String mainIndustry = tcg.TCG_Main_Industry__c;
		String representative = tcg.TCG_Representative_Office__c;
		String displayCity = tcg.TCG_City_Display__c;
		String industry = tcg.TCG_Industry__c;
		String tcgLevel = tcg.TCG_Level__c;
		String result = '';
		if(representative == null || representative == '') {
			result = '请选择一个区域/系统部;';
		} else if(displayCity == null || displayCity == '') {
			result = '请至少选择一个城市;';
		} else if(tcgLevel == null || tcgLevel == '') {
			result = '请选择一个级别;';
		} else if(mainIndustry == null || mainIndustry == '') {
			result = '请至少选择一个行业;';
		} else if(industry == null || industry == '') {
			result = '请至少选择一个子行业;';
		} else {
			List<String> mainInustryList = mainIndustry.split(';');
			for(String m:mainInustryList) {
				if(!industry.contains(m)) result += '请至少选择一个"' + m + '"行业对应的子行业;';
			}
		}
		return result;
	}
	
	/**
	 * 验证TCG覆盖的范围是否有重叠
	 */
	public static String validateDuplicate(Account tcg){
		String result = '';
		String paraCity = getSOQLParameter(tcg.TCG_City_Display__c);
		String paraIndustry = getSOQLParameter(tcg.TCG_Industry__c);
		//判断行业是否覆盖
		String soql = 'SELECT Id,Name FROM Account ' +
									 'WHERE recordtypeid = \''+ CONSTANTS.HUAWEICHINATCGRECORDTYPE +'\' '+
									 	 'AND TCG_Industry__c includes ('+ paraIndustry +') ' +
										 'AND  ( TCG_City_1__c includes ('+ paraCity +') ' +
 												 'OR TCG_City_2__c includes ('+ paraCity +')' + 
 												 'OR TCG_City_3__c includes ('+ paraCity +') ) ';
 		if(tcg.id != null) soql += 'AND id != \''+ tcg.Id +'\'';
		List<Account> accList = (List<Account>)Database.query(soql);
		if(accList.size()>0) {
			result = '当前TCG和"'+accList.get(0).Name+'"(ID:'+ accList.get(0).Id +')覆盖的范围有重叠;'; 
		}
		return result;
	}

	/**
	 * 创建商业目标客户群销售目标
	 */
	private void createTCGTargetImmediately(Set<id> tcgIds){
		List<Sales_Target_Accomplishment__c> targetList = new List<Sales_Target_Accomplishment__c>();
		for(Id tcgId : tcgIds) {
			Sales_Target_Accomplishment__c target = new Sales_Target_Accomplishment__c();
			target.recordtypeid = CONSTANTS.SALESTARGETCHINATCGRECORDTYPE;
			target.Name = 'China TCG';
			target.Year__c = String.valueOf(System.Today().year());
			target.China_TCG_Name__c = tcgId;
			target.China_Order_Target__c = 0;
			target.China_Order_Accomplish__c = 0;
			target.China_Income_Target__c = 0;
			target.China_Income_Accomplish__c = 0;
			target.OwnerId = Userinfo.getUserId();
			targetList.add(target);
		}
		insert targetList;
	}
	
	/**
	 * 更新商业目标客户群销售目标
	 */
	private void updateTCGTargetImmediately(Set<id> tcgIds){
		List<Sales_Target_Accomplishment__c> targetList = 
			[select id,Update_by_System__c  from Sales_Target_Accomplishment__c where China_TCG_Name__c in:tcgIds and inactive__c = false];
		for(Sales_Target_Accomplishment__c st : targetList) {
			st.Update_by_System__c = !st.Update_by_System__c;
		}
		update targetList;
	}
	
	/**
	 * 刷新商业客户的TCG信息；刷新机会点的TCG信息
	 */
	private void createTCGMemberImmediately(String city,String industry,Id tcgId){
		//不允许反复触发客户和机会点的trigger
		AccountTCGUpdateHandler.isFirstRun = false;
		OpportunityUpdateTCGInfo.isFirstRun = false;

		//解除关联的客户
		List<Account> accOldList = 
			[select id,TCG_Name__c from Account where TCG_Name__c =:tcgId AND recordtypeid =: CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE];
		Set<Id> accOldIds = new Set<Id>();
		for(Account acc : accOldList){ 
			acc.TCG_Name__c = null;
			accOldIds.add(acc.id);
		}
		if(accOldList.size()>0){ 
			update accOldList;
		}
		//解除关联的机会点
		List<Opportunity> oppOldList = [Select id,TCG_Name__c From Opportunity Where accountid in:accOldIds and Is_Closed__c = false];
		for(Opportunity opp : oppOldList) {
			opp.TCG_Name__c = null;
		}
		if(oppOldList.size()>0){
			update oppOldList;
		}
		//关联新的客户
		String soql = 'SELECT Id,TCG_Name__c FROM Account '+  
									 'WHERE is_named_account__c = false '+
									 	 'AND recordtypeid = \''+ CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE +'\' '+
									 	 'AND City_Huawei_China__c in ('+ getSOQLParameter(city) +') '+
									 	 'AND Customer_Group__c in ('+ getSOQLParameter(industry) +') ';
		List<Account> accList = (List<Account>)Database.query(soql);
		Set<Id> accIds = new Set<Id>();
		for(Account acc : accList){ 
			acc.TCG_Name__c = tcgId;
			accIds.add(acc.id);
		}
		if(accList.size()>0){ 
			update accList;
		}
		//关联新的机会点
		List<Opportunity> oppList = [Select id,TCG_Name__c From Opportunity Where accountid in:accIds and Is_Closed__c = false];
		for(Opportunity opp : oppList) {
			opp.TCG_Name__c = tcgId;
		}
		if(oppList.size()>0){ 
			update oppList;
		}
	}
	
	/**
	 * 刷新TCG的认证经销商列表
	 */
	private void createTCGPartnerImmediately(String city,String industry,Id tcgId){
		//清除TCG的关联渠道列表 
		List<TCG_Related_Partner__c> oldOCList = [select id from TCG_Related_Partner__c where TCG_Name__c =:tcgId];
		if(oldOCList.size()>0){ 
			delete oldOCList;
		}
		//搜索出所有TCG涵盖的生效的渠道认证记录
		String soql = 'SELECT Id,OC_Name__c FROM TCG_Partner_Certification__c '+  
									 'WHERE Overdue__c = false '+
									 	 'AND Certification_City__c in ('+ getSOQLParameter(city) +') '+
									 	 'AND Certification_Sub_Industry__c includes ('+ getSOQLParameter(industry) +') ';
		List<TCG_Partner_Certification__c> pcList = (List<TCG_Partner_Certification__c>)Database.query(soql);
		Set<Id> partnerIds = new Set<Id>();
		for(TCG_Partner_Certification__c pc : pcList){ 
			partnerIds.add(pc.OC_Name__c);
		}
		if(partnerIds.size()==0) return;
		//插入TCG新的关联渠道列表
		List<TCG_Related_Partner__c> newOCList = new List<TCG_Related_Partner__c>();
		for(Id partnerId : partnerIds){ 
			TCG_Related_Partner__c newOC = new TCG_Related_Partner__c();
			newOC.TCG_Name__c = tcgId;
			newOC.OC_Name__c = partnerId;
			newOCList.add(newOC);
		}
		if(newOCList.size()>0){
			insert newOCList;
		}
	}
	
	/**
	 * TCG失效后同客户和机会点解除关联,删除认证经销商列表
	 */
	private void deleteTCGRelateInfoImmediately(Set<Id> inactiveTCGIds){
		//不允许反复触发客户和机会点的trigger
		AccountTCGUpdateHandler.isFirstRun = false;
		OpportunityUpdateTCGInfo.isFirstRun = false;
		
		//解除旧的关联客户
		List<Account> accOldList = [select id,TCG_Name__c from Account where TCG_Name__c in: inactiveTCGIds];
		Set<Id> accOldIds = new Set<Id>();
		for(Account acc : accOldList){ 
			acc.TCG_Name__c = null;
			accOldIds.add(acc.id);
		}
		if(accOldList.size()>0){ 
			update accOldList;
		}
		//解除旧的关联机会点
		List<Opportunity> oppOldList = [Select id,TCG_Name__c From Opportunity Where accountid in:accOldIds and Is_Closed__c = false];
		for(Opportunity opp : oppOldList) {
			opp.TCG_Name__c = null;
		}
		if(oppOldList.size()>0){ 
			update oppOldList;
		}
		//解除旧的关联认证经销商
		List<TCG_Related_Partner__c> oldOCList = [select id from TCG_Related_Partner__c where TCG_Name__c in: inactiveTCGIds];
		if(oldOCList.size()>0){ 
			delete oldOCList;
		}
	}

	/**
	 * 辅助方法：将MultiPicklist转换成String 如：input="中国;美国"，Output="'中国','美国'" 
	 */
	public static String getSOQLParameter(String industry) {
		String result = '';
		if(industry != null) {
			List<String> inustryList = industry.split(';');
			for(String i:inustryList) {
				result += '\'' + i + '\',';
			}
			if(result.length()>0) {
				result = result.substring(0,result.length()-1);
			}
		}
		return result;
	}
	
  /**
	 * 根据当前用户控制哪个CITY字段显示
	 */
	public static String getUsedCity(String region) {
		String result = '0';
		if(region!=null) {
			if(region.equals('北京代表处')||region.equals('成都代表处')||region.equals('福州代表处')||
				 region.equals('广州代表处')||region.equals('贵阳代表处')||region.equals('哈尔滨代表处')||
				 region.equals('杭州代表处')||region.equals('合肥代表处')||region.equals('呼和浩特代表处')||
				 region.equals('济南代表处')){
				result = '1';
			}
			if(region.equals('昆明代表处')||region.equals('兰州代表处')||region.equals('南昌代表处')|| 
				 region.equals('南京代表处')||region.equals('南宁代表处')||region.equals('上海代表处')|| 
				 region.equals('沈阳代表处')||region.equals('石家庄代表处')||region.equals('太原代表处')||
				 region.equals('天津代表处')){
				result = '2';
			}
			if(region.equals('乌鲁木齐代表处')||region.equals('武汉代表处')||region.equals('西安代表处')|| 
				 region.equals('长春代表处')||region.equals('长沙代表处')||region.equals('郑州代表处')||
				 region.equals('重庆代表处')){
				result = '3';
			}
		}
		return result;
	}
	
	/**
	 * 新增时将TCG共享给商业leader
	 */
	private void shareTCGToBusinessLeader(Set<Id> tcgInsertIds, Map<id,String> tcgRegionMap,Map<id,String> tcgOwnerMap){
		//查询出所有的商业Leader清单
		List<Special_Role__c> srList = 
			[Select User__c,Representative_Office__c  FROM Special_Role__c where Role_Name__c = 'China_Business_Leader'];
		if(srList.size()==0)return;
		//共享给对应区域的商业Leader
		List<AccountShare> shareRecords = new List<AccountShare>();
		for(Id i : tcgInsertIds){
			String region = tcgRegionMap.get(i);
			for(Special_Role__c sr : srList){
				//当商业leader不是TCG的Owner的时候共享权限
				Id tcgOwnerId = tcgOwnerMap.get(i);
				if(sr.Representative_Office__c == region && tcgOwnerId != sr.User__c) { 
					AccountShare sharingRecord = new AccountShare(AccountId = i,UserOrGroupId = sr.User__c,
						AccountAccessLevel = 'Read',OpportunityAccessLevel = 'None',CaseAccessLevel = 'None', ContactAccessLevel = 'None');
					shareRecords.add(sharingRecord);
				}
			}
		}
		if(shareRecords.size()>0) insert shareRecords;
	}
	
	/**
	 * ChangeOwner时将TCG共享给商业leader
	 */
	private void shareTCGToBusinessLeaderWhenChangeOwner(Id oldOwnerId, Id tcgId){
		//查询当前用户是否是商业Leader
		List<Special_Role__c> srList = 
			[Select Id FROM Special_Role__c where Role_Name__c = 'China_Business_Leader' AND User__c =:oldOwnerId];
		if(srList.size()==0)return;
		//共享给商业Leader
		AccountShare sharingRecord = new AccountShare(AccountId = tcgId,UserOrGroupId = oldOwnerId,
			AccountAccessLevel = 'Read',OpportunityAccessLevel = 'None',CaseAccessLevel = 'None', ContactAccessLevel = 'None');
		insert sharingRecord;
	}
	
	/**
	 * 检查用户是否有创建和更改TCG的权限 (暂不启用)
	 */
	/*
	private Boolean validateAuthority() {
		Boolean hasAuth = true;
		User currentUser = [select Profile_Name__c from User where id =: Userinfo.getUserId()];
		//如果是一线客户经理，判断是否是商业Leader,只有商业Leader可以创建TCG
		if(currentUser.Profile_Name__c.contains('(Huawei China)代表处客户经理')){
	    //查询出商业leader权限集的ID
	    List<PermissionSet> permissionSetList = [Select Id,name FROM PermissionSet where name = 'China_Business_Leader'];
	    if(permissionSetList.size()>0) {
	    	Id permissionSetId = permissionSetList.get(0).id;
				//判断当前用户是否分配了商业leader权限集
				List<PermissionSetAssignment> permissionDistributeList = 
					[Select Id FROM PermissionSetAssignment where PermissionSetId =:permissionSetId AND AssigneeId =:Userinfo.getUserId()];
				if(permissionDistributeList.size()==0) {
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'您没有创建权限,只有商业Leader可以创建和编辑中国区TCG.'));
					hasAuth = false;
				}
	    } else {
	    	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'找不到"商业Leader"权限集，请联系系统管理员处理.'));
				hasAuth = false;
	    }
		}
		return hasAuth;
	}
	*/
}