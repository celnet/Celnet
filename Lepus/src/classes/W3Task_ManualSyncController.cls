/**
 * Author : Sunda
 * Des : W3 Task手工同步页面
 **/
public class W3Task_ManualSyncController {
	private List<String> list_needSyncStatus = new List<String>{'Sync Failed Again','Open'};
	
	public List<SyncFailRecordWrapper> list_sfw{get;set;}
	public List<W3_Task_Sync_Fail__c> list_ExpiredSF{get;set;}
	
	public String sSyncMsg{get;set;}
	public String sSyncSeverity{get;set;}
	public Boolean syncMsgShow{get;set;}
	public String sDelAllMsg{get;set;}
	public String sDelAllSeverity{get;set;}
	public Boolean delAllMsgShow{get;set;}
	
	public W3Task_ManualSyncController(){
		this.initSyncFailRecord();
	}
	//找到所有失败记录
	private void initSyncFailRecord(){
		List<W3_Task_Sync_Fail__c> list_FailRecord = [Select w.type_x__c, w.time_x__c, w.taskUUID__c, w.taskURL__c, w.taskDesc__c, 
				w.reserve3__c, w.handler__c, w.applicant__c, w.appURL__c, w.appName__c, w.WorkItem_Step_Id__c, w.Name, 
				w.ProcessInstanceId__c, w.IsDeleted, w.Id, w.ErrorMessage__c, w.ActionType__c, w.SyncStatus__c  
				From W3_Task_Sync_Fail__c w
				Where SyncStatus__c in: list_needSyncStatus order by SyncStatus__c,ActionType__c];
		this.initNeedSyncRecord(list_FailRecord);
	}
	//筛选需要同步的记录，排除已经过期的数据
	private void initNeedSyncRecord(List<W3_Task_Sync_Fail__c> list_FailRecord){
		list_sfw = new List<SyncFailRecordWrapper>();
		list_ExpiredSF = new List<W3_Task_Sync_Fail__c>();
		List<ID> list_addWorkItemIds = new List<ID>();
		List<ID> list_ExpiredRecordIds = new List<ID>();
		for(W3_Task_Sync_Fail__c sf : list_FailRecord){
			if(sf.ActionType__c == 'add'){
				list_addWorkItemIds.add(sf.WorkItem_Step_Id__c);
			}
		}
		Set<String> set_validIds = this.checkDate(list_addWorkItemIds , list_FailRecord);
		for(W3_Task_Sync_Fail__c sf : list_FailRecord){
			if(sf.ActionType__c == 'add'){
				if(set_validIds != null && !set_validIds.contains(sf.taskUUID__c)){
					list_ExpiredSF.add(sf);
					continue;
				}
			}
			list_sfw.add(new SyncFailRecordWrapper(sf));
		}
	}
	//检查work item是否还存在，即需要同步的数据是否有效
	private Set<String> checkDate(List<ID> list_workItemIds , List<W3_Task_Sync_Fail__c> list_syncFail){
		Set<String> set_validIds = new Set<String>();
		for(ProcessInstanceWorkitem wi : [Select id,SystemModstamp From ProcessInstanceWorkitem Where Id in: list_workItemIds]){
			for(W3_Task_Sync_Fail__c sf:list_syncFail){
				//有效数据的条件，WorkItem还存在+workitem的修改时间<=失败记录记载的time x
				String wiTime = wi.SystemModstamp.yearGmt()+'-'+wi.SystemModstamp.monthGmt()+'-'+wi.SystemModstamp.dayGmt()
				+' '+wi.SystemModstamp.hourGmt()+':'+wi.SystemModstamp.minuteGmt()+':'+wi.SystemModstamp.secondGmt();
				if(sf.ActionType__c == 'add' && sf.WorkItem_Step_Id__c == wi.Id && datetime.valueOf(wiTime) <= datetime.valueOf(sf.time_x__c)){
					set_validIds.add(sf.taskUUID__c);
				}
			}
		}
		return set_validIds;
	}
	//Sync按钮
	public void SyncAgain(){
		this.sSyncMsg = '';
		this.sSyncSeverity = '';
		this.syncMsgShow = false;
		List<W3_Task_Sync_Fail__c> list_sf = new List<W3_Task_Sync_Fail__c>();
		for(SyncFailRecordWrapper sfw : list_sfw){
			if(sfw.isSelected){
				list_sf.add(sfw.syncFail);
			}
		}
		if(list_sf.size() > 10){//调用batch
			W3Task_ManualSyncBatch mSyncBatch = new W3Task_ManualSyncBatch(list_sf);
			database.executeBatch(mSyncBatch, 1);
			this.sSyncMsg = '已提交系统，系统运行完成后会邮件通知您。';
			this.sSyncSeverity = 'confirm';
			this.syncMsgShow = true;
		}else if(list_sf.size() <= 10 && list_sf.size() > 0){//直接callout
			this.syncToEIP(list_sf);
			this.sSyncMsg = '同步操作完成。';
			this.sSyncSeverity = 'confirm';
			this.syncMsgShow = true;
		}else{//未选择记录
			this.sSyncMsg = '请选择至少一条记录。';
			this.sSyncSeverity = 'warning';
			this.syncMsgShow = true;
		}
		this.initSyncFailRecord();
	}
	public void DeleteRecord(){
		this.sSyncMsg = '';
		this.sSyncSeverity = '';
		this.syncMsgShow = false;
		List<W3_Task_Sync_Fail__c> list_sf = new List<W3_Task_Sync_Fail__c>();
		for(SyncFailRecordWrapper sfw : list_sfw){
			if(sfw.isSelected){
				list_sf.add(sfw.syncFail);
			}
		}
		if(list_sf.size() > 0){
			delete list_sf;
			this.sSyncMsg = '完成。';
			this.sSyncSeverity = 'confirm';
			this.syncMsgShow = true;
		}else{
			this.sSyncMsg = '请选择至少一条记录。';
			this.sSyncSeverity = 'warning';
			this.syncMsgShow = true;
		}
		this.initSyncFailRecord();
	}
	public void DeleteAll(){
		delete this.list_ExpiredSF;
		this.list_ExpiredSF = new List<W3_Task_Sync_Fail__c>();
		
		this.delAllMsgShow = true;
		this.sDelAllMsg = '完成。';
		this.sDelAllSeverity = 'confirm';

	}
	//发送值EIP
	public void syncToEIP(List<W3_Task_Sync_Fail__c> list_sf){
		List<W3_Task_Sync_Fail__c> list_syncSuccess = new List<W3_Task_Sync_Fail__c>();
		List<W3_Task_Sync_Fail__c> list_syncFailed = new List<W3_Task_Sync_Fail__c>();
		for(W3_Task_Sync_Fail__c sf : list_sf){
			sf = this.SendToW3(this.SyanFailRecord2W3Task(sf), sf, null);
			if(sf.SyncStatus__c == 'Sync Success'){
				list_syncSuccess.add(sf);
			}else{
				list_syncFailed.add(sf);
			}
		}
		//删除同步成功的数据，更新同步失败的数据
		system.debug('delete list'+list_syncSuccess.size()+' update list:'+list_syncFailed.size());
		if(list_syncSuccess.size() > 0){
			delete list_syncSuccess;
		}
		if(list_syncFailed.size() > 0){
			update list_syncFailed;
		}
	}
	//调用发送接口，发送数据
	public W3_Task_Sync_Fail__c SendToW3(W3Task_Info.W3Task task , W3_Task_Sync_Fail__c syncFail , String sEncoded){
		W3Task_Service.SalesForce_W3_HTTPSPort taskToW3 = new W3Task_Service.SalesForce_W3_HTTPSPort();
		taskToW3.timeout_x = W3TASK_CONSTANTS.TIME_OUT;
		taskToW3.PartyInfo = new W3Task_Partyinfo.PartyInfo_element();
		W3Task_Partyinfo.party pFrom = new W3Task_Partyinfo.party();
		pFrom.name = W3TASK_CONSTANTS.SOAP_FROM_NAME;  //'Cu32SF2W3';
		W3Task_Partyinfo.party pTo = new W3Task_Partyinfo.party();
		pTo.name = W3TASK_CONSTANTS.SOAP_TO_NAME; //'Huawei';
		taskToW3.PartyInfo.from_x = pFrom;
		taskToW3.PartyInfo.to = pTo;
		taskToW3.PartyInfo.operationID = 'SalesForce/W3/'+(syncFail.ActionType__c=='add'?'createTask4W3':'deleteTask4W3');
		taskToW3.PartyInfo.operationType = 'SyncRequestResponse';
		taskToW3.PartyInfo.transactionID = task.taskUUID+datetime.now().format('YYYY-MM-DD HH:MM:SS');
		system.debug('Task info:'+task);
		System.debug('TaskToW3:'+taskToW3);
		Boolean success ;
		try{
			if(syncFail.ActionType__c == 'add'){
				success = taskToW3.createTask4W3(task, sEncoded);
				system.debug('Send to W3:' + task);
				system.debug('Create result:===' + success);
			}else if(syncFail.ActionType__c == 'del'){
				success = taskToW3.deleteTask4W3(task.taskUUID, sEncoded);
				system.debug('Delete w3:' + task);
				system.debug('Delete result:===' + success);
			}
			
			if(success){
				syncFail.SyncStatus__c = 'Sync Success';
			}else{
				syncFail.SyncStatus__c = 'Sync Failed Again';
				syncFail.ErrorMessage__c = syncFail.ErrorMessage__c + '\n API Return False';
			}
		}catch(exception e){
			system.debug('get exception :'+e.getMessage());
			syncFail.SyncStatus__c = 'Sync Failed Again';
			syncFail.ErrorMessage__c = syncFail.ErrorMessage__c + '\n'+e.getMessage();
		}
		return syncFail;
	}
	//封装W3Task
	private W3Task_Info.W3Task SyanFailRecord2W3Task(W3_Task_Sync_Fail__c syncFail){
		W3Task_Info.W3Task w3Task = new W3Task_Info.W3Task();
		w3Task.appName = syncFail.appName__c ;
		w3Task.appURL = syncFail.appURL__c ;
		w3Task.type_x = syncFail.type_x__c ;
		w3Task.taskDesc = syncFail.taskDesc__c ;
		w3Task.taskURL = syncFail.taskURL__c ;
		w3Task.taskUUID = syncFail.taskUUID__c ;
		w3Task.time_x = syncFail.time_x__c ;
		w3Task.handler = syncFail.handler__c ;
		w3Task.applicant = syncFail.applicant__c ;
		w3Task.reserve3 = syncFail.reserve3__c;
		return w3Task;
	}
	public class SyncFailRecordWrapper{
		public W3_Task_Sync_Fail__c syncFail{get;set;}
		public Boolean isSelected{get;set;}
		public SyncFailRecordWrapper(W3_Task_Sync_Fail__c sf){this.syncFail = sf;isSelected=false;}
	}
}