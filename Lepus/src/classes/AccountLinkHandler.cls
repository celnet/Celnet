/**
 * @Purpose : 当客户关联的申请被审批通过后,自动建立起父子关系
 * @Author : Steven
 * @Date :   2014-07-01 Update
 */
public without sharing class AccountLinkHandler implements Triggers.Handler{//after update
	public void handle() {
		List<Account> childList = new List<Account>();
		for(Account_Link_Request__c r : (List<Account_Link_Request__c>)Trigger.new){
			if(r.IsApproved__c && !((Account_Link_Request__c)Trigger.oldMap.get(r.id)).IsApproved__c){
				Account child = new Account(id = r.child__c);
				child.ParentId = r.parent__c;
				childList.add(child);
			}
		}
		if(childList.size()>0) update childList;
	}
}