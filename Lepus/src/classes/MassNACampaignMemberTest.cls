@isTest
private class MassNACampaignMemberTest 
{
        static Account[] createNAAccounts()
        {
            Account acc1 = UtilUnitTest.createHWChinaAccount('test account1');
            Account acc2 = UtilUnitTest.createHWChinaAccount('test account2');
            Account acc3 = UtilUnitTest.createHWChinaAccount('test account3');
            acc1.Is_Named_Account__c = true;
            acc2.Is_Named_Account__c = true;
            acc3.Is_Named_Account__c = true;
            Account[] accs = new Account[] {acc1, acc2, acc3};
            update accs;
            return accs;
        } 
        
    static testMethod void testMassCampaignNAMember() 
    {
        List<Account> accs = createNAAccounts();
        test.startTest();
        String query = 'Select ID, Name From Account Where Is_Named_Account__c = true';
        MassNAListSyncBatch naSyncBatch = new MassNAListSyncBatch();
        naSyncBatch.query = query;
            Database.executeBatch(naSyncBatch, 1000); 
            test.stopTest();
        List<NA_List__c> nas = [Select ID, Name, Account__c From NA_List__c Where Account__c In: accs];
        
            ID rtId = [Select ID, Name From RecordType Where DeveloperName = 'Huawei_China_Campaign' And SobjectType = 'Campaign'].ID;
        Campaign camp = new Campaign();
        camp.Name = 'Test Campaign for NA Member';
        camp.Status = 'Planned';
        camp.recordTypeId = rtId;
        insert camp;
        
        ApexPages.currentPage().getParameters().put('id', camp.id);
        ApexPages.StandardSetController setCon = new ApexPages.StandardSetController(new List<China_Campaign_NA_Member__c>());
            MassNACampaignMemberExtension massCon = new MassNACampaignMemberExtension(setCon);
            //system.assertEquals(5, massCon.RowSize);//初始如果没有任何成员,则显示5个空行
            for(MassNACampaignMemberExtension.MemberRow row : massCon.MemberRows)
            {
                system.assert(row.NAMember.NA_Account__c == null);
            }
            
            //massCon.MemberRows[4].Checked = true;
           // massCon.MemberRows[3].Checked = true;
           // massCon.MemberRows[2].Checked = true;
            massCon.del();
            massCon.add();
            massCon.add();
          system.assertEquals(2, massCon.RowSize);
            for(MassNACampaignMemberExtension.MemberRow row : massCon.MemberRows)
            {
                system.assert(row.NAMember.NA_Account__c == null);
            }
            
            massCon.MemberRows[0].NAMember.NA_Account__c = nas[0].Id;
            massCon.MemberRows[1].NAMember.NA_Account__c = nas[1].Id;
            massCon.save();//保存成功
            for(MassNACampaignMemberExtension.MemberRow row : massCon.MemberRows)
            {
                system.assert(row.Saved);
            }
            
            massCon.add();
            system.assertEquals(3, massCon.RowSize);
            massCon.save();//保存失败,因为没有填写NA Account
            system.assert(!massCon.MemberRows[2].Saved);
            
            massCon.MemberRows[2].NAMember.NA_Account__c = nas[0].Id;//选择重复,第三行与第一行重复
            massCon.DuRowNumber = massCon.MemberRows[2].RowNumber;
            massCon.checkDuplicate();
            system.assert(massCon.SaveDisabled);
            
            massCon.MemberRows[2].NAMember.NA_Account__c = nas[2].Id;//这次不重复了, 保存三行记录
            massCon.save();
            for(MassNACampaignMemberExtension.MemberRow row : massCon.MemberRows)
            {
                system.assert(row.Saved);
            }
            
            massCon.MemberRows[2].Checked = true;
            massCon.del();//删除最后一行已经保存的记录
            system.assertEquals(2, massCon.RowSize);
            
            massCon.cancel();
    }
}