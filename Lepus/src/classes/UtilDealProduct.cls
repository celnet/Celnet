/************************************************************************************************** 
* Name             : Utility for TigDealProduct 
* Object           : Deal_Registration__c, Deal_Registration_Product__c, Product Master 
* Requirement      : 
* Purpose          : Update the Deal Registration Product Level1 field of all related Deal Registrations 
* Author           : Lingyanxia 
* Create Date      : 05/31/2013 
* Modify History   :  
* ***************************************************************************************************/
public class UtilDealProduct {
  public static void processDealProductChange(List<Deal_Registration_Product__c> productTrigger) {
    if (trigger.isExecuting && trigger.isAfter) {
      Set<Id> dealIds = new Set<Id>();  
      if (trigger.isDelete || trigger.isInsert) {
        for (Deal_Registration_Product__c product : productTrigger) {  
          dealIds.add(product.Deal_Registration__c);
        }
      }
      else if (trigger.isUpdate) {
        for (Deal_Registration_Product__c newProduct : productTrigger) {
          Deal_Registration_Product__c oldProduct = (Deal_Registration_Product__c) trigger.oldMap.get(newProduct.Id);
          if (oldProduct.Lookup_Product_Master__c != newProduct.Lookup_Product_Master__c) {
            dealIds.add(newProduct.Deal_Registration__c);
          }
        }
        if (dealIds.size() == 0) {
          return;
        }
      }
      if (dealIds.size() > 0) {
        processDealLevel(dealIds);
      }
    }
  }

  public static void processDealLevel(Set<Id> dealIds) {
    List<Deal_Registration__c> deals = [SELECT Id, Deal_Product_Level1__c,  
        ( SELECT Id, Deal_Registration__c, Lookup_Product_Master__r.Level_1__c 
          FROM Deal_Registration_Products__r)
        FROM Deal_Registration__c
        WHERE Id IN : dealIds];
    Map<Id, Deal_Registration__c> dealMap = new Map<Id, Deal_Registration__c>(); 
    for (Deal_Registration__c deal : deals) {
      String dealLevel = '';
      
      for (Deal_Registration_Product__c product : deal.Deal_Registration_Products__r) {
        if (product.Lookup_Product_Master__r.Level_1__c != null && product.Lookup_Product_Master__r.Level_1__c != ''
            && !dealLevel.contains(product.Lookup_Product_Master__r.Level_1__c)) {  
          dealLevel += product.Lookup_Product_Master__r.Level_1__c + '/';
        }
      }
      if(dealLevel.length()>0)
      {
          dealLevel=dealLevel.substring(0,dealLevel.length()-1);
      }
      deal.Deal_Product_Level1__c = dealLevel;
      dealMap.put(deal.Id, deal);
    }
    
    try {
      update dealMap.values();
    }
    catch (DMLException e) {
      if (trigger.isDelete) {
        trigger.old[0].addError(e.getDMLMessage(0));
      }
      else {
        trigger.new[0].addError(e.getDMLMessage(0));
      }
      System.debug('UtilDealProduct.processDealLevel Exception: ' + e);
    }
  }
}