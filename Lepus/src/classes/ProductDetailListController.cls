public class ProductDetailListController {

/*    public String getPath() {
        return null;
    }
    */ 
    public String msg{
        get{return msg;}
        set{msg=value;}
    } 
    public String pn{   //product name
        get{return pn;}
        set{pn=value;}
    }    public String id{  //Opp product id
        get{return id;}
        set{id=value;}
    } 
    public String oppid{  //Opp  id
        get{return oppid;}
        set{oppid=value;}
    }
    List<Product_Details__c> ProductDetail=new List <Product_Details__c>();
    public List<Product_Details__c> getProductDetail(){
        return ProductDetail;
    }
    public class myProductModule{
        Product_Module__c pm;
    //  boolean selected; 
        integer Quantity;
        public myProductModule( Product_Module__c productmodule) { pm=productmodule;}
        public Product_Module__c getPM() { return pm; }
    //  public boolean getSelected() { return selected; }
    //  public void setSelected(boolean sb) { selected = sb; }
        public integer getQuantity() { return Quantity; }
        public void setQuantity(integer i) { Quantity = i; }
    }
    myProductModule [] ProductModules=null;
    public List<myProductModule> getProductModules(){  //根据产品名称和当前用户profile，从产品模块对象中select出所有待配置的产品模块       
        pn=ApexPages.currentPage().getParameters().get('pn');
        msg = msg + ('\n' + pn + '\n');       
        string currentUserProfile,ownerProfile;     
        currentUserProfile=getCurrentUserProfile();      
      // 当前用户和产品模块owner都属于国内或者国际时，当前用户才有查看该产品模块的权限 
        if(ProductModules == null){
         ProductModules = new myProductModule[]{};
            List<Product_Module__c> PMList=[select BOM_Code__c,name,List_Price__c,Module_Name__c,OwnerId,Module_Type__c,Nation_Int__c
                                        from Product_Module__c where name =:pn and Active__c='Yes'  order by Module_Type__c, BOM_Code__c asc];
            for( Product_Module__c pm:PMList ){
             // ownerProfile=getOwnerProfile(pm.OwnerId);               
                 if(((currentUserProfile.contains('国内销服')&&!(currentUserProfile.contains('AP'))) && (pm.Nation_Int__c.contains('Nation')|| pm.Nation_Int__c.contains('国内'))) 
                     ||(currentUserProfile.contains('International') &&( pm.Nation_Int__c.contains('International')|| pm.Nation_Int__c.contains('国际')))
                     ||((currentUserProfile.contains('国内销服')&& currentUserProfile.contains('AP'))&& pm.Nation_Int__c.contains('AP')))
                 
                 ProductModules.add( new myProductModule(pm) );   
                                      
                 else if(currentUserProfile.contains('System Administrator')|| currentUserProfile.contains('系统管理员'))                 
                 ProductModules.add( new myProductModule(pm) );
            }    
        }
        return ProductModules;
    }
    
      
    public List<myProductModule> getmyProductModules(){  //根据用户配置的产品模块数量，决定要添加哪些产品模块
        pn=ApexPages.currentPage().getParameters().get('pn');
        List<myProductModule> myProductModule=new List<myProductModule>();
        
        for(myProductModule PM : ProductModules){
            if(PM.Quantity>0){myProductModule.add(PM);
            }
        } 
        return myProductModule;
    }
    
    
    public string getCurrentUserProfile(){  //获取当前用户的profile名称      
        List<Profile> PList=[select name from profile where id =:userinfo.getProfileId() limit 1];      
        string PName=PList[0].Name;     
        return PName;           
    }
/*  
    public string getOwnerProfile(Id ownerId){  //获取Product Module Owner的profile名称      
        List<User> Owner=[select profileId from user where id=:ownerId limit 1];        
        List<Profile> PList=[select name from profile where id =:Owner[0].ProfileId limit 1];       
        string PName=PList[0].Name;     
        return PName;       
    }
*/

/*
    public PageReference Next(){
        PageReference pr=null;
        String url='';
        id=ApexPages.currentPage().getParameters().get('id');
        oppid=ApexPages.currentPage().getParameters().get('oppid');
    //  doSearch();
        Product_Details__c pd;
        
        integer bool=0,inser=0;
        
        for(Product_Module__c PM : ProductModule){
            pd=new Product_Details__c( 
            lookup__c = PM.id,
            Project_Name__c = oppid, 
            Project_Product__c = id,
            Quantity__c=1,              
        //  Module_Name__c=PM.Module_Name__c,
        //  List_Price__c=PM.List_Price__c,
            name=PM.BOM_Code__c
            );
            ProductDetail.add(pd);
            bool=1;
        }
    
        //url='/apex/MassUpdateOppProductDetail?scontrolCaching=1&id='+id;
        url='/apex/CreateOppProductDetail?scontrolCaching=1';
    
        pr=new PageReference(url);
        pr.setRedirect(true); 
        
        return pr; 
    }
*/      
        
    public PageReference Save(){
        PageReference pr;
        id=ApexPages.currentPage().getParameters().get('id');
        oppid=ApexPages.currentPage().getParameters().get('oppid');
        Product_Details__c pd;          
        for(myProductModule PM : getmyProductModules()){
            if(PM.Quantity >0 ) {pd=new Product_Details__c( 
                    lookup__c = PM.pm.id,
                    Project_Name__c = oppid, 
                    Project_Product__c = id,
                    Quantity__c=PM.Quantity,            
                    name=PM.pm.BOM_Code__c
                );
                ProductDetail.add(pd);   
            }           
        }
        pr=new PageReference(ApexPages.currentPage().getUrl()+'/size='+ProductDetail.size());
        
        try{
            if(ProductDetail!=null && ProductDetail.size()>0)
            {insert(ProductDetail); }
                
        }
        catch(Exception e){             
            msg='创建产品明细失败'+e;
        }   
        return null; 
    }   
    public void cancel(){
        //return null;
    }
    public static testMethod void currClsTest(){
		String msg = null;
			Product_HS__c hs = UtilUnitTest.createProductMaster('test');
			Account acc = UtilUnitTest.createAccount();
			Opportunity opp = UtilUnitTest.createOpportunity(acc);
			UtilUnitTest.createProjectProduct(opp, hs);
        Project_Product__c pm=[select product_name__c, Project_Name__c,id from Project_Product__c limit 1];
        ApexPages.currentPage().getParameters().put('id', pm.id);
        ApexPages.currentPage().getParameters().put('pn', pm.Product_Name__c);
        ApexPages.currentPage().getParameters().put('oppid', pm.Project_Name__c);
        Profile pf=[select id,Name from Profile where Name like '%(Huawei)Country Account Manager%' limit 1];
        List<User> userList = [SELECT Id FROM User WHERE IsActive = true and profileId =: pf.Id];
        User u2 = [select id from user limit 1];
        User u = UtilUnitTest.newUserWithoutInsert(pf.id);
        System.debug('\n' + userList.size() + '\n' + u.Id + '\n');
        System.runAs(u){
            ProductDetailListController pdlc=new ProductDetailListController();
            myProductModule [] ProductModules=pdlc.getProductModules();
            System.debug('\n' + productModules.size() + '\n');
            for(myProductModule PM1 : ProductModules){
            	System.debug('\nEntered!\n');
                PM1.Quantity=1;
                Product_Module__c PModule=PM1.getPM();
                PM1.setQuantity(1);
                integer Q=PM1.getQuantity();
            } 
            
//          pdlc.getmyProductModules();
            pdlc.getCurrentUserProfile();
            List<Product_Details__c> detaillist=pdlc.getProductDetail();
//          pdlc.getOwnerProfile(ownerId)
            //mes.getNewselected();
            pdlc.save();
        }
        
        // Run Test as the System Admin
        ProductDetailListController pdlc=new ProductDetailListController();
        myProductModule [] ProductModules=pdlc.getProductModules();
        System.debug('\n' + productModules.size() + '\n');
        for(myProductModule PM1 : ProductModules){
        	System.debug('\nEntered!\n');
            PM1.Quantity=1;
            Product_Module__c PModule=PM1.getPM();
            PM1.setQuantity(1);
            integer Q=PM1.getQuantity();
        } 
            
        pdlc.getCurrentUserProfile();
        List<Product_Details__c> detaillist=pdlc.getProductDetail();
        pdlc.save();
        
    //  test cancel
        Project_Product__c pm1=[select product_name__c, Project_Name__c,id from Project_Product__c limit 1];
        ApexPages.currentPage().getParameters().put('id', pm1.id);
        ApexPages.currentPage().getParameters().put('pn', pm1.Product_Name__c);
        ApexPages.currentPage().getParameters().put('oppid', pm1.Project_Name__c);
        Profile pf1=[select id,Name from Profile where Name like '%System Administrator%' limit 1];
        User u1 = [select Id,profileId,IsActive from User where IsActive=true and profileId = :pf1.id limit 1];
        System.runAs(u1){
            ProductDetailListController pdlc1=new ProductDetailListController();
            myProductModule [] ProductModules1=pdlc1.getProductModules();
            pdlc1.cancel();
        }
        System.Debug(msg);
    }
}