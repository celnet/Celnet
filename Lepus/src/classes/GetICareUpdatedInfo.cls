public without sharing class GetICareUpdatedInfo 
{

	public String currId {get;set;}
  public Boolean isSuccess {get;set;}
  public String url{get;set;}
  
  public GetICareUpdatedInfo(ApexPages.StandardController stdController){
		currId =stdController.getId();
  }
    
	public PageReference init(){
		//1、判断是否有iCare Number，不存在返回		
		Case sr = [select id,CaseNumber,iCare_Number__c,RecordType.DeveloperName,Actual_Restore_Date_Time__c,
		iCare_Group__r.id,iCare_Group__r.Name,iCare_Group__r.iCare_ID__c,Employee_ID__c,
		iCare_Owner__r.Employee_ID__c,HW_Product__r.Offering_Code__c 
		from case where id = :currId limit 1];
		if(sr.iCare_Number__c == null || sr.iCare_Number__c == ''){	//还未同步到iCare，不能发起请求
			isSuccess=false;
			Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR,'该单还未同步到iCare，无法请求更新。'));
			return null; //Page.ServiceRequestLoading;
		}else{
			//Datetime now = Datetime.now();
		try{	
				//ServiceRequestSyncUtils.writeLog(sr.id,sr.iCare_Number__c, '压力测试主动更新,SF端发出请求', '');
				List<B2bSoapGissue6.tSRStatusGet> icareSrList = new List<B2bSoapGissue6.tSRStatusGet>();
				B2bSoapGissue6.tSRStatusGet icareSr = new B2bSoapGissue6.tSRStatusGet();
				
				icareSr.Salesforce_SR_ID = sr.Id;
				icareSr.SF_NUMBER = sr.CaseNumber;
				icareSr.ICARE_NUMBER = sr.iCare_Number__c;
				icareSr.RecordType_Developer_Name = sr.RecordType.DeveloperName;
				icareSr.Actual_Restore_Time = sr.Actual_Restore_Date_Time__c == null ? null : 
					(sr.Actual_Restore_Date_Time__c).format('yyyy-MM-dd\'T\'HH:mm:ss','Asia/Shanghai') + '+08:00';
				icareSr.iCare_Group_Name = sr.iCare_Group__r.Name ; 
				icareSr.AUX1 = String.valueOf(sr.iCare_Group__r.iCare_ID__c ); 
				icareSr.iCare_Owner_Employee_No = sr.iCare_Owner__r.Employee_ID__c != null ? 
					sr.iCare_Owner__r.Employee_ID__c : sr.Employee_ID__c ;
				icareSr.Prod_Offer_Code = sr.HW_Product__r.Offering_Code__c;
				icareSrList.add(icareSr);
				 
				SoapIssues6.IssueReq_x1_HTTPSPort huaweiCase = 
					new SoapIssues6.IssueReq_x1_HTTPSPort();
				huaweiCase.timeout_x = EIPSYNCCONSTANTS.BATCHTIMEOUTMS;
				B2bSoapGissue6.tSRStatusReply[] srReply = huaweiCase.GRequest_Response(icareSrList,//'http://requestb.in/1jggpq11');
					EIPSYNCCONSTANTS.GET_ENDPOINT+sr.CaseNumber+System.now().format('yyyyMMddHHmmss'));
				System.debug('the response:--------' + srReply);
				//此处为压力测试需要代码,压力测试完成后需要删除 begin
						//ServiceRequestSyncUtils.writeLog(sr.id,sr.iCare_Number__c, ''+ (datetime.now()), now + '');
						//ServiceRequestSyncUtils.writeLog(sr.id,sr.iCare_Number__c, '压力测试主动更新,SF端接收到响应', ''+ (datetime.now()));
				//此处为压力测试需要代码,压力测试完成后需要删除 end
				if(srReply.size() > 0){
					if('true'.equals(srReply.get(0).FLAG)){//返回结果为true(一定为小写),同步成功
						return new PageReference('/'+currId);  //Page.ServiceRequestLoading;
					}else{
						Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'EIP/iCare返回错误信息：' +
							System.Label.Sync_Error_GetUpdate) );
						ServiceRequestSyncUtils.writeLogImmediate(sr.id,sr.iCare_Number__c, srReply.get(0).FLAG, srReply.get(0).DESCRIPTION);
						return null; //Page.ServiceRequestLoading;
					}
				}else{
					Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'EIP或iCare端无返回值') );
						ServiceRequestSyncUtils.writeLog(sr.id,sr.iCare_Number__c, 'EIP或iCare端无返回值', 'EIP或iCare端无返回值');
					return null; //Page.ServiceRequestLoading;
				}
			}
			catch(Exception ex){
				isSuccess = false;
				System.debug('==========ex');
				//Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'EIP或iCare端同步异常') );
				Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.Sync_Error_Msg + System.Label.Sync_Error_GetUpdate));
				ServiceRequestSyncUtils.writeLog(sr.id,sr.iCare_Number__c, 'EIP/iCare返回错误信息或未返回值：'+ex.getMessage(), 
					'EIP/iCare返回错误信息或未返回值：'+ex.getStackTraceString() );
				return null; //Page.ServiceRequestLoading;
			}
		}
	}
	
}