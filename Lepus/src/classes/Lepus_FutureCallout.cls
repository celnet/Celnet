/**
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-8-27
 * Description: 单条记录同步时调用Future方式进行
 */
public class Lepus_FutureCallout {
    @future (callout = true)
    public static void syncData(Id recordId, String action, String objType){
        syncBusinessDataOnly(recordId, action, objType);
    }
    
    // 插入操作时先同步业务数据，后同步成员
    @future (callout = true)
    public static void syncDataAndTeamMember(Id recordId, String objType, String action){
        
        system.debug('----------A1-----------');
        Schema.Sobjecttype sobjecttype = Lepus_SyncUtil.retrieveSobjectType(objType);
        boolean syncSuccess = false;
        
        // 查询出同步的记录
        Sobject sobj = Lepus_SyncUtil.querySobject(recordId, sobjecttype);
        
        Lepus_Log__c log = new Lepus_Log__c();
        log.UniqueId__c = ((String)sobj.get('Id') + String.valueOf(sobj.get('LastModifiedDate')));
        
        String xml;
        
        try{
            
            // 拼xml字符串
            xml = Lepus_EIPCalloutService.DataSyncXmlConcatenation(sobj, sobjectType.getDescribe().getName(), action);
            // 发送出去
            Lepus_WSDL_Info.tGetResponse response = Lepus_EIPCalloutService.sendToEIP(xml,'isaleslepus_lepusglobal_sync',(Id)sobj.get('Id'), 'CI0000194284');
            if(response != null && response.ResultStatus == 'true'){
                syncSuccess = true;
                 system.debug('----------B1-----------');
                syncTeamMemberOnly(new List<Id>{recordid}, objType, action);
                system.debug('----------B2-----------');
                log.Sent_Success__c = true;
                
            } else {
                Lepus_FailureHandler.handleBusinessDataFailure(recordId, response == null?'no response':response.Error, action, sobjecttype);
            	
            	log.Sent_Success__c = false;
            	log.Sent_Failure_Cause__c = (response == null?'no response':response.Error);
            	
            }
            
        } catch (Exception e){
            Lepus_FailureHandler.handleBusinessDataFailure(recordId, Lepus_FailureHandler.retrieveExceptionMessage(e), action, sobjecttype);
        	
        	log.Sent_Success__c = false;
        	log.Sent_Failure_Cause__c = (Lepus_FailureHandler.retrieveExceptionMessage(e));
        }
        
        log.BusinessData_XML__c = xml;
        upsert log UniqueId__c;
    }
    
    @future (callout = true)
    public static void syncFieldUpdate(Id recordId){
        Sobject sobj = Lepus_SyncUtil.querySobject(recordId, Opportunity.sobjecttype);
        List<OpportunityFieldHistory> list_oppFieldHis = Lepus_SyncUtil.queryOppFieldHistories(recordId, (Datetime)sobj.get('LastModifiedDate'));
        if(list_oppFieldHis.size() == 0)
        return;
        syncFieldUpdateCallout(recordId, sobj, list_oppFieldHis);
    }
    
    public static void syncFieldUpdateCallout(Id recordId, Sobject sobj, List<OpportunityFieldHistory> list_oppFieldHis){
        
        String sXML = Lepus_EIPCalloutService.FieldUpdateXmlConcatenation(list_oppFieldHis);
        
        Lepus_Log__c log = new Lepus_Log__c();
        log.UniqueId__c = ((String)sobj.get('Id') + String.valueOf(sobj.get('LastModifiedDate')));
        log.FieldUpdate_XML__c = sXML;
        
        try{
            Lepus_WSDL_Info.tGetResponse response = Lepus_EIPCalloutService.sendToEIP(sXML,'lepusmanage_field_update',(Id)sobj.get('Id'), 'CI0000194284');
            // 返回不为true则加入错误队列
            if(response != null && response.ResultStatus == 'true'){
            	log.Sent_Success__c = true;
            	
            } else {
                Lepus_FailureHandler.handleFieldUpdateFailure(recordId, (Datetime)sobj.get('LastModifiedDate'), response == null?'no response':response.Error);
            	log.Sent_Success__c = false;
            	log.Sent_Failure_Cause__c = (response == null?'no response':response.Error);
            }
        } catch (Exception e){
            Lepus_FailureHandler.handleFieldUpdateFailure(recordId, (Datetime)sobj.get('LastModifiedDate'), Lepus_FailureHandler.retrieveExceptionMessage(e));
        
        	log.Sent_Success__c = false;
        	log.Sent_Failure_Cause__c = (Lepus_FailureHandler.retrieveExceptionMessage(e));
        }
        
        upsert log UniqueId__c;
    }
    
    @future (callout = true)
    public static void syncLeadHistory(List<Id> recordIds){
        list<LeadHistory> leadHistories = Lepus_SyncUtil.queryLeadHistories(recordIds);
        if(leadHistories.size() == 0)
        
        return;
        
        syncLeadHistoryCallout(recordIds, leadHistories);
        
    }
    
    public static void syncLeadHistoryCallout(List<Id> recordIds, List<LeadHistory> leadHistories){
        String xml = Lepus_EIPCalloutService.LeadHistoryXmlConcatenation(leadHistories);
        
        Sobject sobj = Lepus_SyncUtil.querySobject(recordIds[0], Lead.Sobjecttype);
        
        Lepus_Log__c log = new Lepus_Log__c();
        log.FieldUpdate_XML__c = xml;
        log.UniqueId__c = ((String)sobj.get('Id') + String.valueOf(sobj.get('LastModifiedDate')));
        
        try{
            Lepus_WSDL_Info.tGetResponse response = Lepus_EIPCalloutService.sendToEIP(xml, 'salesforcelepus_userrole_sync', '', 'CI0000194284');
            if(response != null && response.ResultStatus == 'true'){
            	log.Sent_Success__c = true;
            	
            } else {
                Lepus_FailureHandler.handleLeadHistoryFailure(recordIds, xml, response == null?'no response':response.Error);
            	log.Sent_Success__c = false;
            	log.Sent_Failure_Cause__c = (response == null?'no response':response.Error);
            }
        } catch (Exception e){
            Lepus_FailureHandler.handleLeadHistoryFailure(recordIds, xml, Lepus_FailureHandler.retrieveExceptionMessage(e));
        
        	log.Sent_Success__c = false;
        	log.Sent_Failure_Cause__c = (Lepus_FailureHandler.retrieveExceptionMessage(e));
        }
        
        upsert log UniqueId__c;
    }
    
    @future (callout = true)
    public static void syncTeamMember(List<Id> recordIds, String objType, String action){
        syncTeamMemberOnly(recordIds, objType, action);
    }
    
    @future (callout = true)
    public static void syncUser(List<Id> recordIds, String action){
        try{
            List<User> userList = Lepus_SyncUtil.querySobjects(new Set<Id>(recordIds), User.sobjecttype);
            String xml = Lepus_EIPCalloutService.UserSyncXMLConcatenation(userList, action);
            
            Lepus_WSDL_Info.tGetResponse response = Lepus_EIPCalloutService.sendToEIP(xml, 'salesforcelepus_userrole_sync', '', 'CI0000194284');
            if(!(response != null && response.ResultStatus == 'true')){
                Lepus_FailureHandler.handleUserFailure(recordIds, response == null?'no response':response.Error);
            }
        } catch (Exception e){
            Lepus_FailureHandler.handleUserFailure(recordIds, Lepus_FailureHandler.retrieveExceptionMessage(e));
        }
    }
    
    public static boolean syncBusinessDataOnly(Id recordId, String action, String objType){
        Schema.Sobjecttype sobjecttype = Lepus_SyncUtil.retrieveSobjectType(objType);
        boolean syncSuccess = false;
        
        String xml = '';
        system.debug('----------D1-----------');
        // 查询出同步的记录
        Sobject sobj = Lepus_SyncUtil.querySobject(recordId, sobjecttype);
        
        Lepus_Log__c log = new Lepus_Log__c();
        log.UniqueId__c = ((String)sobj.get('Id') + String.valueOf(sobj.get('LastModifiedDate')));
        
        try{
            // 拼xml字符串
            xml = Lepus_EIPCalloutService.DataSyncXmlConcatenation(sobj, sobjectType.getDescribe().getName(), action);
            
            system.debug('----------D2-----------');
            
            // 发送出去
            Lepus_WSDL_Info.tGetResponse response = Lepus_EIPCalloutService.sendToEIP(xml,'isaleslepus_lepusglobal_sync',(Id)sobj.get('Id'), 'CI0000194284');
            
            log.BusinessData_XML__c = xml;
            
            if(response != null && response.ResultStatus == 'true'){
            	
                syncSuccess = true;
                
                log.Sent_Success__c = true;
            } else {
            	
                Lepus_FailureHandler.handleBusinessDataFailure(recordId, response == null?'no response':response.Error, action, sobjecttype);
            	
                log.Sent_Success__c = false;
                log.Sent_Failure_Cause__c = (response == null?'no response':response.Error);
            }
        } catch (Exception e){
        	system.debug('----------D3-----------');
        	
            Lepus_FailureHandler.handleBusinessDataFailure(recordId, Lepus_FailureHandler.retrieveExceptionMessage(e), action, sobjecttype);
        	
            log.Sent_Success__c = false;
            log.Sent_Failure_Cause__c = (Lepus_FailureHandler.retrieveExceptionMessage(e));
        }
        
        log.BusinessData_XML__c = xml;
        upsert log UniqueId__c;
        
        return syncSuccess;
    }
    
    public static void syncTeamMemberOnly(List<Id> recordIds, String objType, String action){
        
        Schema.Sobjecttype sobjecttype = Lepus_SyncUtil.retrieveSobjectType(objType);
        List<Sobject> sobjList = Lepus_SyncUtil.querySobjects(new Set<Id>(recordIds), sobjecttype);
        
        Lepus_Log__c log = new Lepus_Log__c();
        log.UniqueId__c = ((String)sobjList[0].get('Id') + String.valueOf(sobjList[0].get('LastModifiedDate')));
        
        String xml;
        
        try{
            system.debug('----------C1-----------');
            
            Map<Id, List<Lepus_EIPMember>> memberMap = Lepus_SyncUtil.retrieveTeamMembers(sobjList, sobjecttype);
            xml = Lepus_EIPCalloutService.memberXmlConcatenation(memberMap, sobjecttype, action);
        	
        	system.debug('----------C2-----------');
        	
            Lepus_WSDL_Info.tGetResponse response = Lepus_EIPCalloutService.sendToEIP(xml, 'mchat_bizdatasync', '', 'CI0000194284');
            if(response != null && response.ResultStatus == 'true'){
            	log.Sent_Success__c = true;
            } else {
                Lepus_FailureHandler.handleTeamMemberFailure(recordIds, response == null?'no response':response.Error, action);
            	
            	log.Sent_Success__c = false;
            	log.Sent_Failure_Cause__c = (response == null?'no response':response.Error);
            }
        } catch(Exception e){
        	system.debug('----------C3-----------');
        	
            Lepus_FailureHandler.handleTeamMemberFailure(recordIds, Lepus_FailureHandler.retrieveExceptionMessage(e), action);
            
            log.Sent_Success__c = false;
            log.Sent_Failure_Cause__c = (Lepus_FailureHandler.retrieveExceptionMessage(e));
        }
        
        log.TeamMember_XML__c = xml;
        upsert log UniqueId__c;
    }
}