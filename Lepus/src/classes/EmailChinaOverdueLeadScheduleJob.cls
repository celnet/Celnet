/*
* @Purpose : 给中国区超期未处理的线索Owner发送邮件提醒；
*            1. 销管在1个工作日内进行分发处理。2.  销售人员在3个工作日内进行确认处理。
* @Author : steven
* @Date :   2014-1-21
*/
global class EmailChinaOverdueLeadScheduleJob implements Schedulable,Database.Batchable<sObject>{    
	public Map<Id,List<Lead>> remindLeadMap {get;set;} //用于分组保存每个用户的待提醒线索信息
	public Set<Id> ownerIdSet {get;set;} //保存所有待提醒用户的ID
	public Map<Id,String> ownerEmailMap {get;set;}
	public Map<Id,String> ownerNameMap {get;set;} 
	public String query;
     
	//Constructor
	public EmailChinaOverdueLeadScheduleJob() {
		remindLeadMap = new Map<Id,List<Lead>>();
		ownerIdSet = new Set<Id>();
		ownerEmailMap = new Map<Id,String>();
		ownerNameMap = new Map<Id,String>();
	}   

	//ScheduleJob Execute
	global void execute(SchedulableContext SC){
		EmailChinaOverdueLeadScheduleJob e = new EmailChinaOverdueLeadScheduleJob();
		e.query='SELECT id From User WHERE Isactive = true';
		Id batchId = Database.executeBatch(e,10);
	}

	//Batch Start
	global Database.QueryLocator start(Database.BatchableContext BC){
		return Database.getQueryLocator(query);   
	}

	//Batch Execute
	global void execute(Database.BatchableContext BC, list<sObject> scope){
		Set<Id> batchOwnerId = new Set<Id>();
		try {
			for(sObject s : scope){ 
				batchOwnerId.add(((User)s).Id); 
			}
			List<Lead> leadList = [Select id,Name,Company,China_Representative_Office__c,LeadSource,Status,CreatedDate,OwnerId,Owner.Email,Owner.Name,owner.IsActive
															 From Lead Where (Status = '待分发' or Status = '待确认') AND Ownerid in:batchOwnerId];
			//遍历线索列表，并按照用户ID进行分组
			for(Lead lead : leadList) {
				ownerIdSet.add(lead.OwnerId);
				if(remindLeadMap.get(lead.OwnerId) == null) {
					//Logic1-1:如果MAP表中不存在该用户，插入该用户，并添加线索
					List<Lead> oList = new List<Lead>();
					oList.add(lead);
					remindLeadMap.put(lead.OwnerId,oList);
					ownerEmailMap.put(lead.OwnerId,lead.owner.Email);
					ownerNameMap.put(lead.OwnerId,lead.owner.Name);
				} else {
					//Logic1-2:如果MAP表中已存在该用户，直接添加线索
					List<Lead> oList = remindLeadMap.get(lead.OwnerId);
					oList.add(lead);
				}
			}
			sendRemindMail(); //发送提醒邮件
		} catch(Exception e) {
			System.debug(e);
		}
	}
    
	//Batch Finished
	global void finish(Database.BatchableContext BC){
	}
    
	/**
	 * 发送提醒邮件
	 */ 
	private void sendRemindMail() {
		try { 
			for(Id ownerId : ownerIdSet) { 
				//Messaging.reserveSingleEmailCapacity(1);
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
				List<string> toAddress = new List<string>();
				toAddress.add(ownerEmailMap.get(ownerId));
				mail.setToAddresses(toAddress);
				mail.setSenderDisplayName('Salesforce Supporter');
				mail.setSubject('您有未处理的线索，请及时登陆Salesforce处理');
				String strbody = htmlBodyConstructor(ownerId);
				mail.setHtmlBody(strbody);
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
			}
		} catch(Exception e) {
			System.debug(e);
		}
	}

	/**
	 * 构造邮件主体 (注意String长度有限制，一般情况下不会超出)
	 */ 
	private String htmlBodyConstructor(Id ownerId) {
		String result = '';
		String distributeList = '';
		String confirmList = '';
		String baseUrl = system.Url.getSalesforceBaseUrl().toExternalForm()+'/';
		String salesContactName = System.Label.China_Sales_Dept_Contact;
		//Step1：获取Owner相关的线索全集
		String ownerName = ownerNameMap.get(ownerId);
		List<Lead> oList = remindLeadMap.get(ownerId);
		//Step2：线索遍历和分组
		for(Lead lead : oList) {
			if(lead.Status == '待分发') distributeList += getHtmlLead(lead,ownerName,baseUrl);
			if(lead.Status == '待确认') confirmList += getHtmlLead(lead,ownerName,baseUrl);
		}
		result = '<p>'+ ownerName +'，您好！</p>';
		result += '<p>&nbsp;&nbsp;您在salesforce系统中，有'+ oList.size() +'条未处理的线索，可能涉及多个项目信息，请尽快登陆系统的“线索模块”处理。</p>';
		//Step3：构造待分发线索列表
		if(distributeList != '') {
			result += '<p>待您分发的线索列表：</p>';
			result += getHtmlTable(distributeList);
		}
		//Step4：构造待确认线索列表
		if(confirmList != '') {
			result += '<p>待您确认的线索列表：</p>';
			result += getHtmlTable(confirmList);
		}
		result += '<p>&nbsp;&nbsp;如有操作疑问，请咨询当地销管或销售业务部'+salesContactName+'。</p>';
		result += '<p>声明：按公司线索管理要求</p>'; 
		result += '<p>&nbsp;&nbsp;1.销管在1个工作日内进行分发处理。</p>';
		result += '<p>&nbsp;&nbsp;2.销售人员在3个工作日内进行确认处理。</p>';
		
		return result;
	}
    
	/**
	 * 构造Html形式的机会点列表（带标题的） 
	 */ 
	private String getHtmlTable(String htmlLeadList) {
		String result = '';
		if(htmlLeadList != null && htmlLeadList != '') {
			result += '<table style="font-family: Arial,微软雅黑;font-size:12px; border:1px solid #AAA;width:1100px;background: #EEE;border-spacing: 1px;text-align:center">';
			result +=   '<tbody>';
			result +=       '<tr style="background: #CCC;font-weight: bold;">';
			result +=           '<th style="width:10%">姓名</th>';
			result +=           '<th style="width:20%">客户名称</th>';
			result +=           '<th style="width:15%">区域/系统部</th>';
			result +=           '<th style="width:10%">线索来源</th>';
			result +=           '<th style="width:8%">线索阶段</th>';
			result +=           '<th style="width:17%">线索所有人</th>';
			result +=           '<th style="width:20%">创建日期</th>';
			result +=       '</tr>';
			result +=       htmlLeadList;
			result +=   '</tbody>';
			result += '</table>';
		}
		return result;
	}
    
	/**
	 * 构造Html表格格式的线索String
	 */ 
	private String getHtmlLead(Lead lead,String ownerName,String baseUrl) {
		String result = '';
		if(lead != null) {
			result += '<tr style="background: #FFF;">';
			result +=   '<td><a href='+ baseUrl + lead.Id +'>' + lead.Name + '</a></td>';
			result +=   '<td>'+ lead.Company +'</td>';
			result +=   '<td>'+ lead.China_Representative_Office__c +'</td>';
			result +=   '<td>'+ lead.LeadSource +'</td>';
			result +=   '<td>'+ lead.Status +'</td>';
			result +=   '<td>'+ ownerName +'</td>';
			result +=   '<td>'+ lead.CreatedDate +'</td>';
			result += '</tr>';
		}
		return result;
	}
	
}