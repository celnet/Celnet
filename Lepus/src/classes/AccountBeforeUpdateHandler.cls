/**
 * @Purpose : 1、中国区客户关键字段更新时会记录更新时间，用于判断是否需要同步到CIS
 *            2、中国区客户失效时会校验是否有关联记录，有关联记录不允许删除
 * @Author : Steven
 * @Date :   2014-07-01 Update
 */
public with sharing class AccountBeforeUpdateHandler implements Triggers.Handler{ //before update
	public void handle() {
		Set<id> inactiveAccIds = new Set<id>();
		Set<id> keyfieldUpdateAccIds = new Set<id>();
		//记录下待失效客户的ID,刷新关键信息修改了的客户的关键信息更新时间
		if(Trigger.isUpdate){
			for(Account acc : (List<Account>)trigger.new){
				if(acc.RecordTypeId == CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE){
					Account oldAcc = (Account)trigger.oldMap.get(acc.id);
					//记录待失效的客户ID
					if(acc.Inactive_NA__c && !oldAcc.Inactive_NA__c){
						inactiveAccIds.add(acc.id);
					}
					//关键字段更新了刷新时间
					if((acc.CIS__c != oldAcc.CIS__c)||
						 (acc.Customer_Group__c != oldAcc.Customer_Group__c)||
						 (acc.Named_Account_Level__c != oldAcc.Named_Account_Level__c)||
						 (acc.Is_Named_Account__c != oldAcc.Is_Named_Account__c)||
						 (acc.parentid != oldAcc.parentid)){
						acc.CIS_Key_Field_Update_Time__c = system.now();
					}
				}
			}
		}
		//校验失效的客户下是否存未关闭的机会点
		if(!inactiveAccIds.isEmpty()) {
			AggregateResult[] groupedResults = [select accountId,count(id) from opportunity where Is_Closed__c = false and accountId in:inactiveAccIds group by accountId];
	 		Map<id,Integer> numMap = new Map<id,Integer> ();
			for(AggregateResult gr : groupedResults){
				numMap.put((String)gr.get('accountId'),(Integer)gr.get('expr0'));
			}
	 		for(Account acc : (List<Account>)trigger.new){
				if(numMap.get(acc.id) > 0){
					acc.addError('此客户有未关闭的机会点，不允许失效!');
				}
			}
		}
	}
}