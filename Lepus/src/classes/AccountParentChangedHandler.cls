/**
 * @Purpose : 1、父子挂靠时，根据父客户的属性去刷新子客户的归属、行业、根节点、NA属性等信息
 *            2、解除挂靠时，清空子客户的NA属性，更新子客户的根节点信息；
 *            3、由于校验规则是在before trigger后执行,因此子客户的na记录变动在after update的逻辑中实现,以避免先将子客户更新为NA时报错
 * @Author : Steven
 * @Date :   2014-07-01 Update
 */
public without sharing class AccountParentChangedHandler implements Triggers.Handler{//before update
	public void handle() {
		if(Trigger.isBefore){
			//Step1: 记录所有父客户发生了变化的客户ID，以及所有新的父客户ID
			Set<Id> parentids = new Set<Id>();
			Set<Id> accountIds = new Set<Id>();
			for(Account acc : (List<Account>)trigger.new){
				if(acc.RecordTypeId == CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE){
					if(acc.ParentId != ((Account)trigger.oldMap.get(acc.id)).ParentId){
						accountIds.add(acc.id);
						if(acc.ParentId != null) parentids.add(acc.ParentId);
					}
				}
			}
			if(accountIds.isEmpty()) return;
			
			//Step2： 一次查询获取父客户的NA属性和关键属性
			Map<id,Account> parentMap = new Map<id,Account>(
				[select id,Named_Account__c,Named_Account_Character__c,NA_Approved_Date__c,Is_403_Customer__c, 
								Named_Account_Level__c,Named_Account_Property__c,Is_Named_Account__c,
								industry,Sub_industry_HW__c,Customer_Group__c,Customer_Group_Code__c,
		    				Region_HW__c,Representative_Office__c,Province__c,City_Huawei_China__c
					 from Account where id in:parentids]);
			
			//Step3: 根据父客户的NA属性以及行业和归属属性去刷新子客户
			for(Account acc : (List<Account>)Trigger.new){
				//Step3-1: 处理父子挂靠场景（判断条件:Parentid发生变化,且父客户为NA的记录）
				if((acc.ParentId != null) && (acc.ParentId != ((Account)trigger.oldMap.get(acc.id)).ParentId ) &&
					  parentMap.get(acc.ParentId).Is_Named_Account__c){ 
					Account parentAcc = parentMap.get(acc.ParentId);
					//Step3-1-1: 为子客户创建一个NA属性标签
					Named_Account__c newNA =  new Named_Account__c(
						Is_Named_Account__c = parentAcc.Is_Named_Account__c,
				 		Is_403_Customer__c = parentAcc.Is_403_Customer__c,
				 		Named_Account_Character__c = parentAcc.Named_Account_Character__c,
				 		Named_Account_Level__c = parentAcc.Named_Account_Level__c,
				 		Approved_Date__c = parentAcc.NA_Approved_Date__c,
				 		Named_Account_Property__c = parentAcc.Named_Account_Property__c,
				 		Record_Description__c = 'Parent changed and system created', Account_Name__c =acc.id);
			 		NamedAccountAfterInsertHandler.isFirstRun = false;
			 		insert newNA;
			 		//Step3-1-2: 更新当前客户的NA属性
					acc.Is_Named_Account__c = parentAcc.Is_Named_Account__c;
					acc.NA_Approved_Date__c = parentAcc.NA_Approved_Date__c;
					acc.Is_403_Customer__c = parentAcc.Is_403_Customer__c;
					acc.Named_Account_Character__c = parentAcc.Named_Account_Character__c;
					acc.Named_Account_Property__c = parentAcc.Named_Account_Property__c;
					acc.Named_Account_Level__c = parentAcc.Named_Account_Level__c;
					acc.Named_Account__c = newNA.id;
					//Step3-1-3: 刷新当前客户的归属、行业、根节点等关键属性
					acc.HQ_Parent_Account__c = acc.ParentId;//不存在多级关系，根节点父客户就是父客户
					acc.industry = parentAcc.industry;
					acc.Sub_industry_HW__c = parentAcc.Sub_industry_HW__c;
					acc.Customer_Group__c = parentAcc.Customer_Group__c;
					acc.Customer_Group_Code__c = parentAcc.Customer_Group_Code__c;
					acc.Region_HW__c = parentAcc.Region_HW__c;
					acc.Representative_Office__c = parentAcc.Representative_Office__c;
					if(acc.Representative_Office__c != '系统部总部') {
						acc.Province__c = parentAcc.Province__c;
					}
					//acc.City_Huawei_China__c = parentAcc.City_Huawei_China__c;
				//Step3-2: 处理解除挂靠场景（判断条件:Parentid变为空,且之前的NA属性为True）
				} else if (acc.ParentId == null && ((Account)trigger.oldMap.get(acc.id)).ParentId != null && acc.Is_Named_Account__c){
					//Step3-2-1: 为子客户创建一个NA属性标签
					Named_Account__c newNA =  UtilNamedAccount.newNotNA(acc.id);														
					NamedAccountAfterInsertHandler.isFirstRun = false;
			 		insert newNA;
			 		acc.Is_Named_Account__c =  false;
			 		acc.NA_Approved_Date__c = null;
			 		acc.Is_403_Customer__c = false;
			 		acc.Named_Account_Character__c = null;
			 		acc.Named_Account_Property__c =  null;
					acc.Named_Account_Level__c =  null;
					acc.Named_Account__c = newNA.id;
					//Step3-2-2: 刷新当前客户根节点属性
					acc.HQ_Parent_Account__c = null;
				}
			} // end of trigger new
		} // end of isBefore  
	} // end of handle 
}