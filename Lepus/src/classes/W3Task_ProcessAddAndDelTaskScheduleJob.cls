global class W3Task_ProcessAddAndDelTaskScheduleJob implements Schedulable,Database.Batchable<Sobject>,Database.AllowsCallouts{
	Set<Id> PIIdSet;//在某段时间内更新的processInstanceId;
	  List<ProcessInstanceWorkItem> wiList=new List<ProcessInstanceWorkItem>();//要增加进入w3系统的workItem
	  List<ProcessInstanceStep> stepList=new List<ProcessInstanceStep>();//要从w3系统中删除的step
	  Integer minute=5;//时间间隔
	   DateTime endTime;
	   DateTime startTime;  
	  	
	  global void execute(SchedulableContext sc){
	  //	endTime=W3TaskSchedualTime__c.getAll().values()[0].StartTime__c;
	  	startTime=W3TaskSchedualTime__c.getAll().values()[0].StartTime__c;
	  	endTime=DateTime.newInstance(DateTime.now().year(),DateTime.now().month(),DateTime.now().day(),DateTime.now().hour(),DateTime.now().minute(),0);
	   	PIIdSet=new Set<Id>(); 
	  	String idPrefix1=Account_Link_Request__c.sObjectType.getDescribe().getKeyPrefix();//获取Id前三位
		  String idPrefix2=Deal_Registration__c.sObjectType.getDescribe().getKeyPrefix();
		   String idPrefix3=Campaign.sObjectType.getDescribe().getKeyPrefix();
		List<ProcessInstance> PIList=[Select p.TargetObjectId, p.Status, p.LastModifiedDate, p.Id, p.CreatedDate From ProcessInstance p where LastModifiedDate<=:endTime
	  	and LastModifiedDate>:startTime];//找到修改时间在这个时间间隔内的processInstance;
	  	 for(ProcessInstance pi:PIList) 
	  	 {   
	  	 	//父对象必须是父子挂靠关系   //===其他对象怎么判断 ？
	  	 	if((((String)(pi.TargetObjectId)).contains(idPrefix1))||(((String)(pi.TargetObjectId)).contains(idPrefix2))||(((String)(pi.TargetObjectId)).contains(idPrefix3)))
	  	 	{
	  	 		PIIdSet.add(pi.Id);
	  	 	} 
	  	 }
	  	List<ProcessInstanceWorkitem> tempwiList=[Select p.SystemModstamp, p.ProcessInstanceId, p.ProcessInstance.TargetObjectId, p.OriginalActorId, p.IsDeleted, p.Id, p.CreatedDate, p.CreatedById, p.ActorId From ProcessInstanceWorkitem p 
	  	 where SystemModstamp<=:endTime
	  	and SystemModstamp>:startTime];//找到在时间间隔内变化的workItem; ===没有过滤对象吗？
	  	for(ProcessInstanceWorkitem piw:tempwiList)
	  	{
	  		if((((String)(piw.ProcessInstance.TargetObjectId)).contains(idPrefix1))||(((String)(piw.ProcessInstance.TargetObjectId)).contains(idPrefix2))||(((String)(piw.ProcessInstance.TargetObjectId)).contains(idPrefix3)))
	  		{
	  			PIIdSet.add(piw.ProcessInstanceId);//将有变化的父PI ID,加入到系统中
	  		}
	  	}
	  	System.debug(startTime+'startTime*****endTime'+endTime);
	  	if(PIIdSet.size()<=5)
	  	{
	  		W3Task_Util_CallOut(PIIdSet,startTime,endTime); 
	  	}else{ 
	  	 W3Task_ProcessAddAndDelTaskScheduleJob job = new W3Task_ProcessAddAndDelTaskScheduleJob(PIIdSet,startTime,endTime);
         Database.executeBatch(job, 1);
	  	} 
	  	W3TaskSchedualTime__c.getAll().values()[0].StartTime__c=endTime;
		update W3TaskSchedualTime__c.getAll().values();
		
		//给海外Deal审批人授权
		if(tempwiList.size()>0)
		W3Task_ProcessApproverPermissionHandler.AddPermissionForApprover(tempwiList);
     } 
      
     public W3Task_ProcessAddAndDelTaskScheduleJob()
     {
     } 
     
      public W3Task_ProcessAddAndDelTaskScheduleJob(Set<Id> PIIdSet,DateTime startTime_1,DateTime endTime_1)
     {
     	this.PIIdSet=PIIdSet;
     	this.startTime=startTime_1;
     	this.endTime=endTime_1;
     }
		     
       global Database.Querylocator start(Database.BatchableContext bc){
       	return  Database.getQueryLocator([Select p.TargetObjectId, p.Status, p.LastModifiedDate, p.Id, p.CreatedDate, 
       	(Select Id, ProcessInstanceId, OriginalActorId, ActorId, IsDeleted, CreatedDate, CreatedById, SystemModstamp From Workitems),
       	 (Select Id, ProcessInstanceId, StepStatus, OriginalActorId, ActorId, Comments, StepNodeId, CreatedDate, CreatedById, SystemModstamp From Steps  Order by CreatedDate)
       	  From ProcessInstance p where Id in:PIIdSet]);//所有要处理的PI
    	}
    
        global void execute(Database.BatchableContext bc, List<Sobject> scope)
        {
    	    wiList=FindWorkItem(scope);
    	    stepList=FindStep(scope);
      	  	System.debug(stepList+'stepList******');
      		System.debug(wiList+'wiList***************');
      		W3Task_SyncUtil_Schedule.W3Task_Util(wiList,stepList,PIIdSet);
        }
        global void finish(Database.BatchableContext bc){
    }
    //根据ProcessInstance找到workItem
    public  List<ProcessInstanceWorkItem> FindWorkItem(List<ProcessInstance> piList)
    {
    	List<ProcessInstanceWorkItem> tempList=new List<ProcessInstanceWorkItem>();
    		for(ProcessInstance pi : (List<ProcessInstance>) piList)
        	{
        		if(pi.Workitems.size()>0)
        		{
        			tempList.addAll(pi.Workitems);
        		}
        	}
        return tempList;
    }
    //根据ProcessInstance找到step
    public  List<ProcessInstanceStep> FindStep(List<ProcessInstance> piList)
    {
    	List<ProcessInstanceStep> tempList=new List<ProcessInstanceStep>();
    		for(ProcessInstance pi : (List<ProcessInstance>) piList)
        	{
        		System.debug(pi.steps+'pi.steps**');
        		System.debug(startTime+'startTime*****'+pi.CreatedDate);
        		if(pi.CreatedDate<=startTime)
        		{
        			for(ProcessInstanceStep pis:pi.Steps)
        			{
						System.debug(pis.SystemModstamp+'*******************'); 
						System.debug(startTime+'startTime*****endTime'+endTime);       			
        				if(pis.CreatedDate>startTime && pis.CreatedDate<=endTime)
        				{
        					tempList.add(pis);
       						break;
        				}	
        			}
        		}
        	}
        return tempList;
    }
    	@future (Callout=true)
    	public static void W3Task_Util_CallOut(Set<Id> PIIdSet,DateTime startTime,DateTime endTime)
		{
			System.debug('CallOut******************');
			List<ProcessInstance> tempPiList=[Select p.TargetObjectId, p.Status, p.LastModifiedDate, p.Id, p.CreatedDate, 
     	  	(Select Id, ProcessInstanceId, OriginalActorId, ActorId, IsDeleted, CreatedDate, CreatedById, SystemModstamp From Workitems),
      	 	 (Select Id, ProcessInstanceId, StepStatus, OriginalActorId, ActorId, Comments, StepNodeId, CreatedDate, CreatedById, SystemModstamp From Steps  Order by CreatedDate)
     	  	  From ProcessInstance p where Id in:PIIdSet];
	  		 W3Task_ProcessAddAndDelTaskScheduleJob pj = new W3Task_ProcessAddAndDelTaskScheduleJob(PIIdSet,startTime,endTime);
	  		 List<ProcessInstanceWorkItem> wiList=pj.FindWorkItem(tempPiList);//要增加进入w3系统的workItem
			  List<ProcessInstanceStep> stepList=pj.FindStep(tempPiList);
			  System.debug(stepList+'stepList**************88 List size:' + stepList.size());
			  System.debug('WorkItemList**************88'+wiList);
	  		W3Task_SyncUtil_Schedule.W3Task_Util(wiList,stepList,PIIdSet);
		}
}