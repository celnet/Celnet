public without sharing class LeadConvertToOpportunity {
    public Opportunity newOpp {get;set;}
    public Lead currentNALead {get;set;}
    public Account acc {get;set;}
    public Boolean isDisabled {get;private set;}
    
    
    public LeadConvertToOpportunity(ApexPages.StandardController controller){
        currentNALead = [select id,Account_Name__c,name,NA_Lead_Name__c,Campaign__c,Status,NA_Lead_Confirmation__c ,Participate_Space__c
            from Lead where id = :controller.getId()];  
        isDisabled = false;
        if(currentNALead.NA_Lead_Confirmation__c != '确认有效'){//只有“确认有效”的线索才可以转换为机会点；
            isDisabled = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'只有“确认有效”的线索才可以转换为机会点'));
        }
        if(currentNALead.Status == '已转机会点'){//只能转换一次
            isDisabled = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'已转化机会点,无法重复转化'));
        }
        try{
        acc = [select id, name,Region_HW__c,Industry,City_Huawei_China__c, Representative_Office__c, Country_HW__c, Country_Code_HW__c,Industry__c,Customer_Group__c,
            Sub_industry_HW__c,System_Department__c,Province__c,city__c,Customer_Group_Code__c
         from Account where id = :currentNALead.Account_Name__c];
        
        }catch(QueryException e){
            acc = new Account();
        }
        newOpp = new Opportunity();
        /*
        newOpp = new Opportunity(AccountId = acc.id,name = currentNALead.NA_Lead_Name__c,Region__c = acc.Region_HW__c,
            Representative_Office__c = acc.Representative_Office__c,Country__c = acc.Country_HW__c ,
            Lead__c = currentNALead.id,Participate_Space__c = currentNALead.Participate_Space__c,
            Country_Code__c = acc.Country_Code_HW__c,Industry__c = acc.Industry__c,Customer_Group__c = acc.Customer_Group__c,
            Sub_Industry__c = acc.Sub_inLdustry_HW__c,System_Department__c = acc.System_Department__c);
            */
    }
    
    public PageReference initial(){
        newOpp.AccountId = acc.id;
        newOpp.name = currentNALead.NA_Lead_Name__c;
        newOpp.Region__c = acc.Region_HW__c;
        newOpp.Representative_Office__c = acc.Representative_Office__c;
        newOpp.Country__c = acc.Country_HW__c ;
        newOpp.Lead__c = currentNALead.id;
        newOpp.Participate_Space__c = currentNALead.Participate_Space__c;
        newOpp.Country_Code__c = acc.Country_Code_HW__c;
        newOpp.Industry__c = acc.Industry__c;
        newOpp.Customer_Group__c = acc.Customer_Group__c;
        newOpp.Sub_Industry__c = acc.Sub_industry_HW__c;
        newOpp.System_Department__c = acc.Industry; 
        newOpp.City__c = acc.City_Huawei_China__c;
        newOpp.RecordTypeId = Constants.CHINAOPPORTUNITYRECORDTYPE;
        newOpp.Customer_Group_Code__c = acc.Customer_Group_Code__c;
        newOpp.Province__c =  acc.Province__c;
        newOpp.CampaignId = currentNALead.Campaign__c;
        return null;
    }
    
    public PageReference convert(){
        currentNALead = [select id,Status from Lead where id =:currentNALead.id];
        if(currentNALead.Status == '已转机会点'){
            isDisabled = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'已转化机会点,无法重复转化'));
            return null;
        }
        try{
            insert newOpp;
        }catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }
        if(currentNALead.status != '已转机会点'){
            currentNALead.status = '已转机会点';
            currentNALead.Convert_Date__c = DateTime.now();//第一次转换机会点时设置转换日期
            update currentNALead;
        }
        return new ApexPages.StandardController(newOpp).view();
    }
    
}