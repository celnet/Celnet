public class LeadArchiveHandler implements Triggers.Handler{
	public static Boolean isFirstRun = true;
    public void handle(){
        if(LeadArchiveHandler.isFirstRun){
            List<Data_Maintain_History__c> archiveList = new List<Data_Maintain_History__c>();
            for(Lead t : (List<Lead>)trigger.old){
                if(t.RecordTypeId == CONSTANTS.CHINALEADRECORDTYPE || t.RecordTypeId == CONSTANTS.OVERSEALEADRECORDTYPE){
                    Data_Maintain_History__c dmh = new Data_Maintain_History__c();
                    dmh.Record_ID__c = t.Id;
                    dmh.Operation_Type__c = 'Delete';
                    dmh.Data_Type__c = (t.RecordTypeId == CONSTANTS.CHINALEADRECORDTYPE?'China Lead':'Oversea Lead');
                    archiveList.add(dmh);
                }
            }
            if(archiveList.size() > 0){
                insert archiveList;
            }
            LeadArchiveHandler.isFirstRun = false;
        }
    }
}