@isTest
private class ComponentRequestRollUpHandlerTest {
	static testMethod void positiveTest(){
			Account acc = UtilUnitTest.createAccount();
    	Case sr = UtilUnitTest.createChinaServiceRequest(); 
    	sr = [select id,Spare_Amount_Remote__c from Case where id =:sr.id];
    	System.assertEquals(null,sr.Spare_Amount_Remote__c);
    	Component_Request__c cr = new Component_Request__c(SR_No__c = sr.id);
    	insert cr;
    	sr = [select id,Spare_Amount_Remote__c from Case where id =:sr.id];
    	System.assertEquals(0,sr.Spare_Amount_Remote__c);
    	cr.Quantity__c = 1;
    	update cr;
    	sr = [select id,Spare_Amount_Remote__c from Case where id =:sr.id];
    	System.assertEquals(1,sr.Spare_Amount_Remote__c);
    	
    	cr.SR_No__c = null;//update
    	update cr;
    	sr = [select id,Spare_Amount_Remote__c from Case where id =:sr.id];
    	System.assertEquals(0,sr.Spare_Amount_Remote__c);
    	
    	cr.SR_No__c = sr.id;//update
    	update cr;
    	sr = [select id,Spare_Amount_Remote__c from Case where id =:sr.id];
    	System.assertEquals(1,sr.Spare_Amount_Remote__c);
    	
    	delete cr;
    	sr = [select id,Spare_Amount_Remote__c from Case where id =:sr.id];
    	System.assertEquals(0,sr.Spare_Amount_Remote__c);
    	
    	undelete cr;
    	sr = [select id,Spare_Amount_Remote__c from Case where id =:sr.id];
    	System.assertEquals(1,sr.Spare_Amount_Remote__c);
	}
}