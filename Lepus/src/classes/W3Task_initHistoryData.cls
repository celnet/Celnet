global class W3Task_initHistoryData implements Database.Batchable<Sobject>, Database.Stateful,Database.AllowsCallouts{
	private Datetime startTime ;
	private Datetime endTime;
	global W3Task_initHistoryData(Datetime sTime , Datetime eTime){
		this.startTime = sTime;
		this.endTime = eTime;
	}
	global W3Task_initHistoryData(){
		this.startTime = datetime.now().addMonths(-1);
		this.endTime = datetime.now();
	}
	global List<Sobject> start(Database.BatchableContext bc){
		return [Select p.SystemModstamp, p.ProcessInstance.CreatedById, p.ProcessInstance.CreatedDate, 
				p.ProcessInstanceId, p.OriginalActorId, p.Id, p.ActorId 
				From ProcessInstanceWorkitem p
				Where p.ProcessInstance.CreatedDate>=:startTime And p.ProcessInstance.CreatedDate<:endTime];
	}
	global void execute(Database.BatchableContext BC, list<Sobject> scope){
		Set<ID> set_piid = new Set<ID>();
		for(Sobject sobj:scope){
			ProcessInstanceWorkitem wi = (ProcessInstanceWorkitem)sobj;
			set_piid.add(wi.ProcessInstanceId);
		}
		W3Task_SyncUtil_Schedule.W3Task_Util((List<ProcessInstanceWorkitem>)scope, new List<ProcessInstanceStep>(), set_piid);
	}
	global void finish(Database.BatchableContext BC){
		
	}
}