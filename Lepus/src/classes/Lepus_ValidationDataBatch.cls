/**
 * @Purpose : 对账数据，每天推送当天数据一次。
 * @Author : 孙达
 * @Date : 2014-08-19
 **/
global class Lepus_ValidationDataBatch implements Schedulable, Database.Batchable<sObject>, Database.Stateful,Database.AllowsCallouts{
	//Scheduler
	global void execute(SchedulableContext sc){
		Lepus_ValidationDataBatch ValidationDataSync = new Lepus_ValidationDataBatch(Account.Sobjecttype);
		Database.executeBatch(ValidationDataSync, 500);
	}
	//Batch
	//开始日期
	private Datetime StartDate = Datetime.now().addDays(-1);
	//同步成功标记
	private boolean SyncSuccess =true;
	//对象类型
	private Schema.Sobjecttype sObjType ;
	public Lepus_ValidationDataBatch(){
		
	}
	public Lepus_ValidationDataBatch(Schema.Sobjecttype sType){
		this.sObjType = sType;
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC){
		//查询对账数据错误的日期，重新定义开始日期
		for(SyncFailRecord__c sfr : [Select Id,SyncTime__c,ObjectType__c From SyncFailRecord__c 
				Where SyncType__c = '对账数据同步' And ObjectType__c =: String.valueOf(sObjType) And SyncTime__c != null] ){
			//Date sDate = Date.valueOf(sfr.SyncTime__c.year()+'-'+sfr.SyncTime__c.month()+'-'+sfr.SyncTime__c.day());
			StartDate = StartDate>sfr.SyncTime__c?sfr.SyncTime__c:StartDate;
		}
		Datetime endDate = datetime.now();
		String sQuery = 'Select Id,LastModifiedDate'
			+ (String.valueOf(sObjType).toLowerCase().equals('contact')?'':',Global_ID__c') 
			+' From '
			+String.valueOf(sObjType)
			+' Where LastModifiedDate <=: endDate and LastModifiedDate >=: StartDate all rows';
		return Database.getQueryLocator(sQuery);
	}
	global void execute(Database.BatchableContext BC, List<sObject> scope){
		String sJson = Lepus_UtilCallOut.ValidationDataToJson(scope,String.valueOf(sObjType));
		String sXml = Lepus_UtilCallOut.concatenateXml('Salesforce', 'validation', String.valueOf(sObjType)+'_Profile', '', '', sJson , '');
		Lepus_WSDL_Info.tGetResponse Res;
		try{
			//发送请求到EIP
			Res = Lepus_EIPCalloutService.sendToEIP(sXml, 'isaleslepus_lepusglobal_accounting', '', 'CI0000194284');
			if(Res == null || Res.ResultStatus == 'false'){
				//response错误，记录同步失败
				Lepus_FailureHandler.handleValidationFailure(Res == null?'no response':Res.Error, StartDate, sObjType);
				SyncSuccess = false;
			}
		}catch(exception e){
			//查到exception，记录同步失败
			Lepus_FailureHandler.handleValidationFailure(e.getMessage(), 
					StartDate, sObjType);
			SyncSuccess = false;
		}
	}
	global void finish(Database.BatchableContext BC){
		//若同步成功，删除失败记录
		if(!this.SyncSuccess){
			delSyncFailRecord(String.valueOf(sObjType));
		}
		
		//同步其他对象
    	Lepus_ValidationDataBatch vdb ;
    	if(sObjType == Account.sobjecttype){
    		vdb = new Lepus_ValidationDataBatch(Opportunity.sobjecttype);
    	}else if(sObjType == Opportunity.sobjecttype){
    		vdb = new Lepus_ValidationDataBatch(Contact.sobjecttype);
    	}else if(sObjType == Contact.sobjecttype){
    		vdb = new Lepus_ValidationDataBatch(Lead.sobjecttype);
    	}
    	if(vdb != null && !Test.isRunningTest()){
    		Database.executeBatch(vdb, 1000);
    	}
   	}
	private void delSyncFailRecord(String sObjType){
    	//删除同步失败记录
    	delete [Select Id From SyncFailRecord__c Where ObjectType__c =: sObjType And SyncType__c = '对账数据同步'];
    }
}