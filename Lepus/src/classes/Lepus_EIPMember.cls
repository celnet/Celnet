/*
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-8-6
 * Description: Member类，用于传递团队成员xml
 */
public class Lepus_EIPMember {
   public String userAcc;
   public String username;
   public String role;
   public String action;
   public String userEmail;
   
   public String wsAccountAccessLevel;
   public String wsAccountId;
   public String wsInsertDate;
   public String wsIsDeleted;
   public String wsSystemModStamp;
   public String wsUpdateDate;
   public String wsUserId;
   
   public String wsOpportunityAccessLevel;
   public String wsOpportunityId;
   
   public String userCustom1;
   public String userCustom2;
   public String userCustom3;
   public String userCustom4;
   public String userCustom5;
}