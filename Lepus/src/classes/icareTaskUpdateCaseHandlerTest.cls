@isTest
private class icareTaskUpdateCaseHandlerTest {

    static testMethod void insertTest() {
    	Case c = UtilUnitTest.createChinaServiceRequest();
    	c = [select id,PSE_Actual_Start_Time__c,R_D_Start_Time__c,HRE_Actual_Time__c,PSE_Planned_Start_Time__c,
    		HPE_Actual_Time__c,R_D_Actual_Time__c,PSE_Planned_End_Time__c,iCare_L1_To_L2_Time__c,iCare_L2_To_L3_Time__c from 
    		Case where id = :c.id];
    	System.assertEquals(null,c.PSE_Actual_Start_Time__c);
    	System.assertEquals(null,c.PSE_Planned_Start_Time__c);
    	System.assertEquals(null,c.PSE_Planned_End_Time__c);
    	System.assertEquals(null,c.R_D_Start_Time__c);
    	System.assertEquals(null,c.HRE_Actual_Time__c);
    	System.assertEquals(null,c.HPE_Actual_Time__c);
    	System.assertEquals(null,c.R_D_Actual_Time__c);
    	System.assertEquals(null,c.iCare_L1_To_L2_Time__c);
    	System.assertEquals(null,c.iCare_L2_To_L3_Time__c);
    	
    	Datetime now = Datetime.now();
    	iCare_Task__c task = new iCare_Task__c(iCare_Creation_Date__c = now,Actual_Start_Date__c = now,Actual_End_Date__c = now,
    		Service_Request__c = c.id,Type__c = 'SR Escalate to L2',Planned_Start_Date__c = now,Planned_End_Date__c = now);
    	iCare_Task__c task1 = new iCare_Task__c(iCare_Creation_Date__c = now,Actual_End_Date__c = now,
    		Service_Request__c = c.id,Type__c = 'SR Escalate to L3',Actual_Start_Date__c = now,
    		Planned_Start_Date__c = now,Planned_End_Date__c = now);
    	insert new iCare_Task__c[]{task,task1};
    	c = [select id,PSE_Actual_Start_Time__c,R_D_Start_Time__c,HRE_Actual_Time__c,PSE_Planned_Start_Time__c,
    		HPE_Actual_Time__c,R_D_Actual_Time__c,PSE_Planned_End_Time__c,iCare_L1_To_L2_Time__c,RD_Planned_Start_Time__c,
    		RD_Planned_End_Time__c,iCare_L2_To_L3_Time__c from 
    		Case where id = :c.id];
    	//L2
    	System.assertEquals(now,c.PSE_Actual_Start_Time__c);
    	System.assertEquals(now,c.PSE_Planned_Start_Time__c);
    	System.assertEquals(now,c.PSE_Planned_End_Time__c);
    	System.assertEquals(now,c.iCare_L1_To_L2_Time__c);
    	System.assertEquals(now,c.HPE_Actual_Time__c);//c.HPE_Actual_Time__c = t.Actual_End_Date__c;
    	//L3
    	System.assertEquals(now,c.R_D_Start_Time__c);//c.R_D_Start_Time__c = t.Actual_Start_Date__c;
    	System.assertEquals(now,c.R_D_Actual_Time__c);//c.R_D_Actual_Time__c = t.Actual_End_Date__c;
    	System.assertEquals(now,c.RD_Planned_Start_Time__c);//c.RD_Planned_Start_Time__c = t.Planned_Start_Date__c;
    	System.assertEquals(now,c.RD_Planned_End_Time__c);//c.RD_Planned_End_Time__c = t.Planned_End_Date__c;
    	System.assertEquals(now,c.iCare_L2_To_L3_Time__c);//c.iCare_L2_To_L3_Time__c = t.iCare_Creation_Date__c;
    }
    
    static testMethod void updateTest() {
    	Case c = UtilUnitTest.createChinaServiceRequest();
    	c = [select id,PSE_Actual_Start_Time__c,R_D_Start_Time__c,HRE_Actual_Time__c,PSE_Planned_Start_Time__c,
    		HPE_Actual_Time__c,R_D_Actual_Time__c from 
    		Case where id = :c.id];
    	System.assertEquals(null,c.PSE_Actual_Start_Time__c);
    	System.assertEquals(null,c.R_D_Start_Time__c);
    	System.assertEquals(null,c.HRE_Actual_Time__c);
    	System.assertEquals(null,c.HPE_Actual_Time__c);
    	System.assertEquals(null,c.R_D_Actual_Time__c);
    	
    	Datetime now = Datetime.now();
    	iCare_Task__c task = new iCare_Task__c(iCare_Creation_Date__c = now,Actual_Start_Date__c = now,Actual_End_Date__c = now,
    		Service_Request__c = c.id,Planned_Start_Date__c = now,Planned_End_Date__c = now);
    	iCare_Task__c task1 = new iCare_Task__c(iCare_Creation_Date__c = now,Actual_End_Date__c = now,
    		Service_Request__c = c.id,Actual_Start_Date__c = now,
    		Planned_Start_Date__c = now,Planned_End_Date__c = now);
    	insert new iCare_Task__c[]{task,task1};
    	c = [select id,PSE_Actual_Start_Time__c,R_D_Start_Time__c,HRE_Actual_Time__c,HPE_Actual_Time__c,R_D_Actual_Time__c from 
    		Case where id = :c.id];
    	System.assertEquals(null,c.PSE_Actual_Start_Time__c);
    	System.assertEquals(null,c.R_D_Start_Time__c);
    	System.assertEquals(null,c.HRE_Actual_Time__c);
    	System.assertEquals(null,c.HPE_Actual_Time__c);
    	System.assertEquals(null,c.R_D_Actual_Time__c);
    	
    	task.Type__c = 'SR Escalate to L2';
    	task1.Type__c = 'SR Escalate to L3';
    	update new iCare_Task__c[]{task,task1};
    	c = [select id,PSE_Actual_Start_Time__c,R_D_Start_Time__c,HRE_Actual_Time__c,PSE_Planned_Start_Time__c,
    		HPE_Actual_Time__c,R_D_Actual_Time__c,PSE_Planned_End_Time__c,iCare_L1_To_L2_Time__c,RD_Planned_Start_Time__c,
    		RD_Planned_End_Time__c,iCare_L2_To_L3_Time__c from 
    		Case where id = :c.id];
    	//L2
    	System.assertEquals(now,c.PSE_Actual_Start_Time__c);
    	System.assertEquals(now,c.PSE_Planned_Start_Time__c);
    	System.assertEquals(now,c.PSE_Planned_End_Time__c);
    	System.assertEquals(now,c.iCare_L1_To_L2_Time__c);
    	System.assertEquals(now,c.HPE_Actual_Time__c);//c.HPE_Actual_Time__c = t.Actual_End_Date__c;
    	//L3
    	System.assertEquals(now,c.R_D_Start_Time__c);//c.R_D_Start_Time__c = t.Actual_Start_Date__c;
    	System.assertEquals(now,c.R_D_Actual_Time__c);//c.R_D_Actual_Time__c = t.Actual_End_Date__c;
    	System.assertEquals(now,c.RD_Planned_Start_Time__c);//c.RD_Planned_Start_Time__c = t.Planned_Start_Date__c;
    	System.assertEquals(now,c.RD_Planned_End_Time__c);//c.RD_Planned_End_Time__c = t.Planned_End_Date__c;
    	System.assertEquals(now,c.iCare_L2_To_L3_Time__c);//c.iCare_L2_To_L3_Time__c = t.iCare_Creation_Date__c;
    	
    }
}