@isTest
private class ServiceRequestContracstHandlerTest {
		static testMethod void notRun(){
				Case serviceRequest = UtilUnitTest.createChinaServiceRequest();
				CommonConstant.serviceRequestTriggerShouldRun = false;
				update serviceRequest;
		}
		
		static testMethod void notRun2(){
				Case serviceRequest = UtilUnitTest.createChinaServiceRequest();
				ServiceRequestOwnerChangedHandler.shouldRun = false;
				update serviceRequest;
		}
		
 		static testMethod void contrastPopulateUpdateTest() {
    	Account acc = UtilUnitTest.createAccount();
    	CSS_Contract__c contract = UtilUnitTest.createContract(acc);
    	SFP__c sfp = UtilUnitTest.createSFP(contract);
    	Installed_Asset__c asset = UtilUnitTest.createAsset(contract);
    	Case serviceRequest = UtilUnitTest.createChinaServiceRequest();
    	SFP_Asset__c sfpA = UtilUnitTest.createSFPAsset(asset, sfp);
    	
    	serviceRequest.barcode__c = asset.id;
    	update serviceRequest;
    	
    	serviceRequest = [select id,Contract_No__c,W_M_SFP__c from Case where id =:serviceRequest.id];
    	System.assertEquals(contract.id,serviceRequest.Contract_No__c);
    	System.assertEquals(sfp.id,serviceRequest.W_M_SFP__c);
    }
}