@isTest
private class ServiceRequestSyncUtilsTest {
    static testMethod void singleTest(){
    	String whereClause = ' Profile.Name  = \'(CSS)国内呼叫中心代表(CCR)\' and userrole.name = \'(N) Call Center Rep(CCR)\'';
    	User ccrUser = UtilUnitTest.queryUser(whereClause);
   		Account acc = UtilUnitTest.createAccount();
   		Contact con = UtilUnitTest.createContact(acc);
   		con.ownerid = ccrUser.id;
   		update con;
    	System.runAs(ccrUser){
	    	Case c = UtilUnitTest.createCCRRequest(con);
	    	Test.startTest();
	    	Test.setMock(WebServiceMock.class, new EIPMockImpl());
	    	ServiceRequestSyncUtils.syncToEIP(c.id);
	    	Test.stopTest();
	    	Case c2 = [select id,Sync_Times__c,iCare_Number__c,iCare_Sync_ACK__c,iCare_Sync_Status__c from case ];
	    	System.assertEquals(c.id,c2.id);
	    	System.assertEquals(0,c2.Sync_Times__c);
	    	//System.assertEquals('123456',c2.iCare_Number__c);
	    	//System.assertEquals('success',c2.iCare_Sync_ACK__c);
	    	//System.assertEquals('同步成功',c2.iCare_Sync_Status__c);
    	}
    }
    
    
    static testMethod void singleExceptionTest(){
    	Case c = createCase();//it is weird,you have to put the code in another method to avoid the pending ... exception
    	Test.startTest();
    	Test.setMock(WebServiceMock.class, new EIPMockImpl());
    	EIPMockImpl.shouldThrowException = true;
    	ServiceRequestSyncUtils.syncToEIP(c.id);
    	Test.stopTest();
    	Case c2 = [select id,Sync_Times__c,iCare_Number__c,iCare_Sync_ACK__c,iCare_Sync_Status__c from case ];
    	System.assertEquals(c.id,c2.id);
    	System.assertEquals(null,c2.iCare_Number__c);
    	//System.assertEquals(null,c2.iCare_Sync_ACK__c);
    	//System.assertEquals(System.Label.Sync_Error_Msg +'数据格式错误，请联系管理员',c2.iCare_Sync_Status__c);
    }
    
    public static case createCase(){
      Product_HS__c hs = new Product_HS__c(Name = 'Test Product 1 ',
                                                  Level_1__c = '1', Active__c = 'Yes',
                                                  Product_Level__c = '1', Offering_Code__c = '12334',
                                                  External_Id__c = String.valueOf(System.now()));
        
      insert hs;
    	RecordType chinaRecordType = [select id from RecordType where isActive = true 
    		and developername = 'China_External_Post_Sales_CCR' and SobjectType = 'case' limit 1];
      Account acc = UtilUnitTest.createAccount();
    	Contact theContact = UtilUnitTest.createContact(acc);
    	Case c = new Case(recordtypeid = chinaRecordType.id,iCare_Owner__c = Userinfo.getUserId(),Subject = 'test',HW_Product__c = hs.id,
    		AccountId = acc.id,ContactId = theContact.id);
    	insert c;
    	return c;
    }
    
    
    static testMethod void singleExceptionTest2(){//miss field
    	Account acc = UtilUnitTest.createAccount();
    	Contact con = UtilUnitTest.createContact(acc);
	    Case c = UtilUnitTest.createCCRRequest(con);
    	Test.startTest();
    	Test.setMock(WebServiceMock.class, new EIPMockImpl());
    	EIPMockImpl.shouldThrowException = true;
    	ServiceRequestSyncUtils.syncToEIP(c.id);
    	Test.stopTest();
    	Case c2 = [select id,Sync_Times__c,iCare_Number__c,iCare_Sync_ACK__c,iCare_Sync_Status__c from case ];
    	System.assertEquals(c.id,c2.id);
    	//System.assertEquals(1,c2.Sync_Times__c);
    	//System.assertEquals(null,c2.iCare_Number__c);
    	//System.assertEquals(null,c2.iCare_Sync_ACK__c);
    	//System.assertEquals('数据不完整，请补齐后刷新页面重新同步',c2.iCare_Sync_Status__c);
    }
    
    
    static testMethod void syncedTest(){//already has icare number
    	Case c = UtilUnitTest.createChinaServiceRequest();
    	c.iCare_Number__c = '11';
    	update c;
    	Test.startTest();
    	//Test.setMock(WebServiceMock.class, new EIPMockImpl());
    	ServiceRequestSyncUtils.syncToEIP(c.id);//already has an icare number,so no callout will be made
    	Test.stopTest();
    	Case c2 = [select id,Sync_Times__c,iCare_Number__c,iCare_Sync_ACK__c,iCare_Sync_Status__c from case ];
    	System.assertEquals(c.id,c2.id);
    	System.assertEquals(0,c2.Sync_Times__c);
    	System.assertEquals('11',c2.iCare_Number__c);
    	System.assertEquals(null,c2.iCare_Sync_ACK__c);
    	System.assertEquals(null,c2.iCare_Sync_Status__c);
    }
    
    static testMethod void syncedTest2(){//not a CCR record type
    	Case c = UtilUnitTest.createChinaServiceRequest();
    	RecordType rt = [select id,name from RecordType where isActive = true and SobjectType = 'Case' and (not name like '%CCR%') limit 1];
    	update c;
    	Test.startTest();
    	//Test.setMock(WebServiceMock.class, new EIPMockImpl());
    	ServiceRequestSyncUtils.syncToEIP(c.id);//not a CCR record type,so no callout will be made
    	Test.stopTest();
    	Case c2 = [select id,Sync_Times__c,iCare_Number__c,iCare_Sync_ACK__c,iCare_Sync_Status__c from case ];
    	System.assertEquals(c.id,c2.id);
    	System.assertEquals(0,c2.Sync_Times__c);
    	System.assertEquals(null,c2.iCare_Sync_ACK__c);
    	System.assertEquals(null,c2.iCare_Sync_Status__c);
    }
    
    static testMethod void batchSyncTest(){
    	Case c = createCase();
    	Test.startTest();
    	Test.setMock(WebServiceMock.class, new EIPMockImpl());
    	EIPMockImpl.isBatchMode = true;
    	ServiceRequestSyncUtils.batchSync(new id[]{c.id});
    	Test.stopTest();
    }
    
    static testMethod void updateToEIPTest(){
    	Case c = UtilUnitTest.createChinaServiceRequest();
			c.subject = 'test';
			c.iCare_Number__c = '11';
			String whereClause = ' Profile.Name  like \'%Integration%\' ';
	    User u = UtilUnitTest.queryUser(whereClause);
	    System.runas(u){//use this to avoid callout in trigger
	    	update c;
	    }
			Test.startTest();
		  Test.setMock(WebServiceMock.class, new EIPMockImpl());
		  EIPMockImpl.isBatchMode = true;
		  EIPMockImpl.isUpdateMode = true;
		  ServiceRequestSyncUtils.updateToEIP(new id[]{c.id});
		  Test.stopTest();
		  //System.assertEquals(0,[select count() from iCareLog__c]);
    }
    
    
}