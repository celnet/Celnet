/*
* @Purpose : it is for incremental blacklist checking in green Accounts 
*
* @Author : Jen Peng
* @Date : 		2013-5-4
*/
global class BlacklistIncrementalCheckBatch implements Database.Batchable<sObject>,Schedulable,Database.Stateful{
	public String query;
	public List<Blacklist_Check_History__c> histories;
	public Integer counter;
	public Set<String> fawuEmails{get;set;}//for no duplicate fawu emails
	public List<Messaging.SendEmailResult> results{get;set;} 
	public List<User> users{get;set;}
	public List<User> adm {get;set;}
	public List<String> admEmails{get;set;}
	public boolean isChain{get;set;}//it is added by jen on 2013-07-24
	
	
	global BlacklistIncrementalCheckBatch(){
		histories=new List<Blacklist_Check_History__c>();
		counter=0;
		isChain=true;//it is added by jen on 2013-07-24
		fawuEmails=new Set<String>();
		admEmails=new List<String>();
		users=new List<User>();
		adm=new List<User>();
		try{
			users=[Select Id,Name,Profile.Name,Email,IsActive From User where Profile.Name='(Huawei) HQ Legal Affairs Mgmt'];
			for(User u:users){
				if(u.IsActive){
					fawuEmails.add(u.Email);
				}
			}
			adm=[Select Id,Name,Profile.Name,Email,IsActive From User where Profile.Name='IT Supporter'];
			for(User u:adm){
				if(u.IsActive){
					admEmails.add(u.Email);
				}
			}
		}catch(Exception e){}
	}
	global Database.QueryLocator start(Database.BatchableContext bc){
		AccountEmailInformHandler.isFirstRun=false;	
		AccountAfterUpdateHandler.isFirstRun=false;
		AccountNAChangedHandler.isFirstRun=false; 
		AccountUpdateRootHandler.isFirstRun=false;
		AccountUpdateRegionDeptHandler.isFirstRun=false;
		AccountUpdateIndustryHandler.isFirstRun=false; 
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC, list<sObject> scope) {
		histories.clear();
		List<BlackList__c> blacklists = new List<BlackList__c>();
		Map<String,BlackList__c> Acc2BlacklistMap = new Map<String,BlackList__c>();
		String blacklistQuery='select id,code__c,country__c,street1__c,street2__c,city__c,state__c,Blacklist_Alias__c from BlackList__c where status__c=\'new\'';
		String cond = '';
		for(sObject s : scope){
			Account acc = (Account)s;
			
			String AccName = acc.Blacklist_Account_Alias__c;
			String Country = acc.Country_Code_HW__c;
			if(Country==null||Country==''){
				cond += ' Blacklist_Alias__c = \''+AccName+'\' OR';

			}else{
				cond +=' (Blacklist_Alias__c = \''+AccName+'\' and (country__c=\''+Country+'\' or country__c=\'XX\' or country__c=\'\')) OR';

			}
		}
		blacklistQuery += ' and ('+cond.substring(0,cond.length()-3)+')';
		
		try{
			blacklists=Database.query(blacklistQuery);
		}catch(Exception e){
		
		}
		
		for(BlackList__c b:blacklists){
			Acc2BlacklistMap.put(b.Blacklist_Alias__c,b);
		}
		List<Account> accCarry = new List<Account>();//it is added by jen on 2013-07-24
		for(sObject s : scope){
			Account acc = (Account)s;
			Blacklist_Check_History__c bch = new Blacklist_Check_History__c();
			
			if(Acc2BlacklistMap.get(acc.Blacklist_Account_Alias__c)!=null){
				counter++;
				acc.Blacklist_Status__c='Yellow';
				acc.blacklist_type__c='Pending';
				acc.blacklist_lock__c = false;
				acc.isFollow__c=false;
				acc.blacklist_comment__c='';
				bch.Account__c=acc.Id;
				bch.BlackList__c=Acc2BlacklistMap.get(acc.Blacklist_Account_Alias__c).id;
				bch.status_date__c=system.now();
				bch.Account_Country__c=acc.Country_Code_HW__c;
				bch.Last_status__c=bch.Blacklist_Status__c;
				bch.Blacklist_Status__c=acc.Blacklist_Status__c;
				bch.blacklist_type__c=acc.blacklist_type__c;
				accCarry.add(acc);//it is added by jen on 2013-07-24
				histories.add(bch);
			}
				
		}
		
		AccountEmailInformHandler.isFirstRun=false;	
		AccountAfterUpdateHandler.isFirstRun=false;
		AccountNAChangedHandler.isFirstRun=false; 
		AccountUpdateRootHandler.isFirstRun=false;
		AccountUpdateRegionDeptHandler.isFirstRun=false;
		AccountUpdateIndustryHandler.isFirstRun=false; 
		if(!histories.isEmpty()){
			insert histories;
		}
				
		try{
			update accCarry;//it is added by jen on 2013-07-24
			//update scope;
		}catch(Exception e){
			Messaging.SingleEmailMessage mail2Adm = new Messaging.SingleEmailMessage();
			mail2Adm.setSubject('Exceptions while Blacklist Checking');
			mail2Adm.setToAddresses(admEmails);
			mail2Adm.setHtmlBody('内容：<br/>'+e+'');
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail2Adm });
		}
	}
	
	global void finish(Database.BatchableContext BC){
		Integer n = fawuEmails.size();	
		system.debug('n========================'+n);
		system.debug('counter========================'+counter);	
		if(counter>0 && n>0){
			Messaging.reserveSingleEmailCapacity(n);
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			List<string> toAddress = new List<string>();
			if(fawuEmails.size()>0){
				for(String s:fawuEmails){
					toAddress.add(s);
				}
			}
			mail.setToAddresses(toAddress);
			mail.setSenderDisplayName('Important No-reply email from Salesforce System');
			mail.setSubject('There some accounts looks like blacklist!');
			String strbody = '<p>Dear Mr/Ms:</p>';
			strbody += '<p>There some accounts are suspected to be Export Control Blacklist  !Please check by following link:</p>';
			strbody += system.Url.getSalesforceBaseUrl().toExternalForm()+'/apex/BlacklistHumanCheck';
			mail.setHtmlBody(strbody);
			results=Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		}
		
		if(isChain){//it is added by jen on 2013-07-24
			BlacklistDecrementalCheckBatch bdcb = new BlacklistDecrementalCheckBatch();
			bdcb.query='select id,Name,Blacklist_Account_Alias__c,Country_Code_HW__c,Blacklist_Status__c,blacklist_type__c from Account where Blacklist_Status__c=\'Red\'';
			Id batchId = Database.executeBatch(bdcb,50);
		}
			
	}
	
	global void execute(SchedulableContext SC){
		BlacklistIncrementalCheckBatch bicb = new BlacklistIncrementalCheckBatch();
		bicb.query='select id,Name,Blacklist_Account_Alias__c,Country_Code_HW__c,blacklist_type__c,Blacklist_Status__c from Account where Blacklist_Status__c=\'Green\'';
		Id batchId = Database.executeBatch(bicb,50);
	}
}