/*
* @Purpose : 特殊角色创建后自动将角色刷新到用户的特殊角色字段上
* @Author : steven
* @Date :		2014-02-17
*/
public without sharing class SpecialRoleHandler implements Triggers.Handler{ 
	
	public void handle(){
		//获得所有的PermissionAset类型
		Set<String> permissionSetNames = new Set<String>();
		Map<Id,String> userRegionMap = new Map<Id,String>();
		if(Trigger.isInsert || Trigger.isUpdate) {
			for(Special_Role__c sr : (List<Special_Role__c>)trigger.new){
				String roleName = sr.Role_Name__c;
				//如果有多个特殊角色使用同一个权限集，考虑到界面显示的角色名称不一样，需要增加多个下拉列表值，因此CODE需要保存为"_X_RN"结尾防止重复
				if(roleName.endsWith('_RN')) roleName = roleName.substring(0,roleName.length()-5);
				permissionSetNames.add(roleName);
			}
		}
		if(trigger.isDelete) {
			for(Special_Role__c sr : (List<Special_Role__c>)trigger.old){
				String roleName = sr.Role_Name__c;
				if(roleName.endsWith('_RN')) roleName = roleName.substring(0,roleName.length()-5);
				permissionSetNames.add(roleName);
			}
		}
		//分别处理每种PermissionAset类型
		for(String permissionSetName:permissionSetNames) {
			Set<id> addUserIds = new Set<id>();
			Set<id> delUserIds = new Set<id>();
			if(Trigger.isInsert || Trigger.isUpdate) {
				for(Special_Role__c sr : (List<Special_Role__c>)trigger.new){
					String roleName = sr.Role_Name__c;
					if(roleName.endsWith('_RN')) roleName = roleName.substring(0,roleName.length()-5);
					if(roleName == permissionSetName) {
						addUserIds.add(sr.User__c);
						userRegionMap.put(sr.User__c,sr.Representative_Office__c);
					}
				}
			}
			if(trigger.isDelete) {
				for(Special_Role__c sr : (List<Special_Role__c>)trigger.old){
					String roleName = sr.Role_Name__c;
					if(roleName.endsWith('_RN')) roleName = roleName.substring(0,roleName.length()-5);
					if(roleName == permissionSetName) {
						delUserIds.add(sr.User__c);
						userRegionMap.put(sr.User__c,sr.Representative_Office__c);
					}
				}
			}
			assignPermissionSet(permissionSetName,addUserIds,delUserIds);
			if(permissionSetName == 'China_Business_Leader') {
				shareTCGToBusinessLeader(addUserIds, delUserIds, userRegionMap);
			}
		}
	}
	/**
	 * 自动Share给商业客户经理只读权限
	 */
	private void shareTCGToBusinessLeader(Set<Id> addUserIds, Set<Id> delUserIds, Map<Id,String> userRegionMap){
		List<AccountShare> addShareRecords = new List<AccountShare>();
		List<AccountShare> delShareRecords = new List<AccountShare>();
		//处理新增的Leader
		if(addUserIds.size()>0) {
			for(Id uId : addUserIds){
				List<Account> tcgList = [select id from account WHERE recordtypeid =: CONSTANTS.HUAWEICHINATCGRECORDTYPE 
																    AND TCG_Representative_Office__c =: userRegionMap.get(uId) AND OwnerId <>: uId];
				for(Account acc : tcgList){
					AccountShare aShare = new AccountShare(AccountId = acc.id,UserOrGroupId = uId,
						AccountAccessLevel = 'Read',OpportunityAccessLevel = 'None',CaseAccessLevel = 'None', ContactAccessLevel = 'None');
					addShareRecords.add(aShare);
				}	
			}
		}
		//处理删除的Leader
		if(delUserIds.size()>0) {
			for(Id uId : delUserIds){
				Set<Id> tcgIds = new Set<Id>();
				List<Account> tcgList = [select id from account WHERE recordtypeid =: CONSTANTS.HUAWEICHINATCGRECORDTYPE 
																    AND TCG_Representative_Office__c =: userRegionMap.get(uId) AND OwnerId <>: uId];
				for(Account acc : tcgList) tcgIds.add(acc.id);
				if(tcgIds.size()>0) {
					List<AccountShare> asList = 
						[select id from AccountShare where AccountId =:tcgIds AND UserOrGroupId=:uId AND AccountAccessLevel = 'Read'];
					for(AccountShare aShare : asList) delShareRecords.add(aShare);
				}
			}
		}
		if(addShareRecords.size()>0) insert addShareRecords;
		if(delShareRecords.size()>0) delete delShareRecords;
	}
	
	@future
	public static void assignPermissionSet(String permissionSetName,Set<id> addUserIds,Set<id> delUserIds){
		List<Special_Role__c> srList = new List<Special_Role__c>();
		List<PermissionSetAssignment> addAssigneeList = new List<PermissionSetAssignment>();
		List<PermissionSetAssignment> delAssigneeList = new List<PermissionSetAssignment>();
		Map<Id,PermissionSetAssignment> assigneeMap = new Map<Id,PermissionSetAssignment>();
		Set<id> assigneeUserIds = new Set<id>();
		
		if(addUserIds.isEmpty() && delUserIds.isEmpty()) return;
		
    //获取权限集Id
    Id permissionSetId = null;
    List<PermissionSet> permissionSetList = 
    	[Select Id FROM PermissionSet where name =:permissionSetName];
    if(permissionSetList.size()>0) {
    	permissionSetId = permissionSetList.get(0).id;
    } else {
    	return;
    }
		
		//获取分配过的权限集
		List<PermissionSetAssignment> permissionDistributeList = 
			[Select Id,AssigneeId FROM PermissionSetAssignment where PermissionSetId =:permissionSetId AND (AssigneeId in:addUserIds OR AssigneeId in:delUserIds)];
		for(PermissionSetAssignment psd : permissionDistributeList) {
			assigneeUserIds.add(psd.AssigneeId);
			assigneeMap.put(psd.AssigneeId,psd);
		}
		
		//分配权限
		for(Id addUserId:addUserIds) {
			if(assigneeUserIds.contains(addUserId)) break;
			PermissionSetAssignment ps = new PermissionSetAssignment();
			ps.PermissionSetId = permissionSetId;
			ps.AssigneeId = addUserId;
			addAssigneeList.add(ps);
		}
		
		//删除权限
		for(Id delUserId:delUserIds) {
			if(!assigneeMap.containsKey(delUserId)) break;
			delAssigneeList.add(assigneeMap.get(delUserId));
		}
		
		if(addAssigneeList.size()>0) insert addAssigneeList;
		if(delAssigneeList.size()>0) delete delAssigneeList;
	}
}