/*
* @Purpose : it is the trigger handler for marking the isNew field on the Blacklist
* check history sobject.
*
* @Author : Jen Peng
* @Date : 		2013-5-13
*/
public with sharing class BlacklistCheckHistoryIsNewInsertHandler implements Triggers.Handler{
	public void handle(){
		try{
			 Set<Id> accids = new Set<Id>();
			 Map<Id,Blacklist_Check_History__c> mapBch = new Map<Id,Blacklist_Check_History__c>();
			for(Blacklist_Check_History__c b:(List<Blacklist_Check_History__c>)Trigger.new){
				accids.add(b.Account__c);
			}//这里取account id是为了控制取值范围
			List<Blacklist_Check_History__c> bchs=[select id,Account__c,isNew__c from Blacklist_Check_History__c
													where isNew__c=true and Account__c in: accids];
			accids.clear();
			if(!bchs.isEmpty()){
				for(Blacklist_Check_History__c b:bchs){
					mapBch.put(b.Account__c,b);
				}
				bchs.clear();
				for(Blacklist_Check_History__c b:(List<Blacklist_Check_History__c>)Trigger.new){
					b.isNew__c=true;
					mapBch.get(b.Account__c).isNew__c=false;
				}
				update mapBch.values();
			}else{
				for(Blacklist_Check_History__c b:(List<Blacklist_Check_History__c>)Trigger.new){
					b.isNew__c=true;
				}
			}
			
		}catch(Exception e){
			System.debug('oppooos~!'+e);
		}
	}
}