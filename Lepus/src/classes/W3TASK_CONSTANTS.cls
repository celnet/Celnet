public without sharing class W3TASK_CONSTANTS {
public static final Integer TIME_OUT = 80 * 1000;
public static final String SOAP_FROM_NAME = 'Cu32SF2W3';
public static final String SOAP_TO_NAME = 'Huawei';
public static final String ENDPOINT = 'https://b2b-test1.huawei.com:3777/SOAP';
}