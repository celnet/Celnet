@isTest
private class AccountIntegrationTest {

    static testMethod void noUpdate() {
    	Account acc = UtilUnitTest.createHWChinaNAAccount('TEST001');
    	//create the custom setting  
    	CISENDPOINT__c setting = new CISENDPOINT__c(name = 'url',url__c='testurl');
    	insert setting;
    	Test.startTest();
    	AccountIntegration controller = new AccountIntegration(new ApexPages.StandardController(acc));
    	controller.submit();
    	Test.stopTest();
    }
    static testMethod void allUpdate() {
    	Account acc = UtilUnitTest.createHWChinaNAAccount('TEST001');
    	//create the custom setting  
    	CISENDPOINT__c setting = new CISENDPOINT__c(name = 'url',url__c='testurl');
    	insert setting;
    	Test.startTest();
    	AccountIntegration controller = new AccountIntegration(new ApexPages.StandardController(acc));
    	controller.a.City_Huawei_China__c = '上海';
    	controller.a.Customer_Group_Code__c = 'test';
    	controller.submit();
    	Test.stopTest();
    }
	static testMethod void onlyCity() {
    	Account acc = UtilUnitTest.createHWChinaNAAccount('TEST001');
    	//create the custom setting  
    	CISENDPOINT__c setting = new CISENDPOINT__c(name = 'url',url__c='testurl');
    	insert setting;
    	Test.startTest();
    	AccountIntegration controller = new AccountIntegration(new ApexPages.StandardController(acc));
    	controller.a.City_Huawei_China__c = '上海';
    	controller.submit();
    	Test.stopTest();
    }
}