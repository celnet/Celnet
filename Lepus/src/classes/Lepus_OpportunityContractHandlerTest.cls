@isTest
private class Lepus_OpportunityContractHandlerTest {
	static testmethod void myUnitTest(){
		
		
		Lead l = new Lead();
		l.LastName = 'ddd';
		l.Company = 'fad';
		insert l;
		
		Account acc = Lepus_EIPCalloutServiceTest.createAccount('tesss');
		insert acc;
		
		Opportunity opp = Lepus_EIPCalloutServiceTest.createOpportunity('ddddfads', acc.Id);
		opp.Lead__c = l.Id;
		insert opp;
		
		Lepus_EIPCalloutServiceTest.setCustomSettings();
		
		Opportunity_Contract__c con = new Opportunity_Contract__c();
		con.Opportunity__c = opp.Id;
		insert con;
	}
}