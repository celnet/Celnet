/**
 * Author : Sunda
 * Des : W3 Task手工同步batch
 **/
global class W3Task_ManualSyncBatch implements Database.Batchable<W3_Task_Sync_Fail__c>, Database.Stateful,Database.AllowsCallouts{
	List<W3_Task_Sync_Fail__c> list_SyncFail ;
	global W3Task_ManualSyncBatch(List<W3_Task_Sync_Fail__c> listsf){
		list_SyncFail = listsf;
	}
	global List<W3_Task_Sync_Fail__c> start(Database.BatchableContext bc){
		system.debug('in batch'+list_SyncFail);
		return list_SyncFail;
	}
	global void execute(Database.BatchableContext BC, list<W3_Task_Sync_Fail__c> scope){
		W3Task_ManualSyncController mSync = new W3Task_ManualSyncController();
		mSync.syncToEIP(scope);
	}
	global void finish(Database.BatchableContext BC){
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		email.setToAddresses(new String[] { [select email from user where id=:UserInfo.getUserId()].email });
		email.setSubject('W3 Task手工同步完成');
		String sBody = '您好, <br/>';
		sBody+='W3 Task手工同步已完成，请登录系统进行查看。<br/>';
		sBody+='请点击此<a href="https:'+URL.getSalesforceBaseUrl().getHost()+'/apex/W3Task_ManualSync">链接</a>进行查看。<br/><br/>';
		sBody+='谢谢<br/>';
		sBody+='_______________________________<br/>';
		sBody+='Salesforce系统邮件，请勿回复<br/>';
		email.setHtmlBody(sBody);
		Messaging.sendEmail(new Messaging.Email[] { email });
    }
}