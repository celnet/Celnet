public class DealRegToVoid {
	public String currId{ 
        get{return currId;}
        set{currId=value;} 
    } 
   
     public DealRegToVoid(ApexPages.StandardController controller) { 
        currId= ApexPages.currentPage().getParameters().get('id');
    }
    
    public DealRegToVoid() { 
        currId= ApexPages.currentPage().getParameters().get('id');
    }
 
   
    public Boolean haveError{ 
        get{return haveError;} 
        set{haveError=value;}
    }  
   
    public String getId(){
        return currId;  
    } 
   
    public void init(){ 
    	String userId=UserInfo.getUserId();
    	Deal_Registration__c DR=[Select 
    	d.owner.id,
    	d.id,
    	d.deal_status__c 
    	from Deal_Registration__c d 
    	where d.id=:currId];
    	
    	DR.deal_status__c='Invalid';
        update(DR);
        
    }
    
    public static testMethod void currClsTest(){
    	Deal_Registration__c dr=[select
	        id,
	        name,
	        deal_status__c,
	        sales_stage__c,
	        win_probability__c,
	        estimated_contracting_date__c,
	        deal_description__c,
	        end_customer_lookup__r.id,
	        Reseller__r.Id ,    
	        reseller_contact__r.Id,       
	        owner.id,
	        expiring_date__c,
	        related_opportunity__c,
	        end_customer_lookup__c
	        from Deal_Registration__c where deal_status__c<>'Invalid' limit 1];
	        DealRegToVoid dto=new DealRegToVoid();
	        dto.currId=dr.Id;
	        dto.init();
    }

}