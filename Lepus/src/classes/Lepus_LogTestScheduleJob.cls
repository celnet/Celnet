public class Lepus_LogTestScheduleJob implements Schedulable{
	public void execute(SchedulableContext sc){
		String cName = '日志测试' + (Datetime.now().getTime() / 1000);
		
		Account acc = createAccount(cName);
		insert acc;
		
		Opportunity opp = createOpportunity(cName, acc.Id);
		insert opp;
		
		Contact con = createContact(cName, acc.Id);
		insert con;
		
		Lead le = createLead(cName);
		insert le;
		
		List<Account> accList = [Select Id, City_Huawei_China__c From Account Where Name like '日志测试%' limit 50];
		
		update accList;
		
		List<Opportunity> oppList = [Select Id From Opportunity Where Name like '日志测试%' limit 50];
		update oppList;
		
		List<Contact> conList = [Select Id From Contact Where LastName like '日志测试%' limit 50];
		update conList;
		
		List<Lead> leadList = [Select Id From Lead Where LastName like '日志测试%' limit 50];
		update leadList;
		
	}
	
	public static Opportunity createOpportunity(String oppName, Id accId){
		Opportunity opp = new Opportunity();
		opp.Name = oppName;
		opp.AccountId = accId;
		opp.OwnerId = UserInfo.getUserId();
		opp.Opportunity_Level_Code__c = 'SubRegion';
		//opp.Opportunity_Level__c = 'Region Level';
		//opp.Opportunity_Type__c = 'Carrier Resales';
		opp.Opportunity_Type_Code__c = 'SM002';
		opp.China_Competitor_Trends__c = 'xxx';
		opp.Project_Risk__c = 'xxx';
		opp.China_Following_Strategy__c = 'xxx';
		opp.Master_Channer_Partner__c = 'xxx';
		opp.StageName = 'SS7 (Implementing)';
		opp.CloseDate = Date.today();
		opp.RecordTypeId = CONSTANTS.HUAWEIOVERSEAOPPRECORDTYPE;
		
		return opp;
	}
	
	public static Account createAccount(String accName){
		Account acc = new Account();
		acc.Name = accName;
		acc.RecordTypeId = CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE;
		acc.City_Huawei_China__c = '北京市';
		acc.OwnerId = UserInfo.getUserId();
		
		return acc;
	}
	
	public static Contact createContact(String LastName2, Id accId){
		Contact con = new Contact();
		con.RecordTypeId = CONSTANTS.HUAWEICHINACONTACTRECORDTYPE;
		con.AccountId = accId;
		con.FirstName = 'Chung';
		con.LastName = LastName2;
		
		return con;
	}
	
	public static Lead createLead(String leadName){
		Lead le = new Lead();
		le.RecordTypeId = CONSTANTS.CHINALEADRECORDTYPE;
		le.Company = leadName;
		le.LastName = leadName;
		
		return le;
	}
	
	
}