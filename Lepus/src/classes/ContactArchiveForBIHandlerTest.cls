@isTest
private class ContactArchiveForBIHandlerTest {
    static testMethod void deleteOverseaContact() {
    	Account acc = UtilUnitTest.createHWChinaAccount('testacc');
    	Contact contact = new Contact();
    	contact.RecordTypeId = CONSTANTS.HUAWEIOVERSEACONTACTRECORDTYPE;
    	contact.LastName = 'test';
    	contact.Email = '1@1.1';
    	contact.AccountId = acc.id;
			insert contact;
			delete contact;
		
		ContactArchiveForBIHandler.isFirstRun = true;
		Contact contact2 = new Contact();
    	contact2.RecordTypeId = CONSTANTS.HUAWEICHINACONTACTRECORDTYPE;
    	contact2.LastName = 'test';
    	contact2.Email = '1@1.1';
    	contact2.AccountId = acc.id;
			insert contact2;
			delete contact2;
    }
}