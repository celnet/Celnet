/*
* @Purpose : 
* 计算SR下所有备件申请的总数量之和（求和Quantity），赋值给SR的Spare Amount(Remote)字段
*
*
* @Author : Brain Zhang
* @Date :       2013-11
*
*
*/
public without sharing class ComponentRequestRollUpHandler implements Triggers.Handler{
	public void handle(){
		Set<Id> caseIDs = new Set<Id>();
    if(trigger.isInsert || trigger.isUnDelete){
    	for(Component_Request__c w : (List<Component_Request__c>)trigger.new){
					caseIDs.add(w.SR_No__c);
    	}
    }
		if(trigger.isUpdate ){
			for(Component_Request__c w : (List<Component_Request__c>)trigger.new){
				if((w.SR_No__c != ((Component_Request__c)trigger.oldmap.get(w.id)).SR_No__c
					||w.Quantity__c != ((Component_Request__c)Trigger.oldmap.get(w.id)).Quantity__c)
					){//关联case变化或对应数量变化
					caseIDs.add(w.SR_No__c);
					//一并加入原来的service request no
					caseIDs.add(((Component_Request__c)trigger.oldmap.get(w.id)).SR_No__c);
				}
			}
        
		}
		if(trigger.isDelete){
       for(Component_Request__c w : (List<Component_Request__c>)trigger.old){
					caseIDs.add(w.SR_No__c);
			}
    }
    if(caseIDs.size() > 0){
    	
    	UtilLREngine.Context ctx = new UtilLREngine.Context(Case.SobjectType, // parent object
                                            Component_Request__c.SobjectType,  // child object
                                            Schema.SObjectType.Component_Request__c.fields.SR_No__c // relationship field name
                                            );     
			//计算SR下的工单备件数量
    	ctx.add(
            new UtilLREngine.RollupSummaryField(
                                            Schema.SObjectType.Case.fields.Spare_Amount_Remote__c,
                                            Schema.SObjectType.Component_Request__c.fields.Quantity__c,
                                            UtilLREngine.RollupOperation.Sum 
                                       )); 

      List<Case> cases = (List<Case>)UtilLREngine.rollUp(ctx, caseIDs);
      System.debug('the cases:--------- ' + cases);
	    CommonConstant.changedFromAccount = true;//add this to ignore the other trigger
	    // ignore all the trigger handlers on service request
	    CommonConstant.serviceRequestTriggerShouldRun = false;
			Database.SaveResult[] saveResults = Database.update(cases,false);
			for(Integer i =0;i<saveResults.size();i++){
				if(!saveResults[i].isSuccess()){
					for(Database.Error e : saveResults[i].getErrors()){
						System.debug('sth wrong happens:------------------------------------------' + e.getMessage());
						if(trigger.isInsert || trigger.isUpdate || trigger.isUnDelete){
							for(Component_Request__c w : (List<Component_Request__c>)trigger.new){
								if(w.SR_No__c == cases.get(i).id){//this triggered the exception
									w.addError('请检查对应的service request:id为' + cases.get(i).id + ',' + e.getMessage());
								}else if(((Component_Request__c)trigger.oldMap.get(w.id)).SR_No__c == cases.get(i).id ){//对更改前记录做同样的检查
									w.addError('请检查对应的service request:id为' + cases.get(i).id + ',' + e.getMessage());
								}
							}
						}
						if(trigger.isDelete || trigger.isUpdate){
							for(Component_Request__c w : (List<Component_Request__c>)trigger.old){
								if(w.SR_No__c == cases.get(i).id){//this triggered the exception
									w.addError(e.getMessage());
								}
							}
						}
					}
				}
			}
    }
	}
}