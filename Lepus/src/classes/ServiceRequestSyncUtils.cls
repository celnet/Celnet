/*
*@Purpose:实现了向ICARE同步的功能
*@Author:Brain Zhang
*@Create Date：2013-9
*/


public without sharing class ServiceRequestSyncUtils {
	
	//接收id作为参数,实时发起同步
	public static Case syncToEIP(Id serviceRequestId){
	 Case serviceRequest = [SELECT Id,RecordType.DeveloperName, Account.Name , Account.CIS__c , 
			Contact.LastName , Contact.firstname,Record_Type_Name__c,
			Contact.MobilePhone , Contact.Email ,contact.phone,
			Contact.RecordTypeDeveloperName__c,contact.RecordType.DeveloperName,
			Contact.Engineer_ID__c,SR_Reporter__r.Engineer_ID__c,SR_Reporter__r.RecordType.DeveloperName,
			Employee_ID__c,
			iCare_Group__r.iCare_ID__c,
			iCare_Owner__r.Employee_ID__c
			,CaseNumber
			,HW_Product__r.Offering_Code__c
			,Type
			,Problem_Priority__c
			,Status
			,CreatedDate
			,Is_Overflowed__c
			,Subject 
			,Description
			,Sync_Times__c
			,iCare_Number__c
			,iCare_Sync_Status__c 
			,iCare_Sync_ACK__c
			,Is_Named_Account__c
			,Named_Account_Character__c
			,Named_Account_Level__c
			,Named_Account_Property__c
			 
			 FROM Case WHERE Id = :serviceRequestId limit 1];
			 System.debug('the recorde type developer name:---------' + serviceRequest.RecordType.DeveloperName);
			 if((serviceRequest.iCare_Number__c != null )|| !(('China_Internal_Post_Sales_CCR' == serviceRequest.RecordType.DeveloperName) ||
			 	('China_External_Post_Sales_InternalEngineer_CCR' == serviceRequest.RecordType.DeveloperName) ||
			 	('China_External_Post_Sales_CCR' == serviceRequest.RecordType.DeveloperName) )){//已经成功同步,或者记录类型不符
			 	return serviceRequest;//直接返回,不再继续同步
			 }
			 String title = '';
			 String description = '';
    	 DateTime occurTime = Datetime.now();
    	 Integer beginTime = occurTime.millisecond();
			 do{//在能够发出callout的情况下，在十秒内无限重试
			 		if(Limits.getCallouts() < Limits.getLimitCallouts()){//先做判断,能够发出请求时再做callout
							serviceRequest.Sync_Times__c ++;//每次同步前将同步次数增1
	    		 try{
							serviceRequest = ServiceRequestSyncUtils.sync(serviceRequest);
							updateCase(serviceRequest.id,serviceRequest.Sync_Times__c,serviceRequest.iCare_Number__c,
					  		serviceRequest.iCare_Sync_ACK__c,serviceRequest.iCare_Sync_Status__c);//异步更新case
							return serviceRequest;
						}catch(Exception e){//捕获callout错误
							System.debug('the e:-----------' + e);
							System.debug('the e get message:-----------' + e.getMessage());
							if(e.getMessage().contains(EIPSYNCCONSTANTS.VALIDATIONMSG)){//the request doesnt meet the validation
								List<ApexPages.Message> errorMsgs = validate(serviceRequest);//校验数据的完整性
								if(errorMsgs.size() > 0){//数据不完整
									for(ApexPages.Message m : errorMsgs){
										ApexPages.addMessage(m);
									}
									serviceRequest.iCare_Sync_Status__c =  '数据不完整，请补齐后刷新页面重新同步';
									updateCase(serviceRequest.id,serviceRequest.Sync_Times__c,null,
					  				null,serviceRequest.iCare_Sync_Status__c);
									writeLog(serviceRequest.id,'','数据不完整导致同步失败',
											'新建问题单时异常:'+ System.Label.Sync_Error_Msg + e.getMessage() + ',' + e.getStackTraceString());
					  			return serviceRequest;
								}
								serviceRequest.iCare_Sync_Status__c = System.Label.Sync_Error_Msg + '数据格式错误，请联系管理员';
								updateCase(serviceRequest.id,serviceRequest.Sync_Times__c,null,
					  		null,serviceRequest.iCare_Sync_Status__c);//直接更新当前记录以保存sync time
								writeLog(serviceRequest.id,'',System.Label.Sync_Error_Msg ,
									'新建问题单时异常:'+ System.Label.Sync_Error_Msg + e.getMessage() + ',' + e.getStackTraceString());
									return serviceRequest;
							}else{//其他异常czh
								serviceRequest.iCare_Sync_Status__c =System.Label.Sync_Error_Msg + '同步失败';
								serviceRequest.iCare_Sync_ACK__c = System.Label.Sync_Error_Msg + System.Label.Sync_Error_Description;//e.getMessage();//updated by czh 2013-12-16
								updateCase(serviceRequest.id,serviceRequest.Sync_Times__c,null,
								serviceRequest.iCare_Sync_ACK__c,serviceRequest.iCare_Sync_Status__c);
								//serviceRequest.iCare_Sync_ACK__c = System.Label.Sync_Error_Msg +  System.Label.Sync_Error_Description;//e.getMessage();
								title = e.getMessage();
								description = e.getStackTraceString();
								writeLog(serviceRequest.id,'',System.Label.Sync_Error_Msg + '处理单条建单失败',
									'新建问题单时异常:'+ System.Label.Sync_Error_Msg + title + ';' +  description);
								return serviceRequest;
							}
						}
			 		}
			 }while(Integer.valueOf(Datetime.now().getTime() - beginTime ) <= 20000 );//10*1000ms//sit测试时暂时修改为20s，上线时恢复为10s
			 serviceRequest.iCare_Sync_Status__c = 'EIP/iCare无返回信息导致同步失败';//超过十秒，未成功获取EIP端信息.
			 updateCase(serviceRequest.id,serviceRequest.Sync_Times__c,null,
				  		null,serviceRequest.iCare_Sync_Status__c);//直接更新当前记录以保存sync time
			 if(String.isnotBlank(title)){//exception happens
			 		writeLog(serviceRequest.id,'',System.Label.Sync_Error_Msg ,
					'新建问题单时异常:'+ System.Label.Sync_Error_Msg + title + ';' +  description);
			 }
			 return serviceRequest;
			
	}
	
	
	//接受list id,异步批量发起同步
	@future(callout=true)
	public static void batchSync(List<id> ids){
		List<Case> cases = [SELECT Id, RecordType.DeveloperName,Account.Name , Account.CIS__c , 
			Contact.LastName , Contact.firstname,Record_Type_Name__c,
			Contact.MobilePhone , Contact.Email ,Contact.RecordType.DeveloperName,
			Contact.Engineer_ID__c,SR_Reporter__r.Engineer_ID__c,
			SR_Reporter__r.RecordType.DeveloperName,
			Employee_ID__c,
			iCare_Group__r.iCare_ID__c,
			iCare_Owner__r.Employee_ID__c
			,CaseNumber
			,HW_Product__r.Offering_Code__c
			,Type
			,Problem_Priority__c
			,Status
			,CreatedDate
			,Is_Overflowed__c
			,Subject
			,Description
			,Sync_Times__c
			,iCare_Number__c
			,iCare_Sync_Status__c 
			,iCare_Sync_ACK__c
			,Is_Named_Account__c
			,Named_Account_Character__c
			,Named_Account_Level__c
			,Named_Account_Property__c
			 
			 FROM Case WHERE Id in :ids];
		cases = sync(cases);
		CommonConstant.serviceRequestTriggerShouldRun = false;//禁用所有case 上trigger以缩短处理速度
		database.saveresult[] results = database.update(cases,false);//允许部分更新成功
		List<iCareLog__c> logs = new List<iCareLog__c>();
		for(Integer i=0;i< results.size();i++){
			//system.debug('the result s:--------------' + s.isSuccess());
			database.saveresult s = results.get(i);
			if(!s.isSuccess()){
				writeLog(cases.get(i).id,'',System.Label.Sync_Error_Msg ,
					'批量同步问题单时异常:' +System.Label.Sync_Error_Msg + s.getErrors().get(0).getMessage());
			}
		}
		//insert logs;
	}
	
	//case更新时,批量(目前只同步单条数据)同步到EIP,used in ServiceRequestUpdateSyncHandler
	@future(callout=true)
	public static void updateToEIP(List<Id> caseIds){
		List<Case> cases = [select id,CaseNumber,iCare_Number__c,Is_Overflowed__c,iCare_Group__r.name,iCare_Group__r.iCare_ID__c,
				iCare_Owner__r.Employee_ID__c,Employee_ID__c,subject from Case where id in:caseIds limit 1];//不考虑批量更新的情况
		List<B2bSoapUissue6.tUpdateIcareSR> rqs = new List<B2bSoapUissue6.tUpdateIcareSR>();
		for(Case c : cases){
			B2bSoapUissue6.tUpdateIcareSR req = new B2bSoapUissue6.tUpdateIcareSR();
			req.Salesforce_SR_ID = c.id;
      req.Salesforce_SR_No = c.CaseNumber;
      req.iCare_SR_No = c.iCare_Number__c;
      req.iCare_Group_Name = c.iCare_Group__r.name;
      req.iCare_Group_Id = String.valueOf(c.iCare_Group__r.iCare_ID__c);
      req.iCare_Owner_No = c.iCare_Owner__r.Employee_ID__c == null ? c.Employee_ID__c : c.iCare_Owner__r.Employee_ID__c;
      req.Summary = c.subject;
      req.AUX1 =String.valueOf(c.Is_Overflowed__c);
      system.debug('hewei---------------'+req.AUX1);
      rqs.add(req);
		}
		SoapIssues6.IssueReq_x1_HTTPSPort huaweiCase = 
					new SoapIssues6.IssueReq_x1_HTTPSPort();
		huaweiCase.timeout_x = EIPSYNCCONSTANTS.BATCHTIMEOUTMS;
		try{
			B2bSoapUissue6.tUpdateIcareSRReply[] responses = huaweiCase.UpdateICareSR(rqs,
				EIPSYNCCONSTANTS.UPDATEENDPOINT + cases.get(0).caseNumber + System.now().format('yyyyMMddHHmmss')
				);
					for(B2bSoapUissue6.tUpdateIcareSRReply r : responses){
						if('true' != r.FLAG){//状态不为成功
							//需要根据返回的case number来分别判断状态
							//由于目前只会同步单条数据,直接取第一条即可
							writeLogImmediate(cases.get(0).id,cases.get(0).casenumber,System.Label.Sync_Error_Msg ,
								'sf更新问题单时同步到icare异常:' +System.Label.Sync_Error_Msg +r.DESCRIPTION);
						}
							System.debug('the FLAG:----' + r.FLAG);
							System.debug('the DESCRIPTION:----' + r.DESCRIPTION);
							System.debug('the AUX1:----' + r.AUX1);
							System.debug('the AUX2:----' + r.AUX2);
							System.debug('the AUX3:----' + r.AUX3);
					}	
		}catch(Exception e){//如果出现异常,记录错误信息
			for(Case c : cases){
				writeLogImmediate(cases.get(0).id,cases.get(0).iCare_Number__c,System.Label.Sync_Error_Msg,
					'sf更新问题单时同步到icare异常:' +System.Label.Sync_Error_Msg +e.getMessage());
			}
		}
	}
	
	//实际的批量同步方法
	public static List<Case> sync(List<Case> cases){
		List<SoapIssues6.tIssueRQ> rqs = new List<SoapIssues6.tIssueRQ>();
		Integer rowId = 1;
		for(Case c : cases){
			c.Sync_Times__c = c.Sync_Times__c == null ?  1 : c.Sync_Times__c +1;
			SoapIssues6.tIssueRQ rq = new SoapIssues6.tIssueRQ();
			  rq.ROW_ID = String.valueOf(rowId);
        rq.CUSTOMER_SR_NUMBER = c.CaseNumber;
        rq.ACCOUNT_NAME = c.Account.Name;
        rq.CUSTOMER_ACCOUNT_NUMBER = c.Account.CIS__c;
        rq.CURRENT_CONTACT_NAME = c.Contact.firstname == null ?  c.Contact.LastName : c.Contact.firstname + c.Contact.LastName;
        rq.CURRENT_CONTACT_TELEPHONE = c.Contact.MobilePhone == null ? c.Contact.phone : c.Contact.MobilePhone;
        rq.CURRENT_CONTACT_EMAIL_ADDRESS = c.Contact.Email;
        rq.EXTERNAL_ATTRIBUTE_1 = c.Contact.Engineer_ID__c;
      	 //对于InternalEngineer，取报修人SR Reporter的record type，其他两种联系人必填,取联系人Contact的记录类型
        rq.RESOURCE_TYPE = c.RecordType.DeveloperName.contains('Engineer') ? //根据记录类型做判断
	    				'Internal Engineer' : 'Contact';
        rq.REPORTED_BY = c.SR_Reporter__r.Engineer_ID__c;
        rq.INVENTORY_ITEM_NO = c.HW_Product__r.Offering_Code__c;
        rq.INCIDENT_TYPE_NAME = c.Record_Type_Name__c;
        rq.INCIDENT_SEVERITY_NAM = c.Problem_Priority__c == null ? 'Minor' : c.Problem_Priority__c;
        rq.INCIDENT_STATUS_CODE = c.Status;
        rq.RESPONSIBLE_GROUP_NAME = String.valueOf(c.iCare_Group__r.iCare_ID__c);
        rq.INCIDENT_OWNER_NAME = c.iCare_Owner__r.Employee_ID__c != null ? c.iCare_Owner__r.Employee_ID__c : c.Employee_ID__c;
        rq.REPORT_DATE = c.CreatedDate.format('yyyy-MM-dd\'T\'HH:mm:ss','Asia/Shanghai') + '+08:00';
        rq.SUMMARY = c.Subject;
        rq.INCIDENT_NOTES = c.Description;
        rq.IS_OVERFLOWED = String.valueOf(c.Is_Overflowed__c);
        rq.SYNC_TIMES = String.valueOf(c.Sync_Times__c);
        rq.SR_ORIGIN = 'Phone';
        rq.AUX1 = c.Is_Named_Account__c ? 'NA客户/' + c.Named_Account_Character__c + '/'+ c.Named_Account_Level__c +'/'+ c.Named_Account_Property__c :'非NA客户';
        rq.AUX2 ='';
        rq.AUX3 ='';
        rqs.add(rq);
        rowId ++;
		}
		try{
		SoapIssues6.IssueReq_x1_HTTPSPort huaweiCase = 
					new SoapIssues6.IssueReq_x1_HTTPSPort();
					huaweiCase.timeout_x = EIPSYNCCONSTANTS.BATCHTIMEOUTMS;
			SoapIssues6.tIssueRS[] responses = huaweiCase.Request_Response(rqs,
				EIPSYNCCONSTANTS.BATCHEIPENDPOINT + System.now().format('yyyyMMddHHmmss')
				//'http://requestb.in/1ai7z4n1'//此代码用于调试时查看发出的request请求,需要把地址改为requestb.in所提供的地址
				);
				for(Case c : cases){
					for(SoapIssues6.tIssueRS r : responses){
						if(r.CUSTOMER_SR_NUMBER == c.CaseNumber){
							c.iCare_Number__c = r.ICareSR_Number;
							if(r.ICARE_SYNC_STATUS == 'S'){//同步成功
								c.iCare_Sync_ACK__c = r.ICARE_ACK;
							}else{
							  c.iCare_Sync_ACK__c = System.Label.Sync_Error_Msg +r.ICARE_ACK;
							}
						  c.iCare_Sync_Status__c = r.ICARE_SYNC_STATUS == 'S' ? '同步成功' : '同步失败';
						  
							break;
						}
					}	
			}
		}catch(Exception e){
			System.debug('the exception in List<Case> sync(List<Case> cases) method:------' + e.getmessage());
			List<iCareLog__c> logs = new List<iCareLog__c>();
			for(Case c : cases){
				iCareLog__c log = createLog(c.id,'',System.Label.Sync_Error_Msg ,
					'批量同步问题单时异常:' +System.Label.Sync_Error_Msg +e.getmessage());
				logs.add(log);
					c.iCare_Sync_ACK__c = System.Label.Sync_Error_Msg +e.getmessage();
					c.iCare_Sync_Status__c = System.Label.Sync_Error_Msg +e.getmessage();
			}
			insert logs;
		}
		return cases;
	}
	
	//单条数据的同步方法
	public static Case sync(Case serviceRequest){
	    	System.debug('   CUSTOMER_SR_NUMBER       				----------' + serviceRequest.CaseNumber  + '-----'+ serviceRequest.id);
				System.debug('   ACCOUNT_Name          ----------' + serviceRequest.Account.Name);
				System.debug('   CUSTOMER_ACCOUNT_NUMBER  ----------' + serviceRequest.Account.CIS__c);
				System.debug('   CURRENT_CONTACT_NAME     ----------' + serviceRequest.Contact.firstname + serviceRequest.Contact.LastName);
				System.debug('   CURRENT_CONTACT_TELEPHONE ----------' + serviceRequest.Contact.MobilePhone == null ? serviceRequest.Contact.phone : serviceRequest.Contact.MobilePhone);
				System.debug('   CURRENT_CONTACT_EMAIL_ADDRESS ----------' + serviceRequest.Contact.Email);
				System.debug('   EXTERNAL_ATTRIBUTE_1    ----------' + serviceRequest.Contact.Engineer_ID__c);
				System.debug('   RESOURCE_TYPE ----------' + 	(serviceRequest.RecordType.DeveloperName.contains('Engineer') ? 'Internal Engineer' : 'Contact'));
				System.debug('   REPORT_BY ----------' + serviceRequest.SR_Reporter__r.Engineer_ID__c );
				System.debug('   INVENTORY_ITEM_NO ----------' + serviceRequest.HW_Product__r.Offering_Code__c);
				System.debug('   INCIDENT_TYPE_NAME ----------' + serviceRequest.RecordType.DeveloperName);
				System.debug('   INCIDENT_SEVERITY_NAM ----------' + serviceRequest.Problem_Priority__c == null ? 'Minor' : serviceRequest.Problem_Priority__c);
				System.debug('   INCIDENT_STATUS_CODE ----------' + serviceRequest.Status);
				System.debug('   RESPONSIBLE_GROUP_NAME ----------' + String.valueOf(serviceRequest.iCare_Group__r.iCare_ID__c));
				System.debug('   INCIDENT_OWNER_NAME ----------' + 	serviceRequest.iCare_Owner__r.Employee_ID__c);
				System.debug('   REPORT_DATE ----------' + serviceRequest.CreatedDate.format('yyyy-MM-dd\'T\'HH:mm:ss','Asia/Shanghai') + '+08:00');
				System.debug('   SUMMARY ----------' + serviceRequest.Subject);
				System.debug('   INCIDENT_NOTES ----------' + serviceRequest.Description);
				System.debug('   IS_OVERFLOWED  ----------' + String.valueOf(serviceRequest.Is_Overflowed__c));
				System.debug('   SYNC_TIMES   ----------' + String.valueof(serviceRequest.Sync_Times__c));
				System.debug('   SR_ORIGIN  ----------' + 'Phone');
		SoapIssues6.IssueReq_x1_HTTPSPort huaweiCase = 
					new SoapIssues6.IssueReq_x1_HTTPSPort();
    		huaweiCase.timeout_x= EIPSYNCCONSTANTS.TIMEOUTMS; 
    		B2bSoapSissue6.tIssueRS response = 
    			huaweiCase.SRequest_Response( 
    				serviceRequest.CaseNumber, 
	    			serviceRequest.Account.Name, 
	    			serviceRequest.Account.CIS__c, 
	    			serviceRequest.Contact.firstname == null ?  serviceRequest.Contact.LastName : serviceRequest.Contact.firstname + serviceRequest.Contact.LastName,
	    			serviceRequest.Contact.MobilePhone == null ? serviceRequest.Contact.phone : serviceRequest.Contact.MobilePhone, //如果mobile无值,取phone
	    			serviceRequest.Contact.Email,
	    			serviceRequest.Contact.Engineer_ID__c,
	    			 //对于InternalEngineer，取报修人SR Reporter的record type，其他两种联系人必填,取联系人Contact的记录类型
	    			serviceRequest.RecordType.DeveloperName.contains('Engineer') ? //根据记录类型做判断
	    				'Internal Engineer' : 'Contact',
	    			serviceRequest.SR_Reporter__r.Engineer_ID__c , 
	    			serviceRequest.HW_Product__r.Offering_Code__c, 
	    			serviceRequest.RecordType.DeveloperName, 
	    			serviceRequest.Problem_Priority__c == null ? 'Minor' : serviceRequest.Problem_Priority__c , 
	    			serviceRequest.Status, 
	    			String.valueOf(serviceRequest.iCare_Group__r.iCare_ID__c),
	    			serviceRequest.iCare_Owner__r.Employee_ID__c != null ? serviceRequest.iCare_Owner__r.Employee_ID__c : serviceRequest.Employee_ID__c, 
	    			serviceRequest.CreatedDate.format('yyyy-MM-dd\'T\'HH:mm:ss','Asia/Shanghai') + '+08:00',
	    			serviceRequest.Subject, 
	    			serviceRequest.Description, 
	    			String.valueOf(serviceRequest.Is_Overflowed__c),
	    			String.valueof(serviceRequest.Sync_Times__c), 
	    			'Phone',//问题来源始终为Phone
	    			EIPSYNCCONSTANTS.SINGLEEIPENDPOINT + serviceRequest.id + serviceRequest.Sync_Times__c,
	    			//'http://requestb.in/11139ex1', //此代码用于调试时查看发出的request请求,需要把地址改为requestb.in所提供的地址
	    			serviceRequest.Is_Named_Account__c ? 'NA客户/' + serviceRequest.Named_Account_Character__c + '/'
	    			+ serviceRequest.Named_Account_Level__c +'/'+ serviceRequest.Named_Account_Property__c :'非NA客户', // NA信息
	    			'',''
	    	);
	    		System.debug('the response is:------' + response);
	    		System.debug('r.ICARE_SYNC_STATUS:-----' + response.ICARE_SYNC_STATUS);
	    			serviceRequest.iCare_Number__c = response.ICareSR_Number;
	    				if(response.ICARE_SYNC_STATUS == 'S'){//同步成功
								serviceRequest.iCare_Sync_ACK__c = response.ICARE_ACK;
							}else{
							  serviceRequest.iCare_Sync_ACK__c = System.Label.Sync_Error_Msg + System.Label.Sync_Error_Description;//response.ICARE_ACK; //updated by czh 2013-12-16
							  writeLog(serviceRequest.Id, '', System.Label.Sync_Error_Msg, response.ICARE_ACK);
							}
				  	serviceRequest.iCare_Sync_Status__c = response.ICARE_SYNC_STATUS == 'S' ? '同步成功' : '同步失败';//icare返回值为E或S,E表示失败,S为成功
				  	return serviceRequest; 
	}
	
	//验证icare所需的必填字段有值
	public static List<ApexPages.Message> validate(Case serviceRequest){
		List<ApexPages.Message> msgs = new List<ApexPages.Message>();
		if(serviceRequest.HW_Product__r.Offering_Code__c == null){
				msgs.add(new ApexPages.Message(ApexPages.Severity.FATAL, '产品编码不允许为空'));
		}
		if(serviceRequest.Subject == null){
				msgs.add(new ApexPages.Message(ApexPages.Severity.FATAL, '问题摘要不允许为空'));
		}
		if('China_External_Post_Sales_CCR' == serviceRequest.RecordType.DeveloperName && 
			 serviceRequest.Contact.MobilePhone == null 
			&&  serviceRequest.Contact.Email == null && serviceRequest.Contact.Phone == null){//外部问题单1,客户联系人,联系方式不允许为空
				msgs.add(new ApexPages.Message(ApexPages.Severity.FATAL, '联系人的联系方式不允许全部为空'));
		}
		if('China_External_Post_Sales_InternalEngineer_CCR' == serviceRequest.RecordType.DeveloperName && 
			serviceRequest.SR_Reporter__r.Engineer_ID__c == null){//外部问题单2,REPORTED_BY字段必须有值
				msgs.add(new ApexPages.Message(ApexPages.Severity.FATAL, '报修人工号不允许为空'));
		}
		if('China_Internal_Post_Sales_CCR' == serviceRequest.RecordType.DeveloperName && 
			serviceRequest.Contact.Engineer_ID__c == null){//内部咨询单,EXTERNAL_ATTRIBUTE_1字段必须有值
				msgs.add(new ApexPages.Message(ApexPages.Severity.FATAL, '联系人工号不允许为空'));
		}
		return msgs;
	}
	
	@future
	public static void updateCase(Id caseId,Decimal syncTimes,
		String careNumber,String ack,String status ){
		Case c = new Case(id = caseId,Sync_Times__c = syncTimes,iCare_Number__c = careNumber,
			iCare_Sync_ACK__c = ack,iCare_Sync_Status__c = status);
		CommonConstant.serviceRequestTriggerShouldRun = false;//禁用所有case 上trigger以缩短处理速度
		try{
			update c;
		}catch(Exception e){
			//cant do anything because this is @future method?
		 //maybe send an email to the current user?
		}
	}
	
	@future
	public static void writeLog(Id caseId,String icareNumber,String title,String description){
		writeLogImmediate(caseId,icareNumber,title,description);
	}
	
	public static void writeLogImmediate(Id caseId,String icareNumber,String title,String description){
		iCareLog__c log = createLog(caseId,icareNumber,title,description);
		insert log;
	}
	
	public static iCareLog__c createLog(Id caseId,String icareNumber,String title,String description){
		iCareLog__c log = new iCareLog__c(ServiceRequest__c = caseId, iCareNumber__c = icareNumber,
				ErrorTitle__c = title,ErrorDescription__c = description,
				LogFrom__c = 'Salesforce');
		return log;
	}
	
	/*
	public static void createTopic(){//用于创建streaming api中的topic,只需要在部署后执行一次
		PushTopic pushTopic;
        try{
        	pushTopic = [select Name, query, ApiVersion, NotifyForOperations, NotifyForFields
            	         from PushTopic where name = 'CaseSnyced'];
        }catch(Exception e){}
        
        if (pushTopic == null){
            pushTopic = new PushTopic();
        }
        pushTopic.Name = 'CaseSnyced';
        pushTopic.Query = 'SELECT Id, iCare_Number__c,iCare_Sync_Status__c FROM case ';
        pushTopic.ApiVersion = 29.0;
        
        pushTopic.NotifyForOperationCreate = false;
        pushTopic.NotifyForOperationDelete = false;
        pushTopic.NotifyForOperationUndelete = false;
        pushTopic.NotifyForFields = 'Referenced';
        upsert pushTopic;
	}
	*/
	
}