@isTest
private class Lepus_AccountHandlerTest {
    static testmethod void myUnitTest(){
        User u = [Select Id, FederationIdentifier From User Where UserType = 'Standard' And IsActive = true limit 1];
        Lepus_EIPCalloutServiceTest.setCustomSettings();
        Account acc = Lepus_EIPCalloutServiceTest.createAccount('acc1');
        insert acc;
        
        Lepus_AccountHandler.isFirstRun = true;
        acc.OwnerId = u.Id;
        update acc;
        
        
    }
    
    static testmethod void deleteTest(){
        Lepus_AccountHandler.isFirstRun = false;
        Account acc = Lepus_EIPCalloutServiceTest.createAccount('acc2');
        insert acc;
        Lepus_AccountHandler.isFirstRun = true;
        delete acc;
        
    }
    static Account createAccount(String accName){
        Account acc = new Account();
        acc.Name = accName;
        acc.RecordTypeId = CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE;
        acc.City_Huawei_China__c = '北京市';
        acc.OwnerId = UserInfo.getUserId();
        
        return acc;
    }
}