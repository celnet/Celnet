/*
*@Purpose:填充SMS_Notification_Message__c和Asset__c字段
*@Author:Brain Zhang
*Create Date：2013-9
*/
public without sharing class WorkOrderFieldPopulater implements Triggers.Handler {
	public void handle(){//before insert,before update
			Map<Id,Work_Order__c> wo_contactIdMap=new Map<Id,Work_Order__c>();//id=contact.id
      Map<Id,Work_Order__c> wo_accountIdMap=new Map<Id,Work_Order__c>();//id=account.id
      Map<Id,Work_Order__c> wo_iacIdMap=new Map<Id,Work_Order__c>();//<id=wo.Barcode__c>
      for(Work_Order__c wo:(List<Work_Order__c>)Trigger.new){
        wo_contactIdMap.put(wo.Contact_Person__c,wo);
        wo_accountIdMap.put(wo.Account_Name__c,wo); 
        wo_iacIdMap.put(wo.Barcode__c,wo);
      }
      Map<Id,Contact> contactMap=new Map<Id,Contact>([select id,name,phone,MobilePhone 
            from Contact 
            where id in :wo_contactIdMap.keySet()]);
      Map<Id,Account> accountMap=new Map<Id,Account>([select id,name from Account where id in :wo_accountIdMap.keySet()]);
      Map<Id,Installed_Asset__c> assetMap=new Map<Id,Installed_Asset__c>([Select i.SubRegion__c, i.Street__c, i.Region__c, i.Other_City__c, i.Id, i.City__c 
            From Installed_Asset__c i
            where i.Id in :wo_iacIdMap.keySet()]);
      
      for(Work_Order__c wo:(List<Work_Order__c>)Trigger.new){
      	if(assetMap.get(wo.Barcode__c) != null){
      		Installed_Asset__c ia = assetMap.get(wo.Barcode__c);
      		String combineAddress='';
            if(ia.Region__c!=null){
            	combineAddress+=ia.Region__c +' ';
            }
            if(ia.SubRegion__c!=null){
            	combineAddress+=ia.SubRegion__c +' ';
            }
            if(ia.City__c!=null){
            	combineAddress+=ia.City__c +' ';
            }
            if(ia.Other_City__c!=null){
            	combineAddress+=ia.Other_City__c +' ';
            }
            if(ia.Street__c!=null){
            	combineAddress+=ia.Street__c;
            }
            wo.Asset__c=combineAddress;//9.设置Asset__c字段
           }
      	
      	 if(wo.SMS_Notification_Message__c!=null){
      	 	continue;
      	 }
      	Account acc=accountMap.get(wo.Account_Name__c);
            Contact c=contactMap.get(wo.Contact_Person__c);
            String accName=acc ==null?' ':acc.Name;
            String cName='',cMobilePhone='',cPhone='', ContractNo='';
            ContractNo=wo.Contract_No_ESPM__c;
            if(c!=null){            	
                if(c.Name!=null)cName=c.Name;
                if(c.MobilePhone!=null)cMobilePhone=','+c.MobilePhone;
                if(c.Phone!=null)cPhone=','+ c.Phone;
            }
            //8. SMS_Notification_Message__c
      	wo.SMS_Notification_Message__c='ContractNo:'+ContractNo+','+accName+','+cName+cMobilePhone +cPhone;
      }
        
	}
}