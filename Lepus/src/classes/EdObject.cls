public class EdObject {
	private static Map<String, Schema.SObjectType> globalDescribe=null; 
	private static Map<String,String> sobjectPrefixTypeMap=new Map<String,String>(); 
	private Schema.SObjectType currObject; 
	private String objectName;
	private Map<String,Schema.SObjectField> currObjectFieldsMap;
	private List<Schema.SObjectField> currObjectFieldsList; 
	
	static{
		globalDescribe=Schema.getGlobalDescribe() ;
		for(String sotkey:globalDescribe.keySet()) {
		  Schema.DescribeSObjectResult dso = globalDescribe.get(sotkey).getDescribe();
		  String prefix=dso.getKeyPrefix();
		  if(prefix!=null)sobjectPrefixTypeMap.put(prefix,dso.getName());
		}
	} 
	
	private EdObject(){
	}
	 
	public EdObject(String objectName){
		this.objectName=objectName;
		this.currObject=globalDescribe.get(objectName);
		initFields(); 
	}

	public Map<String,Schema.SObjectType> getGlobalDescribe(){
		return globalDescribe;
	}
	 
	public List<Schema.SObjectType> getAllObjectTypes(){
		return getGlobalDescribe().values();
	}
	 
	public List<Schema.SObjectField> getCurrObjectFieldList(){
		return currObjectFieldsList;
	} 
	 
	public void initFields(){   
		Schema.DescribeSObjectResult dsr=currObject.getDescribe() ;
		currObjectFieldsMap = dsr.fields.getMap();
    	currObjectFieldsList = currObjectFieldsMap.values() ;
	}
	 
	public List<Schema.ChildRelationship> getObjChildRelations(){
		Schema.DescribeSObjectResult dsr=currObject.getDescribe() ;
		return dsr.getChildRelationships();
	} 
	 
	public String getFieldTypeToString(String fldName){
		String s='';
		try{
			s=''+getFieldType(fldName);
		}catch(Exception e){s='\n查找字段 '+fldName+' 不存在,请先确认\n\n'+e;}
		return s;
	}
	 
	public DisplayType getFieldType(String fldName){ 
		Schema.DescribeFieldResult dfr=getFieldResult(fldName);
		return dfr.getType();
	}
	 
	public Schema.SObjectField getField(String fldName){
		return currObjectFieldsMap.get(fldName); 
	}
          
	public Schema.DescribeFieldResult getFieldResult(String fldName){
		return getField(fldName)==null?null:getField(fldName).getDescribe(); 
	}
	 
	public static String getSObjectTypeNameFromId(String ID_Str){
		String s=ID_Str.substring(0,3);
		if(!sobjectPrefixTypeMap.containsKey(s))return null;
		return sobjectPrefixTypeMap.get(s); 
	}
	
	public static testMethod void currClsTest(){
		//Opportunity opp=[select id,name from Opportunity limit 1];
		Account acc = UtilUnitTest.createAccount();
		acc.RecordTypeId = CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE;
    	acc.Region_HW__c = 'testr';
		acc.Representative_Office__c = 'testro';
		acc.Country_HW__c = 'testch';
		acc.Country_Code_HW__c = 'testcch';
		acc.Industry__c = 'testi';
		acc.Customer_Group__c = 'testcg';
		acc.Sub_industry_HW__c = 'testsih';
		acc.Industry = 'testi';
		acc.City_Huawei_China__c = 'testchc';
		acc.Customer_Group_Code__c = 'testcgc';
		acc.Province__c = 'testp';
		update acc;
		Opportunity opp = new Opportunity(accountid = acc.id,recordtypeid = CONSTANTS.CHINAOPPORTUNITYRECORDTYPE,closedate = Date.today(),
				name = 'testOpp' ,stagename = 'test');
				opp.Opportunity_type__c='Direct Sales';//it is added on 2013/05/25 
		insert opp;
		String oppId=opp.id;
		EdObject eo=new EdObject('Opportunity');
		eo.getCurrObjectFieldList();
		eo.getAllObjectTypes();
		eo.getField('id');
		eo.getFieldResult('id');
		eo.getFieldType('id');
		eo.getFieldTypeToString('id');
		eo.getGlobalDescribe();
		eo.getObjChildRelations();
		EdObject.getSObjectTypeNameFromId(oppId);
	}
}