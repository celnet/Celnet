public class RedirectUpdateCase {
    
    public String currId{
        get{return currId;}
        set{currId=value;}
    } 
    public Boolean haveError{
        get{return haveError;}
        set{haveError=value;}
    }
    /**
    public RedirectUpdateCase(ApexPages.StandardController controller) {
        
    }
    */
    public RedirectUpdateCase() {
        currId= ApexPages.currentPage().getParameters().get('id');
    }
    public void init(){
        System.debug('/////0、进入'); 
        haveError=false;
        String usrId=UserInfo.getUserId();
        Id profileId=UserInfo.getProfileId();  
        /**
        User usr=[Select u.UserRoleId, u.Id ,u.profileId
            From User u 
            where u.id=:usrId];
        if(usr==null || usr.profileId==null)return;
        */  
        Profile pf=[select id,Name from Profile where id = :profileId];
        //if(pf==null)return;
        if( pf.Name.indexOf('HRE')<0 && pf.Name.indexOf('HPE')<0 && pf.Name.indexOf('CSE')<0
            && pf.Name.indexOf('PSE')<0){ 
            haveError=true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '无法开始工作,简档不符 (Just for CSE or PSE profile)')); 
            return;
        }
        System.debug('/////1、验证HRE或HPE'+currId);
        /**
        if(usr.UserRoleId==null)return;
        UserRole ur=[Select ut.Name, ut.Id From UserRole ut where ut.Id=:usr.UserRoleId];
        */
        // check the current user's Role name, -->new requiement as: check current user's profile include 'HRE
        // if it includes HRE, then update HRE Start Time, 
        // if it includes HPE, then update HPE Start Time, 
        // ...
        System.debug('///////////////////////////////////////////'+currId);     
        List<Case> csList=[Select c.id
            ,c.Owner.Id
            ,c.Owner.ProfileId
            ,c.HPE_Start_Time__c
            ,c.HRE_Start_Time__c 
            ,c.CSE_Start_Time__c
            ,c.Actual_Response_Date_Time__c
            ,c.HRE_Actual_Time__c
            ,c.HPE_Actual_Time__c
            ,CSE_Actual_Start_Time__c
            ,PSE_Actual_Start_Time__c           
            from Case c 
            where c.id=:currId];
        
        if(csList.size()>0){Case cs = csList[0];
            if(cs==null)return;
            System.debug('/////2、Case 存在'+currId);
            String csOwner=cs.Owner.Id; 
            if(!usrId.equals(csOwner)){haveError=true;  
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR
                    , '服务请求所有人和当前操作人不一致(Just can be started by the owner)')); 
                return;
            }
            System.debug('/////3、Owner 符合要求');
            /**
            Profile ownerPf=[select id,Name from Profile where id = :cs.Owner.ProfileId];
            if(ownerPf.Name.indexOf('CCR')>0){
                haveError=true; 
                myMsg=new ApexPages.Message(ApexPages.Severity.ERROR
                    , '旧的所有人简档不为CCR');
                ApexPages.addMessage(myMsg); 
                return;
            }
            */
            if(cs.Actual_Response_Date_Time__c==null){
                cs.Actual_Response_Date_Time__c=System.now();
                cs.status='Handling';
            }
        
            //原来点HRE Start Working同时更新Actual Response Time和HRE Actual Time，现取消更新HRE Actual Time，
            //改为更新Actual Response Time和HRE Start Time
            //https://cs5.salesforce.com/a0aO0000000CaxT
            //if(cs.HRE_Actual_Time__c==null)cs.HRE_Actual_Time__c=System.now(); 
            //if(cs.Actual_Response_Date_Time__c!=null)cs.Actual_Response_Date_Time__c=System.now();
            
            if(pf.Name.indexOf('HRE')>0||pf.Name.indexOf('CSE')>0){
                if(cs.CSE_Actual_Start_Time__c==null){//记录CSE实际开始时间，即点start working按钮时间
                    cs.CSE_Actual_Start_Time__c=System.now();
                    //cs.status='Handling';
                }
                
            }
            if(pf.Name.indexOf('HPE')>0||pf.Name.indexOf('PSE')>0){
                //记录PSE实际开始时间，即PSE点start working按钮时间
                    if(cs.PSE_Actual_Start_Time__c==null){                
                    cs.PSE_Actual_Start_Time__c=System.now();
                 }                    
            
            }
            try{
                update(cs);
                System.debug('/////4、Case 更新成功');
            }catch(Exception e){
                haveError=true;  
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '失败:记录中数据缺失或必要数据不完整(Incomplete data)')); 
                ApexPages.addMessages(e);
                System.debug('/////4、Case 更新失败');
            }
            System.debug('/////5、Case 修改结束');
            }
        }
        
        public String getId(){
            return currId;
        } 

}