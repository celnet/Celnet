/*
 * Author: Sunny.sun@celnet.com.cn
 * Date: 2014-8-27
 * Description: Lepus队列处理batch
 */
global class Lepus_QueueBatch implements Database.Batchable<Lepus_Queue__c>,Database.AllowsCallouts{
	global List<Lepus_Queue__c> start(Database.BatchableContext BC){
		List<Lepus_Queue__c> list_Queue = [Select Id,Action__c,ObjectType__c,Opportunity__c,RecordId__c,Account__c, Global_ID__c,
				Contact__c,SyncTime__c,SyncType__c,Lead__c,User__c From Lepus_Queue__c order by CreatedDate];
		return list_Queue;
   	}
	global void execute(Database.BatchableContext BC, List<Lepus_Queue__c> scope){
		for(Lepus_Queue__c queue : (List<Lepus_Queue__c>)scope){
			Lepus_QueueService service = new Lepus_QueueService();
			service.QueueHandle(queue);
		}
    }

	global void finish(Database.BatchableContext BC){
		//检查Lepus Queue中是否有未处理记录
		if(Lepus_QueueManager.HaveUntreated()){
			//启用新的Queue Batch
			Lepus_QueueBatch qb = new Lepus_QueueBatch();
			database.executeBatch(qb, 1);
		} else {
			//Lepus Queue中没有未处理记录后，启用错误处理的Batch
			Lepus_FailureReSyncScheduleJob fb = new Lepus_FailureReSyncScheduleJob();
			database.executeBatch(fb,1);
		}
   	}
}