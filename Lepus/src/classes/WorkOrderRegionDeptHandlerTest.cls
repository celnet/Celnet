@isTest
private class WorkOrderRegionDeptHandlerTest {

    static testMethod void singleTest() {
    	Account acc = UtilUnitTest.createAccount();
    	acc.Region_Dept__c = 'test';
    	update acc;
    	Case sr = UtilUnitTest.createChinaServiceRequest();
    	Work_Order__c wo = UtilUnitTest.createOnSiteWorkOrder(acc,sr);
    	wo = [select id,Region_Dept__c from Work_Order__c where id =:wo.id];
    	System.assertEquals('test',wo.Region_Dept__c);
    }
}