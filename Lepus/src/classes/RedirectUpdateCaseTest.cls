/*
* @Purpose : 
* Refactor the test method in RedirectUpdateCase to a separate test class
*
*
* @Author : Brain Zhang
* @Date :       2013-11
*
*
*/

@isTest
private class RedirectUpdateCaseTest {
	static testMethod void theTest(){
        EdObject.currClsTest();
        EdField.clsTest();
        EdEmail.clsTest();
        //简档更为HRE简档     (CSS) HQ Response Engineer(HRE)
        Profile pf=[select id,Name from Profile where ( Name like '%HQ Response Engineer(HRE)%' or  Name like '%HQ Response Engineer(CSE)%' ) limit 1];
        User u = [select Id,profileId,IsActive from User where IsActive=true and profileId = :pf.id limit 1];
         
        System.debug('/////User ID:'+u.id);
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId());
        Case c = UtilUnitTest.createChinaServiceRequest();
            Case cs=[Select c.id
            ,c.Owner.Id
            ,c.Owner.ProfileId
            ,c.HPE_Start_Time__c
            ,c.HRE_Start_Time__c 
            ,c.CSE_Start_Time__c
            ,c.Actual_Response_Date_Time__c
            ,c.HRE_Actual_Time__c
            ,c.HPE_Actual_Time__c
             from Case c limit 1];                  
        //https://c.cs5.visual.force.com/apex/VPUpdateCase?scontrolCaching=1&id=500O0000000lR83
        System.runAs(u){
            PageReference pr=new PageReference('/apex/VPUpdateCase?scontrolCaching=1&id='+cs.id);
            pr.setRedirect(false);
            Test.setCurrentPage(pr);
            RedirectUpdateCase ruc = new RedirectUpdateCase();
            ruc.getId();
            Boolean b=ruc.haveError;
            ruc.currId=cs.id;
            String iid=ruc.currId;
        }
        cs.Actual_Response_Date_Time__c=null; 
        update(cs);
        System.runAs(u){
            PageReference pr=new PageReference('/apex/VPUpdateCase?scontrolCaching=1&id='+cs.id);
            pr.setRedirect(false);
            Test.setCurrentPage(pr);
            RedirectUpdateCase ruc = new RedirectUpdateCase();
            Boolean b=ruc.haveError;
            ruc.currId=cs.id;
            String iid=ruc.currId;
        }
        ApexPages.currentPage().getParameters().put('id',cs.id);
        RedirectUpdateCase ruc=null;
        //ApexPages.Standardcontroller ctl=null;
        System.runAs(u){
            //ctl=new ApexPages.StandardController(cs);
            ruc=new RedirectUpdateCase();
            ApexPages.currentPage().getParameters().put('id',cs.id);
            ruc.init();
            ruc.getId();
            Boolean b=ruc.haveError;
        }
            
        ApexPages.currentPage().getParameters().put('id',cs.id);
            cs.Actual_Response_Date_Time__c=null; 
        System.runAs(u){
            //ctl=new ApexPages.StandardController(cs);
            ruc=new RedirectUpdateCase();
            ApexPages.currentPage().getParameters().put('id',cs.id);
            Boolean b=ruc.haveError;
            ruc.init();
            ruc.getId();
        }        
        }   
}