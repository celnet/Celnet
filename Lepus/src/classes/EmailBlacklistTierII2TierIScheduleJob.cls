/*
* @Purpose : this schedule job is for emailing to the Region Admin and EGB Sales Management Manager
* when there new Account records switch from Tier II to Tier I.
*
* @Author : Jen Peng
* @Date : 		2013-5-15
*/
global class EmailBlacklistTierII2TierIScheduleJob implements Schedulable{
	public Set<String> regionAdminEmails{get;set;}//for no duplicate regionAdminEmails
	public Set<String> ebgSalesMgrEmails{get;set;}//for no duplicate ebgSalesMgrEmails
	public Set<String> regions{get;set;}//for no duplicate regions
	public List<Account> redAccounts{get;set;} 
	public List<Blacklist_Check_History__c> bchs{get;set;}
	public List<User> users{get;set;}
	public List<Messaging.SendEmailResult> results{get;set;} 
	
	public EmailBlacklistTierII2TierIScheduleJob(){
		try{
			regionAdminEmails=new Set<String>();
			ebgSalesMgrEmails=new Set<String>();
			regions=new Set<String>();
			redAccounts=new List<Account>();
			bchs=new List<Blacklist_Check_History__c>();
			users=new List<User>();
			Set<Id> accIds = new Set<Id>();
			bchs=[Select Account__c,Blacklist_Status__c From Blacklist_Check_History__c 
			      where status_date__c>=TODAY and status_date__c<TOMORROW and Blacklist_Status__c='Red'
			      and Last_status__c='Red' and blacklist_type__c='Tier I'];
			if(!bchs.isEmpty()){
				for(Blacklist_Check_History__c b:bchs){
					accIds.add(b.Account__c);
				}
				bchs.clear();
				redAccounts=[Select id,Region_HW__c from Account where id in:accIds];
				for(Account a:redAccounts){
					regions.add(a.Region_HW__c);//get no duplicate regions
				}
				redAccounts.clear();
				String query = 'Select Id,Name,Role_Name__c,Email,IsActive From User where';
				String cons='';
				for(String r:regions){
					cons+=' Role_Name__c = \'HW '+r+' Region Admin\' or Role_Name__c = \'HW '+r+' Admin\' or';
				}
				regions.clear();
				cons=cons.substring(0,cons.length()-3);
				query+=cons;
				users=Database.query(query);
				for(User u:users){
					if(u.IsActive){
						regionAdminEmails.add(u.Email);//get no duplicate regionAdminEmails
					}
				}
				users.clear();
				if(!regionAdminEmails.isEmpty()){
					String query1 = 'Select Id,Name,Role_Name__c,Email,IsActive From User where Role_Name__c=\'HW EBG Sales Management Manager\'';
					users=Database.query(query1);
					for(User u:users){
						if(u.IsActive){
							ebgSalesMgrEmails.add(u.Email);//get no duplicate ebgSalesMgrEmails
						}
					}
					users.clear();
				}
				//system.debug('regionAdminEmails is here========================='+regionAdminEmails);
				//system.debug('ebgSalesMgrEmails is here========================='+ebgSalesMgrEmails);
			}      
		}catch(Exception e){
			System.debug('==============opooos~!');
		}
			
	}
	
	public void sendEmail(){
		try{
			Integer n = ebgSalesMgrEmails.size()+regionAdminEmails.size();
			List<Report> reports = [Select r.Name, r.Id From Report r where r.Name='Blacklist Account From Tier II to Tier I'];
			if(n>0){
				Messaging.reserveSingleEmailCapacity(n);
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
				List<string> toAddress = new List<string>();
				if(regionAdminEmails.size()>0){
					for(String s:regionAdminEmails){
						toAddress.add(s);
					}
				}
				if(ebgSalesMgrEmails.size()>0){
					for(String s:ebgSalesMgrEmails){
						toAddress.add(s);
					}
				}
				mail.setToAddresses(toAddress);
				mail.setSenderDisplayName('Important No-reply email from Salesforce System');
				mail.setSubject('There some accounts are  changed from Tier II to Tier I and locked now!');
				String strbody = '<p>Dear Mr/Ms:</p>';
				strbody += '<p>There some accounts are  changed from Tier II to Tier I and locked now!Please check following Report Link</p>';
				strbody += system.Url.getSalesforceBaseUrl().toExternalForm()+'/'+reports.get(0).id;
				mail.setHtmlBody(strbody);
				results=Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
				system.debug('results============================'+results);
			}
			
		}catch(Exception e){
			System.debug('=======================Email opooos~!');
		}
	}
	global void execute(SchedulableContext SC){
		EmailBlacklistTierII2TierIScheduleJob e = new EmailBlacklistTierII2TierIScheduleJob();
		e.sendEmail();
	}
}