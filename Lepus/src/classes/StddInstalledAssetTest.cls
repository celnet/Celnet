@isTest
private class StddInstalledAssetTest {
    
    
	static testMethod void currClsTest(){
        Account acc = UtilUnitTest.createAccount();
        CSS_Contract__c c = UtilUnitTest.createContract(acc);
        Installed_Asset__c ia = UtilUnitTest.createAsset(c);
        RecordType rt = [select id from RecordType where SobjectType = 'Installed_Asset__c' and name like 'HW-(Int CSS)%' limit 1];
        ia.Account_Name__c = acc.id;
        ia.RecordTypeId = rt.id;
        update ia;
        Test.startTest();
        ApexPages.currentPage().getParameters().put('id', ia.id);
        
        ApexPages.StandardController controller = new ApexPages.StandardController(ia);
        StddInstalledAsset sic = new StddInstalledAsset(controller);
        sic.init();
        sic.getIciUrl();
        List<SFP__c> SFPs = sic.SFPList;
        /*
        sic.cloneIA();
        sic.editIA();
        sic.deleteIA();
        */
        Test.stopTest();
    }
    
}