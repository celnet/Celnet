/**
 * 
 */
@isTest
private class W3Task_SyncUtil_ScheduleTest {

    static testMethod void myUnitTest() {
        Account acc = Lepus_EIPCalloutServiceTest.createAccount('acc1');
        acc.Inactive_NA__c = false;
        acc.Is_Named_Account__c = true;
        //acc.HQ_Parent_Account_Name__c = acc.Name;
        acc.RecordTypeId = CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE;
        insert acc;
        Account acc2 = Lepus_EIPCalloutServiceTest.createAccount('acc2');
        insert acc2;
        
        Account_Link_Request__c alr = new Account_Link_Request__c();
        alr.parent__c = acc.Id;
        alr.child__c = acc2.Id;
        alr.Approver__c = userinfo.getUserId();
        insert alr;
        
        Approval.ProcessSubmitRequest psr = new Approval.ProcessSubmitRequest();
        psr.setObjectId(alr.Id);
        Approval.ProcessResult result  = Approval.process(psr);
        Set<ID> set_id = new Set<ID>();
        set_id.add(result.getInstanceId());
        
        
        ProcessInstance pi = [Select p.TargetObjectId, p.Status, p.LastModifiedDate, p.Id, p.CreatedDate, 
       	(Select Id, ProcessInstanceId, OriginalActorId, ActorId, IsDeleted, CreatedDate, CreatedById, SystemModstamp From Workitems),
       	 (Select Id, ProcessInstanceId, StepStatus, OriginalActorId, ActorId, Comments, StepNodeId, CreatedDate, CreatedById, SystemModstamp From Steps  Order by SystemModstamp)
       	  From ProcessInstance p where Id in:set_id];
       	system.Test.startTest();
       	W3Task_SyncUtil_Schedule.W3Task_Util(pi.Workitems, pi.Steps, set_id);
       	system.Test.stopTest();
    }
}