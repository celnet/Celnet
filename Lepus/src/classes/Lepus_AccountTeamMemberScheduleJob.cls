/**
 * Author: steven.ke@celnet.com.cn
 * Date: 2014-8-30
 * Description: 同步客户团队成员到EIP的Job, 在Faiulure
 */
global class Lepus_AccountTeamMemberScheduleJob {
	global static void syncAccountTeamMembers(){
		boolean syncTeamMember = (Lepus_Data_Sync_Controller__c.getInstance('客户团队') != null) && 
											Lepus_Data_Sync_Controller__c.getInstance('客户团队').IsSync__c;
		
		if(!syncTeamMember)
		return;
		
		List<Id> accountIds = new List<Id>();
		Map<Id, String> idGlobalIdMap = new Map<Id, String>();
		
		for(AccountTeamMember atm : [Select Id, AccountId, Account.Global_ID__c From AccountTeamMember Where (Account.RecordTypeId =: CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE 
												Or Account.RecordTypeId =: CONSTANTS.HUAWEIOVERSEACUSTOMERRECORDTYPE) 
											And LastModifiedDate >: Datetime.now().addminutes(-16)]){
			accountIds.add(atm.AccountId);
			idGlobalIdMap.put(atm.AccountId, atm.Account.Global_ID__c);
		}
		
		if(accountIds.size() == 1 && Limits.getFutureCalls() < Limits.getLimitFutureCalls()){
			Lepus_FutureCallout.syncTeamMember(accountIds, 'account', '');
		} else if(accountIds.size() > 0){
			Lepus_SyncUtil.initQueue(accountIds, idGlobalIdMap, '', '团队成员同步', datetime.now());
		}
	}
}