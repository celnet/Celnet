/*
*@Purpose:同名vf的controller,调用ServiceRequestSyncUtils实现批量同步
*@Author:Brain Zhang
*Create Date：2013-9
*/
public without sharing class ServiceRequestBatchSyncToEIP {
	public List<wrapper> casesToSync {
		get{
			if(casesToSync == null){
	     casesToSync = new List<Wrapper>();
	     for(Case c : (List<Case>)setController.getRecords()){
	        casesToSync.add(new Wrapper(c));
	     }
			}
			return casesToSync;
		}
		set;	
	}
	
	public Boolean isSyncing {get;private set;}	
	private ApexPages.StandardSetController setController;
	private final integer PAGESIZE = 30;
	
	
	public ServiceRequestBatchSyncToEIP(){
		isSyncing = false;
		String queryString = 'select id,CaseNumber,Sync_Times__c,ownerid ,CreatedDate,Account.CIS__c,RecordType.DeveloperName ,'+
			'HW_Product__r.Offering_Code__c,Contact.MobilePhone,Contact.Email,Contact.Phone,Contact.Engineer_ID__c,SR_Reporter__r.Engineer_ID__c,'+
			'Subject,Origin,iCare_Owner__c ,iCare_Group__c  from Case '+ //need to exclude the hw-int css type
			'where iCare_Number__c = null and (RecordType.DeveloperName = \'China_Internal_Post_Sales_CCR\' or '+
			'	RecordType.DeveloperName = \'China_External_Post_Sales_InternalEngineer_CCR\' or '+
			'	RecordType.DeveloperName = \'China_External_Post_Sales_CCR\') and status != \'Cancelled\' and status != \'待回访\' limit 10000';
		 setController =  new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
     setController.setPageSize(PAGESIZE);
      hasPrevious = setController.getHasPrevious();
      hasNext = setController.getHasNext();
      totalAvailable = setController.getResultSize();
      totalPage = Integer.valueOf(Math.ceil(setController.getResultSize()/Decimal.valueOf(PAGESIZE)));
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 
			'共有' + totalAvailable +'条数据等待同步' ));
	}
	
	public PageReference sync(){//call future method,asynchronous
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 
			'已经发起同步,请等待同步结果.如果长时间无状态更新,请刷新页面查看.刷新或关闭页面不会影响同步.'));
		isSyncing = true;
		List<id> ids = new List<id>();
		for(wrapper w : casesToSync){
			if(w.isSelected){
				w.isSyncing = true;
				ids.add(w.c.id);
			}
		}
		ServiceRequestSyncUtils.batchSync(ids);
		return null;
	}
	
	
	public Integer accepted{get;private set;} //display in the today's summary section
public Integer rejected{get;private set;} //display in the today's summary section
public Integer totalAvailable {get;private set;}//display in the today's summary section
public Boolean hasPrevious {get;private set;}//used in paging 
public Boolean hasNext {get;private set;}//used in pageing
public Integer totalPage {get;private set;}//the total number of pages 
public Integer  currentPage{
		get{
			return setController.getPageNumber();
		}
		private set;
	}

// returns the first page of records
 	public PageReference first() {
 		setController.first();
 		hasPrevious = false;
 		hasNext = setController.getHasNext();
 		casesToSync = null;
    return null; 
 	}
 
 	// returns the last page of records
 	public PageReference last() {
 		setController.last();
 		hasPrevious = setController.getHasPrevious();
 		hasNext = false;
 		casesToSync = null;
    return null; 
 	}
 
 	// returns the previous page of records
 	public PageReference previous() {
 		setController.previous();
 		hasPrevious = setController.getHasPrevious();
 		hasNext = setController.getHasNext();
 		casesToSync = null;
    return null; 
 	}
 
 	// returns the next page of records
 	public PageReference next() {
 		setController.next(); 
 		hasPrevious = setController.getHasPrevious();
 		hasNext = setController.getHasNext();
 		casesToSync = null;
    return null;
 	}
	
	public class wrapper{
		public Boolean isSelected {get;set;}
		public Boolean isSyncing {get;set;}
		public String msg {get;set;}
		public Case c {get;set;}
		public wrapper(Case c){
			this.c = c;
			this.isSelected = true;
			this.msg = '';
			List<ApexPages.Message> msgs = ServiceRequestSyncUtils.validate(c);
			if(msgs.size() > 0){
				this.isSelected = false;
				for(ApexPages.Message m : msgs){
					this.msg += m.getSummary() + ';';
				}
				this.msg = this.msg.substring(0,this.msg.length()-1);
			}
			this.isSyncing = false;
		}
	}
}