/**
 * @Purpose : Lepus 项目 CallOut使用
 * @Author : 孙达
 * @Date : 2014-07-29
 **/
public without sharing class Lepus_UtilCallOut {
    /**
    业务数据推送
    **/
    //根据记录和对象类型，拼接传递给天兔的Json串
    public static String concatenateBusinessDataJson(sObject obj , String objType){
        String sJson = '';
        String sOppProductsJson;
        String winComFieldJson;
        List<Lepus_EIP_FieldMapping.fieldMap> list_fieldMap ;
        //获取字段mapping关系
        if(objType.toLowerCase().equals('opportunity')){
            list_fieldMap = Lepus_EIP_FieldMapping.getOppFieldMap();
            //机会点中标厂商字段处理
            winComFieldJson = concatenateOppWinComField((Opportunity)obj);
            //海外机会点产品处理
            sOppProductsJson = concatenateOppProducts((opportunity)obj);
        }else if(objType.toLowerCase().equals('account')){
            list_fieldMap = Lepus_EIP_FieldMapping.getAccFieldMap();
        }else if(objType.toLowerCase().equals('contact')){
            list_fieldMap = Lepus_EIP_FieldMapping.getContactFieldMap();
        }else if(objType.toLowerCase().equals('lead')){
            list_fieldMap = Lepus_EIP_FieldMapping.getLeadFieldMap();
        }
        Map<String , String> map_FieldType = assignDateOrDatetimeMapFieldType(objType);
        //字段Json拼接
        for(Lepus_EIP_FieldMapping.fieldMap fMap : list_fieldMap){
            //筛选拼接的字段
            if(!fMap.SFField.contains('.')){
                String jKey = fMap.EIPField;
                String jValue;
                
                if(obj.get(fMap.SFField) != null){
                    jValue = String.valueOf(obj.get(fMap.SFField));
                    // 处理日期时间和日期字段格式
                    if(map_FieldType.containsKey(fMap.SFField.toLowerCase())){
                        jValue = formatDateField(jValue , map_FieldType.get(fMap.SFField.toLowerCase()));
                    }
                }
                
                sJson += concatenateJsonKeyValue(jKey, jValue);
            }
        }
        //关系型特殊字段处理(account.name,owner.name等)
        String rJson = concatenateRelationshipFields(obj,objType);
        sJson += rJson;
        //拼接中标厂商字段
        sJson += (winComFieldJson==null?'':winComFieldJson);
        //拼接海外机会点产品字段
        sJson += (sOppProductsJson==null?'':sOppProductsJson);
        
        sJson = '{"resultCode":"001","resultMsg":{'+sJson.substring(0, sJson.length()-1 )+'},"errorMsg":""}';
        
        return sJson;
    }
    
    // 拼接中标厂商字段值
    private static String concatenateOppWinComField(Opportunity opp){
        String winComJson = '';
        if(opp.WinCom_Cisco__c){
            winComJson+='"思科",';
        }
        if(opp.WinCom_EMC__c){
            winComJson+='"EMC",';
        }
        if(opp.WinCom_H3C__c){
            winComJson+='"H3C",';
        }
        if(opp.WinCom_HongShan__c){
            winComJson+='"宏杉",';
        }
        if(opp.WinCom_Polycom__c){
            winComJson+='"Polycom",';
        }
        if(opp.WinCom_ZhongXin__c){
            winComJson+='"中兴",';
        }
        if(opp.WinCom_DELL__c){
            winComJson+='"戴尔",';
        }
        if(opp.WinCom_HP__c){
            winComJson+='"惠普",';
        }
        if(winComJson.length()>0){
            winComJson = winComJson.substring(0, winComJson.length()-1 );
        }
        winComJson= '"winningDealVendor":['+ winComJson + '],';
        return winComJson;
    }
    
    // 拼接关系字段
    private static String concatenateRelationshipFields(sObject obj ,  String objType){
        String rFieldJson = '';
        if(objType.toLowerCase().equals('opportunity')){//机会点特殊字段
            Opportunity opp = (opportunity)obj;
            rFieldJson += concatenateJsonKeyValue('hwOwnerLoginId',opp.Owner.W3ACCOUNT__c);
            rFieldJson += concatenateJsonKeyValue('hwOwnerName',opp.Owner.FullName__c);
            rFieldJson += concatenateJsonKeyValue('createdByLoginId',opp.CreatedBy.W3ACCOUNT__c);
            rFieldJson += concatenateJsonKeyValue('createByName',opp.CreatedBy.FullName__c);
            rFieldJson += concatenateJsonKeyValue('accountName',opp.Account.Name);
        }else if(objType.toLowerCase().equals('account')){//客户特殊字段
            Account acc = (Account)obj;
            rFieldJson += concatenateJsonKeyValue('hwOwnerLoginId',acc.Owner.W3ACCOUNT__c);
            rFieldJson += concatenateJsonKeyValue('hwOwnerName',acc.Owner.FullName__c);
            rFieldJson += concatenateJsonKeyValue('createdByLoginId',acc.CreatedBy.W3ACCOUNT__c);
            rFieldJson += concatenateJsonKeyValue('createByName',acc.CreatedBy.FullName__c);
            rFieldJson += concatenateJsonKeyValue('parentAccount',acc.Parent.Name);
        }else if(objType.toLowerCase().equals('contact')){//联系人特殊字段
            Contact con = (Contact)obj;
            rFieldJson += concatenateJsonKeyValue('hwOwnerLoginId',con.Owner.W3ACCOUNT__c);
            rFieldJson += concatenateJsonKeyValue('hwOwnerName', con.Owner.FullName__c);
            rFieldJson += concatenateJsonKeyValue('createdByLoginId',con.CreatedBy.W3Account__c);
            rFieldJson += concatenateJsonKeyValue('createByName',con.CreatedBy.FullName__c);
            rFieldJson += concatenateJsonKeyValue('accountName',con.Account.Name);
        }else if(objType.toLowerCase().equals('lead')){
            Lead lead = (Lead)obj;
            //线索无法直接查找用户上的字段，因此单独查询
            User owner = [Select id,W3ACCOUNT__c,Employee_ID__c,FullName__c From User Where Id =: lead.OwnerId];
            rFieldJson += concatenateJsonKeyValue('hwOwnerLoginId',owner.W3ACCOUNT__c);
            rFieldJson += concatenateJsonKeyValue('hwOwnerName',owner.FullName__c);
            rFieldJson += concatenateJsonKeyValue('createdByLoginId',lead.CreatedBy.W3Account__c);
            rFieldJson += concatenateJsonKeyValue('createByName',lead.CreatedBy.FullName__c);
            rFieldJson += concatenateJsonKeyValue('accountName',lead.Account_Name__r.Name);
            String opportunityName = '';
            String opportunityId = '';
            if(lead.China_Opportunity__c != null){
            	opportunityId = lead.China_Opportunity__r.Global_ID__c;
            	opportunityName = lead.China_Opportunity__r.Name;
            } else if(lead.ConvertedOpportunityId != null){
            	opportunityId = lead.ConvertedOpportunity.Global_ID__c;
            	opportunityName = lead.ConvertedOpportunity.Name;
            }
            rFieldJson += concatenateJsonKeyValue('opportunityName',opportunityName);
            rFieldJson += concatenateJsonKeyValue('opportunityID',opportunityId);
        }
        return rFieldJson;
    }
    
    // 拼接海外机会点，机会点产品信息
    private static String concatenateOppProducts(Opportunity Opp){
        if(opp.Project_Product__r.size() == 0){
            return null;
        }
        //拼接机会点产品JSON
        String sOppProductsJson = '"overseasProudcts":[';
        for(Integer i=0;i< opp.Project_Product__r.size();i++){
            Project_Product__c pp = opp.Project_Product__r[i];
            String sOppProductJson = '{';
            sOppProductJson += concatenateJsonKeyValue('sRowIdx',pp.Id);
            sOppProductJson += concatenateJsonKeyValue('productId',pp.Lookup__c);
            sOppProductJson += concatenateJsonKeyValue('opportunityCodeSF',pp.Project_Name__c);
            sOppProductJson += concatenateJsonKeyValue('productName',pp.Lookup__r.Name);
            sOppProductJson += concatenateJsonKeyValue('salesPrice',String.valueOf(pp.Sales_Price__c));
            sOppProductJson += concatenateJsonKeyValue('quantity',String.valueOf(pp.Quantity__c));
            sOppProductJson = sOppProductJson.substring(0, sOppProductJson.length() - 1);
            sOppProductJson += '}';
            sOppProductsJson += sOppProductJson;
            if(i == (opp.Project_Product__r.size()-1)){
                continue;
            }
            sOppProductsJson += ',';
        }
        //sOppProductsJson = sOppProductsJson.substring(0, sOppProductsJson.length()-1 );
        sOppProductsJson += '],';
        return sOppProductsJson;
    }
    
    // 字段格式化，日期、日期时间类型字段格式化
    private static string formatDateField(String sFieldValue , String sFieldType){
        if(sFieldType == 'datetime'){
            return datetime.valueOf(sFieldValue).format('MM/dd/yyyy HH:mm:ss');
        }else if(sFieldType == 'date'){
            return datetime.valueOf(sFieldValue).format('MM/dd/yyyy');
        }
        return sFieldValue;
    }
   
    // 给map_FieldType赋值
    private static Map<String, String> assignDateOrDatetimeMapFieldType(String objType){
        Map<String, String> map_FieldType = new Map<String, String>();
        Map<String, Schema.SobjectField> map_fields = new Map<String, Schema.SobjectField>();
        //根据对象类型，获取字段集合
        if(Lepus_SyncUtil.retrieveSobjectType(objType) != null){
            map_fields = Lepus_SyncUtil.retrieveSobjectType(objType).getDescribe().fields.getMap();
        }
        
        for(String sFieldApiName : map_fields.keySet()){
            Schema.SobjectField sField = map_fields.get(sFieldApiName);
            if(sField.getDescribe().isAccessible()){
                //日期、时间类型字段放入Map
                if(String.valueOf(sField.getDescribe().getType()).toLowerCase().equals('datetime') 
                        || String.valueOf(sField.getDescribe().getType()).toLowerCase().equals('date')){
                    map_FieldType.put(sFieldApiName.toLowerCase() , String.valueOf(sField.getDescribe().getType()).toLowerCase());
                } 
            }
        }
        return map_FieldType;
    }
    
    /**
    机会点字段更新推送
    **/
    //根据ID，获取字段更新推送的记录集合(只有机会点需要传字段更新)
    public static String FieldUpdateToJson(List<OpportunityFieldHistory> list_oppFieldHistory){
    	Map<String, String> oppUpdateFieldNameMap = new Map<String, String>();
    	oppUpdateFieldNameMap.put('Actual_PO_Amount__c','A.Sign Amount');
    	oppUpdateFieldNameMap.put('AccountId','End Customer');
    	oppUpdateFieldNameMap.put('Opportunity_Level__c','Opp.Level');
    	oppUpdateFieldNameMap.put('CloseDate','E.Sign Date');
    	oppUpdateFieldNameMap.put('Project_Progress__c','General Progress');
    	oppUpdateFieldNameMap.put('Opportunity_Progress__c','General Progress');
    	oppUpdateFieldNameMap.put('Estimated_ContractSign_Amount__c','E.Sign Amount(Million)');
    	oppUpdateFieldNameMap.put('NextStep','Next Step');
    	oppUpdateFieldNameMap.put('OwnerId','Opp.Owner');
    	oppUpdateFieldNameMap.put('Name','Opp.Name');
    	oppUpdateFieldNameMap.put('Win_Odds__c','Win Probability');
    	oppUpdateFieldNameMap.put('StageName','Sales Stage');
    	oppUpdateFieldNameMap.put('Master_Channer_Partner__c','Master Channel Partner');
    	oppUpdateFieldNameMap.put('Distributor__c','Distributor');
    	oppUpdateFieldNameMap.put('Representative_Office__c','Rep.Office');
    	oppUpdateFieldNameMap.put('Country__c','Country');
    	oppUpdateFieldNameMap.put('City__c','City');
    	oppUpdateFieldNameMap.put('Opportunity_type__c','Sales Mode');
    	
    	Map<String, String> oppUpdateFieldNameCNMap = new Map<String, String>();
    	oppUpdateFieldNameCNMap.put('Actual_PO_Amount__c','实际签单金额');
    	oppUpdateFieldNameCNMap.put('AccountId','最终客户');
    	oppUpdateFieldNameCNMap.put('Opportunity_Level__c','机会点级别');
    	oppUpdateFieldNameCNMap.put('CloseDate','预计签单日期');
    	oppUpdateFieldNameCNMap.put('Project_Progress__c','整体进度');
    	oppUpdateFieldNameCNMap.put('Opportunity_Progress__c','整体进度');
    	oppUpdateFieldNameCNMap.put('Estimated_ContractSign_Amount__c','预计签单金额（万元）');
    	oppUpdateFieldNameCNMap.put('NextStep','下一步计划');
    	oppUpdateFieldNameCNMap.put('OwnerId','机会点所有人');
    	oppUpdateFieldNameCNMap.put('Name','机会点名称');
    	oppUpdateFieldNameCNMap.put('Win_Odds__c','赢率');
    	oppUpdateFieldNameCNMap.put('StageName','销售阶段');
    	oppUpdateFieldNameCNMap.put('Master_Channer_Partner__c','主要渠道合作方');
    	oppUpdateFieldNameCNMap.put('Distributor__c','总经销商');
    	oppUpdateFieldNameCNMap.put('Representative_Office__c','代表处');
    	oppUpdateFieldNameCNMap.put('Country__c','国家');
    	oppUpdateFieldNameCNMap.put('City__c','城市');
    	oppUpdateFieldNameCNMap.put('Opportunity_type__c','销售模式');
    	
        String sJson = '[';
        //根据机会字段历史，拼接JSON
        if(list_oppFieldHistory.size() != 0){
            for(OpportunityFieldHistory oppFieldHistory:list_oppFieldHistory){
                String sField = (oppFieldHistory.Field == null?'Account':oppFieldHistory.Field);
                
                if(sField == 'Account' || sField=='Contract' || sField=='Campaign' || sField=='Territory'){
                    sField += 'Id';
                }
                
                String fieldUpdateJson = '{';
                
                String fieldNameAPI = Schema.Sobjecttype.opportunity.fields.getMap().get(sField) == null?'':Schema.Sobjecttype.opportunity.fields.getMap().get(sField).getDescribe().getName();
                if(oppUpdateFieldNameMap.get(fieldNameAPI) == null)
                continue;
                
                String fieldName = (oppUpdateFieldNameMap.get(fieldNameAPI) == null?fieldNameAPI:oppUpdateFieldNameMap.get(fieldNameAPI));
                String fieldNameCN = (oppUpdateFieldNameCNMap.get(fieldNameAPI) == null?fieldNameAPI:oppUpdateFieldNameCNMap.get(fieldNameAPI));
                
                fieldUpdateJson += concatenateJsonKeyValue('fieldName',fieldName);
                fieldUpdateJson += concatenateJsonKeyValue('fieldNameCN',fieldNameCN);
                fieldUpdateJson += concatenateJsonKeyValue('fieldValue',String.valueOf((oppFieldHistory.NewValue == null?'':oppFieldHistory.NewValue)));
                fieldUpdateJson += concatenateJsonKeyValue('fieldValueCN',String.valueOf((oppFieldHistory.NewValue == null?'':oppFieldHistory.NewValue)));
                fieldUpdateJson += concatenateJsonKeyValue('sRowIdx',(oppFieldHistory.OpportunityId == null?'':oppFieldHistory.Opportunity.Global_ID__c));
                fieldUpdateJson += concatenateJsonKeyValue('updateUser',(oppFieldHistory.OpportunityId == null?'':oppFieldHistory.Opportunity.LastModifiedBy.W3ACCOUNT__c));
                fieldUpdateJson += concatenateJsonKeyValue('lastUpdateTime',(oppFieldHistory.CreatedDate == null?'':oppFieldHistory.CreatedDate.format('MM/dd/yyyy HH:mm:ss')));
                fieldUpdateJson = fieldUpdateJson.substring(0, fieldUpdateJson.length() - 1);
                fieldUpdateJson += '},';
                sJson += fieldUpdateJson;
            }
            sJson = sJson.substring(0, sJson.length()-1 );
        }
        sJson+=']';
        return sJson;
    }
    
    /**
        团队成员更新推送
    **/
    public static String concatenateTeamMemberXml(Map<Id, List<Lepus_EIPMember>> memberMap, Map<Id, Sobject> idObjMap, String dataType, String action){
        String xml = '<?xml version="1.0" encoding="UTF-8"?>';
        Long cTime = Datetime.now().getTime();
        String objType = Lepus_SyncUtil.retrieveSobjectType(dataType).getDescribe().getName();
        String uid = cTime + '_SF_' + objType;
        xml += ('<rooms uid="' + uid + '">');
        //room
        for(Id sid : memberMap.keySet()){
            String codeJson = '{';
            codeJson += concatenateJsonKeyValue('GLobal_ID', String.valueOf(idObjMap.get(sid).get('Global_ID__c')));
            
            if(dataType.toLowerCase() == 'opportunity'){
                codeJson += concatenateJsonKeyValue('opportunityCodeSF',String.valueOf(idObjMap.get(sid).get('Opportunity_Number__c')));
                codeJson += concatenateJsonKeyValue('PRM ID',String.valueOf(idObjMap.get(sid).get('PRM_ID__c')));
            } else if(dataType.toLowerCase() == 'lead'){
                codeJson += concatenateJsonKeyValue('Lead ID',String.valueOf(idObjMap.get(sid).get('ID')));
            } else if(dataType.toLowerCase() == 'account'){
                codeJson += concatenateJsonKeyValue('CIS Number', String.valueOf(idObjMap.get(sid).get('CIS__c')));
            }
            
            codeJson += concatenateJsonKeyValue('source','SF');
            
            codeJson = codeJson.substring(0,codeJson.length() - 1);
            codeJson += '}';
            
            String recordName = String.valueOf(idObjMap.get(sid).get('Name'));
            
            // 2014-09-24 lead naturalName传lastname+firstname+company
            if(dataType.toLowerCase() == 'lead'){
            	recordName = String.valueOf(idObjMap.get(sid).get('LastName'))
            		 + (String.valueOf(idObjMap.get(sid).get('FirstName')) == null?'':String.valueOf(idObjMap.get(sid).get('FirstName')))
            		 + String.valueOf(idObjMap.get(sid).get('Company'));	
            }
            
            Long creationDate = ((Datetime)idObjMap.get(sid).get('CreatedDate')).getTime();
            Long modificationDate = ((Datetime)idObjMap.get(sid).get('LastModifiedDate')).getTime();
            //2014-08-21 sunda 需要传global id而不是记录id
            String globalId = String.valueOf(idObjMap.get(sid).get('Global_ID__c'));
            
            xml += concatenateXmlRoomNode(dataType, globalId, recordName, codeJson, '', 
                                            '1', creationDate, modificationDate, memberMap.get(sid), action);
        }
        xml += '</rooms>';
        return xml;
    }
    
    
    private static String concatenateXmlRoomNode(String dataType, String sid, String naturalName, String oppCode, String subject, 
                String publicRoom, Long creationDate, Long modificationDate, List<Lepus_EIPMember> memberList, String action){
        String xml = '<room>';
        
        String serviceId;
        if(dataType.toLowerCase() == 'opportunity'){
            serviceId = 'lepusebgwisoppo';
        } else {
            serviceId = 'lepusebg' + dataType.toLowerCase();
        }
        
        xml += concatenateXmlNode('serviceId',serviceId);
        xml += concatenateXmlNode('name',sid);
        xml += concatenateXmlNode('naturalName',naturalName);
        xml += concatenateXmlNode('description',oppCode); // 机会点或者Lead的opportunityCode或者opportunityCodeSF
        xml += concatenateXmlNode('subject',subject);
        xml += concatenateXmlNode('publicRoom',publicRoom);
        xml += concatenateXmlNode('creationDate',String.valueOf(creationDate));
        xml += concatenateXmlNode('modificationDate',String.valueOf(modificationDate));
        
        String actionValue = (action == 'delete'?action:'add');
        
        xml += concatenateXmlNode('action', actionValue);
        
        xml += '<members isfull="y">';
        
        if(action != 'delete'){
            for(Lepus_EIPMember member : memberList){
                xml += concatenateXmlMemberNode(member);
            }
        }
        
        xml += '</members>';
        
        xml += '</room>';
        return xml;
    }
    
    private static String concatenateXmlMemberNode(Lepus_EIPMember member){
        String xml = '<member>';
        
        xml += concatenateXmlNode('userAcc', member.userAcc);
        xml += concatenateXmlNode('username', member.username);
        xml += concatenateXmlNode('role', member.role);
        xml += concatenateXmlNode('action', 'add');
        xml += concatenateXmlNode('userEmail', member.userEmail);
        xml += concatenateXmlNode('wsAccountAccessLevel', member.wsAccountAccessLevel);
        xml += concatenateXmlNode('wsAccountId', member.wsAccountId);
        xml += concatenateXmlNode('wsInsertDate', member.wsInsertDate);
        xml += concatenateXmlNode('wsIsDeleted', member.wsIsDeleted);
        xml += concatenateXmlNode('wsSystemModStamp', member.wsSystemModStamp);
        xml += concatenateXmlNode('wsUpdateDate', member.wsUpdateDate);
        xml += concatenateXmlNode('wsUserId', member.wsUserId);
        xml += concatenateXmlNode('wsOpportunityAccessLevel', member.wsOpportunityAccessLevel);
        xml += concatenateXmlNode('wsOpportunityId', member.wsOpportunityId);
        xml += concatenateXmlNode('userCustom1', member.userCustom1);
        xml += concatenateXmlNode('userCustom2', member.userCustom2);
        xml += concatenateXmlNode('userCustom3', member.userCustom3);
        xml += concatenateXmlNode('userCustom4', member.userCustom4);
        xml += concatenateXmlNode('userCustom5', member.userCustom5);
        
        xml += '</member>';
        return xml;
    }
    
    
    /**
        对账数据拼接Json
    **/
    public static String ValidationDataToJson(List<sObject> list_obj , String sObjType){
        String sJson = '';
        for(sObject sObj : list_obj){
            sJson += '{';
            sJson += concatenateJsonKeyValue('sRowIdx', String.valueOf(sObj.get(sObjType.toLowerCase().equals('contact')?'Id':'Global_ID__c')));
            sJson += concatenateJsonKeyValue('lastUpdateTime', ((Datetime)sObj.get('LastModifiedDate')).formatGMT('MM/dd/yyyy HH:mm:ss'));
            sJson = sJson.substring(0, sJson.length() - 1);
            sJson += '},';
        }
        sJson = sJson.length()>0?sJson.substring(0, sJson.length()-1):sJson;
        sJson = '['+sJson + ']';
        return sJson;
    }
    
    /**
        线索字段更新推送
    **/
    public static String concatenateLeadHistoryXml(list<LeadHistory> leadHistories, String iAppName, String iType, String iAction, 
                                                        String iModule, String iSRowIdx, String iLastUpdateTime){
        String lhJson = concatenateLeadHistoriesJson(leadhistories);
        String lhXml = concatenateXml(iAppName, iType, iModule, iSRowIdx, iLastUpdateTime, lhJson, iAction);
        return lhXml;
    }
    
    private static String concatenateLeadHistoriesJson(list<LeadHistory> leadHistories){
        String lhJson = '{';
        lhJson += concatenateJsonKeyValue('resultCode','001');
        lhJson += concatenateJsonKeyValue('errorMsg','');
        lhJson += '"resultMsg":';
        lhJson += '[';
        
        for(LeadHistory lh : leadHistories){
            lhJson += concatenateLeadHistoryJson(lh);
            lhJson += ',';
        }
        
        lhJson = lhJson.substring(0, lhJson.length() - 1);
        lhJson += ']';
        lhJson += '}';
        return lhJson;
    }
    
    private static String concatenateLeadHistoryJson(LeadHistory lh){
        String lJson = '{';
        
        lJson += concatenateJsonKeyValue('sRowIdx',lh.Id);
        lJson += concatenateJsonKeyValue('leadCode',(lh.LeadId == null?'':lh.Lead.Lead_Code__c));
        lJson += concatenateJsonKeyValue('globalId',(lh.LeadId == null?'':lh.Lead.Global_ID__c));
        lJson += concatenateJsonKeyValue('leadId',lh.LeadId);
        lJson += concatenateJsonKeyValue('updateField',lh.Field);
        lJson += concatenateJsonKeyValue('createdBy',lh.CreatedById);
        
        String w3Account = '';
        if(lh.CreatedById != null){
            User u = [Select Id, W3ACCOUNT__c From User Where Id =: lh.CreatedById];
            w3Account = u.W3ACCOUNT__c;
        }
        //String cDate = formatDateField(String.valueOf(lh.CreatedDate), 'datetime');
        
        String cDate = '';
        if(lh.CreatedDate != null){
            cDate = lh.CreatedDate.format('MM/dd/yyyy HH:mm:ss', 'GMT');
        }
        
        lJson += concatenateJsonKeyValue('createdUserAccount',w3Account);
        lJson += concatenateJsonKeyValue('createdUserName',(lh.CreatedById == null?'':lh.CreatedBy.Name));
        lJson += concatenateJsonKeyValue('creationDate',cDate);
        lJson += concatenateJsonKeyValue('oldValue',String.valueOf(lh.OldValue));
        lJson += concatenateJsonKeyValue('newValue',String.valueOf(lh.NewValue));
        
        lJson = lJson.substring(0, lJson.length() - 1);
        lJson += '}';
        
        return lJson;
    }
    
    /**
        用户角色更新推送
    **/
    public static String concatenateUserXml(List<User> userList, String iAppName, String iType, String iAction, 
                                                String iModule, String iSRowIdx, String iLastUpdateTime){
        String uJson = concatenateUserJsonArray(userList);
        String uXml = concatenateXml(iAppName, iType, iModule, iSRowIdx, iLastUpdateTime, uJson, iAction);
        return uXml;
    }
    
    private static String concatenateUserJsonArray(List<User> userList){
        String uJson = '{';
        uJson += concatenateJsonKeyValue('resultCode','001');
        uJson += concatenateJsonKeyValue('errorMsg','');
        uJson += '"resultMsg":';
        uJson += '[';
        for(User u : userList){
            uJson += concatenateUserJson(u);
            uJson += ',';
        }
        if(uJson.length() > 1)
        uJson = uJson.substring(0, uJson.length() - 1);
        
        uJson += ']';
        uJson += '}';
        return uJson;
    }
    
    private static String concatenateUserJson(Sobject obj){
        List<Lepus_EIP_FieldMapping.fieldMap> list_fieldMap = Lepus_EIP_FieldMapping.getUserFieldMap();
        String uJson = '{';
        Map<String , String> map_FieldType = assignDateOrDatetimeMapFieldType('user');
        
        //字段Json拼接
        for(Lepus_EIP_FieldMapping.fieldMap fMap : list_fieldMap){
            String uKey = fMap.EIPField;
            String uValue;
            
            if(obj.get(fMap.SFField) != null){
                uValue = String.valueOf(obj.get(fMap.SFField));
                // 处理日期时间和日期字段格式
                if(map_FieldType.containsKey(fMap.SFField.toLowerCase())){
                    uValue = formatDateField(uValue , map_FieldType.get(fMap.SFField.toLowerCase()));
                }
            }
            
            uJson += concatenateJsonKeyValue(uKey, uValue);
        }
        
        User u = (User)obj;
        uJson += concatenateJsonKeyValue('profileName',u.Profile.Name);
        uJson += concatenateJsonKeyValue('roleName',u.UserRole.Name);
        
        if(uJson.length() > 0)
        uJson = uJson.substring(0, uJson.length() - 1);
        
        uJson += '}';
        return uJson;
    }
    
    //拼接XML
    public static String concatenateXml(String iAppName , String iType , String iModule , String iSRowIdx , String iLastUpdateTime , 
                                            String sJson , String iAction){
        String xml = '';
        xml += '<?xml version="1.0" encoding="UTF-8"?>';
        xml += '<IntegrationMsg>';
        //Header
        xml += '<ObjectHeader>';
        
        xml += concatenateXmlNode('AppName',iAppName);
        xml += concatenateXmlNode('Type', iType);
        xml += concatenateXmlNode('CreateUpdate', iAction); //CreateUpdate Action For Wisdom
        xml += concatenateXmlNode('Module', iModule);
        xml += concatenateXmlNode('SRowIdx', iSRowIdx);
        xml += concatenateXmlNode('LastUpdateTime', iLastUpdateTime);
        
        xml += '</ObjectHeader>';
        
        xml += concatenateXmlNode('Message', sJson); //Message : Json String
        
        xml += '</IntegrationMsg>';
        return xml;
    }
    
    private static String concatenateJsonKeyValue(String key, String value){
        String keyValue = '"';
        keyValue += key;
        keyValue += '":';
        if(value == null){
            keyValue += 'null,';
        } else {
            keyValue += '"';
            keyValue += replaceJsonSpecialChracters(value);
            keyValue += '",';
        }
        return keyValue;
    }
    
    private static String concatenateXmlNode(String nodeName, String nodeValue){
        String node = '<';
        node += nodeName;
        node += '>';
        if(nodeValue == null){
            node += '';
        } else {
            node += replaceXmlSpecialCharacters(nodeValue);
        }
        node += '</';
        node += nodeName;
        node += '>';
        return node;
    }
    
    // 处理拼接特殊字符
    private static String replaceXmlSpecialCharacters(String nodeValue){
        nodeValue = nodeValue.replace('&', '&amp;');
        nodeValue = nodeValue.replace('<', '&lt;');
        return nodeValue;
    }
    
    private static String replaceJsonSpecialChracters(String value){
        value = value.replace('\\', '\\'+'\\');
        value = value.replace('"', '\\'+'"');
        return value;
    }
}