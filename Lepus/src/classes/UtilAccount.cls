/**
 * Utils used in account object
 * Author           : BLS
 * Create Date      : 2013/3
 *     
 */ 
public without sharing class UtilAccount {
	 
	  //给定account ,一次性更新其下所有子节点的na记录
    public static void UpdateChildrenNA(Account parent,String msg){//传入父客户,查找出其下所有子客户
	    list <Account> accountList = getChildren(parent);
	    
	    //Prevent unnecessary code execution Ian Huang 2013-08-09
		if (accountList.isEmpty()) return;
	    
	    
	    //为每个子客户创建一条对应的NA记录
      Map<id,Named_Account__c> nas = new Map<id,Named_Account__c>();//account id,na
	     for(Account a : AccountList){
	     	Named_Account__c newNA =  new Named_Account__c(Is_Named_Account__c = parent.Is_Named_Account__c,
	     		Is_403_Customer__c = parent.Is_403_Customer__c,
	     		Named_Account_Character__c = parent.Named_Account_Character__c,
	     		Named_Account_Level__c = parent.Named_Account_Level__c,
	     		Approved_Date__c = parent.NA_Approved_Date__c,
	     		Record_Description__c = msg,Is_System_Create__c = true, 
	     		Named_Account_Property__c = parent.Named_Account_Property__c,Account_Name__c =a.id);
	     	nas.put(a.id,newNA);
	     }
	     
	     //禁止NamedAccountAfterInsertHandler的运行,对account相关字段的更新在方法内部完成.避免循环触发trigger
	     NamedAccountAfterInsertHandler.isFirstRun = false;
	     
	     insert nas.values();
	     
	     //更新account的相关字段
	     for(Account a : accountList){
	     	a.Named_Account__c =  nas.get(a.id).id;
	     	a.Is_Named_Account__c = parent.Is_Named_Account__c;
	     	a.Is_403_Customer__c = parent.Is_403_Customer__c;
	     	a.Named_Account_Character__c = parent.Named_Account_Character__c;
	     	a.Named_Account_Level__c = parent.Named_Account_Level__c;
	     	a.Named_Account_Property__c = parent.Named_Account_Property__c;
	     	a.NA_Approved_Date__c = parent.NA_Approved_Date__c;
	     }
	     update accountList;
    }
    
    //给定account 和其根父节点id,一次性更新其下所有子节点的根父节点
    public static void UpdateAllHQParentAccount(Account current){
    	list <Account> accountList = getChildren(current);//取得全部子客户
	    if(current.parentid == null){
	     	for(Account a : accountList){
	     		a.HQ_Parent_Account__c =  current.id;
	     	}
	    }else{
	     	for(Account a : accountList){
	     		a.HQ_Parent_Account__c = current.HQ_Parent_Account__c;
	     	}
	     }
	     update accountList;
    }
    
    //add by steven 20130605  modify by steven 20130729 
    //根据当前account的行业和客户群信息,一次性更新其下所有子节点的行业和客户群信息
    public static void UpdateAllChildAccountIndustryInfo(Account current){
    	list <Account> accountList = getChildren(current);//取得全部子客户
     	for(Account a : accountList){
     		a.industry = current.industry;
     		a.Sub_industry_HW__c = current.Sub_industry_HW__c;
     		a.Customer_Group__c = current.Customer_Group__c;
     		a.Customer_Group_Code__c = current.Customer_Group_Code__c;
     		
            a.Region_HW__c = current.Region_HW__c;
            a.Representative_Office__c = current.Representative_Office__c;
            if(current.Representative_Office__c != '系统部总部') {
 				a.Province__c = current.Province__c;
 			}
     	}
     	TRIGGERFIRECONSTANTS.accountTriggerShouldRun = false;//提前禁止子客户上trigger的执行
	    update accountList;
	    TRIGGERFIRECONSTANTS.accountTriggerShouldRun = true;//允许子客户上trigger的执行
    }
    //add by steven 20130605
    //一次性获取所有子客户ID
    public static Set<Id> GetAllChildId(Account current){
    	list <Account> accountList = getChildren(current);//取得全部子客户
    	Set<Id> accountIds = new Set<Id>();
     	for(Account a : accountList){
     		accountIds.add(a.id);
     	}
     	return accountIds;
    }
    
    //给定account,获取其下所有子客户
    private static List<Account> getChildren(Account current){
	     //每次查询五层子客户
    	Map<id,Account> accMap = new Map<id,Account>([select id, Named_Account__c,name,parent.parent.parent.parent.parent.id  
	    				                  ,industry,Sub_industry_HW__c,Customer_Group__c,Customer_Group_Code__c //add by steven 20130605
	    				                  ,Region_HW__c,Representative_Office__c,Province__c,City_Huawei_China__c//add by steven 20130729
	    				                    from Account 
	        where ParentId =: current.id or parent.parent.id =:current.id or parent.parent.parent.id = :current.id
	     or parent.parent.parent.parent.id = :current.id or parent.parent.parent.parent.parent.id = :current.id]);
	    Set<id> childIds = new Set<id>();//all children ids
   		List<Account> acc = accMap.values();//use this list to store each soql result
	    do{
	    		childIds.clear();
	    		for(Account a : acc){
			    	if(a.parent.parent.parent.parent.parent.id != null){
			    		childIds.add(a.id);
			    	}
	    		}
	    		
	    		//Prevent unnecessary SOQL execution Ian Huang 2013-08-09
	    		if (childIds.isEmpty()) break; 
	    		
		    	acc = [select id, Named_Account__c,parent.parent.parent.parent.parent.id  ,name
		    	,industry,Sub_industry_HW__c,Customer_Group__c,Customer_Group_Code__c //add by steven 20130605
		    	,Region_HW__c,Representative_Office__c,Province__c,City_Huawei_China__c//add by steven 20130729
	    				                    from Account 
	        where ParentId in :childIds or parent.parent.id in :childIds or parent.parent.parent.id in :childIds
	     		or parent.parent.parent.parent.id in :childIds or parent.parent.parent.parent.parent.id in :childIds];
	     		accMap.putAll(acc);
	    }while(acc.size() > 0);
	    return accMap.values();
    }
    
    
    //查找根节点父客户的Id
    public static Id GetRootParentAccount(Id AccountId) {

        Id s;
        Id sParentId;
        
        sParentId = AccountId;
        while (true){
            Account a = [SELECT id, name, 
            	parent.id, 
            	parent.parent.id, 
            	parent.parent.parent.id,
            	parent.parent.parent.parent.id,
            	parent.parent.parent.parent.parent.id
 						FROM Account where id =: sParentId];
            
            if(a.ParentId != null){
            	s = a.ParentId;
            	if(a.parent.parent != null){
	            	s = a.parent.ParentId;
            		if(a.parent.parent.parent != null){
		            	s = a.parent.parent.ParentId;
            			if(a.parent.parent.parent.parent != null){
			            	s = a.parent.parent.parent.ParentId;
            				if(a.parent.parent.parent.parent.parent != null){
				            	s = a.parent.parent.parent.parent.ParentId;
            				}
            			}
            		}
            	}
            }
            if (s != null && s!= sParentId){
                sParentId = s;
            }else{
                break;
            }
        }
        
        return sParentId;
    }
    
}