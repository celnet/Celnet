//Generated by wsdl2apex

public class SoapIssues6 {
    public class IssueReq_x1_HTTPSPort {
        //public String endpoint_x = 'https://b2b-test1.huawei.com:3777/SOAP';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR', 'SoapIssues6', 'http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR', 'B2bSoapGissue6', 'http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd', 'wwwTibcoComNamespacesBc200204Part6', 'http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/UIssueSR', 'B2bSoapUissue6', 'http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/SIssueSR', 'B2bSoapSissue6'};
        public B2bSoapSissue6.tIssueRS SRequest_Response(String CUSTOMER_SR_NUMBER,String ACCOUNT_NAME,
        	String CUSTOMER_ACCOUNT_NUMBER,String CURRENT_CONTACT_NAME,String CURRENT_CONTACT_TELEPHONE,
        	String CURRENT_CONTACT_EMAIL_ADDRESS,String EXTERNAL_ATTRIBUTE_1,String RESOURCE_TYPE,
        	String REPORTED_BY,String INVENTORY_ITEM_NO,String INCIDENT_TYPE_NAME,String INCIDENT_SEVERITY_NAM,
        	String INCIDENT_STATUS_CODE,String RESPONSIBLE_GROUP_NAME,String INCIDENT_OWNER_NAME,String REPORT_DATE,
        	String SUMMARY,String INCIDENT_NOTES,String IS_OVERFLOWED,String SYNC_TIMES,String SR_ORIGIN,
        	String endpoint_x,String AUX1,String AUX2,String AUX3) {
            B2bSoapSissue6.tIssueRQ request_x = new B2bSoapSissue6.tIssueRQ();
            B2bSoapSissue6.tIssueRS response_x;
            request_x.CUSTOMER_SR_NUMBER = CUSTOMER_SR_NUMBER;
            request_x.ACCOUNT_NAME = ACCOUNT_NAME;
            request_x.CUSTOMER_ACCOUNT_NUMBER = CUSTOMER_ACCOUNT_NUMBER;
            request_x.CURRENT_CONTACT_NAME = CURRENT_CONTACT_NAME;
            request_x.CURRENT_CONTACT_TELEPHONE = CURRENT_CONTACT_TELEPHONE;
            request_x.CURRENT_CONTACT_EMAIL_ADDRESS = CURRENT_CONTACT_EMAIL_ADDRESS;
            request_x.EXTERNAL_ATTRIBUTE_1 = EXTERNAL_ATTRIBUTE_1;
            request_x.RESOURCE_TYPE = RESOURCE_TYPE;
            request_x.REPORTED_BY = REPORTED_BY;
            request_x.INVENTORY_ITEM_NO = INVENTORY_ITEM_NO;
            request_x.INCIDENT_TYPE_NAME = INCIDENT_TYPE_NAME;
            request_x.INCIDENT_SEVERITY_NAM = INCIDENT_SEVERITY_NAM;
            request_x.INCIDENT_STATUS_CODE = INCIDENT_STATUS_CODE;
            request_x.RESPONSIBLE_GROUP_NAME = RESPONSIBLE_GROUP_NAME;
            request_x.INCIDENT_OWNER_NAME = INCIDENT_OWNER_NAME;
            request_x.REPORT_DATE = REPORT_DATE;
            request_x.SUMMARY = SUMMARY;
            request_x.INCIDENT_NOTES = INCIDENT_NOTES;
            request_x.IS_OVERFLOWED = IS_OVERFLOWED;
            request_x.SYNC_TIMES = SYNC_TIMES;
            request_x.SR_ORIGIN = SR_ORIGIN;
            request_x.AUX1 = AUX1;
            request_x.AUX2 = AUX2;
            request_x.AUX3 = AUX3;
            Map<String, B2bSoapSissue6.tIssueRS> response_map_x = new Map<String, B2bSoapSissue6.tIssueRS>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/SIssueSR',
              'IssueRQ',
              'http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/SIssueSR',
              'IssueRS',
              'B2bSoapSissue6.tIssueRS'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
        public SoapIssues6.tIssueRS[] Request_Response(SoapIssues6.tIssueRQ[] IssueRQ,String endpoint_x) {
            SoapIssues6.RQ_element request_x = new SoapIssues6.RQ_element();
            SoapIssues6.RS_element response_x;
            request_x.IssueRQ = IssueRQ;
            Map<String, SoapIssues6.RS_element> response_map_x = new Map<String, SoapIssues6.RS_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x, 
              '',
              'http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',
              'RQ',
              'http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',
              'RS',
              'SoapIssues6.RS_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.IssueRS;
        }
        public B2bSoapGissue6.tSRStatusReply[] GRequest_Response(B2bSoapGissue6.tSRStatusGet[] SRStatusGet,String endpoint_x) {
            B2bSoapGissue6.RQ_element request_x = new B2bSoapGissue6.RQ_element();
            B2bSoapGissue6.RS_element response_x;
            request_x.SRStatusGet = SRStatusGet;
            Map<String, B2bSoapGissue6.RS_element> response_map_x = new Map<String, B2bSoapGissue6.RS_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR',
              'RQ',
              'http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/GIssueSR',
              'RS',
              'B2bSoapGissue6.RS_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.SRStatusReply;
        }
        public B2bSoapUissue6.tUpdateIcareSRReply[] UpdateICareSR(B2bSoapUissue6.tUpdateIcareSR[] UpdateIcareSR,String endpoint_x) {
            B2bSoapUissue6.RQ_element request_x = new B2bSoapUissue6.RQ_element();
            B2bSoapUissue6.RS_element response_x;
            request_x.UpdateIcareSR = UpdateIcareSR;
            Map<String, B2bSoapUissue6.RS_element> response_map_x = new Map<String, B2bSoapUissue6.RS_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/UIssueSR',
              'RQ',
              'http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/UIssueSR',
              'RS',
              'B2bSoapUissue6.RS_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.UpdateIcareSRReply;
        }
    }
    public class RQ_element {
        public SoapIssues6.tIssueRQ[] IssueRQ;
        private String[] IssueRQ_type_info = new String[]{'IssueRQ','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR','false','false'};
        private String[] field_order_type_info = new String[]{'IssueRQ'};
    }
    public class RS_element {
        public SoapIssues6.tIssueRS[] IssueRS;
        private String[] IssueRS_type_info = new String[]{'IssueRS','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR','false','false'};
        private String[] field_order_type_info = new String[]{'IssueRS'};
    }
    public class tIssueRQ {
        public String ROW_ID;
        public String CUSTOMER_SR_NUMBER;
        public String ACCOUNT_NAME;
        public String CUSTOMER_ACCOUNT_NUMBER;
        public String CURRENT_CONTACT_NAME;
        public String CURRENT_CONTACT_TELEPHONE;
        public String CURRENT_CONTACT_EMAIL_ADDRESS;
        public String EXTERNAL_ATTRIBUTE_1;
        public String RESOURCE_TYPE;
        public String REPORTED_BY;
        public String INVENTORY_ITEM_NO;
        public String INCIDENT_TYPE_NAME;
        public String INCIDENT_SEVERITY_NAM;
        public String INCIDENT_STATUS_CODE;
        public String RESPONSIBLE_GROUP_NAME;
        public String INCIDENT_OWNER_NAME;
        public String REPORT_DATE;
        public String SUMMARY;
        public String INCIDENT_NOTES;
        public String IS_OVERFLOWED;
        public String SYNC_TIMES;
        public String SR_ORIGIN;
        public String AUX1;
        public String AUX2;
        public String AUX3;
        private String[] ROW_ID_type_info = new String[]{'ROW_ID','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'1','1','false'};
        private String[] CUSTOMER_SR_NUMBER_type_info = new String[]{'CUSTOMER_SR_NUMBER','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'1','1','false'};
        private String[] ACCOUNT_NAME_type_info = new String[]{'ACCOUNT_NAME','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'1','1','false'};
        private String[] CUSTOMER_ACCOUNT_NUMBER_type_info = new String[]{'CUSTOMER_ACCOUNT_NUMBER','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'0','1','false'};
        private String[] CURRENT_CONTACT_NAME_type_info = new String[]{'CURRENT_CONTACT_NAME','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'0','1','false'};
        private String[] CURRENT_CONTACT_TELEPHONE_type_info = new String[]{'CURRENT_CONTACT_TELEPHONE','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'0','1','false'};
        private String[] CURRENT_CONTACT_EMAIL_ADDRESS_type_info = new String[]{'CURRENT_CONTACT_EMAIL_ADDRESS','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'0','1','false'};
        private String[] EXTERNAL_ATTRIBUTE_1_type_info = new String[]{'EXTERNAL_ATTRIBUTE_1','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'0','1','false'};
        private String[] RESOURCE_TYPE_type_info = new String[]{'RESOURCE_TYPE','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'1','1','false'};
        private String[] REPORTED_BY_type_info = new String[]{'REPORTED_BY','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'0','1','false'};
        private String[] INVENTORY_ITEM_NO_type_info = new String[]{'INVENTORY_ITEM_NO','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'1','1','false'};
        private String[] INCIDENT_TYPE_NAME_type_info = new String[]{'INCIDENT_TYPE_NAME','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'1','1','false'};
        private String[] INCIDENT_SEVERITY_NAM_type_info = new String[]{'INCIDENT_SEVERITY_NAM','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'1','1','false'};
        private String[] INCIDENT_STATUS_CODE_type_info = new String[]{'INCIDENT_STATUS_CODE','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'1','1','false'};
        private String[] RESPONSIBLE_GROUP_NAME_type_info = new String[]{'RESPONSIBLE_GROUP_NAME','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'0','1','false'};
        private String[] INCIDENT_OWNER_NAME_type_info = new String[]{'INCIDENT_OWNER_NAME','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'0','1','false'};
        private String[] REPORT_DATE_type_info = new String[]{'REPORT_DATE','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'1','1','false'};
        private String[] SUMMARY_type_info = new String[]{'SUMMARY','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'1','1','false'};
        private String[] INCIDENT_NOTES_type_info = new String[]{'INCIDENT_NOTES','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'0','1','false'};
        private String[] IS_OVERFLOWED_type_info = new String[]{'IS_OVERFLOWED','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'1','1','false'};
        private String[] SYNC_TIMES_type_info = new String[]{'SYNC_TIMES','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'1','1','false'};
        private String[] SR_ORIGIN_type_info = new String[]{'SR_ORIGIN','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'1','1','false'};
        private String[] AUX1_type_info = new String[]{'AUX1','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'0','1','false'};
        private String[] AUX2_type_info = new String[]{'AUX2','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'0','1','false'};
        private String[] AUX3_type_info = new String[]{'AUX3','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR','false','false'};
        private String[] field_order_type_info = new String[]{'ROW_ID','CUSTOMER_SR_NUMBER','ACCOUNT_NAME','CUSTOMER_ACCOUNT_NUMBER','CURRENT_CONTACT_NAME','CURRENT_CONTACT_TELEPHONE','CURRENT_CONTACT_EMAIL_ADDRESS','EXTERNAL_ATTRIBUTE_1','RESOURCE_TYPE','REPORTED_BY','INVENTORY_ITEM_NO','INCIDENT_TYPE_NAME','INCIDENT_SEVERITY_NAM','INCIDENT_STATUS_CODE','RESPONSIBLE_GROUP_NAME','INCIDENT_OWNER_NAME','REPORT_DATE','SUMMARY','INCIDENT_NOTES','IS_OVERFLOWED','SYNC_TIMES','SR_ORIGIN','AUX1','AUX2','AUX3'};
    }
    public class tIssueRS {
        public String ROW_ID;
        public String CUSTOMER_SR_NUMBER;
        public String ICareSR_Number;
        public String ICARE_SYNC_STATUS;
        public String ICARE_ACK;
        private String[] ROW_ID_type_info = new String[]{'ROW_ID','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'1','1','false'};
        private String[] CUSTOMER_SR_NUMBER_type_info = new String[]{'CUSTOMER_SR_NUMBER','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'1','1','false'};
        private String[] ICareSR_Number_type_info = new String[]{'ICareSR_Number','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'0','1','false'};
        private String[] ICARE_SYNC_STATUS_type_info = new String[]{'ICARE_SYNC_STATUS','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'1','1','false'};
        private String[] ICARE_ACK_type_info = new String[]{'ICARE_ACK','http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://ns.huawei.com/XSD/HUAWEI/B2B/SOAP/IssueSR','false','false'};
        private String[] field_order_type_info = new String[]{'ROW_ID','CUSTOMER_SR_NUMBER','ICareSR_Number','ICARE_SYNC_STATUS','ICARE_ACK'};
    }
}