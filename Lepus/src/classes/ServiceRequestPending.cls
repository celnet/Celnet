public with sharing class ServiceRequestPending {
        public Case sr {get;set;}
        public SR_Pending__c srp {get;set;}
        public boolean isSaveButtonVisible {get; set;}          //保存按钮是否显示
        public boolean isEditVisible {get; set;}                        //编辑区是否显示

        public ServiceRequestPending(ApexPages.StandardController stdCon){
                this.sr = [select id, CaseNumber from Case where Id = : stdCon.getId()];
                if([select count() from sr_pending__c where Service_Request__c =: this.sr.id and status__c =: 'Open'] > 0){
                        //有未关闭的挂起记录，不能新建挂起
                        sr.addError('Please close service request pending first!');
                        isSaveButtonVisible = false;
                        isEditVisible = false;
                        return;
                }else{
                        isSaveButtonVisible = true;
                        isEditVisible = true;
                }

            UserRecordAccess access ;
            try{
                access = [SELECT RecordId, HasReadAccess, HasEditAccess
                                                 FROM UserRecordAccess
                                                 WHERE UserId = :UserINfo.getUserId()
                                                 AND RecordId = :stdCon.getId() limit 1];
            }catch(QueryException e){
            }
                if(access == null || !access.HasEditAccess){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 
                        'hasediteaccess:---' + System.Label.No_Privilege));
                }
                srp = new SR_Pending__c(Service_Request__c = sr.id,     Status__c = 'Open');
        }

    public PageReference save(){
                try{
                        insert srp;
                        return new ApexPages.StandardController(sr).view();
                }catch(Exception e){
                        ApexPages.addMessages(e);
                        System.debug('the exception e:---' + e.getMessage());
                        return null;
                }       
    }
}