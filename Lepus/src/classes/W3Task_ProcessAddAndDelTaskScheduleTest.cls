/**
 * 
 */
@isTest
private class W3Task_ProcessAddAndDelTaskScheduleTest {

    static testMethod void testSchedual() {
        W3TaskSchedualTime__c w3taskSchedualTime = new W3TaskSchedualTime__c();
        w3taskSchedualTime.Name = '开始时间';
        w3taskSchedualTime.StartTime__c = datetime.now().addMinutes(-1);
        insert w3taskSchedualTime;
        
        Account acc = Lepus_EIPCalloutServiceTest.createAccount('acc1');
        acc.Inactive_NA__c = false;
        acc.Is_Named_Account__c = true;
        //acc.HQ_Parent_Account_Name__c = acc.Name;
        acc.RecordTypeId = CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE;
        insert acc;
        Account acc2 = Lepus_EIPCalloutServiceTest.createAccount('acc2');
        insert acc2;
        
        Account_Link_Request__c alr = new Account_Link_Request__c();
        alr.parent__c = acc.Id;
        alr.child__c = acc2.Id;
        alr.Approver__c = userinfo.getUserId();
        insert alr;
        
        Approval.ProcessSubmitRequest psr = new Approval.ProcessSubmitRequest();
        psr.setObjectId(alr.Id);
        Approval.ProcessResult result  = Approval.process(psr);
        
        system.Test.startTest();
        W3Task_ProcessAddAndDelTaskScheduleJob job = new W3Task_ProcessAddAndDelTaskScheduleJob();
        String CRON_EXP = '0 0 0 3 9 ? 2022';
        String jobId = System.schedule('testJob123', CRON_EXP, new W3Task_ProcessAddAndDelTaskScheduleJob());
        system.Test.stopTest();
    }
    static testMethod void testBatch() {
    	Account acc = Lepus_EIPCalloutServiceTest.createAccount('acc1');
        acc.Inactive_NA__c = false;
        acc.Is_Named_Account__c = true;
        //acc.HQ_Parent_Account_Name__c = acc.Name;
        acc.RecordTypeId = CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE;
        insert acc;
        Account acc2 = Lepus_EIPCalloutServiceTest.createAccount('acc2');
        insert acc2;
        
        Account_Link_Request__c alr = new Account_Link_Request__c();
        alr.parent__c = acc.Id;
        alr.child__c = acc2.Id;
        alr.Approver__c = userinfo.getUserId();
        insert alr;
        datetime d = datetime.now();
        
        //Approval.Processrequest pr = new Approval.Processrequest();
        Approval.ProcessSubmitRequest psr = new Approval.ProcessSubmitRequest();
        psr.setObjectId(alr.Id);
        Approval.ProcessResult result  = Approval.process(psr);
        Set<ID> set_id = new Set<ID>();
        set_id.add(result.getInstanceId());
        system.Test.startTest();
        W3Task_ProcessAddAndDelTaskScheduleJob job = new W3Task_ProcessAddAndDelTaskScheduleJob(set_id, d ,datetime.now());
        database.executeBatch(job, 1);
        system.Test.stopTest();
    }
}