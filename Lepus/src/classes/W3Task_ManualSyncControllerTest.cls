/**
 * Test W3Task_ManualSyncController
 * Test W3Task_ManualSyncBatch
 * Test W3Task_initHistoryData
 */
@isTest
private class W3Task_ManualSyncControllerTest {
	
    static testMethod void testPage() {
        // TO DO: implement unit test
        Account acc = Lepus_EIPCalloutServiceTest.createAccount('acc1');
        acc.Inactive_NA__c = false;
        acc.Is_Named_Account__c = true;
        //acc.HQ_Parent_Account_Name__c = acc.Name;
        acc.RecordTypeId = CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE;
        insert acc;
        Account acc2 = Lepus_EIPCalloutServiceTest.createAccount('acc2');
        insert acc2;
        
        Account_Link_Request__c alr = new Account_Link_Request__c();
        alr.parent__c = acc.Id;
        alr.child__c = acc2.Id;
        alr.Approver__c = userinfo.getUserId();
        insert alr;
        
        Approval.ProcessSubmitRequest psr = new Approval.ProcessSubmitRequest();
        psr.setObjectId(alr.Id);
        Approval.ProcessResult result  = Approval.process(psr);
        Set<ID> set_id = new Set<ID>();
        set_id.add(result.getInstanceId());
        ProcessInstanceWorkitem wi = [select id,SystemModstamp from ProcessInstanceWorkitem where ProcessInstanceId =: result.getInstanceId() limit 1];
        
        List<W3_Task_Sync_Fail__c> list_syncFail = new List<W3_Task_Sync_Fail__c>();
        W3_Task_Sync_Fail__c syncFail1 = new W3_Task_Sync_Fail__c();
        syncFail1.ActionType__c = 'add';
        syncFail1.SyncStatus__c = 'Open';
        syncFail1.applicant__c = 'test';
        syncFail1.appName__c = 'test';
        syncFail1.appURL__c = 'https:test.test.com';
        syncFail1.ErrorMessage__c = 'error';
        syncFail1.handler__c = 'zhangsan w323232';
        syncFail1.ProcessInstanceId__c = result.getInstanceId();
        syncFail1.reserve3__c = 'sdfsd';
        syncFail1.taskDesc__c = 'test';
        syncFail1.taskURL__c = 'https://wefwe.wefe.com';
        syncFail1.taskUUID__c = 'ewfdf';
        syncFail1.time_x__c = wi.SystemModstamp.format('YYYY-MM-DD hh:mm:ss');
        syncFail1.type_x__c = 'wef';
        syncFail1.WorkItem_Step_Id__c = wi.Id;
        list_syncFail.add(syncFail1);
        W3_Task_Sync_Fail__c syncFail2 = new W3_Task_Sync_Fail__c();
        syncFail2.ActionType__c = 'del';
        syncFail2.SyncStatus__c = 'Open';
        syncFail2.applicant__c = 'test';
        syncFail2.appName__c = 'test';
        syncFail2.appURL__c = 'https:test.test.com';
        syncFail2.ErrorMessage__c = 'error';
        syncFail2.handler__c = 'zhangsan w323232';
        syncFail2.ProcessInstanceId__c = result.getInstanceId();
        syncFail2.reserve3__c = 'sdfsd';
        syncFail2.taskDesc__c = 'test';
        syncFail2.taskURL__c = 'https://wefwe.wefe.com';
        syncFail2.taskUUID__c = 'ewfdf';
        syncFail2.time_x__c = '2014-09-10 20:32:30';
        syncFail2.type_x__c = 'wef';
        syncFail2.WorkItem_Step_Id__c = null;
        list_syncFail.add(syncFail2);
        insert list_syncFail;
        
        system.Test.startTest();
        W3Task_ManualSyncController page = new W3Task_ManualSyncController();
        page.DeleteAll();
        page.SyncAgain();
        page.DeleteRecord();
        page.list_sfw[0].isSelected = true;
        page.SyncAgain();
        page.DeleteRecord();
        system.Test.stopTest();
        
    }
    static testMethod void testSyncBatch() {
    	// TO DO: implement unit test
        Account acc = Lepus_EIPCalloutServiceTest.createAccount('acc1');
        acc.Inactive_NA__c = false;
        acc.Is_Named_Account__c = true;
        //acc.HQ_Parent_Account_Name__c = acc.Name;
        acc.RecordTypeId = CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE;
        insert acc;
        Account acc2 = Lepus_EIPCalloutServiceTest.createAccount('acc2');
        insert acc2;
        
        Account_Link_Request__c alr = new Account_Link_Request__c();
        alr.parent__c = acc.Id;
        alr.child__c = acc2.Id;
        alr.Approver__c = userinfo.getUserId();
        insert alr;
        
        Approval.ProcessSubmitRequest psr = new Approval.ProcessSubmitRequest();
        psr.setObjectId(alr.Id);
        Approval.ProcessResult result  = Approval.process(psr);
        Set<ID> set_id = new Set<ID>();
        set_id.add(result.getInstanceId());
        ProcessInstanceWorkitem wi = [select id,SystemModstamp from ProcessInstanceWorkitem where ProcessInstanceId =: result.getInstanceId() limit 1];
        
        List<W3_Task_Sync_Fail__c> list_syncFail = new List<W3_Task_Sync_Fail__c>();
        W3_Task_Sync_Fail__c syncFail1 = new W3_Task_Sync_Fail__c();
        syncFail1.ActionType__c = 'add';
        syncFail1.SyncStatus__c = 'Open';
        syncFail1.applicant__c = 'test';
        syncFail1.appName__c = 'test';
        syncFail1.appURL__c = 'https:test.test.com';
        syncFail1.ErrorMessage__c = 'error';
        syncFail1.handler__c = 'zhangsan w323232';
        syncFail1.ProcessInstanceId__c = result.getInstanceId();
        syncFail1.reserve3__c = 'sdfsd';
        syncFail1.taskDesc__c = 'test';
        syncFail1.taskURL__c = 'https://wefwe.wefe.com';
        syncFail1.taskUUID__c = 'ewfdf';
        syncFail1.time_x__c = wi.SystemModstamp.format('YYYY-MM-DD hh:mm:ss');
        syncFail1.type_x__c = 'wef';
        syncFail1.WorkItem_Step_Id__c = wi.Id;
        list_syncFail.add(syncFail1);
        W3_Task_Sync_Fail__c syncFail2 = new W3_Task_Sync_Fail__c();
        syncFail2.ActionType__c = 'del';
        syncFail2.SyncStatus__c = 'Open';
        syncFail2.applicant__c = 'test';
        syncFail2.appName__c = 'test';
        syncFail2.appURL__c = 'https:test.test.com';
        syncFail2.ErrorMessage__c = 'error';
        syncFail2.handler__c = 'zhangsan w323232';
        syncFail2.ProcessInstanceId__c = result.getInstanceId();
        syncFail2.reserve3__c = 'sdfsd';
        syncFail2.taskDesc__c = 'test';
        syncFail2.taskURL__c = 'https://wefwe.wefe.com';
        syncFail2.taskUUID__c = 'ewfdf';
        syncFail2.time_x__c = '2014-09-10 20:32:30';
        syncFail2.type_x__c = 'wef';
        syncFail2.WorkItem_Step_Id__c = null;
        list_syncFail.add(syncFail2);
        insert list_syncFail;
        
        system.Test.startTest();
        W3Task_ManualSyncBatch batch = new W3Task_ManualSyncBatch(list_syncFail);
        database.executeBatch(batch, 2);
        system.Test.stopTest();
    }
    
    static testMethod void testInitBatch() {
    	// TO DO: implement unit test
        Account acc = Lepus_EIPCalloutServiceTest.createAccount('acc1');
        acc.Inactive_NA__c = false;
        acc.Is_Named_Account__c = true;
        //acc.HQ_Parent_Account_Name__c = acc.Name;
        acc.RecordTypeId = CONSTANTS.HUAWEICHINACUSTOMERRECORDTYPE;
        insert acc;
        Account acc2 = Lepus_EIPCalloutServiceTest.createAccount('acc2');
        insert acc2;
        
        Account_Link_Request__c alr = new Account_Link_Request__c();
        alr.parent__c = acc.Id;
        alr.child__c = acc2.Id;
        alr.Approver__c = userinfo.getUserId();
        insert alr;
        
        Approval.ProcessSubmitRequest psr = new Approval.ProcessSubmitRequest();
        psr.setObjectId(alr.Id);
        Approval.ProcessResult result  = Approval.process(psr);
        
        system.Test.startTest();
        W3Task_initHistoryData hd = new W3Task_initHistoryData(datetime.now().addMonths(-1) , datetime.now());
        W3Task_initHistoryData hd1 = new W3Task_initHistoryData();
        database.executeBatch(hd1,3);
        
        system.Test.stopTest();
    }
}