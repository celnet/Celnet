/**
 * Author : Sunny
 * 批量修改后盘点挥发罐
 * 
 */
public class Ctrl_StockTackingBatchPage {
	List<ID> list_VapInfoIds = new List<ID>();
	public List<StockChecking__c> list_StockChecking{get;set;}
    private Boolean blnIsSaved;
    public Ctrl_StockTackingBatchPage (Apexpages.Standardsetcontroller SetController){
    	blnIsSaved = false ;
    	for(SObject sObj : SetController.getSelected()){
    		list_VapInfoIds.add(sObj.Id);
    	}
    	if(list_VapInfoIds.size() <= 0){
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '请选择需要修改后盘点的挥发罐') ;            
            ApexPages.addMessage(msg) ;
            return ;
    	}
    	this.initStockCheckingInfo();
    }
    private void initStockCheckingInfo(){
    	list_StockChecking = new List<StockChecking__c>();
    	for(VaporizerInfo__c VapInfo : [Select Id,Hospital__c,location__c,OperationRoom_No__c,Name From VaporizerInfo__c Where Id in: list_VapInfoIds]){
            StockChecking__c StockChecking = new StockChecking__c();
            StockChecking.ConfirmStatus__c = '未确认';
            StockChecking.VaporizerInfo__c = VapInfo.Id ;
            //StockChecking.VaporizerInfo__r.Name = VapInfo.Name ;
            StockChecking.Hospital__c = VapInfo.Hospital__c;
            StockChecking.Location__c = VapInfo.location__c;
            StockChecking.OperationRoom_No__c = VapInfo.OperationRoom_No__c;
            StockChecking.StockCheckingDate__c = date.today();
            list_StockChecking.add(StockChecking);
    	}
    }
    public void SaveStockChecking(){
    	if(blnIsSaved){
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '请勿重复保存。') ;            
            ApexPages.addMessage(msg) ;
            return ;
    	}
    	if(list_StockChecking==null || list_StockChecking.size() == 0){
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '没有课保存的数据。') ;            
            ApexPages.addMessage(msg) ;
            return ;
    	}
    	try{
            insert this.list_StockChecking ;
        }catch(system.Dmlexception de ){
            String strErr = '';
            for (Integer i = 0; i < de.getNumDml(); i++) {
                // Process exception here
                System.debug(de.getDmlMessage(i)); 
                if(de.getDmlMessage(i) != null && de.getDmlMessage(i) != ''){
                    strErr = de.getDmlMessage(i) ;
                }
            }
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, strErr) ;            
            ApexPages.addMessage(msg) ;
            return ;
        }
        List<Approval.Processsubmitrequest> list_req = new List<Approval.Processsubmitrequest>();
        for(StockChecking__c stockCheck : this.list_StockChecking){
            Approval.Processsubmitrequest req = new Approval.Processsubmitrequest();
            req.setObjectId(stockCheck.Id);
            
            list_req.add(req);
        }
        List<Approval.Processresult> result = Approval.process(list_req);
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM , '盘点成功。已提交给管理员进行审批。') ;            
        ApexPages.addMessage(msg) ;
        blnIsSaved = true;
    }
}