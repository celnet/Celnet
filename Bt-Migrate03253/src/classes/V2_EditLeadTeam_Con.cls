/* 
 * Author: Sunny
 * Created on: 2011-12-26
 * Description: 
 *修改日期：2012-1-11
 *修改人：scott
 *新加功能，保存并提交审批
 */
public without sharing class V2_EditLeadTeam_Con 
{
	public V2_Lead_Team__c objVLeadTeam {get;set;}
	public boolean blnIsMesShow{get;set;}
	public String strMessage{get;set;}
	
	private Lead objLead ;
	public String Month{get;set;}
	public String Year{get;set;}
	private Set<ID> set_UserIds = new Set<ID>() ;
	public List<SelectOption> ListYears{get;set;}
	
	//整个页面不可用
	public Boolean Isdisabled{get;set;}
	
	public List<SelectOption> getListMonths()
    {
        list<SelectOption> options=new list<SelectOption>();
        options.add(new SelectOption('1','1'));options.add(new SelectOption('2','2'));
        options.add(new SelectOption('3','3'));options.add(new SelectOption('4','4'));
        options.add(new SelectOption('5','5'));options.add(new SelectOption('6','6'));
        options.add(new SelectOption('7','7'));options.add(new SelectOption('8','8'));
        options.add(new SelectOption('9','9'));options.add(new SelectOption('10','10'));
        options.add(new SelectOption('11','11'));options.add(new SelectOption('12','12'));
        return options;
    }
	
	public V2_EditLeadTeam_Con(ApexPages.StandardController controller)
	{
		//2011-1-11
		Isdisabled= false;
		//取要操作的小组
		objVLeadTeam = [select id,V2_IsSyn__c,V2_User__c,V2_Lead__c,V2_Effective_Month__c,V2_Effective_Year__c from V2_Lead_Team__c where id =: controller.getId()] ;
		//取自己的下属
		V2_UtilClass cls = new V2_UtilClass();
		set_UserIds = cls.GetUserSubIds();
		//判断是否是自己的下属
		if(!set_UserIds.contains(objVLeadTeam.V2_User__c))
		{
				Isdisabled= true;
				blnIsMesShow = true ;
				strMessage = '您只能设置您的下属。' ;
				return ;
		}
		//判断是否已经同步
		if(objVLeadTeam.V2_IsSyn__c)
		{
			Isdisabled= true;
			blnIsMesShow = true ;
			strMessage = '您不能修改一个已经被同步到客户上的小组成员。' ;
		}
		
		objLead = [select id,V2_ApprovalStauts__c from Lead where id=: objVleadTeam.V2_Lead__c] ;
		if(objLead.V2_ApprovalStauts__c == '审批中')
		{
			Isdisabled= true;
			blnIsMesShow = true ;
			strMessage = '审批中，禁止修改小组成员。' ;
			//2011-1-11
		}
		//init year
		ListYears = new  List<SelectOption>();
        ListYears.add(new SelectOption(String.valueOf(date.today().addYears(-1).year()),String.valueOf(date.today().addYears(-1).year())));
        ListYears.add(new SelectOption(String.valueOf(date.today().year()),String.valueOf(date.today().year())));
        ListYears.add(new SelectOption(String.valueOf(date.today().addYears(1).year()),String.valueOf(date.today().addYears(1).year())));
		//init current year & month
		Year = objVLeadTeam.V2_Effective_Year__c;
        Month = objVLeadTeam.V2_Effective_Month__c;
	}
	public PageReference doCancel()
	{
		return new PageReference('/'+objVLeadTeam.V2_Lead__c) ;
	}

	//2012-1-11新加功能，提交并审批。
	public PageReference saveChange2()
	{

			Set<String> set_User = new Set<String>() ;
			for(V2_Lead_Team__c objVLT :  [select id,V2_User__c from V2_Lead_Team__c where V2_Lead__c =: objVLeadTeam.V2_Lead__c And V2_IsSyn__c = false])
			{
				if(objVLT.Id == objVLeadTeam.Id)
				{
					continue ;	
				}
				set_User.add(objVLT.V2_User__c) ;
			}
			if(!set_UserIds.contains(objVLeadTeam.V2_User__c))
			{
				objVLeadTeam.V2_User__c.addError('您只能设置您的下属。') ;
				return null ;
			}
			if(set_User.contains(objVLeadTeam.V2_User__c))
			{
				objVLeadTeam.V2_User__c.addError('已存在该小组成员。') ;
				return null ;
			}
			objVLeadTeam.V2_Effective_Year__c = this.Year ;
			objVLeadTeam.V2_Effective_Month__c = this.Month ;
			objLead.V2_ApprovalStauts__c = '待审批' ;
			update objVLeadTeam ;
			update objLead ;
			
			//提交审批
			Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
	        req.setObjectId(objVLeadTeam.V2_Lead__c);//客户联系人或自定义对象
	       	Approval.ProcessResult result = Approval.process(req);
		
		return new PageReference('/'+objVLeadTeam.V2_Lead__c) ;
	}
	//测试
	static testMethod void TestPage() 
	{
		//----------------New UserRole ------------------
    	//List<UserRole> list_userRole = new List<UserRole >() ;
    	UserRole objUserRole = new UserRole() ;
    	objUserRole.Name = 'Renal-Rep-大上海-PD-Rep(陈喆令)' ;
    	insert objUserRole ;
    	UserRole objUserRole2 = new UserRole() ;
    	objUserRole2.Name = 'Renal-Rep-大上海-PB-Rep(陈喆令)' ;
    	objUserRole2.ParentRoleId = objUserRole.Id ;
    	insert objUserRole2 ;
		//----------------Create User-------------
    	List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.UserRoleId = objUserRole2.Id ;
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	use2.UserRoleId = objUserRole2.Id ;
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.UserRoleId = objUserRole.Id ;
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
    	use3.ManagerId = use2.Id ;
    	update use3 ;
    	//-------------New Lead------------------
    	List<Lead> list_lead = new List<Lead>() ;
    	Lead objLead = new Lead() ;
    	objLead.LastName = 'testlead' ;
    	objLead.FirstName = 'T' ;
    	objLead.Company = 'testcom' ;
    	objLead.V2_Mid__c = 'poiuuiop' ;
    	objLead.V2_ApprovalStauts__c = '待审批' ;
    	list_lead.add(objLead) ;
    	insert list_lead ;
    	//-----------------New Lead Team-----------------------
    	List<V2_Lead_Team__c> list_leadTeam = new List<V2_Lead_Team__c>();
    	V2_Lead_Team__c objLeadTeam1 = new V2_Lead_Team__c() ;
    	objLeadTeam1.V2_User__c = use1.Id ;
    	objLeadTeam1.V2_Lead__c = objLead.Id ;
    	list_leadTeam.add(objLeadTeam1) ;
    	insert list_leadTeam ;
    	//------------------Start Test-----------------------
    	system.test.startTest() ;
    	system.runAs(use3)
    	{
    		ApexPages.StandardController STController = new ApexPages.StandardController(objLeadTeam1);
			ApexPages.currentPage().getParameters().put('camid', objLead.Id);
			V2_EditLeadTeam_Con EditLeadTeam = new V2_EditLeadTeam_Con(STController);
			EditLeadTeam.getListMonths() ;
			
			
			EditLeadTeam.saveChange2() ;
    	}
    	system.test.stopTest() ;
	}
	static testMethod void TestPage2() 
	{
		//----------------New UserRole ------------------
    	//List<UserRole> list_userRole = new List<UserRole >() ;
    	UserRole objUserRole = new UserRole() ;
    	objUserRole.Name = 'Renal-Rep-大上海-PD-Rep(陈喆令)' ;
    	insert objUserRole ;
    	UserRole objUserRole2 = new UserRole() ;
    	objUserRole2.Name = 'Renal-Rep-大上海-PB-Rep(陈喆令)' ;
    	objUserRole2.ParentRoleId = objUserRole.Id ;
    	insert objUserRole2 ;
		//----------------Create User-------------
    	List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.UserRoleId = objUserRole2.Id ;
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	use2.UserRoleId = objUserRole2.Id ;
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.UserRoleId = objUserRole.Id ;
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
    	use3.ManagerId = use2.Id ;
    	update use3 ;
    	//-------------New Lead------------------
    	List<Lead> list_lead = new List<Lead>() ;
    	Lead objLead = new Lead() ;
    	objLead.LastName = 'testlead' ;
    	objLead.FirstName = 'T' ;
    	objLead.Company = 'testcom' ;
    	objLead.V2_Mid__c = 'poiuuiop' ;
    	objLead.V2_ApprovalStauts__c = '待审批' ;
    	list_lead.add(objLead) ;
    	insert list_lead ;
    	//-----------------New Lead Team-----------------------
    	List<V2_Lead_Team__c> list_leadTeam = new List<V2_Lead_Team__c>();
    	V2_Lead_Team__c objLeadTeam1 = new V2_Lead_Team__c() ;
    	objLeadTeam1.V2_User__c = use3.Id ;
    	objLeadTeam1.V2_Lead__c = objLead.Id ;
    	list_leadTeam.add(objLeadTeam1) ;
    	insert list_leadTeam ;
    	//------------------Start Test-----------------------
    	system.test.startTest() ;
    	system.runAs(use1)
    	{
    		ApexPages.StandardController STController = new ApexPages.StandardController(objLeadTeam1);
			ApexPages.currentPage().getParameters().put('camid', objLead.Id);
			V2_EditLeadTeam_Con EditLeadTeam = new V2_EditLeadTeam_Con(STController);
			EditLeadTeam.getListMonths() ;
			
			
			EditLeadTeam.saveChange2() ;
    	}
    	system.test.stopTest() ;
	}
}