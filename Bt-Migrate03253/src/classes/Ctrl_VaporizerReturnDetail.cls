/**
 * 作者:Bill
 * 说明：创建发货明细的控制类
**/ 
public class Ctrl_VaporizerReturnDetail {
	//退回申请ID
	private ID returnApplyId;
	//保存按钮是否显示
	public boolean IsEnable{get;set;}
	//挥发罐
	public List<AppDetialWrapper> list_Vap{get;set;}
	private double sumClick = 0;
	public Vaporizer_ReturnAndMainten_Detail__c vapReturnDetail {get;set;} 
	
	public Ctrl_VaporizerReturnDetail(apexpages.Standardcontroller controller)
	{
		vapReturnDetail = (Vaporizer_ReturnAndMainten_Detail__c)controller.getRecord();
		returnApplyId = vapReturnDetail.Vaporizer_ReturnAndMainten__c;
		list_Vap = new List<AppDetialWrapper>();
		vapReturnDetail = new Vaporizer_ReturnAndMainten_Detail__c(); 
		vapReturnDetail.Vaporizer_ReturnAndMainten__c = returnApplyId;
		IsEnable = true;
		initVap(returnApplyId);
	}

    private void initVap(ID returnApplyId){
    	list_Vap.clear();
    	ID hospitalId;
    	
    	List<Vaporizer_ReturnAndMainten__c> list_return = [Select v.Id, v.Approve_Result__c, v.Hospital__c From Vaporizer_ReturnAndMainten__c v where Id =:returnApplyId];
    	if(list_return != null && list_return.size()>0)
    	{
    		if(list_return[0].Approve_Result__c =='审批中' || list_return[0].Approve_Result__c =='通过' || list_return[0].Approve_Result__c =='全部完成')
    		{
    			IsEnable = false;
    			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '退回申请的审批状态为审批中、通过或全部完成，不允许操作') ;            
	            ApexPages.addMessage(msg) ;
    		}
    		hospitalId = list_return[0].Hospital__c;
    	}
    	//客户下面的所有挥发罐
    	Set<ID> set_vapIds =  new Set<ID>();
        for(VaporizerInfo__c vap : [Select v.Id, v.VaporType__c, v.VaporInterface__c, v.VaporBrand__c, v.OperationRoom_No__c, v.Name, v.AppProduct__c From VaporizerInfo__c v where v.Hospital__c =: hospitalId and v.location__c = '医院' and v.Status__c = '使用' and v.MissApply__c = false]){
    		set_vapIds.add(vap.Id);
    	}
    	for(Vaporizer_ReturnAndMainten_Detail__c vapReDetail : [Select v.VaporizerInfo__c, v.Id From Vaporizer_ReturnAndMainten_Detail__c v where VaporizerInfo__c in:set_vapIds and v.IsDelivered__c = false])
    	{
    		if(set_vapIds.contains(vapReDetail.VaporizerInfo__c))
    		{
    			set_vapIds.remove(vapReDetail.VaporizerInfo__c);
    		}
    	}
    	for(VaporizerInfo__c vap1 : [Select v.Id, v.VaporType__c, v.VaporInterface__c, v.VaporBrand__c, v.OperationRoom_No__c, v.Name, v.AppProduct__c From VaporizerInfo__c v where Id =: set_vapIds]){
    		AppDetialWrapper adw = new AppDetialWrapper();
    		adw.blnSelected = false;
    		adw.vaporizerInfo = vap1;
    		list_Vap.add(adw);
    	}
    }
    
    //保存
    public PageReference SaveVaporizerInfo()
    {
    	ID vapId;
    	double selectSum = 0;
    	for(AppDetialWrapper vapWra : list_Vap){
    		if(vapWra.blnSelected == true){
    			vapId = vapWra.vaporizerInfo.Id;
    			selectSum++;
    		}
    	}
    	if(selectSum != 1)
    	{
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '只能选择一个挥发罐，不能多选') ;            
	        ApexPages.addMessage(msg) ;
	        return null;
    	}else{
    	   sumClick++;
    	   Vaporizer_ReturnAndMainten_Detail__c rerunDetail = new Vaporizer_ReturnAndMainten_Detail__c();
    	   rerunDetail.VaporizerInfo__c = vapId;
    	   rerunDetail.Operate__c = vapReturnDetail.Operate__c;
    	   rerunDetail.DamagedParts__c = vapReturnDetail.DamagedParts__c;
    	   rerunDetail.ReturnDetails__c = vapReturnDetail.ReturnDetails__c;
    	   rerunDetail.ReqireNewOne__c = vapReturnDetail.ReqireNewOne__c;
    	   rerunDetail.NewAppProduct__c = vapReturnDetail.NewAppProduct__c;
    	   rerunDetail.OperationRoom_No__c = vapReturnDetail.OperationRoom_No__c;
    	   rerunDetail.NewVaporBrand__c = vapReturnDetail.NewVaporBrand__c;
    	   rerunDetail.Vaporizer_ReturnAndMainten__c = returnApplyId;
    	   rerunDetail.NewVaporType__c = vapReturnDetail.NewVaporType__c;
    	   rerunDetail.NewVaporInterface__c = vapReturnDetail.NewVaporInterface__c;
    	   system.debug(vapReturnDetail.ReqireNewOne__c+'klm');
    	   if(vapReturnDetail.ReqireNewOne__c)
    	   {
	    	   	if(vapReturnDetail.NewAppProduct__c == null || vapReturnDetail.NewAppProduct__c =='')
	    	   	{
	    	   		vapReturnDetail.NewAppProduct__c.addError('必须输入一个值');
	    	   		return null;
	    	   	}
	    	   	if(vapReturnDetail.NewVaporBrand__c == null || vapReturnDetail.NewVaporBrand__c =='')
	    	   	{
	    	   		vapReturnDetail.NewVaporBrand__c.addError('必须输入一个值');
	    	   		return null;
	    	   	}
	    	   	if(vapReturnDetail.NewVaporType__c == null || vapReturnDetail.NewVaporType__c =='')
	    	   	{
	    	   		vapReturnDetail.NewVaporType__c.addError('必须输入一个值');
	    	   		return null;
	    	   	    
	    	   	}
	    	   	if(vapReturnDetail.NewVaporInterface__c == null || vapReturnDetail.NewVaporInterface__c =='')
	    	   	{
	    	   		vapReturnDetail.NewVaporInterface__c.addError('必须输入一个值');
	    	   		return null;
	    	   	}
    	   }
    	   insert rerunDetail;
    	   PageReference pageRef = new PageReference(System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+returnApplyId);
           return pageRef;
    	}
    }
    
    public class AppDetialWrapper{
        public boolean blnSelected{get;set;}
        public VaporizerInfo__c vaporizerInfo{get;set;}
    }
    
    
    static testMethod void MyTest() 
    {
    	//客户
    	Account acc = new Account();
    	acc.Name = 'bill';
    	insert acc;
    	
    	//挥发罐
    	VaporizerInfo__c vap = new VaporizerInfo__c();
    	vap.Name = 'huifaguanceshi';
    	vap.Status__c = '使用';
    	vap.location__c = '医院';
    	vap.Hospital__c = acc.Id;
    	insert vap;
    	VaporizerInfo__c vap1 = new VaporizerInfo__c();
    	vap1.Name = 'huifaguanshiyong';
    	vap1.Status__c = '使用';
    	vap1.location__c = '医院';
    	vap1.Hospital__c = acc.Id;
    	insert vap1;
    	
    	//退回申请
    	Vaporizer_ReturnAndMainten__c vapReturn = new Vaporizer_ReturnAndMainten__c();
    	vapReturn.Hospital__c = acc.Id;
    	insert vapReturn;
    
        //退回明细
    	Vaporizer_ReturnAndMainten_Detail__c vapReturnDetail = new Vaporizer_ReturnAndMainten_Detail__c();
        vapReturnDetail.Vaporizer_ReturnAndMainten__c = vapReturn.Id;
    	//vapReturnDetail.VaporizerInfo__c = vap.Id;
    	
    	system.Test.startTest();
    	apexpages.Standardcontroller controller = new apexpages.Standardcontroller(vapReturnDetail);
    	Ctrl_VaporizerReturnDetail vapRet = new Ctrl_VaporizerReturnDetail(controller);
    	//vapRet.initVap(vapReturn.Id);
    	vapRet.list_Vap[0].blnSelected = true;
    	vapReturnDetail.ActionType__c = '维修';
    	vapReturnDetail.Operate__c = 'A楼101室';
    	vapReturnDetail.DamagedParts__c = '刻度转盘';
    	vapReturnDetail.ReturnDetails__c = '使用损坏';
    	vapReturnDetail.ReqireNewOne__c = true;
    	vapReturnDetail.NewAppProduct__c = '七氟丸';
    	vapReturnDetail.NewVaporBrand__c = 'V2000';
    	vapReturnDetail.NewVaporInterface__c = 'Drager';
    	vapReturnDetail.NewVaporType__c = 'OHMEDA';
    	vapRet.SaveVaporizerInfo();
    	
    	vapRet.list_Vap[0].blnSelected = true;
    	vapRet.list_Vap[1].blnSelected = true;
    	vapRet.SaveVaporizerInfo();
    	system.Test.stopTest();
    	
    }
}