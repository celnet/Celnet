/**
 * Author : Sunny
 * 
**/
public class Ctrl_StockTackingPage {
	public StockChecking__c StockChecking{get;set;}
	private VaporizerInfo__c VapInfo;
	private Boolean blnIsSaved;
    public Ctrl_StockTackingPage(Apexpages.Standardcontroller controller){
    	blnIsSaved = false;
    	VapInfo = [Select Id,Hospital__c,location__c,OperationRoom_No__c From VaporizerInfo__c Where Id =: controller.getId()];
    	this.initStockCheckingInfo();
    }
    private void initStockCheckingInfo(){
    	this.StockChecking = new StockChecking__c();
    	this.StockChecking.ConfirmStatus__c = '未确认';
        this.StockChecking.VaporizerInfo__c = this.VapInfo.Id ;
        this.StockChecking.Hospital__c = this.VapInfo.Hospital__c;
        this.StockChecking.Location__c = this.VapInfo.location__c;
        this.StockChecking.OperationRoom_No__c = this.VapInfo.OperationRoom_No__c;
        this.StockChecking.StockCheckingDate__c = date.today();
    }
    public void SaveStockChecking(){
    	if(blnIsSaved){
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '请勿重复保存。') ;            
            ApexPages.addMessage(msg) ;
            return ;
    	}
    	if(this.StockChecking.Location__c != this.VapInfo.location__c || this.StockChecking.OperationRoom_No__c != this.VapInfo.OperationRoom_No__c || this.StockChecking.Hospital__c != this.VapInfo.Hospital__c){
    		try{
    			insert this.StockChecking ;
    		}catch(system.Dmlexception de ){
    			String strErr = '';
				for (Integer i = 0; i < de.getNumDml(); i++) {
					// Process exception here
					System.debug(de.getDmlMessage(i)); 
					if(de.getDmlMessage(i) != null && de.getDmlMessage(i) != ''){
						strErr = de.getDmlMessage(i) ;
					}
				}
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, strErr) ;            
                ApexPages.addMessage(msg) ;
                return ;
    		}
    		Approval.Processsubmitrequest req = new Approval.Processsubmitrequest();
            req.setObjectId(this.StockChecking.Id);
            Approval.Processresult result = Approval.process(req);
            if(!result.isSuccess()){
            	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '提交审批失败，请联系管理员。') ;            
                ApexPages.addMessage(msg) ;
                return ;
            }
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM , '盘点成功。已提交给管理员进行审批。') ;            
            ApexPages.addMessage(msg) ;
            blnIsSaved = true;
    	}else{
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '检测到您未修改任何内容，若您不需要修改挥发罐信息，请您关掉此页面，重新选择直接盘点。') ;            
            ApexPages.addMessage(msg) ;
    	}
    }
}