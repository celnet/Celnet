public class WeixinException extends Exception
{
	WeixinError error = new WeixinError();
	public class WeixinError
	{
		public string errcode;
		public string errmsg;
	}
	public WeixinException(HttpResponse res)
	{
		this.error = (WeixinError)JSON.deserializeStrict(res.getBody(),WeixinError.class);
	}
}