public class WeixinHttpClient 
{
	public string get(List<PostParameter> params)
	{
		return this.execute('GET', this.getURL(params));
	}
	
	public string post(List<PostParameter> params)
	{
		return this.execute('POST', this.getURL(params));
	}   
	public string post(List<PostParameter> params,string body)
	{
		return this.execute('POST', this.getURL(params),body);
	}
	public string execute(string method,string url)
	{
		Http h = new Http();	//创建Http对象
		HttpRequest req = new HttpRequest();	//创建HttpRequest对象
		req.setMethod(method);		//设置请求的方法
		req.setTimeout(60000);		//设置超时时间 
		req.setEndpoint(url);		//设置请求的url地址
		HttpResponse response = h.send(req);		//发送请求，返回HttpResponse
		system.debug('*****************res******************'+response.getBody());
		if(response.getStatus() == 'OK' && response.getStatusCode() == 200)
		{
			return response.getBody();
		}
		else
		{	
			WeixinException exc = new WeixinException(response);
			throw exc;
		}
	}
	public string execute(string method,string url,string body)
	{
		Http h = new Http();	//创建Http对象
		HttpRequest req = new HttpRequest();	//创建HttpRequest对象
		req.setMethod(method);		//设置请求的方法
		req.setTimeout(60000);		//设置超时时间 
		req.setEndpoint(url);		//设置请求的url地址
		req.setBody(body);
		HttpResponse response = h.send(req);		//发送请求，返回HttpResponse
		system.debug('*****************res******************'+response.getBody());
		if(response.getStatus() == 'OK' && response.getStatusCode() == 200)
		{
			return response.getBody();
		}
		else
		{	
			WeixinException exc = new WeixinException(response);
			throw exc;
		}
	}
	public string getURL(list<PostParameter> parameters)
	{
		WeixinClient weixinClient = new WeixinClient();
		system.debug('****baseURL***'+WeixinConfig.getValue('baseURL'));
		string url = WeixinConfig.getValue('baseURL')+methodParameters(parameters)+'?access_token='+WeixinConfig.getAccessToken('Access_Token');
		if(parameters.size()>0)
		{
			url += '&'+encodeParameters(parameters);
		}
		system.debug('/***********url*******************/'+url);
		return url; 
	}
	
	public string getAccessTokenByCode(string url , list<PostParameter> parameters)
	{
		return execute('POST',url+'?'+encodeParameters(parameters));
	}
	public string getAccessTokenByRefreshToken(string url , list<PostParameter> parameters)
	{
		return execute('POST',url+'?'+encodeParameters(parameters));
	}
	public string getAccessToken(string url , list<PostParameter> parameters)
	{
		return execute('GET',url+'?'+encodeParameters(parameters));
	}
	/*
	 * 对parameters进行encode处理
	 */
	public static String encodeParameters(List<PostParameter> postParams) 
	{
		String buf = '';
		for (Integer j = 0; j < postParams.size(); j++) 
		{
			if (j != 0) 
			{
				buf+='&';
			}
			system.debug('*******ParamsName********'+postParams[j].getName());
			system.debug('*******ParamsValue********'+postParams[j].getValue());
			buf+=EncodingUtil.urlEncode(postParams[j].getName(), 'UTF-8');
			buf+='=';
			buf+=EncodingUtil.urlEncode(postParams[j].getValue(),'UTF-8');
		}
		return buf;
	}
	/*
	 *将method从参数列表中提取出来紧跟在baseURL之后
	 */
	public static String methodParameters(List<PostParameter> postParams)
	{
		String buf = '';
		for (Integer j = 0; j < postParams.size(); j++)
		{
			if (postParams[j].getName() == 'method') 
			{
				buf = postParams[j].getValue();
				postParams.remove(j);
			}
		}
		return buf;
	}
}