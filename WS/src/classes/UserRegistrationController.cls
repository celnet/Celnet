public without sharing class UserRegistrationController 
{
	public RegisterUser__c registerUser{set;get;}
	public string userId{get;set;}
	public boolean isRegistration{get;set;}
	public UserRegistrationController()
	{
		userId = Apexpages.currentPage().getParameters().get('userId');
		registerUser = new RegisterUser__c(Id=userId);
	}
	public void Save()
	{
		try
		{
			if(registerUser.Name ==null ||registerUser.E_mail__c ==null ||registerUser.ContactPhone__c ==null )
			{
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR , '请完善注册信息') ;            
            	ApexPages.addMessage(msg); 
            	return;
			}
			registerUser.isRegister__c=true;
			update registerUser;
		}
		catch(dmlexception e)
		{
			SetErrorToPage(e);
		}
		isRegistration=true;
	}
	private void SetErrorToPage(system.Dmlexception dmlExc)
    {
        for (Integer i = 0; i < dmlExc.getNumDml(); i++) 
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR , dmlExc.getDmlMessage(i)) ;            
            ApexPages.addMessage(msg); 
   	        break;
    	}
    }
}