@RestResource(urlMapping='/Weixin/*')
global class WeixinRestApiController {
	global class Message
	{
		public string ToUserName;
		public string FromUserName;
		public string CreateTime;
		public string Content;
		public string MsgId;
		public string MsgType;
		public string Event;
		public string EventKey;
		public string Title;
		public string Description;
		public string PicUrl;
		public string Url;
	}
    @HttpGet
    global static long getWidgets() {
    	long echostr = long.valueOf(RestContext.request.params.get('echostr'));
    	string signature = RestContext.request.params.get('signature');
    	string timestamp = RestContext.request.params.get('timestamp');
    	string nonce = RestContext.request.params.get('nonce');
        return echostr;
    } 
    @HttpPost 
    global static void createNewWidget() {
    	RestRequest req = RestContext.request;
    	XmlStreamReader xsr = new XmlStreamReader(req.requestBody.toString());
    	Message rm = getReceiveMessage(xsr);
    	RegisterUser__c user = upsertWeiXinUser(rm);
    	
    	if(rm.MsgType =='text')
    	{
    		insertMessage(rm,'上行',user.Id);
    		if(rm.Content.contains('Pro'))
    		{
    			list<Opportunity> list_opp = [select StageName,OppNumber__c From Opportunity Where OppNumber__c =:rm.Content];
    			if(list_opp.size()>0)
    			{
    				rm.Content = '您好!您的业务<'+list_opp[0].OppNumber__c+'>目前状态为<'+list_opp[0].StageName+'>，详情请咨询18611111111<Fendi Bai>';
    			}
    			else
    			{
    				rm.Content = '抱歉！没有找到您查找的业务！';
    			}
	    		rm = sendMessage(rm,user.Id);
    		}
    		/*else
    		{
    			string content = 'http://weixincideatech-developer-edition.ap1.force.com/apex/CraeteProjectPage';
    			rm.Content = content;  
	    		rm.ToUserName = rm.FromUserName;
	    		rm.FromUserName = 'CideatechWeiXin';
	    		insertMessage(rm,'下行',user.Id);
    		}*/
    	}
    	else if(rm.MsgType == 'event')
    	{
    		if(rm.Event =='subscribe')
    		{
    			rm.Content = SubscribeEventMessage();  
	    		rm = sendMessage(rm,user.Id);
    		}
    		else if(rm.Event =='CLICK')
    		{
    			if(rm.EventKey=='I_WANT_BUY')
	    		{
		    		rm = BuyEventMessage(rm,user.Id); 
	    			RestResponse res = RestContext.response;
			    	res.addHeader('Content-Type','text/xml');
			    	if(rm.PicUrl != null)
			    	{
			    		res.responseBody = Blob.valueOf(getReturnPicMessage(rm));
			    	}
			    	else
			    	{
			    		res.responseBody = Blob.valueOf(getReturnNoPicMessage(rm));
			    	}
			    	return;
	    		}
	    		else if(rm.EventKey=='I_WANT_SELL')
	    		{
	    			rm = SellEventMessage(rm,user.Id); 
	    			RestResponse res = RestContext.response;
			    	res.addHeader('Content-Type','text/xml');
			    	if(rm.PicUrl != null)
			    	{
			    		res.responseBody = Blob.valueOf(getReturnPicMessage(rm));
			    	}
			    	else
			    	{
			    		res.responseBody = Blob.valueOf(getReturnNoPicMessage(rm));
			    	}
			    	return;
	    		}
	    		else if(rm.EventKey=='I_WANT_BACKDOOR')
	    		{
		    		rm = BackDoorEventMessage(rm,user.Id); 
	    			RestResponse res = RestContext.response;
			    	res.addHeader('Content-Type','text/xml');
			    	if(rm.PicUrl != null)
			    	{
			    		res.responseBody = Blob.valueOf(getReturnPicMessage(rm));
			    	}
			    	else
			    	{
			    		res.responseBody = Blob.valueOf(getReturnNoPicMessage(rm));
			    	}
			    	return;
	    		}
	    		else if(rm.EventKey=='I_WANT_FINANCING')
	    		{
		    		rm = FinancingEeventMessage(rm,user.Id); 
	    			RestResponse res = RestContext.response;
			    	res.addHeader('Content-Type','text/xml');
			    	if(rm.PicUrl != null)
			    	{
			    		res.responseBody = Blob.valueOf(getReturnPicMessage(rm));
			    	}
			    	else
			    	{
			    		res.responseBody = Blob.valueOf(getReturnNoPicMessage(rm));
			    	}
			    	return;
	    		}
	    		else if(rm.EventKey=='NEW_SALON')
	    		{
	    			rm=SalonEventMessage(rm,user.Id);
	    			RestResponse res = RestContext.response;
			    	res.addHeader('Content-Type','text/xml');
			    	if(rm.Url != null)
			    	{
			    		res.responseBody = Blob.valueOf(getReturnPicMessage(rm));
			    	}
			    	else
			    	{
			    		res.responseBody = Blob.valueOf(getReturnNoUMessage(rm));
			    	}
			    	return;
	    		}
	    		else if(rm.EventKey=='NEW_LESSON')
	    		{
	    			rm=LessonEventMessage(rm,user.Id);
	    			RestResponse res = RestContext.response;
			    	res.addHeader('Content-Type','text/xml');
			    	if(rm.PicUrl != null)
			    	{
			    		res.responseBody = Blob.valueOf(getReturnPicMessage(rm));
			    	}
			    	else
			    	{
			    		res.responseBody = Blob.valueOf(getReturnNoUMessage(rm));
			    	}
			    	return;
	    		}
	    		else if(rm.EventKey=='SIGN_IN')
	    		{
	    			rm = SignInEventMessage(rm,user.Id); 
	    			RestResponse res = RestContext.response;
			    	res.addHeader('Content-Type','text/xml');
			    	if(rm.PicUrl != null)
			    	{
			    		res.responseBody = Blob.valueOf(getReturnPicMessage(rm));
			    	}
			    	else
			    	{
			    		res.responseBody = Blob.valueOf(getReturnNoUrlMessage(rm));
			    	}
			    	
			    	return; 
		    		//rm = sendMessage(rm,user.Id);
	    		}
	    		else if(rm.EventKey=='MY_PROJECT')
	    		{
	    			rm.Content = MyProjectEventMessage(user.Id);  
		    		rm = sendMessage(rm,user.Id);
	    		}
	    		else if(rm.EventKey=='MY_SALON')
	    		{
	    			rm.Content = MySalonEventMessage(user.Id);  
		    		rm = sendMessage(rm,user.Id);
	    		}
	    		else if(rm.EventKey=='MY_LESSON')
	    		{
	    			rm.Content = MyLessonEventMessage(user.Id);  
		    		rm = sendMessage(rm,user.Id);
	    		}
	    		else if(rm.EventKey=='MY_INFO')
	    		{
	    			rm.Content = MyInfoEventMessage(user.Id);  
		    		rm = sendMessage(rm,user.Id);
	    		}
    		}
    	}
    	if(rm.ToUserName!=null)
    	{
    		RestResponse res = RestContext.response;
	    	res.addHeader('Content-Type','text/xml');
	    	res.responseBody = Blob.valueOf(getReturnMessage(rm));
    	}
   }
   //注册信息
   global static Message SignInEventMessage(Message rm,string id)
   {
   		RegisterUser__c registerUser = [select isRegister__c,ContactPhone__c,E_mail__c,Contribution_Value__c,Level__c From RegisterUser__c Where Id =:id];
   		WeChartImageTextInfo__c itInfo = [select Title__c,Description__c,ImageUrl__c From WeChartImageTextInfo__c Where Type__c='注册' Order By CreatedDate Desc limit 1];
   		if(registerUser.isRegister__c)
   		{
   			rm.Title = itInfo.Title__c;
   			rm.Description = '您已经成功注册，您的个人信息如下\n'
		   					 +'联系电话：'+registerUser.ContactPhone__c+'\n'
		   					 +'电子邮箱：'+registerUser.E_mail__c+'\n'
		   					 +'贡献值：'+registerUser.Contribution_Value__c+'\n'
		   					 +'等级：'+registerUser.Level__c+'\n'
		   					 +'\r\n'
		   					 +'期待与您的更多合作！';
   		}
   		else
   		{
   			rm.PicUrl = 'http://'+itInfo.ImageUrl__c;
   			rm.Title = itInfo.Title__c;
   			rm.Description = '        '+itInfo.Description__c;
   			rm.Url = 'http://weixincideatech-developer-edition.ap1.force.com/apex/UserRegistration?userId='+id;
   		}
   		rm.ToUserName = rm.FromUserName;
		rm.FromUserName = 'CideatechWeiXin';
   		return rm;
   }
   //最新沙龙
   global static Message SalonEventMessage(Message rm,String id)
   {
   		list<Campaign> list_Campaign = [select Id,Name,Address__c,ImageUrl__c,PeopleNumber__c,Description,StartDate,EndDate,Type From Campaign Where Type ='沙龙' and IsActive =true and Status='未开始' Order By CreatedDate desc limit 1];
   		if(list_Campaign.size()>0)
   		{
   			rm.PicUrl = 'http://'+list_Campaign[0].ImageUrl__c;
   			//rm.PicUrl = 'http://t12.baidu.com/it/u=856262174,3512957437&fm=21&gp=0.jpg';
   			rm.Title = list_Campaign[0].Name;
   			rm.Description = list_Campaign[0].Description;
   			rm.Url = 'http://weixincideatech-developer-edition.ap1.force.com/apex/JoinSharon?userId='+id
   						+'&Id='+EncodingUtil.urlEncode(list_Campaign[0].Id,'UTF-8')
   						;
   		}
   		else
   		{
   			rm.Title = '筹备中...';
   			rm.Description = '亲，您来早了~最新沙龙正在筹备中~请您耐心等待~';
   			rm.PicUrl ='http://pic20.nipic.com/20120406/2457331_185117054374_2.jpg';
   		}
   		rm.ToUserName = rm.FromUserName;
		rm.FromUserName = 'CideatechWeiXin';
   		return rm;
   }
   //最新课程
   global static Message LessonEventMessage(Message rm,String id)
   {
   		list<Campaign> list_Campaign = [select Id,Name,Address__c,ImageUrl__c,PeopleNumber__c,Description,StartDate,EndDate,Type From Campaign Where Type ='课程' and IsActive =true and Status='未开始' Order By CreatedDate desc limit 1];
   		if(list_Campaign.size()>0)
   		{
   			rm.PicUrl = 'http://'+list_Campaign[0].ImageUrl__c;
   			//rm.PicUrl = 'http://www.allpinpai.com/UpFile/201102/2011022050548517.jpg';
   			rm.Title = list_Campaign[0].Name;
   			rm.Description = list_Campaign[0].Description;
   			rm.Url = 'http://weixincideatech-developer-edition.ap1.force.com/apex/JoinSharon?userId='+id
   						+'&Id='+EncodingUtil.urlEncode(list_Campaign[0].Id,'UTF-8')
   						;
   		}
   		else
   		{
   			rm.Title = '筹备中...';
   			rm.Description = '亲，您来早了~最新课程正在筹备中~请您耐心等待~';
   			rm.PicUrl ='http://pic20.nipic.com/20120406/2457331_185117054374_2.jpg';
   		}
   		rm.ToUserName = rm.FromUserName;
		rm.FromUserName = 'CideatechWeiXin';
   		return rm;
   }
   //我要买
   global static Message BuyEventMessage(Message rm,string id)
   {
   		RegisterUser__c ru = [select Name,ContactPhone__c,E_mail__c,Level__c,Contribution_Value__c,isRegister__c 
   							  From RegisterUser__c 
   							  Where Id =: id];
   		WeChartImageTextInfo__c itInfo = [select Title__c,Description__c,ImageUrl__c From WeChartImageTextInfo__c Where Type__c='我要买' Order By CreatedDate Desc limit 1];
   		if(ru.isRegister__c)
   		{
   			rm.PicUrl = 'http://'+itInfo.ImageUrl__c;
   			rm.Title = itInfo.Title__c;
   			rm.Description = '        '+itInfo.Description__c;
   			rm.Url = 'http://weixincideatech-developer-edition.ap1.force.com/apex/CraeteProjectPage?userId='+id+'&BuyOrSell='+EncodingUtil.urlEncode('收购 ','UTF-8');
   		}
   		else
   		{
   			rm.Title = '携手高圣，从注册开始';
   			rm.Description = '您尚未成为东方高圣注册用户！携手高圣，马上注册！';
   			rm.Url = 'http://weixincideatech-developer-edition.ap1.force.com/apex/UserRegistration?userId='+id;
   		}
   		rm.ToUserName = rm.FromUserName;
		rm.FromUserName = 'CideatechWeiXin';
   		return rm;
   }
   //我要卖
   global static Message SellEventMessage(Message rm,string id)
   {
   		RegisterUser__c ru = [select Name,ContactPhone__c,E_mail__c,Level__c,Contribution_Value__c,isRegister__c 
   							  From RegisterUser__c 
   							  Where Id =: id];
   		WeChartImageTextInfo__c itInfo = [select Title__c,Description__c,ImageUrl__c From WeChartImageTextInfo__c Where Type__c='我要卖' Order By CreatedDate Desc limit 1];
   		if(ru.isRegister__c)
   		{
   			rm.PicUrl = 'http://'+itInfo.ImageUrl__c;
   			rm.Title = itInfo.Title__c;
   			rm.Description = '        '+itInfo.Description__c;
   			rm.Url = 'http://weixincideatech-developer-edition.ap1.force.com/apex/CraeteProjectPage?userId='+id+'&BuyOrSell='+EncodingUtil.urlEncode('出售 ','UTF-8');
   		}
   		else
   		{
   			rm.Title = '携手高圣，从注册开始';
   			rm.Description = '您尚未成为东方高圣注册用户！携手高圣，马上注册！';
   			rm.Url = 'http://weixincideatech-developer-edition.ap1.force.com/apex/UserRegistration?userId='+id;
   		}
   		rm.ToUserName = rm.FromUserName;
		rm.FromUserName = 'CideatechWeiXin';
   		return rm;
   }
   //我要借壳
   global static Message BackDoorEventMessage(Message rm,string id)
   {
   		RegisterUser__c ru = [select Name,ContactPhone__c,E_mail__c,Level__c,Contribution_Value__c,isRegister__c 
   							  From RegisterUser__c 
   							  Where Id =: id];
   		WeChartImageTextInfo__c itInfo = [select Title__c,Description__c,ImageUrl__c From WeChartImageTextInfo__c Where Type__c='我要借壳' Order By CreatedDate Desc limit 1];
   		if(ru.isRegister__c)
   		{
   			rm.PicUrl = 'http://'+itInfo.ImageUrl__c;
   			rm.Title = itInfo.Title__c;
   			rm.Description = '        '+itInfo.Description__c;
   			rm.Url = 'http://weixincideatech-developer-edition.ap1.force.com/apex/CraeteProjectPage?userId='+id+'&BuyOrSell='+EncodingUtil.urlEncode('借壳 ','UTF-8');
   		}
   		else
   		{
   			rm.Title = '携手高圣，从注册开始';
   			rm.Description = '您尚未成为东方高圣注册用户！携手高圣，马上注册！';
   			rm.Url = 'http://weixincideatech-developer-edition.ap1.force.com/apex/UserRegistration?userId='+id;
   		}
   		rm.ToUserName = rm.FromUserName;
		rm.FromUserName = 'CideatechWeiXin';
   		return rm;
   }
   //我融资
   global static Message FinancingEeventMessage(Message rm,string id)
   {
   		RegisterUser__c ru = [select Name,ContactPhone__c,E_mail__c,Level__c,Contribution_Value__c,isRegister__c 
   							  From RegisterUser__c 
   							  Where Id =: id];
   		WeChartImageTextInfo__c itInfo = [select Title__c,Description__c,ImageUrl__c From WeChartImageTextInfo__c Where Type__c='我要融资' Order By CreatedDate Desc limit 1];
   		if(ru.isRegister__c)
   		{
   			rm.PicUrl = 'http://'+itInfo.ImageUrl__c;
   			rm.Title = itInfo.Title__c;
   			rm.Description = '        '+itInfo.Description__c;
   			rm.Url = 'http://weixincideatech-developer-edition.ap1.force.com/apex/CraeteProjectPage?userId='+id+'&BuyOrSell='+EncodingUtil.urlEncode('融资 ','UTF-8');
   		}
   		else
   		{
   			rm.Title = '携手高圣，从注册开始';
   			rm.Description = '您尚未成为东方高圣注册用户！携手高圣，马上注册！';
   			rm.Url = 'http://weixincideatech-developer-edition.ap1.force.com/apex/UserRegistration?userId='+id;
   		}
   		rm.ToUserName = rm.FromUserName;
		rm.FromUserName = 'CideatechWeiXin';
   		return rm;
   }
   //我的项目信息
   global static string MyProjectEventMessage(string id)
   {
   		string message ='';
   		list<Opportunity> list_Opportunity = [select Name,StageName, OppNumber__c,Buy_Or_Sell__c,RegisterUser__r.Name
						   							  From Opportunity 
						   							  Where RegisterUser__c =: id  Order By CreatedDate Desc];
   		if(list_Opportunity.size()>0)
   		{
   			message += '您好！'+list_Opportunity[0].RegisterUser__r.Name+' 先生/女士，您的项目信息如下\r\n';
   			for(Opportunity cm : list_Opportunity )
   			{
   				message +='项目名称：'+cm.Name+'\n'
   						 +'项目编号：'+cm.OppNumber__c+'\n'
	   					 +'状态：'+cm.StageName+'\n'
	   					 +'交易类型：'+cm.Buy_Or_Sell__c+'\n'
	   					 +'\r\n';
   			}
   			message +='期待与您的更多合作！';
   		}
   		else
   		{
   			message ='您尚未提交任何项目，点击【微高圣】即可提交您的第一个项目。';
   		}
   		return message;
   }
   //我的沙龙 信息
   global static string MySalonEventMessage(string id)
   {
   		string message ='';
   		list<CampaignJoiner__c> list_campaignMember = [select Campaign__r.Name,Campaign__r.Description, Campaign__r.Status,Campaign__r.Address__c,Campaign__r.PeopleNumber__c,
   														Campaign__r.StartDate,Campaign__r.EndDate,RegisterUser__r.Name
						   							  From CampaignJoiner__c 
						   							  Where RegisterUser__c=:id and Campaign__r.Type ='沙龙' and Campaign__r.IsActive = true Order By CreatedDate Desc];
   		if(list_campaignMember.size()>0)
   		{
   			message += '您好！'+list_campaignMember[0].RegisterUser__r.Name+' 先生/女士，您的沙龙信息如下\r\n';
   			for(CampaignJoiner__c cm : list_campaignMember )
   			{
   				message +='沙龙主题：'+cm.Campaign__r.Name+'\n'
	   					 +'状态：'+cm.Campaign__r.Status+'\n'
	   					 +'时间：'+cm.Campaign__r.StartDate.format()+'~'+cm.Campaign__r.EndDate.format()+'\n'
	   					 +'地点：'+cm.Campaign__r.Address__c+'\n'
	   					 +'\r\n';
   			}
   			message +='期待您的更多参与！';
   		}
   		else
   		{
   			message ='您尚未预约任何沙龙，请点击【最新沙龙】关注我们的沙龙信息。';
   		}
   		return message;
   }
   //我的课程 信息
   global static string MyLessonEventMessage(string id)
   {
   		string message ='';
   		list<CampaignJoiner__c> list_campaignMember = [select Campaign__r.Name,Campaign__r.Description, Campaign__r.Status,Campaign__r.Address__c,Campaign__r.PeopleNumber__c,
   														Campaign__r.StartDate,Campaign__r.EndDate,RegisterUser__r.Name
						   							  From CampaignJoiner__c 
						   							  Where RegisterUser__c =:id and Campaign__r.Type ='课程' and Campaign__r.IsActive = true Order By CreatedDate Desc];
   		if(list_campaignMember.size()>0)
   		{
   			message += '您好！'+list_campaignMember[0].RegisterUser__r.Name+' 先生/女士，您的课程信息如下\r\n';
   			for(CampaignJoiner__c cm : list_campaignMember )
   			{
   				message +='课程题目：'+cm.Campaign__r.Name+'\n'
	   					 +'状态：'+cm.Campaign__r.Status+'\n'
	   					 +'时间：'+cm.Campaign__r.StartDate.format()+'~'+cm.Campaign__r.EndDate.format()+'\n'
	   					 +'地点：'+cm.Campaign__r.Address__c+'\n'
	   					 +'\r\n';
   			}
   			message +='期待您的更多参与！';
   		}
   		else
   		{
   			message ='您尚未预约任何课程，请点击【最新课程】关注我们的课程信息。';
   		}
   		return message;
   }
   //我的信息
   global static string MyInfoEventMessage(string id)
   {
   		string message ='';
   		RegisterUser__c ru = [select Name,ContactPhone__c,E_mail__c,Level__c,Contribution_Value__c,isRegister__c 
   							  From RegisterUser__c 
   							  Where Id =: id];
   		if(ru.isRegister__c)
   		{
   			message = '您好！'+ru.Name+' 先生/女士，您的个人信息如下\n'
   					 +'联系电话：'+ru.ContactPhone__c+'\n'
   					 +'电子邮箱：'+ru.E_mail__c+'\n'
   					 +'贡献值：'+ru.Contribution_Value__c+'\n'
   					 +'等级：'+ru.Level__c+'\n'
   					 +'\r\n'
   					 +'期待与您的更多合作！';
   		}
   		else
   		{
   			message ='您尚未成为东方高圣注册用户！携手高圣，<a href="http://weixincideatech-developer-edition.ap1.force.com/apex/UserRegistration?userId='+id+'">马上注册！</a>';
   		}
   		return message;
   }
   //关注事件推送
   global static string SubscribeEventMessage()
   {
   		string enter = '\n';
   		string content = '微信POC实现功能：'+enter
						+'1.将微信用户和雨花石服务号的文本类消息互动存储在SF系统中。'+enter
						+'2.当微信用户向雨花石服务号推送消息时，记录用户的OpenId（微信用户的加密微信号）'+enter
						+'3.当微信用户点击雨花石服务号的菜单项时，弹出业务表单窗口用于收集客户信息。用户填完信息后，系统会自动根据客户信息创建潜在客户、客户、联系人和业务机会。并返回业务单号供用户查询业务信息。'+enter
						+'4.当用户向雨花石服务号发送业务单号（如：Pro-000064）时，系统会自动通过微信向用户发送该业务的当前状态等信息。';
   		return content;
   }
   //发送消息处理
   global static Message sendMessage(Message rm,Id userId)
   {	
		rm.ToUserName = rm.FromUserName;
		rm.FromUserName = 'CideatechWeiXin';
		insertMessage(rm,'下行',userId);
		return rm;
   }
   //当接收到用户消息的时候为用户新建记录，如果存在则更新
   global static RegisterUser__c upsertWeiXinUser(Message rm)
   {
   		RegisterUser__c user = new RegisterUser__c();
   		user.OpenId__c = rm.FromUserName;
   		upsert user OpenId__c;
   		return user;
   }
   //将接收和发送的消息存入数据库
   global static void insertMessage(Message rm,string Category,string UserId)
   {
   		Weixin_Message__c w = new Weixin_Message__c();
		w.ToUserName__c = rm.ToUserName;
	    w.FromUserName__c = rm.FromUserName;
	    DateTime dt = DateTime.newInstance(1970, 1, 1,8,0,0);
	    w.CreateTime__c = dt.addSeconds(integer.valueOf(rm.CreateTime));
	    w.Content__c = rm.Content;
	    w.MsgId__c = rm.MsgId;
	    w.MsgType__c = rm.MsgType;
	    w.Category__c = Category;
	  //  w.RegisterUser__c = UserId;
	    insert w;
   }
   //发送被动响应消息XML构造
   global static string getReturnMessage(Message rm)
   {
   		XmlStreamWriter w = new XmlStreamWriter();
		w.writeStartElement(null,'xml',null);
		w.writeStartElement(null,'ToUserName',null);
		w.writeCData(rm.ToUserName);
		w.writeEndElement();//end ToUserName
		w.writeStartElement(null,'FromUserName',null);
		w.writeCData(rm.FromUserName);
		w.writeEndElement();//end FromUserName
		w.writeStartElement(null,'CreateTime',null);
		w.writeCharacters(rm.CreateTime);
		w.writeEndElement();//end CreateTime
		w.writeStartElement(null,'MsgType',null);
		w.writeCData('text');
		w.writeEndElement();//end MsgType
		w.writeStartElement(null,'Content',null);
		w.writeCData(rm.content);
		w.writeEndElement();//end Content
		w.writeEndElement(); //end xml
		String xmlOutput = w.getXmlString();
		w.close();
		return xmlOutput;
   }
   //发送被动响应图文消息XML构造
   global static string getReturnPicMessage(Message rm)
   {
   		XmlStreamWriter w = new XmlStreamWriter();
		w.writeStartElement(null,'xml',null);
			w.writeStartElement(null,'ToUserName',null);
				w.writeCData(rm.ToUserName);
			w.writeEndElement();//end ToUserName
			w.writeStartElement(null,'FromUserName',null);
				w.writeCData(rm.FromUserName);
			w.writeEndElement();//end FromUserName
			w.writeStartElement(null,'CreateTime',null);
				w.writeCharacters(rm.CreateTime);
			w.writeEndElement();//end CreateTime
			w.writeStartElement(null,'MsgType',null);
				w.writeCData('news');
			w.writeEndElement();//end MsgType
			w.writeStartElement(null,'ArticleCount',null);
				w.writeCharacters('1');
			w.writeEndElement();//end ArticleCount
			w.writeStartElement(null,'Articles',null);
				w.writeStartElement(null,'item',null);
					w.writeStartElement(null,'Title',null);
						w.writeCData(rm.Title);
					w.writeEndElement();//end Title
					w.writeStartElement(null,'Description',null);
						w.writeCData(rm.Description);
					w.writeEndElement();//end Description
					w.writeStartElement(null,'PicUrl',null);
						w.writeCharacters(rm.PicUrl);
					w.writeEndElement();//end PicUrl
					w.writeStartElement(null,'Url',null);
						w.writeCData(rm.Url);
					w.writeEndElement();//end Url
				w.writeEndElement(); //end item
			w.writeEndElement(); //end Articles
		w.writeEndElement(); //end xml
		String xmlOutput = w.getXmlString();
		w.close();
		return xmlOutput;
   }
   //发送被动响应图文消息XML构造  不带图片和URL
   global static string getReturnNoUrlMessage(Message rm)
   {
   		XmlStreamWriter w = new XmlStreamWriter();
		w.writeStartElement(null,'xml',null);
			w.writeStartElement(null,'ToUserName',null);
				w.writeCData(rm.ToUserName);
			w.writeEndElement();//end ToUserName
			w.writeStartElement(null,'FromUserName',null);
				w.writeCData(rm.FromUserName);
			w.writeEndElement();//end FromUserName
			w.writeStartElement(null,'CreateTime',null);
				w.writeCharacters(rm.CreateTime);
			w.writeEndElement();//end CreateTime
			w.writeStartElement(null,'MsgType',null);
				w.writeCData('news');
			w.writeEndElement();//end MsgType
			w.writeStartElement(null,'ArticleCount',null);
				w.writeCharacters('1');
			w.writeEndElement();//end ArticleCount
			w.writeStartElement(null,'Articles',null);
				w.writeStartElement(null,'item',null);
					w.writeStartElement(null,'Title',null);
						w.writeCData(rm.Title);
					w.writeEndElement();//end Title
					w.writeStartElement(null,'Description',null);
						w.writeCData(rm.Description);
					w.writeEndElement();//end Description
				w.writeEndElement(); //end item
			w.writeEndElement(); //end Articles
		w.writeEndElement(); //end xml
		String xmlOutput = w.getXmlString();
		w.close();
		return xmlOutput;
   }
   //发送被动响应图文消息XML构造  不带图片
   global static string getReturnNoPicMessage(Message rm)
   {
   		XmlStreamWriter w = new XmlStreamWriter();
		w.writeStartElement(null,'xml',null);
			w.writeStartElement(null,'ToUserName',null);
				w.writeCData(rm.ToUserName);
			w.writeEndElement();//end ToUserName
			w.writeStartElement(null,'FromUserName',null);
				w.writeCData(rm.FromUserName);
			w.writeEndElement();//end FromUserName
			w.writeStartElement(null,'CreateTime',null);
				w.writeCharacters(rm.CreateTime);
			w.writeEndElement();//end CreateTime
			w.writeStartElement(null,'MsgType',null);
				w.writeCData('news');
			w.writeEndElement();//end MsgType
			w.writeStartElement(null,'ArticleCount',null);
				w.writeCharacters('1');
			w.writeEndElement();//end ArticleCount
			w.writeStartElement(null,'Articles',null);
				w.writeStartElement(null,'item',null);
					w.writeStartElement(null,'Title',null);
						w.writeCData(rm.Title);
					w.writeEndElement();//end Title
					w.writeStartElement(null,'Description',null);
						w.writeCData(rm.Description);
					w.writeEndElement();//end Description
					w.writeStartElement(null,'Url',null);
						w.writeCData(rm.Url);
					w.writeEndElement();//end Url
				w.writeEndElement(); //end item
			w.writeEndElement(); //end Articles
		w.writeEndElement(); //end xml
		String xmlOutput = w.getXmlString();
		w.close();
		return xmlOutput;
   }
   //发送被动响应图文消息XML构造  不带Url
   global static string getReturnNoUMessage(Message rm)
   {
   		XmlStreamWriter w = new XmlStreamWriter();
		w.writeStartElement(null,'xml',null);
			w.writeStartElement(null,'ToUserName',null);
				w.writeCData(rm.ToUserName);
			w.writeEndElement();//end ToUserName
			w.writeStartElement(null,'FromUserName',null);
				w.writeCData(rm.FromUserName);
			w.writeEndElement();//end FromUserName
			w.writeStartElement(null,'CreateTime',null);
				w.writeCharacters(rm.CreateTime);
			w.writeEndElement();//end CreateTime
			w.writeStartElement(null,'MsgType',null);
				w.writeCData('news');
			w.writeEndElement();//end MsgType
			w.writeStartElement(null,'ArticleCount',null);
				w.writeCharacters('1');
			w.writeEndElement();//end ArticleCount
			w.writeStartElement(null,'Articles',null);
				w.writeStartElement(null,'item',null);
					w.writeStartElement(null,'Title',null);
						w.writeCData(rm.Title);
					w.writeEndElement();//end Title
					w.writeStartElement(null,'Description',null);
						w.writeCData(rm.Description);
					w.writeEndElement();//end Description
					w.writeStartElement(null,'PicUrl',null);
						w.writeCharacters(rm.PicUrl);
					w.writeEndElement();//end PicUrl
				w.writeEndElement(); //end item
			w.writeEndElement(); //end Articles
		w.writeEndElement(); //end xml
		String xmlOutput = w.getXmlString();
		w.close();
		return xmlOutput;
   }
   //接收消息XML解析
   global static Message getReceiveMessage(XmlStreamReader xsr)
   {
   		Message rm = new Message();
		while(xsr.hasNext()) {
			if(xsr.getEventType() == XmlTag.START_ELEMENT)
			{
				string localName = xsr.getLocalName();
				xsr.next();
				if(xsr.getEventType() == XmlTag.CHARACTERS)
				{
					if(localName == 'ToUserName')rm.ToUserName = xsr.getText();
					if(localName == 'FromUserName')rm.FromUserName = xsr.getText();
					if(localName == 'CreateTime')rm.CreateTime = xsr.getText();
					if(localName == 'Content')rm.Content = xsr.getText();
					if(localName == 'MsgId')rm.MsgId = xsr.getText();
					if(localName == 'MsgType')rm.MsgType = xsr.getText();
					if(localName == 'Event')rm.Event = xsr.getText();
					if(localName == 'EventKey')rm.EventKey = xsr.getText();
					xsr.next();
				}
			}
	        xsr.next();
	    }
	    return rm;
   }
   /* @HttpDelete
    global static String deleteWidgetById() {
        return 'error';
    }

    @HttpPut
    global static String updateWidget(String Id, String NewName) {
        return 'error';
    }*/
}