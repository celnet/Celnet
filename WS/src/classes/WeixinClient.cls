public class WeixinClient 
{
	WeixinHttpClient client = new WeixinHttpClient();
	public class AccessToken
	{
		public string access_token;
		public string expires_in;
	} 
	public string getAccessToken()
	{
		WeixinAccessToken__c wat = WeixinAccessToken__c.getInstance('Weixin');
		if(wat.Access_Token__c == null || wat.Expires_In__c != null || DateTime.now()>wat.Expires_In__c)
		{
			list<PostParameter> Allparameter = new list<PostParameter>();
			Allparameter.add(new PostParameter('grant_type','client_credential')); 
			Allparameter.add(new PostParameter('appid',WeixinConfig.getValue('Client_ID'))); 
			Allparameter.add(new PostParameter('secret',WeixinConfig.getValue('Client_SERCRET')));
			string res = client.getAccessToken(WeixinConfig.getValue('baseURL')+'/token',Allparameter);
			AccessToken atk = (AccessToken)JSON.deserializeStrict(res,AccessToken.class);
			wat.Access_Token__c = atk.access_token;
			wat.Expires_In__c = DateTime.now().addSeconds(integer.valueOf(atk.expires_in));
			update wat;
		}
		return wat.Access_Token__c;
	}
}