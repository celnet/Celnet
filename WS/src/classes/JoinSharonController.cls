public without sharing class JoinSharonController 
{
	public string userId;
	public string camId;
	public string JoinDate{set;get;}
	public Campaign cam{get;set;}
	public boolean isJoin{get;set;}
	public boolean hasJoin{get;set;}
	public JoinSharonController()
	{
		userId = Apexpages.currentPage().getParameters().get('userId');
   		camId = Apexpages.currentPage().getParameters().get('Id');
   		list<Campaign> list_Campaign = [select Id,Name,Address__c,ImageUrl__c,PeopleNumber__c,Description,StartDate,EndDate,Type From Campaign Where id=:camId];
		cam = list_Campaign[0];
		JoinDate = cam.StartDate.format()+'~'+cam.EndDate.format();
		list<CampaignJoiner__c> list_cj = [select id From CampaignJoiner__c Where Campaign__c =:camId and RegisterUser__c=:userId];
		if(list_cj.size()>0)
		{
			hasJoin = true;
		}
	}
	public void Join()
	{
		try
		{
			if(cam.PeopleNumber__c==0)
			{
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR , '人数已满，请期待下次活动！') ;            
            	ApexPages.addMessage(msg); 
            	return;
			}
			/*list<CampaignJoiner__c> list_cj = [select id From CampaignJoiner__c Where Campaign__c =:camId and RegisterUser__c=:userId];
			if(list_cj.size()>0)
			{
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR , '您已经预约了本次活动！') ;            
            	ApexPages.addMessage(msg); 
            	return;
			}*/
			cam.PeopleNumber__c = cam.PeopleNumber__c-1;
			update cam;
			CampaignJoiner__c cm = new CampaignJoiner__c();
			cm.RegisterUser__c = userId;
			cm.Campaign__c = cam.Id;
			insert cm;
		}
		catch(dmlexception e)
		{
			SetErrorToPage(e);
		}
		isJoin = true;
	}
	private void SetErrorToPage(system.Dmlexception dmlExc)
    {
        for (Integer i = 0; i < dmlExc.getNumDml(); i++) 
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR , dmlExc.getDmlMessage(i)) ;            
            ApexPages.addMessage(msg); 
   	        break;
    	}
    }
}