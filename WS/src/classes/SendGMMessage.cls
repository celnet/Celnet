public without sharing class SendGMMessage 
{
	AccessToken atk = new AccessToken();
	Http h = new Http();	//创建Http对象
	HttpRequest req = new HttpRequest();	//创建HttpRequest对象
	HttpResponse response = new HttpResponse();
	public class AccessToken
	{
		public string access_token;
		public string expires_in;
	}
	public class WXUser
	{
		public string subscribe;
		public string openid;
		public string nickname;
		public string sex;
		public string language;
		public string city;
		public string province;
		public string country;
		public string headimgurl;
		public string subscribe_time;		
	}
	public void getAccessToken()
	{
		Http h = new Http();	//创建Http对象
		HttpRequest req = new HttpRequest();	//创建HttpRequest对象
		req.setMethod('GET');		//设置请求的方法
		req.setTimeout(60000);		//设置超时时间 
		req.setEndpoint('https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wxd5b3bd16e3c0665d&secret=c54fe9824f16b39583a20926cdbe0b78');		//设置请求的url地址
		response = h.send(req);		//发送请求，返回HttpResponse
		atk = (AccessToken)JSON.deserializeStrict(response.getBody(),AccessToken.class);
		system.debug('***********************accesstoken**************'+atk.access_token);
		
	}
	public void sendMessage(string OpenId,string content)
	{
		JSONGenerator gen = JSON.createGenerator(true);
	 	gen.writeStartObject();
        gen.writeStringField('touser', OpenId);
        gen.writeStringField('msgtype', 'text');
        gen.writeFieldName('text');
        gen.writeStartObject();
        gen.writeStringField('content', content);
        gen.writeEndObject();
        gen.writeEndObject();
		string body = gen.getAsString();
		req.setMethod('POST');		//设置请求的方法
		req.setTimeout(60000);		//设置超时时间 
		req.setBody(body);
		string url = 'https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=';
		url+=EncodingUtil.urlEncode(atk.access_token, 'UTF-8');
		req.setEndpoint(url);		//设置请求的url地址
		response = h.send(req);		//发送请求，返回HttpResponse
		system.debug('***********************accesstoken**************'+response.getbody());
	}
	public void getUser(string OpenId)
	{
		req.setMethod('GET');		//设置请求的方法
		req.setTimeout(60000);		//设置超时时间 
		string url = 'https://api.weixin.qq.com/cgi-bin/user/info?access_token='+atk.access_token+'&openid='+OpenId;
		req.setEndpoint(url);		//设置请求的url地址
		response = h.send(req);		//发送请求，返回HttpResponse
		system.debug('****json****'+response.getBody());
		WXUser user = new WXUser();
		user = (WXUser)JSON.deserializeStrict(response.getBody(),WXUser.class);
		WeiXinUser__c u = new WeiXinUser__c();
		u.OpenId__c = user.openid;
		u.City__c = user.city;
		u.SubscribeTime__c = user.subscribe_time;
		u.Country__c = user.country;
		u.Province__c = user.province;
		u.Subscribe__c = user.subscribe;
		u.HeadImgURL__c = user.headimgurl;
		u.Sex__c = user.sex;
		u.Language__c = user.language;
		u.NickName__c = user.nickname;
		u.Name = user.nickname;
		upsert u OpenId__c;
		
	}
}