global class WebServiceUtil 
{
	webService static void sendMessage(string OpenId)
	{
		SendGMMessage a = new SendGMMessage();
		a.getAccessToken();
		a.sendMessage(OpenId,'你好');
	}
	webService static void getUser(string OpenId)
	{
		SendGMMessage a = new SendGMMessage();
		a.getAccessToken();
		a.getUser(OpenId);
	}
	webService static void sendMessage1(string OpenId)
	{
		WeixinHttpClient client = new WeixinHttpClient();
		JSONGenerator gen = JSON.createGenerator(true);
	 	gen.writeStartObject();
        gen.writeStringField('touser', OpenId);
        gen.writeStringField('msgtype', 'text');
        gen.writeFieldName('text');
        gen.writeStartObject();
        gen.writeStringField('content', '对！没错！');
        gen.writeEndObject();
        gen.writeEndObject();
		string body = gen.getAsString();
		list<PostParameter> Allparameter = new list<PostParameter>();
		Allparameter.add(new PostParameter('method','/message/custom/send')); 
		string res = client.post(Allparameter,body);
	}
}