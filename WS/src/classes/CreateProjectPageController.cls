public  class CreateProjectPageController 
{
	public Lead lead{set;get;}
	public boolean isClose{set;get;}
	public string message1{set;get;}
	public string message2{set;get;}
	public string message3{set;get;}
	public string message4{set;get;}
	public string userId{get;set;}
	public CreateProjectPageController()
	{
		string BuyOrSell = Apexpages.currentPage().getParameters().get('BuyOrSell');
		userId = Apexpages.currentPage().getParameters().get('userId');
		lead = new Lead();		
		lead.Buy_Or_Sell__c = BuyOrSell;
		lead.RegisterUser__c = userId;
	}
	public void Save()
	{
		try
		{
			insert lead;
			Account account = new Account();
			account.Name = lead.Company;
			account.RegisterUser__c =userId;
			insert account;
			Contact contact = new Contact();
			contact.FirstName = lead.FirstName;
			contact.LastName = lead.LastName;
			contact.Phone = lead.Phone;
			contact.Email = lead.Email;
			contact.AccountId = account.Id;
			insert contact;
			Opportunity opp = new Opportunity();
			opp.AccountId = account.Id;
			opp.Name = lead.Buy_Or_Sell__c+':'+lead.Company;
			opp.Buy_Or_Sell__c = lead.Buy_Or_Sell__c;
			opp.Company_Industry__c = lead.Industry;
			opp.Company_Category__c = lead.Company_Category__c;
			opp.Company_Scale__c = lead.Company_Scale__c;
			opp.Turnover_Time__c = lead.Turnover_Time__c;
			opp.StageName = '未处理';
			opp.CloseDate = Date.Today().addYears(1);
			opp.RegisterUser__c = userId;
			insert opp;
			Opportunity oppor = [select OppNumber__c From Opportunity Where Id =: opp.Id];
			message1 ='提交成功,您的业务单号为'+oppor.OppNumber__c+'。';
			message2 ='目前与您需求匹配的客户有30家。';
			message3 ='具体信息请联络：18611112222';
			message4 ='Fendi Bai';
		}
		catch(dmlexception e)
		{
			SetErrorToPage(e);
		}
	
		isClose = true;
	}
	public class AccessToken
	{
		public string access_token;
		public string expires_in;
	}
	private void SetErrorToPage(system.Dmlexception dmlExc)
    {
        for (Integer i = 0; i < dmlExc.getNumDml(); i++) 
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR , dmlExc.getDmlMessage(i)) ;            
            ApexPages.addMessage(msg); 
   	        break;
    	}
    }
}