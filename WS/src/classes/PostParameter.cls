/*
 *作者：Michael
 *时间：2013-4-16
 *功能：接口参数键值对存储
*/
public class PostParameter implements Comparable
{
	
	String name;
    String value;
    
    public Integer compareTo(Object compareTo) 
    {
    	PostParameter compareToEmp = (PostParameter)compareTo;
        if (name == compareToEmp.name) return 0;
        if (name > compareToEmp.name) return 1;
        return -1;        
    }
    
    public PostParameter(String name, String value) 
    {
        this.name = name;
        this.value = value;
    }
    public PostParameter(String name, double value) 
    {
        this.name = name;
        this.value = String.valueOf(value);
    }
    public PostParameter(String name, integer value) 
    {
        this.name = name;
        this.value = String.valueOf(value);
    }
    public PostParameter(String name, boolean value) 
    {
        this.name = name;
        this.value = String.valueOf(value);
    }
    public PostParameter(String name, datetime value) 
    {
        this.name = name;
        this.value = String.valueOf(value);
    }
    public PostParameter(String name, date value) 
    {
        this.name = name;
        this.value = String.valueOf(value);
    }
    public String getName(){
        return name;
    }
    public String getValue(){
        return value;
    }
    public static list<PostParameter> getParameterArray(String name, String value) 
    {
        return new list<PostParameter>{new PostParameter(name,value)};
    }
    public static list<PostParameter> getParameterArray(String name, integer value)
    {
        return getParameterArray(name,String.valueOf(value));
    }
}