global class WeixinRefreshTokenSchedule implements Schedulable 
{
	public class AccessToken
	{
		public string access_token;
		public string expires_in;
	} 
	global void execute(SchedulableContext SC) 
	{
		WeixinHttpClient client = new WeixinHttpClient();
		WeixinAccessToken__c wat = WeixinAccessToken__c.getInstance('Weixin');
		list<PostParameter> Allparameter = new list<PostParameter>();
		Allparameter.add(new PostParameter('grant_type','client_credential')); 
		Allparameter.add(new PostParameter('appid',WeixinConfig.getValue('Client_ID'))); 
		Allparameter.add(new PostParameter('secret',WeixinConfig.getValue('Client_SERCRET')));
		string res = client.getAccessToken(WeixinConfig.getValue('baseURL')+'/token',Allparameter);
		AccessToken atk = (AccessToken)JSON.deserializeStrict(res,AccessToken.class);
		wat.Access_Token__c = atk.access_token;
		wat.Expires_In__c = DateTime.now().addSeconds(integer.valueOf(atk.expires_in));
		update wat;
	}
}