public class WeixinConfig 
{
	private static SNConfig__c wc = new SNConfig__c();
	private static WeixinAccessToken__c wat = new WeixinAccessToken__c();
	static
	{
		wc = SNConfig__c.getInstance('WeixinConfig');
		wat = WeixinAccessToken__c.getInstance('Weixin');
	}
	public static String getValue(String key)
	{
		map<string,string> map_config = new map<string,string>();
		map_config.put('baseURL',wc.baseURL__c);
		map_config.put('Client_ID',wc.Client_ID__c);
		map_config.put('Client_SERCRET',wc.Client_SERCRET__c);
		return map_config.get(key);
	}
	public static String getAccessToken(string Key)
	{
		map<string,string> map_config = new map<string,string>();
		map_config.put('Access_Token',wat.Access_Token__c);
		return map_config.get(key);
		
	}
}