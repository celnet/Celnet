/**
 * Author: Steven
 * Date: 2014-11-7
 * Description: 合同删除时验证
 */
trigger DeleteValidationContract on contract__c (before delete) {
	Set<Id> contractIdSet = trigger.oldMap.keySet();
	
	List<Staff_contribution__c> scList = [Select Id, ProjectContract__c From Staff_contribution__c Where ProjectContract__c IN: contractIdSet];
	List<productArrival__c> paList = [Select Id, relateContract__c From productArrival__c Where relateContract__c IN: contractIdSet];
	List<contractProduct__c> cpList = [Select Id, relateContract__c From contractProduct__c Where relateContract__c IN: contractIdSet];
	List<reDetail__c> rdList = [Select Id, relateContract__c From reDetail__c Where relateContract__c IN: contractIdSet];
	
	if(!scList.isEmpty() || !paList.isEmpty() || !cpList.isempty() || !rdList.isEmpty()){
		trigger.old[0].addError('该项目合同有关联的人员贡献记录，不允许删除');
	}
}