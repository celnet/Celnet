trigger StaffContCannotChangeByOthers on Staff_contribution__c (before delete, before update) {
  Profile p = [SELECT Id,Name FROM Profile WHERE Id=:UserInfo.getProfileId()];
	if(p.Name != '系统管理员')
	{
			if(Trigger.isUpdate)
		     {
			    for(Staff_contribution__c aNew:trigger.new)
		        {
		        	for(Staff_contribution__c aOld:trigger.old)
					{
						if(aNew.Id == aOld.Id)
						{
							Boolean IsOk = false;
							
							IsOk = (aNew.ManDayDate__c != aOld.ManDayDate__c )||
									(aNew.describe__c != aOld.describe__c )||
									(aNew.HumanResourcesLibrary__c != aOld.HumanResourcesLibrary__c )||
									(aNew.day_count__c != aOld.day_count__c )||
									(aNew.HRL_project__c != aOld.HRL_project__c )||
									(aNew.ProjectContract__c != aOld.ProjectContract__c );
							if(IsOk)
							{
													
						      if(aNew.CreatedById != UserInfo.getUserId() )
							    {
							        
							            trigger.new[0].addError('非创建人员不能操作');
							        
							    }									
				    		}
						}
					}
		        	
		        	
		        	
		        	
		        	
		        	
		        	
		        	
		        
		        }
		     }
		     if(Trigger.isDelete)
		     {
		     	 for(Staff_contribution__c aNew:trigger.old)
		        {
			      if(aNew.CreatedById != UserInfo.getUserId() )
				    {
				        
				            trigger.old[0].addError('非创建人员不能操作');
				        
				    }
		        }
		     }

	}
}