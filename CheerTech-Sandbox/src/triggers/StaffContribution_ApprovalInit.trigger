/*************************************************
Description: 新建人员贡献时，把合同的PM赋值到人员贡献的对象上，以便确定审批人。
Author：Ward.Zhou
CreateDate：   2014-10-10
*************************************************/
trigger StaffContribution_ApprovalInit on Staff_contribution__c (before insert) 
{
    set<Id> ContractIdSet=new set<Id>();
    for(Staff_contribution__c S: trigger.new)
    {
        ContractIdSet.Add(S.ProjectContract__c);
    }
    map<Id,Id> PMMap=new map<Id,Id>();
    for(contract__c C:[select Id, relatePerson__c from contract__c where Id IN:ContractIdSet])
    {
        PMMap.put(C.Id,C.relatePerson__c);
    }
    for(Staff_contribution__c S: trigger.new)
    {
        if(PMMap.get(S.ProjectContract__c)!=null)
        {
        S.PM__c=PMMap.get(S.ProjectContract__c);
        }
    }
}