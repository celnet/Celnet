/*
 *@Spirng
 *@2014-10-22
 *当删除项目阶段时，同时删除与项目阶段相关连的，阶段人员贡献
 *
**/
trigger ProjectStage on ProjectStage__c (before delete,before insert,before update) {
	if(trigger.isDelete){
		List<ProjectStage__c> cc = Trigger.old;
		for(ProjectStage__c obj : cc){
			//查询阶段人员贡献对象==删除与这个阶段相关的记录
			list<StageContribute__c> stagec = [Select s.Id From StageContribute__c s where s.ContributeStage__r.Id =: obj.Id];
			System.debug('------需要删除的List.size---'+stagec.size());
			if(stagec != null && stagec.size() > 0){
				delete stagec;
			}
		}
	} else if(trigger.isInsert){
		//项目阶段不能超过100%
		List<ProjectStage__c> cc = Trigger.new;
		boolean flag = false;
		for(ProjectStage__c obj : cc){
			//projectId
			String projectId = obj.StageProject__c;
			AggregateResult rs = [select sum(StagePercentage__c) stage from ProjectStage__c where StageProject__c =: projectId];
			Object sumStage = rs.get('stage');
			System.debug(obj.StagePercentage__c+'----------sumStage--------'+sumStage);
			Double doSumStage = 0;
			if(sumStage !=null){
				doSumStage = Double.valueOf(sumStage) + obj.StagePercentage__c;
			}else{
				doSumStage = obj.StagePercentage__c;
			}
			
			if(doSumStage > 100){
				flag = true;
				obj.StagePercentage__c.addError('同一项目的阶段权重比例不能超过100');
				break;
			}
		}
		
		if(flag){
			return;
		}
	}else if(trigger.isUpdate){
		//项目阶段不能超过100%
		List<ProjectStage__c> cold = Trigger.old;
		List<ProjectStage__c> cc = Trigger.new;
		boolean flag = false;
		for(ProjectStage__c obj : cc){
			for(ProjectStage__c o : cold){
				if(obj.id !=o.id){
					continue;
				}
				//projectId
				String projectId = obj.StageProject__c;
				AggregateResult rs = [select sum(StagePercentage__c) stage from ProjectStage__c where StageProject__c =: projectId];
				Object sumStage = rs.get('stage');
				System.debug(obj.StagePercentage__c+'----------sumStage--------'+sumStage);
				Double doSumStage = 0;
				if(sumStage !=null){
					//加上新的减去原来的
					doSumStage = Double.valueOf(sumStage) + obj.StagePercentage__c - o.StagePercentage__c;
				}else{
					doSumStage = obj.StagePercentage__c;
				}
				
				if(doSumStage > 100){
					flag = true;
					obj.StagePercentage__c.addError('同一项目的阶段权重比例不能超过100');
					break;
				}
			}
			if(flag)break;
		}
		
		if(flag){
			return;
		}
	}
}