trigger AssignmentShare on Assignment__c (after insert) {
	list<Assignment__Share> listShare=new list<Assignment__Share>();
	list<Id> listId = new list<Id>();
	list<Assignment__c> listAssi = new list<Assignment__c>();
	map<ID,ID> mapProjOwnid = new map<ID,ID>();
	
	for(Assignment__c a : trigger.new)
	{
		listId.add(a.AssignmentProject__c);
		listAssi.add(a);
	}
	list<project__c> listProject = [select Id,OwnerId from project__c where Id in:listId];
	for(project__c p :listProject){
		mapProjOwnid.put(p.id,p.OwnerId);
	}
	for(Assignment__c a : listAssi){
		if(mapProjOwnid.containsKey(a.AssignmentProject__c)){
			Assignment__Share ash=new Assignment__Share();
			ash.UserOrGroupId=mapProjOwnid.get(a.AssignmentProject__c);
			ash.ParentId=a.Id;
			ash.AccessLevel='Read';
			listShare.add(ash);
		}
	}
	
	try{
		insert listShare;
	} catch(Exception e){
		System.debug(e.getMessage());
	}
}