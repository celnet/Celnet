/*************************************************
class:  productArrivalLockAfterContractLocked
Description:   项目合同关闭后锁定 不能创建相关合同下的产品交付
Table Accessed: project
Table Updated:  
Author：Snowz.zhu
CreateDate：  2014年07月17日11:53:57
*************************************************/
trigger productArrivalLockAfterContractLocked on productArrival__c (before delete, before insert, before update) {
	if (Trigger.isInsert)
		{
			list <Id> ContractId=new list<Id>();
			for(productArrival__c sc:trigger.new)
			{
				ContractId.add(sc.relateContract__c);
			}
			List <contract__c> contractList=[select Id,contractStatus__c,ContractNum__c from  contract__c where Id in:ContractId];
			for(contract__c c:contractList)
			{
				if(c.contractStatus__c =='完成'||c.contractStatus__c =='失败' )
				{
					Trigger.New[0].addError('项目合同已经结束，不能添加 合同编号' + c.ContractNum__c);
				}
			}
		}
			if (Trigger.isUpdate)
		{
			list <Id> ContractId=new list<Id>();
			for(productArrival__c sc:trigger.new)
			{
				ContractId.add(sc.relateContract__c);
			}
			List <contract__c> contractList=[select Id,contractStatus__c,ContractNum__c from  contract__c where Id in:ContractId];
			for(contract__c c:contractList)
			{
				if(c.contractStatus__c =='完成'||c.contractStatus__c =='失败' )
				{
					Trigger.New[0].addError('项目合同已经结束，不能添加 合同编号' + c.ContractNum__c);
				}
			}
		}
		if (Trigger.isDelete)
		{
			list <Id> ContractId=new list<Id>();
			for(productArrival__c sc:trigger.old)
			{
				ContractId.add(sc.relateContract__c);
			}
			List <contract__c> contractList=[select Id,contractStatus__c,ContractNum__c from  contract__c where Id in:ContractId];
			for(contract__c c:contractList)
			{
				if(c.contractStatus__c =='完成'||c.contractStatus__c =='失败' )
				{
					Trigger.old[0].addError('项目合同已经结束，不能删除 合同编号' + c.ContractNum__c);
				}
			}
		}
}