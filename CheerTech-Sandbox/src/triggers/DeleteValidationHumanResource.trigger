/**
 * Author: Steven
 * Date: 2014-11-7
 * Description: 成本级别记录删除时，判断该记录是否有关联人力资源库记录，若有则不允许删除
 */
trigger DeleteValidationHumanResource on humanRresources__c (before delete) {
	Set<Id> hrIdSet = trigger.oldMap.keySet();
	List<HRManage__c> hrmList = [Select Id, HR__c From HRManage__c Where HR__c IN: hrIdSet];
	if(!hrmList.isEmpty()){
		((humanRresources__c)trigger.oldMap.get(hrmList[0].HR__c)).addError('该成本级别人力资源库有记录，不允许删除');
	}
}