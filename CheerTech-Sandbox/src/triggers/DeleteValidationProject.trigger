/**
 * Author: Steven
 * Date: 2014-11-7
 * Description: 项目记录删除时，判断该记录是否有关联合同记录、任务记录、暂支记录，若有并且考核阶段、产品、人员未填写，则不允许删除
 */
trigger DeleteValidationProject on project__c (before delete) {
	Set<Id> projectIdSet = trigger.oldMap.keySet();
	
	// 合同记录，任务记录，暂支记录
	List<contract__c> contractList = [Select Id, relateProject__c From contract__c Where relateProject__c IN: projectIdSet ];
	List<Assignment__c> assignmentList = [Select Id, AssignmentProject__c From Assignment__c Where AssignmentProject__c IN: projectIdSet];
	List<advance__c> advanceList = [Select Id, projectRecordNum__c From advance__c Where projectRecordNum__c IN: projectIdSet];
	
	// 考核产品，考核人员，考核阶段
	List<ProjectProduct__c> productList = [Select Id From ProjectProduct__c Where ProductProject__c IN: projectIdSet];
	List<projectPerson__c> personList = [Select Id From projectPerson__c Where relateProject__c IN: projectIdSet];
	List<ProjectStage__c> stageList = [Select Id From ProjectStage__c Where StageProject__c IN: projectIdSet];
	
	if(!contractList.isEmpty()){
		((project__c)trigger.oldMap.get(contractList[0].relateProject__c)).addError('该项目有关联的合同记录，不允许删除');
	} else if(!assignmentList.isEmpty()){
		((project__c)trigger.oldMap.get(assignmentList[0].AssignmentProject__c)).addError('该项目有关联的任务记录，不允许删除');
	} else if(!advanceList.isEmpty()){
		((project__c)trigger.oldMap.get(advanceList[0].projectRecordNum__c)).addError('该项目有关联的暂支记录，不允许删除');
	} else if(productList.isEmpty() || personList.isEmpty() || stageList.isEmpty()){ 
		((project__c)trigger.old[0]).addError('该项目未填写考核记录，不允许删除');
	}
}