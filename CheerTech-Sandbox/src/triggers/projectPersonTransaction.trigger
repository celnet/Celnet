/******************************************************
 *Spring
 *2014-10-30
 *处理当考核人的增删改所导致的阶段人员贡献和产品人员贡献问题
*******************************************************/
trigger projectPersonTransaction on projectPerson__c (after delete, after insert, after update) {
	if(trigger.isInsert || trigger.isUpdate){
		//由于考核人添加后全是0，不造成影响
		//这里考虑不能编辑，只能添加和删除
	}else if(trigger.isDelete){
		//当删除考核人员时，要删除在当前项目中与考核人员有关的“阶段人员贡献”和“产品人员贡献”
		List<projectPerson__c> list_person = trigger.old;
		for(projectPerson__c p : list_person){
			String projectId = p.relateProject__c;
			String personId = p.joinPerson__c;
			System.debug(projectId+'项目id----人的id--'+personId);
			//阶段贡献
			List<StageContribute__c> list_stage = [select id from StageContribute__c where 	MPersonName__c =:personId and ContributeStage__r.StageProject__c =:projectId];
			System.debug('--阶段--'+list_stage);
			if(list_stage != null && list_stage.size() > 0 ){
				for(StageContribute__c o : list_stage){
					delete o;
				}
			}
			//产品贡献
			List<ProductContribute__c> list_product = [select id from ProductContribute__c where PersonName__c =:personId and ProjectProduct__r.ProductProject__c =:projectId ];
			System.debug('--产品--'+list_product);
			if(list_product !=null && list_product.size() >0 ){
				for(ProductContribute__c o : list_product){
					delete o;
				}
			}
		}
		
	}
	
}