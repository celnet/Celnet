/**
 *	@Spring
 *	@2014-10-24
 *	@一个项目参与人只能有一个销售
 *  @更改日期:2014-10-30
 *  @一个项目参与人只能有一个销售角色
*/
trigger projectPerson on projectPerson__c (before insert, before update ,before delete) {
	if(!trigger.isDelete){
		List<projectPerson__c> objs =  Trigger.new;
		boolean flag = false;
		for(projectPerson__c obj : objs){
			System.debug('--当前添加修改人obj---'+obj.Role__c);
			if(obj.Role__c == '销售'){
				//取到当前项目id
				String id = obj.relateProject__c;
				//查询当前项目所有的参与人
				List<projectPerson__c> joinPerson = [Select p.Role__c, p.Id,p.relateProject__c From projectPerson__c p where p.relateProject__c =: id];
				if(joinPerson == null || joinPerson.size() == 0){
					break;
				}
				
				for(projectPerson__c person : joinPerson){
					//if(person.joinPerson__r.department__c)
					if(person.Role__c == '销售'){
						flag = true;
						obj.joinPerson__c.addError('项目参与人中，已经存在销售角色了!');
						break;
					}
				}
				
				if(flag)break;
			}
		}
		
		if(flag){
			return;
		}
	}
	
}