/**
 * Author:bill
 * Date:2014-6-27
 * Description:项目完成或者终止锁定记录，不允许再次修改
 */
trigger ProjectLocked on project__c (after update) 
{
    project__c project = Trigger.new[0];
    if(project.projectStatus__c != Trigger.old[0].projectStatus__c 
        && (project.projectStatus__c == '完成' || project.projectStatus__c == '失败'))
    {
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setObjectId(project.id);
        Approval.ProcessResult result = Approval.process(req);

    }
}