trigger EditValidationProductArrival on productArrival__c (before update) {
	List<contract__c> contractList = [Select Id, contractStatus__c From contract__c Where Id =: trigger.new[0].relateContract__c];
	if(!contractList.isEmpty() && (contractList[0].contractStatus__c == '完成' || contractList[0].contractStatus__c == '终止'))
	trigger.new[0].addError('项目合同已锁定，不允许新建或编辑');
}