/*************************************************
class:  ContractLocked
Description:   项目合同关闭后锁定 
Table Accessed: contract__c
Table Updated:  
Author：Snowz.zhu
CreateDate：   2014-06-30
*************************************************/

trigger ContractLocked on contract__c (after update) {
	
	contract__c contract = Trigger.new[0];
    if(contract.contractStatus__c != Trigger.old[0].contractStatus__c 
        && (contract.contractStatus__c == '完成'||contract.contractStatus__c == '终止'))
    {
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setObjectId(contract.id);
        Approval.ProcessResult result = Approval.process(req);
    }
}