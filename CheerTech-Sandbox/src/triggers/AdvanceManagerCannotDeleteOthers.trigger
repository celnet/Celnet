/*************************************************
trigger:      AdvanceManagerCannotDeleteOthers
Description:  暂支不能被删除，只有管理员和自己除外，因为他的上级可以删除，所以写此trigger
Table Accessed: 
Table Updated:  
Author：Snowz.zhu
CreateDate：   2014年8月3日15:42:23
*************************************************/
trigger AdvanceManagerCannotDeleteOthers on advance__c (before delete, before update) {
	  Profile p = [SELECT Id,Name FROM Profile WHERE Id=:UserInfo.getProfileId()];
	if(p.Name != '系统管理员')
	{
	if(Trigger.isUpdate)
     {
	    for(advance__c aNew:trigger.new)
        {
        	for(advance__c aOld:trigger.old)
			{
				if(aNew.Id == aOld.Id)
				{
					Boolean IsOk = false;
					
					IsOk = (aNew.amount__c != aOld.amount__c )||
							(aNew.Name != aOld.Name )||
							(aNew.user__c != aOld.user__c )||
							(aNew.Date__c != aOld.Date__c )||
							(aNew.reason__c != aOld.reason__c )||
							(aNew.projectRecordNum__c != aOld.projectRecordNum__c );
					if(IsOk)
					{
											
				      if(aNew.CreatedById != UserInfo.getUserId() )
					    {
					        
					            trigger.new[0].addError('非暂支创建人员不能操作');
					        
					    }									
		    		}
				}
			}
        	
        }
     }
     if(Trigger.isDelete)
     {
     	 for(advance__c aNew:trigger.old)
        {
	      if(aNew.CreatedById != UserInfo.getUserId() )
		    {
		        
		            trigger.old[0].addError('非暂支创建人员不能操作');
		        
		    }
        }
     }
}
}