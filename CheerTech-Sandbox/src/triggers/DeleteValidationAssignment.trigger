/**
 * Author: Steven
 * Date: 2014-11-7
 * Description: 
 */
trigger DeleteValidationAssignment on Assignment__c (before delete) {
	Set<Id> assignmentIdSet = trigger.oldMap.keySet();
	
	List<event__c> eventList = [Select Id, EventTask__c From event__c Where EventTask__c IN: assignmentIdSet];
	
	if(!eventList.isEmpty()){
		((Assignment__c)trigger.oldMap.get(eventList[0].EventTask__c)).addError('该任务有关联的进程纪要记录，不允许删除');
	}
}