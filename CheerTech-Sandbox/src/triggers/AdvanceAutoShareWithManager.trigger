/*************************************************
trigger:      AdvanceAutoShareWithManager
Description:  暂支自动共享给合同和项目创建人员
Table Accessed: 
Table Updated:  
Author：Snowz.zhu
CreateDate：   2014年8月3日15:42:23
*************************************************/
trigger AdvanceAutoShareWithManager on advance__c (after insert) {
	public class AllProjectInfo
    {
        Id project{get;set;}
       project__Share ps{get;set;}
        advance__Share ProjectManageres{get;set;}
       advance__c advance{get;set;}
        Id porjectuser{get;set;}
      
       public AllProjectInfo()
       {
        ps = new project__Share();
        ProjectManageres = new advance__Share();
        advance = new advance__c();
       }
    }
    list<advance__Share> esList = new list<advance__Share>(); 
    List <AllProjectInfo> apiList = new List<AllProjectInfo>();
    list <Id> ProjectId=new list<Id>();
   
    for(advance__c e:trigger.new)
    {
        ProjectId.add(e.projectRecordNum__c);
    }
    List <project__c> projectList = [select Id from  project__c  where Id in:ProjectId];
    List <project__Share> ProjectShareList=[select Id,ParentId,RowCause,UserOrGroupId
                                       from  project__Share  where ParentId in:ProjectId];
                                       
    List<advance__c> eNewList = [SELECT Id,CreatedById, projectRecordNum__c,projectRecordNum__r.CreatedById,projectRecordNum__r.OwnerId FROM advance__c where Id in:trigger.new];
    map<Id,project__Share> ProjectIdShareMap = new Map<Id,project__Share>();
    for(project__Share ps:ProjectShareList)
    {
        ProjectIdShareMap.put(ps.ParentId,ps);
    }                         
    for(advance__c e:eNewList)
    {
        AllProjectInfo api = new AllProjectInfo();
        api.project = e.projectRecordNum__c;
        api.ps = ProjectIdShareMap.get(e.projectRecordNum__c);
        api.porjectuser = e.projectRecordNum__r.CreatedById;
        api.advance =e;
        if(api.porjectuser != e.CreatedById)
       	{	
       		apiList.add(api);
       	}
    }
    
    for(AllProjectInfo api :apiList)
    {
        api.ProjectManageres.ParentId = api.advance.Id;
        api.ProjectManageres.UserOrGroupId = api.porjectuser;
        api.ProjectManageres.AccessLevel = 'Read';
       
        esList.add(api.ProjectManageres);
        
    }
    insert(esList);
}