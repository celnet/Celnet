/*************************************************
Description: 产品交付中产品数量的控制，不能大于订单上的数量。
Author：Ward.Zhou
CreateDate：   2014-9-28
*************************************************/
trigger ProductArrivalQuantityCotroll on productArrival__c (before insert, before update) 
{
	//这里考虑到不会批处理产品交付，所以在循环中直接查询出数量，验证是否正确。
	
	for(productArrival__c P:trigger.new)
	{
		double Quantity;
		double MaxNum=[select quantity__c from contractProduct__c where Id=:P.product__c][0].quantity__c;
		AggregateResult[] Ag=[select Sum(quantity__c) SQ from  productArrival__c where product__c=:P.product__c];
		
		if(Ag.size()>0)
		{		
			Quantity=Double.valueOf(Ag[0].get('SQ'));
			if(Quantity==null)
			{
				Quantity=0;
			}
		}
		else
		{
			Quantity=0;
		}
		//system.debug('本次的数量'+P.quantity__c+'@@@@@@@@@@已经存在的数量'+Quantity+'####最大数量'+MaxNum);
		if(P.quantity__c+Quantity>MaxNum)
		{
			P.quantity__c.AddError('产品交付的数量，不能超过订单产品的总数量！');
		}
		
	}
	
}