/**
 *Spring
 *2014-10-25
 *汇总合同上的"维保费用"到项目中的"预估维保费用"
*/
trigger ContractAutoServiceCostToProject on contract__c (after delete, after insert, after update) {
	System.debug('--------ContractAutoServiceCostToProject---------');
	Set<ID> set_ids = new Set<ID>();
	if(!trigger.isDelete){
		for(contract__c con : trigger.new){
			set_ids.add(con.relateProject__c);
		}
	}else{
		for(contract__c con : trigger.old){
			set_ids.add(con.relateProject__c);
		}
	}
	
	map<String,Double> map_proAmount = new map<String,Double>();
	for(AggregateResult result :[select sum(serviceCost__c) cost , 	relateProject__c from contract__c where relateProject__c IN : set_ids Group by relateProject__c ]){
		object cost = result.get('cost');
    	object projectId = result.get('relateProject__c');
    	
    	Double douCost = 0;
    	String strProjectId = '';
    	
    	if(cost!=null)douCost = Double.valueOf(cost);
    	if(projectId!=null)strProjectId = String.valueOf(projectId);
    	map_proAmount.put(strProjectId,douCost);
	}
	
	list<project__c> list_pro = [select Id, preServiceCost__c from project__c where Id IN : set_ids];
	for(project__c pro : list_pro){
		if(map_proAmount.containsKey(pro.id)){
			pro.preServiceCost__c = map_proAmount.get(pro.id);
		}else{
			pro.preServiceCost__c = 0;
		}
	}
	
	if(list_pro !=null && list_pro.size() > 0){
		update list_pro;
	}
}