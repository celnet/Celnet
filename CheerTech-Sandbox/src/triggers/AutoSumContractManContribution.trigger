/*************************************************
class:      AutoSumContractManContribution
Description:   自动汇总 项目合同里的人员贡献金额 
Table Accessed: 
Table Updated:  
Author：Snowz.zhu
CreateDate：   2014-06-30
*************************************************/
trigger AutoSumContractManContribution on Staff_contribution__c (after delete, after insert, after update) 
{
	

	//项目ID
	set<ID> set_proId = new set<ID>();
	if(!trigger.isDelete)
	{
		for(Staff_contribution__c sc : trigger.new)
		{
			set_proId.add(sc.ProjectContract__c);
		}
	}else{
		for(Staff_contribution__c sc : trigger.old)
		{
			set_proId.add(sc.ProjectContract__c);
		}	
	}
	//项目ID，发生总费用
	map<String,Double> map_proAmount = new map<String,Double>(); 	
    for(AggregateResult result : [select sum(ManCostAmount__c) cost, ProjectContract__c from Staff_contribution__c where 
        		 ProjectContract__c IN : set_proId  and ApprovalStatus__c = '通过' Group by ProjectContract__c])
    {
    	object cost = result.get('cost');
    	object projectId = result.get('ProjectContract__c');
    	
    	Double douCost = 0;
    	String strProjectId = '';
    	
    	if(cost!=null)douCost = Double.valueOf(cost);
    	if(projectId!=null)strProjectId = String.valueOf(projectId);
    	map_proAmount.put(strProjectId,douCost);
    }
    
    list<contract__c> list_pro = [Select c.ManCostAmount__c, c.Id From contract__c c where Id IN : set_proId];
    for(contract__c pro : list_pro)
    {
    	If(map_proAmount.containsKey(pro.Id))
    	{
    		pro.ManCostAmount__c = map_proAmount.get(pro.Id);
    	}else{
    		pro.ManCostAmount__c = 0;
    	}
    }
    System.debug('-----------------'+list_pro);
    if(list_pro != null && list_pro.size()>0)
    {
    	update list_pro;
    }	
}