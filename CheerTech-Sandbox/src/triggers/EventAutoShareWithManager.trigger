/*************************************************
class:      EventAutoShareWithManager
Description:   进程纪要自动共享给项目和项目合同创建人 
Table Accessed: 
Table Updated:  
Author：Snowz.zhu
CreateDate：   2014年07月23日15:58:20
*************************************************/
trigger EventAutoShareWithManager on event__c (after insert) {
	   public class AllProjectInfo
    {
        Id project{get;set;}
       project__Share ps{get;set;}
        event__Share ProjectManageres{get;set;}
       event__c event{get;set;}
        Id porjectuser{get;set;}
      
       public AllProjectInfo()
       {
        ps = new project__Share();
        ProjectManageres = new event__Share();
        event = new event__c();
       }
    }
    list<event__Share> esList = new list<event__Share>(); 
    List <AllProjectInfo> apiList = new List<AllProjectInfo>();
    list <Id> ProjectId=new list<Id>();
   
    for(event__c e:trigger.new)
    {
        ProjectId.add(e.project__c);
    }
    List <project__c> projectList = [select Id from  project__c  where Id in:ProjectId];
    List <project__Share> ProjectShareList=[select Id,ParentId,RowCause,UserOrGroupId
                                       from  project__Share  where ParentId in:ProjectId];
                                       
    List<event__c> eNewList = [SELECT Id,CreatedById, project__c,project__r.CreatedById,project__r.OwnerId FROM event__c where Id in:trigger.new];
    map<Id,project__Share> ProjectIdShareMap = new Map<Id,project__Share>();
    for(project__Share ps:ProjectShareList)
    {
        ProjectIdShareMap.put(ps.ParentId,ps);
    }                         
    for(event__c e:eNewList)
    {
        AllProjectInfo api = new AllProjectInfo();
        api.project = e.project__c;
        api.ps = ProjectIdShareMap.get(e.project__c);
        api.porjectuser = e.project__r.CreatedById;
        api.event =e;
        if(api.porjectuser != e.CreatedById)
       	{	
       		apiList.add(api);
       	}
        
    }
    
    for(AllProjectInfo api :apiList)
    {
        api.ProjectManageres.ParentId = api.event.Id;
        api.ProjectManageres.UserOrGroupId = api.porjectuser;
        api.ProjectManageres.AccessLevel = 'Read';
       
        esList.add(api.ProjectManageres);
        
    }
    
    try{
    	insert(esList);
    } catch(Exception e){
    	System.debug(e.getMessage());
    }
}