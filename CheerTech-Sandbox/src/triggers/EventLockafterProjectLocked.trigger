/*************************************************
class:  EventLockafterProjectLocked
Description:   项目关闭后锁定 不能创建相关项目下的进程纪要 
Table Accessed: project
Table Updated:  
Author：Snowz.zhu
CreateDate：   2014年07月17日11:19:15
*************************************************/
trigger EventLockafterProjectLocked on event__c (before delete, before insert, before update) {

	if (Trigger.isInsert)
	{
		list <Id> ProjectId=new list<Id>();
		for(event__c e:trigger.new)
		{
			ProjectId.add(e.project__c);
		}
		List <project__c> projectList=[select Id,projectStatus__c,Name from  project__c where Id in:ProjectId];
		for(project__c p:projectList)
		{
			if(p.projectStatus__c =='完成'||p.projectStatus__c =='失败' )
			{
				Trigger.New[0].addError('项目已经结束，不能添加 项目名称：'+p.Name);
			}
		}
	}
	if (Trigger.isUpdate)
	{
		list <Id> ProjectId=new list<Id>();
		for(event__c e:trigger.new)
		{
			ProjectId.add(e.project__c);
		}
		List <project__c> projectList=[select Id,projectStatus__c,Name from  project__c where Id in:ProjectId];
		for(project__c p:projectList)
		{
			if(p.projectStatus__c =='完成'||p.projectStatus__c =='失败' )
			{
				Trigger.New[0].addError('项目已经结束，不能添加 项目名称：'+p.Name);
			}
		}
	}	
	if (Trigger.isDelete)
	{
		list <Id> ProjectId=new list<Id>();
		for(event__c e:trigger.old)
		{
			ProjectId.add(e.project__c);
		}
		List <project__c> projectList=[select Id,projectStatus__c,Name from  project__c where Id in:ProjectId];
		for(project__c p:projectList)
		{
			if(p.projectStatus__c =='完成'||p.projectStatus__c =='失败' )
			{
				Trigger.old[0].addError('项目已经结束，不能删除 项目名称：'+p.Name);
			}
		}
	}
}