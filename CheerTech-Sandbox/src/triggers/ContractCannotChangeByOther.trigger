trigger ContractCannotChangeByOther on contract__c (before update,before delete) {
     Profile p = [SELECT Id,Name FROM Profile WHERE Id=:UserInfo.getProfileId()];
	if(p.Name != '系统管理员')
	{
	
     if(Trigger.isDelete)
     {
	    for(contract__c cNew:trigger.old)
        {
	      if(cNew.CreatedById != UserInfo.getUserId() )
		    {
		        
		            trigger.old[0].addError('非项目合同创建人员不能删除项目合同');
		        
		    }
        }
     }
     
     
     if(Trigger.isUpdate)
     {
     	for(contract__c pNew:trigger.new)
        {
            for(contract__c pOld:trigger.old)
            {
                if(pNew.Id == pOld.Id)
                {
                	 Boolean IsOk =false;
                 
                    IsOk =  (pNew.contractSummary__c != pOld.contractSummary__c)||
						(pNew.RecordType != pOld.RecordType)||
						(pNew.contractDate__c != pOld.contractDate__c)||
						(pNew.contractStatus__c != pOld.contractStatus__c)||
						(pNew.relateAccount__c != pOld.relateAccount__c)||
						(pNew.contractDetail__c != pOld.contractDetail__c)||
						(pNew.relateProject__c != pOld.relateProject__c)||
						(pNew.contractServiceMon__c != pOld.contractServiceMon__c)||
						(pNew.ContractNum__c != pOld.ContractNum__c)||
						(pNew.contractAgent__c != pOld.contractAgent__c);
                    if(IsOk)
                    {
                    	if(pNew.CreatedById != UserInfo.getUserId() )
                        {
                            
                                trigger.new[0].addError('非项目合同创建人员不能修改项目合同信息');
                            
                        }
                    }

                }
            }
        
            
            
        }
     }
	}
}