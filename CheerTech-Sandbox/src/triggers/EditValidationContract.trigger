/**
 * Author: Steven
 * Date: 2014-11-7
 * Description: 
 */
trigger EditValidationContract on contract__c (before update) {
	
	List<project__c> projectList = [Select Id, projectStatus__c From project__c Where Id =: trigger.new[0].relateProject__c];
	if(!projectList.isEmpty() && (projectList[0].projectStatus__c == '完成' || projectList[0].projectStatus__c == '失败')){
		trigger.new[0].addError('项目已锁定，不允许新建或编辑');
	} 
	
	
}