/**
 * Author: Steven
 * Date: 2014-11-7
 * Description: 部门记录删除时，判断该记录是否有关联人力资源库记录，若有则不允许删除
 */
trigger DeleteValidationDepartment on department__c (before delete) {
	Set<Id> departmentIdSet = trigger.oldMap.keySet();
	List<HRManage__c> hrmList = [Select Id, department__c From HRManage__c Where department__c IN: departmentIdSet];
	if(!hrmList.isEmpty()){
		((department__c)trigger.oldMap.get(hrmList[0].department__c)).addError('该部门人力资源库有记录，不允许删除');
	}
}