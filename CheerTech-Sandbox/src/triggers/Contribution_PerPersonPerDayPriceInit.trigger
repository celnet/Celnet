/*************************************************
Description: 新建人员共享对象的实例时，自动把当时的人力资源库中人天单价取过来赋值给人员贡献上的人天单价字段。
Author：Ward.Zhou
CreateDate：   2014-9-28
*************************************************/
trigger Contribution_PerPersonPerDayPriceInit on Staff_contribution__c (before insert) 
{
	set<Id> ResourceIdSet=new set<Id>(); 
	for(Staff_contribution__c S:trigger.new)
	{
		ResourceIdSet.Add(S.HumanResourcesLibrary__c);
	}
	map<Id,HRManage__c> ResourceMap=new map<Id,HRManage__c>();
	for(HRManage__c H:[select ManDayCost__c from HRManage__c where Id IN: ResourceIdSet])
	{
		ResourceMap.put(H.Id,H);
	}
	for(Staff_contribution__c S:trigger.new)
	{
		S.CurrentPerDayPrice__c=ResourceMap.get(S.HumanResourcesLibrary__c).ManDayCost__c;
	}
}