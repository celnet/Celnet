/********************************************
 *Spring
 *2014-10-26
 *每个项目阶段，人员贡献不能超过100
/********************************************/
trigger StageContributeLimit on StageContribute__c (before insert, before update) {
	System.debug('------StageContributeLimit----------');
	if(trigger.isInsert){
		List<StageContribute__c> newObjList = trigger.new;
		boolean flag = false;
		for(StageContribute__c newObj : newObjList){
			String ContributeStageId = newObj.ContributeStage__c;
			AggregateResult rs = [select sum(StageContributePercentage__c) stage from StageContribute__c where ContributeStage__c =: ContributeStageId];
			System.debug('---------rs----'+rs);
			Object sumStage = rs.get('stage');
			Double doSumStage = 0;
			if(sumStage !=null){
				doSumStage = Double.valueOf(sumStage) + newObj.StageContributePercentage__c;
			}else{
				doSumStage = newObj.StageContributePercentage__c;
			}
			System.debug('---isInsert-----doSumStage-------'+doSumStage);
			if(doSumStage > 100){
				//ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO,'同一项目考核阶段,阶段贡献比例不能超过100!');
            	//ApexPages.addMessage(msg);
				newObj.StageContributePercentage__c.addError('同一项目考核阶段,阶段贡献比例不能超过100');
				flag = true;
				break;
			}
		}
		if(flag){return;}
	}else{
		List<StageContribute__c> newObjList = trigger.new;
		List<StageContribute__c> oldObjList = trigger.old;
		boolean flag = false;
		for(StageContribute__c newObj : newObjList){
			for(StageContribute__c oldObj : oldObjList){
				if(newObj.id != oldObj.id){
					continue;
				}
				String ContributeStageId = newObj.ContributeStage__c;
				AggregateResult rs = [select sum(StageContributePercentage__c) stage from StageContribute__c where ContributeStage__c =: ContributeStageId];
				Object sumStage = rs.get('stage');
				Double doSumStage = 0;
				if(sumStage !=null){
					doSumStage = Double.valueOf(sumStage) + newObj.StageContributePercentage__c - oldObj.StageContributePercentage__c;
				}else{
					doSumStage = newObj.StageContributePercentage__c;
				}
				if(doSumStage > 100){
					//ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO,'同一项目考核阶段,阶段贡献比例不能超过100!');
            		//ApexPages.addMessage(msg);
					newObj.StageContributePercentage__c.addError('同一项目考核阶段,阶段贡献比例不能超过100');
					flag = true;
					break;
				}
			}
			if(flag){break;}
		}
		if(flag){return;}
	}
}