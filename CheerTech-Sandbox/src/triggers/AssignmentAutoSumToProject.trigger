/** 
*	Spring
*   2014-10-21
*	将该项目下所有的任务费用汇总到费用总金额
*
*/
trigger AssignmentAutoSumToProject on Assignment__c (before insert,after delete, after update) {

	if(trigger.isAfter && trigger.isInsert)
	{
		Set<ID> set_pIds = new set<ID>();
		map<ID,ID> map_u = new map<ID,ID>();
		for(Assignment__c obj : trigger.new)
		{
			set_pIds.add(obj.AssignmentProject__c);
			map_u.put(obj.AssignmentProject__c,obj.OwnerId);
		}
		list<Assignment__Share> list_a=new list<Assignment__Share>();
		for(project__c p : [select Id,OwnerId from project__c where Id in:set_pIds])
		{
			if(map_u.ContainsKey(p.id))
			{
				if(map_u.get(p.id)==p.OwnerId)continue;
			}
			Assignment__Share a=new Assignment__Share();
			a.UserOrGroupId=p.OwnerId;
			a.ParentId=p.Id;
			a.AccessLevel='Read';
			list_a.add(a);
		}
		if(list_a.size()>0)
		insert list_a;
	}
	
	if(trigger.isAfter && (trigger.isUpdate || trigger.isDelete))
	{
		System.debug('-----------AssignmentAutoSumToProject----------');
		set<ID> set_proId = new set<ID>();
		if(!trigger.isDelete)
		{
			for(Assignment__c obj : trigger.new)
			{
				set_proId.add(obj.AssignmentProject__c);
				//if(obj.AssignmentVerify__c == '通过')set_proId.add(obj.AssignmentProject__c);
			}
		}else{
			for(Assignment__c obj : trigger.old)
			{
				set_proId.add(obj.AssignmentProject__c);
				//if(obj.AssignmentVerify__c == '通过')set_proId.add(obj.AssignmentProject__c);
			}	
		}	
		System.debug('------set_proId-------'+set_proId);
		//任务ID，发生总费用
		map<String,Double> map_proAmount = new map<String,Double>(); 	
	    for(AggregateResult result : [select sum(AssignmentCost__c) cost, AssignmentProject__c from Assignment__c where 
	        		 AssignmentProject__c IN : set_proId  Group by AssignmentProject__c])
	    {
	    	object cost = result.get('cost');
	    	object projectId = result.get('AssignmentProject__c');
	    	
	    	Double douCost = 0;
	    	String strProjectId = '';
	    	
	    	if(cost!=null)douCost = Double.valueOf(cost);
	    	if(projectId!=null)strProjectId = String.valueOf(projectId);
	    	map_proAmount.put(strProjectId,douCost);
	    }
	    System.debug('------map_proAmount-------'+map_proAmount);
	    list<project__c> list_pro = [select Id, costs_total_amount__c from project__c where Id IN : set_proId];
	    for(project__c pro : list_pro)
	    {
	    	If(map_proAmount.containsKey(pro.Id))
	    	{
	    		pro.costs_total_amount__c = map_proAmount.get(pro.Id);
	    	}else{
	    		pro.costs_total_amount__c = 0;	
	    	}
	    }
	    if(list_pro != null && list_pro.size()>0)
	    {
	    	update list_pro;
	    }
	}
}