/** 
*	Spring
*   2014-10-21
*	“利息费用”汇总到项目的“收款超期费用”
*
*/
trigger ReDetailAutoSumToProject on reDetail__c (after delete, after insert, after update) {
	System.debug('-----------ReDetailAutoSumToProject----------');
	set<ID> set_proId = new set<ID>();
	if(!trigger.isDelete)
	{
		for(reDetail__c obj : trigger.new)
		{
			if(obj.FinanceVerificated__c == '已确认')set_proId.add(obj.relateProject__c);
		}
	}else{
		for(reDetail__c obj : trigger.old)
		{
			if(obj.FinanceVerificated__c == '已确认')set_proId.add(obj.relateProject__c);
		}	
	}	
	System.debug('------set_proId-------'+set_proId);
	//任务ID，发生总费用
	map<String,Double> map_proAmount = new map<String,Double>(); 	
    for(AggregateResult result : [select sum(interestCost__c) cost, relateProject__c from reDetail__c where 
        		FinanceVerificated__c = '已确认' and relateProject__c IN : set_proId  Group by relateProject__c])
    {
    	object cost = result.get('cost');
    	object projectId = result.get('relateProject__c');
    	
    	Double douCost = 0;
    	String strProjectId = '';
    	
    	if(cost!=null)douCost = Double.valueOf(cost);
    	if(projectId!=null)strProjectId = String.valueOf(projectId);
    	map_proAmount.put(strProjectId,douCost);
    }
    System.debug('------map_proAmount-------'+map_proAmount);
    list<project__c> list_pro = [select Id, getSuperCost__c from project__c where Id IN : set_proId];
    for(project__c pro : list_pro)
    {
    	If(map_proAmount.containsKey(pro.Id))
    	{
    		pro.getSuperCost__c = map_proAmount.get(pro.Id);
    	}else{
    		pro.getSuperCost__c = 0;	
    	}
    }
    if(list_pro != null && list_pro.size()>0)
    {
    	update list_pro;
    }
}