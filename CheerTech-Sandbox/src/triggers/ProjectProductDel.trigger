/*************************************
 *spring 2014-10-29
 *当删除一个考核产品时，连带删除个人产品贡献
 *************************************/
trigger ProjectProductDel on ProjectProduct__c (before delete) {
	if(trigger.isDelete){
		List<ProjectProduct__c> obj = trigger.old;
		for(ProjectProduct__c o : obj){
			//项目id
			String projectId = o.ProductProject__c;
			//产品id
			String productId = o.ProjectProduct__c;
			//查询产品贡献
			List<ProductContribute__c> pctb = [Select p.ProjectProduct__r.ProjectProduct__c, p.ProjectProduct__r.ProductProject__c, p.ProjectProduct__c, p.Id From ProductContribute__c p where p.ProjectProduct__r.ProductProject__c =:projectId and p.ProjectProduct__r.ProjectProduct__c =:productId ];
			System.debug('-----要删除的人----'+pctb);
			if(pctb != null && pctb.size() >0){
				delete pctb;
			}
		}
	}
}