trigger EventManagerCannotDeleteEditOthers on event__c (after delete, before update) {
     Profile p = [SELECT Id,Name FROM Profile WHERE Id=:UserInfo.getProfileId()];
	if(p.Name != '系统管理员')
	{
	
     if(Trigger.isUpdate)
     {
	    for(event__c eNew:trigger.new)
          {
          	
          	for(event__c eOld:trigger.old)
			{
				if(eNew.Id == eOld.Id)
				{
					Boolean IsOk = false;
					
					IsOk = (eNew.eventAction__c != eOld.eventAction__c )||
							(eNew.Name != eOld.Name )||
							(eNew.eventDate__c != eOld.eventDate__c )||
							(eNew.taskType__c != eOld.taskType__c )||
							(eNew.project__c != eOld.project__c )||
							(eNew.eventDetail__c != eOld.eventDetail__c );
					if(IsOk)
					{
						 if(eNew.CreatedById != UserInfo.getUserId() )
					    {
					        
					            trigger.new[0].addError('非进程纪要创建人员不能操作');
					        
					    }
					}
				}
			}
          	
		          	

        }
     }
     if(Trigger.isDelete)
     {
     	 for(event__c eNew:trigger.old)
           {
	      if(eNew.CreatedById != UserInfo.getUserId() )
		    {
		        
		            trigger.old[0].addError('非进程纪要创建人员不能操作');
		        
		    }
        }
     }
	}
}