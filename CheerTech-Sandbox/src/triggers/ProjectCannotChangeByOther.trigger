trigger ProjectCannotChangeByOther on project__c (before update) {
	
	Profile p = [SELECT Id,Name FROM Profile WHERE Id=:UserInfo.getProfileId()];
	system.debug(p);
	if(p.Name != '系统管理员')
	{
	
	
	for(project__c pNew:trigger.new)
	{
		for(project__c pOld:trigger.old)
		{
			if(pNew.Id == pOld.Id)
			{
				
				Boolean IsOk = false;
					
				IsOk =  (pNew.Name != pOld.Name)||
						(pNew.RecordType != pOld.RecordType)||
						(pNew.projectStatus__c != pOld.projectStatus__c)||
						(pNew.relateAccount__c != pOld.relateAccount__c)||
						(pNew.projectCategory__c != pOld.projectCategory__c)||
						(pNew.projectType__c != pOld.projectType__c)||
						(pNew.projectEndDate__c != pOld.projectEndDate__c)||
						(pNew.projectDetail__c != pOld.projectDetail__c)||
						(pNew.projectStartDate__c != pOld.projectStartDate__c);
								
				if(IsOk)
				{
					if(pNew.CreatedById != UserInfo.getUserId() )
					{
						trigger.new[0].addError('非项目创建人员不能修改项目信息');
					}
				}
				
			}
		}
		
		
		
		
	}
	
	}

}