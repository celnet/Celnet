/*************************************************
trigger:      AutoShareWithManager
Description: 项目人天自动共享给合同 和项目创建人员
Table Accessed: 
Table Updated:  
Author：Snowz.zhu
CreateDate：   2014年8月3日15:42:23
*************************************************/
trigger AutoShareWithManager on Staff_contribution__c (after insert) {
    /****
    这里写了一个内部类，帮助我沥青思路，字段太多，写个内部类比较清晰，
    将一些需要的字段放在一个内部类中，然后处理的时候遍历起链表就行
    *****/
    public class AllProjectInfo
    {
        Id project{get;set;}
        Id contract{get;set;}
        //下面这个字段是项目共享信息，暂时没用，我怕客户提出需要给项目上所有人员（项目是私有的，需要共享给需要的人）共享，所有暂时保留
        project__Share ps{get;set;}
        Staff_contribution__Share ProjectManagerscs{get;set;}
        Staff_contribution__Share ContractManagerscs{get;set;}
        Staff_contribution__c sc{get;set;}
        Id porjectuser{get;set;}
        Id contractuser{get;set;}
       public AllProjectInfo()
       {
        ps = new project__Share();
        ProjectManagerscs = new Staff_contribution__Share();
        ContractManagerscs = new Staff_contribution__Share();
        sc = new Staff_contribution__c();
       }
    }
    //所有创建的共享 项都放在这个链表中  需要insert
    list<Staff_contribution__Share> scsList = new list<Staff_contribution__Share>(); 
    List <AllProjectInfo> apiList = new List<AllProjectInfo>();
    list <Id> ProjectId=new list<Id>();
    List <Id> ContractId=new List<Id>();
    for(Staff_contribution__c sc:trigger.new)
    {
        ProjectId.add(sc.HRL_project__c);
        ContractId.add(sc.ProjectContract__c);
    }
    List <project__c> projectList = [select Id from  project__c  where Id in:ProjectId];
    List <project__Share> ProjectShareList=[select Id,ParentId,RowCause,UserOrGroupId
                                       from  project__Share  where ParentId in:ProjectId];
                                       
    List<Staff_contribution__c> scNewList = [SELECT Id, CreatedById,HRL_project__c,HRL_project__r.CreatedById,HRL_project__r.OwnerId, 
                            ProjectContract__c,ProjectContract__r.CreatedById FROM Staff_contribution__c where Id in:trigger.new];
    map<Id,project__Share> ProjectIdShareMap = new Map<Id,project__Share>();
    for(project__Share ps:ProjectShareList)
    {
        ProjectIdShareMap.put(ps.ParentId,ps);
    }                         
    for(Staff_contribution__c sc:scNewList)
    {
        AllProjectInfo api = new AllProjectInfo();
        api.project = sc.HRL_project__c;
        api.contract = sc.ProjectContract__c;
        api.ps = ProjectIdShareMap.get(sc.HRL_project__c);
        api.porjectuser = sc.HRL_project__r.CreatedById;
        api.contractuser = sc.ProjectContract__r.CreatedById;
        api.sc =sc;
        apiList.add(api);
        
    }
    
    for(AllProjectInfo api :apiList)
    {
        api.ProjectManagerscs.ParentId = api.sc.Id;
        api.ProjectManagerscs.UserOrGroupId = api.porjectuser;
        api.ProjectManagerscs.AccessLevel = 'Read';
        api.ContractManagerscs.ParentId = api.sc.Id;
        api.ContractManagerscs.UserOrGroupId = api.contractuser;
        api.ContractManagerscs.AccessLevel = 'Read';
        if(api.porjectuser != api.sc.CreatedById)
        {
            scsList.add(api.ProjectManagerscs);
        }
        if(api.contractuser!= api.sc.CreatedById)
        {
            scsList.add(api.ContractManagerscs);
        }
        
        
    }
    insert(scsList);
}