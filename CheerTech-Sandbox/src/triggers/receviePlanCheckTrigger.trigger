//author:snow 2014-6-27
/*
 * Author:Snow
 * Date:2014-6-27
 * Description:填写收款计划时，销售会录入金额或比例，这两个字段相互转化（即 比例优先原则）
 * 1.如果收款金额等于空和比例不为空   收款金额等于收款比例 * 合同总金额
 * 2.如果收款金额不等于空 和 比例等于空   则比例等于收款金额 /合同总金额
 * 3.两者都不为空，则收款金额等于收款比例 * 合同总金额
 */



trigger receviePlanCheckTrigger on receviePlan__c (before insert, before update) {
//获取合同的ID集合
    set<ID> set_ConId = new set<ID>();
    for(receviePlan__c rp : Trigger.new)
    {
      set_ConId.add(rp.relateContract__c);
    }  
    
    //获取合同下的合同金额
    map<ID,Double> map_ConAmount = new map<ID,Double>();
    for(contract__c contract : [Select c.Id, c.ContractAmount__c From contract__c c 
        Where Id IN:set_ConId and c.ContractAmount__c != null and c.ContractAmount__c != 0])
    {
      map_ConAmount.put(contract.Id,contract.ContractAmount__c);
    }
    
    //计算比例和收款金额
    for(receviePlan__c rp : Trigger.new)
    {
    if(!map_ConAmount.containsKey(rp.relateContract__c))
    {
      rp.addError('合同总金额不存在，不可以创建收款计划');
    }
    if((null==rp.proportion__c)&&(null == rp.reMoney__c ))
    {
      rp.addError('收款比例和收款金额不能同时为空值');
    }
    if(rp.proportion__c != null)
    {
      rp.reMoney__c = map_ConAmount.get(rp.relateContract__c)*rp.proportion__c/100;
    }else if(rp.reMoney__c != null){
      rp.proportion__c = rp.reMoney__c*100/map_ConAmount.get(rp.relateContract__c);
    }
    
    }  
}