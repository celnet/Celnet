/*******************************
 *Spring
 *2014-10-31
 *当项目阶段增删改时，处理对人员的产品贡献的影响
********************************/
trigger ProjectStageTransaction on ProjectStage__c (after delete, after insert, after update) {
	//阶段的增删改都要把人员的产品贡献重算
	//获得这个项目最新的阶段
	String projectId;
	if(trigger.isDelete){
		//不存在批量操作
		ProjectStage__c cc = Trigger.old[0];
		projectId = cc.StageProject__c;
	}else{
		ProjectStage__c cc = Trigger.new[0];
		projectId = cc.StageProject__c;
	}
	//查询最新阶段
	List<ProjectStage__c> list_projectStage = [select StagePercentage__c,id from ProjectStage__c where StageProject__c =:projectId];
	//查询人员贡献
	List<StageContribute__c> list_StageContribute = [select StageContributePercentage__c,MPersonName__c,ContributeStage__c from StageContribute__c where ContributeStage__r.StageProject__c =:projectId];
	//查询产品
	List<ProjectProduct__c> list_projectProduct = [select ProductAmount__c,ProductProject__c from ProjectProduct__c where ProductProject__c =:projectId];
	//查询产品贡献
	List<ProductContribute__c> list_ProductContribute = [select PcontributePercentage__c,ProjectProduct__c,PersonName__c,id from ProductContribute__c where ProjectProduct__r.ProductProject__c =:projectId];
	//考核人
	List<projectPerson__c> list_projectPerson = [select joinPerson__c,relateProject__c from projectPerson__c where relateProject__c =:projectId];
	//计算人员贡献度系数
	decimal temp;
	decimal temp1;
	for(projectPerson__c per : list_projectPerson){
		temp = 0;
		for(ProjectStage__c proStage : list_projectStage){
			for(StageContribute__c stageCon :list_StageContribute){
				if(proStage.id == stageCon.ContributeStage__c && stageCon.MPersonName__c == per.joinPerson__c){
					if(proStage.StagePercentage__c == null){ proStage.StagePercentage__c = 0; }
                    if(stageCon.StageContributePercentage__c == null){stageCon.StageContributePercentage__c = 0 ;}
					temp += proStage.StagePercentage__c * stageCon.StageContributePercentage__c * 0.01*0.01;
				}
			}
		}
		for(ProjectProduct__c pp : list_projectProduct){
			for(ProductContribute__c p : list_ProductContribute){
				if(p.PersonName__c == per.joinPerson__c && pp.id == p.ProjectProduct__c ){
					//重新赋值
					temp1 =  pp.ProductAmount__c * temp;
					System.debug('-----temp1----'+temp1);
					p.PcontributePercentage__c = temp1.setScale(2);
				}
			}
		}
	}
	System.debug(list_ProductContribute);
	//更新产品贡献
	update list_ProductContribute;
}