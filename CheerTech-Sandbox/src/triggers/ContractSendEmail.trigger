/*************************************************
Description: 合同生成或发生变更后，自动发送邮件通知相关负责人。
Author：Ward.Zhou
CreateDate：   2014-9-28
2014-10-25 修改
合同修改时，只有合同金额发生改变才发送邮件和通知
*************************************************/
trigger ContractSendEmail on contract__c (after insert, after update) 
{
    
    if(trigger.isInsert)
    {
    	//把所有邮箱不为空的用户放入set_AllUser以备检查是否是用户，而不是小组。
    	set<Id> set_AllUser = new set<Id>();
    	for(User U:[select Id from User where Email!=null])
    	{
    		set_AllUser.Add(U.Id);
    	}
    	
        set<Id> set_myUser=new set<Id>();//所有本次需要邮件和任务提醒用户的Id
        
        //把所有本次处理的合同的项目Id放入ProjectIdSet
        set<Id> ProjectIdSet=new set<Id>();
        for(contract__c Cont:trigger.new)
        {
            ProjectIdSet.Add(Cont.relateProject__c);
        }
        
        //项目和项目人员匹配的Map
        map<Id,list<Id>> Map_ProjectUser=new map<Id,list<Id>>();
        list<Id> List_UserId ;
        for(project__Share U:[Select p.UserOrGroupId, p.RowCause, p.ParentId, p.Id, p.AccessLevel 
                    From project__Share p where ParentId IN:ProjectIdSet])
        {
            if(Map_ProjectUser.containsKey(U.ParentId))
            {
                List_UserId=Map_ProjectUser.get(U.ParentId);
                List_UserId.Add(U.UserOrGroupId);
                Map_ProjectUser.Put(U.ParentId,List_UserId);
            }
            else
            {
                List_UserId=new list<Id>();
                List_UserId.Add(U.UserOrGroupId);
                Map_ProjectUser.Put(U.ParentId,List_UserId);
            }
        }
        
        //用户Id和邮件匹配的Map
        map<Id,string> Map_AllUserMail=new map<Id,string>();
        for(User Ur:[select Id, Email from User where Email!=null])
        {
            Map_AllUserMail.put(Ur.Id,Ur.Email);
        }
        
        //用户小组Id和小组内元素匹配的Map
        map<Id,list<Id>> Map_GroupUser = new  map<Id,list<Id>>();
        list<Id> List_gUserId;
        for(GroupMember g: [Select g.UserOrGroupId, g.GroupId From GroupMember g])
        {
        	if(Map_GroupUser.containsKey(g.GroupId))
            {
                List_gUserId=Map_GroupUser.get(g.GroupId);
                List_gUserId.Add(g.UserOrGroupId);
                Map_GroupUser.Put(g.GroupId,List_gUserId);
            }
            else
            {
                List_gUserId=new list<Id>();
                List_gUserId.Add(g.UserOrGroupId);
                Map_GroupUser.Put(g.GroupId,List_gUserId);
            }
        }
        
        //构建客户小组里的UserORGroupId和角色里人员Id匹配的Map
        map<Id,list<Id>> Map_RoleUser = new  map<Id,list<Id>>();
        map<Id,Id> Map_RaleGroup = new  map<Id,Id>();
        list<Id> List_rUserId;
        for(Group Gp:[Select g.RelatedId, g.Id From Group g where RelatedId!=null])
        {
        	Map_RaleGroup.put(Gp.Id,Gp.RelatedId);
        }
        for(User Ur:[Select Id, UserRoleId From User where UserRoleId!=null ])
        {
        	
        		if(Map_RoleUser.containsKey(Ur.UserRoleId))
        		{
        			List_rUserId = Map_RoleUser.get(Ur.UserRoleId);
        			List_rUserId.Add(Ur.Id);
        			Map_RoleUser.put(Ur.UserRoleId,List_rUserId);
        		}
        		else
        		{
        			List_rUserId = new list<Id>();
        			List_rUserId.Add(Ur.Id);
        			Map_RoleUser.put(Ur.UserRoleId,List_rUserId);
        		}
        	
        }
        
        
        for(Contract__c Con:trigger.new)
        {
            if(Map_ProjectUser.get(Con.relateProject__c)!=null)
            {
                list<Id> List_UserOrGroup = Map_ProjectUser.get(Con.relateProject__c);
                for(Id i:List_UserOrGroup)
            	{
            		//如果i是用户，则加入到用户list
            		if(set_AllUser.contains(i))
            		{
            			set_myUser.Add(i);
            		}
            		//如果不是用户，那么就是用户小组
            		else
            		{
            			if(Map_GroupUser.get(i)!=null)
            			{
            				List<Id> List_GroupUserId = Map_GroupUser.get(i);
            				for(Id gu :List_GroupUserId)
	                		{
	                			//如果用户小组里的是用户，则加入用户List
	                			if(set_AllUser.contains(gu))
				            	{
				            		set_myUser.add(gu);
				            	}
				            	//如果不是用户，那么就是角色
				            	else
				            	{
				            		system.debug('ROlE****************'+gu);
				            		if(Map_RaleGroup.get(gu)!=null)
				            		{
				            			Id TempId = Map_RaleGroup.get(gu);
				            			if(Map_RoleUser.get(TempId)!=null)
				            			{
					            			List<Id> List_UserRoleId = Map_RoleUser.get(TempId);
					            			for(Id ru:List_UserRoleId)
					            			{	
					            				if(set_AllUser.contains(ru))
					            				{
					            					set_myUser.add(ru);
					            				}
					            			}
				            			}
				            		}
				            	}
	                		}
            			}
            		}
            	}
            }
            else
            {
                continue;
            }
            
            Id MyId = UserInfo.getUserId();
            if(set_myUser.contains(MyId))
            {
            	set_myUser.remove(MyId);
            }
            
            system.debug('************************haha'+set_myUser);
            list<string> List_Email = new list<string>();
            for(Id i:set_myUser)
            {
	            if(Map_AllUserMail.get(i)!=null)
	            {
	            	List_Email.Add(Map_AllUserMail.get(i));
	            }
            }
            system.debug('************************haha'+List_Email);
            
            if(List_Email.size()==0)
            {
                continue;
            }
                
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String repBody = '';
            
            repBody+='您好，'+'<br>'+'合同：'+Con.Name+'现已新建,请点击链接查看详细信息：';
            repbody+='<Br>'+'https://ap1.salesforce.com/'+Con.Id; 
            repBody += '<br> 如有任何疑问或者要求，请联系系统管理人员。<br>';
            string title='合同：'+Con.Name+'新建完成，现注意';
            //String[] repAddress =new string[]{emailAddress};
            String[] repAddress = List_Email;
            mail.setCharset('UTF-8');
            mail.setToAddresses(repAddress);
            mail.setHtmlBody(repBody);
            mail.setSubject(title);
            mail.setSenderDisplayName('Salesforce');
            //system.debug('**************emailAddress****************'+emailAddress);
        
        // *****************start****************************
    	 system.debug('OO##@@@@@@@@@@@@@@@@@@@@OOO@@@@#####'+repAddress);
            if(repAddress != null)
            {
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                
            }
        
     		list<Task> List_Task = new list<Task>();
     		for(Id U:set_myUser)
     		{
     			if(set_AllUser.contains(U))
     			{
     			Task T = new Task();
     			T.IsReminderSet = true;
     			T.Priority = 'Normal';
     			T.ReminderDateTime = datetime.now();
     			//T.Status='Completed';
     			T.Subject = title;
     			T.OwnerId = U;
     			List_Task.Add(T);
     			}
     			
     		}
     		//system.debug(List_Task);
     		insert List_Task;
        
        
        }
        
      }
      
      //---IsInsert结束---------------------------------------------------------------------------------------------------
      
      
      if(trigger.isUpdate)
      {
      	//把所有邮箱不为空的用户放入set_AllUser以备检查是否是用户，而不是小组。
    	set<Id> set_AllUser = new set<Id>();
    	for(User U:[select Id from User where Email!=null])
    	{
    		set_AllUser.Add(U.Id);
    	}
    	
        set<Id> set_myUser=new set<Id>();//所有本次需要邮件和任务提醒用户的Id
        
        //把所有本次处理的合同的项目Id放入ProjectIdSet
        set<Id> ProjectIdSet=new set<Id>();
        for(contract__c Cont:trigger.new)
        {
            ProjectIdSet.Add(Cont.relateProject__c);
        }
        
        //项目和项目人员匹配的Map
        map<Id,list<Id>> Map_ProjectUser=new map<Id,list<Id>>();
        list<Id> List_UserId ;
        for(project__Share U:[Select p.UserOrGroupId, p.RowCause, p.ParentId, p.Id, p.AccessLevel 
                    From project__Share p where ParentId IN:ProjectIdSet])
        {
            if(Map_ProjectUser.containsKey(U.ParentId))
            {
                List_UserId=Map_ProjectUser.get(U.ParentId);
                List_UserId.Add(U.UserOrGroupId);
                Map_ProjectUser.Put(U.ParentId,List_UserId);
            }
            else
            {
                List_UserId=new list<Id>();
                List_UserId.Add(U.UserOrGroupId);
                Map_ProjectUser.Put(U.ParentId,List_UserId);
            }
        }
        
        //用户Id和邮件匹配的Map
        map<Id,string> Map_AllUserMail=new map<Id,string>();
        for(User Ur:[select Id, Email from User where Email!=null])
        {
            Map_AllUserMail.put(Ur.Id,Ur.Email);
        }
        
        //用户小组Id和小组内元素匹配的Map
        map<Id,list<Id>> Map_GroupUser = new  map<Id,list<Id>>();
        list<Id> List_gUserId;
        for(GroupMember g: [Select g.UserOrGroupId, g.GroupId From GroupMember g])
        {
        	if(Map_GroupUser.containsKey(g.GroupId))
            {
                List_gUserId=Map_GroupUser.get(g.GroupId);
                List_gUserId.Add(g.UserOrGroupId);
                Map_GroupUser.Put(g.GroupId,List_gUserId);
            }
            else
            {
                List_gUserId=new list<Id>();
                List_gUserId.Add(g.UserOrGroupId);
                Map_GroupUser.Put(g.GroupId,List_gUserId);
            }
        }
        
        //构建客户小组里的UserORGroupId和角色里人员Id匹配的Map
        map<Id,list<Id>> Map_RoleUser = new  map<Id,list<Id>>();
        map<Id,Id> Map_RaleGroup = new  map<Id,Id>();
        list<Id> List_rUserId;
        for(Group Gp:[Select g.RelatedId, g.Id From Group g where RelatedId!=null])
        {
        	Map_RaleGroup.put(Gp.Id,Gp.RelatedId);
        }
        for(User Ur:[Select Id, UserRoleId From User where UserRoleId!=null ])
        {
        	
        		if(Map_RoleUser.containsKey(Ur.UserRoleId))
        		{
        			List_rUserId = Map_RoleUser.get(Ur.UserRoleId);
        			List_rUserId.Add(Ur.Id);
        			Map_RoleUser.put(Ur.UserRoleId,List_rUserId);
        		}
        		else
        		{
        			List_rUserId = new list<Id>();
        			List_rUserId.Add(Ur.Id);
        			Map_RoleUser.put(Ur.UserRoleId,List_rUserId);
        		}
        	
        }
        
        
        for(Contract__c Con:trigger.new)
        {
        	
        	if(Con.ContractAmount__c==trigger.oldMap.get(Con.Id).ContractAmount__c)
        	{
        		continue;
        	}
        	
            if(Map_ProjectUser.get(Con.relateProject__c)!=null)
            {
                list<Id> List_UserOrGroup = Map_ProjectUser.get(Con.relateProject__c);
                for(Id i:List_UserOrGroup)
            	{
            		//如果i是用户，则加入到用户list
            		if(set_AllUser.contains(i))
            		{
            			set_myUser.Add(i);
            		}
            		//如果不是用户，那么就是用户小组
            		else
            		{
            			if(Map_GroupUser.get(i)!=null)
            			{
            				List<Id> List_GroupUserId = Map_GroupUser.get(i);
            				for(Id gu :List_GroupUserId)
	                		{
	                			//如果用户小组里的是用户，则加入用户List
	                			if(set_AllUser.contains(gu))
				            	{
				            		set_myUser.add(gu);
				            	}
				            	//如果不是用户，那么就是角色
				            	else
				            	{
				            		system.debug('ROlE****************'+gu);
				            		if(Map_RaleGroup.get(gu)!=null)
				            		{
				            			Id TempId = Map_RaleGroup.get(gu);
				            			List<Id> List_UserRoleId = Map_RoleUser.get(TempId);
				            			for(Id ru:List_UserRoleId)
				            			{	
				            				if(set_AllUser.contains(ru))
				            				{
				            					set_myUser.add(ru);
				            				}
				            			}
				            		}
				            	}
	                		}
            			}
            		}
            	}
            }
            else
            {
                continue;
            }
            
            Id MyId = UserInfo.getUserId();
            if(set_myUser.contains(MyId))
            {
            	set_myUser.remove(MyId);
            }
            
            //system.debug('************************haha'+set_myUser);
            list<string> List_Email = new list<string>();
            for(Id i:set_myUser)
            {
	            if(Map_AllUserMail.get(i)!=null)
	            {
	            	List_Email.Add(Map_AllUserMail.get(i));
	            }
            }
            system.debug('************************haha'+List_Email);
            
            if(List_Email.size()==0)
            {
                continue;
            }
                
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String repBody = '';
            
            repBody+='您好，'+'<br>'+'合同：'+Con.Name+'现状态发生更改，请点击链接查看详细信息：';
            repbody+='<Br>'+'https://ap1.salesforce.com/'+Con.Id; 
            repBody += '<br> 如有任何疑问或者要求，请联系系统管理人员。<br>';
            string title='合同：'+Con.Name+'状态发生更改，请注意!';
            //String[] repAddress =new string[]{emailAddress};
            String[] repAddress = List_Email;
            mail.setCharset('UTF-8');
            mail.setToAddresses(repAddress);
            mail.setHtmlBody(repBody);
            mail.setSubject(title);
            mail.setSenderDisplayName('Salesforce');
            //system.debug('**************emailAddress****************'+emailAddress);
        
        // *****************start****************************
    	 
            if(repAddress != null)
            {
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                
            }
        
     		list<Task> List_Task = new list<Task>();
     		for(Id U:set_myUser)
     		{
     			if(set_AllUser.contains(U))
     			{
     			Task T = new Task();
     			T.IsReminderSet = true;
     			T.Priority = 'Normal';
     			T.ReminderDateTime = datetime.now();
     			//T.Status='Completed';
     			T.Subject = title;
     			T.OwnerId = U;
     			List_Task.Add(T);
     			}
     			
     		}
     		//system.debug(List_Task);
     		insert List_Task;
        
        
        }
      }
    
        

}