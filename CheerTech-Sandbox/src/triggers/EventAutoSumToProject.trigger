/** 
*	功能：进程记要自动汇总金额
*	作者：bill
*	日期：2014-6-27 
*   
*	修改:
*	Spring
*   2014-10-21
*	将该任务下所有进程纪要自动汇总到任务的费用下
*
*/
trigger EventAutoSumToProject on event__c (after update, after delete ) 
{
	System.debug('-----------EventAutoSumToProject----------');
	//---任务id
	set<ID> set_proId = new set<ID>();
	if(!trigger.isDelete)
	{
		for(event__c event : trigger.new)
		{
			System.debug(event.eventCostApprove__c+'-----event.eventCostApprove__c------event----------'+event);
			//if(event.eventCostApprove__c == '已审批')set_proId.add(event.project__c);
			if(event.eventCostApprove__c == '已审批')set_proId.add(event.EventTask__c);
		}
	}else{
		for(event__c event : trigger.old)
		{
			//if(event.eventCostApprove__c == '已审批')set_proId.add(event.project__c);
			if(event.eventCostApprove__c == '已审批')set_proId.add(event.EventTask__c);
		}	
	}	
	System.debug('------set_proId-------'+set_proId);
	//任务ID，发生总费用
	map<String,Double> map_proAmount = new map<String,Double>(); 	
    for(AggregateResult result : [select sum(eventCost__c) cost, EventTask__c from event__c where 
        		eventCostApprove__c = '已审批' AND EventTask__c IN : set_proId  Group by EventTask__c])
    {
    	object cost = result.get('cost');
    	object projectId = result.get('EventTask__c');
    	
    	Double douCost = 0;
    	String strProjectId = '';
    	
    	if(cost!=null)douCost = Double.valueOf(cost);
    	if(projectId!=null)strProjectId = String.valueOf(projectId);
    	map_proAmount.put(strProjectId,douCost);
    }
    System.debug('------map_proAmount-------'+map_proAmount);
    list<Assignment__c> list_pro = [select Id, AssignmentCost__c from Assignment__c where Id IN : set_proId];
    for(Assignment__c pro : list_pro)
    {
    	If(map_proAmount.containsKey(pro.Id))
    	{
    		pro.AssignmentCost__c = map_proAmount.get(pro.Id);
    	}else{
    		pro.AssignmentCost__c = 0;	
    	}
    }
    if(list_pro != null && list_pro.size()>0)
    {
    	update list_pro;
    }
    
	
	
	//项目ID，发生总费用
	/*map<String,Double> map_proAmount = new map<String,Double>(); 	
    for(AggregateResult result : [select sum(eventCost__c) cost, project__c from event__c where 
        		eventCostApprove__c = '已审批' AND project__c IN : set_proId  Group by project__c])
    {
    	object cost = result.get('cost');
    	object projectId = result.get('project__c');
    	
    	Double douCost = 0;
    	String strProjectId = '';
    	
    	if(cost!=null)douCost = Double.valueOf(cost);
    	if(projectId!=null)strProjectId = String.valueOf(projectId);
    	map_proAmount.put(strProjectId,douCost);
    }
    
    list<project__c> list_pro = [select Id, costs_total_amount__c from project__c where Id IN : set_proId];
    for(project__c pro : list_pro)
    {
    	If(map_proAmount.containsKey(pro.Id))
    	{
    		pro.costs_total_amount__c = map_proAmount.get(pro.Id);
    	}else{
    		pro.costs_total_amount__c = 0;	
    	}
    }
    if(list_pro != null && list_pro.size()>0)
    {
    	update list_pro;
    }
    */
}