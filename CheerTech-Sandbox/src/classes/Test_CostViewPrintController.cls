/*************************************************
class:      Test_CostViewPrintController
Description:   进程纪要打印页面控制器测试类 
Table Accessed: 
Table Updated:  
Author：Snowz.zhu
CreateDate：   2014-07-02
*************************************************/
@isTest
private class Test_CostViewPrintController {

    static testMethod void myUnitTest() {
        humanRresources__c hR = new humanRresources__c();
        hR.Name = '100';
        insert hR;
        
        department__c dp = new department__c();
        dp.Name = '软件部';
        insert dp;
        
        HRManage__c hrm = new HRManage__c();
        hrm.Name = '商务人员';
        hrm.department__c = dp.Id;
        hrm.HR__c = hR.Id;
        hrm.department__c =dp.Id;
        insert hrm;
        
        Account a = new Account();
        a.Name='au8i';
        a.accountCode__c='adfsf';
        a.status_del__c='启用';
        insert a;
        
        
        project__c p = new project__c();
        p.Name = 'p';
        p.projectType__c='不间断电源';
        p.projectStatus__c = '新建';
        p.projectStartDate__c=Date.today();
        p.relateAccount__c=a.Id;
        insert p;
        
        event__c e=new event__c();
        e.Name = '600yuan';
        e.project__c = p.Id;
        e.eventActionPerson__c=hrm.Id;
        e.eventDate__c=Date.today();
        e.eventCostApprove__c = '审批中';
        insert e;
        
        CostDetail__c cd = new CostDetail__c();
        cd.costAmount__c=100;
        cd.relateEvent__c=e.Id;
        //cd.relateProject__c=p.Id;
        
        cd.Name='83723e';
        
        insert cd;
        CostDetail__c cd1 = new CostDetail__c();
        cd1.costAmount__c=100;
        cd1.relateEvent__c=e.Id;
       // cd1.relateProject__c=p.Id;
        cd1.Name='82372f';
        
        insert cd1;
        
          CostDetail__c cd2 = new CostDetail__c();
        cd2.costAmount__c=100;
        cd2.relateEvent__c=e.Id;
     //   cd2.relateProject__c=p.Id;
        
        cd2.Name='837323';
        
        insert cd2;
        
        CostDetail__c cd3 = new CostDetail__c();
        cd3.costAmount__c=100;
        cd3.relateEvent__c=e.Id;
      //  cd3.relateProject__c=p.Id;
        cd3.Name='83732f';
        
        
        insert cd3;
        
          CostDetail__c cd4 = new CostDetail__c();
        cd4.costAmount__c=100;
        cd4.relateEvent__c=e.Id;
      //  cd4.relateProject__c=p.Id;
        cd4.Name='834732f';
        insert cd4;
        
        CostDetail__c cd5 = new CostDetail__c();
        cd5.costAmount__c=100;
        cd5.relateEvent__c=e.Id;
       // cd5.relateProject__c=p.Id;
        cd5.Name='837532f';
        
        insert cd5;
         ApexPages.currentPage().getParameters().put('id',e.Id);
        ApexPages.currentPage().getParameters().put('userId',hrm.Id);
        CostViewPrintController cvpc = new CostViewPrintController();
    
    }
}