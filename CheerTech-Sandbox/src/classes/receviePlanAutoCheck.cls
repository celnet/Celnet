/*
Author:Ward
Time:2014-7-2
Function:receviePlanAutoCheck测试类
*/
@isTest
private class receviePlanAutoCheck {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
           Account Acc=new Account();
        Acc.Name='1';
        Acc.status_del__c='启用';
        insert Acc;
        
        humanRresources__c hum=new humanRresources__c();
        hum.Name='a';
        insert hum;
        department__c dep=new department__c();
        dep.Name='销售部';
        insert dep;
        
        HRManage__c hr=new HRManage__c();
        hr.Name='a';
        hr.department__c=dep.Id;
        hr.HR__c=hum.Id;
        insert hr;
        
        project__c proj=new project__c();
        proj.projectType__c='自助设备';
        proj.projectStatus__c='新建';
        proj.projectStartDate__c=date.today();
        proj.Name='1';
        insert proj;
        
        contract__c cont=new contract__c();
        cont.relateProject__c=proj.Id;
        cont.ContractNum__c='hkjhkj';
        cont.relateAccount__c=Acc.Id;
        cont.contractSummary__c='dfsafda';
        insert cont;
        
        
        //新建一个产品分类（主）和产品(子)
        productClassify__c pc=new productClassify__c();
        pc.Name='MNghfsgh';  
        pc.classifyCode__c='fdjslahfdjsa';
        pc.remark__c='sdfsagfgadfagf';
        insert pc;
        
        product__c pd1 = new product__c();
        pd1.Name='pos1';
        pd1.productClassify__c=pc.Id;
        pd1.Status__c='启用';
        product__c pd2 = new product__c();
        pd2.Name='pos2';
        pd2.Status__c='启用';
        pd2.productClassify__c=pc.Id;
        product__c pd3 = new product__c();
        pd3.Name='pos3';
        pd3.Status__c='启用';
        pd3.productClassify__c=pc.Id; 
        
        insert pd1;
        insert pd2;
        insert pd3;
        contractProduct__c pduct=new contractProduct__c();
        pduct.relateProject__c=proj.Id;
        pduct.relateContract__c=cont.Id;
        pduct.DealUnitPrice__c=111;
        pduct.quantity__c=1;
        pduct.contractProductName__c=pd1.Id;
        insert pduct;
        
        
        receviePlan__c rec=new receviePlan__c();
        rec.receviePlanDate__c=date.today();
        rec.relateContract__c=cont.Id;
        rec.relateProject__c=proj.Id;
        rec.reMoney__c=10.00;
        rec.relateContract__c=cont.Id;
        
        receviePlan__c rec1=new receviePlan__c();
        rec1.relateContract__c=cont.Id;
        rec1.relateProject__c=proj.Id;
        rec1.proportion__c=0.5;
        rec1.proportion__c=0.8;
        rec1.relateContract__c=cont.Id;
        insert rec;
    }
}