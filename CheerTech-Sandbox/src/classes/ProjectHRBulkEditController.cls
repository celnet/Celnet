/** 
*	功能：项目人员批量编辑
*	作者：thunder
*	日期：2014-5-16 
*/
public with sharing class ProjectHRBulkEditController {

//CostDetail class 集合
	public list<projectPerson> projectPersonList {get;set;}
	public list<projectPerson> projectPersonList1 {get;set;}
	public list<projectPerson> projectPersonList2 {get;set;}
	//项目ID
	public string projectId {get;set;}
	//费用类型ID集合
	public String projectPersonIds {get;set;}
	
	
	//	构造方法
	
	public ProjectHRBulkEditController()
	{
		this.projectId=Apexpages.currentPage().getParameters().get('pid');
	}
	public ProjectHRBulkEditController(ApexPages.StandardController controller)
	{
		project__c project = (project__c)controller.getRecord();
		this.projectId = project.Id;		
		init();
	}
	public void init()
	{	
		boolean is2= true;
		projectPersonList=new list<projectPerson>();
		projectPersonList1=new list<projectPerson>();
		projectPersonList2=new list<projectPerson>();
		for(projectPerson__c pP:[Select p.relateProject__c, p.relatePerson__r.Name, p.relatePerson__c, p.Name From projectPerson__c p where p.relateProject__c=:projectId])
		{
			projectPersonList.add(new ProjectPerson(false,pP));
			if(is2){
				projectPersonList1.add(new ProjectPerson(false,pP));
				is2=false;
			}else{
				projectPersonList2.add(new ProjectPerson(false,pP));
				is2=true;
			}
		}		
	}
	
	public class ProjectPerson
	{
		public boolean isDelete {get;set;}
		public projectPerson__c pP {get;set;}
		
		public ProjectPerson(boolean isDelete,projectPerson__c pP)
		{
			this.isDelete = isDelete;
			this.pP = pP;
		}
	}
	
	//	根据选择的费用类型ID集合自动创建行
	
	public void ProjectAddProjectHRs()
	{
		
		if(projectPersonIds.indexOf(',')==-1)
		{
			return;
		}
		list<String> str_cost = projectPersonIds.split(',');
		for(User user : [Select u.Id, u.Name, u.Department From User u where Id IN : str_cost])
		{
			
			projectPerson__c pP=new projectPerson__c();
			pP.relateProject__c = projectId;			
			pP.relatePerson__c = user.id;
			insert pP;	
			//projectPersonList.add(new ProjectPerson(false,pP));
			init();
		}
		
				
	}
	
	//	删除行
	
	public void DeleteProjectPerson()
	{	
		//设置一个删除集合
		list<projectPerson__c> listDeleteProjectPerson = new list<projectPerson__c>();
		//对listCostDetail进行循环，找出需要删除的CostDetail__c，放入删除集合中
		for(Integer i = projectPersonList.size()-1;i>=0;i--)
		{
			if(projectPersonList[i].isDelete)
			{	
				if(projectPersonList[i].pP.Id != null)
				{
					listDeleteProjectPerson.add(projectPersonList[i].pP);
				}
				projectPersonList.remove(i);
			}
		}
		if(listDeleteProjectPerson.size()>0)
		{
			//执行删除
			delete listDeleteProjectPerson;
		}
		//********************************************
		//设置一个删除集合
		listDeleteProjectPerson = new list<projectPerson__c>();
		//对listCostDetail进行循环，找出需要删除的CostDetail__c，放入删除集合中
		for(Integer i = projectPersonList1.size()-1;i>=0;i--)
		{
			if(projectPersonList1[i].isDelete)
			{	
				if(projectPersonList1[i].pP.Id != null)
				{
					listDeleteProjectPerson.add(projectPersonList[i].pP);
				}
				projectPersonList1.remove(i);
			}
		}
		if(listDeleteProjectPerson.size()>0)
		{
			//执行删除
			delete listDeleteProjectPerson;
		}
		//*****************************************************
		//设置一个删除集合
		listDeleteProjectPerson = new list<projectPerson__c>();
		//对listCostDetail进行循环，找出需要删除的CostDetail__c，放入删除集合中
		for(Integer i = projectPersonList2.size()-1;i>=0;i--)
		{
			if(projectPersonList2[i].isDelete)
			{	
				if(projectPersonList2[i].pP.Id != null)
				{
					listDeleteProjectPerson.add(projectPersonList[i].pP);
				}
				projectPersonList2.remove(i);
			}
		}
		if(listDeleteProjectPerson.size()>0)
		{
			//执行删除
			delete listDeleteProjectPerson;
		}
		init();
	}	
}