/**
author:thunder
date:2014-07-09
desc:批量添加收款信息
此页面对应项目合同下的收款详细 自定义页面，可以动态添加收款详细；
1，class BatchAddRecevie 为内部类， 包装了一下 原始数据格式，加上 IsDelete（页面为一个checkbox） 用来达到批量选中
    的效果
2，addToDeleteList 函数  因为客户要求在编辑状态下也有一个删除按钮，并且这个删除  是在客户按下保存的时候才真正删除
      1，用户按下 编辑-》 选中项-》删除-》保存-》 真正删除
      2，用户按下编辑-》 选中项-》删除-》取消-》恢复原样 
      所以我就先将其加入到待删除链表，在每次保存的时候都进行一下检查，如果待删除链表有值，则进行删除
3，AddRecevie  函数，用户要求可以动态添加，   addrecevie 就是这个功能
          用户按下页面按钮  触发这个action  初始化一个新的reDetail__c  并生成一个包装类，将其加入到包装类
    链表，再在页面上显示出来，并且重新计算下值

4 收款明细部分字段  ：   （因为收款计划  和收款详细合并为一个字段，所以既有计划字段  又有 收款明细字段）
                （销售填写收款计划，商务填写收款）   
    说明                              字段名     API                 数据类型                

                                    项目              relateProject__c    主-详细信息(项目)
                                    项目合同                relateContract__c   主-详细信息(项目合同)
    此备注为商务填写                    备注          reMoneyRemark__c    长文本区(32768)
    此为计划收款比例                    收款比例        PlanProportion__c   数字(16, 2)
    此为计划收款数字（= 总额   * 计划收款比例）收款金额       receiveMoney__c     数字(16, 2)
    
    
    此比例为实际收款比例                  比例          proportion__c   公式 (百分比) 
**/
public with sharing class BatchAddRecevieController {
    
    //public reDetail__c recevie{get;set;}
    //当前合同
    public contract__c contract{get;set;}
    //包装类链表
    public list<BatchAddRecevie> list_BatchAddRecevie{get;set;}
    //是否是编辑状态
    //非编辑状态不显示 保存按钮，编辑状态的 删除按钮只有保存了才生效，非编辑状态的删除按钮 直接生效
    public boolean IsEidt {get;set;}
    //下面的是收款 计划 比例金额   收款实际（开票）比例和金额  都是总    是手动计算，并且显示在页面上（客户要求要动态显示）
    public Decimal TotalPlanProportion{set;get;}
    public Decimal TotalPlanamount{set;get;}
    public Decimal TotalReceiveProportion{set;get;}
    public Decimal TotalReceiveAmount{set;get;}
    //保存是否成功，成功 页面进行刷新否则不进行刷新
    public Integer saveIsOk{set;get;}
    public Profile pfile{get;set;}//用户的profile
    //待删除链表
    public list<reDetail__c> listReDetail_delete {get;set;}
    
    list<reDetail__c> list_recevie{get;set;}
    public BatchAddRecevieController(ApexPages.StandardController controller){
        TotalPlanProportion=0;
        TotalPlanamount=0;
        TotalReceiveProportion=0;
        TotalReceiveAmount=0;
        
        IsEidt=false;
         listReDetail_delete = new list <reDetail__c>();
        contract=(contract__c)controller.getRecord();
        //查询一下合同信息      
        contract=[Select c.relateProject__c, c.relatePerson__c, CreatedById,c.relateAccount__c, c.contractSummary__c,
                         c.contractStatus__c, c.contractServiceMon__c, c.contractDetail__c, c.contractDate__c, 
                         c.contractAmountRev__c, c.contractAmountPay__c, c.contractAgent__c, c.SystemModstamp, 
                          c.RecordTypeId, c.PurchaseAmount__c, c.Name, c.IsDeleted, c.Id, 
                         c.ContractNum__c, c.ContractAmount__c From contract__c c where id=:contract.Id];
        //查询一下当前用户的票file、信息  因为页面要求  那些人能够操作 或者看见哪些列或字段     不能看见哪些列或字段
        //都是通过 pfile、 来判断当前用户的身份  从页面源码也能够看出来
        pfile = [SELECT Id, CreatedById, CreatedDate, Description, LastModifiedById, LastModifiedDate, Name, UserLicenseId, UserType FROM Profile where id=:UserInfo.getProfileId()];
        init();
    }
        
    public void init(){
        //查询一下当前已经存在的 收款 信息   并将其封装为一个包装类
         list_recevie=[Select r.FinanceVerificated__c, r.salesRemark__c, r.relateProject__c, r.relatePerson__c,
                        r.relatePerson__r.FirstName,r.relatePerson__r.LastName,r.reachDate__c ,r.lostDay__c,
                        r.relateContract__c, r.receiveMoney__c, r.reMoney__c, r.reMoneyRemark__c,
                        r.reData__c, r.proportion__c, r.outNum__c, r.operatorTime__c, r.invoice__c,
                        r.ReceivedAmount__c, r.PlanProportion__c, r.PlanDate__c, r.Name,
                        r.InvoiceDate__c, r.Id, r.ContractAccount__c From reDetail__c r
                        where r.relateContract__c=:contract.Id ORDER BY r.PlanDate__c Asc];
        
        list_BatchAddRecevie=new list<BatchAddRecevie>();
        BatchAddRecevie bar;
        if(list_recevie!=null && list_recevie.size()>0){
            for(reDetail__c re:list_recevie){
                if(re.PlanProportion__c == null)
                {
                    re.PlanProportion__c=0;
                }
                TotalPlanProportion += re.PlanProportion__c;
                TotalPlanProportion = Math.round(TotalPlanProportion * 100) / 100.0;
                        //为什么加上一个0呢？
                        //如果不加   上面的值经过 math  round、 函数处理后以科学计数法  在页面进行显示  什么 5e+2.。。。这种
                TotalPlanProportion+=0;
                
                TotalPlanamount = TotalPlanamount+re.receiveMoney__c;
                TotalReceiveProportion=TotalReceiveProportion+ re.proportion__c;
                TotalReceiveAmount=TotalReceiveAmount+re.reMoney__c;
                bar=new BatchAddRecevie(re);
                bar.invoiceProportion=re.proportion__c;
                bar.IsDelete = false;
                //加入到封装类链表，在页面上显示
                list_BatchAddRecevie.add(bar);
            }
        }                       
        
    }
    
    public void addToDeleteList()
    {
        //加入到待删除链表，从封装类链表 中删除
        for(Integer i = 0;i < list_BatchAddRecevie.size();i++)
        {
            if(list_BatchAddRecevie[i].isDelete)
            {
                if(list_BatchAddRecevie[i].recevie.Id != null)
                {
                    listReDetail_delete.add(list_BatchAddRecevie[i].recevie);
                }
                
                list_BatchAddRecevie.remove(i);
                //因为被删除一项，所以i 需要停留在此数组下标  
                //比如 第三项被删除了，i==2，删除后 i++ 为3 但是此时新数组还要从第三项 保证此时 i为2 所以i--
                i--;
            }
            
        }
        changeAll();
    }
    public class BatchAddRecevie{
        
        public boolean IsDelete{get;set;}
        public reDetail__c recevie{get;set;}
   
        public Decimal invoiceProportion{set;get;}//为了动态显示开票比例 汇总
        
        public BatchAddRecevie(reDetail__c re)
        {
            recevie=re;
            IsDelete=false;
        }
    
    }
    //添加行
    public void AddRecevie(){
        changeAll();
        reDetail__c re=new reDetail__c();
        re.relateProject__c=contract.relateProject__c;      
        re.relateContract__c=contract.id;
        re.PlanProportion__c=100-TotalPlanProportion;//收款比例, 计划收款比例
        re.receiveMoney__c =contract.ContractAmount__c *  re.PlanProportion__c * 0.01;//收款金额, 计划收款金额
        re.PlanDate__c = date.today();//计划收款日期
        re.salesRemark__c = '';//销售备注
        re.outNum__c='';//出库单编号
        re.invoice__c='';//发票编号
        re.ContractAccount__c=contract.ContractAmount__c;//合同金额
        //re.reMoney__c=0;//开票金额
        re.reMoney__c = re.receiveMoney__c;
        //re.proportion__c=0;//开票比例
        //re.proportion__c = re.PlanProportion__c;
        //re.InvoiceDate__c=date.today();//开票日期
        //re.reData__c=date.today();//收款日期
        re.reMoneyRemark__c='';//商务备注
        //re.relatePerson__c='';//经办人
        //re.operatorTime__c=date.today();//经办时间
        re.FinanceVerificated__c='未确认';//财务确认
        //re.reMoney__c=    re.reProportion__c/100*contract.ContractAmount__c;
        //re.FinanceVerificated__c='未确认';
        //re.InvoiceDate__c=date.today();
        //re.reData__c=date.today();
        //re.reachDate__c = date.today();

        list_BatchAddRecevie.add(new BatchAddRecevie(re));
        //新加一条，重新计算所有的值  
        changeAll();
         IsEidt=true;   
    }
    
   
    public void ChangeProportion(){    
    //按照比例进行修改金额
        for(BatchAddRecevie bar :list_BatchAddRecevie){
            if(bar.recevie.PlanProportion__c!=null)
            bar.recevie.receiveMoney__c=bar.recevie.PlanProportion__c*contract.ContractAmount__c*0.01;
        	//当修改收款比例时，同时更改开票金额---spring
        	bar.recevie.reMoney__c = bar.recevie.receiveMoney__c;
        }
        //更改其他的值  比如汇总 比例 和金额变化
         changeAll();
         //ChangeReMoney();
}

 
    public void ChangeReMoney(){
        for(BatchAddRecevie bar :list_BatchAddRecevie){
            
            if(bar.recevie.receiveMoney__c!=null)
            {
            	   bar.recevie.reMoney__c=bar.recevie.receiveMoney__c;
                    if(contract.ContractAmount__c == 0.00)
                    {
                        bar.recevie.PlanProportion__c=0;
                    }
                    else
                    {
                        bar.recevie.PlanProportion__c=bar.recevie.receiveMoney__c/contract.ContractAmount__c*100;
                        //这里做了一个四舍五入
                        bar.recevie.PlanProportion__c = Math.round(bar.recevie.PlanProportion__c * 100) / 100.0;
                        //为什么加上一个0呢？
                        //如果不加   上面的值经过 math  round、 函数处理后以科学计数法  在页面进行显示  什么 5e+2.。。。这种
                        bar.recevie.PlanProportion__c+=0;
                        
                    }
                    
            }
 
        }
        changeAll();
    
    }
    //改变开票金额的值，自动修改开票金额的比例；这里修改用于显示比例的 字段
       public void ChangeInvoiceProportion(){    
    
        for(BatchAddRecevie bar :list_BatchAddRecevie){
            if(bar.recevie.reMoney__c!=null)
            {
                if(contract.ContractAmount__c != 0.00 && contract.ContractAmount__c != null)
                {
                    bar.invoiceProportion=bar.recevie.reMoney__c*100/contract.ContractAmount__c;
                    bar.invoiceProportion = Math.round(bar.invoiceProportion * 100) / 100.0;
                    bar.invoiceProportion+=0;
                }
                else
                {
                    bar.invoiceProportion=0.00;
                }
            }
        }
         changeAll();
}
	

    //改变所有的汇总值 动态显示成本金额，成交金额和 采购总金额  合同总金额
    public void changeAll()
    {
        TotalPlanProportion=0;
        TotalPlanamount=0;
        TotalReceiveProportion=0;
        TotalReceiveAmount=0;
        if(list_BatchAddRecevie.size() == 0)
        {
            return;
        }
        for(BatchAddRecevie re:list_BatchAddRecevie){
            if(re == null || re.recevie == null)
            {
                return;
            }
            
            if(re.recevie.PlanProportion__c == null)
            {
                re.recevie.PlanProportion__c = 0.00;
            }
            
            if(re.recevie.receiveMoney__c == null)
            {
                re.recevie.receiveMoney__c=0.00;
            }
            
            if(re.invoiceProportion == null)
            {
                re.invoiceProportion=0.00;
            }
            
            if(re.recevie.reMoney__c == null)
            {
                re.recevie.reMoney__c=0.00;
            }
            if(re.recevie.receiveMoney__c != (re.recevie.PlanProportion__c * contract.ContractAmount__c * 0.01))
            {
                re.recevie.receiveMoney__c = (re.recevie.PlanProportion__c * contract.ContractAmount__c * 0.01);
            }
            
            
            
            
            
            TotalPlanProportion = TotalPlanProportion+re.recevie.PlanProportion__c;
            TotalPlanProportion = Math.round(TotalPlanProportion * 100) / 100.0;
                        //为什么加上一个0呢？
                        //如果不加   上面的值经过 math  round、 函数处理后以科学计数法  在页面进行显示  什么 5e+2.。。。这种
            TotalPlanProportion+=0;
            system.debug(TotalPlanProportion);
            
            TotalPlanamount = TotalPlanamount+re.recevie.receiveMoney__c;
            TotalReceiveProportion=TotalReceiveProportion+ re.invoiceProportion;
            TotalReceiveAmount=TotalReceiveAmount+re.recevie.reMoney__c;
            }
            system.debug(list_BatchAddRecevie);
    }
    //保存
    public pageReference SaveReDetail(){
   
        list<reDetail__c> listInsert_recevie = new list<reDetail__c>();
        list<reDetail__c> listUpdate_recevie = new list<reDetail__c>();
        changeAll();
        
        if(listReDetail_delete.size()>0)
        {
            try
            {
                //执行删除
                delete listReDetail_delete;
                listReDetail_delete.clear();
            } 
            catch(Exception e)
            {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
                ApexPages.addMessage(msg);
            }
            
        }
        system.debug(list_BatchAddRecevie);
        //此处进行检查，如果发现某必填项没有写，则不刷新页面，不进行保存，停留在编辑页面，等待修改
        //如果用户强制刷新页面，则此时用户在table里的值是不进行保存的
        for(BatchAddRecevie bar:list_BatchAddRecevie){
        	bar.recevie.reachDate__c = bar.recevie.PlanDate__c;
            if(bar.recevie.PlanProportion__c == null  )
            {
                saveIsOk=0;
                IsEidt=true;
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'收款比例不能空');
                ApexPages.addMessage(msg);
                return null ;
            }
            if(bar.recevie.receiveMoney__c == null )
            {
                saveIsOk=0;
                IsEidt=true;
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'收款金额不能空');
                ApexPages.addMessage(msg);
                return null;
            }
            if(bar.recevie.reMoney__c == null )
            {
                saveIsOk=0;
                IsEidt=true;
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'收款比例不能为0或空');
                ApexPages.addMessage(msg);
                return null ;
            }
            //当收款金额大于合同金额时，报错
            if(TotalPlanamount>contract.ContractAmount__c)
            {
                saveIsOk=0;
                IsEidt=true;
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'收款金额总和不能超过合同金额');
                ApexPages.addMessage(msg);
                return null ;
            }

            //未确定，当收款被财务确认了还是否能够修改
            if(TotalPlanProportion > 101 )
            {
                saveIsOk=0;
                IsEidt=true;
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'收款比例不能超过100');
                ApexPages.addMessage(msg);
                return null ;
            }
            
             if(bar.recevie.receiveMoney__c<bar.recevie.reMoney__c)
            {
                
                IsEidt=true;
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'开票金额必须等于收款金额');
                ApexPages.addMessage(msg);
                return null ;
            }
            
                 
            if(bar.recevie.id!=null){
                
                listUpdate_recevie.add(bar.recevie);
            }else{
                listInsert_recevie.add(bar.recevie);
            }
              
        }
        try{
            //system.debug(listUpdate_recevie);
            insert listInsert_recevie;
            update listUpdate_recevie;  
            
            saveIsOk=1;
            IsEidt=false;
            listInsert_recevie.clear();
            listUpdate_recevie.clear();
            return null;  
        }catch(exception e){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            ApexPages.addMessage(msg);
            saveIsOk=2;
            IsEidt=true;
            //return null;
        }
        //this.init();
        
        return null;
    }
    
    //删除
    public void DeleteRecevie(){
    
        list<reDetail__c> listDelete_recevie = new list<reDetail__c>();
        if(list_BatchAddRecevie.size() == 0)
        {
            return;
        }
        for(Integer i = list_BatchAddRecevie.size()-1;i>=0;i--)
        {
            if(list_BatchAddRecevie[i] == null)
            {
                continue;
            }
            if(list_BatchAddRecevie[i].IsDelete)
            {
                if(list_BatchAddRecevie[i].recevie.id!=null)
                {
                    listDelete_recevie.add(list_BatchAddRecevie[i].recevie);
                }
                list_BatchAddRecevie.remove(i);                
            }
        }
        try{
            delete listDelete_recevie;
            listDelete_recevie.clear();
            
        }catch(exception e){
            Apexpages.Message mag=new Apexpages.Message(ApexPages.Severity.FATAL,'删除失败！');
            Apexpages.addMessage(mag);
        }
          changeAll();
    }
    
    //取消
    public void Cancel(){
        this.init();
        IsEidt=false;  
    }
   
 //切换编辑状态
  
  public void EditChange()
  {
    
    IsEidt=true;    

  }
    

}