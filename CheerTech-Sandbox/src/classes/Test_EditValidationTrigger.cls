/**
 * Author: Steven
 * Date: 2014-11-11
 * Description Test EditValidationXXX.trigger and DeleteValidationXXX.trigger
 */
@isTest
private class Test_EditValidationTrigger {
	static testmethod void myUnitTest(){
		
		project__c pc = new project__c();
		pc.projectStatus__c = '新建项目';
		insert pc;
		
		advance__c ac = new advance__c();
		ac.projectRecordNum__c = pc.Id;
		insert ac;
		
		Assignment__c asw = new Assignment__c();
		asw.AssignmentProject__c = pc.Id;
		insert asw;
		
		Account Acc=new Account();
        Acc.Name='4rfgfgfds1';
        Acc.status_del__c='启用';
        insert Acc;
		
		contract__c cc = new contract__c();
		cc.relateProject__c = pc.Id;
		cc.contractSummary__c = 'test';
		cc.ContractNum__c = 'test';
		cc.relateAccount__c = acc.Id;
		insert cc;
		
		productClassify__c pcc = new productClassify__c();
		insert pcc;
		
		product__c prc = new product__c();
		prc.productClassify__c = pcc.Id;
		insert prc;
		
		contractProduct__c cp = new contractProduct__c();
		cp.quantity__c = 4;
		cp.contractProductName__c = prc.Id;
		cp.relateContract__c = cc.Id;
		cp.relateProject__c = pc.Id;
		insert cp;
		
		productArrival__c pa = new productArrival__c();
		pa.relateContract__c = cc.Id;
		pa.quantity__c = 3;
		pa.product__c = cp.Id;
		pa.relateProject_c__c = pc.Id;
		insert pa;
		
		/*
		department__c dp = new department__c();
		insert dp;
		
		HRManage__c hrm = new HRManage__c();
		hrm.department__c = dp.Id;
		insert hrm;
        */
        
        humanRresources__c hum=new humanRresources__c();
        hum.Name='a';
        hum.HRPrice__c=5000;
        hum.status__c='启用';
        insert hum;
        
        department__c dep=new department__c();
        dep.Name='软件部';
        insert dep;
        
        HRManage__c hr=new HRManage__c();
        hr.Name='a';
        hr.department__c=dep.Id;
        hr.HR__c=hum.Id;
        hr.status__c='启用';
        insert hr;
		
		Staff_contribution__c sc = new Staff_contribution__c();
		sc.ProjectContract__c = cc.Id;
		sc.HumanResourcesLibrary__c = hr.Id;
		sc.HRL_project__c = pc.Id;
		sc.day_count__c = 3;
		insert sc;
		
		cc.contractStatus__c = '终止';
		update cc;
		
		try{
		update pa;
		} catch(Exception e){
			
		}
		
		try{
		update sc;
		} catch (Exception e){
			
		}
		
		pc.projectStatus__c = '失败';
		update pc;
		
		try{
		update asw;
		}catch(Exception e){
			
		}
		
		try{
		update cc;
		}catch(Exception e){
			
		}
		
		try{
		delete pc;
		} catch(Exception e){
			
		}
		
		try{
		delete hum;
		} catch(Exception e){
			
		}
		
		try{
		delete dep;
		} catch(Exception e){
			
		}
		
		try{
		delete asw;
		} catch(Exception e){
			
		}
		
		try{
			delete cc;
		} catch(Exception e){
			
		}
		
		project__c pc1 = new project__c();
		pc1.projectStatus__c = '新建项目';
		insert pc1;
		
		try{
		delete pc1;
		} catch(Exception e){
			
		}
		
	}
}