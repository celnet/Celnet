/*************************************************
class:      ProductDetailBulkEditController
Description:   添加产品明细页面的控制类 
Table Accessed: 
Table Updated:  
Author：Snowz.zhu
CreateDate：   2014-06-30
*************************************************/

public with sharing class ProductDetailBulkEditController
{
  public list<ContractProduct> listContractProductDetail {get;set;}
  //ProductDetail class 集合
  public list<contractProduct__c> listProductDetail {get;set;}
  //product list
  public list<product__c> addedProductList{get;set;}
  public list<contractProduct__c> listcontractProduct_delete {get;set;}
  public Integer NumberIsZero{get;set;}
  //产品ID
  public string ProductId {get;set;}
  //产品明细ID
  public string contractId {get;set;}
  //费用类型ID集合
  //public String costTypeIds {get;set;}
  //页面编辑状态
  public Boolean IsEidt {get;set;}
  public boolean isDelete {get;set;}
  //保存的时候进行检查，如果一些必填项没有填则不保存，报错，否则保存，并刷新父页面
  //本控制器并没有检查，添加功能，如果以后客户要求，可以直接添加
  //现在考虑到填写的时候可能 并不确定一些字段，如数量，可能天填写者不太确定，所以可以输入0
  public Integer saveIsOk{set;get;}
   public Integer deleteIsOk{set;get;}
    //采购总金额
  public Decimal costTotalMoney{set;get;}
  public  Decimal  totalAmount{set;get;}
  public contract__c contractproduct{get;set;}
public Profile pfile{get;set;}//用户的profile
  
  public ProductDetailBulkEditController(ApexPages.StandardController controller)
  {
  	listContractProductDetail=new list<ContractProduct>();
  	listcontractProduct_delete=new list<contractProduct__c>();
    IsEidt = false;
    ProductId='';
  	costTotalMoney = 0;
  	saveIsOk=5;
  	deleteIsOk=5;
  	NumberIsZero=0;
    //点击按钮获取到主关系event__c的ID
    //system.debug('费用类型ID集合' + controller.getSubject());
    contractproduct = (contract__c)controller.getRecord();
    this.contractId = contractproduct.Id;
    contractproduct=[Select c.relateProject__c, c.relatePerson__c, c.CreatedById,c.relateAccount__c, c.contractSummary__c,
	 					 c.contractStatus__c, c.contractServiceMon__c, c.contractDetail__c, c.contractDate__c, 
	 					 c.contractAmountRev__c, c.contractAmountPay__c, c.contractAgent__c, c.SystemModstamp, 
	 					  c.RecordTypeId, c.PurchaseAmount__c, c.Name, c.IsDeleted, c.Id, 
	 					 c.ContractNum__c, c.ContractAmount__c From contract__c c where id=:this.contractId];
    totalAmount = contractproduct.ContractAmount__c;    
    pfile = [SELECT Id, CreatedById, CreatedDate, Description, LastModifiedById, LastModifiedDate, Name, UserLicenseId, UserType FROM Profile where id=:UserInfo.getProfileId()];
    Initialize();
    
  }
   
 
  
  public void Initialize()
  {  
  	ContractProduct cp;
    listContractProductDetail=new list<ContractProduct>();
    listProductDetail = new list<contractProduct__c>();
    //通过外键ID（relateEvent__c）查询每个CostDetail__c对象，
    for(contractProduct__c cd : [Select c.Id, c.unit__c, c.relateProject__c,c.relateContract__c,c.quantity__c, ApprovalStatus__c, c.productDetail__c, c.contractProductName__c, c.contractProductName__r.name, c.amount__c, c.Name, c.IsDeleted, c.DealUnitPrice__c, c.CostUnitPrice__c From contractProduct__c c where relateContract__c=:contractId])
    {
      listProductDetail.add(cd);
      cp=new ContractProduct();
      cp.ProName=cd.contractProductName__r.name;
      cp.cd=cd;
      cp.perTotalMoney = cd.amount__c;
      cp.pName=cd.contractProductName__c;
      if(cd.quantity__c == null)
      {
      	cd.quantity__c=0;
      }
      if(cd.CostUnitPrice__c == null)
      {
      	cd.CostUnitPrice__c=0;
      }
      costTotalMoney+=cd.CostUnitPrice__c *  cd.quantity__c;
      cp.PurchaseTotalAmount=cd.CostUnitPrice__c *  cd.quantity__c;
      listContractProductDetail.add(cp);
      
    } 
  }
  
  
  //自动汇总合同金额
  public void AutoSumContractAmount(){
  	   totalAmount = 0;
  	   costTotalMoney = 0;
	for(Integer i = 0;i < listContractProductDetail.size();i++)
    {
    	//如果为空就赋值0
    	if(null == listContractProductDetail[i].cd.quantity__c )
    	{
    		listContractProductDetail[i].cd.quantity__c =0;
    	}
    	if(null == listContractProductDetail[i].cd.DealUnitPrice__c )
    	{
    		listContractProductDetail[i].cd.DealUnitPrice__c =0;
    	}
    	
	      if(listContractProductDetail[i].cd.CostUnitPrice__c == null)
	      {
	      	listContractProductDetail[i].cd.CostUnitPrice__c=0;
	      }
      costTotalMoney = costTotalMoney + listContractProductDetail[i].cd.quantity__c*listContractProductDetail[i].cd.CostUnitPrice__c;
      totalAmount = totalAmount + listContractProductDetail[i].cd.quantity__c * listContractProductDetail[i].cd.DealUnitPrice__c;
      listContractProductDetail[i].perTotalMoney = listContractProductDetail[i].cd.DealUnitPrice__c * listContractProductDetail[i].cd.quantity__c;   
      listContractProductDetail[i].PurchaseTotalAmount=listContractProductDetail[i].cd.CostUnitPrice__c *  listContractProductDetail[i].cd.quantity__c;
    }
	
}
  public class ContractProduct{
    public boolean isSelected{get;set;}
    public String ProName{get;set;}
    //合同总金额，这里方便显示
    public Decimal perTotalMoney{set;get;}
  //产品名字
  	public string pName{set;get;}
  	//每种产品的成本总金额  = 数量乘以成本单价
  //采购总金额就是所有产品 的成本金额加起来
  public Decimal PurchaseTotalAmount{get;set;}
    //public String unit{get;set;}
    public contractProduct__c cd{get;set;}
    public ContractProduct(){
      cd=new contractProduct__c();
      isSelected = false;    
    }
  }
  
  public void SubmitApproval()
  {
  	list<Id> SubmitIdList=new list<Id>();
  	for(ContractProduct CP:listContractProductDetail)
  	{
  		if(CP.isSelected==true&&(CP.cd.ApprovalStatus__c==null||CP.cd.ApprovalStatus__c=='待提交'||CP.cd.ApprovalStatus__c=='未通过'))
  		{
  			SubmitIdList.Add(CP.cd.Id);
  		}
  	}
  	try{
  		if(SubmitIdList.size()!=0)
  		{
  			system.Submit(SubmitIdList,'','');
  		}
  	}
  	catch(Exception e)
  	{
  		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'您无法提交正在审批的项目，如有疑问请联系管理员。');
		ApexPages.addMessage(msg);
  	}
  }
  
  
  public void addToDeleteList()
	{
		for(Integer i = 0;i < listContractProductDetail.size();i++)
		{
			if(listContractProductDetail[i].isSelected)
			{
				if(listContractProductDetail[i].cd.Id != null)
				{
					listcontractProduct_delete.add(listContractProductDetail[i].cd);
				}
				
				listContractProductDetail.remove(i);
				i--;
			}
			
		}
		AutoSumContractAmount();
	}
 
  public void EditChange()
  {
  	
    if(IsEidt)
    {
      IsEidt=false;
    }else{
      IsEidt=true;    
    }
   
    
  }
   
  public void cancelAddProduct()
  {
  	listContractProductDetail.clear();
  	 ContractProduct cp;
  	for(contractProduct__c cd : [Select c.Id, c.unit__c, c.relateProject__c,c.relateContract__c,c.quantity__c, c.productDetail__c, c.contractProductName__c, c.contractProductName__r.name, c.amount__c, c.Name, c.IsDeleted, ApprovalStatus__c , c.DealUnitPrice__c, c.CostUnitPrice__c From contractProduct__c c where relateContract__c=:contractId])
    {
      cp=new ContractProduct();
      cp.ProName=cd.contractProductName__r.name;
      cp.cd=cd;
      cp.perTotalMoney = cd.amount__c;
       cp.pName=cd.contractProductName__c;
      listContractProductDetail.add(cp);
    } 
 IsEidt = false;
  }
  
  
   
  public Pagereference SaveProductDetail()
  {
  	//客户要求在编辑状态下删除时，保存的时候才真正删除
  	//这里真正删除那些用户在编辑状态下删除的
    list<contractProduct__c> listSaveProductDetail = new list<contractProduct__c>();
    if(listcontractProduct_delete.size()>0)
		{
			try
			{
				//执行删除
			delete listcontractProduct_delete;
			listcontractProduct_delete.clear();
			} 
			catch(Exception e)
			{
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
				ApexPages.addMessage(msg);
			}
		}

    //执行保存
    for(Integer i = 0;i < listContractProductDetail.size();i++)
    {
    	if(listContractProductDetail[i].cd.CostUnitPrice__c == null)
    	{
    		listContractProductDetail[i].cd.CostUnitPrice__c = 0;
    	}
    	if(listContractProductDetail[i].cd.DealUnitPrice__c == null)
    	{
    		listContractProductDetail[i].cd.DealUnitPrice__c = 0;
    	}
    	
   	  if(listContractProductDetail[i].cd.CostUnitPrice__c > listContractProductDetail[i].cd.DealUnitPrice__c)
   	  {
   	  	saveIsOk = 2;
   	  	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'成本单价不能大于成交单价');
		ApexPages.addMessage(msg);
		return null;
   	  }
      listSaveProductDetail.add(listContractProductDetail[i].cd);    
    }
    if(listSaveProductDetail.size() > 0)
    {
    try{
    	
      upsert listSaveProductDetail;
      saveIsOk = 1;
     
      }catch(exception e){
      	saveIsOk = 2;
         ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
        ApexPages.addMessage(msg);
      }
      IsEidt=false;
    }
   
    return null;
  }
  
  //  删除行，如果发现一条产品明细（contractProduct__c） 不能删除，请检查是否已经关联到产品交付了
   // 已经关联产品交付的产品明细是不能被删除的
  
  public void DeleteProductDetail()
  {  
    //设置一个删除集合
    
    list<contractProduct__c> listDeleteProductDetail = new list<contractProduct__c>();
    //对listCostDetail进行循环，找出需要删除的CostDetail__c，放入删除集合中
    for(Integer i = listContractProductDetail.size()-1;i>=0;i--)
    {
 
      if(listContractProductDetail[i].isSelected)
      {  
        if(listContractProductDetail[i].cd.Id != null)
        {
          listDeleteProductDetail.add(listContractProductDetail[i].cd);
        }
        
        listContractProductDetail.remove(i);
      }
    }
    if(listDeleteProductDetail.size()>0)
    {
      //执行删除
      try{
       delete listDeleteProductDetail;
       listDeleteProductDetail.clear();
       deleteIsOk = 1;
      }catch(exception e){ 
      	deleteIsOk = 2;
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
        ApexPages.addMessage(msg);}
     
    }
    
    
  }

  
  
  //  根据选择的费用类型ID集合自动创建行

  public void contractAddProductDetail()
  {
  
    if(ProductId.indexOf(',')==-1)
    {
      return;
    }
    
    list<String> str_ProductId = ProductId.split(',');
  //  list<String> needCreateNewContractProduct = new list<String>();
  //  needCreateNewContractProduct.add(str_ProductId);

    list<integer> needDelete = new list<integer>();
   
 
    if(listContractProductDetail.size() != 0)
    {
    	for(integer i=0;i<str_ProductId.size();i++)
    	{
    		
    		for(integer j=0;j<listContractProductDetail.size();j++)
    		{
    			
    			if( str_ProductId[i] == listContractProductDetail[j].pName)
    			{
    			
    				needDelete.add(i);
    				break;
    			}
    		}
    	}
    } 
    
    if(needDelete.size() != 0)
    {
    	for(integer i= needDelete.size()-1;i>-1;i--)
    	{
    		str_ProductId.remove(needDelete[i]);
    	}

    }
  
 
    
    for(product__c p : [Select c.Id ,c.Name, c.productCode__c,   c.unit__c,c.decription__c From product__c c  where Id IN : str_ProductId])
    {
      ContractProduct cp=new ContractProduct();
      cp.cd.relateContract__c=contractId;
      cp.cd.relateProject__c=contractproduct.relateProject__c;
      cp.cd.contractProductName__c = p.Id;
      cp.cd.Name=p.productCode__c;
      cp.ProName=p.Name;
      cp.cd.unit__c=p.unit__c;
      cp.cd.productDetail__c=p.decription__c;
      cp.perTotalMoney = 0;
      cp.pName=p.Id;
      listContractProductDetail.add(cp);
    }
    
	ProductId='';
    
    IsEidt = true;
  }

  
}