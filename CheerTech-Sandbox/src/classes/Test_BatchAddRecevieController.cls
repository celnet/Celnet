/*************************************************
class:      Test_BatchAddRecevieController
Description:  批量添加收款详细的控制器测试类
Table Accessed: 
Table Updated:  
Author：Snowz.zhu
CreateDate：   2014-07-14
*************************************************/
@isTest
private class Test_BatchAddRecevieController {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
         Account Acc=new Account();
        Acc.Name='1';
        Acc.status_del__c='启用';
        insert Acc;
        humanRresources__c hum=new humanRresources__c();
        hum.Name='a';
        hum.HRPrice__c=5000;
        hum.status__c='启用';
        
       
        insert hum;
        department__c dep=new department__c();
        dep.Name='软件部';
        insert dep;
        
        HRManage__c hr=new HRManage__c();
        hr.Name='a';
        hr.department__c=dep.Id;
        hr.HR__c=hum.Id;
         hr.status__c='启用';
        insert hr;
        
        project__c proj=new project__c();
        proj.projectType__c='自助设备';
        proj.projectStatus__c='新建';
        proj.projectStartDate__c=date.today();
        proj.Name='1';
        insert proj;
        
        contract__c cont=new contract__c();
        cont.relateProject__c=proj.Id;
        cont.ContractNum__c='hkjhkj';
        cont.relateAccount__c=Acc.Id;
        cont.contractSummary__c='sfdsfdjsjfds';
        insert cont;
        
           // TO DO: implement unit test
        //新建一个产品分类（主）和产品(子)
        productClassify__c pc=new productClassify__c();
        pc.Name='MNghfsgh';  
        pc.classifyCode__c='fdjslahfdjsa';
        pc.remark__c='sdfsagfgadfagf';
        insert pc;
        
        product__c pd1 = new product__c();
        pd1.Name='pos1';
        pd1.productClassify__c=pc.Id;
        pd1.Status__c='启用';
        product__c pd2 = new product__c();
        pd2.Name='pos2';
        pd2.Status__c='启用';
        pd2.productClassify__c=pc.Id;
        product__c pd3 = new product__c();
        pd3.Name='pos3';
        pd3.Status__c='启用';
        pd3.productClassify__c=pc.Id; 
        
        insert pd1;
        insert pd2;
        insert pd3;
         product__c pd4 = new product__c();
        pd4.Name='pos4';
        pd4.Status__c='启用';
        pd4.productClassify__c=pc.Id;
         product__c pd5 = new product__c();
        pd5.Name='pos4';
        pd5.Status__c='启用';
        pd5.productClassify__c=pc.Id;
        
           //新建一个客户
        Account a = new Account();
        a.Name='a12';
        a.accountCode__c='adfsf';
        a.status_del__c='启用';
        insert a;
        
        //新建一个项目
        project__c p = new project__c();
        p.Name = 'p';
        p.projectType__c='不间断电源';
        p.projectStatus__c = '新建';
        p.projectStartDate__c=Date.today();
        p.relateAccount__c=a.Id;
        insert p;
        
        contract__c c=new contract__c();
       // c.Name='32798';
        c.relateProject__c = p.Id;
        c.relateAccount__c=a.id;
         c.ContractNum__c='hkjhkj';
         c.contractSummary__c='sfdsfdjsjfds';
        insert c;
        
        //新建产品明细
        contractProduct__c cp1 = new contractProduct__c();
        cp1.Name='10taipos1';
        cp1.relateProject__c=p.Id;
        cp1.relateContract__c = c.Id;
        cp1.contractProductName__c=pd1.Id;
        //cp1.amount__c=100;
        cp1.quantity__c=100;
        cp1.DealUnitPrice__c=1000;
        
        contractProduct__c cp2 = new contractProduct__c();
        cp2.Name='10taipos2';
        cp2.relateProject__c=p.Id;
        cp2.relateContract__c = c.Id;
        cp2.contractProductName__c=pd2.Id;
        //cp2.amount__c=100;
        cp2.quantity__c=100;
        cp2.DealUnitPrice__c=1000;
        
        contractProduct__c cp3 = new contractProduct__c();
        cp3.Name='10taipos3';
        cp3.relateProject__c=p.Id;
        cp3.relateContract__c = c.Id;
        cp3.contractProductName__c=pd1.Id;
        //cp3.amount__c=100;
        cp3.quantity__c=100;
        cp3.DealUnitPrice__c=1000;
        
     	insert cp1;
     	insert cp2;
     	insert cp3;
     	contractProduct__c cp4 = new contractProduct__c();
        cp4.Name='10taipos4';
        cp4.relateProject__c=p.Id;
        cp4.relateContract__c = c.Id;
        cp4.contractProductName__c=pd1.Id;
        //cp3.amount__c=100;
        cp4.quantity__c=100;
         contractProduct__c cp5 = new contractProduct__c();
        cp5.Name='10taipos5';
        cp5.relateProject__c=p.Id;
        cp5.relateContract__c = c.Id;
        cp5.contractProductName__c=pd1.Id;
        //cp3.amount__c=100;
        cp5.quantity__c=100;
     	
     	
     	contract__c ct = [select ContractAmount__c from contract__c where id =:c.Id ];
     	
     	reDetail__c re1=new reDetail__c();
		re1.relateProject__c=p.Id;		
		re1.relateContract__c=c.Id;
		re1.PlanProportion__c=10;//收款比例, 计划收款比例
		re1.receiveMoney__c =ct.ContractAmount__c -  10;//收款金额, 计划收款金额
		re1.PlanDate__c = date.today();//计划收款日期
		re1.salesRemark__c = 'grtgw';//销售备注
		re1.outNum__c='gwgtwrg';//出库单编号
		re1.invoice__c='gwgrtgh';//发票编号
		re1.ContractAccount__c=ct.ContractAmount__c;//合同金额
		re1.reMoney__c=10000;//开票金额
		//re1.proportion__c=0;//开票比例
		re1.InvoiceDate__c=date.today();//开票日期
		re1.reData__c=date.today();//收款日期
		re1.reMoneyRemark__c='';//商务备注
		//re.relatePerson__c='';//经办人
		re1.operatorTime__c=date.today();//经办时间
		re1.FinanceVerificated__c='未确认';//财务确认
		//re1.reMoney__c=	100;
		//re.FinanceVerificated__c='未确认';
		re1.InvoiceDate__c=date.today();
		re1.reData__c=date.today();
		insert re1;
		reDetail__c re2=new reDetail__c();
		re2.relateProject__c=p.Id;		
		re2.relateContract__c=c.Id;
			re1.PlanProportion__c=10;//收款比例, 计划收款比例
		re2.receiveMoney__c =ct.ContractAmount__c ;//收款金额, 计划收款金额
		re2.PlanDate__c = date.today();//计划收款日期
		re2.salesRemark__c = 'uyku';//销售备注
		re2.outNum__c='kuk';//出库单编号
		re2.invoice__c='ukku';//发票编号
		re2.ContractAccount__c=ct.ContractAmount__c;//合同金额
		re2.reMoney__c=12220;//开票金额
		//re2.proportion__c=0;//开票比例
		re2.InvoiceDate__c=date.today();//开票日期
		re1.reData__c=date.today();//收款日期
		re2.reMoneyRemark__c='';//商务备注
		//re.relatePerson__c='';//经办人
		re1.operatorTime__c=date.today();//经办时间
		re2.FinanceVerificated__c='未确认';//财务确认
		//re2.reMoney__c=	re.reProportion__c/100*contract.ContractAmount__c;
		//re.FinanceVerificated__c='未确认';
		re2.InvoiceDate__c=date.today();
		re2.reData__c=date.today();
		insert re2;
		
        ApexPages.StandardController controller1 = new ApexPages.StandardController(c);
        ProductDetailBulkEditController pdbec = new ProductDetailBulkEditController(controller1);
        
        //新建内部类，ContractProduct 包装了一下 ContractProduct__c
        ProductDetailBulkEditController.ContractProduct pcp1 = new ProductDetailBulkEditController.ContractProduct();
        pcp1.isSelected = true;
        pcp1.cd = cp1;
        
        ProductDetailBulkEditController.ContractProduct pcp2 = new ProductDetailBulkEditController.ContractProduct();
        pcp2.isSelected = true;
        pcp2.cd = cp2;
        
        ProductDetailBulkEditController.ContractProduct pcp3 = new ProductDetailBulkEditController.ContractProduct();
        pcp3.isSelected = false;
        pcp3.cd = cp3;
        
        pdbec.listContractProductDetail.add(pcp1);
        pdbec.listContractProductDetail.add(pcp2);
        pdbec.listContractProductDetail.add(pcp3);
        
        pdbec.DeleteProductDetail();
         pdbec.listContractProductDetail.clear();
         pdbec.listContractProductDetail.add(pcp1);
        pdbec.listContractProductDetail.add(pcp2);
        pdbec.listContractProductDetail.add(pcp3);
        pdbec.addToDeleteList();
        
        pdbec.SaveProductDetail();
        pdbec.cancelAddProduct();
        pdbec.ProductId = pd4+','+pd5;
        pdbec.contractAddProductDetail();
        pdbec.AutoSumContractAmount();
        
        pdbec.IsEidt = true;
        pdbec.EditChange();
        pdbec.IsEidt = false;
        pdbec.EditChange();
        
        ApexPages.StandardController controller2 = new ApexPages.StandardController(c);
        BatchAddRecevieController barc = new BatchAddRecevieController(controller2);
        
        list <reDetail__c> re = [Select r.FinanceVerificated__c, r.salesRemark__c, r.relateProject__c, r.relatePerson__c,
		r.relatePerson__r.FirstName,r.relatePerson__r.LastName,
		 r.relateContract__c, r.receiveMoney__c, r.reMoney__c, r.reMoneyRemark__c,
		  r.reData__c, r.proportion__c, r.outNum__c, r.operatorTime__c, r.invoice__c,
		   r.ReceivedAmount__c, r.PlanProportion__c, r.PlanDate__c, r.Name,
		    r.InvoiceDate__c, r.Id, r.ContractAccount__c From reDetail__c r
		     where r.relateContract__c=:c.Id];
        BatchAddRecevieController.BatchAddRecevie bb1=new  BatchAddRecevieController.BatchAddRecevie(re[0]);
        BatchAddRecevieController.BatchAddRecevie bb2=new  BatchAddRecevieController.BatchAddRecevie(re[1]);
        
       
        
        bb1.IsDelete = true;
        bb2.IsDelete = false;
        //bb1.recevie = re[0];
        //bb2.recevie = re[1];
        //bb1.recevie = re2;
        
        barc.list_BatchAddRecevie.add(bb1);
        barc.list_BatchAddRecevie.add(bb2);
        barc.EditChange();
        barc.AddRecevie();
        barc.ChangeReMoney();
        barc.ChangeInvoiceProportion();
        
        barc.ChangeProportion();
       // barc.changeAll();
       
       
        barc.SaveReDetail();
        
        re[0].PlanProportion__c=null;
        bb1.recevie = re[0];
        barc.list_BatchAddRecevie.add(bb1);
         barc.SaveReDetail();
        re[0].receiveMoney__c=null;
         bb1.recevie = re[0];
        barc.list_BatchAddRecevie.add(bb1);
        barc.addToDeleteList();
         barc.SaveReDetail();
         re[0].reMoney__c=null;
          barc.SaveReDetail();
      
         barc.TotalPlanProportion=120;
        barc.list_BatchAddRecevie.add(bb1);
          barc.SaveReDetail();
        
        barc.DeleteRecevie();
        
        
        barc.Cancel();
        
    }
}