/*
Author:Ward
Time:2014-7-2
Function:EventAutoSumToProject测试类
*/

@isTest
private class Test_EventAutoSumToProject {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Account Acc=new Account();
        Acc.Name='1e3d';
        Acc.status_del__c='启用';
        insert Acc;
        
        humanRresources__c hum=new humanRresources__c();
        hum.Name='a';
        insert hum;
        department__c dep=new department__c();
        dep.Name='软件部';
        insert dep;
        
        HRManage__c hr=new HRManage__c();
        hr.Name='a';
        hr.department__c=dep.Id;
        hr.HR__c=hum.Id;
        insert hr;
        
        project__c proj=new project__c();
        proj.projectType__c='自助设备';
        proj.projectStatus__c='新建';
        proj.projectStartDate__c=date.today();
        proj.Name='1';
        insert proj;
        
        event__c even =new event__c();
        even.Name='a';
        even.eventActionPerson__c=hr.Id;
        even.project__c=proj.id;
        even.eventDate__c=date.today();
        even.eventCostApprove__c = '已审批';
        insert even;
        update even;
        
    }
}