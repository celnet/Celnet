/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_Staff_contribLockAfterContractLock {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        //合同锁定后不能修改下面的项目人天 
          // TO DO: implement unit test
        Account Acc=new Account();
        Acc.Name='4rfgfds1';
        Acc.status_del__c='启用';
        insert Acc;
        
        humanRresources__c hum=new humanRresources__c();
        hum.Name='a';
        hum.HRPrice__c=5000;
        hum.status__c='启用';
        
       
        insert hum;
        department__c dep=new department__c();
        dep.Name='软件部';
        insert dep;
        
        HRManage__c hr=new HRManage__c();
        hr.Name='a';
        hr.department__c=dep.Id;
        hr.HR__c=hum.Id;
         hr.status__c='启用';
        insert hr;
        
        project__c proj=new project__c();
        proj.projectType__c='自助设备';
        proj.projectStatus__c='新建';
        proj.projectStartDate__c=date.today();
        proj.Name='1';
        insert proj;
        
        contract__c cont=new contract__c();
        cont.relateProject__c=proj.Id;
        cont.ContractNum__c='hkjhkj';
        cont.relateAccount__c=Acc.Id;
        cont.contractSummary__c='dfsafda';
        insert cont;
        
        Staff_contribution__c stf = new Staff_contribution__c();
        stf.HRL_project__c=proj.Id;
        stf.ProjectContract__c=cont.Id;
        stf.ManDayDate__c=date.today();
        stf.day_count__c=10;
        stf.HumanResourcesLibrary__c = hr.Id;
        insert stf;
        
        cont.contractStatus__c='失败';
        update cont;
        
        try{ 
        	update stf;
        delete stf;
        Staff_contribution__c stf2 = new Staff_contribution__c();
        stf2.HRL_project__c=proj.Id;
        stf2.ProjectContract__c=cont.Id;
        stf2.ManDayDate__c=date.today();
        stf2.day_count__c=10;
        stf2.HumanResourcesLibrary__c = hr.Id;
        
        
        insert stf2;
        }
        catch(exception e){}
       
        try{ 
        	
        delete stf;
        Staff_contribution__c stf2 = new Staff_contribution__c();
        stf2.HRL_project__c=proj.Id;
        stf2.ProjectContract__c=cont.Id;
        stf2.ManDayDate__c=date.today();
        stf2.day_count__c=10;
        stf2.HumanResourcesLibrary__c = hr.Id;
        
        
        insert stf2;
        }
        catch(exception e){}
        try{ 
        	
        Staff_contribution__c stf2 = new Staff_contribution__c();
        stf2.HRL_project__c=proj.Id;
        stf2.ProjectContract__c=cont.Id;
        stf2.ManDayDate__c=date.today();
        stf2.day_count__c=10;
        stf2.HumanResourcesLibrary__c = hr.Id;
        
        
        insert stf2;
        }
        catch(exception e){}
        
        
    }
}