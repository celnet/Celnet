/**
 * Author: Steven
 * Date: 2014-11-11
 * Description: Test ProjectProductDel.trigger
 */
@isTest
private class Test_ProjectProductDel {
	static testmethod void myUnitTest(){
		project__c pc = new project__c();
		pc.projectStatus__c = '新建项目';
		insert pc;
		
		ProjectProduct__c ppc = new ProjectProduct__c();
		ppc.ProjectProduct__c = '软件';
		ppc.ProductProject__c = pc.Id;
		insert ppc;
		
		ProductContribute__c pcc = new ProductContribute__c();
		pcc.ProjectProduct__c = ppc.Id;
		insert pcc;
		
		delete ppc;
		
	}
}
