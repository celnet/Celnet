/*
Author:Ward
Time:2014-7-2
Function:SumManCostAmount   autoSumcontractManContributon 测试类 一个就通过两个
此人天贡献插入引起 trigger autoSumcontractManContributon触发，所以一箭双雕

*/
@isTest
private class Test_SumManCostAmount {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Account Acc=new Account();
        Acc.Name='4rgfds1';
        Acc.status_del__c='启用';
        insert Acc;
        
        humanRresources__c hum=new humanRresources__c();
        hum.Name='a';
        hum.HRPrice__c=5000;
        hum.status__c='启用';
        
       
        insert hum;
        department__c dep=new department__c();
        dep.Name='软件部';
        insert dep;
        
        HRManage__c hr=new HRManage__c();
        hr.Name='a';
        hr.department__c=dep.Id;
        hr.HR__c=hum.Id;
         hr.status__c='启用';
        insert hr;
        
        project__c proj=new project__c();
        proj.projectType__c='自助设备';
        proj.projectStatus__c='新建';
        proj.projectStartDate__c=date.today();
        proj.Name='1';
        insert proj;
        
        contract__c cont=new contract__c();
        cont.relateProject__c=proj.Id;
        cont.ContractNum__c='hkjhkj';
        cont.relateAccount__c=Acc.Id;
        cont.contractSummary__c='dfsafda';
        insert cont;
        
        Staff_contribution__c stf = new Staff_contribution__c();
        stf.HRL_project__c=proj.Id;
        stf.ProjectContract__c=cont.Id;
        stf.ManDayDate__c=date.today();
        stf.day_count__c=10;
        stf.HumanResourcesLibrary__c = hr.Id;
        insert stf;
    }
}