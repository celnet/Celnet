/**
 * Author: Steven
 * Date: 2014-11-11
 * Description: Test EventManagerCannotDeleteEditOthers.trigger
 */
@isTest
private class Test_EventManagerCannotDeleteEditOthers {
	static testmethod void myUnitTest(){
		User testUser = [Select Id, ProfileId From User Where IsActive = true And Profile.Name != '系统管理员' And Department = '软件部' And UserType = 'Standard' limit 1];
		
		System.runAs(testUser){
			event__c e = new event__c();
			e.Name = 'ts';
			insert e;
			
			e.Name = 'tt';
			update e;
			
			delete e;
			
		}
	}
}