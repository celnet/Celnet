@RestResource(urlMapping='/Weixin/*')
global class WeixinRestApiClass{
    global class Message
    {
        public string ToUserName;
        public string FromUserName;
        public string CreateTime;
        public string Content;
        public string MsgId;
        public string MsgType;
        public string Event;
        public string EventKey;
        public string Title;
        public string Description;
        public string PicUrl;
        public string Url;
    }
    @HttpGet
    global static long getWidgets() {
        long echostr = long.valueOf(RestContext.request.params.get('echostr'));
        string signature = RestContext.request.params.get('signature');
        string timestamp = RestContext.request.params.get('timestamp');
        string nonce = RestContext.request.params.get('nonce');
        return echostr;
    } 
    @HttpPost 
    global static void createNewWidget() {
        RestRequest req = RestContext.request;
        system.debug('RestRequest' + req);
        XmlStreamReader xsr = new XmlStreamReader(req.requestBody.toString());
        Message rm = getReceiveMessage(xsr);
        //RegisterUser__c user = upsertWeiXinUser(rm);      
        
        if(rm.MsgType =='text')
        {
            String content = rm.Content;    
            rm.Content = '谢谢您的宝贵建议，我们会竭诚为您服务！';
            rm = sendMessage(rm);
        }
        else if(rm.MsgType == 'event')
        {
            if(rm.Event =='subscribe')
            {
                rm.Content = SubscribeEventMessage();  
                rm = sendMessage(rm);
            }
            else if(rm.Event =='CLICK')
            {
                if(rm.EventKey=='I_WANT_BUY')
                {
                    String content = rm.Content;    
                    rm.Content = '谢谢您的宝贵建议，我们会竭诚为您服务！';
                    rm = sendMessage(rm);
                    /*rm = BuyEventMessage(rm); 
                    RestResponse res = RestContext.response;
                    res.addHeader('Content-Type','text/xml');
                    if(rm.PicUrl != null)
                    {
                        res.responseBody = Blob.valueOf(getReturnPicMessage(rm));
                    }
                    else
                    {
                        res.responseBody = Blob.valueOf(getReturnNoPicMessage(rm));
                    }
                    //system.debug('I_WANT_BUY'+getReturnNoPicMessage(rm));
                    return;*/
                }
                else if(rm.EventKey=='I_WANT_SELL')
                {
                    //rm = SellEventMessage(rm); 
                    RestResponse res = RestContext.response;
                    res.addHeader('Content-Type','text/xml');
                    if(rm.PicUrl != null)
                    {
                        res.responseBody = Blob.valueOf(getReturnPicMessage(rm));
                    }
                    else
                    {
                        res.responseBody = Blob.valueOf(getReturnNoPicMessage(rm));
                    }
                    return;
                }
                else if(rm.EventKey=='I_WANT_BACKDOOR')
                {
                    //rm = BackDoorEventMessage(rm); 
                    RestResponse res = RestContext.response;
                    res.addHeader('Content-Type','text/xml');
                    if(rm.PicUrl != null)
                    {
                        res.responseBody = Blob.valueOf(getReturnPicMessage(rm));
                    }
                    else
                    {
                        res.responseBody = Blob.valueOf(getReturnNoPicMessage(rm));
                    }
                    return;
                }
                else if(rm.EventKey=='I_WANT_FINANCING')
                {
                    //rm = FinancingEeventMessage(rm); 
                    RestResponse res = RestContext.response;
                    res.addHeader('Content-Type','text/xml');
                    if(rm.PicUrl != null)
                    {
                        res.responseBody = Blob.valueOf(getReturnPicMessage(rm));
                    }
                    else
                    {
                        res.responseBody = Blob.valueOf(getReturnNoPicMessage(rm));
                    }
                    return;
                }
                else if(rm.EventKey=='NEW_SALON')
                {
                    //rm=SalonEventMessage(rm);
                    RestResponse res = RestContext.response;
                    res.addHeader('Content-Type','text/xml');
                    if(rm.Url != null)
                    {
                        res.responseBody = Blob.valueOf(getReturnPicMessage(rm));
                    }
                    else
                    {
                        res.responseBody = Blob.valueOf(getReturnNoUMessage(rm));
                    }
                    return;
                }
                else if(rm.EventKey=='NEW_LESSON')
                {
                    //rm=LessonEventMessage(rm);
                    RestResponse res = RestContext.response;
                    res.addHeader('Content-Type','text/xml');
                    if(rm.PicUrl != null)
                    {
                        res.responseBody = Blob.valueOf(getReturnPicMessage(rm));
                    }
                    else
                    {
                        res.responseBody = Blob.valueOf(getReturnNoUMessage(rm));
                    }
                    return;
                }
                else if(rm.EventKey=='SIGN_IN')
                {
                    //rm = SignInEventMessage(rm); 
                    RestResponse res = RestContext.response;
                    res.addHeader('Content-Type','text/xml');
                    if(rm.PicUrl != null)
                    {
                        res.responseBody = Blob.valueOf(getReturnPicMessage(rm));
                    }
                    else
                    {
                        res.responseBody = Blob.valueOf(getReturnNoUrlMessage(rm));
                    }
                    
                    return; 
                    //rm = sendMessage(rm);
                }
                else if(rm.EventKey=='MY_PROJECT')
                {
                    //rm.Content = MyProjectEventMessage(user.Id);  
                    rm = sendMessage(rm);
                }
                else if(rm.EventKey=='MY_SALON')
                {
                    //rm.Content = MySalonEventMessage(user.Id);  
                    rm = sendMessage(rm);
                }
                else if(rm.EventKey=='MY_LESSON')
                {
                    //rm.Content = MyLessonEventMessage(user.Id);  
                    rm = sendMessage(rm);
                }
                else if(rm.EventKey=='MY_INFO')
                {
                    //rm.Content = MyInfoEventMessage(user.Id);  
                    rm = sendMessage(rm);
                }
            }
        }
        if(rm.ToUserName!=null)
        {
            RestResponse res = RestContext.response;
            res.addHeader('Connection', 'keep-alive');
            res.addHeader('Content-Type','text/html');
            res.responseBody = Blob.valueOf(getReturnMessage(rm));
            return;
        }
   }

   //关注事件推送
   global static string SubscribeEventMessage()
   {
        string enter = '\n';
        string content = '微信POC实现功能：'+enter
                        +'1.将微信用户和雨花石服务号的文本类消息互动存储在SF系统中。'+enter
                        +'2.当微信用户向雨花石服务号推送消息时，记录用户的OpenId（微信用户的加密微信号）'+enter
                        +'3.当微信用户点击雨花石服务号的菜单项时，弹出业务表单窗口用于收集客户信息。用户填完信息后，系统会自动根据客户信息创建潜在客户、客户、联系人和业务机会。并返回业务单号供用户查询业务信息。'+enter
                        +'4.当用户向雨花石服务号发送业务单号（如：Pro-000064）时，系统会自动通过微信向用户发送该业务的当前状态等信息。';
        return content;
   }
   //发送消息处理
   global static Message sendMessage(Message rm)
   {    
        rm.ToUserName = rm.FromUserName;
        rm.FromUserName = 'CideatechWeiXin';
        return rm;
   }

   //获取HttpResponse
   global static HttpResponse getHttpResponse(string url)
   {
        Http h = new Http();    //创建Http对象
        HttpRequest re = new HttpRequest(); //创建HttpRequest对象
        re.setMethod('GET');        //设置请求的方法
        re.setTimeout(60000);       //设置超时时间 
        re.setEndpoint(url);        //设置请求的url地址
        HttpResponse response = h.send(re);     //发送请求，返回HttpResponse
        return response;               
   }  
   
   //发送被动响应TEXT消息XML构造 
   global static string getReturnMessage(Message rm)
   {
        XmlStreamWriter w = new XmlStreamWriter();
        w.writeStartElement(null,'xml',null);
        w.writeStartElement(null,'ToUserName',null);
        w.writeCData(rm.ToUserName);
        w.writeEndElement();//end ToUserName
        w.writeStartElement(null,'FromUserName',null);
        w.writeCData(rm.FromUserName);
        w.writeEndElement();//end FromUserName
        w.writeStartElement(null,'CreateTime',null);
        w.writeCharacters(rm.CreateTime);
        w.writeEndElement();//end CreateTime
        w.writeStartElement(null,'MsgType',null);
        w.writeCData('text');
        w.writeEndElement();//end MsgType
        w.writeStartElement(null,'Content',null);
        w.writeCData(rm.content);
        w.writeEndElement();//end Content
        //w.writeStartElement(null,'FuncFlag',null);
        //w.writeCharacters('0');
        //w.writeEndElement();//end FuncFlag
        w.writeEndElement(); //end xml
        String xmlOutput = w.getXmlString();
        w.close();
        system.debug('*******XML******'+xmlOutput); 
        return xmlOutput;
        
   }
   //发送被动响应图文消息XML构造 带URL和图片
   global static string getReturnPicMessage(Message rm)
   {
        XmlStreamWriter w = new XmlStreamWriter();
        w.writeStartElement(null,'xml',null);
            w.writeStartElement(null,'ToUserName',null);
                w.writeCData(rm.ToUserName);
            w.writeEndElement();//end ToUserName
            w.writeStartElement(null,'FromUserName',null);
                w.writeCData(rm.FromUserName);
            w.writeEndElement();//end FromUserName
            w.writeStartElement(null,'CreateTime',null);
                w.writeCharacters(rm.CreateTime);
            w.writeEndElement();//end CreateTime
            w.writeStartElement(null,'MsgType',null);
                w.writeCData('news');
            w.writeEndElement();//end MsgType
            w.writeStartElement(null,'ArticleCount',null);
                w.writeCharacters('1');
            w.writeEndElement();//end ArticleCount
            w.writeStartElement(null,'Articles',null);
                w.writeStartElement(null,'item',null);
                    w.writeStartElement(null,'Title',null);
                        w.writeCData(rm.Title);
                    w.writeEndElement();//end Title
                    w.writeStartElement(null,'Description',null);
                        w.writeCData(rm.Description);
                    w.writeEndElement();//end Description
                    w.writeStartElement(null,'PicUrl',null);
                        w.writeCharacters(rm.PicUrl);
                    w.writeEndElement();//end PicUrl
                    w.writeStartElement(null,'Url',null);
                        w.writeCData(rm.Url);
                    w.writeEndElement();//end Url
                w.writeEndElement(); //end item
            w.writeEndElement(); //end Articles
        w.writeEndElement(); //end xml
        String xmlOutput = w.getXmlString();
        w.close();
        return xmlOutput;
   }
   //发送被动响应图文消息XML构造  不带图片和URL
   global static string getReturnNoUrlMessage(Message rm)
   {
        XmlStreamWriter w = new XmlStreamWriter();
        w.writeStartElement(null,'xml',null);
            w.writeStartElement(null,'ToUserName',null);
                w.writeCData(rm.ToUserName);
            w.writeEndElement();//end ToUserName
            w.writeStartElement(null,'FromUserName',null);
                w.writeCData(rm.FromUserName);
            w.writeEndElement();//end FromUserName
            w.writeStartElement(null,'CreateTime',null);
                w.writeCharacters(rm.CreateTime);
            w.writeEndElement();//end CreateTime
            w.writeStartElement(null,'MsgType',null);
                w.writeCData('news');
            w.writeEndElement();//end MsgType
            w.writeStartElement(null,'ArticleCount',null);
                w.writeCharacters('1');
            w.writeEndElement();//end ArticleCount
            w.writeStartElement(null,'Articles',null);
                w.writeStartElement(null,'item',null);
                    w.writeStartElement(null,'Title',null);
                        w.writeCData(rm.Title);
                    w.writeEndElement();//end Title
                    w.writeStartElement(null,'Description',null);
                        w.writeCData(rm.Description);
                    w.writeEndElement();//end Description
                w.writeEndElement(); //end item
            w.writeEndElement(); //end Articles
        w.writeEndElement(); //end xml
        String xmlOutput = w.getXmlString();
        w.close();
        return xmlOutput;
   }
   //发送被动响应图文消息XML构造  不带图片
   global static string getReturnNoPicMessage(Message rm)
   {
        XmlStreamWriter w = new XmlStreamWriter();
        w.writeStartElement(null,'xml',null);
            w.writeStartElement(null,'ToUserName',null);
                w.writeCData(rm.ToUserName);
            w.writeEndElement();//end ToUserName
            w.writeStartElement(null,'FromUserName',null);
                w.writeCData(rm.FromUserName);
            w.writeEndElement();//end FromUserName
            w.writeStartElement(null,'CreateTime',null);
                w.writeCharacters(rm.CreateTime);
            w.writeEndElement();//end CreateTime
            w.writeStartElement(null,'MsgType',null);
                w.writeCData('news');
            w.writeEndElement();//end MsgType
            w.writeStartElement(null,'ArticleCount',null);
                w.writeCharacters('1');
            w.writeEndElement();//end ArticleCount
            w.writeStartElement(null,'Articles',null);
                w.writeStartElement(null,'item',null);
                    w.writeStartElement(null,'Title',null);
                        w.writeCData(rm.Title);
                    w.writeEndElement();//end Title
                    w.writeStartElement(null,'Description',null);
                        w.writeCData(rm.Description);
                    w.writeEndElement();//end Description
                    w.writeStartElement(null,'Url',null);
                        w.writeCData(rm.Url);
                    w.writeEndElement();//end Url
                w.writeEndElement(); //end item
            w.writeEndElement(); //end Articles
        w.writeEndElement(); //end xml
        String xmlOutput = w.getXmlString();
        w.close();
        return xmlOutput;
   }
   //发送被动响应图文消息XML构造  不带Url
   global static string getReturnNoUMessage(Message rm)
   {
        XmlStreamWriter w = new XmlStreamWriter();
        w.writeStartElement(null,'xml',null);
            w.writeStartElement(null,'ToUserName',null);
                w.writeCData(rm.ToUserName);
            w.writeEndElement();//end ToUserName
            w.writeStartElement(null,'FromUserName',null);
                w.writeCData(rm.FromUserName);
            w.writeEndElement();//end FromUserName
            w.writeStartElement(null,'CreateTime',null);
                w.writeCharacters(rm.CreateTime);
            w.writeEndElement();//end CreateTime
            w.writeStartElement(null,'MsgType',null);
                w.writeCData('news');
            w.writeEndElement();//end MsgType
            w.writeStartElement(null,'ArticleCount',null);
                w.writeCharacters('1');
            w.writeEndElement();//end ArticleCount
            w.writeStartElement(null,'Articles',null);
                w.writeStartElement(null,'item',null);
                    w.writeStartElement(null,'Title',null);
                        w.writeCData(rm.Title);
                    w.writeEndElement();//end Title
                    w.writeStartElement(null,'Description',null);
                        w.writeCData(rm.Description);
                    w.writeEndElement();//end Description
                    w.writeStartElement(null,'PicUrl',null);
                        w.writeCharacters(rm.PicUrl);
                    w.writeEndElement();//end PicUrl
                w.writeEndElement(); //end item
            w.writeEndElement(); //end Articles
        w.writeEndElement(); //end xml
        String xmlOutput = w.getXmlString();
        w.close();
        return xmlOutput;
   }
   //接收消息XML解析
   global static Message getReceiveMessage(XmlStreamReader xsr)
   {
        Message rm = new Message();
        while(xsr.hasNext()) {
            if(xsr.getEventType() == XmlTag.START_ELEMENT)
            {
                string localName = xsr.getLocalName();
                xsr.next();
                if(xsr.getEventType() == XmlTag.CHARACTERS)
                {
                    if(localName == 'ToUserName')rm.ToUserName = xsr.getText();
                    if(localName == 'FromUserName')rm.FromUserName = xsr.getText();
                    if(localName == 'CreateTime')rm.CreateTime = xsr.getText();
                    if(localName == 'Content')rm.Content = xsr.getText();
                    if(localName == 'MsgId')rm.MsgId = xsr.getText();
                    if(localName == 'MsgType')rm.MsgType = xsr.getText();
                    if(localName == 'Event')rm.Event = xsr.getText();
                    if(localName == 'EventKey')rm.EventKey = xsr.getText();
                    xsr.next();
                }
            }
            xsr.next();
        }
        return rm;
   }
}