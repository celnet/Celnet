/**
	项目锁定以后 event不能更改
 */
@isTest
private class Test_EventLockAfterProjectLocked {

    static testMethod void myUnitTest() {
        Account Acc=new Account();
        Acc.Name='1ef3d';
        Acc.status_del__c='启用';
        insert Acc;
        
        humanRresources__c hum=new humanRresources__c();
        hum.Name='as';
        insert hum;
        department__c dep=new department__c();
        dep.Name='软件部';
        insert dep;
        
        HRManage__c hr=new HRManage__c();
        hr.Name='add';
        hr.department__c=dep.Id;
        hr.HR__c=hum.Id;
        insert hr;
        
        project__c proj=new project__c();
        proj.projectType__c='自助设备';
        proj.projectStatus__c='新建';
        proj.projectStartDate__c=date.today();
        proj.Name='1';
        insert proj;
        
        event__c even =new event__c();
        even.Name='a';
        even.eventActionPerson__c=hr.Id;
        even.project__c=proj.id;
        even.eventDate__c=date.today();
        even.eventCostApprove__c = '已审批';
        insert even;
        
        proj.projectStatus__c='失败';
        update proj;
        try{	
        	update even;
        }catch(exception e)
        {}
        event__c even2 =new event__c();
        even2.Name='af';
        even2.eventActionPerson__c=hr.Id;
        even2.project__c=proj.id;
        even2.eventDate__c=date.today();
        even2.eventCostApprove__c = '已审批';
        
        try{	
        	insert even2;
        }catch(exception e)
        {}
        try{	
        	delete even;
        }catch(exception e)
        {}
    }
}