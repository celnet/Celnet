/**
 * Author: Steven
 * Date: 2014-11-11
 * Description: Test AssignmentShare.trigger
 */
@isTest
private class Test_AssignmentShare {
	static testmethod void myUnitTest(){
		
		User testUser = [Select Id, ProfileId From User Where IsActive = true And Profile.Name != '系统管理员' And Department = '软件部' And UserType = 'Standard' limit 1];
		
		System.runAs(testUser){
			project__c pc = new project__c();
			pc.projectStatus__c = '新建项目';
			insert pc;
			
			Assignment__c asw = new Assignment__c();
			asw.AssignmentProject__c = pc.Id;
			insert asw;
		}
	}
}