/*************************************************
class:      Test_AdvanceAccountPrintController
Description:   暂支单打印页面控制器测试类 
Table Accessed: 
Table Updated:  
Author：Snowz.zhu
CreateDate：   2014-07-02
*************************************************/
@isTest
private class Test_AdvanceAccountPrintController {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        humanRresources__c hR = new humanRresources__c();
        hR.Name = '100';
        insert hR;
        
        department__c dp = new department__c();
        dp.Name = '软件部';
        insert dp;
        
        HRManage__c hrm = new HRManage__c();
        hrm.Name = 'hrm';
        hrm.department__c = dp.Id;
        hrm.HR__c = hR.Id;
        insert hrm;
        
        Account a = new Account();
        a.Name='aed';
        a.accountCode__c='adfsf';
        a.status_del__c='启用';
        insert a;
        
        
        project__c p = new project__c();
        p.Name = 'p';
        p.projectType__c='不间断电源';
        p.projectStatus__c = '新建';
        p.projectStartDate__c=Date.today();
        p.relateAccount__c=a.Id;
        insert p;
        
        advance__c ad = new advance__c();
        ad.Name = '500yuan';
        ad.amount__c = 100;
        ad.Date__c=Date.today();
        ad.projectRecordNum__c=p.Id;
        ad.user__c=hrm.Id;
        insert ad;{}
        
       
        
        ApexPages.currentPage().getParameters().put('advanceId',ad.Id);
        ApexPages.currentPage().getParameters().put('userId',hrm.Id);
        AdvanceAccountPrintController aapc = new AdvanceAccountPrintController();
        
        
        
        
    }
}