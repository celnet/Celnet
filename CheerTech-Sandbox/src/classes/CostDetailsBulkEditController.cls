/** 
*	功能：费用明细批量编辑
*	作者：winter
*	日期：2014-5-6 
*/
public with sharing class CostDetailsBulkEditController 
{
	//CostDetail class 集合
	public list<CostDetail> listCostDetail {get;set;}
	public list<CostDetail__c> listCostDetail_delete {get;set;}
	//进程纪要ID
	public string eventId {get;set;}
	//项目ID
	public string projectId {get;set;}
	//费用类型ID集合
	public String costTypeIds {get;set;}
	//页面编辑状态
	public Boolean IsEidt {get;set;}
	//按钮区域显示是否
	public Boolean IsViewButton {get;set;}
	public Decimal  totalAmount{set;get;}
	public Integer saveIsOk{set;get;}
    public	event__c cost{set;get;}
    public User thisUser{get;set;}
    //当前用户是否是进程纪要创建人
    public Boolean IsCreatUser {get;set;}
   // list<CostDetail__c> listSaveCostDetail{get;set;}
	
	//	构造方法
	
	public CostDetailsBulkEditController(ApexPages.StandardController controller)
	{
		IsEidt = false;
		IsViewButton = true;
		saveIsOk=0;
		costTypeIds='';
		system.debug('CostDetailsBulkEditController');
		//点击按钮获取到主关系event__c的ID
		//system.debug('费用类型ID集合' + controller.getSubject());
		cost = (event__c)controller.getRecord();
		this.eventId = cost.Id;
		listCostDetail_delete=new list<CostDetail__c>() ;
		 thisUser=[Select u.Title, u.Name From User u where u.id=:UserInfo.getUserId()];
		for(event__c event : [Select e.project__c,CreatedById,e.project__r.projectStatus__c,e.eventCost__c, e.eventCostApprove__c,e.salesManagerApproval__c,
							 e.eventCostVerify__c, e.Id From event__c e where Id =:eventId   ])
		{
			projectId = event.project__c;
			IsCreatUser = (event.CreatedById == thisUser.Id);
			if(event.eventCostApprove__c != '未审批' && event.eventCostApprove__c != '拒绝')
			{
				IsViewButton = false;
			}
			if(event.eventCostVerify__c != '未审核' && event.eventCostVerify__c != '拒绝')
			{
				IsViewButton = false;
			}
			totalAmount=event.eventCost__c;
		}
		
		Initialize();
		
	}
	
	
	public void changTotalAmount()
	{
		//汇总一下总费用
		totalAmount=0;
		for(Integer i = 0;i < listCostDetail.size();i++)
		{
			if(listCostDetail[i].cdc.costAmount__c == null) 
			{
				continue;
			}
			totalAmount+=listCostDetail[i].cdc.costAmount__c;
		}
	}
	
	public void addToDeleteList()
	{
		//加入到删除链表，只有在保存的时候执行dml操作，真正删除
		
		for(Integer i = 0;i < listCostDetail.size();i++)
		{
			if(listCostDetail[i].isDelete)
			{
				if(listCostDetail[i].cdc.Id != null)
				{
					listCostDetail_delete.add(listCostDetail[i].cdc);
				}
				
				listCostDetail.remove(i);
				i--;
			}
			
		}
		changTotalAmount();
	}
	
		//取消添加
	
	public void  cancelAddCostDetail()
	{
		listCostDetail.clear();
	
		for(CostDetail__c cd : [SELECT Id,Name,costRemark__c,costName__c,costAmount__c FROM CostDetail__c where relateEvent__c=:eventId])
		{
			if(cd.costAmount__c != null && cd.costAmount__c != 0)
			{
				listCostDetail.add(new CostDetail(false,cd));
			}
			
		}
		IsEidt = false;
	}
	 
	//根据选择的费用类型ID集合自动创建行
	
	public void CostAddCostDetail()
	{
		if(costTypeIds.indexOf(',')==-1)
		{
			costTypeIds='';
			return;
		}
		list<String> str_cost = costTypeIds.split(',');
		if(str_cost.isEmpty())
		{
			return;
		}
		for(cost__c cost : [Select c.status__c, c.detail__c, c.costCode__c, c.Name, c.Id From cost__c c where Id IN : str_cost])
		{
			CostDetail__c cd=new CostDetail__c();
			cd.relateEvent__c = eventId;
			//cd.relateProject__c = projectId;
			cd.costName__c = cost.Name;
			cd.Name = cost.costCode__c;
			CostDetail costdetail = new CostDetail(true,cd);
			listCostDetail.add(costdetail);
		}
		costTypeIds='';
		IsEidt = true;
	}
	
	
	//	让VFpage获取费用详单信息(costdetails)
	
	public class CostDetail
	{
		public boolean isDelete {get;set;}
		public CostDetail__c cdc {get;set;}
		public CostDetail(boolean isDelete,CostDetail__c cdc)
		{
			this.isDelete = isDelete;
			this.cdc = cdc;
		}
	}
	
	
	//	初始化
	
	public void Initialize()
	{
		listCostDetail = new list<CostDetail>();
		//通过外键ID（relateEvent__c）查询每个CostDetail__c对象，
		for(CostDetail__c cd : [SELECT Id,Name,costRemark__c,costName__c,costAmount__c FROM CostDetail__c
					 where relateEvent__c=:eventId ORDER BY Name])
		{
			listCostDetail.add(new CostDetail(false,cd));
		}
	}

	
	//	删除行
	
	public void DeleteCostDetail()
	{	
		//设置一个删除集合
		list<CostDetail__c> listDeleteCostDetail = new list<CostDetail__c>();
		//对listCostDetail进行循环，找出需要删除的CostDetail__c，放入删除集合中
		for(Integer i = listCostDetail.size()-1;i>=0;i--)
		{
			if(listCostDetail[i].isDelete)
			{	
				if(listCostDetail[i].cdc.Id != null)
				{
					listDeleteCostDetail.add(listCostDetail[i].cdc);
				}
				listCostDetail.remove(i);
			}
		}
		if(listDeleteCostDetail.size()>0)
		{
			try
			{
				//执行删除
			delete listDeleteCostDetail;
			listDeleteCostDetail.clear();
			saveIsOk =1;
			} 
			catch(Exception e)
			{
				saveIsOk =2;
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
				ApexPages.addMessage(msg);
			}
		}
	}
	
	//	保存
	
	public void SaveCostDetail()
	{
		//设置一个保存集合
		
		//将删除的真正删除
		
		if(listCostDetail_delete.size()>0)
		{
			try
			{
				//执行删除
			delete listCostDetail_delete;
			listCostDetail_delete.clear();
			} 
			catch(Exception e)
			{
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
				ApexPages.addMessage(msg);
			}
		}
		
		
		list<CostDetail__c> listSaveCostDetail = new list<CostDetail__c>();
		//对listCostDetail进行循环，对每一个修改过或者添加过的CostDetail__c进行校验,保证每个CostDetail__c为正确格式
		for(Integer i = 0;i < listCostDetail.size();i++)
		{
			CostDetail__c cd = listCostDetail[i].cdc;
			if(cd.costAmount__c == null || cd.costAmount__c == 0)
			{
				saveIsOk =2;
				IsEidt=true;				
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'金额不能为空');
				ApexPages.addMessage(msg);
				return ;
			}
			//将正确格式的CostDetail__c放入保存集合
			if(cd.Name != null && cd.costAmount__c != null)
			{
				listSaveCostDetail.add(cd);
			}
		}
		//执行保存
		if(listSaveCostDetail.size() > 0)
		{
			try
			{
				upsert listSaveCostDetail;
				listSaveCostDetail.clear();
				saveIsOk = 1;
			} 
			catch(Exception e)
			{
				saveIsOk =2;				
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
				ApexPages.addMessage(msg);
				return ;
			}
			
		 }
		

		IsEidt=false;
		
	}
	
	
	//切换编辑状态
	
	public void EditChange()
	{
		if(IsEidt)
		{
			IsEidt=false;
		}else{
			IsEidt=true;		
		}
	}

	
}