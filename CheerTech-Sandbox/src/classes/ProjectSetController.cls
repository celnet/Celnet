/*
 *@author:Spring
 *@date:2014-10-20
 *<h1>description<h1>
 *处理项目中"人员阶段贡献"的控制类<br/>。
 *流程:<br/>
 *根据传入id==>查项目对象===》参与人对象，产品对象
 *参与人对象==>阶段人员贡献对象
 *涉及到的公式:
 *贡献度系数=各阶段比例*各阶段贡献系数 之和
 *利润贡献=贡献度系数*项目利润
 *产品贡献度=贡献度系数*产品数量
 */
public without sharing class ProjectSetController {
    //projectId
    public String projectId{get;set;}
    //项目
    public project__c project{get;set;}
    //项目阶段
    public List<ProjectStage__c> list_ProjectStage{get;set;}
    //项目人员
    public List<projectPerson__c> proPerson{get;set;}
    //产品
    public List<ProjectProduct__c> proProduct{get;set;}
    //组装集合
    public List<ProjectContribute> result{get;set;}
    //每阶段，人员贡献之和
    public Map<String,decimal> sumStageContribute{get;set;}
    public List<decimal> list_sumStageContribute{get;set;}
    //每个产品贡献之和
    public Map<String,decimal> sumProductStage{get;set;}
    public List<decimal> list_sumProductStage{get;set;}
    //每个贡献度系数之和
    public decimal sumStage{get;set;}
    //每个产品之和
    public decimal sumProduct{get;set;}
    //控制页面，显示table和编辑table
    public Integer flag{get;set;}
    //新增加的考核产品后，让每个人=====考核产品    1:N 关系
    public List<ProductContribute__c> list_patch_product;
    
    
    public ProjectSetController(ApexPages.StandardController controller){
        projectId=controller.getId();
        //初始化
        list_patch_product = new List<ProductContribute__c>();
        flag = 0;
        init();
    }
    
    public void init(){
        project = [Select p.rev_total_amount__c, p.relateAccount__c, p.project_total_amount__c, p.projectType__c, p.projectStatus__c, p.projectStartDate__c, p.projectPayoutBudget__c, p.projectIncomeBudget__c, p.projectEndDate__c, p.projectDetail__c, p.projectCategory__c, p.income_total_amount__c, p.costs_total_amount__c, p.WeeklyProfitPercentage__c, p.SystemModstamp, p.RecordTypeId, p.PurchaseAmount__c, p.ProjectRestProfit__c, p.ProjectProfitRatePrd__c, p.ProjectManCostAmount__c, p.ProjectExpenseTotal__c, p.PredictProfitContribute__c, p.PredictIncomePercentage__c, p.Name, p.Id From project__c p where id =:projectId];
        //--
        proPerson=[Select p.relateProject__c, p.joinPerson__r.Name, p.joinPerson__c, p.Name, p.IsDeleted, p.Id From projectPerson__c p where relateProject__c =:projectId order by p.CreatedDate];
        if(proPerson == null || proPerson.size() == 0){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO,'请添加项目考核人员!');
            ApexPages.addMessage(msg);
            flag = 2;
            return;
        }
        
        //按阶段序号排序
        list_ProjectStage = [Select p.SystemModstamp,p.StageProject__c, p.StagePercentage__c, p.StageNumber__c, p.Name, p.Id From ProjectStage__c p where StageProject__c =:projectId order by p.StageNumber__c];
        if(list_ProjectStage == null || list_ProjectStage.size() == 0){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO,'请添加项目考核阶段!');
            ApexPages.addMessage(msg);
            flag = 2;
            return;
        }
        
        proProduct=[Select ProjectProduct__c,p.ProductProject__c, p.ProductAmount__c, p.Name, p.Id From ProjectProduct__c p where ProductProject__c  =:projectId order by p.CreatedDate];
        if(proProduct == null || proProduct.size() == 0){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO,'请添加考核产品!');
            ApexPages.addMessage(msg);
            flag = 2;
            return;
        }
        
        //初始化sumStageContribute
        sumStageContribute = new Map<String,decimal>();
        for(ProjectStage__c o : list_ProjectStage){
            sumStageContribute.put(o.id,0);
        }
        //初始化sumProductStage
        sumProductStage = new Map<String,decimal>();
        for(ProjectProduct__c o : proProduct){
            sumProductStage.put(o.id,0);
        }
        //初始化sumStage
        sumStage = 0;
        //初始化sumProduct
        sumProduct = 0;
        result = new List<ProjectContribute>();
        //先遍历人员
        for(projectPerson__c per : proPerson){
            ProjectContribute obj = new ProjectContribute();
            obj.projectPerson=per;
            //当前参与人，在整个项目中的所有贡献的记录<where 人id and 所属项目>
            //System.debug('项目参与人='+per.joinPerson__r.Name);
            List<StageContribute__c> scbc=[Select s.StageContributePercentage__c, s.ProfitContribute__c,  s.Name, s.MPersonName__c, s.Id, s.ContributeStage__c, s.ContributeCoefficient__c From StageContribute__c s where MPersonName__c =: per.joinPerson__c and s.ContributeStage__r.StageProject__c =:projectId ];
            //System.debug('=============scbc='+scbc);
            //表格显示时：
            obj.projectStage = new List<StageContribute__c>();
            //中转一下，处理某阶段，没有参与
            Map<String,StageContribute__c> tempStage = new Map<String,StageContribute__c>();
            //人员贡献度计算
            //项目各阶段比例*人贡献之和
            decimal temp=0;
            for(ProjectStage__c pjsc : list_ProjectStage){
                for(StageContribute__c scbc_1 : scbc){
                    if(pjsc.id==scbc_1.ContributeStage__c){
                        tempStage.put(pjsc.id,scbc_1);
                        //计算值
                        if(pjsc.StagePercentage__c == null){ pjsc.StagePercentage__c = 0; }
                        if(scbc_1.StageContributePercentage__c == null){scbc_1.StageContributePercentage__c = 0; }
                        //System.debug(pjsc.StagePercentage__c+'=============scbc='+scbc_1.StageContributePercentage__c);
                        temp += pjsc.StagePercentage__c * scbc_1.StageContributePercentage__c * 0.01*0.01;
                        
                        sumStageContribute.put(pjsc.id,(sumStageContribute.get(pjsc.id)+scbc_1.StageContributePercentage__c));
                    }
                }
                
            }
            //当某阶段，人员未参与时，给空
            for(ProjectStage__c pjsc : list_ProjectStage){
                if(tempStage.get(pjsc.id) != null){
                    obj.projectStage.add(tempStage.get(pjsc.id));
                }else{
                    StageContribute__c temp_sc = new StageContribute__c();
                    temp_sc.StageContributePercentage__c = 0;
                    temp_sc.MPersonName__c = per.joinPerson__c;
                    temp_sc.ContributeStage__c = pjsc.id;
                    obj.projectStage.add(temp_sc);
                }
            }
            //System.debug(per.joinPerson__r.Name+'--人员产品贡献--'+obj.projectStage);
            tempStage = null;
            //daisy 人员贡献度
            obj.humanContribute = temp.setscale(2);
            sumStage += temp.setscale(2);
            temp = 0;
            //System.debug('------obj.humanContribute-------->'+obj.humanContribute);
            //产品贡献度 = 项目贡献度系数 * 产品数量
            obj.productContribute = new List<decimal>();
            decimal tempProductContribut ;
            decimal tempProfitContribute ;
           //查出每个人对应产品的产品贡献 where(考核人id and 考核产品所属项目)
           List<ProductContribute__c> productHuman = [Select p.ProjectProduct__c, p.PersonName__c, p.PcontributePercentage__c,p.ProjectProduct__r.ProductProject__c, p.Id From ProductContribute__c p where p.PersonName__c =:per.joinPerson__c and p.ProjectProduct__r.ProductProject__c =:projectId ];
           Map<String,ProductContribute__c> productHuman_map = new Map<String,ProductContribute__c>();
           for(ProjectProduct__c pjpc : proProduct){
	       		for(ProductContribute__c p : productHuman){
	       			if(pjpc.id==p.ProjectProduct__c){
	       				productHuman_map.put(pjpc.id,p);
	       			}
	       		}
           }
           //人--对应产品贡献
           List<ProductContribute__c> productStage = new List<ProductContribute__c>();
           // integer index=1;
            for(ProjectProduct__c pjpc : proProduct){
            	if(pjpc.ProductAmount__c == null){
            		pjpc.ProductAmount__c = 0;
            	}
            	//如果存在就重新计算
            	if(productHuman_map.get(pjpc.id) !=null){
            		ProductContribute__c p = productHuman_map.get(pjpc.id);
            		tempProductContribut=pjpc.ProductAmount__c * obj.humanContribute;
            		obj.productContribute.add( tempProductContribut.setscale(2));
            		p.PcontributePercentage__c = tempProductContribut.setscale(2);
            		productStage.add(p);
            	}else{
            		//不存在创建
            		ProductContribute__c createPro = new ProductContribute__c();
            		createPro.ProjectProduct__c = pjpc.id;
            		createPro.PersonName__c = per.joinPerson__c;
            		
            		tempProductContribut=pjpc.ProductAmount__c * obj.humanContribute;
            		obj.productContribute.add( tempProductContribut.setscale(2));
            		createPro.PcontributePercentage__c = tempProductContribut.setscale(2);
            		
            		productStage.add(createPro);
            		list_patch_product.add(createPro);
            		System.debug('---------不存在------'+createPro);
            	}
                if(sumProductStage.containsKey(pjpc.id)){
                    tempProductContribut=pjpc.ProductAmount__c * obj.humanContribute + sumProductStage.get(pjpc.id);
                    sumProductStage.put(pjpc.id,tempProductContribut.setscale(2));
                }
            }
            obj.ProductStage = productStage;
            //System.debug(per.joinPerson__r.Name+'--产品贡献--'+productStage);
            //利润贡献度 = 项目贡献率 * 预估利润贡献
            tempProfitContribute = obj.humanContribute * project.PredictProfitContribute__c;
            obj.profitContribute = tempProfitContribute.setscale(0);
            sumProduct += obj.profitContribute;
            result.add(obj);
        }
        //map==>list
        list_sumStageContribute = new List<decimal>();
        for(ProjectStage__c o : list_ProjectStage){
            if(sumStageContribute.get(o.id) != null){
                list_sumStageContribute.add(sumStageContribute.get(o.id));
            }else{
                list_sumStageContribute.add(0);
            }
        }
        sumStageContribute.clear();
        //System.debug('=======>list_sumStageContribute=='+list_sumStageContribute);
        list_sumProductStage = new List<decimal>();
        for(ProjectProduct__c o : proProduct){
            if(sumProductStage.get(o.id) != null){
                list_sumProductStage.add(sumProductStage.get(o.id));
            }else{
                list_sumProductStage.add(0);
            }
        }
        sumProductStage.clear();
        //System.debug('=======>list_sumProductStage=='+list_sumProductStage);
    }
    
    class ProjectContribute{
        //项目人员
        public projectPerson__c projectPerson{get;set;}
        //项目人员在每个阶段的项目贡献
        public List<StageContribute__c> projectStage{get;set;}
        //项目人员在每个产品的产品贡献
        public List<ProductContribute__c> ProductStage{get;set;}
        //人员贡献度
        public decimal humanContribute{get;set;}
        //利润贡献度
        public decimal profitContribute{get;set;}
        //产品贡献度
        public List<decimal> productContribute{get;set;}
    }
    
    /*********************
     *当新增加考核产品时持久化到数据库，持久化后清除集合值
     ********************/
    public void patch(){
    	if(list_patch_product !=null && list_patch_product.size() >0){
    		System.debug('--------要插入的集合-----'+list_patch_product);
    		insert list_patch_product;
    		list_patch_product.clear();
    		System.debug('--------要插入的集合是否清除了-----'+list_patch_product);
    	}
    }
    
    /*******************************************************************************************************
	 *Spring 2014-10-29
	 *描述:现在要把产品贡献持久化到数据库，由于产品贡献=贡献度系数*产品数量，贡献度系数=阶段人员贡献*阶段权重，
	 *阶段人员贡献前台修改保存时scbc能保存值，产品人员贡献不会pdtb 所以重算贡献度系数，和产品人员贡献，然后更新pdtb
	 *
	 *******************************************************************************************************/
    public void save(){
    	boolean isError = false;
        for(ProjectContribute obj : result){
        	//阶段人员贡献
            List<StageContribute__c> scbc = obj.projectStage;
            decimal temp=0;
            for(ProjectStage__c pjsc : list_ProjectStage){
                for(StageContribute__c scbc_1 : scbc){
                    if(pjsc.id==scbc_1.ContributeStage__c){
                        //计算值
                        if(pjsc.StagePercentage__c == null){ pjsc.StagePercentage__c = 0; }
                        if(scbc_1.StageContributePercentage__c == null){scbc_1.StageContributePercentage__c = 0; }
                        temp += pjsc.StagePercentage__c * scbc_1.StageContributePercentage__c * 0.01*0.01;
                    }
                }
            }
            //重算出来的贡献度系数
            temp = temp.setscale(2);
            System.debug(obj.projectPerson.joinPerson__r.Name+'---------重算出来的贡献度系数-----'+temp);
            //产品人员贡献
            List<ProductContribute__c> pdtb = obj.ProductStage;
            for(ProjectProduct__c pjpc : proProduct){
            	for(ProductContribute__c p : pdtb){
            		if(pjpc.id==p.ProjectProduct__c){
            			p.PcontributePercentage__c = temp * pjpc.ProductAmount__c;
            		}
            	}
            }
            System.debug('---重算出来的贡献度系数---'+pdtb);
            try{
            	upsert scbc;
            	upsert pdtb;
            }catch(Exception e){
            	isError = true;
            	System.debug('----error------'+e.getMessage());
            	break;
            }
        }
        if(isError){
        	return;
        }
        flag = 0;
        init();
    }
    
    public void edit(){
        if(flag == 1){
            flag  = 0;
        }else{
            flag = 1;
        }
        
    }
   
}