/**
 * Author: Steven
 * Date: 2014-11-11
 * Description: Test StaffContCannotChangeByOthers.trigger
 */
@isTest
private class Test_StaffContCannotChangeByOthers {
	static testmethod void myUnitTest(){
		User testUser = [Select Id, ProfileId From User Where IsActive = true And Profile.Name != '系统管理员' And Department = '软件部' And UserType = 'Standard' limit 1];
		
		System.runAs(testUser){
			project__c pc = new project__c();
			pc.projectStatus__c = '新建项目';
			insert pc;
			
			Account Acc=new Account();
	        Acc.Name='4rfgfgfds1';
	        Acc.status_del__c='启用';
	        insert Acc;
			
			contract__c cc = new contract__c();
			cc.relateProject__c = pc.Id;
			cc.contractSummary__c = 'test';
			cc.ContractNum__c = 'test';
			cc.relateAccount__c = acc.Id;
			insert cc;
			
			humanRresources__c hum=new humanRresources__c();
	        hum.Name='a';
	        hum.HRPrice__c=5000;
	        hum.status__c='启用';
	        insert hum;
	        
	        department__c dep=new department__c();
	        dep.Name='软件部';
	        insert dep;
	        
	        HRManage__c hr=new HRManage__c();
	        hr.Name='a';
	        hr.department__c=dep.Id;
	        hr.HR__c=hum.Id;
	        hr.status__c='启用';
	        insert hr;
			
			Staff_contribution__c sc = new Staff_contribution__c();
			sc.ProjectContract__c = cc.Id;
			sc.HumanResourcesLibrary__c = hr.Id;
			sc.HRL_project__c = pc.Id;
			sc.day_count__c = 3;
			insert sc;
		
			sc.day_count__c = 4;
			
			update sc;
			
			delete sc;
		}
		
	}
}