/**
 * Author: Steven
 * Date: 2014-11-11
 * Description: test AdvanceLockAfterProjectLocked.trigger
 */
@isTest
private class Test_AdvanceLockAfterProjectLocked {
	static testmethod void myUnitTest(){
		project__c pc = new project__c();
		pc.projectStatus__c = '新建项目';
		insert pc;
		
		advance__c ac = new advance__c();
		ac.projectRecordNum__c = pc.Id;
		insert ac;
		
		pc.projectStatus__c = '失败';
		update pc;
		
		try{
			update ac;
		}catch(Exception e){
			
		}
		
		try{
			delete ac;
		} catch(Exception e ){
			
		}
	}
}