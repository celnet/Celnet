/*************************************************
class:      AdvanceAccountPrintController
Description:   暂支单打印的控制器类 
Table Accessed: 暂支单 advance__c 用户 user 员工 employee
Table Updated:  
Author：Snowz.zhu
CreateDate：   2014-06-30

因为暂支原因和暂支主题过长还有项目名称，导致页面table 会自动拉长，导致打印不出来
下面的暂支主题和暂支原因 就是经过函数 public string AutoBranch(string strInput,Integer len)
处理，每个几个字换下行，



*************************************************/
public with sharing class AdvanceAccountPrintController 
{
     public advance__c advance{get;set;}
     //当前用户
     public User user{get;set;}
      //从url传过来的暂支的Id
     public String advanceId{get;set;}
      //从url传过来的用户Id
     public String userId{get;set;}
      //此暂支执行人 （用户）  查找 人力资源库的员工
     public HRManage__c employee{get;set;} 
      //项目名字
     public string projectName{get;set;}
      //暂支原因    
      public string reason{get;set;}
      //暂支主题
      public string title{get;set;}
    //构造函数
    public AdvanceAccountPrintController()  
    {   
        projectName='';
        advanceId=ApexPages.currentPage().getParameters().get('advanceId');
        if(advanceId!=null && advanceId!='')
        {
            advance=[Select a.user__c, a.reviewer__c, a.review__c,a.salesManagerApproval__c, a.reviewOpinion__c, a.reason__c,a.projectRecordNum__c, a.projectRecordNum__r.Name,a.approve__c, a.approvalOpinion__c, a.amount__c,  a.Name, a.Id, a.Date__c, a.Approver__c From advance__c a where id=:advanceId];
            try{
                user=[Select u.Title, u.Name, u.Department From User u where u.id=:ApexPages.currentPage().getParameters().get('userId')];
            }catch(exception e){}
            
            employee=[Select h.department__r.Name,department__c, h.Name, h.Id , h.HRCode__c
             From HRManage__c h where h.Id=:advance.user__c];
        }
        
        //这里处理 暂支主题 项目名称 和暂支原因，让他们自动换行
        String tmpProjectStr = '' ;
        tmpProjectStr = advance.projectRecordNum__r.Name;
        projectName=AutoBranch(tmpProjectStr,50);
        tmpProjectStr = advance.reason__c;
        reason=AutoBranch(tmpProjectStr,19);
        tmpProjectStr = advance.Name;
        title=AutoBranch(tmpProjectStr,19);
    }
    //根据字段 自动换行
    //strInput 为输入的字符串  len为每个 len个字符换下行
    public string AutoBranch(string strInput,Integer len){
        if(strInput == null)
        {
            return '';//判断为空 直接返回
        }
        Integer len_Input=strInput.length();//获取总长度
        string result='';//需要返回的字符串初始化，经过换行处理的字符串 有result 返回
        
        //如果小于len，就不用换行，
        if(len_Input<len){  
            return strInput;
        }else{
            
            while(len_Input>=len){
                result+=strInput.trim().substring(0,len)+'<br/>';//加换行符，并且将 前len个字符加上换行符付给result
                strInput=strInput.trim().substring(len,strInput.length());//将开始的len个字符删去
                len_Input-=len;//len——input 变为  剩下的字符串
                if(len_Input<len){  //结束处理
                    result+=strInput;
                }
            }
            system.debug(result);
            return result;//返回
        }
    }
}