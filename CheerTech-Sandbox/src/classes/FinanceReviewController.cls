/**
 * Author:bill
 * Date:2014-6-23
 * Description:财务费用管理控制类
 */
public class FinanceReviewController 
{

	public List<event__c> list_event{get;set;}
	public String IsConfirmed{get;set;}
	public event__c event{get;set;}
	public FinanceReviewController()
	{
		IsConfirmed='未确认';
		event = new event__c();
		advance = new advance__c();
		list_event = new List<event__c>();
		list_advance = new List<advance__c>();
		list_reDetail = new List<reDetail__c>(); 
	}
	//进程记要
	
	public Date Date_Start{get;set;}
	public Date Date_End{get;set;}	
	public String EventConfirmStatus{get;set;}
	//进程纪要查询
	public void eventQuery()
	{
		list_event.clear();
		String strQuery = 'Select e.eventActionPerson__r.Name,e.eventCostApprove__c, e.project__c, e.eventDetail__c, e.eventDate__c, e.eventCost__c, e.eventCostVerify__c, e.Name, e.Id, e.eventActionPerson__c From event__c e ';
		if(event.eventActionPerson__c != null)
		{
			//strQuery += ' where e.eventActionPerson__c = \''+event.eventActionPerson__c+'\' AND eventCostVerify__c IN (\'审核中\',\'已审核\') AND eventCostVerify__c != \'已放款\'';
			strQuery += ' where e.eventActionPerson__c = \''+event.eventActionPerson__c+'\'  ';
		}else{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO,'请选择一个员工');
			ApexPages.addMessage(msg);
			return;		
		}
		if(EventConfirmStatus!=null)
		{
			strQuery += ' AND  eventCostVerify__c = \'' + EventConfirmStatus + '\'';
		}
		if(Date_Start!=null)
		{
			strQuery += ' AND eventDate__c>=:Date_Start';
		}
		if(Date_End!=null)
		{
			strQuery += ' AND eventDate__c<=:Date_End';
		}	
		//system.debug(strQuery +'1231231');
		list_event = Database.Query(strQuery);	
	}
	//暂支
	public advance__c advance{get;set;}	
	public Date Date_Start2{get;set;}
	public Date Date_End2{get;set;}	
	public List<advance__c> list_advance{get;set;}	
	public String AdvanceConfirmStatus{set;get;}
	//暂支查询
	public void advanceQuery()
	{
		list_advance.clear();
		String strQuery = 'Select a.review__c, a.user__r.Name, a.user__c, a.projectRecordNum__c, a.approve__c, a.amount__c, a.Name, a.Id, a.Date__c From advance__c a ';
		if(advance.user__c != null)
		{
			//strQuery += ' where user__c =\''+advance.user__c+'\' AND review__c IN (\'审核中\',\'已审核\') AND review__c != \'消单\'';
			strQuery += ' where user__c =\''+advance.user__c+'\'  ';
		}else{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO,'请选择一个员工');
			ApexPages.addMessage(msg);
			return;		
		}
		if(AdvanceConfirmStatus!=null)
		{
			strQuery += ' AND  review__c = \'' + AdvanceConfirmStatus + '\'';
		}
		if(Date_Start!=null)
		{
			strQuery += ' AND Date__c>=:Date_Start2';
		}
		if(Date_End!=null)
		{
			strQuery += ' AND Date__c<=:Date_End2';
		}	
		list_advance = Database.Query(strQuery);		
	}
	
	public ID eventId{get;set;}
	//已收款
	public void eventReveiw()
	{
		List<event__c> list_eventReview = [Select Id, eventCostVerify__c from event__c where Id =: eventId];
		if(list_eventReview != null && list_eventReview.size()>0)
		{
			list_eventReview[0].eventCostVerify__c = '已放款';
			update list_eventReview[0];
		}
		eventQuery();
	}
	public ID advanceId{get;set;}
	//消除暂支
	public void advanceReveiw()
	{
		List<advance__c> list_advanceReview = [Select Id, review__c from advance__c where Id =: advanceId];
		if(list_advanceReview != null && list_advanceReview.size()>0)
		{
			list_advanceReview[0].review__c = '放款';
			update list_advanceReview[0];
		}
		advanceQuery();
	}	
	public void advanceReveiw2()
	{
		List<advance__c> list_advanceReview = [Select Id, review__c from advance__c where Id =: advanceId];
		if(list_advanceReview != null && list_advanceReview.size()>0)
		{
			list_advanceReview[0].review__c = '消单';
			update list_advanceReview[0];
		}
		advanceQuery();
	}	
	
	//收款明细确认
	public String AccName{get;set;}	
	public String outNum{get;set;}	
	public String InvoiceNum{get;set;}	
	public Date Date_Start3{get;set;}
	public Date Date_End3{get;set;}
	public Double StartM{get;set;}
	public Double EndM{get;set;}			
	public List<reDetail__c> list_reDetail{get;set;}
	
			
	//暂支查询
	public void reDetailQuery()
	{
		try{
			list_reDetail.clear();
			String strQuery = 'Select r.relateProject__r.relateAccount__c, r.InvoiceDate__c, r.relateProject__c, r.relatePerson__c, r.relateContract__c, relateContract__r.ContractNum__c, r.reMoney__c, r.reMoneyRemark__c, r.reData__c, r.proportion__c, r.outNum__c, r.operatorTime__c, r.invoice__c, r.Name, r.Id, r.FinanceVerificated__c From reDetail__c r where ';
			if(IsConfirmed != null)
			{
				strQuery += ' FinanceVerificated__c = \'' + IsConfirmed + '\'';
			}
			if(AccName != null && AccName.trim() != '')
			{
				strQuery += ' and relateProject__r.relateAccount__r.Name like \'%' + AccName + '%\'';
			}
			if(outNum != null && outNum.trim() != '')
			{
				strQuery += ' and outNum__c = \'' + outNum + '\'';
			}
			if(InvoiceNum != null && InvoiceNum.trim() != '')
			{
				strQuery += ' and invoice__c = \'' + InvoiceNum + '\'';
			}			
			if(Date_Start3!=null)
			{
				strQuery += ' AND InvoiceDate__c>=:Date_Start3';
			}
			if(Date_End3!=null)
			{
				strQuery += ' AND InvoiceDate__c<=:Date_End3';
			}
			if(StartM != null && StartM>0)
			{
				strQuery += ' AND reMoney__c>=:StartM';
			}	
			if(EndM != null && EndM>0)
			{
				strQuery += ' AND reMoney__c<=:EndM';
			}
			
			strQuery += ' AND reMoney__c>0 AND reMoney__c!=NULL AND invoice__c!= NULL AND  InvoiceDate__c!= NULL';
				
			list_reDetail = Database.Query(strQuery);	
		}catch(Exception ex){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage()+'  '+ex.getStackTraceString());
			ApexPages.addMessage(msg);
			return;		
		}
	}	
	
	public ID reDetailId{get;set;}
	//确认收款 
	public void reDetailReveiw()
	{
		for(reDetail__c rede : list_reDetail)
		{
			if(reDetailId == rede.Id)
			{
				if(rede.reData__c == null)
				{
					ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO,'请填写收款日期');
					ApexPages.addMessage(msg);
					return;						
				}
				rede.FinanceVerificated__c = '已确认';
				rede.relatePerson__c = Userinfo.getUserId();
				rede.operatorTime__c = system.now();
				update rede;
			}
		}
		reDetailQuery();
	}
	//确认修改收款 
	public void reDetailChange()
	{
		for(reDetail__c rede : list_reDetail)
		{
			if(reDetailId == rede.Id)
			{
				if(rede.reData__c == null)
				{
					ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO,'请填写收款日期');
					ApexPages.addMessage(msg);
					return;						
				}
				//rede.FinanceVerificated__c = '已确认';
				rede.relatePerson__c = Userinfo.getUserId();
				rede.operatorTime__c = system.now();
				update rede;
			}
		}
		reDetailQuery();
	}
}