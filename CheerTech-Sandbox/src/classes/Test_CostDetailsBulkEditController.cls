/*************************************************
class:      Test_CostDetailsBulkEditController
Description:   进程纪要批量添加页面控制器测试类 
Table Accessed: 进程纪要 advance__c 用户 user 员工 employee 人力资源库 人力成本 部门
Table Updated:  
Author：Snowz.zhu
CreateDate：   2014-07-02
*************************************************/
@isTest
private class Test_CostDetailsBulkEditController {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        humanRresources__c hR = new humanRresources__c();
        hR.Name = '100';
        insert hR;
        
        department__c dp = new department__c();
        dp.Name = '软件部';
        insert dp;
        
        HRManage__c hrm = new HRManage__c();
        hrm.Name = '商务人员';
        hrm.department__c = dp.Id;
        hrm.HR__c = hR.Id;
        hrm.department__c =dp.Id;
        insert hrm;
        
        Account a = new Account();
        a.Name='a345';
        a.accountCode__c='adfsf';
        a.status_del__c='启用';
        insert a;
        
        
        project__c p = new project__c();
        p.Name = 'p';
        p.projectType__c='不间断电源';
        p.projectStatus__c = '新建';
        p.projectStartDate__c=Date.today();
        p.relateAccount__c=a.Id;
        insert p;
        
        event__c e=new event__c();
        e.Name = '600yuan';
        e.project__c = p.Id;
        e.eventActionPerson__c=hrm.Id;
        e.eventDate__c=Date.today();
        e.eventCostApprove__c = '审批中';
        e.eventCostVerify__c = '审批中';
        insert e;
        
        cost__c cost1 = new cost__c();
        cost1.Name='12345';
        cost1.costCode__c='100002';
        cost1.detail__c='12344555';
        insert cost1;
        cost__c cost2 = new cost__c();
         cost2.Name='123452';
        cost2.costCode__c='100002';
        cost2.detail__c='12344555';
        insert cost2;
        cost__c cost3 = new cost__c();
         cost3.Name='123425';
        cost3.costCode__c='100002';
        cost3.detail__c='12344555';
        insert cost3;
        CostDetail__c cd = new CostDetail__c();
        cd.costAmount__c=100;
        cd.relateEvent__c=e.Id;
        //cd.relateProject__c=p.Id;
        
        cd.Name='83723e';
        
        insert cd;
        CostDetail__c cd1 = new CostDetail__c();
        cd1.costAmount__c=100;
        cd1.relateEvent__c=e.Id;
       // cd1.relateProject__c=p.Id;
        cd1.Name='82372f';
        
        insert cd1;
        
          CostDetail__c cd2 = new CostDetail__c();
        cd2.costAmount__c=100;
        cd2.relateEvent__c=e.Id;
       // cd2.relateProject__c=p.Id;
        
        cd2.Name='837323';
        
        insert cd2;
        
        CostDetail__c cd3 = new CostDetail__c();
        cd3.costAmount__c=100;
        cd3.relateEvent__c=e.Id;
       // cd3.relateProject__c=p.Id;
        cd3.Name='83732f';
        
        
        insert cd3;
        
          CostDetail__c cd4 = new CostDetail__c();
       // cd4.costAmount__c=100;
        cd4.relateEvent__c=e.Id;
        //cd4.relateProject__c=p.Id;
        cd4.Name='834732f';
        
          CostDetail__c cd5 = new CostDetail__c();
        cd5.costAmount__c=100;
        cd5.relateEvent__c=e.Id;
        //cd5.relateProject__c=p.Id;
        cd5.Name='837532f';
    
         CostDetail__c cd6 = new CostDetail__c();
        cd6.costAmount__c=100;
        cd6.relateEvent__c=e.Id;
       // cd6.relateProject__c=p.Id;
        cd6.Name='834732f';
        
          CostDetail__c cd7 = new CostDetail__c();
        cd7.costAmount__c=100;
        cd7.relateEvent__c=e.Id;
       // cd7.relateProject__c=p.Id;
        cd7.Name='837532f';
       ApexPages.StandardController controller = new ApexPages.StandardController(e);
        CostDetailsBulkEditController cdbec = new CostDetailsBulkEditController(controller);
           
       // CostDetail cdi1 = new CostDetail();
        CostDetailsBulkEditController.CostDetail cdi1 = new CostDetailsBulkEditController.CostDetail(true,cd4);
        CostDetailsBulkEditController.CostDetail cdi2 = new CostDetailsBulkEditController.CostDetail(true,cd5);
        
        cdbec.listCostDetail.add(cdi1);
        cdbec.listCostDetail.add(cdi2);
        cdbec.SaveCostDetail();
        cdbec.cancelAddCostDetail();
        
        cdbec.costTypeIds=cost1.Id+','+cost2.Id;
        cdbec.CostAddCostDetail();
        cd5.costAmount__c=null;
        //cd5.relateEvent__c=e.Id;
        //cd5.relateProject__c=p.Id;
        cd5.Name='837532f';
         cdbec.costTypeIds=cd2.Id+','+cd3.Id;
        cdbec.CostAddCostDetail();
          cdbec.costTypeIds=cd1.Id+','+cd3.Id;
        cdbec.CostAddCostDetail();
         cdbec.costTypeIds=',,';
        cdbec.CostAddCostDetail();
         cdbec.costTypeIds='';
        cdbec.CostAddCostDetail();
        cdbec.EditChange();
        //cdbec.listCostDetail[0].cdc.Id=cd7.Id;
        cdbec.listCostDetail[0].isDelete=true;
        cdbec.DeleteCostDetail();
        list<CostDetail__c> costTmp = new list<CostDetail__c>();
        costTmp.add(cd1);
        costTmp.add(cd2);
        costTmp.add(cd3);
        cdbec.SaveCostDetail();
        cdbec.isEidt=true;
         cdbec.EditChange();
         cdbec.isEidt=false;
         cdbec.EditChange();
        
    }
}