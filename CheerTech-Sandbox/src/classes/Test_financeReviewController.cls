/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_financeReviewController {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        FinanceReviewController frc = new FinanceReviewController();
        frc.Date_Start = Date.today().addDays(-100);
        frc.Date_End = Date.today();
        frc.event.eventActionPerson__c = UserInfo.getUserId();
        frc.advance.user__c = UserInfo.getUserId();
        frc.AdvanceConfirmStatus = 'test';
        frc.AccName = 'test';
        frc.outNum = 'test';
        frc.InvoiceNum = 'test';
        frc.Date_Start3 = Date.today().addDays(-1);
        frc.Date_End3 = Date.today();
        frc.StartM = 10;
        frc.EndM = 20;
        
        frc.eventQuery();
		frc.advanceQuery();
		frc.eventReveiw();
		frc.advanceReveiw();
		frc.advanceReveiw2();
		frc.reDetailQuery();
        
        project__c pc = new project__c();
		pc.projectStatus__c = '新建项目';
		insert pc;
		
		Account Acc=new Account();
        Acc.Name='4rfgfgfds1';
        Acc.status_del__c='启用';
        insert Acc;
		
		contract__c cc = new contract__c();
		cc.relateProject__c = pc.Id;
		cc.contractSummary__c = 'test';
		cc.ContractNum__c = 'test';
		cc.relateAccount__c = acc.Id;
		insert cc;
		
        reDetail__c rc = new reDetail__c();
        rc.reData__c = Date.today();
        rc.relateContract__c = cc.Id;
        rc.relateProject__c = pc.Id;
        insert rc;
        
        frc.reDetailId = rc.Id;
        frc.list_reDetail.add(rc);
        
        
		frc.reDetailReveiw();
		
		frc.reDetailId = rc.Id;
        frc.list_reDetail.add(rc);
		frc.reDetailChange();
    }
}