public with sharing class AccountRegisterController 
{
	public Account acc{get;set;}
	
	public string lastName{get;set;}
	/*
	public string mobilePhone{get;set;}
	public string gender{get;set;}
	public date birthDate{get;set;}
	public string ageRange{get;set;}
	public string email{get;set;}
	public string province{get;set;}
	public string city{get;set;}
	public string address{get;set;}
	*/
	public AccountRegisterController()
	{
		
		acc = new Account();
	}
	 

	
	/*public void TransferLeadAToLeadB(Lead A , Lead B)
	{
		B.LeadSource = A.LeadSource;
		B.Address_Block__c = A.Address_Block__c;
		B.Address_Building__c = A.Address_Building__c;
		B.Address_District__c = A.Address_District__c;
		B.Address_Flat__c = A.Address_Flat__c;
		B.Address_Floor__c = A.Address_Floor__c;
		B.Address_Street__c = A.Address_Street__c;
		B.Address_Territory__c = A.Address_Territory__c;
		B.Region__c = A.Region__c;
		B.FirstName = A.FirstName;
		B.LastName = A.LastName;
		B.Email = A.Email;
		B.Salutation = A.Salutation;
		B.Birthday_1st_Child__c = A.Birthday_1st_Child__c;
		B.Birthday_2nd_Child__c = A.Birthday_2nd_Child__c;
		B.Birthday_3rd_Child__c = A.Birthday_3rd_Child__c;
		B.Birthday_4th_Child__c = A.Birthday_4th_Child__c;
		B.Last_Name_1st_Child__c = A.Last_Name_1st_Child__c;
		B.Last_Name_2nd_Child__c = A.Last_Name_2nd_Child__c;
		B.Last_Name_3rd_Child__c = A.Last_Name_3rd_Child__c;
		B.Last_Name_4th_Child__c = A.Last_Name_4th_Child__c;
		B.MobilePhone = A.MobilePhone;
	}
	*/
	public void Register()
	{
		acc = new Account();
		
		acc.AccountSource = '微信注册';
		
		acc.LastName = lastName;
		/*
		acc.mobilephone__c = mobilePhone;
		acc.gender__c = gender;
		acc.date_of_birth__c = birthDate;
		acc.age_range__c = ageRange;
		acc.PersonEmail = email;
		acc.province__c = province;
		acc.City__c = city;
		acc.address__c = address;
		*/
		insert acc;
	}
}