/**
 *
 *
 *
 **/
public without sharing class WeChatUserRegisterController {
	public Lead l{get;set;}
	public String birthDate{get;set;}
	public String inputBirthDate{get;set;}
	public String displayBirthDate{get;set;}
	public String openId;
	public WeChatUserRegisterController(){
		String code = ApexPages.currentPage().getParameters().get('code');
		String state = ApexPages.currentPage().getParameters().get('state');
		System.debug(code + '---' + state);
		Welink.WechatCalloutService wcs = new Welink.WechatCalloutService('Celnet');
		openId = wcs.GetOpenIdbyCode(code);
		System.debug(openId);
		l = new Lead();
		l.wechat__c = openId;
		
		inputBirthDate = '';
		displayBirthDate = 'display:none;';
	}
	
	public void saveLead(){
		if(birthDate != null && birthDate != ''){
			l.date_of_birth__c = Date.valueOf(birthDate);
		}
		
		inputBirthDate = 'display:none;';
		displayBirthDate = '';
		
		if(checkDuplicate()){
			try{
				insert l;
				// 注册成功
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.confirm, '恭喜，注册成功');
				ApexPages.addMessage(myMsg);
			} catch(Exception e){
				// 报错则在页面提示
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.error, e.getMessage());
				ApexPages.addMessage(myMsg);
			}
		} else {
			// 重复则在页面提示
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.error, '手机号码已注册，请重新注册');
			ApexPages.addMessage(myMsg);
		}
		
	}
	
	public boolean checkDuplicate(){
		
		String phoneNo = l.Phone;
		List<Lead> leads = [Select Id, Phone From Lead Where Phone =: phoneNo];
		List<Account> accs = [Select Id, mobilephone__c From Account Where mobilephone__c =: phoneNo];
		
		if(leads.isEmpty() && accs.isEmpty()){
			return true;	// 不存在重复记录，则返回true
		} else {
			return false;	// 存在重复记录，则返回false
		}
	}
}