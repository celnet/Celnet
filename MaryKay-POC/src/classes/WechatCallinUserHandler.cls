/**
 *
 *
 * Description: 判断是否位关注事件，是的话获取用户OpenId，根据用户OpenId获取微信用户信息保存到WeChatUser对象
 **/
global class WechatCallinUserHandler extends Welink.WechatCallinMsgHandler{
	global override void Handle(Welink.WechatCallinMsgPipelineContext context){
		Welink.WechatEntity.InEventMsg inEvent = (Welink.WechatEntity.InEventMsg)Context.InMsg;
		//string replace = '<a href="'+WechatBusinessUtility.WECHAT_SITE+'WechatSMSVerificationPage?openId='+inEvent.FromUserName+'">【注册/绑定】</a>';
		//string link = WechatBusinessUtility.QueryRemindingByKey('RN001',replace);
		
		
		//String targetUrl = 'http://www.baidu.com';
		
		String oauth_base_url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=';
		oauth_base_url += 'wx69caf6d5788371de';
		oauth_base_url += '&redirect_uri=';
		oauth_base_url += 'http://isvmaster-146a1ab1ef0-14a615bc0bf.force.com/Success/WeChatUserRegister';
		oauth_base_url += '&response_type=code&scope=snsapi_base';
		oauth_base_url += '&state=' + context.PublicAccountName;
		oauth_base_url += '#wechat_redirect';
		
		string link_mk = '感谢关注MaryKay官方微信，<a href="' + oauth_base_url + '">注册绑定</a>';
		
		if(inEvent.MsgType != Welink.WechatEntity.MESSAGE_TYPE_EVENT)
		{
			return ;
		}
		
		if(inEvent.Event == Welink.WechatEntity.EVENT_TYPE_SUBSCRIBE)
		{
			Welink__Wechat_Callout_Task_Queue__c userInfoQueue = new Welink__Wechat_Callout_Task_Queue__c();
			userInfoQueue.Welink__Public_Account_Name__c = context.PublicAccountName;
			userInfoQueue.Welink__Open_ID__c = inEvent.FromUserName;
			userInfoQueue.Welink__Type__c = Welink.WechatEntity.EVENT_TYPE_SUBSCRIBE;
			userInfoQueue.Welink__Processor_Name__c = 'WechatCalloutGetUserProcessor';
			userInfoQueue.Name = 'WechatCalloutGetUserProcessor';
			Welink.WechatCalloutQueueManager.EnQueue(userInfoQueue);
			Welink__Wechat_User__c sfUser = new Welink__Wechat_User__c();
			sfUser.Welink__Open_Id__c = inEvent.FromUserName;
			sfUser.Welink__Public_Account_Name__c = Context.PublicAccountName;
			upsert sfUser Welink__Open_Id__c;
			Welink.WechatEntity.OutTextMsg outMsg = Welink.WechatEntity.GenerateOutMsg(inEvent, link_mk);
			Context.OutMsg = outMsg;
		}
		
	}
}