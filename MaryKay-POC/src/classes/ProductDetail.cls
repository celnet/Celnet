public with sharing class ProductDetail 
{
	public string IsSelect1;
	public string IsSelect2;
	public string IsSelect3;
	public string IsSelect4;
	public integer P1;
	public integer P2;
	public integer P3;
	public integer P4;
	public integer Sum{get;set;}
	public list<ProInfo> List_P{get;set;}
	public ProductDetail()
	{
		IsSelect1 = ApexPages.currentPage().getParameters().get('IsSelect1');
		IsSelect2 = ApexPages.currentPage().getParameters().get('IsSelect2');
		IsSelect3 = ApexPages.currentPage().getParameters().get('IsSelect3');
		IsSelect4 = ApexPages.currentPage().getParameters().get('IsSelect4');
		P1 = 0;
		P2 = 0;
		P3 = 0;
		P4 = 0;
		List_P = new list<ProInfo>();
		init();
	}
	
	public void init()
	{
		if(IsSelect1=='true')
		{
			ProInfo P = new ProInfo();
			P.PName = 'MaryKay玫琳凯舒颜三件套';
			P.Price = 5270;
			P.Count = 1;
			P.Total = 5270;
			ProductInfo__c Prot = new ProductInfo__c();
			Prot = [select Picture__c from ProductInfo__c where PictureID__c='1419236110000'][0];
			P.Pro = Prot;
			P.Url = '/ProductList/resource/1419230118000/mlk_pic/Pic1.jpg';
			List_P.Add(P);
		}
		if(IsSelect2=='true')
		{
			ProInfo P = new ProInfo();
			P.PName = 'MaryKay玫琳凯美白淡斑套装';
			P.Price = 4770;
			P.Count = 1;
			P.Total = 4770;
			ProductInfo__c Prot = new ProductInfo__c();
			Prot = [select Picture__c from ProductInfo__c where PictureID__c='1419236147000'][0];
			P.Pro = Prot;
			P.Url = '/ProductList/resource/1419230118000/mlk_pic/Pic2.jpg';
			List_P.Add(P);
		}
		if(IsSelect3=='true')
		{
			ProInfo P = new ProInfo();
			P.PName = 'MaryKay玫琳凯幻时护肤2件套';
			P.Price = 4360;
			P.Count = 1;
			P.Total = 4360;
			ProductInfo__c Prot = new ProductInfo__c();
			Prot = [select Picture__c from ProductInfo__c where PictureID__c='1419236040000'][0];
			P.Pro = Prot;
			P.Url = '/ProductList/resource/1419230118000/mlk_pic/Pic3.jpg';
			List_P.Add(P);
		}
		if(IsSelect4=='true')
		{
			ProInfo P = new ProInfo();
			P.PName = 'MaryKay玫琳凯防晒套装';
			P.Price = 2000;
			P.Count = 1;
			P.Total = 2000;
			ProductInfo__c Prot = new ProductInfo__c();
			Prot = [select Picture__c from ProductInfo__c where PictureID__c='1419236174000'][0];
			P.Pro = Prot;
			P.Url = '/ProductList/resource/1419230118000/mlk_pic/Pic4.jpg';
			List_P.Add(P);
		}
		
		Sum=0;
		for(ProInfo P: List_P)
		{
			Sum += P.Total;
		}
	}
	
	public void SumTotal()
	{
		for(ProInfo P: List_P)
		{
			if(P.Count == 0)
			{
				P.Count = 1;
			}
			P.Total = P.Price * P.Count;
		}
		Sum=0;
		for(ProInfo P: List_P)
		{
			Sum += P.Total;
		}
	}
	
	public PageReference NextStep()
	{
		for(ProInfo P: List_P)
		{
			if(P.PName == 'MaryKay玫琳凯舒颜三件套')
			{
				P1 = P.Count;
			}
			else if(P.PName == 'MaryKay玫琳凯美白淡斑套装')
			{
				P2 = P.Count;
			}
			else if(P.PName == 'MaryKay玫琳凯幻时护肤2件套')
			{
				P3 = P.Count;
			}
			else if(P.PName == 'MaryKay玫琳凯防晒套装')
			{
				P4 = P.Count;
			}
		}
		
		PageReference pr = new PageReference('http://isvmaster-146a1ab1ef0-14a615bc0bf.force.com/ProductSubmit?P1='+P1+'&P2='+P2+'&P3='+P3+'&P4='+P4);
	  	pr.setRedirect(true);
	  	return pr;
	}
	
	public class ProInfo
	{
		public string PName{get;set;}
		public integer Price{get;set;}
		public integer Count{get;set;}
		public integer Total{get;set;}
		public string Pic{get;set;}
		public string Url{get;set;}
		public ProductInfo__c Pro{get;set;}
	
	}
}