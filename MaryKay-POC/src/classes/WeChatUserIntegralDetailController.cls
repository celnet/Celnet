public without sharing class WeChatUserIntegralDetailController {
	
	public String openId;
	public List<IntegralDetailWrapper> idwList{get;set;}
	
	public class IntegralDetailWrapper{
		public Integral_Detail__c idc{get;set;}
		public String points{get;set;}
		public String pointsdate{get;set;}
		public IntegralDetailWrapper(Integral_Detail__c i){
			this.idc = i;
			this.points = String.valueOf(i.Integral__c ==null?0:i.Integral__c);
			this.pointsdate = i.CreatedDate.year() + '-' + i.CreatedDate.month() + '-' + i.CreatedDate.day();
		}
	}
	
	public WeChatUserIntegralDetailController(){
		
		idwList = new List<IntegralDetailWrapper>();
		
		String code = ApexPages.currentPage().getParameters().get('code');
		String state = ApexPages.currentPage().getParameters().get('state');
		
		Welink.WechatCalloutService wcs = new Welink.WechatCalloutService('Celnet');
		openId = wcs.GetOpenIdbyCode(code);
		
		list<account> accs = [Select Id From Account Where wechat__c =: openId];
		list<lead> leads = [Select Id From Lead Where wechat__c =: openId];
		
		if(leads.isEmpty()){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.error, '您还没注册，请先注册绑定微信号');
			ApexPages.addMessage(myMsg);
			return;
		} else if(accs.isEmpty()){
			// 
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.error, '您还没消费积分');
			ApexPages.addMessage(myMsg);
			return;
		} 
		
		for(Integral_Detail__c i : [Select CreatedDate, Id,Type__c,Integral__c,Integral2__c From Integral_Detail__c Where Account__c =: accs[0].Id]){
			IntegralDetailWrapper idw = new IntegralDetailWrapper(i);
			idwList.add(idw);
		}
		
	}
}