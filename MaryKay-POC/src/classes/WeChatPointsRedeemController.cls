public without sharing class WeChatPointsRedeemController {
	
	public List<ProductWrapper> pwList{get;set;}
	public String openId;
	public String address{get;set;}
	public Lead l;
	public Account acc{get;set;}
	public String points{get;set;}
	
	public List<SelectOption> getOrderNumbers() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('0','0'));
        options.add(new SelectOption('1','1'));
        options.add(new SelectOption('2','2'));
        options.add(new SelectOption('3','3'));
        return options;
    }
	
	public class ProductWrapper{
		public ProductInfo__c pi{get;set;}
		public Integer num{get;set;}
		public String num_str{get;set;}
		public String picurl{get;set;}
		public ProductWrapper(ProductInfo__c p){
			this.pi = p;
			this.num = 0;
			this.num_str = '0';
			if(p.PictureID__c == null){
				this.picurl = 'http://isvmaster-146a1ab1ef0-14a615bc0bf.force.com/Success/resource/1419234915000/mlk0301';
			} else {
				this.picurl = 'http://isvmaster-146a1ab1ef0-14a615bc0bf.force.com/Success/resource/'+p.PictureID__c+'/'+p.PictureName__c;
			}
		}
	}
	
	public WeChatPointsRedeemController(){
		String code = ApexPages.currentPage().getParameters().get('code');
		String state = ApexPages.currentPage().getParameters().get('state');
		
		Welink.WechatCalloutService wcs = new Welink.WechatCalloutService('Celnet');
		openId = wcs.GetOpenIdbyCode(code);
		
		pwList = new List<ProductWrapper>();
		
		List<Lead> leads = [Select Id, LastName, Phone, address__c, gender__c, Email, wechat__c, date_of_birth__c From Lead Where wechat__c =: openId];
		
		if(leads.isEmpty()){
			// 未注册
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.error, '您还没注册，请先注册绑定微信号');
			ApexPages.addMessage(myMsg);
			return;
		} else {
			l = leads[0];
		}
		
		List<Account> accs = [Select Name, Id, wechat__c, total_grades__c From Account Where wechat__c =: openId];
		
		if(accs.isEmpty()){
			acc = new Account();
			acc.Name = l.LastName;
			acc.gender__c = l.gender__c;
			acc.address__c = l.address__c;
			acc.mobilephone__c = l.Phone;
			acc.wechat__c = l.wechat__c;
			acc.AccountSource = '微信注册';
			acc.date_of_birth__c = l.date_of_birth__c;
			points = '0';
		} else {
			acc = accs[0];
			points = String.valueOf(acc.total_grades__c);
		}
		
		for(ProductInfo__c pi : [Select Name,Id,PictureID__c,PictureName__c,Unit_Price__c,
									Based_Integral__c,Integral_Unit__c From ProductInfo__c limit 10]){
			ProductWrapper pw = new ProductWrapper(pi);
			pwList.add(pw);
		}
		
	}
	
	public void orderProducts(){
		Integer totalPoints = 0;
		
		list<consuming_record__c> crs = new list<consuming_record__c>();
		
		for(ProductWrapper pw : pwList){
			consuming_record__c cr = new consuming_record__c();
			cr.account__c = acc.Id;
			
			totalPoints += Integer.valueOf(pw.pi.Integral_Unit__c * Integer.valueOf(pw.num_str));
			
			cr.product__c = pw.pi.Id;
			cr.channels__c = '微信';
			cr.Type__c = '积分兑换';
			cr.quantity__c = Integer.valueOf(pw.num_str);
			cr.transaction_date__c = Date.today();
			cr.Address__c = address;
			if(cr.quantity__c != 0){
				crs.add(cr);
			}
		}
		
		if(totalPoints > Integer.valueOf(points)){
			// 积分数量不够
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.error, '您的积分数量不够，请重新选择商品');
			ApexPages.addMessage(myMsg);
			return;
		}
		
		if(address == null && address == ''){
			// 地址未填写
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.error, '请填写收货地址');
			ApexPages.addMessage(myMsg);
			return;
		}
		
		insert crs;
		points = String.valueOf(Integer.valueOf(points) - totalPoints);
		
		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.confirm, '订购成功');
		ApexPages.addMessage(myMsg);
	}
}