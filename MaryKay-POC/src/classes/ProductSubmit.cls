public with sharing class ProductSubmit 
{
	public integer P1;
	public integer P2;
	public integer P3;
	public integer P4;
	public integer Sum{get;set;}
	public list<ProInfo> List_P{get;set;}
	public consuming_record__c CField{get;set;}
	
	public string myAddress{get;set;}
	public string myUserName{get;set;}
	public string myPhone{get;set;}
	
	public ProductSubmit()
	{
		List_P = new list<ProInfo>();
		CField = new consuming_record__c();
		P1 = Integer.valueOf(ApexPages.currentPage().getParameters().get('P1'));
		P2 = Integer.valueOf(ApexPages.currentPage().getParameters().get('P2'));
		P3 = Integer.valueOf(ApexPages.currentPage().getParameters().get('P3'));
		P4 = Integer.valueOf(ApexPages.currentPage().getParameters().get('P4'));
		init();
	}
	
	public void Init()
	{
		if(P1!=0&&P1!=null)
		{
			ProInfo P = new ProInfo();
			P.PName = 'MaryKay玫琳凯舒颜三件套';
			P.Price = 5270;
			P.Count = P1;
			P.Total = 5270*P1;
			//P.Pic = 'URLFOR($Resource.mlk_pic, \'/Pic1.jpg\')';
			ProductInfo__c Prot = new ProductInfo__c();
			Prot = [select Picture__c from ProductInfo__c where PictureID__c='1419236110000'][0];
			P.Pro = Prot;
			P.Url = '/ProductList/resource/1419230118000/mlk_pic/Pic1.jpg';
			List_P.Add(P);
		}
		if(P2!=0&&P2!=null)
		{
			ProInfo P = new ProInfo();
			P.PName = 'MaryKay玫琳凯美白淡斑套装';
			P.Price = 4770;
			P.Count = P2;
			P.Total = 4770*P2;
			//P.Pic = 'URLFOR($Resource.mlk_pic, \'/Pic2.jpg\')';
			ProductInfo__c Prot = new ProductInfo__c();
			Prot = [select Picture__c from ProductInfo__c where PictureID__c='1419236147000'][0];
			P.Pro = Prot;
			P.Url = '/ProductList/resource/1419230118000/mlk_pic/Pic2.jpg';
			List_P.Add(P);
		}
		if(P3!=0&&P3!=null)
		{
			ProInfo P = new ProInfo();
			P.PName = 'MaryKay玫琳凯幻时护肤2件套';
			P.Price = 4360;
			P.Count = P3;
			P.Total = 4360*P3;
			//P.Pic = 'URLFOR($Resource.mlk_pic, \'/Pic3.jpg\')';
			ProductInfo__c Prot = new ProductInfo__c();
			Prot = [select Picture__c from ProductInfo__c where PictureID__c='1419236040000'][0];
			P.Pro = Prot;
			P.Url = '/ProductList/resource/1419230118000/mlk_pic/Pic3.jpg';
			List_P.Add(P);
		}
		if(P4!=0&&P4!=null)
		{
			ProInfo P = new ProInfo();
			P.PName = 'MaryKay玫琳凯防晒套装';
			P.Price = 2000;
			P.Count = P4;
			P.Total = 2000*P4;
			//P.Pic = 'URLFOR($Resource.mlk_pic, \'/Pic4.jpg\')';
			ProductInfo__c Prot = new ProductInfo__c();
			Prot = [select Picture__c from ProductInfo__c where PictureID__c='1419236174000'][0];
			P.Pro = Prot;
			P.Url = '/ProductList/resource/1419230118000/mlk_pic/Pic4.jpg';
			List_P.Add(P);
		}
		
		Sum = 0;
		for(ProInfo P: List_P)
		{
			Sum += P.Total;
		}
	}
	
	public class ProInfo
	{
		public string PName{get;set;}
		public integer Price{get;set;}
		public integer Count{get;set;}
		public integer Total{get;set;}
		public string Pic{get;set;}
		public string Url{get;set;}
		public ProductInfo__c Pro{get;set;}
	
	}
	
	public PageReference mySubmit()
	{
		if(myUserName==null||myUserName=='')
		{
			Apexpages.Message msg1=new Apexpages.Message(ApexPages.Severity.info,'请填写收货人姓名！');
        	Apexpages.addMessage(msg1);
        	return null;
		}
		if(myPhone==null||myPhone=='')
		{
			Apexpages.Message msg2=new Apexpages.Message(ApexPages.Severity.info,'请填写收货人联系电话！');
        	Apexpages.addMessage(msg2);
        	return null;
		}
		if(myAddress==null||myAddress=='')
		{
			Apexpages.Message msg3=new Apexpages.Message(ApexPages.Severity.info,'请填写收货地址！');
        	Apexpages.addMessage(msg3);
        	return null;
		}
		
		
		list<consuming_record__c> List_Insert = new list<consuming_record__c>();
	
		for(ProInfo P : List_P)
		{
			if(P.PName=='MaryKay玫琳凯舒颜三件套')
			{
				consuming_record__c Cr = new consuming_record__c();
				Cr.product__c = 'a029000000OzVmQAAV';
				Cr.total_amount2__c = 0 ; 	//本次消费金额	
				Cr.transaction_date__c = date.today();
				Cr.account__c = '0019000001BP9XhAAL';
				Cr.Type__c = '积分兑换';
				Cr.channels__c = '玫琳凯官方网站';
				Cr.UserName__c = myUserName;
				Cr.Phone__c = myPhone;
				Cr.Address__c = myAddress;
				Cr.quantity__c = double.valueOf(P.Count);
				List_Insert.Add(Cr);
			}
			else if(P.PName=='MaryKay玫琳凯美白淡斑套装')
			{
				consuming_record__c Cr = new consuming_record__c();
				Cr.product__c = 'a029000000OzVnYAAV';
				Cr.total_amount2__c = 0 ; 	//本次消费金额
				Cr.transaction_date__c = date.today();
				Cr.account__c = '0019000001BP9XhAAL';
				Cr.Type__c = '积分兑换';
				Cr.channels__c = '玫琳凯官方网站';
				Cr.UserName__c = myUserName;
				Cr.Phone__c = myPhone;
				Cr.Address__c = myAddress;
				Cr.quantity__c = double.valueOf(P.Count);
				List_Insert.Add(Cr);
			}
			else if(P.PName=='MaryKay玫琳凯幻时护肤2件套')
			{
				consuming_record__c Cr = new consuming_record__c();
				Cr.product__c = 'a029000000OzViJAAV';
				Cr.total_amount2__c = 0 ; 	//本次消费金额
				Cr.transaction_date__c = date.today();
				Cr.account__c = '0019000001BP9XhAAL';
				Cr.Type__c = '积分兑换';
				Cr.channels__c = '玫琳凯官方网站';
				Cr.UserName__c = myUserName;
				Cr.Phone__c = myPhone;
				Cr.Address__c = myAddress;
				Cr.quantity__c = double.valueOf(P.Count);
				List_Insert.Add(Cr);
			}
			else if(P.PName=='MaryKay玫琳凯防晒套装')
			{
				consuming_record__c Cr = new consuming_record__c();
				Cr.product__c = 'a029000000OzVolAAF';
				Cr.total_amount2__c = 0 ; 	//本次消费金额
				Cr.transaction_date__c = date.today();
				Cr.account__c = '0019000001BP9XhAAL';
				Cr.Type__c = '积分兑换';
				Cr.channels__c = '玫琳凯官方网站';
				Cr.UserName__c = myUserName;
				Cr.Phone__c = myPhone;
				Cr.Address__c = myAddress;
				Cr.quantity__c = double.valueOf(P.Count);
				List_Insert.Add(Cr);
			}
		}
		
		try{
		insert List_Insert;
		}
		catch(exception ex)
		{
			Apexpages.Message msg=new Apexpages.Message(ApexPages.Severity.Error,'积分不足，无法兑换！');
        	Apexpages.addMessage(msg);
        	return null;
		}
	
		PageReference pr = new PageReference('http://isvmaster-146a1ab1ef0-14a615bc0bf.force.com/Success');
	  	pr.setRedirect(true);
	  	return pr;
	}
}