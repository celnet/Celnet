trigger PhoneRegistrationScoreRecord on Account (after insert) 
{
	Id PhoneScore = [select Id from Integral_Rule__c where Name ='手机注册积分赠送'][0].Id;
	list<Integral_Detail__c> list_Insert = new list<Integral_Detail__c>();
	for(Account Acc : trigger.new)
	{
		if(Acc.PhoneRegistrationTime__c!=null)
		{
			Integral_Detail__c IDetail = new Integral_Detail__c();
			IDetail.Account__c =  Acc.Id;
			IDetail.Integral_Rule__c = PhoneScore;
			list_Insert.Add(IDetail);
		}
	}
	
	insert list_Insert;

}