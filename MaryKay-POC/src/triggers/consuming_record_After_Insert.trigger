trigger consuming_record_After_Insert on consuming_record__c (after insert) 
{
    consuming_record__c cr=trigger.new[0];
    Integral_Rule__c In_Rich =null;
    try{ In_Rich = [select id, Rule__c from Integral_Rule__c where Target_Amount__c <:cr.total_amount__c order by Target_Amount__c][0];}catch(exception e){}
    Integral_Rule__c In_Birth =null;
    if(cr.IsAccountBirthMonth__c){
        In_Birth = [select id, Rule__c from Integral_Rule__c where Type__c ='生日'][0];
    }
    Integral_Rule__c In_Other =null;
    date inPutDate = date.valueof(cr.transaction_date__c);
    try{ In_Other = [select id, Rule__c from Integral_Rule__c where (StartDate__c<=:inPutDate and EndDate__c >=:inPutDate) or Type__c ='正常积分' order by Rule__c][0];}catch(exception e){}
    
    Integral_Rule__c In_Max = In_Other;
    
    if(In_Rich !=null && In_Rich.Rule__c>In_Max.Rule__c){
        In_Max=In_Rich;
    }
    if(In_Birth !=null && In_Birth.Rule__c>In_Max.Rule__c){
        In_Max=In_Birth;
    }
    
    Integral_Detail__c ID_ToInsert = new Integral_Detail__c();
    ID_ToInsert.Account__c = cr.account__c;
    ID_ToInsert.Consuming_Record__c = cr.Id;
    ID_ToInsert.Product__c = cr.product__c;
    if(cr.Type__c != '积分兑换'){
        ID_ToInsert.Integral_Rule__c = In_Max.id;
    }
    insert ID_ToInsert;
}