trigger AutoCreateProjectWhenCreateQuote on Quote__c (after insert) {
	for(Quote__c NewQuote : trigger.new){
		//在用户建立Quote的页面中,当用户点击Save,创建quote的时候,同时创建一个Project.
		Project__c InsertProject = new Project__c();
		InsertProject.Name = NewQuote.Name;
		InsertProject.Opportunity__c = NewQuote.Opportunity__c;
		InsertProject.Client__c = NewQuote.Account__c;
		InsertProject.Project_Status__c = 'Not Start';
		InsertProject.Quote__c = NewQuote.Id;
		insert InsertProject;
	}
}