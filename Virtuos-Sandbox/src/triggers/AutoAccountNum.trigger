trigger AutoAccountNum on Account (before insert) {
	Integer TheClientNum;
	Integer TheGroupNum;
	for(Account acc : trigger.new){
		if(acc.RecordTypeId == '01290000000DH8pAAG'){
			//Subsidiary
			string ClientNO = 'C';

			string yearStr = date.today().year() + '';
			ClientNO += yearStr.substring(2);
			string serialNumber;
			if(TheClientNum == null || TheClientNum == 0){
				List<Account> ClientNumList = [select Serial_Number__c, Client_No__c 
													from Account 
													where Client_No__c like : ClientNO + '%'
														and RecordTypeId = '01290000000DH8pAAG'	
														and Serial_Number__c > 0
													ORDER BY Serial_Number__c DESC
													limit 1];
				if(ClientNumList == null || ClientNumList.size() == 0){
					TheClientNum = 10;
				}
				else{
					Account account = ClientNumList[0];
					double MaxClientNo = account.Serial_Number__c;
					if(MaxClientNo != null && MaxClientNo > 0){
						TheClientNum = MaxClientNo.intValue();
					}
					else
					{
						TheClientNum = 10;
					}
				}
			}
			
			TheClientNum ++;
			
			serialNumber = TheClientNum + '';
			if(serialNumber.length() == 1){
				serialNumber = '00' + serialNumber;
			}
			else if(serialNumber.length() == 2){
				serialNumber = '0' + serialNumber;
			}
			//将生成的序列号赋值回去
			acc.Serial_Number__c = TheClientNum;
			acc.Client_No__c = ClientNO + serialNumber;
		}
		else if(acc.RecordTypeId == '01290000000DH8kAAG'){
			//Parent Account
			string GroupNO = 'G';
			
			string yearStr = date.today().year() + '';
			GroupNO += yearStr.substring(2);
			string serialNumber;
			if(TheGroupNum == null || TheGroupNum == 0){
				List<Account> GroupNumList = [select Serial_Number__c, Group_No__c 
													from Account 
													where Group_No__c like : GroupNO + '%'
														and RecordTypeId = '01290000000DH8kAAG'	
														and Serial_Number__c > 0
													ORDER BY Serial_Number__c DESC
													limit 1];
				if(GroupNumList == null || GroupNumList.size() == 0){
					TheGroupNum = 10;
				}
				else{
					Account account = GroupNumList[0];
					double MaxGroupNo = account.Serial_Number__c;
					if(MaxGroupNo != null && MaxGroupNo > 0){
						TheGroupNum = MaxGroupNo.intValue();
					}
					else
					{
						TheGroupNum = 10;
					}
				}
			}
			
			TheGroupNum ++;
			
			serialNumber = TheGroupNum + '';
			if(serialNumber.length() == 1){
				serialNumber = '00' + serialNumber;
			}
			else if(serialNumber.length() == 2){
				serialNumber = '0' + serialNumber;
			}
			//将生成的序列号赋值回去
			acc.Serial_Number__c = TheGroupNum;
			acc.Group_No__c = GroupNO + serialNumber;	
		}
	}
}