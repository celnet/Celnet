trigger AutoProjectNum on Project__c (before insert, before update) {
	Integer TheNum;
//规则：C0738P09009 C指客户 07是客户在系统里边创建时间 38指第38个当年在系统里边创建的客户  P指项目 09指09年 09指当年顺序号
	for(Project__c project : trigger.new){
		if(project.Client__c == null){
			return;
		}
		if(project.Project_No__c != null){
			return;
		}
		
		string ClentNO = project.Client_No__c + 'P';
		
		datetime createdate;
		if(trigger.isInsert){
			createdate = datetime.now();
		}
		else
		{
			createdate = project.CreatedDate;
		}
		Integer createYear = createdate.year();
		string yearStr = createYear + '';
		ClentNO += yearStr.substring(2);
		string serialNumber;
		if(TheNum == null || TheNum == 0){
			List<Project__c> ProjectNumList = [select Serial_Number__c, Project_No__c 
												from Project__c 
												where Client__c =: project.Client__c 
													and id != :project.Id
													and Project_No__c like : ClentNO + '%'
													and Serial_Number__c > 0
												ORDER BY Serial_Number__c DESC
												limit 1];
			//如果是客户的第一条project则用默认编号,否则加1
			if(ProjectNumList == null || ProjectNumList.size() == 0){
				TheNum = 0;
			}
			else{
				Project__c MaxProject = ProjectNumList[0];
				double ProjectMaxNo = MaxProject.Serial_Number__c;
				if(ProjectMaxNo != null && ProjectMaxNo > 0){
					TheNum = ProjectMaxNo.intValue();
				}
				else
				{
					TheNum = 0;
				}
			}
		}
		
		TheNum ++;
		
		serialNumber = TheNum + '';
		if(serialNumber.length() == 1){
			serialNumber = '00' + serialNumber;
		}
		else if(serialNumber.length() == 2){
			serialNumber = '0' + serialNumber;
		}
		//将生成的序列号赋值回去
		project.Serial_Number__c = TheNum;
		project.Project_No__c = ClentNO + serialNumber;		
	}
}