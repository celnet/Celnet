trigger SumPoProjectMilestoneComRateByDeliverable on PO_Deliverable__c (before delete, after insert, after undelete, 
before update) {
	//if(trigger.isInsert || trigger.isDelete || trigger.isUnDelete){
		//Milestone ID List
		List<PO_Project_Milestone__c> OldMilestoneList = new List<PO_Project_Milestone__c>();
		
		//取得原有Deliverable
		List<PO_Deliverable__c> OldDeliverable = new List<PO_Deliverable__c>();
		
		//现有Deliverable
		List<PO_Deliverable__c> NewDeliverable = new List<PO_Deliverable__c>();
		
		//删除的Deliverable
		List<PO_Deliverable__c> SubDeliverable = new List<PO_Deliverable__c>();

		//取得待更新的Milestone的ID列表
		if(trigger.isInsert || trigger.isUnDelete){
			for(PO_Deliverable__c deliverable : trigger.new){
				if(deliverable.PO_Project_Milestone__c != null){
					PO_Project_Milestone__c Milestone = new PO_Project_Milestone__c(id = deliverable.PO_Project_Milestone__c );
					OldMilestoneList.add(Milestone);
				}
			}
		}
		if(trigger.isDelete){
			for(PO_Deliverable__c deliverable : trigger.old){
				if(deliverable.PO_Project_Milestone__c != null){
					PO_Project_Milestone__c Milestone = new PO_Project_Milestone__c(id = deliverable.PO_Project_Milestone__c );
					OldMilestoneList.add(Milestone);
					SubDeliverable.add(deliverable);
				}
			}
		}

		if(trigger.isUpdate){
			for(PO_Deliverable__c deliverable : trigger.new){
				if(deliverable.PO_Project_Milestone__c != null){
					PO_Project_Milestone__c Milestone = new PO_Project_Milestone__c(id = deliverable.PO_Project_Milestone__c );
					OldMilestoneList.add(Milestone);
					NewDeliverable.add(deliverable);
				}
			}
		}
		if(trigger.isUpdate){
			for(PO_Deliverable__c deliverable : trigger.old){
				if(deliverable.PO_Project_Milestone__c != null){
					PO_Project_Milestone__c Milestone = new PO_Project_Milestone__c(id = deliverable.PO_Project_Milestone__c );
					OldMilestoneList.add(Milestone);
					SubDeliverable.add(deliverable);
				}
			}
		}
		
		OldDeliverable = [select Real_Man_Days__c, Total_Man_Days__c, PO_Project_Milestone__c 
													from PO_Deliverable__c 
													where PO_Project_Milestone__c in : OldMilestoneList];
		
		//如果是新建或解除删除,则合并两个Deliverable列表										
		if(NewDeliverable.size() > 0){
			OldDeliverable.addAll(NewDeliverable);
		}
		
		//加和Milestone的人天
		Map<ID,PO_Project_Milestone__c> MilestoneMapManday = new Map<ID,PO_Project_Milestone__c>();
		
		for(PO_Deliverable__c deliverable : OldDeliverable){
			string MilestoneID = deliverable.PO_Project_Milestone__c;
			if(MilestoneMapManday.containsKey(MilestoneID)){
				//如果Map里已经包含,则更新人天
				PO_Project_Milestone__c milestone = MilestoneMapManday.get(MilestoneID);
				if(deliverable.Real_Man_Days__c != null){
					milestone.Real_Man_Days__c = milestone.Real_Man_Days__c + deliverable.Real_Man_Days__c;
				}
				if(deliverable.Total_Man_Days__c != null){
					milestone.Total_Man_Days__c = milestone.Total_Man_Days__c + deliverable.Total_Man_Days__c;
				}
				MilestoneMapManday.put(MilestoneID, milestone);
			}
			else
			{
				PO_Project_Milestone__c milestone = new PO_Project_Milestone__c(id = MilestoneID);
				if(deliverable.Real_Man_Days__c != null){
					milestone.Real_Man_Days__c = deliverable.Real_Man_Days__c;
				}
				else
				{
					milestone.Real_Man_Days__c = 0;
				}
				if(deliverable.Total_Man_Days__c != null){
					milestone.Total_Man_Days__c = deliverable.Total_Man_Days__c;
				}
				else
				{
					milestone.Total_Man_Days__c = 0;
				}
				MilestoneMapManday.put(MilestoneID, milestone);
			}
		}
		if(SubDeliverable.size() > 0){
			for(PO_Deliverable__c deliverable : SubDeliverable){
				string MilestoneID = deliverable.PO_Project_Milestone__c;
				if(MilestoneMapManday.containsKey(MilestoneID)){
					//如果Map里已经包含,则更新人天
					PO_Project_Milestone__c milestone = MilestoneMapManday.get(MilestoneID);
					if(deliverable.Real_Man_Days__c != null){
						milestone.Real_Man_Days__c = milestone.Real_Man_Days__c - deliverable.Real_Man_Days__c;
					}
					if(deliverable.Total_Man_Days__c != null){
						milestone.Total_Man_Days__c = milestone.Total_Man_Days__c - deliverable.Total_Man_Days__c;
					}
					MilestoneMapManday.put(MilestoneID, milestone);
				}
			}
		}
		//更新Milestone的完成率
		List<PO_Project_Milestone__c> UpMilestoneList = MilestoneMapManday.values();
		update UpMilestoneList;
	//}
	
}