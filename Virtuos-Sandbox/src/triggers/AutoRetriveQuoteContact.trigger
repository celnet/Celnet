trigger AutoRetriveQuoteContact on Quote__c (before insert) {
	/*
		业务规则为一个account下只有一个Contact_Type为Sales Contact的联系人
	*/
	Set<string> AccNameSet =  new Set<string>();
	
	for(Quote__c ConQuote : trigger.new){
		if(ConQuote.Contact__c != null){
			return;
		}
		else if(ConQuote.Account__c != null){
			AccNameSet.add(ConQuote.Account__c);
		}
	}
	List<Contact> ContactList = [select ID, Name, AccountId, Contact_Type__c from Contact where AccountId in: AccNameSet and Contact_Type__c = 'Sales Contact'];
	
	for(Quote__c ConQuote : trigger.new){
		string AccID = ConQuote.Account__c;
		if(AccID != null){
			for(Contact con : ContactList){
				if(con.AccountId == AccID){
					ConQuote.Contact__c = con.ID;
				}
			}
		}
	}	
}