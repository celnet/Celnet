trigger AutoPaymentMilestoneNo on Invoice__c (before insert) {
	Integer TheNum;
	//编码规则: [PO对应Project编码]PA[##] (##从01开始)
	//业务逻辑上po一定对应一条po project,po project一定有编码,并且po不会被修改,paymentmilestone不会被删除
	//所以代码按正常逻辑处理,如果出现没有po,或po没有po project,po project没有编码的情况
	//trigger错误提示,Payment Milestone No处理为:'zzzzzzzzzzzPA[##]'
	for(Invoice__c invoice : trigger.new){
		string poStr = invoice.PO__c;
		string ProjectNum = 'zzzzzzzzzzz';
		if(poStr == null){
			throw new applicationException('You must enter a PO value.');
		}
		if(poStr != null){
			//PO__c与PO_Project__c是一对一的关系,所以project编码取第一条就可以
			List<PO_Project__c> PoProjectList = [select PO_Project_No__c from PO_Project__c where PO__c =: poStr ];
			if(PoProjectList == null || PoProjectList.size() == 0){
				throw new applicationException('Must have a PO Project related to PO.');
			}
			if(PoProjectList.size()>0){
				PO_Project__c PoProject = PoProjectList[0];
				if(PoProject.PO_Project_No__c == null){
					throw new applicationException('The PO Project must have a PO Project No.');
				}
				ProjectNum = PoProject.PO_Project_No__c;
			}
		}
		
		string serialNumber;
		if(TheNum == null || TheNum == 0){
			//如果是第一条则从系统中取序列号
			List<Invoice__c> InvoiceList = [select Payment_Milestone_No__c from Invoice__c where PO__c =:poStr and id !=:invoice.Id ORDER BY Payment_Milestone_No__c DESC];
						
			if(InvoiceList == null || InvoiceList.size()==0){
				TheNum = 0;
			}
			else
			{
				Invoice__c InvoiceNum = InvoiceList[0];
				string mileStoneNo = InvoiceNum.Payment_Milestone_No__c;
				if(mileStoneNo!=null && mileStoneNo.length()>0){
					mileStoneNo = mileStoneNo.substring(mileStoneNo.length() -2);
					if(mileStoneNo>='0' && mileStoneNo<='99'){
						TheNum = Integer.valueOf(mileStoneNo);					
					}
					else
					{
						throw new applicationException('You payment milestone no is abnormal.');
					}
				}
				else
				{
					TheNum = 0;
				}
			}
		}
		
		TheNum ++;
		
		serialNumber = TheNum + '';
		if(serialNumber.length() == 1){
			serialNumber = '0' + serialNumber;
		}
		//将生成的序列号赋值回去
		System.debug('*****************************************Payment_Milestone_No__c' + ProjectNum + 'PA' + serialNumber);
		invoice.Payment_Milestone_No__c = ProjectNum + 'PA' + serialNumber;
		
	}
}