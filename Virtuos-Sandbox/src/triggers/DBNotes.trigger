trigger DBNotes on DB_Notes__c (before insert, before update) 
{
	//根据Project No 设置到Project的关系并设置到业务机会的关系，本功能为ProjectForecastReport服务
	set<String> prjNoSet = new set<String>(); 
	Integer year = 0;
	for(DB_Notes__c dN : trigger.new)
	{
		prjNoSet.add(dN.Project_NO__c);
		if(dN.Date__c != null && dN.Date__c.year() > year)
		{
			year = dN.Date__c.year();
		}
	}
	if(year == 0)
	{
		year = system.now().year();
	}
	map<Integer, map<Integer, MonthlyExchangeRate__c>> rateMap = ExchangeRateHelper.GetExchangeRate(year);//获取历史汇率表
	map<String, Project__c> prgMap = new map<String, Project__c>();
	for(Project__c prj : [Select Project_NO__c,CurrencyIsoCode, ID, Opportunity__c, Opportunity__r.CurrencyIsoCode From Project__c Where Project_NO__c In:prjNoSet])
	{
		prgMap.put(prj.Project_NO__c, prj);
	}
	system.debug('####1');
	system.debug(prgMap.size());
	system.debug(prgMap);
	for(DB_Notes__c dN : trigger.new)
	{
		//强制Debit Notes日期为Date月份的最后一天
		Date firDay = dN.Date__c.toStartOfMonth();
		//System.Debug(firDay);
		dN.Date__c = firDay.addMonths(1).addDays(-1);
		//System.Debug(endDay);
		
		Project__c prj = prgMap.get(dN.Project_NO__c);
		String sCode = dN.CurrencyIsoCode;//原始货币类型
		if(prj != null)
		{
			dN.Project__c = prj.ID;
			dN.Opportunity__c = prj.Opportunity__c;
		}
		else
		{
			dN.addError('The project of this Debit Notes is not exsit, Project NO:' + dN.Project_NO__c);
			system.debug('####2 The project of this Debit Notes is not exsit, Project NO:' + dN.Project_NO__c);
			continue;
		}
		if(prj!= null && prj.Opportunity__r != null && prj.Opportunity__r.CurrencyIsoCode != null)
		{ 
			dN.CurrencyIsoCode = prj.Opportunity__r.CurrencyIsoCode;
		}
		else if(prj!= null && prj.CurrencyIsoCode != null)
		{
			dN.CurrencyIsoCode = prj.CurrencyIsoCode;
		}
		
		
		//根据月份的历史汇率计算等价的USDAmount，并按业务机会的货币类型转换金额
		if(dN.Amount__c != null)
		{
			if(sCode != null )
			{
				ExchangeRateItem__c sRateItem = ExchangeRateHelper.matchExchangeRateItem(dN.Date__c.Year(), dN.Date__c.Month(), sCode, rateMap);
				if(sRateItem != null)
				{
					dN.USDAmount__c = dN.Amount__c/sRateItem.ConversionRate__c;
				}
				if(dN.CurrencyIsoCode != null)
				{
					ExchangeRateItem__c cRateItem = ExchangeRateHelper.matchExchangeRateItem(dN.Date__c.Year(), dN.Date__c.Month(), dN.CurrencyIsoCode, rateMap);
					if(sRateItem != null && cRateItem != null)
					{
						dN.Amount__c = (dN.Amount__c/sRateItem.ConversionRate__c)*cRateItem.ConversionRate__c;
					}
				}	
			}
			else 
			{
				if(dN.CurrencyIsoCode != null)
				{
					ExchangeRateItem__c rateItem = ExchangeRateHelper.matchExchangeRateItem(dN.Date__c.Year(), dN.Date__c.Month(), dN.CurrencyIsoCode, rateMap);
					if(rateItem != null)
					{
						dN.USDAmount__c = dN.Amount__c/rateItem.ConversionRate__c;
					}
				}
				
			}
		}
		
	}
	/*
	private Decimal exchangeUSD(Decimal oriAmount, Integer year, Integer month, String currencyCode, map<Integer, map<Integer, MonthlyExchangeRate__c>> rateMap)
	{
		
		ExchangeRateItem__c rateItem = ForecastRow.matchExchangeRateItem(year, month, currencyCode, rateMap);
		if(oriAmount != null && rateItem.ConversionRate__c != null)
		{
			return oriAmount/rateItem.ConversionRate__c;
		}
		return null;
	}
	*/
	
}