//作者：杜全勇
//协助：Tommy
//描述：
trigger AutoAmountSum on Invoice__c (after delete, after insert, after undelete, after update) 
{	  
	   //得到invID集合
	   Set<ID> poIds = new Set<ID>();
	   if(trigger.isInsert)
	   {
		   for(Invoice__c inv :trigger.new)
		   {
		   		if(inv.PO__c != null)
		   		{
		   			poIds.add(inv.PO__c);
		   		}
		   }
	   }
	   if(trigger.isdelete)
	   {
	   		for(Invoice__c inv :trigger.old)
		   {
		   		if(inv.PO__c != null)
		   		{
		   			poIds.add(inv.PO__c);
		   		}
		   }
	   }
	   if(trigger.isUpdate)
	   {
	   		for(Invoice__c newInv :trigger.new)
		   {
		   		Invoice__c oldInv = trigger.oldMap.get(newInv.Id);
		   		if(newInv.PO__c != oldInv.PO__c)
		   		{
		   			if(newInv.PO__c != null)
		   			{
		   				poIds.add(newInv.PO__c);
		   			}
		   			if(oldInv.PO__c != null)
		   			{
		   				poIds.add(oldInv.PO__c);
		   			}
		   		}
		   		else if(newInv.Amount__c != oldInv.Amount__c || newInv.Received_Amount__c != oldInv.Received_Amount__c || newInv.Invoice_Due_Amount__c != oldInv.Invoice_Due_Amount__c  || newInv.flag__c != oldInv.flag__c)
		   		{
		   			if(newInv.PO__c != null)
		   			{
		   				poIds.add(newInv.PO__c);
		   			}
		   		}
		   }
	   }
	   if(trigger.isUnDelete)
	   {
	   		for(Invoice__c inv :trigger.new)
		   {
		   		if(inv.PO__c != null)
		   		{
		   			poIds.add(inv.PO__c);
		   		}
		   }
	   } 
	   ////////////////////////////////////////////////////////////////
	   Map<Id, PO__c> poMap = new Map<Id, PO__c>();
		for(Id poId : poIds)
		{
			PO__c po = new PO__c(Id = poId);
			//初始化为0
			po.Invoice_Amount_Sum__c = 0;
			po.Invoice_Received_Amount_Sum__c = 0;
			po.Invoice_Due_Amount_Sum__c = 0;
			poMap.put(poId, po);
		}
	
	  // List<PO__c> poList = new List<PO__c>();
	   /*List<AggregateResult> aggInvList = [select Sum(Amount__c) sumAmount, Sum(Received_Amount__c) sumReceived, Sum(Invoice_Due_Amount__c) sumDue, PO__c
	   											from Invoice__c 
	   											where PO__c In:poIds 
	   											GROUP BY PO__c];*/
	   											
	   List<Invoice__c>	invList = [select PO__c, Amount__c, Received_Amount__c, Invoice_Due_Amount__c
	   											from Invoice__c 
	   											where PO__c In:poIds];
	   for(Invoice__c inv :invList)
	   {
	   		if(!poMap.containsKey(inv.PO__c))
			{
				continue;
			}
			PO__c po = poMap.get(inv.PO__c);
			if(inv.Amount__c != null){
				po.Invoice_Amount_Sum__c += inv.Amount__c;
			}
	   		if(inv.Received_Amount__c != null){
	   			po.Invoice_Received_Amount_Sum__c += inv.Received_Amount__c;
	   		}
	   		if(inv.Invoice_Due_Amount__c != null){ 
	   			po.Invoice_Due_Amount_Sum__c +=inv.Invoice_Due_Amount__c;
	   		}
	   		
	   		System.debug('@@@@@@@@@@@@@@@@@@@@@@@@Invoice_Amount_Sum__c' + po.Invoice_Amount_Sum__c);
	   		System.debug('@@@@@@@@@@@@@@@@@@@@@@@@@Invoice_Due_Amount_Sum__c' + po.Invoice_Due_Amount_Sum__c);
	   		System.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@Invoice_Received_Amount_Sum__c' + po.Invoice_Received_Amount_Sum__c);
	   		//System.debug('****************CurrencyIsoCode' + inv.CurrencyIsoCode);
	   }
	   		
	 /*  for(AggregateResult aggObj : aggInvList) 
	   {
	   		ID poId = (Id)aggObj.get('PO__c');
	   		PO__c po = new PO__c(Id = poId);
	   		po.Invoice_Amount_Sum__c = (Decimal)aggObj.get('sumAmount') ;
	   		System.debug('@@@@@@@@@@@@sumAmount'+(Decimal)aggObj.get('sumAmount'));
	   		
	   		po.Invoice_Received_Amount_Sum__c = (Decimal)aggObj.get('sumReceived') ;
	   		System.debug('@@@@@@@@@@@@@@@@@sumReceived'+(Decimal)aggObj.get('sumReceived'));
	   		
	   		po.Invoice_Due_Amount_Sum__c = (Decimal)aggObj.get('sumDue') ;
	   		System.debug('@@@@@@@@@@@@@@@sumDue'+(Decimal)aggObj.get('sumDue'));
	   		poList.add(po);
	   		
	   }*/
	   update poMap.values();  
}