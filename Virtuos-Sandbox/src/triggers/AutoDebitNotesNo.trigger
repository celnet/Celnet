trigger AutoDebitNotesNo on Debit_Notes__c (before insert) 
{
	//Debit Notes No 编码规则：DN+月份+年份+序列号（每个月从11开始） 格式：DN{MM}{YY}{011}
	Date debDay = null;
	String monthNo = null;
	Integer maxSerial = 10;//如果找不到则本月内还没有记录创建,序列号从11开始
	for(Debit_Notes__c deb : trigger.new)
	{
		if(debDay == null)//第一个插入的记录
		{
			debDay = deb.Date__c;
			String sDate = String.valueOf(debDay);
			String[] partList = sDate.split('-', 0);
			String mNo = partList[1];
			String yNo = partList[0].substring(2);
			monthNo = mNo + yNo;
			List<Debit_Notes__c> maxDebList = [Select Serial_No__c From Debit_Notes__c Where Month_No__c = : monthNo Order By Serial_No__c Desc limit 1];
			if(maxDebList.size() > 0)//如果找到则本月内有记录创建,序列号从最大的开始
			{
				if(maxDebList[0].Serial_No__c != null)
				{
					maxSerial = maxDebList[0].Serial_No__c.intValue();
				}				
			}
		}
		//要保证批量插入的deb必须在同一个月份.
		if(deb.Date__c.year() != debDay.year() || deb.Date__c.month() != debDay.month())
		{
			deb.Date__c.addError('Bulk inserted debit notes must be with same month Date__c');
		}
		deb.Month_No__c = monthNo;
		maxSerial ++;
		deb.Serial_No__c = maxSerial;
		String ser = String.valueOf(maxSerial);
		Integer length = ser.length();
		for(Integer i = 1; i<= 3 - length; i ++)//用于在少于3位数,在前面补'0'
		{
			ser = '0' + ser;
		}
		deb.Name = 'DN' + monthNo + ser;
	}
}