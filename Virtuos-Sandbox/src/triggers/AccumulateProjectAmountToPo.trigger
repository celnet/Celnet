//统计po project的金额(Amount)到po的project amount字段. 
//开发人：Patrick
trigger AccumulateProjectAmountToPo on PO_Project__c (after delete, after insert, after undelete, 
after update) {
	
	Set<ID> PoIdSet = new Set<ID>();
	if(trigger.isInsert || trigger.isUnDelete)
	{
		for(PO_Project__c project : trigger.new)
		{
			if(project.PO__c != null)
			{
				PoIdSet.add(project.PO__c);
			}
		}
	}
	else if(trigger.isDelete)
	{
		for(PO_Project__c project : trigger.old)
		{
			if(project.PO__c != null)
			{
				PoIdSet.add(project.PO__c);
			}
		}
	}
	else if(trigger.isUpdate)
	{
        Set<ID> newIdSet = trigger.newMap.keySet();
        for(ID newId : newIdSet)
        {
            PO_Project__c newProject = trigger.newMap.get(newId);
            PO_Project__c oldProject = trigger.oldMap.get(newId);
            if(newProject.PO__c != oldProject.PO__c)
            {
            	if(newProject.PO__c != null)
            	{
					PoIdSet.add(newProject.PO__c);
            	}
            	if(oldProject.PO__c != null)
            	{
					PoIdSet.add(oldProject.PO__c);
            	}
            }
            else if(newProject.Debit_Notes_Amount__c != oldProject.Debit_Notes_Amount__c)
            {
            	if(newProject.PO__c != null)
            	{
            		PoIdSet.add(newProject.PO__c);
            	}
            }
        }
	}

	if(PoIdSet.size() == 0)
	{
		return;
	}
	
	Map<Id, PO__c> poMap = new Map<Id, PO__c>();
	for(Id poId : PoIdSet)
	{
		PO__c po = new PO__c(Id = poId);
		//初始化为0
		po.Project_Amount__c = 0;
		poMap.put(poId, po);
	}
	
	for(PO_Project__c project : [Select PO__c, Debit_Notes_Amount__c
												From PO_Project__c 
												Where PO__c in : PoIdSet
												])
	{
	   	if(!poMap.containsKey(project.PO__c))
		{
			continue;
		}
		PO__c po = poMap.get(project.PO__c);
		if(project.Debit_Notes_Amount__c != null){
			po.Project_Amount__c += project.Debit_Notes_Amount__c;
		}
	}
	update poMap.values();
}