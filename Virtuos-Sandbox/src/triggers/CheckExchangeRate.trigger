//验证新建的记录是否已存在系统中
//开发人:米立业
trigger CheckExchangeRate on MonthlyExchangeRate__c (after undelete, before insert, before update) {
Set<string> OwnerSet =  new Set<string>();
	
	for(MonthlyExchangeRate__c monthRate : trigger.new){
		OwnerSet.add(monthRate.OwnerId);				
	}
	List<MonthlyExchangeRate__c> exchangeRateList = [select Id, Year__c, Month__c ,OwnerId 
													from MonthlyExchangeRate__c 
													where OwnerId in:OwnerSet];
	if(trigger.isInsert||trigger.isUnDelete){
		for(MonthlyExchangeRate__c exchangeRate : trigger.new){
			Decimal year = exchangeRate.Year__c;
			String month = exchangeRate.Month__c;
			String ownerId = exchangeRate.OwnerId;
			for(MonthlyExchangeRate__c monthRate:exchangeRateList){
				if(monthRate.Year__c == year&& monthRate.Month__c==month){
					exchangeRate.addError(year.intValue()+'年'+month+'月的记录已经存在');
				}
			}
		}
	}
	
	if(trigger.isUpdate){
		for(MonthlyExchangeRate__c exchangeRate : trigger.new){
			String Id = exchangeRate.Id;
			Decimal year = exchangeRate.Year__c;
			String month = exchangeRate.Month__c;
			String ownerId = exchangeRate.OwnerId;
			for(MonthlyExchangeRate__c monthRate:exchangeRateList){
				if(monthRate.Id!=Id&&monthRate.Year__c == year&& monthRate.Month__c==month&&monthRate.OwnerId==ownerId){
					exchangeRate.addError(year.intValue()+'年'+month+'月的记录已经存在');
				}
			}
		}
	}	
}