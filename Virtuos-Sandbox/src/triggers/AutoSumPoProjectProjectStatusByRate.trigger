trigger AutoSumPoProjectProjectStatusByRate on PO_Project__c (before insert, before update) {
	//项目完成率为100%的时候,需要标识PO Project的Project Status字段为"Complete"
	for(PO_Project__c PoProject : trigger.new){
		if(PoProject.Completion_Rate__c != null)
		{
			if(PoProject.Completion_Rate__c == 100){
				PoProject.Project_Status__c = 'Complete';
			}
		}
	}
}