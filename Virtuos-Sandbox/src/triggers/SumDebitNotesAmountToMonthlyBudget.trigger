trigger SumDebitNotesAmountToMonthlyBudget on Debit_Notes__c (after insert, after update) {
	/*
		Debit Notes创建更新时都要计算
		Debit Notes的Amount__c汇总到Monthly Budget 的Monthly Finish字段
		汇总依据是此客户下的Debit Note,日期为createdate,Monthly Budget 有一个月份的下拉列表
	*/
	for(Debit_Notes__c DebitNote : trigger.new){
		double Amount = 0;
		string AccID = DebitNote.Client__c;
		string MonthNo = DebitNote.Month_No__c;
		if(AccID == null){
			return;
		}
		if(MonthNo == null){
			return;
		}
		List<AggregateResult> AmountList = [select Sum(Amount__c) from Debit_Notes__c where Client__c =:AccID and Month_No__c =: MonthNo GROUP BY Month_No__c];	
		for (AggregateResult ar : AmountList)  {
			Amount = double.valueOf(ar.get('expr0'));
		}
				
		string Month = MonthNo.substring(0,2);
		string Year = '20' + MonthNo.substring(2);
		
		string MonthStr = '';
		if(Month == '01')
		{ MonthStr = 'Jan.'; }
		else if(Month == '02')
		{ MonthStr = 'Feb.'; }
		else if(Month == '03')
		{ MonthStr = 'Mar.'; }
		else if(Month == '04')
		{ MonthStr = 'Apr.'; }
		else if(Month == '05')
		{ MonthStr = 'May.'; }
		else if(Month == '06')
		{ MonthStr = 'Jun.'; }
		else if(Month == '07')
		{ MonthStr = 'Jul.'; }
		else if(Month == '08')
		{ MonthStr = 'Aug.'; }
		else if(Month == '09')
		{ MonthStr = 'Sep.'; }
		else if(Month == '10')
		{ MonthStr = 'Oct.'; }
		else if(Month == '11')
		{ MonthStr = 'Nov.'; }
		else if(Month == '12')
		{ MonthStr = 'Dec.'; }


		List<Annual_Budget__c> AnnBudgetList = [select id from Annual_Budget__c where Account__c =: AccID and Year__c =: Year];
		if(AnnBudgetList != null && AnnBudgetList.size()>0){
			string AnnbudgetID = AnnBudgetList[0].Id;
			List<Monthly_Budget__c> MonthBudgetList = [select id from Monthly_Budget__c where Annual_Budget__c =:AnnbudgetID and Month__c =: MonthStr ];
			if(MonthBudgetList != null && MonthBudgetList.size()>0){
				Monthly_Budget__c updateMonthBudget = MonthBudgetList[0];
				updateMonthBudget.Monthly_Finish__c = Amount;
				update updateMonthBudget;
			}
		}
	}
	//更新时如果客户或月份更新,则要重新计算更新前的客户或月份
	if(trigger.isUpdate){
		for(Integer i=0;i<trigger.new.size(); i++){
			Debit_Notes__c NewDebit = trigger.new[i];
			Debit_Notes__c OldDebit = trigger.old[i];
			string NewAccID = NewDebit.Client__c;
			string NewMonthNo = NewDebit.Month_No__c;
			string OldAccID = OldDebit.Client__c;
			string OldMonthNo = OldDebit.Month_No__c;
			if(NewAccID != OldAccID || NewMonthNo != OldMonthNo){
				double Amount = 0;
				string AccID = OldAccID;
				string MonthNo = OldMonthNo;
				if(AccID == null){
					return;
				}
				if(MonthNo == null){
					return;
				}
				List<AggregateResult> AmountList = [select Sum(Amount__c) from Debit_Notes__c where Client__c =:AccID and Month_No__c =: MonthNo GROUP BY Month_No__c];	
				for (AggregateResult ar : AmountList)  {
					Amount = double.valueOf(ar.get('expr0'));
				}
						
				string Month = MonthNo.substring(0,2);
				string Year = '20' + MonthNo.substring(2);
				string MonthStr = '';
				if(Month == '01')
				{ MonthStr = 'Jan.'; }
				else if(Month == '02')
				{ MonthStr = 'Feb.'; }
				else if(Month == '03')
				{ MonthStr = 'Mar.'; }
				else if(Month == '04')
				{ MonthStr = 'Apr.'; }
				else if(Month == '05')
				{ MonthStr = 'May.'; }
				else if(Month == '06')
				{ MonthStr = 'Jun.'; }
				else if(Month == '07')
				{ MonthStr = 'Jul.'; }
				else if(Month == '08')
				{ MonthStr = 'Aug.'; }
				else if(Month == '09')
				{ MonthStr = 'Sep.'; }
				else if(Month == '10')
				{ MonthStr = 'Oct.'; }
				else if(Month == '11')
				{ MonthStr = 'Nov.'; }
				else if(Month == '12')
				{ MonthStr = 'Dec.'; }
		
		
				List<Annual_Budget__c> AnnBudgetList = [select id from Annual_Budget__c where Account__c =: AccID and Year__c =: Year];
				if(AnnBudgetList != null && AnnBudgetList.size()>0){
					string AnnbudgetID = AnnBudgetList[0].Id;
					List<Monthly_Budget__c> MonthBudgetList = [select id from Monthly_Budget__c where Annual_Budget__c =:AnnbudgetID and Month__c =: MonthStr ];
					if(MonthBudgetList != null && MonthBudgetList.size()>0){
						Monthly_Budget__c updateMonthBudget = MonthBudgetList[0];
						updateMonthBudget.Monthly_Finish__c = Amount;
						update updateMonthBudget;
					}
				}
			}
		}
	}
}