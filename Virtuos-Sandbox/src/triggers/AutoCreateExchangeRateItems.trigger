//新建月汇率时,取出系统中的静态汇率自动添加到新建月汇率信息下的汇率信息项中
//owner:米立业
trigger AutoCreateExchangeRateItems on MonthlyExchangeRate__c (after insert) {	
	if(trigger.isInsert){				
		//取出系统中的静态汇率
		List<CurrencyType> listType = [SELECT ConversionRate, IsCorporate, IsoCode, Id FROM CurrencyType Where IsActive = true];
		//新的自定义汇率
		List<ExchangeRateItem__c> rateItemList = new List<ExchangeRateItem__c>();
		for(MonthlyExchangeRate__c monthRate : trigger.new){
			for(CurrencyType cType : listType){
				ExchangeRateItem__c rateItem = new ExchangeRateItem__c();
				rateItem.Corporate__c = cType.IsCorporate;
				rateItem.CurrencyIsoCode = cType.IsoCode;
				rateItem.ConversionRate__c = cType.ConversionRate;
				rateItem.MonthlyExchangeRate__c = monthRate.Id;
				rateItemList.add(rateItem);
			} 
		}
		insert(rateItemList);
	}
}