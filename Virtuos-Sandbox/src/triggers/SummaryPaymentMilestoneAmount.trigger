trigger SummaryPaymentMilestoneAmount on PO_Deliverable__c(after delete, after insert, after undelete, after update) 
{
    Set<ID> paymantMLIdSet = new Set<ID>();//用来存储需要汇总计算总价的panymant milestone Id 
    //Invoice__c
    if(trigger.isAfter)
    {
        if(trigger.isInsert)
        {
            for(PO_Deliverable__c delver : trigger.new)
            {
                if(delver.Payment_Milestone__c != null)
                {
                    if(!paymantMLIdSet.contains(delver.Payment_Milestone__c))
                    {
                        paymantMLIdSet.add(delver.Payment_Milestone__c);
                    }
                }
            }
        }
        else if(trigger.isUpdate)
        {
            Set<ID> newIdSet = trigger.newMap.keySet();
            for(ID newId : newIdSet)
            {
                PO_Deliverable__c newDelver = trigger.newMap.get(newId);
                PO_Deliverable__c oldDelver = trigger.oldMap.get(newId);
                if(oldDelver.Payment_Milestone__c != newDelver.Payment_Milestone__c)//测试是否父ID有变化
                {
                    if(!paymantMLIdSet.contains(oldDelver.Payment_Milestone__c))
                    {
                        paymantMLIdSet.add(oldDelver.Payment_Milestone__c);//新旧都需要重新计算
                    }
                    if(!paymantMLIdSet.contains(newDelver.Payment_Milestone__c))
                    {
                        paymantMLIdSet.add(newDelver.Payment_Milestone__c);//新旧都需要重新计算
                    }
                }
                else
                {
                    if(oldDelver.Asset_Price__c != newDelver.Asset_Price__c)//测试总价是否有变化
                    {
                        if(!paymantMLIdSet.contains(newDelver.Payment_Milestone__c))
                        {
                            paymantMLIdSet.add(newDelver.Payment_Milestone__c);
                        }
                    }
                }
            }
        }
        else if(trigger.isDelete)
        {
            for(PO_Deliverable__c delver : trigger.old)
            {
                if(delver.Payment_Milestone__c != null)
                {
                    if(!paymantMLIdSet.contains(delver.Payment_Milestone__c))
                    {
                        paymantMLIdSet.add(delver.Payment_Milestone__c);
                    }
                }
            }
        }
    }
     
    //计算集合中的Payment milestone 的Amout
    List<Invoice__c> paymantMilList = new List<Invoice__c>();
    AggregateResult[] groupedResults = [SELECT Payment_Milestone__c, SUM(Asset_Price__c) Amount FROM PO_Deliverable__c Where Payment_Milestone__c In : paymantMLIdSet GROUP BY Payment_Milestone__c];
    for(AggregateResult re : groupedResults)
    {
        ID paymId = (ID)re.get('Payment_Milestone__c');
        if(paymId == null)
        {
            continue;
        }
        Invoice__c paym = new Invoice__c(Id = paymId);
        paym.Amount__c = (Decimal)re.get('Amount');
        paymantMilList.add(paym);
    }
    update paymantMilList;
}