/*
 * Author: Steven
 * Date: 2014-3-4
 * Description: 比较Debit Notes的Date字段和对应的Opportunity的Project_End_Date字段, 把较晚的时间赋给Project_End_Date
 */
trigger CheckOpportunityProjectEndDate on DB_Notes__c (after insert, after update) 
{
	Set<Id> oppIdSet = new Set<Id>();
	List<Opportunity> updateOpps = new List<Opportunity>();
	
	for(DB_Notes__c dn : trigger.new)
	{
		oppIdSet.add(dn.Opportunity__c);
	}
	
	for(AggregateResult dnAggre : [Select 
										Opportunity__r.Id OppId,
										Opportunity__r.Project_End_Date__c OppProjectEndDate,
										max(Date__c) maxDNDate
									From DB_Notes__c 
									Where Opportunity__c IN: oppIdSet
									GROUP BY Opportunity__r.Id, Opportunity__r.Project_End_Date__c])
	{
		if(dnAggre.get('OppId') == null)
		{
			continue;
		}
		ID oppId = (ID)dnAggre.get('OppId');
		Date oppDate = null;
		Date maxDnDate = null;
		if(dnAggre.get('OppProjectEndDate') != null)
		{
			oppDate = (Date)dnAggre.get('OppProjectEndDate');
		}
		if(dnAggre.get('maxDNDate') != null)
		{
			maxDnDate = (Date)dnAggre.get('maxDNDate');
		}
		if(oppDate != null)
		{
			if(maxDnDate != null)
			{
				if(maxDnDate > oppDate)
				{
					updateOpps.add(new Opportunity(ID = oppId, Project_End_Date__c = maxDnDate));
				}
			}
		}
		else
		{
			if(maxDnDate != null)
			{
				updateOpps.add(new Opportunity(ID = oppId, Project_End_Date__c = maxDnDate));
			}
		}
	}
	update updateOpps;
}