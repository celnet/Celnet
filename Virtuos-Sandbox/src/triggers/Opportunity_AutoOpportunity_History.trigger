/*
 * 开发者：Ziyuegao
 * 开发时间：2013-5-15
 * 功能描述：
 1.	(销售更新Opportunity 的Stage 或者更新 Opportunity 的Amount ) 且Probability (%) 大于或等于40%时需要按照以下条件生成Qualified Opportunity History Report的记录
	a)	对于同一个Opportunity，销售在同一月内将Stage的Probability (%)从10%更新到40%，同月将Stage的Probability (%)从40%更新到80%
	则进入到Qualified Opportunity History Report的记录只存储Stage的Probability (%)从40%更新到80%的opportunity记录（报表里只显示最近更改的Opportunity记录）
	b)	对于同一个Opportunity，销售在同一月内更新Probability (%) 大于或等于40% 的Opportunity  Amount 从50000更新到100000
	则进入到Qualified Opportunity History Report的记录只存储 Amount 从50000更新到100000的记录（报表里只显示最近更改的Opportunity记录）
	c)	对于一个Probability (%) 大于或等于40% 的Opportunity销售在某月内没有进行任何更新，则到下月时，系统自动存储上一月份最近更新的Opportunity记录
 * 注：一条业务机会每个月都会增加一条业务机会历史记录
       通过计划实现：每个月月初为每条业务机会创建一条该月的业务机会历史
       通过Trigger实现，如果新建一个业务机会，为该业务机会创建一条该月的业务机会历史。如果更新业务机会（Stage、Owner、Amount、Probability (%)、Close Date、Forecast Category），则将更新值同步到当前月的业务机会历史记录中
*/
trigger Opportunity_AutoOpportunity_History on Opportunity (after insert, after update) 
{
	Integer y=Date.today().year();
	Integer m=Date.today().month();
	string Month;
	if(m>9)
	{
		Month=y+'-'+m;
	}
	else
	{
		Month=y+'-0'+m;
	}
	set<ID> oppIds=new set<ID>();
	list<Qualified_Opportunity_History__c> In_QOH=new list<Qualified_Opportunity_History__c>();
	for(Opportunity opp:trigger.new)
	{
		if(trigger.isInsert)
		{
			Qualified_Opportunity_History__c qoh=new Qualified_Opportunity_History__c();
			qoh.Opportunity_Name__c=opp.id;
			qoh.Amount__c=opp.Amount;
			qoh.Close_Date__c=opp.CloseDate;
			qoh.Forecast_Category__c=opp.ForecastCategory;
			qoh.From_Stage__c=opp.StageName;
			qoh.Last_Modified__c=opp.CreatedDate;
			qoh.Last_Modified_By__c=opp.CreatedById;
			qoh.Month__c=Month;
			qoh.Probability__c=opp.Probability;
			qoh.To_Stage__c=opp.StageName;
			qoh.OwnerId=opp.OwnerId;
			In_QOH.add(qoh);
		}
		else
		{
			Opportunity oldOpp=trigger.oldMap.get(opp.id);
			if(oldOpp.StageName!=opp.StageName||oldOpp.OwnerId!=opp.OwnerId||oldOpp.Amount!=opp.Amount||oldOpp.Probability!=opp.Probability||oldOpp.CloseDate!=opp.CloseDate||oldOpp.ForecastCategory!=opp.ForecastCategory)
			{
				oppIds.add(opp.id);
			}
		}
	}
	if(In_QOH!=null&&In_QOH.size()>0)
	{
		insert In_QOH;
	}
	if(oppIds!=null&&oppIds.size()>0&&trigger.isUpdate)
	{
		list<Qualified_Opportunity_History__c> qohs=[select Opportunity_Name__c,Amount__c,Close_Date__c,Forecast_Category__c,From_Stage__c,
		                                             Last_Modified__c,Last_Modified_By__c,Month__c,Probability__c,To_Stage__c,OwnerId from Qualified_Opportunity_History__c
		                                             where Month__c=:Month and Opportunity_Name__c in:oppIds];
		//业务机会ID：业务机会历史
		map<ID,Qualified_Opportunity_History__c> map_qoh=new map<ID,Qualified_Opportunity_History__c>();
		if(qohs!=null&&qohs.size()>0)
		{
			for(Qualified_Opportunity_History__c qoh:qohs)
			{
				map_qoh.put(qoh.Opportunity_Name__c,qoh);
			}
		}
			//需要更新的业务机会历史
		list<Qualified_Opportunity_History__c> upd_qoh=new list<Qualified_Opportunity_History__c>();
		//需要添加的业务机会历史
		list<Qualified_Opportunity_History__c> Insert_qoh=new list<Qualified_Opportunity_History__c>();
		for(Opportunity opp:trigger.new)
		{
			Opportunity oldOpp=trigger.oldMap.get(opp.id);
			if(oldOpp.StageName!=opp.StageName||oldOpp.OwnerId!=opp.OwnerId||oldOpp.Amount!=opp.Amount||oldOpp.Probability!=opp.Probability||oldOpp.CloseDate!=opp.CloseDate||oldOpp.ForecastCategory!=opp.ForecastCategory)
			{
				if(map_qoh!=null&&map_qoh.containsKey(opp.id))
				{
					Qualified_Opportunity_History__c qoh=map_qoh.get(opp.id);
					qoh.Opportunity_Name__c=opp.id;
					qoh.Amount__c=opp.Amount;
					qoh.Close_Date__c=opp.CloseDate;
					qoh.Forecast_Category__c=opp.ForecastCategory;
					qoh.Last_Modified__c=opp.LastModifiedDate;
					qoh.Last_Modified_By__c=opp.LastModifiedById;
					qoh.Month__c=Month;
					qoh.Probability__c=opp.Probability;
					qoh.OwnerId=opp.OwnerId;
					if(oldOpp.StageName!=opp.StageName)
					{
						qoh.From_Stage__c=oldOpp.StageName;
						qoh.To_Stage__c=opp.StageName;
					}
					upd_qoh.add(qoh);
				}
				else
				{
					
					Qualified_Opportunity_History__c qoh=new Qualified_Opportunity_History__c();
					qoh.Opportunity_Name__c=opp.id;
					qoh.Amount__c=opp.Amount;
					qoh.Close_Date__c=opp.CloseDate;
					qoh.Forecast_Category__c=opp.ForecastCategory;
					qoh.From_Stage__c=oldOpp.StageName;
					qoh.Last_Modified__c=opp.LastModifiedDate;
					qoh.Last_Modified_By__c=opp.LastModifiedById;
					qoh.Month__c=Month;
					qoh.Probability__c=opp.Probability;
					qoh.To_Stage__c=opp.StageName;
					qoh.OwnerId=opp.OwnerId;
					Insert_qoh.add(qoh);
				}
			}
		}
		if(Insert_qoh!=null&&Insert_qoh.size()>0)
		{
			insert Insert_qoh;
		}
		if(upd_qoh!=null&&upd_qoh.size()>0)
		{
			update upd_qoh;
		}
	}
}