trigger AccumulateInvoiceAmountToAccount on Invoice__c (after delete, after insert, after undelete, 
after update) {
	   //得到invID集合
	   Set<ID> accIds = new Set<ID>();
	   if(trigger.isInsert)
	   {
		   for(Invoice__c inv :trigger.new)
		   {
		   		if(inv.Client__c != null)
		   		{
		   			accIds.add(inv.Client__c);
		   		}
		   }
	   }
	   if(trigger.isdelete)
	   {
	   		for(Invoice__c inv :trigger.old)
		   {
		   		if(inv.Client__c != null)
		   		{
		   			accIds.add(inv.Client__c);
		   		}
		   }
	   }
	   if(trigger.isUpdate)
	   {
	   		for(Invoice__c newInv :trigger.new)
		   {
		   		Invoice__c oldInv = trigger.oldMap.get(newInv.Id);
		   		if(newInv.Client__c != oldInv.Client__c)
		   		{
		   			if(newInv.Client__c != null)
		   			{
		   				accIds.add(newInv.Client__c);
		   			}
		   			if(oldInv.Client__c != null)
		   			{
		   				accIds.add(oldInv.Client__c);
		   			}
		   		}
		   		else if(newInv.Amount__c != oldInv.Amount__c || newInv.Received_Amount__c != oldInv.Received_Amount__c || newInv.Invoice_Due_Amount__c != oldInv.Invoice_Due_Amount__c || newInv.flag__c != oldInv.flag__c)
		   		{
		   			if(newInv.Client__c != null)
		   			{
		   				accIds.add(newInv.Client__c);
		   			}
		   		}
		   }
	   }
	   if(trigger.isUnDelete)
	   {
	   		for(Invoice__c inv :trigger.new)
		   {
		   		if(inv.Client__c != null)
		   		{
		   			accIds.add(inv.Client__c);
		   		}
		   }
	   } 
	   ///////////////////////////////////////////////////////////////////////////////////////
	   
	    Map<Id, Account> accMap = new Map<Id, Account>();
		for(Id accId : accIds)
		{
			Account acc = new Account(Id = accId);
			//初始化为0
			acc.Invoice_Amount_Sum__c = 0;
			acc.Invoice_Received_Amount_Sum__c =0; 
			acc.Invoice_Due_Amount_Sum__c = 0;
			accMap.put(accId, acc);
		}
	   
	   List<Invoice__c>	invList = [select Client__c, Amount__c, Received_Amount__c, Invoice_Due_Amount__c
	   											from Invoice__c 
	   											where Client__c In:accIds];
	   											
	   for(Invoice__c inv :invList)
	   {
	   		if(!accMap.containsKey(inv.Client__c))
			{
				continue;
			}
			Account acc = accMap.get(inv.Client__c);
			if(inv.Amount__c != null){
				acc.Invoice_Amount_Sum__c  += inv.Amount__c;
			}
	   		if(inv.Received_Amount__c != null){
	   			acc.Invoice_Received_Amount_Sum__c += inv.Received_Amount__c;
	   		}
	   		if(inv.Invoice_Due_Amount__c != null){ 
	   			acc.Invoice_Due_Amount_Sum__c += inv.Invoice_Due_Amount__c;
	   		}
	   }												
	  /* List<Account> accList = new List<Account>();
	   List<AggregateResult> aggInvList = [select Sum(Amount__c) sumAmount, Sum(Received_Amount__c) sumReceived, Sum(Invoice_Due_Amount__c) sumDue, Client__c
	   											from Invoice__c 
	   											where Client__c In:accIds 
	   											GROUP BY Client__c];
	   for(AggregateResult aggObj : aggInvList)
	   {
	   		ID accId = (Id)aggObj.get('Client__c');
	   		Account acc = new Account(Id = accId);
	   		acc.Invoice_Amount_Sum__c = (Decimal)aggObj.get('sumAmount') ;
	   		acc.Invoice_Received_Amount_Sum__c = (Decimal)aggObj.get('sumReceived') ;
	   		acc.Invoice_Due_Amount_Sum__c = (Decimal)aggObj.get('sumDue') ;
	   		accList.add(acc);
	   }*/
	   update  accMap.values();  
}