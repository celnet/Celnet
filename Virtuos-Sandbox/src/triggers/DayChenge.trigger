//scott
//描述 ： 当Payment Terms选项列表 更改时  会立即 做出相应的金额汇总更新  daychange!!!
trigger DayChenge on Account (after update) {
	set<Id> accid = new set<Id>();
	
	for(Account acc: trigger.new){
		//acc.Id;
		//accid.add(acc.Id);
		Account oldacc = trigger.oldMap.get(acc.Id);
		if(acc.Payment_Terms__c != oldacc.Payment_Terms__c){
			accid.add(acc.Id);
		}
	}
	
	List<Invoice__c> inv = [select Client__c, flag__c from Invoice__c where Client__c In: accid];
	
	for(Invoice__c invo:inv){
		if(invo.flag__c == null){
			invo.flag__c = 0;
		}
		invo.flag__c += 1;
	}
	update inv;
}