/*
 * 开发者：Ziyuegao
 * 开发时间：2013-5-15
 * 功能描述：通过计划实现：每个月月初为每条业务机会创建一条该月的业务机会历史
*/
global class AutoOpportunity_HistorySchedule implements Schedulable
{
	global static final String CRON_EXP = '0 0 1 * * ?';
	global void execute(SchedulableContext sc) 
	{
		AutoOpportunity_HistoryBatch  HistoryBatch  = new AutoOpportunity_HistoryBatch();
		Database.executeBatch(HistoryBatch,500);
	}
	static testMethod void test()
	{
		Opportunity opp=new Opportunity();
		opp.Name='opp';
		opp.CloseDate=Date.today().addMonths(1);
		opp.StageName='Closed Won(Signed PO or contract)';
		opp.Project_End_Date__c=Date.today().addMonths(1);
		insert opp;
		list<Qualified_Opportunity_History__c> qohs=[select id from Qualified_Opportunity_History__c where Opportunity_Name__c=:opp.id];
		delete qohs;
        system.Test.startTest();
        string sch=DateTime.now().second()+' '+DateTime.now().minute()+' '+DateTime.now().hour()+' * * ?';
        AutoOpportunity_HistorySchedule oppsched=new AutoOpportunity_HistorySchedule();
        System.schedule('autoOutTest1',sch , oppsched);
        ID batchprocessid = Database.executeBatch(new AutoOpportunity_HistoryBatch ());
        system.Test.stopTest();
		
	}
}