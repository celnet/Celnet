/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_AutoSumPoProjectProjectStatusByRate {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        List<PO_Project__c> POProjectInsertList = new List<PO_Project__c>();
        
        PO__c testPO = new PO__c();
        testPO.Name = 'test po';
        insert testPO;
        
        PO_Project__c onePoProject = new PO_Project__c();
        onePoProject.Name = 'testpoproject';
        insert onePoProject;
        
		PO_Deliverable__c insertDeliver = new PO_Deliverable__c();
		insertDeliver.Name = 'test Deliver';
		insertDeliver.Total_Man_Days__c = 2;
		insertDeliver.Complete_Rate__c = 100;
		insertDeliver.PO__c = testPO.Id;
		insertDeliver.PO_Project__c = onePoProject.Id;
		insert insertDeliver;
        
    }
}