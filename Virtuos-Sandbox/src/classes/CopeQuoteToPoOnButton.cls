global class CopeQuoteToPoOnButton {

	webservice static void CopeQuoteToPoOnButton(string Quoteid) {
		
		string POID = '';
		
		List<PO_Project__c> Po_ProjectList = new List<PO_Project__c>();
		List<PO_Project_Milestone__c> PO_ProjectMilestoneList = new List<PO_Project_Milestone__c>();
		List<PO_Deliverable__c> PO_DeliverableList = new List<PO_Deliverable__c>();
		List<Invoice__c> PO_InvoiceList = new List<Invoice__c>();
		
		List<Project__c> ProjectList = [select Actual_End__c,
												Client__c,
												Delay_Days__c,
												Name,
												Project_No__c,
												Plan_End__c,
												Plan_Start__c,
												Project_Status__c from Project__c where Quote__c =: Quoteid];
		List<Project_Milestone__c> ProjectMilestoneList = [select Name,
																Project_Milestone_No__c,
																Date__c
		 														from Project_Milestone__c where Quote__c =: Quoteid];
		List<Deliverable__c> DeliverableList = [select Asset_Price__c,
														Category__c,
														Deliverable_No__c,
														Group__c,
														Man_Day_Unit_Price__c,
														Name,
														Sub_Group__c,
														Sub_Category__c,
														Total_Man_Days__c,
														Project__c,
														Project_Milestone__c from Deliverable__c where Quote__c =: Quoteid];

		//第一步,根据Quote新建po
		List<Quote__c> QuoteList = [select Account__c,
											Name,
											Opportunity__c from Quote__c where id =:Quoteid ];
		Quote__c Insert_Quote = QuoteList[0];
		PO__c Insert_Po = new PO__c();
		string TypeStr;
		string SubTypeStr;
		string OppID = Insert_Quote.Opportunity__c;
		if(OppID != null){
			List<Opportunity> OpportunityList = [select Type,
													Subtype__c from Opportunity where id =:OppID ];
			Opportunity OpportunityType = OpportunityList[0];
			TypeStr = OpportunityType.Type;
			SubTypeStr = OpportunityType.Subtype__c;
		}
		Insert_Po.Client__c = Insert_Quote.Account__c;
		Insert_Po.Name = Insert_Quote.Name;
		Insert_Po.Quote__c = Quoteid;
		Insert_Po.Type__c = TypeStr;
		Insert_Po.Subtype__c = SubTypeStr;
		insert Insert_Po;
		POID = 	Insert_Po.Id;							
					
		//第二步,插入PO_Project__c	and PO_Project_Milestone__c		
		for(Project__c Project_insert : ProjectList){
			PO_Project__c Po_Project_insert = new PO_Project__c();
			Po_Project_insert.Actual_End__c = Project_insert.Actual_End__c ;
			Po_Project_insert.Client__c = Project_insert.Client__c ;
			Po_Project_insert.Delay_Days__c = Project_insert.Delay_Days__c ;
			Po_Project_insert.Name = Project_insert.Name ;
			Po_Project_insert.PO_Project_No__c = Project_insert.Project_No__c ;
			Po_Project_insert.PO__c = POID ;
			Po_Project_insert.Plan_End__c = Project_insert.Plan_End__c ;
			Po_Project_insert.Plan_Start__c = Project_insert.Plan_Start__c ;
			Po_Project_insert.Project_Status__c = Project_insert.Project_Status__c ;
			Po_ProjectList.add(Po_Project_insert);
		}
			
		for(Project_Milestone__c Project_Milestone_insert : ProjectMilestoneList){
			PO_Project_Milestone__c PO_ProjectMilestone_Insert = new PO_Project_Milestone__c();
			PO_ProjectMilestone_Insert.Name = Project_Milestone_insert.Name;
			PO_ProjectMilestone_Insert.Project_Milestone_No__c = Project_Milestone_insert.Project_Milestone_No__c;
			PO_ProjectMilestone_Insert.PO__c = POID;
			PO_ProjectMilestone_Insert.Date__c = Project_Milestone_insert.Date__c;
			PO_ProjectMilestoneList.add(PO_ProjectMilestone_Insert);
			
			Invoice__c Invoice_Insert = new Invoice__c();
			Invoice_Insert.Name = Project_Milestone_insert.Name;
			Invoice_Insert.PO__c = POID;
			Invoice_Insert.Client__c = Insert_Po.Client__c;
			PO_InvoiceList.add(Invoice_Insert);
		}									
		insert Po_ProjectList;										
		insert PO_ProjectMilestoneList;
		insert PO_InvoiceList;
		
		for(Deliverable__c Deliverable : DeliverableList){
			PO_Deliverable__c PO_Deliverable_Insert = new PO_Deliverable__c();
			string ProjectID = Deliverable.Project__c;
			string ProjectMilestoneID = Deliverable.Project_Milestone__c;
			
			if(ProjectID!=null && ProjectID.length()>0){
				for(Integer i=0; i<ProjectList.size(); i++){
					Project__c ProjectRe = ProjectList[i];
					if(ProjectID == ProjectRe.Id){
						PO_Project__c PO_ProjectRe = Po_ProjectList[i];
						PO_Deliverable_Insert.PO_Project__c = PO_ProjectRe.Id;
                        break;
					}
				}
			}
			
			if(ProjectMilestoneID!=null && ProjectMilestoneID.length()>0){
				for(Integer i=0; i<ProjectMilestoneList.size(); i++){
					Project_Milestone__c Project_MilestoneRe = ProjectMilestoneList[i];
					if(ProjectMilestoneID == Project_MilestoneRe.Id){
						PO_Project_Milestone__c PO_Project_MilestoneRe = PO_ProjectMilestoneList[i];
						PO_Deliverable_Insert.PO_Project_Milestone__c = PO_Project_MilestoneRe.Id;
                        break;
					}
				}				
			}
			
			PO_Deliverable_Insert.PO__c = POID;
			//PO_Deliverable_Insert.Asset_Price__c = Deliverable.Asset_Price__c;	//readonly
			PO_Deliverable_Insert.Category__c = Deliverable.Category__c;
			PO_Deliverable_Insert.Deliverable_No__c = Deliverable.Deliverable_No__c;
			PO_Deliverable_Insert.Group__c = Deliverable.Group__c;
			PO_Deliverable_Insert.Man_Day_Unit_Price__c = Deliverable.Man_Day_Unit_Price__c;
			PO_Deliverable_Insert.Name = Deliverable.Name;
			PO_Deliverable_Insert.SubGroup__c = Deliverable.Sub_Group__c;
			PO_Deliverable_Insert.Sub_Category__c = Deliverable.Sub_Category__c;
			PO_Deliverable_Insert.Total_Man_Days__c = Deliverable.Total_Man_Days__c;
			
			PO_DeliverableList.add(PO_Deliverable_Insert);
		}
		insert PO_DeliverableList;
	}	
}