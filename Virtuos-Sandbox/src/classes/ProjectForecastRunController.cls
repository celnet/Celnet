/*
Author: Tommyliu of Cideatech at 2012-8-10
*/
public class ProjectForecastRunController 
{
    private Integer year; 
    private List<AsyncApexJob> jobList;
    private Boolean sendNotificationMail;
    private Boolean msgShow;
    private String msgSeverity;
    private String msg;
    public Boolean  isSystemAdmin{get;set;}    //是否具有系统管理员权限
    public Virtuos_Config__c lastDNDate{get;set;}   //Tobe 2013-07-03 手动设置时间分割点
    
    public ProjectForecastRunController ()
    {
        this.year = Date.today().year(); 
        this.sendNotificationMail = false;   
        this.lastDNDate = Virtuos_Config__c.getValues('LastDNDateConfig');    
        String profileId = userinfo.getProfileId();
        list<Profile> admins = [Select Name From Profile Where Id =: profileId ];
        for(Profile admin : admins)
        {
            if(admin.Name == 'System Administrator'|| admin.Name == '系统管理员')
                isSystemAdmin = true;
            else
                isSystemAdmin = false;
        }
    }
    
    public String UserName
    {
        get
        {
            return UserInfo.getName();
        }
    }
    
    public Boolean IsSendNotificationMail
    {
        get
        {
            return this.sendNotificationMail;
        }
        set
        {
            this.sendNotificationMail = value;
        }
    }
    
    public String ForecastReportUrl
    {
        get
        {
            List<Report> reList = [Select Name, Id, DeveloperName From Report Where DeveloperName = 'Project_Forecast' Order By LastModifiedDate];
            if(reList.size() == 0)
            {
                return null;
            }
            return URL.getSalesforceBaseUrl().toExternalForm() + '/' + reList[0].Id;
        }
    }
    
    public Boolean IsShowReportLink
    {
        get
        {
            if(ForecastReportUrl != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    
    public List<SelectOption> YearOptions 
    { 
        get 
        {
            Integer currentYear = Date.today().year();
            List<SelectOption> options = new List<SelectOption>();
            for(Integer i = currentYear+5; i>= currentYear - 8; i--)
            {
                options.add(new SelectOption(i + '',i + ''));
            }
            return options;
        } 
    }
    
    public String SelectedYear
    {
        get
        {
            if(this.year != null)
            {
                return this.year + '';
            }
            return null;
        }
        set
        {
            this.year = Integer.valueOf(value);
        }
    }
    
    public List<AsyncApexJob> JobRows
    {
        get
        {
            return this.jobList;
        }
    }
    
    public Boolean MessageShow
    {
        get
        {
            return this.msgShow;
        }
    } 
    public String MessageSeverity
    {
        get
        {
            return this.msgSeverity;
        }
    }
    
    public String Message
    {
        get
        {
            return this.msg;
        }
    }
    
    public void RefreshJob()
    {
        this.msgShow = false;
        this.msg = '';
        try
        {
            this.jobList  = [
                Select
                TotalJobItems, 
                Status, 
                NumberOfErrors, 
                JobItemsProcessed, 
                Id, 
                CreatedDate, 
                CompletedDate, 
                ApexClass.Name,
                ApexClassId,
                CreatedBy.Name 
                From AsyncApexJob
                Where ApexClass.Name = 'ProjectForecastBatch'
                Order By CreatedDate DESC
                Limit 25
            ];
        }
        catch(Exception exc)
        {
            this.ShowError(exc);
        }
    }
    
    public void Run() 
    {
        this.msgShow = false;
        this.msg = '';
        try
        {
           update this.lastDNDate;
           system.debug('********************lastDNDate************************'+lastDNDate.Name);
           system.debug('********************lastDNDate************************'+lastDNDate.Value__c);
            //throw new ProjectForecast.ProjectForecastException('this is bad thing!');
            ProjectForecastBatch pfBatch = new ProjectForecastBatch(this.year, this.sendNotificationMail);
            Database.executeBatch(pfBatch, 50);
            RefreshJob();
            this.msg = 'The Calculation Job has been submitted successfully, the job will be completed during a few minutes.';
            this.msgSeverity = 'info';
            this.msgShow = true;
        }
        catch(System.LimitException lexc)
        {
            this.ShowLError(lexc);
        }
        catch(Exception exc)
        {
            this.ShowError(exc);
        }
    }
    
    public void ShowError(Exception exc)
    {
        this.msg = 'Failed! There is error:' + exc;
        this.msgSeverity = 'error';
        this.msgShow = true;
    }
    
    public void ShowLError(System.LimitException lexc)
    {
        this.msg = 'Failed! There is error:';
        if(lexc != null)
        {
             msg += lexc;
        }
        this.msgSeverity = 'error';
        this.msgShow = true;
    }
    
    static testMethod void testForecast() 
    {
        ProjectForecastRunController pfController = new ProjectForecastRunController();
        pfController.SelectedYear = '2012';
        String year = pfController.SelectedYear;
        pfController.IsSendNotificationMail = true;
        Boolean isSend = pfController.IsSendNotificationMail;
        pfController.Run();
        pfController.RefreshJob();
        List<AsyncApexJob> jobList = pfController.JobRows;
        List<SelectOption> yearOptions = pfController.YearOptions;
        Boolean isShowLink  = pfController.IsShowReportLink;
        String UserName = pfController.UserName;
        Boolean msgShow  = pfController.MessageShow;
        String msgS = pfController.MessageSeverity;
        String msg  = pfController.Message;
        String reUrl = pfController.ForecastReportUrl;
        Boolean showlink = pfController.IsShowReportLink;
        pfController.ShowError(new ProjectForecast.ProjectForecastException('this is bad thing!'));
        pfController.ShowLError(null);
    }
}