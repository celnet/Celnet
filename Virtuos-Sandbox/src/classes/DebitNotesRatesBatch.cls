//测试DebitNotesRatesBatch
//开发人:米立业
global class DebitNotesRatesBatch implements Database.Batchable<sObject>, Database.Stateful {
	
	 global String configId;
	 private map<String, ExchangeRateItem__c> rateItemMap;
	 private Integer total = 0;
	 private String corporateCode;
	 private MonthlyExchangeRate__c mxr;
	 global Database.QueryLocator start(Database.BatchableContext BC){
	 	this.mxr =[select HasExecuted__c,Month__c,Result__c,SendNoticeMail__c,Year__c from MonthlyExchangeRate__c where ID = :configId];
		if(rateItemMap == null)
	 	{
	 		this.rateItemMap = new map<String, ExchangeRateItem__c> ();
	 	}
		for(ExchangeRateItem__c rateItem :[select id, CurrencyIsoCode,ConversionRate__c,Corporate__c from ExchangeRateItem__c where MonthlyExchangeRate__c = :mxr.Id])
		{
			if(rateItem.Corporate__c)
			{
				if(this.corporateCode != null && this.corporateCode != '')
				{
					throw new MonthlyExchangeRateException('Only one currency can be set as corporate');
				}
				else
				{
					this.corporateCode = rateItem.CurrencyIsoCode;
				}
			}
			if(!rateItemMap.containsKey(rateItem.CurrencyIsoCode))
			{
				rateItemMap.put(rateItem.CurrencyIsoCode, rateItem);
			}
			else
			{
				throw new MonthlyExchangeRateException('Currency iso code must be unique in monthly exchange rate item list');
			}
		}
		if(this.corporateCode == null || this.corporateCode == '')
		{
			throw new MonthlyExchangeRateException('None of currency in monthly exchange rate be set as corporate');
		}
	 	date start = date.newInstance(mxr.Year__c.intValue(), Integer.valueOf(mxr.Month__c), 1);
	 	date enddate = date.newInstance(mxr.Year__c.intValue(),Integer.valueOf(mxr.Month__c) + 1,1).addDays(-1);
        return Database.getQueryLocator([select id,CurrencyIsoCode,Amount__c,Date__c,USDAmount__c from Debit_Notes__c where Date__c >= :start and Date__c <= :enddate]);
	 }
	 
	 global void execute(Database.BatchableContext BC, List<sObject> scope){
	 	InvoicePaymentRateBatch rb = new InvoicePaymentRateBatch();
	 	for(sObject obj : scope){
	 		Debit_Notes__c debit = (Debit_Notes__c)obj;
	 		if(!rateItemMap.containsKey(debit.CurrencyIsoCode))
		 	{
		 		throw new MonthlyExchangeRateException('the currency of invoice is not in monthly exchange rate item list, invoice Id:' + debit.Id);
		 	}
		 	Decimal usdRate = rateItemMap.get('USD').ConversionRate__c;
		 	if(debit.CurrencyIsoCode=='USD'){	 				
		 		debit.USDAmount__c = debit.Amount__c;
		 	}
		 	else
		 	{
		 		Decimal currentRate =  rateItemMap.get(debit.CurrencyIsoCode).ConversionRate__c;
		 		Decimal amountForCorporate = debit.Amount__c /currentRate;
		 		Decimal amountForUsd = amountForCorporate * usdRate;
		 		debit.USDAmount__c = amountForUsd;
		 	}	 		
	 	}
	 	mxr.HasExecuted__c = true;
	 	this.total += scope.size();
	 	update scope;
	 }
	 global void finish(Database.BatchableContext BC){
	 	String result = '';
	 	AsyncApexJob a = [Select Id, Status, NumberOfErrors, ExtendedStatus,JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id = :BC.getJobId()];
	 	if(mxr.SendNoticeMail__c){
	 		//User user = [select email from User where id = :System.UserInfo.getUserId()];
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			String[] toAddresses = new String[] {a.CreatedBy.Email};
			//String[] toAddresses = new String[] {a.CreatedBy.Email,'satanmi@frensworkz.com'};
			
	        mail.setToAddresses(toAddresses);
	        mail.setSubject('DebitNotes汇率计算结果:' );
	        mail.setCharset('gbk');
	        result = 'DebitNotes计算状态: '+a.Status+'共计算了'+this.total+'条记录!'+'信息:'+a.ExtendedStatus+'!错误数:'+a.NumberOfErrors+'!';  
	        if(a.ExtendedStatus==null){
	        	result = 'DebitNotes计算状态: '+a.Status+'共计算了'+this.total+'条记录!错误数:'+a.NumberOfErrors+'!';
	        }
	        if(a.NumberOfErrors==0){
	        	result = 'DebitNotes计算状态: '+a.Status+'!共计算了'+this.total+'条记录!';	
	        }
	        mail.setPlainTextBody(result);	        
	        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	        if(result.length()>255){
	        	result = result.substring(0,255);
	        }	       
	 	}
	 	mxr.Result__c += result;
	 	if((mxr.Result__c.length()+result.length())>255){
	 		mxr.Result__c = (mxr.Result__c+result).substring(0,255);
	 	}
	 	update mxr;
	 }
	 public class MonthlyExchangeRateException extends Exception {}
}