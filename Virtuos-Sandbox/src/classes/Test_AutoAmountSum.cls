@isTest
private class Test_AutoAmountSum {

    static testMethod void myUnitTest() {
    	
	   Account insAcc = new Account();
	   insAcc.Name = 'testinsAcc';
	   insert insAcc;
	   
	   PO__c insPO = new PO__c();
	   insPO.Name = 'testinsPO';
	   insPO.Client__c = insAcc.Id;
	   insert insPO;
	   
	   PO_Project__c insPro = new PO_Project__c();
	   insPro.Name = 'testinsPro';
	   insPro.PO_Project_No__c = '11111111111';
	   insPro.PO__c = insPO.Id;
	   insert insPro;
	   
	   Invoice__c insInv = new Invoice__c();
	   insInv.Name = 'testinsInv';
	   insInv.PO__c = insPO.Id;
	   insInv.Client__c = insAcc.Id;
	   //insInv.Invoice_Sent_Date__c = date.today();
	   insInv.Amount__c = 2000;
	   insInv.Received_Amount__c = 1000;
	   insert insInv;
	   
	   insInv.Amount__c = 30;
	   update insInv;
	   
	   delete insInv;
	   
	   undelete insInv;
	   
	   insPO = [select Invoice_Amount_Sum__c, Invoice_Received_Amount_Sum__c, Invoice_Due_Amount_Sum__c
	   			from PO__c
	   			where id =: insPO.Id];
	   
	   insAcc = [select Invoice_Amount_Sum__c, Invoice_Received_Amount_Sum__c, Invoice_Due_Amount_Sum__c
	   			from Account
	   			where id =: insAcc.Id];
	   			
	   			
	   
	  /* System.debug('*****************' + insPO.Invoice_Amount_Sum__c);
	   System.debug('*****************' + insPO.Invoice_Received_Amount_Sum__c);
	   System.debug('*****************' + insPO.Invoice_Due_Amount_Sum__c);*/
	  /* 
	   System.assertEquals(20, insPO.Invoice_Amount_Sum__c);
	   System.assertEquals(10, insPO.Invoice_Received_Amount_Sum__c);
	   System.assertEquals(10, insPO.Invoice_Due_Amount_Sum__c);
	   
	   System.assertEquals(20, insAcc.Invoice_Amount_Sum__c);
	   System.assertEquals(10, insAcc.Invoice_Received_Amount_Sum__c);
	   System.assertEquals(10, insAcc.Invoice_Due_Amount_Sum__c);*/
	   
    }
}