//CreateDebitNodeBatch bc = new CreateDebitNodeBatch(new Id[]{'a0kO00000004CLjIAM', 'a0kO00000004CMhIAM'});
//CreateDebitNodeBatch bc = new CreateDebitNodeBatch();
//ID batchprocessid = Database.executeBatch(bc, 100);
//AsyncApexJob aaj = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =: batchprocessid ];
global class CreateDebitNodeBatch implements Database.Batchable<sObject>, Database.Stateful
{
	global final Date toDay; //Job是长时间运行的,所以要保证一次批处理所建立的Debit 有相同的Date
	global final List<ID> poProjectIds;
	global CreateDebitNodeBatch(List<ID> poProjectIds)
	{
		toDay = Date.today();
		poProjectIds = poProjectIds;
	}
	
	global CreateDebitNodeBatch()
	{
		toDay = Date.today();
	}

	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		if(this.poProjectIds != null && this.poProjectIds.size() > 0)
		{
			String idStr = '(';
			Integer s = poProjectIds.size();
			for(Integer i = 0; i < s; i ++ )
			{
				if(poProjectIds[i] == null)
				{
					continue;
				}
				idStr += poProjectIds[i];
				if(i < s - 1 && poProjectIds[s - 1] != null)
				{
					idStr +=',';
				}
			}
			idStr +=')';
			return Database.getQueryLocator('Select Id From PO_Project__c Where PO__r.PO_Status__c In(\'Start\', \'Execution\') And Id In ' + idStr );
		}
		//String stateValue = '\'Start\', \'Execution\''; //只有在PO的状态为'Start', 'Execution'时才计算Debit notes
		return Database.getQueryLocator('Select Id From PO_Project__c Where PO__r.PO_Status__c In(\'Start\', \'Execution\')');
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		List<ID> poIdList = new List<ID>();
		List<Debit_Notes__c> newDebitList = new List<Debit_Notes__c>();
		List<Debit_Notes__c> existDebitList = new List<Debit_Notes__c>();
		Map<ID, List<Debit_Notes__c>> projectDebSet = new Map<ID, List<Debit_Notes__c>>();
		for(sObject obj : scope)
		{
			poIdList.add((ID)obj.get('Id'));
		}
		
		for(PO_Project__c poProj : [Select Id, (Select Id, Sum_Price__c, Amount__c, Client__c, PO_Project__c, Date__c From Debit_Notes__r) From PO_Project__c Where Id in : poIdList])
		{
			projectDebSet.put(poProj.Id, poProj.Debit_Notes__r);
		}

		for(AggregateResult aR : [Select PO_Project__c ProjectId, PO_Project__r.Client__c ClientId, SUM(Real_Price__c) SumPrice 
			From PO_Deliverable__c
			Where PO_Project__c In : poIdList 
			GROUP BY PO_Project__c, PO_Project__r.Client__c
			])
		{	
			ID projectId = (ID)aR.get('ProjectId');
			ID clientId = (ID)aR.get('ClientId');
			Decimal sumPrice = (Decimal)aR.get('SumPrice');
			
			//针对相应的Project,查找已经存在的本月和上个月的Debit notes
			List<Debit_Notes__c> thisAndLast = this.findLastAndThisDeb(projectId, projectDebSet);
			Debit_Notes__c thisDeb = thisAndLast[1];
			Debit_Notes__c lastDeb = thisAndLast[0];
			if(thisDeb == null)//不存在则创建一个新的
			{
				thisDeb = new Debit_Notes__c();
				thisDeb.Client__c = clientId;
				thisDeb.PO_Project__c = projectId;
				thisDeb.Sum_Price__c = sumPrice;
				thisDeb.Date__c = this.toDay;
				if(lastDeb == null)//如果是没有上个月的，那么本月是初始月，sumPrice 直接作为Amount
				{
					thisDeb.Amount__c = sumPrice;			
				}
				else
				{
					Decimal lastSumPrice = 0;
					if(lastDeb.Sum_Price__c != null)
					{
						lastSumPrice = lastDeb.Sum_Price__c;
					}
					thisDeb.Amount__c = sumPrice - lastSumPrice;
				}
				//TODO 设置编号
				newDebitList.add(thisDeb);
			}
			else//存在则更新已经存在的
			{
				thisDeb.Sum_Price__c = sumPrice;
				thisDeb.Date__c = this.toDay;
				if(lastDeb == null)//如果是没有上个月的，那么本月是初始月，sumPrice 直接作为Amount
				{
					thisDeb.Amount__c = sumPrice;			
				}
				else
				{
					Decimal lastSumPrice = 0;
					if(lastDeb.Sum_Price__c != null)
					{
						lastSumPrice = lastDeb.Sum_Price__c;
					}
					thisDeb.Amount__c = sumPrice - lastSumPrice;
				}
				//update thisDeb;
				existDebitList.add(thisDeb);
			}
		}
		insert newDebitList;
		update existDebitList;
	}
	
	global void finish(Database.BatchableContext BC)
	{
		AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id = :BC.getJobId()];
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		String[] toAddresses = new String[] {a.CreatedBy.Email};
		mail.setToAddresses(toAddresses);
		mail.setSubject('Create Debit Notes:' + a.Status);
		mail.setPlainTextBody
		('The batch Apex job processed ' + a.TotalJobItems +
		' batches with '+ a.NumberOfErrors + ' failures.');
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
	
	//用于寻找本月存在的Deb和上个月存在的Deb, List[0]存储上个月的, List[1]存储这个月的
	private List<Debit_Notes__c> findLastAndThisDeb(ID poProjectId, Map<ID, List<Debit_Notes__c>> projectDebSet)
	{
		Date lastMonthToDay = this.toDay.addMonths(-1);
		Debit_Notes__c thisDeb;
		Debit_Notes__c lastDeb;
		List<Debit_Notes__c> debList = projectDebSet.get(poProjectId);
		//[Select Id, Sum_Price__c, Amount__c, Client__c, PO_Project__c, Date__c From Debit_Notes__c Where PO_Project__c =: poProjectId ORDER BY Date__c];
		for(Debit_Notes__c deb : debList)
		{
			if(deb.Date__c == null)
			{
				continue;	
			}
			if(deb.Date__c.year() == this.toDay.year() && deb.Date__c.month() == this.toDay.month())
			{
				thisDeb = deb;
				break;
			}
			if(deb.Date__c.year() == lastMonthToDay.year() && deb.Date__c.month() == lastMonthToDay.month())
			{
				lastDeb = deb;
				break;
			}
		}
		while(lastDeb == null)//如果还是找不到上个月的,则需找前一个月的,一直寻找到有,循环完毕找还找不到,则证明是一个本月的是初始月
		{
			lastMonthToDay = lastMonthToDay.addMonths(-1);
			for(Debit_Notes__c deb : debList)
			{
				if(deb.Date__c == null)
				{
					continue;	
				}
				if(deb.Date__c.year() == lastMonthToDay.year() && deb.Date__c.month() == lastMonthToDay.month())
				{
					lastDeb = deb;
					break;
				}
			}
		}
		List<Debit_Notes__c> reList = new Debit_Notes__c[2];
		reList[0] = lastDeb;
		reList[1] = thisDeb;
		return reList;
	}
}