@isTest
private class Test_AccumulateInvoiceAmountToPo {

    static testMethod void myUnitTest() {
    	PO__c insertPO = new PO__c();
    	insertPO.Name = 'testinsertPO';
    	insert insertPO;
    	
    	PO__c insertPO2 = new PO__c();
    	insertPO2.Name = 'testinsertPO';
    	insert insertPO2;
    	
		PO_Project__c insertProject = new PO_Project__c();
		insertProject.Name = 'insertProject';
		insertProject.PO_Project_No__c = 'ccc00001';
		insertProject.PO__c = insertPO.Id;
		insert insertProject;
    	
		PO_Project__c insertProject2 = new PO_Project__c();
		insertProject2.Name = 'insertProject';
		insertProject2.PO_Project_No__c = 'ccc00001';
		insertProject2.PO__c = insertPO2.Id;
		insert insertProject2;
    	
		Invoice__c testInvoice = new Invoice__c();
		testInvoice.Amount__c = 23;
		testInvoice.Name = 'testInvoice';
		testInvoice.PO__c = insertPO.Id;
		insert testInvoice;
		
		testInvoice.PO__c = insertPO2.Id;
		update testInvoice;
		
		testInvoice.Amount__c = 232;
		update testInvoice;
		
		delete testInvoice;
		
		undelete testInvoice;
    }
}