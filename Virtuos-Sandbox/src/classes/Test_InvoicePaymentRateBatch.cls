@isTest
//测试InvoicePaymentRateBatch
//开发人:米立业
private class Test_InvoicePaymentRateBatch {

    static testMethod void myUnitTest() {
        
        MonthlyExchangeRate__c monthRate = new MonthlyExchangeRate__c();
        monthRate.Year__c = 2018;
        monthRate.Month__c = '7';
        insert monthRate;
        
        List<ExchangeRateItem__c> rateItems = [select id, CurrencyIsoCode,
        									   ConversionRate__c,Corporate__c from 
        									   ExchangeRateItem__c where MonthlyExchangeRate__c = :monthRate.Id];
        
        PO__c po = new PO__c();
        po.Name = 'TestInvoiceBatchPO';     
        insert po;
        
        PO_Project__c poProject = new PO_Project__c();
        poProject.Name='TestInvoiceBatchPOProject';
        poProject.PO_Project_No__c = '00000001xxx';
        poProject.PO__c = po.Id;
        insert poProject;
        
        List<Invoice__c> invoiceList = new List<Invoice__c>();
        
        Invoice__c invoice = new Invoice__c();
        invoice.Name = 'TestInvoiceBatch';
        invoice.CurrencyIsoCode = 'USD';
        invoice.Should_Invoice_Date__c = date.newInstance(2018, 7, 6);
        invoice.Amount__c = 500;
        invoice.PO__c = po.Id;
        invoiceList.add(invoice);
        
        Invoice__c invoiceCny = new Invoice__c();
        invoiceCny.Name = 'TestInvoiceBatch';
        invoiceCny.CurrencyIsoCode = 'CNY';
        invoiceCny.Should_Invoice_Date__c = date.newInstance(2018, 7, 6);
        invoiceCny.Amount__c = 500;
        invoiceCny.PO__c = po.Id;
        invoiceList.add(invoiceCny);
        
        insert invoiceList;
        
        test.startTest();
        InvoicePaymentRateBatch pb = new InvoicePaymentRateBatch();
        pb.configId = monthRate.Id;
        ID backId = Database.executeBatch(pb, 100);
        test.stopTest();
        
        Decimal wantValue;
        for(ExchangeRateItem__c rateItem : rateItems){
        	if(rateItem.CurrencyIsoCode=='CNY'){
        		wantValue = invoiceCny.Amount__c / rateItem.ConversionRate__c;
        	}
        }       
        
        List<Invoice__c> newInvoiceList = [select Id,CurrencyIsoCode, Amount__c,USDAmount__c from Invoice__c where Id IN(: invoice.Id ,:invoiceCny.Id)];
        for(Invoice__c newInvoice : newInvoiceList){
        	if(newInvoice.CurrencyIsoCode=='USD'){
        		System.assertEquals(newInvoice.Amount__c,newInvoice.USDAmount__c);
        	}else{
        		System.assertEquals(wantValue, newInvoice.USDAmount__c);
        	}
        }       
    }
}