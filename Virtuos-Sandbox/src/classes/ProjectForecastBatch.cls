//ProjectForecastBatch pfBatch = new ProjectForecastBatch(2012, true);
//Database.executeBatch(pfBatch, 3);
/*
Author: Tommyliu of Cideatech at 2012-8-9
*/
global class ProjectForecastBatch implements Database.Batchable<SObject>
{
	private final Integer currentYear;
	private map<Integer, map<Integer, MonthlyExchangeRate__c>> exchangeRateMap;//存储历史汇率表， key1指定年份2011， key2指定月份 1-12
	private String query;
	private boolean sendNotice;
	global ProjectForecastBatch(Integer year, boolean sendNoticeMail)
	{
		this.sendNotice = sendNoticeMail;
		this.currentYear = year;
		this.exchangeRateMap = ExchangeRateHelper.GetExchangeRate(this.currentYear);
		this.query =
		' Select ' 
		+'	Id,'
		+'	Name,'
		+'	CurrencyIsoCode,'
		+'	CloseDate, '
		+'	Project_End_Date__c,'
		+'	Probability,'
		+'	Amount,'
		+'	Account.Owner.Name,'
		+'	Account.Owner.ID,'
		+'	Account.Name,'
		+'	Account.Id,'
		+'	Account.Account_No__c,'
		+'	(Select Id, Amount__c, Date__c From Debit_Notes__r Order by Date__c DESC)'
		+'	From Opportunity '
		+'	Where ((CALENDAR_YEAR(CloseDate) = ' + this.currentYear + ' Or CALENDAR_YEAR(Project_End_Date__c) = ' + this.currentYear + ')'
		+'	Or (CALENDAR_YEAR(CloseDate) < ' + this.currentYear + ' And CALENDAR_YEAR(Project_End_Date__c) > ' + this.currentYear + ' ))'
		+'	And Project_End_Date__c != null And CloseDate != null And Amount != null And Probability != null'
		+'	order By CloseDate DESC';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		//---------Tommy added at 2012-12-12---------------
		//每次计算前删除当年的预测数据，从新生产当前的数据，这样报表更准确，规避了修改时间带来的问题
		for(List<Project_Forecast__c> pfList:[Select id From Project_Forecast__c Where Year__c =:currentYear])
		{
			delete pfList;
		}
		//-----------------------------------------------------
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC, List<SObject> scope)
	{
	     ProjectForecast pf = new ProjectForecast(this.currentYear, this.exchangeRateMap);
	     pf.Calculate(scope); 
	}
	
	global void finish(Database.BatchableContext BC)
	{
		if(this.sendNotice)
		{ 
			AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, CreatedDate,
		      TotalJobItems, ExtendedStatus, CreatedBy.Email, CreatedBy.TimeZoneSidKey
		      FROM AsyncApexJob WHERE Id =
		      :BC.getJobId()];
		    String sbTime;
		    if(a.CreatedBy != null && a.CreatedBy.TimeZoneSidKey == 'Asia/Shanghai')
		    {
		    	sbTime =  '' + a.CreatedDate.addHours(8);
		    }
		    else
		    {
		    	sbTime = 'GMT ' + a.CreatedDate;
		    }
		   	// Send an email to the Apex job's submitter notifying of job completion.
		   	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		   	String[] toAddresses = new String[] {a.CreatedBy.Email};
		   	mail.setToAddresses(toAddresses);
		   	mail.setReplyTo('no-reply@salesforce.com');
		   	mail.setSubject('Project Forecast Calculation Job Process ' + a.Status);
		   	mail.setSenderDisplayName('Saleforce Job Processor');
		  	mail.setPlainTextBody
		   	(
		   	 'The Project Forecast Calculation Job that you submitted at ' + sbTime + ' is ' + a.Status + '\n\n' +
		   	 ' Total Batches: ' + a.TotalJobItems + '\n' +
		  	 ' Failures: '+ a.NumberOfErrors + '\n' +
		  	 ' Detail: ' + a.ExtendedStatus
		  	 );
		  	if(!Test.isRunningTest())
		  	{
		   		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		  	}
		}
	}
	/*
	static Decimal testSumRow(ForecastRow row)
	{
		if (Row == null)
		{
			return 0;
		}
		Decimal allCellValue  = 0;
		if(row.Month1 != null)
		{
			allCellValue += row.Month1;
		}
		if(row.Month2 != null)
		{
			allCellValue += row.Month2;
		}
		if(row.Month3 != null)
		{
			allCellValue += row.Month3;
		}
		if(row.Month4 != null)
		{
			allCellValue += row.Month4;
		}
		if(row.Month5 != null)
		{
			allCellValue += row.Month5;
		}
		if(row.Month6 != null)
		{
			allCellValue += row.Month6;
		}
		if(row.Month7 != null)
		{
			allCellValue += row.Month7;
		}
		if(row.Month8 != null)
		{
			allCellValue += row.Month8;
		}
		if(row.Month9 != null)
		{
			allCellValue += row.Month9;
		}
		if(row.Month10 != null)
		{
			allCellValue += row.Month10;
		}
		if(row.Month11 != null)
		{
			allCellValue += row.Month11;
		}
		if(row.Month12 != null)
		{
			allCellValue += row.Month12;
		}
		return allCellValue;
	} 
	*/
	
	static testMethod void testForecast() 
    {
    	Integer theYear = 1945;
    	Account acc = new Account();
    	acc.Name = 'T_Test Account';
    	insert acc;
    	Opportunity opp1 = new Opportunity();
    	opp1.Name = 'Opp1';
    	opp1.AccountId = acc.Id;
    	opp1.StageName = 'Approved';
    	opp1.Amount = 120000;
    	opp1.CurrencyIsoCode = 'CNY';
    	opp1.CloseDate = Date.newInstance(theYear, 1, 2);
    	opp1.Project_End_Date__c = Date.newInstance(theYear, 12, 5);
    	opp1.Probability = 100;
    	insert new Opportunity[] {opp1};
    	Project__c prj1 = new Project__c();
    	prj1.Name = 'Project1';
    	prj1.Opportunity__c = opp1.Id;
    	prj1.Project_No__c = 'C001';
    	insert new Project__c[]{prj1};
    	
    	//准备汇率表
		List<MonthlyExchangeRate__c> rateIntheYear = [Select Id From MonthlyExchangeRate__c Where Year__c =:theYear];
		if(rateIntheYear.size() > 0)
		{
			delete rateIntheYear; //如果系统已经存在删除已经存在的汇率表，然后创建新的
		}		
		List<MonthlyExchangeRate__c> monthRateList = new List<MonthlyExchangeRate__c>();
		for(Integer i = 0; i <= 11; i ++)
		{
			MonthlyExchangeRate__c rateMonth = new MonthlyExchangeRate__c();
			rateMonth.Year__c = theYear;
			rateMonth.Month__c = (i+1) + '';
			monthRateList.add(rateMonth);
		}
		insert monthRateList;
		
		List<ExchangeRateItem__c> rateItemList = new List<ExchangeRateItem__c>();
		for(Integer i = 0; i <= 11; i ++)
		{
			ExchangeRateItem__c rateItem = new ExchangeRateItem__c ();
			rateItem.MonthlyExchangeRate__c = monthRateList[i].Id;
			rateItem.CurrencyIsoCode = 'CNY';
			rateItem.ConversionRate__c = i+1; //每个月的汇率是不一样的，与月份相等。
			rateItemList.add(rateItem);
		}
		insert rateItemList;
    	
    	List<DB_Notes__c> dNList = new List<DB_Notes__c>();
    	for(Integer i = 1; i<=6; i++)
    	{
	    	DB_Notes__c dN1 = new DB_Notes__c();
	    	dN1.Amount__c = 5000;
	    	dN1.Date__c = Date.newInstance(theYear, i, 25);
	    	dN1.NO__c = 'DN00A' + i;
			dN1.Project_NO__c = 'C001';
			dN1.CurrencyIsoCode = null;
			dNList.add(dN1);
			DB_Notes__c dN2 = new DB_Notes__c();
	    	dN2.Amount__c = 5000;
	    	dN2.Date__c = Date.newInstance(theYear, i, 25);
	    	dN2.NO__c = 'DN00B' + i;
			dN2.Project_NO__c = 'C001';
			dN2.CurrencyIsoCode = null;
			dNList.add(dN2);
    	}
		upsert dNList NO__c;
		
		Test.startTest();
		
		ProjectForecastBatch pfBatch = new ProjectForecastBatch(theYear, true);
		Database.executeBatch(pfBatch, 50);
		
		Test.stopTest();
		
		//验证预测
		/*
		
		ForecastRow forRow;
		ForecastRow conRow;
		List<ForecastRow> rowList = forecastController.Rows;	
		for(ForecastRow row : rowList)
		{
			if(row.Opp.Id == opp1.Id)
			{
				if(row.RowType == ForecastRow.Type.Forecast)
				{
					forRow = row;
				}
				else if(row.RowType == ForecastRow.Type.Confirmed)
				{
					conRow = row;
				}
			}	
		}
		
		system.debug('SSSSSOPPA' + opp1.Amount);
		Decimal differ = opp1.Amount-(testSumRow(forRow) + testSumRow(conRow));
		
		system.Debug('*****dif:' + differ);
		system.Debug('*****forRow:' + testSumRow(forRow));
		system.Debug('*****comRow:' + testSumRow(conRow));
		system.Debug('*****forMonth1:' + forRow.Month1);
		system.Debug('*****forMonth2:' + forRow.Month2);
		system.Debug('*****forMonth3:' + forRow.Month3);
		system.Debug('*****forMonth4:' + forRow.Month4);
		system.Debug('*****forMonth5:' + forRow.Month5);
		system.Debug('*****forMonth6:' + forRow.Month6);
		system.Debug('*****forMonth7:' + forRow.Month7);
		system.Debug('*****forMonth8:' + forRow.Month8);
		system.Debug('*****forMonth9:' + forRow.Month9);
		system.Debug('*****forMonth10:' + forRow.Month10);
		system.Debug('*****forMonth11:' + forRow.Month11);
		system.Debug('*****forMonth12:' + forRow.Month12);
		system.Debug('*****conMonth1:' + conRow.Month1);
		system.Debug('*****conMonth2:' + conRow.Month2);
		system.Debug('*****conMonth3:' + conRow.Month3);
		system.Debug('*****conMonth4:' + conRow.Month4);
		system.Debug('*****conMonth5:' + conRow.Month5);
		system.Debug('*****conMonth6:' + conRow.Month6);
		system.Debug('*****conMonth7:' + conRow.Month7);
		system.Debug('*****conMonth8:' + conRow.Month8);
		system.Debug('*****conMonth9:' + conRow.Month9);
		system.Debug('*****conMonth10:' + conRow.Month10);
		system.Debug('*****conMonth11:' + conRow.Month11);
		system.Debug('*****conMonth12:' + conRow.Month12); 
		
		system.assert(differ <= 100 && differ >= -100);//因为平摊后的值之和与总是可能会产生误差，误差在一定范围内是允许的
		
		Decimal differ1 = conRow.Month1 - (conRow.USDMonth1 * 1);
		system.assert(differ1 <= 20 && differ1 >= -20);
		Decimal differ2 = conRow.Month2 - (conRow.USDMonth2 * 2);
		system.assert(differ2 <= 20 && differ2 >= -20);
		Decimal differ3 = conRow.Month3 - (conRow.USDMonth3 * 3);
		system.assert(differ3 <= 20 && differ3 >= -20);
		Decimal differ4 = conRow.Month4 - (conRow.USDMonth4 * 4);
		system.assert(differ4 <= 20 && differ4 >= -20);
		Decimal differ5 = conRow.Month5 - (conRow.USDMonth5 * 5);
		system.assert(differ5 <= 20 && differ5 >= -20);
		Decimal differ6 = conRow.Month6 - (conRow.USDMonth6 * 6);
		system.assert(differ6 <= 20 && differ6 >= -20);
		
		Decimal fdiffer7 = forRow.Month7 - (forRow.USDMonth7 * 7);
		system.assert(fdiffer7 <= 20 && fdiffer7 >= -20);
		Decimal fdiffer8 = forRow.Month8 - (forRow.USDMonth8 * 8);
		system.assert(fdiffer8 <= 20 && fdiffer8 >= -20);
		Decimal fdiffer9 = forRow.Month9 - (forRow.USDMonth9 * 9);
		system.assert(fdiffer9 <= 20 && fdiffer9 >= -20);
		Decimal fdiffer10 = forRow.Month10 - (forRow.USDMonth10 * 10);
		system.assert(fdiffer10 <= 20 && fdiffer10 >= -20);
		Decimal fdiffer11 = forRow.Month11 - (forRow.USDMonth11 * 11);
		system.assert(fdiffer11 <= 20 && fdiffer11 >= -20);
		Decimal fdiffer12 = forRow.Month12 - (forRow.USDMonth12 * 12);
		system.assert(fdiffer12 <= 20 && fdiffer12 >= -20);	
		*/			
    }
}