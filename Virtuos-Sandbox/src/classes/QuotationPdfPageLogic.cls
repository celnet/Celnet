public class QuotationPdfPageLogic {

    private final List<Deliverable__c> DeliverableList;
    private final Quote__c QuoteEnt;
    private final String ContactName;
    private final String AccountName;
    private final String DateStr;
    private final String ProjectName;
    private Double TotalManday = 0;
    private Double AssetPrice = 0;

    public List<Deliverable__c> getDeliverables(){
        return DeliverableList;
    }
    
    public Quote__c getQuoteEnt(){
        return QuoteEnt;
    }
    
    public String getContactName(){
        return ContactName;
    }
    
    public String getAccountName(){
        return AccountName;
    }
    
    public String getDateStr(){
        return DateStr;
    }
    
    public String getProjectName(){
        return ProjectName;
    }
    
    public Double getTotalManday(){
        return TotalManday;
    }
    
    public Double getAssetPrice(){
        return AssetPrice;
    }
    
    /*构造函数 页面接受数据并且取得列表*/
	public QuotationPdfPageLogic(ApexPages.StandardController controller){
    	//取Quote的Id
    	string QuoteID = ApexPages.currentPage().getParameters().get('id');
    	
    	//取Deliverable列表
		List<Deliverable__c> DeliverableDataList = [select Name, Total_Man_Days__c, Man_Day_Unit_Price__c, Asset_Price__c, Group__c, CategoryGroup__c, Sub_Group__c, Sub_Sub_Group__c 
									from Deliverable__c 
									where Quote__c =: QuoteID
									ORDER BY Group__c DESC,
											Sub_Group__c DESC,
											Sub_Sub_Group__c DESC
									];
		DeliverableList = MakeGroup(DeliverableDataList);
    	
    	//取Quote对象
    	QuoteEnt = [select Contact__c, Account__c, CreatedDate, Unit__c, CurrencyIsoCode from Quote__c where Id =: QuoteID];
    	//取联系人名字
    	if(QuoteEnt.Contact__c != null){
    		Contact NameContact = [select Name from Contact where Id =: QuoteEnt.Contact__c];
    		ContactName = NameContact.Name;
    	}
    	
    	//取AccountName
    	if(QuoteEnt.Account__c != null){
    		Account NameAcc = [select Name from Account where Id =:QuoteEnt.Account__c];
    		AccountName = NameAcc.Name;
    	}
    	
    	//取date
		if(QuoteEnt.CreatedDate != null){
			datetime CreateDate = QuoteEnt.CreatedDate;
			DateStr = CreateDate.month() + '-' + CreateDate.day() + '-' + CreateDate.year().format().substring(3);
		}

		//取ProjectName,uote与Project是一对一的关系,取第0条
		List<Project__c> ProjectList = [select Name from Project__c where Quote__c =: QuoteID ];
   		if(ProjectList != null && ProjectList.size()>0){
   			ProjectName = ProjectList[0].Name;
   		}
    }
    
    private List<Deliverable__c> MakeGroup(List<Deliverable__c> DeliverableDataList){
    	List<Deliverable__c> ReList = new List<Deliverable__c>();
    	if(DeliverableDataList != null && DeliverableDataList.size() > 0){
    		string GroupFlag = '';
    		string SubGroupFlag = '';
    		string SubSubGroupFlag = '';
    		for(Integer i=0;i<DeliverableDataList.size();i++){
    			Deliverable__c Deliver = DeliverableDataList[i];
    			//如果是一个新Group,则插入三条,且标志位替换
    			if(Deliver.Group__c != GroupFlag){
    				Deliverable__c GroupDeliver = new Deliverable__c();
    				GroupDeliver.Name = Deliver.Group__c;
    				GroupDeliver.Tr_Sytle__c = '#c0c0c0';
    				
    				Deliverable__c SubGroupDeliver = new Deliverable__c();
    				SubGroupDeliver.Group__c = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    				SubGroupDeliver.Name = Deliver.Sub_Group__c;
    				
    				Deliverable__c SubSubGroupDeliver = new Deliverable__c();
    				SubSubGroupDeliver.Group__c = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    				SubSubGroupDeliver.Name = Deliver.Sub_Sub_Group__c;
    				
    				ReList.add(GroupDeliver);
    				ReList.add(SubGroupDeliver);
    				ReList.add(SubSubGroupDeliver);
    				GroupFlag = Deliver.Group__c;
    				SubGroupFlag = Deliver.Sub_Group__c;
    				SubSubGroupFlag = Deliver.Sub_Sub_Group__c;
    			}
    			else if(Deliver.Sub_Group__c != SubGroupFlag){
    				//如果是一个新SubGroup,则插入两条,且标志位替换
    				Deliverable__c SubGroupDeliver = new Deliverable__c();
    				SubGroupDeliver.Group__c = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    				SubGroupDeliver.Name = Deliver.Sub_Group__c;
    				
    				Deliverable__c SubSubGroupDeliver = new Deliverable__c();
    				SubSubGroupDeliver.Group__c = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    				SubSubGroupDeliver.Name = Deliver.Sub_Sub_Group__c;
    				
    				ReList.add(SubGroupDeliver);
    				ReList.add(SubSubGroupDeliver);
    				SubGroupFlag = Deliver.Sub_Group__c;
    				SubSubGroupFlag = Deliver.Sub_Sub_Group__c;    			
    			}
    			else if(Deliver.Sub_Sub_Group__c != SubSubGroupFlag){
    				//如果是一个新SubSubGroup,则插入一条,且标志位替换
    				Deliverable__c SubSubGroupDeliver = new Deliverable__c();
    				SubSubGroupDeliver.Group__c = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    				SubSubGroupDeliver.Name = Deliver.Sub_Sub_Group__c;
    				ReList.add(SubSubGroupDeliver);
    				
    				SubSubGroupFlag = Deliver.Sub_Sub_Group__c;    			
    			}
    			Deliver.Group__c = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    			ReList.add(Deliver);
    			if(Deliver.Total_Man_Days__c != null){
    				TotalManday += Deliver.Total_Man_Days__c;
    			}
    			if(Deliver.Asset_Price__c != null){
    				AssetPrice += Deliver.Asset_Price__c;
    			}
    		}
    	}
    	return ReList;
    }
    
}