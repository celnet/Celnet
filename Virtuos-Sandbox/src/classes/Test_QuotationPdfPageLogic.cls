@isTest
private class Test_QuotationPdfPageLogic {

    static testMethod void myUnitTest() { 
    	Date theDate = date.newinstance(2010, 1, 11);
    	   	
    	Account testAcc = new Account();
    	testAcc.Name = 'test Account';
    	insert testAcc;
    	
    	Contact testCon = new Contact();
    	testCon.LastName = 'testCon';
    	insert testCon;
    	
    	Opportunity InsertOpp = new Opportunity();
    	InsertOpp.Name = 'testOpp';
    	InsertOpp.CloseDate = theDate;
    	InsertOpp.StageName = 'Qualifying';
    	InsertOpp.Type = 'Dev';
    	InsertOpp.Subtype__c = 'Full game';
    	InsertOpp.Project_End_Date__c = Date.newInstance(2012, 12, 5);
    	insert InsertOpp;
    	
    	Quote__c testQuote = new Quote__c();
    	testQuote.Opportunity__c = InsertOpp.Id;
    	testQuote.Contact__c = testCon.Id;
    	testQuote.Account__c = testAcc.Id;
    	testQuote.Name = 'test Quote';
    	insert testQuote;
    	
    	
    	Project__c testProject = new Project__c();
    	testProject.Actual_End__c = theDate;
    	testProject.Client__c = testAcc.Id;
    	testProject.Delay_Days__c = 3;
    	testProject.Name = 'test Project';
    	testProject.Plan_End__c = theDate;
    	testProject.Plan_Start__c = theDate;
    	testProject.Project_Status__c = 'Start';
    	testProject.Quote__c = testQuote.Id;
    	
    	insert testProject;
    	
    	Project_Milestone__c testMilestone = new Project_Milestone__c();
    	testMilestone.Name = 'test Milestone';
    	testMilestone.Project_Milestone_No__c = 'P002M004';
    	testMilestone.Quote__c = testQuote.Id;
    	
    	insert testMilestone;
    	
    	Project_Milestone__c testMilestone2 = new Project_Milestone__c();
    	testMilestone2.Name = 'test Milestone';
    	testMilestone2.Project_Milestone_No__c = 'P002M005';
    	testMilestone2.Quote__c = testQuote.Id;
    	
    	insert testMilestone2;
    	
    	Deliverable__c testDeliver = new Deliverable__c();
    	testDeliver.Category__c = 'Art';
    	testDeliver.Deliverable_No__c = 'D0019381713';
    	testDeliver.Group__c = 'Fash';
    	testDeliver.Man_Day_Unit_Price__c = 234;
    	testDeliver.Name = 'test Deliver';
    	testDeliver.Sub_Group__c = 'Concept';
    	testDeliver.Sub_Sub_Group__c = 'Cart';
    	testDeliver.Total_Man_Days__c = 14;
    	testDeliver.Project__c = testProject.Id;
    	testDeliver.Project_Milestone__c = testMilestone.Id;
    	testDeliver.Quote__c = testQuote.Id; 	
    	
    	insert testDeliver;
    	
    	Deliverable__c testDeliver3 = new Deliverable__c();
    	testDeliver3.Category__c = 'Art';
    	testDeliver3.Deliverable_No__c = 'D0019381733';
    	testDeliver3.Group__c = 'Fash';
    	testDeliver3.Man_Day_Unit_Price__c = 234;
    	testDeliver3.Name = 'test Deliver';
    	testDeliver3.Sub_Group__c = 'Vehicle';
    	testDeliver3.Sub_Sub_Group__c = 'Cart';
    	testDeliver3.Total_Man_Days__c = 14;
    	testDeliver3.Project__c = testProject.Id;
    	testDeliver3.Project_Milestone__c = testMilestone.Id;
    	testDeliver3.Quote__c = testQuote.Id; 	
    	
    	insert testDeliver3;
    	
    	Deliverable__c testDeliver4 = new Deliverable__c();
    	testDeliver4.Category__c = 'Art';
    	testDeliver4.Deliverable_No__c = 'D0019381743';
    	testDeliver4.Group__c = 'Fash';
    	testDeliver4.Man_Day_Unit_Price__c = 234;
    	testDeliver4.Name = 'test Deliver';
    	testDeliver4.Sub_Group__c = 'Vehicle';
    	testDeliver4.Sub_Sub_Group__c = 'Cartfw';
    	testDeliver4.Total_Man_Days__c = 14;
    	testDeliver4.Project__c = testProject.Id;
    	testDeliver4.Project_Milestone__c = testMilestone.Id;
    	testDeliver4.Quote__c = testQuote.Id; 	
    	
    	insert testDeliver4;

    	Deliverable__c testDeliver2 = new Deliverable__c();
    	testDeliver2.Category__c = 'Porting';
    	testDeliver2.Deliverable_No__c = 'D0019381714';
    	testDeliver2.Group__c = 'Cat';
    	testDeliver2.Man_Day_Unit_Price__c = 234;
    	testDeliver2.Name = 'test Deliver';
    	testDeliver2.Sub_Group__c = 'Engineering';
    	testDeliver2.Sub_Sub_Group__c = 'HelloCat';
    	testDeliver2.Total_Man_Days__c = 14;
    	testDeliver2.Project__c = testProject.Id;
    	testDeliver2.Project_Milestone__c = testMilestone2.Id;
    	testDeliver2.Quote__c = testQuote.Id;
    	
    	insert testDeliver2;

    	System.debug('*******************' + testQuote.Contact__c);

		ApexPages.StandardController con = new ApexPages.StandardController(new Quote__c());
		ApexPages.currentPage().getParameters().put('id',testQuote.Id);
		QuotationPdfPageLogic testQuotePdf = new QuotationPdfPageLogic(con);
		testQuotePdf.getDeliverables();
		testQuotePdf.getQuoteEnt();
		testQuotePdf.getContactName();
		testQuotePdf.getAccountName();
		testQuotePdf.getDateStr();
		testQuotePdf.getProjectName();
		testQuotePdf.getTotalManday();
		testQuotePdf.getAssetPrice();
    }
}