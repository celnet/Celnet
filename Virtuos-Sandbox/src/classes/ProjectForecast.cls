public class ProjectForecast 
{
    public class ProjectForecastException extends Exception {} 
    private Integer year; 
    private date lastDNDateConfig;
    public List<ForecastRow> rowList  = new List<ForecastRow>();
    private map<Integer, map<Integer, MonthlyExchangeRate__c>> exchangeRateMap;//存储历史汇率表， key1指定年份2011， key2指定月份 1-12
    public ProjectForecast(Integer year, map<Integer, map<Integer, MonthlyExchangeRate__c>> exchangeRateMap)
    {
        this.year = year;
        this.exchangeRateMap = exchangeRateMap;
        this.lastDNDateConfig =  Virtuos_Config__c.getValues('LastDNDateConfig').Value__c;
    }
    public ProjectForecast(Integer year)
    {
        this.year = year;
         this.lastDNDateConfig =  Virtuos_Config__c.getValues('LastDNDateConfig').Value__c;
    }
    
    private String GenerateCID(ForecastRow row)
    {
        return Row.Opp.Id + '-' + this.year + '-' + row.RowType.Name();
    }
    
    private void Save()
    {
        system.debug('*****p' + rowList.size());
        if(this.rowList.size() == 0)
        {
            return;
        }
        DateTime updateOn = system.now();
        List<Project_Forecast__c> pfList = new List<Project_Forecast__c>();
        for(ForecastRow row : this.rowList)
        {
            Project_Forecast__c pf = new Project_Forecast__c();
            if(row.Opp == null)
            {
                throw new ProjectForecastException('Opp of ForecastRow cannot be null');
            }
            pf.Opp__c = row.Opp.Id;
            pf.CID__c = this.GenerateCID(row);
            pf.CurrencyIsoCode = 'USD';
            pf.Amount__c = row.USDPAmount;
            pf.Month1__c = row.USDMonth1;
            pf.Month2__c = row.USDMonth2;
            pf.Month3__c = row.USDMonth3;
            pf.Month4__c = row.USDMonth4;
            pf.Month5__c = row.USDMonth5;
            pf.Month6__c = row.USDMonth6;
            pf.Month7__c = row.USDMonth7;
            pf.Month8__c = row.USDMonth8;
            pf.Month9__c = row.USDMonth9;
            pf.Month10__c = row.USDMonth10;
            pf.Month11__c = row.USDMonth11;
            pf.Month12__c = row.USDMonth12;
            pf.Type__c = row.RowType.name();
            pf.LastUpdate__c = updateOn;
            pf.Year__c = this.year;
            pfList.add(pf);
        }
        if(pfList.size() == 0)
        {
            return;
        }
        upsert pfList CID__c;
    }
    
    //针对指定的业务机会列表计算预测数据，并存储到预测数据表中
    public void Calculate(List<SObject> oppList) 
    {
        system.debug('*****p' + oppList.size());
        this.rowList.clear();
        Date toDay = Date.today();
        for(SObject obj : oppList)
        {
            Opportunity opp = (Opportunity)obj;
            Date projStartDate = opp.CloseDate;
            Date projEndDate = opp.Project_End_Date__c;
            
            //是否在当前的年份需要计算的业务机会
            if(projEndDate == null || projStartDate == null)
            {
                system.debug('*****' + opp.Name);
                system.debug('*****p1');
                continue;
            }
            if(projEndDate < projStartDate)
            {
                system.debug('*****p2');
                continue;
            }
            //2013-4-25 del,
            //if(projStartDate.year() < this.year && projEndDate.year() > this.year)//存在BUG，未计算头尾跨年的记录
            //{
            //  system.debug('*****p3');
            //  continue;
            //}
            
            Decimal oppAmount = 0;
            Double p = 1.0;
            if(opp.Probability != null)
            {
                p = opp.Probability/100;
            }
            if(opp.Amount != null)
            {
                //oppAmount = opp.Amount;//业务机会总金额
                oppAmount = opp.Amount * p;//业务机会总金额
            }
            Decimal dNSUMAmount = 0;//Debit Notes 总金额，也是实际金额
            map<Integer, Decimal> dNMonthAmountMap = new map<Integer, Decimal>();//Debit Notes在当前年的每个月的汇总金额，跨过当前年不算
            //Date lastDNDate = opp.CloseDate; //最后的Debit Notes日期,初始值是项目开始日期
            Date lastDNDate = opp.CloseDate.addDays(-1); //Tommy modified at 2013-5-16， LastDNDate的后一天是预测的开始
            if(this.lastDNDateConfig != null)
            {
                if(lastDNDate < this.lastDNDateConfig)
                {
                    lastDNDate = this.lastDNDateConfig;
                }
            }
            if(Opp.Debit_Notes__r != null && Opp.Debit_Notes__r.size() > 0)
            {
                for(DB_Notes__c  dN : Opp.Debit_Notes__r)
                {
                    if(dN == null)
                    {
                        continue;
                    }
                    //Tommy del at 2013-7-3, 本逻辑被废弃,按照用户手填写的最后 DN时间为准
                    //if(dN.Date__c > lastDNDate)
                    //{
                    //  lastDNDate = dN.Date__c;
                    //}
                    if(dN.Amount__c != null)//tommy added at 2012-12-12, 修复执行错误：Argument 1 cannot be null
                    {
                        dNSUMAmount += dN.Amount__c;
                    }
                    if(dN.Date__c != null && dN.Date__c.year() == this.year)
                    {
                        Integer dBmonth = dN.Date__c.month();
                        if(!dNMonthAmountMap.containsKey(dBmonth))
                        {
                            dNMonthAmountMap.put(dBmonth, 0);
                        }
                        Decimal am = dNMonthAmountMap.get(dBmonth);
                        if(am == null)
                        {
                            am = 0;
                        }
                        if(dN.Amount__c != null)
                        {
                            dNMonthAmountMap.put(dBmonth, am + dN.Amount__c);
                        }
                    }   
                }
            }
            //Tommy del at 2013-7-3, 本逻辑被废弃
            //else//Tommy added at 2013-5-16, 处理没有任何DN导入的情况，当前日期向前移动2个月来作为LastDNDate
            //{
            //  Date firstDayOfMonth = toDay.toStartOfMonth();
            //  Date forcastStartDay = firstDayOfMonth.addMonths(-1);
            //  if(forcastStartDay < opp.CloseDate)
            //  {
            //      forcastStartDay = opp.CloseDate;
            //  }
            //  lastDNDate = forcastStartDay.addDays(-1);
            //}
            //构建预测行
            ForecastRow foreRow = new ForecastRow(this.year, this.exchangeRateMap);
            foreRow.Account = opp.Account;
            foreRow.Opp = opp;
            foreRow.RowType = ForecastRow.Type.Forecast;
            Integer daysBetween = lastDNDate.daysBetween(projEndDate);
            System.Debug('##########1' + daysBetween);
            if(daysBetween > 0)
            {
                Decimal amountEveryDay = (oppAmount - dNSUMAmount) / daysBetween;//每天的预测分摊金额
                List<Integer> daysInMonthList = CalculateDaysInMonth(lastDNDate, projEndDate); //从最后的Debit Notes到Project End Date之间的月份每个月有几天
                Integer cout = 0;
                for(Integer i = 1; i < 13; i ++)
                {
                    if(daysInMonthList[i] != null && daysInMonthList[i] != 0)
                    {
                        foreRow.AddValue(i, daysInMonthList[i] * amountEveryDay);
                        cout += daysInMonthList[i];
                    }
                }
                //system.assertEquals(daysBetween, cout);跨年不适用
            }
            this.rowList.add(foreRow);
            
            //构建实际行
            ForecastRow confRow = new ForecastRow(this.year, this.exchangeRateMap);
            confRow.Account = opp.Account;
            confRow.Opp = opp;
            confRow.RowType = ForecastRow.Type.Confirmed;
            Set<Integer> monthSet = dNMonthAmountMap.keySet();
            for(Integer month : monthSet)
            {
                confRow.AddValue(month, dNMonthAmountMap.get(month));
            }
            this.rowList.add(confRow);
        }
        this.Save();
    }
    
        //计算从最后的Debit Notes到Project End Date之间的月份每个月有几天 
    public List<Integer> CalculateDaysInMonth(Date lastDNDate, Date projEndDate)
    {
        List<Integer> daysInMonthList = new Integer[13];
        if(projEndDate <= lastDNDate)//对项目结束日期早于最后的Debit Notes 日期不做预测的计算（非常重要）
        {
            return daysInMonthList;
        }
         //从最后的Debit Notes到Project End Date之间的月份每个月有几天 
        Integer monthsBetween = lastDNDate.monthsBetween(projEndDate);
        if(monthsBetween == 0)//在同一个月的情况
        {
            if(lastDNDate.Year() == this.year)
            {
                daysInMonthList[lastDNDate.month()] = lastDNDate.daysBetween(projEndDate);
            }
        }
        else if(monthsBetween > 0)//不在同一个月，夸一个以上的月
        {
            Date startOfMonth = projEndDate.toStartOfMonth();
            if(projEndDate.Year() == this.year)
            {
                daysInMonthList[projEndDate.month()] = startOfMonth.daysBetween(projEndDate) + 1;//计算最后一个月的实际天数
            }
            
            //下面试图向前按月为单位搜索每个月的天数，直到紧相邻Debit Notes最后日期所在的月份结束
            startOfMonth = startOfMonth.addDays(-1);
            Date endOfMonth;
            while(lastDNDate.monthsBetween(startOfMonth) > 0)//如果紧相邻Debit Notes最后日期所在的月份结束
            {
                endOfMonth = startOfMonth;//复制保存上个月的最后一天
                startOfMonth = endOfMonth.toStartOfMonth();//计算上个月的第一天
                if(endOfMonth.Year() == this.year)
                {
                    daysInMonthList[endOfMonth.month()] = startOfMonth.daysBetween(endOfMonth) + 1;
                }
                startOfMonth = startOfMonth.addDays(-1);
            }
            //计算Debit Notes所在的那个月份
            endOfMonth = startOfMonth;
            if(lastDNDate.Year() == this.year)
            {
                daysInMonthList[lastDNDate.month()] = lastDNDate.daysBetween(endOfMonth);
            }
        }
        return daysInMonthList;
    }
}