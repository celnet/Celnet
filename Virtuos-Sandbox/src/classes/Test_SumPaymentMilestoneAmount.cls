@isTest
private class Test_SumPaymentMilestoneAmount 
{
    static testMethod void runTest() 
    {
		PO__c InsertPO = new PO__c();
		InsertPO.Name = 'InsertPO';
		insert InsertPO;
		
		PO_Project__c InsertPoPro = new PO_Project__c();
		InsertPoPro.Name = 'insertPOProject';
		InsertPoPro.PO__c = InsertPO.Id;
		InsertPoPro.PO_Project_No__c = 'P12345667';
		insert InsertPoPro;
		
		PO_Deliverable__c insertDeliver = new PO_Deliverable__c();
		insertDeliver.Name = 'test Deliver';
		insertDeliver.Total_Man_Days__c = 2;
		insertDeliver.Complete_Rate__c = 100;
		insertDeliver.PO__c = InsertPO.Id;
		insertDeliver.PO_Project__c = InsertPoPro.Id;
		insert insertDeliver;
		
		Invoice__c payMlstone1 = new Invoice__c(Name = 'Test_Payment Milestone1', PO__c = InsertPO.Id);
		Invoice__c payMlstone2 = new Invoice__c(Name = 'Test_Payment Milestone1', PO__c = InsertPO.Id);
		
		insert payMlstone1;
		insert payMlstone2;
		//Database.SaveResult re1 = Database.insert(payMlstone1, true);
		//Database.SaveResult re2 = Database.insert(payMlstone2, true);
		
		PO_Deliverable__c del_p1 = new PO_Deliverable__c(Name='For p1', Man_Day_Unit_Price__c = 20 ,Total_Man_Days__c = 1, Payment_Milestone__c = payMlstone1.Id, PO__c = InsertPO.Id, PO_Project__c = InsertPoPro.Id);
		PO_Deliverable__c del_p2 = new PO_Deliverable__c(Name='For p2', Man_Day_Unit_Price__c = 10 ,Total_Man_Days__c = 1, Payment_Milestone__c = payMlstone2.Id, PO__c = InsertPO.Id, PO_Project__c = InsertPoPro.Id);
		PO_Deliverable__c del_p1_2 = new PO_Deliverable__c(Name='For p1', Man_Day_Unit_Price__c = 10 ,Total_Man_Days__c = 1, Payment_Milestone__c = payMlstone1.Id, PO__c = InsertPO.Id, PO_Project__c = InsertPoPro.Id);
		insert(new PO_Deliverable__c [] {del_p1, del_p2} );
		insert del_p1_2;
		//Database.SaveResult delRe3 = Database.insert(del_p1_2, true);
		
		//System.assertEquals(30, [Select Amount__c From Invoice__c Where Id = :payMlstone1.Id].Amount__c);
		//System.assertEquals(10, [Select Amount__c From Invoice__c Where Id = :payMlstone2.Id].Amount__c);
		del_p1_2 = [Select Id, Payment_Milestone__c From PO_Deliverable__c Where Id = : del_p1_2.Id];
		del_p1_2.Payment_Milestone__c = payMlstone2.Id;
		
		update del_p1_2;//从P2更新到P1
		//System.assertEquals(20, [Select Amount__c From Invoice__c Where Id = :payMlstone1.Id].Amount__c);
		//System.assertEquals(20, [Select Amount__c From Invoice__c Where Id = :payMlstone2.Id].Amount__c);
		
		delete new PO_Deliverable__c(Id = del_p1_2.Id);
		//System.assertEquals(20, [Select Amount__c From Invoice__c Where Id = :payMlstone1.Id].Amount__c);
		//System.assertEquals(10, [Select Amount__c From Invoice__c Where Id = :payMlstone2.Id].Amount__c);
    	
    }
}