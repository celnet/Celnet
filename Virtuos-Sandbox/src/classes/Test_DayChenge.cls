@isTest
private class Test_DayChenge {

    static testMethod void myUnitTest() {
      
       Account insAcc = new Account();
	  insAcc.Name = 'frensworkz';
	  insAcc.Payment_Terms__c = '10 days'; //一开始 是最晚10天
	  insert insAcc;
	   
	   PO__c TestPo = new PO__c();
		TestPo.Name = 'test po';
		insert TestPo;
	   
	    /*PO__c TestPo1 = new PO__c();
    	TestPo1.Name = 'testinsertPO';
    	insert TestPo1;*/
    	//因为有一个 命名Trigger 要求必须 写PO_Project__c 且 NO也必须写所以
    	
    	PO_Project__c TestPoProject = new PO_Project__c();
		TestPoProject.Name = 'Test Project';
		TestPoProject.PO__c = TestPo.Id;
		TestPoProject.PO_Project_No__c = 'P12345667';
		insert TestPoProject;
		
    	
		/*PO_Project__c TestPoProject1 = new PO_Project__c();
		 TestPoProject1.PO_Project_No__c = '11232132';
		 TestPoProject1.Name = 'poproTest';
	 	 TestPoProject1.PO__c = TestPo1.Id;
		 insert TestPoProject1;*/
	 
	  Invoice__c invoice = new Invoice__c();
	   invoice.Name ='testTEst@@';
	   invoice.Amount__c = 2000;
	   invoice.Client__c = insAcc.Id;
	   invoice.PO__c = TestPo.Id;
	   invoice.Received_Amount__c = 1000;
	   invoice.Invoice_Sent_Date__c = date.newInstance(2011,1,20);
	   insert invoice;
	  
	   Account acc = [SELECT Id,Payment_Terms_Num__c,Invoice_Amount_Sum__c, Invoice_Due_Amount_Sum__c, 
	   				  Invoice_Received_Amount_Sum__c FROM Account 
	   				  where id =: insAcc.Id];
	   System.debug('！！！！！！！！！！！！！！！！'+acc.Id);				  
	   System.debug('！！！！！！！！！！！！！！！！！'+ acc.Payment_Terms_Num__c);//测试正常的话 应该值为 10
	   System.debug('！！！！！！！！！！！！！！！'+ acc.Invoice_Amount_Sum__c);//总计应为 2000
	   System.debug('1！！！！！！！！！！！！！'+ acc.Invoice_Received_Amount_Sum__c);//收到应为1000
	   System.debug('！！！！！！！！！！！！！！'+ acc.Invoice_Due_Amount_Sum__c);//拖欠应为1000
	   
	   insAcc.Payment_Terms__c = '60 days';
	   update insAcc;
	   
	   System.debug('！！！！！！！！！！！！！！！！！'+ acc.Payment_Terms_Num__c);//测试正常的话 应该值为 60
	    System.debug('！！！！！！！！！！！！！！！！！！！！'+ acc.Invoice_Due_Amount_Sum__c);//拖欠应为0
    }
}