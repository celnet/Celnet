/*
 * 开发者：Ziyuegao
 * 开发时间：2013-5-15
 * 功能描述：通过计划实现：每个月月初为每条业务机会创建一条该月的业务机会历史
*/
global class AutoOpportunity_HistoryBatch implements Database.Batchable<sObject>
{
	global database.Querylocator start(Database.BatchableContext bc) 
	{
		Integer y=Date.today().year();
		Integer m=Date.today().month();
		string Month;
		if(m>9)
		{
			Month=y+'-'+m;
		}
		else
		{
			Month=y+'-0'+m;
		}
		return database.getQueryLocator([Select StageName, CreatedDate,CreatedById,Probability, OwnerId, LastModifiedDate, LastModifiedById, IsWon, 
		                                IsDeleted, ForecastCategory, CloseDate, Amount, (Select OwnerId, IsDeleted, Name, 
		                                From_Stage__c, Amount__c, Probability__c, Close_Date__c, Won__c, Last_Modified__c,
		                                Last_Modified_By__c, Forecast_Category__c, Month__c, Opportunity_Name__c 
		                                From Qualified_Opportunity_Historys_Oppor__r where Month__c=:Month) From Opportunity where IsClosed__c=false	                 
                                        order by LastModifiedDate Desc limit 50000]);
	}
	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		Integer y=Date.today().year();
		Integer m=Date.today().month();
		string Month;
		if(m>9)
		{
			Month=y+'-'+m;
		}
		else
		{
			Month=y+'-0'+m;
		}
		if(scope!=null&&scope.size()>0)
		{
			list<Qualified_Opportunity_History__c> in_qoh=new list<Qualified_Opportunity_History__c>();
			for(sObject sc:scope)
			{
				Opportunity opp=(Opportunity) sc;
				if(opp.Qualified_Opportunity_Historys_Oppor__r==null||opp.Qualified_Opportunity_Historys_Oppor__r.size()==0)
				{
					Qualified_Opportunity_History__c qoh=new Qualified_Opportunity_History__c();
					qoh.Opportunity_Name__c=opp.id;
					qoh.Amount__c=opp.Amount;
					qoh.Close_Date__c=opp.CloseDate;
					qoh.Forecast_Category__c=opp.ForecastCategory;
					qoh.From_Stage__c=opp.StageName;
					qoh.Last_Modified__c=opp.CreatedDate;
					qoh.Last_Modified_By__c=opp.CreatedById;
					qoh.Month__c=Month;
					qoh.Probability__c=opp.Probability;
					qoh.To_Stage__c=opp.StageName;
					qoh.OwnerId=opp.OwnerId;
					in_qoh.add(qoh);
				}
			}
			if(in_qoh!=null&&in_qoh.size()>0)
			{
				insert in_qoh;
			}
		}
	}
	global void finish(Database.BatchableContext BC)
	{
	
	}
}