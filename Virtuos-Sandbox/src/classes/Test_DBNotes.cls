@isTest
private class Test_DBNotes 
{
    static testMethod void testDBNotesTrigger() 
    {
    	Account acc = new Account();
    	acc.Name = 'T_Test Account';
    	insert acc;
    	Opportunity opp1 = new Opportunity();
    	opp1.Name = 'Opp1';
    	opp1.AccountId = acc.Id;
    	opp1.StageName = 'Approved';
    	opp1.CloseDate = Date.today();
    	opp1.Project_End_Date__c = Date.today() + 30;
    	Opportunity opp2 = new Opportunity();
    	opp2.Name = 'Opp2';
    	opp2.AccountId = acc.Id;
    	opp2.StageName = 'Approved';
    	opp2.CloseDate = Date.today();
    	opp2.Project_End_Date__c = Date.today() + 30;
    	insert new Opportunity[] {opp1, opp2};
    	Project__c prj1 = new Project__c();
    	prj1.Name = 'Project1';
    	prj1.Opportunity__c = opp1.Id;
    	prj1.Project_No__c = 'C001';
    	Project__c prj2 = new Project__c();
    	prj2.Name = 'Project2';
    	prj2.Opportunity__c = opp2.Id;
    	prj2.Project_No__c = 'C002';
    	insert new Project__c[]{prj1, prj2};
    	
    	DB_Notes__c dN1 = new DB_Notes__c();
    	dN1.Amount__c = 200;
    	dN1.Date__c = Date.today();
    	dN1.NO__c = 'DN001';
		dN1.Project_NO__c = 'C001';
		DB_Notes__c dN2 = new DB_Notes__c();
    	dN2.Amount__c = 200;
    	dN2.Date__c = Date.today();
    	dN2.NO__c = 'DN002';
		dN2.Project_NO__c = 'C001';
		List<DB_Notes__c> dNList = new DB_Notes__c[] {dN1, dN2};
		upsert dNList NO__c;
		
		DB_Notes__c newDN1 = [Select ID, NO__c, Project_NO__c, Project__c, Opportunity__c From DB_Notes__c Where Id =: DN1.Id];
		DB_Notes__c newDN2 = [Select ID, NO__c, Project_NO__c, Project__c, Opportunity__c From DB_Notes__c Where Id =: DN2.Id];
		System.assertEquals(prj1.ID, newDN1.Project__c);
		System.assertEquals(prj1.ID, newDN2.Project__c);
		System.assertEquals(prj1.Opportunity__c, newDN1.Opportunity__c);
		System.assertEquals(prj1.Opportunity__c, newDN2.Opportunity__c);
		
		dN1.Project_NO__c = 'C002';
		dN2.Project_NO__c = 'C002';
		upsert dNList NO__c;
		
		DB_Notes__c new2DN1 = [Select ID, NO__c, Project_NO__c, Project__c, Opportunity__c From DB_Notes__c Where Id =: DN1.Id];
		DB_Notes__c new2DN2 = [Select ID, NO__c, Project_NO__c, Project__c, Opportunity__c From DB_Notes__c Where Id =: DN2.Id];
		System.assertEquals(prj2.ID, new2DN1.Project__c);
		System.assertEquals(prj2.ID, new2DN2.Project__c);
		System.assertEquals(prj2.Opportunity__c, new2DN1.Opportunity__c);
		System.assertEquals(prj2.Opportunity__c, new2DN2.Opportunity__c);	
    }
}