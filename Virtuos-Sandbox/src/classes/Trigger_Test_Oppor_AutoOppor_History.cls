/**
 * 开发者：Ziyuegao
 * 开发时间：2013-5-15
 * 功能描述：Trigger.Opportunity_AutoOpportunity_History的测试类
 * and triggers.
 */
@isTest
private class Trigger_Test_Oppor_AutoOppor_History 
{
    static testMethod void myUnitTest() 
    {
    	system.Test.startTest();
        Opportunity opp=new Opportunity();
		opp.Name='opp';
		opp.CloseDate=Date.today().addMonths(1);
		opp.StageName='Approved';
		opp.Project_End_Date__c=Date.today().addMonths(1);
		insert opp;
		opp.CloseDate=Date.today().addMonths(2);
		opp.StageName='Closed Won(Signed PO or contract)';
		update opp;
		list<Qualified_Opportunity_History__c> qohs=[select id from Qualified_Opportunity_History__c where Opportunity_Name__c=:opp.id];
		delete qohs;
		opp.CloseDate=Date.today().addMonths(1);
		update opp;
		system.Test.stopTest();
    }
}