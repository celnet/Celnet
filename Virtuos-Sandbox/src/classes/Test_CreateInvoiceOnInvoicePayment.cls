/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_CreateInvoiceOnInvoicePayment {

    static testMethod void myUnitTest() {
    	
    	Date testDate = date.newInstance(2010, 11, 11);
    	
		PO__c TestPo = new PO__c();
		TestPo.Name = 'test po';
		insert TestPo;
		
		PO_Project__c TestPoProject = new PO_Project__c();
		TestPoProject.Name = 'Test Project';
		TestPoProject.PO__c = TestPo.Id;
		TestPoProject.PO_Project_No__c = 'P12345667';
		insert TestPoProject;
		
		Invoice__c TestInv = new Invoice__c();
		TestInv.Name = 'TestInv';
		TestInv.PO__c = TestPo.Id;
		TestInv.Should_Invoice_Date__c = testDate;
		
		insert TestInv;
		
		CreateInvoiceOnInvoicePayment.CreateInvoiceNo(TestInv.Id);

		TestInv.Invoice_No__c = 'I1110011';
		update TestInv;
		
		CreateInvoiceOnInvoicePayment.CreateInvoiceNo(TestInv.Id);

		TestInv.Invoice_No__c = 'I1210011';
		update TestInv;
		
		CreateInvoiceOnInvoicePayment.CreateInvoiceNo(TestInv.Id);
		
		Invoice__c TestInv2 = new Invoice__c();
		TestInv2.Name = 'TestInv';
		TestInv2.PO__c = TestPo.Id;
		
		insert TestInv2;
		
		CreateInvoiceOnInvoicePayment.CreateInvoiceNo(TestInv2.Id);
		
		Invoice__c TestInv3 = new Invoice__c();
		TestInv3.Name = 'TestInv';
		TestInv3.PO__c = TestPo.Id;
		TestInv3.Should_Invoice_Date__c = testDate;
		
		insert TestInv3;
		
		CreateInvoiceOnInvoicePayment.CreateInvoiceNo(TestInv3.Id);
		
    	Date testDate1 = date.newInstance(2010, 5, 5);
		Invoice__c TestInv4 = new Invoice__c();
		TestInv4.Name = 'TestInv';
		TestInv4.PO__c = TestPo.Id;
		TestInv4.Should_Invoice_Date__c = testDate1;
		
		insert TestInv4;
		
		CreateInvoiceOnInvoicePayment.CreateInvoiceNo(TestInv4.Id);
    }
}