@isTest
//测试DebitNotesRateBatch
//开发人:米立业
private class Test_DebitNotesRatesBatch {

    static testMethod void myUnitTest() {
    	MonthlyExchangeRate__c monthRate = new MonthlyExchangeRate__c();
		monthRate.Year__c = 1982;
		monthRate.Month__c = '7';
		insert monthRate;
		
		List<ExchangeRateItem__c> rateItems = [select id, CurrencyIsoCode,
        									   ConversionRate__c,Corporate__c from 
        									   ExchangeRateItem__c where MonthlyExchangeRate__c = :monthRate.Id];
		
		PO__c po = new PO__c();
		po.Name = 'TestDebitBatchPO';		
		insert po;
		
		PO_Project__c poProject = new PO_Project__c();
		poProject.Name = 'TestDebitNotesBatchPO_Project';
		poProject.PO_Project_No__c = '00000001xxx';
		poProject.PO__c = po.Id;
		insert poProject;
		
		List<Debit_Notes__c> debitList = new List<Debit_Notes__c>();
		Debit_Notes__c debit = new Debit_Notes__c();
		debit.Name = 'TestDebitNotes';
		debit.CurrencyIsoCode = 'USD';
		debit.Date__c = date.newInstance(1982, 7, 6);
		debit.Amount__c = 500;
		debit.PO_Project__c = poProject.Id;
		debitList.add(debit);
		
		Debit_Notes__c debitCny = new Debit_Notes__c();
		debitCny.Name = 'TestDebitNotes';
		debitCny.CurrencyIsoCode = 'CNY';
		debitCny.Date__c = date.newInstance(1982, 7, 6);
		debitCny.Amount__c = 500;
		debitCny.PO_Project__c = poProject.Id;
		debitList.add(debitCny);
		
		insert debitList;
		
		Decimal wantCnyValue;
		for(ExchangeRateItem__c rateItem :rateItems ){
			if(rateItem.CurrencyIsoCode=='CNY'){
				wantCnyValue = debitCny.Amount__c / rateItem.ConversionRate__c;
			}
		}
		
		//开始测试
		test.startTest();
		DebitNotesRatesBatch pb = new DebitNotesRatesBatch();
		pb.configId = monthRate.Id;
		ID backId = Database.executeBatch(pb, 100);
		test.stopTest();
		
		List<Debit_Notes__c> newDebitList = [select CurrencyIsoCode,Amount__c,USDAmount__c 
											from Debit_Notes__c where id IN(: debit.Id,:debitCny.Id)];
		for(Debit_Notes__c debitNote:newDebitList){
			if(debitNote.CurrencyIsoCode=='USD'){
				System.assertEquals(debitNote.Amount__c, debitNote.USDAmount__c);		
			}
			if(debitNote.CurrencyIsoCode=='CNY'){
				System.assertEquals(wantCnyValue, debitNote.USDAmount__c);
			}
		}		
    }
}