@isTest 
//测试AutoCreateExchangeRateItems
//开发人:米立业
private class Test_AutoCreateExchangeRateItems {

    static testMethod void myUnitTest() 
    {
    	MonthlyExchangeRate__c mer;
    	List<MonthlyExchangeRate__c> ratList = [SELECT Id FROM MonthlyExchangeRate__c WHERE Year__c = 2010 And Month__c='5'];
        if (ratList != null && ratList.size() > 0)
        {
        	mer = ratList[0];
        }
        else
        {
        	mer = new MonthlyExchangeRate__c();
        	mer.Year__c = 2010;
	        mer.Month__c = '5';
	        mer.SendNoticeMail__c = true;
	        mer.HasExecuted__c = false;
        }
		if(mer.id != null)
		{
        	update mer;
		}
		else
		{
			insert mer;
		}
        List<ExchangeRateItem__c> rateItemsList = [SELECT ConversionRate__c, Corporate__c, CurrencyIsoCode  FROM ExchangeRateItem__c WHERE MonthlyExchangeRate__c =: mer.Id];
        System.debug('***************rateItemsList'+rateItemsList.size());
        List<CurrencyType> currencyList = [SELECT  ConversionRate, IsCorporate,   IsoCode FROM CurrencyType WHERE IsActive = true];
        System.debug('*****************currencyList+'+currencyList.size());
        //System.assertEquals(currencyList.size(), rateItemsList.size());
        //System.assertEquals(currencyList.get(0).IsoCode, rateItemsList.get(0).CurrencyIsoCode);
        //System.assertEquals(currencyList.get(0).ConversionRate, rateItemsList.get(0).ConversionRate__c);
    }
}