@isTest
private class TestAutoDebitNotesNo 
{
	    static testMethod void runSingleTest() 
    {
    	PO_Project__c[] projects = [Select Id From PO_Project__c limit 1];
    	if(projects.size() <= 0)
    	{
    		PO_Project__c project = new PO_Project__c(Name = 'Test for create Debit Notes');
    		system.assertEquals(null, project.Id);
    		insert project;
    		system.assertNotEquals(null, project.Id);
    		projects.add(project);
    	}
    	
    	Date theDate1 = date.newinstance(1945, 8, 1);
    	Date theDate2 = date.newinstance(1945, 8, 2);
    	Debit_Notes__c deb1 = new Debit_Notes__c(Date__c = theDate1, PO_Project__c = projects[0].Id);
    	Debit_Notes__c deb2 = new Debit_Notes__c(Date__c = theDate2, PO_Project__c = projects[0].Id);
    	test.startTest();
    	insert deb1;
    	insert deb2;
    	test.stopTest();
    	deb1 = [Select Id, Name From Debit_Notes__c Where Date__c = : theDate1];
    	deb2 = [Select Id, Name From Debit_Notes__c Where Date__c = : theDate2];
    	//system.assertEquals('DN0845011', deb1.Name);
    	//system.assertEquals('DN0845012', deb2.Name);
    }
    
    static testMethod void runMultipleTest() 
    {
    	PO_Project__c[] projects = [Select Id From PO_Project__c limit 1];
    	if(projects.size() <= 0)
    	{
    		PO_Project__c project = new PO_Project__c(Name = 'Test for create Debit Notes');
    		system.assertEquals(null, project.Id);
    		insert project;
    		system.assertNotEquals(null, project.Id);
    		//Database.SaveResult sr = Database.insert(project);
    		//system.assertEquals(true, sr.isSuccess());
    		projects.add(project);
    	}
    	
    	Date theDate1 = date.newinstance(1945, 8, 1);
    	Date theDate2 = date.newinstance(1945, 8, 2);
    	Debit_Notes__c deb1 = new Debit_Notes__c(Date__c = theDate1, PO_Project__c = projects[0].Id);
    	Debit_Notes__c deb2 = new Debit_Notes__c(Date__c = theDate2, PO_Project__c = projects[0].Id);
    	test.startTest();
    	insert new Debit_Notes__c[] {deb1, deb2};
    	test.stopTest();
    	deb1 = [Select Id, Name From Debit_Notes__c Where Date__c = : theDate1];
    	deb2 = [Select Id, Name From Debit_Notes__c Where Date__c = : theDate2];
    	//system.assertEquals('DN0845011', deb1.Name);
    	//system.assertEquals('DN0845012', deb2.Name);
    }
    
     static testMethod void runNegativeTest() 
    {
    	PO_Project__c[] projects = [Select Id From PO_Project__c limit 1];
    	if(projects.size() <= 0)
    	{
    		PO_Project__c project = new PO_Project__c(Name = 'Test for create Debit Notes');
    		system.assertEquals(null, project.Id);
    		insert project;
    		system.assertNotEquals(null, project.Id);
    		projects.add(project);
    	}
    	
    	Date theDate1 = date.newinstance(1945, 8, 1);
    	Date theDate2 = date.newinstance(1945, 9, 2);
    	Debit_Notes__c deb1 = new Debit_Notes__c(Date__c = theDate1, PO_Project__c = projects[0].Id);
    	Debit_Notes__c deb2 = new Debit_Notes__c(Date__c = theDate2, PO_Project__c = projects[0].Id);
    	test.startTest();
    	try
    	{
    	insert new Debit_Notes__c[] {deb1, deb2};
    	}
    	catch(DmlException exc)
    	{
    		exc.getMessage().contains('Bulk inserted debit notes must be with same month Date__c');
    	}
    	test.stopTest();
    }
}