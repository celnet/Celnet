@isTest
private class Test_InvoicePrintPageLogic {

    static testMethod void myUnitTest() {
    	Date theDate = date.newinstance(2010, 1, 11);
    	   	
    	   	
		PO__c InsertPO = new PO__c();
		InsertPO.Name = 'InsertPO';
		insert InsertPO;
		
		PO_Project__c InsertPoPro = new PO_Project__c();
		InsertPoPro.Name = 'insertPOProject';
		InsertPoPro.PO__c = InsertPO.Id;
		InsertPoPro.PO_Project_No__c = 'P12345667';
		insert InsertPoPro;
		
		PO_Deliverable__c insertDeliver = new PO_Deliverable__c();
		insertDeliver.Name = 'test Deliver';
		insertDeliver.Total_Man_Days__c = 2;
		insertDeliver.Complete_Rate__c = 100;
		insertDeliver.PO__c = InsertPO.Id;
		insertDeliver.PO_Project__c = InsertPoPro.Id;
		insert insertDeliver;
		
    	Account testAcc = new Account();
    	testAcc.Name = 'test Account';
    	testAcc.Payment_Terms__c = '10 days';
    	insert testAcc;
    	
    	Contact testCon = new Contact();
    	testCon.LastName = 'testCon';
		testCon.MailingStreet = '3244';
		testCon.MailingCity = '3244';
		testCon.MailingState = '3244';
		testCon.MailingPostalCode = '3244';
		testCon.MailingCountry = '3244';
		testCon.Contact_Type__c = 'Billing Contact';
		testCon.AccountId = testAcc.Id;
    	insert testCon;
    	
    	Invoice__c insertInvoice = new Invoice__c();
    	insertInvoice.Client__c = testAcc.Id;
    	insertInvoice.Amount__c = 300;
    	insertInvoice.PO__c = InsertPO.Id;
    	insertInvoice.Date__c = theDate;
    	insertInvoice.Contact__c = testCon.Id;
    	insert insertInvoice;
    	
		ApexPages.StandardController con = new ApexPages.StandardController(new Invoice__c());
		ApexPages.currentPage().getParameters().put('id',insertInvoice.Id);
		InvoicePrintPageLogic testInvoicePrint = new InvoicePrintPageLogic(con);
		testInvoicePrint.getAccountTerms();
		testInvoicePrint.getDateStr();
		testInvoicePrint.getAmountStr();
		testInvoicePrint.getMailingCity();
		testInvoicePrint.getMailingCountry();
		testInvoicePrint.getMailingPostalCode();
		testInvoicePrint.getMailingState();
		testInvoicePrint.getMailingStreet();
    }
}