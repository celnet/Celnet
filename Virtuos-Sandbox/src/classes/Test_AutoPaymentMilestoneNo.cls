@isTest
private class Test_AutoPaymentMilestoneNo {

    static testMethod void runSingleTest() {
		PO__c TestPo = new PO__c();
		TestPo.Name = 'test po';
		insert TestPo;
		
		PO_Project__c TestPoProject = new PO_Project__c();
		TestPoProject.Name = 'Test Project';
		TestPoProject.PO__c = TestPo.Id;
		TestPoProject.PO_Project_No__c = 'P12345667';
		insert TestPoProject;
		
		Invoice__c TestInv = new Invoice__c();
		TestInv.Name = 'TestInv';
		TestInv.PO__c = TestPo.Id;
		
		insert TestInv;
		
		Invoice__c TestInv2 = new Invoice__c();
		TestInv2.Name = 'TestInv';
		TestInv2.PO__c = TestPo.Id;
		
		insert TestInv2;

    }
    static testMethod void runMultipleTest() {
		PO__c TestPo = new PO__c();
		TestPo.Name = 'test po';
		insert TestPo;
		
		PO_Project__c TestPoProject = new PO_Project__c();
		TestPoProject.Name = 'Test Project';
		TestPoProject.PO__c = TestPo.Id;
		TestPoProject.PO_Project_No__c = 'P12345667';
		insert TestPoProject;
		
		List<Invoice__c> InsertInvList = new List<Invoice__c>();
		
		Invoice__c TestInv1 = new Invoice__c();
		TestInv1.Name = 'TestInv';
		TestInv1.PO__c = TestPo.Id;
		InsertInvList.add(TestInv1);
		
		Invoice__c TestInv2 = new Invoice__c();
		TestInv2.Name = 'TestInv';
		TestInv2.PO__c = TestPo.Id;
		InsertInvList.add(TestInv2);
		
		insert InsertInvList;

    }
    /*
    static testMethod void runNoProjectNoTest() {
		PO__c TestPo = new PO__c();
		TestPo.Name = 'test po';
		insert TestPo;
		
		PO_Project__c TestPoProject = new PO_Project__c();
		TestPoProject.Name = 'Test Project';
		TestPoProject.PO__c = TestPo.Id;
		insert TestPoProject;
		Invoice__c TestInv2 = new Invoice__c();
		TestInv2.Name = 'TestInv';
		TestInv2.PO__c = TestPo.Id;
		
		insert TestInv2;

    }
    static testMethod void runNoProjectTest() {
		PO__c TestPo = new PO__c();
		TestPo.Name = 'test po';
		insert TestPo;
		Invoice__c TestInv2 = new Invoice__c();
		TestInv2.Name = 'TestInv';
		TestInv2.PO__c = TestPo.Id;
		
		insert TestInv2;

    }
    static testMethod void runNoPo() {
		Invoice__c TestInv2 = new Invoice__c();
		TestInv2.Name = 'TestInv';
		
		insert TestInv2;

    }
    */
}