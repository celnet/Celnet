public class InvoicePrintPageLogic {

    private String AccountTerms;
    private String DateStr;
    private String DueDateStr;
    private String AmountStr;
    private String MailingStreet;
    private String MailingCity;
    private String MailingState;
    private String MailingPostalCode;
    private String MailingCountry;
    private String FormatContact;
    private List<Particulars_Amount__c> ParticularsAmountList = new List<Particulars_Amount__c>();

   public String getAccountTerms() {
      return AccountTerms;
   }

   public String getDateStr() {
      return DateStr;
   }
   
   public String getDueDateStr() {
      return DueDateStr;
   }

   public String getAmountStr() {
      return AmountStr;
   }

   public String getMailingStreet() {
      return MailingStreet;
   }

   public String getMailingCity() {
      return MailingCity;
   }

   public String getMailingState() {
      return MailingState;
   }

   public String getMailingPostalCode() {
      return MailingPostalCode;
   }

   public String getMailingCountry() {
      return MailingCountry;
   }
   
   public String getFormatContact(){
      return FormatContact;
   }
    
    public List<Particulars_Amount__c> getparticularsAmountList() {
        return ParticularsAmountList;
    }
    

    
    public InvoicePrintPageLogic(ApexPages.StandardController controller){
        //Contact__r.MailingCountry & Contact__r.MailingState & Contact__r.MailingCity & Contact__r.MailingStreet
        String InvoiceID = ApexPages.currentPage().getParameters().get('id');
        List<Invoice__c> InvoiceList = [select Amount__c, CurrencyIsoCode, Client__c, Contact__c, Should_Invoice_Date__c from Invoice__c where id =: InvoiceID];
        if(InvoiceList.size()>0){
            Invoice__c Invoice = InvoiceList[0];
            //获取客户Payment_Terms
            AccountTerms = GetAccountTerms(Invoice.Client__c);
            //取联系人地址
            RetriveContactAddress(Invoice.Contact__c);
            //格式化日期
            DateStr = FormatDate(Invoice.Should_Invoice_Date__c);
            //格式化金额
            AmountStr = FormatAmount(Invoice.Amount__c, Invoice.CurrencyIsoCode);
            //格式化联系人姓名
            FormatContact = FormatContactName(Invoice.Contact__c);
            //DueDateStr  thunder 2014-12-12
            try{DueDateStr = FormatDate(GetDueDate(Invoice.Should_Invoice_Date__c,AccountTerms));}catch(exception e){DueDateStr='2012-12-12';}
        }
        List<Particulars_Amount__c> ParticularsList = [select Name, Amount__c from Particulars_Amount__c where Invoice_Payment_Milestone__c =: InvoiceID ORDER BY CreatedDate ASC];
        ForamtParticularsAmount(ParticularsList);
    }
    private Date GetDueDate(date Should_Invoice_Date,string strTerms){
        if(strTerms.indexof(' days') > 0){
            Integer days = Integer.valueof(strTerms.substring(0,strTerms.indexof(' days')));
            return Should_Invoice_Date.addDays(days);
        }
        return Should_Invoice_Date;
    }
    //格式化联系人姓名
    private String FormatContactName(String contactID){
        String ReturStr = '';
        if(ContactID != null){
            List<Contact> ContactList = [select FirstName,LastName,AccountId from Contact where id =:ContactID ];
            if(ContactList != null && ContactList.size()>0){
                Contact contact = ContactList[0];   
                Account acc = [select name from Account where ID=:contact.AccountId];
                ReturStr = contact.FirstName + '&nbsp;' + contact.LastName + '<br/>' + acc.Name;
            }
        }
        return ReturStr;
    }
    
    private void ForamtParticularsAmount(List<Particulars_Amount__c> ParticularsList){
        for(Particulars_Amount__c particulars : ParticularsList){
            //particulars.formatAmount__c = FormatAmount(particulars.Amount__c, particulars.CurrencyIsoCode);
            particulars.formatAmount__c = FormatAmount(particulars.Amount__c, '');
            ParticularsAmountList.add(particulars);
        }
    //ParticularsAmountList = ;
    }

    //获取客户Payment_Terms
    private String GetAccountTerms(string AccID){
        String ReturStr = '';
        if(AccID == null){
            return ReturStr;
        }
        List<Account> AccList = [select Payment_Terms__c from account where id =:AccID];
        if(AccList.size()>0){
            Account acc = AccList[0];
            if(acc.Payment_Terms__c != null){
                ReturStr = acc.Payment_Terms__c;
            }
        }
        return ReturStr;
    }
    
    //格式化金额1,023.00 EUR
    private String FormatAmount(Decimal Amount, string CurrencyIsoCode){
        String ReturStr = '&nbsp;';
        if(Amount == null){
            return ReturStr;            
        }
        String FormatAmount = Amount.format();
        if(FormatAmount.indexOf('.') > 0){
            string decm = FormatAmount.substring(FormatAmount.indexOf('.'));
            if(decm.length() == 2){
                FormatAmount = FormatAmount + '0';
            }
        }
        else{
            FormatAmount = FormatAmount + '.00';
        }
        /*
        if(FormatAmount.substring(FormatAmount.length()-3, FormatAmount.length()-2) != '.')
        {
            FormatAmount = FormatAmount + '.00';
        }
        else if(FormatAmount.substring(FormatAmount.length()-3, FormatAmount.length()-2) != '.')
        {
            FormatAmount = FormatAmount + '.00';
        }
        */
        ReturStr = FormatAmount + '&nbsp;' + CurrencyIsoCode;
        return ReturStr;
    }
    
    //格式化日期Aug.1.2010
    private String FormatDate(date InvDate){
        String ReturStr = '';
        if(InvDate != null){
            Integer Month = InvDate.month();
            if(Month == 1)
            { ReturStr = 'Jan'; }
            else if(Month == 2)
            { ReturStr = 'Feb'; }
            else if(Month == 3)
            { ReturStr = 'Mar'; }
            else if(Month == 4)
            { ReturStr = 'Apr'; }
            else if(Month == 5)
            { ReturStr = 'May'; }
            else if(Month == 6)
            { ReturStr = 'Jun'; }
            else if(Month == 7)
            { ReturStr = 'Jul'; }
            else if(Month == 8)
            { ReturStr = 'Aug'; }
            else if(Month == 9)
            { ReturStr = 'Sept'; }
            else if(Month == 10)
            { ReturStr = 'Oct'; }
            else if(Month == 11)
            { ReturStr = 'Nov'; }
            else if(Month == 12)
            { ReturStr = 'Dec'; }
            else {ReturStr = '';}
            
            ReturStr = ReturStr + '.' + InvDate.day() + '.' + InvDate.year();
        }
        return ReturStr;
    }
    
    
    //获取联系人的地址
    private void RetriveContactAddress(string ContactID){
        String ReturStr = '';
        if(ContactID != null){
            List<Contact> ContactList = [select MailingCountry, MailingState, MailingCity, MailingStreet, MailingPostalCode from Contact where id =:ContactID ];
            if(ContactList != null && ContactList.size()>0){
                Contact AddressCon = ContactList[0];    
                MailingStreet = AddressCon.MailingStreet;
                MailingCity = AddressCon.MailingCity + ',';
                MailingState = AddressCon.MailingState;
                MailingPostalCode = AddressCon.MailingPostalCode;
                MailingCountry = AddressCon.MailingCountry;
            }
        }
    }
}