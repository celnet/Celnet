@isTest
private class Test_SumMilestoneComRateByDeliver {

    static testMethod void myUnitTest() {
		PO_Project_Milestone__c Milestone = new PO_Project_Milestone__c();
		Milestone.Name = 'Milestone';
		insert Milestone;
		
		PO_Project_Milestone__c Milestone2 = new PO_Project_Milestone__c();
		Milestone2.Name = 'Milestone';
		insert Milestone2;
		
		
		PO__c InsertPO = new PO__c();
		InsertPO.Name = 'InsertPO';
		insert InsertPO;
		
		PO_Project__c InsertPoPro = new PO_Project__c();
		InsertPoPro.Name = 'insertPOProject';
		InsertPoPro.PO__c = InsertPO.Id;
		InsertPoPro.PO_Project_No__c = 'P12345267';
		insert InsertPoPro;
		
		PO_Deliverable__c insertDeliver = new PO_Deliverable__c();
		insertDeliver.Name = 'test Deliver';
		insertDeliver.PO_Project_Milestone__c = Milestone.Id;
		insertDeliver.Complete_Rate__c = 20;
		insertDeliver.Total_Man_Days__c = 50;
		insertDeliver.PO__c = InsertPO.Id;
		insertDeliver.PO_Project__c = InsertPoPro.Id;
		insert insertDeliver;
		
		insertDeliver.Total_Man_Days__c = 100;
		update insertDeliver;
		
		delete insertDeliver;
		
		undelete insertDeliver;
		
		
		
		PO_Deliverable__c insertDeliver2 = new PO_Deliverable__c();
		insertDeliver2.Name = 'test Deliver';
		insertDeliver2.PO_Project_Milestone__c = Milestone2.Id;
		insertDeliver2.PO__c = InsertPO.Id;
		insertDeliver2.PO_Project__c = InsertPoPro.Id;
		insert insertDeliver2;
		
    }
}