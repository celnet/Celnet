/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_SumDebitNotesAmountToMonthlyBudget {
	//新建和更新都要测试
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Account TestAcc = new Account();
        TestAcc.Name = 'Test Account';
        insert TestAcc;
        
        Annual_Budget__c TestAnnuBudget = new Annual_Budget__c();
        TestAnnuBudget.Name = 'TestAnnubudget';
        TestAnnuBudget.Account__c = TestAcc.Id;
        TestAnnuBudget.Year__c = '2010';
        insert TestAnnuBudget;
        
        Monthly_Budget__c TestMonthBudget = new Monthly_Budget__c();
        TestMonthBudget.Annual_Budget__c = TestAnnuBudget.Id;
        TestMonthBudget.Month__c = 'Sep.';
        insert TestMonthBudget;
        
        Monthly_Budget__c TestMonthBudget2 = new Monthly_Budget__c();
        TestMonthBudget2.Annual_Budget__c = TestAnnuBudget.Id;
        TestMonthBudget2.Month__c = 'Oct.';
        insert TestMonthBudget2;
        
    	PO_Project__c[] projects = [Select Id From PO_Project__c limit 1];
    	if(projects.size() <= 0)
    	{
    		PO_Project__c project = new PO_Project__c(Name = 'Test for create Debit Notes');
    		system.assertEquals(null, project.Id);
    		insert project;
    		system.assertNotEquals(null, project.Id);
    		projects.add(project);
    	}
    	
    	Date theDate1 = date.newinstance(2010, 1, 11);
    	
    	Debit_Notes__c TestDebit1 = new Debit_Notes__c(Date__c = theDate1, PO_Project__c = projects[0].Id);
        TestDebit1.Amount__c = 232;
        TestDebit1.Client__c = TestAcc.Id;
        
    	Debit_Notes__c TestDebit = new Debit_Notes__c(Date__c = theDate1, PO_Project__c = projects[0].Id);
        TestDebit.Amount__c = 232;
        TestDebit.Client__c = TestAcc.Id;
		insert TestDebit1;
		insert TestDebit;
		
		TestDebit.Client__c = null;
		update TestDebit;
		
		TestDebit.Client__c = TestAcc.Id;
		TestDebit.Month_No__c = null;
		update TestDebit;
		
		TestDebit.Month_No__c = '0110';
		update TestDebit;
		
		TestDebit.Month_No__c = '0210';
		update TestDebit;
		
		TestDebit.Month_No__c = '0310';
		update TestDebit;
		
		TestDebit.Month_No__c = '0410';
		update TestDebit;
		
		TestDebit.Month_No__c = '0510';
		update TestDebit;
		
		TestDebit.Month_No__c = '0610';
		update TestDebit;
		
		TestDebit.Month_No__c = '0710';
		update TestDebit;
		
		TestDebit.Month_No__c = '0810';
		update TestDebit;
		
		TestDebit.Month_No__c = '0910';
		update TestDebit;
		
		TestDebit.Month_No__c = '1010';
		update TestDebit;
		
		TestDebit.Month_No__c = '1110';
		update TestDebit;
		
		TestDebit.Month_No__c = '1210';
		update TestDebit;
		
		TestDebit.Month_No__c = '0110';
		update TestDebit;
    }
}