public class ExchangeRateHelper 
{
	public class ExchangeRateException extends Exception {}
	
	public static map<Integer, map<Integer, MonthlyExchangeRate__c>> GetExchangeRate(Integer year)
	{
		map<Integer, map<Integer, MonthlyExchangeRate__c>> exchangeRateMap = new map<Integer, map<Integer, MonthlyExchangeRate__c>>();
		for(MonthlyExchangeRate__c exchangeRate : [Select Year__c, Month__c, Id, (Select CurrencyIsoCode, MonthlyExchangeRate__c, ConversionRate__c, Corporate__c From ExchangeRateItems__r) From MonthlyExchangeRate__c Where Year__c >=: year-10 And Year__c <=: year])//只取最近10年的
		{
			Integer theYear = (Integer)exchangeRate.Year__c;
			if(!exchangeRateMap.containsKey(theYear))
			{
				exchangeRateMap.put(theYear, new map<Integer, MonthlyExchangeRate__c>());
			}
			 map<Integer, MonthlyExchangeRate__c> monthRateMap = exchangeRateMap.get(theYear);
			 monthRateMap.put(Integer.valueOf(exchangeRate.Month__c), exchangeRate);
		}
		return exchangeRateMap;
	}
	
	
		//获取当前Row货币的汇率项目，如果当前的月份的汇率不存在，则取最近月份的汇率
	public static ExchangeRateItem__c matchExchangeRateItem(Integer year, Integer month, String currencyCode, map<Integer, map<Integer, MonthlyExchangeRate__c>> exchangeRateMap)
	{		
		Boolean matchYear = false;
		Integer theYear = year;
		map<Integer, MonthlyExchangeRate__c> yearMap = exchangeRateMap.get(theYear);
		if(yearMap != null)
		{
			matchYear = true;
		}
		Integer couty = 1;
		while(yearMap == null)
		{
			if(couty > exchangeRateMap.size())//如果已经搜索了集合的所有元素还没有找到就退出搜索
			{
				break;
			}
			theYear--;
			yearMap = exchangeRateMap.get(theYear);
			couty ++;
		}
		if(yearMap == null)
		{
			throw new ExchangeRateException('cannot find year exchange rate for year:' + year + ' last searched year: ' + theYear);
		}
		//在年汇率表中搜索对应月份的汇率项
		MonthlyExchangeRate__c mothRate;
		Integer theMonth = month;
		if(matchYear)//在当前年中搜索
		{
			mothRate = yearMap.get(theMonth);
			Integer coutm = 1;
			while(mothRate == null)
			{
				if(coutm > 12)//如果已经搜索了集合的所有元素还没有找到就退出搜索
				{
					break; 
				}
				theMonth--;
				mothRate = yearMap.get(theMonth);
				coutm ++;
			}
		}
		else//在上年中搜索最后一个月的汇率
		{
			for(theMonth = 12; theMonth >= 1; theMonth --)
			{
				mothRate = yearMap.get(theMonth);
				if(mothRate != null)
				{
					break; //找到最后的一个月就退出
				}
			}
		}
		if(mothRate == null)
		{
			throw new ExchangeRateException('cannot find month exchange rate in year:' + theYear + ' month:' + month);
		}
		ExchangeRateItem__c item;
		for(ExchangeRateItem__c rateItem : mothRate.ExchangeRateItems__r)
		{
			if(rateItem.CurrencyIsoCode == currencyCode)
			{
				item = rateItem;
			}
		}
		if(item == null)
		{
			throw new ExchangeRateException('cannot find exchange rate item for code:' + currencyCode + ' in year:' + theYear + ' month:' + theMonth);
		}
		return item;	
	}
}