/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_AutoCreateProjectWhenCreateQuote {

    static testMethod void runSingleTest() {
        // TO DO: implement unit test
        Date testDate = date.newInstance(2010, 11, 23);
        Opportunity testOpp = new Opportunity();
        testOpp.Name = 'test Opp';
        testOpp.StageName = 'Approved';
        testOpp.CloseDate = testDate;
        testOpp.Project_End_Date__c = Date.newInstance(2012, 12, 5);
        insert testOpp;
        
        Account testAcc = new Account();
        testAcc.Name = 'test Acc';
        insert testAcc;
        
        Quote__c InsertQuote = new Quote__c();
        InsertQuote.Name = 'Test Quote';
        InsertQuote.Opportunity__c = testOpp.Id;
        InsertQuote.Account__c = testAcc.Id;
        insert InsertQuote;
        
    }
    static testMethod void runMutilpleTest() {
        Date testDate = date.newInstance(2010, 11, 23);
        Opportunity testOpp = new Opportunity();
        testOpp.Name = 'test Opp';
        testOpp.StageName = 'Approved';
        testOpp.CloseDate = testDate;
        testOpp.Project_End_Date__c = Date.newInstance(2012, 12, 5);
        insert testOpp;
        
        Account testAcc = new Account();
        testAcc.Name = 'test Acc';
        insert testAcc;
        
        List<Quote__c> QuoteList = new List<Quote__c>();
        
        Quote__c InsertQuote = new Quote__c();
        InsertQuote.Name = 'Test Quote';
        InsertQuote.Opportunity__c = testOpp.Id;
        InsertQuote.Account__c = testAcc.Id;
        QuoteList.add(InsertQuote);
        
        Quote__c InsertQuote2 = new Quote__c();
        InsertQuote2.Name = 'Test Quote';
        InsertQuote2.Opportunity__c = testOpp.Id;
        InsertQuote2.Account__c = testAcc.Id;
        QuoteList.add(InsertQuote2);
        
        insert QuoteList;
    }
}