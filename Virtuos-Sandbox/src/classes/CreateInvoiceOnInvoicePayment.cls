global class CreateInvoiceOnInvoicePayment {

	webservice static string CreateInvoiceNo(string MilestoneID) {
		//生成编号时当前操作人必须对Invoice Payment Milestone具有读的权限
			
		string TheNo = '';
	
		//1.根据MilestoneID找到Invoice Number
		List<Invoice__c> InvoiceList = [select Invoice_No__c,Should_Invoice_Date__c from Invoice__c where id =: MilestoneID];
		Invoice__c InvoiceNo = InvoiceList[0];

		//如果编号为空或这不是当前月的编号,则重新生成编号
		
		Date ShouldInvoiceDate = InvoiceNo.Should_Invoice_Date__c;
		if(ShouldInvoiceDate == null){
			return '日期为空.';
		}
		
		if(InvoiceNo.Invoice_No__c != null && IsCurrMonth(InvoiceNo.Invoice_No__c, InvoiceNo.Should_Invoice_Date__c)){
			return '不需要编号.';
		}
		//DateStr
		string DateStr = GenerateDateStr(ShouldInvoiceDate);
		
		//根据should invoice date找到本月最大的Invoice Number
		List<Invoice__c> InvoiceNOList = [select Invoice_No__c from Invoice__c where Invoice_No__c like : (DateStr + '%') ORDER BY Invoice_No__c DESC];
		
		if(InvoiceNOList == null || InvoiceNOList.size() == 0){
			TheNo = DateStr + '011';
		}
		else{
			//将最大的Invoice Number加1
			string MaxNo = InvoiceNOList[0].Invoice_No__c;
			string SerrNO = MaxNo.substring(5);
			Integer SerrInt = Integer.valueOf(SerrNO);
			SerrNO = '' + (SerrInt + 1);
			if(SerrNO.length()==2){
				SerrNO = '0' + SerrNO;
			}
			TheNo = DateStr + SerrNO;
		}
		
		//更新Invoice Number
		Invoice__c UpdateInvoice = new Invoice__c(id = MilestoneID);
		UpdateInvoice.Invoice_No__c = TheNo;
		update UpdateInvoice;
		return 'Generate success.';
	}
	
	//根据ShouldInvoiceDate判断当前编号是否是本月,如果是则返回true,否则返回false;
	private static Boolean IsCurrMonth(String InvoiceNo, Date ShouldInvoiceDate){
		string DateStr = GenerateDateStr(ShouldInvoiceDate);
		string NODateStr = InvoiceNo.substring(0,5);
		if(DateStr == NODateStr){
			return true;
		}
		return false;
	}
	
	//根据日期生成字符串:I0910
	private static string GenerateDateStr(Date ShouldInvoiceDate){
		string DateStr = 'I';
		Integer Month = ShouldInvoiceDate.month();		
		if(Month<10){
			DateStr += '0' + Month;
		}
		else
		{
			DateStr += '' + Month;
		}
		Integer Year = ShouldInvoiceDate.year();
		string YearStr = '' + Year;
		YearStr = YearStr.substring(2);
		DateStr += YearStr;
		return DateStr;
	}
}