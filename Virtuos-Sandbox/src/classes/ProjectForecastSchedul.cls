/*
Author: Tommyliu of Cideatech at 2012-8-9
每天自动计算一次Project Forecast数据
*/
global class ProjectForecastSchedul implements Schedulable
{
   global void execute(SchedulableContext sc) 
   {
		ProjectForecastBatch pfBatch = new ProjectForecastBatch(System.now().year(), false);
		Database.executeBatch(pfBatch, 50);
   }
}