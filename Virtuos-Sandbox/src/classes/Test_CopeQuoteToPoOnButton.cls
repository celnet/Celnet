@isTest
private class Test_CopeQuoteToPoOnButton {

    static testMethod void myUnitTest() {
    	Date theDate = date.newinstance(2010, 1, 11);
    	
    	Opportunity InsertOpp = new Opportunity();
    	InsertOpp.Name = 'testOpp';
    	InsertOpp.CloseDate = theDate;
    	InsertOpp.StageName = 'Qualifying';
    	InsertOpp.Type = 'Dev';
    	InsertOpp.Subtype__c = 'Full game';
    	InsertOpp.Project_End_Date__c = Date.newInstance(2012, 12, 5);
    	insert InsertOpp;
    	
    	Quote__c testQuote = new Quote__c();
    	testQuote.Opportunity__c = InsertOpp.Id;
    	testQuote.Name = 'test Quote';
    	insert testQuote;
    	
    	Account testAcc = new Account();
    	testAcc.Name = 'test Account';
    	insert testAcc;
    	
	    Project__c testProject = new Project__c();
	    
		List<Project__c> ProjectList = [select ID from Project__c where Quote__c =: testQuote.Id];
		if(ProjectList != null && ProjectList.size() > 0){
			testProject = ProjectList[0];
			testProject.Project_No__c = '5545454';
			update testProject;
		}
		else
		{
	    	testProject.Actual_End__c = theDate;
	    	testProject.Project_No__c = 'afqagjdaegaw';
	    	testProject.Client__c = testAcc.Id;
	    	testProject.Delay_Days__c = 3;
	    	testProject.Name = 'test Project';
	    	testProject.Plan_End__c = theDate;
	    	testProject.Plan_Start__c = theDate;
	    	testProject.Project_Status__c = 'Start';
	    	testProject.Quote__c = testQuote.Id;
	    	
	    	insert testProject;
		}
    	
		
		
		
    	Project_Milestone__c testMilestone = new Project_Milestone__c();
    	testMilestone.Name = 'test Milestone';
    	testMilestone.Project_Milestone_No__c = 'P002M009';
    	testMilestone.Quote__c = testQuote.Id;
    	
    	insert testMilestone;
    	
    	Deliverable__c testDeliver = new Deliverable__c();
    	testDeliver.Category__c = 'Art';
    	testDeliver.Deliverable_No__c = 'D0019381714';
    	testDeliver.Group__c = 'Fash';
    	testDeliver.Man_Day_Unit_Price__c = 234;
    	testDeliver.Name = 'test Deliver';
    	testDeliver.Sub_Group__c = 'Cart';
    	testDeliver.Sub_Category__c = 'Concept';
    	testDeliver.Total_Man_Days__c = 14;
    	testDeliver.Project__c = testProject.Id;
    	testDeliver.Project_Milestone__c = testMilestone.Id;
    	testDeliver.Quote__c = testQuote.Id;
    	
    	insert testDeliver;
    	
		
		CopeQuoteToPoOnButton.CopeQuoteToPoOnButton(testQuote.Id);
		//CopeQuoteToPoOnButton.CopeQuoteToPoOnButton(testQuote.Id);
    }
}