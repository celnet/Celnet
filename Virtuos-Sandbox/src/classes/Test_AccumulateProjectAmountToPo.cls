@isTest
private class Test_AccumulateProjectAmountToPo {

    static testMethod void myUnitTest() {
    	PO__c insertPO = new PO__c();
    	insertPO.Name = 'testinsertPO';
    	insert insertPO;
    	
		PO_Project__c insertProject = new PO_Project__c();
		insertProject.Name = 'insertProject';
		insertProject.PO_Project_No__c = 'ccc00001';
		insertProject.PO__c = insertPO.Id;
		insert insertProject;
		
		Debit_Notes__c insertDB = new Debit_Notes__c();
		insertDB.Name = 'insertDBtest';
		insertDB.Amount__c = 23;
		insertDB.PO_Project__c = insertProject.Id;
		insertDB.Date__c = date.today();
		insert insertDB;
		
		insertDB.Amount__c = 24;
		update insertDB;
		
		delete insertProject;
		
		undelete insertProject;
    }
}