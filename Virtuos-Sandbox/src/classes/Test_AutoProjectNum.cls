@isTest
private class Test_AutoProjectNum {

    static testMethod void singleTest() {
    	Account insertAcc = new Account();
    	insertAcc.Name ='insertAcc';
    	insert insertAcc;
    	
		Project__c insertPro = new Project__c();
		insertPro.Name = 'insertPro';
		insertPro.Client__c = insertAcc.Id;
		
		insert insertPro;
    	
		Project__c insertPro3 = new Project__c();
		insertPro3.Name = 'insertPro';
		insertPro3.Client__c = insertAcc.Id;
		
		insert insertPro3;
		
		insertPro.Delay_Days__c = 2;
		update insertPro;

    	
		Project__c insertPro2 = new Project__c();
		insertPro2.Name = 'insertPro';
		insert insertPro2;
		
		insertPro2.Project_No__c = null;
		update insertPro2;
    }

    static testMethod void multiTest() {
    	Account insertAcc = new Account();
    	insertAcc.Name ='insertAcc';
    	insert insertAcc;
    	
    	List<Project__c> ProjectList = new List<Project__c>();
    	
		Project__c insertPro = new Project__c();
		insertPro.Name = 'insertPro';
		insertPro.Client__c = insertAcc.Id;
		ProjectList.add(insertPro);
    	
		Project__c insertPro2 = new Project__c();
		insertPro2.Name = 'insertPro';
		insertPro2.Client__c = insertAcc.Id;
		ProjectList.add(insertPro2);
		
		insert ProjectList;
		
    	List<Project__c> updateList = new List<Project__c>();
		Project__c updatePro = ProjectList[0];
		updatePro.Delay_Days__c = 23;
		updateList.add(updatePro);
		Project__c updatePro1 = ProjectList[1];
		updatePro1.Delay_Days__c = 23;
		updateList.add(updatePro1);
		
		update updateList;

    }
}