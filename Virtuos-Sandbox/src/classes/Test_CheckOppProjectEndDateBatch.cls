/*
 * Author: Steven
 * Date: 2014-3-4
 * Description: test 
 */
@isTest
public class Test_CheckOppProjectEndDateBatch 
{
	static testmethod void testCase()
	{
		// 准备数据
		RecordType rt = [Select Id, Name From RecordType Where Name = 'Subsidiary'];
		Account acc1 = new Account();
		Account acc2 = new Account();
		Account acc3 = new Account();
		
		acc1.Name = 'NVIDIA';
		acc1.CurrencyIsoCode = 'USD';
		acc1.RecordTypeId = rt.Id;
		acc2.Name = 'PhysX';
		acc2.CurrencyIsoCode = 'USD';
		acc2.RecordTypeId = rt.Id;
		acc3.Name = 'Google';
		acc3.CurrencyIsoCode = 'USD';
		acc3.RecordTypeId = rt.Id;
		
		insert new List<Account> {acc1, acc2, acc3};
		
		Opportunity opp1 = new Opportunity();
		Opportunity opp2 = new Opportunity();
		Opportunity opp3 = new Opportunity();
		
		opp1.AccountId = acc1.Id;
		opp1.Name = 'NVIDIAOpp';
		opp1.CloseDate = Date.today().addDays(-10);
		opp1.Project_End_Date__c = Date.today().addDays(-7);
		opp1.StageName = 'Approved';
		opp1.CurrencyIsoCode = 'USD';
		opp2.AccountId = acc2.Id;
		opp2.Name = 'PhysXOpp';
		opp2.CloseDate = Date.today().addDays(-100);
		opp2.Project_End_Date__c = Date.today().addDays(-27);
		opp2.StageName = 'Approved';
		opp2.CurrencyIsoCode = 'USD';
		opp3.AccountId = acc3.Id;
		opp3.Name = 'GoogleOpp';
		opp3.CloseDate = Date.today().addDays(1);
		opp3.Project_End_Date__c = Date.today().addDays(5);
		opp3.StageName = 'Approved';
		opp3.CurrencyIsoCode = 'USD';
		
		insert new List<Opportunity> {opp1, opp2, opp3};
		
		Project__c p1 = new Project__c();
		Project__c p2 = new Project__c();
		Project__c p3 = new Project__c();
		
		p1.Name = 'p1';
		p1.CurrencyIsoCode = 'USD';
		p1.Opportunity__c = opp1.Id;
		p1.Project_No__c = 'pro1';
		p2.Name = 'p22';
		p2.CurrencyIsoCode = 'USD';
		p2.Opportunity__c = opp2.Id;
		p2.Project_No__c = 'pro2';
		p3.Name = 'p3';
		p3.CurrencyIsoCode = 'USD';
		p3.Opportunity__c = opp3.Id;
		p3.Project_No__c = 'pro3';
		
		insert new List<Project__c> {p1, p2, p3};
		
		DB_Notes__c dn1 = new DB_Notes__c();
		DB_Notes__c dn2 = new DB_Notes__c();
		DB_Notes__c dn3 = new DB_Notes__c();
		DB_Notes__c dn4 = new DB_Notes__c();
		DB_Notes__c dn5 = new DB_Notes__c();
		DB_Notes__c dn6 = new DB_Notes__c();
		DB_Notes__c dn7 = new DB_Notes__c();
		
		dn1.Opportunity__c = opp1.Id;
		dn1.Date__c = Date.today().addDays(-6);
		dn1.Project_NO__c = 'pro1';
		dn1.NO__c = 'dn1';
		dn1.Project__c = p1.Id;
		dn1.CurrencyIsoCode = 'USD';
		
		dn2.Opportunity__c = opp1.Id;
		dn2.Date__c = Date.today().addDays(-6);
		dn2.Project_NO__c = 'pro1';
		dn2.NO__c = 'dn2';
		dn2.Project__c = p1.Id;
		dn2.CurrencyIsoCode = 'USD';
		
		dn3.Opportunity__c = opp2.Id;
		dn3.Date__c = Date.today().addDays(-6);
		dn3.Project_NO__c = 'pro2';
		dn3.NO__c = 'dn3';
		dn3.Project__c = p2.Id;
		dn3.CurrencyIsoCode = 'USD';
		
		dn4.Opportunity__c = opp2.Id;
		dn4.Date__c = Date.today().addDays(-6);
		dn4.Project_NO__c = 'pro2';
		dn4.NO__c = 'dn4';
		dn4.Project__c = p2.Id;
		dn4.CurrencyIsoCode = 'USD';
		
		dn5.Opportunity__c = opp3.Id;
		dn5.Date__c = Date.today().addDays(-6);
		dn5.Project_NO__c = 'pro3';
		dn5.NO__c = 'dn5';
		dn5.Project__c = p3.Id;
		dn5.CurrencyIsoCode = 'USD';
		
		dn6.Opportunity__c = opp3.Id;
		dn6.Date__c = Date.today().addDays(-6);
		dn6.Project_NO__c = 'pro3';
		dn6.NO__c = 'dn6';
		dn6.Project__c = p3.Id;
		dn6.CurrencyIsoCode = 'USD';
		
		dn7.Opportunity__c = opp3.Id;
		dn7.Date__c = Date.today().addDays(-6);
		dn7.Project_NO__c = 'pro3';
		dn7.NO__c = 'dn7';
		dn7.Project__c = p3.Id;
		dn7.CurrencyIsoCode = 'USD';
		
		insert new List<DB_Notes__c> {dn1, dn2, dn3, dn4, dn5, dn6, dn7};
		
		System.Test.startTest();
		CheckOppProjectEndDateBatch cbatch = new CheckOppProjectEndDateBatch();
		Database.executeBatch(cbatch);
		System.Test.stopTest();
	}
}