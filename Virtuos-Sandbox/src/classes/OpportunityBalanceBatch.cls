/* 
 * Author: Steven 
 * Date: 2014-3-12 
 * Description: 为每个符合条件业务机会计算并生成Balance历史数据, 每个月每个业务机会生成一条Balance记录
 */
global class OpportunityBalanceBatch implements Database.Batchable<sObject>, Schedulable
{ 
    global String query;
    
    global OpportunityBalanceBatch()
    {
        Datetime thisMonth = Date.today().toStartOfMonth();
        this.query = 'Select Id, Amount, CurrencyIsoCode, CloseDate, Project_End_Date__c, (Select Id, CreatedDate, StageName From OpportunityHistories Where StageName=\'Approved\' OR StageName = \'Closed Won(Signed PO or contract)\' Order By CreatedDate ASC), (Select Id, Amount__c, Date__c From Debit_Notes__r Order By Date__c ASC) From Opportunity Where (StageName = \'Approved\' OR StageName = \'Closed Won(Signed PO or contract)\') And Project_End_Date__c >=';
        query += thisMonth.format('YYYY-MM-dd');
    }
    
    //第一次运行，把历史的数据补齐
    //String p = 'Select Id, Amount, CurrencyIsoCode, CloseDate, Project_End_Date__c, (Select Id, CreatedDate, StageName From OpportunityHistories Where StageName=\'Approved\' OR StageName = \'Closed Won(Signed PO or contract)\' Order By CreatedDate ASC), (Select Id, Amount__c, Date__c From Debit_Notes__r Order By Date__c ASC) From Opportunity Where StageName = \'Approved\' OR StageName = \'Closed Won(Signed PO or contract)\'';
    //OpportunityBalanceBatch obb = new OpportunityBalanceBatch(p);
    //Database.executeBatch(obb, 100);
    global OpportunityBalanceBatch(String q)
    {
        query = q;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }
    
    private List<Balance__c> calculateBalances(Opportunity opp)
    {
        Date balanceStartDate = opp.CloseDate;
        Date balanceEndDate = opp.Project_End_Date__c;
        List<DB_Notes__c> oppDNList = opp.Debit_Notes__r;
        List<OpportunityHistory> oppHisList = opp.OpportunityHistories;
        List<Balance__c> bLList = new List<Balance__c>();
        
        //如果最早的DN date早于 Close Date 则从早的DN date开始
        if(oppDNList != null && oppDNList.size() > 0 && oppDNList[0].Date__c != null)
        {
            if(balanceStartDate == null || oppDNList[0].Date__c < balanceStartDate)
            {
                balanceStartDate = oppDNList[0].Date__c;
            }
        }
        
        //如果Approved 或Closed Won(Signed PO or contract) 历史时间最早则取最早的历史时间
        if(oppHisList != null && oppHisList.size() > 0 && oppHisList[0].CreatedDate != null)
        {
            if(balanceStartDate == null || oppHisList[0].CreatedDate < balanceStartDate)
            {
                balanceStartDate = oppHisList[0].CreatedDate.date();
            }
        }
        
        //不为有问题的数据计算BL
        if(balanceStartDate == null || balanceEndDate == null || balanceStartDate > balanceEndDate)
        {
            return bLList;
        }
        
        Date cursor = balanceStartDate.toStartOfMonth();
        while(cursor <= balanceEndDate.toStartOfMonth())
        {
            Balance__c bl = new Balance__c();
            bl.Date__c = cursor;//注意：有变化，为了方便代码开发，换成每个月开始时间
            bl.CurrencyIsoCode = opp.CurrencyIsoCode;
            bl.Opp_Amount__c = opp.Amount;
            bl.OppId_Month__c = opp.Id + '-' + bl.Date__c.year() + '-' + bl.Date__c.month();
            bl.Opportunity__c = opp.Id;
            bl.DN_Monthly_Amount__c = 0;
            bl.DN_Sum_Amount__c = 0;
            for(DB_Notes__c dN : oppDNList)
            {
                if(dN.Date__c != null)
                {
                    if(dN.Amount__c == null)
                    {
                        dN.Amount__c = 0;
                    }
                    
                    if(dN.Date__c.toStartOfMonth() == cursor)
                    {
                        bl.DN_Monthly_Amount__c += dN.Amount__c;//计算这个月的DN Amount
                        bl.DN_Sum_Amount__c += dN.Amount__c;
                    }
                    else if(dN.Date__c.toStartOfMonth() < cursor)//累计以前的 DN Amount
                    {
                        bl.DN_Sum_Amount__c += dN.Amount__c;
                    }
                }
            }
            bLList.add(bl);
            cursor = cursor.addMonths(1);
        }
        return bLList;
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        List<Balance__c> balanceList = new List<Balance__c>();
        for(Opportunity opp : (List<Opportunity>) scope)
        {
            balanceList.addAll(this.calculateBalances(opp));
        }
        upsert balanceList OppId_Month__c;
    }
    
    global void finish(Database.BatchableContext BC)
    {
            
    }
    
    global void execute(SchedulableContext sc)
    {
        OpportunityBalanceBatch obb = new OpportunityBalanceBatch();
        Database.executeBatch(obb, 100);
    }
}