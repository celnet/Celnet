/*
Author: Tommyliu of Cideatech at 2012-7-6 
Note: 静态的 function:matchExchangeRateItem() 被DBNotes.Trigger使用计算汇率
*/ 
public class ForecastRow 
{
	private map<Integer, Decimal> valueMap = new map<Integer, Decimal>();
	private String accountOwnerName; 
	private Integer year;
	private map<Integer, map<Integer, MonthlyExchangeRate__c>> exchangeRateMap;//存储历史汇率表， key1指定年份2011， key2指定月份 1-12
	
	public Account Account{get; set;}
	public Opportunity Opp{get; set;}
	public Type RowType{get; set;} 
	
	public String AccountURL
	{
		get
		{
			if(this.Account != null)
			{
				return URL.getSalesforceBaseUrl().toExternalForm() + '/' + this.Account.Id;
			}
			return null;
		}
	}
	public String AccountOwnerURL
	{
		get
		{
			if(this.Account != null && this.Account.Owner != null)
			{
				return URL.getSalesforceBaseUrl().toExternalForm() + '/' + this.Account.Owner.Id;
			}
			return null;
		}
	}
	public String OppURL
	{
		get
		{
			if(this.Opp != null)
			{
				return URL.getSalesforceBaseUrl().toExternalForm() + '/' + this.Opp.Id;
			}
			return null;
		}
	}
	
	public ForecastRow(Integer year, map<Integer, map<Integer, MonthlyExchangeRate__c>> exchangeRateMap)
	{
		this.year = year;
		this.exchangeRateMap = exchangeRateMap;
	}
	//根据当前Row的货币和历史月份汇率表换算美金，Row的货币取关联的OPP，如果当前的月份的汇率不存在，则取最近月份的汇率
	//系统的基础汇率必须是美金USD，才能保证本方法计算正确
	private Decimal exchangeUSD(Decimal oriAmount, Integer month)
	{
		ExchangeRateItem__c rateItem = ExchangeRateHelper.matchExchangeRateItem(this.year, month, this.CurrencyIsoCode, this.exchangeRateMap);
		if(oriAmount != null && rateItem.ConversionRate__c != null)
		{
			return oriAmount/rateItem.ConversionRate__c;
		}
		return null;
	}
	
	
	//为实际行进行补零操作
	private Decimal getMonthValue(Integer month)
	{
		Decimal value = this.valueMap.get(month);
		Integer startMonth = 1;
		Integer endMonth = 12;
		if(this.Opp.CloseDate != null)
		{
			startMonth = this.Opp.CloseDate.month();
		}
		if(this.Opp.Project_End_Date__c != null)
		{
			endMonth = this.Opp.Project_End_Date__c.month();
		}
		
		if(RowType == Type.Confirmed && value == null)
		{
			if(month>=startMonth && month <= endMonth)
			{
				return 0;
			}
		}
		return value;
	}
	
	public String CurrencyIsoCode
	{
		get
		{
			if(this.Opp == null || this.Opp.CurrencyIsoCode == null)
			{
				throw new ExchangeRateException('Opportunity of this row is null or CurrencyIsoCode of opportunity is null, cannot get currecy code of this row');
			}
			return this.Opp.CurrencyIsoCode;
		}
	}
	
	public String AccountOwner
	{
		get
		{
			if(accountOwnerName != null)
			{
				return this.accountOwnerName;
			}
			if(this.Account != null && this.Account.Owner != null)
			{
				return this.Account.Owner.Name;
			}
			return null;
		}
		set
		{
			this.accountOwnerName = value;
		}
	}
	
	public Decimal Month1
	{
		get
		{
			//return this.valueMap.get(1);
			return this.getMonthValue(1);
		}
		set
		{
			this.valueMap.put(1, value);
		}
	}
	public Decimal Month2
	{
		get
		{
			return this.getMonthValue(2);
		}
		set
		{
			this.valueMap.put(2, value);
		}
	}
	public Decimal Month3
	{
		get
		{
			return this.getMonthValue(3);
		}
		set
		{
			this.valueMap.put(3, value);
		}
	}
	public Decimal Month4
	{
		get
		{
			return this.getMonthValue(4);
		}
		set
		{
			this.valueMap.put(4, value);
		}
	}
	public Decimal Month5
	{
		get
		{
			return this.getMonthValue(5);
		}
		set
		{
			this.valueMap.put(5, value);
		}
	}
	public Decimal Month6
	{
		get
		{
			return this.getMonthValue(6);
		}
		set
		{
			this.valueMap.put(6, value);
		}
	}
	public Decimal Month7
	{
		get
		{
			return this.getMonthValue(7);
		}
		set
		{
			this.valueMap.put(7, value);
		}
	}
	public Decimal Month8
	{
		get
		{
			return this.getMonthValue(8);
		}
		set
		{
			this.valueMap.put(8, value);
		}
	}
	public Decimal Month9
	{
		get
		{
			return this.getMonthValue(9);
		}
		set
		{
			this.valueMap.put(9, value);
		}
	}
	public Decimal Month10
	{
		get
		{
			return this.getMonthValue(10);
		}
		set
		{
			this.valueMap.put(10, value);
		}
	}
	public Decimal Month11
	{
		get
		{
			return this.getMonthValue(11);
		}
		set
		{
			this.valueMap.put(11, value);
		}
	}
	public Decimal Month12
	{
		get
		{
			return this.getMonthValue(12);
		}
		set
		{
			this.valueMap.put(12, value);
		}
	}
	public Decimal USDMonth1
	{
		get
		{
			return exchangeUSD(this.Month1, 1);
		}
	}
	public Decimal USDMonth2
	{
		get
		{
			return exchangeUSD(this.Month2, 2);
		}
	}
	public Decimal USDMonth3
	{
		get
		{
			return exchangeUSD(this.Month3, 3);
		}
	}
	public Decimal USDMonth4
	{
		get
		{
			return exchangeUSD(this.Month4, 4);
		}
	}
	public Decimal USDMonth5
	{
		get
		{
			return exchangeUSD(this.Month5, 5);
		}
	}
	public Decimal USDMonth6
	{
		get
		{
			return exchangeUSD(this.Month6, 6);
		}
	}
	public Decimal USDMonth7
	{
		get
		{
			return exchangeUSD(this.Month7, 7);
		}
	}
	public Decimal USDMonth8
	{
		get
		{
			return exchangeUSD(this.Month8, 8);
		}
	}
	public Decimal USDMonth9
	{
		get
		{
			return exchangeUSD(this.Month9, 9);
		}
	}
	public Decimal USDMonth10
	{
		get
		{
			return exchangeUSD(this.Month10, 10);
		}
	}
	public Decimal USDMonth11
	{
		get
		{
			return exchangeUSD(this.Month11, 11);
		}
	}
	public Decimal USDMonth12
	{
		get
		{
			return exchangeUSD(this.Month12, 12);
		}
	}
	
	//按可能性计算金额
	public Decimal PAmount
	{	
		get
		{
			Double p = 1.0;
			if(this.Opp.Probability != null)
			{
				p = this.Opp.Probability/100;
			}
			if(Opp.Amount == null)
			{
				return null;
			} 
			return this.Opp.Amount * p;
		}
	}
	
	public Decimal USDPAmount//总额按照当年最新汇率计算
	{
		get
		{
			if(this.PAmount == null)
			{
				return null;
			}
			if(RowType == Type.Forecast)
			{
				return exchangeUSD(this.PAmount, 12);
			}
			return null;
		}
	}
	
	public Decimal USDAmount//总额按照当年最新汇率计算
	{
		get
		{
			if(RowType == Type.Forecast)
			{
				return exchangeUSD(this.opp.Amount, 12);
			}
			return null;
		}
	}
	
	public void AddValue(Integer month, Decimal value)
	{
		this.valueMap.put(month, value);	
	}
	
	public enum Type { Forecast, Confirmed }
	public class ExchangeRateException extends Exception {}
}