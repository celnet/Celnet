<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Provides Google AdWords and Web-to-Lead tracking capability.  Understand which keywords, ads, campaigns, and landing pages are generating results for your company.</description>
    <label>Google AdWords</label>
    <tab>GoogleAdWordsSetup</tab>
    <tab>standard-Lead</tab>
    <tab>standard-Opportunity</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Problems_Fix__c</tab>
    <tab>PO_Project__c</tab>
    <tab>Annual_Budget__c</tab>
    <tab>MonthlyExchangeRate__c</tab>
    <tab>DB_Notes__c</tab>
    <tab>Project_Forecast__c</tab>
</CustomApplication>
