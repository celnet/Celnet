package wsc;
 
import com.sforce.soap.enterprise.Connector;
import com.sforce.soap.enterprise.DeleteResult;
import com.sforce.soap.enterprise.EnterpriseConnection;
import com.sforce.soap.enterprise.Error;
import com.sforce.soap.enterprise.QueryResult;
import com.sforce.soap.enterprise.SaveResult;
import com.sforce.soap.enterprise.sobject.Account;
import com.sforce.soap.enterprise.sobject.Contact;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;
 
public class Main {
 
static final String USERNAME = "steven@heroku.com";
    static final String PASSWORD = "online2014";
    static EnterpriseConnection connection;
 
    public static void main(String[] args) {
 
        ConnectorConfig config = new ConnectorConfig();
        config.setUsername(USERNAME);
        config.setPassword(PASSWORD);
        // config.setTraceMessage(true);
 
        try {
         
            connection = Connector.newConnection(config);
 
            // display some current settings
            System.out.println("Auth EndPoint: " + config.getAuthEndpoint());
            System.out.println("Service EndPoint: "
                    + config.getServiceEndpoint());
            System.out.println("Username: " + config.getUsername());
            System.out.println("SessionId: " + config.getSessionId());
         
            System.out.println("**********************************");
             
            com.sforce.soap.HelloService.SoapConnection soap =    com.sforce.soap.HelloService.Connector.newConnection("","");
            soap.setSessionHeader(config.getSessionId());
                         
            System.out.println(soap.sayHello("Google"));
             
             
 
            // run the different examples
            //queryContacts();
            //createAccounts();
            //updateAccounts();
            //deleteAccounts();
 
        } catch (ConnectionException e1) {
            e1.printStackTrace();
        }
 
    }
}