global class StoreVisitPlanBatch implements Database.Batchable<sObject>, Schedulable{
	global Database.Querylocator start(Database.BatchableContext bc){
		// 获取所有Store
		return Database.getQueryLocator('Select Id, Name, Sales_Rep__c, Sales_Rep__r.Name From Store__c Where Sales_Rep__c != null');
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<Task> taskList = new List<Task>();
		Map<String, Id> userMap = new Map<String, Id>();
		for(User u : [Select Id, Field_Sales_Code__c From User Where Field_Sales_Code__c != null]) {
			userMap.put(u.Field_Sales_Code__c, u.Id);
		}
		
		for(Store__c s : (List<Store__c>)scope){
			Id userId = userMap.get(s.Sales_Rep__r.Name);
			if(userId != null){
				Task t = new Task(); 
		        t.OwnerId = userId;
		        t.WhatId = s.Id;
		        t.Description = 'Visit Store';
		        t.ActivityDate = Date.today().addMonths(2).addDays(-1);
		        t.Subject = 'Visit Store: ' + s.Name;
		        t.IsReminderSet = true;
				t.ReminderDateTime = Datetime.newInstance(Date.today().year(), (Date.today().month() + 1), 1, 8, 0, 0);
		        
		        taskList.add(t);
			}
		}
		
		if(taskList.size() > 0){
			insert taskList;
		}
	}
	
	global void finish(Database.BatchableContext BC){
		
	}
	
	global void execute(SchedulableContext SC) {
      	StoreVisitPlanBatch svpb = new StoreVisitPlanBatch();
      	Database.executeBatch(svpb);
   	}
}