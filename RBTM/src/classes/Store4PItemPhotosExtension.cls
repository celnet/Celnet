public class Store4PItemPhotosExtension{
    public List<Attachment> atts{get;set;}
    public Integer attNumber{get;set;}
    //List<FeedItem> fis;
    
    public Store4PItemPhotosExtension(ApexPages.StandardController controller){
        atts = [Select Id, CreatedDate From Attachment Where ParentId =: controller.getId()];
        attNumber = atts.size();
       // fis = [Select Id From FeedItem];
    }
}