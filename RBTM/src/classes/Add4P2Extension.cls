global with sharing class Add4P2Extension {
    public String displaynocheckin{get;set;}
    public String displayRecordType{get;set;}
    
    public List<SelectOption> store4PRecordType {
        get {
            List<SelectOption> options = new List<SelectOption>();
            List<RecordType> recordTypes = [Select Id, Name From RecordType Where SobjectType = 'Store_4P__c'];
            options.add(new SelectOption('','--None--'));
            for(RecordType rt : recordTypes){
                options.add(new SelectOption(rt.Id, rt.Name));
            }
            return options;
        }
        private set;
    }
    
    global Add4P2Extension(ApexPages.StandardController controller) {
        User currentUser = [Select Id, Name, Field_Sales_Code__c From User Where Id =: UserInfo.getUserId()];
        Field_Sales__c currentFS = [Select Id, Name From Field_Sales__c Where Name =: currentUser.Field_Sales_Code__c];
        List<Checkin_History__c> chList = [Select Id From Checkin_History__c 
                                            Where Store__c =: controller.getId() 
                                            And Field_Sales__c =: currentFS.Id
                                            And Checkin_Time__c != null
                                            And Checkout_Time__c = null];
        
        if(chList.size() == 0){
            // 尚未Check in
            displaynocheckin = 'margin-top:80px;';
            displayRecordType = 'display:none;';
        } else {
            displaynocheckin = 'display:none;';
            displayRecordType = 'margin-top:120px;';
        }
    }
}