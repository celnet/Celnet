trigger Store4P on Store_4P__c (before insert) {
	User u = [Select Id, Field_Sales_Code__c From User Where Id =: UserInfo.getUserId()];
	Field_Sales__c fs = [Select Id From Field_Sales__c Where Name =: u.Field_Sales_Code__c];
	List<Checkin_History__c> chList = [Select Id, Store__c From Checkin_History__c 
										Where Field_Sales__c =: fs.Id 
										and Checkin_Time__c >=: Date.today() 
										and Checkout_Time__c = null 
										Order by LastModifiedDate desc];
	if(chList.size() > 0){
		for(Store_4P__c s4 : trigger.new){
			s4.Checkin_History__c = chList[0].Id;
			s4.Store__c = chList[0].Store__c;
		}
	}
}