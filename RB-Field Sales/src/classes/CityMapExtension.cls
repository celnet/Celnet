/**
 * Author: Steven
 * Date: 2014-9-3
 * Description: Monthly Plan Map
 */
global class CityMapExtension {
	
	global CityMapExtension(ApexPages.StandardController controller){
		currentMonth = 'Current Month【' + (Date.today().year() + '-' + Date.today().month())+'】';
		City__c c = [Select Id, City_Local_Name__c From City__c Where Id =: controller.getId()];
		currentCity = c.City_Local_Name__c;
		
		List<Plan_Item__c> planItems = [Select Id, Store__c, Status__c, Store__r.Name, 
							Store__r.Geolocation__Latitude__s, Store__r.Geolocation__Longitude__s 
						From Plan_Item__c 
						Where Date__c >=: Date.today().toStartOfMonth() 
						And Date__c <=: Date.today().addMonths(1).toStartOfMonth().addDays(-1)
						And Store__r.City_Local__c =: currentCity];
		
		totalPlanSize = planItems.size();
		uncoveredSize = 0;
		coveredWithPhotoSize = 0;
		coveredWithoutPhotoSize = 0;
		
		for(Plan_Item__c pi : planItems){
			if(pi.Status__c == 'UnCovered'){
				uncoveredSize++;
			} else if(pi.Status__c == 'Covered With Photo'){
				coveredWithPhotoSize++;
			} else if(pi.Status__c == 'Covered Without Photo'){
				coveredWithoutPhotoSize++;
			}
		}
	}
	
	public String currentMonth{get;set;}
	public String currentCity{get;set;}
	
    public integer totalPlanSize{get;set;}
    public integer uncoveredSize{get;set;}
    public integer coveredWithPhotoSize{get;set;}
    public integer coveredWithoutPhotoSize{get;set;}
    
    @RemoteAction
    // retrieve Monthly Plans
    global static List<Plan_Item__c> retrievePlanItems(String city) {
        return [Select Id, Plan_Calls__c, Plan_Photos__c, Actual_Calls__c, Actual_Photos__c, Store__c, Status__c, 
			        Store__r.Geolocation__Latitude__s, Store__r.Geolocation__Longitude__s, Store__r.Name, 
			        Store__r.OwnerId, Store__r.Owner.Name, Store__r.City__c, Store__r.City_Local__c 
        		From Plan_Item__c 
		        Where Date__c >=: Date.today().toStartOfMonth() 
		        And Date__c <=: Date.today().addMonths(1).toStartOfMonth().addDays(-1)
		        And Store__r.City_Local__c =: city];
        
    }
    
}