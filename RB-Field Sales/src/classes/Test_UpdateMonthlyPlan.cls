/*
 *date:2014-09-01
 *owner:mark
 *function:测试UpdateMonthlyPlan
 */
@isTest
private class Test_UpdateMonthlyPlan {

    static testMethod void myUnitTest() 
    {
    	system.Test.startTest();
    	
    	Account customer = new Account();
    	customer.Name = 'test';
    	insert customer;
    	
    	Chain__c chain = new Chain__c();
    	insert chain;
    	
    	Store__c store = new Store__c();
    	store.Name = 'test';
    	store.Chain__c = chain.id;
    	store.Geolocation__Latitude__s = 39.918340;
    	store.Geolocation__Longitude__s = 116.443084;
    	store.Customer__c = customer.Id;
    	insert store;
    	
    	Profile pf = [select id from Profile where name = 'FA'];
		User rec = new User(
	      Username = 'psateam@thoughtworks.com',
	      FirstName = 'PSA',
	      LastName = 'Unit Test User',
	      Email = 'psateam@thoughtworks.com',
	      Alias = 'PSA Test',
	      CommunityNickname = 'psateam@thoughtworks.com',
	      TimeZoneSidKey = 'America/Chicago',
	      LocaleSidKey = 'en_US',
	      EmailEncodingKey = 'UTF-8',
	      LanguageLocaleKey = 'en_US',
	      Profileid = pf.Id,
	      IsActive = true
	    );
        
        insert rec;
    	
    	Plan_Item__c pi = new Plan_Item__c();
    	pi.Store__c = store.id;
    	pi.Plan_Calls__c = 2;
    	pi.Plan_Photos__c = 2;
    	pi.OwnerId = rec.id;
    	pi.Date__c = Date.today();
    	insert pi;
    	
    	Checkin_History__c ch = new Checkin_History__c();
    	ch.Checkin_Location__Latitude__s = 39.918340;
    	ch.Checkin_Location__Longitude__s = 116.443084;
    	ch.Checkout_Location__Latitude__s = 39.918340;
    	ch.Checkout_Location__Longitude__s = 116.443084;
    	ch.Store__c = store.id;
    	ch.OwnerId = rec.id;
    	ch.Check_Valid_Range__c = 1;
    	insert ch;
    	
    	
    	Checkin_History__c c = [select Checkin_Presence__c , Checkout_Presence__c from Checkin_History__c where id = :ch.id];
    	
    	Store_4P__c sp = new Store_4P__c();
    	sp.Checkin_History__c = ch.id;
    	insert sp;
    	
    	Attachment a=new Attachment();
    	a.parentId=sp.Id;
    	Blob b=Blob.valueOf('aa');
    	a.Body=b;
    	a.name='test';
    	insert a;
    	
    	Store_4P__c p = [select Photos__c from Store_4P__c where id = :sp.id];
    	
    	System.schedule('AutoUpdateMonthlyPlan2' , '0 0 0 1 * ?' , new UpdateMonthlyPlan());   
    	
    	system.Test.stopTest();
    	
    	Date tDate = Date.today();
		Date monthDate = Date.newInstance(tDate.year(), tDate.month(), 1);
    	string key = pi.OwnerId + '_' + string.valueof(pi.Store__c) + '_' + string.valueOf(monthDate);
    	
    	Plan_Item__c newPi = [select Actual_Calls__c , Actual_Photos__c from Plan_Item__c where Unique_ID__c = :key];
    	
    	system.assertEquals(1, newPi.Actual_Calls__c);
    	system.assertEquals(1, newPi.Actual_Photos__c);
    }
}