/*
 * Author: mark
 * Date: 2014-9-1
 * function: Monthly Copy PlanSchema To MonthlyPlan
 * start way: System.schedule('CopyPlanSchemaToMonthlyPlan','0 0 0 1 * *', new CopyPlanSchemaToMonthlyPlan());
*/
global class CopyPlanSchemaToMonthlyPlan implements Schedulable
{
	global void execute(SchedulableContext sc) 
	{
		List<Plan_Schema__c> planList = new List<Plan_Schema__c>();
		planList = [select Calls__c, Photos__c, Store__c, Store_City__c, OwnerID from Plan_Schema__c];
		if(planList.isempty())
		{
			return;
		}
		Date tDate = Date.today();
		Date monthDate = Date.newInstance(tDate.year(), tDate.month(), 1);
		List<Plan_Item__c> itemList = new List<Plan_Item__c>();
		for(Plan_Schema__c ps : planList)
		{
			Plan_Item__c pi = new Plan_Item__c();
			pi.Plan_Calls__c = ps.Calls__c;
			pi.Plan_Photos__c = ps.Photos__c;
			pi.Store__c = ps.Store__c;
			pi.OwnerId = ps.OwnerID;
			pi.Date__c = monthDate;
			pi.Unique_ID__c = pi.OwnerId + '_' + string.valueof(ps.Store__c)  + '_' + string.valueOf(monthDate);
			itemList.add(pi);
		}
		if(!itemList.isempty())
		{
			insert itemList;
		}
	}
}