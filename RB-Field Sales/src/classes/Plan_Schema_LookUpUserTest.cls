/**
	作者：winter
	时间：2014-9-1
	功能：Plan_Schema_LookUpUser测试类
	覆盖率：100%
 */
@isTest
private class Plan_Schema_LookUpUserTest {

    static testMethod void myUnitTest() {
        //插入一个Plan_Schema__c
        Store__c s = new Store__c();
        s.Name = 'Plan_Schema_LookUpUserTest';
        insert s;
        
        //User owner = createUser(true,'owner@test.com','System Administrator');
        User u1 = createUser(true,'u1@test.com','System Administrator');
        User u2 = createUser(true,'u2@test.com','System Administrator');
        
        Plan_Schema__c ps = new Plan_Schema__c();
        ps.Store__c = s.Id;
        ps.User__c = u1.Id;
        insert ps;
        
        ps = [select ownerid from Plan_Schema__c where id =: ps.id];
        
        //验证结果(期望值，实际值)
        System.debug('@@@@@@@@@@2'+ps.OwnerId);
        System.assertEquals(u1.Id,ps.OwnerId);
        //修改Plan_Schema__c的User
        ps.User__c = u2.Id;
        update ps;
        
        ps = [select ownerid from Plan_Schema__c where id = :ps.id];
        
        //验证结果(期望值，实际值)
        System.assertEquals(u2.Id,ps.OwnerId);
    }
    
    //获取简档
    private static Profile queryProfile(string profileName)
    {
        Profile pf = [select id from Profile where name = : profileName];
        return pf;
    }
    
    //创建User
     public static User createUser(boolean doInsert , string name , string profile)
    {
        Profile pf = queryProfile(profile);
        User rec = new User(
			          		  Username = name,
					          FirstName = 'test',
					          LastName = 'Unit Test User',
					          Email = 'test@test.com',
					          Alias = 'test',
					          CommunityNickname = name,
					          TimeZoneSidKey = 'America/Chicago',
					          LocaleSidKey = 'en_US',
					          EmailEncodingKey = 'UTF-8',
					          LanguageLocaleKey = 'en_US',
					          Profileid = pf.Id,
					          IsActive = true
        					);
        
        if(doInsert)
        {
            insert rec;
        }
        return rec;
    }
}