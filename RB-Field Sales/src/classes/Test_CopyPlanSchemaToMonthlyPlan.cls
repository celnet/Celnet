/*
*Date : 2014-09-01
*owner: mark
*/
@isTest
private class Test_CopyPlanSchemaToMonthlyPlan {

    static testMethod void myUnitTest() 
    {
    	system.Test.startTest();
    	
    	Account customer = new Account();
    	customer.Name = 'test';
    	insert customer;
    	
    	Chain__c chain = new Chain__c();
    	insert chain;
    	
    	Store__c store = new Store__c();
    	store.Chain__c = chain.id;
    	store.Customer__c = customer.Id;
    	insert store;
    	
    	Profile pf = [select id from Profile where name = 'FA'];
		User rec = new User(
	      Username = 'psateam@thoughtworks.com',
	      FirstName = 'PSA',
	      LastName = 'Unit Test User',
	      Email = 'psateam@thoughtworks.com',
	      Alias = 'PSA Test',
	      CommunityNickname = 'psateam@thoughtworks.com',
	      TimeZoneSidKey = 'America/Chicago',
	      LocaleSidKey = 'en_US',
	      EmailEncodingKey = 'UTF-8',
	      LanguageLocaleKey = 'en_US',
	      Profileid = pf.Id,
	      IsActive = true
	    );
        
        insert rec;
       
    	Plan_Schema__c ps = new Plan_Schema__c();
    	ps.Calls__c = 2;
    	ps.Photos__c = 2;
    	ps.User__c = rec.id;
    	ps.Store__c = store.id;
    	insert ps;
    	
    	System.schedule('AutoCopyPlanSchemaToMonthlyPlan2' , '0 0 0 1 * ?' , new CopyPlanSchemaToMonthlyPlan());   
    	
    	system.Test.stopTest();
    	
    	Date tDate = Date.today();
		Date monthDate = Date.newInstance(tDate.year(), tDate.month(), 1);
    	
    	List<Plan_Item__c> itemList = [select id from Plan_Item__c where Date__c = :monthDate];
    	system.assertEquals(1, itemList.size());
    }
}