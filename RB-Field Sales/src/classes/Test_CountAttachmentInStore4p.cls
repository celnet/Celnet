/*
 *date:2014-09-02
 *owner:mark
 *function:测试CountAttachmentInStore4p
 */
@isTest
private class Test_CountAttachmentInStore4p {

    static testMethod void myUnitTest() 
    {
    	Store_4P__c sp = new Store_4P__c();
    	insert sp;
		
		Attachment a=new Attachment();
    	a.parentId=sp.Id;
    	Blob b=Blob.valueOf('aa');
    	a.Body=b;
    	a.name='test';
    	insert a;
    	
    	Store_4P__c newSp = [Select Photos__c, Id From Store_4P__c where id = :sp.id];
    	system.assertEquals(1, newSp.Photos__c);
    	
    	delete a;
    	
    	Store_4P__c newSp2 = [Select Photos__c, Id From Store_4P__c where id = :sp.id];
    	system.assertEquals(0, newSp2.Photos__c);
    }
}