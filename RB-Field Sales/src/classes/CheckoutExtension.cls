global class CheckoutExtension {
    public String displayexception{get;set;}
    public String displaycheckout{get;set;}

    global CheckoutExtension(ApexPages.StandardController controller) {
        // 查询出当前User对应的CheckinHistory
        List<Checkin_History__c> chList = [Select Id From Checkin_History__c 
                                            Where Store__c =: controller.getId() 
                                            And OwnerId =: UserInfo.getUserId()
                                            And Checkin_Time__c != null
                                            And Checkout_Time__c = null];
        if(chList.size() == 0){
            // 不存在CheckinHistory
            displayexception = '';
            displaycheckout = 'display:none;';
        } else {
            displayexception = 'display:none;';
            displaycheckout = '';
        }
    }
    
    @RemoteAction
    global static Sf1Result newSubmitData(CheckinHistoryWrapper chw){
        Id recordId;
        
        try{
            List<Checkin_History__c> chList = [Select Id From Checkin_History__c 
                                                Where Store__c =: chw.checkinHistory.Store__c
                                                And OwnerId =: UserInfo.getUserId()
                                                And Checkin_Time__c != null
                                                And Checkout_Time__c = null];
            
            List<Task> updateTaskList = [Select Id, Status From Task Where WhoId =: UserInfo.getUserId() 
                                            And WhatId =: chw.checkinHistory.Store__c 
                                            And ActivityDate =: Date.today().addMonths(1).toStartOfMonth().addDays(-1)];
            
            if(chList.size() > 0){
                Checkin_History__c updateCheckinHistory = chList[0];
                updateCheckinHistory.Checkout_Location__Latitude__s = chw.checkinHistory.Checkout_Location__Latitude__s;
                updateCheckinHistory.Checkout_Location__Longitude__s = chw.checkinHistory.Checkout_Location__Longitude__s;
                updateCheckinHistory.Checkout_Time__c = Datetime.now();
                update updateCheckinHistory;
                recordId = chw.checkinHistory.Id;
                if(updateTaskList.size() > 0){
                    for(Task t : updateTaskList){
                        t.Status = 'Completed';
                    }
                    update updateTaskList;
                }
            }
            
        } catch (Exception ex){
            return new Sf1Result(ex);
        }
        return new Sf1Result(recordId);
    }
    
    global class CheckinHistoryWrapper{
        global Checkin_History__c checkinHistory{get;set;}
    }
}