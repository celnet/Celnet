/*
*date : 2014-09-01
*owner: mark
*function : 测试PlanItemCreateTask
*/
@isTest
private class Test_PlanItemCreateTask {

    static testMethod void myUnitTest() 
    {
    	Account customer = new Account();
    	customer.Name = 'test';
    	insert customer;
    	
    	Chain__c chain = new Chain__c();
    	insert chain;
    	
    	Store__c store = new Store__c();
    	store.Name = 'test';
    	store.Chain__c = chain.id;
    	store.Customer__c = customer.Id;
    	insert store;
    	
    	Profile pf = [select id from Profile where name = 'FA'];
		User rec = new User(
	      Username = 'psateam@thoughtworks.com',
	      FirstName = 'PSA',
	      LastName = 'Unit Test User',
	      Email = 'psateam@thoughtworks.com',
	      Alias = 'PSA Test',
	      CommunityNickname = 'psateam@thoughtworks.com',
	      TimeZoneSidKey = 'America/Chicago',
	      LocaleSidKey = 'en_US',
	      EmailEncodingKey = 'UTF-8',
	      LanguageLocaleKey = 'en_US',
	      Profileid = pf.Id,
	      IsActive = true
	    );
        
        insert rec;
    	
    	Plan_Item__c pi = new Plan_Item__c();
    	pi.Store__c = store.id;
    	pi.Plan_Calls__c = 2;
    	pi.Plan_Photos__c = 2;
    	pi.OwnerId = rec.id;
    	pi.Date__c = Date.today();
    	insert pi;
    	
    	List<Task> tList = [select id from Task where ownerId = : rec.id];
    	
    	system.assertEquals(2, tList.size());
    	
    }
}