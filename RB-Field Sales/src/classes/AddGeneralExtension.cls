global class AddGeneralExtension {
    public Id storeId{get;set;}
    public Id historyId{get;set;}
    public String selectedOption{get;set;}
    public String selectedType{get;set;}
    
    public String displayGeneral{get;set;}
    public String displaytest{get;set;}
    public void displayGen(){
    	displayGeneral = '';
    	displaytest = 'display:none;';
    }
    
    public List<QuestionWrapper> qwList{get;set;}
    
    public boolean noqueryresult{
    	get{
    		return (qwList.size() == 0?true:false);
    	}
    	private set;
    }
    
    public Integer size{
    	get{
    		return qwList.size();
    	}
    	private set;
    }
    
    public List<SelectOption> options{
    	get{
		    return retrievePicklistValues('Question_Option__c');
    	}
    	private set;
    }
    
    public List<SelectOption> types{
    	get{
    		return retrievePicklistValues('Question_Type__c');
    	}
    	private set;
    }
    
    private List<SelectOption> retrievePicklistValues(String fieldName){
    	list<SelectOption> options = new list<SelectOption>();
	    Map<String, Schema.SObjectField> fieldMap = Question__c.sobjecttype.getDescribe().fields.getMap(); 
	    list<Schema.PicklistEntry> values = fieldMap.get(fieldName).getDescribe().getPickListValues();
	    for (Schema.PicklistEntry a : values)
	    { 
	       options.add(new SelectOption(a.getLabel(), a.getValue())); 
	    }
	    return options;
    }
    
    public void queryQuestions(){
    	List<Question__c> questions = [Select Question__c, Id, Question_Option__c, Question_Type__c, Question_Value_Type__c 
	    								From Question__c Where Question_Type__c =: selectedType 
	    								And Question_Option__c =: selectedOption];
	    qwList.clear();
	    for(Question__c q : questions){
	    	QuestionWrapper qw = new QuestionWrapper();
		    qw.question = q;
		    if(q.Question_Value_Type__c == 'Text'){
		    	qw.isString = true;
		    } else if(q.Question_Value_Type__c == 'Boolean'){
		    	qw.isBoolean = true;
		    } else if(q.Question_Value_Type__c == 'Number'){
		    	qw.isDouble = true;
		    }
	    	qwList.add(qw);
	    }
    }
    
    public void saveResult(){
    	List<Question_Result__c> qrList = new List<Question_Result__c>();
    	for(QuestionWrapper qw : qwList){
    		Question_Result__c qr = new Question_Result__c();
    		qr.Store__c = storeId;
    		if(historyId != null){
	    		qr.Checkin_History__c = historyId;
    		}
    		qr.Question_No__c = qw.question.Id;
    		qr.Question_Value_Type__c = qw.question.Question_Value_Type__c;
    		qr.Question_Type__c = qw.question.Question_Type__c;
    		qr.Question_Option__c = qw.question.Question_Option__c;
    		qr.Question__c = qw.question.Question__c;
    		if(qw.question.Question_Value_Type__c == 'Text'){
    			qr.Result_Value_Text__c = qw.text;
    		} else if(qw.question.Question_Value_Type__c == 'Boolean'){
    			qr.Result_Value_Boolean__c = qw.isChecked;
    		} else if(qw.question.Question_Value_Type__c == 'Number'){
    			qr.Result_Value_Number__c = qw.numbers;
    		}
    		
    		qrList.add(qr);
    	}
    	
    	if(qrList.size() > 0){
    		insert qrList;
    	}
    }
    
    public void cancel(){
    	
    }
    
    global AddGeneralExtension(ApexPages.StandardController controller) {
    	List<Checkin_History__c> chList = [Select Id, Store__c From Checkin_History__c 
                                        Where OwnerId =: UserInfo.getUserId() 
                                        and Checkin_Time__c >=: Date.today() 
                                        and Checkout_Time__c = null 
                                        Order by LastModifiedDate desc];
        if(chList.size() > 0){
        	historyId = chList[0].Id;
        }
        storeId = controller.getId();
        qwList = new List<QuestionWrapper>();
        displayGeneral = 'display:none;';
        displaytest = '';
    }  
    
    public class QuestionWrapper{
    	public Question__c question{get;set;}
    	public boolean isString{get;set;}
    	public boolean isBoolean{get;set;}
    	public boolean isDouble{get;set;}
    	
    	public String text{get;set;}
    	public boolean isChecked{get;set;}
    	public Double numbers{get;set;}
   		public QuestionWrapper(){
   			isString = false;
   			isBoolean = false;
   			isDouble = false;
   		}
    }
}