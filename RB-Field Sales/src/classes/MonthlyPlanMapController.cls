/**
 * Author: Steven
 * Date: 2014-9-3
 * Description: Monthly Plan Map
 */
global class MonthlyPlanMapController {
	public String titleWithCurrentMonth{
		get{
			if(titleWithCurrentMonth == null){
				return 'Current Month【' + (Date.today().year() + '-' + Date.today().month())+'】Store Check Status：';
			}else{
				return titleWithCurrentMonth;
			}
		}
	}
    public list<String> cities{
        get{
            if(cities == null){
                list<String> cts = new list<String>();
                Schema.DescribeFieldResult f = Store__c.City__c.getDescribe();
                for(Schema.PicklistEntry ple : f.getPicklistValues()){
                    String ct = hanziMap.get(ple.getValue().toLowerCase());
                    if(ct != null){
                        cts.add(ct);
                    }
                }
                return cts;
            } else {
                return cities;
            }
        }
        set;
    }
    
    public map<String, String> hanziMap{
        get{
            map<String,String> hm = new map<String,String>();
            hm.put('beijing','北京');
            hm.put('tianjin','天津');
            hm.put('shanghai','上海');
            // to add more...
            return hm;
        }
        set;
    }
    
    public list<Plan_Item__c> planItems{
        get{
            if(planItems != null){
                System.debug('========' + planItems[0].Store__r.Name);
                return planItems;
            } else {
                return [Select Id, Store__c, Status__c, Store__r.Geolocation__Latitude__s, Store__r.Geolocation__Longitude__s, Store__r.Name From Plan_Item__c Where Date__c >=: Date.today().toStartOfMonth() 
                                                                                    And Date__c <=: Date.today().addMonths(1).toStartOfMonth().addDays(-1)];
            }
        }
        private set;
    }
    
    public integer totalPlanSize{
        get{
            return totalPlanSize == null?planItems.size():totalPlanSize;
            /*
            if(totalPlanSize != null){
                return totalPlanSize;
            } else {
                return planItems.size();
            }*/
        }
        private set;
    }
    
    public integer uncoveredSize{
        get{
            if(uncoveredSize != null){
                return uncoveredSize;
            } else {
                Integer size = 0;
                for(Plan_Item__c pi : planItems){
                    if(pi.Status__c == 'UnCovered'){
                        size++;
                    }
                }
                
                return size;
            }
        }
        private set;
    }
    
    public integer coveredWithPhotoSize{
        get{
            if(coveredWithPhotoSize != null){
                return coveredWithPhotoSize;
            } else {
                Integer size = 0;
                for(Plan_Item__c pi : planItems){
                    if(pi.Status__c == 'Covered With Photo'){
                        size++;
                    }
                }
                
                return size;
            }
        }
        private set;
    }
    
    public integer coveredWithoutPhotoSize{
        get{
            if(coveredWithoutPhotoSize != null){
                return coveredWithoutPhotoSize;
            } else {
                Integer size = 0;
                for(Plan_Item__c pi : planItems){
                    if(pi.Status__c == 'Covered Without Photo'){
                        size++;
                    }
                }
                
                return size;
            }
        }
        private set;
    }
    
    
    @RemoteAction
    // retrieve Monthly Plans
    global static List<Plan_Item__c> retrievePlanItems() {
        return [Select Id, Plan_Calls__c, Plan_Photos__c, Actual_Calls__c, Actual_Photos__c, Store__c, Status__c, Store__r.Geolocation__Latitude__s, Store__r.Geolocation__Longitude__s, Store__r.Name, Store__r.OwnerId, Store__r.Owner.Name, Store__r.City__c, Store__r.City_Local__c From Plan_Item__c Where Date__c >=: Date.today().toStartOfMonth() 
                        And Date__c <=: Date.today().addMonths(1).toStartOfMonth().addDays(-1)];
        
    }
    
}