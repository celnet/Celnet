/*
*Author: Tommy
*Created On: 2014-8-25 
*Function: 根据Store Check Plan生成Store Check Performance的Plan部分的字段指标，记录类型为Calls的
*每天晚上10：00运行一次，每次更新本月的Performance记录，跳月在更新/创建下一个月的
*/
global class PerformancePlanCallsBatch //implements Database.Batchable<SObject>, Database.Stateful, Schedulable
{
	/*
	private map<String, Store_Check_Performance__c> profMap;
	private Date month;
	private map<String, ID> rdTypeMap;
	
	global PerformancePlanCallsBatch()
	{
		this.profMap = new map<String, Store_Check_Performance__c>();
		this.month = Date.today().toStartOfMonth();//每个月生产一条记录
		this.rdTypeMap = new map<String, ID>();
		for(RecordType rt : [Select Name, DeveloperName, Id From RecordType Where SobjectType = 'Store_Check_Performance__c'])
		{
			this.rdTypeMap.put(rt.DeveloperName, rt.Id);
		}
	}
	
  	global DataBase.Querylocator start(DataBase.BatchableContext BC)
  	{
    	return DataBase.getQueryLocator('Select Store__r.City__c, Store__r.Store_Type__c, Store__r.Customer__c, Store__r.Chain__c, Store__r.Province__c, Store__r.Id, Store__c, Photo__c, OwnerId, Name, RecordTypeID, Id From Store_Check_Plan__c ');
	}
	
	//此ID保证用Upsert方法每个月产生一条记录
	private String GetProfID(Store_Check_Plan__c plan)
	{
		String ID = plan.OwnerId != null? plan.OwnerId:'NULL';
		ID += '-';
		ID += this.month != null? this.month.format():'NULL';
		ID += '-';
		ID += plan.Store__r != null && plan.Store__r.City__c != null? plan.Store__r.City__c:'NULL';
		ID += '-';
		ID += 'Calls';
		ID += '-';
		ID += 'Monthly';
		return ID;
	}
	
	private Store_Check_Performance__c initPerformance()
	{
		Store_Check_Performance__c per = new Store_Check_Performance__c();
		per.Actual_Hyper__c = 0;
		per.Actual_Mini__c = 0;
		per.Actual_Photo__c = 0;
		per.Actual_Super__c = 0;
		per.Actual_Total__c = 0;
		per.Plan_Hyper__c = 0;
		per.Plan_Mini__c = 0;
		per.Plan_Photo__c = 0;
		per.Plan_Super__c = 0;
		per.Plan_Total__c = 0;
		return per;
	}
	
	global void execute(DataBase.BatchableContext BC, List<SObject> scope)
	{
		for(SObject obj : scope)
		{
			Store_Check_Plan__c plan = (Store_Check_Plan__c)obj;
			String perId = this.GetProfID(plan);//计算出ID表示当前的Plan应该并入到哪个Performance中
			if(!this.profMap.containsKey(perId))
			{
				Store_Check_Performance__c per = this.initPerformance();
				per.Unique_ID__c = perId;
				if(plan.Store__r != null)
				{
					per.City__c = plan.Store__r.City__c;
					per.Province__c = plan.Store__r.Province__c;
				}
				per.Date__c = this.month;
				per.OwnerId = plan.OwnerId;
				per.Time_Detail__c = 'Monthly';
				per.RecordTypeId = this.rdTypeMap.get('Calls');
				//TODO: Cover Or Call?
				this.profMap.put(per.Unique_ID__c, per);
			}
			Store_Check_Performance__c per = this.profMap.get(perId);
			if(plan.Store__r != null && plan.Store__r.Store_Type__c == 'Hyper')
			{
				per.Plan_Hyper__c ++;
			}
			if(plan.Store__r != null && plan.Store__r.Store_Type__c == 'Super')
			{
				per.Plan_Super__c ++;
			}
			if(plan.Store__r != null && plan.Store__r.Store_Type__c == 'Mini')
			{
				per.Plan_Mini__c ++;
			}
			if(plan.Store__c != null)
			{
				per.Plan_Total__c ++;
			}
			per.Plan_Photo__c += plan.Photo__c;
		}
	}
	
	global void finish(DataBase.BatchableContext BC)
	{
		List<Store_Check_Performance__c> pers = this.profMap.values();
		upsert pers Unique_ID__c;
	}
	
	global void execute(SchedulableContext SC) 
	{
      	database.executeBatch(this);
   	}
   	*/
}