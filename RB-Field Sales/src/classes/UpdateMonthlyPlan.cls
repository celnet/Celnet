/*
 * Author: mark
 * Date: 2014-9-1
 * function: Update MonthlyPlan Everyday
 * start way: System.schedule('UpdateMonthlyPlanEveryDay','0 0 0 * * ?', new UpdateMonthlyPlan());
*/
global class UpdateMonthlyPlan implements Schedulable
{
	global void execute(SchedulableContext sc)
	{
		map<string , Plan_Item__c> checkMap = new map<string , Plan_Item__c>();
		Date tDate = Date.today();
		Date monthDate = Date.newInstance(tDate.year(), tDate.month(), 1);
		for(Checkin_History__c ch : [select id, (Select Photos__c From Store_4Ps__r), OwnerId, Store__c from Checkin_History__c where CreatedDate = THIS_MONTH and Checkin_Presence__c = true and Checkout_Presence__c = true])
		{
			Decimal c = 0;
			for(Store_4P__c sp : ch.Store_4Ps__r)
			{
				c +=sp.Photos__c;
			}
			string key = ch.OwnerId + '_' + string.valueof(ch.Store__c) + '_' + string.valueOf(monthDate);
			if(!checkMap.containsKey(key))
			{
				Plan_Item__c  pi = new Plan_Item__c();
				pi.Unique_ID__c = key;
				pi.Actual_Calls__c = 1;
				pi.Actual_Photos__c = c;
				checkMap.put(key , pi);
			}
			else
			{
				Plan_Item__c pi = checkMap.get(key);
				Decimal ti = pi.Actual_Calls__c;
				pi.Actual_Calls__c = ti + 1;
				pi.Actual_Photos__c += c;
				checkMap.put(key , pi);
			}
		}
		if(!checkMap.isempty())
		{
			upsert checkMap.values();
		}
	}
}