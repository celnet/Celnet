/*
*date: 2014-09-02
*owner:mark
*function:统计store4p下的附件数量
*/
trigger CountAttachmentInStore4p on Attachment (after insert , after delete) 
{
	map<id , Integer> attMap = new map<id , Integer>();
	List<Store_4P__c> storeList = new List<Store_4P__c>();
	
	if(trigger.isDelete)
	{
		for(Attachment a:trigger.old)
		{ 
			if(attMap.containskey(a.parentId))
			{
				Integer c = attMap.get(a.parentId);
				attMap.put(a.parentId , c-1);
			}
			else
			{
				attMap.put(a.parentId , -1);
			}
		}
	}
	else
	{
		for(Attachment a:trigger.new)
		{ 
			if(attMap.containskey(a.parentId))
			{
				Integer c = attMap.get(a.parentId);
				attMap.put(a.parentId , c+1);
			}
			else
			{
				attMap.put(a.parentId , 1);
			}
		}
	}
	
	for(Store_4P__c sp : [Select Photos__c, Id From Store_4P__c where id in : attMap.keyset()])
	{
		Integer c = attMap.get(sp.id);
		sp.Photos__c += c; 
		storeList.add(sp);
	}
	
	if(!storeList.isempty())
	{
		update storeList;	
	}
}