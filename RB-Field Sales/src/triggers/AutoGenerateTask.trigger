/*
*date : 2014-09-01
*owner: mark
*/
trigger AutoGenerateTask on Plan_Item__c (after insert) 
{
    List<Task> taskList = new List<Task>();
    
    for(Plan_Item__c pi : Trigger.new)
    {
        if(pi.Plan_Calls__c > 0)
        {
            for(Integer i = 0 ;i < pi.Plan_Calls__c ; i++)
            {
                Task t = createTask(pi);
                taskList.add(t);
            }
        }
    }
    
    if(!taskList.isempty())
    {
        insert taskList;
    }
    
    public Task createTask(Plan_Item__c pi)
    {
        Task t = new Task(); 
        t.OwnerId = pi.OwnerId;
        t.WhatId = pi.Id;
        t.Description = 'Visit Store';
        t.ActivityDate = Date.today().addMonths(2).addDays(-1);
        t.Subject = 'Visit Store: ' + pi.Store_Name__c;
        t.IsReminderSet = true;
        t.ReminderDateTime = getRemindDate(pi.Date__c);
        return t;
    }
    public DateTime getRemindDate(Date d)
    {
        string month1 = ',1,3,5,7,8,10,12,';
        string month2 = ',4,6,9,11,';
        
        string month =  ',' + string.valueOf(d.Month()) + ',';
        if(month1.indexof(month) > 0)
        {
            return DateTime.newInstance(d.year(), d.month() , 31 , 8 , 0 , 0);
        }
        else if(month2.indexof(month) > 0)
        {
            return DateTime.newInstance(d.year(), d.month() , 30 , 8 , 0 , 0);
        }
        else
        {
            return Math.mod(d.year() , 4) == 0? (DateTime.newInstance(d.year(), d.month() , 29 , 8 , 0 , 0)) : (DateTime.newInstance(d.year(), d.month() , 28 , 8 , 0 , 0));
        }
    }
}