trigger AutoSetValidRangeAndStoreGeo on Checkin_History__c (before insert) 
{
    for(Checkin_History__c ch : trigger.new)
    {
        if(RB_Setting__c.getInstance('Check Valid Range') != null)
        {
            ch.Check_Valid_Range__c = RB_Setting__c.getInstance('Check Valid Range').Value__c;
        }
        
        if(ch.Store__c != null)
        {
            Store__c s = [Select Geolocation__Latitude__s, Geolocation__Longitude__s From Store__c Where Id =: ch.Store__c];
            ch.Store_Geolocation__Latitude__s = s.Geolocation__Latitude__s;
            ch.Store_Geolocation__Longitude__s = s.Geolocation__Longitude__s;
        }
    }
}