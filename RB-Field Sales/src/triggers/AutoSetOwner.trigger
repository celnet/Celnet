/*
    作者：winter
    时间：2014-9-1
    功能：创建Plan_Schema，将其Owner修改为指定的User（新字段,在布局上此字段为必须）
*/
trigger AutoSetOwner on Plan_Schema__c (before insert, before update) 
{
    if(trigger.isInsert)
    {
        for(Plan_Schema__c ps : trigger.new)
        {
            if(ps.User__c != null)
            {
                ps.OwnerId = ps.User__c;
            }
        }
    }
    if(trigger.isUpdate)
    {
        for(Plan_Schema__c ps : trigger.new)
        {
            if(ps.User__c != null)
            {
                ps.OwnerId = ps.User__c;
            }
        }
    }
}