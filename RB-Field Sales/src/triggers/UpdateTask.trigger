/**
 * Author: Steven
 * Date: 2014-9-2
 * Description: 更新Task到In progress和 Completed
 */
trigger UpdateTask on Checkin_History__c (after insert, after update) {
    // 查询出当前月份的所有 Task
    list<Task> tasks = [Select Id, Status, WhatId From Task 
                                            Where ActivityDate >: Date.today().toStartOfMonth() 
                                            And ActivityDate <: Date.today().toStartOfMonth().addMonths(1).addDays(-1)];
    
    // Task对应的 WhatId
    Set<Id> chIds = new Set<Id>();
    
    for(Task t : tasks){
    	chIds.add(t.WhatId);
    }
    
    // 查询出当前月份所有Task的 WhatId对应的Checin History
    map<Id,Checkin_History__c> chs = new map<Id,Checkin_History__c>([Select Id, Store__c From Checkin_History__c Where Id IN: chIds]);
    
    // 待更新的updateTask
    list<Task> updateTasks = new list<Task>();
    
    for(Checkin_History__c ch : trigger.new){
        if(ch.Checkin_Presence__c){
            for(task t : tasks){
                if(chs.get(t.WhatId) != null && chs.get(t.WhatId).Store__c == ch.Store__c && t.Status == 'Not Started'){
                    t.Status = 'In Progress';
                    updateTasks.add(t);
                }
            }
        }
        
        if(ch.Checkout_Presence__c){
             for(task t : tasks){
                    if(chs.get(t.WhatId) != null && chs.get(t.WhatId).Store__c == ch.Store__c && t.Status == 'In Progress'){
                        t.Status = 'Completed';
                        updateTasks.add(t);
                    }
             }
        }
    }
    
    update updateTasks;
}