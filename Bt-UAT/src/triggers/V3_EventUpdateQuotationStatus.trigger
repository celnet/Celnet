/**
发送quotation email 之后，更新quotaiton 状态为sent
harvery 2012.9.29
**/

trigger V3_EventUpdateQuotationStatus on Task (after insert) {
   set<ID> setRelationID = new Set<ID>();//event中相关项Id集合；
   for(Task evt:trigger.new){
   		setrelationID.add(evt.whatid);
   } 
   
   //查找对应的quotation
   Quote[] lstQuotes = [select Status from Quote where id in :setRelationID];
   for(Quote qte:lstQuotes){
   	 qte.Status='Sent';
   } 
   update lstQuotes;
}