/*
 * Tobe
 * 2013.10.23
 * 病人insert或update时，如果联系方式优先个数不足2个，则按优先级，
 * 为有值的联系电话字段对应的优先字段赋值为True，上限为2个
 * 2013.10.27 Sunny 添加逻辑：
 * 1.病人创建时，自动将病人的终端配送商赋值为插管医院，病人的经销商=插管医院的经销商
 * 2.病人的终端配送商改变时，自动更新病人的经销商=终端配送商的经销商
 * 2013.11.28 Sunny 添加逻辑：
 * 1.病人创建时，将创建人的别名赋值到病人创建人字段PM_Temporary_PatientCreator__c
 * 2.病人更新时，将病人的修改人的别名赋值到病人修改人字段PM_Temporary_PatientModifier__c
 * 3.病人更新时，将病人的修改时间赋值到病人最后修改时间字段PM_Temporary_PatientModifyTime__c
 */
trigger PM_AutoFillContactPriorityForPatient on PM_Patient__c (before insert, before update) 
{
    //sunny 添加逻辑开始
    List<ID> listTeDisId = new List<ID>();
    //获取系统管理员的简档Id
    ID userProID = [Select ID from Profile where Name = '系统管理员'][0].Id;
    if(trigger.isInsert)
    {
        for(PM_Patient__c patient : trigger.new)
        {
            if(patient.PM_Temporary_PatientCreator__c == null)
            {
                patient.PM_Temporary_PatientCreator__c = Userinfo.getName(); 
            }
            
            if(patient.PM_InHospital__c != null)
            {
                patient.PM_Terminal__c = patient.PM_InHospital__c ; 
                listTeDisId.add(patient.PM_InHospital__c);
            }
        }
    }
    else if(trigger.isUpdate)
    {
        for(PM_Patient__c patient : trigger.new)
        {
        	//排除系统管理员修改
            if(UserInfo.getProfileId() != userProID)
            {
                patient.PM_Temporary_PatientModifier__c = Userinfo.getName();
                patient.PM_Temporary_PatientModifyTime__c = patient.LastModifiedDate;
            }        
            if((patient.PM_Terminal__c != trigger.oldMap.get(patient.Id).PM_Terminal__c && patient.PM_Terminal__c != null)
            	|| (patient.PM_TerminalID__c != null && patient.PM_TerminalID__c.indexOf('001') >=0))
            {
            	listTeDisId.add(patient.PM_TerminalID__c==null?patient.PM_Terminal__c:patient.PM_TerminalID__c);
            }
            if(patient.PM_TerminalID__c != null && patient.PM_TerminalID__c.indexOf('001') >=0)
			{
				patient.PM_Terminal__c = patient.PM_TerminalID__c;
				patient.PM_TerminalID__c = null;
				patient.PM_TerminalName__c = null;
			}
        }
    }
    Map<ID,ID> map_HosDealerId = new Map<ID,ID>();
    if(listTeDisId.size() > 0)
    {
        for(PM_HosDealerRelation__c hdr : [Select id,PM_Distributor__c,PM_Hospital__c 
              From PM_HosDealerRelation__c Where PM_Hospital__c in: listTeDisId And PM_Status__c = '启用'])
        {
            map_HosDealerId.put(hdr.PM_Hospital__c , hdr.PM_Distributor__c);
        }
    }
    if(map_HosDealerId.size() > 0)
    {
        for(PM_Patient__c patient : trigger.new)
        {
            if(map_HosDealerId.containsKey(patient.PM_Terminal__c))
            {
                patient.PM_Distributor__c = map_HosDealerId.get(patient.PM_Terminal__c) ;
            }
        }
    }
    //sunny 添加逻辑结束
    static final integer PRIORITY_SIZE = 2;
    for(PM_Patient__c patient : trigger.new)
    {
        /***************************上次拜访成功失败 时间字段赋值****************************************/
        if(patient.PM_LastFailDateTime__c!=null)
        {
            string hour;
            string minute;
            if(patient.PM_LastFailDateTime__c.Hour()<10)
            {
                hour = '0'+patient.PM_LastFailDateTime__c.Hour();
            }
            else
            {
                hour = ''+patient.PM_LastFailDateTime__c.Hour();
            }
            if(patient.PM_LastFailDateTime__c.Minute()<10)
            {
                minute = '0'+patient.PM_LastFailDateTime__c.Minute();
            }
            else
            {
                minute = ''+patient.PM_LastFailDateTime__c.Minute();
            }
            patient.PM_LastFailTime2__c = hour+':'+minute;
        }
        else
        {
            patient.PM_LastFailTime2__c = null;
        }
        if(patient.PM_LastSuccessDateTime__c!=null)
        {
            string hour;
            string minute;
            if(patient.PM_LastSuccessDateTime__c.Hour()<10)
            {
                hour = '0'+patient.PM_LastSuccessDateTime__c.Hour();
            }
            else
            {
                hour = ''+patient.PM_LastSuccessDateTime__c.Hour();
            }
            if(patient.PM_LastSuccessDateTime__c.Minute()<10)
            {
                minute = '0'+patient.PM_LastSuccessDateTime__c.Minute();
            }
            else
            {
                minute = ''+patient.PM_LastSuccessDateTime__c.Minute();
            }
            patient.PM_LastSuccessTime2__c = hour+':'+minute;
        }
        else
        {
            patient.PM_LastSuccessTime2__c = null;
        }
        /***********************************电话优先级设置*************************************/
     /*   if(patient.PM_TelPriorityFirst__c != null && patient.PM_TelPriorityFirst__c != ''
        && patient.PM_TelPrioritySecond__c != null && patient.PM_TelPrioritySecond__c != '')
        {
            continue;
        }
        list<string> telNumbers = new list<string>();
        if(patient.PM_HomeTel__c != null && patient.PM_HomeTel__c != '')
        {
            telNumbers.add(PM_Patient__c.PM_HomeTel__c.getDescribe().getLabel());
        }
        if(patient.PM_PmPhone__c != null && patient.PM_PmPhone__c != '')
        {
            telNumbers.add(PM_Patient__c.PM_PmPhone__c.getDescribe().getLabel());
        }
        if(patient.PM_PmTel__c != null && patient.PM_PmTel__c != '')
        {
            telNumbers.add(PM_Patient__c.PM_PmTel__c.getDescribe().getLabel());
        }
        if(patient.PM_FamilyPhone__c != null && patient.PM_FamilyPhone__c != '')
        {
            telNumbers.add(PM_Patient__c.PM_FamilyPhone__c.getDescribe().getLabel());
        }
        if(patient.PM_FamilyTel__c != null && patient.PM_FamilyTel__c != '')
        {
            telNumbers.add(PM_Patient__c.PM_FamilyTel__c.getDescribe().getLabel());
        }
        if(patient.PM_OtherTel2__c != null && patient.PM_OtherTel2__c != '')
        {
            telNumbers.add(PM_Patient__c.PM_OtherTel2__c.getDescribe().getLabel());
        }
        if(telNumbers.size()>0 &&(patient.PM_TelPriorityFirst__c == null || patient.PM_TelPriorityFirst__c == ''))
        {
            patient.PM_TelPriorityFirst__c = telNumbers[0];
            telNumbers.remove(0);
        }
        if(telNumbers.size()>0 &&(patient.PM_TelPrioritySecond__c == null || patient.PM_TelPrioritySecond__c == ''))
        {
            for(string numberType : telNumbers)
            {
                if(numberType == patient.PM_TelPriorityFirst__c)
                {
                    continue;
                }
                else
                {
                    patient.PM_TelPrioritySecond__c = numberType;
                    break;
                }
            }
            
        }*/
        
    }
}