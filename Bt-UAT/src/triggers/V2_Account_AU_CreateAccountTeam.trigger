/* 
 * Author: Sunny
 * Created on: 2011-12-26
 * Description: 
 * 将Account的修改同步到Lead上
 * sunny 2012-2-21 注释掉根据account审批状态创建客户小组成员（account上审批状态字段需删除）
 */
trigger V2_Account_AU_CreateAccountTeam on Account (after update) 
{
	V2_SynchField objSynchField = new V2_SynchField() ;
	List<ID> list_AccIds = new List<ID>() ;
	Map<ID,List<V2_Account_Team__c>> map_AccIdAT = new Map<ID,List<V2_Account_Team__c>>() ;
	List<String> list_mid = new List<String>() ;
	for(Account objAccount : trigger.new)
	{
		if(objAccount.Mid__c != null)
		{
			list_mid.add(objAccount.Mid__c) ;
		}
		/*
		if(objAccount.V2_ApprovalStauts__c == trigger.oldMap.get(objAccount.Id).V2_ApprovalStauts__c)
		{
			
			continue ;
		}
		if(objAccount.V2_ApprovalStauts__c != '审批通过')
		{
			continue ;
		}
		list_AccIds.add(objAccount.Id) ;
		*/
	}
	/*
	if(list_AccIds.size() != 0)
	{
		List<V2_Account_Team__c> list_VAT  ;
		List<ID> list_DelUserId = new List<ID>() ;
		List<ID> list_DelAccId = new List<ID>() ;
		List<AccountTeamMember> list_ATM = new List<AccountTeamMember>();
		List<V2_Account_Team__c> list_AccT = new List<V2_Account_Team__c>() ;
		for(V2_Account_Team__c objAT : [Select Id,V2_Role__c,V2_Is_Delete__c,V2_ImmediateDelete__c,V2_User__c,V2_Account__c From V2_Account_Team__c Where V2_Account__c in: list_AccIds And V2_History__c = false ])
		{
			if(objAT.V2_Is_Delete__c )
			{
				if(objAT.V2_ImmediateDelete__c)
				{
					list_DelUserId.add(objAT.V2_User__c) ;
					list_DelAccId.add(objAT.V2_Account__c) ;
					objAT.V2_History__c = true ;
					list_AccT.add(objAT) ;
				}
				continue ;
			}
			if(map_AccIdAT.containsKey(objAT.V2_Account__c))
			{
				list_VAT = map_AccIdAT.get(objAT.V2_Account__c) ;
				list_VAT.add(objAT) ;
				map_AccIdAT.put(objAT.V2_Account__c, list_VAT) ;
			}else
			{
				list_VAT = New List<V2_Account_Team__c>() ;
				list_VAT.add(objAT) ;
				map_AccIdAT.put(objAT.V2_Account__c, list_VAT) ;
			}
		}
		
		for(Account objAccount : trigger.new)
		{
			if(map_AccIdAT.containsKey(objAccount.Id))
			{
				for(V2_Account_Team__c objVAT : map_AccIdAT.get(objAccount.Id))
				{
					AccountTeamMember objATM = new AccountTeamMember() ;
					objSynchField.cloneFieldFromAccountTeamToATM(objVAT, objATM) ;
					objATM.AccountId = objAccount.Id ;
					objVAT.V2_IsSyn__c = true ;
					list_ATM.add(objATM) ;
					list_AccT.add(objVAT) ;
				}
			}
		}
		if(list_ATM.size()!=0)
		{
			insert list_ATM ;
			update list_AccT ;
		}
		if(list_DelUserId.size() != 0 && list_DelAccId.size() != 0)
		{
			List<AccountTeamMember> list_ATMDel = [Select id From AccountTeamMember Where UserId in: list_DelUserId And AccountId in: list_DelAccId] ;
			if(list_ATMDel != null)
			{
				delete list_ATMDel ;
			}
		}
		
	}
	*/
	if(list_mid.size() != 0)
	{
		Map<String, Lead> map_midLead = new Map<String, Lead>() ;
		for(Lead objLead : [Select V2_Address__c, V2_BillingCity__c, V2_BillingCountry__c, 
				V2_BillingPostalCode__c, V2_BillingState__c, V2_BillingStreet__c, V2_Cities__c, 
				V2_GAccountType__c, V2_Grade__c, V2_InsuranceAmtByYear__c, V2_IsEducation__c, 
				V2_ManTimeHospital__c, V2_MedicineAmtByYear__c, V2_Mid__c,  V2_OperationTime__c, 
				V2_OutPatientAmtByMonth__c, V2_Ownership__c,  V2_PersonInHospital__c, V2_PostCode__c, 
				V2_Provinces__c, V2_RenalMarketType__c, V2_ShippingCity__c, V2_ShippingCountry__c, 
				V2_ShippingPostalCode__c, V2_ShippingState__c, V2_ShippingStreet__c, V2_Sic__c, 
				V2_Site__c, V2_Stauts__c, V2_TickerSymbol__c, V2_TotalBedsActual__c,  
				V2_TotalBedsStandard__c, V2_TotalDoctors__c, V2_AccountNumber__c, V2_Type__c, Phone, 
				Fax, Website, Industry, Description, AnnualRevenue,  NumberOfEmployees, Rating, Name 
				From Lead Where V2_Mid__c in: list_mid])
		{
			map_midLead.put(objLead.V2_Mid__c , objLead) ;
		}
		List<Lead> list_Lead = new List<Lead>() ;
		for(Account objAc : trigger.new)
		{
			if(map_midLead.containsKey(objAc.Mid__c))
			{
				Lead objL = map_midLead.get(objAc.Mid__c) ;
				objSynchField.cloneFieldFromAccountToLead(objL, objAc) ;
				list_Lead.add(objL) ;
			}
			
		}
		if(list_Lead.size() != 0)
		{
			update list_Lead ;
		}
		
	}
	
	
	
}