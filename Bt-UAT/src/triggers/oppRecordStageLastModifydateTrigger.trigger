trigger oppRecordStageLastModifydateTrigger on Opportunity (before update) {
    Map <id,List<OpportunityHistory >> oppHistoryMap = new Map <id,List<OpportunityHistory >>();
    List <OpportunityHistory> OppHistoryList= [Select o.SystemModstamp, o.OpportunityId
                                                     From OpportunityHistory o  
                                                     where o.opportunityId In: trigger.new];
    for(OpportunityHistory oppH:OppHistoryList){
        if(!oppHistoryMap.containsKey(oppH.OpportunityId)){
            List <OpportunityHistory> tempOppHistoryList = new List <OpportunityHistory>();
            tempOppHistoryList.add(oppH);
            oppHistoryMap.put(oppH.OpportunityId,tempOppHistoryList);           
        } else {
            List <OpportunityHistory> tempOppHistoryList = oppHistoryMap.get(oppH.OpportunityId);           
            tempOppHistoryList.add(oppH);
            oppHistoryMap.put(oppH.OpportunityId,tempOppHistoryList);           
        }   
    }
    for(Opportunity opp:trigger.new){
        List <OpportunityHistory> tempOppHistoryList = oppHistoryMap.get(opp.id);              
        if(tempOppHistoryList!=null&&tempOppHistoryList.size()!=0){
            Datetime lastmodifydatetime =  tempOppHistoryList[0].SystemModstamp;      
            for(OpportunityHistory oppH : tempOppHistoryList) {
                if(lastmodifydatetime < oppH.SystemModstamp)
                    lastmodifydatetime =  oppH.SystemModstamp;
                else
                    continue;   
            }      
            opp.Last_StageModify_DateTime__c = lastmodifydatetime;        
        }
          
    }
                                                        
}