/**
 * Author : Bill
 * date: 2013-9-9
 * 功能：1.当创建SP联系人修改时，自动把联系人的月全麻数量/台、月吸入比例（100%）、平均手术时长h
 * 七氟烷浓度%、七氟烷FGF(L/min)同步到联系人修改申请对应字段上来
 * 2.当SP联系人修改申请被审批通过后要把修改的信息同步到联系人信息上去
**/
trigger SP_ContactApprovalUpdate on Contact_Mod__c (after update, before insert, before update) {

	//记录类型ID 
	Set<ID> set_record = new Set<ID>();
	Id Updateid ;
	Id Insertid;
	list<RecordType> list_record = [Select r.DeveloperName, r.Id From RecordType r where r.DeveloperName in ('SP_Insert','SP_Update') and IsActive = true and r.SobjectType = 'Contact_Mod__c'];
	if(list_record != null && list_record.size() > 0)
    {
        for(RecordType record:list_record)
        {
            if(record.DeveloperName =='SP_Update')
            {
            	Updateid = record.Id;
            }
            else 
            {
            	Insertid = record.Id;
            }
            set_record.add(record.Id);
        }
    }
	//联系人ID
	Set<ID> set_ConIds = new Set<ID>();
	
	
	
	/*************************联系人修改申请被审批通过*******************************/
	//联系人Ids
	Set<ID> set_contact = new Set<ID>();
	//联系人修改申请
	Map<ID,Contact_Mod__c> map_conMod = new Map<ID,Contact_Mod__c>();
	//新增联系人
	Map<ID,Contact_Mod__c> map_conInsert = new Map<ID,Contact_Mod__c>();
	/************************联系人修改申请被审批通过*****************************/
	if(trigger.isInsert)
	{
		for(Contact_Mod__c con : trigger.new)
		{
			if(set_record.contains(con.RecordTypeId) && con.Name__c!=null)
			{
				set_ConIds.add(con.Name__c);
			}
		}
	}
	if(trigger.isUpdate)
	{
		
		for(Contact_Mod__c newcon : trigger.new)
		{
			if(!set_record.contains(newcon.RecordTypeId))
			{
				continue;
			}
			Contact_Mod__c oldcon = trigger.oldMap.get(newcon.Id);
			
			if(trigger.isBefore && newcon.Name__c != oldcon.Name__c && newcon.Name__c != null  )
			{
				set_ConIds.add(newcon.Name__c);
			}
			else if(trigger.isAfter &&  newcon.Status__c != oldcon.Status__c && newcon.Status__c == '通过')
			{
				if(newcon.RecordTypeId == Updateid)
				{
					set_contact.add(newcon.Name__c);
					map_conMod.put(newcon.Name__c, newcon);
				}
				else if(newcon.Account__c != null)
				{
					map_conInsert.put(newcon.Account__c, newcon);
				}
			}
		}
		
	}
	
	
	/*把联系人原信息保存到联系人修改申请上*/
	
	if(set_ConIds.size()>0)
	{
		for(Contact contact : [Select c.V2_PassPortNo__c, c.V2_OfficerNo__c, c.Phone, c.MobilePhone, c.ID_card__c,
								c.SP_DesConcentration__c, c.SP_DesAccLevel__c, c.SP_DesPotentialSale__c,SP_DesQtySale__c,
								c.Gender__c, c.GISContactType__c, c.Fax, c.Email, c.Description, c.ContactType__c,
								c.SP_inhaleMonthyNum__c, c.SP_SevoFGF__c, c.Birthdate, interest__c, 
								c.SP_SevoConcentration__c, c.SP_OperationTime__c, c.SP_Job__c, c.SP_Department__c, 
								c.SP_AnesthesiaMonthyNum__c,SP_DesFGF__c  From Contact c where Id in : set_ConIds])
		{
			for(Contact_Mod__c con : trigger.new)
			{
				if(con.Name__c == contact.Id)
				{
					//原手机	
					con.SP_OldPhone__c = contact.Phone;
					//原电话	
					con.SP_OldMobile__c = contact.MobilePhone;
					//原身份证号
					con.SP_OldIdcard2__c = contact.ID_card__c;
					//原性别	
					con.SP_OldGender__c = contact.Gender__c;
					//原兴趣爱好	
					con.SP_OldV2interest__c = contact.interest__c;
					//原生日	
					con.SP_OldBirthday__c = contact.Birthdate;
					//原护照号
					con.SP_OldV2PassPortNo__c = contact.V2_PassPortNo__c;
					//原军官号
					con.SP_OldV2OfficerNo__c = contact.V2_OfficerNo__c;
					//原联系人类别
					con.SP_OldContactType__c = contact.ContactType__c;
					//原电子邮件
					con.SP_OldEmail__c = contact.Email;
					//原传真	
					con.SP_OldFax__c = contact.Fax;
					//原备注	
					con.SP_OldComment__c = contact.Description;
					
					con.SP_OldInhaleMonthyNum__c = contact.SP_inhaleMonthyNum__c;
					con.SP_OldSevoFGF__c = contact.SP_SevoFGF__c;
					con.SP_OldSevoConcentration__c = contact.SP_SevoConcentration__c;
					con.SP_OldOperationTime__c = contact.SP_OperationTime__c;
					con.SP_OldJob__c = contact.SP_Job__c;
					con.SP_OldDepartment__c = contact.SP_Department__c;
					con.SP_OldAnesthesiaMonthyNum__c = contact.SP_AnesthesiaMonthyNum__c;
					con.SP_OldDesConcentration__c = contact.SP_DesConcentration__c;
					con.SP_OldDesFGF__c = contact.SP_DesFGF__c;
				}
			}
		}
	}
	
	RecordType rt  = [select Id from RecordType where SobjectType='Contact' and IsActive = true and DeveloperName = 'SP'];
	/*联系人修改申请被审批通过*/
	if(set_contact != null && set_contact.size() > 0)
	{
		List<Contact> list_Contact = [Select c.V2_PassPortNo__c, c.V2_OfficerNo__c, c.Phone, c.MobilePhone, c.ID_card__c, 
											c.Gender__c, c.GISContactType__c, c.Fax, c.Email, c.Description, c.ContactType__c,
										    c.Birthdate, c.SP_inhaleMonthyNum__c, c.Id, c.LastName, 
											c.SP_SevoQtySale__c, c.SP_SevoPotentialSale__c, 
											c.SP_SevoFGF__c, c.SP_SevoConcentration__c, c.SP_SevoAccLevel__c, 
											c.SP_OperationTime__c, c.SP_Job__c, c.SP_Department__c, 
											c.SP_DesConcentration__c , c.SP_DesAccLevel__c, c.SP_DesPotentialSale__c, c.SP_DesQtySale__c										
											From Contact c where Id in : set_contact];
		for(Contact con : list_Contact) 
		{
			if(!map_conMod.containsKey(con.Id))
			{
				continue;
			}
			con.LastName = map_conMod.get(con.Id).NewContact__c;
			//手机	
			con.Phone = map_conMod.get(con.Id).Phone__c;
			//电话	
			con.MobilePhone = map_conMod.get(con.Id).Mobile__c;
			//原身份证号
			con.ID_card__c = map_conMod.get(con.Id).Id_card2__c;
			//性别	
			con.Gender__c = map_conMod.get(con.Id).Gender__c;
			//兴趣爱好	
			con.interest__c = map_conMod.get(con.Id).V2_interest__c;
			//生日	
			con.Birthdate = map_conMod.get(con.Id).Birthday__c;
			//护照号
			con.V2_PassPortNo__c = map_conMod.get(con.Id).V2_PassPortNo__c;
			//军官号
			con.V2_OfficerNo__c = map_conMod.get(con.Id).V2_OfficerNo__c;
			//联系人类别
			con.ContactType__c = map_conMod.get(con.Id).Contact_Type__c;
			//电子邮件
			con.Email = map_conMod.get(con.Id).Email__c;
			//传真	
			con.Fax = map_conMod.get(con.Id).Fax__c;
			//GIS客户类型	
			con.GISContactType__c = map_conMod.get(con.Id).GISContactType__c;
			//备注	
			con.Description = map_conMod.get(con.Id).Comment__c;
			//记录类型
			con.RecordTypeId = rt.Id;
			//操作人
			con.SP_Approve__c = map_conMod.get(con.Id).OwnerId;
			//月吸入比例%
			con.SP_InhaleMonthyNum__c = map_conMod.get(con.Id).SP_inhaleMonthyNum__c;
			//七氟烷FGF(L/min)
			con.SP_SevoFGF__c = map_conMod.get(con.Id).SP_SevoFGF__c;
			//七氟烷浓度%
			con.SP_SevoConcentration__c = map_conMod.get(con.Id).SP_SevoConcentration__c;
			//平均手术时长h
			con.SP_OperationTime__c = map_conMod.get(con.Id).SP_OperationTime__c;
			//职务(SP)
			con.SP_Job__c = map_conMod.get(con.Id).SP_Job__c;
			//部门(SP)
			con.SP_Department__c = map_conMod.get(con.Id).SP_Department__c;
			//月全麻数量/台
			con.SP_AnesthesiaMonthyNum__c = map_conMod.get(con.Id).SP_AnesthesiaMonthyNum__c;
			//七氟烷实际销量（瓶）公式：月全麻数量/台*月吸入比例%*平均手术时长h*七氟烷浓度*七氟烷FGF (L/min)*3.3/250
			con.SP_SevoQtySale__c = 
				(con.SP_AnesthesiaMonthyNum__c == null?0:con.SP_AnesthesiaMonthyNum__c)*
				((con.SP_InhaleMonthyNum__c == null?0:con.SP_InhaleMonthyNum__c)/100)*
				(con.SP_SevoConcentration__c == null?0:con.SP_SevoConcentration__c)*
				(con.SP_OperationTime__c == null?0:con.SP_OperationTime__c)*
				(con.SP_SevoFGF__c == null?0:con.SP_SevoFGF__c)*3.3/250;
			//地氟烷浓度%
			con.SP_DesConcentration__c = map_conMod.get(con.Id).SP_DesConcentration__c;
			//地氟烷FGF(L/min)
			con.SP_DesFGF__c = map_conMod.get(con.Id).SP_DesFGF__c;
			/*
			//七氟烷潜力销量（瓶）：自动计算，计算公式：=月全麻数量/台*3*2*2*3.3/250
			con.SP_SevoPotentialSale__c = (con.SP_AnesthesiaMonthyNum__c == null?0:con.SP_AnesthesiaMonthyNum__c)*3*2*2*3.3/250;
			double sale = con.SP_SevoPotentialSale__c;
			string job = con.SP_Job__c;
			con.SP_SevoAccLevel__c = AccountLevelExec(sale, job);
			
			//地氟烷浓度%
			con.SP_DesConcentration__c = map_conMod.get(con.Id).SP_DesConcentration__c;
			//地氟烷FGF(L/min)
			con.SP_DesFGF__c = map_conMod.get(con.Id).SP_DesFGF__c;

			//地氟烷实际销量
			con.SP_DesQtySale__c = 
				(con.SP_AnesthesiaMonthyNum__c == null?0:con.SP_AnesthesiaMonthyNum__c)*
				((con.SP_InhaleMonthyNum__c == null?0:con.SP_InhaleMonthyNum__c)/100)*
				(con.SP_DesConcentration__c == null?0:con.SP_DesConcentration__c)*
				(con.SP_OperationTime__c == null?0:con.SP_OperationTime__c)*
				(con.SP_DesFGF__c == null?0:con.SP_DesFGF__c)*3.3/240;
			//地氟烷潜力销量
			con.SP_DesPotentialSale__c = (con.SP_AnesthesiaMonthyNum__c == null?0:con.SP_AnesthesiaMonthyNum__c)*3*2*6.5*3.3/240;
			double saleDes = con.SP_DesPotentialSale__c;
			//地氟烷客户等级
			con.SP_DesAccLevel__c = AccountDesLevelExec(saleDes, job);
			*/
		}
		
		if(list_Contact != null && list_Contact.size()>0)
		{
			update list_Contact;
		}
	}
	
	//插入联系人
	if(!map_conInsert.isEmpty())
	{
		//新联系人
		List<Contact> list_NewContact = new List<Contact>();
		for(Contact_Mod__c con : map_conInsert.values())
		{
			if(!map_conInsert.containsKey(con.Account__c))
			{
				continue;
			}
			Contact newCon = new Contact();
			newCon.AccountId = con.Account__c;
			newCon.LastName = map_conInsert.get(con.Account__c).NewContact__c;
			//手机	
			newCon.Phone = map_conInsert.get(con.Account__c).Phone__c;
			//电话	
			newCon.MobilePhone = map_conInsert.get(con.Account__c).Mobile__c;
			//身份证号
			newCon.ID_card__c = map_conInsert.get(con.Account__c).Id_card2__c;
			//性别	
			newCon.Gender__c = map_conInsert.get(con.Account__c).Gender__c;
			//兴趣爱好	
			newCon.interest__c = map_conInsert.get(con.Account__c).V2_interest__c;
			//生日	
			newCon.Birthdate = map_conInsert.get(con.Account__c).Birthday__c;
			//护照号
			newCon.V2_PassPortNo__c = map_conInsert.get(con.Account__c).V2_PassPortNo__c;
			//军官号
			newCon.V2_OfficerNo__c = map_conInsert.get(con.Account__c).V2_OfficerNo__c;
			//联系人类别
			newCon.ContactType__c = map_conInsert.get(con.Account__c).Contact_Type__c;
			//电子邮件
			newCon.Email = map_conInsert.get(con.Account__c).Email__c;
			//传真	
			newCon.Fax = map_conInsert.get(con.Account__c).Fax__c;
			//GIS客户类型	
			newCon.GISContactType__c = map_conInsert.get(con.Account__c).GISContactType__c;
			//备注	
			newCon.Description = map_conInsert.get(con.Account__c).Comment__c;
			//记录类型
			newCon.RecordTypeId = rt.Id;
			//操作人
			newCon.SP_Approve__c = con.OwnerId;
			//月吸入比例%
			newCon.SP_InhaleMonthyNum__c = con.SP_inhaleMonthyNum__c;
			//七氟烷FGF(L/min)
			newCon.SP_SevoFGF__c = con.SP_SevoFGF__c;
			//七氟烷浓度%
			newCon.SP_SevoConcentration__c = con.SP_SevoConcentration__c;
			//平均手术时长h
			newCon.SP_OperationTime__c = con.SP_OperationTime__c;
			//职务(SP)
			newCon.SP_Job__c = con.SP_Job__c;
			//部门(SP)
			newCon.SP_Department__c = con.SP_Department__c;
			//月全麻数量/台
			newCon.SP_AnesthesiaMonthyNum__c = con.SP_AnesthesiaMonthyNum__c;
			//地氟烷浓度%
			newCon.SP_DesConcentration__c = con.SP_DesConcentration__c;
			//地氟烷FGF(L/min)
			newCon.SP_DesFGF__c = con.SP_DesFGF__c;
			//七氟烷实际销量（瓶）公式：月全麻数量/台*月吸入比例%*平均手术时长h*七氟烷浓度*七氟烷FGF (L/min)*3.3/250
			newCon.SP_SevoQtySale__c = 
				(newCon.SP_AnesthesiaMonthyNum__c == null?0:newCon.SP_AnesthesiaMonthyNum__c)*
				((newCon.SP_InhaleMonthyNum__c == null?0:newCon.SP_InhaleMonthyNum__c)/100)*
				(newCon.SP_SevoConcentration__c == null?0:newCon.SP_SevoConcentration__c)*
				(newCon.SP_OperationTime__c == null?0:newCon.SP_OperationTime__c)*
				(newCon.SP_SevoFGF__c == null?0:newCon.SP_SevoFGF__c)*3.3/250;
			//七氟烷潜力销量（瓶）：自动计算，计算公式：=月全麻数量/台*3*2*2*3.3/250
			newCon.SP_SevoPotentialSale__c = ((newCon.SP_AnesthesiaMonthyNum__c == null?0:newCon.SP_AnesthesiaMonthyNum__c)*3*2*2*3.3/250).setScale(1, System.RoundingMode.HALF_UP);
			double sale = newCon.SP_SevoPotentialSale__c;
			string job = newCon.SP_Job__c;
			newCon.SP_SevoAccLevel__c = AccountLevelExec(sale, job);
			//地氟烷浓度%
			newCon.SP_DesConcentration__c = con.SP_DesConcentration__c;
			//地氟烷FGF(L/min)
			newCon.SP_DesFGF__c = con.SP_DesFGF__c;

			//地氟烷实际销量
			newCon.SP_DesQtySale__c = 
				(newCon.SP_AnesthesiaMonthyNum__c == null?0:newCon.SP_AnesthesiaMonthyNum__c)*
				((newCon.SP_InhaleMonthyNum__c == null?0:newCon.SP_InhaleMonthyNum__c)/100)*
				(newCon.SP_DesConcentration__c == null?0:newCon.SP_DesConcentration__c)*
				(newCon.SP_OperationTime__c == null?0:newCon.SP_OperationTime__c)*
				(newCon.SP_DesFGF__c == null?0:newCon.SP_DesFGF__c)*3.3/240;
			//地氟烷潜力销量
			newCon.SP_DesPotentialSale__c = ((newCon.SP_AnesthesiaMonthyNum__c == null?0:newCon.SP_AnesthesiaMonthyNum__c)*3*2*6.5*3.3/240).setScale(1, System.RoundingMode.HALF_UP);
			double saleDes = newCon.SP_DesPotentialSale__c;
			//地氟烷客户等级
			newCon.SP_DesAccLevel__c = AccountDesLevelExec(saleDes, job);
			list_NewContact.add(newCon);
		}
			
		if(list_NewContact != null && list_NewContact.size()>0)
		{
			insert list_NewContact;
		}
	}
	
	
	//SP客户等级计算   地氟烷
	//销量	联系人职务
	//地氟烷潜力销量	主任/副主任	带组医生	住院医师	领/加药人员	采购	    库管	其他职务
	//销量>=18	       V	    A	    A	    C	    D	    D	     A
	//18>销量>=1	       V	    B	    B	    C	    D	    D	     B
	//1>销量>  0	       V  	    B	    C	    C	    D	    D	     C
	//销量=0	           V	    B	    C	    C	    D	    D	          其他
	public String AccountDesLevelExec(Double SevoPotentialSale,String job)
	{
		string level;
		if(job == '主任' || job == '副主任')
		{
			return 'V';
		}
		else if(job == '带组医生')
		{
			if(SevoPotentialSale >= 18)
			{
				return 'A';
			}
			return 'B';
		}
		else if(job == '住院医师')
		{
			if(SevoPotentialSale >= 18)
			{
				return 'A';
			}
			else if(SevoPotentialSale < 18 && SevoPotentialSale >= 1)
			{
				return 'B';
			}
			return 'C';
		}
		else if(job == '领/加药人员')
		{
			return 'C';
		}
		else if(job == '采购' || job == '库管')
		{
			return 'D';
		}else
		{
			if(SevoPotentialSale >= 18)
			{
				return 'A';
			}
			else if(SevoPotentialSale < 18 && SevoPotentialSale >= 1)
			{
				return 'B';
			}
			else if(SevoPotentialSale < 1 && SevoPotentialSale > 0)
			{
				return 'C';
			}
			return '其他';
		}
	}
	
	//SP客户等级计算  七氟烷
	//销量	联系人职务
	//七氟烷潜力销量	主任/副主任	带组医生	住院医师	领/加药人员	采购	    库管	其他职务
	//销量>=5	       V	    A	    A	    C	    D	    D	     A
	//5>销量>=1	       V	    B	    B	    C	    D	    D	     B
	//1>销量>  0	       V  	    B	    C	    C	    D	    D	     C
	//销量=0	           V	    B	    C	    C	    D	    D	          其他
	public String AccountLevelExec(Double SevoPotentialSale,String job)
	{
		string level;
		if(job == '主任' || job == '副主任')
		{
			return 'V';
		}
		else if(job == '带组医生')
		{
			if(SevoPotentialSale >= 5)
			{
				return 'A';
			}
			return 'B';
		}
		else if(job == '住院医师')
		{
			if(SevoPotentialSale >= 5)
			{
				return 'A';
			}
			else if(SevoPotentialSale < 5 && SevoPotentialSale >= 1)
			{
				return 'B';
			}
			return 'C';
		}
		else if(job == '领/加药人员')
		{
			return 'C';
		}
		else if(job == '采购' || job == '库管')
		{
			return 'D';
		}else
		{
			if(SevoPotentialSale >= 5)
			{
				return 'A';
			}
			else if(SevoPotentialSale < 5 && SevoPotentialSale >= 1)
			{
				return 'B';
			}
			else if(SevoPotentialSale < 1 && SevoPotentialSale > 0)
			{
				return 'C';
			}
			return '其他';
		}
	}
}