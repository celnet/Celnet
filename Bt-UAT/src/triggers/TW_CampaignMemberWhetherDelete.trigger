/*
*功能：当市场活动的记录类型为TW_Campaign(TW_Campaign)时，但当状态的值等于报名结束(Sign Up Closed)、开始回访(Begin Feedback Call)、
*     回访结束(Feedback Call Closed)时,把其下的市场活动成员中的允许删除标示(AllowToDelete__c)设置成fasle
*     当市场活动中的状态值是已计划(Planned)或报名开始(Sign Up Begin)时，把把其下的市场活动成员中的允许删除标示(AllowToDelete__c)设置成true
*负责人：Alisa
*时间：2013年11月12日
*/
trigger TW_CampaignMemberWhetherDelete on Campaign (after update) {
	
	set<Id> IdCamp = new set<Id>(); //存放市场活动的Id
	//存放市场活动Id,和其对应的市场活动成员集合
	map<Id,list<CampaignMember>> mapListCampMember = new map<Id,list<CampaignMember>>();
	
	 
	//获取记录类型为TW_Campaign的市场活动的记录类型
	RecordType CampRecordType = [select r.Id,r.SobjectType,r.DeveloperName
								 from RecordType r
								 where r.SobjectType = 'Campaign' and r.DeveloperName = 'TW_Campaign'];
								 
	//获取市场活动的记录类型为TW_Campaign的市场活动Id
	for(Campaign camp : trigger.new)
	{
		if(camp.RecordTypeId == CampRecordType.Id)
		{
			IdCamp.add(camp.Id);
		}
	}
								 
	//获取所有的市场活动成员
	list<CampaignMember> listCampMember = [Select c.Id, c.ContactId, c.CampaignId, c.AllowToDelete__c
										   From CampaignMember c 
										   where c.CampaignId IN :IdCamp];
								 
	//获取每个市场活动下的对应的市场活动成员
	if(listCampMember != null && listCampMember.size()>0)
	{
		for(CampaignMember campMemb : listCampMember)
		{
				if(mapListCampMember.containsKey(campMemb.CampaignId))
				{
					mapListCampMember.get(campMemb.CampaignId).add(campMemb);
				}
				else
				{
					list<CampaignMember> listMapCampMember = new list<CampaignMember>();
					listMapCampMember.add(campMemb);
					mapListCampMember.put(campMemb.CampaignId,listMapCampMember);
				}
			}
	}
	
	//存放符合条件的市场活动成员
	list<CampaignMember> listUpdateCampMemb = new list<CampaignMember>();
	
	
	//遍历每个市场活动下对应的市场活动成员，将市场活动中字段“状态”的值是报名结束或开始回访或回访结束的市场活动下的市场活动成员
    //中的允许删除标示的值该为false，	把市场活动中字段“状态”的值是已计划或报名开始的其下的市场活动成员中的允许删除标示(AllowToDelete__c)设置成true						 
	for(Campaign newCamp : trigger.new)
	{
		Campaign oldCamp = trigger.oldMap.get(newCamp.Id);
		
		//当市场活动的状态为报名结束或开始回访或回访结束时，把市场活动成员的允许删除标示(AllowToDelete__c)值改为false
		if(newCamp.RecordTypeId == CampRecordType.Id && newCamp.Status != oldCamp.Status && (newCamp.Status == 'Sign Up Closed'
					 || newCamp.Status == 'Begin Feedback Call' || newCamp.Status == 'Feedback Call Closed'))
		{
			if(mapListCampMember != null && mapListCampMember.size()>0)
			{
				for(CampaignMember campMember : mapListCampMember.get(newCamp.Id))
				{
					campMember.AllowToDelete__c=false;
					listUpdateCampMemb.add(campMember);
				}
			}
			
		}
		
		//当市场活动的状态为已计划或报名开始时，把市场活动成员的允许删除标示(AllowToDelete__c)值改为true
		if(newCamp.RecordTypeId == CampRecordType.Id && newCamp.Status != oldCamp.Status && (newCamp.Status == 'Planned'
					 || newCamp.Status == 'Sign Up Begin'))
		{
			if(mapListCampMember != null && mapListCampMember.size()>0)
			{
				for(CampaignMember campMember : mapListCampMember.get(newCamp.Id))
				{
					campMember.AllowToDelete__c=true;
					listUpdateCampMemb.add(campMember);
				}
			}
			
		}
		
	}
	
	//更新满足条件的市场活动成员
	if(listUpdateCampMemb != null && listUpdateCampMemb.size()>0)
	{
		update listUpdateCampMemb;
	}	
	
}