/**
 * Author : Bill
 * 功能：当记录类型为'IVT','IVT Approval'的新建业务机会时，业务机会的 目前百特季度销量（袋）自动取值
 * 值取MD医院信息的对应客户的最新的目前百特季度销量（袋）
 * bill添加功能：2013-7-22
 * 当新建记录类型为Inproject类型的业务机会创建时，自动把此业务机会共享给
 * 销售医院关系下 产品类型为IVT 审批通过 非历史记录 生效日期在当前时间之前的新负责人
 * bill注释代码：2013-8-21
 * “当前百特月均用量” 在业务机会中的这个字段，目前不需要和IVT医院信息有任何关联，就是一个普通的数字填写字段就可以
**/
trigger OppSetNowSalesBags on Opportunity (after insert, after update) {
    Set<ID> set_acc = new Set<ID>();
    Set<ID> set_record = new Set<ID>();
    
    //list<RecordType> list_record = [Select r.Name, r.Id From RecordType r where r.Name in ('IVT','IVT Approval') and IsActive = true and r.SobjectType = 'Opportunity'];
    //记录类型为inproject的ID
    ID projectId = [Select r.Name, r.Id From RecordType r where r.Name in ('IN Project') and IsActive = true and r.SobjectType = 'Opportunity'][0].Id;
    /*if(list_record != null && list_record.size() > 0)
    {
        for(RecordType record:list_record)
        {
            set_record.add(record.Id);
        }
    }
    
    if(trigger.isBefore &&(trigger.isInsert || trigger.isUpdate))
    {
        for(Opportunity opp : trigger.new)
        {
            if(opp.AccountId != null && set_record.contains(opp.RecordTypeId))
            {
                set_acc.add(opp.AccountId);
            }
        }
    }
    
    Map<ID,double> map_accSales = new Map<ID,double>();
    if(set_acc != null && set_acc.size()>0)
    {
    	string year = string.valueOf(date.today().year());
        list<IVSHospitalInfo__c> list_IVSHospita = [Select i.NowQuarterBaxterTotalSalesBags__c, i.CreatedDate, i.Account__c From IVSHospitalInfo__c i where i.Account__c in : set_acc and i.Year__c =: year];
        if(list_IVSHospita != null && list_IVSHospita.size()>0)
        {
            for(IVSHospitalInfo__c ivtHospita : list_IVSHospita)
            {
                map_accSales.put(ivtHospita.Account__c,ivtHospita.NowQuarterBaxterTotalSalesBags__c);
            }
        }
    }
    
    if(trigger.isBefore &&(trigger.isInsert || trigger.isUpdate))
    {
        for(Opportunity opp : trigger.new)
        {
            if(opp.AccountId != null && set_record.contains(opp.RecordTypeId) && opp.current_volume_of_use__c == null)
            {
                //opp.current_volume_of_use__c = map_accSales.get(opp.AccountId);
            }
        }
    }*/
    
    //添加
    //新建的机会客户Id集合
    Set<ID> set_NewOpp = new Set<ID>();
    if(trigger.isInsert || trigger.isUpdate)
    {
        for(Opportunity opp : trigger.new)
        {
            if(opp.AccountId != null && opp.RecordTypeId == projectId )
            {
                set_NewOpp.add(opp.AccountId);
            }
        }
    }
    
    //MAP 需要共享给的销售
    MAP<ID,ID> map_sales = new MAP<ID,ID>();
    //2012-1的销售
    MAP<ID,ID> map_Warnsales = new MAP<ID,ID>();
    system.debug(set_NewOpp.size() + 'klm3');
    if(set_NewOpp != null && set_NewOpp.size() > 0)
    {
    	//2012年1月的错误数据单独出理
    	Date warnDate = date.valueOf('2012-1-1');
        //销售医院关系
        List<V2_Account_Team__c> list_V2_Account = [Select v.V2_User__c , v.V2_Account__c  From V2_Account_Team__c v
                                                    where UserProduct__c =  'IVT' 
                                                    AND V2_History__c = false 
                                                    AND V2_ApprovalStatus__c = '审批通过' 
                                                    AND V2_LastAccessDate__c < TODAY 
                                                    AND V2_User__c != null
                                                    AND V2_Account__c IN : set_NewOpp];
        List<V2_Account_Team__c> list_warn = [Select v.V2_User__c , v.V2_Account__c  From V2_Account_Team__c v
                                                    where UserProduct__c =  'IVT' 
                                                    AND V2_History__c = false  
                                                    AND V2_LastAccessDate__c = : warnDate 
                                                    AND V2_Account__c IN : set_NewOpp];
        if(list_V2_Account != null && list_V2_Account.size() > 0)
        {
            for(V2_Account_Team__c team : list_V2_Account)
            {
                map_sales.put(team.V2_Account__c, team.V2_User__c);
            }
        }
        if(list_warn != null && list_warn.size() > 0)
        {
        	for(V2_Account_Team__c teamWarn : list_warn)
            {
                map_Warnsales.put(teamWarn.V2_Account__c, teamWarn.V2_User__c);
            }
        }
    }
    
    if(!map_sales.isEmpty() || !map_Warnsales.isEmpty())
    {
        List<OpportunityShare> list_share = new List<OpportunityShare>();
        if(trigger.isAfter && (trigger.isInsert || trigger.isUpdate))
        {
            for(Opportunity opp : trigger.new)
            {
                if(opp.AccountId != null && opp.RecordTypeId == projectId && map_sales.get(opp.AccountId) != opp.OwnerId )
                {
                    OpportunityShare share = new OpportunityShare(); 
                    share.OpportunityId = opp.Id;
                    share.UserOrGroupId = map_sales.get(opp.AccountId);
                    if(map_sales.get(opp.AccountId)==null)
                    {
                    	share.UserOrGroupId = map_Warnsales.get(opp.AccountId);
                    }
                    share.OpportunityAccessLevel = 'Read';
                    if(map_sales.get(opp.AccountId)!=null || map_Warnsales.get(opp.AccountId)!=null)
                    {
                    	list_share.add(share);
                    }
                }
            }
        }
        if(list_share != null && list_share.size()>0)
        {
            upsert list_share;
        }
    }
}