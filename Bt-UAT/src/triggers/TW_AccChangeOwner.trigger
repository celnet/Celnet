/*
 *Scott
 *2013-10-24
 *所有人变更审批通过后替换所有人
*/
trigger TW_AccChangeOwner on Account (before update) {
	for(Account acc : trigger.new)
	{
		if(acc.TW_DeveloperName__c !='TW_Hospital' ||  acc.TW_NewOwner__c==null || acc.TW_ChangeOwnerApprovalStatus__c!='通過')
		{
			continue;
		}
		acc.OwnerId = acc.TW_NewOwner__c;
		acc.TW_NewOwner__c = null;
	}
}