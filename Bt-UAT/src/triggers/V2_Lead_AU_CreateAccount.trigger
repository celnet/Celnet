/* 
 * Author: Sunny 
 * Created on: 2011-12-26
 * Description: 当潜在客户审批通过之后，根据该潜在客户创建一个新的客户
 * 1.根据MID查找Account，若不存在则创建，并且同步客户小组
 * 2.若MID值为空，根据Lead ID查找Account，若不存在则创建，并且同步客户小组
 */
trigger V2_Lead_AU_CreateAccount on Lead (before insert , before update,after update) 
{
	if(trigger.isBefore)
	{
		for(Lead objLead : trigger.new)
		{
			if(objLead.FirstName != null)
			{
				objLead.FirstName = null ;
			} 
		}
	}
	if(trigger.isAfter)
	{
		List<String> list_Mids = new List<String>() ;
		List<ID> list_LeadIds = new List<ID>() ;
		Set<String> set_Mids = new Set<String>() ;
		Set<ID> set_LeadIds = new Set<ID>() ;
		V2_SynchField clsSynchField = new V2_SynchField() ;
		List<Account> list_Account = new List<Account>() ;
		Map<String,List<V2_Lead_Team__c>> map_MidLT = new Map<String,List<V2_Lead_Team__c>>() ;
		List<RecordType> list_RecordType = [Select id From RecordType Where DeveloperName = 'RecordType' And SobjectType = 'Account'] ;
		for(Lead objLead : trigger.new)
		{
			if(objLead.V2_ApprovalStauts__c == trigger.oldMap.get(objLead.Id).V2_ApprovalStauts__c)
			{
				continue ;
			}
			if(objLead.V2_ApprovalStauts__c != '审批通过')
			{
				continue ;
			}
			if(objLead.V2_Mid__c != null)
			{
				list_Mids.add(objLead.V2_Mid__c) ;
			}
			list_LeadIds.add(objLead.Id) ;	
		}
		if(list_Mids.size() != 0)
		{
			for(Account objAccount : [select id,Mid__c,V2_Lead__c from Account where Mid__c in: list_Mids ])
			{
				set_Mids.add(objAccount.Mid__c) ;
				list_Account.add(objAccount) ;
			}
		}
		if(list_LeadIds.size() != 0)
		{
			List<V2_Lead_Team__c> list_LT ;
			for(V2_Lead_Team__c objLT : [select id,OwnerId,V2_Effective_Year__c,V2_Effective_Month__c,V2_Lead__c,V2_Lead__r.V2_Mid__c,V2_Role__c,V2_User__c,V2_Effective_date__c from V2_Lead_Team__c where V2_Lead__c in: list_LeadIds And V2_Is_Delete__c = false And V2_IsSyn__c = false])
			{
				if(objLT.V2_Lead__r.V2_Mid__c == null)
				{
					continue ;
				}
				if(map_MidLT.containsKey(objLT.V2_Lead__r.V2_Mid__c))
				{
					list_LT = map_MidLT.get(objLT.V2_Lead__r.V2_Mid__c) ;
					list_LT.add(objLT) ;
					map_MidLT.put(objLT.V2_Lead__r.V2_Mid__c,list_LT) ;
				}else
				{
					list_LT = new List<V2_Lead_Team__c>() ;
					list_LT.add(objLT) ;
					map_MidLT.put(objLT.V2_Lead__r.V2_Mid__c,list_LT) ;
				}
			}
		}
		system.debug('Map:'+map_MidLT) ;
		List<Account> list_AccountCre = new List<Account>() ;
		List<User> list_User = [Select Id From User Where Username = 'baxter@baxter.com'] ;
		for(Lead objLead : trigger.new)
		{
			if(objLead.V2_ApprovalStauts__c == trigger.oldMap.get(objLead.Id).V2_ApprovalStauts__c)
			{
				continue ;
			}
			if(objLead.V2_ApprovalStauts__c != '审批通过')
			{
				continue ;
			}
			system.debug(set_Mids+'=-=-=-=-'+objLead.V2_Mid__c) ;
			if(set_Mids.contains(objLead.V2_Mid__c))
			{
				continue ;
			}
			system.debug('Lead Mss'+objLead.Name+objLead.Company) ;
			Account objAccount = new Account() ;
			if(list_RecordType.size() != 0)
			{
				objAccount.RecordTypeId = list_RecordType[0].Id ;
			}
			clsSynchField.cloneFieldFromLeadToAccount(objLead, objAccount) ;
			objAccount.V2_Lead__c = objLead.Id ;
			if(list_User != null && list_User.size() != 0)objAccount.OwnerId = list_User[0].Id ;
			system.debug(objLead.LastName+'Lead Name :'+objLead.Name+'AccName:'+objAccount.Name) ;
			list_AccountCre.add(objAccount) ;
			
		}
		if(list_AccountCre.size() != 0)
		{
			insert list_AccountCre ;
			
			list_Account.addAll(list_AccountCre) ;
			
		}
		List<V2_Account_Team__c> list_VAT = new List<V2_Account_Team__c>() ;
		List<AccountTeamMember> list_ATM = new List<AccountTeamMember>() ;
		List<V2_Lead_Team__c> list_VLT = new List<V2_Lead_Team__c>() ;
		
		if(list_Account.size() != 0)
		{
			for(Account objAcc : list_Account)
			{
				if(map_MidLT.containsKey(objAcc.Mid__c))
				{
					for(V2_Lead_Team__c objVLT : map_MidLT.get(objAcc.Mid__c))
					{
						V2_Account_Team__c objVAT = new V2_Account_Team__c() ;
						AccountTeamMember objATM = new AccountTeamMember() ;
						clsSynchField.cloneAccTeamFieldFromLeadToAccount(objVLT,objVAT) ;
						clsSynchField.cloneFieldFromLeadTeamToATM(objVLT, objATM) ;
						system.debug('OBJ ATM:'+objATM) ;
						objVAT.V2_Account__c = objAcc.Id ;
						objVAT.V2_IsSyn__c = true ;
						objVAT.V2_BatchOperate__c = '新增';
						objVAT.V2_ApprovalStatus__c = '审批通过';
						objVAT.EffectiveDate__c = date.valueOf(objVLT.V2_Effective_Year__c+'-'+objVLT.V2_Effective_Month__c+'-1');
						objATM.AccountId = objAcc.Id ;
						objVLT.V2_IsSyn__c = true ;
						list_VLT.add(objVLT) ;
						list_ATM.add(objATM) ;
						list_VAT.add(objVAT) ;
					}
				}
			}
			if(list_VAT.size() != 0)
			{
				system.debug('ATM:~'+list_ATM) ;
				insert list_VAT ;
				insert list_ATM ;
				update list_VLT ;
			}
		}
	}
}