/**
*Function: Sync the related information<br> 
*such as setup the fields from the related Contact, Account, etc...
*/
trigger V2_Trg_AVC_SyncFields on AssVisitComments__c (before insert) {
    static String Contact_IPREFIX='003';
    
    if(Trigger.isInsert && Trigger.isBefore){
        Set<String> eventIdSet=new Set<String>();
        Set<String> beReviewedUserIdSet=new Set<String>();//被点评人用户
        Map<String, Contact> visitContactsMap=new Map<String,Contact>();//拜访对象(医生),包含拜访对象(医生)所属医院,即医院信息.
        
        Date currentDate=Date.today();
        Integer iY=currentDate.year();
        Integer iM=currentDate.month();
        for(AssVisitComments__c avc:Trigger.new){
            if(avc.BeReviewed__c!=null){
                beReviewedUserIdSet.add(avc.BeReviewed__c);
            }
            if(avc.EventId__c!=null && avc.EventId__c.trim()!=''){
                eventIdSet.add(avc.EventId__c);
            }
            avc.reviewYearMonth__c=iY+'-'+(iM<10?'0':'')+iM;
        }
        Map<String, User> usersMap=new Map<String, User>(
                [Select id, userRoleId, UserRole.Name From User Where id in: beReviewedUserIdSet]
            );
        
        //批量获取相关:相关项whatId--客户, 名称(whoId)--拜访对象(医生)
        List<Event> relatedEvents=[Select id, whoId, whatId, startDatetime, endDatetime, GAPlan__c, GAExecuteResult__c  
            From Event where id in: eventIdSet];
        Map<String, Event> eventsMap=new Map<String, Event>();
        Set<String> contactIdSet=new Set<String>();//防止关联非医院记录导致数据丢失,从拜访对象(医生)记录上获取医院信息
        for(Event evt: relatedEvents){
            eventsMap.put(evt.id, evt);
            if(evt.whoId!=null)contactIdSet.add(evt.whoId);
        }
        
        Map<String, Contact> contactsMap=new Map<String, Contact>(
                [Select id, accountId From Contact where id in: contactIdSet]
            );
            
        for(AssVisitComments__c avc:Trigger.new){
            //设置被点评人相关
            if(avc.BeReviewed__c!=null){
                String userId=avc.BeReviewed__c;
                if(userId!=null){
	                User beReviewedUser=usersMap.get(userId);
	                if(beReviewedUser!=null){
	                    avc.userRoleName__c=beReviewedUser.UserRole.name;
	                }
                }
            }
            //设置事件相关对象相关
            if(avc.EventId__c!=null && avc.EventId__c.trim()!=''){
                String eventId=avc.EventId__c;
                if(eventId!=null){
	                Event evt=eventsMap.get(eventId);
	                if(evt!=null){
	                    avc.visitDatetime__c=evt.startDatetime;
	                    avc.plan__c=evt.GAPlan__c;
	                    avc.analysis__c=evt.GAExecuteResult__c;
	                    String contactId=evt.whoId;
	                    Contact contact=contactsMap.get(contactId);
	                    if(contact!=null && contactId.startsWith(Contact_IPREFIX)){//防止不是与联系人相关
	                        avc.contact__c=contact.Id;
	                        //avc.account__c=contact.accountId;
	                    }
	                }
                }
            }
        }
        
    }
    
    
}