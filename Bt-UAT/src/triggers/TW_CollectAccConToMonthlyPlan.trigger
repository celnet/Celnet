/*
 *Scott
 *2013-10-28
 *此trigger仅适用于台湾地区
 *汇总销售代表所拜访覆盖到的医生人数，用来计算本月的拜访覆盖率
 *销售代表拜访覆盖的医院数，用来计算本月的拜访覆盖率
*/
trigger TW_CollectAccConToMonthlyPlan on MonthlyPlanDetail__c (after insert, after update,after delete,after undelete) {
	//月计划Ids
	Set<Id> Set_MonthPlanId =  new Set<Id>();
	if(trigger.isInsert)
	{
		for(MonthlyPlanDetail__c Mpd:trigger.new)
		{
			//仅台湾地区记录进入此trigger，通过月计划记录类型和月计划所有人角色名称判断
			if(Mpd.TW_MpDeveloperName__c !='TW_MonthlyPlan' || !Mpd.TW_MpOwnerRole__c.startsWith('TW'))
			{
				continue;
			}
			
			if(Mpd.Planned_Finished_Calls__c == null || Mpd.Planned_Finished_Calls__c ==0 || Mpd.MonthlyPlan__c == null)
			{
				continue;
			}
			Set_MonthPlanId.add(Mpd.MonthlyPlan__c);
		}
	}
	
	if(trigger.isUpdate)
	{
		for(MonthlyPlanDetail__c NewMpd:trigger.new)
		{
			//仅台湾地区记录进入此trigger，通过月计划记录类型和月计划所有人角色名称判断
			if(NewMpd.TW_MpDeveloperName__c !='TW_MonthlyPlan' || !NewMpd.TW_MpOwnerRole__c.startsWith('TW'))
			{
				continue;
			}
			
			MonthlyPlanDetail__c OldMpd = trigger.oldMap.get(NewMpd.Id);
			if(NewMpd.Planned_Finished_Calls__c != OldMpd.Planned_Finished_Calls__c)
			{
				Set_MonthPlanId.add(NewMpd.MonthlyPlan__c);
			}
			if(NewMpd.MonthlyPlan__c != OldMpd.MonthlyPlan__c)
			{
				Set_MonthPlanId.add(NewMpd.MonthlyPlan__c);
				Set_MonthPlanId.add(OldMpd.MonthlyPlan__c);
			}
		}
	}
	if(trigger.isDelete)
	{
		for(MonthlyPlanDetail__c Mpd:trigger.old)
		{
			//仅台湾地区记录进入此trigger，通过月计划记录类型和月计划所有人角色名称判断
			if(Mpd.TW_MpDeveloperName__c !='TW_MonthlyPlan' || !Mpd.TW_MpOwnerRole__c.startsWith('TW'))
			{
				continue;
			}
			if(Mpd.Planned_Finished_Calls__c == null || Mpd.Planned_Finished_Calls__c ==0 || Mpd.MonthlyPlan__c == null)
			{
				continue;
			}
			Set_MonthPlanId.add(Mpd.MonthlyPlan__c);
		}
	}
	if(trigger.isUnDelete)
	{
		for(MonthlyPlanDetail__c Mpd:trigger.new)
		{
			//仅台湾地区记录进入此trigger，通过月计划记录类型和月计划所有人角色名称判断
			if(Mpd.TW_MpDeveloperName__c !='TW_MonthlyPlan' || !Mpd.TW_MpOwnerRole__c.startsWith('TW'))
			{
				continue;
			}
			if(Mpd.Planned_Finished_Calls__c == null || Mpd.Planned_Finished_Calls__c ==0 || Mpd.MonthlyPlan__c == null)
			{
				continue;
			}
			Set_MonthPlanId.add(Mpd.MonthlyPlan__c);
		}
	}
	
	if(Set_MonthPlanId.size()>0)
	{
		List<MonthlyPlan__c> List_UpMp = new List<MonthlyPlan__c>();//更新
		
		/*拜访医师覆盖数&&拜访医院覆盖数  统计*/
		for(MonthlyPlan__c mp: [Select Id,TW_VisitCoveredContact__c,TW_VisitCoveredAcc__c,OwnerId,
								(Select Contact__c,Contact__r.OwnerId,Planned_Finished_Calls__c From MonthlyPlanDetail__r 
								 where Planned_Finished_Calls__c !=null and Planned_Finished_Calls__c>0 and Contact__r.TW_DoctorStatus__c='有效'),
								(Select Account__c,Account__r.OwnerId,ActualTimes__c From MonthlyPlanDetailByAccount__r 
								 where ActualTimes__c !=null and ActualTimes__c >0 and Account__r.TW_AccStatus__c='有效') 
								from MonthlyPlan__c where Id in: Set_MonthPlanId])
		{
			//拜访医师覆盖数
			mp.TW_VisitCoveredContact__c = 0;
			if(mp.MonthlyPlanDetail__r != null && mp.MonthlyPlanDetail__r.size()>0)
			{
				for(MonthlyPlanDetail__c mpd : mp.MonthlyPlanDetail__r )
				{
					if(mpd.Contact__r.OwnerId == mp.OwnerId)
					{
						mp.TW_VisitCoveredContact__c++;
					}
				}
			}
			//拜访医院覆盖数
			mp.TW_VisitCoveredAcc__c = 0;
			if(mp.MonthlyPlanDetailByAccount__r != null && mp.MonthlyPlanDetailByAccount__r.size()>0)
			{
				for(MonthlyPlanDetailByAccount__c mpdba : mp.MonthlyPlanDetailByAccount__r)
				{
					if(mpdba.Account__r.OwnerId == mp.OwnerId)
					{
						mp.TW_VisitCoveredAcc__c++;
					}
				}
			}
			List_UpMp.add(mp);
		}
		
		if(List_UpMp != null && List_UpMp.size()>0)
		{
			update List_UpMp;
		}
		
	}
}