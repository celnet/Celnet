/**
 * Author : Bill
 * date ：2013-12-2
 * PSR拜访月计划，每月每人只允许有一条
**/
trigger PM_PSRMonthyPlanOnlyone on PM_PSRMonthPlan__c (before insert, before update)
{
	//PSR月计划日期
	Set<Date> set_YearMonth = new Set<Date>();
	//PSR OwnerId
	Set<ID> set_Ids = new Set<ID>();
    
    for(PM_PSRMonthPlan__c plan : trigger.new)
    {
    	if(plan.PM_Year__c == null || plan.PM_Month__c == null)
    	{
    		continue;
    	}
    	plan.PM_Date__c = date.valueOf(plan.PM_Year__c+'-'+plan.PM_Month__c+'-1');
    	set_YearMonth.add(plan.PM_Date__c);
    	set_Ids.add(plan.OwnerId);
    }
    
    List<PM_PSRMonthPlan__c> list_plan = [SELECT Id,Name,PM_Date__c,OwnerId,Owner.Alias FROM PM_PSRMonthPlan__c WHERE OwnerId IN : set_Ids and PM_Date__c IN : set_YearMonth];
    Map<String,PM_PSRMonthPlan__c> map_plan = new Map<String,PM_PSRMonthPlan__c>();
    if(list_plan != null && list_plan.size()>0)
    {
    	for(PM_PSRMonthPlan__c plan : list_plan)
    	{
    		if(!map_plan.containsKey(string.ValueOf(plan.PM_Date__c) + plan.OwnerId))
    		{
    			map_plan.put(string.ValueOf(plan.PM_Date__c) + plan.OwnerId, plan);
    		}
    	}
    }
    
    if(trigger.isInsert)
    {
    	for(PM_PSRMonthPlan__c plan : trigger.new)
		{ 
			if(map_plan.containsKey(string.ValueOf(plan.PM_Date__c) + plan.OwnerId))
			{
				plan.addError(map_plan.get(string.ValueOf(plan.PM_Date__c) + plan.OwnerId).Owner.Alias+'在'+plan.PM_Year__c+'年'+plan.PM_Month__c+'月已存在PSR拜访月计划, 不允许再次创建');
			}
		}
    }
    if(trigger.isUpdate)
    {
    	for(PM_PSRMonthPlan__c plan : trigger.new)
	    { 
	    	if(map_plan.containsKey(string.ValueOf(plan.PM_Date__c) + plan.OwnerId) && plan.PM_Date__c != trigger.oldMap.get(plan.Id).PM_Date__c)
	    	{
				plan.addError(map_plan.get(string.ValueOf(plan.PM_Date__c) + plan.OwnerId).Owner.Alias+'在'+plan.PM_Year__c+'年'+plan.PM_Month__c+'月已存在PSR拜访月计划, 不允许再次创建');
	    	}
	    }
    }
}