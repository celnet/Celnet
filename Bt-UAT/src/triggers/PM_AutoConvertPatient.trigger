/*
*作者：Michael
*时间：2013-9-18
*功能：当新病人审批状态等于“通过”且“已转换”字段值等于“假”时触发,新病人转化成病人
*/
trigger PM_AutoConvertPatient on PatientApply__c (before update, after update) 
{
    //病人集合
    list<PM_Patient__c> patientList = new list<PM_Patient__c>();
    /*2013-12-23 Tobe添加  系统管理员 Admin 和 manager 可以修改非申请新病人提交*/
    list<Profile> list_profile = [Select ID from Profile where Name = '系统管理员' or Name ='PM-PSR Admin' or Name ='PM-PSR Manager'];
    set<String> set_Id = new set<String>();
    for(Profile profile : list_profile)
	{	//保存‘日常事件’和‘目标设定’记录类型
		set_Id.add(profile.Id);
	}
    //插管医院
    private Set<ID> set_INids = new Set<ID>();
    
    /*****************BILL ADD*****************************/
    if(trigger.isBefore)
    {
        for(PatientApply__c np : trigger.new)
        {
            PatientApply__c old = trigger.oldMap.get(np.Id);
            //新病人一旦提交审批，就不允许修改信息
            if(set_Id.contains(UserInfo.getProfileId()) && old.Status__c == '待审批')
            {
            	continue;
            }
            if(old.Status__c == '待审批'  || old.Status__c == '通过' || old.Status__c == '拒绝')
            {
                if((old.Status__c != np.Status__c && np.Status__c == '通过') ||
                    (old.Status__c != np.Status__c && np.Status__c == '拒绝'))
                    {
                        continue;
                    }
                    if(old.PM_PSR__c != np.PM_PSR__c)
                	{
                    	continue;
                	}
                	np.addError('新病人申请提交审批后不允许修改信息');
            }
        }
    }
    /*****************BILL ADD*****************************/
    
    for(PatientApply__c np : trigger.new)
    {
        if(np.IntubationHospital__c == null)
        {
            continue;
        }
        PatientApply__c old = trigger.oldMap.get(np.Id);
        if(old.Status__c != np.Status__c && np.Status__c == '通过')
        {
            set_INids.add(np.IntubationHospital__c);
        }
    }
    
    //获取当前的PD部门的负责的销售
    /***********2014-1-9 Tobe修改，负责销售取值变化***********************/
    private Map<ID,ID> map_salePD = NEW Map<ID,ID>();
    for(V2_Account_Team__c team : [Select v.V2_User__c , v.V2_Account__c,v.V2_Effective_Year__c,v.V2_Effective_Month__c,v.V2_Delete_Year__c,v.V2_Delete_Month__c  From V2_Account_Team__c v where UserProduct__c =  'PD' 
                                   AND V2_Account__c IN : set_INids])
    {
    	Date effectiveDate;
    	Date deleteDate;
    	integer effectiveYear = integer.valueOf(team.V2_Effective_Year__c);
    	integer effectiveMonth = integer.valueOf(team.V2_Effective_Month__c);
    	effectiveDate = Date.newInstance(effectiveYear, effectiveMonth, 1);
    	if(team.V2_Delete_Year__c != null)
    	{
	    	integer deleteYear = integer.valueOf(team.V2_Delete_Year__c);
	    	integer deleteMonth = integer.valueOf(team.V2_Delete_Month__c);
	    	deleteDate = Date.newInstance(deleteYear, deleteMonth, 1);
    	}
    	else
    	{
    		deleteDate = Date.newInstance(Date.Today().addMonths(1).year(),Date.Today().addMonths(1).Month(),1);
    	}
    	if(effectiveDate <= Date.Today() && Date.Today() < deleteDate)
    	{
    		map_salePD.put(team.V2_Account__c,team.V2_User__c);
    	}
    }
    /*for(V2_Account_Team__c team : [Select v.V2_User__c , v.V2_Account__c  From V2_Account_Team__c v where UserProduct__c =  'PD' 
                                   AND ((V2_BatchOperate__c = '新增' And V2_ApprovalStatus__c = '审批通过')
                                   OR(V2_BatchOperate__c = '替换' And V2_ApprovalStatus__c = '审批拒绝')
                                   OR(V2_BatchOperate__c = '删除' And V2_ApprovalStatus__c = '审批拒绝'))
                                   AND V2_Account__c IN : set_INids])
    {
        map_salePD.put(team.V2_Account__c,team.V2_User__c);
    }*/
    /***********2014-1-9 Tobe修改，负责销售取值变化 End***********************/
    
    //获取当前的PSR部门的负责的PSR
    private Map<ID,ID> map_salePSR = NEW Map<ID,ID>();
    for(PM_PSRRelation__c psr : [Select p.PM_PSR__c , p.PM_Hospital__c  From PM_PSRRelation__c p where  
                                  PM_Status__c = '启用' and PM_UneffectDate__c = null and PM_Hospital__c IN : set_INids])
    {
        map_salePSR.put(psr.PM_Hospital__c,psr.PM_PSR__c);
    }
    
    if(trigger.IsAfter)
    {
        for(PatientApply__c np : trigger.new)
        {
            if(np.IntubationHospital__c == null || np.Address__c == null)
            {
                continue;
            }
            PatientApply__c old = trigger.oldMap.get(np.Id);
            if(old.Status__c != np.Status__c && np.Status__c == '通过')
            {
                patientList.add(convertPatient(np));
            }
        }
    }
    
    if(patientList.size() > 0)
    {
        insert patientList;
    }
    //新病人转换病人的字段匹配
    PM_Patient__c convertPatient(PatientApply__c np )
    {
        PM_Patient__c p = new PM_Patient__c();
        //记录类型
        p.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName = 'PM_Survive' and IsActive = true and r.SobjectType = 'PM_Patient__c'][0].Id;
        p.Name = np.PatientName__c;
        //p.Name = np.PatientName__c;                       //姓名
        p.PM_Sex__c = np.Sex__c;                                //性别
        p.PM_Province__c = np.Province__c;                      //省份
        p.PM_City__c = np.City__c;                              //城市
        p.PM_Birthday__c = np.Birth__c;                      //出生年月  
        p.PM_Payment__c =  np.Payment__c;                       //付费方式 
        p.PM_Address__c = np.Address__c;                        //地址
        p.PM_HomeTel__c = np.PM_Famliy_Phone__c;                //家庭电话
        p.PM_PmPhone__c = np.PM_Patient_MPhone__c;              //病人手机
        p.PM_PmTel__c = np.PM_Patient_Phone__c;                 //病人电话
        p.PM_FamilyTel__c = np.PM_Famliy_Member_Phone__c;       //家属电话
        p.PM_FamilyPhone__c = np.PM_Family_Member_Mphone__c;    //家属手机
        p.PM_OtherTel2__c = np.PM_Other_Phone__c;                //其他电话
        p.PM_InHospital__c = np.IntubationHospital__c;          //插管医院
        p.PM_InDoctor__c = np.IntubationDoctor__c;              //插管医生
        p.PM_Distributor__c = np.Distributor__c;                    //经销商
        p.PM_InDate__c = np.IntubationDate__c;                  //插管日期
        p.PM_Treat_Nurse__c = np.TreatmentNurse__c;             //治疗护士
        p.PM_FAIncome__c = np.Income__c;                        //家庭年收入  类型不一致  新病人：数字  病人：选项列表
        p.PM_Profession__c = np.Career__c;                      //职业
        p.PM_IdNumber__c = np.Identify__c;                      //身份证
        p.PM_ZipCode__c = string.valueof(np.PostalCode__c);     //邮政编码  
        p.PM_Protopathy__c = np.Illness__c;                     //原发病
        p.PM_NewPatientDate__c = date.today();                  //新病人转入日期 
        p.PM_Treat_Hospital__c = np.IntubationHospital__c; //治疗医院默认等于插管医院 2013-11-8 by sunny
        p.PM_Treat_Doctor__c = np.IntubationDoctor__c;//治疗医生默认等于插管医生 2013-11-8 by sunny
        p.PM_Status__c = 'New';
        p.PM_InInformation__c = np.Comment__c;
        p.PatientApply__c = np.Id;
        p.PM_SuggestVisitTime__c = '9:00-10:00,10:01-11:00,11:01-12:00,12:01-13:00,13:01-15:00,15:01-17:00,17:01-18:00,18:01-20:00';//建议可拨打时间
        p.PM_VisitState__c = '未拜访';
        //当前负责销售
        if(map_salePD.containsKey(np.IntubationHospital__c))
        {
        	//当前负责销售
            p.PM_Current_Saler__c = map_salePD.get(np.IntubationHospital__c);
            //插管销售
            /*****2014-1-9 Tobe修改，将新病人提交的创建人赋值给病人的插管销售******/
            p.PM_InUser__c = np.CreatedById;
            //治疗销售
            p.PM_Treat_Saler__c = map_salePD.get(np.IntubationHospital__c);
        }
        //当前负责PSR
        if(map_salePSR.containsKey(np.IntubationHospital__c))
        {
            p.PM_Current_PSR__c = map_salePSR.get(np.IntubationHospital__c);
            p.OwnerId = map_salePSR.get(np.IntubationHospital__c);
        }
        return p;
    }
}