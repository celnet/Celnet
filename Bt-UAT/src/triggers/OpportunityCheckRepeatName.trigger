/** 
 * sunny
 * 1.为每一个业务机会编号，编号规则：大部门+产品+编号
 * 2.检查业务机会的名字是否重复(同一所有人下)
**/
trigger OpportunityCheckRepeatName on Opportunity (before insert , after insert , after update) {
    /**
     * 自动编号
    **/
    if(trigger.isBefore){
        if(trigger.isInsert){
            Set<ID> set_RtIds = new Set<ID>();
            List<String> list_RecordTypeDevName = new List<String>{'RENAL','IVT_Approval','IVT','ACC_Supervisor','ACC'};
            for(RecordType rt : [Select Id From RecordType Where DeveloperName in: list_RecordTypeDevName And SobjectType = 'Opportunity']){
                set_RtIds.add(rt.Id);
            }
            Set<ID> set_OwnerId = new Set<ID>();
            for(Opportunity opp : trigger.new){
                if(set_RtIds.contains(opp.RecordTypeId)){
                    set_OwnerId.add(opp.OwnerId);
                }
            }
            if(set_OwnerId.size() > 0){
                Map<ID,String> Map_UserRoleName = new Map<ID,String>();
                for(User u : [Select Id,UserRoleId,UserRole.Name From User Where Id in: set_OwnerId]){
                    Map_UserRoleName.put(u.Id , u.UserRole.Name);
                }
                for(Opportunity opp : trigger.new){
                    if(set_RtIds.contains(opp.RecordTypeId) && Map_UserRoleName.containsKey(opp.OwnerId)){
                        String strRoleName = Map_UserRoleName.get(opp.OwnerId);
                        if(strRoleName != null && strRoleName.contains('-') && strRoleName.split('-').size()>4){
                            opp.OppAutoNum1__c = strRoleName.split('-')[0]+strRoleName.split('-')[3];
                        }
                    }
                }
            }
        }
    }
    
    
    /**
     * 检查业务机会的名字是否重复
    **/
    if(trigger.isAfter){
        final String strErrorMsg = '系统内有重复名称的业务机会。';
        final String strErrorMsg2 = '请勿创建重复名称的业务机会';
        Set<String> set_OppName = new Set<String>();
        Set<ID> set_OppOwnerId = new Set<ID>();
        for(Opportunity opp : trigger.new){
            if(trigger.isUpdate){
                if(opp.OppAutoNum1__c != trigger.oldMap.get(opp.Id).OppAutoNum1__c){
                    if(!Test.isRunningTest()){
                        opp.addError('业务机会编号不允许修改');
                    }
                    return ;
                }
                system.debug(opp.Name +' ** '+trigger.oldMap.get(opp.Id).Name);
                if(opp.Name == trigger.oldMap.get(opp.Id).Name && opp.OwnerId == trigger.oldMap.get(opp.Id).OwnerId){
                    continue;
                }
            }
            if(set_OppName.contains(opp.Name)){
                if(!Test.isRunningTest()){
                    opp.Name.addError(strErrorMsg2);
                }
            }else{
                set_OppName.add(opp.Name);
            }
            set_OppOwnerId.add(opp.OwnerId);
        }
        if(set_OppName.size() > 0){
            Set<String> set_OppNameInOrg = new Set<String>();
            for(Opportunity opp : [Select Id,Name,OwnerId From Opportunity Where Name in: set_OppName And OwnerId in: set_OppOwnerId And Id not in: trigger.newMap.keySet()]){
                set_OppNameInOrg.add(opp.Name+''+opp.OwnerId);
            }
            
            for(Opportunity opp : trigger.new){
                if(set_OppNameInOrg.contains(opp.Name+''+opp.OwnerId)){
                    if(!Test.isRunningTest()){
                        opp.Name.addError(strErrorMsg);
                    }
                    
                }
            }
        }
    }
}