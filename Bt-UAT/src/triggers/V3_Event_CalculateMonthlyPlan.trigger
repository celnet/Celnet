trigger V3_Event_CalculateMonthlyPlan on Event (after delete, after insert, after undelete, after update) {
    final String C_VisitTypeName = '拜访';
    
    Map<id,List<V3_EventWithKA>> ContactIdEventMap = new Map<id,List<V3_EventWithKA>>();
    
    List<Contact> contactList = new List<Contact>();

    Set<Id> set_ownerId = new Set<Id>();

    Set<String> Set_YearMonthUserid = new Set<String>(); 
    
    Set<Id> mpids = new Set<Id>();     
    
    List<MonthlyPlan__c>  mpList = new List<MonthlyPlan__c>();
    
    Map<String,MonthlyPlan__c> mpMap = new Map<String,MonthlyPlan__c>();
    
    Map<String,List<Event>> EventMap = new Map<String,List<Event>>();
    
    List<MonthlyPlan__c> updateMPList = new List<MonthlyPlan__c>();
    
    //为限定查询记录数量,增加日期时间限制, Ken
    DateTime startDate=System.now();
    DateTime endDate=System.now();
        
//add 这里找到所有与此次更新事件想关联的所有Event，重点是为了返回一个eventlist，建立了一个key和monthly Plan的map
    List<Event> eventList = new List<Event>();
    
    if(trigger.isInsert || trigger.isUnDelete || trigger.isUpdate)
    {
        for(Event ev : trigger.new)
        {
            if(ev.SubjectType__c != C_VisitTypeName)
            {
                continue;
            }
            if(ev.WhoId == null)
            {
                continue;
            }
            if(ev.OwnerId != null && ev.StartDateTime != null && ev.EndDateTime != null)
            {
                String flag = String.valueOf(ev.StartDateTime.year())+String.valueOf(ev.StartDateTime.month())+String.valueOf(ev.OwnerId).trim();
                Set_YearMonthUserid.add(flag.Substring(0,flag.Length()-3));
            }
            set_ownerId.add(ev.OwnerId);
            //Optimized-002 Ken
            if(ev.StartDateTime<startDate){
                startDate=ev.startDateTime;
            }
            if(ev.EndDateTime>endDate){
                endDate=ev.EndDateTime;
            }
            //Optimized-002
        }
    }
    else if(trigger.isDelete)
    {   
        for(Event ev : trigger.old)
        {
            if(ev.SubjectType__c != C_VisitTypeName)
            {
                continue;
            }
            if(ev.whoId == null)
            {
                continue;
            }
            if(ev.OwnerId != null && ev.StartDateTime != null && ev.EndDateTime != null)
            {
                String keyflag = String.valueOf(ev.StartDateTime.year())+String.valueOf(ev.StartDateTime.month())+String.valueOf(ev.OwnerId).trim();
                Set_YearMonthUserid.add(keyflag.Substring(0,keyflag.Length()-3));
            }
            set_ownerId.add(ev.OwnerId);
            //Optimized-002 Ken
            if(ev.StartDateTime<startDate){
                startDate=ev.startDateTime;
            }
            if(ev.EndDateTime>endDate){
                endDate=ev.EndDateTime;
            }
            //Optimized-002
        }    
    }
    
    mpList = [select Id,V2_MpYearMonthUserId__c,KA_Visit_Times__c,The_Score_of_Visit_Quality__c,Key_Information_Passing__c
                from MonthlyPlan__c 
                where V2_MpYearMonthUserId__c in: Set_YearMonthUserid]; 
   
//这里建立了一个key和monthly Plan的map                
    for(MonthlyPlan__c mp:mpList)
    {
        mpMap.put(mp.V2_MpYearMonthUserId__c,mp);
    }
    
//这里把所有相关的event全部选择出来    
    eventList = [Select Id,Done__c,StartDateTime,EndDateTime,Who.Title,WhoId,OwnerId,Who.Name
                    ,V2_IsExpire__c ,SubjectType__c,Key_Information__c,V2_Score__c
                    From Event 
                    Where OwnerId in:set_ownerId and RecordType.DeveloperName = 'V2_Event'
                    And StartDateTime>:startDate And EndDateTime<:endDate //Ken
                    ]; 
             
    for(Event ev:eventList)
     {
        String key1 = String.valueOf(ev.StartDateTime.year())+String.valueOf(ev.StartDateTime.month()+String.valueOf(ev.OwnerId).trim());
        String key = key1.Substring(0,key1.Length()-3);  
        if(mpMap.containsKey(key)){
            if(EventMap.containsKey(key))
            {
                List<Event> evlist = EventMap.get(key); 
                evlist.add(ev);
                EventMap.put(key,evlist);
            }
            else
            {
                List<Event> evlist = new List<Event>();
                evlist.add(ev);
                EventMap.put(key,evlist);
            }
         } else {
               continue;         
         }         
     }
     
     //以上返回一个Map其中包含External ID和与之对应的EventList
     
//以下内容是为了计算关键信息传递数目
    List <MonthlyPlan__c> keyInfoPassingMPList = new List <MonthlyPlan__c>();
    for(String key : mpMap.keySet()) {
        list<Event> tempEventList = new list<Event>();
        tempEventList = EventMap.get(key);      
        if(tempEventList != null){
            Integer KeyInfoNumber = 0;
            //统计时只计算已完成，未过期的EVENT
            for(Event evt : tempEventList) {
                if(evt.Key_Information__c == null || evt.Key_Information__c == '' || evt.Key_Information__c == 'NONE' || evt.V2_IsExpire__c == true || evt.Done__c == false ) {
                    continue;
                } else {
                    KeyInfoNumber++;
                }
            }
            MonthlyPlan__c mp = mpMap.get(key);
            mp.Key_Information_Passing__c = KeyInfoNumber;
            keyInfoPassingMPList.add(mp);
        } else {
            continue;
        }       
    }
    if(keyInfoPassingMPList!= null || keyInfoPassingMPList.size()!=0)
        update keyInfoPassingMPList;
//以下内容计算拜访质量平均分
   List <MonthlyPlan__c> qualityEventList = new List <MonthlyPlan__c>();
   for(String key : mpMap.keySet()) {
        list<Event> tempEventList = new list<Event>();
        tempEventList = EventMap.get(key);      
        if(tempEventList != null){
            Double CallQuality = 0;
            Double sum = 0;
            Double num = 0;
            for(Event ev : tempEventList) {
                if(ev.V2_Score__c != null && ev.V2_IsExpire__c != true && ev.Done__c == true)
                {
                  sum+=Double.valueOf(ev.V2_Score__c);
                  num++;
                }
            }
            if(num != 0) {
                CallQuality = sum/num;
            } else {
                CallQuality = 0;
            }
            System.debug('sum='+sum);
            System.debug('num='+num);
            System.debug('CallQuality='+CallQuality);
            MonthlyPlan__c mp = mpMap.get(key);
            mp.The_Score_of_Visit_Quality__c = CallQuality;
            qualityEventList.add(mp);
            
        } else {
            continue;
        }
        
   }
    if(qualityEventList!= null || qualityEventList.size()!=0)
        update qualityEventList;
  
   
//以下的内容是为了计算相关的event是否为KA EVENT     
    for(Event ev : eventList){            
        if(ev.SubjectType__c != C_VisitTypeName || ev.V2_IsExpire__c == true || ev.Done__c == false || ev.WhoId == null) {                
           continue;            
        } else {
           String tempIdString = ev.whoid;
           if(contactIdEventMap.containsKey(tempIdString) && tempIdString.subString(0,3).equals('003')){
                List<V3_EventWithKA> tempV3_EventWithKAList = contactIdEventMap.get(tempIdString);    
                V3_EventWithKA EventObj = new V3_EventWithKA();
                EventObj.ConId = ev.whoid;
                EventObj.EvtId = ev.id;
                tempV3_EventWithKAList.add(EventObj);                         
                contactIdEventMap.put(ev.whoid,tempV3_EventWithKAList);          
           }else if(!contactIdEventMap.containsKey(tempIdString) && tempIdString.subString(0,3).equals('003')){
                List<V3_EventWithKA> tempV3_EventWithKAList = new List<V3_EventWithKA>();
                V3_EventWithKA EventObj = new V3_EventWithKA();
                EventObj.ConId = ev.whoid;
                EventObj.EvtId = ev.id;
                tempV3_EventWithKAList.add(EventObj);                         
                contactIdEventMap.put(ev.whoid,tempV3_EventWithKAList);
           }
      
       }
    }
    
    contactList = [select id,DepartmentType__c,Title from Contact where id IN:contactIdEventMap.keySet()];    
    
    for(Contact con : contactList){
        if((con.DepartmentType__c =='行政'|| con.DepartmentType__c =='院办')&&(con.Title == '院长' || con.Title =='副院长') 
          || (con.DepartmentType__c =='药剂科' && (con.Title=='主任' || con.Title=='副主任'))
          || (con.DepartmentType__c =='PIVA' && con.Title=='主任')
          || (con.DepartmentType__c =='护理部' && (con.Title=='主任' || con.Title=='副主任'|| con.Title=='护理部主任'))
          || (con.DepartmentType__c =='院办' && (con.Title=='主任' || con.Title=='书记'))
          || (con.DepartmentType__c =='医务科' && con.Title=='主任')
        ) 
        {
            List<V3_EventWithKA> tempEvList = contactIdEventMap.get(con.id); 
             if(tempEvList != null){
                for(V3_EventWithKA tempEv:tempEvList){
                    tempEv.ISKA = true; 
                }                                                       
                 contactIdEventMap.put(con.id,tempEvList);               
             } else {
                 continue;             
             }   
        } else {
            List<V3_EventWithKA> tempEvList = contactIdEventMap.get(con.id);
            if(tempEvList != null){
                for(V3_EventWithKA tempEv:tempEvList){
                    tempEv.ISKA  = false;                   
                }
                contactIdEventMap.put(con.id,tempEvList);              
            } else {
                continue;             
            }                    
        }       
    }       
      
    Map<id,Boolean> IsKAEventMap = new Map<id,Boolean>();
    for(List<V3_EventWithKA> evList:contactIdEventMap.values()){
        for(V3_EventWithKA ev:evList){
            IsKAEventMap.put(ev.EvtId,ev.ISKA );
        }     
    }

    //以上内容为建立Event是否为KA的逻辑返回内容为IsKAEventMap 其中记录了event的ID和是否为KA

     for(String key:mpMap.keySet()){
        List<Event> tempEvlist = EventMap.get(key);
        Integer Kanumber = 0;
        if(tempEvlist!=null){
            for(Event evt:tempEvlist){
                Boolean isKa = IsKAEventMap.get(evt.id);
                if(isKa != null){               
                    if(isKa){
                        Kanumber++;
                    } else {
                        continue;
                    }                
                }                
                
            }
            MonthlyPlan__c mp = mpMap.get(key);          
            mp.KA_Visit_Times__c = Kanumber;                
            updateMPList.add(mp);       
        } else {
            continue;
        }
     }
     
     update updateMPList;
     
        
}