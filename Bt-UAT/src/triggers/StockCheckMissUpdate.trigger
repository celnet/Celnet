/*
 *开发人:bill
 *时间：2013-5-30
 *模块:Event
 *功能描述：挥发罐丢失后，销售代表发起丢失申请，挥发罐管理员批准后，该挥发罐标识为已丢失
 * 增加功能：bill 2013-7-6
 * 新建挥发罐盘点时，自动给盘点对象中的销售主管字段赋值，来获取创建人的上级主管
*/
trigger StockCheckMissUpdate on StockChecking__c (before insert , after update) {
	
	//销售主管
	User superUser = new User();
	Set<ID> set_parUsers = new Set<ID>();
	List<UserRole> URole = [Select Id,Name,ParentRoleId From UserRole Where Id =: UserInfo.getUserRoleId()];
	for(UserRole role:URole)
	{
		set_parUsers.add(role.ParentRoleId);
	}
	List<User> list_super = [Select u.UserRoleId, u.Id From User u where u.UserRoleId =: set_parUsers and u.IsActive  = true];
	
	system.debug(list_super[0].Id);
	if(list_super != null && list_super.size()>0)
	{
		if(trigger.isInsert){
			for(StockChecking__c stock:trigger.new)
	      	{
	      		stock.RepSupervisor__c = list_super[0].Id;
	      	}
		}
	}
	

    if(trigger.isUpdate){
      for(StockChecking__c stock:trigger.new)
      {
      	if(stock.VaporizerInfo__c == null || stock.MissApply__c != true || stock.ConfirmStatus__c != '已确认')
      	{
      		continue;
      	}
      	StockChecking__c oldStock = trigger.oldMap.get(stock.Id);
      	if(oldStock.VaporizerInfo__c == null || oldStock.MissApply__c != true || oldStock.ConfirmStatus__c != '未确认')
      	{
      		continue;
      	}
      	VaporizerInfo__c vap = [select MissApply__c from VaporizerInfo__c v where v.Id = :stock.VaporizerInfo__c];
      	vap.MissApply__c = true;
      	vap.UsedStatus__c = '未盘出';
      	vap.OperationRoom_No__c = '';
      	update vap;
      }
    }
}