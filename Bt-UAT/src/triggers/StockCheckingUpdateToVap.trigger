/**
 * Author : Sunny
 * 修改后盘点的审批通过后，将医院、位置、手术房间号同步到挥发罐上。
 */
trigger StockCheckingUpdateToVap on StockChecking__c (after update) {
	system.debug('klm!!!!!!!!!!!!');
    if(trigger.isUpdate){
    	List<ID> list_VapInfoIds = new List<ID>();
    	Map<ID,VaporizerInfo__c> map_VapInfo = new Map<ID,VaporizerInfo__c>();
    	List<VaporizerInfo__c> list_VapInfo = new List<VaporizerInfo__c>();
    	for(StockChecking__c StockChecking : trigger.new){
    		if(StockChecking.ConfirmStatus__c != trigger.oldMap.get(StockChecking.Id).ConfirmStatus__c && StockChecking.ConfirmStatus__c=='已确认'){
    			list_VapInfoIds.add(StockChecking.VaporizerInfo__c);
    		}
    	}
    	if(list_VapInfoIds.size() > 0){
    		for(VaporizerInfo__c vapInfo : [Select Id,Hospital__c,location__c,OperationRoom_No__c From VaporizerInfo__c Where Id in: list_VapInfoIds]){
    			map_VapInfo.put(vapInfo.Id , vapInfo);
    		}
    	}
    	if(map_VapInfo.size() > 0){
    		for(StockChecking__c StockChecking : trigger.new){
    			if(map_VapInfo.containsKey(StockChecking.VaporizerInfo__c)){
    				VaporizerInfo__c vapInfo = map_VapInfo.get(StockChecking.VaporizerInfo__c);
    				vapInfo.Hospital__c = StockChecking.Hospital__c;
    				vapInfo.location__c = StockChecking.Location__c;
    				vapInfo.OperationRoom_No__c = StockChecking.OperationRoom_No__c;
    				list_VapInfo.add(vapInfo);
    			}
    		}
    	}
    	if(list_VapInfo.size() >0){
    		update list_VapInfo;
    	}
    }
}