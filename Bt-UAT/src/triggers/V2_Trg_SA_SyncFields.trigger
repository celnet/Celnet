trigger V2_Trg_SA_SyncFields on Sales_Achievement__c (before insert, before update) {
    static String USER_IPREFIX='005';
    
    if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)){
        Set<String> userIdSet=new Set<String>();
        Set<String> yearSet=new Set<String>();
        Set<String> monthSet=new Set<String>();
        for(Sales_Achievement__c sa:Trigger.new){
            String ownerId=sa.OwnerId;
            if(ownerId!=null && ownerId.startsWith(USER_IPREFIX)){
                userIdSet.add(ownerId);
            }
            Date cd=sa.Time__c;
            if(cd!=null){
                String y=cd.year()+'';
                String m=cd.month()+'';
                yearSet.add(y);
                monthSet.add(m);
            }
        }
        
        //不需取经理角色
        List<V2_RoleHistory__c> user_rhs_tmp=[Select Name__c, Month__c, Year__c, Role__c, Manager__c 
                    From V2_RoleHistory__c
                    Where Name__c in: userIdSet
                    AND Year__c in: yearSet
                    AND Month__c in: monthSet
                    ];
        Map<String, List<V2_RoleHistory__c>> user_rhs_map=new Map<String, List<V2_RoleHistory__c>>();
        for(V2_RoleHistory__c rh: user_rhs_tmp){
            String userId=shortId(rh.Name__c);
            List<V2_RoleHistory__c> user_rhs = user_rhs_map.get(userId);
            if(user_rhs==null)user_rhs=new List<V2_RoleHistory__c>();
            user_rhs.add(rh);
            user_rhs_map.put(userId, user_rhs);
        }
        
        List<User> us=new List<User>();
        /*
        us=[Select id, ManagerId From User Where id in: userIdSet];
        for(User u:us){
            if(u.ManagerId!=null){
                userIdSet.add(u.ManagerId);
            }
        }
        */
        Map<String, User> usersMap=new Map<String, User>();
        us=[Select id, userRoleId, UserRole.Name, ManagerId 
                From User 
                Where id in: userIdSet];
        for(User u: us){
            String uId=shortId(u.id);
            usersMap.put(uId, u);
        }
        /*
        Map<String, User> usersMap=new Map<String, User>(
                [Select id, userRoleId, UserRole.Name, ManagerId From User Where id in: userIdSet]
            );
        */  
        for(Sales_Achievement__c sa:Trigger.new){
            String userId=shortId(sa.OwnerId);
            User u=usersMap.get(userId);
            if(u!=null){
                String roleName=u.UserRole.Name;
                String managerId=u.ManagerId;
                Date cd=sa.Time__c;
                if(cd!=null){
                    List<V2_RoleHistory__c> rhs=user_rhs_map.get(userId);
                    if(rhs!=null && rhs.size()>0){
                        String cdY=cd.year()+'';
                        String cdM=cd.month()+'';
                        for(V2_RoleHistory__c rh: rhs){
                            if(cdY.equals(rh.Year__c) && cdM.equals(rh.Month__c)){
                                roleName=rh.Role__c;
                                managerId=rh.Manager__c;
                                break;
                            }
                        }
                    }
                }
                sa.userRoleName__c=roleName;
                sa.userManager__c=managerId;
            }
        }
    }
    
    public static String shortId(String id){
        if(id==null){
            return id;
        }
        if(id.length()>15){
            return id.substring(0,15);
        }
        return id;
    }
}