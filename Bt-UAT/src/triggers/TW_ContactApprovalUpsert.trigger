/*
 *Author：Scott
 *Created on：2010-10-15
 *Description: 
 *参考大陆功能
 * Tommy 修改在2013-10-29 ：去掉对TW_RateHospGrade字段更新，此字段类型被修改为公式字段由客户自动带出
*/
trigger TW_ContactApprovalUpsert on Contact_Mod__c (after update) {
    final String ApprovalStatus = '通過';
    Set<Contact> insertContact = new Set<Contact>();
    Set<Contact> updateContact = new Set<Contact>();
    Set<Id> Set_ConModIds = new Set<Id>();//联系人修改申请Id
    Set<Id> Set_ContactIds = new Set<Id>();//联系人Id
    
    if(trigger.isUpdate)
    {
        for(Contact_Mod__c newcm:trigger.new)
        {
            if(newcm.TW_ApprovalStatus__c != ApprovalStatus  )
            {
                continue;
            }
            if(newcm.TW_DevName__c!='TW_New' && newcm.TW_DevName__c!='TW_Update')
            {
            	continue;
            }
            Set_ConModIds.add(newcm.Id);
            if(newcm.Name__c != null)
            {
                Set_ContactIds.add(newcm.Name__c);
            }
        }
    }
    //联系人
    if(Set_ConModIds.size()==0)
    {
    	return;
    }
    List<RecordType> ContactRT = [select Id,DeveloperName,Name from RecordType where SobjectType='Contact' and DeveloperName = 'TW_Contact'];
    Map<Id,Contact> ConMap = new Map<Id,Contact>([Select LastName,Id,Name,AccountId,Phone,MobilePhone,Email,SP_Job__c,Department,Birthdate,Description,Gender__c,V2_Education__c,V2_ContactTypeByRENAL__c,
                           position__c,V2_Level__c,TW_RateBaxBusiness__c,interest__c,V2_RateLeadership__c,V2_TotalScore__c,V2_Relationship_with_Baxter__c,
                           TW_Nutrition__c,TW_INfusor__c,TW_NutritionType__c,TW_INfusorType__c,TW_Outpatient__c,TW_AverageInpatients__c,TW_PrescriptionPotential__c,
                           TW_DoctorStatus__c from Contact c where Id in: Set_ContactIds]);
                           
                           
    for(Contact_Mod__c cm:[select Name__c,RecordType.DeveloperName,OwnerId,NewContact__c,
                            Account__c ,
                            Phone__c,
                            Mobile__c,
                            Email__c ,
                            Gender__c,
                            TW_Position__c ,
                            Birthday__c ,
                            TW_Title__c,
                            TW_Department__c,
                            TW_DoctorStatus__c,
                            V2_interest__c,
                            TW_DoctorStatusDetail__c,
                            Comment__c,
                            V2_ContactTypeByRENAL__c,
                            V2_Level__c,
                            TW_RateBaxterProduct__c,
                            TW_RateBaxBusiness__c,
                            TW_Outpatient__c,
                            TW_AverageInpatients__c,
                            V2_Education__c,
                            V2_RateLeadership__c,
                            TW_PrescriptionPotential__c,
                            V2_TotalScore__c,
                            TW_INfusor__c,
                            TW_INfusorType__c,
                            TW_Nutrition__c,
                            TW_NutritionType__c 
                            from Contact_Mod__c where id in:Set_ConModIds])
    {
        
        if(cm.RecordType.DeveloperName =='TW_New')
        {
            Contact contact = new Contact();
            contact.RecordTypeId = ContactRT[0].Id; 
            InsertOrUpdateContact(cm,contact);
        }
        else if(cm.RecordType.DeveloperName =='TW_Update')
        {
            if(ConMap.containsKey(cm.Name__c))
            {
                Contact contact = ConMap.get(cm.Name__c);
                InsertOrUpdateContact(cm,contact);
            }
        }
        
    }
    //新增或编辑
    public void InsertOrUpdateContact(Contact_Mod__c curContact,Contact contact)
    {
        
       try
       {
            //所有人
            contact.OwnerId = curContact.OwnerId;
            //姓名
             contact.LastName = curContact.NewContact__c;
             ////////////////////基本信息
            //客户
            contact.AccountId = curContact.Account__c;
            //电话
            contact.Phone = curContact.Phone__c;
            //手机
            contact.MobilePhone =  curContact.Mobile__c;
            //电子邮件
            contact.Email = curContact.Email__c;
            //性别
            //Gender
            contact.Gender__c = curContact.Gender__c;
            //學會職務
            contact.position__c = curContact.TW_Position__c;
            //出生日期
            contact.Birthdate = curContact.Birthday__c;
            //職稱
            contact.Title = curContact.TW_Title__c;
            //所属科别
            contact.TW_Department__c = curContact.TW_Department__c;
            //醫師狀態
            contact.TW_DoctorStatus__c = curContact.TW_DoctorStatus__c;
            //兴趣爱好
            contact.interest__c = curContact.V2_interest__c;
            //醫師狀態/異動
            contact.TW_DoctorStatusDetail__c = curContact.TW_DoctorStatusDetail__c;
            //备注
            contact.Description =  curContact.Comment__c;
            
            /////////////評估資訊
            
            //联系人类型（BIOS）
            contact.V2_ContactTypeByRENAL__c = curContact.V2_ContactTypeByRENAL__c;
            //联系人分级
            contact.V2_Level__c = curContact.V2_Level__c;
            //對百特產品支持程度評分(1~5分)
            contact.TW_RateBaxterProduct__c = curContact.TW_RateBaxterProduct__c;
            //對產品使用/接受分級
            contact.TW_RateBaxBusiness__c = curContact.TW_RateBaxBusiness__c;
            //每週平均門診量
            contact.TW_Outpatient__c = curContact.TW_Outpatient__c;
            //每週平均住院病人數
            contact.TW_AverageInpatients__c = curContact.TW_AverageInpatients__c;
            //学术程度;
            contact.V2_Education__c = curContact.V2_Education__c;
            //影响力
            contact.V2_RateLeadership__c = curContact.V2_RateLeadership__c;
            //處方潛力
            contact.TW_PrescriptionPotential__c = curContact.TW_PrescriptionPotential__c;
            //医生总分
            contact.V2_TotalScore__c = curContact.V2_TotalScore__c;
            
            ////////////////////市場細分資訊
            //INfusor
            contact.TW_INfusor__c = curContact.TW_INfusor__c;
            //INfusor市场细分
            contact.TW_INfusorType__c = curContact.TW_INfusorType__c;
            //Nutrition
            contact.TW_Nutrition__c = curContact.TW_Nutrition__c;
            //Nutrition市场细分
            contact.TW_NutritionType__c = curContact.TW_NutritionType__c;
            
           
            if(curContact.RecordType.DeveloperName =='TW_New')
            {
                insertContact.add(contact);
            }
            else if(curContact.RecordType.DeveloperName =='TW_Update'  &&  curContact.Name__c != null)
            {
                updateContact.add(contact);
            }
        }catch(Exception e)
        {
            System.debug('###################################'+String.valueOf(e)+' 第'+e.getLineNumber()+'行');
        }
    }
    List<Contact> ListinsertContact = new List<Contact>();
    List<Contact> ListupdateContact = new List<Contact>();
    ListupdateContact.addAll(updateContact);
    ListinsertContact.addAll(insertContact);
    update ListupdateContact;
    
    insert ListinsertContact;
    
}