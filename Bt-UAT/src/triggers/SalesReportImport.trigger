/**
 * Author : Bill
 * date : 2013-8-3
 * 功能：当销售数据被导入后触发OpportunityReportDataBatch执行
**/
trigger SalesReportImport on SalesReport__c (after insert, after update) {
    //获取导入的数据是几月份
    integer curMonth = 0;
    for(SalesReport__c sr : trigger.new)
    {
        if(sr.ActualQty__c != null)
        {
            curMonth = ((curMonth>sr.Time__c.Month())?curMonth:sr.Time__c.Month());
        }
    }
    /*********************BILL update 2013-8-8 start***************************/
    //销售预测报表trigger触发batch这个功能取消掉
    //OpportunityReportDataBatch oppBatch = new OpportunityReportDataBatch();
    //oppBatch.curMonth = curMonth;
    //if(!system.test.isRunningTest()){
    //    database.executeBatch(oppBatch,10);         
    //}
	/********************BILL update 2013-8-8 end****************************/
}