/*****************************************************************************
 *Owner:AndyWang
 *Date:2012年9月11日 09:34:49
 *Function:
 *规则 create HKlead Syc update Account
 *edit 2012.10.29 ;add create opp after account create/update;
*****************************************************************************/ 
trigger V3_CreateHKleadSycAccount on HK_Leads__c (before insert,before update,after insert,after update) {
	
			map<String,HK_Leads__c> mapLeads = new map<String,HK_Leads__c>();
	    	Set<String> SetAccName = new Set<String>();
			//获取到Mid集合
			for(HK_Leads__c HKlead : trigger.new){
				if(HKlead.Address_Num__c != null){
					SetAccName.add(HKlead.Address_Num__c);
					mapLeads.put(HKlead.Address_Num__c,HKlead);
				}
			}
			//Sync account
			if(trigger.isBefore){
				
			//查找对应的Account集合
				list<Account> lisAccount = [select id,Name,BillingStreet,ShippingStreet,MID__c,V3_Customer_Type__c,
											V3_Country__c,V3_Payment_Terms__c,V3_Customer_Cluster__c ,Fax,V3_Currency__c,Phone
											from Account where MID__c IN:SetAccName];
				for(Account acc:lisAccount){
 					    HK_Leads__c hk=mapLeads.get(acc.MID__c);
 					    acc.Name=hk.Name;
						acc.V3_Customer_Type__c = hk.Customer_Type__c;
						acc.V3_Country__c = hk.Country__c;
						acc.V3_Payment_Terms__c = hk.Payment_Terms__c;
						acc.V3_Customer_Cluster__c = hk.Customer_Cluster__c;
						acc.Fax = hk.Fax__c;
						acc.V3_Currency__c = hk.Currency__c;
						acc.Phone = hk.Phone_Number__c;
						acc.BillingStreet = hk.Address_1__c;
						acc.ShippingStreet = hk.Address_2__c;
						acc.PriceGroup__c = hk.Price_Group__c;
						//已经更新则从leadmap中去除；
						mapLeads.remove(acc.MID__c);
				}
				
				//找到Mid的客户做更新；
				if(lisAccount.size() > 0){
					update lisAccount;
				}
			    //剩下的leads做新增操作；
			    RecordType HKRecordType = [Select r.Name, r.Id From RecordType r where Name ='医院' and isactive=true and SobjectType='Account'];
			    
				list<Account> lisAcc=new list<Account>();
					for(HK_Leads__c HKlead : mapLeads.Values()){
						Account account = new Account();
						account.Name = HKlead.Name;
						account.V3_Customer_Type__c = HKlead.Customer_Type__c;
						account.V3_Country__c = HKlead.Country__c;
						account.V3_Payment_Terms__c = HKlead.Payment_Terms__c;
						account.V3_Customer_Cluster__c =HKlead.Customer_Cluster__c;
						account.Fax = HKlead.Fax__c;
						account.V3_Currency__c = HKlead.Currency__c;
						account.MID__c = HKlead.Address_Num__c;
						account.Phone = HKlead.Phone_Number__c;
						account.BillingStreet = HKlead.Address_1__c;
						account.ShippingStreet = HKlead.Address_2__c;
						account.PriceGroup__c = HKlead.Price_Group__c;
						account.RecordTypeId=HKRecordType.Id;
						lisAcc.add(account);
					}
					if(lisAcc.size()>0){
					 	insert lisAcc;
					}
			}
			//create opportunity
			if(trigger.isAfter){
			    RecordType HKRecordType = [Select r.Name, r.Id From RecordType r where Name ='HK' 
				    and isactive=true and SobjectType='Opportunity'];
					
				list<Opportunity> lisOpportunity = new List<Opportunity>(); //需要插入的opp
				//查找对应的Account集合,可能有新插入的Account,所以重新查询；
				list<Account> listAccount = [select id,Name,(Select Id, OwnerId,AccountId From Opportunities)
											from Account where MID__c IN:SetAccName];
				//没有Opportunity的创建一个
				for(Account account1:listAccount){
					if(account1.Opportunities==null || account1.Opportunities.size()==0){
							Opportunity Opportuniti = new Opportunity();
							Opportuniti.Name = account1.Name;
							Opportuniti.AccountId = account1.id;
							//Opportuniti.OwnerId = HKLeadsTeam.Team_Member__c;
							Opportuniti.CloseDate = Date.Today();
							Opportuniti.StageName = 'close';
							Opportuniti.RecordTypeId = HKRecordType.Id;
							lisOpportunity.add(Opportuniti);
						
					}
				}
				if(lisOpportunity.size()>0){
								insert lisOpportunity;
				}
	
				
			}
	
}