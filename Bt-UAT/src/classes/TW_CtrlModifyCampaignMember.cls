public without sharing class TW_CtrlModifyCampaignMember {
	//表示行的封装类
	public class ActionRow
	{
		public CampaignMemberHistory__c CampaignMemberHistory {get; set;}
		public CampaignMember RelatedCampaignMember {get; set;}
		public Boolean IsCancel {get; set;}
		public String Type {get; set;}//标识是新增的行NewMemberRow，还是对存在的成员进行删除的行DeleteMemberRow，还是已经提交审批的行HisRow
		public Integer RowId {get; set;}
		public List<SelectOption> Options 
		{
			get
			{
				List<SelectOption> options = new List<SelectOption>();
				if(this.RelatedCampaignMember != null)//表示对已有的Member进行操作，所以只有删除操作可选
				{
		            options.add(new SelectOption('','--无--'));
		            options.add(new SelectOption('删除','删除'));
				}
				else if(this.CampaignMemberHistory.Action__c == '新增')//表示是一个新增的Row，只有新增操作可选
				{
		            options.add(new SelectOption('新增','新增'));
				}
				return options;
			}
		}
		
		
		public Boolean IsDelete
		{
			get
			{
				if(this.Type == 'DeleteMemberRow' && this.CampaignMemberHistory.Action__c == '删除')
				{
					return true;
				}
				return false;
			}
		}
		public Boolean IsNew
		{
			get
			{
				if(this.Type == 'NewMemberRow' && this.CampaignMemberHistory.Action__c == '新增')
				{
					return true;
				}
				return false;
			}
		}
		//用户在已经已经提交的审批则只能看不能做任何修改
		public Boolean IsHisRow
		{
			get
			{
				if(this.Type == 'HisRow')
				{
					return true;
				}
				return false;
			}	
		}
		//新增的可以进行改动
		public Boolean IsNewMemberRow
		{
			get
			{
				if(this.Type == 'NewMemberRow')
				{
					return true;
				}
				return false;//
			}	
		}
		
		//删除的操作行只能修改操作从无到删除
		public Boolean IsDeleteMemberRow
		{
			get
			{
				if(this.Type == 'DeleteMemberRow')
				{
					return true;
				}
				return false;//
			}	
		}
		
		public Boolean ActionEnabled
		{
			get
			{
				if(this.IsDeleteMemberRow || this.IsNewMemberRow)
				{
					return true;
				}
				return false;
			}	
		}
		public Boolean ActionDisabled
		{
			get
			{
				return !this.ActionEnabled;
			}	
		}
		public Boolean FieldEnabled
		{
			get
			{
				if(this.IsNewMemberRow)
				{
					return true;
				}
				return false;
			}	
		}
		public Boolean FieldDisabled
		{
			get
			{
				return !this.FieldEnabled;
			}	
		}
		public String SubmittedTime
		{
			get
			{
				if(this.CampaignMemberHistory == null)
				{
					return null;
				}
				if(this.CampaignMemberHistory.CreatedDate == null)
				{
					return null;
				}
				
				return this.CampaignMemberHistory.CreatedDate.format('yyyy-MM-dd hh:mm');
			}
		}
		
		public ActionRow()
		{
			this.IsCancel = false;
		}
	}
	
	public ID CampaignId{get;set;}
	public Campaign Campaign{get;set;}
	public List<CampaignMember> MyOwnCampaignMemberList{get; set;}
	public List<CampaignMember> CampaignMemberList{get; set;}
	public List<CampaignMemberHistory__c> MyOwnCampaignMemberHistoryList{get;set;}//我目前拥有的
	public List<ActionRow> ActionRowList{get;set;}//显示在界面上：我拥有的 +根据我拥有还没有进行操作的市场成员生成新的+我在页面新加的
	//public Map<Id,String> contactmap = new Map<Id,String>();
	//public Boolean disabled{get;set;}
	//当前用户角色Id
	public Id CurrentRoleId{get;set;}
	//当前用户角色名称
	public String CurrentRoleName{get;set;}
	//当前用户Id
	public Id CurrentUserId{get;set;}
	public User CurrentUser{get;set;}
	public User Rep{get; set;}
	public User Supervisor{get;set;}
	public User AreaManager{get;set;}
	public Boolean SupervisorDefAgree{get;set;}//默认选项是同意
	public Boolean AreaManagerDefAgree{get;set;}//
	public Boolean IsEnabled{get;set;}
	/*****************bill add 2013-6-26 start****************/
	public Boolean SaveIsEnabled{get;set;}
	/*****************bill add 2013-6-26 end****************/
	public Integer RowIdIncreas{get;set;}
	public Integer TiggerRowId{get;set;}//用于在取消/新增一行，确定联系人的客户时，标识行号
	
	private Boolean IsFinish;
	//private String[] SupervisorKeyWordList = new String[] {'Supervisor', 'District', 'DISTRICT'};
	//private String[] AreaManageKeyWordList = new String[] {'Regional'};
	private Map<Integer, ActionRow> RowMap;//用于根据 行号的标识快速索引的对应的Row
	
	final String NationalManager = 'TW National Sales Manager';
	
	public Boolean ShowClose
	{
		get
		{
			return IsFinish;
		}
	}
	
	public TW_CtrlModifyCampaignMember()
	{
		this.IsEnabled = true;
		this.SaveIsEnabled = true;
		this.IsFinish = false;
		this.SupervisorDefAgree = false;
		this.AreaManagerDefAgree = false;
		if(RowIdIncreas == null)
		{
			this.RowIdIncreas = 0;
		}
		this.RowMap = new Map<Integer, ActionRow>();
		//IsClose=false;

		this.CampaignId = ApexPages.currentPage().getParameters().get('camid');
		if(this.CampaignId == null)
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '必須指定一個行銷活動');            
            ApexPages.addMessage(msg);
            this.IsEnabled = false;
            this.SaveIsEnabled = false;
            return;
		}
		System.debug('####################################################################################  '+CampaignId);
		this.Campaign = [select Name, Id, OwnerId, Status, IsNotapprovalBylevel__c, Owner.Id, Owner.Name, Owner.Alias, Owner.Email from Campaign where Id =: this.CampaignId];
		if(this.Campaign.Status != 'Sign Up Closed' && this.Campaign.Status != '报名结束')
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '只有報名結束的行銷活動才適用本功能');            
            ApexPages.addMessage(msg);
            this.IsEnabled = false;
            this.SaveIsEnabled = false;
            return;
		}
		if(!InitUserHierarchy())
		{
			this.IsEnabled = false;
			this.SaveIsEnabled = false;
			return;
		}
		
		this.InitActionRowList();
	}
	
	//按照关键字特征识别销售代表、主管、大区经理，如果主管和大区经理没有找到则设置为空
	private Boolean InitUserHierarchy()
	{
		this.CurrentUser = [Select ManagerId,UserRole.Name, UserRole.Id, UserRole.ParentRoleId, UserRoleId, Id, Name, Alias, Email From User Where Id=:UserInfo.getUserId()];
		this.CurrentRoleName = this.CurrentUser.UserRole.Name;
		this.CurrentUserId = this.CurrentUser.Id;
		this.CurrentRoleId = this.CurrentUser.UserRoleId;
		//判断是否是销售代表
		
		this.Rep = this.CurrentUser;
		if(CurrentRoleName.contains('TW'))
		{
			this.InitUserHierarchyIVT();
		}
		else
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '您不是TW的銷售代表，不可使用本功能');            
            ApexPages.addMessage(msg);
            return false;
		}
		return true;
	}
	/*
	找到当前用户的上级主管
	*/
	private void InitUserHierarchyIVT()
	{
		
		//2013-11-13修改：角色TW National Sales Manager的用户不通知其主管
		if(CurrentUser.UserRole.Name ==NationalManager)
		{
			this.Supervisor = null;
		}
		else
		{
			//向上搜寻主管、大区经理,一直到顶层结束
			//向上第一层是主管也有可能是大区经理
			List<User> Supervisor = [select ManagerId,UserRole.Name, UserRole.Id, UserRole.ParentRoleId, UserRoleId, Id,Name,Alias,Email 
									from User where Id =: this.CurrentUser.ManagerId];
			if(Supervisor != null && Supervisor.size()>0)
			{
				this.Supervisor = Supervisor[0];
			}
			else
			{
				this.Supervisor = null;
			}
		}
	}
	
	private void InitActionRowList()
	{
		this.MyOwnCampaignMemberList = [Select Id, 
			ContactId, 
			CampaignId, 
			V2_Account__c, 
			V2_Comment__c,
			V2_DepartFlight__c,
			V2_DepartDate__c,
			V2_ArriveFlight__c,
			V2_ArriveDate__c,
			User__c
			From CampaignMember Where CampaignId =: this.CampaignId And User__c =: this.CurrentUserId 
			Order BY CreatedDate];
		this.MyOwnCampaignMemberHistoryList = [Select Id, 
			CampaignMemberId__c,
			ContactId__c, 
			CampaignId__c, 
			AccountId__c, 
			Comment__c,
			DepartFlight__c,
			DepartDate__c,
			ArriveFlight__c,
			ArriveDate__c,
			Action__c,
			AreaManagerApprove__c,
			MarketingApprove__c, 
			SupervisorApprove__c,
			User__c,
			AreaManager__c,
			Supervisor__c,
			MarketingRep__c,
			ApproveStep__c,
			CreatedDate
			From CampaignMemberHistory__c Where CampaignId__c =: this.CampaignId And User__c =: this.CurrentUserId
			Order By CreatedDate
			];
		this.ActionRowList = new List<ActionRow>();
		//根据自己以前的历史产生Row加入显示列表，审批中的和完成的，只是为了显示给用户看
		for(CampaignMemberHistory__c his : this.MyOwnCampaignMemberHistoryList)
		{
			ActionRow row = new ActionRow();
			row.CampaignMemberHistory = his;
			row.Type = 'HisRow';
			this.ActionRowList.add(row);
		}
		//根据自己市场活动成员产生新的Row加入显示列表，为用户选择删除操作
		for(CampaignMember cpm : this.MyOwnCampaignMemberList)
		{
			CampaignMemberHistory__c relatedHisNotFinishApprove = this.FindRelatedHisNotFinishApprove(cpm.Id);
			if(relatedHisNotFinishApprove == null)//没找到有正在审批对应的历史则可以产生新的历史以供用户选择删除操作
			{
				CampaignMemberHistory__c newHis = new CampaignMemberHistory__c();
				newHis.CampaignMemberId__c = cpm.Id;
				newHis.ContactId__c = cpm.ContactId;
				newHis.CampaignId__c = cpm.CampaignId;
				newHis.AccountId__c = cpm.V2_Account__c; 
				newHis.Comment__c = cpm.V2_Comment__c;
				newHis.DepartFlight__c = cpm.V2_DepartFlight__c;
				newHis.DepartDate__c = cpm.V2_DepartDate__c;
				newHis.ArriveFlight__c = cpm.V2_ArriveFlight__c;
				newHis.ArriveDate__c = cpm.V2_ArriveDate__c;
				newHis.ApproveUrl__c = URL.getSalesforceBaseUrl().toExternalForm() + '/apex/TW_ModifyCampaignMemberApprove?camid=' + this.Campaign.Id;
				newHis.Action__c = null;
				//newHis.AreaManagerApprove__c,
				//newHis.MarketingApprove__c, 
				//newHis.SupervisorApprove__c,
				newHis.User__c = this.Rep.Id;
				newHis.ApproveStep__c = '提交';
				ActionRow row = new ActionRow();
				row.Type = 'DeleteMemberRow';
				row.CampaignMemberHistory = newHis;
				row.RelatedCampaignMember = cpm;
				this.ActionRowList.add(row);
			}
		}
	}
	
	private CampaignMemberHistory__c FindRelatedHisNotFinishApprove(ID cpmId)
	{
		for(CampaignMemberHistory__c his : this.MyOwnCampaignMemberHistoryList)
		{
			if(his.CampaignMemberId__c == cpmId)//找到
			{
				if(his.ApproveStep__c != '结束')
				{
					return his;
				}
			}
		}
		return null;
	}
	private CampaignMemberHistory__c SetApprover(CampaignMemberHistory__c his)
	{
		if(this.Supervisor != null)
		{
			his.Supervisor__c = this.Supervisor.Id;
			if(this.Campaign.IsNotapprovalBylevel__c == false )//若是不勾选审批结束后不需要逐级审批，则默认审批为空
		    {
			   his.SupervisorDefAgree__c = this.SupervisorDefAgree;
		    }
		}
		his.MarketingRep__c = this.Campaign.OwnerId;
		//2013-5-3 Sunny 设置审批步骤的时候需要判断是否主管和大区经理已经默认通过了。
		//bill 若是勾选报名结束后不需要审批，则直接市场部审批
		/************************bill update 2013/6/14 start*************************************/
		if(this.Campaign.IsNotapprovalBylevel__c == false )
		{
		   if(this.Supervisor != null && this.SupervisorDefAgree == false)
		   {
			   his.ApproveStep__c = '主管审批';
		   }
		   else
		   {
			   his.ApproveStep__c = '市场部审批';
		   }
		}else{
		   his.ApproveStep__c = '市场部审批';
		}
		/************************bill update 2013/6/14 end*************************************/
		return his;
	}
	
	private Integer GenRowId()
	{
		if(this.RowIdIncreas == null)
		{
			this.RowIdIncreas = 0;
		}
		this.RowIdIncreas ++ ;
		return this.RowIdIncreas;
	}
	
	//添加操作
	public void Add()
	{
		CampaignMemberHistory__c newHis = new CampaignMemberHistory__c();
		newHis.Action__c = '新增';
		newHis.User__c = this.Rep.Id;
		newHis.CampaignId__c = this.Campaign.Id;
		newHis.ApproveStep__c = '提交';
		newHis.ApproveUrl__c = URL.getSalesforceBaseUrl().toExternalForm() + '/apex/TW_ModifyCampaignMemberApprove?camid=' + this.Campaign.Id;
		ActionRow newRow = new ActionRow();
		newRow.RowId = this.GenRowId();
		newRow.Type = 'NewMemberRow';
		newRow.CampaignMemberHistory = newHis;
		this.RowMap.put(newRow.RowId, newRow);
		this.ActionRowList.add(newRow);
	}
	
	//取消操作
	public void CancelRow()
	{
		if(this.TiggerRowId == null)
		{
			return;
		}
		if(!this.RowMap.containsKey(this.TiggerRowId))
		{
			return;
		}
		ActionRow triggerRow = this.RowMap.get(this.TiggerRowId);
		if(triggerRow == null)
		{
			return;
		}
		for(Integer i = 0; i < this.ActionRowList.size(); i ++ )
		{
			if(this.ActionRowList[i] == triggerRow)
			{
				this.ActionRowList.remove(i);
			}
		}
		this.RowMap.remove(this.TiggerRowId);

		/*奇怪的问题，需要删多次才能删掉
		for(Integer c = 1; c<= 5; c++)
		{
			for(Integer i = 0; i < this.ActionRowList.size(); i ++ )
			{
				if(this.ActionRowList[i].IsCancel)
				{
					this.ActionRowList.remove(i);
				}
			}
		}
		*/
	}
	
	//保存操作
	public void Save()
	{
		String flag ;
		try
		{
			this.IsFinish = false;
			//this.Cancel();
			List<CampaignMemberHistory__c> newHisList = new List<CampaignMemberHistory__c>();
			Integer delCount = 0;
			Integer newCount = 0;
			Boolean isDataOk = true;
			for(ActionRow row : this.ActionRowList)
			{
				if(row.IsHisRow)//对于以前提交的审批历史则不做任何处理
				{
					continue;
				}
				if(row.IsNew)
				{
					this.SetApprover(row.CampaignMemberHistory);
					if(row.CampaignMemberHistory.ContactId__c != null)
					{
						newHisList.add(row.CampaignMemberHistory);
						newCount ++;
					}
					else
					{
						isDataOk = false;
					}
				}
				else if(row.IsDelete)
				{
					this.SetApprover(row.CampaignMemberHistory);
					newHisList.add(row.CampaignMemberHistory);
					delCount ++;
				}
			}
			if(!isDataOk)
			{
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '提交失败，联系人不能为空!');            
            	ApexPages.addMessage(msg);
            	return;
			}
			if(newHisList.size() != 0)
			{
				flag = newHisList[0].Supervisor__c+' User__c'+newHisList[0].User__c+'MarketingRep__c '+newHisList[0].MarketingRep__c+'CampaignId__c'+newHisList[0].CampaignId__c+'ContactId__c'+newHisList[0].ContactId__c;
				insert newHisList;
				this.SendEmailToApprover(newHisList);
				/*====================2013-11-13修改：如果角色TW National Sales Manager的用户将审批主管置为他自己============*/
				if(CurrentUser.UserRole.Name ==NationalManager)
				{
					List<CampaignMemberHistory__c> Update_newHisList = new List<CampaignMemberHistory__c>();
					for(CampaignMemberHistory__c cmh : newHisList)
					{
						cmh.SupervisorApprove__c = '通过';
						cmh.ApproveStep__c = '市场部审批';
						cmh.Supervisor__c = CurrentUser.Id;
						Update_newHisList.add(cmh);
					}
					update Update_newHisList;
				}
				/*====================2013-11-13修改====================*/
				this.IsFinish = true;
				this.IsEnabled = false;
				this.SaveIsEnabled = false;
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, '審批已經提交成功，新增' + newCount + '個成員，刪除' + delCount + '個成員。請關閉窗口。');            
            	ApexPages.addMessage(msg);
            	//重新初始化列表
            	this.InitActionRowList();
			}
			else
			{
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, '您沒有做出任何修改，沒有任何審批可以提交。');            
            	ApexPages.addMessage(msg);
			}
		}
		catch(Exception e)
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR , String.valueOf(e)+' 第'+ e.getLineNumber()+'行'+flag);            
            ApexPages.addMessage(msg);
            return;
		}
	}
	
	public void CheckContact()
	{
		if(this.TiggerRowId == null)
		{
			return;
		}
		if(!this.RowMap.containsKey(this.TiggerRowId))
		{
			return;
		}
		ActionRow triggerRow = this.RowMap.get(this.TiggerRowId);
		if(triggerRow == null)
		{
			return;
		}
		CampaignMemberHistory__c triggerHis = triggerRow.CampaignMemberHistory;
		if(triggerHis.ContactId__c == null)
		{
			return;
		}
		//检查当前新添加的行中是否已经存在这个联系人
		Boolean dupContactInNew = false;
		for(ActionRow row : this.ActionRowList)
		{
			if(row.CampaignMemberHistory.Action__c == '新增'&& row.CampaignMemberHistory.ApproveStep__c != '结束')
			{
				if(triggerRow.RowId != row.RowId 
					&& triggerRow.CampaignMemberHistory.ContactId__c == row.CampaignMemberHistory.ContactId__c)
				{
					dupContactInNew = true;
				}
			}
		}
		if(dupContactInNew)
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR , '您已經選擇新增了這個聯繫人，不能重複選擇');            
            ApexPages.addMessage(msg);
            triggerHis.ContactId__c = null;
            return;
		}
		//检查当前的市场活动成员中是否已经存在这个联系人
		Boolean dupContactInMeb = false;//是否市场活动成员存在
		Boolean dupConatactInMebAndNoDeleteingItem = false;//市场活动成员存在，当前是否村在针对该联系人待审批的删除项目(只有这个才能确定为真的重复)
		for(CampaignMember cpm: [Select Id From CampaignMember 
			Where CampaignId =: this.CampaignId 
			And ContactId =: triggerRow.CampaignMemberHistory.ContactId__c])
		{
			dupContactInMeb = true;
		}
		if(dupContactInMeb)
		{
			//再查针对该联系人待审批的删除项目是否存在
			dupConatactInMebAndNoDeleteingItem = true;
			for(ActionRow row : this.ActionRowList)
			{
				if(row.CampaignMemberHistory.Action__c == '删除'&& row.CampaignMemberHistory.ApproveStep__c != '结束')
				{
					if((row.CampaignMemberHistory.ContactId__c == triggerRow.CampaignMemberHistory.ContactId__c) && triggerRow.CampaignMemberHistory.ApproveResult__c == '通过')
					{
						dupConatactInMebAndNoDeleteingItem = false;
					}
				}
			}
		}
		if(dupConatactInMebAndNoDeleteingItem)
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR , '您不能選擇在當前行銷活動成員列表中已經存在的聯繫人，除非您之前已經提交了針對該聯繫人成員的刪除操作且審批通過');            
            ApexPages.addMessage(msg);
            triggerHis.ContactId__c = null;
            this.SaveIsEnabled = false;
            return;
		}
		Contact ct = [Select Id, Name, AccountId From Contact Where Id =: triggerHis.ContactId__c];
		triggerHis.AccountId__c = ct.AccountId;
		this.SaveIsEnabled = true;
	}
	public PageReference ReturnCampaign()
	{
		return new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/' + this.CampaignId);
	}
	
	private void SendEmailToApprover(List<CampaignMemberHistory__c> newHisList)
	{
		String approveType;
		User approver;
		//2013-5-3 Sunny 设置审批步骤的时候需要判断是否主管和大区经理已经默认通过了。
		//bill 若勾选审批结束后不需要逐级审批
		/*************************bill update 2013/6/14 start***********************************/
		if(this.Campaign.IsNotapprovalBylevel__c == false )
		{
		   if(this.Supervisor != null && this.SupervisorDefAgree == false)
		   {
			   approveType = '主管审批';
			   approver = this.Supervisor;
		   }
		   else if(this.AreaManager != null && this.AreaManagerDefAgree == false)
		   {
			   approveType = '大区经理审批';
			   approver = this.AreaManager;
		   }
		   else
		   {
			  approveType = '市场部审批';
			  approver = this.Campaign.Owner;
		   }
		}else{
			approveType = '市场部审批';
			approver = this.Campaign.Owner;
		}
		/*************************bill update 2013/6/14 end***********************************/
		String subject = '來自SEP系統通知：行銷活動成員更新審批';
	    String message = '您好  ' + approver.Name + '\n\n' + 
	     '報名結束的行銷活動的成員列表已被銷售代表更改，請點擊下面的連接到系統中進行審批\n' +
	     '	行銷活動: ' + this.Campaign.Name + '\n' +
	     //'	审批提交人（销售代表）: ' + this.Rep.Name + '\n' +
	     //'	审批类型: ' + approveType + '\n' +
	     //'	审批人: ' + approver.Name + '\n' +
	     //'	影响市场活动成员数（新增和删除）: ' + newHisList.size() + '\n' +
	     '	審批連接: ' + URL.getSalesforceBaseUrl().toExternalForm() + '/apex/TW_ModifyCampaignMemberApprove?camid=' + this.Campaign.Id + '\n' + 
        '祝您工作愉快!\n' +
        '__________________________________________________ \n' +
        '本郵件由Baxter Salesforce.com CRM系統產生，請勿回复。\n' +
        '如有任何疑問或者要求，請聯繫系統管理人員。\n' +
	  	'Baxter SEP System';
	   	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	   	String[] toAddresses = new String[] {approver.Email};
	   	mail.setToAddresses(toAddresses);
	   	mail.setReplyTo('no-reply@salesforce.com');
	   	mail.setSubject(subject);
	   	mail.setSenderDisplayName('Baxter SEP System');
	  	mail.setPlainTextBody(message);
	  	if(!Test.isRunningTest()) 
	  	{
	   		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	  	}
	}
	
	/*======================测试=============================*/
    static testMethod void TestApproveProcess() 
    {	
		UserRole regRole = [select Id from UserRole where name='TW South Sales Rep' limit 1];
		UserRole supRole = [select Id from UserRole where name='TW Regional South Sales Manager' limit 1];
		UserRole marRole = [select Id from UserRole where name='TW Marketing Product Manager' limit 1];
		
		/*用户简档*/
		//rep简档
	    Profile RepProRenal = [select Id from Profile where Name  = 'TW Sales Rep' limit 1];
	    //sup简档
	    Profile SupProRenal = [select Id from Profile where Name  = 'TW Sales Supervisor' limit 1];
	    //marketing
	    Profile MarketProRenal = [select Id from Profile where Name='TW Marketing' limit 1];
		
		/************User************/
		List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        
        //主管
        User supUser = new User();
	    supUser.Username='supUser@123.com';
	    supUser.LastName='supUser';
	    supUser.Email='supUser@123.com';
	    supUser.Alias=user[0].Alias;
	    supUser.TimeZoneSidKey=user[0].TimeZoneSidKey;
	    supUser.ProfileId=SupProRenal.Id;
	    supUser.LocaleSidKey=user[0].LocaleSidKey;
	    supUser.LanguageLocaleKey=user[0].LanguageLocaleKey;
        supUser.EmailEncodingKey=user[0].EmailEncodingKey;
	    supUser.CommunityNickname='supUser';
	    supUser.MobilePhone='12345678912';
	    supUser.UserRoleId = supRole.Id ;
	    supUser.IsActive = true;
     	insert supUser;
        
        //销售
        User repUser = new User();
	    repUser.Username='repUser@123.com';
	    repUser.LastName='repUser';
	    repUser.Email='repUser@123.com';
	    repUser.Alias=user[0].Alias;
	    repUser.TimeZoneSidKey=user[0].TimeZoneSidKey;
	    repUser.ProfileId=RepProRenal.Id;
	    repUser.LocaleSidKey=user[0].LocaleSidKey;
	    repUser.LanguageLocaleKey=user[0].LanguageLocaleKey;
        repUser.EmailEncodingKey=user[0].EmailEncodingKey;
	    repUser.CommunityNickname='repUser';
	    repUser.MobilePhone='12345678912';
	    repUser.UserRoleId = regRole.Id ;
	    repUser.IsActive = true;
	    repUser.ManagerId = supUser.Id;
     	insert repUser;
        
        //市场
        User mkUser = new User();
	    mkUser.Username='mkUser@123.com';
	    mkUser.LastName='mkUser';
	    mkUser.Email='mkUser@123.com';
	    mkUser.Alias=user[0].Alias;
	    mkUser.TimeZoneSidKey=user[0].TimeZoneSidKey;
	    mkUser.ProfileId=MarketProRenal.Id;
	    mkUser.LocaleSidKey=user[0].LocaleSidKey;
	    mkUser.LanguageLocaleKey=user[0].LanguageLocaleKey;
        mkUser.EmailEncodingKey=user[0].EmailEncodingKey;
	    mkUser.CommunityNickname='mkUser';
	    mkUser.MobilePhone='12345678912';
	    mkUser.UserRoleId = marRole.Id ;
	    mkUser.IsActive = true;
     	insert mkUser;
		
		
		//客户
		RecordType accrecordtype = [select Id from RecordType where DeveloperName='TW_Hospital' and SobjectType='Account' limit 1];
		Account acc1 = new Account();
		acc1.RecordTypeId = accrecordtype.Id;
        acc1.Name = 'T_医院1';
        insert new Account[] {acc1};
        
        /*联系人*/
		RecordType conrecordtype = [select Id from RecordType where DeveloperName = 'TW_Contact' and SobjectType='Contact' and IsActive = true limit 1 ];
        Contact ct1A = new Contact();
        ct1A.RecordTypeId = conrecordtype.Id;
        ct1A.AccountId = acc1.Id;
        ct1A.LastName = 'T_医院1_医生A';
        Contact ct1B = new Contact();
        ct1B.RecordTypeId = conrecordtype.Id;
        ct1B.AccountId = acc1.Id;
        ct1B.LastName = 'T_医院1_医生B';
        Contact ct1C = new Contact();
        ct1C.RecordTypeId = conrecordtype.Id;
        ct1C.AccountId = acc1.Id;
        ct1C.LastName = 'T_医院1_医生C';
        insert new Contact[]{ct1A, ct1B, ct1C};
		
		Campaign cp = new Campaign();
		cp.Name = 'T_CPA';
		cp.Status = 'Sign Up Closed';
		cp.IsActive = true;
		cp.StartDate = Date.today();
		cp.EndDate = Date.today().addDays(30);
		cp.OwnerId = mkUser.Id;
		insert cp;
		
		CampaignMember cmpA = new CampaignMember();
		cmpA.CampaignId = cp.Id;
		cmpA.ContactId = ct1A.Id;
		cmpA.User__c = repUser.Id;
		CampaignMember cmpB = new CampaignMember();
		cmpB.CampaignId = cp.Id;
		cmpB.ContactId = ct1B.Id;
		cmpB.User__c = repUser.Id;
		insert(new CampaignMember[] {cmpA, cmpB});
		
		CampaignMemberHistory__c newHis = new CampaignMemberHistory__c();
		newHis.CampaignId__c = cp.Id;
		newHis.User__c = repUser.Id;
		insert newHis;
		
		Campaign cp2 = new Campaign();
		cp2.Name = 'T_CPA';
		cp2.IsActive = true;
		cp2.StartDate = Date.today();
		cp2.EndDate = Date.today().addDays(30);
		cp2.OwnerId = mkUser.Id;
		insert cp2;
       
        test.startTest();
        ApexPages.currentPage().getParameters().put('camid', cp.id);
	    TW_CtrlModifyCampaignMember submitController0 = new TW_CtrlModifyCampaignMember();
        System.runAs(repUser)//开始测试提交审批
        {
        	ApexPages.currentPage().getParameters().put('camid', null);
	        TW_CtrlModifyCampaignMember submitController1 = new TW_CtrlModifyCampaignMember(); 
			ApexPages.currentPage().getParameters().put('camid', cp2.id);
	        TW_CtrlModifyCampaignMember submitController2 = new TW_CtrlModifyCampaignMember(); 
	        ApexPages.currentPage().getParameters().put('camid', cp.id);
	        TW_CtrlModifyCampaignMember submitController3 = new TW_CtrlModifyCampaignMember(); 
	        submitController3.ActionRowList[0].CampaignMemberHistory.Action__c = '删除';
	        submitController3.Add();
	        submitController3.TiggerRowId = submitController3.ActionRowList[submitController3.ActionRowList.size()-1].RowId;
	       
	        submitController3.CancelRow();
	        submitController3.Add();
	        submitController3.ActionRowList[submitController3.ActionRowList.size()-1].CampaignMemberHistory.ContactId__c = ct1A.Id;
	        submitController3.TiggerRowId = submitController3.ActionRowList[submitController3.ActionRowList.size()-1].RowId;
	        submitController3.CheckContact();
	        submitController3.Save();
	        
	        submitController3.Add();
	        submitController3.ActionRowList[submitController3.ActionRowList.size()-1].CampaignMemberHistory.ContactId__c = ct1B.Id;
	        submitController3.TiggerRowId = submitController3.ActionRowList[submitController3.ActionRowList.size()-1].RowId;
	        submitController3.CheckContact();
	        submitController3.Save();
	        
	        List<CampaignMemberHistory__c> newHisList = new List<CampaignMemberHistory__c>();
	        CampaignMemberHistory__c newHis2 = new CampaignMemberHistory__c();
			newHis2.Action__c = '新增';
			newHis2.User__c = repUser.Id;
			newHis2.CampaignId__c = submitController3.Campaign.Id;
			newHis2.ApproveStep__c = '提交';
			newHis2.ApproveUrl__c = URL.getSalesforceBaseUrl().toExternalForm() + '/apex/TW_ModifyCampaignMemberApprove?camid=' + submitController3.Campaign.Id;
			newHisList.add(newHis2);
			submitController3.Supervisor = supUser;
	        submitController3.SendEmailToApprover(newHisList);
	        
	        submitController3.Campaign.IsNotapprovalBylevel__c = false;
	        submitController3.Supervisor = submitController3.Campaign.Owner;
	        submitController3.SendEmailToApprover(newHisList);
	        submitController3.ReturnCampaign();
        }
        test.stopTest();  
    }

}