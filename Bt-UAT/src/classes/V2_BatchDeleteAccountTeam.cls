/* Function: 删除需要失效的客户小组成员
 * Author: Sunny
 * Date:2012-1-4
 * 删除客户小组成员、更新销售医院关系为历史状态、更新删除成员所拥有的业务机会的Owner
 */
global class V2_BatchDeleteAccountTeam implements Database.Batchable<sObject> 
{
	global final String strQuery;
	global V2_BatchDeleteAccountTeam(String strQ)
	{
		//'Select v.V2_IsSyn__c, v.V2_User__c, v.V2_Account__c, v.V2_Account__r.OwnerId, v.V2_History__c, v.V2_Delete_Year__c, v.V2_Delete_Month__c From V2_Account_Team__c v Where v.V2_Delete_Year__c = \''+strDelYear+'\' And v.V2_Delete_Month__c = \''+ strDelMonth +'\' And v.V2_History__c = false And v.V2_Is_Delete__c = true'
		this.strQuery = strQ ;
	}
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		return Database.getQueryLocator(strQuery);
	}
	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		Map<String,ID> map_AccTIds = new Map<String,ID>() ;
		Map<String,V2_Account_Team__c> map_accTeam = new Map<String,V2_Account_Team__c>() ;
		Set<ID> set_UserIds = new Set<ID>() ;
		Set<ID> set_AccIds = new Set<ID>() ;
		for(sObject sObj : scope)
		{
			V2_Account_Team__c objAccTeam = (V2_Account_Team__c)sObj ;
			if(objAccTeam.V2_Account__c == null)
			{
				continue ;
			}
			if(objAccTeam.V2_User__c == null)
			{
				continue ;
			}
			system.debug('ACCCCCTTTTT'+objAccTeam) ;
			if(objAccTeam.V2_BatchOperate__c == '替换' && objAccTeam.V2_NewAccUser__c != null)
			{//替换，构建原成员和新成员ID的MAP
				system.debug('Hereerererer '+objAccTeam.V2_User__c+''+objAccTeam.V2_Account__c) ;
				map_AccTIds.put(objAccTeam.V2_User__c+''+objAccTeam.V2_Account__c , objAccTeam.V2_NewAccUser__c) ;
			}else if(objAccTeam.V2_Account__r.OwnerId != null)
			{//其它，构建原成员和其Account的OwnerId的MAP
				map_AccTIds.put(objAccTeam.V2_User__c+''+objAccTeam.V2_Account__c , objAccTeam.V2_Account__r.OwnerId) ;
			}
			set_UserIds.add(objAccTeam.V2_User__c) ;
			set_AccIds.add(objAccTeam.V2_Account__c) ;
			map_accTeam.put(objAccTeam.V2_Account__c+''+objAccTeam.v2_User__c , objAccTeam) ;
		}
		if(set_UserIds.size() == 0)
		{
			return ;
		}
		if(map_accTeam.size() == 0)
		{
			return ;
		}
		//处理相应 业务机会的owner
		List<Opportunity> list_OppUp = new List<Opportunity>() ;
		for(Opportunity objOpp : [Select Id,OwnerId,AccountId From Opportunity Where OwnerId in: set_UserIds And AccountId in: set_AccIds])
		{
			system.debug('Opp Ke '+objOpp.OwnerId+''+objOpp.AccountId) ;
			if(map_AccTIds.containsKey(objOpp.OwnerId+''+objOpp.AccountId))
			{
				system.debug(objOpp.OwnerId+'--==--'+map_AccTIds.get(objOpp.OwnerId+''+objOpp.AccountId)+'==--=='+objOpp.OwnerId+''+objOpp.AccountId) ;
				objOpp.OwnerId = map_AccTIds.get(objOpp.OwnerId+''+objOpp.AccountId) ;
				list_OppUp.add(objOpp) ;
			}
		}
		system.debug('Opp Up '+list_OppUp) ;
		if(list_OppUp.size() != 0) update list_OppUp ;
		//删除客户小组、修改销售医院关系
		List<AccountTeamMember> list_AccTeamDel = new List<AccountTeamMember>() ;
		List<V2_Account_Team__c> list_AccTeamUp = new List<V2_Account_Team__c>() ;
		for(AccountTeamMember objAccTeamMem : [Select a.UserId, a.Id, a.AccountId From AccountTeamMember a Where a.AccountId in: set_AccIds And a.UserId in: set_UserIds])
		{
			if(map_accTeam.keySet().contains(objAccTeamMem.AccountId+''+objAccTeamMem.UserId))
			{
				V2_Account_Team__c objAccTeam = map_accTeam.get(objAccTeamMem.AccountId+''+objAccTeamMem.UserId) ;
				objAccTeam.V2_IsSyn__c = true ;
				objAccTeam.V2_History__c = true ;
				list_AccTeamUp.add(objAccTeam) ;
				list_AccTeamDel.add(objAccTeamMem) ;
			}
		}
		if(list_AccTeamDel.size() != 0)
		{
			delete list_AccTeamDel ;
		}
		if(list_AccTeamUp.size() != 0)
		{
			update list_AccTeamUp;
		}
		
	}
	global void finish(Database.BatchableContext BC)
	{
	}
}