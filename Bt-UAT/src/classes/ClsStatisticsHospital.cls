/**
 * 作者 : Sunny Sun
 * 说明 ：用于统计销售代表所负责的医院数量（SP医院数量）
 * 
**/
public class ClsStatisticsHospital {
	//统计销售代表所负责的医院数量
	//参数：1.需要统计负责医院数量的销售代表ID的List
	//2.需要统计的月份（日期类型，所要统计月份的第一天）
	//返回值：销售ID与其负责医院数量的MAP
    public Map<ID,Integer> StatisticsHospitalNum(List<ID> list_UserId , Date StartDate){
    	//Map 销售ID - 所负责医院数量
    	Map<ID,Integer> map_SalesHospitalNum = new Map<ID,Integer>();
    	//获取销售所负责的医院对应的MAP
    	Map<ID,List<ID>> map_SalesHospital = getSalesHospitalIdMap(list_UserId , StartDate);
    	//统计每个销售所负责的医院数量
    	for(ID SalesId : list_UserId){
    		if(map_SalesHospital.containsKey(SalesId)){
    			map_SalesHospitalNum.put(SalesId , map_SalesHospital.get(SalesId).size());
    		}
    	}
    	return map_SalesHospitalNum;
    }
    //统计销售代表所负责的SP医院数量
    //参数：1.需要统计负责医院数量的销售代表ID的List
    //2.需要统计的月份（日期类型，所要统计月份的第一天）
    //返回值：销售ID与其负责SP医院数量的MAP
    public Map<ID,Integer> StatisticsSPHospitalNum(List<ID> list_UserId , Date StartDate){
    	//Map 销售ID - 所负责SP医院数量
        Map<ID,Integer> map_SalesSPHospitalNum = new Map<ID,Integer>();
        //获取销售所负责的医院对应的MAP
        Map<ID,List<ID>> map_SalesHospital = getSalesHospitalIdMap(list_UserId , StartDate);
        Set<ID> set_HospitalIds = new Set<ID>();
        for(List<ID> list_hosId : map_SalesHospital.values()){
        	set_HospitalIds.addAll(list_hosId);
        }
        Set<ID> set_SpHospitalIds = new Set<ID>();
        for(Account objAcc : [Select a.Id, (Select Id From ACCHospitalInfo__r) From Account a Where Id in: set_HospitalIds]){
        	if(objAcc.ACCHospitalInfo__r!=null&&objAcc.ACCHospitalInfo__r.size()>0){
        		set_SpHospitalIds.add(objAcc.Id);
        	}
        }
        for(ID SalesId : map_SalesHospital.keySet()){
        	Integer i = 0 ;
        	for(ID HospitalId : map_SalesHospital.get(SalesId)){
        		if(set_SpHospitalIds.contains(HospitalId)){
        			i+=1;
        		}
        	}
        	map_SalesSPHospitalNum.put(SalesId , i);
        }
        return map_SalesSPHospitalNum;
    }
    private Map<ID,List<ID>> getSalesHospitalIdMap(List<ID> list_UserId , Date StartDate){
    	//Map 销售ID-{所负责医院ID,...}
        Map<ID,List<ID>> map_SalesHospital = new Map<ID,List<ID>>();
        //查找销售医院关系，条件：
        //1.非历史记录
        //2.新负责人字段为所要统计的代表
        //3.所要统计的月份在生效日期后（最后生效日期）
        for(V2_Account_Team__c AccTeam : [Select Id,V2_Account__c,V2_NewAccUser__c From V2_Account_Team__c 
                                           Where V2_History__c = false 
                                           And V2_NewAccUser__c in: list_UserId
                                           And V2_LastAccessDate__c <: StartDate]){
            if(map_SalesHospital.containsKey(AccTeam.V2_NewAccUser__c)){
                List<ID> list_Account = map_SalesHospital.get(AccTeam.V2_NewAccUser__c);
                list_Account.add(AccTeam.V2_Account__c);
                map_SalesHospital.put(AccTeam.V2_NewAccUser__c , list_Account);
            }else{
                List<ID> list_Account = new List<ID>();
                list_Account.add(AccTeam.V2_Account__c);
                map_SalesHospital.put(AccTeam.V2_NewAccUser__c , list_Account);
            }
        }
    	return map_SalesHospital;
    }
}