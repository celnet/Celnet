/*
作者: Tommy
创建时间: 2011-12-2
测试trigger:V2_Event_SetVisit
*/
@isTest
private class V2_Event_SetVisit_Test 
{
    static testMethod void TestSetVisit() 
    {
    	//----------------Create User-------------
    	List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
    	//-------------New Account-------------
    	List<Account> list_Acc = new List<Account>() ;
    	Account objAcc1 = new Account() ;
    	objAcc1.Name = 'AccNa' ;
    	list_Acc.add(objAcc1) ;
    	insert list_Acc ;
    	//-------------New Contact-------------
    	List<Contact> list_Contact = new List<Contact>() ;
    	Contact objContact1 = new Contact() ;
    	objContact1.LastName = 'lastna1' ;
    	objContact1.AccountId = objAcc1.Id ;
    	list_Contact.add(objContact1) ;
    	Contact objContact2 = new Contact() ;
    	objContact2.LastName = 'lastna1' ;
    	objContact2.AccountId = objAcc1.Id ;
    	list_Contact.add(objContact2) ;
    	insert list_Contact ;
    	
        //-------------New Event-------------
        List<Event> list_Event = new List<Event>() ;
        Event objEve1 = new Event() ;
        //objEve1.IsRecurrence = true ;
        objEve1.OwnerId = use1.Id ;
        objEve1.WhoId = objContact1.Id ;
        objEve1.StartDateTime = datetime.now() ;
        objEve1.EndDateTime = datetime.now().addHours(1) ;
        objEve1.SubjectType__c = '拜访' ;
        objEve1.V2_IsExpire__c = false ;
        list_Event.add(objEve1) ;
        insert list_Event ;
        
        objEve1.V2_IsExpire__c = true ;
        objEve1.Done__c = true ;
        update objEve1 ;
        
        
    }
}