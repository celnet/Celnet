/*
 * Author: Steven
 * Date: 2014-2-21
 * Description: Schedule
 */
global class Vablet_MatchBatchScheduler implements Schedulable{
	global void execute(SchedulableContext SC) {
    	Vablet_RepMatchBatch vrmb = new Vablet_RepMatchBatch();
    	Database.executeBatch(vrmb);
  	}
}