/*
Author：Tommy
Created on：2011-3-17
Description: 
SEP系统每周自动发送当月的所有拜访信息至“市场活动所有人”。
*/
public class CampaignVisitReportSchedule implements Schedulable
{
	//global
	public void execute(SchedulableContext SC) 
	{
		CampaignVisitReportBatch reportBatch = new CampaignVisitReportBatch();
		Database.executeBatch(reportBatch, 10);
	}
}