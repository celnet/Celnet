/**
添加联系人申请，根据记录类型，显示不同的页面；
harvery 2011.12.26 creat
scott:2011-12-30修改
2012-4-4修改：
1.原有的职务给MD用 联系人上新增 2职务 Bios 和Renal 用新的字段  ，MD部门职务为必填。
2.联系人上新增两个联系人类型：Bios：联系人类型（bios）、Md: ivt和is 用的联系人类型（IVT），acc或者sp 用的联系人类型（ACC）
2013-3-5
sunny:页面添加基本信息字段“GIS客户类型”
2013-4-7 
sunny:页面MD部分添加字段“静疗小组”
2013-9-7
bill:新增SP客户分级项目的联系的新增和修改，对应 的记录类型是SP新增和SP修改
新增一个区域SP，显示字段月全麻数量/台、月吸入比例（100%）、平均手术时长h、七氟烷浓度%、七氟烷FGF(L/min)、部门(SP)、职务(SP)
2014-2-12Sunny:Renal部门下HD和ACUTE的用户不需要显示Renal的权重等信息。
2014-3-11
Steven:Renal部门下GA用户不显示Renal权重, 不显示联系人类型字段, 联系人修改申请新增一个联系人类别字段, 仅显示给GA, 
*/

public  class V2_Contro_AddContact {
	//当前操作对象
	public Contact_Mod__c curContact{get;set;}
	//renal 
	public Boolean RenalIsShow{get;set;}
	//md
	public Boolean MDIsShow{get;set;}
	//Bios
	public Boolean BiosIsShow{get;set;} 
	//SP bill新增
	public Boolean SPIsShow{get;set;} 
	
	//是否是新增
	public Boolean IsInsert{get;set;}
	//是否是编辑
	public Boolean IsEdit{get;set;}
	//如果是编辑选择联系人
	public List<SelectOption> ContactOption {get;set;}
	//联系人Id
	public Id contactId {get;set;}
	ApexPages.StandardController con;
	//记录类型
    String RecordTypeName;
    public List<V2_ContactDefaultValue__c> lstDefaultValue{get;set;}
    //显示肾科用户权重
    public List<RenalUserRate__c> RenalUserRlist{get;set;}
	public Boolean IsLock{get;set;}
	//当前登录用户是否为ACUTE或HD用户
	public Boolean IsACUTEorHDuser{get{
		User cUser = [Select Id,UserRoleId,UserRole.Name From User Where Id =: UserInfo.getUserId()];
		if(cUser.UserRole.Name.contains('-ACUTE-') || cUser.UserRole.Name.contains('-HD-')){
			return true;
		}else{
			return false;
		}
	}set;}
	// 当前登录用户是否为GA用户
	public Boolean IsGAUser{
		get{
			User currentUser = [Select Id, UserRoleId, UserRole.Name From User Where Id =: UserInfo.getUserId()];
			if(currentUser.UserRole.Name.contains('-GA-')){
				return true;
			} else {
				return false;
			}
		}
		set;	
	}
	
	
	public V2_Contro_AddContact(ApexPages.StandardController p_con){
	 	try
	 	{
		 	//取得控制器，当前对象
		 	con=p_con;	
		 	curContact = (Contact_Mod__c)p_con.getRecord();
		 	
		 	//记录类型
		 	if(curContact.RecordTypeId == null )
		 	{
		 		IsLock =true;
		 		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning , '无法生成编辑页面，产生此问题的原因可能是您设置了默认记录类型；请点击 "您的名字-设置-我的个人信息-记录类型选项" ，并取消勾选 "联系人修改申请"。');            
			    ApexPages.addMessage(msg);
		  	    return;
		 	}
		 	else
		 	{
		 		RecordTypeName = getRecordTypeById(curContact.RecordTypeId);
		 	}
		 	//根据记录类型来判段所属部门 页面不同显示
		 	if(RecordTypeName =='V2_Renal' || RecordTypeName == 'V2_RenalInsert')
		 	{
		 		if(IsACUTEorHDuser || IsGAUser){
		 			RenalIsShow = false;
		 		}else{
		 			RenalIsShow = true;
		 		}
		 		MDIsShow = false; 
		 		BiosIsShow = false;
		 		SPIsShow = false;
		 	}
		 	else if(RecordTypeName =='V2_MD' || RecordTypeName =='V2_MDInsert')
		 	{
		 		RenalIsShow = false;
		 		MDIsShow = true; 
		 		BiosIsShow = false;
		 		SPIsShow = false;
		 	}
		 	else if(RecordTypeName =='V2_Bios' || RecordTypeName =='V2_BiosInsert')
		 	{
		 		RenalIsShow = false;
		 		MDIsShow = false; 
		 		BiosIsShow = true;
		 		SPIsShow = false;
		 	}
		 	else if(RecordTypeName =='SP_Insert' || RecordTypeName =='SP_Update')//bill新增
		 	{
		 		RenalIsShow = false;
		 		MDIsShow = false; 
		 		BiosIsShow = false;
		 		SPIsShow = true;
		 	}
		 	//修改申请上编辑按钮操作
		 	if(curContact.Id != null)
		 	{
		 		curContact=[select c.ElectrostaticTherapy__c,c.Account__c,GISContactType__c,c.V2_interest__c,c.Birthday__c,c.Comment__c, c.Contact_Type__c, c.CreatedById, c.CreatedDate, c.Department_Type__c, c.Email__c, c.Fax__c, c.GAState__c, c.Gender__c, c.Graduate_College__c, c.Id, c.ID_card2__c, c.Mobile__c, c.Name, c.Name__c, c.NewContact__c, c.OwnerId, c.Phone__c, c.RecordTypeId, c.RenalMarketContactApp__c, c.Status__c, c.Title__c, c.Type__c, c.V2_BIOS_Target_Contact_Score__c, c.V2_CampaignType__c, c.V2_ContactTypeByRENAL__c, c.V2_Education__c, c.V2_Level__c, c.V2_OfficerNo__c, c.V2_PassPortNo__c, c.V2_Position_in_the_Society__c, c.V2_RateBaxBusiness__c, c.V2_RateBaxRelationship__c, c.V2_RateBeds__c, c.V2_RateExperience__c, c.V2_RateGeology__c, c.V2_RateHospGrade__c, c.V2_RateLeadership__c, c.V2_RatePatientDeliver__c, c.V2_RatePDCenter__c, c.V2_RatePDImpPatients__c, c.V2_RatePDPatients__c, c.V2_RateTech__c, c.V2_RateTitle__c, c.V2_Relationship_with_Baxter__c, c.V2_RenalGrade__c, c.V2_Status_of_Academy__c, c.V2_TotalScore__c
		 					, c.SP_inhaleMonthyNum__c, c.SP_SevoFGF__c, c.SP_SevoConcentration__c, c.SP_OperationTime__c, c.SP_Job__c, c.SP_Department__c, c.SP_AnesthesiaMonthyNum__c,
		 					c.SP_DesConcentration__c , c.SP_DesFGF__c
		 					from Contact_Mod__c c where Id=:curContact.Id];
		 		if(curContact.Status__c!='新建'&&curContact.Status__c!='拒绝')
			 	{
			 			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error , '记录锁定，记录正在审批中或已审批通过不能编辑！');            
					    ApexPages.addMessage(msg);
		  	 			IsLock =true;
		  	 			return;
			 	}
			 	if(RecordTypeName.contains('Insert'))
			 	{
			 		IsInsert = true;
		 			IsEdit = false;
			 	}
		 		if(RenalIsShow)
		 		{
		 			getRenalInfo();
		 		}
		 		return;
		 	}
		 	/**************2012-1-18修改*********************/
		 	if(curContact.Name__c ==null && curContact.Account__c == null )
		 	{
		 		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, '您只能在客户或联系人下新建联系人修改申请!');            
			    ApexPages.addMessage(msg);
			    IsLock =true;
  	 			return;
		 	}
		 	
		 	
		 	//判断是否为新增 页面不同显示
		 	if(RecordTypeName.contains('Insert'))
		 	{
		 		IsInsert = true;
		 		IsEdit = false;
		 		curContact.Type__c='新增';
		 		if(RenalIsShow)
		 		{
		 			getRenalInfo();
		 		}
		 		//联系人上新增
		 		if(curContact.Name__c !=null)
		 		{
		 			Contact con = [select AccountId from Contact where Id=:curContact.Name__c];
		 			curContact.Account__c= con.AccountId;
		 		}
		 	}
		 	else
		 	{
		 		
		 		IsInsert = false;
		 		curContact.Type__c='编辑';
		 		//客户上新建编辑记录类型
		 		if(curContact.Name__c ==null)
		 		{
		 			IsEdit = true;
			 		ContactOption = new List<SelectOption>();
			 		List<Contact> conlist = [select Id,Name from Contact where AccountId =:curContact.Account__c];
			 		if(conlist !=null && conlist.size()>0)
			 		{
			 			for(Contact con: conlist)
					 	{
					 		ContactOption.add(new SelectOption(con.Id,con.Name));
					 	}
					 	//默认显示第一个联系人信息
					 	Contact con = getContact(conlist[0].Id);
			 			SetContactPage(con,curContact);
			 			if(RenalIsShow)
					 	{
					 		getRenalInfo();
					 	}
			 		}
			 		else
			 		{
			 			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning , '当前用户下没有联系人信息！');            
				    	ApexPages.addMessage(msg);
				    	IsLock =true;
	  	 				return;
			 		}
				 	
		 		}
		 		//联系人上新建按钮
		 		else if(curContact.Name__c !=null)
		 		{
		 			Contact con = getContact(curContact.Name__c);
		 			SetContactPage(con,curContact);
		 			if(RenalIsShow)
				 	{
				 		getRenalInfo();
				 	}
		 		}
		 		//自身新建按钮
		 		else 
		 		{
		 			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning , '您不能在此新建数据！');            
				    ApexPages.addMessage(msg);
	  	 			return;
		 		}
		 	}
	 	}catch(Exception e)
	 	{
	 		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR , String.valueOf(e)+' 第'+e.getLineNumber()+'行');            
            ApexPages.addMessage(msg);
            return;
	 	}
	 	
	}
	
	//获取所选联系人的信息
	public void getContactInfo()
	{
	 	if(RenalIsShow)
	 	{
	 		getRenalInfo();
	 	}
	 	Contact con = getContact(contactId);
	 	SetContactPage(con,curContact);
	}
	
	//获得Renal部门信息
	
	public void getRenalInfo()
	{
		/////////////////1.获取所有默认设置
    	lstDefaultValue= [Select v.Id, v.Name, v.OwnerId, 
    					  v.SystemModstamp, v.V2_ContactType__c, 
				    	  v.V2_Education__c, v.V2_RateBaxBusiness__c, 
				    	  v.V2_RateBaxRelationship__c, v.V2_RateBeds__c, 
				    	  v.V2_RateExperience__c, v.V2_RateGeology__c, 
				    	  v.V2_RateHospGrade__c, v.V2_RateLeadership__c, 
				    	  v.V2_RatePatientDeliver__c, v.V2_RatePDCenter__c, 
				    	  v.V2_RatePDImpPatients__c, v.V2_RatePDPatients__c, 
				    	  v.V2_RateTech__c, v.V2_RateTitle__c, 
				    	  v.V2_TotalScore__c from V2_ContactDefaultValue__c v
				     	  where v.V2_ContactType__c in ('医生','护士','行政')
    					];
    	if(lstDefaultValue==null || lstDefaultValue.size()==0 ||lstDefaultValue.size()!=3){
    		 ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning , '联系人默认权重取得失败，请联系管理员。错误：数据条数不等于3！');            
             ApexPages.addMessage(msg);
             return;
    	}
    	//分别得到医生，护士，行政的设置；
    	V2_ContactDefaultValue__c DefaultValue1;
    	V2_ContactDefaultValue__c DefaultValue2;
    	V2_ContactDefaultValue__c DefaultValue3;
    	
    	for(V2_ContactDefaultValue__c temp : lstDefaultValue){
    		if(temp.V2_ContactType__c=='医生'){
    			 DefaultValue1 = temp;
    		} else if(temp.V2_ContactType__c=='护士'){
    			 DefaultValue2 = temp;
    		} else if (temp.V2_ContactType__c=='行政'){
    			 DefaultValue3 = temp;
    		}
    	}
    	//判断是否获取到医生，护士，行政的设置；
    	if(DefaultValue1==null){
    		 ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning , '联系人默认权重取得失败，请联系管理员。错误：未获取到医生的默认值设置！');            
             ApexPages.addMessage(msg);
             return ;
    	}
    	if(DefaultValue2==null){
    		 ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning , '联系人默认权重取得失败，请联系管理员。错误：未获取到护士的默认值设置！');            
             ApexPages.addMessage(msg);
             return  ;
    	}
     	if(DefaultValue3==null){
    		 ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning , '联系人默认权重取得失败，请联系管理员。错误：未获取到行政的默认值设置！');            
             ApexPages.addMessage(msg);
             return ;
    	}
		///////////////////2.获取用户设置
		RenalUserRlist = [select ContactType__c,HospGrade__c,Geology__c,Leadership__c,Title__c,Education__c,BaxRelationship__c,
						  PatientDeliver__c,PDPatients__c,Experience__c,PDImpPatients__c,Beds__c,BaxBusiness__c,PDCenter__c,
						  PDTech__c,Total_score__c,V2_Totalscore__c from RenalUserRate__c where UserSalesRep__c =: Userinfo.getUserId()];
    	if(RenalUserRlist == null || RenalUserRlist.size()==0)
    	{
    		RenalUserRlist = new List<RenalUserRate__c>();
    		for(V2_ContactDefaultValue__c cdv:lstDefaultValue)
    		{
    			RenalUserRate__c rurate = new RenalUserRate__c();
    			rurate.ContactType__c = cdv.V2_ContactType__c;
    			rurate.HospGrade__c= cdv.V2_RateHospGrade__c;
    			rurate.Geology__c= cdv.V2_RateGeology__c;
    			rurate.Leadership__c= cdv.V2_RateLeadership__c; 
    			rurate.Title__c= cdv.V2_RateTitle__c;
    			rurate.Education__c= cdv.V2_Education__c;
    			rurate.BaxRelationship__c= cdv.V2_RateBaxRelationship__c;
    			rurate.PatientDeliver__c= cdv.V2_RatePatientDeliver__c;
    			rurate.PDPatients__c= cdv.V2_RatePDPatients__c;
    			rurate.Experience__c= cdv.V2_RateExperience__c;
    			rurate.PDImpPatients__c= cdv.V2_RatePDImpPatients__c;
    			rurate.Beds__c = cdv.V2_RateBeds__c;
    			rurate.BaxBusiness__c= cdv.V2_RateBaxBusiness__c;
    			rurate.PDCenter__c= cdv.V2_RatePDCenter__c;
    			rurate.PDTech__c= cdv.V2_RateTech__c;
    			rurate.V2_Totalscore__c = cdv.V2_TotalScore__c;
    			RenalUserRlist.add(rurate);
    		}
    	}
	}
	
	//获取联系人信息
	 public Contact getContact(Id id){
	 	if(id == null){
	 		return null;
	 	}  
    	Contact contact = [Select c.V2_RenalDepartmentType__c,GISContactType__c,c.V2_BiosDepartmentType__c,c.V2_ContactTypeByIVT__c,c.V2_ContactTypeByACC__c,c.V2_RenalTitle__c,c.V2_BiosTitle__c,c.AccountId, c.AssistantName, c.AssistantPhone, c.Birthdate, c.Cities__c, c.ContactType__c, c.CreatedDate, c.Department, c.DepartmentType__c, c.Description, c.Email, c.Fax, c.FirstName, c.GAContactType__c, c.GAState__c, c.Gender__c, c.GraduateCollege__c, c.HasOptedOutOfEmail, c.HomePhone, c.Id, c.ID_card__c, c.info__c, c.interest__c, c.IsDeleted, c.LastActivityDate, c.LastCURequestDate, c.LastCUUpdateDate, c.LastName, c.LeadSource, c.MailingCity, c.MailingCountry, c.MailingPostalCode, c.MailingState, c.MailingStreet, c.MobilePhone, c.Name, c.OtherCity, c.OtherCountry, c.OtherPhone, c.OtherPostalCode, c.OtherState, c.OtherStreet, c.OwnerId, c.Phone, c.position__c, c.Provinces__c, c.ReportsToId, c.Salutation, c.Title, c.V2_BIOS_Target_Contact_Score__c, c.V2_CallSequence__c, c.V2_CampaignType__c, c.V2_ContactTypeByRENAL__c, c.V2_Education__c, c.V2_Level__c, c.V2_OfficerNo__c, c.V2_PassPortNo__c, c.V2_Position_in_the_Society__c, c.V2_RateBaxBusiness__c, c.V2_RateBaxRelationship__c, c.V2_RateBeds__c, c.V2_RateExperience__c, c.V2_RateGeology__c, c.V2_RateHospGrade__c, c.V2_RateLeadership__c, c.V2_RatePatientDeliver__c, c.V2_RatePDCenter__c, c.V2_RatePDImpPatients__c, c.V2_RatePDPatients__c, c.V2_RateTech__c, c.V2_RateTitle__c, c.V2_Relationship_with_Baxter__c, c.V2_RenalGrade__c, c.V2_RenalScore__c, c.V2_RenalSocialStype__c, c.V2_Status_of_Academy__c, c.V2_TotalScore__c 
							, c.SP_inhaleMonthyNum__c, c.SP_SevoFGF__c, c.SP_SevoConcentration__c, c.SP_OperationTime__c, c.SP_Job__c, c.SP_Department__c, c.SP_AnesthesiaMonthyNum__c,
		 					c.SP_DesConcentration__c , c.SP_DesFGF__c from Contact c where Id =: id];
    	return contact;
    }
	
	
	//设置页面联系人信息
	 public void SetContactPage(Contact contact,Contact_Mod__c curContact){
    	try
    	{
    		if(contact==null)return;
	    	/////////////////基本信息；
	    	//联系人
	    	system.debug(RecordTypeName + 'KKKKKKKKK');
	    	if(!RecordTypeName.contains('Insert'))
			{
	    		curContact.Name__c = contact.Id;
			}
	    	//客户
	    	curContact.Account__c = contact.AccountId;
			//姓名
			curContact.NewContact__c = contact.Name;
			
			//联系人类别
			curContact.Contact_Type__c = contact.ContactType__c;
			
			//GA联系人类别
			curContact.GA_Contact_Type__c = contact.ContactType__c;
			
			
			//部门
			//职务
			//联系人类型
			if(RecordTypeName.contains('Renal'))
			{
				curContact.Title__c= contact.V2_RenalTitle__c;
				//部门
				curContact.Department_Type__c = contact.V2_RenalDepartmentType__c;
			}
			else if(RecordTypeName.contains('Bios'))
			{
				//职务
				curContact.Title__c= contact.V2_BiosTitle__c;
				//类型
				curContact.V2_ContactTypeByRENAL__c = contact.V2_ContactTypeByRENAL__c;
				//部门
				curContact.Department_Type__c = contact.V2_BiosDepartmentType__c;
			}
			else if(RecordTypeName.contains('MD'))
			{
				//部门
				curContact.Department_Type__c = contact.DepartmentType__c;
				//职务
				curContact.Title__c= contact.Title;
				//类型
				Profile p = [select Name from Profile where id =:UserInfo.getProfileId()];
		    	String profileName = p.Name.toUpperCase();
		    	//Md: ivt和is 用的联系人类型（IVT）
		    	if(profileName.Contains('IVT') || profileName.Contains('IS'))
		    	{
		    		curContact.V2_ContactTypeByRENAL__c = contact.V2_ContactTypeByIVT__c;
		    	}
		    	//acc或者sp 用的联系人类型（ACC）
		    	else
		    	{
		    		curContact.V2_ContactTypeByRENAL__c = contact.V2_ContactTypeByACC__c;
		    	}
			}
			//性别
			curContact.Gender__c=contact.Gender__c;
			//生日
			curContact.Birthday__c=contact.Birthdate;
			//id card
			curContact.ID_card2__c=contact.ID_card__c;
			//护照
			curContact.V2_PassPortNo__c=contact.V2_PassPortNo__c;
			//军官
			curContact.V2_OfficerNo__c=contact.V2_OfficerNo__c;
			//phone
			curContact.Phone__c=contact.Phone;
			//
			curContact.Mobile__c=contact.MobilePhone;
			curContact.Fax__c=contact.Fax;
			curContact.Email__c=contact.Email;
			curContact.Graduate_College__c=contact.GraduateCollege__c;
			//备注		
			curContact.Comment__c=contact.Description;
			//兴趣爱好
			curContact.V2_interest__c = contact.interest__c;
	    	/////////////////其他信息；
		    curContact.V2_RenalGrade__c= contact.V2_RenalGrade__c;
		    curContact.V2_CampaignType__c= contact.V2_CampaignType__c;
		    curContact.V2_TotalScore__c= contact.V2_TotalScore__c;
		    curContact.V2_RateHospGrade__c= contact.V2_RateHospGrade__c;
		    curContact.V2_RateGeology__c= contact.V2_RateGeology__c;
		    curContact.V2_RateLeadership__c= contact.V2_RateLeadership__c; 
		    curContact.V2_RateTitle__c= contact.V2_RateTitle__c;
		    curContact.V2_Education__c= contact.V2_Education__c;
		    curContact.V2_RateBaxRelationship__c= contact.V2_RateBaxRelationship__c;
		    curContact.V2_RatePatientDeliver__c= contact.V2_RatePatientDeliver__c;
		    curContact.V2_RatePDPatients__c= contact.V2_RatePDPatients__c;
		    curContact.V2_RateExperience__c= contact.V2_RateExperience__c;
		    curContact.V2_RatePDImpPatients__c= contact.V2_RatePDImpPatients__c;
		    curContact.V2_RateBeds__c = contact.V2_RateBeds__c;
		    curContact.V2_RateBaxBusiness__c= contact.V2_RateBaxBusiness__c;
		    curContact.V2_RatePDCenter__c= contact.V2_RatePDCenter__c;
		    curContact.V2_RateTech__c= contact.V2_RateTech__c;
		    curContact.GISContactType__c = contact.GISContactType__c;
		    //SP客户分级 bill添加
		    if(RecordTypeName.contains('SP'))
			{
			    //月吸入比例%
				curContact.SP_InhaleMonthyNum__c = contact.SP_inhaleMonthyNum__c;
				//七氟烷FGF(L/min)
				curContact.SP_SevoFGF__c = contact.SP_SevoFGF__c;
				//七氟烷浓度%
				curContact.SP_SevoConcentration__c = contact.SP_SevoConcentration__c;
				//平均手术时长h
				curContact.SP_OperationTime__c = contact.SP_OperationTime__c;
				//职务(SP)
				curContact.SP_Job__c = contact.SP_Job__c;
				//部门(SP)
				curContact.SP_Department__c = contact.SP_Department__c;
				//月全麻数量/台
				curContact.SP_AnesthesiaMonthyNum__c = contact.SP_AnesthesiaMonthyNum__c;
				//地氟烷浓度%
				curContact.SP_DesConcentration__c = contact.SP_DesConcentration__c;
				//地氟烷FGF(L/min)
				curContact.SP_DesFGF__c = contact.SP_DesFGF__c;
			}
		    /*//Md bIOS
		    //联系人类型
		    if(RecordTypeName.contains('Bios'))
		    {
		    	curContact.V2_ContactTypeByRENAL__c = contact.V2_ContactTypeByRENAL__c;
		    }
		    else if(RecordTypeName.contains('MD'))
		    {
		    	Profile p = [select Name from Profile where id =:UserInfo.getProfileId()];
		    	String profileName = p.Name.toUpperCase();
		    	//Md: ivt和is 用的联系人类型（IVT）
		    	if(profileName.Contains('IVT') || profileName.Contains('IS'))
		    	{
		    		curContact.V2_ContactTypeByRENAL__c = contact.V2_ContactTypeByIVT__c;
		    	}
		    	//acc或者sp 用的联系人类型（ACC）
		    	else
		    	{
		    		curContact.V2_ContactTypeByRENAL__c = contact.V2_ContactTypeByACC__c;
		    	}
		    }*/
		   	//联系人级别
		    curContact.V2_Level__c = contact.V2_Level__c ;
		    //对百特产品支持程度
		    curContact.V2_Relationship_with_Baxter__c=contact.V2_Relationship_with_Baxter__c;
		    //社会职务
		    curContact.V2_Position_in_the_Society__c=contact.V2_Position_in_the_Society__c;
		    //学术影响力
		    curContact.V2_Status_of_Academy__c=contact.V2_Status_of_Academy__c;
    	}catch(Exception e)
    	{
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning , e.getMessage());            
         	ApexPages.addMessage(msg);
         	return;
    	}
    	
	    
    } 
  
    /**
          根据记录类型id找到developername
    */
    public  String getRecordTypeById(String id){
    	String ret=null;
    	    List<RecordType> rts = [SELECT Id,DeveloperName  FROM RecordType WHERE SObjectType = 'Contact_Mod__c' and IsActive=true];
            if ( rts != null && rts.size() > 0 ) {
                for ( RecordType rt : rts ){
                	String reId = rt.Id;
                    if(reId.contains(id)){
                    	ret = rt.DeveloperName;
                    	break;
                    }
                }
            }
    	
    	return ret;
    }
    
    /*
     *保存
    */
    public PageReference SaveContactMod() 
    {
    	Try{
	    	//检查姓名是否符合要求
	    	if(curContact.NewContact__c != null && (curContact.NewContact__c.contains('院长') || curContact.NewContact__c.contains('医生') || curContact.NewContact__c.contains('护士')
	    	  || curContact.NewContact__c.contains('科长') || curContact.NewContact__c.contains('主任') || curContact.NewContact__c.contains('老师')
	    	  || curContact.NewContact__c.contains('护士长') || curContact.NewContact__c.contains('先生') || curContact.NewContact__c.contains('小姐')
	    	  || curContact.NewContact__c.contains('大夫') || curContact.NewContact__c.contains('护长') || curContact.NewContact__c.contains('药师')))
	    	{
	    		curContact.NewContact__c.addError('您填写的姓名不符合要求，姓名中不能包含：院长、医生、护士、科长、主任、老师、护士长、先生、小姐、大夫、护长、药师，这类称谓词！)');
	    		return null;
	    	}
	    	//检查是否重名
	    	Integer flag = 0;
	    	for(Contact con: [select Id,Name from Contact where AccountId =: curContact.Account__c])
	    	{
	    		if(curContact.Name__c != null && con.Id == curContact.Name__c)
	    		{
	    			continue;
	    		}
	    		if(curContact.NewContact__c == con.Name)
	    		{
	    			flag++;
	    		}
	    	}
	    	if(flag >0)
			{
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning , '当前客户下已经存在联系人：'+curContact.NewContact__c+'，请核实后再操作！');            
	         	ApexPages.addMessage(msg);
	         	return null;
			}
			//检查Id card是否重复
			if(curContact.ID_card2__c != null && curContact.ID_card2__c !='')
	    	{
	    		Integer flag2 = 0;
	    		for(Contact con:[select Name,Id from Contact where ID_card__c =: curContact.ID_card2__c])
	    		{
	    			if(curContact.Name__c != null && con.Id == curContact.Name__c)
	    			{
	    				continue;
	    			}
	    			flag2++;
	    		}
				if(flag2>0)
				{    
					curContact.ID_card2__c.addError('ID card重复，请您重新填写！');
		         	return null;
				}
	    	}
	    	//SP字段的页面值验证
	    	if(RecordTypeName.contains('SP'))
		    {
		    	if(curContact.SP_InhaleMonthyNum__c > 100)
		    	{
		    		curContact.SP_InhaleMonthyNum__c.addError('月吸入比例%不能大于100%');
		    		return null;
		    	}
		    	if(curContact.SP_SevoConcentration__c > 10)
		    	{
		    		curContact.SP_SevoConcentration__c.addError('七氟烷浓度%不能大于10%');
		    		return null;
		    	}
		    	if(curContact.SP_DesConcentration__c > 100)
		    	{
		    		curContact.SP_DesConcentration__c.addError('地氟烷浓度%不能大于100%');
		    		return null;
		    	}
		    }
			//System.debug('####################################################################################'+TotalScore);
	        //curContact.V2_TotalScore__c = double.valueOf(TotalScore);
	        if(curContact.Id == null)
	        {
	        	insert curContact;
	        }
	        else
	        {
	        	update curContact;
	        }
	        
	        //自动提交审批
	        //提交审批
			Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
	        req.setObjectId(curContact.id);//客户联系人或自定义对象
	       	Approval.ProcessResult result = Approval.process(req);
        
    	}Catch(Exception ex){
           	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning , ex.getMessage());            
         	ApexPages.addMessage(msg);
         	return null;
           	
        }
        	PageReference pageRef = new PageReference(System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+curContact.Id);
        	return pageRef;
    }
    
    /****************************************************Test*********************************/
    static testMethod void V2_Contro_AddContact()
    {
    	/*客户*/
		RecordType accrecordtype = [select Id from RecordType where Name='医院' and SobjectType='Account' limit 1];
		Account acc = new Account();
		acc.RecordTypeId = accrecordtype.Id;
		acc.Name = 'AccTest';
		insert acc;
		/*联系人*/
		RecordType conrecordtype = [select Id from RecordType where Name = 'Renal' and SobjectType='Contact' and IsActive = true limit 1 ];
		Contact con1 = new Contact();
		con1.LastName = 'AccTestContact1';
		con1.AccountId=acc.Id;
		insert con1;
		
		Contact con2 = new Contact();
		con2.LastName = 'AccTestContact2';
		con2.AccountId=acc.Id;
		insert con2;
		
		//V2_ContactDefaultValue__c
		List<V2_ContactDefaultValue__c> list_cdv = new List<V2_ContactDefaultValue__c>();
		V2_ContactDefaultValue__c contactType = new V2_ContactDefaultValue__c();
		contactType.V2_ContactType__c = '医生';
		list_cdv.add(contactType);
		V2_ContactDefaultValue__c contactType1 = new V2_ContactDefaultValue__c();
        contactType1.V2_ContactType__c = '护士';
        list_cdv.add(contactType1);
        V2_ContactDefaultValue__c contactType2 = new V2_ContactDefaultValue__c();
        contactType2.V2_ContactType__c = '行政';
        list_cdv.add(contactType2);
		insert list_cdv;
		
		
		//联系人修改申请记录类型
    	List<RecordType> rtlist = [select Id,DeveloperName from RecordType where SObjectType = 'Contact_Mod__c' and IsActive=true ];
    	/*联系人修改申请上 直接新建*/
    	Contact_Mod__c cm = new Contact_Mod__c();
    	for(RecordType rt:rtlist)
    	{
    		if(rt.DeveloperName =='V2_RenalInsert')
    		{
    			cm.RecordTypeId  = rt.Id;
    		}
    	}
    	cm.Name__c = null;
    	cm.Account__c = null; 
    	
    	ApexPages.StandardController STcon = new ApexPages.StandardController(cm);
    	try
    	{
    		V2_Contro_AddContact cac = new V2_Contro_AddContact(STcon);
    	}catch(Exception e)
    	{
    		System.debug('联系人修改申请上直接新建报错提醒'+String.valueOf(e));
    	}
    	/*联系人修改申请上  状态为新建或拒绝 则记录锁定*/
    	Contact_Mod__c cm1 = new Contact_Mod__c();
    	for(RecordType rt:rtlist)
    	{
    		if(rt.DeveloperName =='V2_RenalInsert')
    		{
    			cm1.RecordTypeId  = rt.Id;
    		}
    	}
    	cm1.Status__c = '审批中';
    	insert cm1;
    	ApexPages.StandardController STcon1 = new ApexPages.StandardController(cm1);
    	try
    	{
    		V2_Contro_AddContact cac1 = new V2_Contro_AddContact(STcon1);
    	}catch(Exception e)
    	{
    		System.debug('记录审批中不能编辑'+String.valueOf(e));
    	}
    	V2_Contro_AddContact cac1 = new V2_Contro_AddContact(STcon1);
    	/*联系人修改申请   编辑按钮*/
    	Contact_Mod__c cm2 = new Contact_Mod__c();
    	for(RecordType rt:rtlist)
    	{
    		if(rt.DeveloperName =='V2_RenalInsert')
    		{
    			cm2.RecordTypeId  = rt.Id;
    		}
    	}
    	cm2.Status__c = '新建';
    	insert cm2;
    	ApexPages.StandardController STcon2 = new ApexPages.StandardController(cm2);
    	V2_Contro_AddContact cac2 = new V2_Contro_AddContact(STcon2);
    	Test.startTest();
    	/*客户上新建联系人修改申请    ’新增‘*/
    	//Renal
    	Contact_Mod__c cminsertRenalAcc = new Contact_Mod__c();
    	for(RecordType rt:rtlist)
    	{
    		if(rt.DeveloperName =='V2_RenalInsert')
    		{
    			cminsertRenalAcc.RecordTypeId  = rt.Id;
    		}
    	}
    	cminsertRenalAcc.Account__c = acc.Id;
    	ApexPages.StandardController STconRenalAcc = new ApexPages.StandardController(cminsertRenalAcc);
    	V2_Contro_AddContact cacRenalAcc = new V2_Contro_AddContact(STconRenalAcc);
    	cacRenalAcc.SaveContactMod();
    	
    	//Md
    	Contact_Mod__c cminsertMdAcc = new Contact_Mod__c();
    	for(RecordType rt:rtlist)
    	{
    		if(rt.DeveloperName =='V2_MDInsert')
    		{
    			cminsertMdAcc.RecordTypeId  = rt.Id;
    		}
    	}
    	cminsertMdAcc.Account__c = acc.Id;
    	ApexPages.StandardController STconMdAcc = new ApexPages.StandardController(cminsertMdAcc);
    	V2_Contro_AddContact cacMdAcc = new V2_Contro_AddContact(STconMdAcc);
    	cacMdAcc.SaveContactMod();
    	
    	//Bios
    	Contact_Mod__c cminsertBiosAcc = new Contact_Mod__c();
    	for(RecordType rt:rtlist)
    	{
    		if(rt.DeveloperName =='V2_BiosInsert')
    		{
    			cminsertBiosAcc.RecordTypeId  = rt.Id;
    		}
    	}
    	cminsertBiosAcc.Account__c = acc.Id;
    	
    	ApexPages.StandardController STconBiosAcc = new ApexPages.StandardController(cminsertBiosAcc);
    	V2_Contro_AddContact cacBiosAcc = new V2_Contro_AddContact(STconBiosAcc);
    	cacBiosAcc.SaveContactMod();
    	
    	/*联系人上新建修改申请  ‘新增’*/
    	//Renal
    	Contact_Mod__c cminsertaRenalCon = new Contact_Mod__c();
    	for(RecordType rt:rtlist)
    	{
    		if(rt.DeveloperName =='V2_RenalInsert')
    		{
    			cminsertaRenalCon.RecordTypeId  = rt.Id;
    		}
    	}
    	cminsertaRenalCon.Name__c = con1.Id;
    	ApexPages.StandardController STconRenalCon = new ApexPages.StandardController(cminsertaRenalCon);
    	V2_Contro_AddContact cacRenalCon = new V2_Contro_AddContact(STconRenalCon);
    	cacRenalCon.SaveContactMod();
    	//Md
    	Contact_Mod__c cminsertaMdCon = new Contact_Mod__c();
    	for(RecordType rt:rtlist)
    	{
    		if(rt.DeveloperName =='V2_MdInsert')
    		{
    			cminsertaRenalCon.RecordTypeId  = rt.Id;
    		}
    	}
    	cminsertaMdCon.Name__c = con1.Id;
    	ApexPages.StandardController STconMdCon = new ApexPages.StandardController(cminsertaMdCon);
    	V2_Contro_AddContact cacMdCon = new V2_Contro_AddContact(STconMdCon);
    	cacMdCon.SaveContactMod();
    	//SP
    	/*Contact_Mod__c cminsertSPCon = new Contact_Mod__c();
    	for(RecordType rt:rtlist)
    	{
    		if(rt.DeveloperName =='SP_Insert')
    		{
    			cminsertSPCon.RecordTypeId  = rt.Id;
    		}
    	}
    	cminsertaMdCon.Name__c = con1.Id;
    	ApexPages.StandardController STconSPCon = new ApexPages.StandardController(cminsertSPCon);
    	V2_Contro_AddContact cacSPCon = new V2_Contro_AddContact(STconSPCon);
    	cacSPCon.SaveContactMod();
    	*/
    	//Bios
    	Contact_Mod__c cminsertaBiosCon = new Contact_Mod__c();
    	for(RecordType rt:rtlist)
    	{
    		if(rt.DeveloperName =='V2_BiosInsert')
    		{
    			cminsertaBiosCon.RecordTypeId  = rt.Id;
    		}
    	}
    	cminsertaBiosCon.Name__c = con1.Id;
    	ApexPages.StandardController STconBiosCon = new ApexPages.StandardController(cminsertaBiosCon);
    	V2_Contro_AddContact cacBiosCon = new V2_Contro_AddContact(STconBiosCon);
    	cacBiosCon.SaveContactMod();
    	/*联系人上 新建联系人修改申请  ‘编辑’*/
    	Contact_Mod__c cmeditRenalCon = new Contact_Mod__c ();
    	for(RecordType rt:rtlist)
    	{
    		if(rt.DeveloperName =='V2_Renal')
    		{
    			cmeditRenalCon.RecordTypeId  = rt.Id;
    		}
    	}
    	cmeditRenalCon.Name__c = con1.Id;
    	ApexPages.StandardController STconRenalEditCon = new ApexPages.StandardController(cmeditRenalCon);
    	V2_Contro_AddContact cacRenalEditCon = new V2_Contro_AddContact(STconRenalEditCon);
    	cacRenalEditCon.SaveContactMod();
    	/*联系人上 新建联系人修改申请  ‘编辑’*/
    	/*Contact_Mod__c cmeditSPCon = new Contact_Mod__c ();
    	for(RecordType rt:rtlist)
    	{
    		if(rt.DeveloperName =='SP_Update')
    		{
    			cmeditSPCon.RecordTypeId  = rt.Id;
    		}
    	}
    	cmeditSPCon.Name__c = con1.Id;
    	ApexPages.StandardController STconSPEditCon = new ApexPages.StandardController(cmeditSPCon);
    	V2_Contro_AddContact cacSPEditCon = new V2_Contro_AddContact(STconSPEditCon);
    	cacSPEditCon.SaveContactMod();
    	*/
    	Test.stopTest();
    	/*客户上新建联系人修改申请  ‘编辑’*/
    	Contact_Mod__c cmeditRenalAcc = new Contact_Mod__c ();
    	for(RecordType rt:rtlist)
    	{
    		if(rt.DeveloperName =='V2_Renal')
    		{
    			cmeditRenalAcc.RecordTypeId  = rt.Id;
    		}
    	}
    	cmeditRenalAcc.Account__c = acc.Id;
    	ApexPages.StandardController STconRenalEditAcc = new ApexPages.StandardController(cmeditRenalAcc);
    	V2_Contro_AddContact cacRenalEditAcc = new V2_Contro_AddContact(STconRenalEditAcc);
    	cacRenalEditAcc.getContactInfo();
    	//cacRenalEditAcc.flag = '测试flag';
    	PageReference p = cacRenalEditAcc.SaveContactMod();
    }
    
        /****************************************************Test*********************************/
    static testMethod void V2_Contro_AddContact2()
    {
    	/*客户*/
		RecordType accrecordtype = [select Id from RecordType where Name='医院' and SobjectType='Account' limit 1];
		Account acc = new Account();
		acc.RecordTypeId = accrecordtype.Id;
		acc.Name = 'AccTest';
		insert acc;
		/*联系人*/
		RecordType conrecordtype = [select Id from RecordType where Name = 'Renal' and SobjectType='Contact' and IsActive = true limit 1 ];
		Contact con1 = new Contact();
		con1.LastName = 'AccTestContact1';
		con1.AccountId=acc.Id;
		insert con1;
		
		Contact con2 = new Contact();
		con2.LastName = 'AccTestContact2';
		con2.AccountId=acc.Id;
		insert con2;
		
		//V2_ContactDefaultValue__c
		List<V2_ContactDefaultValue__c> list_cdv = new List<V2_ContactDefaultValue__c>();
		V2_ContactDefaultValue__c contactType = new V2_ContactDefaultValue__c();
		contactType.V2_ContactType__c = '医生';
		list_cdv.add(contactType);
		V2_ContactDefaultValue__c contactType1 = new V2_ContactDefaultValue__c();
        contactType1.V2_ContactType__c = '护士';
        list_cdv.add(contactType1);
        V2_ContactDefaultValue__c contactType2 = new V2_ContactDefaultValue__c();
        contactType2.V2_ContactType__c = '行政';
        list_cdv.add(contactType2);
		insert list_cdv;
		//联系人修改申请记录类型
    	List<RecordType> rtlist = [select Id,DeveloperName from RecordType where SObjectType = 'Contact_Mod__c' and IsActive=true ];
		
		system.Test.startTest();
		//SP
    	Contact_Mod__c cminsertSPCon = new Contact_Mod__c();
    	for(RecordType rt:rtlist)
    	{
    		if(rt.DeveloperName =='SP_Insert')
    		{
    			cminsertSPCon.RecordTypeId  = rt.Id;
    		}
    	}
    	cminsertSPCon.Name__c = con1.Id;
    	ApexPages.StandardController STconSPCon = new ApexPages.StandardController(cminsertSPCon);
    	V2_Contro_AddContact cacSPCon = new V2_Contro_AddContact(STconSPCon);
    	cacSPCon.SaveContactMod();
    	
    	/*联系人上 新建联系人修改申请  ‘编辑’*/
    	Contact_Mod__c cmeditSPCon = new Contact_Mod__c ();
    	for(RecordType rt:rtlist)
    	{
    		if(rt.DeveloperName =='SP_Update')
    		{
    			cmeditSPCon.RecordTypeId  = rt.Id;
    		}
    	}
    	cmeditSPCon.Name__c = con1.Id;
    	ApexPages.StandardController STconSPEditCon = new ApexPages.StandardController(cmeditSPCon);
    	V2_Contro_AddContact cacSPEditCon = new V2_Contro_AddContact(STconSPEditCon);
    	cacSPEditCon.SaveContactMod();
    	system.Test.stopTest();
    }
}