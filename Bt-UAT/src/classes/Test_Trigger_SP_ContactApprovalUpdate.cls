/*
Author：Dean Lv
Created on：2013-09-10
Description: 
trigger测试
被测试trigger名:SP_ContactApprovalUpdate
1.当创建SP联系人修改时，自动把联系人的月全麻数量/台、月吸入比例（100%）、平均手术时长h
七氟烷浓度%、七氟烷FGF(L/min)同步到联系人修改申请对应字段上来
2.当SP联系人修改申请被审批通过后要把修改的信息同步到联系人信息上去
*/
@isTest
private class Test_Trigger_SP_ContactApprovalUpdate {

    static testMethod void myUnitTest() {
	//记录类型ID

	Account acc1 = new Account();
    acc1.Name = '第一医院';
    insert acc1;
	Contact con = new Contact();
	
	con.LastName = '张先生';
	con.AccountId = acc1.Id;
	con.RecordTypeId = [Select r.Id, r.DeveloperName From RecordType r 
    where r.SobjectType = 'Contact' and IsActive = true and r.DeveloperName ='SP'][0].Id;
	insert con;
	Contact con2 = new Contact();
	con2.RecordTypeId = [Select r.Id, r.DeveloperName From RecordType r 
    where r.SobjectType = 'Contact' and IsActive = true and r.DeveloperName ='SP'][0].Id;
	con2.LastName = '张先生';
	con2.AccountId = acc1.Id;
	insert con2;
	
	Contact_Mod__c conmod = new Contact_Mod__c();
	conmod.Name__c=con.Id;
	conmod.NewContact__c='刘先生';
	conmod.RecordTypeId = [Select r.Id, r.DeveloperName From RecordType r 
    where r.SobjectType = 'Contact_Mod__c' and IsActive = true and r.DeveloperName in ('SP_Update')][0].Id;
    insert conmod;
    
    Contact_Mod__c conmod1 = new Contact_Mod__c();
	conmod1.Name__c=con.Id;
	conmod1.NewContact__c='刘先生';
	conmod1.RecordTypeId = [Select r.Id, r.DeveloperName From RecordType r 
    where r.SobjectType = 'Contact_Mod__c' and IsActive = true and r.DeveloperName in ('SP_Insert')][0].Id;
	insert conmod1;
	
	system.Test.startTest();
	conmod.Name__c=con2.Id;
	conmod.Status__c='通过';
	conmod.Account__c = acc1.Id;
	update conmod;
	
	
	conmod1.Name__c=con2.Id;
	conmod1.Status__c='通过';
	conmod1.Account__c = acc1.Id;
	update conmod1;
    system.Test.stopTest();      	
	
    }
}