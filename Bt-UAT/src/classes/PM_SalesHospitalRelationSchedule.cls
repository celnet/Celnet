/**
 * Author : bill
 * Date：2013-11-9
 * PM_SalesHospitalRelationBatch的Schedule
 **/
public class PM_SalesHospitalRelationSchedule implements Schedulable
{
    public void execute(SchedulableContext sc) {
        PM_SalesHospitalRelationBatch Batch = new PM_SalesHospitalRelationBatch();
        database.executeBatch(Batch,10);
    }
    
    static testMethod void myUnitTest() {       
        system.test.startTest();
        string sch=DateTime.now().second()+' '+DateTime.now().minute()+' '+DateTime.now().hour()+' * * ?';
        PM_SalesHospitalRelationSchedule Schedule = new PM_SalesHospitalRelationSchedule();
        System.schedule('test', sch , Schedule);
        system.test.stopTest();
    }
}