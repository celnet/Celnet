/*
 *Author：Scott
 *Created on：2010-10-15
 *Description: 
 *参考大陆功能
 * Tommy 修改在2013-10-29 ：去掉对TW_RateHospGrade字段更新，此字段类型被修改为公式字段由客户自动带出
*/
public without sharing class TW_CtrAddContact {
    //Account Id
    public String AccountId;
    //Page 1
    public Boolean IsPage1{get;set;}
    //Page 2
    public Boolean IsPage2{get;set;}
    //RecordTypeDeveloperName
    public String RtDevName{get;set;}
    public Boolean IsLock{get;set;}
    public Boolean IsEdit{get;set;}
    public Contact_Mod__c curContact{get;set;}
    //记录类型
    public list<SelectOption> getRecordTypelist()
    {
        List<String>RecordTypelist = new List<String>();
        list<SelectOption> options=new list<SelectOption>();
        options.add(new SelectOption('TW_New','新增'));
        options.add(new SelectOption('TW_Update','修改'));
        return options;
    }
    //如果是编辑选择联系人
    public List<SelectOption> ContactOption {get;set;}
    //联系人Id
    public Id contactId {get;set;}
    public TW_CtrAddContact(ApexPages.StandardController controller)
    {
        IsPage1 = true;
        IsPage2 = false;
        IsLock = false;
        IsEdit = false;
        
        curContact = (Contact_Mod__c)controller.getRecord();
        //修改申请上编辑按钮操作
        String curContactId = ApexPages.currentPage().getParameters().get('conid');
        if( curContactId!= null)
        {
            curContact=[select 	TW_Title__c,TW_DoctorStatusDetail__c,TW_RateBaxterProduct__c,TW_ApprovalStatus__c,Type__c ,Name__c,RecordType.DeveloperName,OwnerId,NewContact__c,Account__c ,Phone__c,Mobile__c,Email__c ,SP_Job__c ,TW_Department__c ,
                        Birthday__c ,Comment__c,Gender__c,V2_Education__c,V2_ContactTypeByRENAL__c ,TW_Position__c ,V2_Level__c ,TW_RateBaxBusiness__c,
                        V2_interest__c,V2_RateLeadership__c,V2_TotalScore__c ,V2_Relationship_with_Baxter__c, TW_Nutrition__c ,
                        TW_INfusor__c,TW_NutritionType__c,TW_INfusorType__c,TW_Outpatient__c,TW_AverageInpatients__c ,TW_PrescriptionPotential__c,
                        TW_DoctorStatus__c from Contact_Mod__c c where Id=:curContactId];
            if(curContact.TW_ApprovalStatus__c!='未提交'&&curContact.TW_ApprovalStatus__c!='拒絕')
            {
                    IsPage1 = false;
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error , '記錄鎖定，記錄正在審批中或已審批通過不能編輯！');            
                    ApexPages.addMessage(msg);
                    IsLock =true;
                    return;
            }
            IsPage2 = true;
            IsPage1 = false;
            return;
        }
        else
        {
            AccountId = ApexPages.currentPage().getParameters().get('accid');
            curContact.Account__c= AccountId;
            curContact.TW_ApprovalStatus__c = '未提交';
        }
        
        if(AccountId ==null || AccountId=='')
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning , '無法生成編輯頁面，請重新操作。');            
            ApexPages.addMessage(msg);
            return;
        }
        
        
    }
    //点击继续按钮
    public void PageContinue()
    {
        IsPage2 = true;
        IsPage1 = false;
        
        if(RtDevName =='TW_New')
        {
            //联系人上新增
            curContact.Type__c = '新增';
            curContact.RecordTypeId = getRecordTypeById(RtDevName);
        }
        else if(RtDevName =='TW_Update')
        {
            curContact.Type__c='修改';
            curContact.RecordTypeId = getRecordTypeById(RtDevName);
            IsEdit = true;
            //客户上新建编辑记录类型
            if(curContact.Name__c ==null)
            {
                ContactOption = new List<SelectOption>();
                List<Contact> conlist = [select Id,Name from Contact where AccountId =:AccountId];
                if(conlist !=null && conlist.size()>0)
                {
                    for(Contact con: conlist)
                    {
                        ContactOption.add(new SelectOption(con.Id,con.Name));
                    }
                    //默认显示第一个联系人信息
                    Contact con = getContact(conlist[0].Id);
                    SetContactPage(con,curContact);
                }
                else
                {
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning , '當前Account下沒有聯繫人信息！');            
                    ApexPages.addMessage(msg);
                    IsLock =true;
                    return;
                }
            }
        }
        else
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning , '無法生成編輯頁面，記錄類型選擇錯誤。');            
            ApexPages.addMessage(msg);
            IsLock =true;
            return;
        }
        
    }
    /*
    *获取联系人信息
    */
     public Contact getContact(Id id)
        
     {
        if(id == null)
        {
            return null;
        }  
        Contact contact = [Select Id,Name,AccountId,Phone,MobilePhone,Email,Gender__c,position__c,Birthdate,Title,TW_Department__c,TW_DoctorStatus__c,
							interest__c,TW_DoctorStatusDetail__c,Description,V2_ContactTypeByRENAL__c,V2_Level__c,
							TW_RateBaxterProduct__c,TW_RateBaxBusiness__c,TW_Outpatient__c,TW_AverageInpatients__c,V2_Education__c,
							V2_RateLeadership__c,TW_PrescriptionPotential__c,V2_TotalScore__c,TW_INfusor__c,
							TW_INfusorType__c,TW_Nutrition__c,TW_NutritionType__c from Contact c where Id =: id];
        return contact;
     }
     /*
      *获取客户下的联系人列表信息
     */
    public void getContactInfo() 
    {
        Contact con = getContact(contactId);
        SetContactPage(con,curContact);
    }
     /*
     *设置页面联系人信息
     */
     public void SetContactPage(Contact contact,Contact_Mod__c curContact){
        try
        {
            if(contact==null)return;
            /////////////////基本信息；
            //联系人
            curContact.Name__c = contact.Id;
            //姓名
            curContact.NewContact__c = contact.Name;
            //客户
            curContact.Account__c = contact.AccountId;
            //电话
            curContact.Phone__c = contact.Phone;
            //手机
            curContact.Mobile__c = contact.MobilePhone;
            //电子邮件
            curContact.Email__c = contact.Email;
            //性别
            //Gender
            curContact.Gender__c = contact.Gender__c;
            //學會職務
            curContact.TW_Position__c =contact.position__c;
            //出生日期
            curContact.Birthday__c = contact.Birthdate;
            //職稱
            curContact.TW_Title__c = contact.Title;
            //所属科别
            curContact.TW_Department__c = contact.TW_Department__c;
            //醫師狀態
            curContact.TW_DoctorStatus__c = contact.TW_DoctorStatus__c;
            //兴趣爱好
            curContact.V2_interest__c = contact.interest__c;
            //醫師狀態/異動
            curContact.TW_DoctorStatusDetail__c = contact.TW_DoctorStatusDetail__c;
            //备注
            curContact.Comment__c = contact.Description;
            
            /////////////評估資訊
            
            //联系人类型（BIOS）
            curContact.V2_ContactTypeByRENAL__c = contact.V2_ContactTypeByRENAL__c;
            //联系人分级
            curContact.V2_Level__c = contact.V2_Level__c;
            //對百特產品支持程度評分(1~5分)
            curContact.TW_RateBaxterProduct__c = contact.TW_RateBaxterProduct__c;
            //對產品使用/接受分級
            curContact.TW_RateBaxBusiness__c = contact.TW_RateBaxBusiness__c;
            //每週平均門診量
            curContact.TW_Outpatient__c = contact.TW_Outpatient__c;
            //每週平均住院病人數
            curContact.TW_AverageInpatients__c = contact.TW_AverageInpatients__c;
            //学术程度
            curContact.V2_Education__c = contact.V2_Education__c;
            //影响力
            curContact.V2_RateLeadership__c = contact.V2_RateLeadership__c;
            //處方潛力
            curContact.TW_PrescriptionPotential__c = contact.TW_PrescriptionPotential__c;
            //医生总分
            curContact.V2_TotalScore__c = contact.V2_TotalScore__c;
            
            ////////////////////市場細分資訊
            //INfusor
            curContact.TW_INfusor__c = contact.TW_INfusor__c;
            //INfusor市场细分
            curContact.TW_INfusorType__c = contact.TW_INfusorType__c;
            //Nutrition
            curContact.TW_Nutrition__c = contact.TW_Nutrition__c;
            //Nutrition市场细分
            curContact.TW_NutritionType__c = contact.TW_NutritionType__c;
            
        }catch(Exception e)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning , e.getMessage());            
            ApexPages.addMessage(msg);
            return;
        }
    } 
     /*
     *保存
    */
    public PageReference SaveContactMod() 
    {
        Try{
            //检查姓名是否符合要求
            if(curContact.NewContact__c != null && (curContact.NewContact__c.contains('院長') || curContact.NewContact__c.contains('醫生') || curContact.NewContact__c.contains('護士')
              || curContact.NewContact__c.contains('科長') || curContact.NewContact__c.contains('主任') || curContact.NewContact__c.contains('老師')
              || curContact.NewContact__c.contains('護士長') || curContact.NewContact__c.contains('先生') || curContact.NewContact__c.contains('小姐')
              || curContact.NewContact__c.contains('大夫') || curContact.NewContact__c.contains('護長') || curContact.NewContact__c.contains('藥師')))
            {
                curContact.NewContact__c.addError('您填寫的姓名不符合要求，姓名中不能包含：院長、醫生、護士、科長、主任、老師、護士長、先生、小姐、大夫、護長、藥師，這類稱謂詞！)');
                return null;
            }
            //检查是否重名
            Integer flag = 0;
            for(Contact con: [select Id,Name from Contact where AccountId =: AccountId])
            {
                if(curContact.Name__c != null && con.Id == curContact.Name__c)
                {
                    continue;
                }
                if(curContact.NewContact__c == con.Name)
                {
                    flag++;
                }
            }
            if(flag >0)
            {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning , '當前客戶下已經存在聯繫人：'+curContact.NewContact__c+'，請核實後再操作！');            
                ApexPages.addMessage(msg);
                return null;
            }
            if(curContact.Id == null)
            {
                insert curContact;
            }
            else
            {
                update curContact;
            }
            
            //自动提交审批
            //提交审批
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            req.setObjectId(curContact.id);//客户联系人或自定义对象
            Approval.ProcessResult result = Approval.process(req);
        
        }Catch(Exception ex){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning , ex.getMessage());            
            ApexPages.addMessage(msg);
            return null;
            
        }
            PageReference pageRef = new PageReference(System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+curContact.Id);
            return pageRef;
    }
    /*
     *返回
    */
    public PageReference Cancel()
    {
    	PageReference pageRef = new PageReference(System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+AccountId);
        return pageRef;
    }
    /*
    *根据记录类型developername找到id
    */
    public Id getRecordTypeById(String DevName){
        Id ret=null;
        List<RecordType> rts = [SELECT Id,DeveloperName  FROM RecordType WHERE SObjectType = 'Contact_Mod__c' and IsActive=true and DeveloperName=:DevName];
        if ( rts != null && rts.size() > 0 ) 
        {
             ret = rts[0].Id;
        }
        return ret;
    }
    /****************************************************Test*********************************/
    static testMethod void TW_CtrAddContact()
    {
        /*客户*/
        RecordType accrecordtype = [select Id from RecordType where DeveloperName='TW_Hospital' and SobjectType='Account' limit 1];
        Account acc = new Account();
        acc.RecordTypeId = accrecordtype.Id;
        acc.Name = 'AccTest';
        insert acc;
        /*联系人*/
        RecordType conrecordtype = [select Id from RecordType where DeveloperName = 'TW_Contact' and SobjectType='Contact' and IsActive = true limit 1 ];
        Contact con1 = new Contact();
        con1.RecordTypeId = conrecordtype.Id;
        con1.LastName = 'AccTestContact1';
        con1.AccountId=acc.Id;
        insert con1;
        
        
        //联系人修改申请记录类型
        List<RecordType> rtlist = [select Id,DeveloperName from RecordType where SObjectType = 'Contact_Mod__c' 
                                    and (DeveloperName='TW_New' or DeveloperName='TW_Update') and IsActive=true ];
        //联系人修改申请
        Contact_Mod__c cminsertCon = new Contact_Mod__c();
        cminsertCon.RecordTypeId = rtlist[0].Id;
        insert cminsertCon;
        
        system.Test.startTest();
        ApexPages.currentPage().getParameters().put('conid',cminsertCon.Id);
        ApexPages.StandardController STconSPCon1 = new ApexPages.StandardController( new Contact_Mod__c());
        TW_CtrAddContact cacSPCon1 = new TW_CtrAddContact(STconSPCon1);
        cacSPCon1.getRecordTypelist();
        cminsertCon.TW_ApprovalStatus__c='未提交';
        update cminsertCon;
        TW_CtrAddContact cacSPCon11 = new TW_CtrAddContact(STconSPCon1);
        
        
        ApexPages.currentPage().getParameters().put('conid',null);
        ApexPages.currentPage().getParameters().put('accid',null);
        TW_CtrAddContact cacSPCon21 = new TW_CtrAddContact(STconSPCon1);
        
        ApexPages.currentPage().getParameters().put('accid',acc.Id);
        TW_CtrAddContact cacSPCon2 = new TW_CtrAddContact(STconSPCon1);
        cacSPCon2.RtDevName ='TW_New';
        cacSPCon2.PageContinue();
        cacSPCon2.SaveContactMod();
        cacSPCon2.curContact.NewContact__c = 'AccTestContact1';
        cacSPCon2.SaveContactMod();
        
        TW_CtrAddContact cacSPCon3 = new TW_CtrAddContact(STconSPCon1);
        cacSPCon2.curContact.NewContact__c = '222';
        cacSPCon3.RtDevName ='TW_Update';
        cacSPCon3.PageContinue();
        cacSPCon3.SaveContactMod();
        cacSPCon3.RtDevName ='TW_fffUpdate';
        cacSPCon3.PageContinue();
        
        system.Test.stopTest();
        
    }
}