/**
 * Schedulable ClsCreateOppHistoryEveryMonthBatch
 * 
 */
global class ClsCreateOppHistoryEveryMonthBatchSch implements Schedulable {
    global void execute(SchedulableContext sc) {
        ClsCreateOppHistoryEveryMonthBatch checkBatch = new ClsCreateOppHistoryEveryMonthBatch();
        database.executeBatch(checkBatch);
    }
    
    static testMethod void test(){
        List<RecordType> list_RecordType = new List<RecordType>([select Id,Name from RecordType where (DeveloperName='IVT_Approval' or DeveloperName='IVT') And SobjectType = 'Opportunity']);
        Opportunity opp = new Opportunity();
        opp.Name='test';
        if(list_RecordType.size() > 0){
        	opp.RecordTypeId=list_RecordType[0].Id;
        }
        opp.CloseDate = date.today().addYears(1);
        opp.StageName='搜集信息';
        insert opp;
        system.test.startTest();
        string sch=DateTime.now().second()+' '+DateTime.now().minute()+' '+DateTime.now().hour()+' * * ?';
        ClsCreateOppHistoryEveryMonthBatchSch cp = new ClsCreateOppHistoryEveryMonthBatchSch();
        System.schedule('test',sch , cp);
        system.test.stopTest();
    }
}