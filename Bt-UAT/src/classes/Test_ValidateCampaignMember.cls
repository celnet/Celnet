/*
*开发人：Tommy Liu
*服务提供商：Cideatech
*时间：2013-3-11
*测试 模块：市场活动成员导入验证规则
*测试组件Trigger ValidateCampaignMember 
*/
@isTest
private class Test_ValidateCampaignMember 
{
	public static User CreateUserForTest(String lastName, String firstName, String alias)
	{
		List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey, Email, MobilePhone from User where id =: UserInfo.getUserId()];
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username=firstName + 'bacd@123.com';
    	use1.LastName=lastName;
    	use1.FirstName=firstName;
    	use1.Email=user[0].Email;
    	use1.Alias=alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname=firstName + 'abc';
    	use1.MobilePhone=user[0].MobilePhone;
    	use1.IsActive = true;
    	list_User.add(use1) ;
    	insert list_User;
		return use1;
	}
    static testMethod void myUnitTest() 
    {
        //准备导入相关数据 
        Provinces__c province = new Provinces__c();
        province.Name = '上海';
        insert province;
        Cities__c city = new Cities__c();
        city.Name = '上海';
        city.BelongToProvince__c = province.Id;
        insert city;
        
        Account acc1 = new Account();
        acc1.Name = 'T_医院1';
        acc1.Cities__c = city.Id;
        acc1.Provinces__c = province.Id;
        Account acc2 = new Account();
        acc2.Name = 'T_医院2';
        acc2.Cities__c = city.Id;
        acc2.Provinces__c = province.Id;
        insert new Account[] {acc1, acc2};
        
        Contact ct1A = new Contact();
        ct1A.AccountId = acc1.Id;
        ct1A.LastName = 'T_医院1_医生A';
        Contact ct1B = new Contact();
        ct1B.AccountId = acc1.Id;
        ct1B.LastName = 'T_医院1_医生B';
        Contact ct1C = new Contact();
        ct1C.AccountId = acc1.Id;
        ct1C.LastName = 'T_医院1_医生C';
        Contact ct1C_d = new Contact();
        ct1C_d.AccountId = acc1.Id;
        ct1C_d.LastName = 'T_医院1_医生C';
        Contact ct1D = new Contact();
        ct1D.AccountId = acc1.Id;
        ct1D.LastName = 'T_医生D';
        Contact ct2A = new Contact();
        ct2A.AccountId = acc2.Id;
        ct2A.LastName = 'T_医院2_医生A';
        Contact ct2D = new Contact();
        ct2D.AccountId = acc2.Id;
        ct2D.LastName = 'T_医生D';
        insert new Contact[]{ct1A, ct1B, ct1C, ct1C_d, ct1D, ct2A, ct2D};
        
        User salesA = CreateUserForTest('T', '_代表A', 'T_代表A'); //T _代表A
        User salesB = CreateUserForTest('T', '_代表B', 'T_代表B'); //T _代表B
        Date thisDay = Date.today();
        V2_Account_Team__c accTeam = new V2_Account_Team__c();
		accTeam.V2_User__c = salesA.Id;//代表A
		accTeam.V2_Account__c = acc1.Id;//医院1
		accTeam.V2_BatchOperate__c = '新增';
		accTeam.V2_History__c = false;
		accTeam.V2_ApprovalStatus__c = '审批通过';
		accTeam.V2_Effective_Year__c = '2012';//thisDay.year().format();
		accTeam.V2_Effective_Month__c = '1';//thisDay.month().format();
		V2_Account_Team__c accTeam2 = new V2_Account_Team__c();
		accTeam2.V2_User__c = salesA.Id;//代表A
		accTeam2.V2_Account__c = acc2.Id;//医院2
		accTeam2.V2_BatchOperate__c = '新增';
		accTeam2.V2_History__c = false;
		accTeam2.V2_ApprovalStatus__c = '审批通过';
		accTeam2.V2_Effective_Year__c = '2012';//thisDay.year() + '';
		accTeam2.V2_Effective_Month__c = '1';//thisDay.month() + '';
		insert(new V2_Account_Team__c[] {accTeam, accTeam2});
		
		Campaign cpA = new Campaign();
		cpA.Name = 'T_CPA';
		cpA.IsActive = true;
		cpA.StartDate = Date.today();
		cpA.EndDate = Date.today().addDays(30);
		insert(cpA);
		
        //准备导入数据
        List<CampaignMember> cpmList = new List<CampaignMember>();
        CampaignMember cpm_allNoSpecified = new CampaignMember();
        cpm_allNoSpecified.CampaignAid__c = null;
        cpm_allNoSpecified.ContactAid__c = null;
        cpm_allNoSpecified.UseAid__c = null;
        cpmList.add(cpm_allNoSpecified);
        CampaignMember cpm_dupInBatch = new CampaignMember();
        cpm_dupInBatch.CampaignAid__c = 'T_CPA';
        cpm_dupInBatch.ContactAid__c = 'T_医院1_医生A';
        cpm_dupInBatch.UseAid__c = 'T_代表B';
        cpmList.add(cpm_dupInBatch);
        CampaignMember cpm_dupInBatch_d = new CampaignMember();
        cpm_dupInBatch_d.CampaignAid__c = 'T_CPA';
        cpm_dupInBatch_d.ContactAid__c = 'T_医院1_医生A';
        cpm_dupInBatch_d.UseAid__c = 'T_代表B';
        cpmList.add(cpm_dupInBatch_d);
        CampaignMember cpm_allNoExist = new CampaignMember();
        cpm_allNoExist.CampaignAid__c = 'T_市场活动12abc';
        cpm_allNoExist.ContactAid__c = 'T_医生12abc';
        cpm_allNoExist.UseAid__c = 'T_代表12abc';
        cpmList.add(cpm_allNoExist);
        CampaignMember cpm_noRelated= new CampaignMember();
        cpm_noRelated.CampaignAid__c = 'T_CPA';
        cpm_noRelated.ContactAid__c = 'T_医院1_医生A';
        cpm_noRelated.UseAid__c = 'T_代表B';
        cpmList.add(cpm_noRelated);
        CampaignMember cpm_noRelatedSameAccDupCt= new CampaignMember();
        cpm_noRelatedSameAccDupCt.CampaignAid__c = 'T_CPA';
        cpm_noRelatedSameAccDupCt.ContactAid__c = 'T_医院1_医生C';
        cpm_noRelatedSameAccDupCt.UseAid__c = 'T_代表A';
        cpmList.add(cpm_noRelatedSameAccDupCt);
        CampaignMember cpm_noRelatedDiffAccDupCt= new CampaignMember();
        cpm_noRelatedDiffAccDupCt.CampaignAid__c = 'T_CPA';
        cpm_noRelatedDiffAccDupCt.ContactAid__c = 'T_医生D';
        cpm_noRelatedDiffAccDupCt.UseAid__c = 'T_代表A';
        cpmList.add(cpm_noRelatedDiffAccDupCt);
        CampaignMember cpm_suc1= new CampaignMember();
        cpm_suc1.CampaignAid__c = 'T_CPA';
        cpm_suc1.ContactAid__c = 'T_医院1_医生B';
        cpm_suc1.UseAid__c = 'T_代表A';
        cpmList.add(cpm_suc1);
        CampaignMember cpm_suc2= new CampaignMember();
        cpm_suc2.CampaignAid__c = 'T_CPA';
        cpm_suc2.ContactAid__c = 'T_医院2_医生A';
        cpm_suc2.UseAid__c = 'T_代表A';
        cpmList.add(cpm_suc2);
        
        //开始测试
        test.startTest();
        Integer sucCount = 0;//成功数
        Integer failedCount = 0;
        Database.SaveResult[] reList = Database.insert(cpmList, false);//allOrNone=false,允许部分成功
        Integer i = 0;
        for(Database.SaveResult re: reList)
        {
        	i++;
        	for(Database.Error error : re.getErrors())
        	{
        		System.debug(error.getMessage());
        		System.debug('###:' + i);
        	}
        	
        	if(re.isSuccess())
        	{
        		sucCount ++;
        	}
        	else
        	{
        		failedCount ++;
        	}
        }
        test.stopTest();
        
        //验证测试
        System.assertEquals(sucCount, 2);
        System.assertEquals(failedCount, 7);
    }
}