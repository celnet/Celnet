/*
Author：Scott
Created on：2011-1-6
Description: 
1.得到Renal部门用户奖金计算所涉及到的所有参数信息
2013-12-12 Comment By Tobe 事件字段删除：注释CommentsBySupervisor2__c字段引用
*/
public class V2_RenalBonusDataService {
  
  //当前年月拜访
  public List<Event> List_Event{get;set;}
  //业务机会数
  public Set<Id> Set_Oppids = new Set<Id>();
  //主管协访 完成数
  Double AssistanceFrequency = 0;
  //主管协访评语
  Double CommentsBySupervisor = 0;
  //非进行中的业务机会状态
  //Renal&Bios：休眠、产品培训使用(HD/CRRT)、客户合作失败、签约/缔结(成功)、
  Set<String> Set_Stage = new Set<String>{'休眠','产品培训使用(HD/CRRT)','客户合作失败','签约/缔结(成功)'};
  //总计划数
  Double TotalCallRecords = 0;
  
  //得到用户奖金计算涉及到的参数信息
  public  V2_RenalBonusDataService(Id userid,Integer year,Integer month)
  {
    List<MonthlyPlan__c> mp = [Select Id,V2_TotalCallRecords__c From MonthlyPlan__c where Year__c=:String.valueOf(year) and Month__c =:String.valueOf(month) and OwnerId =: userid limit 1];
    if(mp != null && mp.size()>0)
    {
      if( mp[0].V2_TotalCallRecords__c != null)
      {
        TotalCallRecords = mp[0].V2_TotalCallRecords__c;
      }
    }
    
    //拜访事件
    Date CurrentDate = Date.newInstance(year,month,1);
    DateTime CurrentStartMonth = DateTime.newInstance(CurrentDate.toStartOfMonth(),Time.newInstance(00, 00, 00, 00));
    DateTime CurrentEndMonth = DateTime.newInstance(CurrentDate.toStartOfMonth().addMonths(1),Time.newInstance(00, 00, 00, 00));
    List_Event = [select Done__c,WhatId,SubjectType__c,GAPlan__c,GAExecuteResult__c,WhoId,RecordType.DeveloperName from Event where V2_IsExpire__c = false
            	  and IsRecurrence != true and StartDateTime>=:CurrentStartMonth
                  and StartDateTime <:CurrentEndMonth and OwnerId =: userid];
    //业务机会
    //当月或者之前创建的，结束日期在本月或者以后的
    Date Enddate = CurrentDate.addMonths(1);
    for(Opportunity opp:[select Id from Opportunity where StageName not in:Set_Stage and (Renal_Opp_MGT__c = null or Renal_Opp_MGT__c ='拒绝')
                         and CreatedDate<:Enddate and CloseDate >=: CurrentDate and OwnerId =:userid])
    {
      Set_Oppids.add(opp.Id);
    }
    //主管协访
    //协访计数的逻辑由“必须接受”，改为：接受 OR 填写评语1 OR 填写评语2
    /*2013-12-12 Comment By Tobe
    for(EventAttendee  ea:[Select Event.CommentsBySupervisor__c,Event.CommentsBySupervisor2__c From EventAttendee where AttendeeId =:userid and Event.StartDateTime >=:CurrentStartMonth
                 and Event.StartDateTime <:CurrentEndMonth and Event.Done__c = true 
                 and EventId != null and Event.V2_IsExpire__c =false and Event.SubjectType__c=:'拜访' 
                 and (Status='Accepted' or Event.CommentsBySupervisor__c != null or Event.CommentsBySupervisor2__c != null)])
    {
      AssistanceFrequency++;
      if((ea.Event.CommentsBySupervisor__c != null && ea.Event.CommentsBySupervisor__c != '') || (ea.Event.CommentsBySupervisor2__c != null && ea.Event.CommentsBySupervisor2__c != ''))
      {
        CommentsBySupervisor++;
      }
    }*/
    for(EventAttendee  ea:[Select Event.CommentsBySupervisor__c From EventAttendee where AttendeeId =:userid and Event.StartDateTime >=:CurrentStartMonth
                 and Event.StartDateTime <:CurrentEndMonth and Event.Done__c = true 
                 and EventId != null and Event.V2_IsExpire__c =false and Event.SubjectType__c=:'拜访' 
                 and (Status='Accepted' or Event.CommentsBySupervisor__c != null )])
    {
      AssistanceFrequency++;
      if(ea.Event.CommentsBySupervisor__c != null && ea.Event.CommentsBySupervisor__c != '')
      {
        CommentsBySupervisor++;
      }
    }
  }
  /*
    代表
  */
  
  //得到目标联系人人数：当月月计划明细中联系人数量
  //2012-3-26修改为 当月拜访完成的联系人数，从日历上取
  public Double getTargetContactQuantity()
  {
    //目标联系人数即为Set size；
    Double ContactQuantity = 0;
    Set<Id> Set_Contactids = new Set<Id>();
    if(List_Event != null && List_Event.size()>0)
    {
      for(Event ev:List_Event)
      {
        if(ev.RecordType.DeveloperName != 'V2_Event')
        {
        	continue;
        }
        if(ev.SubjectType__c == '拜访' && ev.Done__c && ev.WhoId != null )
        {
          Set_Contactids.add(ev.WhoId);
        }
      }
    }
    if(Set_Contactids != null && Set_Contactids.size()>0)
    {
      ContactQuantity = Set_Contactids.size();
    }
    return ContactQuantity;
  }
  
  /*
    1.拜访完成率：当月完成的拜访且未过期/计划拜访数
    2.访前计划完成率：当月已完成的拜访中填写访前计划的比例
    3.访后分析完成率:当月已完成的拜访中填写访后分析的比例
  */
  public List<Double> getCallRecordFinishedPercent()
  {
    List<Double> FinishedPercentList = new List<Double>();
    //拜访完成率
    Double FinishPercent =0;
    //访前计划完成率
    Double CallPlanDonePercet = 0;
    //访后分析完成率
    Double ExecuteResultDonePercet = 0;
    if(List_Event != null && List_Event.size()>0)
    {
      //完成数
      Double Done = 0;
      //访前计划数
      Double GAPlan = 0;
      //访后分析数  
      Double GAExecuteResult = 0;
      for(Event ev:List_Event)
      {
        if(ev.RecordType.DeveloperName != 'V2_Event' || ev.SubjectType__c != '拜访')
        {
          continue;
        }
        if(ev.Done__c)
        {
          Done++;
        }
        if(ev.Done__c && ev.GAPlan__c !=null && ev.GAPlan__c!='')
        {
          GAPlan++;
        }
        if(ev.Done__c && ev.GAExecuteResult__c !=null && ev.GAExecuteResult__c != '')
        {
          GAExecuteResult++;
        }
      }
      if(Done>0)
      {
        CallPlanDonePercet = GAPlan/Done;
        ExecuteResultDonePercet = GAExecuteResult/Done;
      }
      if(TotalCallRecords >0)
      {
        FinishPercent = Done/TotalCallRecords;
      }
    }
    FinishedPercentList.add(FinishPercent);
    FinishedPercentList.add(CallPlanDonePercet);
    FinishedPercentList.add(ExecuteResultDonePercet);
    return FinishedPercentList;
  }
    //业务机会数
    public Double getOppQuantity()
    {
      Double OppQuantity = 0;
      if(Set_Oppids != null && Set_Oppids.size()>0)
      {
        OppQuantity = Set_Oppids.size();
      }
      return OppQuantity;
    }
    
  //相关事件达成率:当月业务机会相关的‘非拜访’事件数量
  public Map<Id,Double> getOppEventPercent()
  {
    //业务机会及其对应的相关事件数
    Map<Id,Double> Map_OppEventPercent = new Map<Id,Double>();
    for(Event ev:List_Event)
    {
      if(ev.WhatId == null)
      {
        continue;
      }
      if(Set_Oppids != null && Set_Oppids.contains(ev.WhatId))
      {
        //普通事件
        if(ev.RecordType.DeveloperName != 'V2_Event')
        {
          if(Map_OppEventPercent.containsKey(ev.WhatId))
          {
          	Double num = Map_OppEventPercent.get(ev.WhatId);
          	Map_OppEventPercent.put(ev.WhatId,num+1);
          }
          else
          {
          	Map_OppEventPercent.put(ev.WhatId,1);
          }
        }
      }
    }
    return Map_OppEventPercent;
  }
   //相关拜访达成率：当月业务机会相关的‘拜访’事件数量
   public Map<Id,Double> getOppCallEventPercent()
   {
   		//业务机会及其对应的相关事件数
    	Map<Id,Double> Map_OppCallEventPercent = new Map<Id,Double>();
    	for(Event ev:List_Event)
    	{
    		if(ev.WhatId == null )
    		{
    			continue;
    		}
    		if(Set_Oppids != null && Set_Oppids.contains(ev.WhatId))
    		{
    			//拜访事件
    			if(ev.RecordType.DeveloperName == 'V2_Event')
    			{
    				if(Map_OppCallEventPercent.containsKey(ev.WhatId))
		          	{
		          		Double num = Map_OppCallEventPercent.get(ev.WhatId);
		          		Map_OppCallEventPercent.put(ev.WhatId,num+1);
		          	}
		          	else
		          	{
		          		Map_OppCallEventPercent.put(ev.WhatId,1);
		          	}
    			}
    		}
    	}
    	return Map_OppCallEventPercent;
   }
  /*
   主管
  */
  //得到用户下属业绩平均值
  public Double getRepAverageScore(Set<Id> repids,Integer year,Integer month)
  {
    Double AverageScore = 0;
    Double sum = 0;
    Double reps = 0;
    for(V2_UserBonusInFo__c vubif:[select V2_BonusScore__c from V2_UserBonusInFo__c 
                   where V2_Year__c =: String.valueOf(year)
                   and V2_Month__c =:String.valueOf(month)
                   and V2_BonusScore__c != null
                   and OwnerId in: repids])
    {
      reps++;
      sum+=vubif.V2_BonusScore__c;
    }
    if(reps>0)
    {
      AverageScore = sum/reps;
    }
    return AverageScore;
  }
  //主管带医院下属业绩平均值
  public Double getRenalvalidSuperRepAverageScore(Set<Id> repids,Integer year,Integer month,Id userid)
  {
    Double AverageScore = 0;
    Double sum = 0;
    Double reps = 0;
    //其下属分数
    for(V2_UserBonusInFo__c vubif:[select V2_BonusScore__c from V2_UserBonusInFo__c 
                   where V2_Year__c =: String.valueOf(year)
                   and V2_Month__c =:String.valueOf(month)
                   and V2_BonusScore__c != null
                   and OwnerId in: repids])
    {
      reps++;
      sum+=vubif.V2_BonusScore__c;
    }
    //其作为销售的分数
    List<V2_UserBonusInFo__c> UBI = [select V2_RenalIsHosScore__c from V2_UserBonusInFo__c 
                   					where V2_Year__c =: String.valueOf(year)
                   					and V2_Month__c =:String.valueOf(month)
                   					and V2_RenalIsHosScore__c != null
                   					and OwnerId =:userid and RecordType.DeveloperName = 'V2_RenalSupervisor2'];
    if(UBI != null && UBI.size()>0)
    {
    	reps+=1;
    	sum+=UBI[0].V2_RenalIsHosScore__c;
    }
    if(reps>0)
    {
      AverageScore = sum/reps;
    }
    return AverageScore;
  }
  //主管只带医院不带人，则他的成绩为他作为销售的成绩。
  public Double getRenalSuperRepScore(Integer year,Integer month,Id userid)
  {
  	Double Score = 0;
  	List<V2_UserBonusInFo__c> UBI = [select V2_RenalIsHosScore__c from V2_UserBonusInFo__c 
                   					where V2_Year__c =: String.valueOf(year)
                   					and V2_Month__c =:String.valueOf(month)
                   					and V2_RenalIsHosScore__c != null
                   					and OwnerId =:userid and RecordType.DeveloperName = 'V2_RenalSupervisor2'];
    if(UBI != null && UBI.size()>0)
    {
    	Score =UBI[0].V2_RenalIsHosScore__c;
    }
    return Score;
  }
  //主管协访客户数:当月主管协访的已完成的拜访事件数量。（主管是被邀请人，且该拜访已完成）
  public Double getAssistanceFrequency()
  {
    return AssistanceFrequency;
  }
  //主管协访评语:主管协访数量 中填写了评语的数量，协访超过标准的也要考核评语，加分也扣分（未过期完成）
  public Double getCommentsBySupervisor()
  {
    //主管协访评语百分比
    Double Percent = 0;
    if(AssistanceFrequency>0)
    {
      Percent = CommentsBySupervisor/AssistanceFrequency;
    }
    return Percent;
  }
  //主管月计划评语：主管填写月计划评语的数量 主管填写月计划评语的数量/下属人数
  public Double getSupervisorComment(Set<Id> repids,Integer year,Integer month)
  {
    Double SupervisorComment=0;
    for(MonthlyPlan__c mp:[select supervisor_comment__c from MonthlyPlan__c where OwnerId in: repids 
                 and Month__c=:string.valueOf(month) and Year__c =:string.valueOf(year)])
    {
      if(mp.supervisor_comment__c != null && mp.supervisor_comment__c != '')
      {
        SupervisorComment++;
      }
    }
    return SupervisorComment;
  }
  
  /***************************************************测试类***************************************************/
   static testMethod void V2_RenalBonusDataService() {
   		/*
       //**************************Create User*************************
       //*用户角色
       //经理
       UserRole SupervisorUserRole = new UserRole() ;
       SupervisorUserRole.Name = 'Renal-Supervisor-Shanghai-PD-Supervisor';
       //SupervisorUserRole.ParentRoleId = RegionalUserRole.Id ;
       insert SupervisorUserRole ;
       //销售
       UserRole RepUserRole = new UserRole() ;
       RepUserRole.Name = 'Renal-Rep-Shanghai-HD-Rep';
       RepUserRole.ParentRoleId = SupervisorUserRole.Id ;
       insert RepUserRole ;
       
    //*用户简档
    	Profile RepProRenal=null;
    	Profile SupProRenal=null;
      List<Profile> pros = [select Id from Profile where Name  = 'Standard User - Renal Sales Rep' 
      	or Name='Standard User - Renal Sales Supervisor'];
      for(Profile p:pros){
      	if(p.name.equals('Standard User - Renal Sales Rep')){
      		RepProRenal=p;
      	}else if(p.name.equals('Standard User - Renal Sales Supervisor')){
      		SupProRenal=p;
      	}
      }
    //rep简档
      //Profile RepProRenal = [select Id from Profile where Name  = 'Standard User - Renal Sales Rep' limit 1];
      //Supr简档renal
      //Profile SupProRenal = [select Id from Profile where Name='Standard User - Renal Sales Supervisor' limit 1];
    
    //************User***********
    List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
    
    //*销售
       User RepSu = new User();
       RepSu.Username='RepSu@123.com';
       RepSu.LastName='RepSu';
       RepSu.Email='RepSu@123.com';
       RepSu.Alias=user[0].Alias;
       RepSu.TimeZoneSidKey=user[0].TimeZoneSidKey;
       RepSu.ProfileId=RepProRenal.Id;
       RepSu.LocaleSidKey=user[0].LocaleSidKey;
       RepSu.LanguageLocaleKey=user[0].LanguageLocaleKey;
       RepSu.EmailEncodingKey=user[0].EmailEncodingKey;
       RepSu.CommunityNickname='RepSu';
       RepSu.MobilePhone='12345678912';
       RepSu.UserRoleId = RepUserRole.Id ;
       RepSu.IsActive = true;
       insert RepSu;
   
       //********主管*******
       User RepSs = new User();
       RepSs.Username='RepSs@123.com';
       RepSs.LastName='RepSs';
       RepSs.Email='RepSs@123.com';
       RepSs.Alias=user[0].Alias;
       RepSs.TimeZoneSidKey=user[0].TimeZoneSidKey;
       RepSs.ProfileId=SupProRenal.Id;
       RepSs.LocaleSidKey=user[0].LocaleSidKey;
       RepSs.LanguageLocaleKey=user[0].LanguageLocaleKey;
       RepSs.EmailEncodingKey=user[0].EmailEncodingKey;
       RepSs.CommunityNickname='RepSs';
       RepSs.MobilePhone='123456789112';
       RepSs.UserRoleId = SupervisorUserRole.Id ;
       RepSs.IsActive = true;
       insert RepSs;
       
        //*客户
    RecordType accrecordtype = [select Id from RecordType where Name='医院' and SobjectType='Account' limit 1];
    Account acc = new Account();
    acc.RecordTypeId = accrecordtype.Id;
    acc.Name = 'AccTest';
    insert acc;
    //*客户小组
    AccountTeamMember atm = new AccountTeamMember();
    atm.AccountId = acc.Id;
    atm.UserId = RepSu.Id;
    insert atm;
    //*Acc医院信息
    //CCHospitalInfo__c cch = new CCHospitalInfo__c();
    //cch.Account__c = acc.Id;
    //cch.
    //*联系人
    RecordType conrecordtype = [select Id from RecordType where Name = 'MD' and SobjectType='Contact' and IsActive = true limit 1 ];
    Contact con1 = new Contact();
    con1.LastName = 'AccTestContact1';
    con1.AccountId=acc.Id;
    insert con1;
    
    Contact con2 = new Contact();
    con2.LastName = 'AccTestContact2';
    con2.AccountId=acc.Id;
    con2.V2_Level__c = 'A';
    insert con2;
    //Ka
    Contact con3 = new Contact();
    con3.LastName = 'AccTestContact2';
    con3.AccountId=acc.Id;
    con3.Title = '院长';
    insert con3;
    //*业务机会
    Opportunity opp = new Opportunity();
    opp.Name = 'OppTest';
    opp.AccountId = acc.Id; 
    opp.StageName = '发现/验证机会';
    opp.Type = '其他';
    opp.CloseDate = date.today().addmonths(1);
    opp.OwnerId = RepSu.Id;
    insert opp;
    
    Opportunity opp1 = new Opportunity();
    opp1.Name = 'OppTest1';
    opp1.AccountId = acc.Id;
    opp1.StageName = '发现/验证机会';
    opp1.Type = '其他';
    opp1.CloseDate = date.today().addmonths(1);
    opp.OwnerId = RepSu.Id;
    insert opp1;
    //*市场活动
    Campaign cam = new Campaign();
    cam.Name = 'CamTest';
    cam.StartDate = date.today().addMonths(1);
    cam.EndDate = date.today().addMonths(2);
    cam.IsActive = true;
    insert cam;
    //*市场活动成员
    CampaignMember cm = new CampaignMember();
    cm.CampaignId = cam.Id;
    cm.ContactId = con2.Id;
    cm.User__c = RepSu.Id;
    insert cm;
    //*拜访事件
    RecordType callRt = [Select Id from RecordType where DeveloperName = 'V2_Event' Limit 1];
    
    Event CallEv = new Event();
    CallEv.RecordTypeId = callRt.Id;
    CallEv.WhoId = con1.Id;
    CallEv.StartDateTime = datetime.now();
    CallEv.EndDateTime = datetime.now().addMinutes(1);
    CallEv.SubjectType__c = '拜访';
    CallEv.OwnerId = RepSu.Id;
    CallEv.Done__c = true;
    CallEv.V2_2levelScore__c =String.valueOf(3);
    insert CallEv;
    
    Event CallEv2 = new Event();
    CallEv2.RecordTypeId = callRt.Id;
    CallEv2.WhoId = con2.Id;
    CallEv2.StartDateTime = datetime.now();
    CallEv2.EndDateTime = datetime.now().addMinutes(1);
    CallEv2.WhatId = opp1.Id;
    CallEv2.SubjectType__c = '拜访';
    CallEv2.OwnerId = RepSu.Id;
    CallEv2.Done__c = true;
    CallEv2.GAPlan__c = '3fsfsfsfsfsfsfsfsf';
    callEv2.GAExecuteResult__c = 'nihao hosnfs';
    insert CallEv2;
    
    Event CallEv3 = new Event();
    CallEv3.RecordTypeId = callRt.Id;
    CallEv3.WhoId = con1.Id;
    CallEv3.StartDateTime = datetime.now();
    CallEv3.EndDateTime = datetime.now().addMinutes(1);
    CallEv3.WhatId = cam.Id;
    CallEv3.V2_FollowEventFlag__c = true;
    CallEv3.SubjectType__c = '拜访';
    CallEv3.OwnerId = RepSu.Id;
    CallEv3.GAPlan__c = '3fsfsfsfsfsfsfsfsf';
    callEv3.GAExecuteResult__c = 'nihao hosnfs';
    CallEv3.V2_IsExpire__c = true;
    insert CallEv3;
    
    Event CallEv4 = new Event();
    CallEv4.RecordTypeId = callRt.Id;
    CallEv4.WhoId = con3.Id;
    CallEv4.StartDateTime = datetime.now();
    CallEv4.EndDateTime = datetime.now().addMinutes(1);
    CallEv4.WhatId = cam.Id;
    CallEv4.V2_FollowEventFlag__c = true;
    CallEv4.SubjectType__c = '科室会';
    CallEv4.Done__c = true;
    CallEv4.OwnerId = RepSu.Id;
    insert CallEv4;
    
    Event CallEv41 = new Event();
    CallEv41.RecordTypeId = callRt.Id;
    CallEv41.WhoId = con3.Id;
    CallEv41.StartDateTime = datetime.now();
    CallEv41.EndDateTime = datetime.now().addMinutes(1);
    CallEv41.WhatId = cam.Id;
    CallEv41.V2_FollowEventFlag__c = true;
    CallEv41.SubjectType__c = '跟台';
    CallEv41.Done__c = true;
    CallEv41.OwnerId = RepSu.Id;
    insert CallEv41;
    
    //*月计划
    MonthlyPlan__c mp = new MonthlyPlan__c();
    mp.Year__c = String.valueOf(Date.today().Year());
    mp.Month__c = String.valueOf(Date.today().Month());
    mp.ownerId = RepSu.Id;
    insert mp;
    //*月计划明细
    MonthlyPlanDetail__c mpd1 = new MonthlyPlanDetail__c();
    mpd1.Contact__c = con1.Id;
    mpd1.Account__c = acc.Id;
    mpd1.MonthlyPlan__c = mp.Id;
    mpd1.ArrangedTimes__c = 5;
    mpd1.AdjustedTimes__c = 6;
    mpd1.Planned_Finished_Calls__c = 5;
    insert mpd1;
    
    //*下属分数
    RecordType rt = [select Id from RecordType where SobjectType ='V2_UserBonusInFo__c' and DeveloperName ='V2_BiosRep' limit 1];
    V2_UserBonusInFo__c ubif = new V2_UserBonusInFo__c();
    ubif.RecordTypeId = rt.Id;
    ubif.V2_Year__c = String.valueOf(Date.today().Year());
    ubif.V2_Month__c = String.valueOf(Date.today().Month());
    ubif.V2_BonusScore__c = 90;
    ubif.OwnerId = RepSu.Id;
    insert ubif;
    */
    String repUserId='00590000001EMIp';//RepSu.Id;//徐莹琳
    String repSupUserId='00590000000Zgfu';//RepSs.Id;//周燕凌
    Test.startTest();
      V2_RenalBonusDataService rbds = new V2_RenalBonusDataService(repUserId,Date.today().Year(),Date.today().month());
      rbds.getTargetContactQuantity();
      rbds.getCallRecordFinishedPercent();
      rbds.getOppQuantity();
      rbds.getOppEventPercent();
      rbds.getOppCallEventPercent();
      Set<Id> repids = new Set<Id>();
      repids.add(repUserId);
      
      V2_RenalBonusDataService rbds1 = new V2_RenalBonusDataService(repSupUserId,Date.today().Year(),Date.today().month());
      rbds1.getRenalvalidSuperRepAverageScore(repids,Date.today().Year(),Date.today().month(),repSupUserId);
      rbds1.getRenalSuperRepScore(Date.today().Year(),Date.today().month(),repSupUserId);
      rbds1.getRepAverageScore(repids,Date.today().Year(),Date.today().month());
      rbds1.getAssistanceFrequency();
      rbds1.getCommentsBySupervisor();
      rbds1.getSupervisorComment(repids,Date.today().Year(),Date.today().month());
      	
    Test.stopTest();
    }
}