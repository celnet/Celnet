/**
 * 作者:Bill
 * 说明：SEP HD/CRRT CS KPI计算
**/
public class ClsHdCrrtCsKpiService {
    ID UserId ;
    Integer IntYear ;
    Integer IntMonth ;
    List<Event> list_ValidEvent ;
    
    public ClsHdCrrtCsKpiService(){
        
    }
    public ClsHdCrrtCsKpiService(ID sUserId,Integer sIntYear,Integer sIntMonth){
        UserId = sUserId;
        IntYear = sIntYear;
        IntMonth = sIntMonth ; 
    }
    
    private List<Event> getValidVisit(){
        //已完成、未过期的拜访
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        List<Event> list_Visit=[Select Id,GAPlan__c,GAExecuteResult__c From Event Where IsChild = false And RecordType.DeveloperName = 'V2_Event' And Done__c = true And V2_IsExpire__c = false And StartDateTime >: CheckStartDateTime And StartDateTime <: CheckEndDateTime And OwnerId =: UserId];
        return list_Visit ;
    }
    
    //REP KPI
    //KPI:目标联系人人数
    //说明：当月月计划明细中实际拜访联系人数
    public Map<String , Double> GetActualVisitNumber(){
        Map<String , Double> map_Result = new Map<String , Double>();
        String YearMonth = string.valueOf(IntYear) + string.valueOf(IntMonth);
        List<MonthlyPlan__c> List_MonthlyPlan = [Select m.V2_MpYearMonth__c, m.OwnerId,  (Select Contact__c, Planned_Finished_Calls__c From MonthlyPlanDetail__r where Planned_Finished_Calls__c > 0) From MonthlyPlan__c m where m.V2_MpYearMonth__c =:YearMonth and m.OwnerId =: UserId];
        map_Result.put('Target' , 30);
        map_Result.put('Finish' , list_MonthlyPlan.size()>0?List_MonthlyPlan[0].MonthlyPlanDetail__r.size():0);
        return map_Result;
    }
    
    //KPI:拜访完成率
    //说明：当月完成的拜访且未过期/计划拜访数
     public Map<String , Double> GetCallCompletionRate(){
        Map<String , Double> map_Result = new Map<String , Double>();
        //获取已完成未过期的拜访
        if(list_ValidEvent==null){
            list_ValidEvent = getValidVisit();
        }
        List<Event> list_Event = list_ValidEvent;
        //已完成未过期拜访数
        Double VisitQty = list_Event.size();
        
        //计划拜访数（当月月计划明细中的所有“计划次数”字段的值之和）
        List<MonthlyPlan__c> monthPlan = [Select Id,V2_TotalCallRecords__c From MonthlyPlan__c Where Year__c =: String.valueOf(IntYear) And Month__c =: String.valueOf(IntMonth) And OwnerId =: UserId Limit 1] ;
        Double PlanQty ;
        if(monthPlan.size() > 0){
            PlanQty =  monthPlan[0].V2_TotalCallRecords__c;
        }
        
        
        
        map_Result.put('Target' , PlanQty);
        map_Result.put('Finish' , VisitQty);
        
        if(PlanQty==null || PlanQty==0){
            map_Result.put('Rate' , 0);
        }else{
            map_Result.put('Rate' , VisitQty/PlanQty) ;
        }
        return map_Result;
    }
    
    //KPI:访前计划及访后分析完成率
    //说明：当月已完成的拜访中填写访前计划和访后分析的比例((访前计划完成%+访后分析完成%)/2)
     public Map<String , Double> GetVisitPlanRate(){
        Map<String , Double> map_Result = new Map<String , Double>();
        //拜访次数
        double VisitNum;
        //访前计划次数
        double VisitPlanNum;
        //访后分析次数
        double VisitAnalysisNum;
        //访前计划完成率
        double VisitPlan;
        //访后反洗完成率
        double VisitAnalysis;
        
        List<Event> list_Visit = getValidVisit();
        VisitNum = list_Visit.size() ;
        for(Event objEvent : list_Visit){
            if(objEvent.GAPlan__c != null){
                VisitPlanNum = (VisitPlanNum==null?0:VisitPlanNum) + 1 ;
            }
            if(objEvent.GAExecuteResult__c != null){
                VisitAnalysisNum = (VisitAnalysisNum==null?0:VisitAnalysisNum) + 1 ;
            }
        }
        
        if(VisitNum != null && VisitPlanNum != null){
            VisitPlan = VisitPlanNum / VisitNum ;
        }
        if(VisitNum != null && VisitAnalysisNum != null){
            VisitAnalysis = VisitAnalysisNum / VisitNum;
        }
        map_Result.put('Target' , VisitNum);
        map_Result.put('Finish Before' , VisitPlanNum);
        map_Result.put('Finish After' , VisitAnalysisNum);
        map_Result.put('Rate' , ((VisitPlan==NULL?0:VisitPlan)+(VisitAnalysis==NULL?0:VisitAnalysis))/2);
        return  map_Result;
    }
    
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        //user role
        List<UserRole> listur= new List<UserRole>();
        UserRole ur1 = new UserRole();
        ur1.Name = 'Renal-Supervisor-华南-PD-Supervisor(U1)';
        listur.add(ur1);
        UserRole ur2 = new UserRole();
        ur2.Name = 'Renal-Rep-华南-PD-Rep(U2)';
        listur.add(ur2);
        insert listur;
        //user 
        list<User> user=[Select TimeZoneSidKey,ProfileId,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname, Alias From User Where IsActive = true limit 1];
        List<User> list_user = new List<User>();
        User user1=new User();
        user1.username='user1r2@123.com';
        user1.LastName='user1r2';
        user1.Email='user1r2@123.com';
        user1.Alias=user[0].Alias;
        user1.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user1.ProfileId=UserInfo.getProfileId();
        user1.LocaleSidKey=user[0].LocaleSidKey;
        user1.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user1.EmailEncodingKey=user[0].EmailEncodingKey;
        user1.CommunityNickname='chequ';
        user1.MobilePhone='12345678912';
        user1.IsActive = true;
        user1.UserRoleId=ur1.Id ;
        list_user.add(user1);
        User user2=new User();
        user2.username='user2r2@223.com';
        user2.LastName='user2r2';
        user2.Email='user2r2@223.com';
        user2.Alias=user[0].Alias;
        user2.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user2.ProfileId=UserInfo.getProfileId();
        user2.LocaleSidKey=user[0].LocaleSidKey;
        user2.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user2.EmailEncodingKey=user[0].EmailEncodingKey;
        user2.CommunityNickname='chequ1';
        user2.MobilePhone='22345678922';
        user2.IsActive = true;
        user1.UserRoleId=ur1.Id ;
        list_user.add(user2);
        insert list_user;
        
        //联系人
        List<Contact> listct= new List<Contact>();
        Contact ct = new Contact();
        ct.FirstName = 'jim';
        ct.LastName = 'Green';
        listct.add(ct);
        Contact ct1 = new Contact();
        ct1.FirstName = 'jim1';
        ct1.LastName = 'Green1';
        listct.add(ct1);
        insert listct;
        
        
        //活动
        List<Event> list_Visit = new List<Event>();
        Event ev1 = new Event();
        ev1.DurationInMinutes = 1;
        ev1.StartDateTime = datetime.now();
        ev1.OwnerId = user1.Id ;
        ev1.GAPlan__c = '1111111';
        ev1.GAExecuteResult__c = '111111';
        ev1.RecordTypeId = '01290000000NapsAAC';
        ev1.Done__c = true;
        ev1.V2_IsExpire__c = false; 
        list_Visit.add(ev1);
        Event ev2 = new Event();
        ev2.DurationInMinutes = 2;
        ev2.StartDateTime = datetime.now();
        ev2.OwnerId = user1.Id ;
        ev2.RecordTypeId = '01290000000NapsAAC';
        ev2.Done__c = true;
        ev2.V2_IsExpire__c = false; 
        list_Visit.add(ev2);
        insert list_Visit ;
        
        //月计划
        List<MonthlyPlan__c> List_MonthlyPlan = new List<MonthlyPlan__c>();
        MonthlyPlan__c mc1 = new MonthlyPlan__c();
        mc1.OwnerId = user1.Id;
        mc1.Year__c = string.valueOf(datetime.now().year());
        mc1.Month__c = string.valueOf(datetime.now().month());
        List_MonthlyPlan.add(mc1);
        insert List_MonthlyPlan;
        
        //月计划明细
        List<MonthlyPlanDetail__c> List_MonthlyPlanDetail = new List<MonthlyPlanDetail__c>();
        MonthlyPlanDetail__c mcc1 = new MonthlyPlanDetail__c();
        mcc1.Contact__c = ct.Id;
        mcc1.Planned_Finished_Calls__c = 1;
        mcc1.AdjustedTimes__c = 10;
        mcc1.MonthlyPlan__c = mc1.id;
        List_MonthlyPlanDetail.add(mcc1);
        MonthlyPlanDetail__c mcc2 = new MonthlyPlanDetail__c();
        mcc2.Contact__c = ct1.Id;
        mcc2.Planned_Finished_Calls__c = 12;
        mcc2.MonthlyPlan__c = mc1.Id;
        mcc2.AdjustedTimes__c = 10;
        List_MonthlyPlanDetail.add(mcc2);
        insert List_MonthlyPlanDetail;
        
        system.test.startTest();
        ClsHdCrrtCsKpiService cls = new ClsHdCrrtCsKpiService(user1.Id , datetime.now().year() , datetime.now().month() );
        Map<String , Double> a = cls.GetVisitPlanRate();
        Map<String , Double> b = cls.GetCallCompletionRate();
        Map<String , Double> c = cls.GetActualVisitNumber();
        system.debug(a+'@@@'+b+'@@@'+c);
        system.test.stopTest();
    }
}