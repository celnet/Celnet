/*
 * 作者:Tobe
 * 日期：2013-9-25
 * 说明：Dropout病人拜访列表
 */
public class PM_DropoutpatientvisitlistDevController 
{
    //初始页面显示列表的查询条件
    private   string query = 'Select PM_BVTime1__c,PM_BVTime2__c,PM_BVTime3__c,PM_BVTime4__c,PM_BVTime5__c,PM_BVTime6__c,PM_Pdtreatment__c,PM_LastSuccessDate2__c,PM_LastSuccessTime2__c,PM_CompetingReason__c,PM_TransfornCreated_Date__c,PM_DropoutCreated_Date__c,PM_TransformBrand__c,PM_DropOut_One_Reason__c ,PM_DropOut_Two_Reason__c  , PM_Status__c,PM_InDate__c ,PM_SortDate__c,Name,PM_Sex__c,PM_PmTel__c,PM_PmPhone__c,PM_FamilyTel__c,PM_FamilyPhone__c,PM_HomeTel__c,PM_OtherTel2__c,PM_TelPriorityFirst__c,PM_TelPrioritySecond__c,PM_Current_Saler__c,PM_VisitState__c,PM_PreviousStatus__c,PM_InHospital__c From PM_Patient__c  Where isDeleted=false and PM_Status__c = \'Dropout\'';
    public long Limit_Size
    {
    	get
    	{
    		return PM_QueryUtil.LIMIT_SIZE;
    	}
    	set;
    }
    public PM_Patient__c Patient{get;set;}//病人编号，姓名，医院  
    //基本信息字段  姓名{Name} 当前负责销售{PM_Current_Saler__c} 失败次数{PM_VisitFailCount__c}
    public string MobilePhone{get;set;}//联系电话
    public string PatientStatus = 'Dropout';//病人状态
    public string RawStatus {set;get;}//原状态
    //联动大区、省份、城市、县
    public string BigArea{get;set;}
    public string Province{get;set;}
    public string City{get;set;}
    public string County{get;set;}
    public string PatientProvince{get;set;}
    public string PatientCity{get;set;}
    public string PatientCounty{get;set;}
    public string InHospital{get;set;}//插管医院
    public string TransfornBrand{get;set;}
    public string LastVisitDate_Start{get;set;}//上次拜访时间开始
    public string LastVisitDate_End{get;set;}//上次拜访时间截止 
    public string BestVisitTime{set;get;} //最佳拜访时间
    public string Age_Lower{set;get;} //年龄下限
	public string Age_Upper{set;get;} //年龄上限
	public string Visit_Interval{set;get;}//要求拜访隔天数
	public List<SelectOption> getVisit_Intervals() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('--无--','--无--'));
            options.add(new SelectOption('空','空'));
            options.add(new SelectOption('小于31天','小于31天'));
            options.add(new SelectOption('31-60天','31-60天'));
            options.add(new SelectOption('61-90天','61-90天'));
            options.add(new SelectOption('91-180天','91-180天'));
            options.add(new SelectOption('大于180天','大于180天'));
            return options;
    }  
 	public List<SelectOption> getTransfornBrands() {
            List<SelectOption> options = new List<SelectOption>();
            Schema.DescribeFieldResult payList = PM_Patient__c.PM_TransformBrand__c.getDescribe();
			List<Schema.PicklistEntry> ListPay = payList.getPicklistValues();
			options.add(new SelectOption('--无--','--无--'));
			for(Schema.PicklistEntry sp : ListPay)
			{
				options.add(new SelectOption(sp.getValue(),sp.getValue()));
			}
            return options;
    }
    
    //日期
    public string DropOutCreatedDate_Start{set;get;}
    public string DropOutCreatedDate_End{set;get;}
    public string DropoutActualDate_Start{set;get;}//上次修改时间开始PM_DropoutActualDate__c
    public string DropoutActualDate_End{set;get;}//上次修改时间截止
    public string TransfornCreatedDate_Start{set;get;}//转竞品后掉队创建日期
	public string TransfornCreatedDate_End{set;get;}
	public string TransfornActDate_Start{set;get;}//转竞品后掉队实际日期
	public string TransfornActDate_End{set;get;}
    //查询工具类
    public PM_QueryUtil util = new PM_QueryUtil();
    private PM_PatientSortCommom patientSort= new PM_PatientSortCommom();
    public list<PM_ExportFieldsCommon.Patient> list_Patient//经筛选查询出来的信息
    {
        get
        {
            list_patient = new list<PM_ExportFieldsCommon.Patient>();
            list<PM_Patient__c> list_Patients = new list<PM_Patient__c>();
            for(PM_PatientSortCommom.ComparableClass psort : patientSort.PatientWrapperBeforeSort((list<PM_Patient__c>)conset.getRecords()))
            {
                list_patient.add(psort.p);
            }
            list_PatientExport.addAll(list_patient);
            result = list_patient.size();
            return list_patient;    
        }
        set;
    }
    //页面查询需要的字段
    //实现分页功能
    //构造器
    public PM_DropoutpatientvisitlistDevController(ApexPages.StandardController controller)
    {
        Patient = new PM_Patient__c();
        getExportField();
    }
    // 分页字段 
    public integer result{get;set;}//结果个数
    public Boolean hasPrevious {get {return conset.getHasPrevious();}set;}
    public Boolean hasNext {get {return conset.getHasNext();}set;}
    public Integer pageNumber {get {return conset.getPageNumber();}set;}
    public Integer categoryNumber {get {return conset.getResultSize();}set;}
    public ApexPages.StandardSetController conset //实现分页功能 ApexPages.StandardSetController
    {
        get 
        {
            if(conset == null)
            {
            	if(Userinfo.getProfileId() == util.PSR_Admin_Profile.Id || Userinfo.getProfileId() == util.PSR_Hotline_Profile.Id)
            	{
            		query += ' and OwnerId = \''+ Userinfo.getUserId()+'\' ';
            	}
                conset = util.getSortConset(query + 'Order By PM_DropoutCreated_Date__c desc');
                conset.setPageSize(PM_QueryUtil.pageSize);
            }
            return conset;
        }
        set;
    }
    //分页方法
    public void first() {conset.first();}
    public void last() {conset.last();}
    public void previous() {conset.previous();}
    public void next() {conset.next();}
    
    //查询方法
    public void Check()
    {
        string field = ' PM_BVTime1__c,PM_BVTime2__c,PM_BVTime3__c,PM_BVTime4__c,PM_BVTime5__c,PM_BVTime6__c,PM_Pdtreatment__c,PM_LastSuccessDate2__c,PM_LastSuccessTime2__c,PM_CompetingReason__c,PM_TransfornCreated_Date__c,PM_DropoutCreated_Date__c,PM_TransformBrand__c,PM_DropOut_One_Reason__c ,PM_DropOut_Two_Reason__c,PM_Status__c ,PM_InDate__c ,PM_SortDate__c,Name,PM_Sex__c,PM_PmTel__c,PM_PmPhone__c,PM_FamilyTel__c,PM_FamilyPhone__c,PM_HomeTel__c,PM_OtherTel2__c,PM_TelPriorityFirst__c,PM_TelPrioritySecond__c,PM_Current_Saler__c,PM_VisitState__c,PM_PreviousStatus__c,PM_Disappearanc__c,PM_InHospital__c,PM_InNurser__c ';
        map<string,string> map_Condition = new map<string,string>();
        map_Condition.put('Name',Patient.Name);
        map_Condition.put('MobilePhone',MobilePhone);
        map_Condition.put('PM_Current_Saler__c',Patient.PM_Current_Saler__c);
        map_Condition.put('BigArea',BigArea);
        map_Condition.put('Province',Province);
        map_Condition.put('PatientProvince',PatientProvince);
        map_Condition.put('City',City);
        map_Condition.put('PatientCity',PatientCity);
        map_Condition.put('County',County);
        map_Condition.put('PatientCounty',PatientCounty);
        map_Condition.put('PatientStatus',PatientStatus);
        map_Condition.put('DropoutActualDate_Start',DropoutActualDate_Start);
        map_Condition.put('DropoutActualDate_End',DropoutActualDate_End);
        map_Condition.put('InHospital',InHospital);
       	map_Condition.put('PM_DropOff_One_Reason__c',Patient.PM_DropOut_One_Reason__c);
        map_Condition.put('PM_DropOff_Two_Reason__c',Patient.PM_DropOut_Two_Reason__c);
        map_Condition.put('PM_TransformBrand__c',TransfornBrand);
        map_Condition.put('RawStatus',RawStatus);
	    map_Condition.put('PM_Address__c',Patient.PM_Address__c);
	    map_Condition.put('PM_CompetingReason__c',Patient.PM_CompetingReason__c);
	    map_Condition.put('DropOutCreatedDate_Start',DropOutCreatedDate_Start);
        map_Condition.put('DropOutCreatedDate_End',DropOutCreatedDate_End);
        map_Condition.put('TransfornCreatedDate_Start',TransfornCreatedDate_Start);
        map_Condition.put('TransfornCreatedDate_End',TransfornCreatedDate_End);
        map_Condition.put('TransfornActDate_Start',TransfornActDate_Start);
        map_Condition.put('TransfornActDate_End',TransfornActDate_End);
        map_Condition.put('LastVisitDate_Start',LastVisitDate_Start);
        map_Condition.put('LastVisitDate_End',LastVisitDate_End);
        map_Condition.put('PM_VisitState__c',Patient.PM_VisitState__c);
    	map_Condition.put('PM_Current_PSR__c',Patient.PM_Current_PSR__c);
    	map_Condition.put('PSRAdminUser','true');
    	map_Condition.put('BestVisitTime',BestVisitTime);
    	map_Condition.put('Age_Lower',Age_Lower);
        map_Condition.put('Age_Upper',Age_Upper);
        map_Condition.put('PM_Active_PMT__c',Patient.PM_Active_PMT__c);
        map_Condition.put('Visit_Interval',Visit_Interval);
        map_Condition.put('PM_InNurser__c',Patient.PM_InNurser__c);
        util.connectQueryString(field,map_Condition);
        this.conset = util.getSortConset(util.getPatSql()+' Order By PM_DropoutCreated_Date__c desc '); 
    }
     //排序方法
    public string strOrder{get;set;}
    public string strSortField{get;set;}
    public string strPreviousSortField{get;set;}
    //sort the orders by field selected
    public void sortOrders()
    {
        strOrder ='desc';
        if(strSortField==null)
        {
            strSortField='Name';
        }
        if(strPreviousSortField == strSortField)
        {
            strOrder = 'asc';
            strPreviousSortField = null;
        }
        else
        {
            strPreviousSortField = strSortField;
        }
        string patSql = util.getPatSql();
        if(patSql == null || patSql == '')
        {
            patSql = query;
        }
        sortSql(patSql,strSortField,strOrder);
    }
    public void sortSql(String sql,String strSortField,String strOrder)
    {
        String strSqlSort;
        if(strSortField!=null)
        {
            strSqlSort=sql+' order by '+strSortField+' '+strOrder;
        }
        strSqlSort=strSqlSort;
        this.conset = util.sortOrders(strSqlSort); 
    }
     /********************************导出功能Start****************************************/
    //导出排序工具类
    public PM_ExportFieldsCommon field = new PM_ExportFieldsCommon();
    public list<PM_ExportFieldsCommon.Patient> list_PatientExport = new list<PM_ExportFieldsCommon.Patient>();//导出时需要判断是否选中的list
    //导出excel功能需要的参数  点击导出按钮，原页面隐藏 只显示下面的要导出的列表
	public List<PM_ExportFieldsCommon.ExportField> list_ExportField{get;set;}
	
    private Transient List<PM_Patient__c> list_PatientExcel;
    private Transient List<List<PM_Patient__c>> list_PatientExcellist;

    //页面需要显示的字段集合
    private void getExportField() 
    {
    	list_ExportField = field.getDropoutPatientListFields();
    }
    
    //导出excel
    public PageReference exportExcel()
    {
        //实例化导出的内容集合
        list_PatientExcel = new List<PM_Patient__c>();
        list_PatientExcellist = new List<List<PM_Patient__c>>();
        //IsExcel = true;
        //确定页面选定的记录
        Set<ID> set_patientIds = new Set<ID>();
        set_patientIds.clear();
        for(PM_ExportFieldsCommon.patient pa : list_PatientExport)
        {
            if(pa.IsExport)
            {
                set_patientIds.add(pa.patient.Id);
            }
        }
        system.debug(set_patientIds.size() + '$$$$$$$$$$$$$$$$$$');
        string strSoql = field.getApiNameFields(list_ExportField);
        String exportSql = strSoql;
        if(set_patientIds.size()>0)
        {
            exportSql += ' Where Id in : set_patientIds';
        }else{
            string patSql = util.patSql;
            system.debug(patSql + '*************************zgxm');
            if(patSql == null || patSql == '')
            {
                if(query.indexOf('Where') >=0)
                {
                    exportSql += query.substring(query.indexOf('Where'), query.length());
                }
            }else{
            	list_PatientExcellist = util.getPatientExcellist(exportSql, patSql);
                /*if(patSql.indexOf('Where') >=0)
                {
                    exportSql += patSql.substring(patSql.indexOf('Where'), patSql.length());
                }*/
            } 
        }
        
        if(list_PatientExcellist.size() <= 0)
        {
	        for(PM_Patient__c p : util.getPatientlist((exportSql+' limit '+PM_QueryUtil.LIMIT_SIZE),set_patientIds))
	        {
	        	if(list_PatientExcel.size() < 999)
	        	{
	        		list_PatientExcel.add(p);
	        	}else{
	        		list_PatientExcel.add(p);
	        		list_PatientExcellist.add(list_PatientExcel);
	        		list_PatientExcel = new List<PM_Patient__c>();
	        	}
	        }
	        list_PatientExcellist.add(list_PatientExcel);
        }
        field.SaveSoql(strSoql, list_PatientExcellist);
        //contentType = 'application/vnd.ms-excel#DropoutPatientExcel.xls';
        return new PageReference('/apex/PM_PatientExport');
    }
    /********************************导出功能End****************************************/
}