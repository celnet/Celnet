/*
 * Tobe
 * 2013.10.10
 * Active病人拜访列表页面控制类——测试类
 */
@isTest 
private class PM_Test_ActivePatientVisitsDev {

    static testMethod void myUnitTest() 
    {
    	PM_Test_AllConfig.myUnitTest(); //公用方法，内涵必要的一些测试数据
    	system.Test.startTest();
    	PM_Patient__c patient = new PM_Patient__c();
    	ApexPages.StandardController controller = new ApexPages.StandardController(patient);
    	//实例化控制类
    	PM_ActivePatientVisitsDevController apv = new PM_ActivePatientVisitsDevController(controller);
    	//初始化get部分字段
    	ApexPages.StandardSetController conset = apv.conset;
    	list<PM_ExportFieldsCommon.Patient> list_Patient = apv.list_Patient;
    	apv.first();
	    apv.last();
	    apv.previous();
	    apv.next();
    	//查询流程
    	list<Profile> list_Profile = [Select Id,Name From Profile Where Name = '系统管理员'];
        User user = new User();
        user.ProfileId = list_Profile[0].Id;
        user.Username = 'asfdasdf@acme.com';
        user.FirstName = 'Test';
        user.LastName = 'Test';
        user.Email = 'asdf@aa.com';
        user.Alias = 'asdf';
        user.EmailEncodingKey = 'UTF-8';
        user.TimeZoneSidKey = 'Asia/Shanghai';
        user.LanguageLocaleKey = 'zh_CN';
        user.LocaleSidKey = 'zh_CN';
        insert user;
	 	apv.Patient.Name = '张先生';
 	 	apv.Patient.PM_Current_Saler__c = user.Id;
        apv.Patient.PM_VisitFailCount__c = 1;
        apv.Patient.PM_Payment__c = '自费';
        apv.Patient.PM_HighImpac__c = '是';
        apv.Patient.PM_IsStarPatient__c = '是';
        apv.Patient.PM_HDAndPD__c = '是';
        apv.Patient.PM_UsCoGoSameTime__c = '是';
    	apv.MobilePhone = '18611112222';
	    apv.PatientStatus = 'Active';
	    apv.BigArea = '东一区';
	    apv.Province = '北京';
	    apv.City = '北京市';
	    apv.County = '海淀区';
	    apv.PatientProvince = '北京';
	    apv.PatientCity = '北京市';
	    apv.PatientCounty = '海淀区';
	    apv.InHospital = '朝阳区第三医院';
	    apv.CreatedDate_Start = '2013-10-09';
	    apv.CreatedDate_End = '2013-10-09';
	    apv.InDate_Start = '2013-10-09';
	    apv.InDate_End = '2013-10-09';
	    apv.Check();
	    ///排序流程
	    apv.strOrder = 'desc';
	    apv.util.patSql = null;
	    apv.sortOrders();
	    apv.sortOrders();
	    //导出
	    apv.exportExcel(); 
	    for(PM_ExportFieldsCommon.Patient pa :apv.list_PatientExport)
	    {
	    	pa.IsExport = true;
	    }
	    apv.exportExcel();
	    apv.getItems();
	    system.Test.stopTest();
    }
}