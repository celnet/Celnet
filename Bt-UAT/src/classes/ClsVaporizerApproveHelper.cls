/**
 * Author:Sunny
 * 挥发罐申请审批辅助，自动根据申请初始化出申请审批记录（将各级审批人初始化）
 * 发送邮件给第一个审批人
**/
public class ClsVaporizerApproveHelper {
	Boolean blnIsVapApplication ;
	Boolean blnSendTheEmail = false;
	ID VapAppliId ;
    public ClsVaporizerApproveHelper(){}
    User sendUser ;
    //使用申请
    public Vaporizer_Approve__c InitVaporizerApprove(Vaporizer_Application__c VapApplication){
    	blnIsVapApplication = true;
    	VapAppliId = VapApplication.Id;
    	Vaporizer_Approve__c VapApprove = new Vaporizer_Approve__c();
    	VapApprove.Vaporizer_Application__c = VapApplication.Id ;
    	VapApprove.Submitter__c = VapApplication.OwnerId ;
    	VapApprove.ApproveURL__c = URL.getSalesforceBaseUrl().toExternalForm()+'/apex/VaporizerApplicationApprove?vaid='+VapApplication.Id ;
    	this.InitUserHierarchy(VapApprove) ;
    	return VapApprove;
    }
    //维修/退回申请
    public Vaporizer_Approve__c InitVaporizerApprove(Vaporizer_ReturnAndMainten__c VapReturnAndMainten){
    	blnIsVapApplication = false ;
    	VapAppliId = VapReturnAndMainten.Id;
        Vaporizer_Approve__c VapApprove = new Vaporizer_Approve__c();
        VapApprove.Vaporizer_ReturnAndMainten__c = VapReturnAndMainten.Id ;
        VapApprove.Submitter__c = VapReturnAndMainten.OwnerId ;
        VapApprove.ApproveURL__c = URL.getSalesforceBaseUrl().toExternalForm()+'/apex/VaporizerReturnApprove?vrid='+VapReturnAndMainten.Id ;
        this.InitUserHierarchy(VapApprove) ;
        return VapApprove;
    }
    
    private void InitUserHierarchy(Vaporizer_Approve__c VapApprove){
    	//提交人信息
    	User Submitter = [Select UserRole.Name, UserRole.Id, UserRole.ParentRoleId, UserRoleId, Id, Name, Alias, Email From User Where Id =: VapApprove.Submitter__c] ;
    	
    	//提交人上级查找，直到Regional。
    	V2_UtilClass uc = new V2_UtilClass();
    	List<ID> list_userRoleIds = uc.getRoleIdToRSM(Submitter.UserRoleId) ;
    	Map<String , UserRole> map_UserRole = new Map<String , UserRole>();
    	for(UserRole ur : [Select Id,Name,(Select Id, Name, Email, Alias From Users Where IsActive = true) From UserRole Where id in: list_userRoleIds]){
    		if(ur.Name.toUpperCase().contains('REGIONAL') && ur.Users.size() > 0){
    			map_UserRole.put('REGIONAL' , ur);
    		}else if(ur.Name.toUpperCase().contains('AREA') && ur.Users.size() > 0){
    			map_UserRole.put('AREA' , ur);
    		}else if(ur.Name.toUpperCase().contains('DISTRICT') && ur.Users.size() > 0){
                map_UserRole.put('DISTRICT' , ur); 
            }else if(ur.Name.toUpperCase().contains('SUPERVISOR') && ur.Users.size() > 0){
                map_UserRole.put('SUPERVISOR' , ur);
            }
    	}
    	//Regional
        this.RegionalMangerInfo(VapApprove, map_UserRole) ;
        //Area
        this.AreaMangerInfo(VapApprove, map_UserRole);
        //Direct
        this.DirectMangerInfo(VapApprove, map_UserRole);
    	//Supervisor
        this.SupervisorMangerInfo(VapApprove, map_UserRole);
        
        this.SendEmail(this.sendUser);
    }
    private void RegionalMangerInfo(Vaporizer_Approve__c VapApprove , Map<String , UserRole> map_UserRole){
    	if(map_UserRole.keySet().contains('REGIONAL')){
            VapApprove.RegionalManager__c = map_UserRole.get('REGIONAL').Users[0].Id;
            VapApprove.ApproveStep__c='大区经理(RSM)审批';
            sendUser = map_UserRole.get('REGIONAL').Users[0];
        }else{
            VapApprove.RegionalManager__c = null;
            VapApprove.RegionalManagerDefAgree__c=true;
            VapApprove.RegionalManager_Approve__c='无';
        }
    }
    private void AreaMangerInfo(Vaporizer_Approve__c VapApprove , Map<String , UserRole> map_UserRole){
        if(map_UserRole.keySet().contains('AREA')){
            VapApprove.AreaManager__c=map_UserRole.get('AREA').Users[0].Id;
            VapApprove.ApproveStep__c='区域经理(ASM)审批';
            sendUser = map_UserRole.get('AREA').Users[0];
            
        }else{
            VapApprove.AreaManager__c=null;
            VapApprove.AreaManagerDefAgree__c=true;
            VapApprove.AreaManager_Approve__c='无';
        }
    }
    private void DirectMangerInfo(Vaporizer_Approve__c VapApprove , Map<String , UserRole> map_UserRole){
        if(map_UserRole.keySet().contains('DISTRICT')){
            VapApprove.DirectManager__c=map_UserRole.get('DISTRICT').Users[0].Id;
            VapApprove.ApproveStep__c='地区经理(DSM)审批';
            sendUser = map_UserRole.get('DISTRICT').Users[0];
        }else{
            VapApprove.DirectManager__c=null;
            VapApprove.DirectManagerDefAgree__c=true;
            VapApprove.DirectManager_Approve__c='无';
        }
    }
    private void SupervisorMangerInfo(Vaporizer_Approve__c VapApprove , Map<String , UserRole> map_UserRole){
        if(map_UserRole.keySet().contains('SUPERVISOR')){
            VapApprove.Supervisor__c=map_UserRole.get('SUPERVISOR').Users[0].Id;
            VapApprove.ApproveStep__c='销售主管审批';
            sendUser = map_UserRole.get('SUPERVISOR').Users[0];
        }else{
            VapApprove.Supervisor__c=null;
            VapApprove.SupervisorDefAgree__c=true;
            VapApprove.Supervisor_Approve__c='无';
        }
    }
    private void SendEmail(User eUser){
    	if(eUser != null){
            String strSubject = '来自SEP系统通知：挥发罐申请审批' ;
            String strMessage = '您好：'+eUser.Alias + '\n\n'
            + '有挥发罐申请需要您进行审批，请链接下面的链接到系统中进行审批\n\n'
            //+ '   挥发罐申请：'+this.VaporizerApplication.Name +'\n'
            + '审批链接：'+URL.getSalesforceBaseUrl().toExternalForm()+(blnIsVapApplication?'/apex/VaporizerApplicationApprove?vaid=':'/apex/VaporizerReturnApprove?vaid=')+this.VapAppliId + '\n'
            + '祝您工作愉快!\n' +
            + '__________________________________________________ \n' +
            + '本邮件由Baxter Salesforce.com CRM系统产生，请勿回复。\n' +
            + '如有任何疑问或者要求，请联系系统管理人员。\n' +
            + 'Baxter SEP System';
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {eUser.Email};
            mail.setToAddresses(toAddresses);
            mail.setReplyTo('no-reply@salesforce.com');
            mail.setSubject(strSubject);
            mail.setSenderDisplayName('Baxter SEP System');
            mail.setPlainTextBody(strMessage);
            if(!Test.isRunningTest()) 
            {
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
            
        }
    }
}