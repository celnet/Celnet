/**
 * Author : Sunny
 * 页面默认显示该申请下所有可收货的记录，代表可通过勾选，点击确认按钮进行确认。
**/
public class Ctrl_VaporizerSalesReceive {
    public List<AppDetialWrapper> list_AppDetail{get;set;}
    private ID vId;
    public Ctrl_VaporizerSalesReceive(Apexpages.Standardsetcontroller controller){
        list_AppDetail = new List<AppDetialWrapper>();
        vId = Apexpages.currentPage().getParameters().get('vid');
        system.debug(controller.getSelected());
        this.initAppDetails(vId);
    }
    private void initAppDetails(ID vId){
        list_AppDetail.clear();
        for(Vaporizer_Application_Detail__c AppDetail : [Select Id,OperationRoom_No__c,VaporizerInfo__c,VaporizerInfo__r.Name,VaporizerInfo__r.OperationRoom_No__c,VaporInterface__c,VaporType__c,AppProduct__c,IsDelivered__c,IsReceived__c From Vaporizer_Application_Detail__c Where (Vaporizer_Application__c =: vId Or Vaporizer_ReturnAndMainten__c =: vId) And IsDelivered__c = true And IsReceived__c = false]){
            AppDetialWrapper adw = new AppDetialWrapper();
            adw.blnSelected = false;
            adw.appDetail = AppDetail ;
            list_AppDetail.add(adw);
        }
    }
    public void SaveDetails(){
        List<Vaporizer_Application_Detail__c> list_AppDetailUp = new List<Vaporizer_Application_Detail__c>();
        List<VaporizerInfo__c> list_VapInfo = new List<VaporizerInfo__c>();
        for(AppDetialWrapper appDetailWra : list_AppDetail){
            if(appDetailWra.appDetail.OperationRoom_No__c != null){
                appDetailWra.appDetail.IsReceived__c = true;
                list_AppDetailUp.add(appDetailWra.appDetail);
                
                VaporizerInfo__c vapInfo=appDetailWra.appDetail.VaporizerInfo__r;
                vapInfo.OperationRoom_No__c = appDetailWra.appDetail.OperationRoom_No__c ;
                /******************bill add start 2013-7-15***********************/
                //添加挥发罐初始服务时间
                vapInfo.InitUsedDate__c = date.today();
                /******************bill add end   2013-7-15***********************/
                list_VapInfo.add(vapInfo);
            }
        }
        try{
            if(list_AppDetailUp.size() > 0){
                update list_AppDetailUp;
            }else{
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '请填写手术室号。') ;            
                ApexPages.addMessage(msg) ;
                return ;
            }
            if(list_VapInfo.size() > 0){
                update list_VapInfo;
            }
            this.initAppDetails(vId);
        }catch(system.Dmlexception de){
            String strErr = '';
            for (Integer i = 0; i < de.getNumDml(); i++) {
                System.debug(de.getDmlMessage(i));
                if(de.getDmlMessage(i) != null && de.getDmlMessage(i) != ''){
                    strErr = de.getDmlMessage(i) ;
                }
            }
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, strErr) ;            
            ApexPages.addMessage(msg) ;
            return ;
        }
        
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM , '确认收货成功。') ;            
        ApexPages.addMessage(msg) ;
        return ;
    }
    public class AppDetialWrapper{
        public boolean blnSelected{get;set;}
        public Vaporizer_Application_Detail__c appDetail{get;set;}
    }
    
    /*******************test start***********************/
    static testMethod void MyTest() 
    {
        //挥发罐
        VaporizerInfo__c vap = new VaporizerInfo__c();
        vap.Status__c = '库存';
        vap.location__c = '上海仓库';
        insert vap;
        
        //使用申请
        Vaporizer_Application__c vapApply = new Vaporizer_Application__c();
        insert vapApply;
        
        //申请明细
        Vaporizer_Apply_Detail__c vapApplyDe = new Vaporizer_Apply_Detail__c();
        vapApplyDe.ApplyQty__c = 2;
        vapApplyDe.Vaporizer_Application__c = vapApply.Id;
        insert vapApplyDe;
        
        //发货明细
        Vaporizer_Application_Detail__c vapApp = new Vaporizer_Application_Detail__c();
        vapApp.VaporizerInfo__c = vap.Id;
        vapApp.Vaporizer_Application__c = vapApply.Id;
        vapApp.IsDelivered__c = true;
        insert vapApp;
         
        ApexPages.currentPage().getParameters().put('vid', vapApply.id);
        Apexpages.Standardsetcontroller controller = new Apexpages.Standardsetcontroller(Database.getQueryLocator(
                    [Select Id,OperationRoom_No__c,VaporizerInfo__c,VaporizerInfo__r.Name,VaporizerInfo__r.OperationRoom_No__c,VaporInterface__c,VaporType__c,AppProduct__c,IsDelivered__c,IsReceived__c From Vaporizer_Application_Detail__c]));
        Ctrl_VaporizerSalesReceive ctrlvap = new Ctrl_VaporizerSalesReceive(controller);
        ctrlvap.list_AppDetail[0].appDetail.OperationRoom_No__c ='A楼101室';
        ctrlvap.list_AppDetail[0].blnSelected = true;
        ctrlvap.SaveDetails();
    }
    /*******************test end***********************/
}