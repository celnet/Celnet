/*
*作者：Hank
*时间：2013-10-14
*功能：控制PM_BasicTableBatch
*/
public class PM_BasicTableBatchSchedule implements Schedulable
{
	 public void execute(SchedulableContext sc) 
	 {
    	PM_BasicTableBatch btBatch = new PM_BasicTableBatch();
		database.executeBatch(btBatch,5);
     }
     
 	static testMethod void myUnitTest() 
 	{		
        system.test.startTest();
        string sch=DateTime.now().second()+' '+DateTime.now().minute()+' '+DateTime.now().hour()+' * * ?';
        PM_BasicTableBatchSchedule pa = new PM_BasicTableBatchSchedule();
        System.schedule('test', sch , pa);
        system.test.stopTest();
	}
}