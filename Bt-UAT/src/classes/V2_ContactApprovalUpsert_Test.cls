@isTest
private class V2_ContactApprovalUpsert_Test {

    static testMethod void myUnitTest() {
       /*客户*/
		RecordType accrecordtype = [select Id from RecordType where Name='医院' and SobjectType='Account' limit 1];
		Account acc = new Account();
		acc.RecordTypeId = accrecordtype.Id;
		acc.Name = 'AccTest';
		insert acc;
		/*联系人*/
		RecordType conrecordtype = [select Id from RecordType where Name = 'Renal' and SobjectType='Contact' and IsActive = true limit 1 ];
		Contact con1 = new Contact();
		con1.LastName = 'AccTestContact1';
		con1.AccountId=acc.Id;
		insert con1;
		
		Contact con2 = new Contact();
		con2.LastName = 'AccTestContact2';
		con2.AccountId=acc.Id;
		insert con2;
        //联系人修改申请记录类型
    	List<RecordType> rtlist = [select Id,DeveloperName from RecordType where SObjectType = 'Contact_Mod__c' and IsActive=true ];
    	/*联系人修改申请上 直接新建*/
    	Contact_Mod__c cm = new Contact_Mod__c();
    	for(RecordType rt:rtlist)
    	{
    		if(rt.DeveloperName =='V2_BiosInsert')
    		{
    			cm.RecordTypeId  = rt.Id;
    		}
    	}
    	cm.Type__c = '新增';
    	cm.Account__c = acc.Id;
    	cm.Name__c = con1.Id;
    	cm.NewContact__c = '433333333';
    	insert cm;
    	cm.Status__c = '通过';
    	update cm;
    	
    	Contact_Mod__c cm2 = new Contact_Mod__c();
    	for(RecordType rt:rtlist)
    	{
    		if(rt.DeveloperName =='V2_Bios')
    		{
    			cm2.RecordTypeId  = rt.Id;
    		}
    	}
    	cm2.Type__c = '编辑';
    	cm2.Name__c = con2.Id;
    	cm2.NewContact__c = 'eeeeeeeeeeee';
    	cm2.Account__c = acc.Id;
    	insert cm2;
    	cm2.Status__c = '通过';
    	update cm2;
    }
}