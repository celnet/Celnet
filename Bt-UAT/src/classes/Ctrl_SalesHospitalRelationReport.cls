/**
 * Author:Sunny
 * 系统每月25日发送邮件给主管，邮件内容为其下属所负责的医院列表
 * bill add 8-14
 * 邮件内容的销售医院关系列表不会显示已离职的用户，并且需要显示销售医院关系的起效日期
 **/
public class Ctrl_SalesHospitalRelationReport {
    
    public ID ReportToSupervisorId{get;set{
        ReportToSupervisorId = value;
        this.initSupervisorInfo(ReportToSupervisorId);
        this.buildReportRow();
    }}
    public List<ReportRow> list_Report{get;set;}
    public User sUser ;
    private List<ID> list_SalesUserIds ;
    private Map<ID , Set<String>> map_SalesHospital = new Map<ID , Set<String>>();//销售ID - {所负责医院名称}
    private Map<ID , String> map_SalesName = new Map<ID, String>();//销售ID - 销售别名
    private Map<String , String> map_SaleStart = new Map<String , String>();//销售ID+客户id - 最终生效日期
    //封装报表展示行
    public class ReportRow{
        public String sSalesName{get;set;}
        public String sHospitalName{get;set;}
        public Boolean bShow{get;set;}
        public Integer iRowSpan{get;set;}
        //生效日期
        public String StartTime{get;set;}
    }
    public Ctrl_SalesHospitalRelationReport(){
        list_Report = new List<ReportRow>();
        system.debug(ReportToSupervisorId);
        //this.initSupervisorInfo(ReportToSupervisorId);
        //this.buildReportRow();
    }
    //初始化报表展示行
    private void buildReportRow(){
        if(list_SalesUserIds==null){
             return ;
        }
        system.debug(map_SalesHospital);
        list_Report.clear();
        for(ID UserId : list_SalesUserIds){
            if(map_SalesHospital.containsKey(UserId)){
            	Integer i = map_SalesHospital.get(UserId).size();
                for(String strHospitalName : map_SalesHospital.get(UserId)){
                    ReportRow rr = new ReportRow();
                    if(i != null){
                        rr.iRowSpan = i;
                        i = null;
                        rr.bShow = true;
                    }else{
                        rr.bShow = false;
                    }
                    rr.sSalesName = map_SalesName.get(UserId);
                    rr.sHospitalName = strHospitalName;
                    //bill update 2013-8-14  生效日期显示
                    rr.StartTime = map_SaleStart.get(UserId+strHospitalName);
                    list_Report.add(rr);
                    if(list_Report.size() ==1000){
                    	return;
                    }
                }
            }else if(map_SalesName.containsKey(UserId)){
                ReportRow rr = new ReportRow();
                rr.sSalesName = map_SalesName.get(UserId);
                list_Report.add(rr);
                if(list_Report.size() ==1000){
                    return;
                }
            }
        }
        system.debug('Row sizeeee:'+list_Report.size());
    }
    //初始化邮件发送主管的信息
    private void initSupervisorInfo(ID SupervisorId){
        if(SupervisorId == null){
            return ;
        }
        sUser = [Select Id,Name,Alias,UserRoleId From User Where Id =: SupervisorId] ;
        this.initSubUsers(sUser.UserRoleId);
    }
    //初始化主管下属信息
    private void initSubUsers(ID RoleId){
        V2_UtilClass uc = new V2_UtilClass();
        list_SalesUserIds = uc.getSubordinateIds(RoleId);
        //BILL UPDATE 2013-8-14 用户状态必须为启用
        for(User u : [Select Id,Alias From User Where Id in: list_SalesUserIds and IsActive = true]){
            map_SalesName.put(u.Id , u.Alias);
        }
        this.initSalesHospitalRelation(list_SalesUserIds);
    }
    //初始化该主管下属的销售医院关系
    private void initSalesHospitalRelation(List<ID> list_SalesUserIds){
    	Date LastDate = date.valueOf('2012-1-1');
        system.debug(list_SalesUserIds);
        for(V2_Account_Team__c at : [Select Id,V2_Account__c,V2_Account__r.Name,V2_User__c,V2_LastAccessDate__c From V2_Account_Team__c Where 
            ((V2_BatchOperate__c = '新增' And EffectiveDate__c < TODAY And V2_ApprovalStatus__c = '审批通过' And V2_History__c = false) 
            Or 
            (V2_BatchOperate__c = '替换' And DeleteDate__c > TODAY And V2_ApprovalStatus__c = '审批通过' And V2_History__c = false) 
            Or 
            (V2_BatchOperate__c = '删除' And DeleteDate__c > TODAY And V2_History__c = false)
            Or 
            (V2_BatchOperate__c = '新增' And V2_LastAccessDate__c < TODAY And V2_ApprovalStatus__c = '审批通过' And V2_History__c = false)
            //Or
            //(V2_LastAccessDate__c = : LastDate and V2_History__c = false)
            ) 
            And 
            V2_User__c in: list_SalesUserIds
            ]){
            Set<String> set_HospitalName = new Set<String>();
            if(map_SalesHospital.containsKey(at.V2_User__c)){
                set_HospitalName = map_SalesHospital.get(at.V2_User__c);
            }
            //bill update 2013-8-14 生效日期赋值
            if(at.V2_LastAccessDate__c != null)
            {
            	map_SaleStart.put(at.V2_User__c+at.V2_Account__r.Name,string.valueOf(at.V2_LastAccessDate__c));
            }else{
            	map_SaleStart.put(at.V2_User__c+at.V2_Account__r.Name,'');
            }
            set_HospitalName.add(at.V2_Account__r.Name);
            map_SalesHospital.put(at.V2_User__c , set_HospitalName);
        }
        system.debug(map_SalesHospital.size() + ' sssssizze ');
    }
    
    static testMethod void myUnitTest() {
		//----------------New UserRole ------------------
    	//List<UserRole> list_userRole = new List<UserRole >() ;
    	UserRole objUserRole = new UserRole() ;
    	objUserRole.Name = 'Renal-Rep-大上海-PD-Rep(陈喆令)' ;
    	insert objUserRole ;
    	UserRole objUserRole2 = new UserRole() ;
    	objUserRole2.Name = 'Renal-Rep-大上海-PB-Rep(陈喆令)' ;
    	objUserRole2.ParentRoleId = objUserRole.Id ;
    	insert objUserRole2 ;
    	UserRole objUserRole3 = new UserRole() ;
    	objUserRole3.Name = 'Renal-Rep-大上海-PC-Rep(陈喆令)' ;
    	objUserRole3.ParentRoleId = objUserRole.Id ;
    	insert objUserRole3 ;
		//----------------Create User-------------
    	List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.UserRoleId = objUserRole2.Id ;
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	use2.UserRoleId = objUserRole3.Id ;
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.UserRoleId = objUserRole.Id ;
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
        
        List<V2_Account_Team__c> list_V2_Account = new List<V2_Account_Team__c>();
        V2_Account_Team__c v2_Account = new V2_Account_Team__c();
        v2_Account.V2_User__c = use1.Id;
        v2_Account.V2_BatchOperate__c = '新增';
        v2_Account.EffectiveDate__c = date.today().addDays(-1);
        v2_Account.V2_ApprovalStatus__c = '审批通过';
        v2_Account.V2_History__c = false;
        list_V2_Account.add(v2_Account);
        V2_Account_Team__c v2_Account1 = new V2_Account_Team__c();
        v2_Account1.V2_User__c = use2.Id;
        v2_Account1.V2_BatchOperate__c = '删除';
        v2_Account1.DeleteDate__c = date.today().addDays(1);
        v2_Account1.V2_History__c = false;
        list_V2_Account.add(v2_Account1);
        insert list_V2_Account;
        
        system.Test.startTest();
        PageReference pr = ApexPages.currentPage();
        pr.getHeaders().put('ReportToSupervisorId',use3.Id);
		Ctrl_SalesHospitalRelationReport ctrl = new Ctrl_SalesHospitalRelationReport();
		ctrl.ReportToSupervisorId = use3.Id;
		ctrl.initSupervisorInfo(use3.Id);
		ctrl.buildReportRow();
        system.Test.stopTest();
    }
}