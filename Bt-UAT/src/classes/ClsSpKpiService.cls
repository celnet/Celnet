/**
 * 作者：Sunny Sun
 * 说明：SEP SP KPI计算
 * 2014-1-14 Sunny 调整：
 * 1.SP部门进行中业务机会阶段调整
 * 2.SP代表新增考核<业务机会策略评估>
 * 3.SP代表取消考核<科室会数量>
**/
public  class ClsSpKpiService {
    ID UserId ;
    Integer IntYear ;
    Integer IntMonth ;
    
    public ClsSpKpiService(ID sUserId,Integer sIntYear,Integer sIntMonth){
        UserId = sUserId;
        IntYear = sIntYear;
        IntMonth = sIntMonth ; 
    }
    
    //REP KPI
    
    //KPI:医院潜力分析完成率
    //说明：代表所负责医院中有SP医院信息的医院数量/代表负责的所有医院数量
    public Map<String , Double> HospitalCapacity(){
        Map<String , Double> map_Result = new Map<String , Double>();
        
        //负责的SP医院数量
        Double SpHospitalNum;
        //负责的医院数量
        Double HospitalNum;
        //获取用户所负责的医院ID
        List<ID> list_HospitalId = getValidAccountTeam_New(UserId);
        //所负责医院数量
        HospitalNum = list_HospitalId.size();
        //所负责SP医院数量
        for(Account Hospital : [Select Id,(Select Id From ACCHospitalInfo__r Where Year__c =: String.valueOf(IntYear)) From Account Where Id in: list_HospitalId]){
            if(Hospital.ACCHospitalInfo__r != null && Hospital.ACCHospitalInfo__r.size() > 0){
                SpHospitalNum = (SpHospitalNum==null?0:SpHospitalNum) + 1;
            }
        }
        map_Result.put('Target' , HospitalNum);
        map_Result.put('Finish' , SpHospitalNum);
        if(HospitalNum != null && SpHospitalNum != null && HospitalNum != 0){
            //return SpHospitalNum/HospitalNum ;
            map_Result.put('Rate' , SpHospitalNum/HospitalNum);
        }else {
            //return 0;
            map_Result.put('Rate' , 0);
        }
        return map_Result;
    }
    //Storm提供，获取有效的医院ID，即在指定月份依旧在负责的医院的ID集合
    private List<ID> getValidAccountTeam_New(ID UserId){
        //1. 产品=SP(UserProduct__c='SP')
        //2. 失效年份或失效月份=''(V2_Delete_Month__c,V2_Delete_Year__c)
        List<ID> list_AccId = new List<ID>();
        for(V2_Account_Team__c accountTeam : [select Id,V2_Account__c from V2_Account_Team__c 
         where (UserProduct__c='SP' or V2_UserProduct__c='SP') and 
         (V2_Delete_Year__c=null and V2_Delete_Month__c=null) and V2_History__c=null 
         and V2_User__c =: UserId])
        {
         list_AccId.add(accountTeam.V2_Account__c);
        }
        return list_AccId;
    }
    /*
    //获取有效的医院ID，即在指定月份依旧在负责的医院的ID集合
    private List<ID> getValidAccountTeam(ID UserId,Integer IntYear,Integer IntMonth){
        List<V2_Account_Team__c> list_AccTeam = new List<V2_Account_Team__c>();
        List<ID> list_AccId = new List<ID>();
        Date CheckDate = date.valueOf(IntYear+'-'+IntMonth+'-1');
        for(V2_Account_Team__c accountTeam : [Select v.V2_NewAccUser__c, v.V2_History__c, v.V2_Account__c, v.Id, V2_User__c, v.V2_BatchOperate__c,
           V2_Effective_Year__c, V2_Effective_Month__c, V2_Delete_Year__c, V2_Is_Delete__c, V2_Delete_Month__c, V2_ApprovalStatus__c
           From V2_Account_Team__c v 
           Where (V2_NewAccUser__c = null And V2_User__c =: UserId And V2_ApprovalStatus__c = '审批通过' And V2_History__c = false And V2_BatchOperate__c = '新增') 
           Or (V2_NewAccUser__c != null And V2_User__c =: UserId And V2_History__c = false And V2_BatchOperate__c = '替换')
           Or (V2_User__c =: UserId And V2_History__c = false And V2_BatchOperate__c = '删除')
           ]){
            if(accountTeam.V2_History__c==null){
                continue;
            }
            if(accountTeam.V2_BatchOperate__c == '新增'){
                //新增的销售医院关系，审批通过，而且不为历史状态
                //判断起效月份是否在要检查月份之前，若是则为有效的
                Date EffectiveDate = date.valueOf(accountTeam.V2_Effective_Year__c+'-'+accountTeam.V2_Effective_Month__c+'-1');
                if(EffectiveDate <= CheckDate){
                    list_AccTeam.add(accountTeam);
                    list_AccId.add(accountTeam.V2_Account__c);
                }
            }else if(accountTeam.V2_BatchOperate__c == '替换'){
                //替换的销售医院关系，但是记录不为历史状态，则表示可能被申请替换了，但是起效
                //判断失效月份是否在要检查的月份之后，若是则为有效的
                Date DeleteDate = date.valueOf(accountTeam.V2_Delete_Year__c+'-'+accountTeam.V2_Delete_Month__c+'-1');
                if(DeleteDate > CheckDate){
                    list_AccTeam.add(accountTeam);
                    list_AccId.add(accountTeam.V2_Account__c);
                }
            }else if(accountTeam.V2_BatchOperate__c == '删除'){
                //删除的销售医院关系，但是记录不为历史状态
                //若尚未审批通过，则表示为有效的
                if(accountTeam.V2_ApprovalStatus__c != '审批通过' ){
                    list_AccTeam.add(accountTeam);
                    list_AccId.add(accountTeam.V2_Account__c);
                }else if(accountTeam.V2_ApprovalStatus__c == '审批通过'){
                    //若审批通过了，判断失效月份是否在要检查的月份之后，若是则为有效的
                    Date DeleteDate = date.valueOf(accountTeam.V2_Delete_Year__c+'-'+accountTeam.V2_Delete_Month__c+'-1');
                    if(DeleteDate > CheckDate){
                        list_AccTeam.add(accountTeam);
                        list_AccId.add(accountTeam.V2_Account__c);
                    }
                }
            }
        }
        return list_AccId;
    }
    */
    
    //KPI：业务机会数
    //说明：当月进行中的业务机会。要求3个。
    public Map<String , Double> OpportunityQuantity(){
        Map<String , Double> map_Result = new Map<String , Double>();
        
        Set<ID> set_ValidOppId = new Set<ID>();
        //获取该月份用户所负责的业务机会
        Set<ID> set_OppId = V2_UtilClass.GetOpportunity(IntYear,IntMonth,UserId);
        Date CheckDate = date.valueOf(IntYear+'-'+IntMonth+'-1');
        Date endMonthDate = CheckDate.addMonths(1);
        
        //一直在进行中的业务机会
        //2014-1-14Sunny修改业务机会进行中的阶段
        //List<String> list_OppStage = new List<String>{'发现机会'};
        List<String> list_OppStage = new List<String>{'发现机会','信息收集和分析','确定目标','建立沟通渠道','需求分析和行动方案','导入理念','实践使用'};
        for(Opportunity opp : [Select Id,Renal_Opp_MGT__c From Opportunity 
                Where StageName in: list_OppStage 
                And (RecordType.DeveloperName = 'ACC' Or RecordType.DeveloperName = 'ACC_Supervisor')  
                And CloseDate >=: CheckDate
                And Id in: set_OppId
                And ApproveStatus__c = '通过'
                And CreatedDate <: endMonthDate]){
            //if(opp.Renal_Opp_MGT__c == '接受' ||  opp.Renal_Opp_MGT__c == null){
                set_ValidOppId.add(opp.Id);
            //}
        }
        //对应月份曾经在进行中的业务机会
        for(OpportunityHistory__c OppHistory : [Select Id,Opportunity__c,Opportunity__r.Renal_Opp_MGT__c From OpportunityHistory__c 
                Where ChangedDate__c >=: CheckDate 
                And ChangedDate__c <: endMonthDate 
                And (NewOppStage__c in: list_OppStage OR PastOppStage__c in: list_OppStage)
                And (Opportunity__r.RecordType.DeveloperName = 'ACC' Or Opportunity__r.RecordType.DeveloperName = 'ACC_Supervisor' )
                And Opportunity__r.CloseDate >=: CheckDate
                And Opportunity__r.ApproveStatus__c = '通过'
                And Opportunity__c in: set_OppId]){
            //if(OppHistory.Opportunity__r.Renal_Opp_MGT__c == '接受' ||  OppHistory.Opportunity__r.Renal_Opp_MGT__c == null){
                set_ValidOppId.add(OppHistory.Opportunity__c);
            //}
        }
        map_Result.put('Target' , 3);
        map_Result.put('Finish' , set_ValidOppId.size());
        return map_Result ;
    }
    
    //KPI:业务机会策略评分
    //进行中业务机会的业务机会策略评分的平均分
    public Map<String , Double> OppEvaluationScore(){
    	Map<String , Double> map_Result = new Map<String , Double>();
    	Date CheckDate = date.valueOf(IntYear+'-'+IntMonth+'-1');
        Date endMonthDate = CheckDate.addMonths(1);
    	//获取进行中的业务机会ID
    	Set<ID> set_ValidOppId = new Set<ID>();
    	//参考IVT的逻辑，只需要看一直在进行中的业务机会,并且不需要看业务机会的结束时间
    	List<String> list_OppStage = new List<String>{'发现机会','信息收集和分析','确定目标','建立沟通渠道','需求分析和行动方案','导入理念','实践使用'};
        for(Opportunity opp : [Select Id,Renal_Opp_MGT__c From Opportunity 
                Where StageName in: list_OppStage 
                And (RecordType.DeveloperName = 'ACC' Or RecordType.DeveloperName = 'ACC_Supervisor')  
                And CloseDate >=: CheckDate
                //And Id in: set_OppId
                And OwnerId =: UserId
                And ApproveStatus__c = '通过'
                And CreatedDate <: endMonthDate]){
            set_ValidOppId.add(opp.Id);
        }
    	//策略评分平均分
        Double ScoreAVG = 0 ;
        Double ScoreTotal = 0;
        Double num = 0;
        //获取有效的业务机会策略评分
        for(OppEvaluation__c OppEva : [Select Id,Score__c From OppEvaluation__c Where Opportunity__c in: set_ValidOppId And Score__c != null And Year__c =: String.valueOf(IntYear) And Month__c =: String.valueOf(IntMonth)]){
            ScoreTotal += Double.valueOf(OppEva.Score__c);
            num++;
        }
        if(num != 0){
             ScoreAVG = ScoreTotal/num;
        }
        map_Result.put('TotalScore' , ScoreTotal);
        map_Result.put('AVGScore' , ScoreAVG);
        map_Result.put('Num' , num);
        return map_Result ;
    }
    
    //KPI：拜访数量
    //每月所有的已完成未过期的拜访数总和。要求80个。
    public Map<String , Double> VisitQuantity(){
        Map<String , Double> map_Result = new Map<String , Double>();
        
        List<Event> list_Visit = getValidVisit();
        map_Result.put('Target' , 80);
        map_Result.put('Finish' , list_Visit.size());
        
        return map_Result;
    }
    private List<Event> getValidVisit(){
        //已完成、未过期的拜访
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        List<Event> list_Visit=[Select Id,GAPlan__c,GAExecuteResult__c From Event Where IsChild = false And RecordType.DeveloperName = 'V2_Event' And Done__c = true And V2_IsExpire__c = false And StartDateTime >: CheckStartDateTime And StartDateTime <: CheckEndDateTime And OwnerId =: UserId];
        return list_Visit ;
    }
    
    //KPI:科室会数量
    //组织召开科室会的数量
    //2013-3-28 sunny 修改，科室会添加限制条件，要求是“已完成”
    //2014-1-14 Sunny ,该KPI2014 SP部门不再进行考核
    public Map<String , Double> DepartmentVisitQuantity(){
        Map<String , Double> map_Result = new Map<String , Double>();
        
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        List<Event> list_DepartmentVisit=[Select Id From Event Where RecordType.DeveloperName = 'V2_RecordType' And Done__c = true And SubjectType__c = '科室会' And StartDateTime >: CheckStartDateTime And StartDateTime <: CheckEndDateTime And OwnerId =: UserId];
        
        map_Result.put('Target' , 1);
        map_Result.put('Finish' , list_DepartmentVisit.size());
        return map_Result;
    }
    
    //KPI:访前计划及访后分析完成率
    //当月已完成的未过期的拜访中填写访前计划和访后分析的比例((访前计划完成%+访后分析完成%)/2)
    public Map<String , Double> VisitPlanRate(){
        Map<String , Double> map_Result = new Map<String , Double>();
        //拜访次数
        double VisitNum;
        //访前计划次数
        double VisitPlanNum;
        //访后分析次数
        double VisitAnalysisNum;
        //访前计划完成率
        double VisitPlan;
        //访后反洗完成率
        double VisitAnalysis;
        
        List<Event> list_Visit = getValidVisit();
        VisitNum = list_Visit.size() ;
        for(Event objEvent : list_Visit){
            if(objEvent.GAPlan__c != null){
                VisitPlanNum = (VisitPlanNum==null?0:VisitPlanNum) + 1 ;
            }
            if(objEvent.GAExecuteResult__c != null){
                VisitAnalysisNum = (VisitAnalysisNum==null?0:VisitAnalysisNum) + 1 ;
            }
        }
        
        if(VisitNum != null && VisitPlanNum != null){
            VisitPlan = VisitPlanNum / VisitNum ;
        }
        if(VisitNum != null && VisitAnalysisNum != null){
            VisitAnalysis = VisitAnalysisNum / VisitNum;
        }
        map_Result.put('Target' , VisitNum);
        map_Result.put('Finish Before' , VisitPlanNum);
        map_Result.put('Finish After' , VisitAnalysisNum);
        map_Result.put('Rate' , ((VisitPlan==NULL?0:VisitPlan)+(VisitAnalysis==NULL?0:VisitAnalysis))/2);
        
        return  map_Result;
    }
    
    //KPI:拜访质量评分
    //主管协访后，在协访中根据拜访质量标准给予代表评分的平均分
    //此处取当月有效拜访中为协访的。
    public Map<String , Double> VisitGrade(){
        Map<String , Double> map_Result = new Map<String , Double>();
        List<Event> list_Visit = getValidVisit();
        List<ID> list_EventId = new List<ID>();
        for(Event eve : list_Visit){
            list_EventId.add(eve.Id);
        }
        Double Grade = 0;
        
        List<AssVisitComments__c> list_VisitComm =[Select Id,Grade__c,Comment__c From AssVisitComments__c Where EventId__c in: list_EventId And BeReviewed__c =: UserId And Grade__c != null];
        system.debug('Eve Ids :'+list_EventId +' ~ '+list_VisitComm); 
        for(AssVisitComments__c VisitComm : list_VisitComm){
                Grade = Grade + Integer.valueOf(VisitComm.Grade__c);
        }
        map_Result.put('TotalSorce' , Grade);
        map_Result.put('TotalNum' , list_VisitComm.size());
        
        if(list_VisitComm.size()>0){
            map_Result.put('AVGSorce' , Grade/list_VisitComm.size());
        }else{
            map_Result.put('AVGSorce' , 0);
        }
        return map_Result;
    }
    
    //主管 KPI
    
    //KPI：直接管理的销售团队的平均SEP业绩
    //直接管理的销售团队的平均SEP业绩
    public Map<String , Double> SepTeamPerformanceAVG(Boolean IsHaveHospital , Double SuperValue){
        Map<String , Double> map_Result = new Map<String , Double>();
        Double AVGGrade = 0;
        //直接汇报关系的下属
        List<ID> list_uId = new List<ID>();
        //查询所有在查询月份直接汇报下属
        for(V2_RoleHistory__c RoleHistory : [Select v.Role__c, v.Manager__c, v.Renal_valid_super__c, v.Name__c, v.IsOnHoliday__c, v.IsLeave__c, v.Department__c, v.PG__c, v.Level__c
            From V2_RoleHistory__c v 
            Where Month__c =: String.valueOf(intMonth) And Year__c =: String.valueOf(intYear) And IsOnHoliday__c = false And IsLeave__c = false And Status__c != '停用' And Manager__c=:UserId]){
            list_uId.add(RoleHistory.Name__c);
        }
        //下属人数
        Double TotalNum = 0 ;
        //下属得分
        Double TotalGrade = 0 ;
        //直接下属的奖金评分
        //2013-4-23 Sunny修改bug，查找奖金数据添加年月的条件
        for(Bonus_data__c bd:[Select Id,Total_Score__c From Bonus_data__c Where The_User__c in: list_uId And Month__c =: String.valueOf(intMonth) And Year__c =: String.valueOf(intYear)]){
            TotalNum++;
            
            TotalGrade = TotalGrade + (bd.Total_Score__c==null?0:bd.Total_Score__c);
        }
        system.debug(IsHaveHospital+'xiashu'+TotalGrade+' - '+TotalNum);
        if(IsHaveHospital == true){
            TotalNum+=1;
            TotalGrade += (SuperValue==null?0:SuperValue) ;
        }
        if(TotalNum!=0){
            AVGGrade = TotalGrade / TotalNum;
        }
        map_Result.put('TotalGrade' , TotalGrade);
        map_Result.put('TotalNum' , TotalNum);
        map_Result.put('AVGGrade' , AVGGrade);
        
        system.debug('pingjunfen'+map_Result);
        return map_Result ;
    }
    
    //KPI:业务机会策略评分完成率
    //主管对代表当月进行中的业务机会评分并评语的比例；
    //已经被评分评语的代表当月进行中的业务机会数/代表当月进行中的业务机会总数；
    public Map<String , Double> OppEvaluationRate(){
    	Map<String , Double> map_Result = new Map<String , Double>();
        Date endCheckDate ;
        if(IntMonth == 12){
        	endCheckDate = date.valueOf((IntYear+1)+'-'+'1'+'-1');
        }else{
        	endCheckDate = date.valueOf(IntYear+'-'+(IntMonth+1)+'-1');
        }
        
        Set<ID> set_OppId = new Set<ID>();
        //获取直接下属ID
        Set<ID> list_uid = new Set<ID>();
        for(V2_RoleHistory__c RoleHistory : [Select v.Role__c, v.Manager__c, v.Renal_valid_super__c, v.Name__c, v.IsOnHoliday__c, v.IsLeave__c, v.Department__c, v.PG__c, v.Level__c
            From V2_RoleHistory__c v 
            Where Month__c =: String.valueOf(intMonth) And Year__c =: String.valueOf(intYear) And IsOnHoliday__c = false And IsLeave__c = false And Manager__c=:UserId]){
            list_uId.add(RoleHistory.Name__c);
        }
        //下属对应月份负责的业务机会
        //Set<ID> set_OppIds = V2_UtilClass.GetOpportunity(IntYear,IntMonth,list_uid);
        //进行中的业务机会阶段
        List<String> list_OppStage = new List<String>{'发现机会','信息收集和分析','确定目标','建立沟通渠道','需求分析和行动方案','导入理念','实践使用'};
        for(Opportunity opp : [Select Id,Renal_Opp_MGT__c From Opportunity 
                Where StageName in: list_OppStage 
                And (RecordType.DeveloperName = 'ACC' Or RecordType.DeveloperName = 'ACC_Supervisor')  
                And OwnerId in: list_uId
                And ApproveStatus__c = '通过'
                And CloseDate >=: endCheckDate
                And CreatedDate <: endCheckDate]){
            
            set_OppId.add(opp.Id);
            
        }
        //
        Set<ID> set_eids = new Set<ID>();
        for(OppEvaluation__c oppEva: [Select Id,Score__c,Opportunity__c From OppEvaluation__c Where Opportunity__c in: set_OppId And Score__c!= null And Commentator__c =: UserId And Year__c =: String.valueOf(IntYear) And Month__c =: String.valueOf(IntMonth)]){
        	set_eids.add(oppEva.Opportunity__c);
        }
        system.debug('opp shuliang:'+set_eids.size());
        map_Result.put('Target' , set_OppId.size());
        map_Result.put('Finish' , set_eids.size());
        if(set_OppId.size() == 0){
            map_Result.put('Rate' , 0);
        }else{
            map_Result.put('Rate' , Double.valueOf(set_eids.size()) / set_OppId.size());
        }
        return  map_Result; 
    }
    
    //KPI:协访数量
    //当月主管的协访数。要求24个。
    public Map<String , Double> AssVisitQuantity(){
        Map<String , Double> map_Result = new Map<String , Double>();
        //拜访的起止时间
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        //接受邀请的拜访协访事件
        Set<ID> set_EventId = new Set<ID>();
        //收到邀请，但未接受的协访ID
        Set<ID> set_NotAccEvent = new Set<ID>();
        //找到被邀请的，已完成的拜访
        for(EventRelation er : [Select EventId,Status From EventRelation Where IsInvitee = true And Event.RecordType.DeveloperName = 'V2_Event' And Event.Done__c = true And RelationId =: UserId And Event.StartDateTime >: CheckStartDateTime And Event.StartDateTime <: CheckEndDateTime]){
            if(er.Status=='已接受' || er.Status=='Accepted'){
                set_EventId.add(er.EventId) ;
            }else{
                set_NotAccEvent.add(er.EventId);
            }
        }
        //SP部门要求接受协访，或者填写评语都算有效协访，
        //所以此处需要找该主管填写过评语的拜访
        Date startDate = date.valueOf(IntYear+'-'+IntMonth+'-1');
        Date endDate = startDate.addMonths(2);
        
        Set<ID> set_UEventId = new Set<ID>();
        for(AssVisitComments__c AssVisitComm : [Select Id,EventId__c,IsAssVisit__c From AssVisitComments__c Where CreatedDate >=: startDate And CreatedDate <: endDate And ReUser__c =: UserId]){
            //Set_AssVisitId.add(AssVisitComm.EventId__c);
            //如果填写了点评，协访是收到邀请，但未接受的，是为有效协访
            if(set_NotAccEvent.contains(AssVisitComm.EventId__c) ){
                set_EventId.add(AssVisitComm.EventId__c);
            }else if(AssVisitComm.IsAssVisit__c){
                //勾选了 “是否接受协访” 按钮的点评,需要进一步判断协访的日期。
                set_UEventId.add(AssVisitComm.EventId__c);
            }
        }
        for(Event e: [Select Id,GAPlan__c,WhoId,GAExecuteResult__c,Key_Information__c From Event Where Id in: set_UEventId And RecordType.DeveloperName = 'V2_Event' And Done__c = true And StartDateTime >: CheckStartDateTime And StartDateTime <: CheckEndDateTime]){
            set_EventId.add(e.Id);
        }
        //for(AssVisitComments__c AssVisitComm : [Select Id,EventId__c From AssVisitComments__c Where EventId__c in: list_EventNoAccept And ReUser__c =: UserId]){
        //  set_EventId.add(AssVisitComm.EventId__c);
        //}
        map_Result.put('Target' , 24);
        map_Result.put('Finish' , set_EventId.size());
        
        return map_Result ;
    }
    
    //KPI:协访质量(二级评分)
    //由其主管的主管对主管的协访质量进行评分和点评
    public Map<String , Double> VisitGradeLevelTwo(){
        Map<String , Double> map_Result = new Map<String , Double>();
        Double Grade ;
        Integer num =0;
        Date startDate = date.valueOf(IntYear+'-'+IntMonth+'-1');
        Date endDate = startDate.addMonths(2);
        
        //拜访的起止时间
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        Set<ID> set_UEventId = new Set<ID>();
        Map<ID,AssVisitComments__c> map_AssVisit = new Map<ID,AssVisitComments__c>();
        for(AssVisitComments__c AssVisitComm : [Select Id,EventId__c,Grade__c From AssVisitComments__c Where CreatedDate >=: startDate And CreatedDate <: endDate And BeReviewed__c =: UserId And Grade__c != null]){
            //Set_AssVisitId.add(AssVisitComm.EventId__c);
            set_UEventId.add(AssVisitComm.EventId__c);
            map_AssVisit.put(AssVisitComm.EventId__c , AssVisitComm);
        }
        for(Event e: [Select Id,GAPlan__c,WhoId,GAExecuteResult__c,Key_Information__c From Event Where Id in: set_UEventId And RecordType.DeveloperName = 'V2_Event' And Done__c = true And StartDateTime >: CheckStartDateTime And StartDateTime <: CheckEndDateTime]){
            //set_EventId.add(e.Id);
            if(map_AssVisit.containsKey(e.Id)){
                AssVisitComments__c AssVisitComm = map_AssVisit.get(e.Id) ;
                num++;
                Grade = (Grade==null?0:Grade) + Integer.valueOf(AssVisitComm.Grade__c) ;
            }
        }
        
        /*
        
        
        List<ID> list_EventId = new List<ID>();
        //找到被邀请的，已完成的拜访
        for(EventAttendee EventAtt : [Select e.Status, e.Event.Done__c, e.Event.OwnerId, e.Event.StartDateTime, e.EventId, e.AttendeeId From EventAttendee e Where Event.RecordType.DeveloperName = 'V2_Event' And Event.Done__c = true And AttendeeId =: UserId And Event.StartDateTime >: CheckStartDateTime And Event.StartDateTime <: CheckEndDateTime ]){
            list_EventId.add(EventAtt.EventId);
        }
        
        for(AssVisitComments__c AssVisitComm : [Select Id,EventId__c,Grade__c From AssVisitComments__c Where EventId__c in: list_EventId And BeReviewed__c =: UserId And Grade__c != null]){
            num++;
            Grade = (Grade==null?0:Grade) + Integer.valueOf(AssVisitComm.Grade__c) ;
        }
        */
        map_Result.put('TotalGrade' , Grade);
        map_Result.put('TotalNum' , num);
        
        if(Grade != null && num != 0){
            map_Result.put('AVGGrade' , Grade/num);
        }else{
            map_Result.put('AVGGrade' , 0);
        }
        return map_Result;
    }
    
     static testMethod void myUnitTest() {
        // TO DO: implement unit test
        //user role
        List<UserRole> listur= new List<UserRole>();
        UserRole ur1 = new UserRole();
        ur1.Name = 'IVT-Supervisor-华南-PD-Supervisor(U1)';
        listur.add(ur1);
        UserRole ur2 = new UserRole();
        ur2.Name = 'Renal-Rep-华南-PD-Rep(U2)';
        listur.add(ur2);
        insert listur;
        //user 
        list<User> user=[Select TimeZoneSidKey,ProfileId,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname, Alias From User Where IsActive = true and id =:userInfo.getUserId()];
        List<User> list_user = new List<User>();
        User user1=new User();
        user1.username='user1r2@123.com';
        user1.LastName='user1r2';
        user1.Email='user1r2@123.com';
        user1.Alias=user[0].Alias;
        user1.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user1.ProfileId=UserInfo.getProfileId();
        user1.LocaleSidKey=user[0].LocaleSidKey;
        user1.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user1.EmailEncodingKey=user[0].EmailEncodingKey;
        user1.CommunityNickname='chequ';
        user1.MobilePhone='12345678912';
        user1.IsActive = true;
        user1.UserRoleId=ur1.Id ;
        list_user.add(user1);
        User user2=new User();
        user2.username='user2r2@223.com';
        user2.LastName='user2r2';
        user2.Email='user2r2@223.com';
        user2.Alias=user[0].Alias;
        user2.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user2.ProfileId=UserInfo.getProfileId();
        user2.LocaleSidKey=user[0].LocaleSidKey;
        user2.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user2.EmailEncodingKey=user[0].EmailEncodingKey;
        user2.CommunityNickname='chequ1';
        user2.MobilePhone='22345678922';
        user2.IsActive = true;
        user1.UserRoleId=ur1.Id ;
        list_user.add(user2);
        insert list_user;
        
        //联系人
        List<Contact> listct= new List<Contact>();
        Contact ct1 = new Contact();
        ct1.FirstName = 'jim';
        ct1.LastName = 'Green';
        ct1.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='RecordType' And SobjectType = 'Contact'][0].Id;
        listct.add(ct1);
        Contact ct2 = new Contact();
        ct2.FirstName = 'Tom';
        ct2.LastName = 'Frank';
        ct2.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='RecordType' And SobjectType = 'Contact'][0].Id;
        listct.add(ct2);
        insert listct;
        
        //业务机会
        List<Opportunity> List_Opportunity = new List<Opportunity>();
        Opportunity opp = new Opportunity();
        opp.OwnerId = user1.Id;
        opp.Name = 'yewujihui';
        opp.StageName = '建立沟通渠道';
        opp.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='RENAL' And SobjectType = 'Opportunity'][0].Id;
        opp.CloseDate = date.valueOf(Datetime.now().year()+'-'+Datetime.now().month()+'-1');
        opp.Renal_Opp_MGT__c = '接受';
        List_Opportunity.add(opp);
        Opportunity opp1 = new Opportunity();
        opp1.OwnerId = user1.Id;
        opp1.Name = 'yewujihui1';
        opp1.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='RENAL' And SobjectType = 'Opportunity'][0].Id;
        opp1.StageName = '建立沟通渠道';
        opp1.CloseDate = date.valueOf(Datetime.now().year()+'-'+Datetime.now().month()+'-1');
        opp1.Renal_Opp_MGT__c = '接受';
        List_Opportunity.add(opp1);
        Opportunity opp2 = new Opportunity();
        opp2.OwnerId = user1.Id;
        opp2.Name = 'yewujihui2';
        opp2.StageName = '发现需求';
        opp2.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='IVT' And SobjectType = 'Opportunity'].Id;
        opp2.CloseDate = date.valueOf(Datetime.now().year()+'-'+Datetime.now().month()+'-1');
        opp2.Renal_Opp_MGT__c = '接受';
        List_Opportunity.add(opp2);
        insert List_Opportunity;
        
        //业务机会历史记录
        List<OpportunityHistory__c> List_OpportunityHistory = new List<OpportunityHistory__c>();
        OpportunityHistory__c oppc = new OpportunityHistory__c();
        oppc.ChangedDate__c = date.valueOf(Datetime.now().year()+'-'+Datetime.now().month()+'-2');
        oppc.NewOppStage__c ='需求分析';
        oppc.PastOppStage__c = '提交合作方案/谈判';
        oppc.Opportunity__c = opp.Id;
        List_OpportunityHistory.add(oppc);
        insert List_OpportunityHistory;
        
        //业务机会策略评估
        List<OppEvaluation__c> List_OppEvaluation = new list<OppEvaluation__c>();
        OppEvaluation__c oppe = new OppEvaluation__c();
        oppe.BeCommentUser__c = user1.Id;
        oppe.Opportunity__c = opp2.Id;
        oppe.Score__c = '4';
        oppe.Year__c = string.valueOf(Datetime.now().year());
        oppe.Month__c = string.valueOf(Datetime.now().month());
        List_OppEvaluation.add(oppe);
        OppEvaluation__c oppe1 = new OppEvaluation__c();
        oppe1.Opportunity__c = opp2.Id;
        oppe1.BeCommentUser__c = user1.Id;
        oppe1.Score__c = '3';
        oppe1.Year__c = string.valueOf(Datetime.now().year());
        oppe1.Month__c = string.valueOf(Datetime.now().month());
        List_OppEvaluation.add(oppe1);
        OppEvaluation__c oppe2 = new OppEvaluation__c();
        oppe2.Opportunity__c = opp2.Id;
        oppe2.BeCommentUser__c = user1.Id;
        oppe2.Score__c = '5';
        oppe2.Year__c = string.valueOf(Datetime.now().year());
        oppe2.Month__c = string.valueOf(Datetime.now().month());
        List_OppEvaluation.add(oppe2);
        insert List_OppEvaluation;
        
        //活动
        List<Event> list_Visit = new List<Event>();
        Event ev1 = new Event();
        ev1.DurationInMinutes = 1;
        ev1.StartDateTime = datetime.now();
        ev1.OwnerId = user1.Id ;
        ev1.GAPlan__c = '1111111';
        ev1.GAExecuteResult__c = '111111';
        ev1.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='V2_Event' And SobjectType = 'Event'][0].Id;
        ev1.Done__c = true;
        ev1.V2_IsExpire__c = false; 
        ev1.WhoId = ct1.Id;
        ev1.WhatId = opp.Id;
        list_Visit.add(ev1);
        Event ev2 = new Event();
        ev2.DurationInMinutes = 2;
        ev2.StartDateTime = datetime.now();
        ev2.OwnerId = user1.Id ;
        ev2.GAPlan__c = '222222';
        ev2.GAExecuteResult__c = '222222';
        ev2.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='V2_Event' And SobjectType = 'Event'][0].Id;
        ev2.Done__c = true;
        ev2.V2_IsExpire__c = false; 
        ev2.WhoId = ct1.Id;
        ev2.WhatId = opp.Id;
        list_Visit.add(ev2);
        Event ev3 = new Event();
        ev3.DurationInMinutes = 3;
        ev3.StartDateTime = datetime.now();
        ev3.OwnerId = user1.Id ;
        ev3.GAPlan__c = '333333';
        ev3.GAExecuteResult__c = '3333333';
        ev3.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='V2_Event' And SobjectType = 'Event'][0].Id;
        ev3.Done__c = true;
        ev3.V2_IsExpire__c = false; 
        ev3.WhoId = ct1.Id;
        ev3.WhatId = opp.Id;
        list_Visit.add(ev3);
        Event ev4 = new Event();
        ev4.DurationInMinutes = 4;
        ev4.StartDateTime = datetime.now();
        ev4.OwnerId = user1.Id ;
        ev4.GAPlan__c = '333333';
        //ev4.GAExecuteResult__c = '3333333';
        ev4.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='V2_RecordType' And SobjectType = 'Event'][0].Id;
        ev4.Done__c = true;
        ev4.V2_IsExpire__c = false; 
        ev4.WhoId = ct2.Id;
        ev4.WhatId = opp.Id;
        ev4.SubjectType__c = '科室会';
        list_Visit.add(ev4);
        insert list_Visit ;
        
        //协防事件
        //List<EventAttendee> list_EventAttendee = new List<EventAttendee>();
        //EventAttendee eva = new EventAttendee();
        //eva.EventId = ev1.Id;
        //eva.AttendeeId = user1.Id;
        //eva.Status = '已接受';
        //list_EventAttendee.add(eva);
        //insert list_EventAttendee;
        
        //月计划
        List<MonthlyPlan__c> List_MonthlyPlan = new List<MonthlyPlan__c>();
        MonthlyPlan__c mc1 = new MonthlyPlan__c();
        mc1.OwnerId = user1.Id;
        mc1.Year__c = string.valueOf(datetime.now().year());
        mc1.Month__c = string.valueOf(datetime.now().month());
        //mc1.V2_TotalCallRecords__c = 10;
        List_MonthlyPlan.add(mc1);
        insert List_MonthlyPlan;
        
        //月计划明细
        List<MonthlyPlanDetail__c> List_MonthlyPlanDetail = new List<MonthlyPlanDetail__c>();
        MonthlyPlanDetail__c mcc1 = new MonthlyPlanDetail__c();
        mcc1.Contact__c = ct1.Id;
        mcc1.Planned_Finished_Calls__c = 5;
        mcc1.MonthlyPlan__c = mc1.id;
        mcc1.AdjustedTimes__c = 10;
        List_MonthlyPlanDetail.add(mcc1);
        MonthlyPlanDetail__c mcc2 = new MonthlyPlanDetail__c();
        mcc2.Contact__c = ct1.Id;
        mcc2.Planned_Finished_Calls__c = 5;
        mcc2.MonthlyPlan__c = mc1.Id;
        mcc2.AdjustedTimes__c = 10;
        List_MonthlyPlanDetail.add(mcc2);
        MonthlyPlanDetail__c mcc3 = new MonthlyPlanDetail__c();
        mcc3.Contact__c = ct1.Id;
        mcc3.Planned_Finished_Calls__c = 10;
        mcc3.MonthlyPlan__c = mc1.Id;
        mcc3.AdjustedTimes__c = 10;
        List_MonthlyPlanDetail.add(mcc3);
        insert List_MonthlyPlanDetail;
        
        //协访质量点评
        List<AssVisitComments__c> List_AssVisitComments = new List<AssVisitComments__c>();
        AssVisitComments__c ac = new AssVisitComments__c();
        ac.EventId__c = ev1.Id;
        ac.BeReviewed__c = user1.Id;
        ac.Grade__c = '90';
        List_AssVisitComments.add(ac);
        AssVisitComments__c ac1 = new AssVisitComments__c();
        ac1.EventId__c = ev2.Id;
        ac1.BeReviewed__c = user1.Id;
        ac1.Grade__c = '90';
        List_AssVisitComments.add(ac1);
        AssVisitComments__c ac2 = new AssVisitComments__c();
        ac2.EventId__c = ev3.Id;
        ac2.BeReviewed__c = user1.Id;
        ac2.Grade__c = '90';
        List_AssVisitComments.add(ac2);
        AssVisitComments__c ac3 = new AssVisitComments__c();
        ac3.EventId__c = ev4.Id;
        ac3.BeReviewed__c = user1.Id;
        ac3.Grade__c = '90';
        List_AssVisitComments.add(ac3);
        insert List_AssVisitComments;
        
        //Account
        List<Account> list_Account = new List<Account>();
        Account at = new Account();
        at.Name ='shuju';
        list_Account.add(at);
        Account at1 = new Account();
        at1.Name ='shuju1';
        list_Account.add(at1);
        Account at2 = new Account();
        at2.Name ='shuju2';
        list_Account.add(at2);
        insert list_Account;
        
        //SP医院信息
        List<CCHospitalInfo__c> list_CCHospitalInfo = new List<CCHospitalInfo__c>();
        CCHospitalInfo__c cch = new CCHospitalInfo__c();
        cch.Account__c = at.Id;
        list_CCHospitalInfo.add(cch);
        insert list_CCHospitalInfo;
        
        //
        List<V2_Account_Team__c> list_V2_Account_Team = new List<V2_Account_Team__c>();
        V2_Account_Team__c va = new V2_Account_Team__c();
        va.V2_User__c = user1.Id;
        va.V2_ApprovalStatus__c = '审批通过';
        va.V2_History__c = false;
        va.V2_BatchOperate__c = '新增';
        va.V2_Effective_Month__c =string.valueOf(datetime.now().month()-1);
        va.V2_Effective_Year__c =string.valueOf(datetime.now().Year());
        va.V2_Delete_Month__c =string.valueOf(datetime.now().month()+1);
        va.V2_Delete_Year__c =string.valueOf(datetime.now().Year());
        va.V2_Account__c = at.Id;
        list_V2_Account_Team.add(va);
        V2_Account_Team__c va1 = new V2_Account_Team__c();
        va.V2_NewAccUser__c = user2.Id;
        va1.V2_User__c = user1.Id;
        va1.V2_BatchOperate__c = '替换';
        va1.V2_History__c = false;
        va1.V2_Effective_Month__c =string.valueOf(datetime.now().month()-1);
        va1.V2_Effective_Year__c =string.valueOf(datetime.now().Year());
        va1.V2_Delete_Month__c =string.valueOf(datetime.now().month()+1);
        va1.V2_Delete_Year__c =string.valueOf(datetime.now().Year());
        va1.V2_Account__c = at.Id;
        list_V2_Account_Team.add(va1);
        V2_Account_Team__c va2 = new V2_Account_Team__c();
        va2.V2_User__c = user1.Id;
        va2.V2_BatchOperate__c = '删除';
        va2.V2_History__c = false;
        va2.V2_Effective_Month__c =string.valueOf(datetime.now().month()-1);
        va2.V2_Effective_Year__c =string.valueOf(datetime.now().Year());
        va2.V2_Delete_Month__c =string.valueOf(datetime.now().month()+1);
        va2.V2_Delete_Year__c =string.valueOf(datetime.now().Year());
        va2.V2_Account__c = at.Id;
        list_V2_Account_Team.add(va2);
        V2_Account_Team__c va3 = new V2_Account_Team__c();
        va3.V2_User__c = user1.Id;
        va3.V2_History__c = false;
        va3.V2_BatchOperate__c = '删除';
        va3.V2_ApprovalStatus__c = '审批通过';
        va3.V2_Effective_Month__c =string.valueOf(datetime.now().month()-1);
        va3.V2_Effective_Year__c =string.valueOf(datetime.now().Year());
        va3.V2_Delete_Month__c =string.valueOf(datetime.now().month()+1);
        va3.V2_Delete_Year__c =string.valueOf(datetime.now().Year());
        va3.V2_Account__c = at.Id;
        list_V2_Account_Team.add(va3);
        insert list_V2_Account_Team;
        
        
        system.test.startTest();
        ClsSpKpiService cls = new ClsSpKpiService(user1.Id , datetime.now().year() , datetime.now().month());
         //销售代表KPI
        Map<String , Double> a = cls.HospitalCapacity();
        Map<String , Double> b = cls.OpportunityQuantity();
        Map<String , Double> c = cls.VisitQuantity();
        Map<String , Double> d = cls.DepartmentVisitQuantity();
        Map<String , Double> e = cls.VisitPlanRate();
        Map<String , Double> f = cls.VisitPlanRate();
        Map<String , Double> g = cls.VisitGrade();
        Map<String , Double> h = cls.SepTeamPerformanceAVG(true, 90);
        Map<String , Double> i = cls.OppEvaluationScore();
        //主管 KPI
        //直接管理的销售团队的平均SEP业绩
        //协访数量
        cls.AssVisitQuantity();
        //协访质量(二级评分)
        cls.VisitGradeLevelTwo();
        cls.OppEvaluationRate();
        system.debug(a+'@@'+b+'@@'+c+'@@'+d+'@@'+e+'@@'+f+'@@'+g+'@@');
        system.test.stopTest();
     }
    
}