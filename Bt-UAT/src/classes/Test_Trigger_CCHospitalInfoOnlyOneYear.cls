/**
 * author : bill create
 * date : 2013-8-15
 * CCHospitalInfoOnlyOneYear的测试类
 */
@isTest
private class Test_Trigger_CCHospitalInfoOnlyOneYear {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
                // TO DO: implement unit test
        Account acc= new Account();
        acc.Name = 'testing 2013-8-5';
        insert acc;       
        
        CCHospitalInfo__c sp = new CCHospitalInfo__c();
        sp.Account__c = acc.Id;
        sp.Year__c = '2013';
        system.Test.startTest();
        insert sp;
        CCHospitalInfo__c sp1 = new CCHospitalInfo__c();
        sp1.Account__c = acc.Id;
        sp1.Year__c = '2013';
        try{
        insert sp1;
        }catch(Exception e){}
        system.Test.stopTest();
    }
}