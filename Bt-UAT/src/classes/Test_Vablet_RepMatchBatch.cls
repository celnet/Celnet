/*
 * Author: Steven
 * Date: 2014-2-18
 * Description: 测试Vablet_RepMatchBatch
 */ 
@isTest
private class Test_Vablet_RepMatchBatch 
{
	static testMethod void testVabletRepMatchBatch()
	{
		// 准备数据
		List<User> uList = [Select Id, Email From User Where IsActive = true And Profile.UserLicense.Name = 'Salesforce'];
		Vablet_DocumentRetrival__c vdr1 = new Vablet_DocumentRetrival__c();
		vdr1.Vablet_Folder__c = 'RENAL/IVT/HD/PD/File';
		vdr1.Vablet_StartTime__c = Datetime.now();
		vdr1.Vablet_SalesEmail__c = uList[0].Email;
		Vablet_DocumentRetrival__c vdr2 = new Vablet_DocumentRetrival__c();
		vdr2.Vablet_Folder__c = 'RENAL/MD/CD/google';
		vdr2.Vablet_StartTime__c = Datetime.now();
		vdr2.Vablet_SalesEmail__c = uList[3].Email;
		Vablet_DocumentRetrival__c vdr3 = new Vablet_DocumentRetrival__c();
		vdr3.Vablet_Folder__c = 'RENAL/RE/GD/FX/GG/da/daa/xmf';
		vdr3.Vablet_StartTime__c = Datetime.now();
		vdr3.Vablet_SalesEmail__c = uList[1].Email;
		Vablet_DocumentRetrival__c vdr4 = new Vablet_DocumentRetrival__c();
		vdr4.Vablet_Folder__c = 'RENAL/DA/fold';
		vdr4.Vablet_StartTime__c = Datetime.now().addDays(2);
		vdr4.Vablet_SalesEmail__c = uList[5].Email;
		Vablet_DocumentRetrival__c vdr5 = new Vablet_DocumentRetrival__c();
		vdr5.Vablet_Folder__c = 'RENAL/RE/GD/FX/GG/da/xmf';
		vdr5.Vablet_StartTime__c = Datetime.now();
		vdr5.Vablet_SalesEmail__c = uList[1].Email;
		insert new List<Vablet_DocumentRetrival__c>{vdr1,vdr2,vdr3,vdr4,vdr5};
		
		// 执行Batch
		System.Test.startTest();
		Vablet_RepMatchBatch vrmb = new Vablet_RepMatchBatch();
		Database.executeBatch(vrmb);
		System.Test.stopTest();
		
		// 验证结果
		List<Vablet_DocumentRetrival__c> vList1 = [Select OwnerId, Vablet_Folder1__c, Vablet_Folder2__c, Vablet_Folder3__c, Vablet_Folder4__c, Vablet_Folder5__c From Vablet_DocumentRetrival__c Where Vablet_Folder__c = 'RENAL/IVT/HD/PD/File'];
		List<Vablet_DocumentRetrival__c> vList2 = [Select OwnerId, Vablet_Folder1__c, Vablet_Folder2__c, Vablet_Folder3__c, Vablet_Folder4__c, Vablet_Folder5__c From Vablet_DocumentRetrival__c Where Vablet_Folder__c = 'RENAL/MD/CD/google'];
		List<Vablet_DocumentRetrival__c> vList3 = [Select OwnerId, Vablet_Folder1__c, Vablet_Folder2__c, Vablet_Folder3__c, Vablet_Folder4__c, Vablet_Folder5__c From Vablet_DocumentRetrival__c Where Vablet_Folder__c = 'RENAL/DA/fold'];
		System.assertEquals(vList1[0].Vablet_Folder1__c, 'RENAL');
		System.assertEquals(vList1[0].Vablet_Folder2__c, 'IVT');
		System.assertEquals(vList1[0].Vablet_Folder3__c, 'HD');
		System.assertEquals(vList1[0].Vablet_Folder4__c, 'PD');
		System.assertEquals(vList1[0].Vablet_Folder5__c, 'File');
		System.assertEquals(vList2[0].Vablet_Folder1__c, 'RENAL');
		System.assertEquals(vList2[0].Vablet_Folder2__c, 'MD');
		System.assertEquals(vList2[0].Vablet_Folder3__c, 'CD');
		System.assertEquals(vList2[0].Vablet_Folder4__c, 'google');
		System.assertEquals(vList3[0].OwnerId, Userinfo.getUserId());
		System.assertEquals(vList1[0].OwnerId, uList[0].Id);
		System.assertEquals(vList2[0].OwnerId, uList[3].Id);
	}
}