/**
 * 作者：bill
 * 
 */
@isTest
private class Test_Trigger_VaporizerStatusHistory {

    static testMethod void myUnitTest() {
      
      //医院客户Account
      List<Account> list_acc = new List<Account>();
      Account acc = new Account();
      acc.Name = '第一医院';
      list_acc.Add(acc);
      Account acc1 = new Account();
      acc1.Name = '第二医院';
      list_acc.Add(acc1);
      insert list_acc;
      
      //联系人
      Contact con = new Contact();
      con.AccountId = acc.Id;
      con.LastName = 'zhang';
      insert con;
      
      //挥发罐信息
      List<VaporizerInfo__c> List_vap = new List<VaporizerInfo__c>();
        VaporizerInfo__c vap = new VaporizerInfo__c();
        vap.Name = 'bill001';
        vap.location__c ='上海仓库';
        vap.Status__c = '库存';
        List_vap.add(vap);
        VaporizerInfo__c vap1 = new VaporizerInfo__c();
        vap1.Name = 'bill002';
        vap1.location__c ='上海仓库';
        vap1.Status__c = '库存';
        vap1.Contact__c = con.Id;
        List_vap.add(vap1);
        
        VaporizerInfo__c vap2 = new VaporizerInfo__c();
        vap2.Name = 'bill002';
        vap2.location__c ='上海仓库';
        vap2.Status__c = '库存';
        
        system.Test.startTest();
        insert List_vap;
        try{
          insert vap2;
        }catch(Exception e){}
        
        try{
          update vap2;
        }catch(Exception e){}
        
        vap.location__c = '医院';
        vap.Status__c = '使用';
        vap.Hospital__c = acc.Id;
        update vap;
        
        vap.location__c = '仓库';
        vap.Status__c = '维修';
        update vap;
        
        vap.location__c = '医院';
        vap.Status__c = '使用';
        vap.Hospital__c = acc1.Id;
        update vap;
        system.Test.stopTest();
    }
}