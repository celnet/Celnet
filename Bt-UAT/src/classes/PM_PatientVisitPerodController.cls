/* Tobe 
 * 2013.9.24
 * 病人拜访周期设置页面控制类
 */
public class PM_PatientVisitPerodController 
{
	private static map<string,CallCycleConfig__c> Config =  CallCycleConfig__c.getAll();
	public integer ActivePatientCallCycle1{set;get;}
	public integer APDPatientCallCycle1{set;get;}
	public integer NewPatientCallCycle1{set;get;}
	public integer ActivePatientCallCycle2{set;get;}
	public integer APDPatientCallCycle2{set;get;}
	public integer NewPatientCallCycle2{set;get;}
	public integer ActivePatientVisitCycle{set;get;}
	public integer APDPatientVisitCycle{set;get;}
	public PM_PatientVisitPerodController()
	{
		if(Config.get('ActivePatientCallCycle').CallCycle1__c != null)ActivePatientCallCycle1 = integer.valueOf(Config.get('ActivePatientCallCycle').CallCycle1__c);
		if(Config.get('ActivePatientCallCycle').CallCycle2__c != null)ActivePatientCallCycle2 = integer.valueOf(Config.get('ActivePatientCallCycle').CallCycle2__c);
		if(Config.get('NewPatientCallCycle').CallCycle1__c != null)NewPatientCallCycle1 = integer.valueOf(Config.get('NewPatientCallCycle').CallCycle1__c);
		if(Config.get('NewPatientCallCycle').CallCycle2__c != null)NewPatientCallCycle2 = integer.valueOf(Config.get('NewPatientCallCycle').CallCycle2__c);		
		if(Config.get('APDPatientCallCycle').CallCycle1__c != null)APDPatientCallCycle1 = integer.valueOf(Config.get('APDPatientCallCycle').CallCycle1__c);
		if(Config.get('APDPatientCallCycle').CallCycle2__c != null)APDPatientCallCycle2 = integer.valueOf(Config.get('APDPatientCallCycle').CallCycle2__c);
		if(Config.get('ActivePatientVisitCycle').CallCycle1__c != null)ActivePatientVisitCycle = integer.valueOf(Config.get('ActivePatientVisitCycle').CallCycle1__c);
		if(Config.get('APDPatientVisitCycle').CallCycle1__c != null)APDPatientVisitCycle = integer.valueOf(Config.get('APDPatientVisitCycle').CallCycle1__c);
	}
	public void save()
	{
		if(ActivePatientCallCycle1 !=null )
		{
			Config.get('ActivePatientCallCycle').CallCycle1__c =integer.valueOf(ActivePatientCallCycle1);
		}
		if(ActivePatientCallCycle2 !=null )
		{
			Config.get('ActivePatientCallCycle').CallCycle2__c = integer.valueOf(ActivePatientCallCycle2);
		}
		
		if(NewPatientCallCycle1 !=null )
		{
			Config.get('NewPatientCallCycle').CallCycle1__c = integer.valueOf(NewPatientCallCycle1);
		}
		if(NewPatientCallCycle2 !=null )
		{
			Config.get('NewPatientCallCycle').CallCycle2__c = integer.valueOf(NewPatientCallCycle2);
		}
		if(APDPatientCallCycle1 !=null )
		{
			Config.get('APDPatientCallCycle').CallCycle1__c = integer.valueOf(APDPatientCallCycle1);
		}
		if(APDPatientCallCycle2 !=null )
		{
			Config.get('APDPatientCallCycle').CallCycle2__c = integer.valueOf(APDPatientCallCycle2);
		}
		if(ActivePatientVisitCycle !=null )
		{
			Config.get('ActivePatientVisitCycle').CallCycle1__c = integer.valueOf(ActivePatientVisitCycle);
		}
		if(APDPatientVisitCycle !=null )
		{
			Config.get('APDPatientVisitCycle').CallCycle1__c = integer.valueOf(APDPatientVisitCycle);
		}
		try
		{
       		update Config.values();
       		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, '保存成功!');
       		ApexPages.addMessage(myMsg);
	    }
	    catch(DmlException ex)
	    {
	        ApexPages.addMessages(ex);
	    }
		
	}
   
}