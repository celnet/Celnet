/**
 * AutoOppFocusChangedAccount的trigger的测试类
 */
@isTest
private class Test_Trigger_AutoOppFocusChangedAccount {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Account acc = new Account();
        acc.Name = 'zhangsan';
        insert acc;
        
        List<Opportunity> list_opp = new List<Opportunity>();
        Opportunity opp = new Opportunity();
        opp.Name = 'test123';
        opp.RecordTypeId = [Select r.Name, r.Id From RecordType r where r.Name in ('RENAL') and IsActive = true and r.SobjectType = 'Opportunity'][0].Id;
        opp.AccountId = acc.Id;
        opp.StageName = '导入期';
        opp.CloseDate = date.today();
        opp.IsFocusOpportunity__c = '否';
        list_opp.add(opp);
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'test1234';
        opp1.RecordTypeId = [Select r.Name, r.Id From RecordType r where r.Name in ('RENAL') and IsActive = true and r.SobjectType = 'Opportunity'][0].Id;
        opp1.AccountId = acc.Id;
        opp1.StageName = '导入期';
        opp1.CloseDate = date.today();
        opp1.IsFocusOpportunity__c = '否';
        list_opp.add(opp1);
        insert list_opp;
        
        system.Test.startTest();
        opp.IsFocusOpportunity__c = '是';
        update opp;
        opp.IsFocusOpportunity__c = '否';
        update opp;
        system.Test.stopTest();
        
        
    }
}