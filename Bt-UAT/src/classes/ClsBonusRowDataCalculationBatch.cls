/**
 * 作者：Sunny
 * 奖金数据计算Batch
 * 
**/
global class ClsBonusRowDataCalculationBatch implements Database.Batchable<sObject>{
	global Integer intMonth ;//计算的月份
	global Integer intYear ;//计算的年份
	//global Boolean CalculationBonus ;//标记是否要继续计算奖金
	global ID singleUserId ;
	global String strGBU ;
	
	
	//Map<ID , V2_RoleHistory__c> map_UserRoleHis = new Map<ID , V2_RoleHistory__c>();//用户角色历史记录（计算月的）
    global List<V2_RoleHistory__c> start(Database.BatchableContext BC){
    	//该月份角色历史中是代表的
    	List<V2_RoleHistory__c> list_RepUserRole = new List<V2_RoleHistory__c>();
    	//该月份角色历史中是主管的Supervisor
    	List<V2_RoleHistory__c> list_SuperUserRole = new List<V2_RoleHistory__c>();
    	//该月份角色历史中是DSM
    	List<V2_RoleHistory__c> list_dsmUserRole = new List<V2_RoleHistory__c>();
    	//该月份角色历史中是ASM
        List<V2_RoleHistory__c> list_asmUserRole = new List<V2_RoleHistory__c>();
        //该月份角色历史中是RSM
        List<V2_RoleHistory__c> list_rsmUserRole = new List<V2_RoleHistory__c>();
        //该月份角色历史中是更高的角色
        List<V2_RoleHistory__c> list_highUserRole = new List<V2_RoleHistory__c>();
        
    	Map<ID,V2_RoleHistory__c> map_UserManager = new Map<ID,V2_RoleHistory__c>();
    	//查询需计算奖金月份的角色历史，直接去掉该月份在休假、已离职的。
    	List<V2_RoleHistory__c> list_RoleHis ;
    	if(singleUserId != null){
    		list_RoleHis = [Select v.Role__c, v.Manager__c, v.Renal_valid_super__c, v.Name__c, v.IsOnHoliday__c, v.IsLeave__c, v.Department__c, v.PG__c, v.Level__c, v.GBU__c
		        From V2_RoleHistory__c v 
		        Where Month__c =: String.valueOf(intMonth) And Year__c =: String.valueOf(intYear) And IsOnHoliday__c = false And IsLeave__c = false And Status__c != '停用' And Name__c =: singleUserId];
    	} else {
    		list_RoleHis = [Select v.Role__c, v.Manager__c, v.Renal_valid_super__c, v.Name__c, v.IsOnHoliday__c, v.IsLeave__c, v.Department__c, v.PG__c, v.Level__c, v.GBU__c
                From V2_RoleHistory__c v 
                Where Month__c =: String.valueOf(intMonth) And Year__c =: String.valueOf(intYear) And IsOnHoliday__c = false And IsLeave__c = false And Status__c != '停用'];
    	}
    	for(V2_RoleHistory__c RoleHistory : list_RoleHis){
    		if(RoleHistory.GBU__c == null)continue;
    		if(strGBU != null && RoleHistory.GBU__c.toUpperCase() != strGBU.toUpperCase()){
    			continue;
    		}
    		//map_UserRoleHis.put(RoleHistory.Name__c , RoleHistory);
    		if(RoleHistory.Level__c.toUpperCase().contains('REP')){
    			list_RepUserRole.add(RoleHistory);
    		}else if(RoleHistory.Level__c.toUpperCase().contains('SUPERVISOR')){
    			list_SuperUserRole.add(RoleHistory);
    		}else if(RoleHistory.Level__c.toUpperCase().contains('DSM')){
                list_dsmUserRole.add(RoleHistory);
            }else if(RoleHistory.Level__c.toUpperCase().contains('ASM')){
                list_asmUserRole.add(RoleHistory);
            }else if(RoleHistory.Level__c.toUpperCase().contains('RSM') || RoleHistory.Level__c.toUpperCase().contains('REGIONAL') || RoleHistory.Level__c.toUpperCase().contains('NATIONAL')){
                list_rsmUserRole.add(RoleHistory);
            }else{
            	list_highUserRole.add(RoleHistory);
            }
    	}
    	
    	list_RepUserRole.addAll(list_SuperUserRole);
    	list_RepUserRole.addAll(list_dsmUserRole);
    	list_RepUserRole.addAll(list_asmUserRole);
    	//list_RepUserRole.addAll(list_rsmUserRole);
    	//list_RepUserRole.addAll(list_highUserRole);
    	system.debug('siiiiiii:'+list_RepUserRole.size());
    	return list_RepUserRole;
    	
    }
    
    
    global void execute(Database.BatchableContext BC, List<V2_RoleHistory__c> scope){
    	for(V2_RoleHistory__c RoleHis : scope){
    		ClsBonusRowDataCalculation BonusRowData = new ClsBonusRowDataCalculation(intYear , intMonth , RoleHis.Name__c);
    		Bonus_data__c bonus = BonusRowData.CalculationRowData(RoleHis) ;
    		system.debug('i want insert a bonus'+bonus);
    		upsert bonus ExternalID__c ;
    	}
    }
    global void finish(Database.BatchableContext BC){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        String repBody = '您好: <br><br>';
                    
            repBody += '系统已经为您完成奖金计算。请及时登录Baxter Salesforce.com CRM系统查看。<br>'; 

            repBody += '祝您工作愉快! <br>';
            repBody += '__________________________________________________ <br>';
            repBody += '本邮件由Baxter Salesforce.com CRM系统产生，请勿回复。<br>'; 
            repBody += '如有任何疑问或者要求，请联系系统管理人员。<br>'; 
            User rep = [select Email from User where id =:UserInfo.getUserId()];
            String emailAddress = String.ValueOf(rep.Email);
            String[] repAddress =new string[]{emailAddress};
            mail.setToAddresses(repAddress);
            mail.setHtmlBody(repBody);
            mail.setSubject('奖金计算已完成，请查看');
            mail.setSaveAsActivity(false);//存为活动
            mail.setSenderDisplayName('Salesforce');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}