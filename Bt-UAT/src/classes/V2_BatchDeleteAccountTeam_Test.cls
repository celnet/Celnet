/* Function: 删除需要失效的客户小组成员
 * Author: Sunny
 * Date:2012-1-4
 */
@isTest
private class V2_BatchDeleteAccountTeam_Test {

    static testMethod void TestBatch() 
    {
        List<Account> list_Account = new List<Account>() ;
        Account objAcc1 = new Account() ;
        objAcc1.Name = 'Acc1' ;
        list_Account.add(objAcc1) ;
        Account objAcc2 = new Account() ;
        objAcc2.Name = 'Acc2' ;
        list_Account.add(objAcc2) ;
        insert list_Account ;
        List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
    	
        
        
        List<V2_Account_Team__c> list_V2AccTeam = new List<V2_Account_Team__c>() ;
        V2_Account_Team__c objV2AccTeam1 = new V2_Account_Team__c() ;
        objV2AccTeam1.V2_Account__c = objAcc1.Id ;
        objV2AccTeam1.V2_User__c = use1.Id ;
        objV2AccTeam1.V2_Is_Delete__c = true ;
        objV2AccTeam1.V2_Delete_Year__c = '2012' ;
        objV2AccTeam1.V2_Delete_Month__c = '3' ;
        objV2AccTeam1.V2_BatchOperate__c = '替换' ;
        objV2AccTeam1.V2_NewAccUser__c = use3.Id ;
        list_V2AccTeam.add(objV2AccTeam1) ;
        V2_Account_Team__c objV2AccTeam2 = new V2_Account_Team__c() ;
        objV2AccTeam2.V2_Account__c = objAcc1.Id ;
        objV2AccTeam2.V2_User__c = use2.Id ;
        objV2AccTeam2.V2_Is_Delete__c = false ;
        objV2AccTeam2.V2_Delete_Year__c = '2012' ;
        objV2AccTeam2.V2_Delete_Month__c = '3' ;
        objV2AccTeam2.V2_BatchOperate__c = '替换' ;
        objV2AccTeam2.V2_NewAccUser__c = use3.Id ;
        list_V2AccTeam.add(objV2AccTeam2) ;
        V2_Account_Team__c objV2AccTeam3 = new V2_Account_Team__c() ;
        objV2AccTeam3.V2_Account__c = objAcc2.Id ;
        objV2AccTeam3.V2_User__c = use1.Id ;
        objV2AccTeam3.V2_Is_Delete__c = true ;
        objV2AccTeam3.V2_IsSyn__c = true ;
        objV2AccTeam3.V2_History__c = true ;
        objV2AccTeam3.V2_Delete_Year__c = '2012' ;
        objV2AccTeam3.V2_Delete_Month__c = '3' ;
        list_V2AccTeam.add(objV2AccTeam3) ;
        V2_Account_Team__c objV2AccTeam4 = new V2_Account_Team__c() ;
        objV2AccTeam4.V2_Account__c = objAcc2.Id ;
        objV2AccTeam4.V2_User__c = use2.Id ;
        objV2AccTeam4.V2_Is_Delete__c = true ;
        objV2AccTeam4.V2_Delete_Year__c = '2012' ;
        objV2AccTeam4.V2_Delete_Month__c = '3' ;
        list_V2AccTeam.add(objV2AccTeam4) ;
        V2_Account_Team__c objV2AccTeam5 = new V2_Account_Team__c() ;
        objV2AccTeam5.V2_Account__c = objAcc2.Id ;
        objV2AccTeam5.V2_User__c = use3.Id ;
        objV2AccTeam5.V2_Is_Delete__c = true ;
        objV2AccTeam5.V2_Delete_Year__c = '2012' ;
        objV2AccTeam5.V2_Delete_Month__c = '4' ;
        list_V2AccTeam.add(objV2AccTeam5) ;
        insert list_V2AccTeam ;
        
        List<AccountTeamMember> list_AccTeam = new List<AccountTeamMember>() ;
        AccountTeamMember objAccTeam1 = new AccountTeamMember() ;
        objAccTeam1.AccountId = objAcc1.Id ;
        objAccTeam1.UserId = use1.Id ;
        list_AccTeam.add(objAccTeam1) ;
        AccountTeamMember objAccTeam2 = new AccountTeamMember() ;
        objAccTeam2.AccountId = objAcc1.Id ;
        objAccTeam2.UserId = use2.Id ;
        list_AccTeam.add(objAccTeam2) ;
        AccountTeamMember objAccTeam3 = new AccountTeamMember() ;
        objAccTeam3.AccountId = objAcc2.Id ;
        objAccTeam3.UserId = use1.Id ;
        list_AccTeam.add(objAccTeam3) ;
        AccountTeamMember objAccTeam4 = new AccountTeamMember() ;
        objAccTeam4.AccountId = objAcc2.Id ;
        objAccTeam4.UserId = use2.Id ;
        list_AccTeam.add(objAccTeam4) ;
        AccountTeamMember objAccTeam5 = new AccountTeamMember() ;
        objAccTeam5.AccountId = objAcc2.Id ;
        objAccTeam5.UserId = use3.Id ;
        list_AccTeam.add(objAccTeam5) ;
        insert list_AccTeam ;
        
        List<Opportunity> list_Opp = new List<Opportunity>() ;
        Opportunity objOpp1 = new Opportunity() ;
        objOpp1.AccountId = objAcc1.Id ;
        objOpp1.Name = 'opp1' ;
        objOpp1.StageName = 'ssss' ;
        objOpp1.CloseDate = date.today() ;
        objOpp1.OwnerId = use1.Id ;
        list_Opp.add(objOpp1) ;
        Opportunity objOpp2 = new Opportunity() ;
        objOpp2.AccountId = objAcc1.Id ;
        objOpp2.Name = 'opp2' ;
        objOpp2.StageName = 'ssss' ;
        objOpp2.CloseDate = date.today() ;
        objOpp2.OwnerId = use2.Id ;
        list_Opp.add(objOpp2) ;
        Opportunity objOpp3 = new Opportunity() ;
        objOpp3.AccountId = objAcc2.Id ;
        objOpp3.Name = 'opp3' ;
        objOpp3.StageName = 'ssss' ;
        objOpp3.CloseDate = date.today() ;
        objOpp3.OwnerId = use3.Id ;
        list_Opp.add(objOpp3) ;
        Opportunity objOpp4 = new Opportunity() ;
        objOpp4.AccountId = objAcc2.Id ;
        objOpp4.Name = 'opp4' ;
        objOpp4.StageName = 'ssss' ;
        objOpp4.CloseDate = date.today() ;
        objOpp4.OwnerId = use1.Id ;
        list_Opp.add(objOpp4) ;
        insert list_Opp ;
        
        
        String strDelYear ;
        String strDelMonth ;
        //if(date.today().month() == 1)
        //{
        //	strDelMonth = '12' ;
        //	strDelYear = String.valueOf(date.today().year() - 1) ; 
        //}else
        //{
        //	strDelMonth = String.valueOf(date.today().month() - 1) ;
        //	strDelYear = String.valueOf(date.today().year()) ;
        //}
        strDelMonth = '3' ;
        strDelYear = '2012' ;
        system.test.startTest() ;
        String strQuery = 'Select v.V2_IsSyn__c, v.CreatedDate, v.V2_NewAccUser__c, v.V2_BatchOperate__c, v.V2_User__c, v.V2_Account__c, v.V2_Account__r.OwnerId, v.V2_History__c, v.V2_Delete_Year__c, v.V2_Delete_Month__c From V2_Account_Team__c v Where v.V2_Delete_Year__c = \''+strDelYear+'\' And v.V2_Delete_Month__c = \''+ strDelMonth +'\' And v.V2_History__c = false And v.V2_Is_Delete__c = true ';
		V2_BatchDeleteAccountTeam DelAccTeam = new V2_BatchDeleteAccountTeam(strQuery);
		
		ID batchprocessid = Database.executeBatch(DelAccTeam,200);
		
		system.test.stopTest() ;
		
		List<AccountTeamMember> list_AccTeam1 = [Select id From AccountTeamMember Where AccountId =: objAcc1.Id] ;
		List<AccountTeamMember> list_AccTeam2 = [Select id From AccountTeamMember Where AccountId =: objAcc2.Id] ;
		system.assertEquals(1 , list_AccTeam1.size()) ;
		system.assertEquals(2 , list_AccTeam2.size()) ;
		Opportunity objOppR1 = [Select Id,OwnerId From Opportunity Where Id =: objOpp1.Id] ;
		system.debug('USER3 ID:'+use3.Id) ;
		system.debug('USER2 ID:'+use2.Id) ;
		system.debug('USER1 ID:'+use1.Id) ;
		system.debug('USERNow ID:'+UserInfo.getUserId()) ;
		system.assertEquals(use3.Id, objOppR1.OwnerId) ;
		
    }
}