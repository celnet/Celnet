/*
Author：Scott
Created on：2013-10-18
Description: 

参考大陆功能 

1.在市场活动报名结束后，销售修改市场活动成员（删除、新增），提交审批给主管、大区经理、市场部。（串行审批）
潜在风险：不同用户提交新增重复的联系人在审批通过后会导致创建失败（未解决）
部署条件：需要打开主管简档对市场活动成员历史的权限,读、创建、更新
2013-4-27 Sunny 当审批拒绝时发送通知邮件
*/
public without sharing class TW_CtrlModifyCampaignMemberApprove {
	//表示行的封装类
	public class ActionRow
	{
		public CampaignMemberHistory__c ApproveRequest {get; set;}
		public CampaignMember RelatedCampaignMember {get; set;}
		public User CurrentUser{get; set;}
		
		public Boolean EnabledSupervisorApprove
		{
			get
			{
				if(this.ApproveRequest.ApproveStep__c == '主管审批'
					&& this.ApproveRequest.Supervisor__c == this.CurrentUser.Id)
				{
					return true;
				}
				return false;
			}	
		}
		public Boolean DisabledSupervisorApprove
		{
			get
			{
				return !this.EnabledSupervisorApprove;
			}
		}
		public Boolean EnabledAreaManagerApprove
		{
			get
			{
				if(this.ApproveRequest.ApproveStep__c == '大区经理审批'
					&& this.ApproveRequest.AreaManager__c == this.CurrentUser.Id)
				{
					return true;
				}
				return false;
			}
		}
		public Boolean DisabledAreaManagerApprove
		{
			get
			{
				return !this.EnabledAreaManagerApprove;
			}
		}
		public Boolean EnabledMarketingApprove
		{
			get
			{
				if(this.ApproveRequest.ApproveStep__c == '市场部审批'
					&& this.ApproveRequest.MarketingRep__c == this.CurrentUser.Id)
				{
					return true;
				}
				return false;
			}
		}
		public Boolean DisabledMarketingApprove
		{
			get
			{
				return !this.EnabledMarketingApprove;
			}
		}
	}
	
	public ID CampaignId{get;set;}
	public Campaign Campaign{get;set;}
	public List<CampaignMember> CampaignMemberList{get; set;}
	public List<CampaignMemberHistory__c> MyOwnApprovingHisList{get;set;}//需要我审批的列表
	public List<ActionRow> ActionRowList{get;set;}
	public Id CurrentUserId{get;set;}
	public User CurrentUser{get;set;}
	public Boolean IsEnabledTab{get;set;}
	//public Integer TiggerRowId{get;set;}//用于在取消/新增一行，确定联系人的客户时，标识行号
	private Map<String, User> ToEmailMap;//下一个审批人邮件地址
	private List<CampaignMemberHistory__c> RejectEmail;//拒绝之后邮件提醒申请人
	private Map<ID,CampaignMember> DelCampaignMemberMap;//审批最终通过后删除的成员
	private List<CampaignMember> NewCampaignMemberList;//审批最终通过后新增的成员
	
	private Boolean IsFinish;
	
	public Boolean ShowClose
	{
		get
		{
			return IsFinish;
		}
	}
	
	public Boolean SaveEnabled
	{
		get
		{
			if(!IsEnabledTab)
			{
				return false;
			}
			Boolean isEnabled = false;
			for(ActionRow row : ActionRowList)
			{
				if(row.EnabledSupervisorApprove
					|| row.EnabledAreaManagerApprove
					|| row.EnabledMarketingApprove)
				{
					isEnabled = true;
					break;
				}
			}
			return isEnabled;
		}
	}
	
	public TW_CtrlModifyCampaignMemberApprove()
	{
		this.IsEnabledTab = true;
		this.IsFinish = false;
		this.CampaignId = ApexPages.currentPage().getParameters().get('camid');
		if(this.CampaignId == null)
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '必須指定壹個市場活動');            
            ApexPages.addMessage(msg);
            this.IsEnabledTab = false;
            return;
		}
		this.Campaign = [select Name, Id, OwnerId, Status, Owner.Id, Owner.Name, Owner.Alias, Owner.Email from Campaign where Id =: this.CampaignId];
		this.CurrentUser = [Select UserRole.Name, UserRole.Id, UserRole.ParentRoleId, UserRoleId, Id, Name, Alias, Email From User Where Id=:UserInfo.getUserId()];
		this.CurrentUserId = this.CurrentUser.Id;
		this.InitList();
	}
	
	private void InitList()
	{
		//bill update 2013/7/9  
		// 1.添加拒绝原因
		// 2.修改提交人显示为提交人别名  
		// 3.对页面显示顺序按照提交人的name排序
		this.MyOwnApprovingHisList = [Select Id, 
			SupervisorDefAgree__c,
			AreaManagerDefAgree__c,
			CampaignMemberId__c,
			ContactId__c, 
			ContactId__r.Name,
			CampaignId__c, 
			AccountId__c, 
			Comment__c,
			DepartFlight__c,
			DepartDate__c,
			ArriveFlight__c,
			ArriveDate__c,
			Action__c,
			AreaManagerApprove__c,
			MarketingApprove__c, 
			SupervisorApprove__c,
			RejectReason__c,
			User__c,
			User__r.Name,
			User__r.Alias,
			User__r.Email,
			AreaManager__c,
			AreaManager__r.Email,
			AreaManager__r.Name,
			Supervisor__c,
			Supervisor__r.Email,
			Supervisor__r.Name,
			MarketingRep__c,
			MarketingRep__r.Email,
			MarketingRep__r.Name,
			ApproveStep__c,
			CreatedDate,
			ApproveResult__c
			From CampaignMemberHistory__c 
			Where CampaignId__c =: this.CampaignId 
			And (User__c =: this.CurrentUserId
				Or Supervisor__c =: this.CurrentUserId
				Or AreaManager__c =: this.CurrentUserId
				Or MarketingRep__c =: this.CurrentUserId)
			/*******************BILL update 2013/7/9 start*************************/
			Order By User__r.Name ASC
			//Order By CreatedDate
			/*******************BILL update 2013/7/9 end*************************/
			];
		this.ActionRowList = new List<ActionRow>();
		for(CampaignMemberHistory__c approveRequest : this.MyOwnApprovingHisList)
		{
			//设置审批选项的默认值
			if(approveRequest.ApproveStep__c == '主管审批'
				&& approveRequest.Supervisor__c == this.CurrentUser.Id
				&& approveRequest.SupervisorDefAgree__c
				&& approveRequest.SupervisorApprove__c == null)
			{
				approveRequest.SupervisorApprove__c = '通过';
			}
			else if(approveRequest.ApproveStep__c == '大区经理审批'
				&& approveRequest.AreaManager__c == this.CurrentUser.Id
				&& approveRequest.AreaManagerDefAgree__c
				&& approveRequest.AreaManagerApprove__c == null)
			{
				approveRequest.SupervisorApprove__c = '通过';
			}
			
			ActionRow row = new ActionRow();
			row.CurrentUser = this.CurrentUser;
			row.ApproveRequest = approveRequest;
			this.ActionRowList.add(row);
		}
	}
	
	//保存操作
	public void Save()
	{
		try
		{
			this.ToEmailMap = new Map<String, User>();
			this.RejectEmail = new List<CampaignMemberHistory__c>();
			this.DelCampaignMemberMap = new Map<ID, CampaignMember>();
			this.NewCampaignMemberList = new List<CampaignMember>();
			this.IsFinish = false;
			for(ActionRow row : this.ActionRowList)
			{
				CampaignMemberHistory__c approveRequest = row.ApproveRequest;
				if(row.EnabledSupervisorApprove)
				{
					HandleSupervisorApprove(approveRequest);
				}
				else if(row.EnabledMarketingApprove)
				{
					HandleMarketingApprove(approveRequest);
				}
			}
			this.SendEmailToNextApprover();
			this.SendRejectEmailToRep();
			if(this.DelCampaignMemberMap.size() != 0)
			{
				//由于系统有存在在报名结束限制删除的的Trigger，为了能够在报名结束删除成员必须将在删除前将AllowToDelete__c 更新为true。
				List<CampaignMember> delMemberList =  this.DelCampaignMemberMap.Values();
				update delMemberList;
				delete delMemberList;
			}
			if(this.NewCampaignMemberList.size() != 0)
			{
				insert this.NewCampaignMemberList;
			}
			update this.MyOwnApprovingHisList;
			this.IsFinish = true;
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO , '提交成功，點擊 “返回市場活動”以返回市場活動');            
            ApexPages.addMessage(msg);
			this.InitList();
		}
		catch(Exception e)
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL , String.valueOf(e)+' 第'+ e.getLineNumber()+'行');            
            ApexPages.addMessage(msg);
            this.InitList();
            return;
		}
	}
	
	public PageReference ReturnCampaign()
	{
		return new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/' + this.CampaignId);
	}
	private void HandleSupervisorApprove(CampaignMemberHistory__c approveRequest)
	{
		if(approveRequest.SupervisorApprove__c == '通过')//流转到下一个
		{
			if(approveRequest.AreaManager__c != null)
			{
				approveRequest.ApproveStep__c = '大区经理审批';
				this.ToEmailMap.put(approveRequest.AreaManager__r.Email, approveRequest.AreaManager__r);
			}
			else if(approveRequest.MarketingRep__c != null)
			{
				approveRequest.ApproveStep__c = '市场部审批';
				this.ToEmailMap.put(approveRequest.MarketingRep__r.Email, approveRequest.MarketingRep__r);
			}
		}
		else if(approveRequest.SupervisorApprove__c == '拒绝')//结束审批
		{
			approveRequest.ApproveStep__c = '结束';
			approveRequest.ApproveResult__c = '拒绝';
			this.RejectEmail.add(approveRequest);
		}
		else
		{
			//不做任何处理，没有做这个审批，下次可以继续做
		}
	}
	
	private void HandleMarketingApprove(CampaignMemberHistory__c approveRequest)
	{
		if(approveRequest.MarketingApprove__c == '通过')
		{
			approveRequest.ApproveStep__c = '结束';
			approveRequest.ApproveResult__c = '通过';
			if(approveRequest.Action__c == '新增')
			{
				CampaignMember newCpm = new CampaignMember();
				newCpm.ContactId = approveRequest.ContactId__c;
				newCpm.CampaignId = approveRequest.CampaignId__c;
				newCpm.V2_Account__c = approveRequest.AccountId__c;
				newCpm.V2_Comment__c = approveRequest.Comment__c;
				newCpm.V2_DepartFlight__c = approveRequest.DepartFlight__c;
				newCpm.V2_DepartDate__c = approveRequest.DepartDate__c;
				newCpm.V2_ArriveFlight__c = approveRequest.ArriveFlight__c;
				newCpm.V2_ArriveDate__c = approveRequest.ArriveDate__c;
				newCpm.User__c = approveRequest.User__c;
				newCpm.V2_SupervisoApprove__c = approveRequest.SupervisorApprove__c;
				newCpm.V2_AreaManagerApprove__c = approveRequest.AreaManagerApprove__c;
				newCpm.V2_MarketingApprove__c = approveRequest.MarketingApprove__c;
				newCpm.V2_Participated__c = true;
				this.NewCampaignMemberList.add(newCpm);
			}
			else if(approveRequest.Action__c == '删除')
			{
				if(approveRequest.CampaignMemberId__c != null && approveRequest.CampaignMemberId__c != '')
				{
					//由于系统有存在在报名结束限制删除的的Trigger，为了能够在报名结束删除成员必须将AllowToDelete__c 更新为true。
					this.DelCampaignMemberMap.put(approveRequest.CampaignMemberId__c, new CampaignMember(ID = approveRequest.CampaignMemberId__c, AllowToDelete__c = true));
				}
			}
		}
		else if(approveRequest.MarketingApprove__c == '拒绝')//结束审批
		{
			approveRequest.ApproveStep__c = '结束';
			approveRequest.ApproveResult__c = '拒绝';
			this.RejectEmail.add(approveRequest);
		}
		else
		{
			//不做任何处理，没有做这个审批，下次可以继续做
		}
	}
	
	
	private void SendEmailToNextApprover()
	{	
		for(User approver : this.ToEmailMap.Values())
		{
			String subject = '來自SEP系統通知：市場活動成員更新審批';
		    String message = '您好 ' + approver.Name + '\n\n' + 
		     '報名結束的市場活動的成員列表已被銷售代表更改，請點擊下面的連接到系統中進行審批\n' +
		     '	市場活動: ' + this.Campaign.Name + '\n' +
		     //'	审批提交人（销售代表）: ' + this.Rep.Name + '\n' +
		     //'	审批类型: ' + approveType + '\n' +
		     //'	审批人: ' + approver.Name + '\n' +
		     //'	影响市场活动成员数（新增和删除）: ' + newHisList.size() + '\n' +
		     '	審批連接: ' + URL.getSalesforceBaseUrl().toExternalForm() + '/apex/TW_ModifyCampaignMemberApprove?camid=' + this.Campaign.Id + '\n' + 
		  	    '祝您工作愉快!\n' +
		        '__________________________________________________ \n' +
		        '本郵件由Baxter Salesforce.com CRM系統産生，請勿回複。\n' +
		        '如有任何疑問或者要求，請聯系系統管理人員。\n' +
			  	'Baxter SEP System';
		   	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		   	String[] toAddresses = new String[] {approver.Email};
		   	mail.setToAddresses(toAddresses);
		   	mail.setReplyTo('no-reply@salesforce.com');
		   	mail.setSubject(subject);
		   	mail.setSenderDisplayName('Baxter SEP System');
		  	mail.setPlainTextBody(message);
		  	if(!Test.isRunningTest()) 
		  	{
		   		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		  	}
		}
	}
	
	//拒绝的邮件
    private void SendRejectEmailToRep(){
        List<Messaging.SingleEmailMessage> list_email = new List<Messaging.SingleEmailMessage>();
        for(CampaignMemberHistory__c approver : this.RejectEmail)
        {
            String subject = '來自SEP系統通知：行銷活動成員更新審批';
            String message = 'Hi ' + approver.User__r.Name + '\n\n';
            String level =''; 
            if(approver.SupervisorApprove__c == '拒绝'){
            	level = '主管';
            }else if(approver.AreaManagerApprove__c == '拒绝'){
            	level = '大区经理';
            }else if(approver.MarketingApprove__c == '拒绝'){
            	level = '市場部';
            }
            message += '您提交的行銷活動成員修改申請已被'+level+'拒絕\n' ;
            message += '拒絕原因：'+approver.RejectReason__c+'\n' ;
            message += '  行銷活動: ' + this.Campaign.Name + '\n' +
             '  操作: ' + approver.Action__c + '\n' +
             '  聯系人: ' + approver.ContactId__r.Name + '\n' +
             //'    审批人: ' + approver.Name + '\n' +
             //'    影响市场活动成员数（新增和删除）: ' + newHisList.size() + '\n' +
             '  行銷活動鏈接: ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + approver.CampaignId__c + '\n' + 
                '祝您工作愉快!\n' +
                '__________________________________________________ \n' +
                '本郵件由Baxter Salesforce.com CRM系統産生，請勿回複。\n' +
                '如有任何疑問或者要求，請聯系系統管理人員。\n' +
                'Baxter SEP System';
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {approver.User__r.Email};
            mail.setToAddresses(toAddresses);
            mail.setReplyTo('no-reply@salesforce.com');
            mail.setSubject(subject);
            mail.setSenderDisplayName('Baxter SEP System');
            mail.setPlainTextBody(message);
            list_email.add(mail);
        }
        if(!Test.isRunningTest() && list_email.size() > 0) 
        {
            Messaging.sendEmail(list_email);
        }
    }
    /*======================测试=============================*/
    static testMethod void TW_CtrlModifyCampaignMemberApprove() 
    {	
		UserRole regRole = [select Id from UserRole where name='TW South Sales Rep' limit 1];
		UserRole supRole = [select Id from UserRole where name='TW Regional South Sales Manager' limit 1];
		UserRole marRole = [select Id from UserRole where name='TW Marketing Product Manager' limit 1];
		
		/*用户简档*/
		//rep简档
	    Profile RepProRenal = [select Id from Profile where Name  = 'TW Sales Rep' limit 1];
	    //sup简档
	    Profile SupProRenal = [select Id from Profile where Name  = 'TW Sales Supervisor' limit 1];
	    //marketing
	    Profile MarketProRenal = [select Id from Profile where Name='TW Marketing' limit 1];
		
		/************User************/
		List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        
        //主管
        User supUser = new User();
	    supUser.Username='supUser@123.com';
	    supUser.LastName='supUser';
	    supUser.Email='supUser@123.com';
	    supUser.Alias=user[0].Alias;
	    supUser.TimeZoneSidKey=user[0].TimeZoneSidKey;
	    supUser.ProfileId=SupProRenal.Id;
	    supUser.LocaleSidKey=user[0].LocaleSidKey;
	    supUser.LanguageLocaleKey=user[0].LanguageLocaleKey;
        supUser.EmailEncodingKey=user[0].EmailEncodingKey;
	    supUser.CommunityNickname='supUser';
	    supUser.MobilePhone='12345678912';
	    supUser.UserRoleId = supRole.Id ;
	    supUser.IsActive = true;
     	insert supUser;
        
        //销售
        User repUser = new User();
	    repUser.Username='repUser@123.com';
	    repUser.LastName='repUser';
	    repUser.Email='repUser@123.com';
	    repUser.Alias=user[0].Alias;
	    repUser.TimeZoneSidKey=user[0].TimeZoneSidKey;
	    repUser.ProfileId=RepProRenal.Id;
	    repUser.LocaleSidKey=user[0].LocaleSidKey;
	    repUser.LanguageLocaleKey=user[0].LanguageLocaleKey;
        repUser.EmailEncodingKey=user[0].EmailEncodingKey;
	    repUser.CommunityNickname='repUser';
	    repUser.MobilePhone='12345678912';
	    repUser.UserRoleId = regRole.Id ;
	    repUser.IsActive = true;
	    repUser.ManagerId = supUser.Id;
     	insert repUser;
        
        //市场
        User mkUser = new User();
	    mkUser.Username='mkUser@123.com';
	    mkUser.LastName='mkUser';
	    mkUser.Email='mkUser@123.com';
	    mkUser.Alias=user[0].Alias;
	    mkUser.TimeZoneSidKey=user[0].TimeZoneSidKey;
	    mkUser.ProfileId=MarketProRenal.Id;
	    mkUser.LocaleSidKey=user[0].LocaleSidKey;
	    mkUser.LanguageLocaleKey=user[0].LanguageLocaleKey;
        mkUser.EmailEncodingKey=user[0].EmailEncodingKey;
	    mkUser.CommunityNickname='mkUser';
	    mkUser.MobilePhone='12345678912';
	    mkUser.UserRoleId = marRole.Id ;
	    mkUser.IsActive = true;
     	insert mkUser;
		
		
		//客户
		RecordType accrecordtype = [select Id from RecordType where DeveloperName='TW_Hospital' and SobjectType='Account' limit 1];
		Account acc1 = new Account();
		acc1.RecordTypeId = accrecordtype.Id;
        acc1.Name = 'T_医院1';
        insert new Account[] {acc1};
        
        /*联系人*/
		RecordType conrecordtype = [select Id from RecordType where DeveloperName = 'TW_Contact' and SobjectType='Contact' and IsActive = true limit 1 ];
        Contact ct1A = new Contact();
        ct1A.RecordTypeId = conrecordtype.Id;
        ct1A.AccountId = acc1.Id;
        ct1A.LastName = 'T_医院1_医生A';
        Contact ct1B = new Contact();
        ct1B.RecordTypeId = conrecordtype.Id;
        ct1B.AccountId = acc1.Id;
        ct1B.LastName = 'T_医院1_医生B';
        Contact ct1C = new Contact();
        ct1C.RecordTypeId = conrecordtype.Id;
        ct1C.AccountId = acc1.Id;
        ct1C.LastName = 'T_医院1_医生C';
        insert new Contact[]{ct1A, ct1B, ct1C};
		
		Campaign cp = new Campaign();
		cp.Name = 'T_CPA';
		cp.Status = 'Sign Up Closed';
		cp.IsActive = true;
		cp.StartDate = Date.today();
		cp.EndDate = Date.today().addDays(30);
		cp.OwnerId = mkUser.Id;
		insert cp;
		
		CampaignMember cmpA = new CampaignMember();
		cmpA.CampaignId = cp.Id;
		cmpA.ContactId = ct1A.Id;
		cmpA.User__c = repUser.Id;
		CampaignMember cmpB = new CampaignMember();
		cmpB.CampaignId = cp.Id;
		cmpB.ContactId = ct1B.Id;
		cmpB.User__c = repUser.Id;
		insert(new CampaignMember[] {cmpA, cmpB});
		
		List<CampaignMemberHistory__c> newHisList = new List<CampaignMemberHistory__c>();
		CampaignMemberHistory__c newHis = new CampaignMemberHistory__c();
		newHis.Action__c = '新增';
		newHis.User__c = repUser.Id;
		newHis.CampaignId__c = cp.Id;
		newHis.ApproveStep__c = '提交';
		newHis.ApproveStep__c = '主管审批';
		newHis.Supervisor__c = supUser.Id;
		newHis.SupervisorDefAgree__c = true;
		newHis.SupervisorApprove__c = null;
		newHis.MarketingRep__c = mkUser.Id;
		newHisList.add(newHis);
        CampaignMemberHistory__c newHis2 = new CampaignMemberHistory__c();
		newHis2.Action__c = '新增';
		newHis2.User__c = repUser.Id;
		newHis2.CampaignId__c = cp.Id;
		newHis2.Supervisor__c = supUser.Id;
		newHis2.MarketingRep__c = mkUser.Id;
		newHis2.ApproveStep__c = '主管审批';
		newHis2.User__c = repUser.Id;
		newHisList.add(newHis2);
		CampaignMemberHistory__c newHis3 = new CampaignMemberHistory__c();
		newHis3.Action__c = '新增';
		newHis3.User__c = repUser.Id;
		newHis3.CampaignId__c = cp.Id;
		newHis3.Supervisor__c = supUser.Id;
		newHis3.MarketingRep__c = mkUser.Id;
		newHis3.SupervisorApprove__c= '通过';
		newHis3.ApproveStep__c = '市场部审批';
		newHis3.User__c = repUser.Id;
		newHisList.add(newHis3);
		
		CampaignMemberHistory__c newHis4 = new CampaignMemberHistory__c();
		newHis4.Action__c = '删除';
		newHis4.User__c = repUser.Id;
		newHis4.CampaignId__c = cp.Id;
		newHis4.Supervisor__c = supUser.Id;
		newHis4.MarketingRep__c = mkUser.Id;
		
		newHis4.User__c = repUser.Id;
		newHisList.add(newHis4);
        insert newHisList;
        test.startTest();
        ApexPages.currentPage().getParameters().put('camid', cp.id);
	    TW_CtrlModifyCampaignMember submitController0 = new TW_CtrlModifyCampaignMember();
        System.runAs(supUser)
        {
        	ApexPages.currentPage().getParameters().put('camid', null);
	        TW_CtrlModifyCampaignMemberApprove submitController1 = new TW_CtrlModifyCampaignMemberApprove(); 
			
	        ApexPages.currentPage().getParameters().put('camid', cp.id);
	        TW_CtrlModifyCampaignMemberApprove submitController3 = new TW_CtrlModifyCampaignMemberApprove(); 
	        
	        
	        submitController3.ActionRowList[0].ApproveRequest.SupervisorApprove__c = '拒绝';
	        submitController3.ActionRowList[2].ApproveRequest.SupervisorApprove__c = '拒绝';
	     	submitController3.ActionRowList[1].ApproveRequest.SupervisorApprove__c = '拒绝';
	        submitController3.Save();
	        submitController3.ActionRowList[0].ApproveRequest.SupervisorApprove__c = '通过';
	        submitController3.ActionRowList[2].ApproveRequest.SupervisorApprove__c = '通过';
	     	submitController3.ActionRowList[1].ApproveRequest.SupervisorApprove__c = '通过';
	        submitController3.Save();
	        submitController3.SendEmailToNextApprover();
	        submitController3.SendRejectEmailToRep();
	        submitController3.ReturnCampaign();
	        
        }
        System.runAs(mkUser)
        {
        	ApexPages.currentPage().getParameters().put('camid', null);
	        TW_CtrlModifyCampaignMemberApprove submitController1 = new TW_CtrlModifyCampaignMemberApprove(); 
			
	        ApexPages.currentPage().getParameters().put('camid', cp.id);
	        TW_CtrlModifyCampaignMemberApprove submitController3 = new TW_CtrlModifyCampaignMemberApprove(); 
	       
	        submitController3.ActionRowList[0].ApproveRequest.MarketingApprove__c = '通过';
	     	submitController3.ActionRowList[1].ApproveRequest.MarketingApprove__c = '通过';
	     	submitController3.ActionRowList[2].ApproveRequest.MarketingApprove__c = '通过';
	        submitController3.Save();
	        submitController3.ReturnCampaign();
	        
        }
        test.stopTest();  
    }
}