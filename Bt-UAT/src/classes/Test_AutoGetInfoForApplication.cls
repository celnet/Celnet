/**
 * author: bill
 * AutoGetInfoForApplication的trigger的测试类
 */
@isTest
private class Test_AutoGetInfoForApplication {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        //客户
        Account acc = new Account();
        acc.Name = 'aaaa';
        insert acc;
        //opp
        Opportunity opp = new Opportunity();
        opp.Name = 'ssfde';
        opp.AccountId = acc.Id;
        opp.CloseDate = date.today().addMonths(1);
        opp.StageName = '达成共识';
        insert opp;
        //联系人
        Contact contact = new Contact();
        contact.AccountId = acc.Id;
        contact.FirstName = 'zhang';
        contact.LastName = 'san';
        contact.Phone = '010-1234567';
        insert contact;
        
        //挥发罐申请
        Vaporizer_Application__c vapApply = new Vaporizer_Application__c();
        vapApply.Contact__c = contact.Id;
        vapApply.Hospital__c = acc.Id;
        vapApply.VapOpportunity__c = opp.Id;
        
        test.startTest();
        insert vapApply;
        test.stopTest();
    }
}