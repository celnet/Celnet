/*****************************************************************************
 *Owner:AndyWang
 *Date:2012年9月18日 11:28:31
 *Function:
 *规则  
 *2013-12-30 Sunny ： 如果查询结果相同Item No.有Type为Customer的，则只显示Customer的结果，否则都显示
*****************************************************************************/ 
public  class V3_QuoteLineItems {
	
	public Boolean IfShowPageSearch{get;set;} //第一页面显示
	public Boolean IfShowPageInput{get;set;}//第二页面显示
	
	public Boolean IsShowInfo{get;set;} //info message
	public Boolean IsShowError{get;set;} //error message
	public String strInfo{get;set;} //info message
	public String strError{get;set;} //error message
	public Boolean IsNextEnabled{get;set;}
	
	public Product2  product{get;set;}//Product
	public Quote quoteobj{get;set;}//quote
	public String strsql{get;set;} //全局查询语句
	public Boolean IsInitial{get;set;}

	public list<Product_Price__c> lstSearchProductPrice{get;set;}//list Product
	public list<Product_Price__c> lstSelectProductPrice{get;set;}//list Product

	public list<V3_EntityObject> listEntityObject{get;set;}
	public list<V3_EntityObject> listEntityObjectSelect{get;set;}
	  
	public Integer dataSize{
	get{	
		  if(listEntityObject==null) return 0;
		  else return listentityObject.size();
	   } set;
	}  
	public String Quoteid{get;set;}// Qid
	

    /**
    show search product page
    **/
    public void ShowPageSearch(){
    	IfShowPageSearch=true;
    	IfShowPageInput=false;
    }
    /**
    show input page
    **/
    public void ShowPageInput(){
    	IfShowPageSearch=false;
    	IfShowPageInput=true;
    }
    
    public void ShowInfoMsg(String p_msg){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO ,p_msg);            
	        ApexPages.addMessage(msg);
    }
    public void ShowErrorMsg(String p_msg){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR ,p_msg);            
	        ApexPages.addMessage(msg);
    }
    
    /*****************************************
	* 构造
	*****************************************/
	public V3_QuoteLineItems(ApexPages.StandardController controller){
		try{
			
			//1.构造查询条件
			//2.执行初始化查询
			//3.页面显示控制
			
	    	//TotalCount=0;
			//构造实体 
			product = [select V3_HK_GBU__c,V3_HK_SBU__c,V3_Grouping__c,V3_Item_Number__c from product2 limit 1];
			product.V3_HK_GBU__c =null;
			product.V3_Item_Number__c=null;
						
			Quoteid = ApexPages.currentPage().getParameters().get('QuoteId');
			system.debug('========Quoteid========'+Quoteid);
			if(Quoteid != null){
				quoteobj =[Select V3_Currency__c,Opportunity.Account.MID__c,Opportunity.Account.PriceGroup__c, Status, 
				Pricebook2Id, OpportunityId, Name, Id,(Select Id From QuoteLineItems) From Quote where Id=:Quoteid];
			
			} 
			system.debug('========quoteobj========'+quoteobj);
			
			IsInitial=true;
			//search
			Query();
			
			//show search page
			ShowPageSearch();
			//disable next button
			IsNextEnabled=false;
			
		}catch(exception e){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING ,e.getmessage()+' 行数：'+e.getLineNumber());            
	        ApexPages.addMessage(msg);
	        return;
		}
	}
	//:取消
	public PageReference Cancel(){
      
      return new PageReference( '/' + Quoteid);
    }
    
    	//:价格输入
	public PageReference QueryNext(){
      ShowPageInput();
      return null;
    }
    
    
    
   //:关键字查询
    public PageReference Query(){
    	String HKGBU =null;
    	String HKSBU =null;
    	String Grouping =null;
    	String ItemNo =null;
    	if(!IsInitial){
			HKSBU = product.V3_HK_SBU__c;
			HKGBU = product.V3_HK_GBU__c;
			Grouping = product.V3_Grouping__c;
    		ItemNo = product.V3_Item_Number__c;
    	} 
		String strCurrency = quoteobj.V3_Currency__c;
		String CustomerNo=quoteobj.Opportunity.Account.MID__c;
		String accPriceGroup=quoteobj.opportunity.Account.PriceGroup__c;
		
    	System.debug('**********HKGBU=' + HKGBU);
    	System.debug('**********HKSBU=' + HKSBU);
    	System.debug('**********Grouping=' + Grouping);
    	 
    	lstSearchProductPrice=new list<Product_Price__c>();
    	strSql=' ';
    	String countSql='Select count()' +
     	        ' From Product_Price__c  where '+
    	        ' Currency__c =\''+strCurrency +'\''+
    	        ' and IsEnable__c =\'true\'  '+
    	        ' and Expiration__c =\'false\'' +
    	        ' and ( ' +
    	        ' (Customer_No__c =\'' + CustomerNo + '\' and Price_List_Name__c=null)' +
    	        '  OR (Price_List_Name__c =\'' + accPriceGroup + '\' and Customer_No__c=null))';
    	
    	String searchSql ='Select Name, Product_Description__c,Product__c, Type__c, Expiration__c,UOM__c,Price__c,Qty__c ,V3_valueUOM__c,'+
    	        ' Product__r.V3_Pack_factor__c, Product__r.V3_X1st_UOM__c, Product__r.V3_X2nd_UOM__c, ' +
    	        ' Customer_No__c ' +
     	        ' From Product_Price__c  where '+
    	        ' Currency__c =\''+strCurrency +'\''+
    	        ' and IsEnable__c =\'true\'  '+
    	        ' and Expiration__c =\'false\'' +
    	        ' and ( ' +
    	        ' (Customer_No__c =\'' + CustomerNo + '\' and Price_List_Name__c=null)' +
    	        '  OR (Price_List_Name__c =\'' + accPriceGroup + '\' and Customer_No__c=null))';
    	
    	if( ItemNo!= null && ItemNo != ''){
    		strSql+= ' and  Name like \'%' + product.V3_Item_Number__c +'%\'';
    	}
    	if(HKGBU != null && HKGBU != ''){
    		strSql+= ' and V3_HK_GBU_c__c =\''+ HKGBU + '\'';
    	}
    	if(HKSBU != null && HKSBU !=''){
    		strSql+= ' and V3_HK_SBU_c__c =\''+HKSBU+'\'';
    	}
    	if(Grouping != null && Grouping!=''){
    		strSql+= ' and V3_Grouping_c__c =\''+ Grouping+'\'';
    	}
    	String orderSql =' order by name limit 200';
    	System.debug('++++++++' + searchSql + strSql);
    	//con = new ApexPages.StandardSetController(Database.getQueryLocator(strsql));
        //lstSearchProductPrice = (list<Product_Price__c>)con.getRecords();
       Integer i =Database.countquery(countSql+strSql);
        if(i>200){
                ShowInfoMsg('get datas size:' + i + ',max number is 200,show first 200 rows.');
                IsInitial=false;
                //return null;
        }
        lstSearchProductPrice=Database.query(searchSql+ strsql + orderSql);
        System.debug('++++++++' + lstSearchProductPrice.size());
        //去除重复的item no
        //lstSearchProductPrice=RemoveDupliteObj(lstSearchProductPrice);
        //2013-12-30 Sunny 添加逻辑
        lstSearchProductPrice = SearchCustomerRecord(lstSearchProductPrice);
        
        listEntityObject = new list<V3_EntityObject> ();
        
        if(lstSearchProductPrice.size()>0){
        	for(Product_Price__c ProductPrice:lstSearchProductPrice){
        		V3_EntityObject EntityObject = new V3_EntityObject();
        		EntityObject.obj_ProductPrice = ProductPrice;
        		listEntityObject.add(EntityObject);
        	}
        }
        //第一次查询后就置为false；
        IsInitial=false;
    	return null;
    }
    //2013-12-30 Sunny ： 如果查询结果相同Item No.有Type为Customer的，则只显示Customer的结果，否则都显示
    private List<Product_Price__c> SearchCustomerRecord(List<Product_Price__c> pList)
    {
    	system.debug('pList:'+pList);
    	List<Product_Price__c> ret = new List<Product_Price__c>();
    	if(pList==null || pList.size()==0){
    		return ret;
    	}
    	Map<String , List<Product_Price__c>> map_OtherProductPrice = new Map<String , List<Product_Price__c>>();
    	Map<String , Product_Price__c> map_CustomerProductPrice = new Map<String , Product_Price__c>();
    	for(Product_Price__c productPrice : pList)
    	{
    		
    		if(productPrice.Type__c == 'Customer')
    		{
    			map_CustomerProductPrice.put(productPrice.Name , productPrice);
    		}else
    		{
    			List<Product_Price__c> list_ProductPrice = new List<Product_Price__c>();
    			if(map_OtherProductPrice.containsKey(productPrice.Name ))
    			{
    				list_ProductPrice = map_OtherProductPrice.get(productPrice.Name );    				
    			}
    			list_ProductPrice.add(productPrice);
    			map_OtherProductPrice.put(productPrice.Name , list_ProductPrice);
    		}
    	}
    	for(String itemNo : map_CustomerProductPrice.keySet())
    	{
    		if(map_OtherProductPrice.containsKey(itemNo) )
    		map_OtherProductPrice.remove(itemNo);
    	}
    	system.debug('Map :'+map_CustomerProductPrice);
    	system.debug('Map :'+map_OtherProductPrice);
    	if(map_CustomerProductPrice.size() > 0)
    	ret.addAll(map_CustomerProductPrice.values());
    	if(map_OtherProductPrice.size() > 0)
    	for(List<Product_Price__c> lpp : map_OtherProductPrice.values())
    	{
    		ret.addAll(lpp);
    	}
    	return ret;
    }
    /**
    //相同的Item No 去除type ！= customer的数据
    public List<Product_Price__c> RemoveDupliteObj(List<Product_Price__c> pList){
    	List<Product_Price__c> ret = new List<Product_Price__c>();
    	if(pList==null || pList.size()==0){
    		return ret;
    	}
    	//构造map
    	map<String,Product_Price__c> mapProductPrice = new map<String,Product_Price__c>();
    	for(Product_Price__c productPrice:pList){
    		if(mapProductPrice.containsKey(productPrice.name)){
    			Product_Price__c temp=mapProductPrice.get(productPrice.name);
    			if(temp.Customer_No__c == null || temp.Customer_No__c ==''){
		    		mapProductPrice.put(productPrice.name,productPrice);
    			}
    			
    		} else {
	    		mapProductPrice.put(productPrice.name,productPrice);
    		}
    	}
    	
    	//map to lst；
    	for(Product_Price__c prduct: mapProductPrice.values()){
    		ret.add(prduct);
    	}
		return ret;        
    }
    **/  
    //:选中
    public PageReference Check(){ 
      IsShowError = false; 
      if(listEntityObjectSelect==null) listEntityObjectSelect =new List<V3_EntityObject>();
      if(listEntityObject.size()>0){
      	for(V3_EntityObject EntityObject:listEntityObject){
      		if(EntityObject.binIsSelected==true){
  				//如果已经存在则不添加
  				Boolean IfExists = false;
  				for(V3_EntityObject selectedEntityObject:listEntityObjectSelect){
  					if(selectedEntityObject.obj_ProductPrice.id==EntityObject.obj_ProductPrice.id){
  						IfExists=true;
  					}
  				}
  				if(!IfExists){
  					EntityObject.binIsSelected=false;
	 				listEntityObjectSelect.add(EntityObject);
  					IsNextEnabled=true;
  				}
	 		}
	     }
      }
      if(listEntityObjectSelect.size()>0){
      	IsNextEnabled=true;
      } else {
      	IsNextEnabled=false;
      }
	  return null;
    }
    

    //:删除项
    public PageReference DeleteItem(){ 
	  list<V3_EntityObject> listEntityObjectSelectDelete = new list<V3_EntityObject>(); 
	  if(listEntityObjectSelect==null){
	  	 listEntityObjectSelect =new List<V3_EntityObject>();
	  	 ShowErrorMsg('Not selected Items !');
	  }
	  if(listEntityObjectSelect.size()>0){
	  	for(V3_EntityObject EntityObject :listEntityObjectSelect){
	  		if(EntityObject.binIsSelected==false){
	  			listEntityObjectSelectDelete.add(EntityObject);
	 		}
	  	}
	  }
	  listEntityObjectSelect = listEntityObjectSelectDelete;
	 if(listEntityObjectSelect.size()>0){
      	IsNextEnabled=true;
      } else {
      	IsNextEnabled=false;
      }
	  
      return null;
    }
    
    
    
    //:保存Item
    public PageReference Finish(){ 
    	
    	//取得Price book entry id
    	map<ID,PricebookEntry> mapPricebookEntry = new map<ID,PricebookEntry>();
    	list<PricebookEntry>  lisPricebookEntry=[Select p.Product2Id, p.Pricebook2.Name, p.Pricebook2Id, p.Name, 
    												p.Id From PricebookEntry p where p.Pricebook2.Name ='HK PriceBook'];
		if(lisPricebookEntry.size()>0){
			for(PricebookEntry pricebookEntry:lisPricebookEntry){
				if(!mapPricebookEntry.containsKey(pricebookEntry.Product2Id)){
					mapPricebookEntry.put(pricebookEntry.Product2Id,pricebookEntry);
				}
			}
		} else {
						ShowErrorMsg('can not find price book with name :HK PriceBook');
						return null;

		}
		
    	list<QuoteLineItem> lisQuoteLineItem = new list<QuoteLineItem>();
    	if(listEntityObjectSelect.size() >0){
    		for(V3_EntityObject EntityObject:listEntityObjectSelect){
    			// 1,获取所有选中项集合
    			// 2,将集合数据生成Item项
    			//判断数量是否为0
    			if(EntityObject.obj_ProductPrice.Qty__c<=0){
    					ShowErrorMsg('Qty mast greater than 0');
						return null;
    				
    			}
    			
    			//判断价钱是否为null,如果是,赋值0
    			if(EntityObject.obj_ProductPrice.Price__c == null){
					EntityObject.obj_ProductPrice.Price__c = 0;
				}
				
				//计算价格
				double objPrice=0;
				if(EntityObject.obj_ProductPrice.V3_valueUOM__c == EntityObject.obj_ProductPrice.UOM__c){
					objPrice =  EntityObject.obj_ProductPrice.Price__c; 
				} else {
					if(EntityObject.obj_ProductPrice.V3_valueUOM__c== EntityObject.obj_ProductPrice.Product__r.V3_X1st_UOM__c &&
						EntityObject.obj_ProductPrice.UOM__c== EntityObject.obj_ProductPrice.Product__r.V3_X2nd_UOM__c
					){
							// 如果 Pack factor 等于 null ,error
						if(EntityObject.obj_ProductPrice.Product__r.V3_Pack_factor__c == null){
							ShowErrorMsg('Pack factor is null ! product :' + EntityObject.obj_ProductPrice.name);
							return null;
						}
						objPrice =  EntityObject.obj_ProductPrice.Price__c/EntityObject.obj_ProductPrice.Product__r.V3_Pack_factor__c;
					} else if (
					 	EntityObject.obj_ProductPrice.V3_valueUOM__c== EntityObject.obj_ProductPrice.Product__r.V3_X2nd_UOM__c &&
						EntityObject.obj_ProductPrice.UOM__c== EntityObject.obj_ProductPrice.Product__r.V3_X1st_UOM__c
					) {
							// 如果 Pack factor 等于 null ,error
						if(EntityObject.obj_ProductPrice.Product__r.V3_Pack_factor__c == null){
							ShowErrorMsg('Pack factor is null ! product :' + EntityObject.obj_ProductPrice.name);
							return null;
						}
						objPrice =  EntityObject.obj_ProductPrice.Price__c*EntityObject.obj_ProductPrice.Product__r.V3_Pack_factor__c;
						
					} else {
							ShowErrorMsg('Can not find Pack factor info;! product :' + EntityObject.obj_ProductPrice.name);
							return null;
						
					}
				}
				
				   QuoteLineItem quoteLineItem = new QuoteLineItem();
    				//salesprice
    			   quoteLineItem.UnitPrice =objPrice; 
    				//Qty
    				quoteLineItem.Quantity = EntityObject.obj_ProductPrice.Qty__c;  
    				quoteLineItem.QuoteId = quoteobj.Id;
				    //UOM
    				quoteLineItem.V3_UOM__c = EntityObject.obj_ProductPrice.V3_valueUOM__c; 
    				quoteLineItem.Product_Price__c = EntityObject.obj_ProductPrice.Id;
				   	if(mapPricebookEntry.get(EntityObject.obj_ProductPrice.Product__c) != null){
    					quoteLineItem.PricebookEntryId = mapPricebookEntry.get(EntityObject.obj_ProductPrice.Product__c).id;
    					
    				}
				   lisQuoteLineItem.add(quoteLineItem);
				   
    		}
    		system.debug('*********quoteobj.QuoteLineItems.size()**********'+quoteobj.QuoteLineItems.size());
      		if(lisQuoteLineItem.size()>0){
      							
				//添加Item的时候要确定Quote是否绑定价格PriceBook
				//如果不绑定的话,赋值
				if(quoteobj.Pricebook2Id == null){
					    PricebookEntry pricebookEntry=lisPricebookEntry[0];
						quoteobj.Pricebook2Id =pricebookEntry.Pricebook2Id;
		    			update quoteobj;
				}
    			insert lisQuoteLineItem;
	     		strInfo = 'Success!';
	     		PageReference pageRef = new PageReference('/'+Quoteid);
      			return pageRef;
    		}
    	}
      return null;
    }
}