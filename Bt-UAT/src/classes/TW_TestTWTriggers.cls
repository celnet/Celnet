/*
*Scott
*2013-10-15
*台湾trigger测试类
*/
@isTest
private class TW_TestTWTriggers {
	static testMethod void TW_AccChangeOwner()
	{
		//销售
	    UserRole RepUserRole = new UserRole() ;
	    RepUserRole.Name = 'TW South Sales Rep';
	    insert RepUserRole ;
	    /*用户简档*/
		//rep简档
	    Profile RepPro = [select Id from Profile where Name  = 'TW Sales Rep' limit 1];
		
		/************User************/
		List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ; 
	     
	    /*销售*/
	     User RepSu = new User();
	     RepSu.Username='RepSu@123.com';
	     RepSu.LastName='RepSu';
	     RepSu.Email='RepSu@123.com';
	     RepSu.Alias=user[0].Alias;
	     RepSu.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     RepSu.ProfileId=RepPro.Id;
	     RepSu.LocaleSidKey=user[0].LocaleSidKey;
	     RepSu.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     RepSu.EmailEncodingKey=user[0].EmailEncodingKey;
	     RepSu.CommunityNickname='RepSu';
	     RepSu.MobilePhone='12345678912';
	     RepSu.UserRoleId = RepUserRole.Id ;
	     RepSu.IsActive = true;
	     insert RepSu;
		/*客户*/
		RecordType accrecordtype = [select Id from RecordType where DeveloperName='TW_Hospital' and SobjectType='Account' limit 1];
		
		Account acc = new Account();
		acc.RecordTypeId = accrecordtype.Id;
		acc.Name = 'AccTest';
		insert acc;
		System.Test.startTest();
		acc.TW_NewOwner__c = RepSu.Id;
		acc.TW_ChangeOwnerApprovalStatus__c='通過';
		update acc;
		System.Test.stopTest();
	}
	
	static testMethod void TW_AccountTeamSync()
	{
		//销售
	    UserRole RepUserRole = new UserRole() ;
	    RepUserRole.Name = 'TW South Sales Rep';
	    insert RepUserRole ;
	    /*用户简档*/
		//rep简档
	    Profile RepPro = [select Id from Profile where Name  = 'TW Sales Rep' limit 1];
		
		/************User************/
		List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ; 
	     
	    /*销售*/
	     User RepSu = new User();
	     RepSu.Username='RepSu@123.com';
	     RepSu.LastName='RepSu';
	     RepSu.Email='RepSu@123.com';
	     RepSu.Alias=user[0].Alias;
	     RepSu.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     RepSu.ProfileId=RepPro.Id;
	     RepSu.LocaleSidKey=user[0].LocaleSidKey;
	     RepSu.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     RepSu.EmailEncodingKey=user[0].EmailEncodingKey;
	     RepSu.CommunityNickname='RepSu';
	     RepSu.MobilePhone='12345678912';
	     RepSu.UserRoleId = RepUserRole.Id ;
	     RepSu.IsActive = true;
	     insert RepSu;
		/*客户*/
		RecordType accrecordtype = [select Id from RecordType where DeveloperName='TW_Hospital' and SobjectType='Account' limit 1];
		
		
        System.runAs(RepSu)
        {
        	Account acc = new Account();
			acc.RecordTypeId = accrecordtype.Id;
			acc.Name = 'AccTest';
			insert acc;
        	V2_Account_Team__c ateam = new V2_Account_Team__c();
	        ateam.V2_Account__c = acc.Id;
	        ateam.V2_User__c = RepSu.Id;
	        ateam.V2_BatchOperate__c='新增';
	        ateam.V2_History__c = false;
	        insert ateam;
	        
	        V2_Account_Team__c ateam2 = new V2_Account_Team__c();
	        ateam2.V2_Account__c = acc.Id;
	        ateam2.V2_User__c = RepSu.Id;
	        ateam2.V2_BatchOperate__c='删除';
	        ateam2.V2_History__c = false;
	        insert ateam2;
	        
	        V2_Account_Team__c ateam3 = new V2_Account_Team__c();
	        ateam3.V2_Account__c = acc.Id;
	        ateam3.V2_User__c = RepSu.Id;
	        ateam3.V2_BatchOperate__c='替换';
	        ateam3.V2_NewAccUser__c = RepSu.Id;
	        ateam3.V2_History__c = false;
	        insert ateam3;
	        System.Test.startTest();
        	ateam.V2_ApprovalStatus__c ='审批通过';
	        update ateam;
	        ateam2.V2_ApprovalStatus__c ='审批通过';
	    	update ateam2;
	    	ateam3.V2_ApprovalStatus__c ='审批通过';
	    	update ateam3;
	    	System.Test.stopTest();
        	
        }
	}
	static testMethod void TW_ContactApprovalUpsert()
	{
	
		/*客户*/
		RecordType accrecordtype = [select Id from RecordType where DeveloperName='TW_Hospital' and SobjectType='Account' limit 1];
		
		Account acc = new Account();
		acc.RecordTypeId = accrecordtype.Id;
		acc.Name = 'AccTest';
		insert acc;
		/*联系人*/
		RecordType conrecordtype = [select Id from RecordType where DeveloperName = 'TW_Contact' and SobjectType='Contact' and IsActive = true limit 1 ];
		Contact con1 = new Contact();
		con1.RecordTypeId = conrecordtype.Id;
		con1.LastName = 'AccTestContact1';
		con1.AccountId=acc.Id;
		insert con1;
			
		list<RecordType> list_record = [Select r.DeveloperName, r.Id From RecordType r where r.DeveloperName in ('TW_New','TW_Update') and IsActive = true and r.SobjectType = 'Contact_Mod__c'];
		Contact_Mod__c conMod1 = new Contact_Mod__c();
		conMod1.RecordTypeId = list_record[0].Id;
		conMod1.Name__c = con1.Id;
		conMod1.Account__c = acc.Id;
		conMod1.NewContact__c ='33333';
		insert conMod1;
		
		Contact_Mod__c conMod2 = new Contact_Mod__c();
		conMod2.RecordTypeId = list_record[0].Id;
		conMod2.Name__c = con1.Id;
		conMod2.Account__c = acc.Id;
		conMod2.NewContact__c ='33333';
		insert conMod2;
		System.Test.startTest();
		conMod1.TW_ApprovalStatus__c = '通過';
		update conMod1;
		
		conMod2.TW_ApprovalStatus__c = '通過';
		update conMod2;
		System.Test.stopTest();
		
	}
	
	//测试trigger----TW_VisitExpireWarning
	static testMethod void  TW_VisitExpireWarning()
	{
		//获取事件记录类型为“拜访”的
		RecordType recordType = [Select r.SobjectType, r.Name, r.DeveloperName 
                             From RecordType r 
                             where r.SobjectType = 'Event' and r.DeveloperName = 'V2_Event'];
		
		list<Event> listEvent = new list<Event>();
		Profile p = [SELECT Id,Name FROM Profile WHERE Name='TW Sales Rep' limit 1];
		UserRole userRole = new UserRole();
		userRole.Name = 'TW角色Test';
		insert userRole;
		
        User user1 = new User();
        user1.Alias = 'newUser1'; 
        user1.Email='newuser1@testorg.com';
        user1.EmailEncodingKey='UTF-8';
        user1.LastName='Testing';
        user1.LanguageLocaleKey='en_US';
        user1.LocaleSidKey='en_US';
        user1.ProfileId = p.Id;
        user1.TimeZoneSidKey='America/Los_Angeles';
        user1.UserName='new3de@testorg.com';
        user1.UserRoleId=userRole.Id;
        insert user1;
        Contact con = new Contact();
        con.LastName = 'Contact test';
        con.Email = 'test@test.com';
        insert con;
        for(Integer i=0;i<5;i++)
        {
        	Event e = new Event();
			e.OwnerId=user1.Id;
			e.WhoId=con.Id;
			e.Type='事件Test'+i;
			e.StartDateTime=date.today();
			e.EndDateTime=date.today().addDays(i);
			e.TW_VisitExpireWarning__c=false;
			e.RecordTypeId=recordType.Id;
			e.V2_IsExpire__c=false;
			listEvent.add(e);
        }
        insert listEvent;
		
		for(Event e : listEvent)
		{
			e.TW_VisitExpireWarning__c=true;
			e.V2_IsExpire__c=true;
		}
		System.Test.startTest();
		update listEvent;
		System.Test.stopTest();
	}
	
	//测试trigger----TW_CampaignMemberWhetherDelete
	static testMethod void TW_CampaignMemberWhetherDelete()
	{
		list<CampaignMember> listCampMember = new list<CampaignMember>();
		list<Contact> listCont = new list<Contact>();
		//获取记录类型为TW_Campaign的市场活动的记录类型
		RecordType CampRecordType = [select r.Id,r.SobjectType,r.DeveloperName
									 from RecordType r
									 where r.SobjectType = 'Campaign' and r.DeveloperName = 'TW_Campaign'];
		Account acc = new Account();
		acc.Name = 'accout test';
		insert acc;
		
		for(integer i=0;i<10;i++)
		{
			Contact con = new Contact();
			con.LastName = 'contact test'+i;
			con.AccountId = acc.Id;	
			listCont.add(con);
		}
		insert listCont;
		
		Campaign camp = new Campaign();
		camp.Name = 'Campaign Test';
		camp.Status = 'test';
		camp.RecordTypeId = CampRecordType.Id;
		insert camp;
		
		CampaignMember campMemb1 = new CampaignMember();
		campMemb1.ContactId = listCont[0].Id;
		campMemb1.CampaignId = camp.Id;
		listCampMember.add(campMemb1);
		CampaignMember campMemb2 = new CampaignMember();
		campMemb2.ContactId = listCont[1].Id;
		campMemb2.CampaignId = camp.Id;
		listCampMember.add(campMemb2);
		insert listCampMember;
		System.Test.startTest();
		camp.Status = 'Sign Up Closed';
		update camp;
		/*
		list<CampaignMember> listCampMemb = [select c.Id,c.CampaignId,c.AllowToDelete__c 
											 from CampaignMember c
											 where c.CampaignId = camp.Id];
		for(CampaignMember campMemb : listCampMemb)
		{
			system.
		}									 
		*/									 
		camp.Status = 'Planned';
		update camp;
		System.Test.stopTest();
		
	}
	
}