/*
Author：Scott
Created on：2013-10-17
Description: 
复制大陆功能

1.市场部可以对通过审批并且已参加市场活动的成员创建追踪拜访（2013-3-14修改：废除此限制）
2.只可生成一次。生成的回访不允许删除。
3.生成时判断当前市场活动下的市场活动成员是否已经生成回访，如果没有则新建一条回访
4.生成回访的主题为：市场活动名称加回访
5.如果同一个销售有多个市场活动成员，那么创建的拜访时间要依次往后排
6.拜访时间为：上午9点-下午5点 （一次为15分钟） 拜访日期为：回访日期
7.市场活动追踪拜访 主题为为市场活动+回访
8.跟进内容为 回访 计划
2013-3-14修改：需要检查现系统中是否有对市场活动成员已参加的成员才能进行回访的限制，若有也取消该限制
台湾加上 已参加 这个限制
*/
public with sharing class TW_CtrlRepeatCreateEvent {
	public String CampaignIds{get;set;}
	//默认拜访开始时间
	public Integer StartHour = 9;
	//拜访结束时间
	public Integer EndHour = 17;
	//时间间隔
	public Integer Minute = 15;
	public TW_CtrlRepeatCreateEvent()
	{
		CampaignIds = ApexPages.currentPage().getParameters().get('camid');
	}
	//创建
	public void CreateEvent()
	{
		//key:提交人   value：此提交人的 追踪拜访结束时间
		Map<Id,DateTime> MapUserIdTime = new Map<Id,DateTime>();
		try
		{ 
			/******************2012-2-9新加记录类型*******************************/
			RecordType callRt = [Select Id from RecordType where DeveloperName = 'V2_Event' Limit 1];
			/******************2012-2-9新加记录类型*******************************/
			
			//删除追踪拜访
			//DeleteEvent();
			Integer success = 0;
			Integer camsum = 0;
			List<Event> inserteve = new List<Event>();
			List<CampaignMember>updatecam = new List<CampaignMember>();
			List<CampaignMember> camMemberlist = [select ContactId,User__c,Campaign.V2_FollowEndDate__c,Campaign.Name,Campaign.Content_to_follow_up__c,
							 					  V2_FollowEventFlag__c from CampaignMember 
											      where CampaignId = :CampaignIds
											      //and V2_Participated__c=true //2013-3-14修改去掉
											      //and V2_MarketingApprove__c = '通过'//2013-3-14修改去掉
											      and V2_Participated__c = true
											      and V2_FollowEventFlag__c = false ];
				if(camMemberlist !=null && camMemberlist.size()>0)
				{
					for(CampaignMember cam:camMemberlist)
					{
						DateTime starttime;
						DateTime endtime;
						camsum ++;
						Event ev = new Event();
						ev.RecordTypeId = callRt.Id;
						ev.WhoId = cam.ContactId;
						ev.WhatId = CampaignIds;
						ev.Subject = cam.Campaign.Name+'回訪';
						ev.SubjectType__c = '拜访';
						if(MapUserIdTime.containsKey(cam.User__c))
						{
							starttime = MapUserIdTime.get(cam.User__c);
						}
						else
						{
							starttime = DateTime.newInstance(cam.Campaign.V2_FollowEndDate__c, Time.newInstance(StartHour, 0, 0, 0));
						}
						endtime = starttime.addMinutes(Minute);
						MapUserIdTime.put(cam.User__c,endtime);
						if(cam.Campaign.Content_to_follow_up__c !=null && cam.Campaign.Content_to_follow_up__c != '')
						{
							ev.GAPlan__c = cam.Campaign.Content_to_follow_up__c;
						}
						ev.StartDateTime = starttime;
						ev.EndDateTime = endtime;
						ev.V2_FollowEventFlag__c = true;
						ev.OwnerId = cam.User__c;
						ev.V3_Campaign_FeedBack_End_Day__c = cam.Campaign.V2_FollowEndDate__c;
						inserteve.add(ev);
						cam.V2_FollowEventFlag__c = true;
						updatecam.add(cam);
					}
					success = inserteve.Size();
					//ca.V2_IsCreateFollowEvent__c = true;
					//update ca;
					insert inserteve;
					update updatecam;
					ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM , '成功創建追蹤拜訪：'+success+'人。');            
			        ApexPages.addMessage(msg);
			        return;
				}
				else
				{
					ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO , '當前行銷活動中沒有符合創建拜訪條件的行銷活動成員！');            
	        		ApexPages.addMessage(msg);
	        		return;
				}
		}
		catch(Exception e)
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR , String.valueOf(e));            
            ApexPages.addMessage(msg);
            return;
		}
	}
	 public PageReference ReturnCampaign()
	{
		return new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/' + CampaignIds); 
	}
	/*******************************************************测试类****************************************************/
	static testMethod void TW_CtrlRepeatCreateEvent()
	{
		/*客户*/
		RecordType accrecordtype = [select Id from RecordType where DeveloperName='TW_Hospital' and SobjectType='Account' limit 1];
		Account acc = new Account();
		acc.RecordTypeId = accrecordtype.Id;
		acc.Name = 'AccTest';
		insert acc;
		/*联系人*/
		RecordType conrecordtype = [select Id from RecordType where DeveloperName = 'TW_Contact' and SobjectType='Contact' and IsActive = true limit 1 ];
		Contact con1 = new Contact();
		con1.RecordTypeId =conrecordtype.Id; 
		con1.LastName = 'AccTestContact1';
		con1.AccountId=acc.Id;
		insert con1;
		
		Contact con2 = new Contact();
		con2.RecordTypeId =conrecordtype.Id; 
		con2.LastName = 'AccTestContact2';
		con2.AccountId=acc.Id;
		insert con2;
		/*市场活动*/
		Campaign cam = new Campaign();
		cam.Name = 'CamTest';
		cam.StartDate = date.today().addMonths(1);
		cam.EndDate = date.today().addMonths(2);
		cam.IsActive = true;
		cam.Content_to_follow_up__c = '计划！！';
		cam.V2_FollowEndDate__c = date.today();
		insert cam;
		
		Campaign cam2 = new Campaign();
		cam2.Name = 'CamTest2';
		cam2.StartDate = date.today().addMonths(1);
		cam2.EndDate = date.today().addMonths(2);
		cam2.IsActive = true;
		insert cam2;
		/*市场活动成员*/
		CampaignMember cm = new CampaignMember();
		cm.CampaignId = cam.Id;
		cm.ContactId = con2.Id;
		cm.V2_MarketingApprove__c = '通过';
		cm.V2_Participated__c=true;
		insert cm;
		
		
		Test.startTest();
		ApexPages.currentPage().getParameters().put('camid',cam.Id);
		TW_CtrlRepeatCreateEvent crce = new TW_CtrlRepeatCreateEvent();
		//创建追踪拜访
		crce.CreateEvent();
		
		ApexPages.currentPage().getParameters().put('camid',cam2.Id);
		TW_CtrlRepeatCreateEvent crce2 = new TW_CtrlRepeatCreateEvent();
		//创建追踪拜访
		crce2.CreateEvent();
		Test.stopTest();
		
	}
}