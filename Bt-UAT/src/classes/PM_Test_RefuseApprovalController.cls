/**
 * Hank
 * 2013-10-16
 * PM_RefuseApprovalController 测试类
 */
@isTest
private class PM_Test_RefuseApprovalController {

    static testMethod void myUnitTest() 
    {
    	system.Test.startTest();
    	PatientApply__c patientApply = new PatientApply__c();
    	PM_Patient__c pat =new PM_Patient__c();
    	
        MonthlyPlan__c MonPlan =new MonthlyPlan__c();
        //MonPlan.OwnerId = user.Id;
        insert MonPlan;
        //新建省市
        Region__c zone = new Region__c();
        zone.Name = '南区';
        insert zone;
		Provinces__c pro = new Provinces__c();
    	pro.Name = '浙江省';
    	insert pro;
    	Cities__c city = new Cities__c();
    	city.Name = '杭州市';
    	city.BelongToProvince__c = pro.Id;
    	insert city;
    	
    	//客户
    	list<RecordType> record = [Select Id From RecordType Where Name = '医院'];
    	
    	list<RecordType> record2 = [Select Id From RecordType Where Name = '经销商'];
    	
        Account acc = new  Account();
        acc.Name = '浙江医院';
        acc.RecordTypeId = record[0].Id;
        acc.Provinces__c = pro.Id;
        acc.Cities__c = city.Id;
        acc.Region__c = zone.Id;
        insert acc;
        Account acc2 = new  Account();
        acc2.Name = '浙江医药控股';
        acc2.RecordTypeId = record2[0].Id;
        acc2.Provinces__c = pro.Id;
        acc2.Cities__c = city.Id;
        acc2.Region__c = zone.Id;
        insert acc2;
        
    	patientApply.Status__c = '待审批';
    	patientApply.Distributor__c = acc2.Id;
    	patientApply.IntubationHospital__c = acc.Id;
    	patientApply.PatientApply__c = MonPlan.Id;
    	patientApply.PatientName__c = 'aaa';
    	patientApply.PM_Famliy_Phone__c = '010-12345678';
    	patientApply.PM_Patient_MPhone__c = '12345678901';
    	patientApply.PM_Patient_Phone__c = '010-98765432';
    	patientApply.PM_Family_Member_Mphone__c = '12345678901';
    	patientApply.PM_Famliy_Member_Phone__c = '12345678901';
    	patientApply.PM_Other_Phone__c = '1234567890';
    	patientApply.Address__c = 'Test123';
    	patientApply.PatientApply__c = MonPlan.Id;
    	insert patientApply;
    	
    	
    	
    	PatientApply__c patientApply2 = new PatientApply__c();
    	patientApply2.PatientName__c = 'aa';
    	patientApply2.Status__c = '待审批';
    	patientApply2.Distributor__c = acc2.Id;
    	patientApply2.IntubationHospital__c = acc.Id;
    	patientApply2.PM_Famliy_Phone__c = '010-12345678';
    	patientApply2.PM_Patient_MPhone__c = '12345678901';
    	patientApply2.PM_Patient_Phone__c = '010-98765432';
    	patientApply2.PM_Family_Member_Mphone__c = '12345678901';
    	patientApply2.PM_Famliy_Member_Phone__c = '12345678901';
    	patientApply2.PM_Other_Phone__c = '1234567890';
    	patientApply2.Address__c = 'Test123';
    	patientApply2.PatientApply__c = MonPlan.Id;
    	insert patientApply2;
    	ApexPages.currentPage().getParameters().Put('patId',patientApply.Id);
    	ApexPages.StandardController controller = new ApexPages.StandardController(patientApply);
        //实例化控制类
    	PM_RefuseApprovalController rea = new PM_RefuseApprovalController(controller);
    	rea.Reject_Reason = '拒绝测试';
    	rea.Agree();
    	
    	ApexPages.currentPage().getParameters().Put('patId',patientApply2.Id);
    	ApexPages.StandardController controllerss = new ApexPages.StandardController(patientApply2);
        //实例化控制类
    	PM_RefuseApprovalController reas = new PM_RefuseApprovalController(controllerss);
    	reas.Reject_Reason = '拒绝测试';
    	reas.Agree();
    	
    	system.Test.stopTest();
    }
}