/*
Author: Eleen
Created on: 2012-1-11
Description: 
	This webservice is to be save User's Role to RoleHistory;
	It will be run from a visualforce web named V2_SaveRoleHistory when click confirm.  
*/
global class V2_ClsSaveRoleHistoryWebservice {
	
	webservice static String V2_ClsCheckRoleHistoryHadExisted(String str_year,String str_month)
	{
		string result;//init result
		List<User> list_user=[select Name from User];//select all user
		set<String> set_user=new set<String>();//users' Name set collection
		for(User ur:list_user)
		{
			string str_user=str_year+'-'+str_month+'-'+ur.Id;
			set_user.add(str_user);
		}
		//select count while the record in RoleHistory had existed
		AggregateResult[] agg_vc=[select COUNT(Id)total from V2_RoleHistory__c where Key__c in:set_user and isDeleted=false];
		AggregateResult agg_IdCount;
		if(agg_vc.size()>0)
		{
			agg_IdCount=agg_vc[0];
			result=string.valueOf(agg_IdCount.get('total'));
		}
		return result;
	}
	
	webservice static void V2_ClsSaveRoleHistoryWebservice(String str_year,String str_month)
	{
		V2_ClsSaveRoleHistory.V2_ClsSaveRoleHistory(str_year, str_month);
	}
}