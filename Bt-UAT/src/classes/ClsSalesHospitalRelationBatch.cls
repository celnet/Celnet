/**
 * Author : Sunny
 * 每月1号凌晨，自动处理替换、删除操作
 **/
global class ClsSalesHospitalRelationBatch implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext BC){
    	//获取应处理的销售医院关系
    	//1.替换操作：操作为替换；审批通过；不为历史；生效日期小于等于今天
    	//2.删除操作：操作为删除；审批通过；不为历史；失效日期小于今天
    	return Database.getQueryLocator([Select Id,V2_Account__c,V2_Account__r.Name,V2_User__c,V2_BatchOperate__c,V2_NewAccUser__c From V2_Account_Team__c Where 
            (V2_BatchOperate__c = '替换' And V2_ApprovalStatus__c = '审批通过' And V2_History__c = false And NewEffDate__c <= TODAY) 
            Or 
            (V2_BatchOperate__c = '删除' And V2_ApprovalStatus__c = '审批通过' And V2_History__c = false And DeleteDate__c < TODAY)]);   
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope){
    	List<V2_Account_Team__c> list_atUp = new List<V2_Account_Team__c>();
    	Set<ID> Set_ReplaceAccountId = new Set<ID>();//需要替换的AccountId
    	//Set<ID> Set_ReplaceNewSalesId = new Set<ID>();//需要替换的销售
    	Set<ID> Set_ReplaceOldSalesId = new Set<ID>();//需要被替换的销售
    	Set<ID> Set_DeleteOldSalesId = new Set<ID>();//需要被删除的销售
    	Set<ID> Set_DeleteAccountId = new Set<ID>();//需要删除处理的AccountId
    	Set<String> Set_DeleteSales = new Set<String>();//删除操作，旧销售ID+客户ID
    	Map<String , ID> map_ReplaceSales = new Map<String , ID>(); //key:旧销售ID+客户ID   value:新销售ID
    	//Map<ID , ID> map_SalesId = new Map<ID , ID>();//替换操作时，旧销售ID - 新销售ID
    	for(sObject sObj : scope){
    		V2_Account_Team__c vat = (V2_Account_Team__c)sObj;
    		if(vat.V2_BatchOperate__c == '替换'){//替换操作需要处理业务机会
    			Set_ReplaceAccountId.add(vat.V2_Account__c);
    			Set_ReplaceOldSalesId.add(vat.V2_User__c);
    			map_ReplaceSales.put(vat.V2_User__c+''+vat.V2_Account__c , vat.V2_NewAccUser__c);
    		}else if(vat.V2_BatchOperate__c == '删除'){
    			Set_DeleteSales.add(vat.V2_User__c+''+vat.V2_Account__c);
    			Set_DeleteAccountId.add(vat.V2_Account__c);
    			Set_DeleteOldSalesId.add(vat.V2_User__c);
    		}
    		vat.V2_IsSyn__c = true;
    		vat.V2_History__c = true;
    		
    		list_atUp.add(vat);
    	}
    	
    	//处理业务机会
    	if(Set_ReplaceAccountId.size() > 0 && Set_ReplaceOldSalesId.size() > 0){
    		List<Opportunity> list_opp = new List<Opportunity>();
    		for(Opportunity opp : [Select Id,OwnerId,AccountId From Opportunity Where OwnerId in: Set_ReplaceOldSalesId And AccountId in: Set_ReplaceAccountId]){
    			if(map_ReplaceSales.containsKey(opp.OwnerId +''+opp.AccountId)){
    				opp.OwnerId = map_ReplaceSales.get(opp.OwnerId +''+opp.AccountId) ;
    				list_opp.add(opp);
    			}
    		}
    		if(list_opp.size() > 0){
    			update list_opp;
    		}
    	}
    	
    	//处理客户小组
    	if((Set_ReplaceOldSalesId.size() > 0 || Set_DeleteOldSalesId.size() > 0) && (Set_ReplaceAccountId.size() > 0 || Set_DeleteAccountId.size() >0)){
    		List<AccountTeamMember> list_atmDel = new List<AccountTeamMember>();
    		for(AccountTeamMember atm :[Select a.UserId, a.Id, a.AccountId From AccountTeamMember a Where (a.AccountId in: Set_ReplaceAccountId Or a.AccountId in: Set_DeleteAccountId) And (a.UserId in: Set_DeleteOldSalesId Or a.UserId in: Set_ReplaceOldSalesId)]){
    			if(map_ReplaceSales.containsKey(atm.UserId+''+atm.AccountId) || Set_DeleteSales.contains(atm.UserId+''+atm.AccountId)){
    				list_atmDel.add(atm);
    			}
    		}
    		if(list_atmDel.size() > 0){
    			delete list_atmDel;
    		}
    	}
    	//更新销售医院关系
    	if(list_atUp.size() > 0){
    		update list_atUp;
    	}
    }
    global void finish(Database.BatchableContext BC){
    	
    }
}