/**
 * Author : Sunny
 * 每月25日，自动发邮件给
 **/
global class ClsRemindSalesHospitalRelationBatch implements Database.Batchable<sObject> {
	global string strWhere ='';
    global List<User> start(Database.BatchableContext BC){
        //List<User> list_Supervisor = [Select Id,UserRoleId From User Where UserRole.Name like: '%Supervisor%'];
        //bill update 2013-7-16 自动发邮件给Renal/MD：发给SS、DSM、ASM均收到管辖区域内所有下属
        List<User> list_Supervisor = new List<User>();
        if(strWhere.indexOf('SS')>=0 && strWhere.indexOf('ASM')>=0 && strWhere.indexOf('DSM')>=0)
        list_Supervisor = [Select Id,UserRoleId,UserRole.Name From User Where (UserRole.Name like '%MD%' OR UserRole.Name like '%Renal%') AND (UserRole.Name like '%Supervisor%'  OR UserRole.Name like '%ASM%' OR UserRole.Name like '%DSM%')];
        else if(strWhere.indexOf('SS')>=0 && strWhere.indexOf('ASM')>=0 && strWhere.indexOf('DSM')< 0)
        list_Supervisor = [Select Id,UserRoleId,UserRole.Name From User Where (UserRole.Name like '%MD%' OR UserRole.Name like '%Renal%') AND (UserRole.Name like '%Supervisor%'  OR UserRole.Name like '%ASM%')];
        else if(strWhere.indexOf('SS')>=0 && strWhere.indexOf('DSM')>=0 && strWhere.indexOf('ASM')< 0)
        list_Supervisor = [Select Id,UserRoleId,UserRole.Name From User Where (UserRole.Name like '%MD%' OR UserRole.Name like '%Renal%') AND (UserRole.Name like '%Supervisor%'  OR UserRole.Name like '%DSM%')];
        else if(strWhere.indexOf('ASM')>=0 && strWhere.indexOf('DSM')>=0 && strWhere.indexOf('SS')< 0)
        list_Supervisor = [Select Id,UserRoleId,UserRole.Name From User Where (UserRole.Name like '%MD%' OR UserRole.Name like '%Renal%') AND (UserRole.Name like '%ASM%' OR UserRole.Name like '%DSM%')];
        else if(strWhere.indexOf('SS')>=0 && strWhere.indexOf('ASM')<0 && strWhere.indexOf('DSM')< 0)
        list_Supervisor = [Select Id,UserRoleId,UserRole.Name From User Where (UserRole.Name like '%MD%' OR UserRole.Name like '%Renal%') AND UserRole.Name like '%Supervisor%'];
        else if(strWhere.indexOf('ASM')>=0 && strWhere.indexOf('DSM') < 0 && strWhere.indexOf('SS')< 0)
        list_Supervisor = [Select Id,UserRoleId,UserRole.Name From User Where (UserRole.Name like '%MD%' OR UserRole.Name like '%Renal%') AND UserRole.Name like '%ASM%'];
        else if(strWhere.indexOf('DSM')>=0 && strWhere.indexOf('SS')<0 && strWhere.indexOf('ASM')<0)
        list_Supervisor = [Select Id,UserRoleId,UserRole.Name From User Where (UserRole.Name like '%MD%' OR UserRole.Name like '%Renal%') AND UserRole.Name like '%DSM%'];
        return list_Supervisor;
        //return Database.getQueryLocator('');
    }
    global void execute(Database.BatchableContext BC, List<User> listSupervisor){
        for(User user : listSupervisor){
            this.SendEmailReportTo(user.Id); 
        }
    }
    global void finish(Database.BatchableContext BC){
        
    }
    private void SendEmailReportTo(ID reportToUserId){
    	system.debug('Report To User Id:'+reportToUserId);
        EmailTemplate temp =[Select Name, IsActive, Id, DeveloperName From EmailTemplate Where DeveloperName = 'SalesHospitalRelationReport'];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setSaveAsActivity(false);
        mail.setTemplateId(temp.Id);
        mail.setTargetObjectId(reportToUserId);
        //mail.setToAddresses( new String[] {'tommyliu@cideatech.com'});//测试用
        mail.setReplyTo('no-reply@salesforce.com');
        mail.setSenderDisplayName('Baxter SEP System');
        if(!Test.isRunningTest()) 
        {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
}