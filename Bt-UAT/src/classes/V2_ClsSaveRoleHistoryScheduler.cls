/*
Author: Eleen
Created on: 2012-1-10
Description: 
	This Class will be run at the start of every month for save User's Role to RoleHistory. 
*/
global class V2_ClsSaveRoleHistoryScheduler implements Schedulable
{
	global void execute(SchedulableContext ctx)
	{
		V2_ClsSaveRoleHistory.V2_ClsSaveRoleHistory(string.valueOf(Date.today().year()), string.valueOf(Date.today().month()));
	}
}