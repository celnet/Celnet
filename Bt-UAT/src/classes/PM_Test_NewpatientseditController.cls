/**
 * Hank
 * 2013-10-16
 *PM_NewpatientseditController  测试类
 */
@isTest
private class PM_Test_NewpatientseditController {

    static testMethod void myUnitTest() 
    {
    	system.Test.startTest();
    	PatientApply__c patientApply = new PatientApply__c();
    	PM_Patient__c pat =new PM_Patient__c();
    	 MonthlyPlan__c MonPlan =new MonthlyPlan__c();
        insert MonPlan;
        //新建省市
        Region__c zone = new Region__c();
        zone.Name = '南区';
        insert zone;
		Provinces__c pro = new Provinces__c();
    	pro.Name = '浙江省';
    	insert pro;
    	Cities__c city = new Cities__c();
    	city.Name = '杭州市';
    	city.BelongToProvince__c = pro.Id;
    	insert city;
    	
    	//客户
    	list<RecordType> record = [Select Id From RecordType Where Name = '医院'];
    	
    	list<RecordType> record2 = [Select Id From RecordType Where Name = '经销商'];
    	
        Account acc = new  Account();
        acc.Name = '浙江医院';
        acc.RecordTypeId = record[0].Id;
        acc.Provinces__c = pro.Id;
        acc.Cities__c = city.Id;
        acc.Region__c = zone.Id;
        insert acc;
        Account acc2 = new  Account();
        acc2.Name = '浙江医药控股';
        acc2.RecordTypeId = record2[0].Id;
        acc2.Provinces__c = pro.Id;
        acc2.Cities__c = city.Id;
        acc2.Region__c = zone.Id;
        insert acc2;
    	ApexPages.StandardController controller = new ApexPages.StandardController(patientApply);
        //实例化控制类
    	PM_NewpatientseditController onp = new PM_NewpatientseditController(controller);
    	pat.Name = 'aaa';
    	pat.PM_HomeTel__c = '010-12345678';
    	pat.PM_PmPhone__c = '12345678901';
    	pat.PM_PmTel__c = '010-98765432';
    	pat.PM_FamilyPhone__c = '12345678901';
    	pat.PM_FamilyTel__c = '12345678901';
    	pat.PM_OtherTel2__c = '1234567890';
    	pat.PM_Address__c = 'Test123';
    	insert pat;
    	PM_Patient__c pat1 =new PM_Patient__c();
    	pat1.Name = 'b1';
    	pat1.PM_PmPhone__c = '12345678901';
    	pat1.PM_PmTel__c = '010-98765432';
    	pat1.PM_FamilyPhone__c = '12345678901';
    	pat1.PM_FamilyTel__c = '12345678901';
    	pat1.PM_OtherTel2__c = '1234567890';
    	pat1.PM_Address__c = 'Test123';
    	insert pat1;
    	PM_Patient__c pat2 =new PM_Patient__c();
    	pat2.Name = 'b2';
    	pat2.PM_PmTel__c = '010-98765432';
    	pat2.PM_FamilyPhone__c = '12345678901';
    	pat2.PM_FamilyTel__c = '12345678901';
    	pat2.PM_OtherTel2__c = '1234567890';
    	pat2.PM_Address__c = 'Test123';
    	insert pat2;
    	PM_Patient__c pat3 =new PM_Patient__c();
    	pat3.Name = 'b3';
    	pat3.PM_FamilyPhone__c = '12345678901';
    	pat3.PM_FamilyTel__c = '12345678901';
    	pat3.PM_OtherTel2__c = '1234567890';
    	pat3.PM_Address__c = 'Test123';
    	insert pat3;
    	PM_Patient__c pat4 =new PM_Patient__c();
    	pat4.Name = 'b4';
    	pat4.PM_FamilyTel__c = '12345678901';
    	pat4.PM_OtherTel2__c = '1234567890';
    	pat4.PM_Address__c = 'Test123';
    	insert pat4;
    	PM_Patient__c pat5 =new PM_Patient__c();
    	pat5.Name = 'b5';
    	pat5.PM_OtherTel2__c = '1234567890';
    	pat5.PM_Address__c = 'Test123';
    	insert pat5;
    	PM_Patient__c pat6 =new PM_Patient__c();
    	pat6.Name = 'b6';
    	pat6.PM_Address__c = 'Test123';
    	insert pat6;
    	//姓名电话地址都重复
    	onp.patientApply.PatientName__c = 'aaa';
    	onp.patientApply.PM_Famliy_Phone__c = '010-12345678';
    	onp.patientApply.PM_Patient_MPhone__c = '12345678901';
    	onp.patientApply.PM_Patient_Phone__c = '010-98765432';
    	onp.patientApply.PM_Family_Member_Mphone__c = '12345678901';
    	onp.patientApply.PM_Famliy_Member_Phone__c = '12345678901';
    	onp.patientApply.PM_Other_Phone__c = '1234567890';
    	onp.patientApply.Address__c = 'Test123';
    	onp.Hold();
    	
    	//姓名电话地址都不重复
    	onp.patientApply.PatientName__c = 'xxx';
    	onp.patientApply.PM_Famliy_Phone__c = '010-12345671';
    	onp.patientApply.PM_Patient_MPhone__c = '12345678902';
    	onp.patientApply.PM_Patient_Phone__c = '010-98765433';
    	onp.patientApply.PM_Family_Member_Mphone__c = '12345678904';
    	onp.patientApply.PM_Famliy_Member_Phone__c = '12345678905';
    	onp.patientApply.PM_Other_Phone__c = '1234567896';
    	onp.patientApply.Address__c = 'T';
    	onp.Hold();
    	
    	//姓名电话地址都重复
    	onp.patientApply.PatientName__c = 'b1';
    	onp.patientApply.PM_Famliy_Phone__c = '010-12345678';
    	onp.patientApply.PM_Patient_MPhone__c = '12345678901';
    	onp.patientApply.PM_Patient_Phone__c = '010-98765432';
    	onp.patientApply.PM_Family_Member_Mphone__c = '12345678901';
    	onp.patientApply.PM_Famliy_Member_Phone__c = '12345678901';
    	onp.patientApply.PM_Other_Phone__c = '1234567890';
    	onp.patientApply.Address__c = 'Test123';
    	onp.Hold();
    	
    	//姓名电话地址都重复
    	onp.patientApply.PatientName__c = 'b2';
    	onp.patientApply.PM_Famliy_Phone__c = '010-12345678';
    	onp.patientApply.PM_Patient_MPhone__c = '12345678901';
    	onp.patientApply.PM_Patient_Phone__c = '010-98765432';
    	onp.patientApply.PM_Family_Member_Mphone__c = '12345678901';
    	onp.patientApply.PM_Famliy_Member_Phone__c = '12345678901';
    	onp.patientApply.PM_Other_Phone__c = '1234567890';
    	onp.patientApply.Address__c = 'Test123';
    	onp.Hold();
    	
    	//姓名电话地址都重复
    	onp.patientApply.PatientName__c = 'b3';
    	onp.patientApply.PM_Famliy_Phone__c = '010-12345678';
    	onp.patientApply.PM_Patient_MPhone__c = '12345678901';
    	onp.patientApply.PM_Patient_Phone__c = '010-98765432';
    	onp.patientApply.PM_Family_Member_Mphone__c = '12345678901';
    	onp.patientApply.PM_Famliy_Member_Phone__c = '12345678901';
    	onp.patientApply.PM_Other_Phone__c = '1234567890';
    	onp.patientApply.Address__c = 'Test123';
    	onp.Hold();
    	
    	//姓名电话地址都重复
    	onp.patientApply.PatientName__c = 'b4';
    	onp.patientApply.PM_Famliy_Phone__c = '010-12345678';
    	onp.patientApply.PM_Patient_MPhone__c = '12345678901';
    	onp.patientApply.PM_Patient_Phone__c = '010-98765432';
    	onp.patientApply.PM_Family_Member_Mphone__c = '12345678901';
    	onp.patientApply.PM_Famliy_Member_Phone__c = '12345678901';
    	onp.patientApply.PM_Other_Phone__c = '1234567890';
    	onp.patientApply.Address__c = 'Test123';
    	onp.Hold();
    	
    	//姓名电话地址都重复
    	onp.patientApply.PatientName__c = 'b5';
    	onp.patientApply.PM_Famliy_Phone__c = '010-12345678';
    	onp.patientApply.PM_Patient_MPhone__c = '12345678901';
    	onp.patientApply.PM_Patient_Phone__c = '010-98765432';
    	onp.patientApply.PM_Family_Member_Mphone__c = '12345678901';
    	onp.patientApply.PM_Famliy_Member_Phone__c = '12345678901';
    	onp.patientApply.PM_Other_Phone__c = '1234567890';
    	onp.patientApply.Address__c = 'Test123';
    	onp.Hold();
    	
    	//姓名电话地址都重复
    	onp.patientApply.PatientName__c = 'b6';
    	onp.patientApply.PM_Famliy_Phone__c = '010-12345678';
    	onp.patientApply.PM_Patient_MPhone__c = '12345678901';
    	onp.patientApply.PM_Patient_Phone__c = '010-98765432';
    	onp.patientApply.PM_Family_Member_Mphone__c = '12345678901';
    	onp.patientApply.PM_Famliy_Member_Phone__c = '12345678901';
    	onp.patientApply.PM_Other_Phone__c = '1234567890';
    	onp.patientApply.Address__c = 'Test123';
    	onp.Hold();
    	//姓名电话重复
    	onp.patientApply.PatientName__c = 'aaa';
	 	onp.patientApply.PM_Famliy_Phone__c = '010-12345678';
    	onp.patientApply.PM_Patient_MPhone__c = '12345678901';
    	onp.patientApply.PM_Patient_Phone__c = '010-98765432';
    	onp.patientApply.PM_Family_Member_Mphone__c = '12345678901';
    	onp.patientApply.PM_Famliy_Member_Phone__c = '12345678901';
    	onp.patientApply.PM_Other_Phone__c = '1234567890';
    	onp.patientApply.Address__c = 'Test123';
    	onp.Hold();
    	
    	//地址电话重复
    	onp.patientApply.PatientName__c = 'ccc';
	 	onp.patientApply.PM_Famliy_Phone__c = '010-12345678';
    	onp.patientApply.PM_Patient_MPhone__c = '12345678901';
    	onp.patientApply.PM_Patient_Phone__c = '010-98765432';
    	onp.patientApply.PM_Family_Member_Mphone__c = '12345678901';
    	onp.patientApply.PM_Famliy_Member_Phone__c = '12345678901';
    	onp.patientApply.PM_Other_Phone__c = '1234567890';
    	onp.patientApply.Address__c = 'Test123';
    	onp.Hold();
    	
    	//电话重复
    	onp.patientApply.PatientName__c = 'ccc';
    	onp.patientApply.PM_Famliy_Phone__c = '010-12345678';
    	onp.patientApply.PM_Patient_MPhone__c = '12345678901';
    	onp.patientApply.PM_Patient_Phone__c = '010-98765432';
    	onp.patientApply.PM_Family_Member_Mphone__c = '12345678901';
    	onp.patientApply.PM_Famliy_Member_Phone__c = '12345678901';
    	onp.patientApply.PM_Other_Phone__c = '1234567890';
    	onp.patientApply.Address__c = 'Test';
    	onp.Hold();
    	
    	onp.Confirm();
    	onp.back();
    	onp.Cancel();
		ApexPages.StandardController controllers = new ApexPages.StandardController(patientApply);
        //实例化控制类
    	PM_NewpatientseditController onps = new PM_NewpatientseditController(controllers);
    	onps.patientApply.PatientName__c = 'aaa';
    	onps.patientApply.PM_Famliy_Phone__c = 'aaa';
    	onps.patientApply.PM_Patient_MPhone__c = 'aaa';
    	onps.patientApply.PM_Patient_Phone__c = 'aaa';
    	onps.patientApply.PM_Family_Member_Mphone__c = 'aaa';
    	onps.patientApply.PM_Famliy_Member_Phone__c = 'aaa';
    	onps.patientApply.PM_Other_Phone__c = 'aaa';
    	onps.patientApply.Address__c = 'Test';
    	onps.Hold();
    	
    	onps.patientApply.PatientName__c = 'aaa';
    	onps.patientApply.PM_Famliy_Phone__c = '010-010-010';
    	onps.patientApply.PM_Patient_MPhone__c = 'aaa';
    	onps.patientApply.PM_Patient_Phone__c = '010-010-010';
    	onps.patientApply.PM_Family_Member_Mphone__c = 'aaa';
    	onps.patientApply.PM_Famliy_Member_Phone__c = '010-010-010';
    	onps.patientApply.PM_Other_Phone__c = 'aaa';
    	onps.patientApply.Address__c = 'Test';
    	onps.Hold();
    	
    	onps.patientApply.PatientName__c = 'aaa';
    	onps.patientApply.PM_Famliy_Phone__c = '010-010a';
    	onps.patientApply.PM_Patient_MPhone__c = 'aaa';
    	onps.patientApply.PM_Patient_Phone__c = '010-010a';
    	onps.patientApply.PM_Family_Member_Mphone__c = 'aaa';
    	onps.patientApply.PM_Famliy_Member_Phone__c = '010-a';
    	onps.patientApply.PM_Other_Phone__c = 'aaa';
    	onps.patientApply.Address__c = 'Test';
    	onps.Hold();
    	ApexPages.StandardController controllerss = new ApexPages.StandardController(patientApply);
        //实例化控制类
    	PM_NewpatientseditController onpss = new PM_NewpatientseditController(controllerss);
    	onpss.patientApply.PatientName__c = 'aaa';
    	onpss.patientApply.IntubationDate__c = date.today();
    	onpss.patientApply.PM_Famliy_Phone__c = null;
    	onpss.patientApply.PM_Patient_MPhone__c = null;
    	patientApply.Distributor__c = acc.Id;
    	patientApply.IntubationHospital__c = acc2.Id;
    	onpss.Hold();
    	
    	system.Test.stopTest();
    }
}