/*
 * Author：Bill
 * Created on：2013-6-27
 * Description: 
 * 系统每月25日自动发送邮件给主管
*/
public class ClsReSalesHosRelationBatchSchedule implements Schedulable {
		//global
	public void execute(SchedulableContext SC) 
	{
	    ClsRemindSalesHospitalRelationBatch salesBatch = new ClsRemindSalesHospitalRelationBatch();
	    database.executeBatch(salesBatch,10);
	}
	
	static testMethod void myUnitTest() {		
        system.test.startTest();
        string sch=DateTime.now().second()+' '+DateTime.now().minute()+' '+DateTime.now().hour()+' * * ?';
        ClsReSalesHosRelationBatchSchedule re = new ClsReSalesHosRelationBatchSchedule();
        System.schedule('test', sch , re);
        system.test.stopTest();
	}
}