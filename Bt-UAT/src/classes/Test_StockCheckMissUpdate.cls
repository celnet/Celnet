/**
 * 作者：bill
 * StockCheckMissUpdate的trigger的测试类
 */
@isTest
private class Test_StockCheckMissUpdate {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        VaporizerInfo__c vap = new VaporizerInfo__c();
        vap.Name = 'NM008990111';
        insert vap;
        
        StockChecking__c stock = new StockChecking__c();
        stock.VaporizerInfo__c = vap.Id;
        stock.MissApply__c = true;
        stock.ConfirmStatus__c = '未确认';
        insert stock;
        
        test.startTest();
        stock.ConfirmStatus__c = '已确认';
        update stock;
        test.stopTest();
    }
}