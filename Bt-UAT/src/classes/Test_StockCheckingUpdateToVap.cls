/**
 * author : bill
 * StockCheckingUpdateToVap的trigger的测试类
 */
@isTest
private class Test_StockCheckingUpdateToVap {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        //挥发罐
        VaporizerInfo__c vap = new VaporizerInfo__c();
        insert vap;
        //盘点
        StockChecking__c stock = new StockChecking__c();
        stock.VaporizerInfo__c = vap.Id;
        stock.ConfirmStatus__c = '未确认';
        insert stock;
        
        test.startTest();
        stock.ConfirmStatus__c = '已确认';
        update stock;
        test.stopTest();
    }
}