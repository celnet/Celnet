/**
 * Author：Bill
 * Date:2013.11.10
 * DESCRIPTION:PM_HDRelationController测试类 
 */
@isTest
private class PM_Test_HDRelationController {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
            	//客户
    	list<RecordType> record = [Select Id From RecordType Where SobjectType = 'Account' and DeveloperName = 'RecordType'];
    	list<RecordType> record2 = [Select Id From RecordType Where SobjectType = 'Account' and DeveloperName = 'RecordType_d_2'];
    	
    	//新建省市
        Region__c region = new Region__c();
        region.Name = '南区';
        insert region;
        Provinces__c pro1 = new Provinces__c();
    	pro1.Name = '浙江省';
    	insert pro1;
		PM_Province__c pro = new PM_Province__c();
    	pro.Name = '浙江省';
    	insert pro;
    	Cities__c city = new Cities__c();
    	city.Name = '杭州市';
    	city.PM_Province__c = pro.Id;
    	city.BelongToProvince__c = pro1.Id;
    	insert city;
    	
    	//医院
    	List<Account> list_acc = new List<Account>();
        Account acc = new  Account();
        acc.Name = '浙江医院';
        acc.RecordTypeId = record[0].Id;
        acc.PM_Province__c = pro.Id;
        acc.Cities__c = city.Id;
        acc.Region__c = region.Id;
        list_acc.add(acc);
        Account acc1 = new  Account();
        acc1.Name = '广州医院';
        acc1.RecordTypeId = record[0].Id;
        acc1.Region__c = region.Id;
        list_acc.add(acc1);
        Account acc2 = new  Account();
        acc2.Name = '上海经销商';
        acc2.RecordTypeId = record2[0].Id;
        list_acc.add(acc2);
        insert list_acc;
        
        system.Test.startTest();
    	PM_HosDealerRelation__c psrr = new PM_HosDealerRelation__c();
    	ApexPages.StandardController controller = new ApexPages.StandardController(psrr);
        PM_HDRelationController HD = new PM_HDRelationController(controller);
        ApexPages.StandardSetController conset = HD.conset;
		HD.InHospital = '浙江医院';
		HD.HosDea.PM_Distributor__c = acc2.Id;
    	HD.Province = null;
    	HD.City = null;
    	HD.CheckWithOutDealer();
        List<PM_HDRelationController.HDWrapper> list_pageHD = HD.list_pageHD;
    	HD.next();
    	HD.first();
    	HD.last();
    	HD.previous();
    	HD.Check();
    	for(PM_HDRelationController.HDWrapper wra : HD.list_AllHD)
    	{
    		wra.IsChecked = true;
    	}
    	HD.Save();
    	HD.ContinueSave();

        PM_HDRelationController HD2 = new PM_HDRelationController(controller);
    	HD2.City = '杭州市';
    	HD2.HosDea.PM_NewDistributor__c = acc2.Id;
    	HD2.InHospital = null;
    	HD2.Province = null;
    	HD2.Check();
    	HD2.Save();
    	HD2.ContinueSave();
    	system.Test.stopTest();
    }
}