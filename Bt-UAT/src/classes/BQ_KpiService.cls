/**
 * 作者：Scott
 * 说明：SEP BQ KPI计算
 * 时间：2014-1-20
**/
public without sharing class BQ_KpiService {
	//先默认为22个工作日，如果自定义设置中有录入对应月份的工作日， 则取设置天数
	Integer Workdays = 22;
	//指定月份拜访
  	public List<Event> List_Event{get;set;}
  	//每月科内会/幻灯点评会/小型病案讨论会举行频率
  	final Set<String> Set_Meeting = new Set<String>{'科内会','幻灯点评会','小型病案讨论会'};
  	 
	public BQ_KpiService(Id userid,Integer year,Integer month)
	{
        //本月所有的拜访事件
	    Date CurrentDate = Date.newInstance(year,month,1);
	    DateTime CurrentStartMonth = DateTime.newInstance(CurrentDate.toStartOfMonth(),Time.newInstance(00, 00, 00, 00));
	    DateTime CurrentEndMonth = DateTime.newInstance(CurrentDate.toStartOfMonth().addMonths(1),Time.newInstance(00, 00, 00, 00));
	    List_Event = [select Done__c,WhatId,SubjectType__c,BQ_E_detailing__c,WhoId,RecordType.DeveloperName from Event where V2_IsExpire__c = false
	            	  and StartDateTime>=:CurrentStartMonth and StartDateTime <:CurrentEndMonth and OwnerId =: userid];
        
        
        //本月实际工作日
        List<BQ_WorkingDay__c> List_BQMp = [select BQ_WorkDay__c from BQ_WorkingDay__c where BQ_Month__c =: month limit 1];
    	if(List_BQMp[0].BQ_WorkDay__c !=null || List_BQMp[0].BQ_WorkDay__c > 0)
    	{
    		//固定工作日
        	Workdays = integer.valueOf(List_BQMp[0].BQ_WorkDay__c);
    	}
    }
    //得到BQ奖金参数信息
    public List<V2_BonusParameterDetails__c> getBonusParameterInfo(String BonusType)
    {
    	List<V2_BonusParameterDetails__c> List_BPD = new List<V2_BonusParameterDetails__c>();
        for(V2_BonusParameterDetails__c bpd: [Select Name,V2_BonusStandard__c,V2_Weight__c From V2_BonusParameterDetails__c 
                                              where V2_BonusStandard__c != null and V2_Weight__c != null 
                                              and V2_Bonus__r.V2_BonusType__c =: BonusType and V2_Bonus__r.V2_Department__c =: 'BQ' order by Name asc])
        {
            V2_BonusParameterDetails__c vbp = new V2_BonusParameterDetails__c();
            //奖金计算参数明细名称
            vbp.Name = bpd.Name;
            
            //参数标准
            vbp.V2_BonusStandard__c = bpd.V2_BonusStandard__c;
            //权重
            vbp.V2_Weight__c = bpd.V2_Weight__c;
            List_BPD.add(vbp);
        }
        return List_BPD;
    }
    /*销售*/
	//K1 每日拜访次数
	public Bonus_data__c getK1(Bonus_data__c UserBonus)
	{
		//==实际工作日==
		UserBonus.BQ_Work_Days__c = Workdays;
		//==总拜访数完成且未过期==
		UserBonus.BQ_Completed_Visit__c = 0;
		for(Event ev : List_Event)
		{
			if(ev.SubjectType__c != '拜访' || ev.WhoId == null)
			{
				continue;
			}
			if(ev.Done__c )
			{
				UserBonus.BQ_Completed_Visit__c++;
			}
		} 
		//==每日拜访次数==
		UserBonus.BQ_Daily_Visit__c = UserBonus.BQ_Completed_Visit__c/Workdays;
		
		return UserBonus;
	}
	
	/*14-3-11修改
	 1、当K1（日拜访完成次数）≥12次/天，得2分，每季度满分为6分。
	 2、如果每月拜访完成率<80%，则当月K1评估为0分。
	 3、如果每月拜访完成率≥80％（含80％），而<100%，计1分。
	  日拜访完成次数=每月实际总拜访次数/每月实际工作天数（每日完成数需≥12次） 
	  月拜访完成率＝每月实际拜访次数/每月计划数（计奖及格线需≥ 80%）
	*/
	//活动本月月计划的拜访完成数
	public Bonus_data__c getK1Aid(Bonus_data__c UserBonus)
	{
		for(MonthlyPlan__c mp : [select Percent__c,V2_TotalCallRecords__c,V2_FinishedCallRecords__c from MonthlyPlan__c where Month__c=:UserBonus.Month__c and Year__c=:UserBonus.Year__c and OwnerId=:UserBonus.The_User__c])
		{
			UserBonus.BQ_TotalCallRecords__c = mp.V2_TotalCallRecords__c;
			UserBonus.BQ_FinishedCallRecords__c = mp.V2_FinishedCallRecords__c;
		}
		return UserBonus;
	} 
	
	//K2 每月覆盖客户人数
	public Bonus_data__c getK2(Bonus_data__c UserBonus)
	{
		UserBonus.BQ_Cover_Customer_Number__c =0;
		Set<Id> SetContactId = new Set<Id>();//通过set来避免重复的
		for(Event ev : List_Event)
		{
			if(ev.SubjectType__c != '拜访' || ev.WhoId == null)
			{
				continue;
			}
			if(ev.Done__c )
			{
				SetContactId.add(ev.WhoId);
			}
		}
		if(SetContactId.size()>0)
		{
			UserBonus.BQ_Cover_Customer_Number__c = SetContactId.size();
		}
		return UserBonus;
	}
	//K3 每月E-detailing使用
	public Bonus_data__c getK3(Bonus_data__c UserBonus)
	{
		//总拜访数
		UserBonus.BQ_Month_Visit__c = 0;
		//已完成拜访记录中E-detailing使用次数
		UserBonus.BQ_Completed_Visit_ED_UseNumber__c = 0;
		for(Event ev : List_Event)
		{
			if(ev.SubjectType__c != '拜访' || ev.WhoId == null)
			{
				continue;
			}
			if(ev.Done__c )
			{
				UserBonus.BQ_Month_Visit__c++;
				if(ev.BQ_E_detailing__c)
				{
					UserBonus.BQ_Completed_Visit_ED_UseNumber__c++;
				}
			}
			
		}
		if(UserBonus.BQ_Month_Visit__c>0)
		{
			UserBonus.BQ_ED_UseNumber__c = UserBonus.BQ_Completed_Visit_ED_UseNumber__c/UserBonus.BQ_Month_Visit__c;
		}
		else
		{
			UserBonus.BQ_ED_UseNumber__c = 0;
		}
		return UserBonus;
	}
	//K4 每月科内会/幻灯点评会/小型病案讨论会举行频率
	public Bonus_data__c getK4(Bonus_data__c UserBonus)
	{
		UserBonus.BQ_Meeting_Frequency__c = 0;
		for(Event ev : List_Event)
		{
			if(ev.Done__c && Set_Meeting.contains(ev.SubjectType__c))
			{
				UserBonus.BQ_Meeting_Frequency__c++;
			}
		}
		return UserBonus;
	}
	
	//K4所属季度总会议次数
	public Bonus_data__c getK4Total(Bonus_data__c UserBonus)
	{
		String month = UserBonus.Month__c;
		String Quarter;
		if(month=='1'||month=='2'||month=='3'){Quarter = '第一季度';}
		else if(month=='4'||month=='5'||month=='6'){Quarter = '第二季度';}
		else if(month=='7'||month=='8'||month=='9'){Quarter = '第三季度';}
		else{Quarter = '第四季度';}
		//所属季度总会议次数
		UserBonus.BQ_Meeting_Total__c = UserBonus.BQ_Meeting_Frequency__c;//先赋值本月的
		//本季度此用户奖金数据
		List<Bonus_data__c> List_bonus = [select BQ_Meeting_Frequency__c,BQ_Meeting_Total__c,Month__c from Bonus_data__c 
										 where BQ_Quarter__c=:Quarter and Year__c=:UserBonus.Year__c
										 and The_User__c =:UserBonus.The_User__c];
		if(List_bonus !=null && List_bonus.size()>0)
		{
			for(Bonus_data__c bonus:List_bonus)
			{
				if(bonus.Month__c == UserBonus.Month__c)
				{
					continue;//如果本月已经创建奖金数据需过滤，因为已经赋值本月的次数
				}
				UserBonus.BQ_Meeting_Total__c+=bonus.BQ_Meeting_Frequency__c;
			}
			//update 本季度其他月的数据
			if(UserBonus.BQ_Meeting_Total__c>0)
			{
				for(Bonus_data__c bonus:List_bonus)
				{
					if(bonus.Month__c == UserBonus.Month__c)
					{
						continue;//如果本月已经创建奖金数据需过滤，因为已经赋值本月的次数
					}
					bonus.BQ_Meeting_Total__c+=UserBonus.BQ_Meeting_Total__c;
					update bonus;
				}
			}
			
		}
		
		return UserBonus;
	}
	
	
	
	/*主管*/
	//K5团队拜访执行
	public Bonus_data__c getK5(Bonus_data__c UserBonus)
	{
		//==下属个数==
		UserBonus.BQ_Rep_Count__c = 0;
		Set<Id>Set_Userids = new Set<Id>(); //下属ids
		for(V2_RoleHistory__c u :[select Name__c from V2_RoleHistory__c Where Month__c =: UserBonus.Month__c And Year__c =: UserBonus.Year__c 
		        			And IsOnHoliday__c = false And IsLeave__c = false And Status__c != '停用'  and GBU__c='BQ'and Manager__c=:UserBonus.OwnerId])
		{
			Set_Userids.add(u.Name__c);
		}
		UserBonus.BQ_Rep_Count__c = Set_Userids.size();
		//==下属总分==
		UserBonus.BQ_Rep_Total_Score__c = 0;
		for(Bonus_data__c bonus : [select Total_Score__c from Bonus_data__c where OwnerId in:Set_Userids 
									and Year__c=:UserBonus.Year__c and Month__c=:UserBonus.Month__c and Total_Score__c !=null])
		{
			UserBonus.BQ_Rep_Total_Score__c +=bonus.Total_Score__c;
		}
		//团队拜访执行
		UserBonus.BQ_Rep_Visit__c = 0;
		if(UserBonus.BQ_Rep_Count__c !=0)
		{
			UserBonus.BQ_Rep_Visit__c = UserBonus.BQ_Rep_Total_Score__c/UserBonus.BQ_Rep_Count__c;
		}
		return UserBonus;
	}
	//K6协访的执行
	public Bonus_data__c getK6(Bonus_data__c UserBonus)
	{
		//==协访记录==
		Date startDate = date.valueOf(UserBonus.Year__c+'-'+UserBonus.Month__c+'-1');
        Date endDate = startDate.addMonths(2);
        Set<ID> set_UEventId = new Set<ID>();
        for(AssVisitComments__c AssVisitComm : [Select Id,EventId__c,IsAssVisit__c From AssVisitComments__c Where CreatedDate >=: startDate And CreatedDate <: endDate 
        										And ReUser__c =: UserBonus.OwnerId and Comment__c !=null]){
           set_UEventId.add(AssVisitComm.EventId__c);
        }
        //主管本月个人协访次数
        UserBonus.BQ_Director_Personal_Visit__c = 0;
        Date CurrentDate = startDate;
	    DateTime CurrentStartMonth = DateTime.newInstance(CurrentDate.toStartOfMonth(),Time.newInstance(00, 00, 00, 00));
	    DateTime CurrentEndMonth = DateTime.newInstance(CurrentDate.toStartOfMonth().addMonths(1),Time.newInstance(00, 00, 00, 00));
        for(Event e: [Select Id,GAPlan__c,WhoId,GAExecuteResult__c,Key_Information__c From Event Where Id in: set_UEventId And RecordType.DeveloperName = 'V2_Event'
        			  And StartDateTime >=: CurrentStartMonth And StartDateTime <: CurrentEndMonth]){
            UserBonus.BQ_Director_Personal_Visit__c++;
        }
		return UserBonus;
	}
	/*大区*/
	//K7团队拜访执行
	public Bonus_data__c getK7(Bonus_data__c UserBonus)
	{
		//所有下属
		UserBonus.BQ_Rep_Count__c = 0;
		Set<Id>Set_Userids = new Set<Id>(); //下属ids
		for(V2_RoleHistory__c u :[select Name__c from V2_RoleHistory__c Where Month__c =: UserBonus.Month__c And Year__c =: UserBonus.Year__c 
		        			And IsOnHoliday__c = false And IsLeave__c = false And Status__c != '停用'  and GBU__c='BQ'and Manager__c=:UserBonus.OwnerId])
		{
			Set_Userids.add(u.Name__c);
		}
		UserBonus.BQ_Rep_Count__c = Set_Userids.size();
		//==协访记录==
		Date startDate = date.valueOf(UserBonus.Year__c+'-'+UserBonus.Month__c+'-1');
        Date endDate = startDate.addMonths(2);
        Set<ID> set_UEventId = new Set<ID>();
        for(AssVisitComments__c AssVisitComm : [Select Id,EventId__c,IsAssVisit__c From AssVisitComments__c Where CreatedDate >=: startDate And CreatedDate <: endDate 
        										And ReUser__c in : Set_Userids and Comment__c !=null]){
           set_UEventId.add(AssVisitComm.EventId__c);
        }
        //下属总协访次数
        UserBonus.BQ_Total_Visit__c = 0;
        Date CurrentDate = startDate;
	    DateTime CurrentStartMonth = DateTime.newInstance(CurrentDate.toStartOfMonth(),Time.newInstance(00, 00, 00, 00));
	    DateTime CurrentEndMonth = DateTime.newInstance(CurrentDate.toStartOfMonth().addMonths(1),Time.newInstance(00, 00, 00, 00));
        for(Event e: [Select Id,GAPlan__c,WhoId,GAExecuteResult__c,Key_Information__c From Event Where Id in: set_UEventId And RecordType.DeveloperName = 'V2_Event'
        			  And StartDateTime >=: CurrentStartMonth And StartDateTime <: CurrentEndMonth]){
             UserBonus.BQ_Total_Visit__c++;
        }
        //下属平均协访次数
        UserBonus.BQ_Directors_Visit__c = 0;
        if(UserBonus.BQ_Rep_Count__c != 0)
        {
        	UserBonus.BQ_Directors_Visit__c = UserBonus.BQ_Total_Visit__c/UserBonus.BQ_Rep_Count__c;
        }
		return UserBonus;
	}
	//K8个人协访的执行
	public Bonus_data__c getK8(Bonus_data__c UserBonus)
	{
		//==协访记录==
		Date startDate = date.valueOf(UserBonus.Year__c+'-'+UserBonus.Month__c+'-1');
        Date endDate = startDate.addMonths(2);
        Set<ID> set_UEventId = new Set<ID>();
        for(AssVisitComments__c AssVisitComm : [Select Id,EventId__c,IsAssVisit__c From AssVisitComments__c Where CreatedDate >=: startDate And CreatedDate <: endDate 
        										And ReUser__c =: UserBonus.OwnerId and Comment__c !=null]){
           set_UEventId.add(AssVisitComm.EventId__c);
        }
        //大区经理个人协访次数
        UserBonus.BQ_Regional_Visit__c = 0;
        Date CurrentDate = startDate;
	    DateTime CurrentStartMonth = DateTime.newInstance(CurrentDate.toStartOfMonth(),Time.newInstance(00, 00, 00, 00));
	    DateTime CurrentEndMonth = DateTime.newInstance(CurrentDate.toStartOfMonth().addMonths(1),Time.newInstance(00, 00, 00, 00));
        for(Event e: [Select Id,GAPlan__c,WhoId,GAExecuteResult__c,Key_Information__c From Event Where Id in: set_UEventId And RecordType.DeveloperName = 'V2_Event'
        			  And StartDateTime >=: CurrentStartMonth And StartDateTime <: CurrentEndMonth]){
            UserBonus.BQ_Regional_Visit__c++;
        }
		return UserBonus;
	}
	
	/*
	*季度月平均分数
	*/
	public Bonus_data__c getQuarterScores(Bonus_data__c UserBonus)
	{
		String month = UserBonus.Month__c;
		String Quarter;
		if(month=='1'||month=='2'||month=='3'){Quarter = '第一季度';}
		else if(month=='4'||month=='5'||month=='6'){Quarter = '第二季度';}
		else if(month=='7'||month=='8'||month=='9'){Quarter = '第三季度';}
		else{Quarter = '第四季度';}
		//本季度此用户奖金数据
		List<Bonus_data__c> List_bonus = [select BQ_MonthlyAverageScore__c,Total_Score__c,Month__c from Bonus_data__c 
										 where BQ_Quarter__c=:Quarter and Year__c=:UserBonus.Year__c
										 and The_User__c =:UserBonus.The_User__c];
		//所属季度月平均分
		UserBonus.BQ_MonthlyAverageScore__c = 0;
		//所属季度月总分
		Decimal TotalScore = UserBonus.Total_Score__c;//先赋值本月的								 
		if(List_bonus != null && List_bonus.size()>0)
		{
			for(Bonus_data__c bonus:List_bonus)
			{
				if(bonus.Month__c == UserBonus.Month__c)
				{
					continue;//如果本月已经创建奖金数据需过滤，因为已经赋值本月的次数
				}
				TotalScore+=bonus.Total_Score__c;
			}
			UserBonus.BQ_MonthlyAverageScore__c = TotalScore/3;
			
			//update 本季度其他月的数据
			if(UserBonus.BQ_Meeting_Total__c>0)
			{
				for(Bonus_data__c bonus:List_bonus)
				{
					if(bonus.Month__c == UserBonus.Month__c)
					{
						continue;//如果本月已经创建奖金数据需过滤，因为已经赋值本月的次数
					}
					bonus.BQ_MonthlyAverageScore__c+=UserBonus.BQ_MonthlyAverageScore__c;
					update bonus;
				}
			}
		}								 
		return UserBonus;
	}
}