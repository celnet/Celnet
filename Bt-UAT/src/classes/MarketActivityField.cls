/**
 * 作者:Bill
 * 说明：市场活动相关表的页面显示所需字段
**/
public class MarketActivityField {
	//市场活动成员中联系人
    public string CampaignMen{get;set;}
    //客户名称
    public string AccountName{get;set;}
    //数据拥有人
    public string NewOwner{get;set;}
    
    //实际销量LIST
    public String[] list_ActualSales{get;set;}
    //目标销量LIST
    public String[] list_GoalSales{get;set;}
    //完成率LIST
    public String[] list_CompletionRates{get;set;}
    //Surviving Target
    public String[] SurvivingTargets{get;set;}
    //Surviving This Year
    public String[] SurvivingThisYears{get;set;}
    //New Patient Target
    public String[] NewPatientTargets{get;set;}
    //New Patient This Year
    public String[] NewPatientThisYears{get;set;}
    //DOR Last Year
    public String[] DORLastYears{get;set;}
    //DOR This Year
    public String[] DORThisYears{get;set;}
    //实际金额LIST
    public String[] list_ActualAmount{get;set;}
    //目标金额LIST
    public String[] list_GoalAmount{get;set;}
    //完成率LIST
    public String[] list_AmountRates{get;set;}
    
    public MarketActivityField(){
    	list_ActualSales = new String[]{};
    	list_GoalSales = new String[]{};
    	list_CompletionRates = new String[]{};
    	list_ActualAmount = new String[]{};
    	list_GoalAmount = new String[]{};
    	list_AmountRates = new String[]{};
    	SurvivingTargets = new String[]{};
    	SurvivingThisYears = new String[]{};
    	NewPatientTargets = new String[]{};
    	NewPatientThisYears = new String[]{};
    	DORLastYears = new String[]{};
    	DORThisYears = new String[]{};
    }
}