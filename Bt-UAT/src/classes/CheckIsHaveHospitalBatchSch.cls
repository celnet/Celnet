/**
 * Schedulable CheckIsHaveHospitalBatch
 * 
 */
global class CheckIsHaveHospitalBatchSch implements Schedulable{
    global void execute(SchedulableContext sc) {
    	CheckIsHaveHospitalBatch checkBatch = new CheckIsHaveHospitalBatch();
        database.executeBatch(checkBatch);
    }
    
    static testMethod void test(){
        
        
        system.test.startTest();
        string sch=DateTime.now().second()+' '+DateTime.now().minute()+' '+DateTime.now().hour()+' * * ?';
        CheckIsHaveHospitalBatchSch cp = new CheckIsHaveHospitalBatchSch();
        System.schedule('test',sch , cp);
        system.test.stopTest();
    }
}