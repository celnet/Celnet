/**
 * 作者：Sunny
 * 管理员筛选页面
 */
public class Ctrl_OppEvaluationSearch {
	public List<TableLine> list_line{get;set;}
	public Boolean ShowMsg{get;set;}
	public String strMsg{get;set;}
	public String strAlias{get;set;}
	public String strMonth{get;set;}
	public List<SelectOption> getMonths(){
		List<SelectOption> options = new List<SelectOption>();
		for(Integer i=1 ; i<=12 ; i++){
			options.add(new SelectOption(String.valueOf(i) , String.valueOf(i)));
		}
		return options;
	}
	public String strYear{get;set;}
	public List<SelectOption> getYears(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption(String.valueOf(date.today().year()) , String.valueOf(date.today().year())));
        options.add(new SelectOption(String.valueOf(date.today().addYears(-1).year()) , String.valueOf(date.today().addYears(-1).year())));
        return options;
    }
    public Ctrl_OppEvaluationSearch(){
    	strMonth = String.valueOf(date.today().Month());
    	strYear = String.valueOf(date.today().Year());
    	ShowMsg = false ;
    	strMsg = '';
    }
    public void SearchEva(){
    	ShowMsg = false ;
    	list_line = new List<TableLine>();
    	List<User> listU = [Select Id,Alias,UserRoleId From User Where Alias like: '%'+strAlias+'%'];
    	if(listU==null || listU.size() == 0){
    		strMsg = '没有找到相应主管，请检查所填写主管别名是否正确。';
    		ShowMsg = true;
    	}else{
    		TableLine tl = new TableLine();
    		tl.strName = listU[0].Alias;
    		tl.strMonth = strMonth ;
    		tl.strYear = strYear ;
    		tl.strUrl = 'https://'+system.Url.getSalesforceBaseUrl().getHost()+'/apex/OppStrategyCommentLevelTwoForAdmin?year='+strYear+'&month='+strMonth+'&uid='+listU[0].Id+'&urid='+listU[0].UserRoleId;
    		list_line.add(tl);
    	}
    }
    public class TableLine{
        public String strName{get;set;}
        public String strMonth{get;set;}
        public String strYear{get;set;}
        public String strUrl{get;set;}
    }
    
    static testMethod void myUnitTest() {
    	Ctrl_OppEvaluationSearch oppEvaSearch = new Ctrl_OppEvaluationSearch();
    	oppEvaSearch.getMonths();
    	oppEvaSearch.getYears();
    	oppEvaSearch.strAlias = [Select Id,Alias From User Where Id =: userinfo.getUserId()].Alias;
    	oppEvaSearch.SearchEva();
    	oppEvaSearch.strAlias = 'xx';
    	oppEvaSearch.SearchEva();
    }
}