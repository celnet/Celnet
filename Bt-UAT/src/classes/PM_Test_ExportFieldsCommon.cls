/**
 * Author：bill
 * date：2013-10-15
 * PM_ExportFieldsCommon的测试类
 */
@isTest
private class PM_Test_ExportFieldsCommon {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        system.Test.startTest();
        PM_ExportFieldsCommon field = new PM_ExportFieldsCommon();
        PM_ExportFieldsCommon.Patient patient = new PM_ExportFieldsCommon.Patient();
        
        PM_Patient__c Patient1 = new PM_Patient__c();
    	Patient1.Name = '张先生';
		Patient1.PM_Status__c = 'New';
		Patient1.PM_NewPatientDate__c = Date.today().addMonths(-3);
		Patient1.PM_LastSuccessDateTime__c  = DateTime.now().addMonths(-2);
		Patient1.PM_VisitState__c = '失败';
		Patient1.PM_InDate__c = Date.today().addMonths(-5);
		Patient1.PM_TelPriorityFirst__c = '病人手机';
		Patient1.PM_TelPrioritySecond__c = '家属电话';
		
		field.judgePhoneType(Patient1);
		field.judgeClockNum(50, 30, 90);
		field.judgeClockNum(150, 30, 90);
		field.getActivePatientListFields();
		field.getBaseInfoFields();
		field.getCareInfoFields();
		field.getClinicalInfoFields();
		field.getContactFields();
		field.getDealerInfoFields();
		field.getDropInfoFields();
		field.getDropoutPatientListFields();
		field.getHighEffectFields();
		field.getHighRiskFields();
		field.getInInfoFields();
		field.getNewPatientListFields();
		field.getTreatInfoFields();
		List<PM_ExportFieldsCommon.ExportField> list_Export = field.getUnreachablePatientListFields();
		for(PM_ExportFieldsCommon.ExportField ex : list_Export)
		{
			ex.IsChecked = true;
		}
		field.getApiNameFields(list_Export);
		system.Test.stopTest();
    }
}