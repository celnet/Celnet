/**
 * 2014-1-13
 * Tobe
 * BQ Trigger 测试类
 */
@isTest(SeeAllData=true)

private class BQ_TestBQTrigger {

	static testMethod void BQ_AutoStatisticsMonthPlanAct()
	{
		/************User************/
		//销售
	    UserRole RepUserRole = new UserRole() ;
	    RepUserRole.Name = 'BQ South Sales Rep';
	    insert RepUserRole ;
		List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ; 
	     //rep简档
	    Profile RepPro = [select Id from Profile where Name  = 'BQ Sales Rep' limit 1];
	    /*销售*/
	     User RepSu = new User();
	     RepSu.Username='RepSu@123.com';
	     RepSu.LastName='RepSu';
	     RepSu.Email='RepSu@123.com';
	     RepSu.Alias=user[0].Alias;
	     RepSu.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     RepSu.ProfileId=RepPro.Id;
	     RepSu.LocaleSidKey=user[0].LocaleSidKey;
	     RepSu.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     RepSu.EmailEncodingKey=user[0].EmailEncodingKey;
	     RepSu.CommunityNickname='RepSu';
	     RepSu.MobilePhone='12345678912';
	     RepSu.UserRoleId = RepUserRole.Id ;
	     RepSu.IsActive = true;
	     insert RepSu;
	     /***************月计划**********/
	     MonthlyPlan__c monthlyPlan = new MonthlyPlan__c();
	     monthlyPlan.Year__c = string.valueOf(Date.today().year());
	     monthlyPlan.Month__c = string.valueOf(Date.today().month());
	     monthlyPlan.OwnerId = RepSu.Id;
	     insert monthlyPlan;
	     /*************拜访事件***********/
	     RecordType recordType = [select Id From RecordType Where DeveloperName='V2_Event' and SobjectType = 'Event'];
	     list<Event> list_Event = new list<Event>();
	     Event event1 = new Event();
	     event1.OwnerId = RepSu.Id;
	     event1.RecordTypeId = recordType.Id;
	     event1.StartDateTime = Date.Today();
	     event1.EndDateTime = Date.Today();
	     event1.SubjectType__c = '科室会';
	     list_Event.add(event1);
	     
	     Event event2 = new Event();
	     event2.OwnerId = RepSu.Id;
	     event2.RecordTypeId = recordType.Id;
	     event2.StartDateTime = Date.Today();
	     event2.EndDateTime = Date.Today();
	     event2.SubjectType__c = '幻灯点评会';
	     list_Event.add(event2);
	     
	     Event event3 = new Event();
	     event3.OwnerId = RepSu.Id;
	     event3.RecordTypeId = recordType.Id;
	     event3.StartDateTime = Date.Today();
	     event3.EndDateTime = Date.Today();
	     event3.SubjectType__c = '院内会';
	     list_Event.add(event3);
	     
	     Event event4 = new Event();
	     event4.OwnerId = RepSu.Id;
	     event4.RecordTypeId = recordType.Id;
	     event4.StartDateTime = Date.Today();
	     event4.EndDateTime = Date.Today();
	     event4.SubjectType__c = '小型病案讨论会';
	     list_Event.add(event4);
	     
	     Event event5 = new Event();
	     event5.OwnerId = RepSu.Id;
	     event5.RecordTypeId = recordType.Id;
	     event5.StartDateTime = Date.Today();
	     event5.EndDateTime = Date.Today();
	     event5.SubjectType__c = '科室会';
	     list_Event.add(event5);
	     
	     Event event6 = new Event();
	     event6.OwnerId = RepSu.Id;
	     event6.RecordTypeId = recordType.Id;
	     event6.StartDateTime = Date.Today();
	     event6.EndDateTime = Date.Today();
	     event6.SubjectType__c = '科室会';
	     list_Event.add(event6);
	     
	     insert list_Event;
	     
	}
    static testMethod void BQ_AutoConvertContact() 
    {
    	/*客户*/
		RecordType accrecordtype = [select Id from RecordType where DeveloperName='RecordType' and SobjectType='Account' limit 1];
		
		Account acc = new Account();
		acc.RecordTypeId = accrecordtype.Id;
		acc.Name = 'AccTest';
		insert acc;
		/*联系人*/
		RecordType conrecordtype = [select Id from RecordType where DeveloperName = 'BQ_Contact' and SobjectType='Contact' and IsActive = true limit 1 ];
		Contact con1 = new Contact();
		con1.RecordTypeId = conrecordtype.Id;
		con1.LastName = 'AccTestContact1';
		con1.AccountId=acc.Id;
		insert con1;
			
		list<RecordType> list_record = [Select r.DeveloperName, r.Id From RecordType r where r.DeveloperName in ('BQ_New','BQ_Update') and IsActive = true and r.SobjectType = 'Contact_Mod__c'];
		Contact_Mod__c conMod1 = new Contact_Mod__c();
		conMod1.RecordTypeId = list_record[0].Id;
		conMod1.Name__c = con1.Id;
		conMod1.Account__c = acc.Id;
		conMod1.NewContact__c ='33333';
		insert conMod1;
		
		Contact_Mod__c conMod2 = new Contact_Mod__c();
		conMod2.RecordTypeId = list_record[0].Id;
		conMod2.Name__c = con1.Id;
		conMod2.Account__c = acc.Id;
		conMod2.NewContact__c ='33333';
		insert conMod2;
		System.Test.startTest();
		conMod1.Status__c = '通过';
		update conMod1;
		
		conMod2.TW_ApprovalStatus__c = '通过';
		update conMod2;
		System.Test.stopTest();
    }
    static testMethod void BQ_AccountTeamSync()
	{
		//销售
	    UserRole RepUserRole = new UserRole() ;
	    RepUserRole.Name = 'BQ South Sales Rep';
	    insert RepUserRole ;
	    /*用户简档*/
		//rep简档
	    Profile RepPro = [select Id from Profile where Name  = 'BQ Sales Rep' limit 1];
		
		/************User************/
		List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ; 
	     
	    /*销售*/
	     User RepSu = new User();
	     RepSu.Username='RepSu@123.com';
	     RepSu.LastName='RepSu';
	     RepSu.Email='RepSu@123.com';
	     RepSu.Alias=user[0].Alias;
	     RepSu.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     RepSu.ProfileId=RepPro.Id;
	     RepSu.LocaleSidKey=user[0].LocaleSidKey;
	     RepSu.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     RepSu.EmailEncodingKey=user[0].EmailEncodingKey;
	     RepSu.CommunityNickname='RepSu';
	     RepSu.MobilePhone='12345678912';
	     RepSu.UserRoleId = RepUserRole.Id ;
	     RepSu.IsActive = true;
	     insert RepSu;
		/*客户*/
		RecordType accrecordtype = [select Id from RecordType where DeveloperName='RecordType' and SobjectType='Account' limit 1];
		
		
        System.runAs(RepSu)
        {
        	Account acc = new Account();
			acc.RecordTypeId = accrecordtype.Id;
			acc.Name = 'AccTest';
			insert acc;
        	V2_Account_Team__c ateam = new V2_Account_Team__c();
	        ateam.V2_Account__c = acc.Id;
	        ateam.V2_User__c = RepSu.Id;
	        ateam.V2_BatchOperate__c='新增';
	        ateam.V2_History__c = false;
	        insert ateam;
	        
	        V2_Account_Team__c ateam2 = new V2_Account_Team__c();
	        ateam2.V2_Account__c = acc.Id;
	        ateam2.V2_User__c = RepSu.Id;
	        ateam2.V2_BatchOperate__c='删除';
	        ateam2.V2_History__c = false;
	        insert ateam2;
	        
	        V2_Account_Team__c ateam3 = new V2_Account_Team__c();
	        ateam3.V2_Account__c = acc.Id;
	        ateam3.V2_User__c = RepSu.Id;
	        ateam3.V2_BatchOperate__c='替换';
	        ateam3.V2_NewAccUser__c = RepSu.Id;
	        ateam3.V2_History__c = false;
	        insert ateam3;
	        
	        
	        System.Test.startTest();
	        
        	ateam.V2_ApprovalStatus__c ='审批通过';
	        update ateam;
	        ateam2.V2_ApprovalStatus__c ='审批通过';
	    	update ateam2;
	    	ateam3.V2_ApprovalStatus__c ='审批通过';
	    	update ateam3;
	    	System.Test.stopTest();
        	
        }
	}
	
	
	static testMethod void BQ_KPI()
	{
        //user role
        UserRole RegionalRole = new UserRole();
        RegionalRole.Name = 'BQ-Regional-东一区-Nutrition-Manager(吴以洪)';
        insert RegionalRole;
        
        UserRole SupervisorRole = new UserRole();
        SupervisorRole.Name = 'BQ-District DSM-上海-Nutrition-Manager(吴莉萍)';
        SupervisorRole.ParentRoleId = RegionalRole.Id;
        insert SupervisorRole;
        
        UserRole RepRole = new UserRole();
        RepRole.Name = 'BQ-Rep-上海-Nutrition-Rep(邓冠文)';
        RepRole.ParentRoleId = RepRole.Id;
        insert RepRole;
       	
       	//简档
       	List<Profile> list_pro = [select Id from Profile where Name='BQ Sales Rep' or Name='BQ Sales Supervisor'];
        //user 
        list<User> user=[Select TimeZoneSidKey,ProfileId,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname, Alias From User Where IsActive = true and id =:userInfo.getUserId()];
        List<User> list_user = new List<User>();
        //大区
        User Regional=new User();
        Regional.username='user1r2@123.com';
        Regional.LastName='user1r2';
        Regional.Email='user1r2@123.com';
        Regional.Alias=user[0].Alias;
        Regional.TimeZoneSidKey=user[0].TimeZoneSidKey;
        Regional.ProfileId=list_pro[1].Id;
        Regional.LocaleSidKey=user[0].LocaleSidKey;
        Regional.LanguageLocaleKey=user[0].LanguageLocaleKey;
        Regional.EmailEncodingKey=user[0].EmailEncodingKey;
        Regional.CommunityNickname='chequ';
        Regional.MobilePhone='12345678912';
        Regional.IsActive = true;
        Regional.UserRoleId=RegionalRole.Id ;
        insert Regional;
        //主管
        User Supervisor=new User();
        Supervisor.username='user2r2@223.com';
        Supervisor.LastName='user2r2';
        Supervisor.Email='user2r2@223.com';
        Supervisor.Alias=user[0].Alias;
        Supervisor.TimeZoneSidKey=user[0].TimeZoneSidKey;
        Supervisor.ProfileId=list_pro[1].Id;
        Supervisor.LocaleSidKey=user[0].LocaleSidKey;
        Supervisor.LanguageLocaleKey=user[0].LanguageLocaleKey;
        Supervisor.EmailEncodingKey=user[0].EmailEncodingKey;
        Supervisor.CommunityNickname='chequ1';
        Supervisor.MobilePhone='22345678922';
        Supervisor.IsActive = true;
        Supervisor.UserRoleId=SupervisorRole.Id ;
        Supervisor.ManagerId = Regional.Id;
        insert Supervisor;
        //rep
        User Rep =new User();
        Rep.username='user2r22@223.com';
        Rep.LastName='user2r22';
        Rep.Email='user2r22@223.com';
        Rep.Alias=user[0].Alias;
        Rep.TimeZoneSidKey=user[0].TimeZoneSidKey;
        Rep.ProfileId=list_pro[0].Id;
        Rep.LocaleSidKey=user[0].LocaleSidKey;
        Rep.LanguageLocaleKey=user[0].LanguageLocaleKey;
        Rep.EmailEncodingKey=user[0].EmailEncodingKey;
        Rep.CommunityNickname='chequ21';
        Rep.MobilePhone='223456789222';
        Rep.IsActive = true;
        Rep.UserRoleId=RepRole.Id ;
        Rep.ManagerId = Supervisor.Id;
        insert Rep;
        
        //角色历史
        //Rep
        V2_RoleHistory__c RepHistory = new V2_RoleHistory__c();
        RepHistory.Year__c = String.valueOf(Date.Today().Year());
        RepHistory.Month__c = String.valueOf(Date.Today().Month());
        RepHistory.Role__c = 'Rep';
        RepHistory.GBU__c = 'BQ';
        RepHistory.Manager__c = Supervisor.Id;
        RepHistory.Name__c = Rep.Id;
        insert RepHistory;
        //Supervisor
        V2_RoleHistory__c SupervisorHistory = new V2_RoleHistory__c();
        SupervisorHistory.Year__c = String.valueOf(Date.Today().Year());
        SupervisorHistory.Month__c = String.valueOf(Date.Today().Month());
        SupervisorHistory.Role__c = 'Supervisor';
        SupervisorHistory.GBU__c = 'BQ';
        SupervisorHistory.Manager__c = Regional.Id;
        SupervisorHistory.Name__c = Supervisor.Id;
        insert SupervisorHistory;
        //Regional
        V2_RoleHistory__c RegionalHistory = new V2_RoleHistory__c();
        RegionalHistory.Year__c = String.valueOf(Date.today().Year());
        RegionalHistory.Month__c = String.valueOf(Date.today().Month());
        RegionalHistory.Role__c = 'Regional';
        RegionalHistory.GBU__c = 'BQ';
        RegionalHistory.Name__c = Regional.Id;
        insert RegionalHistory;
        
        //联系人
        Contact ct1 = new Contact();
        ct1.FirstName = 'jim';
        ct1.LastName = 'Green';
        ct1.OwnerId = Rep.Id;
        insert ct1;
        //拜访
        List<Event> list_insert = new List<Event>();
        System.runAs(Rep)
        {
        	
        	Event eve = new Event();
        	eve.WhoId = ct1.Id;
        	eve.Done__c = true;
        	eve.StartDateTime = DateTime.Now();
        	eve.EndDateTime = DateTime.Now().addHours(1);
        	eve.SubjectType__c='拜访';
        	list_insert.add(eve);
        	Event eve2 = new Event();
        	eve2.WhoId = ct1.Id;
        	eve2.Done__c = true;
        	eve2.StartDateTime = DateTime.Now();
        	eve2.EndDateTime = DateTime.Now().addHours(1);
        	eve2.SubjectType__c='拜访';
        	eve2.BQ_E_detailing__c = true;
        	Event eve3 = new Event();
        	eve3.WhoId = ct1.Id;
        	eve3.Done__c = true;
        	eve3.StartDateTime = DateTime.Now();
        	eve3.EndDateTime = DateTime.Now().addHours(1);
        	eve3.SubjectType__c='小型病案讨论会';
        	eve3.BQ_E_detailing__c = true;
        	list_insert.add(eve3);
        	insert list_insert;
        	
        }
        System.runAs(Supervisor)
        {
        	AssVisitComments__c ac = new AssVisitComments__c();
        	ac.EventId__c = list_insert[0].Id;
        	ac.Comment__c = '333';
        	insert ac;
        }
        System.Test.startTest();
        Integer year = Date.today().year();
		Integer month = Date.today().month();
		BQ_BatchComputationBonus BonusBatch = new BQ_BatchComputationBonus();
		BonusBatch.year = year;
		BonusBatch.month = month;
		ID batchprocessid = Database.executeBatch(BonusBatch);
		System.Test.stopTest();
       
	}
}