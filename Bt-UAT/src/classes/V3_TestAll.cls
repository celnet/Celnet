/**
 * V3 trigger Test All
 */
@isTest
private class V3_TestAll {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        try{
            //create HKLeads
            HK_Leads__c HK_Leads = new HK_Leads__c();
            HK_Leads.Name = 'HKLeadsTestAandy';
            HK_Leads.Address_Num__c = 'testAndy2012';
            HK_Leads.Customer_Type__c ='CustomerType';
            HK_Leads.Country__c = 'HKB';
            HK_Leads.Address_1__c='Address1';
            HK_Leads.Address_2__c='Address2';
            HK_Leads.Fax__c = '010943034';
            HK_Leads.Phone_Number__c = '15800000001';
            HK_Leads.Price_Group__c  = 'PriceGroup';
            insert HK_Leads;
            
            HK_Leads__c HK_LeadsTest = new HK_Leads__c();
            HK_LeadsTest.Name = 'HKLeadsTestAandy';
            HK_LeadsTest.Address_Num__c = 'testAndy2012';
            HK_LeadsTest.Customer_Type__c ='CustomerType';
            HK_LeadsTest.Country__c = 'HKB';
            HK_LeadsTest.Address_1__c='Address1';
            HK_LeadsTest.Address_2__c='Address2';
            HK_LeadsTest.Fax__c = '010943034';
            HK_LeadsTest.Phone_Number__c = '15800000001';
            HK_LeadsTest.Price_Group__c  = 'PriceGroup';
            insert HK_LeadsTest;
            
            
             
            HK_Leads.Name = 'HKLeadsTestAandyUpdate';
            HK_Leads.Address_Num__c = 'testAndy2012Update';
            HK_Leads.Customer_Type__c ='CustomerTypeUpdate';
            HK_Leads.Country__c = 'HKBUpdate';
            HK_Leads.Address_1__c='Address1Update';
            HK_Leads.Address_2__c='Address2Update';
            HK_Leads.Fax__c = '010943034Update';
            HK_Leads.Phone_Number__c = '15800000001Update';
            HK_Leads.Price_Group__c  = 'PriceGroupUpdate';
            update HK_Leads;
            
 
        }catch(Exception e){
            
        }
   
    }
    
    //创建HK租
       static testMethod void CreateHKLeadsTeam() {
        // TO DO: implement unit test
        try{
        
            HK_Leads__c HK_Leads = new HK_Leads__c();
            HK_Leads.Name = 'HKLeadsTestAandy';
            HK_Leads.Address_Num__c = 'testAndy2012';
            HK_Leads.Customer_Type__c ='CustomerType';
            HK_Leads.Country__c = 'HKB';
            HK_Leads.Address_1__c='Address1';
            HK_Leads.Address_2__c='Address2';
            HK_Leads.Fax__c = '010943034';
            HK_Leads.Phone_Number__c = '15800000001';
            HK_Leads.Price_Group__c  = 'PriceGroup';
            insert HK_Leads;
            
        }catch(Exception e){
            
        }
   
    }
    
    //创建产品
     static testMethod void CreateProduct() {
        // TO DO: implement unit test
        try{
            
            Product2 Product = new Product2();
            Product.Name = 'ProductTestAndy';
            Product.V3_Item_Number__c = '943034';
            insert Product;

        }catch(Exception e){
            
        }
   
    }
    
    static testMethod void UpdateQuotationStatus(){
    	Opportunity opt = new Opportunity();
    	opt.name='test';
    	opt.StageName='test';
    	opt.CloseDate=Date.today();
    	insert opt;
    	
    	Quote qte = new Quote();
    	qte.Name='test';
    	qte.OpportunityId=opt.id;
    	insert qte;
    	
    	Task evt = new Task();
    	evt.WhatId=qte.Id;
    	evt.Subject='test';
    	insert evt;
    }
    
    //创建产品价钱
    static testMethod void CreateProductPrice() {
        // TO DO: implement unit test
        try{
            
            Account acc = new Account();
            acc.MID__c = '943034110';
            acc.Name ='Name';
            insert acc;
            
            Product2 Product = new Product2();
            Product.Name = 'ProductTestAndy';
            Product.V3_Item_Number__c = '943034110';
            insert Product;
            
            Product_Price__c ProductPrice = new Product_Price__c();
            ProductPrice.Name = '943034110';
            ProductPrice.Customer_No__c = '943034110';
            ProductPrice.Currency__c = 'HKD';
            ProductPrice.Price__c=1000;
            //ProductPrice.Eff_From__c = '2456197';
           // ProductPrice.Eff_To__c = '2456197';
            ProductPrice.UOM__c = 'CA';
            ProductPrice.Account__c = acc.Id;
            insert ProductPrice;
            
            
            Product_Price__c ProductPriceTest = new Product_Price__c();
            ProductPriceTest.Name = '943034110';
            ProductPriceTest.Currency__c = 'HKD';
            ProductPriceTest.Price__c=1000;
            //ProductPriceTest.Eff_From__c = '2456197';
           // ProductPriceTest.Eff_To__c = '2456197';
            ProductPriceTest.UOM__c = 'CA';
            ProductPriceTest.Account__c = acc.Id;
            insert ProductPriceTest;

        }catch(Exception e){
            
        }
   
    }
}