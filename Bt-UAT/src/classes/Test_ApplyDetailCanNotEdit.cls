/**
 * author：bill
 * ApplyDetailCanNotEdit的trigger的测试类
 */
@isTest
private class Test_ApplyDetailCanNotEdit {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        //挥发罐使用申请
        Vaporizer_Application__c vapApply = new Vaporizer_Application__c();
        vapApply.Approve_Result__c = '审批中';
        insert vapApply;
        Vaporizer_Application__c vapApply1 = new Vaporizer_Application__c();
        vapApply1.Approve_Result__c = '待审批';
        insert vapApply1;
        
        //申请明细
        //List<Vaporizer_Apply_Detail__c> list_vapApplyDetail = new List<Vaporizer_Apply_Detail__c>();
        Vaporizer_Apply_Detail__c vapApplyDetail = new Vaporizer_Apply_Detail__c();
        vapApplyDetail.Vaporizer_Application__c = vapApply.Id;
        Vaporizer_Apply_Detail__c vapApplyDetail1 = new Vaporizer_Apply_Detail__c();
        vapApplyDetail1.Vaporizer_Application__c = vapApply1.Id;
        insert vapApplyDetail1;
        
        vapApply1.Approve_Result__c = '审批中';
        update vapApply1;
        
        test.startTest();
        try{
        insert vapApplyDetail;
        }catch(Exception e){system.debug(e);}
        vapApplyDetail1.ApplyQty__c = 2;
        try{
        update vapApplyDetail1;
        }catch(Exception e){system.debug(e);}
        try{
        delete vapApplyDetail1;
        }catch(Exception e){system.debug(e);}
        test.stopTest();
        
    }
}