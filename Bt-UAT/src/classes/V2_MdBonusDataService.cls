/*
Author：Scott
Created on：2011-1-6
Description: 
1.得到Md部门用户奖金计算所涉及到的所有参数信息
2013-12-12 Comment By Tobe 事件字段删除：注释V2_2levelScore__c CommentsBySupervisor2__c字段引用
*/
public class V2_MdBonusDataService {
  //拜访完成数
  Double FinishedPercent = 0;  
  //相关事件
  List<Event> List_Event {get;set;}
  //业务机会数
  public Set<Id> Set_Oppids = new Set<Id>();
  //业务机会
  public Set<Opportunity> Set_Opps = new Set<Opportunity>();  
  //IVT业务机会数
  public Set<Id> Set_IVTOppids = new Set<Id>();
  //IVT业务机会
  public Set<Opportunity> Set_IVTOpps = new Set<Opportunity>();
  //拜访事件联系人Id
  Set<Id> Set_CallContactIds =  new Set<Id>();
  //一年所有拜访联系人Id
  Set<Id> Set_YearCallContactIds = new Set<Id>();
  //主管协防频率
  Double Assistance = 0;
  Double SP_Assistance = 0;
  //二级主管评分总分
  Double sumScore =0;  
  //Md部门非进行状态
  //Md：成功、失败、进药完成、增量完成、目标实现、基本量完成、产品使用、产品培训和使用,'关闭'
  Set<String> Set_Stage = new Set<String>{'成功','失败','进药完成','增量完成','目标实现','基本量完成','产品使用','签约/缔结(成功)','产品培训和使用','关闭','关闭 (失败，放弃)','缔结(成功)'};
  Set<String> Set_Approval_Status = new Set<String>{'新建','拒绝'};
  //得到用户奖金计算涉及到的参数信息
  public  V2_MdBonusDataService(Id userid,Integer year,Integer month)
  {
    //拜访完成率
    for(MonthlyPlan__c mp:[select Percent__c from MonthlyPlan__c 
                 where Year__c=:String.valueOf(year) and Month__c =:String.valueOf(month)
                 and OwnerId =: userid limit 1])
    {
      if(mp.Percent__c != null)
      {
        FinishedPercent = mp.Percent__c;
      }
    }
    //事件
    Date CurrentDate = Date.newInstance(year,month,1);
    DateTime CurrentStartMonth = DateTime.newInstance(CurrentDate.toStartOfMonth(),Time.newInstance(00, 00, 00, 00));
        DateTime CurrentEndMonth = DateTime.newInstance(CurrentDate.toStartOfMonth().addMonths(1),Time.newInstance(00, 00, 00, 00));
    List_Event = [select V2_Score__c,whoId,WhatId,Done__c,SubjectType__c,RecordType.DeveloperName,V2_IsExpire__c,Key_Information__c from Event where 
                  IsRecurrence != true and StartDateTime>=:CurrentStartMonth
                  and StartDateTime <:CurrentEndMonth and OwnerId =: userid ];
    //KA
    if(List_Event != null && List_Event.size()>0)
    {
      for(Event ev:List_Event)
      {
        if(ev.RecordType.DeveloperName != 'V2_Event')
        {
            continue;
        }
        if(ev.SubjectType__c == '拜访')
        {
          Set_CallContactIds.add(ev.WhoId);
        }
      }
    }
    //进行中业务机会
    for(Opportunity opp:[select Id,Strategy_Rating__c,RecordType.DeveloperName from Opportunity where StageName not in: Set_Stage and OwnerId =:userid])
    {
      Set_Oppids.add(opp.Id);
      Set_Opps.add(opp);
    }
    //进行中IVT业务机会
    for(Opportunity opp:[select Id,Strategy_Rating__c,RecordType.DeveloperName from Opportunity where StageName not in: Set_Stage and OwnerId =:userid and ApproveStatus__c not in: Set_Approval_Status and(RecordType.DeveloperName='IVT' OR RecordType.DeveloperName='IVT_Approval')])
    {
      Set_IVTOppids.add(opp.Id);
      Set_IVTOpps.add(opp);
    }
    //年所有 事件 
    Datetime StartYear = Datetime.newInstance(year, 1, 1,0,0,0);
     Datetime EndYear =StartYear.addYears(1);
    for(Event ev:[select WhoId,SubjectType__c from Event Where OwnerId =: userid 
            and IsRecurrence != true and whoId != null and StartDateTime>=:StartYear and Done__c = true and V2_IsExpire__c =false
                  and StartDateTime <:EndYear and RecordType.DeveloperName = 'V2_Event'] )
    {
      if(ev.SubjectType__c =='拜访')
      {
        Set_YearCallContactIds.add(ev.WhoId);
      }
    } 
    //主管协防
    /*for(EventAttendee  ea:[Select Event.V2_2levelScore__c From EventAttendee where AttendeeId =:userid and Event.StartDateTime >=:CurrentStartMonth
                 and Event.StartDateTime <:CurrentEndMonth and EventId != null and Event.V2_IsExpire__c =false and Event.IsRecurrence != true and Event.Done__c=true
                 and Event.SubjectType__c=:'拜访' and (Status='Accepted'  or Event.CommentsBySupervisor__c != null or Event.CommentsBySupervisor2__c != null)])
    {
      
      //当月主管协访的已完成的拜访事件数量
      Assistance++;
      //二级主管评分 平均分
      if(ea.Event.V2_2levelScore__c != null)
      {
      	SP_Assistance++;
        sumScore +=Double.valueOf(ea.Event.V2_2levelScore__c);
      }
    }*/
  }
  
  
  /*
    代表
  */
  //拜访KA频率:KA定义
  //1联系人职务为院长，副院长（2012-4修改增加职务为护理部主任）
  //2部门为药剂科，职务为主任，副主任
  //3部门为PIVAS，职务为主任
  //4部门为护理部，职务为主任，副主任
  /*
    Cognizant fix code 2012-6-28 
    1.行政或者院办部门的职务为院长或者副院长
    2.药剂科的职务为主任或者副主任
    3.PIVA的主任
    4.护理部门的职务为主任，副主任，或者护理部主任
    5.院办部门的书记，或者主任
    6.医务科的主任
  */
  public Double getKaFrequency()
  {
    Double KaFrequency = 0;
    for(Contact con:[select Id,Title,DepartmentType__c from Contact where id in:Set_CallContactIds])
    {
    //Cognizant fix code 2012-6-28 Start
    /*
      if( con.Title == '院长' || con.Title =='副院长' || con.Title =='护理部主任'
          || (con.DepartmentType__c =='药剂科' && (con.Title=='主任' || con.Title=='副主任'))
          || (con.DepartmentType__c =='PIVA' && con.Title=='主任')
          || (con.DepartmentType__c =='护理部' && (con.Title=='主任' || con.Title=='副主任'))
        )
     */
      if(    (con.DepartmentType__c =='行政'|| con.DepartmentType__c =='院办')&& (con.Title == '院长' || con.Title =='副院长') 
          || (con.DepartmentType__c =='药剂科' && (con.Title=='主任' || con.Title=='副主任'))
          || (con.DepartmentType__c =='PIVA' && con.Title=='主任')
          || (con.DepartmentType__c =='护理部' && (con.Title=='主任' || con.Title=='副主任'|| con.Title=='护理部主任'))
          || (con.DepartmentType__c =='院办' && (con.Title=='主任' || con.Title=='书记'))
          || (con.DepartmentType__c =='医务科' && con.Title=='主任')
        )
    //Cognizant fix code 2012-6-28 END
      {
        for(Event ev:List_Event)
        {
          if(ev.WhoId != con.Id)
          {
            continue;
          }
          if(ev.Done__c && !ev.V2_IsExpire__c)
          {
            KaFrequency++;
          }
        }
      }
    }
    return KaFrequency;
  }
  //拜访完成率
  //当月完成的拜访且未过期/计划拜访数
  public Double getCallRecordFinishedPercent()
  {
    FinishedPercent = FinishedPercent/100;
    return FinishedPercent;
  }
  //业务机会相关拜访：除了关闭阶段的，其他阶段的拜访都计入相关拜访
  //每个业务机会都有4个或以上事件，计10分。否则0分。
  public Map<Id,Double> getOppCallEvent(String BonusType) 
  {
    //业务机会及其对应的相关拜访事件数
    Map<Id,Double> Map_OppEventPercent = new Map<Id,Double>();
    if(BonusType != 'IVT'){
	    for(Id oppid : Set_Oppids)
	    {
	        Map_OppEventPercent.put(oppid,0);
	        for(Event ev:List_Event)
	        {
	            if(ev.RecordType.DeveloperName != 'V2_Event')
	            {
	                continue;
	            }
	            if(ev.WhatId != oppid)
	            {
	                continue;
	            }
	            if(ev.SubjectType__c =='拜访' && ev.Done__c && !ev.V2_IsExpire__c)
	            {
	                if(Map_OppEventPercent.containsKey(ev.WhatId))
	                {
	                    Double num = Map_OppEventPercent.get(ev.WhatId);
	                    Map_OppEventPercent.put(ev.WhatId,num+1);
	                }
	            }
	        }
	    }    	
    } else {
    	for(Id oppid : Set_IVTOppids)
	    {
	        Map_OppEventPercent.put(oppid,0);
	        for(Event ev:List_Event)
	        {
	            if(ev.RecordType.DeveloperName != 'V2_Event')
	            {
	                continue;
	            }
	            if(ev.WhatId != oppid)
	            {
	                continue;
	            }
	            if(ev.SubjectType__c =='拜访' && ev.Done__c && !ev.V2_IsExpire__c)
	            {
	                if(Map_OppEventPercent.containsKey(ev.WhatId))
	                {
	                    Double num = Map_OppEventPercent.get(ev.WhatId);
	                    Map_OppEventPercent.put(ev.WhatId,num+1);
	                }
	            }
	        }
	    }  
    } 

    return Map_OppEventPercent;
  }
  //业务机会策略评分
  public Double GetStrategyRating(String BonusType)
  { double Strategy_Rating = 0;
    double OppCount = 0;
    double totalRate = 0;
    if(BonusType != 'IVT'){
	    if(Set_Opps != null && Set_Opps.size() != 0){
	        for(Opportunity opp:Set_Opps){
	            if(opp.Strategy_Rating__c != null && opp.Strategy_Rating__c != '' && opp.Strategy_Rating__c != 'NONE'){
	                OppCount++;
	                totalRate += double.valueof(opp.Strategy_Rating__c);            
	            } else {
	                continue;
	            }
	        }
	    }    	
    } else {
    		if(Set_IVTOpps != null && Set_IVTOpps.size() != 0){
	        for(Opportunity opp:Set_IVTOpps){
	            if(opp.Strategy_Rating__c != null && opp.Strategy_Rating__c != '' && opp.Strategy_Rating__c != 'NONE'){
	                OppCount++;
	                totalRate += double.valueof(opp.Strategy_Rating__c);            
	            } else {
	                continue;
	            }
	        }
	    } 
    }

    if(OppCount != 0) {
        Strategy_Rating = totalRate/OppCount;
    } else {
        Strategy_Rating = 0;
    }
    return Strategy_Rating;
  }
  
  
  //SP拜访数量考核:已完成拜访Event的数量
  public Double getFinishedCallFrequency()
  {
    Double FinishedCall = 0;
    if(List_Event !=null && List_Event.size()>0)
    {
      for(Event ev:List_Event)
      {
        if(ev.RecordType.DeveloperName != 'V2_Event')
        {
            continue;
        }
        if(ev.SubjectType__c=='拜访' && ev.Done__c && !ev.V2_IsExpire__c)
        {
          FinishedCall++;
        }
      }
    }
    return FinishedCall;
  }
  //拜访质量评分:主管打分的平均分
  public Double getCallQuality()
  {
    Double CallQuality = 0;
    Double sum = 0;
    Double num = 0;
    if(List_Event != null && List_Event.size()>0)
    {
      for(Event ev:List_Event)
      {
        if(ev.V2_Score__c != null)
        {
          sum+=Double.valueOf(ev.V2_Score__c);
          num++;
        }
      }
    }
    if(num >0)
    {
      CallQuality = sum/num;
    }
    return CallQuality;
  }

  //关键信息传递率
  public Double getKeyInfoRate()
  {
    Double keyInfoRate = 0;
    Double keyInfoCount = 0;
    Double totalDoneEvent = 0;
    if(List_Event != null && List_Event.size()>0)
    {
      for(Event ev:List_Event)
      {
        if(ev.Done__c && !ev.V2_IsExpire__c && ev.RecordType.DeveloperName == 'V2_Event')
        {
          totalDoneEvent++; 
          if(ev.Key_Information__c != null && ev.Key_Information__c != ''&& ev.Key_Information__c !='NONE'){
            keyInfoCount++;
          }         
        } else {
            continue;
        }
      }
      if(totalDoneEvent != 0) {
        keyInfoRate = keyInfoCount/totalDoneEvent;
      } else {
        keyInfoRate = 0;
      }     
    }
    return keyInfoRate;
  }
  
  //SP科室会:完成的行动类型为“科室会”的事件
  public Double getDepartmentVisit()
  {
    //默认返回false 没有科室会
    Double DepartmentVisit = 0;
    if(List_Event != null && List_Event.size()>0)
    {
      for(Event ev:List_Event)
      {
        if(ev.SubjectType__c=='科室会' && ev.Done__c)
        {  
          DepartmentVisit++;
        }
      }
    }
    return DepartmentVisit;
  }
  //SP潜力分析 ：医院信息完整度  Sum（今年拜访完成的联系人所在医院的ACC医院填写的字段）/ACC医院的所有字段数量* owner为销售的ACC医院数量 (年)
  public Double getCCHospitalInfoCompletePercent()
  {
    Double Percent = 0;
    
    Double numerator = 0;
    Double denominator = 0;
    
    //客户Ids
    Set<Id> accids = new Set<Id>();
    
    if(Set_YearCallContactIds!=null && Set_YearCallContactIds.size()>0)
    {
      for(Contact co:[select AccountId from Contact where id in: Set_YearCallContactIds])
      {
        accids.add(co.AccountId);
      }
    }
    
    for(CCHospitalInfo__c cchi:[select Year__c,ACCTargetHos__c,ACCKA__c,V2_FieldSum1__c,V2_FieldSum2__c,
                  V2_FieldSum3__c,V2_FieldSum4__c,V2_FieldSum5__c,V2_FieldQuantity__c from CCHospitalInfo__c 
                  where Account__c in: accids])
    {
      if(cchi.Year__c != null)
      {
        numerator++;
      }
      if(cchi.ACCTargetHos__c != null)
      {
        numerator++;
      }
      if(cchi.ACCKA__c != null)
      {
        numerator++;
      }
      numerator+=cchi.V2_FieldSum1__c + cchi.V2_FieldSum2__c + cchi.V2_FieldSum3__c + cchi.V2_FieldSum4__c + cchi.V2_FieldSum5__c;
      denominator +=  cchi.V2_FieldQuantity__c;
    } 
    if(denominator != 0)
    {
      Percent = numerator/denominator;
    }
    return Percent;
  }
  //SP目标客户设定，联系人市场细分的完整程度:联系人中填写了市场细分类型的数量/所有拜访过的联系人 (完成未过期)（年）
   public Double getTargetContactPercet()
   {
     Double TargetContactPercet = 0;
       //联系人中填写了市场细分类型的数量
       Double numerator  = 0;
       //所有拜访过的联系人
       Double denominator = 0;
       
       for(Contact con:[select V2_SPCampaignType__c from Contact where id in:Set_YearCallContactIds ])
       {
         denominator ++;
         if(con.V2_SPCampaignType__c != null)
         {
           numerator++;
         }
       }
       
       if(denominator>0)
       {
         TargetContactPercet = numerator/denominator;
       }
     return TargetContactPercet;
   }
  
  //SP业务机会：年度业务机会考核 ，结束日期是今年，Owner是本人，阶段在'成功'
  //
  public Double getOppAnnualAssessment(Id userid,Integer year)
  {
    Double OppAnnualAssessment = 0;
    Date startyear = Date.newInstance(year,1,1);
    Date endyear = startyear.addYears(1);
    //AP-2012-0159:S.4
    /**
    *Requirement ID:AP-2012-0159
    *Description: The condition for getting the number of the opportunities changed on 2012/9/24 when doing the UAT.
    *Done: Ken, on 2012-09-24
    */
    for(Opportunity opp:[Select Id from Opportunity where CloseDate >=:startyear and CloseDate <: endyear 
    	and OwnerId =: userid])
    //AP-2012-0159:S.4
    /*Commented by Ken
    for(Opportunity opp:[Select Id from Opportunity where CloseDate >=:startyear and CloseDate <: endyear and OwnerId =: userid and StageName='成功'])
    */
    {
      OppAnnualAssessment++;
    }
    return OppAnnualAssessment;
  } 
  
  //SP市场活动:市场活动邀请,今年报名的联系人参加人数/今年报名的联系人数量
  public Double getCampaignMemberParticipatedPercet(Id userid,Integer year)
  {
    Double ParticipatedPercet = 0;
    
    Datetime StartYear = Datetime.newInstance(year, 1, 1,0,0,0);
     Datetime EndYear =StartYear.addYears(1);
     //报名参加人数
     Double numerator  = 0;
     //今年报名总数
     Double denominator = 0;
    for(CampaignMember cm:[select V2_Participated__c from CampaignMember where User__c=:userid 
                 and CreatedDate >=:StartYear and CreatedDate <: EndYear])
    {
      denominator++;
      if(cm.V2_Participated__c)
      {
        numerator++;
      }
    }
    if(denominator>0)
    {
      ParticipatedPercet = numerator/denominator;
    }
    return ParticipatedPercet;
  }
  

 //SP市场活动：市场活动跟进拜访 完成的回访数量/回访数量
 public Double getCampaignFollowCallFinishedPercet(Id userid,Integer year)
  {
    Double FinishedPercet = 0;
    
    Datetime StartYear = Datetime.newInstance(year, 1, 1,0,0,0);
    Datetime EndYear =StartYear.addYears(1);
    //报名参加人数
    Double numerator  = 0;
    //今年报名总数
    Double denominator = 0;
    for(Event ev:[select V2_IsExpire__c,Done__c from Event Where OwnerId =: userid 
            and IsRecurrence != true and whoId != null and StartDateTime>=:StartYear
                  and StartDateTime <:EndYear and V2_FollowEventFlag__c= true and RecordType.DeveloperName = 'V2_Event'])
    {
      denominator++;
      if(ev.Done__c && ev.V2_IsExpire__c==false)
      {
        numerator++;
      }
    }
    if(denominator>0)
    {
      FinishedPercet = numerator/denominator;
    }
    return FinishedPercet;
  }
 //cognizant Add Start 2012-07-05
 //市场活动跟进 月度扣分项
 public Integer getMonthlyCampaignUnFollowEventsCount(Id userid,Integer year,Integer month) {
    List <Event> totalFollowEventList = new List <Event>();
    Date StartYearMonth = Date.newInstance(year, month, 1);
    Date EndYearMonth = Date.newInstance(year, month+1, 1);
    Integer unFollowEventCount = 0;
    
    totalFollowEventList = [select V2_IsExpire__c,Done__c,V3_Campaign_FeedBack_End_Day__c,EndDateTime,WhatId
                                from Event 
                                Where OwnerId =: userid 
                                    and IsRecurrence != true and whoId != null and V3_Campaign_FeedBack_End_Day__c>=:StartYearMonth
                                    and V3_Campaign_FeedBack_End_Day__c <:EndYearMonth and V2_FollowEventFlag__c= true 
                                    and RecordType.DeveloperName = 'V2_Event'
                            ];
    if(totalFollowEventList != null && totalFollowEventList.size() != 0){
    for(Event ev:totalFollowEventList) {
        Date campEndDt = ev.V3_Campaign_FeedBack_End_Day__c;
        DateTime evtEndDt = ev.EndDateTime;
        String whatId = ev.WhatId;        
        //如sales的回访记录状态为未完成，或已过期，或拜访事件的相关项没有link这个市场活动或EndDay 在市场活动回访日期之后，任何一种情况发生都扣分
        if(!ev.Done__c || ev.V2_IsExpire__c || !whatId.subString(0,3).equals('701') || campEndDt.dayOfYear() < evtEndDt.dayOfYear()) {
            
            unFollowEventCount++;
        }
    }
   } 
   return unFollowEventCount;
    
 }
 //cognizant Add END 2012-07-05 
 
  //当前销售1月至现在月份平均分
  public Double getMonthScoreAvg(Id userid,Integer year,Integer month)
  {
    Double AverageScore = 0;
    Double SumScore = 0;
    Double num = 0;
    Set<String> Set_Months = new Set<String>();
    for(Integer mon = 1;mon<=month;mon++)
    {
        Set_Months.add(String.valueOf(mon));
    }
    for(V2_UserBonusInFo__c vubif:[select V2_BonusScore__c from V2_UserBonusInFo__c 
                                   where V2_Year__c =: String.valueOf(year)
                                   and V2_Month__c in:Set_Months
                                   and V2_BonusScore__c != null
                                   and OwnerId =: userid])
    {
        num++;
        SumScore+=vubif.V2_BonusScore__c;
    }
    if(num>0)
    {
      AverageScore = SumScore/num;
    }
    return AverageScore;
  }
  /*
    主管
  */
  //得到用户下属业绩平均值
  public Double getRepAverageScore(Set<Id> repids,Integer year,Integer month)
  {
    Double AverageScore = 0;
    Double sum = 0;
    Double reps = 0;
    for(V2_UserBonusInFo__c vubif:[select V2_BonusScore__c from V2_UserBonusInFo__c 
                   where V2_Year__c =: String.valueOf(year)
                   and V2_Month__c =:String.valueOf(month)
                   and V2_BonusScore__c != null
                   and OwnerId in: repids])
    {
      reps++;
      sum+=vubif.V2_BonusScore__c;
    }
    
    if(reps>0)
    {
      AverageScore = sum/reps;
    }
    return AverageScore;
  }
  
  //主管协访频率:当月主管协访的已完成的拜访事件数量。（主管是被邀请人，且该拜访已完成）
  public Double getAssistanceFrequency()
  {
    return Assistance;
  }
  
  //AP-2012-0159:S.0 
  //协防认可及质量:二级主管评语的平均分（1到5）,(有评分,如果没有评分则不计入)
  public Double getSP_AverageScore(){
  	Double rst=SP_Assistance>0?(1.0000*sumScore / SP_Assistance) : 0;
  	System.debug('Result===============:SP_Assistance Number:'+SP_Assistance+', Average Score:'+rst);
    return rst;
  }
  //AP-2012-0159:S.0
  
  //协防认可及质量:二级主管评语的平均分（1到5）
  public Double getAverageScore()
  {
    Double AverageScore = 0;
    if(Assistance>0)
    {
      AverageScore = sumScore / Assistance;
    }
    return AverageScore;
  }
  /***************************************************测试类***************************************************/
   static testMethod void V2_MdBonusDataService() {
       /**************************Create User*************************/
       /*用户角色*/
       //经理
       UserRole SupervisorUserRole = new UserRole() ;
       SupervisorUserRole.Name = 'Renal-Supervisor-Shanghai-PD-Supervisor';
       //SupervisorUserRole.ParentRoleId = RegionalUserRole.Id ;
       insert SupervisorUserRole ;
       //销售
       UserRole RepUserRole = new UserRole() ;
       RepUserRole.Name = 'Renal-Rep-Shanghai-HD-Rep';
       RepUserRole.ParentRoleId = SupervisorUserRole.Id ;
       insert RepUserRole ;
       
    /*用户简档*/
    //rep简档
      Profile RepProRenal = [select Id from Profile where Name  = 'Standard User - Renal Sales Rep' limit 1];
      //Supr简档renal
      Profile SupProRenal = [select Id from Profile where Name='Standard User - Renal Sales Supervisor' limit 1];
    
    /************User************/
    List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
    /*销售*/
       User RepSu = new User();
       RepSu.Username='RepSu@123.com';
       RepSu.LastName='RepSu';
       RepSu.Email='RepSu@123.com';
       RepSu.Alias=user[0].Alias;
       RepSu.TimeZoneSidKey=user[0].TimeZoneSidKey;
       RepSu.ProfileId=RepProRenal.Id;
       RepSu.LocaleSidKey=user[0].LocaleSidKey;
       RepSu.LanguageLocaleKey=user[0].LanguageLocaleKey;
       RepSu.EmailEncodingKey=user[0].EmailEncodingKey;
       RepSu.CommunityNickname='RepSu';
       RepSu.MobilePhone='12345678912';
       RepSu.UserRoleId = RepUserRole.Id ;
       RepSu.IsActive = true;
       insert RepSu;
       
       /********主管********/
       User RepSs = new User();
       RepSs.Username='RepSs@123.com';
       RepSs.LastName='RepSs';
       RepSs.Email='RepSs@123.com';
       RepSs.Alias=user[0].Alias;
       RepSs.TimeZoneSidKey=user[0].TimeZoneSidKey;
       RepSs.ProfileId=SupProRenal.Id;
       RepSs.LocaleSidKey=user[0].LocaleSidKey;
       RepSs.LanguageLocaleKey=user[0].LanguageLocaleKey;
       RepSs.EmailEncodingKey=user[0].EmailEncodingKey;
       RepSs.CommunityNickname='RepSs';
       RepSs.MobilePhone='123456789112';
       RepSs.UserRoleId = SupervisorUserRole.Id ;
       RepSs.IsActive = true;
       insert RepSs;
       
       
        /*客户*/
    RecordType accrecordtype = [select Id from RecordType where Name='医院' and SobjectType='Account' limit 1];
    Account acc = new Account();
    acc.RecordTypeId = accrecordtype.Id;
    acc.Name = 'AccTest';
    insert acc;
    /*客户小组*/
    AccountTeamMember atm = new AccountTeamMember();
    atm.AccountId = acc.Id;
    atm.UserId = RepSu.Id;
    insert atm;
    /*Acc医院信息*/
    //CCHospitalInfo__c cch = new CCHospitalInfo__c();
    //cch.Account__c = acc.Id;
    //cch.
    /*联系人*/
    RecordType conrecordtype = [select Id from RecordType where Name = 'MD' and SobjectType='Contact' and IsActive = true limit 1 ];
    Contact con1 = new Contact();
    con1.LastName = 'AccTestContact1';
    con1.AccountId=acc.Id;
    insert con1;
    
    Contact con2 = new Contact();
    con2.LastName = 'AccTestContact2';
    con2.AccountId=acc.Id;
    con2.Title = '院长';
    insert con2;
    //Ka
    Contact con3 = new Contact();
    con3.LastName = 'AccTestContact2';
    con3.AccountId=acc.Id;
    con3.Title = '院长';
    insert con3;
    /*业务机会*/
    Opportunity opp = new Opportunity();
    opp.Name = 'OppTest';
    opp.AccountId = acc.Id; 
    opp.StageName = '发现/验证机会';
    opp.Type = '其他';
    opp.CloseDate = date.today().addmonths(1);
    opp.OwnerId = RepSu.Id;
    opp.Strategy_Rating__c = '1';
    insert opp;
    
    Opportunity opp1 = new Opportunity();
    opp1.Name = 'OppTest1';
    opp1.AccountId = acc.Id;
    opp1.StageName = '发现/验证机会';
    opp1.Type = '其他';
    opp1.CloseDate = date.today().addmonths(1);
    opp.OwnerId = RepSu.Id;
    insert opp1;
    /*市场活动*/
    Campaign cam = new Campaign();
    cam.Name = 'CamTest';
    cam.StartDate = date.today().addMonths(1);
    cam.EndDate = date.today().addMonths(2);
    cam.IsActive = true;
    insert cam;
    /*市场活动成员*/
    CampaignMember cm = new CampaignMember();
    cm.CampaignId = cam.Id;
    cm.ContactId = con2.Id;
    cm.User__c = RepSu.Id;
    insert cm;
    /*拜访事件*/
    RecordType callRt = [Select Id from RecordType where DeveloperName = 'V2_Event' Limit 1];
    
    Event CallEv = new Event();
    CallEv.RecordTypeId = callRt.Id;
    CallEv.WhoId = con1.Id;
    CallEv.StartDateTime = datetime.now();
    CallEv.EndDateTime = datetime.now().addMinutes(1);
    CallEv.SubjectType__c = '拜访';
    CallEv.OwnerId = RepSu.Id;
    CallEv.Done__c = true;
    /*CallEv.V2_2levelScore__c =String.valueOf(3);*/
    insert CallEv;
    
    Event CallEv2 = new Event();
    CallEv2.RecordTypeId = callRt.Id;
    CallEv2.WhoId = con2.Id;
    CallEv2.StartDateTime = datetime.now();
    CallEv2.EndDateTime = datetime.now().addMinutes(1);
    CallEv2.WhatId = opp1.Id;
    CallEv2.SubjectType__c = '拜访';
    CallEv2.OwnerId = RepSu.Id;
    CallEv2.Done__c = true;
    insert CallEv2;
    
    Event CallEv3 = new Event();
    CallEv3.RecordTypeId = callRt.Id;
    CallEv3.WhoId = con1.Id;
    CallEv3.StartDateTime = datetime.now();
    CallEv3.EndDateTime = datetime.now().addMinutes(1);
    CallEv3.WhatId = cam.Id;
    CallEv3.V2_FollowEventFlag__c = true;
    CallEv3.SubjectType__c = '拜访';
    CallEv3.OwnerId = RepSu.Id;
    insert CallEv3;
    
    Event CallEv4 = new Event();
    CallEv4.RecordTypeId = callRt.Id;
    CallEv4.WhoId = con3.Id;
    CallEv4.StartDateTime = datetime.now();
    CallEv4.EndDateTime = datetime.now().addMinutes(1);
    CallEv4.WhatId = cam.Id;
    CallEv4.V2_FollowEventFlag__c = true;
    CallEv4.SubjectType__c = '科室会';
    CallEv4.OwnerId = RepSu.Id;
    insert CallEv4;
    
    /*月计划*/
    MonthlyPlan__c mp = new MonthlyPlan__c();
    mp.Year__c = String.valueOf(Date.today().Year());
    mp.Month__c = String.valueOf(Date.today().Month());
    mp.ownerId = RepSu.Id;
    insert mp;
    /*月计划明细*/
    MonthlyPlanDetail__c mpd1 = new MonthlyPlanDetail__c();
    mpd1.Contact__c = con1.Id;
    mpd1.Account__c = acc.Id;
    mpd1.MonthlyPlan__c = mp.Id;
    mpd1.ArrangedTimes__c = 5;
    mpd1.AdjustedTimes__c = 6;
    mpd1.Planned_Finished_Calls__c = 5;
    insert mpd1;
    
    /*下属分数*/
    RecordType rt = [select Id from RecordType where SobjectType ='V2_UserBonusInFo__c' and DeveloperName ='V2_MdRep' limit 1];
    V2_UserBonusInFo__c ubif = new V2_UserBonusInFo__c();
    ubif.RecordTypeId = rt.Id;
    ubif.V2_Year__c = String.valueOf(Date.today().Year());
    ubif.V2_Month__c = String.valueOf(Date.today().Month());
    ubif.V2_BonusScore__c = 90;
    ubif.OwnerId = RepSu.Id;
    insert ubif;
    
    
    Test.startTest();
      V2_MdBonusDataService mds = new V2_MdBonusDataService(RepSu.Id,Date.today().Year(),Date.today().month());
      //KA拜访
      System.debug('@@@@@@@@Ka拜访：'+mds.getKaFrequency());
      //拜访完成率
      System.debug('########拜访完成率：'+mds.getCallRecordFinishedPercent());
      //业务机会相关拜访
      System.debug('@@@@@@@@业务机会相关拜访：'+mds.getOppCallEvent('IVT'));
      //业务机会相关拜访
      System.debug('@@@@@@@@业务机会相关拜访：'+mds.getOppCallEvent('SP'));
      //SP拜访数量考核
      System.debug('########SP拜访数量考核: '+mds.getFinishedCallFrequency());
      //主管打分
      System.debug('@@@@@@@@主管打分平均分：'+mds.getCallQuality());
      //sp科室会
      System.debug('########科室会：'+mds.getDepartmentVisit());
      //sp潜力分析
      System.debug('@@@@@@@@sp潜力分析：'+mds.getCCHospitalInfoCompletePercent());
      //目标客户设定
      System.debug('########目标客户设定：'+mds.getTargetContactPercet());
      //sp业务机会
      System.debug('@@@@@@@@SP业务机会： '+ mds.getOppAnnualAssessment(RepSu.Id,date.today().year()));
      //sp 市场活动
      System.debug('########市场活动：'+ mds.getCampaignMemberParticipatedPercet(RepSu.Id,date.today().year()));
      // 市场活动回访
      System.debug('@@@@@@@@@市场活动回访完成率： '+mds.getCampaignFollowCallFinishedPercet(RepSu.Id,date.today().year()));
      
      System.debug('#########关键信息传递:'+mds.getKeyInfoRate());
      
      System.debug('#########业务机会策略评分:'+mds.getStrategyRating('IVT'));
      
       System.debug('#########业务机会策略评分:'+mds.getStrategyRating('SP'));     
      
      System.debug('#########市场活动回访扣分项：'+mds.getMonthlyCampaignUnFollowEventsCount(RepSu.Id, Date.today().Year(), Date.today().month()));
      
        Set<Id> repids = new Set<Id>();
        repids.add(RepSu.Id);
        V2_MdBonusDataService mds1 = new V2_MdBonusDataService(RepSs.Id,Date.today().Year(),Date.today().month());
        mds1.getRepAverageScore(repids,Date.today().Year(),Date.today().month());
        mds1.getMonthScoreAvg(RepSs.Id,Date.today().Year(),Date.today().month());
        mds1.getAssistanceFrequency();
        mds1.getAverageScore();
        mds1.getSP_AverageScore();
    Test.stopTest();
    }
}