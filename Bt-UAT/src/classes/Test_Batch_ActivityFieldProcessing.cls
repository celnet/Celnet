/**
 * ActivityFieldProcessingBatch的测试类
 */
@isTest
private class Test_Batch_ActivityFieldProcessing {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        // TO DO: implement unit test
        List<UserRole> listur= new List<UserRole>();
        UserRole ur0 = new UserRole();
        ur0.Name = 'Renal-Supervisor-华南-BIOS-Supervisor(U0)';
        listur.add(ur0);
        UserRole ur1 = new UserRole();
        ur1.Name = 'Renal-Supervisor-华南-BIOS-Supervisor(U1)';
        listur.add(ur1);
        UserRole ur2 = new UserRole();
        ur2.Name = 'Renal-Rep-华南-BIOS-Rep(U2)';
        listur.add(ur2);
        insert listur;
        ur2.ParentRoleId=ur1.Id;
        update ur2;
        ur1.ParentRoleId = ur0.id;
        update ur1;
        //user 
        list<User> user=[Select TimeZoneSidKey,ProfileId,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname, Alias From User Where IsActive = true limit 1];
        List<User> list_user = new List<User>();
        User user0=new User();
        user0.username='user0r2@023.com';
        user0.LastName='user0r2';
        user0.Email='user0r2@023.com';
        user0.Alias=user[0].Alias;
        user0.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user0.ProfileId=UserInfo.getProfileId();
        user0.LocaleSidKey=user[0].LocaleSidKey;
        user0.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user0.EmailEncodingKey=user[0].EmailEncodingKey;
        user0.CommunityNickname='chequ23';
        user0.MobilePhone='02345678902';
        user0.IsActive = true;
        user0.UserRoleId=ur0.Id ;
        user0.Alias='zhangsan';
        list_user.add(user0);
        User user1=new User();
        user1.username='user1r2@123.com';
        user1.LastName='user1r2';
        user1.Email='user1r2@123.com';
        user1.Alias=user[0].Alias;
        user1.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user1.ProfileId=UserInfo.getProfileId();
        user1.LocaleSidKey=user[0].LocaleSidKey;
        user1.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user1.EmailEncodingKey=user[0].EmailEncodingKey;
        user1.CommunityNickname='chequ';
        user1.MobilePhone='12345678912';
        user1.Alias='aaaa';
        user1.IsActive = true;
        user1.UserRoleId=ur1.Id ;
        list_user.add(user1);
        User user2=new User();
        user2.username='user2r2@223.com';
        user2.LastName='user2r2';
        user2.Email='user2r2@223.com';
        user2.Alias=user[0].Alias;
        user2.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user2.ProfileId=UserInfo.getProfileId();
        user2.LocaleSidKey=user[0].LocaleSidKey;
        user2.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user2.EmailEncodingKey=user[0].EmailEncodingKey;
        user2.CommunityNickname='chequ1';
        user2.MobilePhone='22345678922';
        user2.IsActive = true;
        user2.UserRoleId=ur1.Id ;
        user2.Alias ='bbb';
        list_user.add(user2);
        insert list_user;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'billTest9-3';
        opp.StageName = '需求分析';
        opp.CloseDate = date.today();
        insert opp;
        
        List<Event> list_events = new List<Event>();
        Event event = new Event();
        event.RecordTypeId = [Select r.Name, r.Id From RecordType r where r.Name = '拜访'][0].Id;
        event.DurationInMinutes = 20000;
        event.ActivityDateTime = Datetime.now();
        event.OwnerId = user1.Id;
        event.BeReviewed1__c = user1.Alias;
        event.ReUser1__c = user0.Alias;
        event.Grade1__c = '5';
        event.Comment1__c = 'test1';
        event.WhatId = opp.Id;
        list_events.add(event);
        Event event1 = new Event();
        event1.RecordTypeId = [Select r.Name, r.Id From RecordType r where r.Name = '拜访'][0].Id;
        event1.DurationInMinutes = 20000;
        event1.ActivityDateTime = Datetime.now();
        event1.OwnerId = user1.Id;
        event1.ReUser1__c = user2.Alias;
        event1.BeReviewed2__c = user1.Alias;
        event1.ReUser2__c = user0.Alias;
        event1.Grade2__c = '5';
        event1.Comment2__c = 'test2';
        event1.WhatId = opp.Id;
        list_events.add(event1);
        Event event2 = new Event();
        event2.RecordTypeId = [Select r.Name, r.Id From RecordType r where r.Name = '拜访'][0].Id;
        event2.DurationInMinutes = 20000;
        event2.ActivityDateTime = Datetime.now();
        event2.OwnerId = user1.Id;
        event2.ReUser1__c = user2.Alias;
        event2.BeReviewed3__c = user1.Alias;
        event2.ReUser3__c = user0.Alias;
        event2.Grade3__c = '5';
        event2.Comment3__c = 'test2';
        event2.WhatId = opp.Id;
        list_events.add(event2);
       /* Event event3 = new Event();
        event.RecordTypeId = [Select r.Name, r.Id From RecordType r where r.Name = '拜访'][0].Id;
        event3.DurationInMinutes = 20000;
        event3.ActivityDateTime = Datetime.now();
        event3.OwnerId = user1.Id;
        event3.ReUser1__c = user2.Alias;
        event3.BeReviewed4__c = user1.Alias;
        event3.ReUser4__c = user0.Alias;
        event3.Grade4__c = '5';
        event3.Comment4__c = 'test4';
        event3.WhatId = opp.Id;
        list_events.add(event3);
        Event event4 = new Event();
        event4.RecordTypeId = [Select r.Name, r.Id From RecordType r where r.Name = '拜访'][0].Id;
        event4.DurationInMinutes = 20000;
        event4.ActivityDateTime = Datetime.now();
        event4.OwnerId = user1.Id;
        event4.ReUser1__c = user2.Alias;
        event4.BeReviewed5__c = user1.Alias;
        event4.ReUser5__c = user0.Alias;
        event4.Grade5__c = '5';
        event4.Comment5__c = 'test5';
        event4.WhatId = opp.Id;
        list_events.add(event4);*/
        insert list_events;
        
        system.Test.startTest();
    	ActivityFieldProcessingBatch ActivityBatch = new ActivityFieldProcessingBatch();
        database.executeBatch(ActivityBatch,5);
        system.Test.stopTest();
    }
}