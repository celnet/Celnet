/*
*作者：Tobe
*时间：2013-10-27
*功能：根据销售医院维度表，获取医院维度表,医院基础表和病人计划中的信息，填充到销售医院基础表中
*/
global class PM_HospitalSalesBasicTableBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful
{
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		return Database.getQueryLocator([Select Id,
										(Select  PM_NewPatient__c, PM_Surviving__c, PM_Dropout__c 
											From PM_HospitalBasis__r 
											Where PM_YearMonth__c = THIS_MONTH) ,
										(Select PM_Saler__c, PM_Account__c, PM_Date__c 
											From PM_SHDimensions__r 
											Where PM_Date__c = THIS_MONTH AND PM_UserProduct__c = 'PD'),
										(Select PM_Year__c, PM_PlanAdd1__c, PM_PlanSurviving1__c, PM_PlanAdd2__c, PM_PlanSurviving2__c, PM_PlanAdd3__c, PM_PlanSurviving3__c, PM_PlanAdd4__c, PM_PlanSurviving4__c,PM_PlanAdd5__c, PM_PlanSurviving5__c,PM_PlanAdd6__c, PM_PlanSurviving6__c,PM_PlanAdd7__c, PM_PlanSurviving7__c, PM_PlanAdd8__c, PM_PlanSurviving8__c, PM_PlanSurviving9__c, PM_PlanAdd9__c, PM_PlanAdd10__c, PM_PlanSurviving10__c,  PM_PlanAdd11__c, PM_PlanSurviving11__c, PM_PlanSurviving12__c, PM_PlanAdd12__c 
											From planhjBT__r 
											where PM_Year__c = THIS_YEAR) 
										From Account 
										where RecordType.DeveloperName = 'RecordType']); 
	}
	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		
		//销售医院基础表list：PM_HospitalSaels__c
		list<PM_HospitalSales__c> HospitalSales_list = new list<PM_HospitalSales__c>();
		//循环医院
		for(sObject sObj : scope)
		{
			Account acc = (Account)sObj;
			//如果当前医院下没有销售与医院维度表本月对应记录，跳过
			if(acc.PM_SHDimensions__r.size()==0)
			{
				continue;
			}
			PM_HospitalSales__c HospitalSaels = new PM_HospitalSales__c();
			//“销售”字段取销售医院维度表上对应的销售ID
			HospitalSaels.PM_Saler__c =  acc.PM_SHDimensions__r[0].PM_Saler__c;
			//“插管医院”字段取当前医院ID
			HospitalSaels.PM_Hospital__c = acc.Id;
			//“年月”字段取当前日期
			HospitalSaels.PM_YearMonth__c = date.today();
			//外部ID：医院ID+年月
			HospitalSaels.PM_UniquelyId__c = string.valueOf(acc.Id)+string.valueOf(HospitalSaels.PM_Saler__c) + string.valueOf(date.today().year()) + string.valueOf(date.today().month());
			//拼接“本月计划新增病人数”字段API名
			string PlanAdd = 'PM_PlanAdd';
			PlanAdd+= string.valueOf(date.today().month())+'__c';
			//拼接“本月计划存活病人数”字段API名
			string PlanSurviving = 'PM_PlanSurviving';
			PlanSurviving+= string.valueOf(date.today().month())+'__c';
			//如果当前医院本月病人计划不为空
			if(acc.planhjBT__r.size()>0)
			{
				//为“新病人目标”字段赋值“本月计划新增病人数”
				if(acc.planhjBT__r[0].get(PlanAdd) != null)
				{
					HospitalSaels.PM_NewPatientTarget__c = integer.valueOf(acc.planhjBT__r[0].get(PlanAdd));
				}
				//为“存活目标”字段赋值“本月计划存活病人数”
				if(acc.planhjBT__r[0].get(PlanSurviving) != null )
				{
					HospitalSaels.PM_SurvivingTarget__c = integer.valueOf(acc.planhjBT__r[0].get(PlanSurviving));
				}
			}
			//如果当前医院存在本月医院基础表数据
			if(acc.PM_HospitalBasis__r.size()>0)
			{
				//“New数量”字段取医院基础表上的新病人数量
				HospitalSaels.PM_NewPatient__c = acc.PM_HospitalBasis__r[0].PM_NewPatient__c;
				//“存活数量”字段取医院基础表上的存活病人数量
				HospitalSaels.PM_Surviving__c = acc.PM_HospitalBasis__r[0].PM_Surviving__c;
				//“退出数量”字段取医院基础表上的退出病人数量
				HospitalSaels.PM_Dropout__c = acc.PM_HospitalBasis__r[0].PM_Dropout__c;
			}
			HospitalSales_list.add(HospitalSaels);
		}
		if(HospitalSales_list.size()>0)
		{
			upsert HospitalSales_list PM_UniquelyId__c;
		}
	}
	global void finish(Database.BatchableContext BC)
	{
    	
    }
}