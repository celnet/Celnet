/*
 * 作者:bill
 * 日期：2013-10-27
 * 说明：导出数据页面的控制类
 */
public class PM_PatinetExportController 
{
	//导出excel功能需要的参数  点击导出按钮，原页面隐藏 只显示下面的要导出的列表
    public Transient List<PM_ExportFieldsCommon.ExportField> list_ExportField{get;set;}
    public Transient List<List<PM_Patient__c>> list_PatientExcellist{get;set;}
    public string ExcelTime
    {
    	get
    	{
    		String name = '';
    		name += String.ValueOf(Datetime.Now().year());
    		name += String.ValueOf(Datetime.Now().month());
    		name += String.ValueOf(Datetime.Now().day());
    		name += String.ValueOf(Datetime.Now().hour());
    		name += String.ValueOf(Datetime.Now().minute());
    		name += String.ValueOf(Datetime.Now().second());
    		return name;
    	} 
    	set;
    }
    //导出的字段
    private string strFields;
    
	//导出排序工具类
    private Transient PM_ExportFieldsCommon field = new PM_ExportFieldsCommon();
	
	//构造器
	public PM_PatinetExportController()
	{
		//contentType = 'application/vnd.ms-excel#Patient.xls';
		getlist_PatientExcel();
		getExportField();
	}
	
    //页面需要显示的字段集合
    private void getExportField() 
    {
    	list_ExportField = new List<PM_ExportFieldsCommon.ExportField>();
    	Transient Map<String, Schema.FieldSetMember> map_field = new Map<String, Schema.FieldSetMember>();
        for(Schema.FieldSetMember f : SObjectType.PM_Patient__c.FieldSets.PM_PatientFieldSet.getFields()) 
        {
        	map_field.put(f.getFieldPath(), f);
        }
        if(strFields != null)
        {
	        if(strFields.IndexOf('Select') >=0 && strFields.IndexOf('from') >=0)
	        {
	        	strFields = strFields.substring(strFields.indexOf('Select')+7, strFields.indexOf(' from'));
	        	for(string api : strFields.split(','))
	        	{
	        		if(map_field.get(api) != null)
	        		{
	        			PM_ExportFieldsCommon.ExportField ex = new PM_ExportFieldsCommon.ExportField();
	        			//ex.fieldLabel = map_field.get(api).getLabel(); 
	        			ex.fieldLabel = field.getFieldLabelName(map_field.get(api)); 
	        			ex.apiName = map_field.get(api);
	        			list_ExportField.add(ex);
	        		}
	        	}
	        }
        }
    }
    
    //页面导出Excel
    private void getlist_PatientExcel()
    {
    	List<PM_Soql__c> list_Sql = [Select p.PM_Field__c, p.PM_SOQL_5__c, p.PM_SOQL_4__c, p.PM_SOQL_3__c, p.PM_SOQL_2__c, p.PM_SOQL_1__c, p.PM_SOQL_0__c, p.Name From PM_Soql__c p where p.Name = : UserInfo.getUserId()];
    	if(list_Sql != null && list_Sql.size()>0)
    	{
    		String strSql = '';
    		if(list_Sql[0].PM_SOQL_0__c != null)
    		{
    			strSql += list_Sql[0].PM_SOQL_0__c;
    		}
    		if(list_Sql[0].PM_SOQL_1__c != null)
    		{
    			strSql += list_Sql[0].PM_SOQL_1__c;
    		}
    		if(list_Sql[0].PM_SOQL_2__c != null)
    		{
    			strSql += list_Sql[0].PM_SOQL_2__c;
    		}
    		if(list_Sql[0].PM_SOQL_3__c != null)
    		{
    			strSql += list_Sql[0].PM_SOQL_3__c;
    		}
    		if(list_Sql[0].PM_SOQL_4__c != null)
    		{
    			strSql += list_Sql[0].PM_SOQL_4__c;
    		}
    		if(list_Sql[0].PM_SOQL_5__c != null)
    		{
    			strSql += list_Sql[0].PM_SOQL_5__c;
    		}
    		if(strSql != '' && strSql.length() > 0)
    		{
    			try{
    				strSql = strSql.substring(0, strSql.length()-1);
		    		list<string> list_Ids = new list<string>();
		    		if(strSql.contains(','))
		    		{
		    			list_Ids.addAll(strSql.split(','));
		    		}
		    		else
		    		{
		    			list_Ids.add(strSql);
		    		}
		    		if(list_Sql[0].PM_Field__c != null)
		    		{
		    			strFields = list_Sql[0].PM_Field__c;
    				    Transient List<PM_Patient__c> list_PatientExcel = new List<PM_Patient__c>();
				    	list_PatientExcellist = new List<List<PM_Patient__c>>();
				        for(PM_Patient__c p : Database.query( strFields + 'list_Ids'))
				        {
				        	if(list_PatientExcel.size() < 999)
				        	{
				        		list_PatientExcel.add(p);
				        	}else{
				        		list_PatientExcel.add(p);
				        		list_PatientExcellist.add(list_PatientExcel);
				        		list_PatientExcel = new List<PM_Patient__c>();
				        	}
				        }
				        list_PatientExcellist.add(list_PatientExcel);
		    		}
    			}catch(Exception e){}
    		}
    	}
    }
}