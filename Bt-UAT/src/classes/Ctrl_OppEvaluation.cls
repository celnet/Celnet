/**
 * 作者：Sunny
 * 
 * 
**/
public class Ctrl_OppEvaluation {
	public List<Line> listLine{get;set;}
	public Boolean isAdmin{get;set;}
	public String strSelectYear{get;set;}
	public List<SelectOption> getYears(){
		List<SelectOption> options = new List<SelectOption>();
		Integer intYear = date.today().Year();
		options.add(new SelectOption(String.valueOf(intYear),String.valueOf(intYear)));
		options.add(new SelectOption(String.valueOf(intYear-1),String.valueOf(intYear-1)));
		//strSelectYear = String.valueOf(intYear);
		return options;
	}
    public Ctrl_OppEvaluation(){
    	List<User> listuser= [Select Id,Name,UserRoleId,UserRole.Name,Profile.Name,ProfileId,Alias From User Where Id =: UserInfo.getUserId()];
    	if(listuser.size() > 0 ){
    		String strProName = listuser[0].Profile.Name;
    		if(strProName.toUpperCase().contains('ADMIN') || strProName=='系统管理员'){
                isAdmin=true;
            }else{
                isAdmin=false;
            }
    	}
    	
    	listLine = new List<Line>();
    	strSelectYear = String.valueOf(date.today().year());
    	buildTable();
    }
    public void buildTable(){
    	listLine.clear();
    	Set<String> set_month = new Set<String>();
    	system.debug(UserInfo.getUserId()+' -- '+strSelectYear);
    	for(OppEvaluation__c oppE : [Select Id,Month__c From OppEvaluation__c Where Opportunity__c = null And Commentator__c =: UserInfo.getUserId() And Year__c =: strSelectYear order by Month__c desc]){
    		if(!set_month.contains(oppE.Month__c)){
    			Line tLine = new Line();
    			tLine.strYear=strSelectYear;
    			tLine.strMonth=oppE.Month__c;
    			tLine.strUrlName = strSelectYear+'.'+oppE.Month__c+'下属策略点评列表';
    			tLine.strUrl = 'https://'+system.Url.getSalesforceBaseUrl().getHost()+'/apex/OpportunityStrategyCommentLevelTwo?year='+strSelectYear+'&month='+oppE.Month__c;
    			listLine.add(tLine);
    			set_month.add(oppE.Month__c);
    		}
    	}
    }
    public class Line{
        public String strYear{get;set;}
        public String strMonth{get;set;}
        public String strUrl{get;set;}
        public String strUrlName{get;set;}
    }
    
    static testMethod void myUnitTest() {
    	//OppEvaluation__c
    	List<OppEvaluation__c> list_oppEva = new List<OppEvaluation__c>();
    	OppEvaluation__c oppEva1 = new OppEvaluation__c();
    	oppEva1.Year__c = String.valueOf(date.today().year());
    	oppEva1.Month__c = String.valueOf(date.today().month());
    	oppEva1.Commentator__c = userinfo.getUserId();
    	list_oppEva.add(oppEva1);
    	OppEvaluation__c oppEva2 = new OppEvaluation__c();
        oppEva2.Year__c = String.valueOf(date.today().year());
        oppEva2.Month__c = String.valueOf(date.today().month());
        oppEva2.Commentator__c = userinfo.getUserId();
        list_oppEva.add(oppEva2);
        insert list_oppEva ;
    	
    	system.Test.startTest();
    	Ctrl_OppEvaluation oppEva = new Ctrl_OppEvaluation();
    	oppEva.getYears();
    	system.Test.stopTest();
    }
}