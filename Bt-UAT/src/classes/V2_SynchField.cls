/* 
 * Author: Sunny
 * Created on: 2011-12-26
 * Description: 复制字段
 * 
 * 
 */
public without sharing class V2_SynchField 
{
    public void cloneFieldFromLeadToAccount(Lead objLead, Account objAccount)
    {
        if(objLead.Name != null)
        {
            objAccount.Name = objLead.Name ;            
        }else
        {
            objAccount.Name = objLead.LastName ;
        }
        objAccount.Address__c = objLead.V2_Address__c ;
        objAccount.BillingCity = objLead.V2_BillingCity__c ;
        objAccount.BillingCountry = objLead.V2_BillingCountry__c ;
        objAccount.BillingPostalCode = objLead.V2_BillingPostalCode__c ;
        objAccount.BillingState = objLead.V2_BillingState__c ;
        objAccount.BillingStreet = objLead.V2_BillingStreet__c ;
        objAccount.Cities__c = objLead.V2_Cities__c ;
        objAccount.GAccountType__c = objLead.V2_GAccountType__c ;
        objAccount.Grade__c = objLead.V2_Grade__c ;
        objAccount.InsuranceAmtByYear__c = objLead.V2_InsuranceAmtByYear__c ;
        objAccount.IsEducation__c = objLead.V2_IsEducation__c ;
        objAccount.ManTimeHospital__c = objLead.V2_ManTimeHospital__c ;
        objAccount.MedicineAmtByYear__c = objLead.V2_MedicineAmtByYear__c ;
        objAccount.MID__c = objLead.V2_Mid__c ;
        objAccount.OperationTime__c = objLead.V2_OperationTime__c ;
        objAccount.OutPatientAmtByMonth__c = objLead.V2_OutPatientAmtByMonth__c ;
        objAccount.Ownership = objLead.V2_Ownership__c ;
        objAccount.PersonInHospital__c = objLead.V2_PersonInHospital__c ;
        objAccount.PostCode__c = objLead.V2_PostCode__c ;
        objAccount.Provinces__c = objLead.V2_Provinces__c ;
        objAccount.RenalMarketType__c = objLead.V2_RenalMarketType__c ;
        objAccount.ShippingCity = objLead.V2_ShippingCity__c ;
        objAccount.ShippingCountry = objLead.V2_ShippingCountry__c ;
        objAccount.ShippingPostalCode = objLead.V2_ShippingPostalCode__c ;
        objAccount.ShippingState = objLead.V2_ShippingState__c ;
        objAccount.ShippingStreet = objLead.V2_ShippingStreet__c ;
        objAccount.Sic = objLead.V2_Sic__c ;
        objAccount.Site = objLead.V2_Site__c ;
        objAccount.Status__c = objLead.V2_Stauts__c ;
        objAccount.TickerSymbol = objLead.V2_TickerSymbol__c ;
        objAccount.TotalBedsActual__c = objLead.V2_TotalBedsActual__c ;
        objAccount.TotalBedsStandard__c = objLead.V2_TotalBedsStandard__c ;
        objAccount.TotalDoctors__c = objLead.V2_TotalDoctors__c ; 
        objAccount.AccountNumber = objLead.Company ;
        objAccount.Type = objLead.V2_Type__c ;
        objAccount.OwnerId = objLead.OwnerId ;
        objAccount.Phone = objLead.Phone ;
        objAccount.Fax = objLead.Fax ;
        objAccount.Website = objLead.Website ;
        objAccount.Industry = objLead.Industry ;
        objAccount.Description = objLead.Description ;
        objAccount.AnnualRevenue = objLead.AnnualRevenue ;
        objAccount.NumberOfEmployees = objLead.NumberOfEmployees ;
        objAccount.Rating = objLead.Rating ;
        //objAccount.V2_Lead__c = objLead.Id ;
    }
    public void cloneFieldFromAccountToLead(Lead objLead, Account objAccount)
    {
        objLead.LastName = objAccount.Name ;
        objLead.V2_Address__c = objAccount.Address__c  ;
        objLead.V2_BillingCity__c = objAccount.BillingCity ;
        objLead.V2_BillingCountry__c = objAccount.BillingCountry ;
        objLead.V2_BillingPostalCode__c = objAccount.BillingPostalCode ;
        objLead.V2_BillingState__c = objAccount.BillingState ;
        objLead.V2_BillingStreet__c = objAccount.BillingStreet ;
        objLead.V2_Cities__c = objAccount.Cities__c ;
        objLead.V2_GAccountType__c = objAccount.GAccountType__c ;
        objLead.V2_Grade__c = objAccount.Grade__c ;
        objLead.V2_InsuranceAmtByYear__c = objAccount.InsuranceAmtByYear__c ;
        objLead.V2_IsEducation__c = objAccount.IsEducation__c ;
        objLead.V2_ManTimeHospital__c = objAccount.ManTimeHospital__c ;
        objLead.V2_MedicineAmtByYear__c = objAccount.MedicineAmtByYear__c ;
        objLead.V2_Mid__c = objAccount.MID__c ;
        objLead.V2_OperationTime__c = objAccount.OperationTime__c ;
        objLead.V2_OutPatientAmtByMonth__c = objAccount.OutPatientAmtByMonth__c ;
        objLead.V2_Ownership__c = objAccount.Ownership ;
        objLead.V2_PersonInHospital__c = objAccount.PersonInHospital__c ;
        objLead.V2_PostCode__c = objAccount.PostCode__c ;
        objLead.V2_Provinces__c = objAccount.Provinces__c ;
        objLead.V2_RenalMarketType__c = objAccount.RenalMarketType__c ;
        objLead.V2_ShippingCity__c = objAccount.ShippingCity ;
        objLead.V2_ShippingCountry__c = objAccount.ShippingCountry ;
        objLead.V2_ShippingPostalCode__c = objAccount.ShippingPostalCode ;
        objLead.V2_ShippingState__c = objAccount.ShippingState ;
        objLead.V2_ShippingStreet__c = objAccount.ShippingStreet ;
        objLead.V2_Sic__c = objAccount.Sic ;
        objLead.V2_Site__c = objAccount.Site ;
        objLead.V2_Stauts__c = objAccount.Status__c ;
        objLead.V2_TickerSymbol__c = objAccount.TickerSymbol ;
        objLead.V2_TotalBedsActual__c = objAccount.TotalBedsActual__c ;
        objLead.V2_TotalBedsStandard__c = objAccount.TotalBedsStandard__c ;
        objLead.V2_TotalDoctors__c = objAccount.TotalDoctors__c ; 
        objLead.Company = objAccount.AccountNumber ;
        objLead.V2_Type__c = objAccount.Type ;
        objLead.OwnerId = objAccount.OwnerId ;
        objLead.Phone = objAccount.Phone ;
        objLead.Fax = objAccount.Fax ;
        objLead.Website = objAccount.Website ;
        objLead.Industry = objAccount.Industry ;
        objLead.Description = objAccount.Description ;
        objLead.AnnualRevenue = objAccount.AnnualRevenue ;
        objLead.NumberOfEmployees = objAccount.NumberOfEmployees ;
        objLead.Rating = objAccount.Rating ;
        if(objLead.Company == null)objLead.Company = objAccount.Name ;
    }
    public void cloneAccTeamFieldFromAccountToLead(V2_Account_Team__c objAccTeam,V2_Lead_Team__c objLeadTeam)
    {
        
    }
    public void cloneAccTeamFieldFromLeadToAccount(V2_Lead_Team__c objLeadTeam,V2_Account_Team__c objAccTeam)
    {
        //objAccTeam.V2_AccountPermissions__c = objLeadTeam.V2_AccountPermissions__c ;
        //objAccTeam.V2_CasePermissions__c = objLeadTeam.V2_CasePermissions__c ;
        objAccTeam.V2_Effective_Year__c = objLeadTeam.V2_Effective_Year__c ;
        objAccTeam.V2_Effective_Month__c = objLeadTeam.V2_Effective_Month__c ;
        //objAccTeam.V2_Effective_date__c = objLeadTeam.V2_Effective_date__c ;
        //objAccTeam.V2_OppPermissions__c = objLeadTeam.V2_OppPermissions__c ;
        //objAccTeam.V2_Role__c = objLeadTeam.V2_Role__c ;
        objAccTeam.V2_User__c = objLeadTeam.V2_User__c ;
        objAccTeam.OwnerId = objLeadTeam.OwnerId;
    }
    public void cloneFieldFromLeadTeamToATM(V2_Lead_Team__c objLeadTeam,AccountTeamMember objATM)
    {
        objATM.UserId = objLeadTeam.V2_User__c ;
        objATM.TeamMemberRole = objLeadTeam.V2_Role__c ;
        //objATM.AccountAccessLevel = objLeadTeam.V2_AccountPermissions__c ;
    }
    public void cloneFieldFromAccountTeamToATM(V2_Account_Team__c objAccTeam,AccountTeamMember objATM)
    {
        objATM.UserId = objAccTeam.V2_User__c ;
        objATM.TeamMemberRole = objAccTeam.V2_Role__c ;
        //objATM.AccountAccessLevel = objAccTeam.V2_AccountPermissions__c ;
    }
    static testMethod void TestLeadToAccount()
    {
        //---------------New Lead------------------
        Lead objLead = new Lead() ;
        objLead.LastName = 'nnnkl' ;
        objLead.Company = 'wefdf' ;
        objLead.V2_Mid__c = 'wf' ;
        insert objLead ;
        Lead objLead2 = [Select l.Website, l.V2_Type__c, l.V2_TotalDoctors__c, l.V2_TotalBedsStandard__c, 
            l.V2_TotalBedsActual__c, l.V2_TickerSymbol__c, l.V2_Stauts__c, l.V2_Site__c, l.V2_Sic__c, 
            l.V2_ShippingStreet__c, l.V2_ShippingState__c, l.V2_ShippingPostalCode__c, l.V2_ShippingCountry__c, 
            l.V2_ShippingCity__c, l.V2_RenalMarketType__c, l.V2_Provinces__c, l.V2_PostCode__c, l.V2_PersonInHospital__c, 
            l.V2_Ownership__c, l.V2_OutPatientAmtByMonth__c, l.V2_OperationTime__c, l.V2_Mid__c, l.V2_MedicineAmtByYear__c, 
            l.V2_ManTimeHospital__c, l.V2_IsEducation__c, l.V2_InsuranceAmtByYear__c, l.V2_Grade__c, l.V2_GAccountType__c, 
            l.V2_Cities__c, l.V2_BillingStreet__c, l.V2_BillingState__c, l.V2_BillingPostalCode__c, l.V2_BillingCountry__c, 
            l.V2_BillingCity__c, l.V2_Address__c, l.V2_AccountNumber__c, l.Title, l.SystemModstamp, 
            l.Street, l.Status, l.State, l.Salutation, l.RecordTypeId, l.Rating, l.PostalCode, l.Phone, l.OwnerId, 
            l.NumberOfEmployees, l.Name, l.MobilePhone, l.MasterRecordId, l.LeadSource, l.LastName, l.LastModifiedDate, 
            l.LastModifiedById, l.LastActivityDate, l.JigsawContactId, l.Jigsaw, l.IsUnreadByOwner, l.IsDeleted, 
            l.IsConverted, l.Industry, l.Id, l.HasOptedOutOfEmail, l.FirstName, l.Fax, l.EmailBouncedReason, l.EmailBouncedDate, 
            l.Email, l.Description, l.CreatedDate, l.CreatedById, l.Country, l.ConvertedOpportunityId, l.ConvertedDate, 
            l.ConvertedContactId, l.ConvertedAccountId, l.Company, l.City, l.AnnualRevenue From Lead l Where Id =: objLead.Id] ;
        //---------------New Account------------------
        Account objAccount = new Account() ;
        V2_SynchField objSynchField = new V2_SynchField() ;
        objSynchField.cloneFieldFromLeadToAccount(objLead2, objAccount) ;
        
    } 
    static testMethod void TestAccountToLead()
    {
        //---------------New Account-------------------
        Account objAcc = new Account() ;
        objAcc.Name='accname' ;
        objAcc.MID__c = 'adfew' ;
        insert objAcc ;
        Account objAccount = [Select a.Website, a.V2_Lead__c, a.Type, a.TotalDoctors__c, 
            a.TotalBedsStandard__c, a.TotalBedsActual__c, a.TickerSymbol, a.TSC_SendEmailNotify__c, a.TSC_MachineInfoID__c, 
            a.SystemModstamp, a.Status__c, a.Site, a.Sic, a.ShippingStreet, a.ShippingState, a.ShippingPostalCode, 
            a.ShippingCountry, a.ShippingCity, a.RenalMarketType__c, a.RecordTypeId, a.Rating, a.Provinces__c, a.PostCode__c, 
            a.Phone, a.PersonInHospital__c, a.ParentId, a.Ownership, a.OwnerId, a.OutPatientAmtByMonth__c, a.OperationTime__c, 
            a.NumberOfEmployees, a.Name, a.MedicineAmtByYear__c, a.MasterRecordId, a.ManTimeHospital__c, a.MID__c, 
            a.LastModifiedDate, a.LastModifiedById, a.LastActivityDate, a.JigsawCompanyId, a.Jigsaw, a.IsEducation__c, 
            a.IsDeleted, a.InsuranceAmtByYear__c, a.Industry, a.Id, a.Grade__c, a.GAccountType__c, a.Fax, a.Description, 
            a.CreatedDate, a.CreatedById, a.Cities__c, a.BillingStreet, a.BillingState, a.BillingPostalCode, a.BillingCountry, 
            a.BillingCity, a.AnnualRevenue, a.Address__c, a.AccountNumber From Account a Where Id =: objAcc.Id] ;
        //---------------New Lead--------------------
        Lead objLead = new Lead() ;
        
        V2_SynchField objSynchField = new V2_SynchField() ;
        objSynchField.cloneFieldFromAccountToLead(objLead, objAccount) ;
        //insert objLead ;
    }
}