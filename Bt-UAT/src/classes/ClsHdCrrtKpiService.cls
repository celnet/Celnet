/**
 * 作者:Sunny
 * 说明：SEP HD/CRRT KPI计算
**/
public class ClsHdCrrtKpiService {
    ID UserId ;
    Integer IntYear ;
    Integer IntMonth ;
    List<Event> list_ValidEvent ;
    Set<ID> set_ValidOppIds ;
    MonthlyPlan__c MonthlyPlan;
    public ClsHdCrrtKpiService(ID sUserId,Integer sIntYear,Integer sIntMonth){
        UserId = sUserId;
        IntYear = sIntYear;
        IntMonth = sIntMonth ; 
    }
    
    //KPI:实际拜访目标联系人人数
    //说明：当月月计划明细中实际拜访联系人数
    public Map<String , Double> ActualVisitContactNum(){
        Map<String , Double> map_Result = new Map<String , Double>();
        Set<ID> set_MonthlyPlanDetail = new Set<ID>();
        //当月月计划
        if(MonthlyPlan == null){
            MonthlyPlan = getMonthlyPlan();
        }
        //获取拜访明细中的联系人
        if(MonthlyPlan != null && MonthlyPlan.MonthlyPlanDetail__r!=null && MonthlyPlan.MonthlyPlanDetail__r.size() > 0){
            for(MonthlyPlanDetail__c mpd : MonthlyPlan.MonthlyPlanDetail__r){
                set_MonthlyPlanDetail.add(mpd.Contact__c);
            }
        }
        map_Result.put('Target' , 30);
        map_Result.put('Finish' , set_MonthlyPlanDetail.size());
        return map_Result ;
    }
    //获取当月月计划
    private MonthlyPlan__c getMonthlyPlan(){
        List<MonthlyPlan__c> list_MonthlyPlan = [Select m.Year__c, m.OwnerId, m.Month__c, m.Id, m.V2_TotalCallRecords__c, (Select Contact__c From MonthlyPlanDetail__r Where Contact__c != null And Planned_Finished_Calls__c > 0) From MonthlyPlan__c m Where OwnerId =: UserId And Year__c =: String.valueOf(IntYear) And Month__c =: String.valueOf(IntMonth)];
        return list_MonthlyPlan.size()>0?list_MonthlyPlan[0]:null;
    }
    
    //KPI:拜访完成率
    //说明：当月完成的拜访且未过期/计划拜访数
    public Map<String , Double> VisitRate(){
        Map<String , Double> map_Result = new Map<String , Double>();
        //获取已完成未过期的拜访
        if(list_ValidEvent==null){
            list_ValidEvent = getValidVisit();
        }
        List<Event> list_Event = list_ValidEvent;
        //已完成未过期拜访数
        Double VisitQty = list_Event.size();
        
        //计划拜访数（当月月计划明细中的所有“计划次数”字段的值之和）
        List<MonthlyPlan__c> monthPlan = [Select Id,V2_TotalCallRecords__c From MonthlyPlan__c Where Year__c =: String.valueOf(IntYear) And Month__c =: String.valueOf(IntMonth) And OwnerId =: UserId Limit 1] ;
        Double PlanQty = 0;
        if(monthPlan.size() > 0){
            PlanQty = monthPlan[0].V2_TotalCallRecords__c;
        }
        
        
        map_Result.put('Target' , PlanQty);
        map_Result.put('Finish' , VisitQty);
        if(PlanQty==null || PlanQty==0){
            map_Result.put('Rate' , 0);
        }else{
            map_Result.put('Rate' , VisitQty/PlanQty );
        }
        return map_Result;
    }
    //获取已完成未过期的拜访
    private List<Event> getValidVisit(){
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        //List<Event> list_Visit=[Select Id,GAPlan__c,WhoId,Who.Name,Who.RecordTypeId,GAExecuteResult__c From Event ];
        List<Event> list_Visit=[Select Id,GAPlan__c,WhoId,GAExecuteResult__c From Event Where IsChild = false And RecordType.DeveloperName = 'V2_Event' And Done__c = true And V2_IsExpire__c = false And StartDateTime >: CheckStartDateTime And StartDateTime <: CheckEndDateTime And OwnerId =: UserId];
        return list_Visit ;
    }
    
    //KPI:访前计划及访后分析完成率
    //说明：当月已完成的未过期的拜访中填写访前计划和访后分析的比例((访前计划完成%+访后分析完成%)/2)
    public Map<String , Double> VisitPlanRate(){
        Map<String , Double> map_Result = new Map<String , Double>();
        //拜访次数
        double VisitNum;
        //访前计划次数
        double VisitPlanNum;
        //访后分析次数
        double VisitAnalysisNum;
        //访前计划完成率
        double VisitPlan;
        //访后反洗完成率
        double VisitAnalysis;
        
        //获取已完成未过期的拜访
        if(list_ValidEvent==null){
            list_ValidEvent = getValidVisit();
        }
        List<Event> list_Event = list_ValidEvent;
        
        VisitNum = list_Event.size() ;
        for(Event objEvent : list_Event){
            if(objEvent.GAPlan__c != null){
                VisitPlanNum = (VisitPlanNum==null?0:VisitPlanNum) + 1 ;
            }
            if(objEvent.GAExecuteResult__c != null){
                VisitAnalysisNum = (VisitAnalysisNum==null?0:VisitAnalysisNum) + 1 ;
            }
        }
        
        if(VisitNum != null && VisitPlanNum != null){
            VisitPlan = VisitPlanNum / VisitNum ;
        }
        if(VisitNum != null && VisitAnalysisNum != null){
            VisitAnalysis = VisitAnalysisNum / VisitNum;
        }
        map_Result.put('Target' , VisitNum);
        map_Result.put('Finish Before' , VisitPlanNum);
        map_Result.put('Finish After' , VisitAnalysisNum);
        map_Result.put('Rate' , ((VisitPlan==NULL?0:VisitPlan)+(VisitAnalysis==NULL?0:VisitAnalysis))/2);
        return  map_Result;
        
    }
    
    
    //KPI:业务机会数
    //说明：当月进行中的业务机会
    public Map<String , Double> OpportunityQuantity(){
        Map<String , Double> map_Result = new Map<String , Double>();
        if(set_ValidOppIds == null){
            set_ValidOppIds = getValidOpportunity('RENAL');
        }
        Set<ID> set_OppId = set_ValidOppIds;
        map_Result.put('Target' , 8);
        map_Result.put('Finish' , set_OppId.size());
        return map_Result;
    }
    public Map<String , Double> OpportunityQuantityACUTE(){
    	Map<String , Double> map_Result = new Map<String , Double>();
        if(set_ValidOppIds == null){
            set_ValidOppIds = getValidOpportunity('ACUTE');
        }
        Set<ID> set_OppId = set_ValidOppIds;
        map_Result.put('Target' , 8);
        map_Result.put('Finish' , set_OppId.size());
        return map_Result;
    }
    //获取有效的业务机会
    private Set<ID> getValidOpportunity(String sRecordType){
        Date CheckDate = date.valueOf(IntYear+'-'+IntMonth+'-1');
        Date endMonthDate = CheckDate.addMonths(1);
        
        Set<ID> set_ValidOppId = new Set<ID>();
        //查找用户对应月份负责的业务机会
        Set<ID> set_OppId = V2_UtilClass.GetOpportunity(IntYear,IntMonth,UserId);
        
        //所负责业务机会中，一直在进行中的业务机会
        List<String> list_OppStage = new List<String>{'发现/验证机会','建立沟通渠道','需求分析','提交合作方案/谈判','跟进合作方案'};
        for(Opportunity opp : [Select Id,Renal_Opp_MGT__c From Opportunity 
                Where StageName in: list_OppStage 
                And RecordType.DeveloperName =: sRecordType
                And CloseDate >=: CheckDate
                And Id in: set_OppId
                And CreatedDate <: endMonthDate]){
            if(opp.Renal_Opp_MGT__c == '接受' ||  opp.Renal_Opp_MGT__c == null){
                set_ValidOppId.add(opp.Id);
            }
        }
        //对应月份曾经在进行中的业务机会
        //所负责业务机会中，对应月份曾经在进行中的业务机会 2013-3-29 Sunny修改：对曾经进行中的业务机会，只需要判断查询月曾经有过进行中的阶段，不需要判断结束日期
        for(OpportunityHistory__c OppHistory : [Select Id,Opportunity__c,Opportunity__r.Renal_Opp_MGT__c From OpportunityHistory__c 
                Where ChangedDate__c >=: CheckDate 
                And ChangedDate__c <: endMonthDate 
                And (NewOppStage__c in: list_OppStage OR PastOppStage__c in: list_OppStage)
                And Opportunity__r.RecordType.DeveloperName =: sRecordType
                //And Opportunity__r.CloseDate >=: CheckDate
                And Opportunity__c in: set_OppId]){
            if(OppHistory.Opportunity__r.Renal_Opp_MGT__c == '接受' ||  OppHistory.Opportunity__r.Renal_Opp_MGT__c == null){
                set_ValidOppId.add(OppHistory.Opportunity__c);
            }
        }
        return set_ValidOppId;
        
    }
    
    //KPI:业务机会相关事件达成率
    //说明：
    //2013-3-28 sunny 修改，该指标所有BU不需要考虑曾经进行中的业务机会只要检查当前进行中的业务机会
    //2014-1-16 Sunny HD不再考核该KPI
    public Map<String , Double> OpportunityEventRate(){
        Map<String , Double> map_Result = new Map<String , Double>();
        
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        //获取所有有效的业务机会的ID
        Set<ID> set_OppId = getValidOpportunityForVisitRate('RENAL');
        //有效跟进的业务机会数量
        Double ValidOppQty = 0;
        //所有进行中的业务机会数量
        Double OppQty = set_OppId.size();
        //标准为8个
        Double OppStaQty = 8 ;
        //查找这些业务机会的有效事件（已完成）
        for(Opportunity objOpp : [Select Id,(Select StartDateTime, OwnerId, Done__c, V2_IsExpire__c From Events Where IsChild = false And RecordType.DeveloperName = 'V2_RecordType' And Done__c = true And StartDateTime>:CheckStartDateTime And StartDateTime<:CheckEndDateTime) From Opportunity o Where Id in: set_OppId]){
            if(objOpp.Events!=null && objOpp.Events.size() > 0){
                ValidOppQty++;
            }
        }
        map_Result.put('Target' , 8);
        map_Result.put('Finish' , ValidOppQty);
        map_Result.put('Rate' , ValidOppQty/OppStaQty);
        return map_Result;
    }
    
    //KPI:业务机会相关拜访达成率
    //2013-3-28 sunny 修改，该指标所有BU不需要考虑曾经进行中的业务机会只要检查当前进行中的业务机会
    //2013-4-3 sunny 修改HD/CRRT部门业务机会标准个数为8个。
    public Map<String , Double> OpportunityVisitRate(){
        Map<String , Double> map_Result = new Map<String , Double>();
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        //获取所有有效的业务机会的ID
        Set<ID> set_OppId = getValidOpportunityForVisitRate('RENAL');
        //有效跟进的业务机会数量
        Double ValidOppQty = 0;
        //所有进行中的业务机会数量
        Double OppQty = set_OppId.size();
        //标准为8个
        Double OppStaQty = 8 ;
        //查找这些业务机会的有效拜访（已完成，未过期）
        for(Opportunity objOpp : [Select Id,(Select StartDateTime, OwnerId, Done__c, V2_IsExpire__c From Events Where IsChild = false And RecordType.DeveloperName = 'V2_Event' And Done__c = true And V2_IsExpire__c = false And StartDateTime>:CheckStartDateTime And StartDateTime<:CheckEndDateTime) From Opportunity o Where Id in: set_OppId]){
            if(objOpp.Events!=null && objOpp.Events.size() >= 4){
                ValidOppQty++;
            }
        }
        map_Result.put('Target' , 8);
        map_Result.put('Finish' , ValidOppQty);
        map_Result.put('Rate' , ValidOppQty/OppStaQty);
        return map_Result;
    }
    public Map<String , Double> OpportunityVisitRateACUTE(){
        Map<String , Double> map_Result = new Map<String , Double>();
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        //获取所有有效的业务机会的ID
        Set<ID> set_OppId = getValidOpportunityForVisitRate('ACUTE');
        //有效跟进的业务机会数量
        Double ValidOppQty = 0;
        //所有进行中的业务机会数量
        Double OppQty = set_OppId.size();
        //标准为4个
        Double OppStaQty = 8 ;
        system.debug('i want know'+set_OppId);
        //查找这些业务机会的有效拜访（已完成，未过期）
        for(Opportunity objOpp : [Select Id,(Select StartDateTime, OwnerId, Done__c, V2_IsExpire__c From Events Where IsChild = false And RecordType.DeveloperName = 'V2_Event' And Done__c = true And V2_IsExpire__c = false And StartDateTime>:CheckStartDateTime And StartDateTime<:CheckEndDateTime) From Opportunity o Where Id in: set_OppId]){
            system.debug('event size:'+objOpp.Events.size());
            if(objOpp.Events!=null && objOpp.Events.size() >= 4){
                ValidOppQty++;
            }
        }
        map_Result.put('Target' , 8);
        map_Result.put('Finish' , ValidOppQty);
        map_Result.put('Rate' , ValidOppQty/OppStaQty);
        return map_Result;
    }
    //2013-3-29sunny:业务机会相关拜访达成率和业务机会相关事件达成率  执行 超简单逻辑。不用找曾经负责。
    private Set<ID> getValidOpportunityForVisitRate(String sRecordType){
        Date CheckDate = date.valueOf(IntYear+'-'+IntMonth+'-1');
        Date endMonthDate = CheckDate.addMonths(1);
        
        Set<ID> set_ValidOppId = new Set<ID>();
        //查找用户对应月份负责的业务机会
        //Set<ID> set_OppId = V2_UtilClass.GetOpportunity(IntYear,IntMonth,UserId);
        
        //一直在进行中的业务机会
        List<String> list_OppStage = new List<String>{'发现/验证机会','建立沟通渠道','需求分析','提交合作方案/谈判','跟进合作方案'};
        for(Opportunity opp : [Select Id,Renal_Opp_MGT__c From Opportunity 
                Where StageName in: list_OppStage 
                And RecordType.DeveloperName =: sRecordType
                And CloseDate >=: CheckDate
                And OwnerId =: UserId
                And CreatedDate <: endMonthDate]){
            if(opp.Renal_Opp_MGT__c == '接受' ||  opp.Renal_Opp_MGT__c == null){
                set_ValidOppId.add(opp.Id);
            }
        }
        return set_ValidOppId;
    }
    
    
    //主管KPI
    
    //KPI:直接管理的销售团队的平均SEP业绩
    public Map<String , Double> SepTeamPerformanceAVG(Boolean IsHaveHospital , Double SuperValue){
        Map<String , Double> map_Result = new Map<String , Double>();
        Double AVGGrade = 0;
        //直接汇报关系的下属
        List<ID> list_uId = new List<ID>();
        //查询所有在查询月份直接汇报下属
        for(V2_RoleHistory__c RoleHistory : [Select v.Role__c, v.Manager__c, v.Renal_valid_super__c, v.Name__c, v.IsOnHoliday__c, v.IsLeave__c, v.Department__c, v.PG__c, v.Level__c
            From V2_RoleHistory__c v 
            Where Month__c =: String.valueOf(intMonth) And Year__c =: String.valueOf(intYear) And IsOnHoliday__c = false And IsLeave__c = false And Status__c != '停用' And Manager__c=:UserId]){
            list_uId.add(RoleHistory.Name__c);
        }
        //下属人数
        Double TotalNum = 0 ;
        //下属得分
        Double TotalGrade = 0 ;
        //直接下属的奖金评分
        //2013-4-23 Sunny修改bug，查找奖金数据添加年月的条件
        for(Bonus_data__c bd:[Select Id,Total_Score__c From Bonus_data__c Where The_User__c in: list_uId And Month__c =: String.valueOf(intMonth) And Year__c =: String.valueOf(intYear)]){
            TotalNum++;
            
            TotalGrade = TotalGrade + (bd.Total_Score__c==null?0:bd.Total_Score__c);
        }
        system.debug(IsHaveHospital+'xiashu'+TotalGrade+' - '+TotalNum);
        if(IsHaveHospital == true){
            TotalNum+=1;
            TotalGrade += (SuperValue==null?0:SuperValue) ;
        }
        if(TotalNum!=0){
            AVGGrade = TotalGrade / TotalNum;
        }
        map_Result.put('TotalGrade' , TotalGrade);
        map_Result.put('TotalNum' , TotalNum);
        map_Result.put('AVGGrade' , AVGGrade);
        
        system.debug('pingjunfen'+map_Result);
        return map_Result ;
    }
    
    //KPI:协访数量（且完成评语）
    //当月主管协访的已完成的，且填写评语的拜访事件数量
    public Map<String , Double> AssVisitQuantity(){
        Map<String , Double> map_Result = new Map<String , Double>();
        Set<ID> list_AssVisitIds = getAssVisit();
        map_Result.put('Target' , 8);
        map_Result.put('Finish' , list_AssVisitIds.size());
        return map_Result ;
    }
    //获取有效的协访，部门要求接受了协访，并且填写过评语才算是有效的协访。
    private Set<ID> getAssVisit(){
        //拜访的起止时间
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        //有效协访ID
        Set<ID> Set_AssVisitId = new Set<ID>();
        //接受邀请的协访ID
        Set<ID> set_EventId = new Set<ID>();
        //修改为使用EventRelation而不是EventAttendee
        for(EventRelation er : [Select EventId,Status From EventRelation Where IsInvitee = true And Event.RecordType.DeveloperName = 'V2_Event' And Event.Done__c = true And RelationId =: UserId And Event.StartDateTime >: CheckStartDateTime And Event.StartDateTime <: CheckEndDateTime]){
            if(er.Status=='已接受' || er.Status=='Accepted'){
                set_EventId.add(er.EventId) ;
            }
        }
        //考察月份及下个月进行的评价
        Date startDate = date.valueOf(IntYear+'-'+IntMonth+'-1');
        Date endDate = startDate.addMonths(2);
        
        Set<ID> set_UEventId = new Set<ID>();
        for(AssVisitComments__c AssVisitComm : [Select Id,EventId__c,IsAssVisit__c From AssVisitComments__c Where CreatedDate >=: startDate And CreatedDate <: endDate And ReUser__c =: UserId]){
            //Set_AssVisitId.add(AssVisitComm.EventId__c);
            //如果填写了点评，且接受了协访，是为有效协访
            if(set_EventId.contains(AssVisitComm.EventId__c) ){
                Set_AssVisitId.add(AssVisitComm.EventId__c);
            }else if(AssVisitComm.IsAssVisit__c){
                //勾选了 “是否接受协访” 按钮的点评,需要进一步判断协访的日期。
                set_UEventId.add(AssVisitComm.EventId__c);
            }
        }
        
        for(Event e: [Select Id,GAPlan__c,WhoId,GAExecuteResult__c,Key_Information__c From Event Where Id in: set_UEventId And RecordType.DeveloperName = 'V2_Event' And Done__c = true And StartDateTime >: CheckStartDateTime And StartDateTime <: CheckEndDateTime]){
            Set_AssVisitId.add(e.Id);
        }
        return Set_AssVisitId;
    }
    
    //KPI:主管月计划评语完成率
    //主管填写月计划评语的数量/下属人数
    public Map<String , Double> MonthlyPlanCommentRate(){
        Map<String , Double> map_Result = new Map<String , Double>();
        //填写数量
        Double HaveComment = 0;
        //总共数量
        Double CountComment = 0;
        //直接汇报关系的下属
        List<ID> list_uId = new List<ID>();
        //查询所有在查询月份直接汇报下属
        for(V2_RoleHistory__c RoleHistory : [Select v.Role__c, v.Manager__c, v.Renal_valid_super__c, v.Name__c, v.IsOnHoliday__c, v.IsLeave__c, v.Department__c, v.PG__c, v.Level__c
            From V2_RoleHistory__c v 
            Where Month__c =: String.valueOf(intMonth) And Year__c =: String.valueOf(intYear) And IsOnHoliday__c = false And IsLeave__c = false And Manager__c=:UserId]){
            list_uId.add(RoleHistory.Name__c);
        }
        //需要评语的月计划
        system.debug(IntMonth+'-'+IntYear+'-'+list_uId);
        for(MonthlyPlan__c monthlyPlan : [Select Id,supervisor_comment__c From MonthlyPlan__c Where OwnerId in: list_uId And Month__c =: String.valueOf(IntMonth) And Year__c =: String.valueOf(IntYear)]){
            if(monthlyPlan.supervisor_comment__c != null){
                HaveComment++;
            }
            CountComment++;
        }
        map_Result.put('Target' , CountComment);
        map_Result.put('Finish' , HaveComment);
        if(CountComment == 0){
            map_Result.put('Rate' , 0);
        }else{
            map_Result.put('Rate' , HaveComment / CountComment);
        }
        return map_Result;
    }
    /*
    //查询当前用户所有直接汇报下属的角色ID
    private List<ID> getRoleId(){
        //获取直接下属ID
        List<ID> list_uid = new List<ID>();
        User objU = [Select Id,UserRoleId From User Where Id =: UserId];
        for(User objuser : [Select Id From User Where UserRole.ParentRoleId =: objU.UserRoleId And IsActive = true]){
            list_uid.add(objuser.Id);
        } 
        return list_uid;
    }
    */
    
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        //user role
        List<UserRole> listur= new List<UserRole>();
        UserRole ur1 = new UserRole();
        ur1.Name = 'Renal-Supervisor-华南-PD-Supervisor(U1)';
        listur.add(ur1);
        UserRole ur2 = new UserRole();
        ur2.Name = 'Renal-Rep-华南-PD-Rep(U2)';
        ur2.ParentRoleId = ur1.Id;
        listur.add(ur2);
        insert listur;
        
        //user 
        list<User> user=[Select TimeZoneSidKey,ProfileId,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname, Alias From User Where IsActive = true and id =:userInfo.getUserId()];
        List<User> list_user = new List<User>();
        User user1=new User();
        user1.username='user1r2@123.com';
        user1.LastName='user1r2';
        user1.Email='user1r2@123.com';
        user1.Alias=user[0].Alias;
        user1.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user1.ProfileId=UserInfo.getProfileId();
        user1.LocaleSidKey=user[0].LocaleSidKey;
        user1.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user1.EmailEncodingKey=user[0].EmailEncodingKey;
        user1.CommunityNickname='chequ';
        user1.MobilePhone='12345678912';
        user1.IsActive = true;
        user1.UserRoleId=ur1.Id ;
        list_user.add(user1);
        User user2=new User();
        user2.username='user2r2@223.com';
        user2.LastName='user2r2';
        user2.Email='user2r2@223.com';
        user2.Alias=user[0].Alias;
        user2.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user2.ProfileId=UserInfo.getProfileId();
        user2.LocaleSidKey=user[0].LocaleSidKey;
        user2.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user2.EmailEncodingKey=user[0].EmailEncodingKey;
        user2.CommunityNickname='chequ1';
        user2.MobilePhone='22345678922';
        user2.IsActive = true;
        user1.UserRoleId=ur2.Id ;
        list_user.add(user2);
        insert list_user;
        
        //联系人
        List<Contact> listct= new List<Contact>();
        Contact ct1 = new Contact();
        ct1.FirstName = 'jim';
        ct1.LastName = 'Green';
        ct1.GISContactType__c ='关键客户';
        listct.add(ct1);
        Contact ct2 = new Contact();
        ct2.FirstName = 'Tom';
        ct2.LastName = 'Frank';
        ct2.GISContactType__c ='普通客户';
        listct.add(ct2);
        insert listct;
        
        //业务机会
        List<Opportunity> List_Opportunity = new List<Opportunity>();
        Opportunity opp = new Opportunity();
        opp.OwnerId = user1.Id;
        opp.Name = 'yewujihui';
        opp.StageName = '建立沟通渠道';
        opp.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='RENAL' And SobjectType = 'Opportunity'][0].Id;
        opp.CloseDate = date.valueOf(Datetime.now().year()+'-'+Datetime.now().month()+'-1');
        opp.Renal_Opp_MGT__c = '接受';
        List_Opportunity.add(opp);
        Opportunity opp1 = new Opportunity();
        opp1.OwnerId = user1.Id;
        opp1.Name = 'yewujihui1';
        opp1.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='RENAL' And SobjectType = 'Opportunity'][0].Id;
        opp1.StageName = '建立沟通渠道';
        opp1.CloseDate = date.valueOf(Datetime.now().year()+'-'+Datetime.now().month()+'-1');
        opp1.Renal_Opp_MGT__c = '接受';
        List_Opportunity.add(opp1);
        Opportunity opp2 = new Opportunity();
        opp2.OwnerId = user1.Id;
        opp2.Name = 'yewujihui1';
        opp2.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='RENAL' And SobjectType = 'Opportunity'][0].Id;
        opp2.StageName = '建立沟通渠道';
        opp2.CloseDate = date.valueOf(Datetime.now().year()+'-'+Datetime.now().month()+'-1');
        opp2.Renal_Opp_MGT__c = '接受';
        List_Opportunity.add(opp2);
        insert List_Opportunity;
        
        //业务机会历史记录
        List<OpportunityHistory__c> List_OpportunityHistory = new List<OpportunityHistory__c>();
        OpportunityHistory__c oppc = new OpportunityHistory__c();
        oppc.ChangedDate__c = date.valueOf(Datetime.now().year()+'-'+Datetime.now().month()+'-2');
        oppc.NewOppStage__c ='需求分析';
        oppc.PastOppStage__c = '提交合作方案/谈判';
        oppc.Opportunity__c = opp.Id;
        List_OpportunityHistory.add(oppc);
        insert List_OpportunityHistory;
        
        //活动
        List<Event> list_Visit = new List<Event>();
        Event ev1 = new Event();
        ev1.DurationInMinutes = 1;
        ev1.StartDateTime = datetime.now();
        ev1.OwnerId = user1.Id ;
        ev1.GAPlan__c = '1111111';
        ev1.GAExecuteResult__c = '111111';
        ev1.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='V2_Event' And SobjectType = 'Event'][0].Id;
        ev1.Done__c = true;
        ev1.V2_IsExpire__c = false; 
        ev1.WhoId = ct1.Id;
        ev1.WhatId = opp.Id;
        list_Visit.add(ev1);
        Event ev2 = new Event();
        ev2.DurationInMinutes = 2;
        ev2.StartDateTime = datetime.now();
        ev2.OwnerId = user1.Id ;
        ev2.GAPlan__c = '222222';
        ev2.GAExecuteResult__c = '222222';
        ev2.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='V2_Event' And SobjectType = 'Event'][0].Id;
        ev2.Done__c = true;
        ev2.V2_IsExpire__c = false; 
        ev2.WhoId = ct1.Id;
        ev2.WhatId = opp.Id;
        list_Visit.add(ev2);
        Event ev3 = new Event();
        ev3.DurationInMinutes = 3;
        ev3.StartDateTime = datetime.now();
        ev3.OwnerId = user1.Id ;
        ev3.GAPlan__c = '333333';
        ev3.GAExecuteResult__c = '3333333';
        ev3.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='V2_Event' And SobjectType = 'Event'][0].Id;
        ev3.Done__c = true;
        ev3.V2_IsExpire__c = false; 
        ev3.WhoId = ct1.Id;
        ev3.WhatId = opp.Id;
        list_Visit.add(ev3);
        Event ev4 = new Event();
        ev4.DurationInMinutes = 4;
        ev4.StartDateTime = datetime.now();
        ev4.OwnerId = user1.Id ;
        ev4.GAPlan__c = '333333';
        //ev4.GAExecuteResult__c = '3333333';
        ev4.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='V2_Event' And SobjectType = 'Event'][0].Id;
        ev4.Done__c = true;
        ev4.V2_IsExpire__c = false; 
        ev4.WhoId = ct2.Id;
        ev4.WhatId = opp.Id;
        list_Visit.add(ev4);
        Event ev5 = new Event();
        ev5.DurationInMinutes = 1;
        ev5.StartDateTime = datetime.now();
        ev5.OwnerId = user1.Id ;
        ev5.GAPlan__c = '1111111';
        ev5.GAExecuteResult__c = '111111';
        ev5.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='V2_RecordType' And SobjectType = 'Event'][0].Id;
        ev5.Done__c = true;
        ev5.V2_IsExpire__c = false; 
        ev5.WhoId = ct1.Id;
        ev5.WhatId = opp2.Id;
        list_Visit.add(ev5);
        Event ev6 = new Event();
        ev6.DurationInMinutes = 2;
        ev6.StartDateTime = datetime.now();
        ev6.OwnerId = user1.Id ;
        ev6.GAPlan__c = '222222';
        ev6.GAExecuteResult__c = '222222';
        ev6.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='V2_RecordType' And SobjectType = 'Event'][0].Id;
        ev6.Done__c = true;
        ev6.V2_IsExpire__c = false; 
        ev6.WhoId = ct1.Id;
        ev6.WhatId = opp2.Id;
        list_Visit.add(ev6);
        Event ev7 = new Event();
        ev7.DurationInMinutes = 3;
        ev7.StartDateTime = datetime.now();
        ev7.OwnerId = user1.Id ;
        ev7.GAPlan__c = '333333';
        ev7.GAExecuteResult__c = '3333333';
        ev7.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='V2_RecordType' And SobjectType = 'Event'][0].Id;
        ev7.Done__c = true;
        ev7.V2_IsExpire__c = false; 
        ev7.WhoId = ct1.Id;
        ev7.WhatId = opp2.Id;
        list_Visit.add(ev7);
        Event ev8 = new Event();
        ev8.DurationInMinutes = 4;
        ev8.StartDateTime = datetime.now();
        ev8.OwnerId = user1.Id ;
        ev8.GAPlan__c = '333333';
        //ev8.GAExecuteResult__c = '3333333';
        ev8.RecordTypeId = [Select r.Id From RecordType r where r.DeveloperName ='V2_RecordType' And SobjectType = 'Event'][0].Id;
        ev8.Done__c = true;
        ev4.V2_IsExpire__c = false; 
        ev8.WhoId = ct2.Id;
        ev8.WhatId = opp2.Id;
        list_Visit.add(ev8);
        insert list_Visit ;
        
        //协防事件
        //List<EventAttendee> list_EventAttendee = new List<EventAttendee>();
        //EventAttendee eva = new EventAttendee();
        //eva.EventId = ev1.Id;
        //eva.AttendeeId = user1.Id;
        //eva.Status = '已接受';
        //list_EventAttendee.add(eva);
        //insert list_EventAttendee;
        
        //月计划
        List<MonthlyPlan__c> List_MonthlyPlan = new List<MonthlyPlan__c>();
        MonthlyPlan__c mc1 = new MonthlyPlan__c();
        mc1.OwnerId = user1.Id;
        mc1.Year__c = String.valueOf(datetime.now().year());
        mc1.Month__c = String.valueOf(datetime.now().month());
        mc1.supervisor_comment__c = '1111111';
        List_MonthlyPlan.add(mc1);
        MonthlyPlan__c mc2 = new MonthlyPlan__c();
        mc2.OwnerId = user2.Id;
        mc2.Year__c = String.valueOf(datetime.now().year());
        mc2.Month__c = String.valueOf(datetime.now().month());
        mc2.supervisor_comment__c = '1111111';
        List_MonthlyPlan.add(mc2);
        MonthlyPlan__c mc3 = new MonthlyPlan__c();
        mc3.OwnerId = user2.Id;
        mc3.Year__c = String.valueOf(datetime.now().year());
        mc3.Month__c = String.valueOf(datetime.now().month());
        List_MonthlyPlan.add(mc3);
        insert List_MonthlyPlan;
        
        //月计划明细
        List<MonthlyPlanDetail__c> List_MonthlyPlanDetail = new List<MonthlyPlanDetail__c>();
        MonthlyPlanDetail__c mcc1 = new MonthlyPlanDetail__c();
        mcc1.Contact__c = ct1.Id;
        mcc1.Planned_Finished_Calls__c = 5;
        mcc1.MonthlyPlan__c = mc1.id;
        mcc1.AdjustedTimes__c = 10;
        List_MonthlyPlanDetail.add(mcc1);
        MonthlyPlanDetail__c mcc2 = new MonthlyPlanDetail__c();
        mcc2.Contact__c = ct1.Id;
        mcc2.Planned_Finished_Calls__c = 5;
        mcc2.MonthlyPlan__c = mc1.Id;
        mcc2.AdjustedTimes__c = 10;
        List_MonthlyPlanDetail.add(mcc2);
        MonthlyPlanDetail__c mcc3 = new MonthlyPlanDetail__c();
        mcc3.Contact__c = ct1.Id;
        mcc3.Planned_Finished_Calls__c = 10;
        mcc3.MonthlyPlan__c = mc1.Id;
        mcc3.AdjustedTimes__c = 10;
        List_MonthlyPlanDetail.add(mcc3);
        insert List_MonthlyPlanDetail;
        
        system.test.startTest();
        ClsHdCrrtKpiService cls = new ClsHdCrrtKpiService(user1.Id , datetime.now().year() , datetime.now().month());
        Map<String , Double> a = cls.ActualVisitContactNum();
        Map<String , Double> b = cls.VisitRate();
        Map<String , Double> c = cls.VisitPlanRate();
        Map<String , Double> d = cls.OpportunityQuantity();
        Map<String , Double> e = cls.OpportunityEventRate();
        Map<String , Double> f = cls.OpportunityVisitRate();
        cls.OpportunityQuantityACUTE();
        cls.OpportunityVisitRateACUTE();
        //HD/CRRT BU 主管KPI
        //eventAttendee无法写入数据
        cls.AssVisitQuantity();
        //sep
        //cls.SepTeamPerformanceAVG();
        Map<String , Double> g = cls.MonthlyPlanCommentRate();
        system.debug('@@'+a+'@@'+b+'@@'+c+'@@'+d+'@@'+e+'@@'+f+'@@'+g);
        system.test.stopTest();
     }
    
    
}