/**
 * Test Trigger OpportunitySetFocus
 */
@isTest
private class Test_OpportunitySetFocus {

    static testMethod void myUnitTest() {
        //user role
        List<UserRole> listur= new List<UserRole>();
        UserRole ur1 = new UserRole();
        ur1.Name = 'Renal-Supervisor-华南-PD-Supervisor(U1)';
        listur.add(ur1);
        UserRole ur2 = new UserRole();
        ur2.Name = 'Renal-Rep-华南-PD-Rep(U2)';
        listur.add(ur2);
        insert listur;
        //user 
        list<User> user=[Select TimeZoneSidKey,ProfileId,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname, Alias From User Where IsActive = true limit 1];
        List<User> list_user = new List<User>();
        User user1=new User();
        user1.username='user1r2@123.com';
        user1.LastName='user1r2';
        user1.Email='user1r2@123.com';
        user1.Alias=user[0].Alias;
        user1.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user1.ProfileId=UserInfo.getProfileId();
        user1.LocaleSidKey=user[0].LocaleSidKey;
        user1.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user1.EmailEncodingKey=user[0].EmailEncodingKey;
        user1.CommunityNickname='chequ';
        user1.MobilePhone='12345678912';
        user1.IsActive = true;
        user1.UserRoleId=ur1.Id ;
        list_user.add(user1);
        User user2=new User();
        user2.username='user2r2@223.com';
        user2.LastName='user2r2';
        user2.Email='user2r2@223.com';
        user2.Alias=user[0].Alias;
        user2.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user2.ProfileId=UserInfo.getProfileId();
        user2.LocaleSidKey=user[0].LocaleSidKey;
        user2.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user2.EmailEncodingKey=user[0].EmailEncodingKey;
        user2.CommunityNickname='chequ1';
        user2.MobilePhone='22345678922';
        user2.IsActive = true;
        user2.UserRoleId=ur2.Id ;
        list_user.add(user2);
        insert list_user;
        
        Account objAcc = new Account();
        objAcc.Name = 'test acc ' ;
        insert objAcc ;
        
        
        
        system.test.startTest();
        system.runAs(user1){
            Opportunity opp1 = new Opportunity();
            //opp1.AccountId = objAcc.Id;
            opp1.Name = 'ste test opp1';
            opp1.StageName = '发现/验证机会' ;
            opp1.CloseDate = date.today().addYears(1);
            opp1.IsFocusOpportunity__c = '是';
            insert opp1 ;
        }
        system.runAs(user2){
            Opportunity opp2 = new Opportunity();
            //opp1.AccountId = objAcc.Id;
            opp2.Name = 'ste test opp1';
            opp2.StageName = '发现/验证机会' ;
            opp2.CloseDate = date.today().addYears(1);
            opp2.IsFocusOpportunity__c = '是';
            insert opp2 ;
            
            opp2.OwnerId = user1.Id;
            update opp2;
        }
        system.test.stopTest();
    }
}