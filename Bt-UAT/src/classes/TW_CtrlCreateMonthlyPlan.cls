/*
Author：Scott
Created on：2013-10-17
Description: 
复用大陆功能
创建月计划默认当前年月，创建时若该月份的月计划已经存在，则跳到指定月计划上 
1.  读取上个月的所有行动类型为“拜访”的事件，以联系人分组，同一联系人拜访次数为计划次数。
2.  读取当前月已有的所有行动类型为“拜访”的事件，以联系人分组同上月拜访匹配，如果没有新建一条计划次数为空的明细。
3.  读取指定月份的所有行动类型为“拜访”的事件，若列表不为空，则根据同步规则更新“安排次数”和“完成次数”(未过期)。
2012-2-9修改
事件是新加记录类型
*/
public without sharing class TW_CtrlCreateMonthlyPlan {
	public String Year{get;set;}
    public String Month{get;set;}
    public List<SelectOption> ListYears{get;set;}
    List<MonthlyPlanDetail__c> ListMpd = new List<MonthlyPlanDetail__c>();
    //上月事件
	Map<Id,Integer> PreviousEventMap = new Map<Id,Integer>();
	//上月联系人Id和客户Id
	Map<Id,Id> PreviousWhoIdsMap = new Map<Id,Id>();
	
	//拜访联系人及其拜访次数
	Map<Id,Integer> CurrentEventMap = new Map<Id,Integer>();
	//联系人客户
	Map<Id,Id> CurrentWhoidsMap = new Map<Id,Id>();
	//联系人及其拜访完成数
	Map<Id,Integer> CurrentDoneMap = new Map<Id,Integer>();
	
	/*负责医院数------2013-10-28添加*/
	Decimal TW_OwnAcc = 0;
	/*负责医师数------2013-10-28添加*/
	Decimal TW_OwnContact = 0;
	
    public List<SelectOption> getListMonths()
    {
        list<SelectOption> options=new list<SelectOption>();
        options.add(new SelectOption('1','1'));options.add(new SelectOption('2','2'));
        options.add(new SelectOption('3','3'));options.add(new SelectOption('4','4'));
        options.add(new SelectOption('5','5'));options.add(new SelectOption('6','6'));
        options.add(new SelectOption('7','7'));options.add(new SelectOption('8','8'));
        options.add(new SelectOption('9','9'));options.add(new SelectOption('10','10'));
        options.add(new SelectOption('11','11'));options.add(new SelectOption('12','12'));
        return options;
    }
     public TW_CtrlCreateMonthlyPlan()
    {       
        ListYears = new  List<SelectOption>();
        ListYears.add(new SelectOption(String.valueOf(date.today().addYears(-1).year()),String.valueOf(date.today().addYears(-1).year())));
        ListYears.add(new SelectOption(String.valueOf(date.today().year()),String.valueOf(date.today().year())));
        ListYears.add(new SelectOption(String.valueOf(date.today().addYears(1).year()),String.valueOf(date.today().addYears(1).year())));
        
        Year = String.valueOf(date.today().year());
        Month = String.valueOf(date.today().Month());
        
        /*负责医院数------2013-10-28添加*/
        List<Account> List_Acc = [select Id from Account where OwnerId =:UserInfo.getUserId() and TW_AccStatus__c='有效'];
        if(List_Acc != null && List_Acc.size()>0)
        {
        	TW_OwnAcc = List_Acc.size();
        }
        
        /*负责医师数------2013-10-28添加*/
        List<Contact> List_Contact = [select Id from Contact where OwnerId =: UserInfo.getUserId() and TW_DoctorStatus__c='有效'];
        if(List_Contact != null && List_Contact.size()>0)
        {
        	TW_OwnContact = List_Contact.size();
        }
 
    }
    //判断当前月是否有月计划，如果有直接跳转到月计划，如果没有新建月计划
    public PageReference CreateMonthPlay()
    {
        List<MonthlyPlan__c> Listmp = [select Id from MonthlyPlan__c where Year__c =:Year and Month__c =: Month and OwnerId =: UserInfo.getUserId()];
        if(Listmp != null && Listmp.size()>0)
        {
            PageReference pageRef = new PageReference(System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+Listmp[0].Id);
            return pageRef;
           // ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR , '当前月已经存在月计划！');            
            //ApexPages.addMessage(msg);
            //return null;
        }
        else
        {
            MonthlyPlan__c Mp = new MonthlyPlan__c();
            try
            {
                Mp.TW_ApprovalStatus__c = '未提交';
                Mp.Year__c = Year;
                Mp.Month__c = Month;
                Mp.V2_ExternalID__c = Year+Month+UserInfo.getUserId();
                /*负责医院数------2013-10-28添加*/
                Mp.TW_OwnAcc__c = TW_OwnAcc;
                /*负责医师数------2013-10-28添加*/
                Mp.TW_OwnContact__c = TW_OwnContact;
                upsert Mp V2_ExternalID__c;
                
                Date CurrentDate = Date.newInstance(Integer.valueOf(Year),Integer.valueOf(Month),1);
                //上月时间
                DateTime PreviousStartMonth = DateTime.newInstance(CurrentDate.toStartOfMonth().addMonths(-1),Time.newInstance(00, 00, 00, 00));
                DateTime PreviousEndMonth =  DateTime.newInstance(CurrentDate.toStartOfMonth(),Time.newInstance(00, 00, 00, 00));
                //本月时间
                DateTime CurrentStartMonth = DateTime.newInstance(CurrentDate.toStartOfMonth(),Time.newInstance(00, 00, 00, 00));
                DateTime CurrentEndMonth = DateTime.newInstance(CurrentDate.toStartOfMonth().addMonths(1),Time.newInstance(00, 00, 00, 00));
               	//本月事件
               	CopyPreviousEvent(PreviousStartMonth,PreviousEndMonth);
               	//当前月事件
               	CurrentEvent(CurrentStartMonth,CurrentEndMonth);
       			//判断上月是否有拜访
       			if(PreviousEventMap != null && PreviousEventMap.size()>0)
       			{
       				//本月是否有拜访
       				if(CurrentEventMap != null && CurrentEventMap.size()>0)
       				{
       					//遍历上月本月跟其对应如果本月没有对应则基于上月新建一条
       					for(Id Previouswhoid:PreviousEventMap.keySet())
	       				{
	       					Boolean flag = true;//标记上月是否有  本月未对应的拜访
	       					for(Id CurrentWhoid:CurrentEventMap.keySet())
	       					{
	       						if(CurrentWhoid != Previouswhoid)
	       						{
	       							continue;
	       						}
	       						flag = false;
	       						MonthlyPlanDetail__c mpd = new MonthlyPlanDetail__c();
	       						mpd.MonthlyPlan__c = Mp.Id;
	       						mpd.Contact__c = Previouswhoid;
	       						mpd.Account__c = PreviousWhoIdsMap.get(Previouswhoid);
	       						//计划次数
	       						mpd.AdjustedTimes__c = PreviousEventMap.get(Previouswhoid);
	       						//安排次数
	       						mpd.ArrangedTimes__c = CurrentEventMap.get(CurrentWhoid);
	       						//完成数
	       						if(CurrentDoneMap.containsKey(CurrentWhoid))
				    			{
				    				mpd.Planned_Finished_Calls__c = CurrentDoneMap.get(CurrentWhoid);
				    			}
				    			else
				    			{
				    				mpd.Planned_Finished_Calls__c = 0; 
				    			}
				    			ListMpd.add(mpd);
	       					}
	       					if(flag)
	       					{
	       						MonthlyPlanDetail__c mpd = new MonthlyPlanDetail__c();
	       						mpd.MonthlyPlan__c = Mp.Id;
	       						mpd.Contact__c = Previouswhoid;
	       						mpd.Account__c = PreviousWhoIdsMap.get(Previouswhoid);
	       						//计划次数
	       						mpd.AdjustedTimes__c = PreviousEventMap.get(Previouswhoid);
	       						//安排次数
	       						mpd.ArrangedTimes__c = 0;
	       						//完成数
				    			mpd.Planned_Finished_Calls__c =0;
				    			ListMpd.add(mpd);
	       					}
	       				}
	       				//遍历本月判断上月是否有于本月对应的拜访如果没有新建一条本月记录
	       				for(Id Currentwhoids:CurrentEventMap.keySet())
	       				{
	       					Boolean flag = true;//标记本月是否含有上月没有的拜访
	       					for(Id Previouswhoid:PreviousEventMap.keySet())
	       					{
	       						if(Previouswhoid != Currentwhoids)
	       						{
	       							continue;
	       						}
	       						flag = false;
	       					}
	       					if(flag)
	       					{
	       						MonthlyPlanDetail__c mpd = new MonthlyPlanDetail__c();
	       						mpd.MonthlyPlan__c = Mp.Id;
				    			mpd.Contact__c = Currentwhoids;
				    			mpd.Account__c = CurrentWhoidsMap.get(Currentwhoids);
				    			//计划次数
				    			mpd.AdjustedTimes__c = 0;
				    			//安排次数
				    			mpd.ArrangedTimes__c = CurrentEventMap.get(Currentwhoids);
				    			//完成数
				    			if(CurrentDoneMap.containsKey(Currentwhoids))
				    			{
				    				mpd.Planned_Finished_Calls__c = CurrentDoneMap.get(Currentwhoids);
				    			}
				    			else
				    			{
				    				mpd.Planned_Finished_Calls__c = 0;
				    			}
				    			ListMpd.add(mpd);
	       					}
	       				}
       				}
       				else
       				{
       					for(Id Previouswhoid:PreviousEventMap.keySet())
       					{
       						MonthlyPlanDetail__c mpd = new MonthlyPlanDetail__c();
       						mpd.MonthlyPlan__c = Mp.Id;
       						mpd.Contact__c = Previouswhoid;
       						mpd.Account__c = PreviousWhoIdsMap.get(Previouswhoid);
       						//计划次数
       						mpd.AdjustedTimes__c = PreviousEventMap.get(Previouswhoid);
       						//安排次数
       						mpd.ArrangedTimes__c = 0;
       						//完成数
			    			mpd.Planned_Finished_Calls__c =0;
			    			ListMpd.add(mpd);
       					}
       				}
       				
       			}
       			else if(CurrentEventMap != null && CurrentEventMap.size()>0)
       			{
       				for(Id whoids:CurrentEventMap.keySet())
       				{
       					MonthlyPlanDetail__c mpd = new MonthlyPlanDetail__c();
       					mpd.MonthlyPlan__c = Mp.Id;
		    			mpd.Contact__c = whoids;
		    			mpd.Account__c = CurrentWhoidsMap.get(whoids);
		    			mpd.AdjustedTimes__c = 0;
		    			mpd.ArrangedTimes__c = CurrentEventMap.get(whoids);
		    			if(CurrentDoneMap.containsKey(whoids))
		    			{
		    				mpd.Planned_Finished_Calls__c = CurrentDoneMap.get(whoids);
		    			}
		    			else
		    			{
		    				mpd.Planned_Finished_Calls__c = 0;
		    			}
		    			ListMpd.add(mpd);
       				}
       			}
       			insert ListMpd;
                PageReference pageRef = new PageReference(System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+Mp.Id);
           		return pageRef;
            }catch(Exception e)
            { 
               if(e.getmessage().contains('DUPLICATE_VALUE'))
                {
                	System.debug('#########外键DUPLICATE_VALUE');
                	List<MonthlyPlan__c> Listmp2 = [select Id from MonthlyPlan__c where Year__c =:Year and Month__c =: Month and OwnerId =: UserInfo.getUserId()];
			        if(Listmp2 != null && Listmp2.size()>0)
			        {
			            PageReference pageRef = new PageReference(System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+Listmp2[0].Id);
			            return pageRef;
			        }
			        else
			        {
			        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR , '當前月已經存在月計劃！');            
			            ApexPages.addMessage(msg);
			            return null;
			        }
                }
                else
                {
                	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING , e.getMessage());            
               		ApexPages.addMessage(msg);
                	return null;
                }
                
            } 
        }
    }
    //copy上月的event
    /*2012-3-14 修改：因为事件上 WhoId 和 AccountId 并不一定 一一对应*/
    public void CopyPreviousEvent(DateTime startdays,DateTime enddays)
    {
    	Set<Id> contactIds = new Set<Id>();
    	for(Event ev:[select WhoId,AccountId  from Event
					  where StartDateTime>:startdays and StartDateTime <=:enddays and (SubjectType__c ='拜访' or SubjectType__c ='拜訪')
                	  and AccountId != null and IsRecurrence != true and OwnerId =: UserInfo.getUserId() and RecordType.DeveloperName = 'V2_Event' and WhoId != null])
    	{
    		//PreviousWhoIdsMap.put(ev.WhoId,ev.AccountId); 
    		contactIds.add(ev.WhoId);
    		if(PreviousEventMap.containsKey(ev.WhoId))
    		{
    			PreviousEventMap.put(ev.WhoId,(PreviousEventMap.get(ev.WhoId)+1));
    		}
    		else
    		{
    			PreviousEventMap.put(ev.WhoId,1);
    		}
    	}
    	
    	for(Contact con:[select Id,AccountId from Contact where Id in:contactIds and AccountId != null])
    	{
    		PreviousWhoIdsMap.put(con.Id,con.AccountId); 
    	}
    }
    //本月事件
    /*2012-3-14 修改：因为事件上 WhoId 和 AccountId 并不一定 一一对应*/
    public void CurrentEvent(DateTime startdays,DateTime enddays) 
    {
    	Set<Id> contactIds = new Set<Id>();
    	for(Event ev:[select WhoId,AccountId,Done__c,V2_IsExpire__c from Event 
			   	        where StartDateTime>:startdays and StartDateTime <=:enddays and (SubjectType__c ='拜访' or SubjectType__c ='拜訪')
			            and AccountId != null and IsRecurrence != true and OwnerId =: UserInfo.getUserId() and RecordType.DeveloperName = 'V2_Event' and WhoId != null])
    	{
    		if(CurrentEventMap.containsKey(ev.WhoId))
    		{
    			CurrentEventMap.put(ev.WhoId,(CurrentEventMap.get(ev.WhoId)+1));
    		}
    		else
    		{
    			CurrentEventMap.put(ev.WhoId,1);
    		}
    		if(ev.Done__c && ev.V2_IsExpire__c != true)
    		{
    			if(CurrentDoneMap.containsKey(ev.WhoId))
    			{
    				CurrentDoneMap.put(ev.WhoId,(CurrentEventMap.get(ev.WhoId)+1));
    			}
    			else
    			{
    				CurrentDoneMap.put(ev.WhoId,1);
    			}
    		}
    		//CurrentWhoidsMap.put(ev.WhoId,ev.AccountId);
    		contactIds.add(ev.WhoId);
    	}
    	
    	for(Contact con:[select Id,AccountId from Contact where Id in:contactIds and AccountId != null])
    	{
    		CurrentWhoidsMap.put(con.Id,con.AccountId);
    	}
    	
    }
    /****************************************************Test*********************************/
    static testMethod void TW_CtrlCreateMonthlyPlan()
	{
		/*客户*/
		RecordType accrecordtype = [select Id from RecordType where Name='医院' and SobjectType='Account' limit 1];
		Account acc = new Account();
		acc.RecordTypeId = accrecordtype.Id;
		acc.Name = 'AccTest';
		insert acc;
		/*联系人*/
		RecordType conrecordtype = [select Id from RecordType where Name = 'Renal' and SobjectType='Contact' and IsActive = true limit 1 ];
		Contact con1 = new Contact();
		con1.LastName = 'AccTestContact1';
		con1.AccountId=acc.Id;
		insert con1;
		Contact con2 = new Contact();
		con2.LastName = 'AccTestContact2';
		con2.AccountId=acc.Id;
		insert con2;
		Contact con3 = new Contact();
		con3.LastName = 'AccTestContact2';
		con3.AccountId=acc.Id;
		insert con3;
		/*拜访事件*/
		RecordType callRt = [Select Id from RecordType where DeveloperName = 'V2_Event' Limit 1];
		
		Event CallEv = new Event();
		CallEv.RecordTypeId = callRt.Id;
		CallEv.WhoId = con1.Id;
		CallEv.StartDateTime = datetime.now().addYears(-10);
		CallEv.EndDateTime = datetime.now().addYears(-10).addMinutes(1);
		CallEv.SubjectType__c = '拜访';
		insert CallEv;
		
		Event CallEv2 = new Event();
		CallEv2.RecordTypeId = callRt.Id;
		CallEv2.WhoId = con1.Id;
		CallEv2.StartDateTime = datetime.now().addYears(-10);
		CallEv2.EndDateTime = datetime.now().addYears(-10).addMinutes(1);
		CallEv2.Done__c = true;
		CallEv2.SubjectType__c = '拜访';
		insert CallEv2;
		Event CallEv3 = new Event();
		CallEv3.RecordTypeId = callRt.Id;
		CallEv3.WhoId = con2.Id;
		CallEv3.StartDateTime = datetime.now().addYears(-10);
		CallEv3.EndDateTime = datetime.now().addYears(-10).addMinutes(1);
		CallEv3.V2_FollowEventFlag__c = true;
		CallEv3.SubjectType__c = '拜访';
		insert CallEv3;
		Event CallEv4 = new Event();
		CallEv4.RecordTypeId = callRt.Id;
		CallEv4.WhoId = con1.Id;
		CallEv4.StartDateTime = datetime.now().addYears(-10).addMonths(-1);
		CallEv4.EndDateTime = datetime.now().addYears(-10).addMinutes(1).addMonths(-1);
		CallEv4.V2_FollowEventFlag__c = true;
		CallEv4.SubjectType__c = '拜访';
		insert CallEv4;
		Event CallEv5 = new Event();
		CallEv5.RecordTypeId = callRt.Id;
		CallEv5.WhoId = con3.Id;
		CallEv5.StartDateTime = datetime.now().addYears(-10).addMonths(-1);
		CallEv5.EndDateTime = datetime.now().addYears(-10).addMinutes(1).addMonths(-1);
		CallEv5.V2_FollowEventFlag__c = true;
		CallEv5.SubjectType__c = '拜访';
		insert CallEv5;
		
		
		Test.startTest();
		
		TW_CtrlCreateMonthlyPlan ccmp = new TW_CtrlCreateMonthlyPlan();
		ccmp.getListMonths();
		ccmp.Year = String.ValueOf(date.today().addYears(-10).Year());
		ccmp.Month = String.valueOf(date.today().month());
		ccmp.CreateMonthPlay();
		
		TW_CtrlCreateMonthlyPlan ccmp2 = new TW_CtrlCreateMonthlyPlan();
		ccmp2.getListMonths();
		ccmp2.Year = String.ValueOf(date.today().addYears(-10).Year());
		ccmp2.Month = String.valueOf(date.today().month());
		ccmp2.CreateMonthPlay();
		
		//本月无
		Event CallEv51 = new Event();
		CallEv51.RecordTypeId = callRt.Id;
		CallEv51.WhoId = con3.Id;
		CallEv51.StartDateTime = datetime.now().addYears(-10);
		CallEv51.EndDateTime = datetime.now().addYears(-10).addMinutes(1);
		CallEv51.V2_FollowEventFlag__c = true;
		CallEv51.SubjectType__c = '拜访';
		insert CallEv51;
		
		TW_CtrlCreateMonthlyPlan ccmp3 = new TW_CtrlCreateMonthlyPlan();
		ccmp3.getListMonths();
		ccmp3.Year = String.ValueOf(date.today().addYears(-10).Year());
		ccmp3.Month = String.valueOf(date.today().addMonths(1).month());
		ccmp3.CreateMonthPlay();
		//上月无
		Event CallEv21 = new Event();
		CallEv21.RecordTypeId = callRt.Id;
		CallEv21.WhoId = con1.Id;
		CallEv21.StartDateTime = datetime.now().addYears(-10).addMonths(4);
		CallEv21.EndDateTime = datetime.now().addYears(-10).addMonths(4).addMinutes(1);
		CallEv21.Done__c = true;
		CallEv21.SubjectType__c = '拜访';
		insert CallEv21;
		TW_CtrlCreateMonthlyPlan ccmp4 = new TW_CtrlCreateMonthlyPlan();
		ccmp4.getListMonths();
		ccmp4.Year = String.ValueOf(date.today().addYears(-10).Year());
		ccmp4.Month = String.valueOf(date.today().addMonths(4).month());
		ccmp4.CreateMonthPlay();
		Test.stopTest();
	}
}