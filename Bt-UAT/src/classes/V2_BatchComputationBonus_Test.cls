@isTest
private class V2_BatchComputationBonus_Test {

    static testMethod void myUnitTest() {
      
       
       //----------------New Role ------------------
       	 //Bios
       	 UserRole biosRep_role = new UserRole();
       	 biosRep_role.Name = 'BIOS-Rep-北京-Albumin-Rep' ;
       	 insert biosRep_role;
       	 UserRole biosRep2_role = new UserRole();
       	 biosRep2_role.Name = 'BIOS-Rep-北京-Coseal-Rep' ;
       	 insert biosRep2_role;
       	 
       	 UserRole biosSup_role = new UserRole();
       	 biosSup_role.Name = 'BIOS-Supervisor-北京-Albumin-Rep' ;
       	 insert biosSup_role;
       	 UserRole biosSup2_role = new UserRole();
       	 biosSup2_role.Name = 'BIOS-Supervisor-北京-Coseal-Supervisor' ;
       	 insert biosSup2_role;
       	 
	    //Md
	     UserRole mdRep_role = new UserRole();
       	 mdRep_role.Name = 'MD-Rep-北京-IVT-Rep' ;
       	 insert mdRep_role;
       	 UserRole mdRep2_role = new UserRole();
       	 mdRep2_role.Name = 'MD-Rep-北京-IS-Rep' ;
       	 insert mdRep2_role;
       	 
       	 UserRole mdSup_role = new UserRole();
       	 mdSup_role.Name = 'MD-Supervisor-北京-IVT-Supervisor' ;
       	 insert mdSup_role;
       	 UserRole mdSup2_role = new UserRole();
       	 mdSup2_role.Name = 'MD-Supervisor-北京-IS-Supervisor' ;
       	 insert mdSup2_role;
       	 
       	 //Renal
       	 UserRole renalRep_role = new UserRole();
       	 renalRep_role.Name = 'Renal-Rep-北京-CRRT-Rep' ;
       	 insert renalRep_role;
       	 UserRole renalRep2_role = new UserRole();
       	 renalRep2_role.Name = 'Renal-Rep-北京-PD-Rep' ;
       	 insert renalRep2_role;
       	 
       	 UserRole renalSup_role = new UserRole();
       	 renalSup_role.Name = 'Renal-Supervisor-北京-CRRT-Supervisor' ;
       	 insert renalSup_role;
       	 UserRole renalSup2_role = new UserRole();
       	 renalSup2_role.Name = 'Renal-Supervisor-北京-PD-Supervisor' ;
       	 insert renalSup2_role;
	     
	    //Bios简档
	    Profile BiosRepPro = [select Id from Profile where Name  = 'Standard User - BIOS Sales Rep' limit 1];
	    Profile BiosSupPro = [select Id from Profile where Name  = 'Standard User - BIOS Sales Supervisor' limit 1];
	    //Md简档
	    Profile MdRepPro = [select Id from Profile where Name='Standard User - IVT Sales Rep' limit 1];
	    Profile MdSupPro = [select Id from Profile where Name='Standard User - IVT Sales Supervisor' limit 1];
	   //Renal简档
	    Profile RenalRepPro = [select Id from Profile where Name='Standard User - Renal Sales Rep' limit 1];
	    Profile RenalSupPro = [select Id from Profile where Name='Standard User - Renal Sales Supervisor' limit 1];
	    
	//----------------Create User-------------
	   //用户ids
	    String ids ;
	   
	     List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
         //Bios
	     User BiosRep1 = new User();
	     BiosRep1.Username='BiosRep1@123.com';
	     BiosRep1.LastName='BiosRep1';
	     BiosRep1.Email='BiosRep1@123.com';
	     BiosRep1.Alias=user[0].Alias;
	     BiosRep1.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     BiosRep1.ProfileId=BiosRepPro.Id;
	     BiosRep1.LocaleSidKey=user[0].LocaleSidKey;
	     BiosRep1.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     BiosRep1.EmailEncodingKey=user[0].EmailEncodingKey;
	     BiosRep1.CommunityNickname='BiosRep1';
	     BiosRep1.MobilePhone='12345678912';
	     BiosRep1.UserRoleId = biosRep_role.Id ;
	     BiosRep1.IsActive = true;
	     insert BiosRep1;
	     
	     ids=BiosRep1.Id+',';
	     
	     User BiosRep2 = new User();
	     BiosRep2.Username='BiosRep2@123.com';
	     BiosRep2.LastName='BiosRep2';
	     BiosRep2.Email='BiosRep2@123.com';
	     BiosRep2.Alias=user[0].Alias;
	     BiosRep2.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     BiosRep2.ProfileId=BiosRepPro.Id;
	     BiosRep2.LocaleSidKey=user[0].LocaleSidKey;
	     BiosRep2.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     BiosRep2.EmailEncodingKey=user[0].EmailEncodingKey;
	     BiosRep2.CommunityNickname='BiosRep2';
	     BiosRep2.MobilePhone='12345678912';
	     BiosRep2.UserRoleId = biosRep2_role.Id ;
	     BiosRep2.IsActive = true;
	     insert BiosRep2;
	     
	     ids+=BiosRep2.Id+',';
	     
	     User biosSup = new User();
	     biosSup.Username='biosSup@123.com';
	     biosSup.LastName='biosSup';
	     biosSup.Email='biosSup@123.com';
	     biosSup.Alias=user[0].Alias;
	     biosSup.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     biosSup.ProfileId=BiosSupPro.Id;
	     biosSup.LocaleSidKey=user[0].LocaleSidKey;
	     biosSup.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     biosSup.EmailEncodingKey=user[0].EmailEncodingKey;
	     biosSup.CommunityNickname='biosSup';
	     biosSup.MobilePhone='12345678912';
	     biosSup.UserRoleId = biosSup_role.Id ;
	     biosSup.IsActive = true;
	     insert biosSup;
	     
	     ids+=biosSup.Id+',';
	     
	     User biosSup2 = new User();
	     biosSup2.Username='biosSup2@123.com';
	     biosSup2.LastName='biosSup2';
	     biosSup2.Email='biosSup2@123.com';
	     biosSup2.Alias=user[0].Alias;
	     biosSup2.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     biosSup2.ProfileId=BiosSupPro.Id;
	     biosSup2.LocaleSidKey=user[0].LocaleSidKey;
	     biosSup2.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     biosSup2.EmailEncodingKey=user[0].EmailEncodingKey;
	     biosSup2.CommunityNickname='biosSup2';
	     biosSup2.MobilePhone='12345678912';
	     biosSup2.UserRoleId = biosSup2_role.Id ;
	     biosSup2.IsActive = true;
	     insert biosSup2;
	     
	     ids+=biosSup2.Id+',';
	     
	     //Md
	     User mdRep = new User();
	     mdRep.Username='mdRep@123.com';
	     mdRep.LastName='mdRep';
	     mdRep.Email='mdRep@123.com';
	     mdRep.Alias=user[0].Alias;
	     mdRep.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     mdRep.ProfileId=MdRepPro.Id;
	     mdRep.LocaleSidKey=user[0].LocaleSidKey;
	     mdRep.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     mdRep.EmailEncodingKey=user[0].EmailEncodingKey;
	     mdRep.CommunityNickname='mdRep';
	     mdRep.MobilePhone='12345678912';
	     mdRep.UserRoleId = mdRep_role.Id ;
	     mdRep.IsActive = true;
	     insert mdRep;
	     
	     ids+=mdRep.Id+',';
	     
	     User mdRep2 = new User();
	     mdRep2.Username='mdRep2@123.com';
	     mdRep2.LastName='mdRep2';
	     mdRep2.Email='mdRep2@123.com';
	     mdRep2.Alias=user[0].Alias;
	     mdRep2.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     mdRep2.ProfileId=MdRepPro.Id;
	     mdRep2.LocaleSidKey=user[0].LocaleSidKey;
	     mdRep2.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     mdRep2.EmailEncodingKey=user[0].EmailEncodingKey;
	     mdRep2.CommunityNickname='mdRep2';
	     mdRep2.MobilePhone='12345678912';
	     mdRep2.UserRoleId = mdRep2_role.Id ;
	     mdRep2.IsActive = true;
	     insert mdRep2;
	     
	      ids+=mdRep2.Id+',';
	     
	     User mdSup = new User();
	     mdSup.Username='mdSup@123.com';
	     mdSup.LastName='mdSup';
	     mdSup.Email='mdSup@123.com';
	     mdSup.Alias=user[0].Alias;
	     mdSup.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     mdSup.ProfileId=MdSupPro.Id;
	     mdSup.LocaleSidKey=user[0].LocaleSidKey;
	     mdSup.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     mdSup.EmailEncodingKey=user[0].EmailEncodingKey;
	     mdSup.CommunityNickname='mdSup';
	     mdSup.MobilePhone='12345678912';
	     mdSup.UserRoleId = mdSup_role.Id ;
	     mdSup.IsActive = true;
	     insert mdSup;
	     
	      ids+=mdSup.Id+',';
	     
	     User mdSup2 = new User();
	     mdSup2.Username='mdSup2@123.com';
	     mdSup2.LastName='mdSup2';
	     mdSup2.Email='mdSup2@123.com';
	     mdSup2.Alias=user[0].Alias;
	     mdSup2.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     mdSup2.ProfileId=MdSupPro.Id;
	     mdSup2.LocaleSidKey=user[0].LocaleSidKey;
	     mdSup2.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     mdSup2.EmailEncodingKey=user[0].EmailEncodingKey;
	     mdSup2.CommunityNickname='mdSup2';
	     mdSup2.MobilePhone='12345678912';
	     mdSup2.UserRoleId = mdSup2_role.Id ;
	     mdSup2.IsActive = true;
	     insert mdSup2;
	     
	      ids+=mdSup2.Id+',';
	     
         //Renal
         User renalRep = new User();
	     renalRep.Username='renalRep@123.com';
	     renalRep.LastName='renalRep';
	     renalRep.Email='renalRep@123.com';
	     renalRep.Alias=user[0].Alias;
	     renalRep.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     renalRep.ProfileId=RenalRepPro.Id;
	     renalRep.LocaleSidKey=user[0].LocaleSidKey;
	     renalRep.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     renalRep.EmailEncodingKey=user[0].EmailEncodingKey;
	     renalRep.CommunityNickname='renalRep';
	     renalRep.MobilePhone='12345678912';
	     renalRep.UserRoleId = renalRep_role.Id ;
	     renalRep.IsActive = true;
	     renalRep.Renal_valid_super__c  = false;
	     renalRep.Department = 'Renal-SalesRep';
	     insert renalRep;
	     
	     ids+=renalRep.Id+',';
	     
	     User renalRep2 = new User();
	     renalRep2.Username='renalRep2@123.com';
	     renalRep2.LastName='renalRep2';
	     renalRep2.Email='renalRep2@123.com';
	     renalRep2.Alias=user[0].Alias;
	     renalRep2.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     renalRep2.ProfileId=RenalRepPro.Id;
	     renalRep2.LocaleSidKey=user[0].LocaleSidKey;
	     renalRep2.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     renalRep2.EmailEncodingKey=user[0].EmailEncodingKey;
	     renalRep2.CommunityNickname='renalRep2';
	     renalRep2.MobilePhone='12345678912';
	     renalRep2.UserRoleId = renalRep2_role.Id ;
	     renalRep2.IsActive = true;
	     renalRep2.Renal_valid_super__c = false;
	     renalRep2.Department = 'Renal-SalesRep';
	     insert renalRep2;
	     
	     ids+=renalRep2.Id+',';
	     
	     User renalSup = new User();
	     renalSup.Username='renalSup@123.com';
	     renalSup.LastName='renalSup';
	     renalSup.Email='renalSup@123.com';
	     renalSup.Alias=user[0].Alias;
	     renalSup.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     renalSup.ProfileId=RenalSupPro.Id;
	     renalSup.LocaleSidKey=user[0].LocaleSidKey;
	     renalSup.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     renalSup.EmailEncodingKey=user[0].EmailEncodingKey;
	     renalSup.CommunityNickname='renalSup';
	     renalSup.MobilePhone='12345678912';
	     renalSup.UserRoleId = renalSup_role.Id ;
	     renalSup.IsActive = true;
	     renalSup.Renal_valid_super__c = false;
	     renalSup.Department = 'Renal-SalesSupervisor';
	     insert renalSup;
	     System.debug('#############################################################################');
	     ids+=renalSup.Id+',';
	     
	     User renalSup2 = new User();
	     renalSup2.Username='renalSup2@123.com';
	     renalSup2.LastName='renalSup2';
	     renalSup2.Email='renalSup2@123.com';
	     renalSup2.Alias=user[0].Alias;
	     renalSup2.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     renalSup2.ProfileId=RenalSupPro.Id;
	     renalSup2.LocaleSidKey=user[0].LocaleSidKey;
	     renalSup2.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     renalSup2.EmailEncodingKey=user[0].EmailEncodingKey;
	     renalSup2.CommunityNickname='renalSup2';
	     renalSup2.MobilePhone='12345678912';
	     renalSup2.UserRoleId = renalSup2_role.Id ;
	     renalSup2.IsActive = true;
	    // renalSup2.Renal_valid_super__c = false;
	     renalSup2.Department = 'Renal-SalesSupervisor';
	     insert renalSup2;
	     
	     //ids+=renalSup2.Id+',';
	     ids+=renalSup2.Id+',';
	     
	     System.debug('######################ids  '+ids);
	     Test.startTest();
	     	 V2_BatchComputationBonus BonusBatch = new V2_BatchComputationBonus();
	     	 BonusBatch.flag = '奖金计算测试类flag';
		     BonusBatch.userids =  ids;
			 BonusBatch.year = Date.today().Year();
			 BonusBatch.month = Date.today().Month();
			 ID batchprocessid = Database.executeBatch(BonusBatch,12);
	    Test.stopTest();
	    /*String ids ;
	    for(User u:[select UserRole.Name,Id,Renal_valid_super__c,Department from User 
					//已启用用户
					Where IsActive = true 
					//未休假用户
					and IsOnHoliday__c =false
					//未离职用户
					and IsLeave__c = false
					//角色不能为空
					and UserRoleId !=null
					and UserRole.Name != null
					and Profile.Name =])
	   {
	   		List<String> RoleNameInfos = String.valueOf(u.UserRole.Name).trim().split('-');
	   		if(RoleNameInfos == null)
	   		{
	   			continue;
	   		}
	   		if(RoleNameInfos.size()<4)
	   		{
	   			continue;
	   		}
	   		if(ids == null)
	   		{
	   			ids = u.Id;
	   		}
	   		else
	   		{
	   			ids+=u.Id;
	   		}
	   }
	    Test.startTest();
		//调用batch
		if(ids !=null && ids.length()>0)
		{
			V2_BatchComputationBonus BonusBatch = new V2_BatchComputationBonus();
			BonusBatch.userids = ids;  
			BonusBatch.year = Date.today().Year();
			BonusBatch.month = Date.today().Month();
			Database.executeBatch(BonusBatch,10);
		}
	     
	    
	     /*V2_BatchComputationBonus BonusBatch = new V2_BatchComputationBonus();
	     BonusBatch.userids =  RenalSu.Id+',';
		 BonusBatch.year = Date.today().Year();
		 BonusBatch.month = Date.today().Month();
		 ID batchprocessid = Database.executeBatch(BonusBatch,1);
		 
		 V2_BatchComputationBonus BonusBatch1 = new V2_BatchComputationBonus();
	     BonusBatch1.userids =  MdSu.Id+',';
		 BonusBatch1.year = Date.today().Year();
		 BonusBatch1.month = Date.today().Month();
		 ID batchprocessid1 = Database.executeBatch(BonusBatch1,1);
		 
		 V2_BatchComputationBonus BonusBatch2 = new V2_BatchComputationBonus();
	     BonusBatch2.userids =  BiosSu.Id+',';
		 BonusBatch2.year = Date.today().Year();
		 BonusBatch2.month = Date.today().Month();
		 ID batchprocessid2 = Database.executeBatch(BonusBatch2,1);*/
		//
    }
  	static testMethod void myUnitTest2() {
      
       
       //----------------New Role ------------------
       	 //Bios
       	 UserRole biosRep_role = new UserRole();
       	 biosRep_role.Name = 'BIOS-Rep-北京-Albumin-Rep' ;
       	 insert biosRep_role;
       	 UserRole biosRep2_role = new UserRole();
       	 biosRep2_role.Name = 'BIOS-Rep-北京-Coseal-Rep' ;
       	 insert biosRep2_role;
       	 
       	 UserRole biosSup_role = new UserRole();
       	 biosSup_role.Name = 'BIOS-Supervisor-北京-Albumin-Rep' ;
       	 insert biosSup_role;
       	 UserRole biosSup2_role = new UserRole();
       	 biosSup2_role.Name = 'BIOS-Supervisor-北京-Coseal-Supervisor' ;
       	 insert biosSup2_role;
       	 
	    //Md
	     UserRole mdRep_role = new UserRole();
       	 mdRep_role.Name = 'MD-Rep-北京-IVT-Rep' ;
       	 insert mdRep_role;
       	 UserRole mdRep2_role = new UserRole();
       	 mdRep2_role.Name = 'MD-Rep-北京-SP-Rep' ;
       	 insert mdRep2_role;
       	 
       	 UserRole mdSup_role = new UserRole();
       	 mdSup_role.Name = 'MD-Supervisor-北京-IVT-Supervisor' ;
       	 insert mdSup_role;
       	 UserRole mdSup2_role = new UserRole();
       	 mdSup2_role.Name = 'MD-Supervisor-北京-SP-Supervisor' ;
       	 insert mdSup2_role;
       	 
       	 //Renal
       	 UserRole renalRep_role = new UserRole();
       	 renalRep_role.Name = 'Renal-Rep-北京-CRRT_CS-Rep' ;
       	 insert renalRep_role;
       	 UserRole renalRep2_role = new UserRole();
       	 renalRep2_role.Name = 'Renal-Rep-北京-PD_CS-Rep' ;
       	 insert renalRep2_role;
       	 
       	 UserRole renalSup_role = new UserRole();
       	 renalSup_role.Name = 'Renal-Supervisor-北京-CRRT_CS-Supervisor' ;
       	 insert renalSup_role;
       	 UserRole renalSup2_role = new UserRole();
       	 renalSup2_role.Name = 'Renal-Supervisor-北京-PD_CS-Supervisor' ;
       	 insert renalSup2_role;
	     
	    //Bios简档
	    Profile BiosRepPro = [select Id from Profile where Name  = 'Standard User - BIOS Sales Rep' limit 1];
	    Profile BiosSupPro = [select Id from Profile where Name  = 'Standard User - BIOS Sales Supervisor' limit 1];
	    //Md简档
	    Profile MdRepPro = [select Id from Profile where Name='Standard User - IVT Sales Rep' limit 1];
	    Profile MdSupPro = [select Id from Profile where Name='Standard User - IVT Sales Supervisor' limit 1];
	   //Renal简档
	    Profile RenalRepPro = [select Id from Profile where Name='Standard User - Renal Sales Rep' limit 1];
	    Profile RenalSupPro = [select Id from Profile where Name='Standard User - Renal Sales Supervisor' limit 1];
	    
	//----------------Create User-------------
	   //用户ids
	    String ids ;
	   
	     List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
         //Bios
	     User BiosRep1 = new User();
	     BiosRep1.Username='BiosRep1@123.com';
	     BiosRep1.LastName='BiosRep1';
	     BiosRep1.Email='BiosRep1@123.com';
	     BiosRep1.Alias=user[0].Alias;
	     BiosRep1.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     BiosRep1.ProfileId=BiosRepPro.Id;
	     BiosRep1.LocaleSidKey=user[0].LocaleSidKey;
	     BiosRep1.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     BiosRep1.EmailEncodingKey=user[0].EmailEncodingKey;
	     BiosRep1.CommunityNickname='BiosRep1';
	     BiosRep1.MobilePhone='12345678912';
	     BiosRep1.UserRoleId = biosRep_role.Id ;
	     BiosRep1.IsActive = true;
	     insert BiosRep1;
	     
	     ids=BiosRep1.Id+',';
	     
	     User BiosRep2 = new User();
	     BiosRep2.Username='BiosRep2@123.com';
	     BiosRep2.LastName='BiosRep2';
	     BiosRep2.Email='BiosRep2@123.com';
	     BiosRep2.Alias=user[0].Alias;
	     BiosRep2.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     BiosRep2.ProfileId=BiosRepPro.Id;
	     BiosRep2.LocaleSidKey=user[0].LocaleSidKey;
	     BiosRep2.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     BiosRep2.EmailEncodingKey=user[0].EmailEncodingKey;
	     BiosRep2.CommunityNickname='BiosRep2';
	     BiosRep2.MobilePhone='12345678912';
	     BiosRep2.UserRoleId = biosRep2_role.Id ;
	     BiosRep2.IsActive = true;
	     insert BiosRep2;
	     
	     ids+=BiosRep2.Id+',';
	     
	     User biosSup = new User();
	     biosSup.Username='biosSup@123.com';
	     biosSup.LastName='biosSup';
	     biosSup.Email='biosSup@123.com';
	     biosSup.Alias=user[0].Alias;
	     biosSup.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     biosSup.ProfileId=BiosSupPro.Id;
	     biosSup.LocaleSidKey=user[0].LocaleSidKey;
	     biosSup.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     biosSup.EmailEncodingKey=user[0].EmailEncodingKey;
	     biosSup.CommunityNickname='biosSup';
	     biosSup.MobilePhone='12345678912';
	     biosSup.UserRoleId = biosSup_role.Id ;
	     biosSup.IsActive = true;
	     insert biosSup;
	     
	     ids+=biosSup.Id+',';
	     
	     User biosSup2 = new User();
	     biosSup2.Username='biosSup2@123.com';
	     biosSup2.LastName='biosSup2';
	     biosSup2.Email='biosSup2@123.com';
	     biosSup2.Alias=user[0].Alias;
	     biosSup2.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     biosSup2.ProfileId=BiosSupPro.Id;
	     biosSup2.LocaleSidKey=user[0].LocaleSidKey;
	     biosSup2.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     biosSup2.EmailEncodingKey=user[0].EmailEncodingKey;
	     biosSup2.CommunityNickname='biosSup2';
	     biosSup2.MobilePhone='12345678912';
	     biosSup2.UserRoleId = biosSup2_role.Id ;
	     biosSup2.IsActive = true;
	     insert biosSup2;
	     
	     ids+=biosSup2.Id+',';
	     
	     //Md
	     User mdRep = new User();
	     mdRep.Username='mdRep@123.com';
	     mdRep.LastName='mdRep';
	     mdRep.Email='mdRep@123.com';
	     mdRep.Alias=user[0].Alias;
	     mdRep.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     mdRep.ProfileId=MdRepPro.Id;
	     mdRep.LocaleSidKey=user[0].LocaleSidKey;
	     mdRep.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     mdRep.EmailEncodingKey=user[0].EmailEncodingKey;
	     mdRep.CommunityNickname='mdRep';
	     mdRep.MobilePhone='12345678912';
	     mdRep.UserRoleId = mdRep_role.Id ;
	     mdRep.IsActive = true;
	     insert mdRep;
	     
	     ids+=mdRep.Id+',';
	     
	     User mdRep2 = new User();
	     mdRep2.Username='mdRep2@123.com';
	     mdRep2.LastName='mdRep2';
	     mdRep2.Email='mdRep2@123.com';
	     mdRep2.Alias=user[0].Alias;
	     mdRep2.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     mdRep2.ProfileId=MdRepPro.Id;
	     mdRep2.LocaleSidKey=user[0].LocaleSidKey;
	     mdRep2.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     mdRep2.EmailEncodingKey=user[0].EmailEncodingKey;
	     mdRep2.CommunityNickname='mdRep2';
	     mdRep2.MobilePhone='12345678912';
	     mdRep2.UserRoleId = mdRep2_role.Id ;
	     mdRep2.IsActive = true;
	     insert mdRep2;
	     
	      ids+=mdRep2.Id+',';
	     
	     User mdSup = new User();
	     mdSup.Username='mdSup@123.com';
	     mdSup.LastName='mdSup';
	     mdSup.Email='mdSup@123.com';
	     mdSup.Alias=user[0].Alias;
	     mdSup.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     mdSup.ProfileId=MdSupPro.Id;
	     mdSup.LocaleSidKey=user[0].LocaleSidKey;
	     mdSup.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     mdSup.EmailEncodingKey=user[0].EmailEncodingKey;
	     mdSup.CommunityNickname='mdSup';
	     mdSup.MobilePhone='12345678912';
	     mdSup.UserRoleId = mdSup_role.Id ;
	     mdSup.IsActive = true;
	     insert mdSup;
	     
	      ids+=mdSup.Id+',';
	     
	     User mdSup2 = new User();
	     mdSup2.Username='mdSup2@123.com';
	     mdSup2.LastName='mdSup2';
	     mdSup2.Email='mdSup2@123.com';
	     mdSup2.Alias=user[0].Alias;
	     mdSup2.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     mdSup2.ProfileId=MdSupPro.Id;
	     mdSup2.LocaleSidKey=user[0].LocaleSidKey;
	     mdSup2.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     mdSup2.EmailEncodingKey=user[0].EmailEncodingKey;
	     mdSup2.CommunityNickname='mdSup2';
	     mdSup2.MobilePhone='12345678912';
	     mdSup2.UserRoleId = mdSup2_role.Id ;
	     mdSup2.IsActive = true;
	     insert mdSup2;
	     
	      ids+=mdSup2.Id+',';
	     
         //Renal
         User renalRep = new User();
	     renalRep.Username='renalRep@123.com';
	     renalRep.LastName='renalRep';
	     renalRep.Email='renalRep@123.com';
	     renalRep.Alias=user[0].Alias;
	     renalRep.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     renalRep.ProfileId=RenalRepPro.Id;
	     renalRep.LocaleSidKey=user[0].LocaleSidKey;
	     renalRep.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     renalRep.EmailEncodingKey=user[0].EmailEncodingKey;
	     renalRep.CommunityNickname='renalRep';
	     renalRep.MobilePhone='12345678912';
	     renalRep.UserRoleId = renalRep_role.Id ;
	     renalRep.IsActive = true;
	     renalRep.Renal_valid_super__c  = false;
	     renalRep.Department = 'Renal-SalesRep';
	     insert renalRep;
	     
	     ids+=renalRep.Id+',';
	     
	     User renalRep2 = new User();
	     renalRep2.Username='renalRep2@123.com';
	     renalRep2.LastName='renalRep2';
	     renalRep2.Email='renalRep2@123.com';
	     renalRep2.Alias=user[0].Alias;
	     renalRep2.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     renalRep2.ProfileId=RenalRepPro.Id;
	     renalRep2.LocaleSidKey=user[0].LocaleSidKey;
	     renalRep2.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     renalRep2.EmailEncodingKey=user[0].EmailEncodingKey;
	     renalRep2.CommunityNickname='renalRep2';
	     renalRep2.MobilePhone='12345678912';
	     renalRep2.UserRoleId = renalRep2_role.Id ;
	     renalRep2.IsActive = true;
	     renalRep2.Renal_valid_super__c = false;
	     renalRep2.Department = 'Renal-SalesRep';
	     insert renalRep2;
	     
	     ids+=renalRep2.Id+',';
	     
	     User renalSup = new User();
	     renalSup.Username='renalSup@123.com';
	     renalSup.LastName='renalSup';
	     renalSup.Email='renalSup@123.com';
	     renalSup.Alias=user[0].Alias;
	     renalSup.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     renalSup.ProfileId=RenalSupPro.Id;
	     renalSup.LocaleSidKey=user[0].LocaleSidKey;
	     renalSup.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     renalSup.EmailEncodingKey=user[0].EmailEncodingKey;
	     renalSup.CommunityNickname='renalSup';
	     renalSup.MobilePhone='12345678912';
	     renalSup.UserRoleId = renalSup_role.Id ;
	     renalSup.IsActive = true;
	     renalSup.Renal_valid_super__c = false;
	     renalSup.Department = 'Renal-SalesSupervisor';
	     insert renalSup;
	     System.debug('#############################################################################');
	     ids+=renalSup.Id+',';
	     
	     User renalSup2 = new User();
	     renalSup2.Username='renalSup2@123.com';
	     renalSup2.LastName='renalSup2';
	     renalSup2.Email='renalSup2@123.com';
	     renalSup2.Alias=user[0].Alias;
	     renalSup2.TimeZoneSidKey=user[0].TimeZoneSidKey;
	     renalSup2.ProfileId=RenalSupPro.Id;
	     renalSup2.LocaleSidKey=user[0].LocaleSidKey;
	     renalSup2.LanguageLocaleKey=user[0].LanguageLocaleKey;
	     renalSup2.EmailEncodingKey=user[0].EmailEncodingKey;
	     renalSup2.CommunityNickname='renalSup2';
	     renalSup2.MobilePhone='12345678912';
	     renalSup2.UserRoleId = renalSup2_role.Id ;
	     renalSup2.IsActive = true;
	    // renalSup2.Renal_valid_super__c = false;
	     renalSup2.Department = 'Renal-SalesSupervisor';
	     insert renalSup2;
	     
	     //ids+=renalSup2.Id+',';
	     ids+=renalSup2.Id+',';
	     
	     System.debug('######################ids  '+ids);
	     Test.startTest();
	     	 V2_BatchComputationBonus BonusBatch = new V2_BatchComputationBonus();
	     	 BonusBatch.flag = '奖金计算测试类flag';
		     BonusBatch.userids =  ids;
			 BonusBatch.year = Date.today().Year();
			 BonusBatch.month = Date.today().Month();
			 ID batchprocessid = Database.executeBatch(BonusBatch,12);
	    Test.stopTest();
	    /*String ids ;
	    for(User u:[select UserRole.Name,Id,Renal_valid_super__c,Department from User 
					//已启用用户
					Where IsActive = true 
					//未休假用户
					and IsOnHoliday__c =false
					//未离职用户
					and IsLeave__c = false
					//角色不能为空
					and UserRoleId !=null
					and UserRole.Name != null
					and Profile.Name =])
	   {
	   		List<String> RoleNameInfos = String.valueOf(u.UserRole.Name).trim().split('-');
	   		if(RoleNameInfos == null)
	   		{
	   			continue;
	   		}
	   		if(RoleNameInfos.size()<4)
	   		{
	   			continue;
	   		}
	   		if(ids == null)
	   		{
	   			ids = u.Id;
	   		}
	   		else
	   		{
	   			ids+=u.Id;
	   		}
	   }
	    Test.startTest();
		//调用batch
		if(ids !=null && ids.length()>0)
		{
			V2_BatchComputationBonus BonusBatch = new V2_BatchComputationBonus();
			BonusBatch.userids = ids;  
			BonusBatch.year = Date.today().Year();
			BonusBatch.month = Date.today().Month();
			Database.executeBatch(BonusBatch,10);
		}
	     
	    
	     /*V2_BatchComputationBonus BonusBatch = new V2_BatchComputationBonus();
	     BonusBatch.userids =  RenalSu.Id+',';
		 BonusBatch.year = Date.today().Year();
		 BonusBatch.month = Date.today().Month();
		 ID batchprocessid = Database.executeBatch(BonusBatch,1);
		 
		 V2_BatchComputationBonus BonusBatch1 = new V2_BatchComputationBonus();
	     BonusBatch1.userids =  MdSu.Id+',';
		 BonusBatch1.year = Date.today().Year();
		 BonusBatch1.month = Date.today().Month();
		 ID batchprocessid1 = Database.executeBatch(BonusBatch1,1);
		 
		 V2_BatchComputationBonus BonusBatch2 = new V2_BatchComputationBonus();
	     BonusBatch2.userids =  BiosSu.Id+',';
		 BonusBatch2.year = Date.today().Year();
		 BonusBatch2.month = Date.today().Month();
		 ID batchprocessid2 = Database.executeBatch(BonusBatch2,1);*/
		//
    }
    
    
    
}