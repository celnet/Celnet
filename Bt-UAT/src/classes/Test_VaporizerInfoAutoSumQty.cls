/**
 * author : bill
 * VaporizerInfoAutoSumQty的trigger测试类
 */
@isTest
private class Test_VaporizerInfoAutoSumQty {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        //客户
        Account acc = new  Account();
        acc.Name = 'zhangsan';
        insert acc;
        Account acc1 = new  Account();
        acc1.Name = 'zhangsan';
        insert acc1;
        //挥发罐
        VaporizerInfo__c vap = new VaporizerInfo__c();
        vap.Hospital__c =acc.Id;
        insert vap;
        
        test.startTest();
        vap.Hospital__c =acc1.Id;
        update vap;
        test.stopTest();
    }
}