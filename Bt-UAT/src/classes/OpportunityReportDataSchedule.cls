/**
 * Schedulable OpportunityReportDataBatch
 */	
public class OpportunityReportDataSchedule implements Schedulable{
	public void execute(SchedulableContext sc) {
    	OpportunityReportDataBatch oppBatch = new OpportunityReportDataBatch();
    	oppBatch.curMonth = date.today().month(); 
        database.executeBatch(oppBatch,10);
    }
}