/**
 * 作者:Bill
 * 说明：SEP GIS KPI计算
**/

public class ClsGisKpiService {
    ID UserId ;
    Integer IntYear ;
    Integer IntMonth ;
    string ContactType;
    
    public ClsGisKpiService(){
        
    }
    public ClsGisKpiService(ID sUserId,Integer sIntYear,Integer sIntMonth){
        UserId = sUserId;
        IntYear = sIntYear;
        IntMonth = sIntMonth ; 
    }

    //KPI
    //获取不同等级的联系人的拜访记录
    private list<Contact> GetContactVisit(string type){
        //已完成、未过期的拜访
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
         
        List<Contact> list_Contact = [Select c.GISContactType__c, (Select Id From Events Where RecordType.DeveloperName = 'V2_Event' And Done__c = true And V2_IsExpire__c = false And StartDateTime >: CheckStartDateTime And StartDateTime <: CheckEndDateTime And OwnerId =: UserId ) From Contact c where c.GISContactType__c =:type];
        return list_Contact;
    }
    //获取已完成未过期的拜访
    private List<Event> getValidVisit(){
        DateTime CheckStartDateTime = datetime.newInstance(IntYear, IntMonth, 1, 0, 0, 0) ;
        //DateTime CheckEndDateTime = CheckStartDateTime.addMonths(1);
        DateTime CheckEndDateTime ;
        if(IntMonth == 12){
            CheckEndDateTime = datetime.newInstance(IntYear+1, 1, 1, 0, 0, 0) ;
        }else{
            CheckEndDateTime = datetime.newInstance(IntYear, IntMonth+1, 1, 0, 0, 0) ;
        }
        //List<Event> list_Visit=[Select Id,GAPlan__c,WhoId,Who.Name,Who.RecordTypeId,GAExecuteResult__c From Event ];
        List<Event> list_Visit=[Select Id,GAPlan__c,WhoId,GAExecuteResult__c,Key_Information__c From Event Where IsChild = false And RecordType.DeveloperName = 'V2_Event' And Done__c = true And V2_IsExpire__c = false And StartDateTime >: CheckStartDateTime And StartDateTime <: CheckEndDateTime And OwnerId =: UserId];
        return list_Visit ;
    }
    private Double getContactVisitByType(String strType , Set<ID> set_ContactId , List<Event> list_Event){
        Double num = 0 ;
        Set<ID> set_ContactIds = new Set<ID>();
        for(Contact e : [Select Id,GISContactType__c From Contact Where Id in: set_ContactId]){
            if(strType == '关键客户' && e.GISContactType__c == strType){
                set_ContactIds.add(e.Id);
            }
            if(strType == '普通客户'){
            	set_ContactIds.add(e.Id);
            }
        }
        for(Event e : list_Event){
        	if(set_ContactIds.contains(e.WhoId )){
        		num++;
        	}
        }
        return num;
    }
    
    //KPI 关键客户拜访量（人次/每月）  10(人次/月)达标
    public Map<String , Double> GetKeyContactVisit(){
        Map<String , Double> map_Result = new Map<String , Double>();
        double num = 0;
        ContactType = '关键客户';
        List<Event> list_Event = getValidVisit();
        Set<ID> set_ContactId = new Set<ID>();
        for(Event e : list_Event){
            if(e.WhoId != null){
                set_ContactId.add(e.WhoId);
            }
        }
        num = getContactVisitByType(ContactType , set_ContactId , list_Event) ;
        /*
        List<Contact> list_GisContact = GetContactVisit(ContactType);
        for(Contact ctVisit:list_GisContact){
            for(List<Event> ev:ctVisit.Events){
                num += ev.size();
            }
        }
        */ 
        map_Result.put('Target' , 10);
        map_Result.put('Finish' , num);
        return map_Result;
    }
    
    //KPI 普通客户拜访量（人次/每月）  40(人次/月)达标
    //2013-4-25 sunny:修改逻辑，普通客户拜访不在只看普通客户，而是看所有拜访量要超过50次。
    //PS:奖金参数要随之修改，普通客户拜访量要改为50.
    public Map<String , Double> GetCommonContactVisit(){
        Map<String , Double> map_Result = new Map<String , Double>();
        double num = 0;
        ContactType = '普通客户';
        List<Event> list_Event = getValidVisit();
        Set<ID> set_ContactId = new Set<ID>();
        for(Event e : list_Event){
            if(e.WhoId != null){
                set_ContactId.add(e.WhoId);
            }
        }
        num = getContactVisitByType(ContactType , set_ContactId , list_Event) ;
        /*
        List<Contact> list_GisContact = GetContactVisit(ContactType);
        for(Contact ctVisit:list_GisContact){
            for(List<Event> ev:ctVisit.Events){
                num += ev.size();
            }
        } 
        */
        map_Result.put('Target' , 50);
        map_Result.put('Finish' , num);
        return map_Result;
    }
    
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        //user role
        List<UserRole> listur= new List<UserRole>();
        UserRole ur1 = new UserRole();
        ur1.Name = 'Renal-Supervisor-华南-PD-Supervisor(U1)';
        listur.add(ur1);
        UserRole ur2 = new UserRole();
        ur2.Name = 'Renal-Rep-华南-PD-Rep(U2)';
        listur.add(ur2);
        insert listur;
        //user 
        list<User> user=[Select TimeZoneSidKey,ProfileId,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname, Alias From User Where IsActive = true limit 1];
        List<User> list_user = new List<User>();
        User user1=new User();
        user1.username='user1r2@123.com';
        user1.LastName='user1r2';
        user1.Email='user1r2@123.com';
        user1.Alias=user[0].Alias;
        user1.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user1.ProfileId=UserInfo.getProfileId();
        user1.LocaleSidKey=user[0].LocaleSidKey;
        user1.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user1.EmailEncodingKey=user[0].EmailEncodingKey;
        user1.CommunityNickname='chequ';
        user1.MobilePhone='12345678912';
        user1.IsActive = true;
        user1.UserRoleId=ur1.Id ;
        list_user.add(user1);
        User user2=new User();
        user2.username='user2r2@223.com';
        user2.LastName='user2r2';
        user2.Email='user2r2@223.com';
        user2.Alias=user[0].Alias;
        user2.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user2.ProfileId=UserInfo.getProfileId();
        user2.LocaleSidKey=user[0].LocaleSidKey;
        user2.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user2.EmailEncodingKey=user[0].EmailEncodingKey;
        user2.CommunityNickname='chequ1';
        user2.MobilePhone='22345678922';
        user2.IsActive = true;
        user1.UserRoleId=ur1.Id ;
        list_user.add(user2);
        insert list_user;
        
        //联系人
        List<Contact> listct= new List<Contact>();
        Contact ct1 = new Contact();
        ct1.FirstName = 'jim';
        ct1.LastName = 'Green';
        ct1.GISContactType__c ='关键客户';
        listct.add(ct1);
        Contact ct2 = new Contact();
        ct2.FirstName = 'Tom';
        ct2.LastName = 'Frank';
        ct2.GISContactType__c ='普通客户';
        listct.add(ct2);
        insert listct;
        
        
        //活动
        List<Event> list_Visit = new List<Event>();
        Event ev1 = new Event();
        ev1.DurationInMinutes = 1;
        ev1.StartDateTime = datetime.now();
        ev1.OwnerId = user1.Id ;
        ev1.GAPlan__c = '1111111';
        ev1.GAExecuteResult__c = '111111';
        ev1.RecordTypeId = '01290000000NapsAAC';
        ev1.Done__c = true;
        ev1.V2_IsExpire__c = false; 
        ev1.WhoId = ct1.Id;
        list_Visit.add(ev1);
        Event ev2 = new Event();
        ev2.DurationInMinutes = 2;
        ev2.StartDateTime = datetime.now();
        ev2.OwnerId = user1.Id ;
        ev2.GAPlan__c = '222222';
        ev2.GAExecuteResult__c = '222222';
        ev2.RecordTypeId = '01290000000NapsAAC';
        ev2.Done__c = true;
        ev2.V2_IsExpire__c = false; 
        ev2.WhoId = ct1.Id;
        list_Visit.add(ev2);
        Event ev3 = new Event();
        ev3.DurationInMinutes = 3;
        ev3.StartDateTime = datetime.now();
        ev3.OwnerId = user1.Id ;
        ev3.GAPlan__c = '333333';
        ev3.GAExecuteResult__c = '3333333';
        ev3.RecordTypeId = '01290000000NapsAAC';
        ev3.Done__c = true;
        ev3.V2_IsExpire__c = false; 
        ev3.WhoId = ct1.Id;
        list_Visit.add(ev3);
        Event ev4 = new Event();
        ev4.DurationInMinutes = 4;
        ev4.StartDateTime = datetime.now();
        ev4.OwnerId = user1.Id ;
        ev4.GAPlan__c = '333333';
        ev4.GAExecuteResult__c = '3333333';
        ev4.RecordTypeId = '01290000000NapsAAC';
        ev4.Done__c = true;
        ev4.V2_IsExpire__c = false; 
        ev4.WhoId = ct2.Id;
        list_Visit.add(ev4);
        insert list_Visit ;
        
        system.test.startTest();
        ClsGisKpiService cls1 = new ClsGisKpiService(user1.Id , datetime.now().year() , datetime.now().month());
        Map<String , Double> a = cls1.GetKeyContactVisit();
        Map<String , Double> b = cls1.GetCommonContactVisit();
        system.debug('@@@@'+a+'@@@@'+b);
        system.test.stopTest();
     }
}