global class PM_WebServiceUtil 
{
	webService static void runBasicTableBatch()
	{
		PM_BasicTableBatch btBatch = new PM_BasicTableBatch();
		database.executeBatch(btBatch,5);
	}
	webService static void runPatientDataStatisticsBatch()
	{
		PM_PatientDataStatisticsBatch patientBatch = new PM_PatientDataStatisticsBatch();
        database.executeBatch(patientBatch,10);
	}
	global static void sendMail(string batchName)
	{
		String emailAddress = UserInfo.getUserEmail();
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		String repBody = '';
		repBody += '您好: <br>'+batchName+'Batch已经运行完成。<br><br>';     
        repBody += '如有任何疑问或者要求，请联系系统管理人员。<br>';
        String[] repAddress =new string[]{emailAddress};
        mail.setToAddresses(repAddress);
	    mail.setHtmlBody(repBody);
	    mail.setSubject(batchName+'Batch已经运行完成');
	    mail.setSenderDisplayName('Salesforce');
	    system.debug('**************emailAddress****************'+emailAddress);
		if(emailAddress != null)
	    {
	        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	    } 		
	}
}