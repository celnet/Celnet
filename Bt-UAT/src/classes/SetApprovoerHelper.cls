/**
 * Author : Sunny
 * 设置挥发罐申请审批人帮助类
 */
public class SetApprovoerHelper {
	private ID VaporizerAdminId ;
	private ID VapProductManagerDesId ;
	private ID VapProductManagerSevoId ;
	private Id NationalDirectorId ;
	public Map<String , ID> getHierarchy(ID UserId){
		Map<String , ID> Map_ManagerIds = new Map<String , ID>();
		//申请人信息
        User Submitter = [Select UserRole.Name, UserRole.Id, UserRole.ParentRoleId, UserRoleId, Id, Name, Alias, Email From User Where Id =: UserId] ;
		V2_UtilClass uc = new V2_UtilClass();
        List<ID> list_userRoleIds = uc.getRoleIdToRSM(Submitter.UserRoleId) ;
        for(UserRole ur : [Select Id,Name,(Select Id, Name, Email, Alias From Users Where IsActive = true) From UserRole Where id in: list_userRoleIds]){
            if(ur.Name.toUpperCase().contains('REGIONAL') && ur.Users.size() > 0){
                Map_ManagerIds.put('REGIONAL' , ur.Users[0].Id);
            }else if(ur.Name.toUpperCase().contains('AREA') && ur.Users.size() > 0){
                Map_ManagerIds.put('AREA' , ur.Users[0].Id);
            }else if(ur.Name.toUpperCase().contains('DISTRICT') && ur.Users.size() > 0){
                Map_ManagerIds.put('DISTRICT' , ur.Users[0].Id); 
            }else if(ur.Name.toUpperCase().contains('SUPERVISOR') && ur.Users.size() > 0){
                Map_ManagerIds.put('SUPERVISOR' , ur.Users[0].Id);
            }
        }
        return Map_ManagerIds;
	}
    public ID getVaporizerAdmin(){
    	if(VaporizerAdminId == null){
    		List<User> listVapAdmin = [Select Id From User Where UserRole.Name Like: 'ACC Marketing assistant%' And IsActive = true];
    		if(listVapAdmin.size() > 0){
	            VaporizerAdminId = listVapAdmin[0].Id;
	        }
    	}
    	return VaporizerAdminId ;
    }
    public Id getVapProductManagerDes(){
    	if(VapProductManagerDesId == null){
    		List<User> listVapAdmin = [Select Id From User Where UserRole.Name Like: 'ACC Des Sr. Product Manager%' And IsActive = true];
	        if(listVapAdmin.size() > 0){
	            VapProductManagerDesId = listVapAdmin[0].Id;
	        }
    	}
        return VapProductManagerDesId ;
    }
    public Id getVapProductManagerSevo(){
    	if(VapProductManagerSevoId == null){
    		List<User> listVapAdmin = [Select Id From User Where UserRole.Name Like: 'ACC Sevo Sr.Product Manager%' And IsActive = true];
	        if(listVapAdmin.size() > 0){
	            VapProductManagerSevoId = listVapAdmin[0].Id;
	        }
    	}
        return VapProductManagerSevoId ;
    }
    public Id getNationalDirector(){
    	if(NationalDirectorId == null){
    		List<User> listVapAdmin = [Select Id From User Where UserRole.Name Like: 'MD-National-全国-SP-Director%' And IsActive = true];
            if(listVapAdmin.size() > 0){
                NationalDirectorId = listVapAdmin[0].Id;
            }
    	}
    	return NationalDirectorId;
    }
}