/**
 * Tobe
 * 2013.11.1
 * PM_AccountRegionReplace 测试类
 */
@isTest
private class PM_Test_AccountRegionReplace {

    static testMethod void myUnitTest() 
    {
    	//客户
    	list<RecordType> record = [Select Id From RecordType Where Name = '医院'];
    	list<RecordType> record2 = [Select Id From RecordType Where Name = '经销商'];
    	//新建省市
        Region__c zone = new Region__c();
        zone.Name = '南区';
        insert zone;
		PM_Province__c pro = new PM_Province__c();
    	pro.Name = '浙江省';
    	pro.PM_Region__c = zone.Id;
    	insert pro;
    	Provinces__c proq = new Provinces__c();
    	proq.Name = '浙江省';
    	proq.Region__c = zone.Id;
    	insert proq;
    	Cities__c city = new Cities__c();
    	city.Name = '杭州市';
    	city.BelongToProvince__c = proq.Id;
    	insert city;
        Account acc = new  Account();
        acc.Name = '浙江医院';
        acc.PM_Province__c = pro.Id;
        acc.RecordTypeId = record[0].Id;
        insert acc;
        Account acc2 = new  Account();
        acc2.Name = '浙江医药控股';
        acc2.RecordTypeId = record[0].Id;
        insert acc2;
        acc.PM_Province__c = pro.Id;
        update acc;
    	acc.Provinces__c =null;
        acc.Cities__c = city.Id;
        acc.Region__c = zone.Id;
        update acc;
        acc2.Provinces__c = proq.Id;
        acc2.Cities__c = city.Id;
        acc2.Region__c = zone.Id;
        update acc2;
        Account acc3 = new  Account();
        acc3.Name = '浙江医院';
        acc3.RecordTypeId = record[0].Id;
        acc3.Cities__c = city.Id;
        acc3.PM_Province__c =null;
        insert acc3;
     	Account acc4 = new  Account();
        acc4.Name = '浙江医院';
        acc4.RecordTypeId = record[0].Id;
        acc4.Provinces__c = proq.Id;
        insert acc4;
        
    }
}