public class ContactRole{
    public ContactRole(ApexPages.StandardSetController controller) {

    }


        
    public List<contact_rule__c> roleList;
    public List<contact_rule__c>  listToUpdate;
    public Contact_rule__c newRole {get;set;}
    
    /* constructor */
    
    public ContactRole(ApexPages.StandardController controller) {
        
    }
    public ContactRole(){
    }
   
   /* Method getContactRole returns list of
   
   1. Existing contact Roles for the opportunity
   2. Related contact list of the associated account
   3. Three new rows
   
   */

    public List<contact_rule__c> getContactRoles(){
        
        roleList=new List<contact_rule__c>();
        Id opptyId= ApexPages.currentPage().getParameters().get('id');
        Id acctId=[select accountId from opportunity where id=:opptyId].accountId;
        
        // get existing roles     
        listToUpdate= [select Id, Name,Contact__c,Sum_Dialyse__c,ESRD_Percent__c,
        isPrimary__c,Title__c,Role__c,Influence__c,visit_strategy__c,ChangeMode__c,Modeproof__c,Grade__c,SupportProof__c,Pa_Month__c,Rec_Renal__c,Rec_rate__c,Dialyse_ESRD__c,Dialyse_Percent__c,PD_No__c,PD_Take_on_rate__c from contact_rule__c where contact_rule__c.opportunity_name__r.id=:opptyId];
        
       
        roleList.addAll(listToUpdate);
        
        // get related contacts of the associated account
        List<Contact> contactRelatedList=[select id from contact where AccountId=:acctId and id not in :getIds(listToUpdate)];
        
        for(Contact c:contactRelatedList){
            
            Contact_rule__c cRole= new Contact_rule__c();
            cRole.contact__c=c.id;
            roleList.add(cRole);
        
        }
        
        // Create three new contact roles to be inserted
        
        for(Integer i=0;i<3;i++){
            newRole=new Contact_rule__c();
            roleList.add(newRole);

        }
        
        
        return roleList;
    }
    
    private List<Id> getIds(List<Contact_rule__c> relatedContact){
        List<Id> contactIds =new List<Id>();
        for(contact_rule__c c:listToUpdate){
            contactIds.add(c.contact__c);
        }
        return contactIds;
    }
    
    public String getOpportunityName(){
        return [select name from Opportunity where id=:ApexPages.currentPage().getParameters().get('id')].name;
    }
    
   /* Method save returns to the opportunity detail page after 
   
   1. Updating existing roles for the opportunity
   2. Inserting new roles if any
   
   */

    public PageReference save(){
    
        List<contact_rule__c>  listToInsert=new List<contact_rule__c>();       
        Integer sum_ESRD = 0;
        Integer sum_dialyse = 0;
        for(Integer i=0; i<roleList.size();i++){// prepare the list for new roles to be inserted
            
            roleList[i].opportunity_name__c=
            ApexPages.currentPage().getParameters().get('id');// get the opportunity name
                        
            //if(roleList[i].contact__c!=null && roleList[i].role__c!=null && i>=listToUpdate.size()){
            //changed by Ken on 2011-09-20, 
            //the user want to keep the role field to be non-mandatory
            if(roleList[i].contact__c!=null && i>=listToUpdate.size()){
                listToInsert.add(roleList[i]);
            }
            // Sum total of "" 
            if(roleList[i].Pa_Month__c != null){
                sum_ESRD = sum_ESRD+(roleList[i].Pa_Month__c).intValue();
            }
            if(roleList[i].Dialyse_ESRD__c != null){
                sum_dialyse = sum_dialyse+(roleList[i].Dialyse_ESRD__c).intValue();
            }
            
       }
       if(!listToUpdate.isEmpty()){
                for(contact_rule__c upd : listToUpdate){
                     upd.Sum_ESRD__c = sum_ESRD;
                     upd.Sum_Dialyse__c=sum_dialyse;
                  
                     }
                updateExistingRole();// calls the method to update existing roles
            }
       if(!listToInsert.isEmpty()){// Insert new roles if list is not empty
                try{
                    for(contact_rule__c ins : listToInsert){
                        ins.Sum_ESRD__c = sum_ESRD;
                        ins.Sum_Dialyse__c=sum_dialyse;
                        }
                        
                    insert listToInsert;
                }
                catch(DmlException ex){
                    ApexPages.addMessages(ex);
               } 
            }
        
        Id id=ApexPages.currentPage().getParameters().get('id');
        PageReference pg= new PageReference('/'+id);
        pg.setRedirect(true);
        return pg;               
    
    }
    
    /*
        updateExistingRole method checks if the records are valid and then update the existing roles.
        The records are valid if both Contact and role fields are not empty
    */
    private void updateExistingRole(){
        List<Contact_rule__c> validList=new List<Contact_rule__c>();
        
        for(contact_rule__c c:listToUpdate){// checks if Contact and role fields have values 
            //if(c.role__c!=null && c.contact__c!=null)
            //changed by Ken on 2011-09-20, 
            //the user want to keep the role field to be non-mandatory
            if(c.contact__c!=null)
            validList.add(c);
        }
        if(!validList.isEmpty())
        try{
            update validList;
        }
        catch(DmlException ex){
            ApexPages.addMessages(ex);
       }
            

    }
    /*
        cancel() returns to the opportunity detail page
    */
    public PageReference cancel(){
        
        Id id=ApexPages.currentPage().getParameters().get('id');
        PageReference pg= new PageReference('/'+id);
        pg.setRedirect(true);
        return pg;
    }

    
    public static testMethod void testMyController() {
    
        Opportunity oppty=[select id,name from opportunity where accountId!=null limit 1];
        String opptyid=oppty.id;
        PageReference pageRef = new PageReference('/'+opptyid);
        Test.setCurrentPage(pageRef);
      
        ContactRole controller = new ContactRole();
        
        // Add parameters to page URL 
    
        ApexPages.currentPage().getParameters().put('id', opptyid);
        List<contact_rule__c> cRoles=controller.getContactRoles();
        String opptyname=controller.getOpportunityname();
        System.assertEquals(oppty.name, opptyname);
        String nextPage = controller.cancel().getUrl();
        System.assertEquals('/'+opptyid, nextPage);
        
        Id contactId=[select id from contact limit 1].id;
        controller.newrole.contact__c = contactId;
        controller.newrole.role__c='用户(U)';
        String afterSave = controller.save().getUrl();
        System.assertEquals('/'+opptyid, afterSave);

        
   }


}