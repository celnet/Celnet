/**
 * Hank
 * 2013-9-23
 * 功能：新病人审批界面，批量审批新病人
 */
public class PM_ApplyNewPatientsController 
{
    private static final string query = 'SELECT PM_HaveDuplicatePatient__c,IntubationDate__c, PM_PSR__r.Alias, Id,PatientName__c,IntubationHospital__c,IntubationHospital__r.name,Distributor__c,Distributor__r.name,PatientApply__r.Owner.Alias, CreatedDate,Status__c,PM_Patient_Phone__c,PM_Patient_MPhone__c,PM_Famliy_Member_Phone__c,PM_Family_Member_Mphone__c,PM_Famliy_Phone__c,PM_Other_Phone__c,Address__c,PM_NewPatientApprover__c,PM_ApproverDate__c,RejectReason__c,PM_PSR__c FROM PatientApply__c WHERE Status__c = \'待审批\' ';
    private static final integer PAGE_SIZE = 10;
    private static final integer LIMIT_SIZE = 1000;
    public PatientApply__c patientApply{get;set;}
    
    public list<PatientApply__c> queryResultnew = new list<PatientApply__c>();
    public set<string> set_newname = new set<string>();//存放新病人姓名
    public set<string> set_newtel = new set<string>();//存放新病人电话
    public set<string> set_newaddress = new set<string>();//存放新病人地址
    public set<string> set_name = new set<string>();//存放病人姓名
    public set<string> set_tel = new set<string>();//存放病人电话
    public set<string> set_address = new set<string>();//存放病人地址
    public list<PM_Patient__c> queryResult = new list<PM_Patient__c>();
    public integer result{get;set;}//结果个数
    
    public list<InProjectWrapper> list_repeat{get;set;}
    public void getList()
    {
        list_repeat = new list<InProjectWrapper>();
        queryResultnew = (list<PatientApply__c>)conset.getRecords();
        if(queryResultnew.size()!=null && queryResultnew.size() > 0)
        {
            for(PatientApply__c p:queryResultnew)
            {
                //set_INids.add(p.IntubationHospital__c);
                if(p.PatientName__c != '' && p.PatientName__c != null)
                {
                    set_newname.add(p.PatientName__c);
                }
                if(p.PM_Patient_Phone__c != '' && p.PM_Patient_Phone__c != null)
                {
                    set_newtel.add(p.PM_Patient_Phone__c);
                }
                if(p.PM_Patient_MPhone__c != '' && p.PM_Patient_MPhone__c != null)
                {
                    set_newtel.add(p.PM_Patient_MPhone__c);
                }
                if(p.PM_Famliy_Member_Phone__c != '' && p.PM_Famliy_Member_Phone__c != null)
                {
                    set_newtel.add(p.PM_Famliy_Member_Phone__c);
                }
                if(p.PM_Family_Member_Mphone__c != '' && p.PM_Family_Member_Mphone__c != null)
                {
                    set_newtel.add(p.PM_Family_Member_Mphone__c);
                }
                if(p.PM_Famliy_Phone__c != '' && p.PM_Famliy_Phone__c != null)
                {
                    set_newtel.add(p.PM_Famliy_Phone__c);
                }
                if(p.PM_Other_Phone__c != '' && p.PM_Other_Phone__c != null)
                {
                    set_newtel.add(p.PM_Other_Phone__c);
                }
                if(p.Address__c != '' && p.Address__c != null)
                {
                    set_newaddress.add(p.Address__c);
                }
            }
        }
        /*2014-03-19 Sunny注释掉，不再进行查询，使用新病人提交上的 存在疑似病人 字段进行判断
        queryResult = [SELECT Name,PM_Address__c,PM_PmTel__c,PM_FamilyTel__c,PM_PmPhone__c,PM_FamilyPhone__c,PM_HomeTel__c,PM_OtherTel2__c,PM_Status__c FROM PM_Patient__c WHERE Name in: set_newname or PM_HomeTel__c in: set_newtel  or PM_PmPhone__c in: set_newtel  or PM_PmTel__c in: set_newtel  or PM_FamilyPhone__c in: set_newtel  or PM_FamilyTel__c in: set_newtel or PM_OtherTel2__c in: set_newtel or PM_Address__c in: set_newaddress ];
        //system.debug('result size:'+queryResult.size());
        if(queryResult.size()!=null && queryResult.size() > 0)
        {
            for(PM_Patient__c pa:queryResult)
            {
                if(pa.Name != '' && pa.Name != null)
                {
                    set_name.add(pa.Name);
                }
                if(pa.PM_HomeTel__c != '' && pa.PM_HomeTel__c != null)
                {
                    set_tel.add(pa.PM_HomeTel__c);
                }
                if(pa.PM_PmTel__c != '' && pa.PM_PmTel__c != null)
                {
                    set_tel.add(pa.PM_PmTel__c);
                }
                if(pa.PM_PmPhone__c != '' && pa.PM_PmPhone__c != null)
                {
                    set_tel.add(pa.PM_PmPhone__c);
                }
                if(pa.PM_FamilyPhone__c != '' && pa.PM_FamilyPhone__c != null)
                {
                    set_tel.add(pa.PM_FamilyPhone__c);
                }
                if(pa.PM_FamilyTel__c != '' && pa.PM_FamilyTel__c != null)
                {
                    set_tel.add(pa.PM_FamilyTel__c);
                }
                if(pa.PM_OtherTel2__c != '' && pa.PM_OtherTel2__c != null)
                {
                    set_tel.add(pa.PM_OtherTel2__c);
                }
                if(pa.PM_Address__c != '' && pa.PM_Address__c != null)
                {
                    set_address.add(pa.PM_Address__c);
                }
            }
        }
        */
        for(PatientApply__c np:queryResultnew)
        {
            
            InProjectWrapper Inpw = new InProjectWrapper();
            /*2014-03-19 Sunny注释掉，不再进行查询，使用新病人提交上的 存在疑似病人 字段进行判断
            if(set_name.contains(np.PatientName__c))
            {
                Inpw.detail='详情';
                Inpw.color='#D46049;';
            }
            else if(set_address.contains(np.Address__c))
            {
                Inpw.detail='详情';
                Inpw.color='#D46049;';
            }
            else if(set_tel.contains(np.PM_Patient_Phone__c) || set_tel.contains(np.PM_Patient_MPhone__c) || set_tel.contains(np.PM_Famliy_Member_Phone__c) || set_tel.contains(np.PM_Family_Member_Mphone__c) || set_tel.contains(np.PM_Famliy_Phone__c) || set_tel.contains(np.PM_Other_Phone__c))
            {
                Inpw.detail='详情';
                Inpw.color='#D46049;';
            }
            else
            {
                Inpw.detail='';
                Inpw.color='';
            }
            */
            if(np.PM_HaveDuplicatePatient__c){
            	Inpw.detail='详情';
                Inpw.color='#D46049;';
            }else{
            	Inpw.detail='';
                Inpw.color='';
            }
            Inpw.pat = np;
            if(np.PM_Patient_Phone__c!='' && np.PM_Patient_Phone__c!=null)
            {
                Inpw.tel= np.PM_Patient_Phone__c;
            }
            if(np.PM_Patient_MPhone__c!='' && np.PM_Patient_MPhone__c!=null)
            {
                if(Inpw.tel=='' || Inpw.tel==null)
                {
                    Inpw.tel= np.PM_Patient_MPhone__c;
                }
                else
                {
                    Inpw.tel+=','+np.PM_Patient_MPhone__c;
                }
            }
            if(np.PM_Famliy_Member_Phone__c!='' && np.PM_Famliy_Member_Phone__c!=null)
            {
                if(Inpw.tel=='' || Inpw.tel==null)
                {
                    Inpw.tel= np.PM_Famliy_Member_Phone__c;
                }
                else
                {
                    Inpw.tel+=','+np.PM_Famliy_Member_Phone__c;
                }
            }
            if(np.PM_Family_Member_Mphone__c!='' && np.PM_Family_Member_Mphone__c!=null)
            {
                if(Inpw.tel=='' || Inpw.tel==null)
                {
                    Inpw.tel= np.PM_Family_Member_Mphone__c;
                }
                else
                {
                    Inpw.tel+=','+np.PM_Family_Member_Mphone__c;
                }
            }
            if(np.PM_Famliy_Phone__c!='' && np.PM_Famliy_Phone__c!=null)
            {
                if(Inpw.tel=='' || Inpw.tel==null)
                {
                    Inpw.tel= np.PM_Famliy_Phone__c;
                }
                else
                {
                    Inpw.tel+=','+np.PM_Famliy_Phone__c;
                }
            }
            if(np.PM_Other_Phone__c!='' && np.PM_Other_Phone__c!=null)
            {
                if(Inpw.tel=='' || Inpw.tel==null)
                {
                    Inpw.tel= np.PM_Other_Phone__c;
                }
                else
                {
                    Inpw.tel+=','+np.PM_Other_Phone__c;
                }
            }
            Inpw.isCheck = false;
            list_repeat.add(Inpw);
        }
        result = list_repeat.size();
    }
    public String Reject_Reason{get;set;}
    public Boolean Btn_disabled{get;set;}
    //插管医院
    //private Set<ID> set_INids = new Set<ID>();
    public PM_ApplyNewPatientsController(ApexPages.StandardController controller)
    {
        InitApplyNewPatientsPaper();
    }
    
    //表示行的封装类
    public class InProjectWrapper
    {
        public PatientApply__c pat{get;set;}
        public String detail{get;set;}//详细按钮
        public String color{get;set;}//颜色
        public boolean isCheck{set;get;}
        public String tel{get;set;}//电话
        public boolean isPSRCheck
        {
        	get
        	{
        		if(pat.PM_PSR__c!=null)
                {
                	return true;
                }
                else
                {
                	return false;
                }
        	}
        	set{}
        }
    }
     // 分页字段 
    public Boolean hasPrevious {get {return conset.getHasPrevious();}set;}
    public Boolean hasNext {get {return conset.getHasNext();}set;}
    public Integer pageNumber {get {return conset.getPageNumber();}set;}
    public Integer categoryNumber {get {return conset.getResultSize();}set;}
    public ApexPages.StandardSetController conset //实现分页功能 ApexPages.StandardSetController
    {
        get 
        {
            if(conset == null)
            {
            	system.debug('@@@@@@@@@@'+userinfo.getUserRoleId());
            	UserRole UserRoleName = [SELECT Id, Name FROM UserRole where Id =: userinfo.getUserRoleId()];
            	if(UserRoleName !=null)
            	{
            		system.debug('@@@@@@@@@@'+UserRoleName.Name);
	            	if(UserRoleName.Name.toUpperCase().contains('PSR ADMIN') || UserRoleName.Name.toUpperCase().contains('PM-PSR MANAGER') || UserRoleName.Name.toUpperCase().contains('PM-MANAGER') || UserRoleName.Name.toUpperCase().contains('ADMIN'))
	            	{
	            		conset = getResult(query + ' order by CreatedDate');
	            	}
	            	else if(UserRoleName.Name.toUpperCase().contains('PM-VICE MANAGER GZ') )
	            	{
	            		conset = getResult(query + 'and PM_PSR__r.UserRole.name in (\'PM-Vice Manager GZ\',\'PM-Service Specialist GZ\',\'PM-Edu Specialist GZ\') order by CreatedDate');
	            	}
	            	else if(UserRoleName.Name.toUpperCase().contains('PM-SUPERVISOR BJ'))
	            	{
	            		conset = getResult(query + 'and PM_PSR__r.UserRole.name in (\'PM-Supervisor BJ\',\'PM-Service Specialist BJ\',\'PM-Edu Specialist BJ\') order by CreatedDate');
	            	}
	            	else if(UserRoleName.Name.toUpperCase().contains('PM-VICE MANAGER SH'))
	            	{
	            		conset = getResult(query + 'and PM_PSR__r.UserRole.name in (\'PM-Vice Manager SH\',\'PM-Service Specialist SH\',\'PM-Edu Specialist SH\') order by CreatedDate');
	            	}
	            	else if(UserRoleName.Name.toUpperCase().contains('PM-SERVICE SPECIALIST GZ') || UserRoleName.Name.toUpperCase().contains('PM-EDU SPECIALIST GZ') || UserRoleName.Name.toUpperCase().contains('PM-SERVICE SPECIALIST BJ') || UserRoleName.Name.toUpperCase().contains('PM-EDU SPECIALIST BJ') || UserRoleName.Name.toUpperCase().contains('PM-SERVICE SPECIALIST SH') || UserRoleName.Name.toUpperCase().contains('PM-EDU SPECIALIST SH'))
	            	{
	            		conset = getResult(query + 'and PM_PSR__c = \''+ userinfo.getUserId() + '\' order by CreatedDate');
	            	}
	            	else
	            	{
	            		conset = getResult(query + 'and PM_PSR__c = \''+ userinfo.getUserId() + '\' order by CreatedDate');
	            	}
	                conset.setPageSize(PAGE_SIZE);
            	}
            }
            return conset;
        }
        set;
    }
     //分页方法
    public void first() {conset.first();getList();}
    public void last() {conset.last();getList();}
    public void previous() {conset.previous();getList();}
    public void next() {conset.next();getList();}
    public ApexPages.StandardSetController getResult(string patSql)
    {
        ApexPages.StandardSetController consetNew = new ApexPages.StandardSetController(Database.getQueryLocator(patSql+' limit '+LIMIT_SIZE));
        consetNew.setPageSize(PAGE_SIZE);
        return consetNew;
    }
    //同意按钮
    public void Agree()
    {
        //getPDCurrentSale();
        //getCurrentPSR();
        system.debug('***************agree*******************');
        list<PatientApply__c> pa_list = new list<PatientApply__c>();
        for(InProjectWrapper lr:list_repeat)
        {
            system.debug('***************lr.isCheck*******************'+lr.isCheck);
            if(lr.isCheck==true)
            {
            	system.debug('***************lr.isPSRCheck*******************'+lr.isPSRCheck);
            	//isCheckss = true;
            	if(lr.isPSRCheck==true)
            	{
                	pa_list.add(lr.pat);
            	}
            	else
            	{
            		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR , '新病人没有负责PSR不能进行同意审批') ;            
                	ApexPages.addMessage(msg);
            	}
            }
        }
        //list<PM_Patient__c> patientList = new list<PM_Patient__c>();
        for(PatientApply__c palist:pa_list)
        {
            palist.Status__c = '通过';
            palist.PM_Converted__c = true;
            palist.PM_NewPatientApprover__c = userinfo.getUserId();
            palist.PM_ApproverDate__c = datetime.now();
            palist.PM_Converted__c = true;
            //patientList.add(convertPatient(palist));
        }
        try
        {
        	update pa_list;
        }
        catch(DmlException ex)
        {
            ApexPages.addMessages(ex);
        }
        Check();
        //if(patientList.size() > 0)
        //{
        //  insert patientList;
        //}
    }
    
    //拒绝按钮
    public void Refuse()
    {
        list<PatientApply__c> pa_list = new list<PatientApply__c>();
        for(InProjectWrapper lr:list_repeat)
        {
            if(lr.isCheck==true)
            {
                pa_list.add(lr.pat);
            }
        }
        for(PatientApply__c palist:pa_list)
        {
            palist.Status__c = '拒绝';
            palist.RejectReason__c = Reject_Reason;
            palist.PM_NewPatientApprover__c = userinfo.getUserId();
            palist.PM_ApproverDate__c = datetime.now();
        }
        try
        {
			update pa_list;
        }
        catch(DmlException ex)
        {
            ApexPages.addMessages(ex);
        }
        Reject_Reason = '';
        Check();
    }
    //初始化新病人审批记录
    public void InitApplyNewPatientsPaper()
    {
        Reject_Reason = null;
        patientApply = new PatientApply__c();
        String profileName = [Select Id,Name From Profile Where Id =: Userinfo.getProfileId()].Name;
        String RoleDevName = [Select Id,Name,DeveloperName From UserRole Where id =: UserInfo.getUserRoleId()].name;
        if(RoleDevName.toUpperCase().contains('PM-MANAGER') 
            || RoleDevName.toUpperCase().contains('PM-PSR ADMIN')
            || RoleDevName.toUpperCase().contains('PM-SUPERVISOR BJ')
            || RoleDevName.toUpperCase().contains('PM-VICE MANAGER GZ')
            || RoleDevName.toUpperCase().contains('PM-VICE MANAGER SH'))
        {
        	Btn_disabled = false;
        }else
        {
        	Btn_disabled = true;
        }
        if(profileName.contains('系统管理员')){
        	Btn_disabled = false;
        }      
        getList();
        
    }
    
    /*
    //获取当前的PD部门的负责的销售
    private Map<ID,ID> map_salePD;
    private void getPDCurrentSale()
    {
         map_salePD = NEW Map<ID,ID>();
        for(V2_Account_Team__c team : [Select v.V2_User__c , v.V2_Account__c  From V2_Account_Team__c v where UserProduct__c =  'PD' 
                                       AND V2_History__c = false AND V2_ApprovalStatus__c = '审批通过' AND V2_LastAccessDate__c < TODAY AND V2_Account__c IN : set_INids])
        {
            map_salePD.put(team.V2_Account__c,team.V2_User__c);
        }
    }
    
    //获取当前的PSR部门的负责的PSR
    private Map<ID,ID> map_salePSR;
    private void getCurrentPSR()
    {
        map_salePSR = NEW Map<ID,ID>();
        for(PM_PSRRelation__c psr : [Select p.PM_PSR__c , p.PM_Hospital__c  From PM_PSRRelation__c p where  
                                      PM_Operate__c = '新增' AND PM_IsHistory__c = false AND PM_UneffectDate__c = null AND PM_Hospital__c IN : set_INids])
        {
            map_salePSR.put(psr.PM_Hospital__c,psr.PM_PSR__c);
        }
    }
    */
    //联动大区、省份、城市、县
    public string BigArea{get;set;}
    public string Province{get;set;}
    public string City{get;set;}
    public string County{get;set;}
    public string InHospital{get;set;}//插管医院
    private list<string> countys;//插管医院 县：PM_InHospital__rPM_County__c
    private list<string> Citys;//插管医院 城市：PM_InHospital__r.PM_City__c  
    private list<string> InHospitals;//插管医院多选
    private list<string> Provinces; //插管医院 省份：PM_InHospital__r.PM_Province__c  
    private list<string> BigAreas;//插管医院 区域：PM_Hospital_Region__c 
    
    public void Check()
    {
        system.debug('******************chek*******************');
        string patSql = query;
        if(InHospital != null && InHospital != ''&& InHospital != 'Select options')
        {
            InHospitals = new list<string>();
            if(InHospital.contains(','))
            {
                InHospitals.addAll(InHospital.split(', '));
            }
            else
            {
                InHospitals.add(InHospital);
            }
            patSql += ' and IntubationHospital__r.Name in:InHospitals ';
        }
        if(County != null && County != ''&& County != 'Select options')
        {
            countys = new list<string>();
            if(County.contains(','))
            {
                countys.addAll(County.split(', '));
            }
            else
            {
                countys.add(County);
            }
            patSql += ' and IntubationHospital__r.Country__r.Name in:countys ';
        }
        else if(City != null && City != '' && City != 'Select options')
        {
            Citys = new list<string>();
            if(City.contains(','))
            {
                Citys.addAll(City.split(', '));
            }
            else
            {
                Citys.add(City);
            }
            patSql += ' and IntubationHospital__r.Cities__r.Name in:Citys ';
        }
        else if(Province != null && Province != ''&& Province != 'Select options')
        {
            Provinces = new list<string>();
            if(Province.contains(','))
            {
                Provinces.addAll(Province.split(', '));
            }
            else
            {
                Provinces.add(Province);
            }
            patSql += ' and IntubationHospital__r.PM_Province__r.Name in:Provinces ';
        }
        else if(BigArea != null && BigArea != ''&& BigArea != 'Select options')
        {
            BigAreas = new list<string>();
            if(BigArea.contains(','))
            {
                BigAreas.addAll(BigArea.split(', '));
            }
            else
            {
                BigAreas.add(BigArea);
            }
            patSql += ' and IntubationHospital__r.Region__r.Name in:BigAreas ';
        }
        UserRole UserRoleName = [SELECT Id, Name FROM UserRole where Id =: userinfo.getUserRoleId()];
    	if(UserRoleName.Name.toUpperCase().contains('PSR ADMIN') || UserRoleName.Name.toUpperCase().contains('PM-PSR MANAGER') || UserRoleName.Name.toUpperCase().contains('PM-MANAGER') || UserRoleName.Name.toUpperCase().contains('ADMIN'))
    	{
    		patSql += ' order by CreatedDate' ;
    	}
    	else if(UserRoleName.Name.toUpperCase().contains('PM-VICE MANAGER GZ') )
    	{
			conset = getResult(query + 'and PM_PSR__r.UserRole.name in (\'PM-Vice Manager GZ\',\'PM-Service Specialist GZ\',\'PM-Edu Specialist GZ\') order by CreatedDate');
    	}
    	else if(UserRoleName.Name.toUpperCase().contains('PM-SUPERVISOR BJ'))
    	{
			conset = getResult(query + 'and PM_PSR__r.UserRole.name in (\'PM-Supervisor BJ\',\'PM-Service Specialist BJ\',\'PM-Edu Specialist BJ\') order by CreatedDate');
    	}
    	else if(UserRoleName.Name.toUpperCase().contains('PM-VICE MANAGER SH'))
    	{
			conset = getResult(query + 'and PM_PSR__r.UserRole.name in (\'PM-Vice Manager SH\',\'PM-Service Specialist SH\',\'PM-Edu Specialist SH\') order by CreatedDate');
    	}
    	else if(UserRoleName.Name.toUpperCase().contains('PM-SEVERVICE SPECIALIST GZ') || UserRoleName.Name.toUpperCase().contains('PM-EDU SPECIALIST GZ') || UserRoleName.Name.toUpperCase().contains('PM-SERVICE SPECIALIST BJ') || UserRoleName.Name.toUpperCase().contains('PM-EDU SPECIALIST BJ') || UserRoleName.Name.toUpperCase().contains('PM-SERVICE SPECIALIST SH') || UserRoleName.Name.toUpperCase().contains('PM-EDU SPECIALIST SH'))
    	{
    		patSql += 'and PM_PSR__c = \'' + userinfo.getUserId() + '\' order by CreatedDate' ;
    	}
    	else
    	{
    		patSql += 'and PM_PSR__c = \'' + userinfo.getUserId() + '\' order by CreatedDate' ;
    	}
    	
        system.debug('******************patSql*******************'+patSql);
        conset = getResult(patSql);
        getList();
        
    }
}