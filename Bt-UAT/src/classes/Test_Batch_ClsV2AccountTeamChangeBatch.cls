/**
 * Author : bill
 * ClsV2AccountTeamChangeBatch的测试类
 */
@isTest
private class Test_Batch_ClsV2AccountTeamChangeBatch {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        // TO DO: implement unit test
        //user role
        List<UserRole> listur= new List<UserRole>();
        UserRole ur0 = new UserRole();
        ur0.Name = 'Renal-Supervisor-华南-IVT-Supervisor(U0)';
        listur.add(ur0);
        UserRole ur1 = new UserRole();
        ur1.Name = 'Renal-Supervisor-华南-SP-Supervisor(U1)';
        listur.add(ur1);
        UserRole ur2 = new UserRole();
        ur2.Name = 'Renal-Rep-华南-PD-Rep(U2)';
        listur.add(ur2);
        insert listur;
        ur2.ParentRoleId=ur1.Id;
        update ur2;
        ur1.ParentRoleId = ur0.id;
        update ur1;
        //user 
        list<User> user=[Select TimeZoneSidKey,ProfileId,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname, Alias From User Where IsActive = true limit 1];
        List<User> list_user = new List<User>();
        User user0=new User();
        user0.username='user0r2@023.com';
        user0.LastName='user0r2';
        user0.Email='user0r2@023.com';
        user0.Alias=user[0].Alias;
        user0.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user0.ProfileId=UserInfo.getProfileId();
        user0.LocaleSidKey=user[0].LocaleSidKey;
        user0.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user0.EmailEncodingKey=user[0].EmailEncodingKey;
        user0.CommunityNickname='chequ23';
        user0.MobilePhone='02345678902';
        user0.IsActive = true;
        user0.UserRoleId=ur0.Id ;
        user0.Alias='zhangsan';
        list_user.add(user0);
        User user1=new User();
        user1.username='user1r2@123.com';
        user1.LastName='user1r2';
        user1.Email='user1r2@123.com';
        user1.Alias=user[0].Alias;
        user1.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user1.ProfileId=UserInfo.getProfileId();
        user1.LocaleSidKey=user[0].LocaleSidKey;
        user1.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user1.EmailEncodingKey=user[0].EmailEncodingKey;
        user1.CommunityNickname='chequ';
        user1.MobilePhone='12345678912';
        user1.Alias='aaaa';
        user1.IsActive = true;
        user1.UserRoleId=ur0.Id ;
        list_user.add(user1);
        User user2=new User();
        user2.username='user2r2@223.com';
        user2.LastName='user2r2';
        user2.Email='user2r2@223.com';
        user2.Alias=user[0].Alias;
        user2.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user2.ProfileId=UserInfo.getProfileId();
        user2.LocaleSidKey=user[0].LocaleSidKey;
        user2.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user2.EmailEncodingKey=user[0].EmailEncodingKey;
        user2.CommunityNickname='chequ1';
        user2.MobilePhone='22345678922';
        user2.IsActive = true;
        user2.UserRoleId=ur1.Id ;
        user2.Alias ='bbb';
        list_user.add(user2);
        insert list_user;
        
        Account acc = new Account();
        acc.Name = '123';
        insert acc;
        //md医院信息
        IVSHospitalInfo__c ivt = new IVSHospitalInfo__c();
        ivt.Year__c = string.valueOf(date.today().year());
        ivt.Account__c = acc.Id;
        insert ivt;
        //SP信息
        CCHospitalInfo__c cc = new CCHospitalInfo__c();
        cc.Year__c = string.valueOf(date.today().year());
        cc.Account__c = acc.Id;
        insert cc;
        
		List<V2_Account_Team__c> list_team = new List<V2_Account_Team__c>();
		V2_Account_Team__c team1 = new V2_Account_Team__c();
		team1.V2_BatchOperate__c = '新增';
		team1.V2_Account__c = acc.Id;
		team1.V2_Effective_Year__c = string.valueOf(date.today().year());
		team1.V2_Effective_Month__c = string.valueOf(date.today().month());
		team1.V2_User__c = user1.Id;
		team1.IsPending__c = true;
		list_team.add(team1);
		V2_Account_Team__c team2 = new V2_Account_Team__c();
		team2.V2_BatchOperate__c = '替换';
		team2.V2_Account__c = acc.Id;
		team2.V2_Effective_Year__c = string.valueOf(date.today().year());
		team2.V2_Effective_Month__c = string.valueOf(date.today().month());
		team2.V2_User__c = user2.Id;
		team2.IsPending__c = true;
		list_team.add(team2);
		insert list_team;
		
		List<Opportunity> list_opp = new List<Opportunity>();
        Opportunity opp = new Opportunity();
        opp.Name = 'test';
        opp.AccountId = acc.Id;
        opp.StageName = '导入期';
        opp.CloseDate = date.today();
        opp.RecordTypeId = [Select r.Id, r.SobjectType From RecordType r where r.Name = 'IVT' and IsActive = true and r.SobjectType = 'Opportunity'][0].Id;
        list_opp.add(opp);
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'test';
        opp1.AccountId = acc.Id;
        opp1.StageName = '导入期';
        opp1.CloseDate = date.today();
        opp1.RecordTypeId = [Select r.Id, r.SobjectType From RecordType r where r.Name = 'IN Project' and IsActive = true and r.SobjectType = 'Opportunity'][0].Id;
        list_opp.add(opp1);
        insert list_opp;
        
        VaporizerInfo__c vap = new VaporizerInfo__c();
        vap.Name = '7-23';
        vap.OwnerId = user1.Id;
        vap.Hospital__c = acc.Id;
		insert vap;
		
        system.Test.startTest();
        ClsV2AccountTeamChangeBatch accountBatch = new ClsV2AccountTeamChangeBatch();
        Database.executeBatch(accountBatch, 10);
        system.Test.stopTest();
    }
}