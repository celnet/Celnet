/**
 * 作者:Bill
 * 说明：判断退回申请状态是否符合全部完成的条件
 * 1. 退回明细中已确认收货的的数量和申请中退回旧罐的数量相等
 * 2. 退回申请的发货明细中已确认收货的数量和申请中申请新罐的数量相等
**/
public class ReturnCompleted {
	
	public void IsReturnCompleted(Set<ID> returnApplyIds)
    {
    	system.debug(returnApplyIds+'klm');
    	//退回申请的发货明细中已确认收货的数量和申请中申请新罐的数量相等的退回申请
    	List<Vaporizer_ReturnAndMainten__c> list_CompletedReturn = new List<Vaporizer_ReturnAndMainten__c>();
    	//符合上条件，且退回明细中已确认收货的的数量和申请中退回旧罐的数量相等的退回申请
    	List<Vaporizer_ReturnAndMainten__c> list_FinishReturn = new List<Vaporizer_ReturnAndMainten__c>();
    	
       //发货明细
       List<Vaporizer_Application_Detail__c> list_return = [select v.Id, v.Vaporizer_ReturnAndMainten__c from Vaporizer_Application_Detail__c v where v.Vaporizer_ReturnAndMainten__c in : returnApplyIds and v.IsReceived__c = true];
       //退回申请
       List<Vaporizer_ReturnAndMainten__c> list_ret = [Select v.Id, v.Vaporizer_Return_Quantity__c, v.Vaporizer_Apply_Quantity__c, v.Approve_Result__c From Vaporizer_ReturnAndMainten__c v where v.Id in:returnApplyIds];
       //退回明细
       List<Vaporizer_ReturnAndMainten_Detail__c> list_returnDetail = [Select v.Vaporizer_ReturnAndMainten__c, v.IsReceived__c From Vaporizer_ReturnAndMainten_Detail__c v where v.Vaporizer_ReturnAndMainten__c in : returnApplyIds and v.IsReceived__c = true];
       

          for(Vaporizer_ReturnAndMainten__c ret : list_ret){
          	if(ret.Vaporizer_Apply_Quantity__c > 0)
          	{
      	       double sumRece =0;
      	       for(Vaporizer_Application_Detail__c returns : list_return)
               {
        	       if(ret.Id == returns.Vaporizer_ReturnAndMainten__c)
        	       {
           	  	       sumRece++;
           	       }
               }
               system.debug(sumRece+'klm2');
               if(sumRece == ret.Vaporizer_Apply_Quantity__c)
               {
        	       list_CompletedReturn.add(ret);
               }
           }else if(list_returnDetail.size()>0 && list_returnDetail != null){
          		for(Vaporizer_ReturnAndMainten__c comRet : list_ret){
      	          double sumRece =0;
      	          for(Vaporizer_ReturnAndMainten_Detail__c returnDetail : list_returnDetail)
                  {
        	        if(comRet.Id == returnDetail.Vaporizer_ReturnAndMainten__c)
        	        {
           	  	       sumRece++;
           	        }
                  }
                  system.debug(sumRece+'klm3');
                  if(sumRece == comRet.Vaporizer_Return_Quantity__c)
                  {
             	      comRet.Approve_Result__c = '全部完成';
        	          list_FinishReturn.add(comRet);
                  }
               }
          	}
         }
      
        if(list_CompletedReturn!=null && list_CompletedReturn.size()>0)
        {
          for(Vaporizer_ReturnAndMainten__c comRet : list_CompletedReturn){
      	     double sumRece =0;
      	     for(Vaporizer_ReturnAndMainten_Detail__c returnDetail : list_returnDetail)
             {
        	     if(comRet.Id == returnDetail.Vaporizer_ReturnAndMainten__c)
        	     {
           	  	     sumRece++;
           	     }
             }
             if(sumRece == comRet.Vaporizer_Return_Quantity__c)
             {
             	 comRet.Approve_Result__c = '全部完成';
        	     list_FinishReturn.add(comRet);
             }
          }
        }
        if(list_FinishReturn != null && list_FinishReturn.size() > 0)
        {
           update list_FinishReturn;
        }
    }
}