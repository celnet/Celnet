/*
*功能：该类主要功能是先从事件(Event)和月计划(MonthlyPlan__c)中取值然后计算出台灣KPI(TW_KPI__c)对象中各个字段所需要的值，
*	       然后插入对象台灣KPI中
*作者：Alisa
*时间：2013年11月28日
*2014-2-10修改：'當月完成且未過期的拜訪HCP數' 在汇总此值时 需注意按人进行汇总而不是直接统计拜访的次数。
*/
global class TW_SalesManKPIBatch implements Database.Batchable<sObject>{
	global String strYear;
	global String strMonth;
	//已完成未过期事件中联系人的所屬科別是Oncology/CRS/GS/ICU/Trama/NST
	global final set<String> SETDEPARTMENT = new set<String>{'血液科','腫瘤內科','一般外科','ICU','大腸直腸外科'};
	//当事件中联系人的INfusor細分類型是巴士/貨輪/計程車或Nutrition細分類型是Olive King/Olive Follower/Fish King
	global final set<String> SETPRODUTDETAILTYPE = new set<String>{'巴士','貨輪','計程車','Olive King','Olive Follower','Fish King'};
	
	//存放当月销售代表拜访（已完成未过期）的联系人所属科别是“Oncology/CRS/GS/ICU/Trama/NST”这些中时的拜访数即事件数
	global Integer intDepartNum; 
	//存放销售代表拜访（已完成未过期）的联系人的“Infusor细分类型”值为巴士/货轮/计程车；或者“Nutrition细分类型”值为Olive King/Olive Follower/Fish King的拜访数即事件数
	global Integer intProductDetailTypeNum; 
	//存放销售代表拜访（已完成未过期）中拜訪內容及分析不为空时的拜访数即事件数
	global Integer intVisitedNum;
	//存放销售代表已完成未过期的拜访中拜訪內容及分析不为空且拜訪類型值为“SPIN Call”时的拜访数即事件数
	global Integer intSPINCallNum; 
	//存放完成未过期事件中联系人的个数
	global Integer intHCPVisitedNum;
	global Set<Id> Set_HCPVisted = new Set<Id>();//'當月完成且未過期的拜訪HCP數' 在汇总此值时 需注意按人进行汇总而不是直接统计拜访的次数。
	//存放一个销售代表的月拜访数
	global Decimal DcmTotalNum;
	//存放要插入的对象---台灣KPI(TW_KPI__c)
	global list<TW_KPI__c> listTWKPI = new list<TW_KPI__c>();
	
	global TW_SalesManKPIBatch()
	{
		intDepartNum = 0;
		intProductDetailTypeNum = 0;
		intVisitedNum = 0;
		intSPINCallNum = 0;
		intHCPVisitedNum = 0;
		DcmTotalNum = 0;
	}
	
	global Iterable<sObject> start(Database.BatchableContext BC)
	{
		set<Id> setTWUserRoleId = new set<Id>(); //存放是台湾角色的Id，即角色已“TW”开头的角色Id
		
		//获取角色以以“TW”开头且不是“TW BU Head”的所有启用的用户
		list<User> listTWUser = [Select u.Id, u.UserRoleId, u.Name, u.IsActive 
								 From User u 
								 where u.IsActive = true and UserRole.Name like 'TW%' and UserRole.Name != 'TW BU Head'];
		return listTWUser;
	}
	
	global void execute(Database.BatchableContext BC,List<sObject> scope)
	{                        
		string day = '1';
		string hour = '0';
		string minute = '0';
		string second = '0';
		String strCurrentTime = strYear + '-'+ strMonth + '-' + day + ' ' + hour + ':' + minute + ':'+ second;
		//当前月的第一天：yyyy-MM-01 0:0:0
		Datetime CurrentStartDatetime = Datetime.valueOf(strCurrentTime);
		//下个月的第一天:yyyy-MM-01 0:0:0
		Datetime CurrentEndDatetime = Datetime.valueOf(strCurrentTime).addMonths(1);
		
		//存放事件所有人是以“TW”开头记录类型为“拜访”，行动类型为“拜访”，WhoId不为空的所有完成未过期的当月事件
		list<Event> listEvent = new list<Event>();
		//存放事件所有人是以“TW”开头记录类型为“拜访”，行动类型为“拜访”，WhoId不为空,行动细分类型为SPIN Call的所有完成未过期的当月事件
		list<Event> listSPINCallEvent = new list<Event>();
		set<Id> setContactId = new set<Id>();//存放完成未过期事件上联系人的Id
		
		//获取事件所有人角色是以“TW”开头记录类型为“拜访”，行动类型为“拜访”，WhoId不为空的当月所有事件
		list<Event> listAllEvents = [Select e.Id, e.WhoId, e.StartDateTime, e.SubjectType__c, e.TW_VisitType__c,  
			   					        e.Done__c, e.V2_IsExpire__c, e.RecordTypeId, e.OwnerId,e.Description 
									 From Event e 
									 where e.SubjectType__c = '拜访' And e.WhoId != null And e.OwnerId IN :scope
									       And e.StartDateTime >=: CurrentStartDatetime And e.StartDateTime <: CurrentEndDatetime
									       And RecordType.SobjectType = 'Event' And RecordType.DeveloperName = 'V2_Event'];
									       
		//获取月计划事件所有人角色是以“TW”开头的销售代表当月的月计划
		list<MonthlyPlan__c> listMonthPlan = [Select m.V2_FinishedCallRecords__c, m.Percent__c, m.OwnerId, m.Id,m.Year__c, m.Month__c 
												 From MonthlyPlan__c m
												 where m.OwnerId IN :scope And m.Year__c = :strYear And m.Month__c = :strMonth];							       
								       
		//存放事件所有人的Id和其对应的所有事件
		map<Id,list<Event>> mapUserIdAndAllEvents = new map<Id,list<Event>>();
		//存放已完成未过期事件所有人的Id和其对应的所有事件
		map<Id,list<Event>> mapUserIdAndEvents = new map<Id,list<Event>>();
		//存放已完成未过期SPIN Call事件所有人的Id和其对应的所有事件
		map<Id,list<Event>> mapUserIdAndSPINCallEvents = new map<Id,list<Event>>();
		//存放月计划所有人的Id和其对应的月计划
		map<Id,MonthlyPlan__c> mapUserIdAndMonthlyPlans = new map<Id,MonthlyPlan__c>();
		
		
		/*
			1)获得事件所有人角色是以“TW”开头的销售代表的所有完成未过期的当月事件和所有完成未过期SPIN Call的当月事件
			2)向mapUserIdAndAllEvents、mapUserIdAndEvents、mapUserIdAndSPINCallEvents放值并取得联系人的Id
		*/
		if(listAllEvents !=null && listAllEvents.size()>0)
		{
			for(Event e : listAllEvents)
			{
				//向mapUserIdAndAllEvents放值
				if(mapUserIdAndAllEvents.containsKey(e.OwnerId))
				{
					mapUserIdAndAllEvents.get(e.OwnerId).add(e);
				}
				else 
				{
					list<Event> listMapEvents = new list<Event>();
					listMapEvents.add(e);
					mapUserIdAndAllEvents.put(e.OwnerId,listMapEvents);
				}
				//向mapUserIdAndEvents放值
				if(e.Done__c == true && e.V2_IsExpire__c == false)
				{
					if(mapUserIdAndEvents.containsKey(e.OwnerId))
					{
						mapUserIdAndEvents.get(e.OwnerId).add(e);
					}
					else 
					{
						list<Event> listMapEvents = new list<Event>();
						listMapEvents.add(e);
						mapUserIdAndEvents.put(e.OwnerId,listMapEvents);
					}
					listEvent.add(e);
					//取得事件是完成未过期的联系人的Id
					setContactId.add(e.WhoId);
				}
				//向mapUserIdAndSPINCallEvents放值
				if(e.Done__c == true && e.V2_IsExpire__c == false && e.TW_VisitType__c == 'SPIN Call')
				{
					if(mapUserIdAndSPINCallEvents.containsKey(e.OwnerId))
					{
						mapUserIdAndSPINCallEvents.get(e.OwnerId).add(e);
					}
					else 
					{
						list<Event> listMapEvents = new list<Event>();
						listMapEvents.add(e);
						mapUserIdAndSPINCallEvents.put(e.OwnerId,listMapEvents);
					}
					listSPINCallEvent.add(e);
				}
			}
		}
		
		//获取月计划所有人的Id和其对应的月计划
		if(listMonthPlan != null && listMonthPlan.size()>0)
		{
			for(MonthlyPlan__c mp : listMonthPlan)
			{
				if(!mapUserIdAndMonthlyPlans.containsKey(mp.OwnerId))
				{
					mapUserIdAndMonthlyPlans.put(mp.OwnerId,mp);
				}
			}
		}		
		
		//存放事件Id和其对应的联系人
		map<Id,Contact> mapEventIdAndContact = new map<Id,Contact>([Select c.TW_NutritionType__c, c.TW_INfusorType__c, c.TW_Department__c, c.Id 
											                       From Contact c
											                       Where c.Id IN :setContactId]);
		
		//获得记录类型为TW_Contact，所有人角色以以“TW”开头且不是“TW BU Head”，醫師狀態等于有效地联系人
		list<Contact> listContact = [Select Id,Name,OwnerId,RecordTypeId,TW_DoctorStatus__c
									 From Contact
									 where OwnerId IN :scope And TW_DoctorStatus__c = '有效' 
									      And RecordType.SobjectType = 'Contact' And RecordType.DeveloperName = 'TW_Contact'];
									      
		//存放联系人所有人Id和其对应的联系人
		map<Id,list<Contact>> mapUserIdAndContacts = new map<Id,list<Contact>>();
		if(listContact != null && listContact.size()>0)
		{
			for(Contact con : listContact)
			{
				if(mapUserIdAndContacts.containsKey(con.OwnerId))
				{
					mapUserIdAndContacts.get(con.OwnerId).add(con);
				}
				else
				{
					list<Contact> listCon = new list<Contact>();
					listCon.add(con);
					mapUserIdAndContacts.put(con.OwnerId,listCon);
				}
			}
		}
		
		//遍历每个销售代表当月下的每个事件，当满足不同条件时在其对应条件上的变量加1   
		for(sObject user : scope)
		{
			//创建一个台湾KPI对象
			TW_KPI__c kpi = new TW_KPI__c();
			kpi.OwnerId = user.Id;
			kpi.TW_Salesman__c = user.Id;
			kpi.TW_Year__c = strYear;
			kpi.TW_Month__c = strMonth; 
			
			if(mapUserIdAndAllEvents != null && mapUserIdAndAllEvents.size()>0 && mapUserIdAndAllEvents.containsKey(user.Id))
			{
				kpi.TW_TotalPlanVisitEventNum__c = mapUserIdAndAllEvents.get(user.Id).size();
			}
			
			
			/*
			if(mapUserIdAndSPINCallEvents != null && mapUserIdAndSPINCallEvents.size()>0 && mapUserIdAndSPINCallEvents.containsKey(user.Id))
			{
				kpi.TW_HaveVisitedSPINCallEventNum__c = mapUserIdAndSPINCallEvents.get(user.Id).size();
			}
			*/
			if(mapUserIdAndContacts != null && mapUserIdAndContacts.size()>0 && mapUserIdAndContacts.containsKey(user.Id))
			{
				kpi.TW_ActiveHCPNum__c = mapUserIdAndContacts.get(user.Id).size();
			}
			
			/*
			该for循环主要用于从事件中获得一些KPI指标中的值：intDepartNum、intProductDetailTypeNum、intVisitedNum、intSPINCallNum、intHCPVisitedNum
			*/
			if(mapUserIdAndEvents !=null && mapUserIdAndEvents.size()>0 && mapUserIdAndEvents.containsKey(user.Id))
			{
				for(Event e : mapUserIdAndEvents.get(user.Id))
				{
					if(mapEventIdAndContact != null && mapEventIdAndContact.size()>0 && mapEventIdAndContact.containsKey(e.WhoId))
					{
						//向完成未过期事件中的联系人中的变量intHCPVisitedNum加1
						intHCPVisitedNum++;
						
						Set_HCPVisted.add(e.WhoId);
						
						//当事件中联系人的所屬科別是Oncology/CRS/GS/ICU/Trama/NST中时变量intDepartNum加1
						if(SETDEPARTMENT.contains(mapEventIdAndContact.get(e.WhoId).TW_Department__c))
						{
						   	intDepartNum++;
						}
						
						//当事件中联系人的INfusor細分類型是巴士/貨輪/計程車或Nutrition細分類型是Olive King/Olive Follower/Fish King时变量intProductDetailTypeNum加1
						if(SETPRODUTDETAILTYPE.contains(mapEventIdAndContact.get(e.WhoId).TW_INfusorType__c) || SETPRODUTDETAILTYPE.contains(mapEventIdAndContact.get(e.WhoId).TW_NutritionType__c))
						{
							intProductDetailTypeNum++;
						}
					}
					
					//当事件中拜訪內容及分析不为空时变量intVisitedNum加1
					if(e.Description != null)
					{
						intVisitedNum++;
					}
					
					//当事件中拜訪內容及分析不为空且拜訪類型值为“SPIN Call”时变量intSPINCallNum加1
					if(e.Description != null && e.TW_VisitType__c == 'SPIN Call')
					{
						intSPINCallNum++;
					}
				}
				kpi.TW_HaveVisitedEventNum__c = mapUserIdAndEvents.get(user.Id).size();
			}
			
			/*
			该主要用于从月计划中获得一些KPI指标中的值：月计划、拜访完成率、月拜访数
			*/
			if(mapUserIdAndMonthlyPlans != null && mapUserIdAndMonthlyPlans.size()>0 && mapUserIdAndMonthlyPlans.containsKey(user.Id))
			{
				DcmTotalNum = mapUserIdAndMonthlyPlans.get(user.Id).V2_FinishedCallRecords__c;
				kpi.TW_MonthPlan__c = mapUserIdAndMonthlyPlans.get(user.Id).Id;
				kpi.TW_VisitDonePercent__c = mapUserIdAndMonthlyPlans.get(user.Id).Percent__c;
				kpi.TW_MonthVisitAmount__c = DcmTotalNum;
			}
			
			
			//为台湾KPI对象的其他字段赋值
			kpi.TW_HCPDepartmentNum__c = intDepartNum;
			kpi.TW_ProductDetailTypeNum__c = intProductDetailTypeNum;
			kpi.TW_VisitedNum__c = intVisitedNum;
			kpi.TW_SPINCallNum__c = intSPINCallNum;
			//kpi.TW_VisitedHCPNum__c = intHCPVisitedNum;//2014-2-10修改：'當月完成且未過期的拜訪HCP數' 在汇总此值时 需注意按人进行汇总而不是直接统计拜访的次数。
			kpi.TW_VisitedHCPNum__c = Set_HCPVisted.size();
			//获得台湾ＫＰＩ记录的唯一标识－－－用户Ｉｄ＋年＋月
			String strSign = user.Id + strYear + strMonth;
			kpi.TW_RecordUniqueMark__c = strSign;
			listTWKPI.add(kpi);
			intDepartNum = 0;
			intProductDetailTypeNum = 0;
			intVisitedNum = 0;
			intSPINCallNum = 0;
			intHCPVisitedNum = 0;
			Set_HCPVisted.clear();
		}
		if(listTWKPI != null && listTWKPI.size()>0)
		{
			upsert listTWKPI TW_RecordUniqueMark__c;
		}
	}
	
	global void finish(Database.BatchableContext BC)
	{
		
	}
	//测试batch类---TW_SalesManKPIBatch
	 static testMethod void testKPIBatch() {
	 	system.Test.startTest();
	 	list<User> listUser = new list<User>();
	 	list<Event> listEvent = new list<Event>();
	 	list<Contact> listContact = new list<Contact>();
	 	
    	Profile p = [SELECT Id,Name FROM Profile WHERE Name='TW Sales Rep'];
    	UserRole userRole = [select Id,Name from UserRole where Name = 'TW South Sales Rep'];
		User user1 = new User();
		user1.Alias = 'newUser1'; 
		user1.Email='newuser1@testorg.com';
		user1.EmailEncodingKey='UTF-8';
		user1.LastName='Testing';
		user1.LanguageLocaleKey='en_US';
		user1.LocaleSidKey='en_US';
		user1.UserRoleId = userRole.Id;
		user1.ProfileId = p.Id;
 		user1.TimeZoneSidKey='America/Los_Angeles';
 		user1.UserName='nsdsdr1@testorg.com';
 		user1.IsActive = true;
    	listUser.add(user1);
    	User user2 = new User();
    	user2.Alias = 'newUser2'; 
		user2.Email='newuser2@testorg.com';
		user2.EmailEncodingKey='UTF-8';
		user2.LastName='Testing';
		user2.LanguageLocaleKey='en_US';
		user2.LocaleSidKey='en_US';
		user2.UserRoleId = userRole.Id;
		user2.ProfileId = p.Id;
 		user2.TimeZoneSidKey='America/Los_Angeles';
 		user2.UserName='newuser2@sdfef.com';
 		user2.IsActive = true;
    	listUser.add(user2);
    	insert listUser;
    	
    	system.runAs(user1)
    	{
    		String strCurrentTime = datetime.now().year() + '-'+ datetime.now().month() + '-' + 1 + ' ' + 0 + ':' + 0 + ':'+ 0;
    		Datetime dtmNextMonthtime = datetime.valueOf(strCurrentTime);
    		Account acc = new Account();
    		acc.Name = 'Account_Test';
    		insert acc;
    		
    		Contact con1 = new Contact();
    		con1.LastName = 'Contact_Test1';
    		con1.AccountId = acc.Id;
    		con1.TW_Department__c = 'ICU';
    		
    		listContact.add(con1);
    		Contact con3 = new Contact();
    		con3.LastName = 'Contact_Test3';
    		con3.AccountId = acc.Id;
    		con3.TW_INfusorType__c = '巴士';
    		listContact.add(con3);
    		Contact con5 = new Contact();
    		con5.LastName = 'Contact_Test5';
    		con5.AccountId = acc.Id;
    		listContact.add(con5);
    		insert listContact;
    		
			MonthlyPlan__c monthplan = new 	MonthlyPlan__c();
			monthplan.Year__c = String.valueOf(date.today().year());
			monthplan.Month__c = String.valueOf(date.today().month());
			insert monthplan;
			
			Event event1 = new Event();//
			event1.OwnerId = user1.Id;
			event1.SubjectType__c = '其他';
			event1.StartDateTime = datetime.now();
			event1.EndDateTime = (datetime.now()).addHours(2);
			listEvent.add(event1);
			Event event2 = new Event();//
			event2.OwnerId = user1.Id;
			event2.SubjectType__c = '拜访';
			event2.TW_VisitType__c = '一般拜訪';
			event2.StartDateTime = datetime.now().addDays(1);
			event2.EndDateTime = (datetime.now().addDays(1)).addHours(2);
			listEvent.add(event2);
			Event event3 = new Event();
			event3.OwnerId = user1.Id;
			event3.SubjectType__c = '拜访';
			event3.TW_VisitType__c = '一般拜訪';
			event3.WhoId = con5.Id;
			event3.StartDateTime = datetime.now().addDays(2);
			event3.EndDateTime = (datetime.now().addDays(2)).addHours(2);
			listEvent.add(event3);
			Event event4 = new Event();
			event4.OwnerId = user1.Id;
			event4.SubjectType__c = '拜访';
			event4.TW_VisitType__c = 'SPIN Call';
			event4.WhoId = con5.Id;
			event4.StartDateTime = datetime.now().addDays(3);
			event4.EndDateTime = (datetime.now().addDays(3)).addHours(2);
			listEvent.add(event4);
			Event event5 = new Event();//
			event5.OwnerId = user1.Id;
			event5.WhoId = con5.Id;
			event5.StartDateTime = datetime.now().addDays(4);
			event5.EndDateTime = (datetime.now().addDays(4)).addHours(2);
			listEvent.add(event5);
			Event event6 = new Event();//
			event6.OwnerId = user1.Id;
			event6.WhoId = con1.Id;
			event6.StartDateTime = datetime.now().addDays(5);
			event6.EndDateTime = (datetime.now().addDays(5)).addHours(2);
			listEvent.add(event6);
			Event event7 = new Event();
			event7.OwnerId = user1.Id;
			event7.SubjectType__c = '拜访';
			event7.TW_VisitType__c = 'SPIN Call';
			event7.WhoId = con5.Id;
			event7.StartDateTime = datetime.now().addDays(6);
			event7.EndDateTime = (datetime.now().addDays(6)).addHours(2);
			listEvent.add(event7);
			Event event8 = new Event();
			event8.OwnerId = user1.Id;
			event8.SubjectType__c = '拜访';
			event8.TW_VisitType__c = '一般拜訪';
			event8.WhoId = con5.Id;
			event8.Done__c = true;
			event8.V2_IsExpire__c = false;
			event8.StartDateTime = datetime.now().addDays(7);
			event8.EndDateTime = (datetime.now().addDays(7)).addHours(2);
			listEvent.add(event8);
			Event event9 = new Event();
			event9.OwnerId = user1.Id;
			event9.SubjectType__c = '拜访';
			event9.TW_VisitType__c = 'SPIN Call';
			event9.WhoId = con5.Id;
			event9.Done__c = true;
			event9.V2_IsExpire__c = false;
			event9.StartDateTime = datetime.now().addDays(8);
			event9.EndDateTime = (datetime.now().addDays(8)).addHours(2);
			listEvent.add(event9);
			Event event10 = new Event();
			event10.OwnerId = user1.Id;
			event10.SubjectType__c = '拜访';
			event10.TW_VisitType__c = '一般拜訪';
			event10.WhoId = con5.Id;
			event10.Done__c = true;
			event10.V2_IsExpire__c = true;
			event10.StartDateTime = dtmNextMonthtime.addDays(-8);
			event10.EndDateTime = (dtmNextMonthtime.addDays(-8)).addHours(2);
			listEvent.add(event10);
			Event event11 = new Event();
			event11.OwnerId = user1.Id;
			event11.SubjectType__c = '拜访';
			event11.TW_VisitType__c = 'SPIN Call';
			event11.WhoId = con5.Id;
			event11.Done__c = true;
			event11.V2_IsExpire__c = true;
			event11.StartDateTime = dtmNextMonthtime.addDays(-9);
			event11.EndDateTime = (dtmNextMonthtime.addDays(-9)).addHours(2);
			listEvent.add(event11);
			Event event12 = new Event();
			event12.OwnerId = user1.Id;
			event12.SubjectType__c = '拜访';
			event12.TW_VisitType__c = 'SPIN Call';
			event12.WhoId = con1.Id;
			event12.Done__c = true;
			event12.V2_IsExpire__c = false;
			event12.Description = '反馈test';
			event12.StartDateTime = datetime.now().addDays(11);
			event12.EndDateTime = (datetime.now().addDays(11)).addHours(2);
			listEvent.add(event12);
			Event event13 = new Event();
			event13.OwnerId = user1.Id;
			event13.SubjectType__c = '拜访';
			event13.TW_VisitType__c = 'SPIN Call';
			event13.WhoId = con3.Id;
			event13.Done__c = true;
			event13.V2_IsExpire__c = false;
			event13.Description = '反馈test';
			event13.StartDateTime = datetime.now().addDays(12);
			event13.EndDateTime = (datetime.now().addDays(12)).addHours(2);
			listEvent.add(event13);
			Event event14 = new Event();
			event14.OwnerId = user1.Id;
			event14.SubjectType__c = '拜访';
			event14.TW_VisitType__c = 'SPIN Call';
			event14.WhoId = con5.Id;
			event14.Done__c = true;
			event14.V2_IsExpire__c = true;
			event14.Description = '反馈test';
			event14.StartDateTime = dtmNextMonthtime.addDays(-10);
			event14.EndDateTime = (dtmNextMonthtime.addDays(-10)).addHours(2);
			listEvent.add(event14);
			insert listEvent;
    	}
    	TW_SalesManKPIBatch kpiBatch = new TW_SalesManKPIBatch();
    	kpiBatch.strYear = String.valueOf(date.today().year());
    	kpiBatch.strMonth = String.valueOf(date.today().month());
	    Database.executeBatch(kpiBatch,100);
	    system.Test.stopTest();
    	TW_KPI__c KPI = [Select t.TW_Year__c, t.TW_VisitedNum__c, t.TW_VisitDonePercent__c, t.TW_VisitAccDepartment__c, 
    							t.TW_TotalPlanVisitEventNum__c, t.TW_Salesman__c, t.TW_SPINCallNum__c, 
    							t.TW_RecordUniqueMark__c, t.TW_ProductDetailType__c, t.TW_ProductDetailTypeNum__c, 
    							t.TW_OwnerRole__c, t.TW_Month__c, t.TW_MonthVisitAmount__c, t.TW_MonthPlan__c, 
    						    t.TW_HaveVisitedEventNum__c, t.TW_HCPDepartmentNum__c,
    						    t.TW_AfterVisitRecord__c, t.OwnerId, t.Name, t.Id 
    					 From TW_KPI__c t
    					 where t.OwnerId = :user1.Id];
    	   /* system.assertEquals(KPI.TW_TotalPlanVisitEventNum__c, 9);
	    	system.assertEquals(KPI.TW_HaveVisitedEventNum__c, 4);
	    	system.assertEquals(KPI.TW_HCPDepartmentNum__c,1);
	    	system.assertEquals(KPI.TW_ProductDetailTypeNum__c,1);
	    	system.assertEquals(KPI.TW_VisitedNum__c,2);
	    	system.assertEquals(KPI.TW_SPINCallNum__c,2);
	    	system.assertEquals(KPI.TW_MonthVisitAmount__c,4);	*/
    }
	
}