/**
 * Henry
 *2013-10-11
 *New病人拜访列表页面控制类——测试类
 */
@isTest
private class PM_Test_NewpatientvisitDevController {

    static testMethod void myUnitTest() 
    {
        PM_Test_AllConfig.myUnitTest(); //公用方法，内涵必要的一些测试数据
        system.Test.startTest();
    	PM_Patient__c patient = new PM_Patient__c();
    	ApexPages.StandardController controller = new ApexPages.StandardController(patient);
    	//实例化控制类
    	PM_NewpatientvisitDevController npv = new PM_NewpatientvisitDevController(controller);
    	//初始化get部分字段
    	ApexPages.StandardSetController conset = npv.conset;
    	list<PM_ExportFieldsCommon.Patient> list_Patient = npv.list_Patient;
    	npv.first();
	    npv.last();
	    npv.previous();
	    npv.next();
    	//查询流程
    	list<Profile> list_Profile = [Select Id,Name From Profile Where Name = '系统管理员'];
        User user = new User();
        user.ProfileId = list_Profile[0].Id;
        user.Username = 'asfdasdf@acme.com';
        user.FirstName = 'Test';
        user.LastName = 'Test';
        user.Email = 'asdf@aa.com';
        user.Alias = 'asdf';
        user.EmailEncodingKey = 'UTF-8';
        user.TimeZoneSidKey = 'Asia/Shanghai';
        user.LanguageLocaleKey = 'zh_CN';
        user.LocaleSidKey = 'zh_CN';
        insert user;
	 	npv.Patient.Name = '张先生';
	 	npv.MobilePhone = '18611112222';
 	 	npv.Patient.PM_Current_Saler__c = user.Id;
 	 	npv.Patient.PM_VisitFailCount__c = 1;
 	 	npv.Patient.PM_Address__c = '北京';
 	 	npv.BigArea = '东一区';
	    npv.Province = '北京';
 	 	npv.PatientProvince = '北京';
 	 	npv.City = '北京市';
 	 	npv.PatientCity = '北京市';
	    npv.County = '海淀区';
	    npv.PatientCounty = '海淀区';
 	 	npv.PatientStatus = 'New';
 	 	npv.CreatedDate_Start = '2013-10-09';
	    npv.CreatedDate_End = '2013-10-09';
	    npv.InDate_Start = '2013-10-09';
	    npv.InDate_End = '2013-10-09';
 	 	npv.InHospital = '朝阳区第三医院';

	    npv.Check();
	    //排序流程
	    npv.strOrder = 'desc';
	    npv.util.patSql = null;
	    npv.sortOrders();
	    npv.sortOrders();
	    //导出
	    npv.exportExcel(); 
	    for(PM_ExportFieldsCommon.Patient pa :npv.list_PatientExport)
	    {
	    	pa.IsExport = true;
	    }
	    npv.exportExcel();
	    npv.getItems();
	    system.Test.stopTest();
    }
}