/*
Author：Hank
Created on：2014-1-8
Description: 
导航只是给销售，renal带医院主管（不显示业务机会导航）
导航页面放在首页，根据月计划，拜访，业务机会，市场活动4方面内容进行导航，点击“确定”后跳转到相应的操作界面。
1月计划是否已生成：判断当月的月计划是否已经生成，已经生成就不显示；若未生成，则显示“xxxx年xx月月计划还未生成”和“确定”link点击“确定”，生成当月的月计划，并跳转到查看页面。
2月计划小结是否填写：若当日日期为当月最后一天，或者月初7号以前，判断月计划（当月或上月，最后一天取当月，否则取上月）是否已经填写小结。
                                                     若已填写则不显示，若未填写，则显示“xxxx年xx月月计划还未填写小结”和“确定”link，点击“确定”，跳转到月计划编辑页面。
3拜访是否已安排：计算截至到今日应该的拜访安排数量，取月计划明细的计划次数进行累计，得到本月应该拜访的数量。假设为30次。
                    每月按22个工作日计算，得到每天的拜访次数 30/22=1.36(保留2位)。假设今日为2.14日，则工作日为10日。允许晚3天，那么截至今日应该完成7日的拜访，就是7*1.36=9.52。四舍五入为10次。
                    取截至到今天的拜访数量（不含过期），如果小于10，则未达标。
                若还未创建月计划，或拜访数量达标则不提示，若未达标，则显示“您的拜访数量不够，应创建10个，目前只有8个”和“确定”link，点击“确定”，跳转到批量维护拜访页面。
                
4.拜访是否已过期：计算3天后将过期的拜访数量。若〉0则提示“您有x个拜访将在3天后过期”，拜访开始日期+7 <= 当日+3，点击确定后跳转到“过期视图”
5.业务机会是否已生成4个：计算进行中的业务机会的数量，进行中的定义：结束日期〉当前日期， 阶段不等于：....
                                                  若小于4个，则显示“您的业务机会数不够4个”，点击确定后跳转到新建业务机会页面。
6.市场活动在当月的话, 提示用户做相应的报名操作，查询状态处于“报名开始”，且当月处于市场活动的开始，结束区间，如果数量大于0，则提示用户报名(区分Bu)
    
                
*/
public class BQ_CtrNavigation {
    //当前年
    public String Year{get;set;}
    public String Year1{get;set;}
    //当前月
    public String Month{get;set;}
    public String Month1{get;set;}
    //是否生成月计划
    public Boolean IsCreateMp{get;set;}
    //月计划Id
    public Id MpId{get;set;}
    //月小结是否填写
    public Boolean IsFillingIn{get;set;}
    public Boolean IsFillingIn1{get;set;}
    //拜访是否已安排
    public Boolean IsArrangementCall{get;set;}
    //拜访应完成数
    public Integer Finishedcall{get;set;}
    //拜访实际完成数
    public Integer ActualFinishedcall{get;set;}
    //拜访是否已过期
    public Boolean IsExpiredCall{get;set;}
    //拜访即将过期数
    public Integer ExpireFlag{get;set;}
    //业务机会是否已生成4个
    public Boolean IsCreateOpp{get;set;}
    //目前进行中的业务机会有
    public Integer OppNum{get;set;}
    //市场活动
    public List<Campaign> List_Cam{get;set;}
    //是否显示市场活动
    public Boolean IsCampaign{get;set;}
    //非进行中的业务机会状态
    //BQ:(S5)正式采购,客户合作失败
    Set<String> Set_Stage = new Set<String>{'(S5)正式采购','客户合作失败'};
     
    public BQ_CtrNavigation()
    {
        try
        {
            //是否生成月计划
            IsCreateMp = true;
            //月计划小结是否填写
            IsFillingIn = false;
            IsFillingIn1 = false;
            //拜访是否已安排
            IsArrangementCall = false;
            //拜访是否已过期
            IsExpiredCall = false;
            ExpireFlag = 0;
            //业务机会是否生成4个
            IsCreateOpp= false;
            //市场活动报名
            IsCampaign = false;
            
            //拜访应完成数
            Finishedcall = 0;
            //拜访实际完成数
            ActualFinishedcall = 0;
            
            //当期日期
            Date currentDate = Date.today();
            Year = String.valueOf(currentDate.Year());
            Month = String.valueOf(currentDate.Month());
            //当期用户Id
            Id userid = UserInfo.getUserId();
            
            /*本月月计划*/
            List<MonthlyPlan__c> List_Mp = [select Id,execution_summary__c,Status__c,V2_TotalCallRecords__c,V2_FinishedCallRecords__c from MonthlyPlan__c where OwnerId =: userid
                                            and Month__c =: Month and Year__c =: Year limit 1];
            
            /*本月是否生成月计划*/
            if(List_Mp != null && List_Mp.size()>0)
            {
                IsCreateMp=false;
                /*本月是否填写月小结:如果当天为最后一天则显示本月信息*/
                if((List_Mp[0].execution_summary__c ==null || List_Mp[0].execution_summary__c == '') && currentDate == currentDate.addMonths(1).toStartOfMonth().addDays(-1))
                {
                    MpId = List_Mp[0].Id;
                    IsFillingIn = true;
                }
                /*拜访是否已安排*/
             	if(List_Mp[0].V2_TotalCallRecords__c != null && List_Mp[0].V2_TotalCallRecords__c >0)
                {
                    //得到当前工作日
                    Integer workdays = getWorkDays(currentDate);
                    List<BQ_WorkingDay__c> List_BQMp = [select BQ_WorkDay__c from BQ_WorkingDay__c where BQ_Month__c =: integer.valueOf(Month) limit 1];
                	Integer Fixedworkdays = 22;
                	if(List_BQMp[0].BQ_WorkDay__c !=null || List_BQMp[0].BQ_WorkDay__c > 0)
                	{
                		//固定工作日
                    	Fixedworkdays = integer.valueOf(List_BQMp[0].BQ_WorkDay__c);
                        
                	}
                	else
                	{
                		Fixedworkdays = 22;
                	}
                    //应完成数量
                    Finishedcall = Integer.valueOf(workdays*(List_Mp[0].V2_TotalCallRecords__c/Fixedworkdays).Round());
                }
                //现在已完成数量
                if(List_Mp[0].V2_FinishedCallRecords__c != null && List_Mp[0].V2_FinishedCallRecords__c >0)
                {
                    ActualFinishedcall = Integer.valueOf(List_Mp[0].V2_FinishedCallRecords__c);
                }
                if(ActualFinishedcall < Finishedcall )
                {
                    IsArrangementCall = true;
                }
            }
            /*上月是否填写月小结:如果当天不为本月最后一天则显示上月信息*/
            //上月月计划
            Year1 = String.valueOf(currentDate.addMonths(-1).Year());
            Month1 = String.valueOf(currentDate.addMonths(-1).Month());
            List<MonthlyPlan__c> List_Mp1 = [select Id,execution_summary__c from MonthlyPlan__c where OwnerId =: userid
                                            and Month__c =: Month1 and Year__c =: Year1 limit 1];
            if(List_Mp1 != null && List_Mp1.size()>0)
            {
                if((List_Mp1[0].execution_summary__c ==null || List_Mp1[0].execution_summary__c == '') && currentDate != currentDate.addMonths(1).toStartOfMonth().addDays(-1))
                {
                    MpId = List_Mp1[0].Id;
                    IsFillingIn1 = true;
                }
            }
            /*拜访是否已过期*/
            DateTime CurrentStartMonth = DateTime.newInstance(currentDate.toStartOfMonth(),Time.newInstance(00, 00, 00, 00));
            DateTime CurrentEndMonth = DateTime.newInstance(currentDate.toStartOfMonth().addMonths(1),Time.newInstance(00, 00, 00, 00));
            for(Event ev:[select Id,StartDateTime from Event where V2_IsExpire__c = false 
                          and IsRecurrence != true and whoId != null and StartDateTime>=:CurrentStartMonth
                          and StartDateTime <:CurrentEndMonth and OwnerId =: userid and RecordType.DeveloperName = 'V2_Event' and SubjectType__c='拜访'
                          and V2_IsExpire__c = false and Done__c= false])
            {
                if(ev.StartDateTime.addDays(7)<=currentDate.addDays(3))
                {
                    ExpireFlag++;
                }
            }
            if(ExpireFlag>0)
            {
                IsExpiredCall= true;
            }
            
            /*业务机会*/
            OppNum = 0;
            for(Opportunity opp:[select StageName from Opportunity where CloseDate >: date.today() and OwnerId =: userid])
            {
                if(Set_Stage.contains(opp.StageName))
                {
                    continue;
                }
                else
                {
                    OppNum ++;
                }
            }
            if(OppNum<4)
            {
                IsCreateOpp = true;
            }
            
            /*市场活动*/
            //各BU市场部成员
            Set<Id> MarketUserids = getMarketUsers();
            /**2012-3-22修改，去掉时间 时间上的限制**/
            List_Cam = [select Id,Name from Campaign where Status='Sign Up Begin' and IsActive =:true and OwnerId in: MarketUserids];
            if(List_Cam != null && List_Cam.size()>0)
            {
                IsCampaign = true;
            }
            /*判断当前用户是否为Renal主管：如果带医院则显示除业务机会所有信息；不带医院则都不显示。*/
            Profile pf = [select Id from Profile where Name = 'Standard User - Renal Sales Supervisor' limit 1];
            User u = [select ProfileId,Renal_valid_super__c from User where id =: UserInfo.getUserId() ];
            if(u.ProfileId == pf.Id)
            {
                if(u.Renal_valid_super__c)
                {
                    //主管带医院不显示业务机会信息
                    IsCreateOpp= false;
                }
                else
                {
                    //是否生成月计划
                    IsCreateMp = false;
                    //月计划小结是否填写
                    IsFillingIn = false;
                    IsFillingIn1 = false;
                    //拜访是否已安排
                    IsArrangementCall = false;
                    //拜访是否已过期
                    IsExpiredCall = false;
                    ExpireFlag = 0;
                    //业务机会是否生成4个
                    IsCreateOpp= false;
                    //市场活动报名
                    IsCampaign = false;
                }   
            }
        }catch(Exception e)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, String.valueOf(e)+' 第'+e.getLineNumber()+'行');          
            ApexPages.addMessage(msg);
            return;
        }
    }
    /*
     *得出工作日
    */
    public Integer getWorkDays(Date CurrentDay)
    {
        try
        {
            Integer returnNum = 0;
            //得到当前日期所在月第一天
            Date StartMonthDay = CurrentDay.toStartOfMonth();
            //得到本月第一天所在在周
            Date StartWeek = StartMonthDay.toStartOfWeek().addDays(1);
            //本月第一周工作日
            Integer days1 = 0;
            //如果当前日期等于月开始日期
            if(CurrentDay == StartMonthDay)
            {
                //如果当前日期不为周六日
                if(CurrentDay != StartWeek.addDays(5) && CurrentDay != StartWeek.addDays(6))
                {
                    returnNum = 1;
                }
            }
            //当前日期大于月开始日期
            else
            {
                //如果当前日期在月开始日期所在周
                if(CurrentDay <= StartWeek.addDays(6))
                {
                    if(CurrentDay <= StartWeek.addDays(4))
                    {
                        returnNum = StartMonthDay.daysBetween(CurrentDay)+1;
                    }
                    else
                    {
                        if(StartMonthDay <= StartWeek.addDays(4))
                        {
                            returnNum = StartMonthDay.daysBetween(StartWeek.addDays(4))+1;
                        }
                        else
                        {
                            returnNum = 0;
                        }
                    }
                }
                else 
                {
                    /*得出月开始日期所在周 工作日*/
                    //如果月开始日期为工作日并且为周五
                    if(StartMonthDay == StartWeek.addDays(4))
                    {
                        days1 = 1;
                    }
                    //如果月开始日期为工作日并且不为周五
                    else if(StartMonthDay <= StartWeek.addDays(4))
                    {
                        days1= StartMonthDay.daysBetween(StartWeek.addDays(4))+1;
                    }
                    //月开始日期为周六或周日
                    else
                    {
                        days1 = 0;
                    }
                    
                    
                    /*开始周后一周以后*/
                    returnNum = getWorkDays2(StartWeek.addDays(7),CurrentDay)+days1;
                }
            }
            return returnNum;
        }catch(Exception e)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, String.valueOf(e)+' 第'+e.getLineNumber()+'行');          
            ApexPages.addMessage(msg);
            return null;
        }
    }
    /*
     *得出工作日：传入参数（周一，当前日期）得出工作日
    */
    public Integer getWorkDays2(Date weekstartday,Date Currentday)
    {
        Integer returnNum = 0;
        if(Currentday == weekstartday)
        {
            returnNum+=1;
        }
        else if(Currentday >= weekstartday)
        {
            if(Currentday > weekstartday.addDays(6))
            {
                returnNum +=getWorkDays2(weekstartday.addDays(7),Currentday)+5;
            }
            else if(Currentday <=  weekstartday.addDays(4))
            {
                returnNum += weekstartday.daysBetween(Currentday)+1;
            }
            else 
            {
                returnNum +=5;
            }
        }
        return returnNum;
    }
    /*
     *得到当期用户所在Bu的市场部用户
    */
    public Set<Id> getMarketUsers()
    {
        Set<Id> userids = new Set<Id>();
        User CurrentUser =[select Id,UserRole.Name,Profile.Name from User where Id =: UserInfo.getUserId()];
        //如果当期用户为管理员则显示所有BU市场活动
        if(CurrentUser.Profile.Name=='系统管理员' || CurrentUser.Profile.Name =='System Administrator')
        {
            for(User u:[select Id from User where Profile.Name='Standard User - Renal Marketing' 
                       or Profile.Name='Standard User - IVT Marketing'or Profile.Name='Standard User - BIOS Marketing'])
            {
                userids.add(u.Id);
            }
        }
        else if(CurrentUser.UserRole.Name == null || CurrentUser.UserRole.Name =='')
        {
        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, '当前用户没有分配角色，无法判断其所属部门，请联系系统管理员。');          
            ApexPages.addMessage(msg);
            return null;
        }
        else if(CurrentUser.UserRole.Name.toUpperCase().contains('RENAL'))
        {
            for(User u:[select Id from User where Profile.Name='Standard User - Renal Marketing'])
            {
                userids.add(u.Id);
            }
        }
        else if(CurrentUser.UserRole.Name.toUpperCase().contains('MD'))
        {
            for(User u:[select Id from User where Profile.Name='Standard User - IVT Marketing'])
            {
                userids.add(u.Id);
            }
        }
        else if(CurrentUser.UserRole.Name.toUpperCase().contains('BIOS'))
        {
            for(User u:[select Id from User where Profile.Name='Standard User - BIOS Marketing'])
            {
                userids.add(u.Id);
            }
        }
        return userids;
    } 
    /*******************************************************测试类****************************************************/
    static testMethod void BQ_CtrNavigation()
    {
        /*新建3BU市场部用户*/
        
        List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
         //Bios
         Profile biosp = [select Id from Profile where Name='BQ Marketing'];
         User BiosMarket = new User();
         BiosMarket.Username='BiosMarket@123.com';
         BiosMarket.LastName='BiosMarket';
         BiosMarket.Email='BiosMarket@123.com';
         BiosMarket.Alias=user[0].Alias;
         BiosMarket.TimeZoneSidKey=user[0].TimeZoneSidKey;
         BiosMarket.ProfileId=biosp.Id;
         BiosMarket.LocaleSidKey=user[0].LocaleSidKey;
         BiosMarket.LanguageLocaleKey=user[0].LanguageLocaleKey;
         BiosMarket.EmailEncodingKey=user[0].EmailEncodingKey;
         BiosMarket.CommunityNickname='BiosMarket';
         BiosMarket.MobilePhone='12345678912';
         //BiosMarket.UserRoleId = biosRep_role.Id ;
         BiosMarket.IsActive = true;
         insert BiosMarket;
         
          //Md
         Profile mdsp = [select Id from Profile where Name='BQ Marketing'];
         User MdMarket = new User();
         MdMarket.Username='MdMarket@123.com';
         MdMarket.LastName='MdMarket';
         MdMarket.Email='MdMarket@123.com';
         MdMarket.Alias=user[0].Alias;
         MdMarket.TimeZoneSidKey=user[0].TimeZoneSidKey;
         MdMarket.ProfileId=mdsp.Id;
         MdMarket.LocaleSidKey=user[0].LocaleSidKey;
         MdMarket.LanguageLocaleKey=user[0].LanguageLocaleKey;
         MdMarket.EmailEncodingKey=user[0].EmailEncodingKey;
         MdMarket.CommunityNickname='MdMarket';
         MdMarket.MobilePhone='12345678912';
         //BiosMarket.UserRoleId = biosRep_role.Id ;
         MdMarket.IsActive = true;
         insert MdMarket;
         
          //Renal
         Profile renalsp = [select Id from Profile where Name='BQ Marketing'];
         User RenalMarket = new User();
         RenalMarket.Username='RenalMarket@123.com';
         RenalMarket.LastName='RenalMarket';
         RenalMarket.Email='RenalMarket@123.com';
         RenalMarket.Alias=user[0].Alias;
         RenalMarket.TimeZoneSidKey=user[0].TimeZoneSidKey;
         RenalMarket.ProfileId=renalsp.Id;
         RenalMarket.LocaleSidKey=user[0].LocaleSidKey;
         RenalMarket.LanguageLocaleKey=user[0].LanguageLocaleKey;
         RenalMarket.EmailEncodingKey=user[0].EmailEncodingKey;
         RenalMarket.CommunityNickname='RenalMarket';
         RenalMarket.MobilePhone='12345678912';
         //BiosMarket.UserRoleId = biosRep_role.Id ;
         RenalMarket.IsActive = true;
         insert RenalMarket;
         
         //主管带医院
         UserRole renalSup_role = new UserRole();
         renalSup_role.Name = 'Renal-Supervisor-北京-CRRT-Supervisor' ;
         insert renalSup_role;
         Profile pf = [select Id from Profile where Name = 'BQ Sales Supervisor' limit 1];
         User RenalSu = new User();
         RenalSu.Username='RenalSu@123.com';
         RenalSu.LastName='RenalSu';
         RenalSu.Email='RenalSu@123.com';
         RenalSu.Alias=user[0].Alias;
         RenalSu.TimeZoneSidKey=user[0].TimeZoneSidKey;
         RenalSu.ProfileId=pf.Id;
         RenalSu.LocaleSidKey=user[0].LocaleSidKey;
         RenalSu.LanguageLocaleKey=user[0].LanguageLocaleKey;
         RenalSu.EmailEncodingKey=user[0].EmailEncodingKey;
         RenalSu.CommunityNickname='RenalSu';
         RenalSu.MobilePhone='12345678912';
         RenalSu.UserRoleId = renalSup_role.Id ;
         RenalSu.IsActive = true;
         insert RenalSu;
        //Bios 销售
        Profile BiosRepPro = [select Id from Profile where Name  = 'BQ Sales Rep' limit 1];
        UserRole biosRep_role = new UserRole();
         biosRep_role.Name = 'BIOS-Rep-北京-Albumin-Rep' ;
         insert biosRep_role;
         
         User BiosRep1 = new User();
         BiosRep1.Username='BiosRep1@123.com';
         BiosRep1.LastName='BiosRep1';
         BiosRep1.Email='BiosRep1@123.com';
         BiosRep1.Alias=user[0].Alias;
         BiosRep1.TimeZoneSidKey=user[0].TimeZoneSidKey;
         BiosRep1.ProfileId=BiosRepPro.Id;
         BiosRep1.LocaleSidKey=user[0].LocaleSidKey;
         BiosRep1.LanguageLocaleKey=user[0].LanguageLocaleKey;
         BiosRep1.EmailEncodingKey=user[0].EmailEncodingKey;
         BiosRep1.CommunityNickname='BiosRep1';
         BiosRep1.MobilePhone='12345678912';
         BiosRep1.UserRoleId = biosRep_role.Id ;
         BiosRep1.IsActive = true;
         insert BiosRep1;
        //Md 销售
        Profile MdRepPro = [select Id from Profile where Name='Standard User - IVT Sales Rep' limit 1];
        UserRole mdRep_role = new UserRole();
         mdRep_role.Name = 'MD-Rep-北京-IVT-Rep' ;
         insert mdRep_role;
         
         User mdRep = new User();
         mdRep.Username='mdRep@123.com';
         mdRep.LastName='mdRep';
         mdRep.Email='mdRep@123.com';
         mdRep.Alias=user[0].Alias;
         mdRep.TimeZoneSidKey=user[0].TimeZoneSidKey;
         mdRep.ProfileId=MdRepPro.Id;
         mdRep.LocaleSidKey=user[0].LocaleSidKey;
         mdRep.LanguageLocaleKey=user[0].LanguageLocaleKey;
         mdRep.EmailEncodingKey=user[0].EmailEncodingKey;
         mdRep.CommunityNickname='mdRep';
         mdRep.MobilePhone='12345678912';
         mdRep.UserRoleId = mdRep_role.Id ;
         mdRep.IsActive = true;
         insert mdRep;
         
         Test.startTest();
         
        /*客户*/
        RecordType accrecordtype = [select Id from RecordType where Name='医院' and SobjectType='Account' limit 1];
        Account acc = new Account();
        acc.RecordTypeId = accrecordtype.Id;
        acc.Name = 'AccTest';
        insert acc;
        /*联系人*/
        RecordType conrecordtype = [select Id from RecordType where Name = 'Renal' and SobjectType='Contact' and IsActive = true limit 1 ];
        Contact con1 = new Contact();
        con1.LastName = 'AccTestContact1';
        con1.AccountId=acc.Id;
        insert con1;
        
        /*月计划*/
        MonthlyPlan__c mp = new MonthlyPlan__c();
        mp.Year__c = String.valueOf(Date.today().year());
        mp.Month__c =String.valueOf(Date.today().month());
        insert mp;
        
        MonthlyPlan__c mp1 = new MonthlyPlan__c();
        mp1.Year__c = String.valueOf(Date.today().addMonths(-1).year());
        mp1.Month__c = String.valueOf(Date.today().addMonths(-1).month());
        insert mp1;
        /*拜访事件*/
        RecordType callRt = [Select Id from RecordType where DeveloperName = 'V2_Event' Limit 1];
        
        Event CallEv = new Event();
        CallEv.RecordTypeId = callRt.Id;
        CallEv.WhoId = con1.Id;
        CallEv.StartDateTime = datetime.now().addDays(-4);
        CallEv.EndDateTime = datetime.now().addDays(-4).addMinutes(1);
        CallEv.SubjectType__c ='拜访';
        insert CallEv;
        
        /*月计划明细*/
        MonthlyPlanDetail__c mpd1 = new MonthlyPlanDetail__c();
        mpd1.Contact__c = con1.Id;
        mpd1.Account__c = acc.Id;
        mpd1.MonthlyPlan__c = mp.Id;
        mpd1.ArrangedTimes__c = 5;
        mpd1.AdjustedTimes__c = 6;
        mpd1.Planned_Finished_Calls__c = 5;
        insert mpd1;
        /*市场活动*/
        Campaign cam = new Campaign();
        cam.Name = 'RenalMarketCamTest';
        cam.StartDate = date.today();
        cam.EndDate = date.today().addMonths(1);
        cam.IsActive = true;
        cam.Status = 'Sign Up Begin';
        cam.OwnerId = RenalMarket.Id;
        insert cam;
        
        Campaign cam2 = new Campaign();
        cam2.Name = 'BiosMarketCamTest';
        cam2.StartDate = date.today();
        cam2.EndDate = date.today().addMonths(1);
        cam2.IsActive = true;
        cam2.Status = 'Sign Up Begin';
        cam2.OwnerId = BiosMarket.Id;
        insert cam2;
        
        Campaign cam3 = new Campaign();
        cam3.Name = 'MdMarketCamTest';
        cam3.StartDate = date.today();
        cam3.EndDate = date.today().addMonths(1);
        cam3.IsActive = true;
        cam3.Status = 'Sign Up Begin';
        cam3.OwnerId = MdMarket.Id;
        insert cam3;
        
        //Test.startTest();
        BQ_CtrNavigation cng = new BQ_CtrNavigation();
        cng.getWorkDays(date.today().toStartOfMonth());
        cng.getWorkDays(date.today().toStartOfMonth().addDays(9));
        cng.getWorkDays2(date.today().toStartOfWeek().addDays(1),date.today().addMonths(1).addDays(-1));
        
        System.runAs(RenalSu)
        {
            BQ_CtrNavigation cng2 = new BQ_CtrNavigation();
        }
        
        System.runAs(BiosRep1)
        {
            BQ_CtrNavigation cng3 = new BQ_CtrNavigation();
        }
        System.runAs(mdRep)
        {
            BQ_CtrNavigation cng4 = new BQ_CtrNavigation();
        }
        Test.stopTest();
    }
}