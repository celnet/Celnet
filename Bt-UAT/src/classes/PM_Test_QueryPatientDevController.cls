/**
 * Henry
 *2013-10-11
 *病人查询   页面控制类——测试类
 */
@isTest
private class PM_Test_QueryPatientDevController {

    static testMethod void myUnitTest() 
    {
        PM_Test_AllConfig.myUnitTest(); //公用方法，内涵必要的一些测试数据
        system.Test.startTest();
        PM_Patient__c patient = new PM_Patient__c();
        ApexPages.StandardController controller = new ApexPages.StandardController(patient);
        //实例化控制类
        PM_QueryPatientDevController qp = new PM_QueryPatientDevController(controller);
        //初始化get部分字段
        ApexPages.StandardSetController conset = qp.conset;
        list<PM_ExportFieldsCommon.Patient> list_Patient = qp.list_Patient;
        qp.first();
        qp.last();
        qp.previous();
        qp.next();
        //查询流程
        list<Profile> list_Profile = [Select Id,Name From Profile Where Name = '系统管理员'];
        User user = new User();
        user.ProfileId = list_Profile[0].Id;
        user.Username = 'asfdasdf@acme.com';
        user.FirstName = 'Test';
        user.LastName = 'Test';
        user.Email = 'asdf@aa.com';
        user.Alias = 'asdf';
        user.EmailEncodingKey = 'UTF-8';
        user.TimeZoneSidKey = 'Asia/Shanghai';
        user.LanguageLocaleKey = 'zh_CN';
        user.LocaleSidKey = 'zh_CN';
        insert user;
        //新建客户
        Account acc = new  Account();
        acc.Name = 'Test';
        insert acc;
        RecordType record = [Select Id From RecordType Where Name = 'MD'];
        Contact contact = new Contact();
        contact.RecordTypeId = record.Id;
        contact.LastName = 'Test';
        contact.AccountId = acc.Id;
        insert contact;
        
        qp.Patient.Name = '张先生';
        qp.BigArea = '东一区';
        qp.Province = '北京';
        qp.PatientProvince = '北京';
        qp.City = '北京市';
        qp.PatientCity = '北京市';
        qp.County = '海淀区';
        qp.PatientCounty = '海淀区';
        qp.PatientStatus = 'APD';
        qp.CreatedDate_Start = '2013-10-09';
        qp.CreatedDate_End = '2013-10-09';
        qp.InDate_Start = '2013-10-09';
        qp.InDate_End = '2013-10-09';
        qp.DropDate_Start = '2013-10-11';
        qp.DropDate_End = '2013-10-11';
        qp.DropActDate_Start = '2013-10-10';
        qp.DropActDate_End = '2013-10-10';
        qp.Patient.PM_Sex__c = '男';
        qp.Patient.PM_IdNumber__c = '130121225652654523';
        qp.Patient.PM_InUser__c = user.Id;
        qp.Patient.PM_Protopathy__c = '糖尿病';
        qp.Patient.PM_LiParalysis__c = '是';
        qp.Patient.PM_Delivery__c = '不送货上门';
        qp.Patient.PM_ReCommunication__c = '是';
        qp.Patient.PM_EcDifficulties__c = '是';  
        qp.Patient.PM_WhTransplants__c = '是';
        qp.Patient.PM_Optimistic__c = '是';    
        qp.Patient.PM_Talkative__c = '是';
        qp.Patient.PM_Pdtreatment__c = 'APD';
        qp.Patient.PM_Heatingmode__c = '微波炉加热';
        qp.Patient.PM_Distributor__c = user.Id;

        qp.MobilePhone = '18654269856';
        qp.TeDistributors = user.Id;
        qp.Patient.PM_Address__c = '北京';
        qp.InHospital = '朝阳区第三医院';
        qp.patient.PM_Terminal__c =  user.Id;
        qp.patient.PM_InHospital__c = user.Id;
        qp.TeDistributorsProvince = '北京';
        qp.TeDistributorsCity = '北京';
        qp.TeDistributorsCounty = '海淀区';
        qp.Patient.PM_Birthday__c = date.valueOf('2013-1-11');
        qp.Patient.PM_Sex__c = '男';
        qp.Patient.PM_PatientDegree__c = '高中';
        qp.Patient.PM_Profession__c = '农民';
        qp.Patient.PM_IdNumber__c = '130125488569562351';
        qp.Patient.PM_Email__c = 'ttoo@qq.com';
        qp.Patient.PM_FAIncome__c = '8万-10万';
        qp.Patient.PM_Payment__c = '自费';
        qp.Patient.PM_LastVisitStatus__c = '是';
        qp.Patient.PM_Visit_Interval__c = 1;
        qp.Patient.PM_ZipCode__c = '100000';
        qp.Patient.PM_Internet__c = '是';
        qp.Patient.PM_Disappearanc__c = '更换地址';
        qp.Patient.PM_HomeTel__c = '010-5236541';
        qp.Patient.PM_PmPhone__c = '18654563255';
        qp.Patient.PM_PmTel__c = '010-5641256';
        qp.Patient.PM_FamilyPhone__c = '15653254562';
        qp.Patient.PM_FamilyTel__c = '010-5621452';
        qp.Patient.PM_OtherTel2__c = '010-5485625';
        qp.Patient.PM_BVTime1__c = '12:01-13:00';
        qp.Patient.PM_BVTime2__c = '12:01-13:00';
        qp.Patient.PM_BVTime3__c = '12:01-13:00';
        qp.Patient.PM_BVTime4__c = '12:01-13:00';
        qp.Patient.PM_BVTime5__c = '12:01-13:00';
        qp.Patient.PM_BVTime6__c = '12:01-13:00';
        qp.Patient.PM_PhoneState1__c = '空号';
        qp.Patient.PM_PhoneState2__c = '未启用';
        qp.Patient.PM_PhoneState3__c = '空号';
        qp.Patient.PM_PhoneState4__c = '空号';
        qp.Patient.PM_PhoneState5__c = '空号';
        qp.Patient.PM_PhoneState6__c = '空号';
        qp.Patient.PM_InType__c = '急性插管';
        qp.Patient.PM_InDoctor__c = contact.Id;
        qp.Patient.PM_InNurser__c = contact.Id;
        qp.Patient.PM_InUser__c = user.Id;
        qp.Patient.PM_Current_PSR__c = user.Id;
        qp.Patient.PM_InInformation__c = '无';
        qp.Patient.PM_Protopathy__c = '急性肾衰';
        qp.Patient.PM_DaUroutput__c = 10;
        qp.Patient.PM_DiBlpressure__c = 10;
        qp.Patient.PM_SyBlpressure__c = 5;
        qp.Patient.PM_SeCreatinine__c = 5;
        qp.Patient.PM_Hemoglobin__c  = 5;
        qp.Patient.PM_Albumin__c = 5;
        qp.Patient.PM_UrNitrogen__c = 10;
        qp.Patient.PM_LiParalysis__c = '是';
        qp.Patient.PM_ViImpairment__c = '是';
        qp.Patient.PM_PeEqtest__c = '是';
        qp.Patient.PM_ClInformation__c = '无';
        qp.Patient.PM_LoGifts__c = '是';
        qp.Patient.PM_AcHoVisits__c = '是';
        qp.Patient.PM_WhTocall__c = '是';
        qp.Patient.PM_WhTosend__c = '是';
        qp.Patient.PM_IsFree__c = '是';
        qp.Patient.PM_IsAcceptCall__c = '是';
        qp.Patient.PM_Received_Diary__c = '是';
        qp.Patient.PM_ReCommunication__c = '是';
        qp.Patient.PM_ReCarepackages__c  = '是';
        qp.Patient.PM_IsJoined__c = '是';
        qp.Patient.PM_IsInstruction__c = '是';
        qp.Patient.PM_Delivery__c = '不送货上门';
        qp.Patient.PM_Activities_Satisfactied__c = '一般';
        qp.Patient.PM_Is_Personal_Information__c = '是';
        qp.Patient.PM_OHCEducation__c = '是';
        qp.Patient.PM_PeReInformation__c = '是';
        qp.Patient.PM_CaInformation__c = '无';
        qp.Patient.PM_EcDifficulties__c = '是';
        qp.Patient.PM_WhTransplants__c = '是';
        qp.Patient.PM_TuHeIntention__c = '是';
        qp.Patient.PM_CeDisease__c = '是';
        //qp.Patient.PM_IsOld__c = '是';
        qp.Patient.PM_TuToCoPro__c = '是';
        qp.Patient.PM_DrAcProblems__c = '是';
        qp.Patient.PM_HighRisk__c = '是';
        qp.Patient.PM_HRInformation__c = '无';
        qp.Patient.PM_Optimistic__c = '是';
        qp.Patient.PM_ATUnderstand__c = '是';
        qp.Patient.PM_ReToBaite__c = '是';
        qp.Patient.PM_AdTomovement__c = '是';
        qp.Patient.PM_FaPeace__c = '是';
        qp.Patient.PM_BeWorking__c = '是';
        qp.Patient.PM_BeStudent__c = '是';
        qp.Patient.PM_HiSostatus__c = '是';
        qp.Patient.PM_HighImpac__c = '是';
        qp.Patient.PM_HIInformation__c = '是';
        qp.Patient.PM_HiHobbies__c = '是';
        qp.Patient.PM_Treat_Hospital__c = user.Id;
        qp.Patient.PM_Treat_Doctor__c = user.Id;
        qp.Patient.PM_Treat_Nurse__c = user.Id;
        qp.Patient.PM_Pdtreatment__c = 'APD';
        qp.Patient.PM_Treat_Saler__c = user.Id;
        qp.Patient.PM_WaConsumption__c = 2;
        qp.Patient.PM_HDAndPD__c = '是';
        qp.Patient.PM_FromVendor__c = '是';
        qp.Patient.PM_TrInformation__c = '无';
        qp.Patient.PM_Distributor__c = user.Id;
        qp.Patient.PM_DealerRemark__c = '无';
        qp.Patient.PM_DropOut_Two_Reason__c = '长富的价格更低';
       	qp.Patient.PM_DropOut_One_Reason__c = '转血透';
        qp.Check();
        //排序流程
        qp.strOrder = 'desc';
        qp.sortOrders();
        qp.sortOrders();
        //导出
        qp.exportExcel();  
        qp.getTransfornBrands(); 
        //初始化字段集
        //qp.InitFieldsSet();
        system.Test.stopTest();
    }
}