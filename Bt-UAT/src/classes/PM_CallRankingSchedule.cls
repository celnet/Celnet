/**
 * author：bill
 * date：2013-11-11
 * description：PM_CallRankingBatch的Schedule
 * 运行时间：
 */
public class PM_CallRankingSchedule implements Schedulable
{
    public void execute(SchedulableContext sc) 
    {
    	PM_CallRankingBatch CallBatch = new PM_CallRankingBatch();
    	//CallBatch.Run_Date = date.valueOf('2013-10-3');
        database.executeBatch(CallBatch,1);
    }
}