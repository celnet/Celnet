/**
 *2013-10-18
 *复制大陆功能
 * Author: Sunny
 * 调整销售医院关系页面控制器
 * 1.主管可以在此页面调整销售医院关系（收到日期的限制）
 * 2.管理员可以在此页面不受限制的调整销售医院关系
 * 3.调整时，如果该医院有存货病人或有指标或当月有报过新病人的，只能转给主管或者其他销售
 * BILL ADD 2013-8-14
 * 医院指标，现在系统已经有了医院的指标数据，不需要新建对象来做。
 * MD的医院指标：看本财年该医院，GBU为MD的销售数据是否有目标销量（只要本财年有指标就可以，不需要看当前月）
 * RENAL的医院指标：看本财年该医院的病人关爱数据，Surviving Target，newpatient字段是否有值。
 * 不同的BU在做销售医院关系调整时，会有不同的新病人、存活病人、医院指标的限制，下图为ceci提供不同bu的限制：
 * BU 限制权限
 * IVT 只看目标销量
 * SP 只看目标销量
 * PD 目标销量，新病人，有存活病人
 * HD 都不需要看
**/
public without sharing class BQ_CtrlSalesHospitalRelationAdjust 
{
	public List<SelectOption> listSubUser{get;set;}//原成员列表
    public List<SelectOption> listOperate{get{//操作列表
       List<SelectOption> list_so = new List<SelectOption>();
       list_so.add(new SelectOption( '新增' , '新增'));
       list_so.add(new SelectOption( '替换' , '替换'));
       list_so.add(new SelectOption( '删除' , '刪除'));
       return list_so;
    }set;}
    public String SelectedOperate{get;set;}//选择的操作
    public Boolean EnableAddMember{get;set;}//是否显示新增页面
    public Boolean EnableReplaceMember{get;set;}//是否显示替换页面
    public Boolean EnableDeleteMember{get;set;}//是否显示删除页面
    public Boolean EnablePage{get;set;}//是否显示页面
    public Boolean blnReplace{get;set;}//是否选择替换原成员
    public String SelectedSubUser{get;set;}//选择的被替换的成员
    public String strWarningMsg{get;set;}
    public Boolean blnShowWarning{get;set;}
    public V2_Account_Team__c objAccountTeam{get;set;}
    private Set<ID> set_SubUserIds ;//若当前用户为主管，其下属用户id集合
    private ID HostipalId ;//医院ID
    private Map<ID,V2_Account_Team__c> Map_SalesAccTeam = new Map<ID,V2_Account_Team__c>();
    public void SelectOperate(){//根据所选择的操作，初始化页面
        EnableAddMember = false;
        EnableReplaceMember = false;
        EnableDeleteMember = false;
        if(SelectedOperate == '替换'){//替换
            EnableReplaceMember = true;
        }else if(SelectedOperate == '删除'){//删除
            EnableDeleteMember = true;
        }else{//新增，默认显示新增
            SelectedOperate = '新增';
            EnableAddMember = true;
        }
    }
    public BQ_CtrlSalesHospitalRelationAdjust(Apexpages.Standardcontroller controller)
    {//构造方法
       
        EnablePage = true;
        HostipalId = ApexPages.currentPage().getParameters().get('HosId');
        if(HostipalId == null){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning, '必须指定一个Account。');            
            ApexPages.addMessage(msg);
            EnablePage = false;
            return;
        }
        listSubUser = new  List<SelectOption>();
        objAccountTeam = new V2_Account_Team__c();
        
        try
       {
       	 //初始化当前用户信息
        this.initCurrentUser();
        //检查此医院下是否有正在审批中的销售医院关系
        this.initCurrentSubUserList();
        
        SelectedOperate = '新增';
        this.SelectOperate();
       }catch(Exception e)
       {
       	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()+'   '+e.getlineNumber());            
		    ApexPages.addMessage(msg);
		    return ;
       }
       
        
       
    }
    /**
    *初始化当前用户信息
    **/
    private void initCurrentUser()
    {
        User objuser = [SELECT ID,Profile.Name,UserRole.Name from User WHERE Id =: UserInfo.getUserId()] ;
        if(objuser.UserRole.Name != null && !objuser.UserRole.Name.toUpperCase().contains('BQ'))
        {
        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '此功能只开放给侨光地区用户，您沒有权限进行此操作。');            
            ApexPages.addMessage(msg);
        	EnablePage = false;
        }
        else if(objuser.UserRole.Name == null)
        {
        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '无法确定您的角色。');            
            ApexPages.addMessage(msg);
            EnablePage = false;
        }    
    }
    /**
    *初始化主管下属信息
    **/
    /*private void initSubUser(){
        V2_UtilClass cls = new V2_UtilClass();
        //主管下属，包含下属的下属
        set_SubUserIds = cls.GetUserSubIds();
        //包含自己
        set_SubUserIds.add(UserInfo.getUserId());
        this.initCurrentSubUserList();
    }*/
    /**
    *判断此客户下的销售医院关系记录中是否有正在审批的记录
    **/
    private void initCurrentSubUserList()
    {
        for(V2_Account_Team__c objAT : [select id,V2_User__c,V2_User__r.Name,V2_User__r.UserRole.Name,V2_Role__c,V2_Is_Delete__c,V2_ApprovalStatus__c,
                   V2_LastAccessDate__c,V2_Effective_Year__c,V2_Effective_Month__c,V2_ImmediateDelete__c,EffectiveDate__c
                   from V2_Account_Team__c 
                   where V2_Account__c =: HostipalId And V2_History__c = false ]){
            //判断是否有下属处于审批中
            if(objAT.V2_ApprovalStatus__c=='审批中' && objAT.V2_User__r.UserRole.Name.contains('BQ')){
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '请在审批结束后添加小组成员。');            
                ApexPages.addMessage(msg);
                EnablePage = false;
                return; 
            }
            //判断是否存在下属
          //  if(!objAT.V2_Is_Delete__c)
           // {
                Map_SalesAccTeam.put(objAT.V2_User__c,objAT);
                //ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO , '您在该医院下已存在下属，请选择是否替换已存在的下属。');            
                //ApexPages.addMessage(msg);
               // if(set_SubUserIds!=null && set_SubUserIds.contains(objAT.V2_User__c))
                //{
                   //listSubUser.add(new SelectOption(String.valueOf(objAT.V2_User__c),String.valueOf(objAT.V2_User__r.Name))) ;
               // }
          //  }
        }
        /*2013-11-21修改*/
        for(AccountTeamMember Atm: [select User.Name, UserId from  AccountTeamMember where AccountId =: HostipalId  and UserId in: Map_SalesAccTeam.keySet()])
        {
        	listSubUser.add(new SelectOption(String.valueOf(Atm.UserId),String.valueOf(Atm.User.Name))) ;
        }
    }
    /**
    *取消按钮
    **/
    public PageReference doCancel(){
        return new PageReference('/'+HostipalId) ;
    }
    /**
    *保存按钮
    **/
    public PageReference doSave(){
    	try
    	{
    		//检查信息是否填写完整
	        if(this.SelectedOperate == '新增' || this.SelectedOperate == '替换'){
	            if(this.objAccountTeam.V2_User__c == null){
	                objAccountTeam.V2_User__c.addError('请填写新成员。');
	                return null;
	            }
	        }
	        if(this.SelectedOperate == '替换' || this.SelectedOperate == '删除'){
	            if(SelectedSubUser == null){
	                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '请选择被'+SelectedOperate+'人');            
	                ApexPages.addMessage(msg);
	                return null;
	            }
	        }
	        if(this.objAccountTeam.EffectiveDate__c == null){
	            objAccountTeam.EffectiveDate__c.addError('请填写日期。');
	            return null;
	        }
	        if(this.objAccountTeam.V2_AdjustReson__c == null){
	            objAccountTeam.V2_AdjustReson__c.addError('请填写调整原因。');
	            return null;
	        }
	        //判断所填写的日期
	        String strErrorMsg = this.checkEffectiveDate(this.objAccountTeam.EffectiveDate__c , this.SelectedOperate);
	        if(strErrorMsg != null){
	            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, strErrorMsg);            
	            ApexPages.addMessage(msg);
	            return null;
	        }
	        if(this.SelectedOperate == '新增' || this.SelectedOperate == '替换'){
	            
	            //若填写的新成员已经在该医院下了
	            if(this.Map_SalesAccTeam.containsKey(objAccountTeam.V2_User__c)){
	                objAccountTeam.V2_User__c.addError('您选择用户已在该医院下了。') ;
	                return null ;
	            }
	            //检查同一产品是否有多人负责
	            if(checkProduct()){
	                objAccountTeam.V2_User__c.addError('同一个产品不能有多人负责。') ;
	                return null ;
	            }
	        }
	        
	        Id ApprovalId ;//提交审批的记录ID
	        if(this.SelectedOperate == '新增'){
	            objAccountTeam.V2_Account__c = this.HostipalId ;
	            objAccountTeam.V2_NewAccUser__c = objAccountTeam.V2_User__c;
	            objAccountTeam.V2_BatchOperate__c='新增';
	            objAccountTeam.V2_ApprovalStatus__c='待审批';
	            objAccountTeam.ownerid=objAccountTeam.V2_User__c;
	            objAccountTeam.V2_Effective_Year__c = String.valueOf(objAccountTeam.EffectiveDate__c.year());
	            objAccountTeam.V2_Effective_Month__c = String.valueOf(objAccountTeam.EffectiveDate__c.month());
	            insert objAccountTeam ;
	            ApprovalId = objAccountTeam.id;
	        }else if(this.SelectedOperate == '替换'){
	        	
	            V2_Account_Team__c objAccTeamDel = this.Map_SalesAccTeam.get(SelectedSubUser);
	            //替换操作时，不允许被替换成员的生效日期大于替换操作的生效日期
	            //2013-8-9,根据ceci要求，管理员也要受此限制
	            if(objAccTeamDel.EffectiveDate__c >= objAccountTeam.EffectiveDate__c){
	            	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '新的生效日期必须大于原销售医院关系的生效日期。');            
	                ApexPages.addMessage(msg);
	                return null;
	            }
	            
	                        
	            objAccTeamDel.V2_BatchOperate__c='替换';
	            objAccTeamDel.V2_ApprovalStatus__c='待审批';
	            objAccTeamDel.V2_NewAccUser__c=objAccountTeam.V2_User__c;
	            objAccTeamDel.NewEffDate__c = objAccountTeam.EffectiveDate__c;
	            objAccTeamDel.V2_Effective_NewYear__c = String.valueOf(objAccountTeam.EffectiveDate__c.year());
	            objAccTeamDel.V2_Effective_NewMonth__c = String.valueOf(objAccountTeam.EffectiveDate__c.month());
	            objAccTeamDel.DeleteDate__c = objAccountTeam.EffectiveDate__c.addDays(-1);
	            //2013-10-23修改，因为QV的问题，写失效年、月需要写成失效日期后一个月的年份、月份
	            objAccTeamDel.V2_Delete_Year__c = String.valueOf(objAccTeamDel.DeleteDate__c.addMonths(1).year());
	            objAccTeamDel.V2_Delete_Month__c = String.valueOf(objAccTeamDel.DeleteDate__c.addMonths(1).month());
	            objAccTeamDel.V2_AdjustReson__c=objAccountTeam.V2_AdjustReson__c;
	            ApprovalId = objAccTeamDel.id;
	            update objAccTeamDel;
	            system.debug('tihuanaaaa'+objAccTeamDel) ;
	        }else if(this.SelectedOperate == '删除'){
	            V2_Account_Team__c objAccTeamDel = this.Map_SalesAccTeam.get(SelectedSubUser);
	            //删除操作时，不允许被替换成员的生效日期大于删除操作的生效日期
	            if(objAccTeamDel.EffectiveDate__c > objAccountTeam.EffectiveDate__c){
	                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '您所填写的生效日期，比被删除销售医院关系的生效日期更早。');            
	                ApexPages.addMessage(msg);
	                return null;
	            }
	            
	            objAccTeamDel.V2_Is_Delete__c = true;
	            objAccTeamDel.V2_BatchOperate__c='删除';
	            objAccTeamDel.V2_ApprovalStatus__c='待审批';
	            //objAccTeamDel.V2_NewAccUser__c=objAccountTeam.V2_User__c;
	            objAccTeamDel.DeleteDate__c = objAccountTeam.EffectiveDate__c;
	            //2013-10-23修改，因为QV的问题，写失效年、月需要写成失效日期后一个月的年份、月份
	            objAccTeamDel.V2_Delete_Year__c = String.valueOf(objAccTeamDel.DeleteDate__c.addMonths(1).year());
	            objAccTeamDel.V2_Delete_Month__c = String.valueOf(objAccTeamDel.DeleteDate__c.addMonths(1).month());
	            objAccTeamDel.V2_AdjustReson__c=objAccountTeam.V2_AdjustReson__c;
	            ApprovalId = objAccTeamDel.id;
	            update objAccTeamDel;
	        }
	        //提交审批
	        if(ApprovalId != null)
	        {
	            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
	            req.setObjectId(ApprovalId);//客户联系人或自定义对象
	            Approval.ProcessResult result = Approval.process(req);
	        }
	        return new PageReference('/'+HostipalId) ;
    	}catch(Exception e)
    	{
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()+'   '+e.getlineNumber());            
		    ApexPages.addMessage(msg);
		    return null;
    	}
        
    }
    /**
    *检查同一产品是否有多人负责
    **/
    private boolean checkProduct(){
        if(objAccountTeam.V2_User__c == null){
            return false;
        }
        User u =[select V2_UserProduct__c from User where id=:objAccountTeam.V2_User__c];
        V2_Account_Team__c[] teams =[select v2_User__c,
            V2_Account__c,V2_History__c,V2_UserProduct__c,UserProduct__c from V2_Account_Team__c 
            where V2_Account__c =: HostipalId  and V2_History__c = false and V2_Is_Delete__c=false
            and UserProduct__c=: u.V2_UserProduct__c And V2_User__c !=: SelectedSubUser]; 
        if(teams.size() > 0){
            return true;
        }else{
            return false;
        }
    }
     /**
    *检查日期
    **/
    private String checkEffectiveDate(Date effDate , String strSelectedOperate){
        if(strSelectedOperate == '新增' || strSelectedOperate == '替换'){
            if(effDate.day() != 1){
                return '新增/替换操作，所填写的生效日期必须为自然月的第一天。' ;
            }
        }else if(strSelectedOperate == '删除'){
            if(effDate.day() != effDate.toStartOfMonth().addMonths(1).addDays(-1).day()){
                return '删除操作，失效日期必须为自然月的最后一天。';
            }
        }
        //判断所填写日期是否为当前月之前的月份
        if(effDate.year() < date.today().year() || (effDate < date.today() && effDate.month() != date.today().month())){//为当月之前的月份
            if(effDate.month() == date.today().addMonths(-1).month()){//当前月的前一月
                if(date.today().day() > 15){//当前日期是否大于15
                    return '由于您需要调整上月销售医院关系，且本月已过15日，所以您不能在此进行操作，请线下通过BUD批准，并交由admin进行转代表操作。';
                }else{
                    return null;
                }
            }else{//历史月份
                return '销售医院关系历史数据不可修改。';
            }
        }else{//为当月或者当月之后的月份
            if(date.today().day() > 25 && date.today().month() == effDate.month() && date.today().year() == effDate.year()){
                return '每月25日之后若需要调整本月销售医院关系，需要线下通过BUD审批，并交由admin进行转代表操作。';
            }else{
                return null;
            }
        }
        
    }
/*========================================================测试类=========================================================*/    
    static testMethod void BQ_CtrlSalesHospitalRelationAdjust()
    {
    	  //销售
	    UserRole RepUserRole = new UserRole() ;
	    RepUserRole.Name = 'BQ South Sales Rep';
	    insert RepUserRole ;
	    //rep简档
	    Profile RepPro = [select Id from Profile where Name  = 'BQ Sales Rep' limit 1];
	    /************User************/
		List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ; 
	     
	    /*销售*/
	    User RepSu = new User();
	    RepSu.Username='RepSu@123.com';
	    RepSu.LastName='RepSu';
	    RepSu.Email='RepSu@123.com';
	    RepSu.Alias=user[0].Alias;
	    RepSu.TimeZoneSidKey=user[0].TimeZoneSidKey;
	    RepSu.ProfileId=RepPro.Id;
	    RepSu.LocaleSidKey=user[0].LocaleSidKey;
	    RepSu.LanguageLocaleKey=user[0].LanguageLocaleKey;
        RepSu.EmailEncodingKey=user[0].EmailEncodingKey;
	    RepSu.CommunityNickname='RepSu';
	    RepSu.MobilePhone='12345678912';
	    RepSu.UserRoleId = RepUserRole.Id ;
	    RepSu.IsActive = true;
     	insert RepSu;
     	
     	 /*销售*/
	    User RepSu2 = new User();
	    RepSu2.Username='RepSu2@123.com';
	    RepSu2.LastName='RepSu2';
	    RepSu2.Email='RepSu2@123.com';
	    RepSu2.Alias=user[0].Alias;
	    RepSu2.TimeZoneSidKey=user[0].TimeZoneSidKey;
	    RepSu2.ProfileId=RepPro.Id;
	    RepSu2.LocaleSidKey=user[0].LocaleSidKey;
	    RepSu2.LanguageLocaleKey=user[0].LanguageLocaleKey;
        RepSu2.EmailEncodingKey=user[0].EmailEncodingKey;
	    RepSu2.CommunityNickname='RepSu2';
	    RepSu2.MobilePhone='12345678912';
	    RepSu2.UserRoleId = RepUserRole.Id ;
	    RepSu2.IsActive = true;
     	insert RepSu2;
     	
     	
	    
    	/*客户*/
		RecordType accrecordtype = [select Id from RecordType where DeveloperName='RecordType' and SobjectType='Account' limit 1];
		Account acc = new Account();
		acc.RecordTypeId = accrecordtype.Id;
		acc.Name = 'AccTest';
		insert acc;
		V2_Account_Team__c ateam = new V2_Account_Team__c();
        ateam.V2_Account__c = acc.Id;
        ateam.V2_User__c = RepSu.Id;
        ateam.V2_BatchOperate__c='新增';
        ateam.V2_ApprovalStatus__c = '审批通过';
        ateam.V2_History__c = false;
        insert ateam;
        
		ApexPages.currentPage().getParameters().put('HosId',null);
    	Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(new V2_Account_Team__c());
    	BQ_CtrlSalesHospitalRelationAdjust RelationAdjust1 = new BQ_CtrlSalesHospitalRelationAdjust(controller);
    	
    	ApexPages.currentPage().getParameters().put('HosId',acc.Id);
    	BQ_CtrlSalesHospitalRelationAdjust RelationAdjust22 = new BQ_CtrlSalesHospitalRelationAdjust(controller);
    	
    	System.runAs(RepSu)
    	{
    		ApexPages.currentPage().getParameters().put('HosId',acc.Id);
    		BQ_CtrlSalesHospitalRelationAdjust RelationAdjust = new BQ_CtrlSalesHospitalRelationAdjust(controller);
    		RelationAdjust.doCancel();
	    	RelationAdjust.SelectOperate();
	    	RelationAdjust.doSave();
	    	RelationAdjust.objAccountTeam.V2_AdjustReson__c = null;
	    	RelationAdjust.objAccountTeam.V2_User__c = RepSu.Id; 
	    	RelationAdjust.doSave();
	    	RelationAdjust.objAccountTeam.V2_AdjustReson__c = '333';
	    	RelationAdjust.doSave();
	    	RelationAdjust.SelectedOperate = '替换';
	    	
	    	RelationAdjust.objAccountTeam.V2_User__c = RepSu.Id; 
	    	RelationAdjust.SelectOperate();
	    	RelationAdjust.objAccountTeam.V2_AdjustReson__c = '333';
	    	RelationAdjust.SelectedSubUser = RepSu2.Id;
	    	RelationAdjust.doSave();
	    	RelationAdjust.SelectedOperate = '删除';
	    	
	    	RelationAdjust.objAccountTeam.V2_AdjustReson__c = '333';
	    	RelationAdjust.SelectOperate();
	    	RelationAdjust.SelectedSubUser = RepSu2.Id;
	    	RelationAdjust.doSave();
	    	
	    	
    	}
    	ateam.V2_ApprovalStatus__c = '审批中';
        update ateam;
	    RelationAdjust22.initCurrentSubUserList();	
    	
    	
    }
    
}