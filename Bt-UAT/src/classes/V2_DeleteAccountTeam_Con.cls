/* 
 * Author: Sunny
 * Created on: 2011-12-26
 * Description: 
 *修改日期：2012-1-11
 *修改人：scott
 *新加功能，保存并提交审批。
 */
public with sharing class V2_DeleteAccountTeam_Con
{
	/*
	public String Month{get;set;}
	public String Year{get;set;}
	
	
	public V2_Account_Team__c objOldAccountTeam{get;set;}
	public boolean blnIsWarningShow{get;set;}
	public boolean blnIsWarningShow2{get;set;}
	public String strWarningMsg{get;set;}
	public String strWarningMsg2{get;set;}
	
	public boolean blnEditShow{get;set;}
	public boolean blnDelShow{get;set;}
	
	
	public boolean blnIsNotReadOnly{get;set;}
	private Set<ID> set_UserIds = new Set<ID>() ; 
	public List<SelectOption> ListYears{get;set;}
	*/
	public String strId{get;set;}
	public boolean blnIsReadOnly{get;set;}
	public boolean blnIsMesShow{get;set;}
	public String strMessage{get;set;}
	public V2_Account_Team__c objAccountTeam{get;set;}
	/*
	public List<SelectOption> getListMonths()
    {
        list<SelectOption> options=new list<SelectOption>();
        options.add(new SelectOption('1','1'));options.add(new SelectOption('2','2'));
        options.add(new SelectOption('3','3'));options.add(new SelectOption('4','4'));
        options.add(new SelectOption('5','5'));options.add(new SelectOption('6','6'));
        options.add(new SelectOption('7','7'));options.add(new SelectOption('8','8'));
        options.add(new SelectOption('9','9'));options.add(new SelectOption('10','10'));
        options.add(new SelectOption('11','11'));options.add(new SelectOption('12','12'));
        return options;
    }
    */
	public V2_DeleteAccountTeam_Con(ApexPages.StandardController controller)
	{
		
        
		blnIsMesShow = false ;
		strId = controller.getId() ;
		
		
		
		//objLeadTeam = (V2_Lead_Team__c)controller.getSubject() ;
		//Year = String.valueOf(date.today().year());
        //Month = String.valueOf(date.today().Month());
		
		objAccountTeam = [select id,V2_IsBatch__c,V2_AdjustReson__c,V2_ImmediateDelete__c,V2_IsSyn__c ,V2_Delete_Year__c,V2_Delete_Month__c,V2_Effective_Year__c,V2_Effective_Month__c,V2_History__c,V2_Account__c,V2_ApprovalStatus__c,V2_Account__r.Name,V2_User__c,V2_User__r.Name,V2_Role__c,V2_Is_Delete__c from V2_Account_Team__c where Id =: strId] ;
		
		//2013-7-4 Sunny修改 该功能废弃。
        blnIsMesShow = true ;
        blnIsReadOnly = true ;
        strMessage = '请点击调整销售医院关系进行操作。' ;
        return;
        //
		
		/*
		objOldAccountTeam = [select id,V2_Account__c,V2_User__c from V2_Account_Team__c where Id =: strId] ;
		
		this.initSubordinate() ;
		if(!set_UserIds.contains(objAccountTeam.V2_User__c))
			{
				blnIsMesShow = true ;
				strMessage = '您只能设置您的下属。' ;
				return ;
			}
		
		if(objAccountTeam.V2_ApprovalStatus__c == '审批中')
		{
			blnIsMesShow = true ;
			blnIsReadOnly = true ;
			strMessage = '您不可以修改在审批中的小组成员。' ;
			return;
		}
		if(objAccountTeam.V2_History__c )
			{
				blnIsMesShow = true ;
				blnIsReadOnly = true ;
				strMessage = '您不可以修改一条历史记录。' ;
				return;
			}
		
			if(objAccountTeam.V2_IsSyn__c || objAccountTeam.V2_Is_Delete__c)
			{
				blnEditShow = false ;
				blnDelShow = true ;
				if(objAccountTeam.V2_Delete_Year__c != null)
				{this.Year = objAccountTeam.V2_Delete_Year__c ;}
				
				if(objAccountTeam.V2_Delete_Month__c != null)
				{this.Month = objAccountTeam.V2_Delete_Month__c ;}

				
			}else
			{
				blnEditShow = true ;
				blnDelShow = false ;
				if(objAccountTeam.V2_Effective_Year__c != null)
				{this.Year = objAccountTeam.V2_Effective_Year__c ;}
				if(objAccountTeam.V2_Effective_Month__c != null)
				{this.Month = objAccountTeam.V2_Effective_Month__c ;}
				
			}
						
			if(blnDelShow == true)
			{
				blnIsWarningShow2 = true ;
				strWarningMsg2 = '您只能对该成员设置、修改其失效时间。' ;
			}
			
			Integer intM = 0 ;
			blnIsWarningShow = false ;
			if(blnDelShow == true)
			{
				intM= [select count() from V2_Account_Team__c where V2_Account__c =: objAccountTeam.V2_Account__c And V2_History__c =false And V2_Is_Delete__c = false];
				
				if(intM == 1 )
				{
					blnIsWarningShow = true ;
					strWarningMsg = '这是您在该客户上的最后一名下属，若使其失效，可能导致您在其失效之后不能继续查看该客户信息。' ;
					
				}
			}
			
			
		
		//init year
		ListYears = new  List<SelectOption>();
        ListYears.add(new SelectOption(String.valueOf(date.today().addYears(-1).year()),String.valueOf(date.today().addYears(-1).year())));
        ListYears.add(new SelectOption(String.valueOf(date.today().year()),String.valueOf(date.today().year())));
        ListYears.add(new SelectOption(String.valueOf(date.today().addYears(1).year()),String.valueOf(date.today().addYears(1).year())));
		//init current year & month
		*/
	}
	/**
	保存数据
	**/
	public void Save(){
        /*
		blnIsMesShow=false;
		
			
		if(blnDelShow == true)
		{
			if(this.Year !=null && Integer.valueOf(Year) < date.today().year())
			{
				blnIsMesShow = true ;
				strMessage = '删除生效时间不能在当前年份之前。' ;
				return ;
			}else if(Year !=null && Integer.valueOf(Year) == date.today().year())
			{
				if(Month!=null && Integer.valueOf(Month) < date.today().month())
				{
					blnIsMesShow = true ;
					strMessage = '删除生效时间不能在当前月份之前。' ;
					return ;
				}
			}
			objAccountTeam.V2_Is_Delete__c = true ;
			//objAccountTeam.V2_Delete_Year__c = this.Year ;
			//objAccountTeam.V2_Delete_Month__c = this.Month ;
			objAccountTeam.V2_BatchOperate__c='删除';
			//Sunny add 添加逻辑，若选择立即删除则失效日期改为当前年月
			if(objAccountTeam.V2_ImmediateDelete__c == true)
			{
				objAccountTeam.V2_Delete_Year__c = String.valueOf(date.today().year()) ;
				objAccountTeam.V2_Delete_Month__c = String.valueOf(date.today().month()) ;
			}else
			{
				objAccountTeam.V2_Delete_Year__c = this.Year ;
				objAccountTeam.V2_Delete_Month__c = this.Month ;
			}
			
		}
		if(blnEditShow == true )
		{
			System.debug('qudexiashu:' +objAccountTeam.V2_User__c);

			V2_Account_Team__c[] objAcc = [select id from V2_Account_Team__c where V2_Account__c=: objAccountTeam.V2_Account__c and V2_User__c=:objAccountTeam.V2_User__c and V2_User__c!=:objOldAccountTeam.V2_User__c] ;
			if(objAcc!=null&&objAcc.size()>0){
				blnIsMesShow = true ;
				strMessage = '已有该下属成员。' ;
				return ;
				
			}
			objAccountTeam.V2_Effective_Year__c = this.Year ;
			objAccountTeam.V2_Effective_Month__c = this.Month ;
			objAccountTeam.V2_BatchOperate__c='新增';
		}
		
		objAccountTeam.V2_ApprovalStatus__c ='待审批';
		
		
		update objAccountTeam ;
		*/
	}
	
	public PageReference Cancel()
	{
		return  new PageReference('/'+objAccountTeam.V2_Account__c);
	}
	/*
	private void initSubordinate()
	{
		V2_UtilClass clsUtilClass = new V2_UtilClass() ;
		List<ID> list_UserId = new List<ID>() ;
		list_UserId = clsUtilClass.getSubordinateIds(UserInfo.getUserRoleId()) ;
		if(list_UserId != null && list_UserId.size() != 0)
		{
			set_UserIds.addAll(list_UserId) ;
		}

	}
	*/
	//2012-1-11新加功能，提交并审批。
	public PageReference saveChange2()
	{
		/*
		if(blnIsReadOnly == true)
		{
			return  new PageReference('/'+objAccountTeam.V2_Account__c);
		}
		
		Save();
		if(blnIsMesShow){
			return null;
		}
		//提交审批
		Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setObjectId(objAccountTeam.id);//客户联系人或自定义对象
       	Approval.ProcessResult result = Approval.process(req);
		
		*/
		return  new PageReference('/'+objAccountTeam.V2_Account__c);
	}
	/**
	测试
	**/
	static testMethod void TestPage() 
	{
		//----------------New UserRole ------------------
    	//List<UserRole> list_userRole = new List<UserRole >() ;
    	UserRole objUserRole = new UserRole() ;
    	objUserRole.Name = 'Renal-Rep-大上海-PD-Rep(陈喆令)' ;
    	insert objUserRole ;
    	UserRole objUserRole2 = new UserRole() ;
    	objUserRole2.Name = 'Renal-Rep-大上海-PB-Rep(陈喆令)' ;
    	objUserRole2.ParentRoleId = objUserRole.Id ;
    	insert objUserRole2 ;
		//----------------Create User-------------
    	List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.UserRoleId = objUserRole2.Id ;
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	use2.UserRoleId = objUserRole2.Id ;
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.UserRoleId = objUserRole.Id ;
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
    	//---------------New Account-------------------
    	List<Account> list_acc = new List<Account>() ;
    	Account objAcc = new Account() ;
    	objAcc.Name = 'TAcc' ;
    	list_acc.add(objAcc) ;
    	insert list_acc ;
    	//----------------New Account Team------------------
    	List<V2_Account_Team__c> list_accT = new List<V2_Account_Team__c>() ;
    	V2_Account_Team__c objAccTeam1 = new V2_Account_Team__c() ;
    	objAccTeam1.V2_Account__c = objAcc.Id ;
    	objAccTeam1.V2_User__c = use1.Id ;
    	objAccTeam1.V2_BatchOperate__c = '删除' ;
		//objAccTeam1.V2_ApprovalStatus__c = '待审批' ;
		list_accT.add(objAccTeam1) ;
		insert list_accT ;
		//----------------New Account Team M----------------------
		List<AccountTeamMember> list_AccTeamM = new List<AccountTeamMember>() ;
		AccountTeamMember objAccTeamM1 = new AccountTeamMember() ;
		objAccTeamM1.AccountId = objAcc.Id ;
		objAccTeamM1.UserId = use1.Id ;
		list_AccTeamM.add(objAccTeamM1) ;
		insert list_AccTeamM ;
		
		//-----------------Start Test---------------------
		system.test.startTest() ;
		system.runAs(use3)
    	{
    		ApexPages.StandardController STController = new ApexPages.StandardController(objAccTeam1);
			V2_DeleteAccountTeam_Con DelAccTeam = new V2_DeleteAccountTeam_Con(STController);
			//DelAccTeam.getListMonths() ;
			DelAccTeam.Save() ;
			//DelAccTeam.doCancel() ;
			DelAccTeam.saveChange2() ;
    	}
		system.test.stopTest() ;
		
	}
	static testMethod void TestPage2() 
	{
		//----------------New UserRole ------------------
    	//List<UserRole> list_userRole = new List<UserRole >() ;
    	UserRole objUserRole = new UserRole() ;
    	objUserRole.Name = 'Renal-Rep-大上海-PD-Rep(陈喆令)' ;
    	insert objUserRole ;
    	UserRole objUserRole2 = new UserRole() ;
    	objUserRole2.Name = 'Renal-Rep-大上海-PB-Rep(陈喆令)' ;
    	objUserRole2.ParentRoleId = objUserRole.Id ;
    	insert objUserRole2 ;
		//----------------Create User-------------
    	List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.UserRoleId = objUserRole2.Id ;
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	use2.UserRoleId = objUserRole2.Id ;
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.UserRoleId = objUserRole.Id ;
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
    	//---------------New Account-------------------
    	List<Account> list_acc = new List<Account>() ;
    	Account objAcc = new Account() ;
    	objAcc.Name = 'TAcc' ;
    	list_acc.add(objAcc) ;
    	insert list_acc ;
    	//----------------New Account Team------------------
    	List<V2_Account_Team__c> list_accT = new List<V2_Account_Team__c>() ;
    	V2_Account_Team__c objAccTeam1 = new V2_Account_Team__c() ;
    	objAccTeam1.V2_Account__c = objAcc.Id ;
    	objAccTeam1.V2_User__c = use3.Id ;
    	objAccTeam1.V2_BatchOperate__c = '删除' ;
		//objAccTeam1.V2_ApprovalStatus__c = '待审批' ;
		list_accT.add(objAccTeam1) ;
		insert list_accT ;
		//----------------New Account Team M----------------------
		List<AccountTeamMember> list_AccTeamM = new List<AccountTeamMember>() ;
		AccountTeamMember objAccTeamM1 = new AccountTeamMember() ;
		objAccTeamM1.AccountId = objAcc.Id ;
		objAccTeamM1.UserId = use3.Id ;
		list_AccTeamM.add(objAccTeamM1) ;
		insert list_AccTeamM ;
		
		//-----------------Start Test---------------------
		system.test.startTest() ;
		system.runAs(use1)
    	{
    		ApexPages.StandardController STController = new ApexPages.StandardController(objAccTeam1);
			V2_DeleteAccountTeam_Con DelAccTeam = new V2_DeleteAccountTeam_Con(STController);
			//DelAccTeam.getListMonths() ;
			DelAccTeam.Save() ;
			//DelAccTeam.doCancel() ;
			DelAccTeam.saveChange2() ;
    	}
		system.test.stopTest() ;
		
	}
	static testMethod void TestPage3() 
	{
		//----------------New UserRole ------------------
    	//List<UserRole> list_userRole = new List<UserRole >() ;
    	UserRole objUserRole = new UserRole() ;
    	objUserRole.Name = 'Renal-Rep-大上海-PD-Rep(陈喆令)' ;
    	insert objUserRole ;
    	UserRole objUserRole2 = new UserRole() ;
    	objUserRole2.Name = 'Renal-Rep-大上海-PB-Rep(陈喆令)' ;
    	objUserRole2.ParentRoleId = objUserRole.Id ;
    	insert objUserRole2 ;
		//----------------Create User-------------
    	List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.UserRoleId = objUserRole2.Id ;
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	use2.UserRoleId = objUserRole2.Id ;
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.UserRoleId = objUserRole.Id ;
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
    	//---------------New Account-------------------
    	List<Account> list_acc = new List<Account>() ;
    	Account objAcc = new Account() ;
    	objAcc.Name = 'TAcc' ;
    	list_acc.add(objAcc) ;
    	insert list_acc ;
    	//----------------New Account Team------------------
    	List<V2_Account_Team__c> list_accT = new List<V2_Account_Team__c>() ;
    	V2_Account_Team__c objAccTeam1 = new V2_Account_Team__c() ;
    	objAccTeam1.V2_Account__c = objAcc.Id ;
    	objAccTeam1.V2_User__c = use1.Id ;
    	objAccTeam1.V2_BatchOperate__c = '删除' ;
		objAccTeam1.V2_ApprovalStatus__c = '审批中' ;
		list_accT.add(objAccTeam1) ;
		insert list_accT ;
		//----------------New Account Team M----------------------
		List<AccountTeamMember> list_AccTeamM = new List<AccountTeamMember>() ;
		AccountTeamMember objAccTeamM1 = new AccountTeamMember() ;
		objAccTeamM1.AccountId = objAcc.Id ;
		objAccTeamM1.UserId = use1.Id ;
		list_AccTeamM.add(objAccTeamM1) ;
		insert list_AccTeamM ;
		
		//-----------------Start Test---------------------
		system.test.startTest() ;
		system.runAs(use3)
    	{
    		ApexPages.StandardController STController = new ApexPages.StandardController(objAccTeam1);
			V2_DeleteAccountTeam_Con DelAccTeam = new V2_DeleteAccountTeam_Con(STController);
			//DelAccTeam.getListMonths() ;
			DelAccTeam.Save() ;
			//DelAccTeam.doCancel() ;
			DelAccTeam.saveChange2() ;
    	}
		system.test.stopTest() ;
		
	}
	static testMethod void TestPage4() 
	{
		//----------------New UserRole ------------------
    	//List<UserRole> list_userRole = new List<UserRole >() ;
    	UserRole objUserRole = new UserRole() ;
    	objUserRole.Name = 'Renal-Rep-大上海-PD-Rep(陈喆令)' ;
    	insert objUserRole ;
    	UserRole objUserRole2 = new UserRole() ;
    	objUserRole2.Name = 'Renal-Rep-大上海-PB-Rep(陈喆令)' ;
    	objUserRole2.ParentRoleId = objUserRole.Id ;
    	insert objUserRole2 ;
		//----------------Create User-------------
    	List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.UserRoleId = objUserRole2.Id ;
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	use2.UserRoleId = objUserRole2.Id ;
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.UserRoleId = objUserRole.Id ;
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
    	//---------------New Account-------------------
    	List<Account> list_acc = new List<Account>() ;
    	Account objAcc = new Account() ;
    	objAcc.Name = 'TAcc' ;
    	list_acc.add(objAcc) ;
    	insert list_acc ;
    	//----------------New Account Team------------------
    	List<V2_Account_Team__c> list_accT = new List<V2_Account_Team__c>() ;
    	V2_Account_Team__c objAccTeam1 = new V2_Account_Team__c() ;
    	objAccTeam1.V2_Account__c = objAcc.Id ;
    	objAccTeam1.V2_User__c = use1.Id ;
    	objAccTeam1.V2_BatchOperate__c = '删除' ;
    	objAccTeam1.V2_History__c = true ;
		//objAccTeam1.V2_ApprovalStatus__c = '审批中' ;
		list_accT.add(objAccTeam1) ;
		insert list_accT ;
		//----------------New Account Team M----------------------
		List<AccountTeamMember> list_AccTeamM = new List<AccountTeamMember>() ;
		AccountTeamMember objAccTeamM1 = new AccountTeamMember() ;
		objAccTeamM1.AccountId = objAcc.Id ;
		objAccTeamM1.UserId = use1.Id ;
		list_AccTeamM.add(objAccTeamM1) ;
		insert list_AccTeamM ;
		
		//-----------------Start Test---------------------
		system.test.startTest() ;
		system.runAs(use3)
    	{
    		ApexPages.StandardController STController = new ApexPages.StandardController(objAccTeam1);
			V2_DeleteAccountTeam_Con DelAccTeam = new V2_DeleteAccountTeam_Con(STController);
			//DelAccTeam.getListMonths() ;
			DelAccTeam.Save() ;
			//DelAccTeam.doCancel() ;
			DelAccTeam.saveChange2() ;
    	}
		system.test.stopTest() ;
		
	}
	static testMethod void TestPage5() 
	{
		//----------------New UserRole ------------------
    	//List<UserRole> list_userRole = new List<UserRole >() ;
    	UserRole objUserRole = new UserRole() ;
    	objUserRole.Name = 'Renal-Rep-大上海-PD-Rep(陈喆令)' ;
    	insert objUserRole ;
    	UserRole objUserRole2 = new UserRole() ;
    	objUserRole2.Name = 'Renal-Rep-大上海-PB-Rep(陈喆令)' ;
    	objUserRole2.ParentRoleId = objUserRole.Id ;
    	insert objUserRole2 ;
		//----------------Create User-------------
    	List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
    	use1.Username='user2@123.com';
    	use1.LastName='user2';
    	use1.Email='user2@123.com';
    	use1.Alias=user[0].Alias;
    	use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use1.ProfileId=user[0].ProfileId;
    	use1.LocaleSidKey=user[0].LocaleSidKey;
    	use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use1.EmailEncodingKey=user[0].EmailEncodingKey;
    	use1.CommunityNickname='chequ1';
    	use1.MobilePhone='12345678912';
    	use1.UserRoleId = objUserRole2.Id ;
    	use1.IsActive = true;
    	list_User.add(use1) ;
        User use2=new User();
    	use2.Username='user2@223.com';
    	use2.LastName='user2';
    	use2.Email='user2@223.com';
    	use2.Alias=user[0].Alias;
    	use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use2.ProfileId=user[0].ProfileId;
    	use2.LocaleSidKey=user[0].LocaleSidKey;
    	use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use2.EmailEncodingKey=user[0].EmailEncodingKey;
    	use2.CommunityNickname='chequ2';
    	use2.UserRoleId = objUserRole2.Id ;
    	use2.MobilePhone='22345678922';
    	use2.IsActive = true;
    	list_User.add(use2) ;
        User use3=new User();
    	use3.Username='user2@323.com';
    	use3.LastName='user2';
    	use3.Email='user2@323.com';
    	use3.Alias=user[0].Alias;
    	use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
    	use3.ProfileId=user[0].ProfileId;
    	use3.LocaleSidKey=user[0].LocaleSidKey;
    	use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
    	use3.EmailEncodingKey=user[0].EmailEncodingKey;
    	use3.CommunityNickname='chequ3';
    	use3.MobilePhone='32345678932';
    	use3.UserRoleId = objUserRole.Id ;
    	use3.IsActive = true;
    	list_User.add(use3) ;
    	insert list_User ;
    	//---------------New Account-------------------
    	List<Account> list_acc = new List<Account>() ;
    	Account objAcc = new Account() ;
    	objAcc.Name = 'TAcc' ;
    	list_acc.add(objAcc) ;
    	insert list_acc ;
    	//----------------New Account Team------------------
    	List<V2_Account_Team__c> list_accT = new List<V2_Account_Team__c>() ;
    	V2_Account_Team__c objAccTeam1 = new V2_Account_Team__c() ;
    	objAccTeam1.V2_Account__c = objAcc.Id ;
    	objAccTeam1.V2_User__c = use1.Id ;
    	objAccTeam1.V2_BatchOperate__c = '删除' ;
    	objAccTeam1.V2_Is_Delete__c = true ;
		//objAccTeam1.V2_ApprovalStatus__c = '审批中' ;
		list_accT.add(objAccTeam1) ;
		insert list_accT ;
		//----------------New Account Team M----------------------
		List<AccountTeamMember> list_AccTeamM = new List<AccountTeamMember>() ;
		AccountTeamMember objAccTeamM1 = new AccountTeamMember() ;
		objAccTeamM1.AccountId = objAcc.Id ;
		objAccTeamM1.UserId = use1.Id ;
		list_AccTeamM.add(objAccTeamM1) ;
		insert list_AccTeamM ;
		
		//-----------------Start Test---------------------
		system.test.startTest() ;
		system.runAs(use3)
    	{
    		ApexPages.StandardController STController = new ApexPages.StandardController(objAccTeam1);
			V2_DeleteAccountTeam_Con DelAccTeam = new V2_DeleteAccountTeam_Con(STController);
			//DelAccTeam.getListMonths() ;
			DelAccTeam.Save() ;
			//DelAccTeam.doCancel() ;
			DelAccTeam.saveChange2() ;
    	}
		system.test.stopTest() ;
		
	}
}