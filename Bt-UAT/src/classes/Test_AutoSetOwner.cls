/**
 * Test AutoSetAssVisitOwner
 * Test AutoSetOppEvaluationOwner
 */
@isTest
private class Test_AutoSetOwner {

    static testMethod void TestAutoSetAssVisitOwner() {
        AssVisitComments__c av = new AssVisitComments__c();
        av.BeReviewed__c = userinfo.getuserid();
        try{
        insert av;
        }catch(exception e){
        	system.debug(e);
        }
    }
    static testMethod void TestAutoSetOppEvaluationOwner() {
    	OppEvaluation__c oe = new OppEvaluation__c();
        oe.BeCommentUser__c = userinfo.getUserId();
        try{
        insert oe;
        }catch(exception e){
        	system.debug(e);
        }
    }
}