/**
*Author：Dean
*Date：2013-11-23
*功能：PSR拜访月计划，每天统计PSR部门的各种类型的完成量和时长
**/ 
global class PM_CallRankingBatch implements Database.Batchable<sObject>
{
	//需要统计的月份	
	public Date Run_Date;
	global Database.QueryLocator start(Database.BatchableContext BC)
	{ 
		if(Run_Date == null)
		{
			Run_Date = date.today();
		}
		string year = string.valueOf(Run_Date.Year());
		string month = string.valueOf(Run_Date.Month());
		//以用户或者建档为单位运行Batch
    	return Database.getQueryLocator([Select p.PM_Year__c,p.PM_Month__c,p.PM_TargetVisit__c, p.PM_TargetTraveling__c, 
				 p.PM_TargetSaveInfo__c, p.PM_TargetPatientMan__c, p.PM_TargetOther__c, p.PM_TargetMakeReport__c, 
				 p.PM_TargetCommunicate__c, p.PM_TargetCall__c, p.PM_TargetTraining__c, 
				 p.PM_TargetCallLength__c, p.PM_TargetCallAnswer__c, p.PM_Ranking__c, 
				 p.PM_ActualVisit__c, p.PM_ActualTraveling__c, p.PM_TargetEmail__c,
				 p.PM_ActualTraining__c, p.PM_ActualSaveInfo__c, p.PM_ActualPatientMan__c, p.PM_ActualOther__c, 
				 p.PM_ActualMakeReport__c, p.PM_ActualEmail__c, p.PM_ActualCommunicate__c, 
				 p.PM_ActualCall__c, p.PM_ActualCallLength__c, p.PM_ActualCallAnswer__c, 
				 p.OwnerId, p.Id From PM_PSRMonthPlan__c p 
				where p.Owner.IsActive = true and PM_Year__c = : year and PM_Month__c = : month]);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
		if(Run_Date == null)
		{
			Run_Date = date.today();
		}
    	//被分配人
    	set<Id> set_OwnerId = new set<Id>();
    	//psr事件类型的ID
		//ID PSREventId = [Select r.Id  From RecordType r where r.SobjectType = 'Event' and r.DeveloperName = 'RoutineEvent' and r.IsActive = true][0].Id;
    	//PSR拜访月计划
    	List<PM_PSRMonthPlan__c> list_monthPlan = new List<PM_PSRMonthPlan__c>();
    	for(sObject sObj : scope)
        {
            PM_PSRMonthPlan__c monthPlan = (PM_PSRMonthPlan__c)sObj;
            set_OwnerId.add(monthPlan.OwnerId);
            list_monthPlan.add(monthPlan);
        }
        //'电话随访','保存信息','邮件处理','沟通协调','制作报告','参加培训',
        //'其他日常工作','旅行病人服务','病人信息管理','上门家访','来电接听处理'
        //这些算事件数量
        //电话随访，沟通协调，来电接听处理
        //这些算时间长度
        //map的key值，算数量：number+type值
        //map的key值  算时长：Length+type值
        Map<String,Double> map_event = new Map<String,Double>();
        //开始时间
        Date startDate = Run_Date.toStartOfMonth();
        //结束时间
        Date endDate = Run_Date.addMonths(1).toStartOfMonth();
        for(Event event : [Select e.RecordTypeId, e.PM_VisitStatus__c, e.OwnerId, e.Done__c,Custom_Duration__c,e.type 
        					From Event e where EndDateTime >= : startDate and StartDateTime < : endDate and 
        					OwnerId IN : set_OwnerId and RecordType.DeveloperName = 'RoutineEvent'
        					and e.Done__c = true and type IN ('电话随访','保存信息','邮件处理','沟通协调','制作报告','参加培训',
        					'其他日常工作','旅行病人服务','病人信息管理','上门家访','来电接听处理')])
		{
			if(map_event.containsKey('Length'+event.type))
			{
				map_event.put('Length'+event.type,map_event.get('Length'+event.type)+event.Custom_Duration__c);
			}else{
				map_event.put('Length'+event.type,event.Custom_Duration__c);
			}
			if(event.type == '电话随访' && event.PM_VisitStatus__c == '完成')
			{
				if(map_event.containsKey('number'+event.type))
				{
					map_event.put('number'+event.type,map_event.get('number'+event.type)+1);
				}else{
					map_event.put('number'+event.type,1);
				}
			}
		}
		//给PSR拜访月计划的字段赋值  实际值
		for(PM_PSRMonthPlan__c monthplan : list_monthPlan)
		{
			//monthplan.PM_Date__c = date.valueOf(monthplan.PM_Year__c+'-'+monthplan.PM_Month__c+'-1');
			//电话实际量
			monthplan.PM_ActualCall__c = map_event.get('number电话随访')==null?0:map_event.get('number电话随访');
			//电话实际时长
			monthplan.PM_ActualCallLength__c = map_event.get('Length电话随访')==null?0:map_event.get('Length电话随访');
			//邮件实际时长
			monthplan.PM_ActualEmail__c = map_event.get('Length邮件处理')==null?0:map_event.get('Length邮件处理');
			//保存信息实际时长
			monthplan.PM_ActualSaveInfo__c = map_event.get('Length保存信息')==null?0:map_event.get('Length保存信息');
			//沟通协调实际时长
			monthplan.PM_ActualCommunicate__c = map_event.get('Length沟通协调')==null?0:map_event.get('Length沟通协调');
			//制作报告实际时长
			monthplan.PM_ActualMakeReport__c = map_event.get('Length制作报告')==null?0:map_event.get('Length制作报告');
			//参加培训实际时长
			monthplan.PM_ActualTraining__c = map_event.get('Length参加培训')==null?0:map_event.get('Length参加培训');
			//其他日常工作实际时长
			monthplan.PM_ActualOther__c = map_event.get('Length其他日常工作')==null?0:map_event.get('Length其他日常工作');
			//旅行病人服务实际时长
			monthplan.PM_ActualTraveling__c = map_event.get('Length旅行病人服务')==null?0:map_event.get('Length旅行病人服务');
			//病人信息管理实际时长
			monthplan.PM_ActualPatientMan__c = map_event.get('Length病人信息管理')==null?0:map_event.get('Length病人信息管理');
			//上门家访实际时长
			monthplan.PM_ActualVisit__c = map_event.get('Length上门家访')==null?0:map_event.get('Length上门家访');
			//来电接听处理实际时长
			monthplan.PM_ActualCallAnswer__c = map_event.get('Length来电接听处理')==null?0:map_event.get('Length来电接听处理');
		}
		if(list_monthPlan != null && list_monthPlan.size()>0)
		{
			update list_monthPlan;
		}
    }
    global void finish(Database.BatchableContext BC)
    {
		if(Run_Date == null)
		{
			Run_Date = date.today();
		}
		//年份
		string year = string.valueOf(Run_Date.Year());
		//月份
		string month = string.valueOf(Run_Date.Month());
		//服务团队 ServiceTeam 数量排名
		Map<String,Double> map_ServiceTeam = new Map<String,Double>();
		//教育团队Education team 数量排名
		Map<String,Double> map_EducationTeam = new Map<String,Double>();
		//数据团队Data team 数量排名
		Map<String,Double> map_DataTeam = new Map<String,Double>();
		//资深教育团队Senior Education team 数量排名
		Map<String,Double> map_SrEducationTeam = new Map<String,Double>();
		//服务团队 ServiceTeam 时长排名
		Map<String,Double> map_ServiceTeamLength = new Map<String,Double>();
		//教育团队Education team 时长排名
		Map<String,Double> map_EducationTeamLength = new Map<String,Double>();
		//数据团队Data team 时长排名
		Map<String,Double> map_DataTeamLength = new Map<String,Double>();
		//资深教育团队Senior Education team 时长排名
		Map<String,Double> map_SrEducationTeamLength = new Map<String,Double>();
		
		List<PM_PSRMonthPlan__c> list_psr = [Select p.PM_PhoneNumRate__c,p.Owner.Title,p.OwnerId,p.PM_Ranking__c,PM_ActualCallLength__c
				 From PM_PSRMonthPlan__c p where p.Owner.IsActive = true and PM_Year__c = : year and PM_Month__c = : month and PM_Status__c = '批准'
				 order by PM_PhoneNumRate__c asc];
		for(PM_PSRMonthPlan__c monthPlan : list_psr)
		{
			//服务团队 Service Team
			if(monthPlan.Owner.Title.toUpperCase().contains('SERVICE'))
			{
				map_ServiceTeam.put(monthPlan.OwnerId,(monthPlan.PM_PhoneNumRate__c==null?0:monthPlan.PM_PhoneNumRate__c));
				map_ServiceTeamLength.put(monthPlan.OwnerId,(monthPlan.PM_ActualCallLength__c==null?0:monthPlan.PM_ActualCallLength__c));
			}
			//教育团队Education Team
			if(monthPlan.Owner.Title.toUpperCase().contains('EDUCATION') && !monthPlan.Owner.Title.toUpperCase().contains('SENIOR'))
			{
				map_EducationTeam.put(monthPlan.OwnerId,(monthPlan.PM_PhoneNumRate__c==null?0:monthPlan.PM_PhoneNumRate__c));
				map_EducationTeamLength.put(monthPlan.OwnerId,(monthPlan.PM_ActualCallLength__c==null?0:monthPlan.PM_ActualCallLength__c));
			}
			//数据团队Data Team
			if(monthPlan.Owner.Title.toUpperCase().contains('DATA'))
			{
				map_DataTeam.put(monthPlan.OwnerId,(monthPlan.PM_PhoneNumRate__c==null?0:monthPlan.PM_PhoneNumRate__c));
				map_DataTeamLength.put(monthPlan.OwnerId,(monthPlan.PM_ActualCallLength__c==null?0:monthPlan.PM_ActualCallLength__c));
			}
			//资深教育团队Senior Education Team
			if(monthPlan.Owner.Title.toUpperCase().contains('SENIOR'))
			{
				map_SrEducationTeam.put(monthPlan.OwnerId,(monthPlan.PM_PhoneNumRate__c==null?0:monthPlan.PM_PhoneNumRate__c));
				map_SrEducationTeamLength.put(monthPlan.OwnerId,(monthPlan.PM_ActualCallLength__c==null?0:monthPlan.PM_ActualCallLength__c));
			}
		}
		//服务团队 Service Team  数量排名
		map<double,double> map_ServiceRanking = RankingSort(map_ServiceTeam);
		//教育团队Education Team 数量排名
		map<double,double> map_Eduking = RankingSort(map_EducationTeam);
		//数据团队Data Team 数量排名
		map<double,double> map_DataRanking = RankingSort(map_DataTeam);
		//资深教育团队Senior Education Team 数量排名
		map<double,double> map_SeniorRanking = RankingSort(map_SrEducationTeam);
		//服务团队 Service Team  时长排名
		map<double,double> map_ServiceLengthRanking = RankingSort(map_ServiceTeamLength);
		//教育团队Education Team 时长排名
		map<double,double> map_EduLengthking = RankingSort(map_EducationTeamLength);
		//数据团队Data Team 时长排名
		map<double,double> map_DataLengthRanking = RankingSort(map_DataTeamLength);
		//资深教育团队Senior Education Team 时长排名
		map<double,double> map_SeniorLengthRanking = RankingSort(map_SrEducationTeamLength);
		
		//团队内部成员排名
		for(PM_PSRMonthPlan__c monthPlan : list_psr)
		{
			monthPlan.PM_Ranking__c = 0;
			monthPlan.PM_LengthRanking__c = 0;
			//数量排名
			if(map_ServiceTeam.containsKey(monthPlan.OwnerId))
			{
				monthPlan.PM_Ranking__c = map_ServiceRanking.get(map_ServiceTeam.get(monthPlan.OwnerId))-1;
			}
			else if(map_EducationTeam.containsKey(monthPlan.OwnerId))
			{
				monthPlan.PM_Ranking__c = map_Eduking.get(map_EducationTeam.get(monthPlan.OwnerId))-1;
			}
			else if(map_DataTeam.containsKey(monthPlan.OwnerId))
			{
				monthPlan.PM_Ranking__c = map_DataRanking.get(map_DataTeam.get(monthPlan.OwnerId))-1;
			}
			else if(map_SrEducationTeam.containsKey(monthPlan.OwnerId))
			{
				monthPlan.PM_Ranking__c = map_SeniorRanking.get(map_SrEducationTeam.get(monthPlan.OwnerId))-1;
			}
			//时长排名
			if(map_ServiceTeamLength.containsKey(monthPlan.OwnerId))
			{
				monthPlan.PM_LengthRanking__c = map_ServiceLengthRanking.get(map_ServiceTeamLength.get(monthPlan.OwnerId))-1;
			}
			else if(map_EducationTeamLength.containsKey(monthPlan.OwnerId))
			{
				monthPlan.PM_LengthRanking__c = map_EduLengthking.get(map_EducationTeamLength.get(monthPlan.OwnerId))-1;
			}
			else if(map_DataTeamLength.containsKey(monthPlan.OwnerId))
			{
				monthPlan.PM_LengthRanking__c = map_DataLengthRanking.get(map_DataTeamLength.get(monthPlan.OwnerId))-1;
			} 
			else if(map_SrEducationTeamLength.containsKey(monthPlan.OwnerId))
			{
				monthPlan.PM_LengthRanking__c = map_SeniorLengthRanking.get(map_SrEducationTeamLength.get(monthPlan.OwnerId))-1;
			}
		}
		
		if(list_psr!=null && list_psr.size()>0)
		{
			update list_psr;
		}
		PM_WebServiceUtil.sendMail('拜访月计划汇总');
    }
    
    //排名次方法
    private map<Double,double> RankingSort(map<string,double> map_Rarking)
    {
    	map<Double,double> map_Sort = new map<Double,double>();
		//double + 电话量名次
		Set<double> set_double = new Set<double>();
		for(Double num : map_Rarking.values())
		{
			set_double.add(num);
		}
		List<double> list_double = new List<double>();
		list_double.addAll(set_double);
		double start;
		for(integer i=0; i<list_double.size();i++)
		{
			start = 1;
			for(integer j=0; j<list_double.size();j++)
			{
				if(list_double[i]<=list_double[j])
				{
					start++;
					map_Sort.put(list_double[i],start);
				}
			}
		}
		return map_Sort;
    }
}