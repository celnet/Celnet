/**
 * Hank
 * 2013-10-16
 * 功能：点击新病人审批拒绝按钮，对新病人进行单一拒绝审批
 */
public with sharing class PM_RefuseApprovalController 
{
    public PatientApply__c patientApply{get;set;}
    public String Reject_Reason{get;set;}
    public string patId{get;set;}
    public boolean after_determine{get;set;}
    public PM_RefuseApprovalController(ApexPages.StandardController controller)
    {
        patId = ApexPages.currentPage().getParameters().get('patId');
        if(patId!=null)
        {
            list<PatientApply__c> queryResultnew = [SELECT Id,Status__c FROM PatientApply__c WHERE Id =: patId ];
            if(queryResultnew !=null && queryResultnew.size() > 0)
            {
                patientApply = queryResultnew[0];
            }
        }
    }
    //确定按钮
    public void Agree()
    {
        if(patientApply.Status__c == '通过' || patientApply.Status__c == '拒绝') 
        { 
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR , '该病人已进行过审批，不能重复审批！') ;            
            ApexPages.addMessage(msg);
        }
        else
        {
            patientApply.Status__c = '拒绝';
            patientApply.RejectReason__c = Reject_Reason;
            patientApply.PM_NewPatientApprover__c = userinfo.getUserId();
            patientApply.PM_ApproverDate__c = datetime.now();
            if(patientApply!=null)
            {
                try
                {
                    update patientApply;
                    after_determine = true;
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM , '拒绝成功') ;            
                    ApexPages.addMessage(msg);
                }
                catch(DmlException ex)
                {
                    ApexPages.addMessages(ex);
                }
            }
        }
    }
}