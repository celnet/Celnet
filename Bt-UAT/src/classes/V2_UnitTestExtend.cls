/**
Description: test class, just for testing, as some reason, such as, when the percent of coverage is not
enough which caused by other code which wrote by others, and we can't change them, so, append this class for
writing the new test code to improve the coverage. 
@Date:2012/09/25, Ken
*/
public class V2_UnitTestExtend {

    public static testMethod void testMyController() {
    	Test.startTest();
    	String ids='';
    	List<ProcessInstance> pis=[Select p.Id,p.TargetObjectId From ProcessInstance p limit 5];
    	if(pis!=null && pis.size()>0){
    		for(ProcessInstance pi: pis){
    			ids+=pi.TargetObjectId+',';
    		}
    	}
    	V2_BatchApprovalProcess.SubmitApproval(ids);
    	V2_BatchApprovalProcess.SubmitRejection(ids);
    	
    	contact_rule__c cr=[select Id, Name,Contact__c,Contact__r.id,Sum_Dialyse__c,ESRD_Percent__c
        ,isPrimary__c,Title__c,Role__c,Influence__c,visit_strategy__c,ChangeMode__c,Modeproof__c,Grade__c,SupportProof__c
        ,Pa_Month__c,Rec_Renal__c,Rec_rate__c,Dialyse_ESRD__c,Dialyse_Percent__c,PD_No__c
        ,PD_Take_on_rate__c
        ,contact_rule__c.opportunity_name__r.accountId
        ,contact_rule__c.opportunity_name__r.id
        from contact_rule__c where contact_rule__c.opportunity_name__r.id!=null 
        and contact_rule__c.opportunity_name__r.accountId!=null
        limit 1];
        
        Opportunity oppty=[select id,name from opportunity where id=:cr.opportunity_name__r.id limit 1];
        String opptyid=oppty.id;
        PageReference pageRef = new PageReference('/'+opptyid);
        Test.setCurrentPage(pageRef);
      
      	ContactRole controller = new ContactRole();
        
        // Add parameters to page URL 
    
        ApexPages.currentPage().getParameters().put('id', opptyid);
        List<contact_rule__c> cRoles=controller.getContactRoles();
        String opptyname=controller.getOpportunityname();
        System.assertEquals(oppty.name, opptyname);
        String nextPage = controller.cancel().getUrl();
        System.assertEquals('/'+opptyid, nextPage);
        
        //Contact contact=[select id from contact limit 1];
        Id contactId=cr.Contact__r.id;
        controller.newrole.contact__c = contactId;
        controller.newrole.role__c='用户(U)';
        String afterSave = controller.save().getUrl();
        System.assertEquals('/'+opptyid, afterSave);
		/*
        ApexPages.StandardController stCtl = new ApexPages.StandardController(cr);
        ContactRole ctl = new ContactRole(stCtl);
        nextPage = ctl.cancel().getUrl();
        ctl.newrole.contact__c = contactId;
        ctl.newrole.role__c='用户(U)';
        afterSave = ctl.save().getUrl();
        */
    	Test.stopTest();
   }
}