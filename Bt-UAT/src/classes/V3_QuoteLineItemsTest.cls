/**
 */
@isTest
private class V3_QuoteLineItemsTest {

    
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        try{
        	Account acc = new Account();
            acc.MID__c = '943034110';
            acc.PriceGroup__c='GOVTHOSP';
            acc.Name ='Name';
 			insert acc;
 			Contact cont = new Contact();
 			cont.LastName='test';
 			cont.AccountId=acc.id;
 			insert cont;
 			
        //创建业务机会	
        Opportunity opt = new Opportunity();
        opt.AccountId=acc.id;
    	opt.name='test';
    	opt.StageName='test';
    	opt.CloseDate=Date.today();
    	insert opt;
    	//创建报价
    	Quote qte = new Quote();
    	qte.V3_Currency__c='HKD';
    	qte.Name='test';
    	qte.OpportunityId=opt.id;
    	qte.ContactId=cont.id;
    	insert qte;
        
        //创建价格手册
        Pricebook2 pb = new Pricebook2();
        pb.Name='HK PriceBook';
        pb.IsActive = true;
        insert pb;	
        
        //创建产品;	
        Product2 pro=new Product2();
        pro.Name='p1';
        pro.V3_Item_Number__c='p1';
        pro.Description='test description';
        pro.IsActive=true;
        pro.V3_Pack_factor__c=100;
        pro.V3_X1st_UOM__c='EA';
        pro.V3_X2nd_UOM__c='CA';
        //pro.Product_Code__c ='123';
        insert pro;
        
        //创建价格
        Product_Price__c pp = new Product_Price__c();
        pp.Currency__c='HKD';
        pp.Customer_No__c='943034110';
        pp.Eff_From__c=date.today()-1;
        pp.Eff_To__c=date.today()+1;
  		pp.UOM__c='EA';
  		pp.Price__c=200;
  		pp.Name='p1';
  		insert pp;
  		
        PageReference page = System.Page.V3_QuoteLineItems;
        page.getParameters().put('QuoteId',qte.Id);
        Test.setCurrentPage(page);
        
        ApexPages.StandardController con = new ApexPages.StandardController(qte);
        V3_QuoteLineItems control = new V3_QuoteLineItems(con);
        
        System.debug('**************'+control.listEntityObject.size());        
        
        Integer i = control.dataSize;
        control.product.GBU__c='1';
        control.product.SBU__c='2';
        control.product.V3_Grouping__c='3';
        control.product.Name='4';
        
        control.Query();
        
        control.product.GBU__c=null;
        control.product.SBU__c=null;
        control.product.V3_Grouping__c=null;
        control.product.Name=null;
        
        control.Query();
        
        
 		control.listEntityObject[0].binIsSelected=true;
        control.Check();
        
        System.debug('**************'+control.listEntityObjectSelect.size());        
        
        control.listEntityObjectSelect[0].binIsSelected=true;
        control.DeleteItem();
        
        control.listEntityObject[0].binIsSelected=true;
        control.Check();
        
        control.QueryNext();
        
       for(V3_EntityObject EntityObject:control.listEntityObjectSelect){
       		EntityObject.obj_ProductPrice.V3_valueUOM__c='EA';
       		EntityObject.obj_ProductPrice.Qty__c=1;
       }
       control.Finish();
            
         control.ShowErrorMsg('dddd');
         control.ShowInfoMsg('ddddddd');   
            
         control.Cancel();
            
         /////////////////////////////////////////////edit
        page = System.Page.V3_EditQuoteLineItems;
        page.getParameters().put('QuoteId',qte.Id);
        Test.setCurrentPage(page);
        
        QuoteLineItem item = [select id from quotelineitem where quoteid=:qte.id limit 1];
        ApexPages.StandardController con1 = new ApexPages.StandardController(item);
        V3_EditQuoteLineItems control1 = new V3_EditQuoteLineItems(con1);
  
        control1.Save()    ;
        
        	for(QuoteLineItem EntityObject:control1.lstSelectProductPrice){
        		EntityObject.Product_Price__r.V3_valueUOM__c='CA';
        	}
        control1.Save()    ;
        	for(QuoteLineItem EntityObject:control1.lstSelectProductPrice){
        		EntityObject.Product_Price__r.V3_valueUOM__c='AA';
        	}
        control1.Save()    ;

        control1.ShowErrorMsg('333');
        control1.ShowInfoMsg('ddd');
        control1.Cancel();
        
        }catch(exception e){
            
        }
    }

    
  

}