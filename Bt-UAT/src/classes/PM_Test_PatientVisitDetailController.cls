/**
 * author : Sunny Sun
 * Test PM_PatientVisitDetailController
 */
@isTest
private class PM_Test_PatientVisitDetailController {

    static testMethod void myUnitTest() {
        PM_Test_AllConfig.myUnitTest(); //公用方法，内涵必要的一些测试数据
        //account
        ID HospitalRTId = [Select id from RecordType Where SobjectType = 'Account' And DeveloperName = 'RecordType'].Id;
        ID DisRTId = [Select id from RecordType Where SobjectType = 'Account' And DeveloperName = 'RecordType_d_2'].Id;
        List<Account> listAcc = new List<Account>();
        Account acc1= new Account();
        acc1.Name = '张北医院';
        acc1.RecordTypeId = HospitalRTId;
        listAcc.add(acc1);
        //insert listAcc;
        Account acc2= new Account();
        acc2.Name = '张北经销商';
        acc2.RecordTypeId = DisRTId;
        listAcc.add(acc2);
        insert listAcc;
        //医院经销商关系
        List<PM_HosDealerRelation__c> listhdr = new List<PM_HosDealerRelation__c>();
        PM_HosDealerRelation__c hdr1 = new PM_HosDealerRelation__c();
        hdr1.PM_Hospital__c = acc1.Id ;
        hdr1.PM_Distributor__c = acc2.Id;
        listhdr.add(hdr1);
        insert listhdr;
        //病人
        List<PM_Patient__c> lp = new List<PM_Patient__c>();
        PM_Patient__c patient = new PM_Patient__c();
        patient.Name = '张杰' ;
        patient.PM_Status__c = 'New' ;
        patient.PM_TelPriorityFirst__c = '家庭电话';
        patient.PM_TelPrioritySecond__c = '病人手机';
        patient.PM_DealerRemark__c = 'xxx';
        //patient.PM_Pdtreatment__c = 'APD';
        lp.add(patient);
        PM_patient__c patient2 = new PM_patient__c();
        patient2.Name = '张杰' ;
        patient2.PM_Status__c = 'Active' ;
        patient2.PM_DropOut_One_Reason__c = '死亡';
        patient2.PM_TelPriorityFirst__c = '病人电话';
        patient2.PM_TelPrioritySecond__c = '家属手机';
        patient2.PM_Terminal__c = acc1.Id;
        lp.add(patient2);
        PM_patient__c patient3 = new PM_patient__c();
        patient3.Name = '张杰' ;
        patient3.PM_Status__c = 'Unreachable' ;
        patient3.PM_TelPriorityFirst__c = '家属电话';
        patient3.PM_TelPrioritySecond__c = '其他电话';
        lp.add(patient3);
        PM_patient__c patient4 = new PM_patient__c();
        patient4.Name = '张杰' ;
        patient4.PM_Status__c = 'Dropout' ;
        patient4.PM_TelPriorityFirst__c = '病人手机';
        patient4.PM_TelPrioritySecond__c = '家庭电话';
        patient4.PM_DropOut_One_Reason__c = '转用竞争产品';
        lp.add(patient4);
        PM_patient__c patient5 = new PM_patient__c();
        patient5.Name = '张杰' ;
        patient5.PM_Status__c = 'Dropout' ;
        patient5.PM_TelPriorityFirst__c = '家属手机';
        patient5.PM_TelPrioritySecond__c = '病人电话';
        lp.add(patient5);
        PM_patient__c patient6 = new PM_patient__c();
        patient6.Name = '张杰' ;
        patient6.PM_Status__c = 'New' ;
        patient6.PM_TelPriorityFirst__c = '其他电话';
        patient6.PM_TelPrioritySecond__c = '家属电话';
        lp.add(patient6);
        insert lp;
        
        system.test.startTest();
        Apexpages.currentPage().getParameters().put('id',patient.Id);
        ApexPages.StandardController controller = new ApexPages.StandardController(patient);
        PM_PatientVisitDetailController PatientPage1 = new PM_PatientVisitDetailController(controller);
        
        system.debug(PatientPage1.patientInfo.PM_TelPriorityFirst__c + ' - '+PatientPage1.patientInfo.PM_TelPrioritySecond__c);
        PatientPage1.getList_DealerFeedback();
        PatientPage1.getList_FreeTPApplication();
        PatientPage1.getList_HotLine();
        PatientPage1.getList_Mail();
        PatientPage1.getList_PatientFeedback();
        PatientPage1.getList_ProductFeedback();
        PatientPage1.getList_VisitFail();
        PatientPage1.visitSuccess();
        PatientPage1.visitFail();
        //PatientPage1.setUnreachable() ;
        //PatientPage1.setDropOut();
        //PatientPage1.setDiversionDropout();
        //PatientPage1.setActive();
        
        system.Test.stopTest();
        
    }
    static testMethod void myUnitTest0() {
        PM_Test_AllConfig.myUnitTest(); //公用方法，内涵必要的一些测试数据
        //account
        ID HospitalRTId = [Select id from RecordType Where SobjectType = 'Account' And DeveloperName = 'RecordType'].Id;
        ID DisRTId = [Select id from RecordType Where SobjectType = 'Account' And DeveloperName = 'RecordType_d_2'].Id;
        List<Account> listAcc = new List<Account>();
        Account acc1= new Account();
        acc1.Name = '张北医院';
        acc1.RecordTypeId = HospitalRTId;
        listAcc.add(acc1);
        //insert listAcc;
        Account acc2= new Account();
        acc2.Name = '张北经销商';
        acc2.RecordTypeId = DisRTId;
        listAcc.add(acc2);
        insert listAcc;
        //医院经销商关系
        List<PM_HosDealerRelation__c> listhdr = new List<PM_HosDealerRelation__c>();
        PM_HosDealerRelation__c hdr1 = new PM_HosDealerRelation__c();
        hdr1.PM_Hospital__c = acc1.Id ;
        hdr1.PM_Distributor__c = acc2.Id;
        listhdr.add(hdr1);
        insert listhdr;
        //病人
        List<PM_Patient__c> lp = new List<PM_Patient__c>();
        PM_Patient__c patient = new PM_Patient__c();
        patient.Name = '张杰' ;
        patient.PM_Status__c = 'New' ;
        patient.PM_TelPriorityFirst__c = '家庭电话';
        patient.PM_TelPrioritySecond__c = '病人手机';
        patient.PM_DealerRemark__c = 'xxx';
        //patient.PM_Pdtreatment__c = 'APD';
        lp.add(patient);
        PM_patient__c patient2 = new PM_patient__c();
        patient2.Name = '张杰' ;
        patient2.PM_Status__c = 'Active' ;
        patient2.PM_DropOut_One_Reason__c = '死亡';
        patient2.PM_TelPriorityFirst__c = '病人电话';
        patient2.PM_TelPrioritySecond__c = '家属手机';
        patient2.PM_Terminal__c = acc1.Id;
        lp.add(patient2);
        PM_patient__c patient3 = new PM_patient__c();
        patient3.Name = '张杰' ;
        patient3.PM_Status__c = 'Unreachable' ;
        patient3.PM_TelPriorityFirst__c = '家属电话';
        patient3.PM_TelPrioritySecond__c = '其他电话';
        lp.add(patient3);
        PM_patient__c patient4 = new PM_patient__c();
        patient4.Name = '张杰' ;
        patient4.PM_Status__c = 'Dropout' ;
        patient4.PM_TelPriorityFirst__c = '病人手机';
        patient4.PM_TelPrioritySecond__c = '家庭电话';
        patient4.PM_DropOut_One_Reason__c = '转用竞争产品';
        lp.add(patient4);
        PM_patient__c patient5 = new PM_patient__c();
        patient5.Name = '张杰' ;
        patient5.PM_Status__c = 'Dropout' ;
        patient5.PM_TelPriorityFirst__c = '家属手机';
        patient5.PM_TelPrioritySecond__c = '病人电话';
        lp.add(patient5);
        PM_patient__c patient6 = new PM_patient__c();
        patient6.Name = '张杰' ;
        patient6.PM_Status__c = 'New' ;
        patient6.PM_TelPriorityFirst__c = '其他电话';
        patient6.PM_TelPrioritySecond__c = '家属电话';
        lp.add(patient6);
        insert lp;
        
        system.test.startTest();
        Apexpages.currentPage().getParameters().put('id',patient.Id);
        ApexPages.StandardController controller = new ApexPages.StandardController(patient);
        PM_PatientVisitDetailController PatientPage1 = new PM_PatientVisitDetailController(controller);
        
        system.debug(PatientPage1.patientInfo.PM_TelPriorityFirst__c + ' - '+PatientPage1.patientInfo.PM_TelPrioritySecond__c);
        PatientPage1.getList_DealerFeedback();
        PatientPage1.getList_FreeTPApplication();
        PatientPage1.getList_HotLine();
        PatientPage1.getList_Mail();
        PatientPage1.getList_PatientFeedback();
        PatientPage1.getList_ProductFeedback();
        //PatientPage1.getList_VisitFail();
        //PatientPage1.visitSuccess();
        //PatientPage1.visitFail();
        PatientPage1.setUnreachable() ;
        PatientPage1.setDropOut();
        PatientPage1.setDiversionDropout();
        PatientPage1.setActive();
        
        system.Test.stopTest();
        
    }
    
    static testMethod void myUnitTest2() 
    {
        PM_Test_AllConfig.myUnitTest(); //公用方法，内涵必要的一些测试数据
        system.Test.startTest();
        list<PM_Patient__c> patient = [Select Id From PM_Patient__c Where Name = '张先生Test'];
        ApexPages.currentPage().getParameters().put('id',patient[0].Id);
        ApexPages.currentPage().getParameters().put('pStatus','Active');
        ApexPages.StandardController controller = new ApexPages.StandardController(patient[0]);
        PM_PatientVisitDetailController pvc = new PM_PatientVisitDetailController(controller);
        pvc.save_question();
        pvc.savePatientBasicInfo();
        system.Test.stopTest();
    }
    static testMethod void myUnitTest3() 
    {
        PM_Test_AllConfig.myUnitTest(); //公用方法，内涵必要的一些测试数据
        //account
        ID HospitalRTId = [Select id from RecordType Where SobjectType = 'Account' And DeveloperName = 'RecordType'].Id;
        ID DisRTId = [Select id from RecordType Where SobjectType = 'Account' And DeveloperName = 'RecordType_d_2'].Id;
        List<Account> listAcc = new List<Account>();
        Account acc1= new Account();
        acc1.Name = '张北医院';
        acc1.RecordTypeId = HospitalRTId;
        listAcc.add(acc1);
        //insert listAcc;
        Account acc2= new Account();
        acc2.Name = '张北经销商';
        acc2.RecordTypeId = DisRTId;
        listAcc.add(acc2);
        insert listAcc;
        //医院经销商关系
        List<PM_HosDealerRelation__c> listhdr = new List<PM_HosDealerRelation__c>();
        PM_HosDealerRelation__c hdr1 = new PM_HosDealerRelation__c();
        hdr1.PM_Hospital__c = acc1.Id ;
        hdr1.PM_Distributor__c = acc2.Id;
        listhdr.add(hdr1);
        insert listhdr;
        //病人
        List<PM_Patient__c> lp = new List<PM_Patient__c>();
        PM_Patient__c patient = new PM_Patient__c();
        patient.Name = '张杰' ;
        patient.PM_Status__c = 'New' ;
        patient.PM_TelPriorityFirst__c = '家庭电话';
        patient.PM_TelPrioritySecond__c = '病人手机';
        patient.PM_DealerRemark__c = 'xxx';
        patient.PM_Pdtreatment__c = 'APD';
        lp.add(patient);
        PM_patient__c patient2 = new PM_patient__c();
        patient2.Name = '张杰' ;
        patient2.PM_Status__c = 'Active' ;
        patient2.PM_DropOut_One_Reason__c = '死亡';
        patient2.PM_TelPriorityFirst__c = '病人电话';
        patient2.PM_TelPrioritySecond__c = '家属手机';
        patient2.PM_Terminal__c = acc1.Id;
        lp.add(patient2);
        PM_patient__c patient3 = new PM_patient__c();
        patient3.Name = '张杰' ;
        patient3.PM_Status__c = 'Unreachable' ;
        patient3.PM_TelPriorityFirst__c = '家属电话';
        patient3.PM_TelPrioritySecond__c = '其他电话';
        lp.add(patient3);
        PM_patient__c patient4 = new PM_patient__c();
        patient4.Name = '张杰' ;
        patient4.PM_Status__c = 'Dropout' ;
        patient4.PM_TelPriorityFirst__c = '病人手机';
        patient4.PM_TelPrioritySecond__c = '家庭电话';
        patient4.PM_DropOut_One_Reason__c = '转用竞争产品';
        lp.add(patient4);
        PM_patient__c patient5 = new PM_patient__c();
        patient5.Name = '张杰' ;
        patient5.PM_Status__c = 'New' ;
        patient5.PM_TelPriorityFirst__c = '家属手机';
        patient5.PM_TelPrioritySecond__c = '病人电话';
        lp.add(patient5);
        PM_patient__c patient6 = new PM_patient__c();
        patient6.Name = '张杰' ;
        patient6.PM_Status__c = 'New' ;
        patient6.PM_TelPriorityFirst__c = '其他电话';
        patient6.PM_TelPrioritySecond__c = '家属电话';
        lp.add(patient6);
        insert lp;
        
        system.test.startTest();
        
        Apexpages.currentPage().getParameters().put('id',patient.Id);
        ApexPages.StandardController controller = new ApexPages.StandardController(patient);
        PM_PatientVisitDetailController PatientPage1 = new PM_PatientVisitDetailController(controller);
        
        Apexpages.currentPage().getParameters().put('id',patient4.Id);
        ApexPages.StandardController controller4 = new ApexPages.StandardController(patient4);
        PM_PatientVisitDetailController PatientPage4 = new PM_PatientVisitDetailController(controller4);
        system.debug(PatientPage4.patientInfo.PM_TelPriorityFirst__c + ' - '+PatientPage4.patientInfo.PM_TelPrioritySecond__c);
        PatientPage4.patientInfo.PM_UrNitrogen__c = 2000;
        PatientPage4.savePatientBasicInfo();
        PatientPage4.patientStatusInfo.PM_UrNitrogen__c = 2000;
        PatientPage4.visitFail();
        PatientPage4.visitSuccess();
        PatientPage4.setUnreachable();
        PatientPage4.setDropOut();
        PatientPage4.setDiversionDropout();
        PatientPage4.setActive();
        
        Apexpages.currentPage().getParameters().put('id',patient5.Id);
        ApexPages.StandardController controller5 = new ApexPages.StandardController(patient5);
        PM_PatientVisitDetailController PatientPage5 = new PM_PatientVisitDetailController(controller5);
        system.debug(PatientPage5.patientInfo.PM_TelPriorityFirst__c + ' - '+PatientPage5.patientInfo.PM_TelPrioritySecond__c);
        PatientPage5.patientInfo.PM_SuggestVisitTime__c = 'sdfsdf';
        PatientPage5.visitFail();
        /*
        Apexpages.currentPage().getParameters().put('id',patient6.Id);
        ApexPages.StandardController controller6 = new ApexPages.StandardController(patient6);
        PM_PatientVisitDetailController PatientPage6 = new PM_PatientVisitDetailController(controller6);
        system.debug(PatientPage6.patientInfo.PM_TelPriorityFirst__c + ' - '+PatientPage6.patientInfo.PM_TelPrioritySecond__c);
        PatientPage6.Defaultdate = null;
        PatientPage6.setDropOut();
        
        Apexpages.currentPage().getParameters().put('id',patient6.Id);
        ApexPages.StandardController controller7 = new ApexPages.StandardController(patient6);
        PM_PatientVisitDetailController PatientPage7 = new PM_PatientVisitDetailController(controller7);
        system.debug(PatientPage7.patientInfo.PM_TelPriorityFirst__c + ' - '+PatientPage7.patientInfo.PM_TelPrioritySecond__c);
        PatientPage7.setUnreachable();
        */
        system.test.stopTest();
    }
    static testMethod void myUnitTest31() 
    {
        PM_Test_AllConfig.myUnitTest(); //公用方法，内涵必要的一些测试数据
        //account
        ID HospitalRTId = [Select id from RecordType Where SobjectType = 'Account' And DeveloperName = 'RecordType'].Id;
        ID DisRTId = [Select id from RecordType Where SobjectType = 'Account' And DeveloperName = 'RecordType_d_2'].Id;
        List<Account> listAcc = new List<Account>();
        Account acc1= new Account();
        acc1.Name = '张北医院';
        acc1.RecordTypeId = HospitalRTId;
        listAcc.add(acc1);
        //insert listAcc;
        Account acc2= new Account();
        acc2.Name = '张北经销商';
        acc2.RecordTypeId = DisRTId;
        listAcc.add(acc2);
        insert listAcc;
        //医院经销商关系
        List<PM_HosDealerRelation__c> listhdr = new List<PM_HosDealerRelation__c>();
        PM_HosDealerRelation__c hdr1 = new PM_HosDealerRelation__c();
        hdr1.PM_Hospital__c = acc1.Id ;
        hdr1.PM_Distributor__c = acc2.Id;
        listhdr.add(hdr1);
        insert listhdr;
        //病人
        List<PM_Patient__c> lp = new List<PM_Patient__c>();
        PM_Patient__c patient = new PM_Patient__c();
        patient.Name = '张杰' ;
        patient.PM_Status__c = 'New' ;
        patient.PM_TelPriorityFirst__c = '家庭电话';
        patient.PM_TelPrioritySecond__c = '病人手机';
        patient.PM_DealerRemark__c = 'xxx';
        patient.PM_Pdtreatment__c = 'APD';
        lp.add(patient);
        PM_patient__c patient2 = new PM_patient__c();
        patient2.Name = '张杰' ;
        patient2.PM_Status__c = 'Active' ;
        patient2.PM_DropOut_One_Reason__c = '死亡';
        patient2.PM_TelPriorityFirst__c = '病人电话';
        patient2.PM_TelPrioritySecond__c = '家属手机';
        patient2.PM_Terminal__c = acc1.Id;
        lp.add(patient2);
        PM_patient__c patient3 = new PM_patient__c();
        patient3.Name = '张杰' ;
        patient3.PM_Status__c = 'Unreachable' ;
        patient3.PM_TelPriorityFirst__c = '家属电话';
        patient3.PM_TelPrioritySecond__c = '其他电话';
        lp.add(patient3);
        PM_patient__c patient4 = new PM_patient__c();
        patient4.Name = '张杰' ;
        patient4.PM_Status__c = 'Dropout' ;
        patient4.PM_TelPriorityFirst__c = '病人手机';
        patient4.PM_TelPrioritySecond__c = '家庭电话';
        patient4.PM_DropOut_One_Reason__c = '转用竞争产品';
        lp.add(patient4);
        PM_patient__c patient5 = new PM_patient__c();
        patient5.Name = '张杰' ;
        patient5.PM_Status__c = 'New' ;
        patient5.PM_TelPriorityFirst__c = '家属手机';
        patient5.PM_TelPrioritySecond__c = '病人电话';
        lp.add(patient5);
        PM_patient__c patient6 = new PM_patient__c();
        patient6.Name = '张杰' ;
        patient6.PM_Status__c = 'New' ;
        patient6.PM_TelPriorityFirst__c = '其他电话';
        patient6.PM_TelPrioritySecond__c = '家属电话';
        lp.add(patient6);
        insert lp;
        
        system.test.startTest();
        /*
        Apexpages.currentPage().getParameters().put('id',patient.Id);
        ApexPages.StandardController controller = new ApexPages.StandardController(patient);
        PM_PatientVisitDetailController PatientPage1 = new PM_PatientVisitDetailController(controller);
        
        Apexpages.currentPage().getParameters().put('id',patient4.Id);
        ApexPages.StandardController controller4 = new ApexPages.StandardController(patient4);
        PM_PatientVisitDetailController PatientPage4 = new PM_PatientVisitDetailController(controller4);
        system.debug(PatientPage4.patientInfo.PM_TelPriorityFirst__c + ' - '+PatientPage4.patientInfo.PM_TelPrioritySecond__c);
        PatientPage4.patientInfo.PM_UrNitrogen__c = 2000;
        PatientPage4.savePatientBasicInfo();
        PatientPage4.patientStatusInfo.PM_UrNitrogen__c = 2000;
        PatientPage4.visitFail();
        PatientPage4.visitSuccess();
        PatientPage4.setUnreachable();
        PatientPage4.setDropOut();
        PatientPage4.setDiversionDropout();
        PatientPage4.setActive();
        
        Apexpages.currentPage().getParameters().put('id',patient5.Id);
        ApexPages.StandardController controller5 = new ApexPages.StandardController(patient5);
        PM_PatientVisitDetailController PatientPage5 = new PM_PatientVisitDetailController(controller5);
        system.debug(PatientPage5.patientInfo.PM_TelPriorityFirst__c + ' - '+PatientPage5.patientInfo.PM_TelPrioritySecond__c);
        PatientPage5.patientInfo.PM_SuggestVisitTime__c = 'sdfsdf';
        PatientPage5.visitFail();
        */
        Apexpages.currentPage().getParameters().put('id',patient6.Id);
        ApexPages.StandardController controller6 = new ApexPages.StandardController(patient6);
        PM_PatientVisitDetailController PatientPage6 = new PM_PatientVisitDetailController(controller6);
        system.debug(PatientPage6.patientInfo.PM_TelPriorityFirst__c + ' - '+PatientPage6.patientInfo.PM_TelPrioritySecond__c);
        PatientPage6.Defaultdate = null;
        PatientPage6.setDropOut();
        
        Apexpages.currentPage().getParameters().put('id',patient6.Id);
        ApexPages.StandardController controller7 = new ApexPages.StandardController(patient6);
        PM_PatientVisitDetailController PatientPage7 = new PM_PatientVisitDetailController(controller7);
        system.debug(PatientPage7.patientInfo.PM_TelPriorityFirst__c + ' - '+PatientPage7.patientInfo.PM_TelPrioritySecond__c);
        PatientPage7.setUnreachable();
        system.test.stopTest();
    }
    
    static testMethod void myUnitTest4() {
        PM_Test_AllConfig.myUnitTest(); //公用方法，内涵必要的一些测试数据
        //account
        ID HospitalRTId = [Select id from RecordType Where SobjectType = 'Account' And DeveloperName = 'RecordType'].Id;
        ID DisRTId = [Select id from RecordType Where SobjectType = 'Account' And DeveloperName = 'RecordType_d_2'].Id;
        List<Account> listAcc = new List<Account>();
        Account acc1= new Account();
        acc1.Name = '张北医院';
        acc1.RecordTypeId = HospitalRTId;
        listAcc.add(acc1);
        //insert listAcc;
        Account acc2= new Account();
        acc2.Name = '张北经销商';
        acc2.RecordTypeId = DisRTId;
        listAcc.add(acc2);
        insert listAcc;
        //医院经销商关系
        List<PM_HosDealerRelation__c> listhdr = new List<PM_HosDealerRelation__c>();
        PM_HosDealerRelation__c hdr1 = new PM_HosDealerRelation__c();
        hdr1.PM_Hospital__c = acc1.Id ;
        hdr1.PM_Distributor__c = acc2.Id;
        listhdr.add(hdr1);
        insert listhdr;
        //病人
        List<PM_Patient__c> lp = new List<PM_Patient__c>();
        PM_Patient__c patient = new PM_Patient__c();
        patient.Name = '张杰' ;
        patient.PM_Status__c = 'New' ;
        patient.PM_TelPriorityFirst__c = '家庭电话';
        patient.PM_TelPrioritySecond__c = '病人手机';
        patient.PM_DealerRemark__c = 'xxx';
        //patient.PM_Pdtreatment__c = 'APD';
        lp.add(patient);
        PM_patient__c patient2 = new PM_patient__c();
        patient2.Name = '张杰' ;
        patient2.PM_Status__c = 'Active' ;
        patient2.PM_DropOut_One_Reason__c = '死亡';
        patient2.PM_TelPriorityFirst__c = '病人电话';
        patient2.PM_TelPrioritySecond__c = '家属手机';
        patient2.PM_Terminal__c = acc1.Id;
        lp.add(patient2);
        PM_patient__c patient3 = new PM_patient__c();
        patient3.Name = '张杰' ;
        patient3.PM_Status__c = 'Unreachable' ;
        patient3.PM_TelPriorityFirst__c = '家属电话';
        patient3.PM_TelPrioritySecond__c = '其他电话';
        lp.add(patient3);
        PM_patient__c patient4 = new PM_patient__c();
        patient4.Name = '张杰' ;
        patient4.PM_Status__c = 'Dropout' ;
        patient4.PM_TelPriorityFirst__c = '病人手机';
        patient4.PM_TelPrioritySecond__c = '家庭电话';
        patient4.PM_DropOut_One_Reason__c = '转用竞争产品';
        lp.add(patient4);
        PM_patient__c patient5 = new PM_patient__c();
        patient5.Name = '张杰' ;
        patient5.PM_Status__c = 'Dropout' ;
        patient5.PM_TelPriorityFirst__c = '家属手机';
        patient5.PM_TelPrioritySecond__c = '病人电话';
        lp.add(patient5);
        PM_patient__c patient6 = new PM_patient__c();
        patient6.Name = '张杰' ;
        patient6.PM_Status__c = 'New' ;
        patient6.PM_TelPriorityFirst__c = '其他电话';
        patient6.PM_TelPrioritySecond__c = '家属电话';
        lp.add(patient6);
        insert lp;
        
        system.test.startTest();
        Apexpages.currentPage().getParameters().put('id',patient2.Id);
        ApexPages.StandardController controller2 = new ApexPages.StandardController(patient2);
        PM_PatientVisitDetailController PatientPage2 = new PM_PatientVisitDetailController(controller2);
        system.debug(PatientPage2.patientInfo.PM_TelPriorityFirst__c + ' - '+PatientPage2.patientInfo.PM_TelPrioritySecond__c);
        PatientPage2.visitSuccess();
        PatientPage2.visitFail();
        
        Apexpages.currentPage().getParameters().put('id',patient3.Id);
        ApexPages.StandardController controller3 = new ApexPages.StandardController(patient3);
        PM_PatientVisitDetailController PatientPage3 = new PM_PatientVisitDetailController(controller3);
        system.debug(PatientPage3.patientInfo.PM_TelPriorityFirst__c + ' - '+PatientPage3.patientInfo.PM_TelPrioritySecond__c);
        //PatientPage3.setUnreachable();
        
        
        system.Test.stopTest();
        
    }
}