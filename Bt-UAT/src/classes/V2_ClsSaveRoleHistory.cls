/*
Author: Eleen
Created on: 2012-1-10
Description: 
	This Class include the method for save User's Role to RoleHistory.
	It is run by V2_SaveRoleHistoryWebservice and V2_ClsSaveRoleHistoryScheduler 
*/
public class V2_ClsSaveRoleHistory {
	public static void V2_ClsSaveRoleHistory(String str_year,String str_month)
	{
		set<ID> set_Role=new set<ID>();//User's Role ID set collection
		
		set<String> set_GBU=new set<String>{'Renal','MD','BIOS','BQ'};//GBU set collection
		
		set<String> set_Level=new set<String>{'BU Head','National','Regional','Area ASM','District DSM','Supervisor','Rep'};//Level set collection
		
		List<UserRole> list_ur=[select Id,Name from UserRole];//Select all User's Role
		
		Map<ID,String> map_GBU=new Map<ID,String>();//UserRoleId and GBU map collection  
		Map<ID,String> map_Level=new Map<ID,String>();//UserRoleId and Level map collection
		Map<ID,String> map_Territory=new Map<ID,String>();//UserRoleId and Territory map collection
		Map<ID,String> map_PG=new Map<ID,String>();//UserRoleId and PG map collection
		
		for(UserRole ur:list_ur)
		{
			if(ur.Name.trim().contains('-') && ur.Name.trim().length()-ur.Name.trim().replaceAll('-','').length()==4)
			{
				Integer i=0;//init index
				string str_GBU,str_Level,str_Territory,str_PG,str_Title;//init GBU,Level,Territory,PG,Title
				i=ur.Name.lastIndexOf('-');//GET INDEX
				str_GBU=ur.Name.substring(0, ur.Name.indexOf('-'));//GET GBU
				str_Territory=ur.Name.substring(ur.Name.indexOf('-')+1, i);//GET Level+Territory+PG
				str_Level=str_Territory.substring(0, str_Territory.indexOf('-'));//GET Level
				str_Territory=str_Territory.substring(str_Territory.indexOf('-')+1,str_Territory.length());//GET Territory+PG
				str_PG=str_Territory.substring(str_Territory.indexOf('-')+1,str_Territory.length());//GET PG
				str_Territory=str_Territory.substring(0,str_Territory.indexOf('-'));//GET Territory
				str_Title=ur.Name.substring(i+1, ur.Name.length());//GET Title
				
				if(set_Level.contains(str_Level))
				{
					if(set_GBU.contains(str_GBU))
					{
						set_Role.add(ur.Id);
						if(!map_GBU.containsKey(ur.Id))map_GBU.put(ur.Id,str_GBU);
						if(!map_Level.containsKey(ur.Id))map_Level.put(ur.Id,str_Level);
						if(!map_Territory.containsKey(ur.Id))map_Territory.put(ur.Id,str_Territory);
						if(!map_PG.containsKey(ur.Id))map_PG.put(ur.Id,str_PG);
					}
				}
			}
		}
		
		if(set_Role.size()==0)return;
		else
		{
			//select all user that match the rule
			List<UserRole> list_UserRole=[select Id,Name,ParentRoleId,(select Id,Name,isActive,IsLeave__c,IsOnHoliday__c,EmployeeNumber,Department,ManagerId,Renal_valid_super__c from Users order by Name)from UserRole where Id in:set_Role];
			//Execute the batch
			V2_ClsSaveRoleHistoryBatch vc=new V2_ClsSaveRoleHistoryBatch();
			vc.list_ur=list_UserRole;
			vc.str_year=str_year;
			vc.str_month=str_month;
			vc.map_GBU=map_GBU;
			vc.map_Level=map_Level;
			vc.map_PG=map_PG;
			vc.map_Territory=map_Territory;
			Database.executeBatch(vc,1);
		}
	}
	//Get User Method
	public static User GetUserMethod(List<User> list_user)
	{
		User usr=new User();//init user
		if(list_user.size()>0)
		{
			if(list_user.size()==1)usr=list_user[0];
			else
			{
				for(User us:list_user)
				{
					if(us.IsActive && us.IsLeave__c==false)
					{
						usr=us;break;
					}
				}
				if(usr.Id==null)usr=list_user[0];
			}
		}
		return usr;
	}
	//Check the user is Supervisor and Renal valid super or not
	/*public static Boolean CheckUser(String Level,User us)
	{
		boolean result=false;
		if(Level=='Supervisor' && us.Renal_valid_super__c)result=true;
		return result;
	}*/
	//Get UserRole Tree Method
	public static Map<String,Id> map_Id=new Map<String,Id>();
	
	public static Map<String,Id> GetRoleTree(Id id_roleId)
	{
		V2_ClsSaveRoleHistory.GetRoleTreeMethod(id_roleId);
		return map_Id;
	}
	
	public static void GetRoleTreeMethod(Id roleId)
	{
		if(roleId!=null)
		{
			UserRole urname=[select Name from UserRole where Id=:roleId];
			if(urname.Name.trim().contains('-') && urname.Name.trim().length()-urname.Name.trim().replaceAll('-','').length()==4)
			{
				Integer i=0;//init index
				string str_Level,str_Territory;//init Level,Territory
				i=urname.Name.lastIndexOf('-');//GET INDEX
				str_Territory=urname.Name.substring(urname.Name.indexOf('-')+1, i);//GET Level+Territory+PG
				str_Level=str_Territory.substring(0, str_Territory.indexOf('-'));//GET Level
				List<User> list_user=[select Id,IsActive,IsLeave__c from User where UserRoleId=:roleId];
				User u=V2_ClsSaveRoleHistory.GetUserMethod(list_user);
				//boolean checkResult=V2_ClsSaveRoleHistory.CheckUser(str_Level, u);&& checkResult==false
				if(!map_Id.containsKey(str_Level))map_Id.put(str_Level,u.Id);
				List<UserRole> list_UserRole=[select ParentRoleId from UserRole where Id=:roleId];
				if(list_UserRole.size()>0)V2_ClsSaveRoleHistory.GetRoleTreeMethod(list_UserRole[0].ParentRoleId);
			}
		}
	}
}