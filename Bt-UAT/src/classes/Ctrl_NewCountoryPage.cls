/*
 * Author: Steven Ke
 * Date: 2014-2-24
 * Description: controller
 */
public class Ctrl_NewCountoryPage 
{
	public List<NewCountory__c> ncList{get;set;}
	public Boolean isEditing1{get;set;}
	public Boolean isNotEditing1{get;set;}
	public Boolean isEditing2{get;set;}
	public Boolean isNotEditing2{get;set;}
	
	public Ctrl_NewCountoryPage(ApexPages.standardController controller)
	{
		Id cityID = controller.getId();
		ncList = new List<NewCountory__c>();
		ncList = [Select 
						GA_HD_ContainsAdjuvantDrug__c, 
						GA_HD_PatientPayScale__c, 
						GA_HD_DialysisPayment__c,
						GA_HD_StartToPay__c,
						GA_HD_FirstPayScale__c,
						GA_HD_HealthInsuranceType__c,
						GA_HD_HealthInsurancePayScale__c,
						GA_HD_PolicyTendency__c,
						GA_HD_PayObstacle__c,
						GA_HD_MaximumLimit__c,
						GA_PD_ContainsAdjuvantDrug__c, 
						GA_PD_PatientPayScale__c, 
						GA_PD_DialysisPayment__c,
						GA_PD_StartToPay__c,
						GA_PD_FirstPayScale__c,
						GA_PD_HealthInsuranceType__c,
						GA_PD_HealthInsurancePayScale__c,
						GA_PD_PolicyTendency__c,
						GA_PD_PayObstacle__c,
						GA_PD_MaximumLimit__c
					 From 
					 	NewCountory__c
					 Where 
					 	City__c =: cityId];
		if(ncList.size() != 0)
		{
			isEditing1 = false;
			isNotEditing1 = true;
			isEditing2 = false;
			isNotEditing2 = true;
		}
		else
		{
			isEditing1 = false;
			isNotEditing1 = false;
			isEditing2 = false;
			isNotEditing2 = false;
		}
	}
	
	public void Saveit1()
	{
		update ncList;
		isEditing1 = false;
		isNotEditing1 = true;
	}
	
	public void Editit1()
	{
		isEditing1 = true;
		isNotEditing1 = false;
	}
	
	public void CancelEdit1()
	{
		isEditing1 = false;
		isNotEditing1 = true;
	}
	
	public void Saveit2()
	{
		update ncList;
		isEditing2 = false;
		isNotEditing2 = true;
	}
	
	public void Editit2()
	{
		isEditing2 = true;
		isNotEditing2 = false;
	}
	
	public void CancelEdit2()
	{
		isEditing2 = false;
		isNotEditing2 = true;
	}
}