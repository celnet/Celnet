public with sharing class V3_EditQuoteLineItems {
	

	
	public Boolean IsShowInfo{get;set;} //info message
	public Boolean IsShowError{get;set;} //error message
	public String strInfo{get;set;} //info message
	public String strError{get;set;} //error message
	
	
	public Quote quoteobj{get;set;}//quote

	public list<QuoteLineItem> lstSelectProductPrice{get;set;}//list Product

	public String Quoteid{get;set;}// Qid
	
	public ApexPages.StandardSetController con
    {
        get ;
        set;
    }
    
    public void ShowInfoMsg(String msg){
    	IsShowInfo=true;
    	strInfo=msg;
    }
    public void ShowErrorMsg(String msg){
    	IsShowError=true;
    	strError=msg;
    }

	
	/*****************************************
	* 构造
	*****************************************/
	public V3_EditQuoteLineItems(ApexPages.StandardController controller){
		try{
			
			list<QuoteLineItem> lstLineItems = new list<QuoteLineItem>();
			lstSelectProductPrice = new list<QuoteLineItem>();
			String itemId=controller.getId();
			Quoteid = ApexPages.currentPage().getParameters().get('QuoteId');
			system.debug('========Quoteid========'+Quoteid);
			if(itemId == null){
				lstLineItems =[Select Id, PricebookEntryId,QuoteId,ProductName__c,
							Quantity,  V3_UOM__c , Product_Price__r.V3_valueUOM__c,Product_Price__r.UOM__c, 
							 Product_Price__r.Price__c ,
							 Product_Price__r.Product_Description__c,
							 V3_Pack_factor__c,X1UOM__c,X2UOM__c
							From QuoteLineItem where QuoteId=:Quoteid];
			
			} else {
				lstLineItems =[Select Id, PricebookEntryId,QuoteId,ProductName__c,
							Quantity,  V3_UOM__c , Product_Price__r.V3_valueUOM__c,Product_Price__r.UOM__c, 
							 Product_Price__r.Price__c ,
							 Product_Price__r.Product_Description__c,
							 V3_Pack_factor__c,X1UOM__c,X2UOM__c
			     			From QuoteLineItem where Id=:itemId];
			     Quoteid = lstLineItems[0].QuoteId;
				
			}
			
			
					//赋值UOM 
				for(QuoteLineItem item:lstLineItems){
					if(item.Product_Price__c==null){
						ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING ,'The Price info was lost, item no=：'+item.ProductName__c);            
				        ApexPages.addMessage(msg);
					} else {
						item.Product_Price__r.V3_valueUOM__c = item.V3_UOM__c;
						lstSelectProductPrice.add(item);
					}
				}
			}catch(exception e){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING ,e.getmessage()+' Line No：'+e.getLineNumber());            
	        ApexPages.addMessage(msg);
	        return;
		}
	}
	
	

    
    //:保存
	public PageReference Save(){
		
		for(QuoteLineItem EntityObject:lstSelectProductPrice){
						//计算价格
				double objPrice=0;
				if(EntityObject.Product_Price__r.V3_valueUOM__c == EntityObject.Product_Price__r.UOM__c){
					objPrice =  EntityObject.Product_Price__r.Price__c; 
				} else {
					if(EntityObject.Product_Price__r.V3_valueUOM__c== EntityObject.X1UOM__c &&
						EntityObject.Product_Price__r.UOM__c== EntityObject.X2UOM__c
					){
							// 如果 Pack factor 等于 null ,error
						if(EntityObject.V3_Pack_factor__c == null || EntityObject.V3_Pack_factor__c == 0){
							ShowErrorMsg('Pack factor is null ! product :' + EntityObject.ProductName__c);
							return null;
						}
						objPrice =  EntityObject.Product_Price__r.Price__c/EntityObject.V3_Pack_factor__c;
					} else if(EntityObject.Product_Price__r.V3_valueUOM__c== EntityObject.X2UOM__c &&
						EntityObject.Product_Price__r.UOM__c== EntityObject.X1UOM__c
					){
							// 如果 Pack factor 等于 null ,error
						if(EntityObject.V3_Pack_factor__c == null || EntityObject.V3_Pack_factor__c == 0){
							ShowErrorMsg('Pack factor is null ! product :' + EntityObject.ProductName__c);
							return null;
						}
						objPrice =  EntityObject.Product_Price__r.Price__c*EntityObject.V3_Pack_factor__c;
						
					} else {
							ShowErrorMsg('Can not find Pack factor info;! product :' +  EntityObject.ProductName__c);
							return null;
						
					}
				}
				 EntityObject.V3_UOM__c=EntityObject.Product_Price__r.V3_valueUOM__c ;
				 EntityObject.UnitPrice=objPrice;
		
		}
		
		    update lstSelectProductPrice;
		
		
		
			return new PageReference( '/' + Quoteid);
		

			
		//return null;
    }
    
    //:取消
	public PageReference Cancel(){
      
      return new PageReference( '/' + Quoteid);
    }
}