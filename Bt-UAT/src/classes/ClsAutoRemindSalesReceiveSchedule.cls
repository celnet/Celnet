/*
 * Author：Bill
 * Created on：2013-6-27
 * Description: 
 * 工程师发货15天后如果代表没有收货，自动发送邮件给代表提醒其收货
 * 维修退回申请回退旧罐子时，销售发货15天后如果工程师没有收货，自动发邮件给工程师提醒其收货
*/
public class ClsAutoRemindSalesReceiveSchedule implements Schedulable
{
	//global
	public void execute(SchedulableContext SC) 
	{
		ClsAutoRemindSalesReceive remindReceive = new ClsAutoRemindSalesReceive();
		Database.executeBatch(remindReceive, 10);
	}
}