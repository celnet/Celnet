/**
 * 作者:Bill
 * 日期：2013-9-23
 * 说明：PM项目列表导出功能的公共类
**/
global with sharing class PM_ExportFieldsCommon {
	
	//页面的table所需要的类型封装成内部类
    global Class Patient
    {
    	public PM_Patient__c patient{set;get;}
    	public Transient string color{set;get;} 
    	public Transient string phone1{set;get;}
    	public Transient string phone2{set;get;}
    	public Transient string priorType1{set;get;}
    	public Transient string priorType2{set;get;}
    	public Transient double clockNum{set;get;}
    	public boolean IsExport{set;get;}
    	//默认上次拜访时间，为空的话是创建时间
    	public Transient date SortDate{set;get;}
    	//插管日期
    	public Transient date InDate{set;get;}
    }
    public class phoneInfo
    {
    	public string phoneNumber;
    	public string phoneType;
    }
    //判断电话优先1和电话优先2是什么类型
    public map<string,phoneInfo> judgePhoneType(PM_Patient__c patient)
    {
    	map<string,phoneInfo> map_PI = new map<string,phoneInfo>();
    	list<phoneInfo> listPrior = new list<phoneInfo>();
    	//电话优先1
    	//家庭电话
    	if(patient.PM_TelPriorityFirst__c == PM_Patient__c.PM_HomeTel__c.getDescribe().getLabel())
    	{
    		phoneInfo pi = new phoneInfo();
    		pi.phoneNumber = patient.PM_HomeTel__c;
    		pi.phoneType = patient.PM_BVTime1__c;
    		map_PI.put('prior1',pi);
    	}
    	//病人手机
    	else if(patient.PM_TelPriorityFirst__c == PM_Patient__c.PM_PmPhone__c.getDescribe().getLabel())
    	{
    		phoneInfo pi = new phoneInfo();
    		pi.phoneNumber = patient.PM_PmPhone__c;
    		pi.phoneType = patient.PM_BVTime2__c;
    		map_PI.put('prior1',pi);
    	}
    	//病人电话
    	else if(patient.PM_TelPriorityFirst__c == PM_Patient__c.PM_PmTel__c.getDescribe().getLabel())
    	{
    		phoneInfo pi = new phoneInfo();
    		pi.phoneNumber = patient.PM_PmTel__c;
    		pi.phoneType = patient.PM_BVTime3__c;
    		map_PI.put('prior1',pi);
    	}
    	//家属手机
    	else if(patient.PM_TelPriorityFirst__c == PM_Patient__c.PM_FamilyPhone__c.getDescribe().getLabel())
    	{
    		phoneInfo pi = new phoneInfo();
    		pi.phoneNumber = patient.PM_FamilyPhone__c;
    		pi.phoneType = patient.PM_BVTime4__c;
    		map_PI.put('prior1',pi);
    	}
    	//家属电话
    	else if(patient.PM_TelPriorityFirst__c == PM_Patient__c.PM_FamilyTel__c.getDescribe().getLabel())
    	{
    		phoneInfo pi = new phoneInfo();
    		pi.phoneNumber = patient.PM_FamilyTel__c;
    		pi.phoneType = patient.PM_BVTime5__c;
    		map_PI.put('prior1',pi);
    	}
    	//其他电话
    	else if(patient.PM_TelPriorityFirst__c == PM_Patient__c.PM_OtherTel2__c.getDescribe().getLabel())
    	{
    		phoneInfo pi = new phoneInfo();
    		pi.phoneNumber = patient.PM_OtherTel2__c;
    		pi.phoneType = patient.PM_BVTime6__c;
    		map_PI.put('prior1',pi);
    	}
    	//电话优先2
    	//家庭电话
    	if(patient.PM_TelPrioritySecond__c == PM_Patient__c.PM_HomeTel__c.getDescribe().getLabel())
    	{
    		phoneInfo pi = new phoneInfo();
    		pi.phoneNumber = patient.PM_HomeTel__c;
    		pi.phoneType = patient.PM_BVTime1__c;
    		map_PI.put('prior2',pi);
    	}
    	//病人手机
    	else if(patient.PM_TelPrioritySecond__c == PM_Patient__c.PM_PmPhone__c.getDescribe().getLabel())
    	{
    		phoneInfo pi = new phoneInfo();
    		pi.phoneNumber = patient.PM_PmPhone__c;
    		pi.phoneType = patient.PM_BVTime2__c;
    		map_PI.put('prior2',pi);
    	}
    	//病人电话
    	else if(patient.PM_TelPrioritySecond__c == PM_Patient__c.PM_PmTel__c.getDescribe().getLabel())
    	{
    		phoneInfo pi = new phoneInfo();
    		pi.phoneNumber = patient.PM_PmTel__c;
    		pi.phoneType = patient.PM_BVTime3__c;
    		map_PI.put('prior2',pi);
    	}
    	//家属手机
    	else if(patient.PM_TelPrioritySecond__c == PM_Patient__c.PM_FamilyPhone__c.getDescribe().getLabel())
    	{
    		phoneInfo pi = new phoneInfo();
    		pi.phoneNumber = patient.PM_FamilyPhone__c;
    		pi.phoneType = patient.PM_BVTime4__c;
    		map_PI.put('prior2',pi);
    	}
    	//家属电话
    	else if(patient.PM_TelPrioritySecond__c == PM_Patient__c.PM_FamilyTel__c.getDescribe().getLabel())
    	{
    		phoneInfo pi = new phoneInfo();
    		pi.phoneNumber = patient.PM_FamilyTel__c;
    		pi.phoneType = patient.PM_BVTime5__c;
    		map_PI.put('prior2',pi);
    	}
    	//其他电话
    	else if(patient.PM_TelPrioritySecond__c == PM_Patient__c.PM_OtherTel2__c.getDescribe().getLabel())
    	{
    		phoneInfo pi = new phoneInfo();
    		pi.phoneNumber = patient.PM_OtherTel2__c;
    		pi.phoneType = patient.PM_BVTime6__c;
    		map_PI.put('prior2',pi);
    	}
    	return map_PI;
    }
	
	
	//判断病人该显示几个闹钟
	public integer judgeClockNum(integer dateNum, integer target1, integer target2)
	{
		if(dateNum > target2)
		{
			//大于第二个周期显示两个闹钟
			return 2;
		}
		else if(dateNum <=target2 && dateNum >= target1)
		{
			//在周期之间显示一个闹钟
			return 1;
		}
		return 0;
	}
	
	
    //获取导出字段集封装类
    global class ExportField
    {
    	//字段标签
    	public String fieldLabel{get;set;}
    	//选中
    	public boolean IsChecked{get;set;}
    	//api name
    	public Schema.FieldSetMember apiName{get;set;}
    }
    
    //判断导出的字段label
    public String getFieldLabelName(Schema.FieldSetMember f)
    {
    	if(f.fieldPath.toUpperCase().contains('CREATEDBY')) return '创建人';
    	if(f.fieldPath.toUpperCase().contains('OWNER')) return '所有人';
    	if(f.fieldPath.toUpperCase().contains('LASTMODIFIEDBY')) return '上次修改人';
    	if(f.fieldPath.toUpperCase().contains('PM_INNURSER')) return '插管护士';
    	if(f.fieldPath.toUpperCase().contains('PM_INUSER')) return '插管销售';
    	if(f.fieldPath.toUpperCase().contains('PM_INDOCTOR')) return '插管医生';
    	if(f.fieldPath.toUpperCase().contains('PM_INHOSPITAL__')) return '插管医院';
    	if(f.fieldPath.toUpperCase().contains('PM_CURRENT_PSR')) return '当前负责PSR';
    	if(f.fieldPath.toUpperCase().contains('PM_CURRENT_SALER')) return '当前负责销售';
    	if(f.fieldPath.toUpperCase().contains('PM_PROVINCE')) return '居住省份';
    	if(f.fieldPath.toUpperCase().contains('PM_CITY')) return '居住城市';
    	if(f.fieldPath.toUpperCase().contains('PM_COUNTY')) return '居住县';
    	if(f.fieldPath.toUpperCase().contains('PM_DISTRIBUTOR')) return '经销商';
    	if(f.fieldPath.toUpperCase().contains('PM_INNURSE')) return '联系人';
    	if(f.fieldPath.toUpperCase().contains('PM_TREAT_NURSE')) return '治疗护士';
    	if(f.fieldPath.toUpperCase().contains('PM_TREAT_SALER')) return '治疗销售姓名';
    	if(f.fieldPath.toUpperCase().contains('PM_TREAT_DOCTOR')) return '治疗医生';
    	if(f.fieldPath.toUpperCase().contains('PM_TREAT_HOSPITAL')) return '治疗医院';
    	if(f.fieldPath.toUpperCase().contains('PM_TERMINAL')) return '终端配送商';
    	return f.Label;
    }
    
    //导出插管信息的字段集
    public List<ExportField> getInInfoFields() 
    {
    	List<ExportField> list_ExportField = new List<ExportField>();
    	List<Schema.FieldSetMember> list_fields = SObjectType.PM_Patient__c.FieldSets.PM_InInfo.getFields();
        for(Schema.FieldSetMember f : list_fields) 
        {
        	ExportField ex = new ExportField();
        	ex.fieldLabel = getFieldLabelName(f);
        	ex.apiName = f;
        	ex.IsChecked = false;
        	list_ExportField.add(ex);
        }
        return list_ExportField;
    }
    //掉队信息
    public List<ExportField> getDropInfoFields() 
    {
    	List<ExportField> list_ExportField = new List<ExportField>();
    	List<Schema.FieldSetMember> list_fields = SObjectType.PM_Patient__c.FieldSets.PM_DropInfo.getFields();
        for(Schema.FieldSetMember f : list_fields) 
        {
        	ExportField ex = new ExportField();
        	ex.fieldLabel = getFieldLabelName(f);
        	ex.apiName = f;
        	ex.IsChecked = false;
        	list_ExportField.add(ex);
        }
        return list_ExportField;
    }
    //高风险掉队信息
    public List<ExportField> getHighRiskFields() 
    {
    	List<ExportField> list_ExportField = new List<ExportField>();
    	List<Schema.FieldSetMember> list_fields = SObjectType.PM_Patient__c.FieldSets.PM_HighRisk.getFields();
        for(Schema.FieldSetMember f : list_fields) 
        {
        	ExportField ex = new ExportField();
        	ex.fieldLabel = getFieldLabelName(f);
        	ex.apiName = f;
        	ex.IsChecked = false;
        	list_ExportField.add(ex);
        }
        return list_ExportField;
    }
    //高影响信息
    public List<ExportField> getHighEffectFields() 
    {
    	List<ExportField> list_ExportField = new List<ExportField>();
    	List<Schema.FieldSetMember> list_fields = SObjectType.PM_Patient__c.FieldSets.PM_HighEffect.getFields();
        for(Schema.FieldSetMember f : list_fields) 
        {
        	ExportField ex = new ExportField();
        	ex.fieldLabel = getFieldLabelName(f);
        	ex.apiName = f;
        	ex.IsChecked = false;
        	list_ExportField.add(ex);
        }
        return list_ExportField;
    }
    //关爱信息
    public List<ExportField> getCareInfoFields() 
    {
    	List<ExportField> list_ExportField = new List<ExportField>();
    	List<Schema.FieldSetMember> list_fields = SObjectType.PM_Patient__c.FieldSets.PM_CareInfo.getFields();
        for(Schema.FieldSetMember f : list_fields) 
        {
        	ExportField ex = new ExportField();
        	ex.fieldLabel = getFieldLabelName(f);
        	ex.apiName = f;
        	ex.IsChecked = false;
        	list_ExportField.add(ex);
        }
        return list_ExportField;
    }
    //基本信息
    public List<ExportField> getBaseInfoFields() 
    {
    	List<ExportField> list_ExportField = new List<ExportField>();
    	List<Schema.FieldSetMember> list_fields = SObjectType.PM_Patient__c.FieldSets.PM_BaseInfo.getFields();
        for(Schema.FieldSetMember f : list_fields) 
        {
        	ExportField ex = new ExportField();
        	ex.fieldLabel = getFieldLabelName(f);
        	system.debug(f.fieldPath + '@@' +ex.fieldLabel + 'zgxm');
        	ex.apiName = f;
        	ex.IsChecked = false;
        	list_ExportField.add(ex);
        }
        return list_ExportField;
    }
    //经销商信息
    public List<ExportField> getDealerInfoFields() 
    {
    	List<ExportField> list_ExportField = new List<ExportField>();
    	List<Schema.FieldSetMember> list_fields = SObjectType.PM_Patient__c.FieldSets.PM_DealerInfo.getFields();
        for(Schema.FieldSetMember f : list_fields) 
        {
        	ExportField ex = new ExportField();
        	ex.fieldLabel = getFieldLabelName(f);
        	ex.apiName = f;
        	ex.IsChecked = false;
        	list_ExportField.add(ex);
        }
        return list_ExportField;
    }
    //联系拜访
    public List<ExportField> getContactFields() 
    {
    	List<ExportField> list_ExportField = new List<ExportField>();
    	List<Schema.FieldSetMember> list_fields = SObjectType.PM_Patient__c.FieldSets.PM_Contact.getFields();
        for(Schema.FieldSetMember f : list_fields) 
        {
        	ExportField ex = new ExportField();
        	ex.fieldLabel = getFieldLabelName(f);
        	ex.apiName = f;
        	ex.IsChecked = false;
        	list_ExportField.add(ex);
        }
        return list_ExportField;
    }
    //临床信息
    public List<ExportField> getClinicalInfoFields() 
    {
    	List<ExportField> list_ExportField = new List<ExportField>();
    	List<Schema.FieldSetMember> list_fields = SObjectType.PM_Patient__c.FieldSets.PM_ClinicalInfo.getFields();
        for(Schema.FieldSetMember f : list_fields) 
        {
        	ExportField ex = new ExportField();
        	ex.fieldLabel = getFieldLabelName(f);
        	ex.apiName = f;
        	ex.IsChecked = false;
        	list_ExportField.add(ex);
        }
        return list_ExportField;
    }
    //治疗信息
    public List<ExportField> getTreatInfoFields() 
    {
    	List<ExportField> list_ExportField = new List<ExportField>();
    	List<Schema.FieldSetMember> list_fields = SObjectType.PM_Patient__c.FieldSets.PM_TreatInfo.getFields();
        for(Schema.FieldSetMember f : list_fields) 
        {
        	ExportField ex = new ExportField();
        	ex.fieldLabel = getFieldLabelName(f);
        	ex.apiName = f;
        	ex.IsChecked = false;
        	list_ExportField.add(ex);
        }
        return list_ExportField;
    }
    //New病人拜访列表
    public List<ExportField> getNewPatientListFields()
    {
    	List<ExportField> list_ExportField = new List<ExportField>();
    	List<Schema.FieldSetMember> list_fields = SObjectType.PM_Patient__c.FieldSets.PM_NewPatientList.getFields();
        for(Schema.FieldSetMember f : list_fields) 
        {
        	ExportField ex = new ExportField();
        	ex.fieldLabel = getFieldLabelName(f);
        	ex.apiName = f;
        	ex.IsChecked = false;
        	list_ExportField.add(ex);
        }
        return list_ExportField;
    }
    //Unreachable病人拜访
    public List<ExportField> getUnreachablePatientListFields()
    {
    	List<ExportField> list_ExportField = new List<ExportField>();
    	List<Schema.FieldSetMember> list_fields = SObjectType.PM_Patient__c.FieldSets.PM_UnreachablelList.getFields();
        for(Schema.FieldSetMember f : list_fields) 
        {
        	ExportField ex = new ExportField();
        	ex.fieldLabel = getFieldLabelName(f);
        	ex.apiName = f;
        	ex.IsChecked = false;
        	list_ExportField.add(ex);
        }
        return list_ExportField;
    }
    //Active病人拜访列表PM_ActiveList  APD病人的列表信息是一致的
    public List<ExportField> getActivePatientListFields()
    {
    	List<ExportField> list_ExportField = new List<ExportField>();
    	List<Schema.FieldSetMember> list_fields = SObjectType.PM_Patient__c.FieldSets.PM_ActiveList.getFields();
        for(Schema.FieldSetMember f : list_fields) 
        {
        	ExportField ex = new ExportField();
        	ex.fieldLabel = getFieldLabelName(f);
        	ex.apiName = f;
        	ex.IsChecked = false;
        	list_ExportField.add(ex);
        }
        return list_ExportField;
    }
    //Dropout病人拜访列表
    public List<ExportField> getDropoutPatientListFields()
    {
    	List<ExportField> list_ExportField = new List<ExportField>();
    	List<Schema.FieldSetMember> list_fields = SObjectType.PM_Patient__c.FieldSets.PM_DropoutList.getFields();
        for(Schema.FieldSetMember f : list_fields) 
        {
        	ExportField ex = new ExportField();
        	ex.fieldLabel = getFieldLabelName(f);
        	ex.apiName = f;
        	ex.IsChecked = false;
        	list_ExportField.add(ex);
        }
        return list_ExportField;
    }
    
    //获取需要导出的字段sql语句  也即是API name的集合
    public String getApiNameFields(List<ExportField> list_ExportField)
    {
    	string strSql = 'Select ';
    	for(ExportField field : list_ExportField)
    	{
    		if(strSql == 'Select ')
    		{
    			strSql += field.apiName.fieldPath;
    		}
    		else
    		{
    			strSql += (','+field.apiName.fieldPath);
    		}
    		//strSql += field.apiName.fieldPath + ', ';
    	}
    	strSql +=' from PM_Patient__c ';
        return strSql;
    }
    
    //把查询语句保存到数据库
    public void SaveSoql(string strSoqlField, List<List<PM_Patient__c>> lists)
    {
    	string strSoql = '';
    	string strSoql1 = '';
    	string strSoql2 = '';
    	string strSoql3 = '';
    	string strSoql4 = '';
    	string strSoql5 = '';
    	for(List<PM_Patient__c> listNode : lists)
    	{
    		for(PM_Patient__c pa : listNode)
    		{
    			if(strSoql.length()<32700)
    			{
    				strSoql += pa.Id + ',';
    			}
    			else if(strSoql1.length()<32700)
    			{
    				strSoql1 += pa.Id + ',';
    			}
    			else if(strSoql2.length()<32700)
    			{
    				strSoql2 += pa.Id + ',';
    			}
    			else if(strSoql3.length()<32700)
    			{
    				strSoql3 += pa.Id + ',';
    			}
    			else if(strSoql4.length()<32700)
    			{
    				strSoql4 += pa.Id + ',';
    			}
    			else if(strSoql5.length()<32700)
    			{
    				strSoql5 += pa.Id + ',';
    			}
    		}
    	}
    	//strSoql = strSoql.substring(strSoql.length()-1, strSoql.length()) + ')';
		Transient PM_Soql__c strSql = new PM_Soql__c();
		strSql.PM_Field__c = strSoqlField + 'where Id In : ';
		strSql.Name = UserInfo.getUserId();
		strSql.PM_SOQL_0__c = strSoql;
		strSql.PM_SOQL_1__c = strSoql1; 
		strSql.PM_SOQL_2__c = strSoql2;
		strSql.PM_SOQL_3__c = strSoql3;
		strSql.PM_SOQL_4__c = strSoql4;
		strSql.PM_SOQL_5__c = strSoql5;
		upsert strSql name;
    }
}