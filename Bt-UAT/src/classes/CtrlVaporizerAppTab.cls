/*
 * Author : Sunny
 * 
 */
public class CtrlVaporizerAppTab {
	public String strVapInfoUrl{get;set;}
	public String strVapAppUrl{get;set;}
	public String strVapRetUrl{get;set;}
    public CtrlVaporizerAppTab(){
    	this.initUrl();
    }
    private void initUrl(){
    	Schema.DescribeSObjectResult VapInfo = VaporizerInfo__c.SObjectType.getDescribe();
    	strVapInfoUrl = '/'+VapInfo.getKeyPrefix();
    	
    	Schema.DescribeSObjectResult VapApp = Vaporizer_Application__c.SObjectType.getDescribe();
        strVapAppUrl = '/'+VapApp.getKeyPrefix();
        
        Schema.DescribeSObjectResult VapRet = Vaporizer_ReturnAndMainten__c.SObjectType.getDescribe();
        strVapRetUrl = '/'+VapRet.getKeyPrefix();
    }
    public List<Report> getVapReports()
    {
    	List<Report> reports = [Select r.NamespacePrefix, r.Name, r.LastRunDate, 
    							r.Id, r.DeveloperName, r.Description From Report r 
    							where r.DeveloperName like 'VapApp_%'];
    	return reports;
    }
}