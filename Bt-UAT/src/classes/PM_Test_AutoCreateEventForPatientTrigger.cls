/**
 * Tobe
 * 2013.10.11
 * AutoCreateEventForPatient Trigger测试类 
 */
@isTest
private class PM_Test_AutoCreateEventForPatientTrigger 
{
    static testMethod void myUnitTest() 
    {
    	//account
    	Account acc = new Account();
    	acc.Name = 'test20131115';
    	insert acc;
    	
    	ID DieId = [Select r.Id  From RecordType r where r.SobjectType = 'PM_Patient__c' and r.DeveloperName = 'PM_Die' and r.IsActive = true][0].Id;
        PM_Patient__c patient = new PM_Patient__c();
        patient.Name = '张美丽';
        patient.PM_Status__c = 'Active';
        insert patient;
        PM_Patient__c patient2 = new PM_Patient__c();
        patient2.Name = '张美丽';
        patient2.PM_Status__c = 'Active';
        patient2.PM_DropOut_One_Reason__c = '死亡';
        insert patient2;
        patient2.Name = '张美';
        patient2.PM_Treat_Hospital__c = acc.Id;
        try
        {
        update patient2;
        }
        catch(DMLexception e){}
        patient.PM_LastSuccessDateTime__c = DateTime.now().addDays(-1);
        patient.PM_LastFailDateTime__c = DateTime.now().addDays(-1);
        patient.PM_EventStartDate__c = DateTime.now().addDays(-1);
        patient.PM_EventEndDate__c = DateTime.now().addDays(-1);
        update patient;
        patient.PM_LastSuccessDateTime__c = DateTime.now();
        patient.PM_LastFailDateTime__c = DateTime.now();
        patient.PM_EventStartDate__c = DateTime.now();
        patient.PM_EventEndDate__c = DateTime.now();
        update patient;
    }
}