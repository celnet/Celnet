/*
 Scott
 沿用大陆功能,按照台湾需求调整
*/
/*
Author: Eleen
Created on: 2012-1-10
Description: 
	This Class will be run at the start of every month for save User's Role to RoleHistory. 
*/
global class TW_SaveRoleHistoryScheduler implements Schedulable{
	global static final String CRON_EXP = '0 0 1 * * ?';
	global void execute(SchedulableContext sc) 
	{
		TW_SaveRoleHistoryBatch  SaveRoleHistory  = new TW_SaveRoleHistoryBatch();
		SaveRoleHistory.str_year = String.valueOf(date.today().year());
		SaveRoleHistory.str_month = String.valueOf(date.today().month());
		Database.executeBatch(SaveRoleHistory,100); 
	}
	static testMethod void test()
	{
        system.Test.startTest();
        string sch=DateTime.now().second()+' '+DateTime.now().minute()+' '+DateTime.now().hour()+' * * ?';
        TW_SaveRoleHistoryScheduler oppsched=new TW_SaveRoleHistoryScheduler();
        System.schedule('autoOutTest1',sch , oppsched); 
        TW_SaveRoleHistoryBatch  SaveRoleHistory  = new TW_SaveRoleHistoryBatch();
		SaveRoleHistory.str_year = String.valueOf(date.today().year());
		SaveRoleHistory.str_month = String.valueOf(date.today().month());
		
        ID batchprocessid = Database.executeBatch(SaveRoleHistory);
        system.Test.stopTest();
	}
}