/*
 Scott
 沿用大陆功能,按照台湾需求调整
*/
/*
Author: Eleen
Created on: 2012-2-7
Description: 
	This batch include the method to save role history.
	It is run by V2_ClsSaveRoleHistory. 
*/
global class TW_SaveRoleHistoryBatch implements Database.Batchable<User>
{
	global String str_year; 
	global String str_month; 
	//角色和其对应的用户 只取一个
	Map<String,User> Map_RoleUsers= new Map<String,User>();
	//台湾用户
	List<User> List_TWUsers = new List<User>();
	global TW_SaveRoleHistoryBatch()
	{
		//台湾地区角色id
		Set<Id> Set_Roles = new Set<Id>();
		for(UserRole ur : [select ID,Name from UserRole where Name like 'TW%'])
		{
			Set_Roles.add(ur.Id);
		}
		
		for(User u:[Select Id,Name,IsLeave__c,IsOnHoliday__c,Department,ManagerId,
					Renal_valid_super__c,EmployeeNumber,isActive,UserRoleId,UserRole.Name From User where UserRoleId in:Set_Roles])
		{
			List_TWUsers.add(u);
			//先启用的装一遍 保证取时启用的先被取到
			if(u.IsActive)
			{
				if(!Map_RoleUsers.containsKey(u.UserRole.Name))
				{
	
					Map_RoleUsers.put(u.UserRole.Name,u);
				}
			}
			//在禁用的装一遍
			else
			{
				if(!Map_RoleUsers.containsKey(u.UserRole.Name))
				{
					Map_RoleUsers.put(u.UserRole.Name,u);
				}
			}
			
		}
		System.debug('##############################################Map_RoleUsers   '+Map_RoleUsers.size());
	}
	
	global List<User> start(Database.BatchableContext BC)
	{
		System.debug('########################################################jinru');
		return List_TWUsers;
	}
	
	global void execute(Database.BatchableContext BC,list<User> scope)
	{
		//角色历史
		List<V2_RoleHistory__c> list_vr = new List<V2_RoleHistory__c>();
		for(User user:scope)
		{
			V2_RoleHistory__c vr=new V2_RoleHistory__c();
			vr.Name=str_year+'-'+str_month+'-'+user.Name;
			vr.Key__c=str_year+'-'+str_month+'-'+user.Id;
			vr.Year__c=str_year;
			vr.Month__c=str_month;
			vr.Name__c=user.Id;
			vr.IsLeave__c=user.IsLeave__c;
			vr.IsOnHoliday__c=user.IsOnHoliday__c;
			vr.Role__c=user.UserRole.Name;
			vr.Department__c=user.Department;
			vr.Manager__c=user.ManagerId;
			//vr.Renal_valid_super__c=user.Renal_valid_super__c;
			vr.EmployeeNumber__c=user.EmployeeNumber;
			if(user.isActive)
				vr.Status__c='启用';
			else
				vr.Status__c='停用';
			
			if(user.UserRole.Name =='TW South Sales Rep')
			{
				vr.Level__c = 'Rep';
				//TW South Sales Rep
				vr.SalesName1__c = user.Id;//SalesName1(Rep)
				//TW Regional South Sales Manager
				if(Map_RoleUsers.containsKey('TW Regional South Sales Manager'))
				vr.SalesName2__c = Map_RoleUsers.get('TW Regional South Sales Manager').Id;//SalesName2(Super)
				//TW National Sales Manager 
				if(Map_RoleUsers.containsKey('TW National Sales Manager'))
				vr.SalesName6__c = Map_RoleUsers.get('TW National Sales Manager').Id;//SalesName6(National)
				//TW BU Head
				if(Map_RoleUsers.containsKey('TW BU Head'))
				vr.SalesName7__c = Map_RoleUsers.get('TW BU Head').Id;//SalesName7(Head)
				
			}
			else if(user.UserRole.Name =='TW Regional South Sales Manager')
			{
				vr.Level__c = 'Super';
				//TW Regional South Sales Manager 
				vr.SalesName2__c = user.Id;
				//TW National Sales Manager
				if(Map_RoleUsers.containsKey('TW National Sales Manager'))
				vr.SalesName6__c = Map_RoleUsers.get('TW National Sales Manager').Id;//SalesName6(National)
				//TW BU Head
				if(Map_RoleUsers.containsKey('TW BU Head'))
				vr.SalesName7__c = Map_RoleUsers.get('TW BU Head').Id;//SalesName7(Head)
			}
			else if(user.UserRole.Name =='TW North Sales Rep')
			{
				vr.Level__c = 'Rep';
				//TW North Sales Rep
				vr.SalesName1__c = user.Id;//SalesName1(Rep)
				//TW Regional North Sales Manager 
				if(Map_RoleUsers.containsKey('TW Regional North Sales Manager'))
				vr.SalesName2__c = Map_RoleUsers.get('TW Regional North Sales Manager').Id;//SalesName2(Super)
				//TW National Sales Manager
				if(Map_RoleUsers.containsKey('TW National Sales Manager'))
				vr.SalesName6__c = Map_RoleUsers.get('TW National Sales Manager').Id;//SalesName6(National)
				//TW BU Head
				if(Map_RoleUsers.containsKey('TW BU Head'))
				vr.SalesName7__c = Map_RoleUsers.get('TW BU Head').Id;//SalesName7(Head)
			}
			else if(user.UserRole.Name =='TW Regional North Sales Manager')
			{
				vr.Level__c = 'Super';
				//TW Regional North Sales Manager
				vr.SalesName2__c = user.Id;
				//TW National Sales Manager
				if(Map_RoleUsers.containsKey('TW National Sales Manager'))
				vr.SalesName6__c = Map_RoleUsers.get('TW National Sales Manager').Id;//SalesName6(National)
				//TW BU Head
				if(Map_RoleUsers.containsKey('TW BU Head'))
				vr.SalesName7__c = Map_RoleUsers.get('TW BU Head').Id;//SalesName7(Head)
			}
			else if(user.UserRole.Name =='TW Center Sales Rep')
			{
				vr.Level__c = 'Rep';
				//TW Center Sales Rep
				vr.SalesName1__c = user.Id;//SalesName1(Rep)
				//TW Regional Center Sales Manager 
				if(Map_RoleUsers.containsKey('TW Regional Center Sales Manager'))
				vr.SalesName2__c = Map_RoleUsers.get('TW Regional Center Sales Manager').Id;//SalesName2(Super)
				//TW National Sales Manager
				if(Map_RoleUsers.containsKey('TW National Sales Manager'))
				vr.SalesName6__c = Map_RoleUsers.get('TW National Sales Manager').Id;//SalesName6(National)
				//TW BU Head
				if(Map_RoleUsers.containsKey('TW BU Head'))
				vr.SalesName7__c = Map_RoleUsers.get('TW BU Head').Id;//SalesName7(Head)
			}
			else if(user.UserRole.Name =='TW Regional Center Sales Manager')
			{
				vr.Level__c = 'Super';
				//TW Regional Center Sales Manager 
				vr.SalesName2__c = user.Id;
				//TW National Sales Manager
				if(Map_RoleUsers.containsKey('TW National Sales Manager'))
				vr.SalesName6__c = Map_RoleUsers.get('TW National Sales Manager').Id;//SalesName6(National)
				//TW BU Head
				if(Map_RoleUsers.containsKey('TW BU Head'))
				vr.SalesName7__c = Map_RoleUsers.get('TW BU Head').Id;//SalesName7(Head)
			}
			else if(user.UserRole.Name =='TW National Sales Manager')
			{
				vr.Level__c = 'National';
				//TW National Sales Manager
				vr.SalesName6__c = User.Id;//SalesName6(National)
				//TW BU Head
				if(Map_RoleUsers.containsKey('TW BU Head'))
				vr.SalesName7__c = Map_RoleUsers.get('TW BU Head').Id;//SalesName7(Head)
			}
			else if(user.UserRole.Name =='TW Marketing Product Manager')
			{
				vr.Level__c = 'Super';
				//TW Marketing Product Manager
				vr.SalesName2__c = user.Id;
				//TW National Marketing Manager
				if(Map_RoleUsers.containsKey('TW National Marketing Manager'))
				vr.SalesName6__c = Map_RoleUsers.get('TW National Marketing Manager').Id;//SalesName6(National)
				//TW BU Head
				if(Map_RoleUsers.containsKey('TW BU Head'))
				vr.SalesName7__c = Map_RoleUsers.get('TW BU Head').Id;//SalesName7(Head)
			}
			else if(user.UserRole.Name =='TW National Marketing Manager')
			{
				vr.Level__c = 'National';
				//TW National Marketing Manager 
				vr.SalesName6__c = user.Id;
				//TW BU Head
				if(Map_RoleUsers.containsKey('TW BU Head'))
				vr.SalesName7__c = Map_RoleUsers.get('TW BU Head').Id;//SalesName7(Head)
			}
			else if(user.UserRole.Name =='TW BU Head')
			{
				vr.Level__c = 'Head';
				//TW BU Head
				vr.SalesName7__c =User.Id;//SalesName7(Head)
			}
				
			list_vr.add(vr);
		}
		if(list_vr.size()>0)upsert list_vr Key__c;
		
	}
	global void finish(Database.BatchableContext BC){
		
	}
}