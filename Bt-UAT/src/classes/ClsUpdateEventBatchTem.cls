global class ClsUpdateEventBatchTem implements Database.Batchable<sObject>{
	global Database.QueryLocator start(Database.BatchableContext BC){
		//查找所有点评，构建以拜访ID为Key，以点评List为value的map
		Map<ID,List<AssVisitComments__c>> mapAssVisit = new Map<ID,List<AssVisitComments__c>>() ;
		for(AssVisitComments__c ass : [Select Id,EventId__c,BeReviewed__c,BeReviewed__r.Alias,ReUser__c,ReUser__r.Alias,Grade__c,Comment__c,IsAssVisit__c From AssVisitComments__c]){
		    if(mapAssVisit.containsKey(ass.EventId__c)){
		        List<AssVisitComments__c> list_AssVisit = mapAssVisit.get(ass.EventId__c);
		        list_AssVisit.add(ass);
		        mapAssVisit.put(ass.EventId__c , list_AssVisit);
		    }else{
		        List<AssVisitComments__c> list_AssVisit = new List<AssVisitComments__c>();
		        list_AssVisit.add(ass);
		        mapAssVisit.put(ass.EventId__c , list_AssVisit);
		    }
		}
		 return Database.getQueryLocator([Select Id,StartDateTime,BeReviewed1__c,BeReviewed2__c,BeReviewed3__c,ReUser1__c,ReUser2__c,ReUser3__c,Grade1__c,Grade2__c,Grade3__c,Comment1__c,Comment2__c,Comment3__c,IsAssVisit1__c,IsAssVisit2__c,IsAssVisit3__c From Event Where Id in: mapAssVisit.keySet() order by createddate]);
	}
	global void execute(Database.BatchableContext BC, List<sObject> scope){
		Map<ID,List<AssVisitComments__c>> mapAssVisit = new Map<ID,List<AssVisitComments__c>>() ;
        for(AssVisitComments__c ass : [Select Id,EventId__c,BeReviewed__c,BeReviewed__r.Alias,ReUser__c,ReUser__r.Alias,Grade__c,Comment__c,IsAssVisit__c From AssVisitComments__c]){
            if(mapAssVisit.containsKey(ass.EventId__c)){
                List<AssVisitComments__c> list_AssVisit = mapAssVisit.get(ass.EventId__c);
                list_AssVisit.add(ass);
                mapAssVisit.put(ass.EventId__c , list_AssVisit);
            }else{
                List<AssVisitComments__c> list_AssVisit = new List<AssVisitComments__c>();
                list_AssVisit.add(ass);
                mapAssVisit.put(ass.EventId__c , list_AssVisit);
            }
        }
		List<Event> list_EventUp = new List<Event>();
		for(sObject sObj : scope){
			Event objEvent = (Event)sObj;
			for(AssVisitComments__c assVisit : mapAssVisit.get(objEvent.Id)){
	            if(objEvent.BeReviewed1__c == assVisit.BeReviewed__r.Alias && objEvent.ReUser1__c==assVisit.ReUser__r.Alias){
	                objEvent.put('BeReviewed'+1+'__c' , assVisit.BeReviewed__r.Alias);
	                objEvent.put('ReUser'+1+'__c' , assVisit.ReUser__r.Alias);
	                objEvent.put('Grade'+1+'__c' , assVisit.Grade__c);
	                objEvent.put('Comment'+1+'__c' , assVisit.Comment__c);
	                objEvent.put('IsAssVisit'+1+'__c' , assVisit.IsAssVisit__c);
	            }else if(objEvent.BeReviewed2__c == assVisit.BeReviewed__r.Alias && objEvent.ReUser2__c==assVisit.ReUser__r.Alias){
	                objEvent.put('BeReviewed'+2+'__c' , assVisit.BeReviewed__r.Alias);
	                objEvent.put('ReUser'+2+'__c' , assVisit.ReUser__r.Alias);
	                objEvent.put('Grade'+2+'__c' , assVisit.Grade__c);
	                objEvent.put('Comment'+2+'__c' , assVisit.Comment__c);
	                objEvent.put('IsAssVisit'+2+'__c' , assVisit.IsAssVisit__c);
	            }else if(objEvent.BeReviewed3__c == assVisit.BeReviewed__r.Alias && objEvent.ReUser3__c==assVisit.ReUser__r.Alias){
	                objEvent.put('BeReviewed'+3+'__c' , assVisit.BeReviewed__r.Alias);
	                objEvent.put('ReUser'+3+'__c' , assVisit.ReUser__r.Alias);
	                objEvent.put('Grade'+3+'__c' , assVisit.Grade__c);
	                objEvent.put('Comment'+3+'__c' , assVisit.Comment__c);
	                objEvent.put('IsAssVisit'+3+'__c' , assVisit.IsAssVisit__c);
	            }else if(objEvent.BeReviewed1__c == null && objEvent.ReUser1__c == null){
	            	objEvent.put('BeReviewed'+1+'__c' , assVisit.BeReviewed__r.Alias);
                    objEvent.put('ReUser'+1+'__c' , assVisit.ReUser__r.Alias);
                    objEvent.put('Grade'+1+'__c' , assVisit.Grade__c);
                    objEvent.put('Comment'+1+'__c' , assVisit.Comment__c);
                    objEvent.put('IsAssVisit'+1+'__c' , assVisit.IsAssVisit__c);
	            }else if(objEvent.BeReviewed2__c == null && objEvent.ReUser2__c==null){
                    objEvent.put('BeReviewed'+2+'__c' , assVisit.BeReviewed__r.Alias);
                    objEvent.put('ReUser'+2+'__c' , assVisit.ReUser__r.Alias);
                    objEvent.put('Grade'+2+'__c' , assVisit.Grade__c);
                    objEvent.put('Comment'+2+'__c' , assVisit.Comment__c);
                    objEvent.put('IsAssVisit'+2+'__c' , assVisit.IsAssVisit__c);
                }else if(objEvent.BeReviewed3__c == null && objEvent.ReUser3__c==null){
                    objEvent.put('BeReviewed'+3+'__c' , assVisit.BeReviewed__r.Alias);
                    objEvent.put('ReUser'+3+'__c' , assVisit.ReUser__r.Alias);
                    objEvent.put('Grade'+3+'__c' , assVisit.Grade__c);
                    objEvent.put('Comment'+3+'__c' , assVisit.Comment__c);
                    objEvent.put('IsAssVisit'+3+'__c' , assVisit.IsAssVisit__c);
                }
	        }
	        list_EventUp.add(objEvent);
		}
		if(list_EventUp.size() > 0){
		    update list_EventUp;
		}
	}
    global void finish(Database.BatchableContext BC){
        
    }
}