/**
 * Author : Bill
 * date : 2013-10-13
 * deacription : PM_ExportFieldsCommon的测试类
 **/
@isTest
private class PM_Test_PatientSortCommom {

    static testMethod void myUnitTest() {
    	
    	//病人
    	List<PM_Patient__c> list_pa = new List<PM_Patient__c>();
    	PM_Patient__c Patient = new PM_Patient__c();
    	Patient.Name = '张先生';
		Patient.PM_Status__c = 'New';
		Patient.PM_NewPatientDate__c = Date.today().addMonths(-3);
		Patient.PM_LastSuccessDateTime__c  = DateTime.now().addMonths(-2);
		Patient.PM_VisitState__c = '失败';
		Patient.PM_InDate__c = Date.today().addMonths(-5);
		Patient.PM_Pdtreatment__c ='NAPD';
    	list_pa.add(Patient);
    	PM_Patient__c Patient2 = new PM_Patient__c();
    	Patient2.Name = '张先生2';
		Patient2.PM_Status__c = 'Active';
		Patient2.PM_NewPatientDate__c = Date.today().addMonths(-2);
		Patient2.PM_LastSuccessDateTime__c  = DateTime.now().addMonths(-1);
		Patient2.PM_VisitState__c = '成功';
		Patient2.PM_InDate__c = Date.today().addMonths(-4);
		Patient2.PM_Pdtreatment__c ='NAPD';
    	list_pa.add(Patient2);
    	PM_Patient__c Patient3 = new PM_Patient__c();
    	Patient3.Name = '张先生3';
		Patient3.PM_Status__c = 'Unreachable';
		Patient3.PM_NewPatientDate__c = Date.today().addMonths(-6);
		Patient3.PM_LastSuccessDateTime__c  = DateTime.now().addMonths(-4);
		Patient3.PM_VisitState__c = '失败';
		Patient3.PM_Pdtreatment__c ='APD';
    	list_pa.add(Patient3);
    	PM_Patient__c Patient4 = new PM_Patient__c();
    	Patient4.Name = '张先生4';
		Patient4.PM_Status__c = 'Dropout';
		Patient4.PM_NewPatientDate__c = Date.today().addMonths(-1);
		Patient4.PM_LastSuccessDateTime__c  = DateTime.now();
		Patient4.PM_VisitState__c = '成功';
		Patient4.PM_InDate__c = Date.today().addMonths(-4);
		Patient4.PM_Pdtreatment__c ='NAPD';
    	list_pa.add(Patient4);
    	insert list_pa;
    	
    	//拜访周期
    	map<string,CallCycleConfig__c> Config = new map<string,CallCycleConfig__c>();
    	CallCycleConfig__c c1 = new CallCycleConfig__c();
    	c1.Name = 'ActivePatientCallCycle';
    	c1.CallCycle1__c = 90;
    	c1.CallCycle2__c = 60;
    	Config.put(c1.Name,c1);
    	CallCycleConfig__c c2 = new CallCycleConfig__c();
    	c2.Name = 'NewPatientCallCycle';
    	c2.CallCycle1__c = 90;
    	c2.CallCycle2__c = 60;
    	Config.put(c2.Name,c2);
    	CallCycleConfig__c c3 = new CallCycleConfig__c();
    	c3.Name = 'APDPatientCallCycle';
    	c3.CallCycle1__c = 90;
    	c3.CallCycle2__c = 60;
    	Config.put(c3.Name,c3);
    	CallCycleConfig__c c4 = new CallCycleConfig__c();
    	c4.Name = 'UnreachablePatientCallCycle';
    	c4.CallCycle1__c = 90;
    	c4.CallCycle2__c = 60;
    	Config.put(c4.Name,c4);
    	insert Config.values();
    	
        // TO DO: implement unit test
        system.Test.startTest();
        PM_PatientSortCommom psort = new PM_PatientSortCommom();
        List<PM_Patient__c> list_PaS = [Select PM_SortDate__c,PM_Pdtreatment__c, PM_TelPriorityFirst__c,PM_TelPrioritySecond__c, PM_VisitState__c, 
        							PM_InDate__c,PM_Status__c From PM_Patient__c];
        psort.PatientSort(list_PaS);
        //psort.PatientSortNormal(list_pa);
        system.Test.stopTest();
    }
}