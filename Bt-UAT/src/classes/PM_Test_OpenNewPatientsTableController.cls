/**
 * Henry
 *2013-10-12
 *PM_OpenNewPatientsTableController 测试类
 */
@isTest
private class PM_Test_OpenNewPatientsTableController {

    static testMethod void myUnitTest() 
    {
    	system.Test.startTest();
    	
    	PatientApply__c patientApply = new PatientApply__c();
    	PM_Patient__c pat =new PM_Patient__c();
    	
        MonthlyPlan__c MonPlan =new MonthlyPlan__c();
        //MonPlan.OwnerId = user.Id;
        insert MonPlan;
        //新建省市
        Region__c zone = new Region__c();
        zone.Name = '南区';
        insert zone;
		Provinces__c pro = new Provinces__c();
    	pro.Name = '浙江省';
    	insert pro;
    	Cities__c city = new Cities__c();
    	city.Name = '杭州市';
    	city.BelongToProvince__c = pro.Id;
    	insert city;
    	
    	//客户
    	list<RecordType> record = [Select Id From RecordType Where Name = '医院'];
    	
    	list<RecordType> record2 = [Select Id From RecordType Where Name = '经销商'];
    	
        Account acc = new  Account();
        acc.Name = '浙江医院';
        acc.RecordTypeId = record[0].Id;
        acc.Provinces__c = pro.Id;
        acc.Cities__c = city.Id;
        acc.Region__c = zone.Id;
        insert acc;
        Account acc2 = new  Account();
        acc2.Name = '浙江医药控股';
        acc2.RecordTypeId = record2[0].Id;
        acc2.Provinces__c = pro.Id;
        acc2.Cities__c = city.Id;
        acc2.Region__c = zone.Id;
        insert acc2;
        
    	patientApply.Status__c = '待审批';
    	patientApply.Distributor__c = acc2.Id;
    	patientApply.IntubationHospital__c = acc.Id;
    	patientApply.PatientApply__c = MonPlan.Id;
    	patientApply.PatientName__c = 'aaa';
    	patientApply.PM_Famliy_Phone__c = '010-12345678';
    	patientApply.PM_Patient_MPhone__c = '12345678901';
    	patientApply.PM_Patient_Phone__c = '010-98765432';
    	patientApply.PM_Family_Member_Mphone__c = '12345678901';
    	patientApply.PM_Famliy_Member_Phone__c = '12345678901';
    	patientApply.PM_Other_Phone__c = '1234567890';
    	patientApply.Address__c = 'Test123';
    	insert patientApply;
    	
    	PatientApply__c patientApply2 = new PatientApply__c();
    	patientApply2.Status__c = '待审批';
    	patientApply2.Distributor__c = acc2.Id;
    	patientApply2.IntubationHospital__c = acc.Id;
    	patientApply2.PatientApply__c = MonPlan.Id;
    	patientApply2.PatientName__c = 'xxx';
    	patientApply2.PM_Famliy_Phone__c = '010-12345671';
    	patientApply2.PM_Patient_MPhone__c = '12345678902';
    	patientApply2.PM_Patient_Phone__c = '010-98765433';
    	patientApply2.PM_Family_Member_Mphone__c = '12345678904';
    	patientApply2.PM_Famliy_Member_Phone__c = '12345678905';
    	patientApply2.PM_Other_Phone__c = '1234567896';
    	patientApply2.Address__c = 'T';
    	insert patientApply2;
    	
    	PatientApply__c patientApply3 = new PatientApply__c();
    	patientApply3.Status__c = '待审批';
    	patientApply3.Distributor__c = acc2.Id;
    	patientApply3.IntubationHospital__c = acc.Id;
    	patientApply3.PatientApply__c = MonPlan.Id;
    	patientApply3.PatientName__c = 'b1';
    	patientApply3.PM_Famliy_Phone__c = '010-12345678';
    	patientApply3.PM_Patient_MPhone__c = '12345678901';
    	patientApply3.PM_Patient_Phone__c = '010-98765432';
    	patientApply3.PM_Family_Member_Mphone__c = '12345678901';
    	patientApply3.PM_Famliy_Member_Phone__c = '12345678901';
    	patientApply3.PM_Other_Phone__c = '1234567890';
    	patientApply3.Address__c = 'Test123';
    	insert patientApply3;
    	
    	PatientApply__c patientApply4 = new PatientApply__c();
    	patientApply4.Status__c = '待审批';
    	patientApply4.Distributor__c = acc2.Id;
    	patientApply4.IntubationHospital__c = acc.Id;
    	patientApply4.PatientApply__c = MonPlan.Id;
    	patientApply4.PatientName__c = 'b2';
    	patientApply4.PM_Famliy_Phone__c = '010-12345678';
    	patientApply4.PM_Patient_MPhone__c = '12345678901';
    	patientApply4.PM_Patient_Phone__c = '010-98765432';
    	patientApply4.PM_Family_Member_Mphone__c = '12345678901';
    	patientApply4.PM_Famliy_Member_Phone__c = '12345678901';
    	patientApply4.PM_Other_Phone__c = '1234567890';
    	patientApply4.Address__c = 'Test123';
    	insert patientApply4;
    	
    	PatientApply__c patientApply5 = new PatientApply__c();
    	patientApply5.Status__c = '待审批';
    	patientApply5.Distributor__c = acc2.Id;
    	patientApply5.IntubationHospital__c = acc.Id;
    	patientApply5.PatientApply__c = MonPlan.Id;
    	patientApply5.PatientName__c = 'b3';
    	patientApply5.PM_Famliy_Phone__c = '010-12345678';
    	patientApply5.PM_Patient_MPhone__c = '12345678901';
    	patientApply5.PM_Patient_Phone__c = '010-98765432';
    	patientApply5.PM_Family_Member_Mphone__c = '12345678901';
    	patientApply5.PM_Famliy_Member_Phone__c = '12345678901';
    	patientApply5.PM_Other_Phone__c = '1234567890';
    	patientApply5.Address__c = 'Test123';
    	insert patientApply5;
    	
    	PatientApply__c patientApply6 = new PatientApply__c();
    	patientApply6.Status__c = '待审批';
    	patientApply6.Distributor__c = acc2.Id;
    	patientApply6.IntubationHospital__c = acc.Id;
    	patientApply6.PatientApply__c = MonPlan.Id;
    	patientApply6.PatientName__c = 'b4';
    	patientApply6.PM_Famliy_Phone__c = '010-12345678';
    	patientApply6.PM_Patient_MPhone__c = '12345678901';
    	patientApply6.PM_Patient_Phone__c = '010-98765432';
    	patientApply6.PM_Family_Member_Mphone__c = '12345678901';
    	patientApply6.PM_Famliy_Member_Phone__c = '12345678901';
    	patientApply6.PM_Other_Phone__c = '1234567890';
    	patientApply6.Address__c = 'Test123';
    	insert patientApply6;
    	
    	PatientApply__c patientApply7 = new PatientApply__c();
    	patientApply7.Status__c = '待审批';
    	patientApply7.Distributor__c = acc2.Id;
    	patientApply7.IntubationHospital__c = acc.Id;
    	patientApply7.PatientApply__c = MonPlan.Id;
    	patientApply7.PatientName__c = 'b5';
    	patientApply7.PM_Famliy_Phone__c = '010-12345678';
    	patientApply7.PM_Patient_MPhone__c = '12345678901';
    	patientApply7.PM_Patient_Phone__c = '010-98765432';
    	patientApply7.PM_Family_Member_Mphone__c = '12345678901';
    	patientApply7.PM_Famliy_Member_Phone__c = '12345678901';
    	patientApply7.PM_Other_Phone__c = '1234567890';
    	patientApply7.Address__c = 'Test123';
    	insert patientApply7;
    	
    	PatientApply__c patientApply8 = new PatientApply__c();
    	patientApply8.Status__c = '待审批';
    	patientApply8.Distributor__c = acc2.Id;
    	patientApply8.IntubationHospital__c = acc.Id;
    	patientApply8.PatientApply__c = MonPlan.Id;
    	patientApply8.PatientName__c = 'b6';
    	patientApply8.PM_Famliy_Phone__c = '010-12345678';
    	patientApply8.PM_Patient_MPhone__c = '12345678901';
    	patientApply8.PM_Patient_Phone__c = '010-98765432';
    	patientApply8.PM_Family_Member_Mphone__c = '12345678901';
    	patientApply8.PM_Famliy_Member_Phone__c = '12345678901';
    	patientApply8.PM_Other_Phone__c = '1234567890';
    	patientApply8.Address__c = 'Test123';
    	insert patientApply8;
    	
    	PatientApply__c patientApply9 = new PatientApply__c();
    	patientApply9.Status__c = '待审批';
    	patientApply9.Distributor__c = acc2.Id;
    	patientApply9.IntubationHospital__c = acc.Id;
    	patientApply9.PatientApply__c = MonPlan.Id;
    	patientApply9.PatientName__c = 'aaa';
	 	patientApply9.PM_Famliy_Phone__c = '010-12345678';
    	patientApply9.PM_Patient_MPhone__c = '12345678901';
    	patientApply9.PM_Patient_Phone__c = '010-98765432';
    	patientApply9.PM_Family_Member_Mphone__c = '12345678901';
    	patientApply9.PM_Famliy_Member_Phone__c = '12345678901';
    	patientApply9.PM_Other_Phone__c = '1234567890';
    	patientApply9.Address__c = 'Test123';
    	insert patientApply9;
    	
    	PatientApply__c patientApply10 = new PatientApply__c();
    	patientApply10.Status__c = '待审批';
    	patientApply10.Distributor__c = acc2.Id;
    	patientApply10.IntubationHospital__c = acc.Id;
    	patientApply10.PatientApply__c = MonPlan.Id;
    	patientApply10.PatientName__c = 'ccc';
	 	patientApply10.PM_Famliy_Phone__c = '010-12345678';
    	patientApply10.PM_Patient_MPhone__c = '12345678901';
    	patientApply10.PM_Patient_Phone__c = '010-98765432';
    	patientApply10.PM_Family_Member_Mphone__c = '12345678901';
    	patientApply10.PM_Famliy_Member_Phone__c = '12345678901';
    	patientApply10.PM_Other_Phone__c = '1234567890';
    	patientApply10.Address__c = 'Test123';
    	insert patientApply10;
    	
    	PatientApply__c patientApply11 = new PatientApply__c();
    	patientApply11.Status__c = '待审批';
    	patientApply11.Distributor__c = acc2.Id;
    	patientApply11.IntubationHospital__c = acc.Id;
    	patientApply11.PatientApply__c = MonPlan.Id;
    	patientApply11.PatientName__c = 'ccc';
    	patientApply11.PM_Famliy_Phone__c = '010-12345678';
    	patientApply11.PM_Patient_MPhone__c = '12345678901';
    	patientApply11.PM_Patient_Phone__c = '010-98765432';
    	patientApply11.PM_Family_Member_Mphone__c = '12345678901';
    	patientApply11.PM_Famliy_Member_Phone__c = '12345678901';
    	patientApply11.PM_Other_Phone__c = '1234567890';
    	patientApply11.Address__c = 'Test';
    	insert patientApply11;
    	
    	pat.Name = 'aaa';
    	pat.PM_HomeTel__c = '010-12345678';
    	pat.PM_PmPhone__c = '12345678901';
    	pat.PM_PmTel__c = '010-98765432';
    	pat.PM_FamilyPhone__c = '12345678901';
    	pat.PM_FamilyTel__c = '12345678901';
    	pat.PM_OtherTel2__c = '1234567890';
    	pat.PM_Address__c = 'Test123';
    	insert pat;
    	PM_Patient__c pat1 =new PM_Patient__c();
    	pat1.Name = 'b1';
    	pat1.PM_PmPhone__c = '12345678901';
    	pat1.PM_PmTel__c = '010-98765432';
    	pat1.PM_FamilyPhone__c = '12345678901';
    	pat1.PM_FamilyTel__c = '12345678901';
    	pat1.PM_OtherTel2__c = '1234567890';
    	pat1.PM_Address__c = 'Test123';
    	insert pat1;
    	PM_Patient__c pat2 =new PM_Patient__c();
    	pat2.Name = 'b2';
    	pat2.PM_PmTel__c = '010-98765432';
    	pat2.PM_FamilyPhone__c = '12345678901';
    	pat2.PM_FamilyTel__c = '12345678901';
    	pat2.PM_OtherTel2__c = '1234567890';
    	pat2.PM_Address__c = 'Test123';
    	insert pat2;
    	PM_Patient__c pat3 =new PM_Patient__c();
    	pat3.Name = 'b3';
    	pat3.PM_FamilyPhone__c = '12345678901';
    	pat3.PM_FamilyTel__c = '12345678901';
    	pat3.PM_OtherTel2__c = '1234567890';
    	pat3.PM_Address__c = 'Test123';
    	insert pat3;
    	PM_Patient__c pat4 =new PM_Patient__c();
    	pat4.Name = 'b4';
    	pat4.PM_FamilyTel__c = '12345678901';
    	pat4.PM_OtherTel2__c = '1234567890';
    	pat4.PM_Address__c = 'Test123';
    	insert pat4;
    	PM_Patient__c pat5 =new PM_Patient__c();
    	pat5.Name = 'b5';
    	pat5.PM_OtherTel2__c = '1234567890';
    	pat5.PM_Address__c = 'Test123';
    	insert pat5;
    	PM_Patient__c pat6 =new PM_Patient__c();
    	pat6.Name = 'b6';
    	pat6.PM_Address__c = 'Test123';
    	insert pat6;
		ApexPages.currentPage().getParameters().put('newid',patientApply.Id);
    	ApexPages.StandardController controller = new ApexPages.StandardController(patientApply);
        //实例化控制类
    	PM_OpenNewPatientsTableController onp = new PM_OpenNewPatientsTableController(controller);
    	
    	ApexPages.currentPage().getParameters().put('newid',patientApply2.Id);
    	ApexPages.StandardController controller2 = new ApexPages.StandardController(patientApply2);
        //实例化控制类
    	PM_OpenNewPatientsTableController onp2 = new PM_OpenNewPatientsTableController(controller2);
    	
    	ApexPages.currentPage().getParameters().put('newid',patientApply3.Id);
    	ApexPages.StandardController controller3 = new ApexPages.StandardController(patientApply3);
        //实例化控制类
    	PM_OpenNewPatientsTableController onp3 = new PM_OpenNewPatientsTableController(controller3);
    	
    	ApexPages.currentPage().getParameters().put('newid',patientApply4.Id);
    	ApexPages.StandardController controller4 = new ApexPages.StandardController(patientApply4);
        //实例化控制类
    	PM_OpenNewPatientsTableController onp4 = new PM_OpenNewPatientsTableController(controller4);
    	
    	ApexPages.currentPage().getParameters().put('newid',patientApply5.Id);
    	ApexPages.StandardController controller5 = new ApexPages.StandardController(patientApply5);
        //实例化控制类
    	PM_OpenNewPatientsTableController onp5 = new PM_OpenNewPatientsTableController(controller5);
    	
    	ApexPages.currentPage().getParameters().put('newid',patientApply6.Id);
    	ApexPages.StandardController controller6 = new ApexPages.StandardController(patientApply6);
        //实例化控制类
    	PM_OpenNewPatientsTableController onp6 = new PM_OpenNewPatientsTableController(controller6);
    	
    	ApexPages.currentPage().getParameters().put('newid',patientApply7.Id);
    	ApexPages.StandardController controller7 = new ApexPages.StandardController(patientApply7);
        //实例化控制类
    	PM_OpenNewPatientsTableController onp7 = new PM_OpenNewPatientsTableController(controller7);
    	
    	ApexPages.currentPage().getParameters().put('newid',patientApply8.Id);
    	ApexPages.StandardController controller8 = new ApexPages.StandardController(patientApply8);
        //实例化控制类
    	PM_OpenNewPatientsTableController onp8 = new PM_OpenNewPatientsTableController(controller8);
    	
    	ApexPages.currentPage().getParameters().put('newid',patientApply9.Id);
    	ApexPages.StandardController controller9 = new ApexPages.StandardController(patientApply9);
        //实例化控制类
    	PM_OpenNewPatientsTableController onp9 = new PM_OpenNewPatientsTableController(controller9);
    	
    	ApexPages.currentPage().getParameters().put('newid',patientApply10.Id);
    	ApexPages.StandardController controller10 = new ApexPages.StandardController(patientApply10);
        //实例化控制类
    	PM_OpenNewPatientsTableController onp10 = new PM_OpenNewPatientsTableController(controller10);
    	
    	ApexPages.currentPage().getParameters().put('newid',patientApply11.Id);
    	ApexPages.StandardController controller11 = new ApexPages.StandardController(patientApply11);
        //实例化控制类
    	PM_OpenNewPatientsTableController onp11 = new PM_OpenNewPatientsTableController(controller11);
    	system.Test.stopTest();
    }
}