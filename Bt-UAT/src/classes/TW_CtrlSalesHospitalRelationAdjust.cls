/**
 *2013-10-18
 *复制大陆功能
 * Author: Sunny
 * 调整销售医院关系页面控制器
 * 1.主管可以在此页面调整销售医院关系（收到日期的限制）
 * 2.管理员可以在此页面不受限制的调整销售医院关系
 * 3.调整时，如果该医院有存货病人或有指标或当月有报过新病人的，只能转给主管或者其他销售
 * BILL ADD 2013-8-14
 * 医院指标，现在系统已经有了医院的指标数据，不需要新建对象来做。
 * MD的医院指标：看本财年该医院，GBU为MD的销售数据是否有目标销量（只要本财年有指标就可以，不需要看当前月）
 * RENAL的医院指标：看本财年该医院的病人关爱数据，Surviving Target，newpatient字段是否有值。
 * 不同的BU在做销售医院关系调整时，会有不同的新病人、存活病人、医院指标的限制，下图为ceci提供不同bu的限制：
 * BU 限制权限
 * IVT 只看目标销量
 * SP 只看目标销量
 * PD 目标销量，新病人，有存活病人
 * HD 都不需要看
**/
public without sharing class TW_CtrlSalesHospitalRelationAdjust {
	 public List<SelectOption> listSubUser{get;set;}//原成员列表
    public List<SelectOption> listOperate{get{//操作列表
       List<SelectOption> list_so = new List<SelectOption>();
       list_so.add(new SelectOption( '新增' , '新增'));
       list_so.add(new SelectOption( '替换' , '替換'));
       list_so.add(new SelectOption( '删除' , '刪除'));
       return list_so;
    }set;}
    public String SelectedOperate{get;set;}//选择的操作
    public Boolean EnableAddMember{get;set;}//是否显示新增页面
    public Boolean EnableReplaceMember{get;set;}//是否显示替换页面
    public Boolean EnableDeleteMember{get;set;}//是否显示删除页面
    public Boolean EnablePage{get;set;}//是否显示页面
    public Boolean blnReplace{get;set;}//是否选择替换原成员
    public String SelectedSubUser{get;set;}//选择的被替换的成员
    public String strWarningMsg{get;set;}
    public Boolean blnShowWarning{get;set;}
    public V2_Account_Team__c objAccountTeam{get;set;}
    private Boolean isBuAdmin ;//是否为admin
    private Set<ID> set_SubUserIds ;//若当前用户为主管，其下属用户id集合
    private ID HostipalId ;//医院ID
    private Map<ID,V2_Account_Team__c> Map_SalesAccTeam = new Map<ID,V2_Account_Team__c>();
    public void SelectOperate(){//根据所选择的操作，初始化页面
        EnableAddMember = false;
        EnableReplaceMember = false;
        EnableDeleteMember = false;
        if(SelectedOperate == '替换'){//替换
            EnableReplaceMember = true;
        }else if(SelectedOperate == '删除'){//删除
            EnableDeleteMember = true;
        }else{//新增，默认显示新增
            SelectedOperate = '新增';
            EnableAddMember = true;
        }
    }
    public TW_CtrlSalesHospitalRelationAdjust(Apexpages.Standardcontroller controller)
    {//构造方法
       
       
        EnablePage = true;
        HostipalId = ApexPages.currentPage().getParameters().get('HosId');
        if(HostipalId == null){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning, '必須指定一個Account。');            
            ApexPages.addMessage(msg);
            EnablePage = false;
            return;
        }
        listSubUser = new  List<SelectOption>();
        objAccountTeam = new V2_Account_Team__c();
        
        try
       {
       	 //初始化当前用户信息
        this.initCurrentUser();
        //检查此医院下是否有正在审批中的销售医院关系
        this.initCurrentSubUserList();
        
        SelectedOperate = '新增';
        this.SelectOperate();
       }catch(Exception e)
       {
       	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()+'   '+e.getlineNumber());            
		    ApexPages.addMessage(msg);
		    return ;
       }
       
        
       
    }
    /**
    *初始化当前用户信息
    **/
    private void initCurrentUser()
    {
        User objuser = [SELECT ID,Profile.Name,UserRole.Name from User WHERE Id =: UserInfo.getUserId()] ;
        if(objuser.UserRole.Name != null && !objuser.UserRole.Name.toUpperCase().contains('TW'))
        {
        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '此功能只開放給台灣地區用戶，您沒有權限進行此操作。');            
            ApexPages.addMessage(msg);
        	EnablePage = false;
        }
        else if(objuser.UserRole.Name == null && !isBuAdmin)
        {
        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '無法確定您的角色。');            
            ApexPages.addMessage(msg);
            EnablePage = false;
        }    
    }
    /**
    *初始化主管下属信息
    **/
    /*private void initSubUser(){
        V2_UtilClass cls = new V2_UtilClass();
        //主管下属，包含下属的下属
        set_SubUserIds = cls.GetUserSubIds();
        //包含自己
        set_SubUserIds.add(UserInfo.getUserId());
        this.initCurrentSubUserList();
    }*/
    /**
    *判断此客户下的销售医院关系记录中是否有正在审批的记录
    **/
    private void initCurrentSubUserList()
    {
        for(V2_Account_Team__c objAT : [select id,V2_User__c,V2_User__r.Name,V2_Role__c,V2_Is_Delete__c,V2_ApprovalStatus__c,
                   V2_LastAccessDate__c,V2_Effective_Year__c,V2_Effective_Month__c,V2_ImmediateDelete__c,EffectiveDate__c
                   from V2_Account_Team__c 
                   where V2_Account__c =: HostipalId And V2_History__c = false ]){
            //判断是否有下属处于审批中
            if(objAT.V2_ApprovalStatus__c=='审批中'){
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '請在審批結束後添加小組成員。');            
                ApexPages.addMessage(msg);
                EnablePage = false;
                return;
            }
            //判断是否存在下属
          //  if(!objAT.V2_Is_Delete__c)
           // {
                Map_SalesAccTeam.put(objAT.V2_User__c,objAT);
                //ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO , '您在该医院下已存在下属，请选择是否替换已存在的下属。');            
                //ApexPages.addMessage(msg);
               // if(set_SubUserIds!=null && set_SubUserIds.contains(objAT.V2_User__c))
                //{
                   //listSubUser.add(new SelectOption(String.valueOf(objAT.V2_User__c),String.valueOf(objAT.V2_User__r.Name))) ;
               // }
          //  }
        }
        /*2013-11-21修改*/
        for(AccountTeamMember Atm: [select User.Name, UserId from  AccountTeamMember where AccountId =: HostipalId  and UserId in: Map_SalesAccTeam.keySet()])
        {
        	listSubUser.add(new SelectOption(String.valueOf(Atm.UserId),String.valueOf(Atm.User.Name))) ;
        }
    }
    /**
    *取消按钮
    **/
    public PageReference doCancel(){
        return new PageReference('/'+HostipalId) ;
    }
    /**
    *保存按钮
    **/
    public PageReference doSave(){
    	try
    	{
    		//检查信息是否填写完整
	        if(this.SelectedOperate == '新增' || this.SelectedOperate == '替换'){
	            if(this.objAccountTeam.V2_User__c == null){
	                objAccountTeam.V2_User__c.addError('請填寫新成員。');
	                return null;
	            }
	        }
	        if(this.SelectedOperate == '替换' || this.SelectedOperate == '删除'){
	        	System.debug('###############################################################'+SelectedSubUser);
	            if(SelectedSubUser == null){
	                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '請選擇被'+SelectedOperate+'人');            
	                ApexPages.addMessage(msg);
	                return null;
	            }
	        }
	        /*if(this.objAccountTeam.EffectiveDate__c == null){
	            objAccountTeam.EffectiveDate__c.addError('请填写日期。');
	            return null;
	        }*/
	        if(this.objAccountTeam.V2_AdjustReson__c == null){
	            objAccountTeam.V2_AdjustReson__c.addError('請填寫調整原因。');
	            return null;
	        }
	        
	       
	        
	        Id ApprovalId ;//提交审批的记录ID
	        if(this.SelectedOperate == '新增'){
	            objAccountTeam.V2_Account__c = this.HostipalId ;
	            objAccountTeam.V2_NewAccUser__c = objAccountTeam.V2_User__c;
	            objAccountTeam.V2_BatchOperate__c='新增';
	            objAccountTeam.V2_ApprovalStatus__c='待审批';
	            objAccountTeam.ownerid=objAccountTeam.V2_User__c;
	            insert objAccountTeam ;
	            ApprovalId = objAccountTeam.id;
	        }else if(this.SelectedOperate == '替换'){
	        	
	            V2_Account_Team__c objAccTeamDel = this.Map_SalesAccTeam.get(SelectedSubUser);
	            objAccTeamDel.V2_BatchOperate__c='替换';
	            objAccTeamDel.V2_ApprovalStatus__c='待审批';
	            objAccTeamDel.V2_NewAccUser__c=objAccountTeam.V2_User__c;
	            objAccTeamDel.V2_AdjustReson__c=objAccountTeam.V2_AdjustReson__c;
	            ApprovalId = objAccTeamDel.id;
	            update objAccTeamDel;
	            system.debug('tihuanaaaa'+objAccTeamDel) ;
	        }else if(this.SelectedOperate == '删除'){
	            V2_Account_Team__c objAccTeamDel = this.Map_SalesAccTeam.get(SelectedSubUser);
	         
	            objAccTeamDel.V2_BatchOperate__c='删除';
	            objAccTeamDel.V2_ApprovalStatus__c='待审批';
	            objAccTeamDel.V2_AdjustReson__c=objAccountTeam.V2_AdjustReson__c;
	            ApprovalId = objAccTeamDel.id;
	            update objAccTeamDel;
	        }
	        //提交审批
	        if(ApprovalId != null)
	        {
	            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
	            req.setObjectId(ApprovalId);//客户联系人或自定义对象
	            Approval.ProcessResult result = Approval.process(req);
	        }
	        return new PageReference('/'+HostipalId) ;
    	}catch(Exception e)
    	{
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()+'   '+e.getlineNumber());            
		    ApexPages.addMessage(msg);
		    return null;
    	}
        
    }
/*========================================================测试类=========================================================*/    
    static testMethod void TW_CtrlSalesHospitalRelationAdjust()
    {
    	  //销售
	    UserRole RepUserRole = new UserRole() ;
	    RepUserRole.Name = 'TW South Sales Rep';
	    insert RepUserRole ;
	    //rep简档
	    Profile RepPro = [select Id from Profile where Name  = 'TW Sales Rep' limit 1];
	    /************User************/
		List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ; 
	     
	    /*销售*/
	    User RepSu = new User();
	    RepSu.Username='RepSu@123.com';
	    RepSu.LastName='RepSu';
	    RepSu.Email='RepSu@123.com';
	    RepSu.Alias=user[0].Alias;
	    RepSu.TimeZoneSidKey=user[0].TimeZoneSidKey;
	    RepSu.ProfileId=RepPro.Id;
	    RepSu.LocaleSidKey=user[0].LocaleSidKey;
	    RepSu.LanguageLocaleKey=user[0].LanguageLocaleKey;
        RepSu.EmailEncodingKey=user[0].EmailEncodingKey;
	    RepSu.CommunityNickname='RepSu';
	    RepSu.MobilePhone='12345678912';
	    RepSu.UserRoleId = RepUserRole.Id ;
	    RepSu.IsActive = true;
     	insert RepSu;
     	
     	 /*销售*/
	    User RepSu2 = new User();
	    RepSu2.Username='RepSu2@123.com';
	    RepSu2.LastName='RepSu2';
	    RepSu2.Email='RepSu2@123.com';
	    RepSu2.Alias=user[0].Alias;
	    RepSu2.TimeZoneSidKey=user[0].TimeZoneSidKey;
	    RepSu2.ProfileId=RepPro.Id;
	    RepSu2.LocaleSidKey=user[0].LocaleSidKey;
	    RepSu2.LanguageLocaleKey=user[0].LanguageLocaleKey;
        RepSu2.EmailEncodingKey=user[0].EmailEncodingKey;
	    RepSu2.CommunityNickname='RepSu2';
	    RepSu2.MobilePhone='12345678912';
	    RepSu2.UserRoleId = RepUserRole.Id ;
	    RepSu2.IsActive = true;
     	insert RepSu2;
     	
     	
	    
    	/*客户*/
		RecordType accrecordtype = [select Id from RecordType where DeveloperName='TW_Hospital' and SobjectType='Account' limit 1];
		Account acc = new Account();
		acc.RecordTypeId = accrecordtype.Id;
		acc.Name = 'AccTest';
		insert acc;
		V2_Account_Team__c ateam = new V2_Account_Team__c();
        ateam.V2_Account__c = acc.Id;
        ateam.V2_User__c = RepSu.Id;
        ateam.V2_BatchOperate__c='新增';
        ateam.V2_ApprovalStatus__c = '审批通过';
        ateam.V2_History__c = false;
        insert ateam;
        
		ApexPages.currentPage().getParameters().put('HosId',null);
    	Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(new V2_Account_Team__c());
    	TW_CtrlSalesHospitalRelationAdjust RelationAdjust1 = new TW_CtrlSalesHospitalRelationAdjust(controller);
    	
    	ApexPages.currentPage().getParameters().put('HosId',acc.Id);
    	TW_CtrlSalesHospitalRelationAdjust RelationAdjust22 = new TW_CtrlSalesHospitalRelationAdjust(controller);
    	
    	System.runAs(RepSu)
    	{
    		ApexPages.currentPage().getParameters().put('HosId',acc.Id);
    		TW_CtrlSalesHospitalRelationAdjust RelationAdjust = new TW_CtrlSalesHospitalRelationAdjust(controller);
    		RelationAdjust.doCancel();
	    	RelationAdjust.SelectOperate();
	    	RelationAdjust.doSave();
	    	RelationAdjust.objAccountTeam.V2_AdjustReson__c = null;
	    	RelationAdjust.objAccountTeam.V2_User__c = RepSu.Id; 
	    	RelationAdjust.doSave();
	    	RelationAdjust.objAccountTeam.V2_AdjustReson__c = '333';
	    	RelationAdjust.doSave();
	    	RelationAdjust.SelectedOperate = '替换';
	    	
	    	RelationAdjust.objAccountTeam.V2_User__c = RepSu.Id; 
	    	RelationAdjust.SelectOperate();
	    	RelationAdjust.objAccountTeam.V2_AdjustReson__c = '333';
	    	RelationAdjust.SelectedSubUser = RepSu2.Id;
	    	RelationAdjust.doSave();
	    	RelationAdjust.SelectedOperate = '删除';
	    	
	    	RelationAdjust.objAccountTeam.V2_AdjustReson__c = '333';
	    	RelationAdjust.SelectOperate();
	    	RelationAdjust.SelectedSubUser = RepSu2.Id;
	    	RelationAdjust.doSave();
	    	
	    	
    	}
    	ateam.V2_ApprovalStatus__c = '审批中';
        update ateam;
	    RelationAdjust22.initCurrentSubUserList();	
    	
    	
    }
    
}