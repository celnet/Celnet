/**
 * 作者：Sunny
 * 功能：协防质量评分
 * 2013-5-16 Sunny注释掉保存评论之后自动跳转到代码，由于主管在点评之后需要点击发送评分评语按钮。
 * 2013-12-12 Tobe : 事件字段清除，去掉有关点评相关的4、5的字段的引用
**/
public class BQ_CtrlVisitComments {
  //页面呈现List
  public List<AssVisitComments__c> list_AssVisitComm{get;set;}
  public List<VisitCommentWrapper> list_AssVisitCommWrapper{get;set;}
  
  //
  public List<SelectOption> getEventAttendees(){
    List<SelectOption> options = new List<SelectOption>();
    options.add(new SelectOption('--无--','--无--'));
    List<String> list_AttendeeName = new List<String>();
    list_AttendeeName.addAll(map_EventAttendeeNameId.keySet());
    list_AttendeeName.sort();
    for(String strEventAttendeeName : list_AttendeeName){
      options.add(new SelectOption(map_EventAttendeeNameId.get(strEventAttendeeName),strEventAttendeeName));
    }
    return options;
  }
  public String strEditId{get;set;}
  public boolean ismanager{get;set;}
  //事件ID
  public ID eventId{get;set;}
  //当前登陆人（点评人ID）
  public ID UserId{get{return UserInfo.getUserId();}set;}
  //是否参加协访
  private boolean IsAssVisit;
  //参与人名字与ID
  private Map<String,Id> map_EventAttendeeNameId = new Map<String,Id>();
  //参与人别名与ID
  private Map<ID,String> map_ID_Name = new Map<ID,String>();
  //系统已有点评人ID+被点评人ID
  private Set<String> set_Ids = new Set<String>();
  //Event
  Event objEvent;
  public Boolean CanNew{get;set;}
  Boolean isAdmin;
  
  //构造函数
    public BQ_CtrlVisitComments(Apexpages.Standardcontroller controller){
      list_AssVisitCommWrapper = new List<VisitCommentWrapper>();
      
      eventId = Apexpages.currentPage().getParameters().get('peid');
      if(eventId == null ){
            eventId = controller.getId();
        }
      //2013-12-12 commented by Tobe
      //objEvent = [Select Id,StartDateTime,BeReviewed1__c,BeReviewed2__c,BeReviewed3__c,BeReviewed4__c,BeReviewed5__c,ReUser1__c,ReUser2__c,ReUser3__c,ReUser4__c,ReUser5__c,Grade1__c,Grade2__c,Grade3__c,Grade4__c,Grade5__c,Comment1__c,Comment2__c,Comment3__c,Comment4__c,Comment5__c,IsAssVisit1__c,IsAssVisit2__c,IsAssVisit3__c,IsAssVisit4__c,IsAssVisit5__c From Event Where Id =: eventId];
      objEvent = [Select Id,StartDateTime,BeReviewed1__c,BeReviewed2__c,BeReviewed3__c,ReUser1__c,ReUser2__c,ReUser3__c,Grade1__c,Grade2__c,Grade3__c,Comment1__c,Comment2__c,Comment3__c,IsAssVisit1__c,IsAssVisit2__c,IsAssVisit3__c From Event Where Id =: eventId];
      if(objEvent.StartDateTime.month() == date.today().month()){
                CanNew=true;
        //2013-7-9 Sunny修改BUG
        //}else if(objEvent.StartDateTime.addMonths(1).month() == date.today().month() && date.today().day() <= 7){
        //}else if((objEvent.StartDateTime.month()+1) == date.today().month() && date.today().day() <= 7){
        }else if(date.valueOf(objEvent.StartDateTime.year()+'-'+objEvent.StartDateTime.month()+'-1').addMonths(1).addDays(7) >= date.today()){
                CanNew=true;
        }
      getVisitComment();
      getEventAttendee();
      
      List<User> listuser= [Select Id,Name,UserRoleId,UserRole.Name,Profile.Name,ProfileId,Alias From User Where Id =: UserInfo.getUserId() and IsActive = true];
      //List<UserRole> listur = [Select Id,Name From UserRole Where Id =: Userinfo.getUserRoleId()];
        if(listuser.size() > 0 ){
          map_ID_Name.put(listuser[0].Id , listuser[0].Alias);
            String strRolName = listuser[0].UserRole.Name; 
            String strProName = listuser[0].Profile.Name;
            if(strRolName.toUpperCase().contains('REP')){
                ismanager=false;
            }else {
                ismanager=true;
            }
            
            if(strProName.toUpperCase().contains('ADMIN')){
              CanNew=true;
              isAdmin=true;
            }else{
              isAdmin=false;
            }
        }
    }
    //获取已评分的记录
    private void getVisitComment(){
      if(eventId!=null){
        list_AssVisitCommWrapper.clear();
        set_Ids.clear();
        //list_AssVisitComm = [Select Id,EventId__c,BeReviewed__c,ReUser__c,Comment__c,Grade__c,IsAssVisit__c From AssVisitComments__c Where EventId__c =: eventId] ;
        for(AssVisitComments__c vc:[Select Id,EventId__c,BeReviewed__c,BeReviewed__r.Name,ReUser__c,ReUser__r.Name,Comment__c,Grade__c,IsAssVisit__c From AssVisitComments__c Where EventId__c =: eventId And BeReviewed__c!=null]){
          system.debug(vc);
          //统计系统已有的点评（点评人id+被点评人id）
          set_Ids.add(vc.ReUser__c+''+vc.BeReviewed__c);
          VisitCommentWrapper vcw = new VisitCommentWrapper();
          vcw.strBeRe = vc.BeReviewed__r.Name;
          vcw.AssVisitComment=vc;
          vcw.IsEdit=false;
          vcw.IsNoEdit=true;
          if(vc.ReUser__c == UserInfo.getUserId()){
            if(objEvent.StartDateTime.month() == date.today().month()){
                          vcw.CanEdit=true;
                  }else if(objEvent.StartDateTime.addMonths(1).month() == date.today().month() && date.today().day() <= 7){
                          vcw.CanEdit=true;
                  }
                  if(isAdmin==true){
                    vcw.CanEdit=true;
                  }
          }
          
          
          list_AssVisitCommWrapper.add(vcw);
        }
      }
      
    }
    //获取该事件下所有参与人（被邀请协访的和事件owner）
    private void getEventAttendee(){
      Event Eve = [Select Id,OwnerId,Owner.Name,Owner.UserRoleId,Owner.Alias From Event Where Id =: eventId];
      map_EventAttendeeNameId.put(Eve.Owner.Name , Eve.OwnerId);
      map_ID_Name.put(Eve.OwnerId , Eve.Owner.Alias);
      V2_UtilClass uc = new V2_UtilClass();
      List<ID> list_userId = getHighLevel(UserInfo.getUserId() , Eve.OwnerID);
	  if(list_userId == null)
	  {
	    return;
	  }
      for(User objUser : [Select Id,Name,Alias From User Where Id in: list_userId and IsActive = true]){
        map_EventAttendeeNameId.put(objUser.Name , objUser.Id);
        map_ID_Name.put(objUser.Id , objUser.Alias);
      }
      map_EventAttendeeNameId.put(Eve.Owner.Name , Eve.OwnerId);
      
      
      /*
      //List<String> list_EventAttendeeName = new List<String>();
      map_EventAttendeeNameId.clear();
      if(eventId!=null){
        for(EventAttendee ea : [Select Id,Attendee.Name,AttendeeId From EventAttendee Where EventId =: eventId]){
              //list_EventAttendeeName.add(ea.Attendee.Name);
              map_EventAttendeeNameId.put(ea.Attendee.Name , ea.AttendeeId);
          }
          Event Eve = [Select Id,OwnerId,Owner.Name From Event Where Id =: eventId];
          //list_EventAttendeeName.add(Eve.Owner.Name);
          map_EventAttendeeNameId.put(Eve.Owner.Name,Eve.OwnerId);
      }
      //return list_EventAttendeeName;
      */
      
    }
    private list<ID> getHighLevel(ID userId,ID eventOwnerId)
    {
    	map<ID,ID> map_User_Manager = new map<ID,ID>();
    	for(User user : [select ID,ManagerId From User Where isActive = true])
    	{
    		map_User_Manager.put(user.Id,user.ManagerId);
    	}
    	list<ID> list_userID = new list<ID>();
    	for(ID thisID = eventOwnerId ; map_User_Manager.containsKey(thisID) && map_User_Manager.get(thisID)!=null;thisID=map_User_Manager.get(thisID))
    	{
    		if(map_User_Manager.get(thisID) == userId)
    		{
    			list_userID.add(thisID);
    			return list_userId;
    		}
    		else
    		{
    			list_userID.add(thisID);
    		}
    	}
    	return null;
    	
    }
    //添加新的评价行
    public void addComment(){
      VisitCommentWrapper vcw = new VisitCommentWrapper();
      AssVisitComments__c avc = new AssVisitComments__c();
      avc.ReUser__c=UserInfo.getUserId();
      avc.IsAssVisit__c=(IsAssVisit==true?IsAssVisit:false);
      avc.EventId__c=eventId;
      vcw.AssVisitComment=avc;
      vcw.IsNew=true;
      vcw.IsNoEdit=false;
      if(list_AssVisitCommWrapper!=null && list_AssVisitCommWrapper.size() > 0){
        list_AssVisitCommWrapper.add(0,vcw);
      }else{
        list_AssVisitCommWrapper=new List<VisitCommentWrapper>();
        list_AssVisitCommWrapper.add(vcw);
      }
    }
    //保存
    //public Pagereference CommentSave(){
    public void CommentSave(){
      
      /*************bill 添加  2013/5/2 start*******************/
      //是否接受协防
      boolean isAccepted;
      //被评分人ID
      ID userId;
      //评分人ID
      ID reUserId;
      /*************bill 添加  2013/5/2 end*******************/
      
      List<AssVisitComments__c> list_avc = new List<AssVisitComments__c>();
      List<AssVisitComments__c> list_avcUp = new List<AssVisitComments__c>();
      system.debug('wrapper info:'+list_AssVisitCommWrapper);
      String struid;
      if(list_AssVisitCommWrapper!=null&&list_AssVisitCommWrapper.size() > 0){
        Integer i = Integer.valueOf(strEditId);
        i=i-1;
        VisitCommentWrapper vcw = list_AssVisitCommWrapper[i];
          /*************bill 添加  2013/5/2 start*******************/
          userId = list_AssVisitCommWrapper[i].AssVisitComment.BeReviewed__c;
          reUserId = list_AssVisitCommWrapper[i].AssVisitComment.ReUser__c;
          isAccepted = list_AssVisitCommWrapper[i].AssVisitComment.IsAssVisit__c;
          system.debug(userId+'$$$$$$$$$$$$$$'+reUserId);
          /*************bill 添加  2013/5/2 end*******************/
        if(list_AssVisitCommWrapper[i].AssVisitComment.IsAssVisit__c == true){
          
        }else{
          //检查该用户是否已经接受了邀请，如果接受了，自动打钩。
          list_AssVisitCommWrapper[i].AssVisitComment.IsAssVisit__c = checkIsAssVisit();
        }
        if(list_AssVisitCommWrapper[i].AssVisitComment.Id == null ){
          list_AssVisitCommWrapper[i].AssVisitComment.BeReviewed__c = list_AssVisitCommWrapper[i].strBeRe;
          list_avc.add(list_AssVisitCommWrapper[i].AssVisitComment);
        }else{
          list_avcUp.add(list_AssVisitCommWrapper[i].AssVisitComment);
        }
        struid=list_AssVisitCommWrapper[i].AssVisitComment.BeReviewed__c;
        //vcw.CanEdit=true;
        list_AssVisitCommWrapper[i].IsNoEdit=true;
        list_AssVisitCommWrapper[i].IsEdit=false;
        list_AssVisitCommWrapper[i].IsNew=false;
        if(list_AssVisitCommWrapper[i].AssVisitComment.ReUser__c == UserInfo.getUserId()){
          list_AssVisitCommWrapper[i].CanEdit=true;
        }
        
        /*
        for(VisitCommentWrapper vcw : list_AssVisitCommWrapper){
          if(strEditId == 'new'){
            system.debug('record id:'+vcw.AssVisitComment.Id );
            if(vcw.AssVisitComment.Id == null && vcw.strBeRe!=null && !vcw.strBeRe.contains('无')){
                      vcw.AssVisitComment.BeReviewed__c = vcw.strBeRe;
                      system.debug('Comment info:'+vcw.AssVisitComment);
                      list_avc.add(vcw.AssVisitComment);
                  }
          }else if(vcw.AssVisitComment.Id == strEditId){
            if(vcw.IsEdit == true){
              list_avcUp.add(vcw.AssVisitComment);
            }
          }
          
              
          }
          */
      }else{
        
      }
      system.debug('insert list:'+list_avc);
      //try{
        	/**************bill add 2013/4/26 begin***********************/
            UpdateVisitorCompleted visit = new UpdateVisitorCompleted();
            //Sunny 2013-5-14 注释掉自动发邮件。
            //发送邮件通知
            //AutoSendmail autoSendmail = new AutoSendmail();
            if(list_avc.size() > 0){
                insert list_avc;
                //autoSendmail.AutoSendmailEvent(eventId, list_avc[0].Id, '评分');
                if(list_avc[0].IsAssVisit__c)
                {
            	  visit.IsReUserExist(eventId, list_avc[0].ReUser__c);
            	  system.debug('**********************success');
                }
                updateEvent(list_avc , 'New');
            }
            if(list_avcUp.size() > 0){
                update list_avcUp;
                //autoSendmail.AutoSendmailEvent(eventId, list_avcUp[0].Id, '评分修改');
                if(list_avcUp[0].IsAssVisit__c)
                {
            	   visit.IsReUserExist(eventId, list_avcUp[0].ReUser__c);
            	   system.debug('**********************success');
                }
                updateEvent(list_avcUp , 'Update');
            }
            /**************bill add 2013/4/26 end  ***********************/
      //}catch(exception e){
      //  system.debug('Have Error'+e);
      //  return null;
      //}
      //return null;
      /********************bill update 2013/5/2 start*****************/
      /*2013-5-16Sunny注释掉
      if(isAccepted){
      	return new Pagereference('/apex/JumpMiddPage?Uid='+reUserId);
      }else{
        return new Pagereference('/apex/JumpMiddPage?Uid='+userId);
      }
      */
      /********************bill update 2013/5/2 end  *****************/
      //return new Pagereference('/apex/JumpMiddPage?Uid='+struid);
      //getVisitComment();
    }
    public void SendEmail(){
    	if(strEditId!=null){
    		Integer i = Integer.valueOf(strEditId);
			i=i-1;
			AutoSendmail autoSendmail = new AutoSendmail();
			autoSendmail.AutoSendmailEvent(list_AssVisitCommWrapper[i].AssVisitComment.EventId__c , list_AssVisitCommWrapper[i].AssVisitComment.Id, '评分');
    	}
    	
    }
    private void updateEvent(List<AssVisitComments__c> list_assVisitComment , String strOperaate){
    	system.debug('In Up Eve '+strOperaate);
      if(strOperaate == 'New'){
        if(objEvent.BeReviewed1__c == null){
          Write(list_assVisitComment[0] , 1);
        }else if(objEvent.BeReviewed2__c == null){
          Write(list_assVisitComment[0] , 2);
        }else if(objEvent.BeReviewed3__c == null){
                Write(list_assVisitComment[0] , 3);
            }
            /*2013-12-12 commented by Tobe
            else if(objEvent.BeReviewed4__c == null){
                Write(list_assVisitComment[0] , 4);
            }else if(objEvent.BeReviewed5__c == null){
                Write(list_assVisitComment[0] , 5);
            }*/
            else{
              return ;
            }
      }else if(strOperaate == 'Update'){
        if(objEvent.BeReviewed1__c == map_ID_Name.get(list_assVisitComment[0].BeReviewed__c) && objEvent.ReUser1__c==map_ID_Name.get(list_assVisitComment[0].ReUser__c)){
                Write(list_assVisitComment[0] , 1);
            }else if(objEvent.BeReviewed2__c == map_ID_Name.get(list_assVisitComment[0].BeReviewed__c) && objEvent.ReUser2__c==map_ID_Name.get(list_assVisitComment[0].ReUser__c)){
                Write(list_assVisitComment[0] , 2);
            }else if(objEvent.BeReviewed3__c == map_ID_Name.get(list_assVisitComment[0].BeReviewed__c) && objEvent.ReUser3__c==map_ID_Name.get(list_assVisitComment[0].ReUser__c)){
                Write(list_assVisitComment[0] , 3);
            }
            /*2013-12-12 commented by Tobe 
            else if(objEvent.BeReviewed4__c == map_ID_Name.get(list_assVisitComment[0].BeReviewed__c) && objEvent.ReUser4__c==map_ID_Name.get(list_assVisitComment[0].ReUser__c)){
                Write(list_assVisitComm[0] , 4);
            }else if(objEvent.BeReviewed5__c == map_ID_Name.get(list_assVisitComment[0].BeReviewed__c) && objEvent.ReUser5__c==map_ID_Name.get(list_assVisitComment[0].ReUser__c)){
                Write(list_assVisitComment[0] , 5);
            }*/
            else{
                return ;
            }
      }
    }
    private void Write(AssVisitComments__c avc ,Integer intNum){
    	system.debug('In write'+intNum+' - '+avc);
      objEvent.put('BeReviewed'+intNum+'__c' , map_ID_Name.get(avc.BeReviewed__c));
      objEvent.put('ReUser'+intNum+'__c' , map_ID_Name.get(avc.ReUser__c));
      objEvent.put('Grade'+intNum+'__c' , avc.Grade__c);
      objEvent.put('Comment'+intNum+'__c' , avc.Comment__c);
      objEvent.put('IsAssVisit'+intNum+'__c' , avc.IsAssVisit__c);
      update objEvent;
    }
    
    private boolean checkIsAssVisit(){
      //List<EventAttendee> listEventAtt= null;
        //listEventAtt = [select Id,Status,RespondedDate,EventId,AttendeeId from EventAttendee 
        //where AttendeeId=:UserId and EventId=:eventId];
        List<EventRelation> listEventRe = [Select EventId,Status From EventRelation Where RelationId =: UserId And EventId =: eventId] ;
        if(listEventRe!=null&&listEventRe.size() > 0){
          if(listEventRe[0].Status == 'Accepted'){
            return true;
          }
        }
        return false;
    }
    /*
    private void setAssVisit(){
      List<EventAttendee> listEventAtt= null;
        listEventAtt = [select Id,Status,RespondedDate,EventId,AttendeeId from EventAttendee 
        where AttendeeId=:UserId and EventId=:eventId];
        if(listEventAtt!=null&&listEventAtt.size() > 0){
            if(listEventAtt[0].Status != 'Accepted'){
                //listEventAtt[0].Status='Accepted';
                //update listEventAtt[0];
            }
        }
    }
    */
    public void editComment(){
      Integer i = Integer.valueOf(strEditId);
      i=i-1;
      list_AssVisitCommWrapper[i].IsEdit=true;
      list_AssVisitCommWrapper[i].CanEdit=false;
      list_AssVisitCommWrapper[i].IsNoEdit=false;
      
    }
    public void cannelEdit(){
      getVisitComment();
      //return  new Pagereference('/apex/VisitComment');
    }
    
    public class VisitCommentWrapper{
      public String strBeRe{get;set;}
      public Boolean IsNew{get;set;}
      public Boolean IsEdit{get;set;}
      public Boolean IsNoEdit{get;set;}
      public Boolean CanEdit{get;set;}
      public AssVisitComments__c AssVisitComment{get;set;}
    }
    
    
    static testMethod void myUnitTest() {
         //user role
        List<UserRole> listur= new List<UserRole>();
        UserRole ur0 = new UserRole();
        ur0.Name = 'Renal-Supervisor-华南-PD-Supervisor(U0)';
        listur.add(ur0);
        UserRole ur1 = new UserRole();
        ur1.Name = 'Renal-Supervisor-华南-PD-Supervisor(U1)';
        listur.add(ur1);
        UserRole ur2 = new UserRole();
        ur2.Name = 'Renal-Rep-华南-PD-Rep(U2)';
        listur.add(ur2);
        insert listur;
        ur2.ParentRoleId=ur1.Id;
        update ur2;
        ur1.ParentRoleId = ur0.id;
        update ur1;
        //user 
        list<User> user=[Select TimeZoneSidKey,ProfileId,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname, Alias From User Where IsActive = true limit 1];
        List<User> list_user = new List<User>();
        User user0=new User();
        user0.username='user0r2@023.com';
        user0.LastName='user0r2';
        user0.Email='user0r2@023.com';
        user0.Alias=user[0].Alias;
        user0.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user0.ProfileId=UserInfo.getProfileId();
        user0.LocaleSidKey=user[0].LocaleSidKey;
        user0.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user0.EmailEncodingKey=user[0].EmailEncodingKey;
        user0.CommunityNickname='chequ23';
        user0.MobilePhone='02345678902';
        user0.IsActive = true;
        user0.UserRoleId=ur0.Id ;
        list_user.add(user0);
        User user1=new User();
        user1.username='user1r2@123.com';
        user1.LastName='user1r2';
        user1.Email='user1r2@123.com';
        user1.Alias=user[0].Alias;
        user1.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user1.ProfileId=UserInfo.getProfileId();
        user1.LocaleSidKey=user[0].LocaleSidKey;
        user1.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user1.EmailEncodingKey=user[0].EmailEncodingKey;
        user1.CommunityNickname='chequ';
        user1.MobilePhone='12345678912';
        user1.IsActive = true;
        user1.UserRoleId=ur1.Id ;
        list_user.add(user1);
        User user2=new User();
        user2.username='user2r2@223.com';
        user2.LastName='user2r2';
        user2.Email='user2r2@223.com';
        user2.Alias=user[0].Alias;
        user2.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user2.ProfileId=UserInfo.getProfileId();
        user2.LocaleSidKey=user[0].LocaleSidKey;
        user2.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user2.EmailEncodingKey=user[0].EmailEncodingKey;
        user2.CommunityNickname='chequ1';
        user2.MobilePhone='22345678922';
        user2.IsActive = true;
        user2.UserRoleId=ur2.Id ;
        list_user.add(user2);
        insert list_user;
        //event
        RecordType rt =[Select Id From RecordType Where DeveloperName = 'V2_Event' And SobjectType = 'Event'];
        Event objEvent = new Event();
        objEvent.OwnerId = user2.Id;
        objEvent.StartDateTime = datetime.now();
        objEvent.EndDateTime = datetime.now().addHours(1);
        objEvent.RecordTypeId = rt.Id;
        objEvent.Subject = '拜访';
        insert objEvent ;
        
        AssVisitComments__c avc = new AssVisitComments__c();
        avc.EventId__c = objEvent.Id;
        avc.BeReviewed__c = user2.Id;
        avc.ReUser__c = user1.Id;
        avc.Comment__c = 'sss';
        avc.Grade__c='1';
        avc.IsAssVisit__c = true;
        insert avc ;
        
        system.test.startTest();
        BQ_CtrlVisitComments vc = new BQ_CtrlVisitComments(new Apexpages.Standardcontroller(objEvent));
        vc.getEventAttendees() ;
        vc.addComment();
        vc.strEditId = '1' ;
        vc.list_AssVisitCommWrapper[0].AssVisitComment.BeReviewed__c = userinfo.getUserId();
        vc.list_AssVisitCommWrapper[0].AssVisitComment.ReUser__c = userinfo.getUserId();
        /*****************bill 添加 2013/4/26*******************/
        vc.list_AssVisitCommWrapper[0].AssVisitComment.IsAssVisit__c = true;
        /*****************bill 添加 2013/4/26*******************/
        vc.CommentSave();
        
        vc.editComment();
        vc.CommentSave();
        vc.cannelEdit();
        system.test.stopTest();
    }
}