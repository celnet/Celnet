/**
 * Author : bill
 * date：2013-10-26
 * description：调整医院与经销商的关系，实现批量调整
**/
public class PM_HDRelationController 
{
    //客户
    public PM_HosDealerRelation__c HosDea{get;set;}
    //psr销售医院关系
    public List<PM_HosDealerRelation__c> list_hd{get;set;}
    //大区
    public string BigArea{get;set;}
    //省份
    public string Province{get;set;}
    //城市
    public string City{get;set;}
    //插管医院
    public string InHospital{get;set;}
    //继续保存
    public boolean IsContinued{get;set;}
    //已存在关系经销商的医院
    public boolean IsHDView{get;set;}
    public boolean IsMsgView{get;set;}
    
    //医院经销商关系
    public List<HDWrapper> list_AllHD = new List<HDWrapper>();
    public List<HDWrapper> list_pageHD
    {
        get
        {
            list_pageHD = new list<HDWrapper>();
            for(PM_HosDealerRelation__c hd : (list<PM_HosDealerRelation__c>)conset.getRecords())
            {
                HDWrapper wra = new HDWrapper();
                wra.hd = hd;
                wra.IsChecked = false;
                list_pageHD.add(wra);
            }
            list_AllHD.addAll(list_pageHD);
            result = list_pageHD.size();
            return list_pageHD; 
        }
        set;
    }
    
    public PM_HDRelationController(Apexpages.Standardcontroller controller)
    {
        HosDea = new PM_HosDealerRelation__c();
        IsHDView = false;
        IsContinued = false;
        IsMsgView = false;
    }
    
    //初始页面显示列表的查询条件
    //分页字段 
    public integer result{get;set;}//结果个数
    public Boolean hasPrevious {get {return conset.getHasPrevious();}set;}
    public Boolean hasNext {get {return conset.getHasNext();}set;}
    public Integer pageNumber {get {return conset.getPageNumber();}set;}
    public Integer categoryNumber {get {return conset.getResultSize();}set;}
    public ApexPages.StandardSetController conset //实现分页功能 ApexPages.StandardSetController
    {
        get 
        {
            if(conset == null)
            {
                getHospitalRelation(false);
                conset = new ApexPages.StandardSetController(list_hd);
                conset.setPageSize(10);
            }
            return conset;
        }
        set;
    }
    //分页方法
    public void first() {conset.first();}
    public void last() {conset.last();}
    public void previous() {conset.previous();}
    public void next() {conset.next();}
    
    //获取医院关系
    private void getHospitalRelation(boolean IsWithOutHd)
    {
        if(list_hd != null)list_hd.clear();
        ID HDId = HosDea.PM_Distributor__c;
        system.debug(HDId + '@@@@@@@@@@@@@@@@');
        string query = 'Select p.PM_Distributor__c, p.PM_UneffectDate__c, p.PM_Status__c, PM_EffectDate__c, p.PM_Distributor__r.Name, p.PM_NewDistributor__c, PM_Hospital__r.Name From PM_HosDealerRelation__c p WHERE PM_UneffectDate__c = null AND PM_Status__c = \'启用\' ';
        if(IsWithOutHd)
        {
            if(set_Hos == null || set_Hos.size()<=0){query += ' AND PM_Distributor__c = : HDId';}
			else if(HDId == null){query += ' AND PM_Hospital__c IN : set_Hos';}
			else {query += ' AND PM_Hospital__c IN : set_Hos AND PM_Distributor__c = : HDId';}
        }else{
            query += ' AND PM_Hospital__c IN : set_Hos';
        }
        list_hd = Database.query(query);
    }
    
    //保存医院和经销商关系
    //新经销商要同步到病人对象的经销商字段
    //调整医院与经销商的关系
    //要添加医院的Id集合
    private Set<ID> set_Hos = new Set<ID>();
    //要调换的对应关系经销商的医院
    private Set<ID> set_Rehos = new Set<ID>();
    //医院和经销商的map对应关系
    private Map<ID,ID> map_Relation = new Map<ID,ID>();
    
    public void Save()
    {
        List<Account> list_Account = [Select Id, RecordType.DeveloperName from Account where Id = : HosDea.PM_NewDistributor__c and RecordType.DeveloperName = 'RecordType_d_2'];
        system.debug(list_Account+'zgxm');
        //经销商的记录类型的developername是‘RecordType_d_2’
        if(list_Account == null || list_Account.size()<=0)
        {
            HosDea.PM_NewDistributor__c.addError('请选择正确的经销商');
            return;
        }
        Set<ID> set_HosChecked = new Set<ID>();
        set_HosChecked.clear();
        for(HDWrapper hd : list_AllHD)
        {
            if(hd.IsChecked)
            {
                set_HosChecked.add(hd.hd.PM_Hospital__c);
                hd.IsChecked = false;
            }
        }
        if(set_Hos != null)set_Hos.clear();
        if(set_HosChecked.size()<=0)
        {
            for(PM_HosDealerRelation__c hos : list_hd)
            {
                set_Hos.add(hos.PM_Hospital__c);
            }
        }
        else
        {
            set_Hos.addAll(set_HosChecked);
        }
        getHospitalRelation(false);
        If(list_hd != null && list_hd.size()>0)
        {
            string strMsg = '选取的医院已存在经销商关系，是否确认覆盖，详情如下';
            set_Rehos.clear();
            for(PM_HosDealerRelation__c hos : list_hd)
            {
                set_Rehos.add(hos.PM_Hospital__c);
                map_Relation.put(hos.PM_Hospital__c, hos.Id);
            } 
            IsContinued = true;
            IsHDView = true;
            IsMsgView = true;
            this.conset = new ApexPages.StandardSetController(list_hd);
			this.conset.setPageSize(10);
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING , strMsg);            
            ApexPages.addMessage(msg);
            return;
        }
        else
        {
            ContinueSave();
            return;
        }
    }
    
    //继续保存
    //不存在经销商关系的医院
    private Set<ID> set_Newhos = new Set<ID>();
    public void ContinueSave()
    {
    	list_hd.clear();
        //替换医院经销商关系
        List<PM_HosDealerRelation__c> list_ReHD = new List<PM_HosDealerRelation__c>();
        //新建医院经销商关系
        List<PM_HosDealerRelation__c> list_NewHD = new List<PM_HosDealerRelation__c>();
        //需要同步PSR和病人的所有人
        Set<ID> set_SyncAcc = new Set<ID>();
        set_Newhos.clear();
        for(ID Id : set_Hos)
        {
            if(!set_Rehos.contains(Id))
            {
                set_Newhos.add(Id);
            }
        }
        
        //清除医院那条空白的医院与经销商关系
        delete [Select Id From PM_HosDealerRelation__c p 
                        WHERE PM_Distributor__c = null and PM_UneffectDate__c = null and PM_EffectDate__c = null 
                        and PM_NewDistributor__c = null and PM_Status__c = null and PM_Hospital__c IN : set_Hos];
        
        //插入医院经销商关系
        for(ID accId : set_Newhos)
        {
            PM_HosDealerRelation__c NewHD = new PM_HosDealerRelation__c();
            NewHD.PM_Hospital__c = accId;
            NewHD.PM_Distributor__c = HosDea.PM_NewDistributor__c;
            NewHD.PM_Status__c = '启用';
            NewHD.PM_EffectDate__c = datetime.now();
            list_NewHD.add(NewHD);
            set_SyncAcc.add(accId);
        }
        
        //替换医院经销商关系
        for(ID accId : set_Rehos)
        {
            PM_HosDealerRelation__c NewHD = new PM_HosDealerRelation__c();
            NewHD.PM_Hospital__c = accId;
            NewHD.PM_Distributor__c = HosDea.PM_NewDistributor__c;
            NewHD.PM_Status__c = '启用';
            NewHD.PM_EffectDate__c = datetime.now();
            list_NewHD.add(NewHD);
            PM_HosDealerRelation__c ReHD = new PM_HosDealerRelation__c();
            ReHD.Id = map_Relation.get(accId);
            ReHD.PM_NewDistributor__c = HosDea.PM_NewDistributor__c;
            ReHD.PM_Status__c = '禁用';
            ReHD.PM_UneffectDate__c = datetime.now();
            ReHD.PM_IsPending__c = true;
            if(map_Relation.containsKey(accId))
            {
                list_ReHD.add(ReHD);
            }
            set_SyncAcc.add(accId);
        }
        try{
            if(list_NewHD != null && list_NewHD.size()>0)
            {
            	insert list_NewHD;
            	list_NewHD.clear();
            }
            if(list_ReHD != null && list_ReHD.size()>0)
            {
            	update list_ReHD;
            	list_ReHD.clear();
            }
            if(!set_SyncAcc.isEmpty())
            {
                PM_PSRRelationSyncOwner relation = new PM_PSRRelationSyncOwner();
                relation.set_Hospital = set_SyncAcc;
                relation.NewAcc_Id = HosDea.PM_NewDistributor__c;
                relation.IsHosDealerSync = true;
                database.executeBatch(relation,1);
            }
            IsContinued = false;
            IsHDView = false;
            Set<ID> newIds = new Set<ID>();
            if(newIds != null)newIds.clear();
            for(PM_HosDealerRelation__c hd : list_NewHD)
            {
                newIds.add(hd.Id);
            }
            this.conset = new ApexPages.StandardSetController(list_hd);
            this.conset.setPageSize(10);
            IsMsgView = true;
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, '调整医院成功');            
            ApexPages.addMessage(msg);
        }catch(Exception e)
        {
            IsMsgView = true;
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.error, '调整医院关系失败');            
            ApexPages.addMessage(msg);
        }
    }
    
    //获取页面选择的医院，若是医院没有选择，就以城市为准，若是城市未选中，就以省份为准
    private Boolean CheckHospitalSelected()
    {
        List<string> strHos = new List<string>();
        set<ID> set_Ids = new set<ID>();
        set_Ids.clear();
        if(InHospital != null && InHospital != '')
        {
            if(InHospital.contains(','))
            {
                strHos.addAll(InHospital.split(', '));
            }
            else
            {
                strHos.add(InHospital);
            }
            for(Account acc : [Select Id from Account where RecordType.DeveloperName IN ('RecordType','RecordType_d_2') and Name in : strHos])
            {
                set_Ids.add(acc.Id);
            }    
        }
        else if(City != null && City != '')
    	{
    		if(City.contains(','))
    		{
    			strHos.addAll(City.split(', '));
    		}
    		else
    		{
    			strHos.add(City);
    		}
    		for(Account acc : [Select Id from Account where RecordType.DeveloperName IN ('RecordType','RecordType_d_2') and Cities__r.Name in : strHos])
    		{
    			set_Ids.add(acc.Id);
    		} 
    	}
        if(set_Ids != null && set_Ids.size()>0)
        {
        	if(set_Hos!=null)set_Hos.clear();
        	set_Hos.addAll(set_Ids);
        }else{
        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, '请选择需要调整的终端配送商');            
            ApexPages.addMessage(msg);
            return false;
        }
        return true;
    }
    
    //查询按钮相应
    public void Check()
    {
        IsMsgView = false;
        list_AllHD.clear();
        if(list_hd != null)list_hd.clear();
        if(set_Hos != null)set_Hos.clear();
        CheckHospitalSelected();
        getHospitalRelation(true);
        this.conset = new ApexPages.StandardSetController(list_hd);
        this.conset.setPageSize(10);
    }
    
    //查询没有经销商的医院
    public void CheckWithOutDealer()
    {
    	list_AllHD.clear();
        IsMsgView = false;
        if(list_hd != null)list_hd.clear();
        if(set_Hos != null)set_Hos.clear();
        if(CheckHospitalSelected())
        {
            getHospitalRelation(false);
            for(PM_HosDealerRelation__c hd:list_hd)
            {
                set_Hos.remove(hd.PM_Hospital__c);
            }
            list_hd = [Select p.Id, p.PM_Distributor__c, p.PM_Status__c, p.PM_EffectDate__c, p.PM_Hospital__c, PM_Hospital__r.Name From PM_HosDealerRelation__c p 
                        WHERE PM_Distributor__c = null and PM_UneffectDate__c = null and PM_EffectDate__c = null 
                        and PM_NewDistributor__c = null and PM_Status__c = null and PM_Hospital__c IN : set_Hos];
            for(PM_HosDealerRelation__c nullPSR : list_hd)
            {
                set_Hos.remove(nullPSR.PM_Hospital__c);
            }
            List<PM_HosDealerRelation__c> list_nullHD = new List<PM_HosDealerRelation__c>();
            for(ID hosId : set_Hos)
            {
                PM_HosDealerRelation__c NUllhd = new PM_HosDealerRelation__c();
                NUllhd.PM_Hospital__c = hosId;
                list_nullHD.add(NUllhd);
            }
            if(list_nullHD != null && list_nullHD.size() > 0)insert list_nullHD;
            list_nullHD.clear();
            list_hd.addAll([Select p.PM_Distributor__c, p.PM_Status__c, p.PM_EffectDate__c, p.PM_Hospital__c, PM_Hospital__r.Name From PM_HosDealerRelation__c p 
                        WHERE PM_Distributor__c = null and PM_UneffectDate__c = null and PM_EffectDate__c = null 
                        and PM_NewDistributor__c = null and PM_Status__c = null and PM_Hospital__c IN : set_Hos]);
        }
        this.conset = new ApexPages.StandardSetController(list_hd);
        this.conset.setPageSize(10);
    }
    
    //医院关系内部类
    public class HDWrapper
    {
        public PM_HosDealerRelation__c hd{get;set;}
        public boolean IsChecked{get;set;}
    }
}