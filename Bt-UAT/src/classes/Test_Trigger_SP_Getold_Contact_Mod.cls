/*
Author：Dean Lv
Created on：2013-09-10
Description: 
trigger测试
被测试trigger名:SP_Getold_Contact_Mod
改变七氟烷实际销量（瓶）、七氟烷潜力销量（瓶）、七氟烷客户等级等三个字段。
测试当三个字段中任意有一个值被修改后，记录每一次修改前的值以及修改后的数值。
*/
@isTest
private class Test_Trigger_SP_Getold_Contact_Mod {
    static testMethod void myUnitTest() {
	ID TeId = [Select r.Id, r.DeveloperName From RecordType r 
    	where SobjectType = 'Contact' and IsActive = true and DeveloperName = 'SP'][0].Id;
      //医院客户Account
      List<Account> list_Acc = new List<Account>();
      Account acc1 = new Account();
      acc1.Name = '第一医院';
      list_Acc.Add(acc1);
      Account acc2 = new Account();
      acc2.Name = '第二医院';
      list_Acc.Add(acc2);
      insert list_Acc;
      //联系人     
      List<Contact> list_Con = new List<Contact>();
      Contact con1 = new Contact();
      con1.AccountId = acc1.Id;
      con1.LastName = '王先生';
      con1.Phone = '13212344321';
      con1.Gender__c = '男';
      con1.ContactType__c = '医生';
      con1.RecordTypeId = TeId;      
      con1.SP_Department__c = '骨科';
      con1.SP_SevoQtySale__c = 10;
      con1.SP_SevoAccLevel__c = 'C';
      con1.SP_SevoPotentialSale__c = 10;
      list_Con.Add(con1);   
      Contact con2=new Contact();
      con2.AccountId = acc2.Id;
      con2.LastName = '刘小姐';
      con2.Phone = '18689890912';
      con2.Gender__c = '女';
      con2.ContactType__c = '行政';
      con2.RecordTypeId = TeId;
      con2.SP_Department__c = '儿科';
      con2.SP_SevoQtySale__c = 30;
      con2.SP_SevoAccLevel__c = 'A';
      con2.SP_SevoPotentialSale__c = 90;
      list_Con.Add(con2);    
      Contact con3=new Contact();
      con3.AccountId = acc2.Id;
      con3.LastName = '赵女士';
      con3.Phone = '18912340987';
      con3.Gender__c = '女';
      con3.ContactType__c = '护士';
      con3.RecordTypeId = TeId;
      con3.SP_Department__c = '妇科';
      con3.SP_SevoQtySale__c = 20;
      con3.SP_SevoAccLevel__c = 'B';
      con3.SP_SevoPotentialSale__c = 30;
      con3.SP_DesQtySale__c = 30;
      con3.SP_DesAccLevel__c = 'B';
      con3.SP_DesPotentialSale__c = 90;
      list_Con.Add(con3);
      system.Test.startTest();
      //插入新建联系人
      insert list_Con;
      //并更联系人信息
      con1.SP_SevoQtySale__c = 10;
      con1.SP_SevoAccLevel__c = 'A';
      con1.SP_SevoPotentialSale__c = 90;
      con1.SP_DesQtySale__c = 10;
      con1.SP_DesAccLevel__c = 'A';
      con1.SP_DesPotentialSale__c = 90;
      update con1;
      con2.SP_SevoQtySale__c = 30;
      con2.SP_SevoAccLevel__c = 'B';
      con2.SP_SevoPotentialSale__c = 90;
      con2.SP_DesQtySale__c = 30;
      con2.SP_DesAccLevel__c = 'B';
      con2.SP_DesPotentialSale__c = 90;
      update con2;
      con3.SP_SevoQtySale__c = 30;
      con3.SP_SevoAccLevel__c = 'A';
      con3.SP_SevoPotentialSale__c = 70;
      con3.SP_DesQtySale__c = 20;
      con3.SP_DesAccLevel__c = 'A';
      con3.SP_DesPotentialSale__c = 40; 
      update con3;    
      system.Test.stopTest();      
    }
}