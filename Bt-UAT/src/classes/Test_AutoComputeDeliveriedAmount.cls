/**
 * author: bill
 * AutoComputeDeliveriedAmount的trigger的测试类
 */
@isTest
private class Test_AutoComputeDeliveriedAmount {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        //挥发罐 
        VaporizerInfo__c vap = new VaporizerInfo__c();
        vap.location__c = '上海仓库';
        vap.Status__c = '库存';
        insert vap;
        VaporizerInfo__c vap1 = new VaporizerInfo__c();
        vap1.location__c = '上海仓库';
        vap1.Status__c = '库存';
        insert vap1;
        VaporizerInfo__c vap2 = new VaporizerInfo__c();
        vap2.location__c = '上海仓库';
        vap2.Status__c = '库存';
        insert vap2;
        
        //挥发罐申请
        Vaporizer_Application__c vapApp = new Vaporizer_Application__c();
        insert vapApp;
        
        //挥发罐维修/退回申请
        Vaporizer_ReturnAndMainten__c vapAplyReturn = new Vaporizer_ReturnAndMainten__c();
        insert vapAplyReturn;
        
        //申请明细
        Vaporizer_Apply_Detail__c vapApply = new Vaporizer_Apply_Detail__c();
        vapApply.Vaporizer_Application__c = vapApp.Id;
        vapApply.DeliveredAmount__c = 2;
        insert vapApply;
        
        //挥发罐维修/退回申请明细
        Vaporizer_ReturnAndMainten_Detail__c vapAplyReturnDetail = new Vaporizer_ReturnAndMainten_Detail__c();
        vapAplyReturnDetail.Vaporizer_ReturnAndMainten__c = vapAplyReturn.Id;
        vapAplyReturnDetail.DeliveredAmount__c = 3;
        try{
        insert vapAplyReturnDetail;
        }catch(Exception e){system.debug('@@@@@@@@@@@@@@@@@@@'+e);}
         
        //发货明细
        Vaporizer_Application_Detail__c vapSent = new Vaporizer_Application_Detail__c();
        vapSent.Vaporizer_Apply_Detail__c = vapApply.Id;
        vapSent.Vaporizer_Application__c = vapApp.Id;
        vapSent.VaporizerInfo__c = vap.Id;
        insert vapSent;
        Vaporizer_Application_Detail__c vapSent1 = new Vaporizer_Application_Detail__c();
		vapSent1.Vaporizer_ReturnAndMainten__c = vapAplyReturn.Id; 
        vapSent1.Vaporizer_ReturnAndMainten_Detail__c = vapAplyReturnDetail.Id;
        vapSent1.VaporizerInfo__c = vap1.Id;
        insert vapSent1;
        Vaporizer_Application_Detail__c vapSent2 = new Vaporizer_Application_Detail__c();
        vapSent2.Vaporizer_Apply_Detail__c = vapApply.Id;
        vapSent2.Vaporizer_Application__c = vapApp.Id;
        vapSent2.VaporizerInfo__c = vap2.Id;
        insert vapSent2;
        
        test.startTest();
        delete vapSent2;
        
        vapSent.IsDelivered__c = true;
        vapSent1.IsDelivered__c = true;
        try{
        	update vapSent;
        }catch(Exception e){}
        
        try{
        	update vapSent1;
        }catch(Exception e){}
        
        try{
        	delete vapSent;
        }catch(Exception e){}
        vapSent.IsReceived__c = true;
        vapSent1.IsReceived__c = true;
        update vapSent;
        update vapSent1;
       
        test.stopTest();
    }
}