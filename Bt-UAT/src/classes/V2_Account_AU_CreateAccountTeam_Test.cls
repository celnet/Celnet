/* 
 * Author: Sunny
 * Created on: 2011-12-26
 * Description: 
 * 将Account的修改同步到Lead上
 */
@isTest
private class V2_Account_AU_CreateAccountTeam_Test {

    static testMethod void TestSync() {
        //----------------New UserRole ------------------
        //List<UserRole> list_userRole = new List<UserRole >() ;
        UserRole objUserRole = new UserRole() ;
        objUserRole.Name = 'Renal-Rep-大上海-PD-Rep(陈喆令)' ;
        insert objUserRole ;
        UserRole objUserRole2 = new UserRole() ;
        objUserRole2.Name = 'Renal-Rep-大上海-PB-Rep(陈喆令)' ;
        objUserRole2.ParentRoleId = objUserRole.Id ;
        insert objUserRole2 ;
        //----------------Create User-------------
        List<User> user = [select id,ProfileId,Alias,TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey from User where id =: UserInfo.getUserId()] ;
        List<User> list_User = new List<User>() ;
        User use1=new User();
        use1.Username='user2@123.com';
        use1.LastName='user2';
        use1.Email='user2@123.com';
        use1.Alias=user[0].Alias;
        use1.TimeZoneSidKey=user[0].TimeZoneSidKey;
        use1.ProfileId=user[0].ProfileId;
        use1.LocaleSidKey=user[0].LocaleSidKey;
        use1.LanguageLocaleKey=user[0].LanguageLocaleKey;
        use1.EmailEncodingKey=user[0].EmailEncodingKey;
        use1.CommunityNickname='chequ1';
        use1.MobilePhone='12345678912';
        use1.UserRoleId = objUserRole2.Id ;
        use1.IsActive = true;
        list_User.add(use1) ;
        User use2=new User();
        use2.Username='user2@223.com';
        use2.LastName='user2';
        use2.Email='user2@223.com';
        use2.Alias=user[0].Alias;
        use2.TimeZoneSidKey=user[0].TimeZoneSidKey;
        use2.ProfileId=user[0].ProfileId;
        use2.LocaleSidKey=user[0].LocaleSidKey;
        use2.LanguageLocaleKey=user[0].LanguageLocaleKey;
        use2.EmailEncodingKey=user[0].EmailEncodingKey;
        use2.CommunityNickname='chequ2';
        use2.UserRoleId = objUserRole2.Id ;
        use2.MobilePhone='22345678922';
        use2.IsActive = true;
        list_User.add(use2) ;
        User use3=new User();
        use3.Username='user2@323.com';
        use3.LastName='user2';
        use3.Email='user2@323.com';
        use3.Alias=user[0].Alias;
        use3.TimeZoneSidKey=user[0].TimeZoneSidKey;
        use3.ProfileId=user[0].ProfileId;
        use3.LocaleSidKey=user[0].LocaleSidKey;
        use3.LanguageLocaleKey=user[0].LanguageLocaleKey;
        use3.EmailEncodingKey=user[0].EmailEncodingKey;
        use3.CommunityNickname='chequ3';
        use3.MobilePhone='32345678932';
        use3.UserRoleId = objUserRole.Id ;
        use3.IsActive = true;
        list_User.add(use3) ;
        insert list_User ;
        //------------New Lead-----------------
        List<Lead> list_lead = new List<Lead>() ;
        Lead objLead1 = new Lead() ;
        objLead1.LastName = 'Names1' ;
        objLead1.Company = 'names1' ;
        objLead1.V2_Mid__c = 'minds1' ;
        list_lead.add(objLead1) ;
        Lead objLead2 = new Lead() ;
        objLead2.LastName = 'Names2' ;
        objLead2.Company = 'names2' ;
        objLead2.V2_Mid__c = 'minds2' ;
        list_lead.add(objLead2) ;
        Lead objLead3 = new Lead() ;
        objLead3.LastName = 'Names3' ;
        objLead3.Company = 'names3' ;
        objLead3.V2_Mid__c = 'minds3' ;
        list_lead.add(objLead3) ;
        insert list_lead ;
        system.debug('S D :'+list_lead) ;
        List<String> list_s = new List<String>() ;
        list_s.add('minds3') ;
        list_s.add('minds2') ;
        list_s.add('minds1') ;
        for(Lead objl : [select id,V2_Mid__c from Lead where V2_Mid__c in: list_s])
        {
        	system.debug('Sunny debug:'+objl) ;
        }
        //------------New Account-----------------
        List<Account> list_Account = new List<Account>() ;
        Account objAccount1 = new Account() ;
        objAccount1.Name = 'Names1' ;
        //objAccount1.V2_ApprovalStauts__c = '待审批' ;
        objAccount1.MID__c = 'minds1' ;
        list_Account.add(objAccount1) ;
        Account objAccount2 = new Account() ;
        objAccount2.Name = 'Names2' ;
        //objAccount2.V2_ApprovalStauts__c = '待审批' ;
        objAccount2.MID__c = 'minds2' ;
        list_Account.add(objAccount2) ;
        Account objAccount3 = new Account() ;
        objAccount3.Name = 'Names3' ;
        //objAccount3.V2_ApprovalStauts__c = '待审批' ;
        objAccount3.MID__c = 'minds3' ;
        list_Account.add(objAccount3) ;
        insert list_Account ;
        //-----------------New Account Team--------------------------
        List<V2_Account_Team__c> list_AccountTeam = new List<V2_Account_Team__c>() ;
        V2_Account_Team__c objAccTeam1 = new V2_Account_Team__c() ;
        objAccTeam1.V2_User__c = use1.Id ;
        objAccTeam1.V2_Account__c = objAccount1.Id ;
        list_AccountTeam.add(objAccTeam1) ;
        V2_Account_Team__c objAccTeam2 = new V2_Account_Team__c() ;
        objAccTeam2.V2_User__c = use2.Id ;
        objAccTeam2.V2_Account__c = objAccount2.Id ;
        list_AccountTeam.add(objAccTeam2) ;
        V2_Account_Team__c objAccTeam3 = new V2_Account_Team__c() ;
        objAccTeam3.V2_User__c = use1.Id ;
        objAccTeam3.V2_Account__c = objAccount3.Id ;
        list_AccountTeam.add(objAccTeam3) ;
        V2_Account_Team__c objAccTeam4 = new V2_Account_Team__c() ;
        objAccTeam4.V2_User__c = use3.Id ;
        objAccTeam4.V2_Account__c = objAccount3.Id ;
        objAccTeam4.V2_ImmediateDelete__c = true ;
        objAccTeam4.V2_Is_Delete__c = true ;
        list_AccountTeam.add(objAccTeam4) ;
        insert list_AccountTeam ;
        //-------------New AccountTeamM---------------
        List<AccountTeamMember> list_AccTeamM = new List<AccountTeamMember>() ;
        AccountTeamMember objAccTeamM1 = New AccountTeamMember() ;
        objAccTeamM1.UserId = use3.Id ;
        objAccTeamM1.AccountId = objAccount3.Id ;
        list_AccTeamM.add(objAccTeamM1) ;
        insert list_AccTeamM ;
        //----------------Start Test------------------
        system.test.startTest() ;
        List<Account> list_Accs = new List<Account>() ;
        //objAccount1.V2_ApprovalStauts__c = '待审批' ;
        list_Accs.add(objAccount1) ;
        //objAccount2.V2_ApprovalStauts__c = '审批通过' ;
        list_Accs.add(objAccount2) ;
        //objAccount3.V2_ApprovalStauts__c = '审批通过' ;
        list_Accs.add(objAccount3) ;
        update list_Accs ;
        system.test.stopTest() ;
    }
}