/**
 * Author : bill
 * PM_SalesHospitalRelationBatch的测试类
 */
@isTest
public class PM_Test_SalesHospitalRelationBatch {
	 static testMethod void myUnitTest() {
        // TO DO: implement unit test
        // TO DO: implement unit test
        //user role
        List<UserRole> listur= new List<UserRole>();
        UserRole ur0 = new UserRole();
        ur0.Name = 'Renal-Supervisor-华南-PD-Supervisor(U0)';
        listur.add(ur0);
        UserRole ur1 = new UserRole();
        ur1.Name = 'Renal-Supervisor-华南-PD-Supervisor(U1)';
        listur.add(ur1);
        UserRole ur2 = new UserRole();
        ur2.Name = 'Renal-Rep-华南-PD-Rep(U2)';
        listur.add(ur2);
        insert listur;
        ur2.ParentRoleId=ur1.Id;
        update ur2;
        ur1.ParentRoleId = ur0.id;
        update ur1;
        //user 
        list<User> user=[Select TimeZoneSidKey,ProfileId,LocaleSidKey,LanguageLocaleKey,EmailEncodingKey,CommunityNickname, Alias From User Where IsActive = true limit 1];
        List<User> list_user = new List<User>();
        User user0=new User();
        user0.username='user0r2@023.com';
        user0.LastName='user0r2';
        user0.Email='user0r2@023.com';
        user0.Alias=user[0].Alias;
        user0.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user0.ProfileId=UserInfo.getProfileId();
        user0.LocaleSidKey=user[0].LocaleSidKey;
        user0.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user0.EmailEncodingKey=user[0].EmailEncodingKey;
        user0.CommunityNickname='chequ23';
        user0.MobilePhone='02345678902';
        user0.IsActive = true;
        user0.UserRoleId=ur0.Id ;
        user0.Alias='zhangsan';
        list_user.add(user0);
        User user1=new User();
        user1.username='user1r2@123.com';
        user1.LastName='user1r2';
        user1.Email='user1r2@123.com';
        user1.Alias=user[0].Alias;
        user1.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user1.ProfileId=UserInfo.getProfileId();
        user1.LocaleSidKey=user[0].LocaleSidKey;
        user1.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user1.EmailEncodingKey=user[0].EmailEncodingKey;
        user1.CommunityNickname='chequ';
        user1.MobilePhone='12345678912';
        user1.Alias='aaaa';
        user1.IsActive = true;
        user1.UserRoleId=ur0.Id ;
        list_user.add(user1);
        User user2=new User();
        user2.username='user2r2@223.com';
        user2.LastName='user2r2';
        user2.Email='user2r2@223.com';
        user2.Alias=user[0].Alias;
        user2.TimeZoneSidKey=user[0].TimeZoneSidKey;
        user2.ProfileId=UserInfo.getProfileId();
        user2.LocaleSidKey=user[0].LocaleSidKey;
        user2.LanguageLocaleKey=user[0].LanguageLocaleKey;
        user2.EmailEncodingKey=user[0].EmailEncodingKey;
        user2.CommunityNickname='chequ1';
        user2.MobilePhone='22345678922';
        user2.IsActive = true;
        user2.UserRoleId=ur1.Id ;
        user2.Alias ='bbb';
        list_user.add(user2);
        insert list_user;
        
        Account acc = new Account();
        acc.Name = '123';
        insert acc;
        Account acc1 = new Account();
        acc1.Name = '123';
        insert acc1;
        
		List<V2_Account_Team__c> list_team = new List<V2_Account_Team__c>();
		V2_Account_Team__c team1 = new V2_Account_Team__c();
		team1.V2_BatchOperate__c = '新增';
		team1.V2_Account__c = acc.Id;
		team1.V2_ApprovalStatus__c = '审批通过';
		team1.V2_Effective_Year__c = string.valueOf(date.today().year());
		team1.V2_Effective_Month__c = string.valueOf(date.today().month());
		team1.V2_User__c = user1.Id;
		team1.V2_History__c = false;
		list_team.add(team1);
		V2_Account_Team__c team2 = new V2_Account_Team__c();
		team2.V2_BatchOperate__c = '替换';
		team2.V2_Account__c = acc1.Id;
		team2.V2_ApprovalStatus__c = '审批通过';
		team2.V2_Effective_Year__c = string.valueOf(date.today().year());
		team2.V2_Effective_Month__c = string.valueOf(date.today().month());
		team2.V2_User__c = user2.Id;
		team2.V2_NewAccUser__c = user1.Id;
		team2.V2_History__c = false;
		list_team.add(team2);
		V2_Account_Team__c team3 = new V2_Account_Team__c();
		team3.V2_BatchOperate__c = '删除';
		team3.V2_Account__c = acc.Id;
		team3.V2_ApprovalStatus__c = '审批通过';
		team3.V2_Delete_Year__c = string.valueOf(date.today().year());
		team3.V2_Delete_Month__c = string.valueOf(date.today().month());
		team3.V2_User__c = user2.Id;
		team3.V2_History__c = false;
		list_team.add(team3);
		insert list_team;
		
        PM_Patient__c Patient1 = new PM_Patient__c();
    	Patient1.Name = '张先生';
		Patient1.PM_Status__c = 'New';
		Patient1.PM_NewPatientDate__c = Date.today().addMonths(-3);
		Patient1.PM_LastSuccessDateTime__c  = DateTime.now().addMonths(-2);
		Patient1.PM_VisitState__c = '失败';
		Patient1.PM_InDate__c = Date.today().addMonths(-5);
		Patient1.PM_TelPriorityFirst__c = '病人手机';
		Patient1.PM_TelPrioritySecond__c = '家属电话';
		Patient1.PM_InHospital__c = acc.Id;
		Patient1.PM_Treat_Hospital__c = acc.Id; 
		insert Patient1;
		
        system.Test.startTest();
        PM_SalesHospitalRelationBatch accountBatch = new PM_SalesHospitalRelationBatch();
        Database.executeBatch(accountBatch, 10);
        system.Test.stopTest();
	 }
}