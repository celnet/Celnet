/**
 * author : bill
 * IsVapAttributeHospital的trigger的测试类
 */
@isTest
private class Test_IsVapAttributeHospital {

    static testMethod void myUnitTest() {
    	//account
        Account acc = new Account();
        acc.Name = 'klm';
        insert acc;
        Account acc1 = new Account();
        acc1.Name = 'zgxm';
        insert acc1;
        
        //挥发罐
        VaporizerInfo__c vap = new VaporizerInfo__c();
        vap.Name = 'NM008990336';
        vap.location__c = '医院';
        vap.Status__c = '使用';
        vap.Hospital__c = acc.Id;
        insert vap;
        
        //维修退回申请
        Vaporizer_ReturnAndMainten__c vapReturn = new Vaporizer_ReturnAndMainten__c();
        //vapReturn.Name = '20130522-0008';
        vapReturn.Approve_Result__c = '待审批';
        vapReturn.Hospital__c = acc1.Id;
        insert vapReturn;
        
        //维修退回明细
        Vaporizer_ReturnAndMainten_Detail__c vapReturnDetail = new Vaporizer_ReturnAndMainten_Detail__c();
        vapReturnDetail.Vaporizer_ReturnAndMainten__c = vapReturn.Id;
        vapReturnDetail.VaporizerInfo__c = vap.Id;
        
        test.startTest();
        system.debug(vap.Id + 'zgxm');
        try{
        insert vapReturnDetail;
        }catch(Exception e){system.debug(e+'@@@@@@@@@@@@@@');}
        test.stopTest();
        
    }
}