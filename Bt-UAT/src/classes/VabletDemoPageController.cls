public class VabletDemoPageController
{
    //public List<DemoPack>  demoDPList{get;set;}
    public List<Vablet_VisitDate__c> vvdList{get;set;}
    /*
    public class DemoPack
    {
        public String demoname{get;set;}
        public String demotype{get;set;}
        public String demotime{get;set;}
        public String demoduration{get;set;}
        public String demoisRead{get;set;}
    }
    */
    public VabletDemoPageController(ApexPages.standardController controller)
    {
        Id eId = controller.getId();
        //Id eId = [Select Id From Event Where Id =: controller.getId()].Id;
        //system.debug('Event id:'+eId);
        this.vvdList = new List<Vablet_VisitDate__c>();
        vvdList = [Select 
                    Vablet_FileName__c, Vablet_FileType__c, Vablet_Duration__c, Vablet_StartTime__c, Vablet_IsCompleted__c, Vablet_Folder__c 
                   From 
                    Vablet_VisitDate__c
                   Where 
                    Vablet_EventId__c =: eId];
        /*
        this.demoDPList = new List<DemoPack>();
        DemoPack dp1 = new DemoPack();
        DemoPack dp2 = new DemoPack();
        DemoPack dp3 = new DemoPack();
        DemoPack dp4 = new DemoPack();
        dp1.demoname = 'CKD和腹透基础知识加强学员版';
        dp1.demotype = 'MP3';
        dp1.demotime = '2014-1-23 下午10:00';
        dp1.demoduration = '8';
        dp1.demoisRead = '是';
        dp2.demoname = 'MMS-一体化治疗';
        dp2.demotype = 'DOC';
        dp2.demotime = '2014-1-23 下午10:09';
        dp2.demoduration = '9';
        dp2.demoisRead = '是';
        dp3.demoname = '克凌诺--氧化应激';
        dp3.demotype = 'MP4';
        dp3.demotime = '2014-1-23 下午10:22';
        dp3.demoduration = '12';
        dp3.demoisRead = '是';
        this.demoDPList.add(dp1);
        this.demoDPList.add(dp2);
        this.demoDPList.add(dp3);
        */
    }
}