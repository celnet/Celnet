/**
 * Author : Bill
 * Create Date : 2013-11-26
 * 查找终端配送商
**/
public without sharing class PM_SelectTerminalController 
{
	public PM_Soql__c acc{get;set;}
	//构造函数
	public PM_SelectTerminalController(ApexPages.StandardController controller)
    {
    	acc = new PM_Soql__c();
    	acc.Name = ' ';
    }
    
    //account
    public List<Account> list_Acc
    {
        get
        {
            list_Acc = new list<Account>();
            list_Acc = (list<Account>)conset.getRecords(); 
            result = list_Acc.size();
            return list_Acc;
        }
        set;
    }
    
    //初始页面显示列表的查询条件
    //分页字段 
    public integer result{get;set;}//结果个数
    public Boolean hasPrevious {get {return conset.getHasPrevious();}set;}
    public Boolean hasNext {get {return conset.getHasNext();}set;}
    public Integer pageNumber {get {return conset.getPageNumber();}set;}
    public Integer categoryNumber {get {return conset.getResultSize();}set;}
    public ApexPages.StandardSetController conset //实现分页功能 ApexPages.StandardSetController
    {
        get 
        {
            if(conset == null)
            {
                conset = new ApexPages.StandardSetController([Select Id,Name,PM_Province__r.Name,Cities__r.Name,RecordType.Name from Account limit 100]);
                conset.setPageSize(10);
            }
            return conset;
        }
        set;
    }
    //分页方法
    public void first() {conset.first();}
    public void last() {conset.last();}
    public void previous() {conset.previous();}
    public void next() {conset.next();}
    
    //根据医院名称去模糊查找医院
    public void Check()
    {
    	//system.debug(acc.Name+'&&&&&&&&&&&&');
    	conset = new ApexPages.StandardSetController(Database.Query('Select Id,Name,PM_Province__r.Name,Cities__r.Name,RecordType.Name from Account where Name like \'%'+acc.Name.trim()+'%\' limit 100'));
        conset.setPageSize(10);
    }
}