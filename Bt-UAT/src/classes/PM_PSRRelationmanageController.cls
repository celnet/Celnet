/**
 * Author : bill
 * date：2013-10-10
 * description：调整PSR和医院的关系，实现批量调整
 * 
**/
public class PM_PSRRelationmanageController {

	//客户
	public PM_PSRRelation__c PSR{get;set;}
	//psr销售医院关系
	public List<PM_PSRRelation__c> list_psr{get;set;}
	//大区
	public string BigArea{get;set;}
	//省份
	public string Province{get;set;}
	//城市
	public string City{get;set;}
	//插管医院
	public string InHospital{get;set;}
	//继续保存
	public boolean IsContinued{get;set;}
	//已存在负责PSR的医院
	public boolean IsPSRView{get;set;}
	//结束后按钮只显示关闭
	public boolean IsBtnView{get;set;}
	public boolean IsMsgView{get;set;}
	
    //psr销售医院关系
    public List<PSRWrapper> list_Allpsr = new List<PSRWrapper>();
	public List<PSRWrapper> list_pagePSR
	{
		get
		{
			list_pagePSR = new list<PSRWrapper>();
			for(PM_PSRRelation__c psr : (list<PM_PSRRelation__c>)conset.getRecords())
			{
				PSRWrapper wra = new PSRWrapper();
				wra.psr = psr;
				wra.IsChecked = false;
				list_pagePSR.add(wra);
			}
			list_Allpsr.addAll(list_pagePSR);
			result = list_pagePSR.size();
			return list_pagePSR;	
		}
		set;
	}
	
    public PM_PSRRelationmanageController(Apexpages.Standardcontroller controller)
    {
    	PSR = new PM_PSRRelation__c();
    	IsPSRView = false;
    	IsContinued = false;
    	IsBtnView = true;
    	IsMsgView = false;
    }
    
    //初始页面显示列表的查询条件
    //private static final string query = 'select Id From PM_PSRRelation__c Where Name = \'null\'';
    // 分页字段 
	public integer result{get;set;}//结果个数
	public Boolean hasPrevious {get {return conset.getHasPrevious();}set;}
	public Boolean hasNext {get {return conset.getHasNext();}set;}
	public Integer pageNumber {get {return conset.getPageNumber();}set;}
	public Integer categoryNumber {get {return conset.getResultSize();}set;}
    public ApexPages.StandardSetController conset //实现分页功能 ApexPages.StandardSetController
    {
		get 
		{
			if(conset == null)
			{
				getHospitalRelation(false);
				conset = new ApexPages.StandardSetController(list_psr);
				conset.setPageSize(10);
			}
			return conset;
		}
		set;
	}
    //分页方法
	public void first() {conset.first();}
	public void last() {conset.last();}
	public void previous() {conset.previous();}
	public void next() {conset.next();}
	
    //获取医院关系
    private void getHospitalRelation(boolean IsWithOutPSR)
    {
    	if(list_psr != null)list_psr.clear();
    	ID PSRId = PSR.PM_PSR__c;
    	string query = 'Select p.PM_PSR__c, PM_PDSaler__c, p.PM_UneffectDate__c, p.PM_Status__c, PM_EffectDate__c, p.PM_PSR__r.Alias, p.PM_NewPSR__c, p.PM_Hospital__c, PM_Hospital__r.Name From PM_PSRRelation__c p WHERE PM_UneffectDate__c = null AND PM_Status__c = \'启用\' ';
		if(IsWithOutPSR)
		{
			if(set_Hos == null || set_Hos.size()<=0){query += ' AND PM_PSR__c = : PSRId';}
			else if(PSRId == null){query += ' AND PM_Hospital__c IN : set_Hos';}
			else {query += ' AND PM_Hospital__c IN : set_Hos AND PM_PSR__c = : PSRId';}
		}else{
			query += ' AND PM_Hospital__c IN : set_Hos';
		}
		list_psr = Database.query(query);
		system.debug('###########zgxm'+list_psr.size());
    }
    
    //保存PSR销售医院关系
    //新负责PSR放到对应客户小组里面
    //原负责PSR从客户小组中去掉
    //调整医院关系的医院
    private Set<ID> set_Hos = new Set<ID>();
    //存在负责人的医院
    private Set<ID> set_Rehos = new Set<ID>();
    //存在负责人的医院关系的ID
    private Map<ID,ID> map_Relation = new Map<ID,ID>();
	
    public void Save()
    {
    	List<User> list_User = [Select Id from User where Id = : PSR.PM_NewPSR__c and IsActive = true];
    	if(list_User == null || list_User.size()<=0)
    	{
        	PSR.PM_NewPSR__c.addError('请选择启用状态的新负责人');
        	return;
    	}
    	Set<ID> set_HosChecked = new Set<ID>();
    	set_HosChecked.clear();
		for(PSRWrapper psr : list_Allpsr)
	 	{
	 		system.debug('测试****************'+psr.psr.PM_Hospital__c+'&&'+psr.IsChecked);
	 		if(psr.IsChecked)
	 		{
	 			set_HosChecked.add(psr.psr.PM_Hospital__c);
	 			psr.IsChecked = false;
	 		}
	 	}
	 	if(set_Hos != null)set_Hos.clear();
	    if(set_HosChecked.size()<=0)
	 	{
	 		for(PM_PSRRelation__c hos : list_psr)
			{
				set_Hos.add(hos.PM_Hospital__c);
			}
	 	}
	 	else
	 	{
	 		set_Hos.addAll(set_HosChecked);
	 	}
		getHospitalRelation(false);
		If(list_psr != null && list_psr.size()>0)
		{
			string strMsg = '选取的医院已经有PSR在负责，是否确认覆盖，详情如下';
			set_Rehos.clear();
			for(PM_PSRRelation__c hos : list_psr)
			{
				set_Rehos.add(hos.PM_Hospital__c);
				map_Relation.put(hos.PM_Hospital__c, hos.Id);
			} 
			IsContinued = true;
			IsPSRView = true;
			IsMsgView = true;
	    	this.conset = new ApexPages.StandardSetController(list_psr);
			this.conset.setPageSize(10);
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING , strMsg);            
        	ApexPages.addMessage(msg);
        	return;
		}
		else
		{
			ContinueSave();
			return;
		}
    }
    
    //继续保存
    //不存在负责人的医院
    private Set<ID> set_Newhos = new Set<ID>();
    public void ContinueSave()
    {
    	list_psr.clear();
    	//替换医院关系
    	List<PM_PSRRelation__c> list_RePsr = new List<PM_PSRRelation__c>();
    	//新建医院关系
    	List<PM_PSRRelation__c> list_NewPsr = new List<PM_PSRRelation__c>();
    	//添加客户小组成员
    	List<AccountTeamMember> list_AddTeam = new List<AccountTeamMember>();
    	//删除客户小组成员
    	Set<ID> set_Acc = new Set<ID>();
    	//需要同步PSR和病人的所有人
    	Set<ID> set_SyncAcc = new Set<ID>();
    	set_Newhos.clear();
    	for(ID Id : set_Hos)
    	{
    		if(!set_Rehos.contains(Id))
    		{
    			set_Newhos.add(Id);
    		}
    	}
    	//清除医院那条空白的PSR与医院关系
    	delete [Select p.PM_PSR__c, p.PM_PDSaler__c, p.PM_Status__c, p.PM_EffectDate__c, p.PM_Hospital__c, PM_Hospital__r.Name From PM_PSRRelation__c p 
						WHERE PM_PSR__c = null and PM_UneffectDate__c = null and PM_EffectDate__c = null 
						and PM_NewPSR__c = null and PM_Status__c = null and PM_Hospital__c IN : set_Hos];
		getSalerWithHos();
    	//插入新PSR关系
    	for(ID accId : set_Newhos)
		{
			PM_PSRRelation__c NewPsr = new PM_PSRRelation__c();
			NewPsr.PM_Hospital__c = accId;
			NewPsr.PM_PSR__c = PSR.PM_NewPSR__c;
			NewPsr.PM_Status__c = '启用';
			NewPsr.PM_EffectDate__c = datetime.now();
			NewPsr.PM_PDSaler__c = map_salerHos.get(accId);
			if(map_salerHos.containsKey(accId))NewPsr.PM_PDSaler__c = map_salerHos.get(accId);
			list_NewPsr.add(NewPsr);
			AccountTeamMember accTeam = new AccountTeamMember();
			accTeam.AccountId = accId;
			accTeam.UserId = PSR.PM_NewPSR__c;
			list_AddTeam.add(accTeam);
			set_SyncAcc.add(accId);
		}
		//替换PSR医院关系
		for(ID accId : set_Rehos)
		{
			PM_PSRRelation__c NewPsr = new PM_PSRRelation__c();
			NewPsr.PM_Hospital__c = accId;
			NewPsr.PM_PSR__c = PSR.PM_NewPSR__c;
			NewPsr.PM_PDSaler__c = map_salerHos.get(accId);
			NewPsr.PM_Status__c = '启用';
			NewPsr.PM_EffectDate__c = datetime.now();
			if(map_salerHos.containsKey(accId))NewPsr.PM_PDSaler__c = map_salerHos.get(accId);
			list_NewPsr.add(NewPsr);
			PM_PSRRelation__c RePsr = new PM_PSRRelation__c();
			RePsr.Id = map_Relation.get(accId);
			RePsr.PM_NewPSR__c = PSR.PM_NewPSR__c;
			RePsr.PM_Status__c = '禁用';
			RePsr.PM_UneffectDate__c = datetime.now();
			RePsr.PM_IsPending__c = true;
			if(map_Relation.containsKey(accId))
			{
				list_RePsr.add(RePsr);
			}
			AccountTeamMember accTeam = new AccountTeamMember();
			accTeam.AccountId = accId;
			accTeam.UserId = PSR.PM_NewPSR__c;
			list_AddTeam.add(accTeam);
			set_Acc.add(accId);
			set_SyncAcc.add(accId);
		}
		try{
			if(list_NewPsr != null && list_NewPsr.size()>0)
			{
				insert list_NewPsr;
				list_NewPsr.clear();
			}
			if(list_AddTeam != null && list_AddTeam.size()>0)
			{
				insert list_AddTeam;
				list_AddTeam.clear();
			}
			if(list_RePsr != null && list_RePsr.size()>0)
			{
				update list_RePsr;
				list_RePsr.clear();
			}
			if(!set_SyncAcc.isEmpty())
			{
				PM_PSRRelationSyncOwner relation = new PM_PSRRelationSyncOwner();
				relation.set_Hospital = set_SyncAcc;
				relation.NewPSR_Id = PSR.PM_NewPSR__c;
				relation.IsHosDealerSync = false;
        		database.executeBatch(relation,1);
			}
			IsContinued = false;
			IsPSRView = false;
			IsBtnView = true;
			Set<ID> newIds = new Set<ID>();
			if(newIds != null)newIds.clear();
			for(PM_PSRRelation__c pr : list_NewPsr)
			{
				newIds.add(pr.Id);
			}
			this.conset = new ApexPages.StandardSetController(list_psr);
    		this.conset.setPageSize(10);
    		IsMsgView = true;
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM , '调整关系成功，病人Owner更换需要时间，具体时间依据调整病人数量而定，最长时间为5分钟');            
        	ApexPages.addMessage(msg);
		}catch(Exception e)
		{
			IsMsgView = true;
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.error , '调整医院关系失败');            
        	ApexPages.addMessage(msg);
		}
    }
    
    //获取页面选择的医院，若是医院没有选择，就以城市为准，若是城市未选中，就以省份为准
    private Boolean CheckHospitalSelected()
    {
    	//医院ID，筛选病人数量不为空的医院
    	Set<ID> set_allHos = new Set<ID>();
    	set_allHos.clear();
    	List<string> strHos = new List<string>();
    	if(InHospital != null && InHospital != '')
    	{
    		if(InHospital.contains(','))
    		{
    			strHos.addAll(InHospital.split(', '));
    		}
    		else
    		{
    			strHos.add(InHospital);
    		}
    		for(Account acc : [Select Id from Account where RecordType.DeveloperName = 'RecordType' and Name in : strHos])
    		{
    			set_allHos.add(acc.Id);
    		}    	
    	}
    	else if(City != null && City != '')
    	{
    		if(City.contains(','))
    		{
    			strHos.addAll(City.split(', '));
    		}
    		else
    		{
    			strHos.add(City);
    		}
    		for(Account acc : [Select Id from Account where RecordType.DeveloperName = 'RecordType' and Cities__r.Name in : strHos])
    		{
    			set_allHos.add(acc.Id);
    		} 
    	}
    	else if(Province != null && Province != '')
    	{
    		if(Province.contains(','))
    		{
    			strHos.addAll(Province.split(', '));
    		}
    		else
    		{
    			strHos.add(Province);
    		}
    		for(Account acc : [Select Id from Account where RecordType.DeveloperName = 'RecordType' and PM_Province__r.Name in : strHos])
    		{
    			set_allHos.add(acc.Id);
    		} 
    	}
    	else if(BigArea != null && BigArea != '')
    	{
    		if(BigArea.contains(','))
    		{
    			strHos.addAll(BigArea.split(', '));
    		}
    		else
    		{
    			strHos.add(BigArea);
    		}
    		for(Account acc : [Select Id from Account where RecordType.DeveloperName = 'RecordType' and Region__r.Name in : strHos])
    		{
    			set_allHos.add(acc.Id);
    		} 
    	}
    	for(Account acc : [select Id,Name,(Select Id From PMMNib__r limit 1) from Account Where Id IN :set_allHos])
    	{
    		if(acc.PMMNib__r != null && acc.PMMNib__r.size()>0)
    		{
    			set_Hos.add(acc.Id);
    		}
    	}
    	return true;
    }
    
    //查询按钮相应
    public void Check()
    {
    	list_Allpsr.clear();
    	IsMsgView = false;
		if(list_psr != null)list_psr.clear();
		if(set_Hos != null)set_Hos.clear();
		CheckHospitalSelected();
		getHospitalRelation(true);
    	this.conset = new ApexPages.StandardSetController(list_psr);
    	this.conset.setPageSize(10);
    }
    
    //查询没有PSR负责的医院
    public void CheckWithOutPSR()
    {
    	list_Allpsr.clear();
    	IsMsgView = false;
		if(list_psr != null)list_psr.clear();
		if(set_Hos != null)set_Hos.clear();
    	if(CheckHospitalSelected())
    	{
    		getHospitalRelation(false);
			for(PM_PSRRelation__c psr:list_psr)
			{
				set_Hos.remove(psr.PM_Hospital__c);
			}
			list_psr.clear();
			list_psr = [Select p.PM_PSR__c, PM_PDSaler__c, p.PM_Status__c, p.PM_EffectDate__c, p.PM_Hospital__c, PM_Hospital__r.Name From PM_PSRRelation__c p 
						WHERE PM_PSR__c = null and PM_UneffectDate__c = null and PM_EffectDate__c = null 
						and PM_NewPSR__c = null and PM_Status__c = null and PM_Hospital__c IN : set_Hos];
			for(PM_PSRRelation__c nullPSR : list_psr)
			{
				set_Hos.remove(nullPSR.PM_Hospital__c);
			}
			List<PM_PSRRelation__c> list_nullPSR = new List<PM_PSRRelation__c>();
			for(ID hosId : set_Hos)
			{
				PM_PSRRelation__c NUllpsr = new PM_PSRRelation__c();
				NUllpsr.PM_Hospital__c = hosId;
				list_nullPSR.add(NUllpsr);
			}
			if(list_nullPSR != null && list_nullPSR.size() > 0)insert list_nullPSR;
			list_psr.addAll([Select p.PM_PSR__c, PM_PDSaler__c, p.PM_Status__c, p.PM_EffectDate__c, p.PM_Hospital__c, PM_Hospital__r.Name From PM_PSRRelation__c p 
						WHERE PM_PSR__c = null and PM_UneffectDate__c = null and PM_EffectDate__c = null 
						and PM_NewPSR__c = null and PM_Status__c = null and PM_Hospital__c IN : set_Hos]);
    	}
    	this.conset = new ApexPages.StandardSetController(list_psr);
    	this.conset.setPageSize(10);
    }
    
    //查询销售负责的医院
    public void CheckWithSaler()
    {
    	list_Allpsr.clear();
    	IsMsgView = false;
		if(list_psr != null)list_psr.clear();
		if(set_Hos != null)set_Hos.clear();
		set<ID> set_Ids = new set<ID>();
        set_Ids.clear();
		//查询销售负责的医院
		for(V2_Account_Team__c team : [Select v.V2_Account__c  From V2_Account_Team__c v where UserProduct__c =  'PD' 
                                   AND ((V2_BatchOperate__c = '新增' And V2_ApprovalStatus__c = '审批通过')
                                   OR(V2_BatchOperate__c = '替换' And V2_ApprovalStatus__c = '审批拒绝')
                                   OR(V2_BatchOperate__c = '删除' And V2_ApprovalStatus__c = '审批拒绝'))
                                   AND v.V2_User__c = : PSR.PM_PDSaler__c])
        {
        	set_Ids.add(team.V2_Account__c);
        }
    	for(Account acc : [select Id,Name,(Select Id From PMMNib__r limit 1) from Account Where Id IN :set_Ids])
    	{
    		if(acc.PMMNib__r != null && acc.PMMNib__r.size()>0)
    		{
    			set_Hos.add(acc.Id);
    		}
    	}
		getHospitalRelation(false);
		for(PM_PSRRelation__c psr:list_psr)
		{
			set_Hos.remove(psr.PM_Hospital__c);
		}
		list<PM_PSRRelation__c> list_spacePSR = [Select p.Id, p.PM_PSR__c, PM_PDSaler__c, p.PM_Status__c, p.PM_EffectDate__c, p.PM_Hospital__c, PM_Hospital__r.Name From PM_PSRRelation__c p 
					WHERE PM_PSR__c = null and PM_UneffectDate__c = null and PM_EffectDate__c = null 
					and PM_NewPSR__c = null and PM_Status__c = null and PM_Hospital__c IN : set_Hos];
					
		if(list_spacePSR!= null)list_psr.addAll(list_spacePSR);
		for(PM_PSRRelation__c nullPSR : list_spacePSR)
		{
			set_Hos.remove(nullPSR.PM_Hospital__c);
		}
		List<PM_PSRRelation__c> list_nullPSR = new List<PM_PSRRelation__c>();
		for(ID hosId : set_Hos)
		{
			PM_PSRRelation__c NUllpsr = new PM_PSRRelation__c();
			NUllpsr.PM_Hospital__c = hosId;
			list_nullPSR.add(NUllpsr);
		}
		if(list_nullPSR != null && list_nullPSR.size() > 0)insert list_nullPSR;
		list_psr.addAll([Select p.Id, p.PM_PSR__c, PM_PDSaler__c, p.PM_Status__c, p.PM_EffectDate__c, p.PM_Hospital__c, PM_Hospital__r.Name From PM_PSRRelation__c p 
					WHERE PM_PSR__c = null and PM_UneffectDate__c = null and PM_EffectDate__c = null 
					and PM_NewPSR__c = null and PM_Status__c = null and PM_Hospital__c IN : set_Hos]);
    	this.conset = new ApexPages.StandardSetController(list_psr);
    	this.conset.setPageSize(10);
    }
    
    //获取销售医院关系
    private Map<ID,ID> map_salerHos = new Map<ID,ID>(); 
    private void getSalerWithHos()
    {
    	for(V2_Account_Team__c team : [Select v.V2_User__c, v.V2_Account__c  From V2_Account_Team__c v where UserProduct__c =  'PD' 
                                   AND ((V2_BatchOperate__c = '新增' And V2_ApprovalStatus__c = '审批通过')
                                   OR(V2_BatchOperate__c = '替换' And V2_ApprovalStatus__c = '审批拒绝')
                                   OR(V2_BatchOperate__c = '删除' And V2_ApprovalStatus__c = '审批拒绝'))
                                   AND (v.V2_User__c = : PSR.PM_PDSaler__c OR V2_Account__c IN : set_Hos)])
        {
        	map_salerHos.put(team.V2_Account__c, team.V2_User__c);
        }
    }
    
    //医院关系内部类
    public class PSRWrapper
    {
    	public PM_PSRRelation__c psr{get;set;}
    	public boolean IsChecked{get;set;}
    }
}