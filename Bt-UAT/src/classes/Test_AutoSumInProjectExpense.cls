/**
 * Triggger的AutoSumInProjectExpense的测试类
 */
@isTest
private class Test_AutoSumInProjectExpense {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        //业务机会
		Opportunity opp = new Opportunity();
        opp.Name = 'Billceshi1';
        opp.StageName = '进行中';
        opp.CloseDate = date.today();
		insert opp;
		Opportunity opp2 = new Opportunity();
        opp2.Name = 'Billceshi2';
        opp2.StageName = '进行中';
        opp2.CloseDate = date.today();
		insert opp2;
		
		//费用信息
		InProject_Expense__c InProject = new InProject_Expense__c();
        InProject.Opportunity__c = opp.Id;
        InProject.Project_Expense__c = 100;
        
		test.startTest();
        insert InProject;
        InProject.Project_Expense__c = 200;
        update InProject;
        InProject.Opportunity__c = opp2.Id;
        InProject.Project_Expense__c = 300;
        update InProject;
        delete InProject;
        test.stopTest();
    }
}