global class Add4PPhotoExtension {
    public Id storeId{get;set;}
    
    global Add4PPhotoExtension(ApexPages.StandardController controller) {
        Store_4P__c s4p = [Select Id, Checkin_History__c, Checkin_History__r.Store__c From Store_4P__c Where Id =: controller.getId()];
        if(s4p.Checkin_History__r.Store__c != null){
          storeId = s4p.Checkin_History__r.Store__c;
        }
    }
  
    @RemoteAction
    global static Sf1Result submitData(sf1Data dat) {
        ID recordID;
        
        try {
            dat.addImage();
            recordID = dat.store4P.id;
            System.debug('Data Processed');
        } catch (Exception ex) {
            return new sf1Result(ex);
        }
        return new sf1Result(recordID); 
    }
    
    global class sf1Data {
        global Store_4P__c store4P{get;set;}
        global AttachStore4PPhoto.sf1Data image{get;set;}
        global String note{get;set;}
        public void addImage() {
            image.ParentId = store4P.Id;
            image.contentType = 'image';
            image.note = note;
            sf1Result result = AttachStore4PPhoto.submitData(image);
            if(!result.isSuccess){
                // throw new sf1Exception(result.message);
            }
        }
    }
}