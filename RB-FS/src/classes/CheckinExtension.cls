global class CheckinExtension {
    public String displayCheckinInfo{get;set;}
    public String displayCheckType{get;set;}
    public Store__c record{get;set;}
    
    global CheckinExtension(ApexPages.StandardController controller) {
    	record = (Store__c)controller.getRecord();
        displayCheckinInfo = '';
        displayCheckType = 'display:none;';
        displayGeneral = 'display:none;';
        qwList = new List<QuestionWrapper>();
        
        displaySuccess = 'display:none;';
    }
    
    public void navToCheckType(){
      displayCheckinInfo = 'display:none;';
      displayCheckType = 'margin-top:80px;';
    }
    
    
    @RemoteAction
    global static sf1Result newSubmitData(CheckinHistoryWrapper chw){
        Id recordId;
        try{
            List<Checkin_History__c> chList = [Select Id From Checkin_History__c 
									            Where Store__c =: chw.checkinHistory.Store__c 
									            And OwnerId =: UserInfo.getUserId() And Checkout_Time__c = null];
            if(chList.size() > 0){
                Checkin_History__c updateCheckinHistory = chList[0];
                updateCheckinHistory.Checkin_Location__Latitude__s = chw.checkinHistory.Checkin_Location__Latitude__s;
                updateCheckinHistory.Checkin_Location__Longitude__s = chw.checkinHistory.Checkin_Location__Longitude__s;
                updateCheckinHistory.Checkin_Time__c = Datetime.now();
                update updateCheckinHistory;
                recordId = updateCheckinHistory.Id;
            } else {
                chw.checkinHistory.OwnerId = UserInfo.getUserId();
                chw.checkinHistory.Checkin_Time__c = Datetime.now();
                insert chw.checkinHistory;
                recordId = chw.checkinHistory.Id;
            }
        } catch (Exception ex){
            return new Sf1Result(ex);
        }
        return new Sf1Result(recordId);
    }
    
    global class CheckinHistoryWrapper{
        global Checkin_History__c checkinHistory{get;set;}
    }
    
    
    // Add 4P
    public String displayGeneral{get;set;}
    public Id historyId{get;set;}
    public String selectedOption{get;set;}
    public String selectedType{get;set;}
    
    public String displaySuccess{get;set;}
    
    public void addGeneral(){
    	displayGeneral = '';
    	displayCheckType = 'display:none;';
    	queryQuestions();
    	
    	
    }
    
    public List<QuestionWrapper> qwList{get;set;}
    
    public boolean noqueryresult{
    	get{
    		return (qwList.size() == 0?true:false);
    	}
    	private set;
    }
    
    public String size{get;set;}
    
    public List<SelectOption> options{
    	get{
		    return retrievePicklistValues('Question__c');
    	}
    	private set;
    }
    
    public List<SelectOption> types{
    	get{
    		return retrievePicklistValues('Question_Type__c');
    	}
    	private set;
    }
    
    private List<SelectOption> retrievePicklistValues(String fieldName){
    	list<SelectOption> options = new list<SelectOption>();
	    Map<String, Schema.SObjectField> fieldMap = Question__c.sobjecttype.getDescribe().fields.getMap(); 
	    list<Schema.PicklistEntry> values = fieldMap.get(fieldName).getDescribe().getPickListValues();
	    options.add(new SelectOption('全部','全部'));
	    
	    
	    for (Schema.PicklistEntry a : values)
	    {
	    	String label = a.getLabel();
	    	if(label.length() > 9){
	    		label = label.substring(0,9);
	    	} 
	       options.add(new SelectOption(label, label)); 
	    }
	    return options;
    }
    
    public void queryQuestions(){
    	if(historyId == null){
    		List<Checkin_History__c> chList = [Select Id, Store__c From Checkin_History__c 
                                        Where OwnerId =: UserInfo.getUserId() 
                                        and Checkin_Time__c >=: Date.today() 
                                        and Checkout_Time__c = null 
                                        Order by LastModifiedDate desc];
	        if(chList.size() > 0){
	        	historyId = chList[0].Id;
	        }
    	}
    	
    	Set<Id> questionIds = new Set<Id>();
    	for(Question_Result__c result : [Select Id, Question_No__c From Question_Result__c Where Checkin_History__c =: historyId]){
    		questionIds.add(result.Question_No__c);
    	}
    	this.size = historyId;
    	
    	String query = 'Select Question__c, Id, Question_Option__c, Question_Type__c, Question_Value_Type__c From Question__c ';
    	
    	//List<Question__c> questions = new List<Question__c>();
    	if(selectedType != '全部'){
    		//questions = [Select Question__c, Id, Question_Option__c, Question_Type__c, Question_Value_Type__c From Question__c Where Id Not IN: questionIds And Question_Type__c =: selectedType];
    		query += ' Where Question_Type__c =\'' + selectedType + '\'';
    		if(selectedOption != '全部'){
    			query += ' And Question__c =\'' + selectedOption + '\'';
    			//questions = [Select Question__c, Id, Question_Option__c, Question_Type__c, Question_Value_Type__c From Question__c Where Id Not IN: questionIds And Question_Type__c =: selectedType And Question_Option__c =: selectedOption];
    		}
    	} else if(selectedOption != '全部'){
    		query += ' Where Question__c = \'' + selectedOption + '\'';
    		//questions = [Select Question__c, Id, Question_Option__c, Question_Type__c, Question_Value_Type__c From Question__c Where Id Not IN: questionIds And Question_Option__c =: selectedOption];
    	} //else {
	    	//questions = [Select Question__c, Id, Question_Option__c, Question_Type__c, Question_Value_Type__c From Question__c ];
    	//}
    	
    	List<Question__c> questions = Database.query(query);
    	
	    qwList.clear();
	    for(Question__c q : questions){
	    	QuestionWrapper qw = new QuestionWrapper();
		    qw.question = q;
		    if(q.Question_Value_Type__c == 'Text'){
		    	qw.isString = true;
		    } else if(q.Question_Value_Type__c == 'Boolean'){
		    	qw.isBoolean = true;
		    } else if(q.Question_Value_Type__c == 'Number'){
		    	qw.isDouble = true;
		    }
	    	qwList.add(qw);
	    }
    }
    
    public void saveResult(){
    	displaySuccess = '';
    	
    	List<Question_Result__c> qrList = new List<Question_Result__c>();
    	for(QuestionWrapper qw : qwList){
    		Question_Result__c qr = new Question_Result__c();
    		qr.Store__c = record.Id;
    		if(historyId != null){
	    		qr.Checkin_History__c = historyId;
    		}
    		qr.Question_No__c = qw.question.Id;
    		qr.Question_Value_Type__c = qw.question.Question_Value_Type__c;
    		qr.Question_Type__c = qw.question.Question_Type__c;
    		qr.Question_Option__c = qw.question.Question_Option__c;
    		qr.Question__c = qw.question.Question__c;
    		if(qw.question.Question_Value_Type__c == 'Text'){
    			qr.Result_Value_Text__c = qw.text;
    		} else if(qw.question.Question_Value_Type__c == 'Boolean'){
    			qr.Result_Value_Boolean__c = qw.isChecked;
    		} else if(qw.question.Question_Value_Type__c == 'Number'){
    			qr.Result_Value_Number__c = qw.numbers;
    		}
    		
    		qrList.add(qr);
    	}
    	
    	if(qrList.size() > 0){
    		insert qrList;
    	}
    	
    	//qwList.clear();
    }
    
    public void cancel(){
    	
    }
    
    public class QuestionWrapper{
    	public Question__c question{get;set;}
    	public boolean isString{get;set;}
    	public boolean isBoolean{get;set;}
    	public boolean isDouble{get;set;}
    	
    	public String text{get;set;}
    	public boolean isChecked{get;set;}
    	public Double numbers{get;set;}
   		public QuestionWrapper(){
   			isString = false;
   			isBoolean = false;
   			isDouble = false;
   		}
    }
}