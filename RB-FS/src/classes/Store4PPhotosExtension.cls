public class Store4PPhotosExtension{
    public List<Attachment> atts{get;set;}
    public Integer attNumber{get;set;}
    
    public Store4PPhotosExtension(ApexPages.StandardController controller){
        atts = [Select Id, CreatedDate From Attachment Where ParentId =: controller.getId()];
        attNumber = atts.size();
    }
}