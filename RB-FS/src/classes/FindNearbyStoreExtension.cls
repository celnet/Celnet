global with sharing class FindNearbyStoreExtension {
     public FindNearbyStoreExtension(ApexPages.StandardSetController controller) { }

    @RemoteAction
    // Find stores nearest a geolocation
    global static List<Store__c> getNearby(String lat, String lon) {
		
		Integer range = (Integer)RB_Setting__c.getInstance('Checkin Range').Value__c / 1000;

        // If geolocation isn't set, use null
        if(lat == null || lon == null || lat.equals('') || lon.equals('')) {
            lat = '0';
            lon = '0';
        }

        // SOQL query to get the nearest stores
        String queryString =
            'SELECT Id, Name, Geolocation__Longitude__s, Geolocation__Latitude__s, ' +
                'Address__c, Phone__c, City__c ' +
            'FROM Store__c ' +
            'WHERE DISTANCE(Geolocation__c, GEOLOCATION('+lat+','+lon+'), \'km\') < ' + range + ' ' +
           // ' AND CreatedBy = \'' + UserInfo.getUserId() + '\'' +
            ' ORDER BY DISTANCE(Geolocation__c, GEOLOCATION('+lat+','+lon+'), \'km\') ';

        // Run and return the query results
        return(database.Query(queryString));
    }
}