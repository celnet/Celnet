global class Sf1Result {
    public Boolean isSuccess { get; set; }
    public String message { get; set; }
    public String recordID { get; set; }
    
    public Sf1Result(Exception ex) {
        this();
        isSuccess = false;
        message = 'Apex Exception: ' + ex.getStackTraceString() + ' : ' + ex.getMessage();
    }

    public Sf1Result(ID recordID) {
        this();
        isSuccess = true;
        message = 'Apex completed succesfully';     
        this.recordID = recordID;   
    }
    
    private Sf1Result() {}
}