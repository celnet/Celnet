trigger Store4P on Store_4P__c (before insert) {
    List<Checkin_History__c> chList = [Select Id, Store__c From Checkin_History__c 
                                        Where OwnerId =: UserInfo.getUserId() 
                                        and Checkin_Time__c >=: Date.today() 
                                        and Checkout_Time__c = null 
                                        Order by LastModifiedDate desc];
    if(chList.size() > 0){
        for(Store_4P__c s4 : trigger.new){
            s4.Checkin_History__c = chList[0].Id;
        }
    }
}