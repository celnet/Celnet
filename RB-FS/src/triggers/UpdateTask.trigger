/**
 * Author: Steven
 * Date: 2014-9-2
 * Description: 更新Task到In progress和 Completed
 */
trigger UpdateTask on Checkin_History__c (after insert, after update) {
    list<Task> tasks = [Select Id, Status From Task 
                                            Where ActivityDate >: Date.today().toStartOfMonth() 
                                            And ActivityDate <: Date.today().toStartOfMonth().addMonths(1).addDays(-1)];
    list<Task> updateTasks = new list<Task>();
    
    for(Checkin_History__c ch : trigger.new){
        if(ch.Checkin_Presence__c){
            for(task t : tasks){
                if(t.What.get('Store__c') == ch.Store__c && t.Status == 'Not Started'){
                    t.Status = 'In Progress';
                    updateTasks.add(t);
                }
            }
        }
        
        if(ch.Checkout_Presence__c){
             for(task t : tasks){
                    if(t.What.get('Store__c') == ch.Store__c && t.Status == 'In Progress'){
                        t.Status = 'Completed';
                        updateTasks.add(t);
                    }
             }
        }
    }
    
    update updateTasks;
}