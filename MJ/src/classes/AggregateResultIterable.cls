global class AggregateResultIterable implements Iterable<AggregateResult> {
    String query;
    public AggregateResultIterable(String query){
        this.query = query;
    }
    
    global Iterator<AggregateResult> Iterator(){
      return new AggregateResultIterator(query);
   }
}