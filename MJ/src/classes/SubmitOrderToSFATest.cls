@isTest
public class SubmitOrderToSFATest{

    public static testMethod void submitOrder()
    {
        createCustomSetting();
        
       // Create a test Account
        Account testAccount = new Account();
        testAccount.Name = 'Account';
        testAccount.Last_Name__c='Account';
        testAccount.Mobile_Phone__c='90010909';
        insert testAccount;
        
        //Create test Order
        Order__c order =new Order__c();
        order.Delivery_Center__c = 'DKSH';
        order.HouseHold__c = testAccount.ID;
        order.Delivery_Instruction__c = 'TEST';
        order.Expected_Delivery_Date__c = Date.today().addDays(7);
        insert order;
        
        List<Order_Item__c> orderItems;
        Order__c selectedOrder = new Order__c();
        
        //Test Case 0 : No valid parameter -ID 
        //Instantiate and construct the controller class
        ApexPages.StandardController standardController = new ApexPages.StandardController(selectedOrder);
        SubmitSingleOrderExtension controller = new SubmitSingleOrderExtension(standardController);
        Boolean displayOrder = controller.getDisplayOrder();
        System.assertEquals(false,displayOrder);
        
        //Set Order ID
        ApexPages.currentPage().getParameters().put('id', order.Id);
        
        //Instantiate and construct the controller class
        standardController = new ApexPages.StandardController(selectedOrder);
        controller = new SubmitSingleOrderExtension(standardController);
        
        //Test Case 1: Blank Order
        displayOrder = controller.getDisplayOrder();
        System.assertEquals(false,displayOrder);
        
        //Create a product
        Product__c product = new Product__c();
        product.Name = 'Product 1';
        product.Product_SKU__c = 'Product 1 900g';
        product.Product_Code__c = 'Product01';
        product.Product_Nature__c = 'MJ';
        product.Is_DSO_Product__c = True;
        product.Default_Order_Quantity__c = 10;
        product.Allowed_Order_Period__c = 20;
        product.Min_Order_Quantity__c = 1;
        product.Max_Order_Quantity__c = 50;
        product.Unit_Price__c = 8.88;
        insert product;
        
        //Create a order item
        Order_Item__c item = new Order_Item__c();
        item.Order__c = order.Id;
        item.Quantity__c = 5;
        item.Product__c = product.ID;
        insert item;
        
        //Test Case 2: Need Send To SFA = False
        order.Delivery_Center__c = 'Not DKSH';
        update order;

        Order__c savedOrder = [SELECT ID,Name,HouseHold__c,First_Name__c,Last_Name__c,Cancelled__c,Need_Send_to_SFA__c
                               FROM Order__c WHERE ID = :order.ID];

        System.assertEquals(false,savedOrder.Need_Send_to_SFA__c);
        //Instantiate and construct the controller class
        controller = new SubmitSingleOrderExtension(standardController);
        displayOrder = controller.getDisplayOrder();
        System.assertEquals(false,displayOrder);
        
        order.Delivery_Center__c = 'DKSH';
        update order;
        
        //Instantiate and construct the controller class
        controller = new SubmitSingleOrderExtension(standardController);
        
        displayOrder = controller.getDisplayOrder();
        selectedOrder = controller.getSelectedOrder();
        orderItems = controller.getOrderItems();
        System.assertEquals(true,displayOrder);
        System.assertEquals(1,orderItems.size());

        // Submit Order
        String nextPage = controller.submitOrder().getUrl(); 
        System.assertEquals('/'+order.Id, nextPage);
        
        //Test Case 4: Re-submit the Same Order
        //Instantiate and construct the controller class
        controller = new SubmitSingleOrderExtension(standardController);
        
        // Re-submit Order
        displayOrder = controller.getDisplayOrder();
        System.assertEquals(false,displayOrder);
    }

	public static testMethod void submitCancelledOrder()
    {
        createCustomSetting();
        
       // Create a test Account
        Account testAccount = new Account();
        testAccount.Name = 'Account';
        testAccount.Last_Name__c='Account';
        testAccount.Mobile_Phone__c='90010909';
        insert testAccount;
        
        //Create test Order
        Order__c order =new Order__c();
        order.Delivery_Center__c = 'DKSH';
        order.HouseHold__c = testAccount.ID;
        order.Delivery_Instruction__c = 'TEST';
        order.Expected_Delivery_Date__c = Date.today().addDays(7);
        insert order;
        
        List<Order_Item__c> orderItems;
        Order__c selectedOrder = new Order__c();
        
        //Set Order ID
        ApexPages.currentPage().getParameters().put('id', order.Id);
        
        //Instantiate and construct the controller class
        ApexPages.StandardController standardController = new ApexPages.StandardController(selectedOrder);
        SubmitSingleOrderExtension controller = new SubmitSingleOrderExtension(standardController);
        
        
        //Create a product
        Product__c product = new Product__c();
        product.Name = 'Product 1';
        product.Product_SKU__c = 'Product 1 900g';
        product.Product_Code__c = 'Product01';
        product.Product_Nature__c = 'MJ';
        product.Is_DSO_Product__c = True;
        product.Default_Order_Quantity__c = 10;
        product.Allowed_Order_Period__c = 20;
        product.Min_Order_Quantity__c = 1;
        product.Max_Order_Quantity__c = 50;
        product.Unit_Price__c = 8.88;
        insert product;
        
        //Create a order item
        Order_Item__c item = new Order_Item__c();
        item.Order__c = order.Id;
        item.Quantity__c = 5;
        item.Product__c = product.ID;
        insert item;
        

        Order__c savedOrder = [SELECT ID,Name,HouseHold__c,First_Name__c,Last_Name__c,Cancelled__c,Need_Send_to_SFA__c
                               FROM Order__c WHERE ID = :order.ID];

        //Instantiate and construct the controller class
        controller = new SubmitSingleOrderExtension(standardController);
        boolean displayOrder = controller.getDisplayOrder();
        System.assertEquals(true,displayOrder);
        
        //Test Case 2: Canncelled Order
        order.Delivery_Center__c = 'DKSH';
        order.Cancelled__c = TRUE;
        order.Cancel_Reason__c = 'Test Reason';
        update order;
        //Instantiate and construct the controller class
        controller = new SubmitSingleOrderExtension(standardController);
        displayOrder = controller.getDisplayOrder();
        System.assertEquals(false,displayOrder);
        
      
    }
    
    public static testMethod void submitBulkOrders()
    {
       createCustomSetting();
        
       // Create a test Account
        Account testAccount = new Account();
        testAccount.Name = 'Account';
        testAccount.Last_Name__c='Account';
        testAccount.Mobile_Phone__c='90010909';
        insert testAccount;
        
        //Create a product
        Product__c product = new Product__c();
        product.Name = 'Product 1';
        product.Product_SKU__c = 'Product 1 900g';
        product.Product_Code__c = 'Product01';
        product.Product_Nature__c = 'MJ';
        product.Is_DSO_Product__c = True;
        product.Default_Order_Quantity__c = 10;
        product.Allowed_Order_Period__c = 20;
        product.Min_Order_Quantity__c = 1;
        product.Max_Order_Quantity__c = 50;
        product.Unit_Price__c = 8.88;
        insert product;
        
        //Create test Order 1 - blank order with no item
        Order__c order1 =new Order__c();
        order1.HouseHold__c = testAccount.ID;
        order1.Delivery_Center__c = 'DKSH';
        order1.Delivery_Instruction__c = 'TEST';
        order1.Expected_Delivery_Date__c = Date.today().addDays(7);
        insert order1;
        
        //Create test Order 2 - simulate submitted order
        Order__c order2 =new Order__c();
        order2.HouseHold__c = testAccount.ID;
        order2.Delivery_Center__c = 'DKSH';
        order2.Delivery_Instruction__c = 'TEST';
        order2.Expected_Delivery_Date__c = Date.today().addDays(7);
        insert order2;
        
        //Create a order item
        Order_Item__c item1 = new Order_Item__c();
        item1.Order__c = order2.Id;
        item1.Quantity__c = 5;
        item1.Product__c = product.ID;
        insert item1;
        
        SF2SFHelper.submitOrderToSFA(order2.ID,false);
        
        //Create test Order 3 - Valid Order
        Order__c order3 =new Order__c();
        order3.HouseHold__c = testAccount.ID;
        order3.Delivery_Center__c = 'DKSH';
        order3.Delivery_Instruction__c = 'TEST';
        order3.Expected_Delivery_Date__c = Date.today().addDays(7);
        insert order3;
        
        //Create a order item
        Order_Item__c item2 = new Order_Item__c();
        item2.Order__c = order3.Id;
        item2.Quantity__c = 5;
        item2.Product__c = product.ID;
        insert item2;
        
        //Create test Order 4 - Cancelled Order
        Order__c order4 =new Order__c();
        order4.HouseHold__c = testAccount.ID;
        order4.Delivery_Center__c = 'DKSH';
        order4.Delivery_Instruction__c = 'TEST';
        order4.Expected_Delivery_Date__c = Date.today().addDays(7);
        insert order4;
        order4.Cancelled__c = true;
        order4.Cancel_Reason__c = 'Test Reason';
        update order4;
        
        //Create test Order 5 - Order Delivery Center is not DKSH - Need Send to SFA = False
        Order__c order5 =new Order__c();
        order5.HouseHold__c = testAccount.ID;
        order5.Delivery_Center__c = 'Not DKSH';
        order5.Delivery_Instruction__c = 'TEST';
        order5.Expected_Delivery_Date__c = Date.today().addDays(7);
        insert order5;
        
        List<Order__c> selectedList = new List<Order__c>();
        
        //Test Case 0 : No selected order
        //Instantiate and construct the controller class
        ApexPages.StandardSetController standardSetController = new ApexPages.StandardSetController(selectedList);
        standardSetController.setSelected(selectedList);
        SubmitBulkOrderExtension controller = new SubmitBulkOrderExtension(standardSetController);
        Boolean displayDetail = controller.getDisplayDetail();
        System.assertEquals(false,displayDetail);
        
        //Set Selected Orders
        selectedList.add(order1);
        selectedList.add(order2);
        selectedList.add(order3);
        selectedList.add(order4);
        selectedList.add(order5);
        
        //Instantiate and construct the controller class
        standardSetController = new ApexPages.StandardSetController(selectedList);
        standardSetController.setSelected(selectedList);
        controller = new SubmitBulkOrderExtension(standardSetController);
        
        displayDetail = controller.getDisplayDetail();
        List<Order__c> validOrders = controller.getValidOrders();
		List<Order__c> blankOrders = controller.getBlankOrders();
        List<Order__c> submittedOrders = controller.getSubmittedOrders();
        List<Order__c> cancelledOrders = controller.getCancelledOrders();
        List<Order__c> noSFAOrders = controller.getNoSFAOrders();
        Boolean showBlankOrders = controller.getShowBlankOrders();
        Boolean showSubmittedOrders = controller.getShowSubmittedOrders();
        Boolean showCancelledOrders = controller.getShowCancelledOrders();
        Boolean showNoSFAOrders = controller.getShowNoSFAOrders();
        
        System.assertEquals(true,displayDetail);
        System.assertEquals(true,showBlankOrders);
        System.assertEquals(true,showSubmittedOrders);
        System.assertEquals(true,showCancelledOrders);
        System.assertEquals(true,showNoSFAOrders);
        System.assertEquals(1,controller.getBlankOrdersSize());
        System.assertEquals(1,controller.getSubmittedOrdersSize());
		System.assertEquals(1,controller.getValidOrdersSize());
        System.assertEquals(1,controller.getCancelledOrdersSize());
        System.assertEquals(1,controller.getNoSFAOrdersSize());
        
        // Submit Order
        String nextPage = controller.submitOrders().getUrl(); 
        System.assertEquals('/apex/SubmitBulkOrdersSuccess', nextPage);
        
        //Re-Instantiate and construct the controller class
        controller = new SubmitBulkOrderExtension(standardSetController);
        validOrders = controller.getValidOrders();
		blankOrders = controller.getBlankOrders();
        submittedOrders = controller.getSubmittedOrders();
        cancelledOrders = controller.getCancelledOrders();
        noSFAOrders = controller.getNoSFAOrders();
        System.assertEquals(1,blankOrders.size());
        System.assertEquals(2,submittedOrders.size());
		System.assertEquals(0,validOrders.size());
        System.assertEquals(1,cancelledOrders.size());
        System.assertEquals(1,noSFAOrders.size());
        
        
        
        //Submit Orders with no valid
        System.assertEquals(NULL, controller.submitOrders());
    }
    
    private static void createCustomSetting()
    {
        //Create custom setting in test calss
        String curYear = String.valueOf(Date.today().year());
        Member_ID_Setting__c setting = new Member_ID_Setting__c();
        setting.Name=curYear;
        setting.Next_ID__c=1888;
        insert setting;
    }
}