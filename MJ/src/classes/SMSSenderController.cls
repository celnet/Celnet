/*Author:leo.bi@celnet.com.cn
 *Created On:2014-4-16
 *function:The Controller of visualforce page named "SMSSender"
 *Apply To:CN
 */ 
public with sharing class SMSSenderController 
{
	public String receivePhone{get;set;} 
	public String selectedTemplate{get;set;}
	public String templateContent{get;set;}
	private String ErrorMessage = '';
	public Contact con{get;set;}
	public Lead lea{get;set;} 
	public SMS_Message__c sms{get;set;}{sms = new SMS_Message__c();}
	public Boolean inputField_Lead_IsShow{get;set;}{inputField_Lead_IsShow=false;}
	public Boolean inputField_Contact_IsShow{get;set;}{inputField_Contact_IsShow=false;}
	Map<String, SMS_Template__c> templates = new Map<String, SMS_Template__c>();
	private Map<String,String> map_phones = new Map<String,String>();//define a map collection containing the list of selected phones list, which is used for return the phone value instead of the key
	public String selectedTitle{get;set;}
	public String selectedSubtitle{get;set;}
	private Map<String,Set<String>> map_Title_Subtitle = new Map<String,Set<String>>();//map for title and subtitle
	private Map<String,List<String>> map_Subtitle_Name = new Map<String,List<String>>();//map for subtitle and name
	public Boolean returnButtonIsShow{get;set;} {returnButtonIsShow = true;}
	private string userGroup;//check the current user whether is a inbound or outbound user 
	public Boolean contentDisabled{get;set;}{contentDisabled = true;}
	
	public Boolean subTitleDisable{get;set;}{subTitleDisable = true;}
	public Boolean nameDisable{get;set;}{nameDisable = true;}
	
	public Boolean isClose{get;set;}{isClose = false;}
	//public String isInConsole{get;set;}//in console
	//private String lookupId = '003N0000009nxJ9';
	//init contact and lead
	private void initContactLead()
	{
		String conId = ApexPages.currentPage().getParameters().get('contactId');
		String leaId = ApexPages.currentPage().getParameters().get('leadId');
		Profile userFrofile = [Select Name, Id From Profile where Id = :Userinfo.getProfileId()][0];
		if(userFrofile.Name.contains('CN_Inbound'))
		{
			userGroup = 'ib';
		}
		else if(userFrofile.Name.contains('CN_Outbound'))
		{
			userGroup = 'ob';
		}
		else if(userFrofile.Name.contains('System Administrator'))
		{
			userGroup = 'admin';
		}/*
		if(userGroup == 'ib')
		{
			contentDisabled = true;
		}
		else
		{
			contentDisabled = false;
		}*/
		if((conId==null || conId=='') && (leaId != null && leaId != ''))
		{
			lea = [Select Id, MobilePhone,Phone From Lead where Id = :leaId];
			sms.Recipient_Lead__c = lea.Id;
			inputField_Lead_IsShow = true;
			
		}
		if((leaId==null || leaId=='') && (conId != null && conId != ''))
		{
			con = [Select Id,Account.Other_Phone_2__c, Account.Other_Phone_1__c,Account.Mobile_Phone__c, Account.Phone,AccountId
			      From Contact where Id=:conId];
			sms.Recipient_Contact__c = con.Id;
			inputField_Contact_IsShow = true;
		}
		if(lea==null && con==null)
		{
			returnButtonIsShow = false;
			return;
		}
	}
	
	/*
	 *funtion:init maps for seletlists
	 *description: load map_Title_Subtitle and map_Subtitle_Name, because the names of two subTitles belonging to different
	 * 		 	   titles may be same, the form of key for map_Subtitle_Name is 'title + subTitle'
	 */
	private void initMaps()
	{
		//User_Profile_Type__c
		Map<String, SMS_Template__c> templatesAll = SMS_Template__c.getAll();
		if(userGroup == 'admin')
		{
			templates = SMS_Template__c.getAll();
		}
		else
		{
			for(String sms : templatesAll.keySet())
			{
				SMS_Template__c temp_SMS_Template = templatesAll.get(sms);
				if(temp_SMS_Template.User_Profile_Type__c == userGroup)
				{
					templates.put(temp_SMS_Template.Name,temp_SMS_Template);
				}
			}
		}
		if(templates.Size() != 0)
		{
			for(String templateKey : templates.keySet())
			{
				SMS_Template__c temp_SMS_Template = templates.get(templateKey);
				Set<String> set_Title_SubTitle = new Set<String>();
				List<String> list_SubTitle_Name = new List<String>();
				//List<String> temp_SubTitle_Name = new List<String>();
				if(map_Title_Subtitle.containsKey(temp_SMS_Template.Title__c))
				{
					set_Title_SubTitle = map_Title_Subtitle.get(temp_SMS_Template.Title__c);
					set_Title_SubTitle.add(temp_SMS_Template.SubTitle__c);
					system.debug('---------------------set_Title_SubTitle---------------------' + set_Title_SubTitle);
					system.debug('---------------------map_Title_Subtitle---------------------' + map_Title_Subtitle);
					if(map_Subtitle_Name.containsKey(temp_SMS_Template.Title__c + temp_SMS_Template.SubTitle__c))
					{
						list_SubTitle_Name = map_Subtitle_Name.get(temp_SMS_Template.Title__c + temp_SMS_Template.SubTitle__c);
						list_SubTitle_Name.add(temp_SMS_Template.Name);
					}
					else
					{
						list_SubTitle_Name.add(temp_SMS_Template.Name);
						map_Subtitle_Name.put(temp_SMS_Template.Title__c + temp_SMS_Template.SubTitle__c,list_SubTitle_Name);
					}
				}
				else
				{
					set_Title_SubTitle.add(temp_SMS_Template.SubTitle__c);
					map_Title_Subtitle.put(temp_SMS_Template.Title__c,set_Title_SubTitle);
					
					list_SubTitle_Name.add(temp_SMS_Template.Name);
					map_Subtitle_Name.put(temp_SMS_Template.Title__c + temp_SMS_Template.SubTitle__c,list_SubTitle_Name);
				}
			}
		}
	}
	//Constructor
	public SMSSenderController()
	{
		//this.isInConsole = ApexPages.currentPage().getParameters().get('isInConsole');
		initContactLead();
		initMaps();
	}
	
	public void refreshPhone()
	{
		if(con != null)
		{
			con = [Select Id,Account.Other_Phone_2__c, Account.Other_Phone_1__c,Account.Mobile_Phone__c, Account.Phone,AccountId
				      From Contact where Id=:sms.Recipient_Contact__c];
		}
		if(lea != null)
		{
			lea = [Select Id, MobilePhone,Phone From Lead where Id = :sms.Recipient_Lead__c];
		}
	}
	
	public void refreshSubTitle()
	{
		selectedSubTitle = null;
		selectedTemplate = null;
		templateContent = null;
		if(selectedTitle == null)
		{
			subTitleDisable = true;
			nameDisable = true;
			contentDisabled = true;
		}
		else
		{
			subTitleDisable = false;
		}
	} 
	
	public void refreshName()
	{
		selectedTemplate = null;
		templateContent = null;
		if(selectedSubTitle == null)
		{
			nameDisable = true;
			contentDisabled = true;
		}
		else
		{
			nameDisable = false;
		}
	}
	
	//refresh the template content when the template name is changed
	public void refreshContent()
	{
		if(selectedTemplate != null && selectedTemplate != '')
		{
			SMS_Template__c template = templates.get(selectedTemplate);
			templateContent = template.Content__c;
			contentDisabled = false;
			if(userGroup == 'ob')
			{
				contentDisabled = true;
			}
			
		}
		else
			{
				templateContent = null;
				contentDisabled = true;
			}
	}
	
	
	
	public List<SelectOption> titleItems
	{
		get
		{
			List<SelectOption> options = new List<SelectOption>();
			options.add(new SelectOption('', '-- Select One --'));
			List<String> list_Items = new List<String>();
			list_Items.addAll(map_Title_Subtitle.keySet());
			list_Items.sort();
			for(String titleKey : list_Items)
			{
				options.add(new SelectOption(titleKey,titleKey));
			}
			
			return options;
		}
	}
	
	public List<SelectOption> subtitleItems
	{
		get
		{
			List<SelectOption> options = new List<SelectOption>();
			options.add(new SelectOption('', '-- Select One --'));
			if(selectedTitle != null && selectedTitle != '')
			{
				system.debug('-----------------selectedTitle-------------------' + selectedTitle);
				Set<String> set_Items = map_Title_Subtitle.get(selectedTitle);
				system.debug('-----------------set_Items-------------------' + set_Items);
				//sort the SelectOptions
				List<String> list_Items = new List<String>();
				list_Items.addAll(set_Items);
				system.debug('-----------------list_Items-------------------' + list_Items);
				list_Items.sort();
				for(String item : list_Items)
				{
					options.add(new SelectOption(item,item));
				}
			}
			return options;
		}
	}
	
	//get the SMS_Template list of selectOptions
	public List<SelectOption> SMS_TemplateItems
	{
		get
		{
			List<SelectOption> options = new List<SelectOption>();
	        options.add(new SelectOption('', '-- Select One --'));        
			if(selectedSubtitle != null && selectedSubtitle != '' && selectedTitle != null && selectedTitle != '')
			{
				List<String> list_Items = map_SubTitle_Name.get(selectedTitle + selectedSubtitle);
				list_Items.sort();
				for(String item : list_Items)
				{
					options.add(new SelectOption(item,item));
				}
			}
	        return options;
		}
	}
	
	//get the phones list of selectOptions   
	public List<SelectOption> PhoneItems
	{
		get
		{
			List<SelectOption> options = new List<SelectOption>();
				if(con != null)
				{
					if(con.Account.Mobile_Phone__c != null && con.Account.Mobile_Phone__c != '')
					{
						options.add(new SelectOption('phone1',con.Account.Mobile_Phone__c));
						map_phones.put('phone1',con.Account.Mobile_Phone__c);
					}
					if(con.Account.Phone != null && con.Account.Phone != '')
					{
						options.add(new SelectOption('Phone2',con.Account.Phone));
						map_phones.put('Phone2',con.Account.Phone);
					}
					if(con.Account.Other_Phone_1__c != null && con.Account.Other_Phone_1__c != '')
					{
						options.add(new SelectOption('Phone3',con.Account.Other_Phone_1__c));
						map_phones.put('Phone3',con.Account.Other_Phone_1__c);
					}
					if(con.Account.Other_Phone_2__c != null && con.Account.Other_Phone_2__c != '')
					{
						options.add(new SelectOption('Phone4',con.Account.Other_Phone_2__c));
						map_phones.put('Phone4',con.Account.Other_Phone_2__c);
					}
				}
				if(lea != null && lea.MobilePhone!=null)
				{
					options.add(new SelectOption('lPhone',lea.MobilePhone));
					map_phones.put('lPhone',lea.MobilePhone);
				}
				if(lea != null && lea.Phone!=null)
				{
					options.add(new SelectOption('lPhone2',lea.Phone));
					map_phones.put('lPhone2',lea.Phone);
				}
			return options;
		}
	}
	
	public PageReference returnTo()
	{
		string returnID = '';
		if(lea != null)
		{
			returnID = lea.Id;
		}
		if(con != null)
		{
			returnID = con.Id;
		}
		PageReference pageRef = new PageReference(System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+returnID);
    	return pageRef;
	}
	
	//send email and save log
	public void sendSMS()
	{
		SMSProxy smsp = new SMSProxy();
		string stateCode;
    	if(receivePhone==null || receivePhone=='')
    	{
    		ErrorMessage += '请选择收信人手机号';
    	}
    	else if(!Pattern.matches('^(13[0-9]|14[0-9]|15[0-9]|18[0-9])([0-9]{8,8})$', map_phones.get(receivePhone)))
    	{
    		
    		ErrorMessage += '选择的号码格式不正确' + 'the phone number is' + receivePhone + 'match result is ' + String.valueOf(Pattern.matches('^(13[0-9]|14[0-9]|15[0-9]|18[0-9])([0-9]{8,8})$', receivePhone));
    	}
    	if( templateContent==null || templateContent=='')
    	{
    		ErrorMessage += '请输入短信内容';
    	}
    	if(ErrorMessage != '')
    	{
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.WARNING, ErrorMessage);
	    	ApexPages.addMessage(myMsg);
			ErrorMessage = '';
			return;
    	}
    	try 
    	{
			if(receivePhone != null && receivePhone != '')
			{
				sms.Recipient_Mobile__c = map_phones.get(receivePhone);
			}
			if(selectedTemplate != null && selectedTemplate != '')
			{
				sms.Template_Name__c = selectedTemplate;
			} 
			if(templateContent != null && templateContent != '')
			{
				sms.Content__c = templateContent;
			}
			smsp.send(map_phones.get(receivePhone), templateContent);
			stateCode = smsp.getState();
			sms.Error__c = stateCode;
			sms.Return_Body__c = smsp.returnBody;
			if(sms.Return_Body__c == null || sms.Return_Body__c == '')
			{
				sms.Error__c = 'Server Return No XML';
			}
			system.debug('----------------stateCode---------------------------' + stateCode);
			system.debug('----------------sms.Return_Body__c---------------------------' + sms.Return_Body__c);
			
    	}
    	catch (Exception e)
    	{
    		system.debug('*******************e.getMessage()**********************************' + e.getMessage());
    		sms.Error__c = e.getMessage();
    	}
    	if(stateCode == '001')
		{
			sms.Result__c = 'Success';
		}
		else
		{
			sms.Result__c = 'Failed';
		}
		insert sms;
		//return this.returnTo();
		isClose = true;
    	
	}
}