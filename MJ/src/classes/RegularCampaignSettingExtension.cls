public class RegularCampaignSettingExtension{
    
    private final ApexPages.StandardController controller;
    
    @TestVisible 
    private final Campaign_Scheduler__c campaignScheduler;
    
    @TestVisible
    private static final SelectOption DEFAULT_OPTION =  new SelectOption('000','Select Member Type First', true); 
    
    //Constructor
    public RegularCampaignSettingExtension(ApexPages.StandardController controller) {
        this.controller = controller;
        campaignScheduler = (Campaign_Scheduler__c) controller.getRecord();
        campaignScheduler.Data_Type__c = 'Contacts';
        onDataTypeChange();
    }
    
    public SelectOption[] listViewOptions {
        get{
            if (listViewOptions == null){
                    listViewOptions = new SelectOption[]{DEFAULT_OPTION};
             }                           
             return listViewOptions;
        }
        set;
     }
    
    // retrieve respective list view options when member type changes
    public void onDataTypeChange(){
        String selectedType = campaignScheduler.Data_Type__c;
        if (selectedType == 'Contacts'){
            listViewOptions = SetControllerFactory.newContactController().getListViewOptions();
        }else if (selectedType == 'Leads'){
            listViewOptions = SetControllerFactory.newLeadController().getListViewOptions();
        }else{
            listViewOptions = null;
        }
    }
    
    public SelectOption[] batchWindowOptions {
    	get{
    		if (batchWindowOptions==null){
				batchWindowOptions = new List<SelectOption>();
		        //Find all the windows in the custom setting
		        Map<Decimal, Batch_Campaign_Window__c> windows = new Map<Decimal, Batch_Campaign_Window__c>();
		        for (Batch_Campaign_Window__c window : Batch_Campaign_Window__c.getAll().values()){
		        	windows.put(window.Start_Hour__c, window);
		        }	        
		        
		        // Sort them by start hour
		        List<Decimal> hours = new List<Integer>();
		        hours.addAll(windows.keyset());
		        hours.sort();
		        
		        // Create the Select Options.
		        for (Decimal hour : hours) {
		            Batch_Campaign_Window__c window = windows.get(hour);
		            batchWindowOptions.add(new SelectOption(''+window.Start_Hour__c, window.Name));
		        }
    		}
	        return batchWindowOptions;
    	}
    	set;
    }
    public PageReference save(){
        campaignScheduler.List_view_Name__c = getLabelFromViewId(campaignScheduler.List_view_id__c); 
        campaignScheduler.Preferred_Execution_Time__c = getLabelFromStartHour(''+campaignScheduler.Preferred_Execution_Start_Hour__c);
        return controller.save();
    }
    
    private String getLabelFromViewId(String id){
        for (SelectOption opt : listViewOptions){
            if (opt.getValue() == id)
                return opt.getLabel();
        }
        return null;
    }
     private String getLabelFromStartHour(String hour){
        for (SelectOption opt : batchWindowOptions){
            if (opt.getValue() == hour)
                return opt.getLabel();
        }
        return null;
    }
}