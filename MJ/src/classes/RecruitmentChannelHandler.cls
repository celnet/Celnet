/*
	Before Update:
    Make sure cannot unset the only primary. 
    
    After Insert/Update:
    After set a recruitment channel as primary, 
    if there’re already another recruitment channel as primary, un-set the previous one
    
    Before Insert:
    Make sure first channel is set Primary - Disabled now as it's Done via VR
*/
public with sharing class RecruitmentChannelHandler {
	public static final String CANNOT_UNSET_PRIMARY = 'Cannot unset Primary Recruitment Channel.';
	private boolean m_isExecuting = false;
	private integer BatchSize = 0;
	
	public RecruitmentChannelHandler(boolean isExecuting, integer size){
		m_isExecuting = isExecuting;
		BatchSize = size;
	}
	
	public void OnBeforeInsert(Recruitment_Channel__c[] channels){
		//mustSetPrimaryOnFirst(channels); - Disabled now as it's Done via VR
	}
	
	public void OnAfterInsert(Recruitment_Channel__c[] channels){
		
        resetPrimaryChannel(channels);
	}
	
	public void OnBeforeUpdate(Recruitment_Channel__c[] oldChannels, Map<ID, Recruitment_Channel__c> oldChannelsMap, Recruitment_Channel__c[] newChannels, Map<ID, Recruitment_Channel__c> channelsMap){
		preventUnsetOnlyPrimary(newChannels, oldChannelsMap);
	}
	
	public void OnAfterUpdate(Recruitment_Channel__c[] oldChannels, Recruitment_Channel__c[] newChannels, Map<ID, Recruitment_Channel__c> channelsMap){
		
        resetPrimaryChannel(newChannels);
	}

	/** END Trigger Handler
		START helper methods **/
		
	private void resetPrimaryChannel(Recruitment_Channel__c[] newChannels)
    {
    	Map<Id,List<Recruitment_Channel__c>> priRecruitChannelByContactId 
			= getExistingPrimaryChannels(newChannels);
		Map<Id,Recruitment_Channel__c> channelsToUpdate = new Map<Id,Recruitment_Channel__c>();
       
		for (Recruitment_Channel__c channel : newChannels) 
    	{
    		if (channel.Primary__c == true)
    		{
    			List<Recruitment_Channel__c> priChnList = priRecruitChannelByContactId.get(channel.Contact__c);
    			if (priChnList != null){ 
    				for (Recruitment_Channel__c priChn : priChnList){
    					if (priChn.Id != channel.Id){
    						priChn.Primary__c = false;
    						channelsToUpdate.put(priChn.Id, priChn);
    					}
    				}
    			}
    		}
    	}
    	if (channelsToUpdate.isEmpty()==false)
    	update channelsToUpdate.values();
    	
    }
    
    
	/* disabled now, done by VR 
	private void mustSetPrimaryOnFirst(Recruitment_Channel__c[] channels){
		Map<Id,List<Recruitment_Channel__c>> priRecruitChannelByContactId 
			= getExistingPrimaryChannels(channels); 
        
		for (Recruitment_Channel__c channel : channels) 
    	{
    		if (channel.Primary__c == false)
    		{
    			List<Recruitment_Channel__c> priChnList = priRecruitChannelByContactId.get(channel.Contact__c);
    			if (priChnList == null || priChnList.size() == 0 ){
    				channel.addError('First Recruitment Channel has to be set Primary');
    			}
    		}
    	}
	}
	*/
	
	
    //Check for existing Primary Recruitment Channels on Baby Record (for before insert and before update)
    private  Map<Id,List<Recruitment_Channel__c>>  getExistingPrimaryChannels(Recruitment_Channel__c[] channels){
    	Set<Id> contactIds = new Set<Id>();
        for (Recruitment_Channel__c channel : channels) 
        {
            contactIds.add(channel.Contact__c);
        }
        
        Map<Id,List<Recruitment_Channel__c>> priRecruitChannelByContactId = new Map<Id,List<Recruitment_Channel__c>>(); 
        
        for (Recruitment_Channel__c channel  : 
            [SELECT Recruitment_Channel__c.Contact__r.Id, Recruitment_Channel__c.Id
            FROM Recruitment_Channel__c 
            WHERE Recruitment_Channel__c.Primary__c = true
            AND Recruitment_Channel__c.Contact__r.Id IN :contactIds]
            )
        {
        	List<Recruitment_Channel__c> chnList = priRecruitChannelByContactId.get(channel.Contact__c);
        	if (chnList == null){
        		chnList = new List<Recruitment_Channel__c>();
        	}
        	chnList.add(channel);
            priRecruitChannelByContactId.put(channel.Contact__c, chnList);
        }
        
        
        return priRecruitChannelByContactId;
    	
    }
    
    
    private void preventUnsetOnlyPrimary(Recruitment_Channel__c[] newChannels, Map<ID, Recruitment_Channel__c> oldChannelsMap){
    	Map<Id,List<Recruitment_Channel__c>> priRecruitChannelByContactId
    		 = getExistingPrimaryChannels(newChannels); 
        
        //Allow true to false only if there's other Primary Recruitment Channel
        for (Recruitment_Channel__c newChannel : newChannels) 
        {
            Recruitment_Channel__c oldChannel = oldChannelsMap.get(newChannel.id);
            if (oldChannel.Primary__c == true && newChannel.Primary__c == false)
            {
                List<Recruitment_Channel__c> priChnList = priRecruitChannelByContactId.get(oldChannel.Contact__c);
    			if (priChnList != null  && priChnList.size() == 1 && priChnList[0].id == oldChannel.Id){
    				
    				newChannel.addError(CANNOT_UNSET_PRIMARY);
    				
    			}
            }
        }
    }
}