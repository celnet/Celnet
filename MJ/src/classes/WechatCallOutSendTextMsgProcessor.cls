/*
*call services api send message
*/
public class WechatCallOutSendTextMsgProcessor extends WechatCalloutProcessor
{
    public override void DoCallout(Wechat_Callout_Task_Queue__c WechatTask)
    {
        WechatCalloutService wcs = new WechatCalloutService(WechatTask.Public_Account_Name__c);
        wcs.SendMsg(WechatTask.Msg_Body__c);
    }
}