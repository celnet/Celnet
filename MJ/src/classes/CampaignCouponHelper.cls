public with sharing class CampaignCouponHelper {

    
    
    /*
    This Method is called by:
        Campaign Member Trigger (before insert, if campaign type = coupon)
    */
    public static void generateAndAssignCoupons(Map<ID,List<CampaignMember>> campaignId_Members){
    	//Filter for Coupon-related campaigns 
    	Map<Id,Campaign> campaigns = new Map<Id,Campaign>(
            	[SELECT Coupon_Type__c, Status FROM Campaign 
                 WHERE ID in :campaignId_Members.keySet() 
                 	and Coupon_Type__c <> null 
                 	and Type='Coupon']    
            );
         
         //Get Contact-Account Map
         Map<Id,Id> contactAccountMap = 
         getContactAccountIDMap(campaignId_Members, campaigns); 
    	 
    	 for (ID i : campaignId_Members.keySet()){
        	if (campaigns.get(i)==null) continue;
             
    		List<CampaignMember> members = campaignId_Members.get(i);
            
             if (campaigns.get(i).Status == 'Cancelled') {
                 for (CampaignMember member : members)
                 	member.addError('Cannot add member to a cancelled campaign');
                 
                 continue;
             }
            //Generate Coupons by Coupon Type
            String couponType = campaigns.get(i).Coupon_Type__c;
            List<Iterator<Coupon__c>> couponList 
            	= UniqueCodeGenerator.bulkGenerateCouponForCampaignMembers(couponType, members, contactAccountMap);
            
            //Assign coupons to members, and update coupon which account/lead it is assigned to
            for (CampaignMember member : members){
                List<Iterator<Coupon__c>> cpn = couponList;
                if (cpn!=null){
                    if (cpn.size() > 0 && cpn[0]!=null && cpn[0].hasNext()){
                    	Coupon__c c = cpn[0].next();
                        member.Coupon_I__c = c.Id;
                    }
                    if (cpn.size() > 1 && cpn[1]!=null && cpn[1].hasNext()){
                    	Coupon__c c = cpn[1].next();
                        member.Coupon_II__c = c.Id;
                    }
                }
            }
        }
        
    }
    
    //   1. Campaign Trigger (before update, type/coupon change) 
    // returns map of list of list
	public static Map<Id,List<List<Coupon__c>>> generateAllCoupons(Map<Id,Campaign> campaigns){
        Map<Id,List<List<Coupon__c>>> allCoupons = new Map<Id,List<List<Coupon__c>>>();
       
       //Map of Campaign ID to Members
		Map<Id,List<CampaignMember>> campaignId_Members = new  Map<Id,List<CampaignMember>>();
		for (CampaignMember member : [select campaignId, Coupon_I__c, Coupon_II__c, Coupon_Type__c, LeadId, ContactId
        	 from campaignMember WHERE campaignId IN :campaigns.keySet() order by Id asc] ){
        	
        	List<CampaignMember> members = campaignId_Members.get(member.campaignId);
        	if (members == null){
        		 members = new List<CampaignMember>();
        		campaignId_Members.put(member.campaignId, members);
        	}
        	members.add(member);  	
       }
       
        //Get Contact-Account Map
		Map<Id,Id> contactAccountMap = 
         getContactAccountIDMap(campaignId_Members, campaigns); 
         
		for (ID i : campaignId_Members.keySet()){
		if (campaigns.get(i)==null) continue;
		List<CampaignMember> members = campaignId_Members.get(i);

		String couponType = campaigns.get(i).Coupon_Type__c;
        List<Iterator<Coupon__c>> cpn 
           		= UniqueCodeGenerator.bulkGenerateCouponForCampaignMembers(couponType, members, contactAccountMap);
		List<List<Coupon__c>> cpnList = new List<List<Coupon__c>>();
	    for (Iterator<Coupon__c> it : cpn){
	    	List<Coupon__c> lst =  new List<Coupon__c>();
	        while(it.hasNext())
	        	lst.add(it.next());
            	cpnList.add(lst);
	        }
	        allCoupons.put(i,cpnList);
		}
         
       /*
          //Get campaign member count for each campaigns
        Map<Id,Integer> campaignMemberCount = new Map<Id,Integer>();	
		for  (AggregateResult ar : 
      			[select campaignId, count(id) c from campaignMember 
      				WHERE campaignId IN :campaigns.keySet() group by campaignId] ){
         	campaignMemberCount.put( Id.valueOf(String.valueOf(ar.get('campaignId'))) 
               ,  Integer.valueOf(ar.get('c'))  );
     	}
          
        Map<Id,List<List<Coupon__c>>> allCoupons = new Map<Id,List<List<Coupon__c>>>();
        for (ID i : campaignMemberCount.keySet()){
        	if (campaigns.get(i)!=null){
	            Integer qty = campaignMemberCount.get(i);
	            String couponType = campaigns.get(i).Coupon_Type__c;
	            List<Iterator<Coupon__c>> cpn = UniqueCodeGenerator.bulkGenerateCouponByType(couponType, qty);
	            List<List<Coupon__c>> cpnList = new List<List<Coupon__c>>();
	            for (Iterator<Coupon__c> it : cpn){
	            	List<Coupon__c> lst =  new List<Coupon__c>();
	            	while(it.hasNext())
	            		lst.add(it.next());
            		cpnList.add(lst);
	            }
	            allCoupons.put(i,cpnList);
	        }
        }
        */
        return allCoupons;
    }
    

	/** Cancel coupons by Campaign **/
	public static void cancelCampaignCoupon(Map<Id,String> campaignIds_CancelReasons){
		//get all coupon ids (I and II ) of each campaing mebers in the given campaign id
		CampaignMember[] members = [SELECT Id, Coupon_I__c, Coupon_I__r.Id, Coupon_II__c, Coupon_II__r.Id, CampaignId
      									FROM CampaignMember
      									 WHERE CampaignId IN :campaignIds_CancelReasons.keySet() LIMIT 10000];
        Map<Id,String> couponIds_CancelReasons = new Map<Id,String>();
        for (CampaignMember member : members){
            String reason = campaignIds_CancelReasons.get(member.CampaignId);
        	if (member.Coupon_I__c != null)
        	couponIds_CancelReasons.put(member.Coupon_I__r.Id,reason);
        	if (member.Coupon_II__c != null)
        	couponIds_CancelReasons.put(member.Coupon_II__r.Id,reason);
        }
        if (couponIds_CancelReasons.isEmpty()==false)
        	UniqueCodeGenerator.cancelCoupons(couponIds_CancelReasons);
	}
	
	private static Map<ID,ID> getContactAccountIDMap(Map<ID,List<CampaignMember>> campaignId_Members, Map<Id,Campaign> campaigns){
		//Flatten campaignId_Members
		List<CampaignMember> campaignMembers = new List<CampaignMember>();
		for (List<CampaignMember> members : campaignId_Members.values()){
			campaignMembers.addAll(members);
		}
		
		Map<ID,ID> contactAccountMap = new Map<ID,ID>();
		for (CampaignMember member : campaignMembers) 
		{
			Campaign campaign = campaigns.get(member.CampaignId);
			if (campaign != null && campaign.Coupon_Type__c != NULL && member.ContactId != null)
			 {
				contactAccountMap.put(member.ContactId,null);
			 }
		}
		for (Contact c : [Select Account.Id from Contact WHERE id IN :contactAccountMap.keySet() ])
		{
			contactAccountMap.put(c.Id, c.AccountId);
		}
		return contactAccountMap;
	}

}