/*Author:leo.bi@celnet.com.cn
 *Created On:2014-4-17
 *function: test class of SMSSenderController
 *Apply To:CN
 */ 
@isTest
private class SMSSenderControllerTest 
{

     static testMethod void myUnitTest() {
     	//Create custom settings
        TestUtility.setupMemberIDCustomSettings();
        Profile p = [SELECT Id FROM Profile WHERE Name='CN_Inbound Function Admin']; 
        User u = new User(Alias = 'TestLeo', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='testleo@testorg.com');
        System.runAs(u)
        {
			//new Account
			Account acc = new Account();
			acc.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_HouseHold' and SobjectType=:'Account'].id;
			acc.Name = 'HouseHold11';
			acc.Last_Name__c ='HouseHold11';
			acc.Other_Phone_1__c = '15012341234';
			acc.Other_Phone_2__c = '15011111111';
			acc.Mobile_Phone__c = '15411111111';
			acc.Phone = '15711111111';
			insert acc;
			
			//new Lead
			Lead lea = new Lead();
			lea.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Leads' and SobjectType=:'Lead'].id;
			lea.LastName = 'Last Name';
			
			//lea.Lead_source__c = 'Telephone';
			lea.Status = 'Willing to join';
			lea.MobilePhone = '15012341234';
			insert lea;
			
			//new Contact
			Contact con = new Contact();
			con.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
			con.AccountId = acc.Id;
			con.LastName = 'conLast';
			con.Birthdate = Date.today().addDays(-5);
			insert con;
			
			//new smstemplate
			List<SMS_Template__c> list_SMS = new List<SMS_Template__c>();
			SMS_Template__c tem = new SMS_Template__c();
			tem.Title__c = 'title';
			tem.SubTitle__c = 'subtitle';
			tem.Name = 'name';
			tem.Content__c = 'content';
			tem.User_Profile_Type__c = 'ib';
			SMS_Template__c tem2 = new SMS_Template__c();
			tem2.Title__c = 'title';
			tem2.SubTitle__c = 'subtitle';
			tem2.Name = 'name2';
			tem2.Content__c = 'content';
			tem2.User_Profile_Type__c = 'ib';
			list_SMS.add(tem);
			list_SMS.add(tem2);
			insert list_SMS;
			//Run as Contact
			ApexPages.currentPage().getParameters().put('contactId',con.Id);
			SMSSenderController smssc = new SMSSenderController();
			smssc.refreshPhone();
			
			List<SelectOption> temp = new List<SelectOption>();
			
			temp = smssc.titleItems;
			smssc.selectedTitle = tem.Title__c;
			smssc.refreshSubTitle();
			temp = smssc.subtitleItems;
			
			smssc.selectedSubtitle = tem.SubTitle__c;
			smssc.refreshName();
			temp = smssc.SMS_TemplateItems;
			
			smssc.selectedTemplate = tem.Name;
			smssc.refreshContent();
			smssc.templateContent = tem.Content__c + '1111';
			smssc.con = [select Id,LastName,AccountId,Account.Other_Phone_2__c, Account.Other_Phone_1__c,Account.Mobile_Phone__c, Account.Phone
					     From Contact 
					     where LastName =:'conLast'];
			List<SelectOption> testlist = smssc.SMS_TemplateItems;
			testlist = smssc.PhoneItems;
			smssc.receivePhone = 'phone1'; 
			smssc.templateContent = '111111';
			Test.setMock(HttpCalloutMock.class, new SMSProxyTest());
			smssc.sendSMS();
			smssc.sms.Recipient_Contact__c = null;
			smssc.sms.Recipient_Mobile__c = null;
			smssc.sms.Recipient_Lead__c = null;
			smssc.templateContent = null;
			smssc.sendSMS();
			smssc.returnTo(); 
        }
    }
}