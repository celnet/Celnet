@isTest
public class CaseQuestionMassEditExtensionTest{
    
public static testMethod void testCaseQuestionMassEditExtension() 
    {
        // Create a test case
        Case testCase = new Case();
        testCase.type='Enquiry';
        insert testCase;
        
        //Creat Product 1 - MJ & Is_DSO_Product__c = False
        Product__c product1 = new Product__c();
        product1.Name = 'Product 1';
        product1.Product_Code__c = 'Product01';
        product1.Product_Nature__c = 'MJ';
        product1.Is_DSO_Product__c = false;
        insert product1;
        
        //Creat Product 2 - MJ & Is_DSO_Product__c = False
        Product__c product2 = new Product__c();
        product2.Name = 'Product 2';
        product2.Product_Code__c = 'Product02';
        product2.Product_Nature__c = 'MJ';
        product2.Is_DSO_Product__c = false;
        insert product2;
        
        //Creat Product 3 - Non MJ
        Product__c product3 = new Product__c();
        product3.Name = 'Product 3';
        product3.Product_Code__c = 'Product03';
        product3.Product_Nature__c = 'Non MJ';
        product3.Is_DSO_Product__c = false;
        insert product3;
        
        ApexPages.currentPage().getParameters().put('id', testCase.Id);
        
        //Instantiate and construct the controller class.   
        List<Case_Question__c> questionList = new List<Case_Question__c>();
        ApexPages.StandardSetController standardSetController = new ApexPages.StandardSetController(questionList);
        CaseQuestionMassEditExtension controller = new CaseQuestionMassEditExtension(standardSetController);
        
        List<Case_Question__c> questions = controller.getQuestions();
        System.assertEquals(5,questions.size());
        
        // Add 3 new lines
        controller.addNew();
        questions = controller.getQuestions();
        System.assertEquals(8,questions.size());
        
        // Reset
        controller.reset();
        questions = controller.getQuestions();
        System.assertEquals(5,questions.size());
        
        Case parentCase = controller.getParentCase();
        System.assertEquals(testCase.ID,parentCase.ID);
        
        // Cancel
        String nextPage = controller.cancel().getUrl(); 
        System.assertEquals('/'+testCase.ID, nextPage);
        
        //Instantiate a new controller
        controller = new CaseQuestionMassEditExtension(new ApexPages.StandardSetController(questionList)); 
        
        //Test Case - Verify List Options
        List<SelectOption> options = controller.getMJNProducts();
        System.assertEquals(3,options.size());

        questions = controller.getQuestions();
        
        //Create a case question
        Case_Question__c question = questions.get(1);
        question.Type__c = 'Enquiry';
        question.Sub_Type__c  = 'Health';
        question.Question__c = 'TEST Question';
        question.Product__c = product1.ID;
        questions.set(1, question);
        
        nextPage = controller.save().getUrl();
        System.assertEquals('/'+testCase.ID, nextPage);
        Integer count = [SELECT COUNT() FROM Case_Question__c WHERE Case__c = :testCase.ID];
        System.assertEquals(1, count);
        
        //Test Edit Exsiting Record
        controller = new CaseQuestionMassEditExtension(new ApexPages.StandardSetController(questionList)); 
        questions = controller.getQuestions();
        
        //Edit Existing Question
        Case_Question__c existingQuestion = questions.get(0);
        existingQuestion.Remark__c = 'Updated';
        questions.set(0, existingQuestion);
        
        //Create a new case question
        Case_Question__c question2 = questions.get(1);
        question2.Type__c = 'Compliant';
        question2.Sub_Type__c  = 'Product';
        question2.Question__c = 'TEST Question';
        question2.Product__c = product2.ID;
        questions.set(1, question2);
        
        nextPage = controller.save().getUrl();
        System.assertEquals('/'+testCase.ID, nextPage);
        count = [SELECT COUNT() FROM Case_Question__c WHERE Case__c = :testCase.ID];
        System.assertEquals(2, count);
   }
}