/*
Author: scott.zhang@celnet.com.cn 
Created On: 2014-05-12
Function: Generate and assign Q-Task
Support Event: BeforeInsert BeforeUpdate
Apply To: CN 
*/
public class QTaskDistributorHandler implements Triggers.Handler 
{
	private Map<String,Group> Map_Queue;
	public void Handle()
	{
		if(!Context.ApplyTo(new String[] {'CN'})) 
    	{
    		return;
		}
		if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate))
		{
			if(trigger.new[0] instanceof Q_Task__c)
			{
				this.AssignToQueue();
			}
		}
	}
	private void AssignToQueue() 
	{
		
		Set<Id> Set_ConId = new Set<Id>();
		List<Q_Task__c> List_QTask = trigger.new;
		for(Q_Task__c qtask : List_QTask)
		{
			Boolean IsInsert =(trigger.isInsert?true:false);
			Boolean IsUpdate =(trigger.isUpdate?true:false);
			Q_Task__c oldqtask = (IsUpdate?(Q_Task__c)trigger.oldMap.get(qtask.Id):new Q_Task__c());
			
			//Assign
			if((IsInsert && qtask.Contact__c !=null) ||(IsUpdate && (qtask.Type__c != oldqtask.Type__c || qtask.Business_Type__c != oldqtask.Business_Type__c)))//
			{
				Set_ConId.add(qtask.Contact__c);
			} 
			//Clear My_Last_Call_Result__c
			if(IsUpdate && (qtask.OwnerId != oldqtask.OwnerId))
			{
				qtask.My_Last_Call_Result__c = null;
			}
		}
		
		if(Set_ConId.size()==0)
		{
			return;
		}
		
		Map_Queue = new Map<String,Group>();
		for(Group Gr : [Select Type,Name,Id,DeveloperName From Group where Type='Queue' ])
		{
			Map_Queue.put(Gr.DeveloperName,Gr);
		}
		
		
		for(Contact con : [select Id,OwnerId,Account.Province__c,Account.Sales_City__c,Account.Area__c,Account.Sub_Region__c,Account.Standard_City__r.Name,Account.Standard_City__r.Region__c,Account.Administrative_Area__c,Specific_Code__c,Is_Winning__c,Account.status__c,
						   Last_Routine_Call_Result__c,Last_Routine_Call_Time__c,Is_Last_Digestive_Symptoms__c,Digestive_Symptoms_Type__c,
						   Primary_RecruitChannel_Type__c,Primary_RecruitChannel_Sub_type__c,Birthdate,S0_Sum__c,S2_Sum__c,S3_Sum__c,
						   Is_Last_Allergy_Symptoms__c,Allergy_Symptoms_Type__c,Archive_Month__c from Contact where id in:Set_ConId])
		{
			//Last_Routine_Call_Result__c,Last_Routine_Call_Time__c,
			for(Q_Task__c qtask : List_QTask)
			{
				if(qtask.Contact__c == con.Id)
				{
					qtask.Region__c = con.Account.Standard_City__r.Region__c;
					qtask.Sub_Region__c = con.Account.Sub_Region__c;
					qtask.Area__c = con.Account.Area__c; 
					qtask.Sales_City__c = con.Account.Sales_City__c;
					qtask.Province__c = con.Account.Province__c;
					qtask.City__c = con.Account.Standard_City__r.Name;
					qtask.Administrative_Area__c = con.Account.Administrative_Area__c;
					
					qtask.Archive_Month__c = con.Archive_Month__c;
					qtask.Specific_Code__c = con.Specific_Code__c;
					qtask.Last_Routine_Call_Result__c = con.Last_Routine_Call_Result__c;
					qtask.Last_Routine_Call_Time__c = con.Last_Routine_Call_Time__c;
					qtask.Is_Win__c = con.Is_Winning__c;
					qtask.Member_Status__c = con.Account.status__c;
					qtask.Digestive_Symptoms__c = con.Is_Last_Digestive_Symptoms__c;
					qtask.Digestive_Symptoms_Type__c = con.Digestive_Symptoms_Type__c;
					qtask.Allergy_Symptoms__c = con.Is_Last_Allergy_Symptoms__c;
					qtask.Symptoms_Type__c = con.Allergy_Symptoms_Type__c;
					qtask.Primary_RecruitChannel_Type__c = con.Primary_RecruitChannel_Type__c;
					qtask.Primary_RecruitChannel_Sub_type__c = con.Primary_RecruitChannel_Sub_type__c;
					qtask.Birthdate__c = con.Birthdate;
					qtask.Is_Distribute_S0__c = (con.S0_Sum__c!=null&& con.S0_Sum__c>0?'Y':'N');
					qtask.Is_Distribute_S2__c = (con.S2_Sum__c!=null&& con.S2_Sum__c>0?'Y':'N');
					qtask.Is_Distribute_S3__c = (con.S3_Sum__c!=null&& con.S3_Sum__c>0?'Y':'N');
					
					Group queue = this.GetQueue(qtask.Region__c,qtask.Type__c,qtask.Business_Type__c);
					qtask.OwnerId = queue.Id;
					qtask.Original_Queue__c = queue.Id;
					break;
				} 
			}
		}
	}
	private Group GetQueue(String Region,String Type,String BusinessType)
	{
		Group Rtu = (Map_Queue.containsKey('CN_Undefined')?Map_Queue.get('CN_Undefined'):null);
		String RegionFlag;
		if(Region == '华东区'){RegionFlag = 'EC';}
		else if(Region == '华南区'){RegionFlag = 'SC';}
		else if(Region == '华西区'){RegionFlag = 'WC';}
		else if(Region == '华北区'){RegionFlag = 'NC';}
		
		String QueueName;
		if(Type == 'Routine' && BusinessType == 'Routine Greet'){QueueName = 'CN_Routine_A_Greet';}
		else if(Type == 'Routine' && BusinessType == 'Routine Reciprocal'){QueueName ='CN_Routine_A_Reciprocal';}
		else if(Type == 'Routine' && BusinessType == 'Routine Distribution'){QueueName ='CN_Routine_Distribution';}
		else if(Type == 'Invitation' && (BusinessType == 'Invitation' || BusinessType == 'Invitation Reciprocal')){QueueName ='CN_Invitation';}
		else if(Type == 'Invitation' && BusinessType == 'Invitation Special'){QueueName ='CN_Special';}
		else if(Type == 'Elite' ){QueueName ='CN_Elite';}
		
		String QueueDevName = QueueName+'_'+RegionFlag;
		Rtu = (Map_Queue.containsKey(QueueDevName)?Map_Queue.get(QueueDevName):Rtu);
		
		return Rtu;
	}
}