/*Author:leo.bi@celnet.com.cn
 *CreateDate:2014-06-05
 *function:Test MassFinishCampaignCall and downgrade qtask
 */
@isTest(SeeAllData = true)
private class MassFinishCampaignCallBatchTest 
{
    static testMethod void myUnitTest() 
    {
    	User u = TestUtility.new_CN_User();
    	system.runas(u)   
        {
        	system.debug('-------------------------------' + UserInfo.getName());
        	system.debug('-------------------------------' + u); 
		    String commenName = 'LeoTest' + String.valueOf(DateTime.now());
		    //new campaign invitation special
		    Campaign c = new Campaign();
		    c.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Invitation_Special' and SobjectType=:'Campaign'].id;
		    c.Name = commenName;
		    c.Description = 'Test Description';
		    c.Campaign_Type__c = 'Invitation Special';
		    insert c;
		    //new Account
			Account acc = new Account();
			acc.Name = commenName;
			acc.Last_Name__c =commenName;
			acc.status__c = 'Mother';
			acc.Phone = '11000000000';
			insert acc;
		    //new Contact
		    Contact con = new Contact();
			con.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
			con.AccountId = acc.Id;
			con.LastName = commenName;
			con.Birthdate = Date.today().addDays(-5);
			con.Register_Date__c = Date.today().addDays(-4);
			insert con;
			con = [select Id,Questionnaire__c from Contact where Id = :con.Id];
			Answer__c ans = new Answer__c();
			ans.Questionnaire__c = con.Questionnaire__c;
			ans.Contact__c = con.Id;
			ans.Status__c = 'Draft';
			insert ans;
		    //new campainmember Waiting for invitation
		    CampaignMember cm = new CampaignMember();
		    cm.ContactId = con.Id;
		    cm.Call_Status__c = 'Waiting Invitation';
		    cm.Status = 'Sent';
		    cm.CampaignId = c.Id; 
		    insert cm;
		    //new campaign invitation special
		    Campaign c2 = new Campaign();
		    c2.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Invitation' and SobjectType=:'Campaign'].id;
		    c2.Name = commenName;
		    c2.Description = 'Test Description';
		    c2.Campaign_Type__c = 'Invitation';
		    insert c2;
		    //new campainmember Waiting for invitation
		    CampaignMember cm2 = new CampaignMember();
		    cm2.ContactId = con.Id;
		    cm2.Call_Status__c = 'Waiting Invitation';
		    cm2.Status = 'Sent';
		    cm2.CampaignId = c2.Id; 
		    insert cm2;
		    //now there is a elite qtask with invitation、special invitation and routine
		    Test.startTest();
		    MassFinishCampaignCallBatch mfccb = new MassFinishCampaignCallBatch(c.Id);
		    DataBase.executeBatch(mfccb);
		    MassFinishCampaignCallWebService.ExetuteBatch(c2.Id);
		    Test.stopTest();
        }
    }
    static testMethod void myUnitTest2() 
    {
    	system.runas(TestUtility.new_CN_User())
        {
	        String commenName = 'LeoTest' + String.valueOf(DateTime.now());
	        //new campaign invitation special
	        Campaign c = new Campaign();
	        c.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Invitation_Special' and SobjectType=:'Campaign'].id;
	        c.Name = commenName;
	        c.Description = 'Test Description';
	        c.Campaign_Type__c = 'Invitation Special';
	        insert c;
	        //new Account
			Account acc = new Account();
			acc.Name = commenName;
			acc.Last_Name__c =commenName;
			acc.status__c = 'Mother';
			acc.Phone = '11000000000';
			insert acc;
	        //new Contact
	        Contact con = new Contact();
			con.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
			con.AccountId = acc.Id;
			con.LastName = commenName;
			con.Birthdate = Date.today().addDays(-5);
			con.Register_Date__c = Date.today().addDays(-4);
			insert con;
			con = [select Id,Questionnaire__c from Contact where Id = :con.Id];
			con.Questionnaire__c = null;
			update con;
	        //new campainmember Waiting for invitation
	        CampaignMember cm = new CampaignMember();
	        cm.ContactId = con.Id;
	        cm.Call_Status__c = 'Waiting Invitation';
	        cm.Status = 'Sent';
	        cm.CampaignId = c.Id; 
	        insert cm;
	        //new campaign invitation special
	        Campaign c2 = new Campaign();
	        c2.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Invitation' and SobjectType=:'Campaign'].id;
	        c2.Name = commenName;
	        c2.Description = 'Test Description';
	        c2.Campaign_Type__c = 'Invitation';
	        insert c2;
	        //new campainmember Waiting for invitation
	        CampaignMember cm2 = new CampaignMember();
	        cm2.ContactId = con.Id;
	        cm2.Call_Status__c = 'Waiting Invitation';
	        cm2.Status = 'Sent';
	        cm2.CampaignId = c2.Id; 
	        insert cm2;
	        //now there is a elite qtask with invitation、special invitation and routine
	        Test.startTest();
	        MassFinishCampaignCallBatch mfccb = new MassFinishCampaignCallBatch(c.Id);
	        DataBase.executeBatch(mfccb);
	        MassFinishCampaignCallBatch mfccb2 = new MassFinishCampaignCallBatch(c2.Id);
	        DataBase.executeBatch(mfccb2);
	        Test.stopTest();
    	}  
    }
}