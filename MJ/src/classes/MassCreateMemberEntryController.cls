/*
Author: scott.zhang@celnet.com.cn
Created On: 2014-04-16
Function: Mass Create Member Entry
Apply To: CN
*/
public  class MassCreateMemberEntryController {
	public List<ObjMember> List_ObjMembers{get;set;}
	public Member_Entry__c PublicMember{get;set;}
	public Integer DelRowId{get;set;}
	public Boolean SaveFlag{get;set;}
	public List<SelectOption> RegisterSourceList{get;set;}
	public List<SelectOption> RegisterSubSourceList{get;set;}
	public Map<String,Set<String>> Map_Register{get;set;} 
	public Map<String,List<SelectOption>> Map_Source{get;set;}
	public Boolean SpecificCodeConfirmShow{get;set;}
	public Boolean DoNotCheckSpecificCode{get;set;}
	public Boolean PhoneConfirmShow{get;set;}
	public Boolean DoNotCheckPhone{get;set;}
	final String FieldRequired = System.Label.Field_Required;
	final String IstatusNew='New';
	final Set<String> Set_FilterValue = new Set<String>{'短信','互联网','外呼手工创建','NC User 手工/纸质Store'};
	public MassCreateMemberEntryController(ApexPages.StandardController controller)
	{
		DoNotCheckSpecificCode = false;
		DoNotCheckPhone = false;
		List_ObjMembers = new List<ObjMember>();
		PublicMember = new Member_Entry__c();
		
		for(Integer i=0;i<5;i++)
		{
			Member_Entry__c Entry = new Member_Entry__c();
			ObjMember Member = new ObjMember(); 
			Member.MemberEntry = Entry;
			Member.RowId=i;
			List_ObjMembers.add(Member);
		}
		this.CrtRegisterSelectOption();
	}
	public void CrtRegisterSelectOption()
	{
		//Register Source Mapping
		Map_Register = new Map<String,Set<String>>();
		for(BF_Register_Source_Mapping__c RS : BF_Register_Source_Mapping__c.getAll().values())
		{
			if(Set_FilterValue.contains(RS.Register_Source_Label__c))
			{
				continue;
			}
			
			if(Map_Register.containsKey(RS.Register_Source_Label__c))
			{
				Set<String> Set_SubSource = Map_Register.get(RS.Register_Source_Label__c);
				Set_SubSource.add(RS.Register_Sub_Source_Label__c);
				Map_Register.put(RS.Register_Source_Label__c,Set_SubSource);
			}
			else
			{
				Set<String> Set_SubSource = new Set<String>();
				if(RS.Register_Sub_Source_Label__c !=null)
				Set_SubSource.add(RS.Register_Sub_Source_Label__c);
				Map_Register.put(RS.Register_Source_Label__c,Set_SubSource);
			}
		}
		RegisterSourceList = new List<SelectOption>();
		RegisterSourceList.add(new SelectOption('',''));
		Map_Source =  new Map<String,List<SelectOption>>();
		for(String ReSource : Map_Register.KeySet())
		{
			RegisterSourceList.add(new SelectOption(ReSource,ReSource));
			List<SelectOption> List_SO = new List<SelectOption>();
			List_SO.add(new SelectOption('',''));
			for(String ReSubSurce : Map_Register.get(ReSource))
			{
				if(ReSubSurce.Contains('订阅') || ReSubSurce.Contains('PP-class'))
				{
					continue;
				}
				List_SO.add(new SelectOption(ReSubSurce,ReSubSurce));
			}
			Map_Source.put(ReSource,List_SO);
		}
		if(RegisterSourceList.size()>0)
		this.AutoShowSubSource();		
	}
	public void AutoShowSubSource()
	{
		RegisterSubSourceList = new List<SelectOption>();
		if(Map_Source.containsKey(PublicMember.Register_Source__c))
		RegisterSubSourceList = Map_Source.get(PublicMember.Register_Source__c);
	}
	public void SaveMember()
	{
		SpecificCodeConfirmShow = false;
		PhoneConfirmShow = false;
		List<Member_Entry__c> List_InMemberEntry = new List<Member_Entry__c>();
		try
		{
			//Verification
			if(this.Verification())
			{
				SaveFlag = false;
				return;
			}
			
			Integer flag = 0;
			Integer PhoneCheckFlag = 0;
			for(ObjMember Obj:List_ObjMembers)
			{
				Member_Entry__c Entry = Obj.MemberEntry;
				
				if(Entry.Name == null )
				{
					continue;
				}
				
				if(Entry.Name == null && (Entry.Phone__c != null || Entry.Email__c != null || Entry.Baby1_Birthday__c != null || Entry.Address_Street__c != null))
				{
					Entry.Name.addError(FieldRequired);
					return;
				}
				else if(Entry.Name != null)
				{
					if(Entry.Phone__c == null)
					{
						Entry.Phone__c.addError(FieldRequired);
						return;
					}
					else if(Entry.Phone__c != null && !DoNotCheckPhone && (!(Pattern.matches('^[0-9]{12}$',Entry.Phone__c)||Pattern.matches('^[0-9]{11}$',Entry.Phone__c) || Pattern.matches('^[0-9]{8}$',Entry.Phone__c) || Pattern.matches('^[0-9]{7}$',Entry.Phone__c))))
					{
						Entry.Phone__c.addError(System.Label.MemberEntry_PhoneError);
						PhoneCheckFlag++;
					}  
					if(Entry.Baby1_Birthday__c == null)
					{
						Entry.Baby1_Birthday__c.addError(FieldRequired);
						return;
					}
					
				}
				
				Entry.I_Status__c = IstatusNew;
				Entry.Administrative_Area__c = PublicMember.Administrative_Area__c;
				Entry.Register_Date__c = PublicMember.Register_Date__c;
				Entry.Specific_Code__c = PublicMember.Specific_Code__c;
				Entry.Sales_Rep__c = PublicMember.Sales_Rep__c;
				Entry.Archive_Month__c = PublicMember.Archive_Month__c;
				Entry.Register_Source__c = PublicMember.Register_Source__c;
				Entry.Register_Sub_Source__c = PublicMember.Register_Sub_Source__c;
				List_InMemberEntry.add(Entry);
				flag++;
			}
			
			if(flag==0)
			{
				List_ObjMembers[0].MemberEntry.Name.addError(FieldRequired);
    			return;
			}
			
			if(PhoneCheckFlag>0 && !DoNotCheckPhone)
			{
				PhoneConfirmShow = true;
				//return;
			}
			
			
			
			List<Account> List_Acc =[ select Id from Account where Specific_Code__c =:PublicMember.Specific_Code__c 
									 and Specific_Code__c !=null and Standard_City__c =:PublicMember.Administrative_Area__c];
			if((List_Acc == null || List_Acc.size()==0) && !DoNotCheckSpecificCode)
			{
				PublicMember.Specific_Code__c.addError(System.Label.MemberEntry_CodeError);
				SpecificCodeConfirmShow = true; 
				//return ;
			}
			
			if(PhoneConfirmShow || SpecificCodeConfirmShow)
			{
				return;
			}
			
			if(List_InMemberEntry.size()>0)
			{
				List<Member_Entry__c> List_InME = new List<Member_Entry__c>();
				String  AreaCode = this.GetPhoneAreaCode(PublicMember.Administrative_Area__c);
				for(Member_Entry__c Entry:List_InMemberEntry)
				{
					 if(Entry.Phone__c != null && (Pattern.matches('^[0-9]{8}$',Entry.Phone__c) || Pattern.matches('^[0-9]{7}$',Entry.Phone__c)) && AreaCode != null)
					 Entry.Phone__c = AreaCode+Entry.Phone__c;
					 List_InME.add(Entry);
				}
				insert List_InME;
				SaveFlag = true;
			}
			
		}catch(Exception e)
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, 'Error:'+e.getmessage()+' LineNumber:'+e.getLineNumber());    		
    		ApexPages.addMessage(msg);
    		return;
		}
		//this.PageInitialize();
	}
	public void AddMember()
	{
		for(Integer i=1;i<=5;i++)
		{
			Member_Entry__c Entry = new Member_Entry__c();
			ObjMember Member = new ObjMember(); 
			Member.MemberEntry = Entry;
			Member.RowId = List_ObjMembers.size();
			List_ObjMembers.add(Member);
		}
		
	}
	public void CancelRow()
	{
		List<ObjMember> List_NewObjMembers = new List<ObjMember>();
		try
		{
			for(ObjMember Obj:List_ObjMembers)
			{
				if(DelRowId==null)
				{
					return;
				}
				if(Obj.RowId == DelRowId)
				{
					continue;
				}
				List_NewObjMembers.add(Obj);
			}
			List_ObjMembers.clear();
			List_ObjMembers = List_NewObjMembers;
		}catch(Exception e)
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, 'Error:'+e.getmessage()+' LineNumber:'+e.getLineNumber());    		
    		ApexPages.addMessage(msg);
    		return;
		}
		
	}
	public Boolean Verification()
	{
		
		if(PublicMember.Administrative_Area__c == null)
		{
			PublicMember.Administrative_Area__c.addError(FieldRequired);
			return true;
		}
		else if(PublicMember.Register_Date__c == null)
		{
			PublicMember.Register_Date__c.addError(FieldRequired);
			return true;
		}
		else if(PublicMember.Specific_Code__c == null)
		{
			PublicMember.Specific_Code__c.addError(FieldRequired);
			return true;
		}
		else if(PublicMember.Archive_Month__c == null)
		{
			PublicMember.Archive_Month__c.addError(FieldRequired);
			return true;
		}
		else  if(!Pattern.matches('^[1-9][0-9]{3}([0][0-9]|([1][0-2]))$',PublicMember.Archive_Month__c))
		{
			PublicMember.Archive_Month__c.addError(FieldRequired+'" YYYYMM"');
			return true;
		}
		else
		{
			return false;
		}
	}
	public String GetPhoneAreaCode(Id AdministrativeAreaId)
	{
		String Rtu =null;
		for(Address_Management__c am : [select Phone_Area_Code__c from Address_Management__c where Id =:AdministrativeAreaId])
		{
			Rtu = (am.Phone_Area_Code__c!=null&&am.Phone_Area_Code__c!=''?am.Phone_Area_Code__c:null);
		}
		return Rtu;
	}
	
	public class ObjMember
	{
		public Integer RowId{get;set;}
		public Member_Entry__c MemberEntry{get;set;}
	}
}