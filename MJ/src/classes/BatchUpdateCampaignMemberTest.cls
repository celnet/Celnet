/*
	Apply to [HK/CN/ALL]: HK 
*/
@isTest
private class BatchUpdateCampaignMemberTest {
	
	static void initSettings(){
       	//Creat test custom setting 1 for coupon
		Coupon_ID_Setting__c couponSetting = new Coupon_ID_Setting__c();
        couponSetting.Name = 'TEST-COUPON1';
        couponSetting.Next_ID__c = 1234;
        couponSetting.Prefix__c = 'SS';
        couponSetting.Postfix__c = 'AA';
        insert couponSetting;
        
        //Creat test custom setting 2 for coupon
		Coupon_ID_Setting__c couponSetting2 = new Coupon_ID_Setting__c();
        couponSetting2.Name = 'TEST-COUPON2';
        couponSetting2.Next_ID__c = 1888;
        couponSetting2.Prefix__c = 'KK';
        couponSetting2.Postfix__c = 'AA;BB';
        insert couponSetting2;
        
        //create mapping custom settings
    	List<Campaign_Member_Status_Mapping__c> settings = new List<Campaign_Member_Status_Mapping__c>();
    	Campaign_Member_Status_Mapping__c s1 = new Campaign_Member_Status_Mapping__c();
    	s1.Name = 'Test Setting 1';
    	s1.Campaign_Status__c='Completed';
    	s1.Campaign_Member_Status__c='Responded';
    	settings.add(s1);
    	insert settings;
     }
     
 	static testMethod void updateStatusTest() {
 	  system.runas(TestUtility.new_HK_User()){
 		initSettings();
    	//create campaigns
    	List<Campaign> campaigns = TestUtility.generateTestCampaigns(1);
    	    
    	//create contacts
    	Contact[] c1 = TestUtility.generateTestContacts(200);
    	//create campaign members
    	List<CampaignMember> campaignmembers = new List<CampaignMember>();
      	for(Contact c: c1){
         	CampaignMember member = new CampaignMember();
            member.CampaignId = campaigns[0].Id;
            member.ContactId = c.id;         
            member.Status = 'Sent';     
            campaignmembers.add(member);
        }
       insert campaignmembers;
    	
    	Test.startTest();
    	//assert cm status before c status update
    	system.assertEquals(campaigns[0].Status, 'Planned');
    	system.assertEquals(campaignmembers[0].Status, 'Sent');
    	
    	//simulate update
    	campaigns[0].Status = 'Completed';
    	update campaigns[0];
    	
    	// assert batch running >>cm status change.
    	Test.stopTest();
    	
    	system.assertEquals(campaigns[0].Status, 'Completed');
    	system.assertEquals([SELECT status from CampaignMember LIMIT 1][0].Status, 'Responded');
 	  }
    }
    
    static testMethod void batchUpdateNonCouponToCouponTest() {
      system.runas(TestUtility.new_HK_User()){
    	initSettings();
        
    	//create campaign
    	Campaign camp = new Campaign();
        camp.Name = 'Test Camp';
        camp.Coupon_Type__c=null;
        camp.Type = 'Non-Coupon';
        camp.IsActive = true;
        insert camp;
    	    
    	//create contacts
    	Contact[] c1 = TestUtility.generateTestContacts(200);
    	//create campaign members
    	List<CampaignMember> campaignmembers = new List<CampaignMember>();
      	for(Contact c: c1){
         	CampaignMember member = new CampaignMember();
            member.CampaignId = camp.Id;
            member.ContactId = c.id;    
            campaignmembers.add(member);
        }
       insert campaignmembers;
       
    	Test.startTest();
    	//assert cm cpn null before c type change
    	system.assertEquals(campaignmembers[0].Coupon_I__c, null);
    	//simulate update
    	camp.Coupon_Type__c='TEST-COUPON2';
        camp.Type = 'Coupon';
    	update camp;
    	
    	// assert batch running >>cm status change.
    	Test.stopTest();
    	List<CampaignMember> members = [SELECT Coupon_I__c, Coupon_II__c from CampaignMember LIMIT 2];
    	system.assertNotEquals(members[0].Coupon_I__c, null);
    	system.assertNotEquals(members[0].Coupon_II__c, null);
    	system.assertNotEquals(members[0].Coupon_I__c, members[1].Coupon_I__c);
      }
     }
     
     static testMethod void batchUpdateCouponToNonCouponTest() {
      system.runas(TestUtility.new_HK_User()){	
    	initSettings();
        
    	//create campaign
    	Campaign camp = new Campaign();
        camp.Name = 'Test Camp';
        camp.Coupon_Type__c='TEST-COUPON1';
        camp.Type = 'Coupon';
        camp.IsActive = true;
        insert camp;
    	    
    	//create contacts
    	Contact[] c1 = TestUtility.generateTestContacts(200);
    	//create campaign members
    	List<CampaignMember> campaignmembers = new List<CampaignMember>();
      	for(Contact c: c1){
         	CampaignMember member = new CampaignMember();
            member.CampaignId = camp.Id;
            member.ContactId = c.id;    
            campaignmembers.add(member);
        }
       insert campaignmembers;
       List<CampaignMember> members = [SELECT Coupon_I__c from CampaignMember LIMIT 2];
    	Test.startTest();
    	//assert cm cpn not null before c type change
    	system.assertNotEquals(members[0].Coupon_I__c, null);
    	//simulate update
    	camp.Coupon_Type__c=null;
        camp.Type = 'Non-Coupon';
    	update camp;
    	
    	// assert batch running >>cm status change.
    	Test.stopTest();
    	members = [SELECT Coupon_I__c from CampaignMember LIMIT 2];
    	system.assertEquals(members[0].Coupon_I__c, null);
    	//assert coupon cancalled
    	Coupon__c[] cpn =[SELECT Status__c, Cancel_Reason__c FROM Coupon__c];
    	system.assertEquals('Cancelled',cpn[0].Status__c);
    	system.assertEquals(CommonHelper.COUPON_TYPE_CHANGED ,cpn[0].Cancel_Reason__c); 
      }
     }
     
    static testMethod void batchUpdateCouponCampaignCancelled() {
      system.runas(TestUtility.new_HK_User()){
    	initSettings();
        
    	//create campaign
    	Campaign camp = new Campaign();
        camp.Name = 'Test Camp';
        camp.Coupon_Type__c='TEST-COUPON1';
        camp.Type = 'Coupon';
        camp.IsActive = true;
        insert camp;
    	    
    	//create contacts
    	Contact[] c1 = TestUtility.generateTestContacts(200);
    	//create campaign members
    	List<CampaignMember> campaignmembers = new List<CampaignMember>();
      	for(Contact c: c1){
         	CampaignMember member = new CampaignMember();
            member.CampaignId = camp.Id;
            member.ContactId = c.id;    
            campaignmembers.add(member);
        }
       insert campaignmembers;
       List<CampaignMember> members = [SELECT Coupon_I__c from CampaignMember LIMIT 2];
    	Test.startTest();
    	//assert cm cpn not null before c type change
    	system.assertNotEquals(members[0].Coupon_I__c, null);
    	//simulate update
    	//camp.Coupon_Type__c=null;
        camp.Status= 'Cancelled';
    	update camp;
    	
    	// assert batch running >>cm status change.
    	Test.stopTest();
    	members = [SELECT Coupon_I__c from CampaignMember LIMIT 2];
    	system.assertEquals(members[0].Coupon_I__c, null);
    	//assert coupon cancalled
    	Coupon__c[] cpn =[SELECT Status__c, Cancel_Reason__c FROM Coupon__c];
    	system.assertEquals('Cancelled',cpn[0].Status__c);
    	system.assertEquals(CommonHelper.COUPON_CANCELLED ,cpn[0].Cancel_Reason__c); 
     }
    }
        
        
}