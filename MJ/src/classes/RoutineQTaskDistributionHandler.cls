/*
Author: scott.zhang@celnet.com.cn 
Created On: 2014-05-08
Function: Listen New Baby (After Contact Insert),  launch real time Evaluation Flow for Greet
Support Event: AfterInsert
Apply To: CN 
*/
public class RoutineQTaskDistributionHandler implements Triggers.Handler 
{
	public void Handle()
	{
		if(!Context.ApplyTo(new String[] {'CN'})) 
    	{
    		return;
		}
		if(trigger.isAfter && trigger.isInsert)
		{
			if(trigger.new[0] instanceof Contact)
			{
				this.EvaluationGreet();
			}
		}
	}
	private void EvaluationGreet()
	{
		List<Contact> conList = trigger.new;
		Id RecordTypeId = ObjectUtil.GetRecordTypeID(Contact.getSObjectType(), 'CN_Child');
		Set<Id> Set_ConIds = new Set<Id>();
		for(Contact con : conList)
		{
			if(con.RecordTypeId != RecordTypeId || con.Inactive__c || con.Duplicate_Status__c != null || con.Cannot_Be_Contacted__c )
			{
				continue;
			}
			Set_ConIds.add(con.Id);
		}
		if(Set_ConIds.size()==0)
		{
			return;
		}
		
		List<Contact> List_Contact =[Select CN_Flag__c,Questionnaire__c,Id,Last_Routine_Call_Time__c,Cannot_Be_Contacted__c,Verified__c,Register_Date__c,Birthdate,Routine_Evaluated_On__c,Next_Routine_Evaluation_Date__c,Appended_Log__c,Duplicate_Status__c,Inactive__c,RecordTypeId,
									(Select Questionnaire__c,Questionnaire__r.Business_Type__c,Finished_On__c From Answers__r where Status__c ='Finish' and Finished_On__c !=null) 
									from Contact where Id in:Set_ConIds];
		if(List_Contact != null && List_Contact.size()>0)
		{
			RoutineEvaluator RoutineEva = new RoutineEvaluator(List_Contact,NULL);
			RoutineEva.TimingType = 'Real Time'; 
			RoutineEva.Run();
		}
	}
}