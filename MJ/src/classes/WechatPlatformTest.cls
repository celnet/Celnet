/**
 * 
 *
 *
 */
@isTest
private class WechatPlatformTest {
    
    static testMethod void EntityTest() 
    {
         WechatEntity we = new WechatEntity(); 
         
         string xml1 = BuildCallinTextMsg('text');
         we.DeserializeCallInRequestMsg(xml1);
         string xml2 = BuildCallinTextMsg('image');
         we.DeserializeCallInRequestMsg(xml2);
         string xml3 = BuildCallinTextMsg('voice');
         we.DeserializeCallInRequestMsg(xml3);
         string xml4 = BuildCallinTextMsg('event');
         we.DeserializeCallInRequestMsg(xml4);
         
         WechatEntity.OutTextMsg msg1 = new WechatEntity.OutTextMsg();
         msg1.Content = 'content';
         msg1.ToUserName = 'toUserName';
         msg1.FromUserName = 'fromUserName';
         msg1.CreateTime = datetime.now();
         msg1.MsgType = 'text';
         we.SerializeCallinResponseMsg(msg1);
         WechatEntity.OutImageMsg msg2 = new WechatEntity.OutImageMsg();
         msg2.MediaId = '123';
         msg2.ToUserName = 'toUserName';
         msg2.FromUserName = 'fromUserName';
         msg2.CreateTime = datetime.now();
         msg2.MsgType = 'image';
         we.SerializeCallinResponseMsg(msg2);
         WechatEntity.OutTextMsg msg3 = new WechatEntity.OutTextMsg();
         msg3.Content = '1235';
         msg3.ToUserName = 'toUserName';
         msg3.FromUserName = 'fromUserName';
         msg3.CreateTime = datetime.now();
         msg3.MsgType = 'transfer_customer_service';
         we.SerializeCallinResponseMsg(msg3);
         WechatEntity.OutRichMediaMsg msg4 = new WechatEntity.OutRichMediaMsg();
         list<WechatEntity.Article> Articles = new list<WechatEntity.Article>();
         msg4.Articles = Articles;
         WechatEntity.Article art = new WechatEntity.Article();
         art.title = 'title';
         art.description = 'description';
         art.picURL = 'picURL';
         art.url = 'url';
         msg4.ToUserName = 'toUserName';
         msg4.FromUserName = 'fromUserName';
         msg4.CreateTime = datetime.now();
         msg4.MsgType = 'news';
         we.SerializeCallinResponseMsg(msg4);
         
         WechatEntity.SvcTextMsg svc1 = new WechatEntity.SvcTextMsg();
         WechatEntity.Text svc2 = new WechatEntity.Text();
         WechatEntity.SvcImageMsg svc3 = new WechatEntity.SvcImageMsg();
         WechatEntity.Image svc4 = new WechatEntity.Image();
         WechatEntity.News svc5 = new WechatEntity.News();
         WechatEntity.File svc6 = new WechatEntity.File();
         WechatEntity.User svc7 = new WechatEntity.User();
    }
    
    
    static testMethod void WechatCallOutQueueManagerTest()
    {
        WechatPlatformRequestMock fakeResponse = new WechatPlatformRequestMock();
        system.Test.setMock(HttpCalloutMock.class, fakeResponse);
        Wechat_Callout_Task_Queue__c textQueue = GenerateWechatCalloutTaskQueue(false);
        Wechat_Callout_Task_Queue__c textQueue2 = GenerateWechatCalloutTaskQueue(false);
        WechatCalloutQueueManager.EnQueue(textQueue);
        WechatCalloutQueueManager.EnQueue(new List<Wechat_Callout_Task_Queue__c>{textQueue2});
        WechatCalloutQueueManager.DeQueue();
        WechatCalloutQueueManager.FinalizeTasks(new List<Wechat_Callout_Task_Queue__c>{textQueue2});
        WechatCalloutQueueManager.HasTask();
    }
    
    static testMethod void WechatCallinListenerGetTest()
    {
        WechatPlatformRequestMock fakeResponse = new WechatPlatformRequestMock();
        system.Test.setMock(HttpCalloutMock.class, fakeResponse);
        GenerateWechatSetting();
        System.RestContext.request = new RestRequest();
        RestContext.request.requestURI = '/Wechat/CideaatechSF';
        RestContext.request.params.put('echostr' ,'123');
        RestContext.request.params.put('signature','b73c30471118ff50738e25d0128e6146ce7b43a2');
        RestContext.request.params.put('timestamp','20140909');
        RestContext.request.params.put('nonce','1111');
        RestContext.request.addHeader('echostr', 'blahblah@some.org');
        WechatCallinListener.CheckSignNature();
        
    }
    
    static testMethod void WechatCallinListenerGetPost()
    {
        WechatPlatformRequestMock fakeResponse = new WechatPlatformRequestMock();
        system.Test.setMock(HttpCalloutMock.class, fakeResponse);
        GenerateWechatSetting();
        System.RestContext.request = new RestRequest();
        RestContext.request.requestURI = '/Wechat/CideaatechSF';
        RestContext.request.params.put('echostr' ,'123');
        RestContext.request.params.put('signature','b73c30471118ff50738e25d0128e6146ce7b43a2');
        RestContext.request.params.put('timestamp','20140909');
        RestContext.request.params.put('nonce','1111');
        RestContext.request.requestURI = '/Wechat/CideaatechSF';
        RestContext.request.requestBody = Blob.valueOf('<xml><ToUserName><![CDATA[toUser]]></ToUserName><FromUserName><![CDATA[fromUser]]></FromUserName> <CreateTime>1348831860</CreateTime><MsgType><![CDATA[text]]></MsgType><Content><![CDATA[this is a test]]></Content><MsgId>1234567890123456</MsgId></xml>');
        WechatCallinListener.ReceiveCallinMsg();
    
        System.RestContext.request = new RestRequest();
        RestContext.request.requestURI = '/Wechat/CideaatechSF';
        WechatCallinListener.ReceiveCallinMsg();
    }
    
    static testMethod void WechatCallinMsgPipelineTest()
    {
        GenerateCallinHandler();
        WechatPlatformRequestMock fakeResponse = new WechatPlatformRequestMock();
        system.Test.setMock(HttpCalloutMock.class, fakeResponse);
        WechatEntity.CallinBaseMsg baseMsg = GenerateCallinBaseMsg();
        GenerateCallinHandler();
        WechatCallinMsgPipeline wcp = new WechatCallinMsgPipeline(baseMsg , 'CideaatechSF');
        wcp.execute();
    }
    
    static testMethod void WechatCalloutProcessorTest()
    {
        
    }
    
    static testMethod void WechatMenuControllerTest()
    {
        GenerateWechatSetting();
        GenerateWechatMenu('click' , 'A' , true);
        GenerateWechatMenu('click' , 'B' , false);
        GenerateWechatMenu('view' , 'http://www.baidu.com' , false);
        WechatPlatformRequestMock fakeResponse = new WechatPlatformRequestMock();
        system.Test.setMock(HttpCalloutMock.class, fakeResponse);
        system.test.starttest();
        WechatMenuApi.CreateMenu('CideaatechSF');
        WechatMenuApi.DeleteMenu('CideaatechSF');
        system.test.stoptest();
        //WechatMenuController.GenerateMenuList();
    }
    
    static testMethod void WechatCallOutService()
    {
        WechatPlatformRequestMock fakeResponse = new WechatPlatformRequestMock();
        system.Test.setMock(HttpCalloutMock.class, fakeResponse);
        GenerateWechatSetting();
        WechatCallOutService wcs = new WechatCallOutService('CideaatechSF');
        system.test.starttest();
        string t1 = wcs.GetOpenIdbyCode('abc');
        system.test.stoptest();
    }
    
    static testMethod void WechatCallOutServiceSendMsg()
    {
        WechatPlatformRequestMock fakeResponse = new WechatPlatformRequestMock();
        system.Test.setMock(HttpCalloutMock.class, fakeResponse);
        GenerateWechatSetting();
        WechatCallOutService wcc = new WechatCallOutService();
        wcc.SetPublicAccountName('CideaatechSF');
        system.test.starttest();
        wcc.SendMsg('SendMsg');
        system.test.stoptest();
        
    }
    
     static testMethod void WechatCallOutServiceRefreshToken()
    {
        WechatPlatformRequestMock fakeResponse = new WechatPlatformRequestMock();
        system.Test.setMock(HttpCalloutMock.class, fakeResponse);
        GenerateWechatSetting();
        WechatCallOutService wcc = new WechatCallOutService();
        wcc.SetPublicAccountName('CideaatechSF');
        system.test.starttest();
        WechatCalloutService.AccessToken at = wcc.RefreshAccessToken();
        system.test.stoptest();
        
    }
    
    static testMethod void WechatCallOutServiceGetUserInfo()
    {
        WechatPlatformRequestMock fakeResponse = new WechatPlatformRequestMock();
        system.Test.setMock(HttpCalloutMock.class, fakeResponse);
        GenerateWechatSetting();
        WechatCallOutService wcc = new WechatCallOutService();
        wcc.SetPublicAccountName('CideaatechSF');
        system.test.starttest();
        WechatEntity.User user = wcc.GetUserInfo('1234566');
        system.test.stoptest();
    }
    
    static testMethod void WechatCallOutServiceGCreateMenu()
    {
        /*GenerateWechatSetting();
        WechatPlatformRequestMock fakeResponse = new WechatPlatformRequestMock();
        system.Test.setMock(HttpCalloutMock.class, fakeResponse);
        WechatCallOutService wcc = new WechatCallOutService();
        wcc.SetPublicAccountName('CideaatechSF');
        system.test.starttest();
        wcc.CreateMenu('CideaatechSF');
        system.test.stoptest();*/
    }
    
    static testMethod void WechatCallOutServiceGDeleteMenu()
    {
        WechatPlatformRequestMock fakeResponse = new WechatPlatformRequestMock();
        system.Test.setMock(HttpCalloutMock.class, fakeResponse);
        GenerateWechatSetting();
        WechatCallOutService wcc = new WechatCallOutService();
        wcc.SetPublicAccountName('CideaatechSF');
        system.test.starttest();
        wcc.DeleteMenu();
        system.test.stoptest();
    }
    
    static testMethod void WechatCallOutServiceGDownloadMedia()
    {
        WechatPlatformRequestMock fakeResponse = new WechatPlatformRequestMock();
        system.Test.setMock(HttpCalloutMock.class, fakeResponse);
        GenerateWechatSetting();
        WechatCallOutService wcc = new WechatCallOutService();
        wcc.SetPublicAccountName('CideaatechSF');
        system.test.starttest();
        WechatEntity.File file = wcc.DowloadMedia('111');
        wcc.UploadMedia();
        wcc.UploadNews();
        system.test.stoptest();
    }
    
    static testMethod void WechatCalloutTaskQueueTest()
    {
        WechatPlatformRequestMock fakeResponse = new WechatPlatformRequestMock();
        system.Test.setMock(HttpCalloutMock.class, fakeResponse);
        system.test.starttest();
        Wechat_Callout_Task_Queue__c textQueue = GenerateWechatCalloutTaskQueue(true);
        system.test.stoptest();
    }
    
    static testMethod void WechatRefreshAccessTokenTest()
    {
        GenerateWechatSetting();
        WechatPlatformRequestMock fakeResponse = new WechatPlatformRequestMock();
        system.Test.setMock(HttpCalloutMock.class, fakeResponse);
        system.Test.startTest();
        WechatRefreshAccessTokenScheduler.callOut('CideaatechSF');
        system.Test.stopTest();
        WechatCalloutService wcs = new WechatCalloutService('CideaatechSF');
        string token = wcs.GetAccessToken();
        system.debug('------------------------------------------------' + token);
        system.assertEquals('awegaegfaefafawfawgaegawefawfawfawkfawlgkwaelkfawfaefaewfawnfwaeitejigrjobjdrnbserg4wjvcnrnwiutieowruweroifworgnaewgoaegnaoegnaoegnagoanegoabngoabgaoebgaoebgaoegbaoegbaeobgorabgorbgoaogaoegbaeoafgoaeogaoegaoegaoegoaegffnaoenfoanf===aegnorieaweiafeofnaoefneofnaoewn,snlsregnorigoirgitgnronvonaonfeoafoaenfaoefnbalgaekm,tsgknvkrt,knsbobosrlnerlnbosrebnseorbnosrglsrpognarogaegoaegoweageavevaeooaewfnaeifenldnvslknlrgersoriwgawjeogabvmsdcnmznnlvdkjaroivahoeewpjafkvsnfovlaknnlernfcaogelvjanobrglvnoerighwoaeghawgouaengroagnangenagoeig', token);
    }
    
    static testMethod void WechatRefreshAccessTokenTest2()
    {
    	GenerateWechatSetting();
    	WechatPlatformRequestMock fr = new WechatPlatformRequestMock('{"access_token":"awegaegfaefafawfawgaegawefawf","expires_in":7200}');
        system.Test.setMock(HttpCalloutMock.class, fr);
        system.Test.startTest();
        WechatRefreshAccessTokenScheduler.callOut('CideaatechSF');
        system.Test.stopTest();
    }
    
    static testMethod void WechatRefreshAccessTokenTestScheduler()
    {
        GenerateWechatSetting();
        System.schedule('Refresh AccessToken2','0 10 0/1 * * ?', new WechatRefreshAccessTokenScheduler());
    }
    
    public static WechatEntity.CallinBaseMsg GenerateCallinBaseMsg()
    {
        GenerateWechatSetting();
        GenerateCallinHandler();    
        WechatEntity.InTextMsg textMsg = new WechatEntity.InTextMsg();
        textMsg.CreateTime = Datetime.now();
        textMsg.MsgType = WechatEntity.MESSAGE_TYPE_TEXT;
        textMsg.FromUserName = 'oKYGMtxZooTk6g1EydN8pwZxyo_M';
        textMsg.ToUserName = 'ol0Orjgr4N_UpaejqQSMGPiOvf4U';
        textMsg.MsgId = '1234567890123456';
        textMsg.Content = '這是測試，這是測試！！！！';
        
        return textMsg;
    }
    
    public static Wechat_Callout_Task_Queue__c GenerateWechatCalloutTaskQueue(boolean isInsert)
    {
        Wechat_Callout_Task_Queue__c textQueue = new Wechat_Callout_Task_Queue__c();
        textQueue.Public_Account_Name__c =  'CideaatechSF';
        textQueue.Type__c = WechatEntity.MESSAGE_TYPE_TEXT;
        textQueue.Msg_Body__c = '';
        textQueue.Processor_Name__c = 'WechatCallOutSendTextMsgProcessor';
        textQueue.Name = 'WechatCallOutSendTextMsgProcessor';
        textQueue.Status__c = 'waiting';
        if(isInsert)
        {
            insert textQueue;
        }
                
        return textQueue;
    }
    
    
    public static void GenerateWechatSetting()
    {
        Wechat_Setting__c ws = new Wechat_Setting__c();
        ws.Name = 'CideaatechSF';
        ws.App_Id__c = 'wx76c641c11a6462e5';
        ws.App_Secret__c = '4ce571024be7e0093afbc2f536a6bda9';
        ws.Token__c = 'Token';
        ws.API_Base_Url__c = 'https://api.weixin.qq.com k0MF5eSNtr1z5vCU7S1NKCQj_z1LWhyMxGQQf6UpHN3tlbD71Yf04XK15ziBfSH9NP-';
        ws.Access_Token1__c = 'k0MF5eSNtr1z5vCU7S1NKCQj_z1LWhyMxGQQf6UpHN3tlbD71Yf04XK15ziBfSH9NP-__XS6R0U196t21JMLjA';
        insert ws;
    }
    
    public static void GenerateWechatMenu(string type , string key , boolean isCreateSub)
    {
        Wechat_Menu__c wm = new Wechat_Menu__c();
        wm.Menu_Level__c = 'Menu';
        wm.Menu_Type__c =  type;
        wm.Event_Key__c = key;
        wm.Active__c = true;
        insert wm;
        if(isCreateSub)
        {
            Wechat_Menu__c wm1 = new Wechat_Menu__c();
            wm1.Menu_Level__c = 'Sub Menu';
            wm1.Menu_Type__c =  type;
            wm1.Event_Key__c = key;
            wm1.Active__c = true;
            wm1.Higher_Level_Menu__c = wm.id;
            insert wm1;
        }
    }
    
    public static void GenerateCallinHandler()
    {
        Wechat_Handler_Binding__c whb = new Wechat_Handler_Binding__c();
        whb.Active__c = true;
        whb.Execute_Order__c = 1;
        whb.Handle_Msg_Type__c = 'text';
        whb.Handler_Name__c = 'WechatCallinMsgRepeaterHandler';
        whb.Public_Account_Name__c = 'CideaatechSF';
        whb.Name = 'WechatCallinMsgRepeaterHandler';
        whb.Event_Key__c = 'WechatCallinMsgRepeaterHandler';
        insert whb;
    }
    
      
    //构造xml格式消息
    public static string BuildCallinTextMsg(string str)
    {
        String xmlOutput = '';
        if(str == 'text')
        {
            XmlStreamWriter w = new XmlStreamWriter();
               //start
                w.writeStartElement(null,'xml',null);
                    w.writeStartElement(null,'ToUserName',null);
                        w.writeCData('1235465');
                    w.writeEndElement();
                    w.writeStartElement(null,'FromUserName',null);
                        w.writeCData('fromUser');
                    w.writeEndElement();
                    w.writeStartElement(null,'CreateTime',null);
                        w.writeCharacters('123456789');
                    w.writeEndElement();
                    w.writeStartElement(null,'MsgType',null);
                        w.writeCData('text');
                    w.writeEndElement();
                    w.writeStartElement(null,'Content',null);
                        w.writeCData('content');
                    w.writeEndElement();
               w.writeEndElement(); 
               //end
               xmlOutput = w.getXmlString();
                w.close();
        }
        else if(str == 'image')
        {
            XmlStreamWriter w = new XmlStreamWriter();
                //start
                w.writeStartElement(null,'xml',null);
                
                    w.writeStartElement(null,'ToUserName',null);
                        w.writeCData('toUser');
                    w.writeEndElement();
                    
                    w.writeStartElement(null,'FromUserName',null);
                        w.writeCData('fromUser');
                    w.writeEndElement();
                    
                    w.writeStartElement(null,'CreateTime',null);
                        w.writeCharacters('123456789');
                    w.writeEndElement();
                    
                    w.writeStartElement(null,'MsgType',null);
                        w.writeCData('image');
                    w.writeEndElement();
                    
                    w.writeStartElement(null,'Event',null);
                        w.writeCData('subscribe');
                    w.writeEndElement();
                    
               w.writeEndElement(); 
               //end
               xmlOutput = w.getXmlString();
                w.close();
        }else if(str == 'voice')
        {
            XmlStreamWriter w = new XmlStreamWriter();
            //start
            w.writeStartElement(null,'xml',null);
            
                w.writeStartElement(null,'ToUserName',null);
                    w.writeCData('toUser');
                w.writeEndElement();
                
                w.writeStartElement(null,'FromUserName',null);
                    w.writeCData('fromUser');
                w.writeEndElement();
                
                w.writeStartElement(null,'CreateTime',null);
                    w.writeCharacters('123456789');
                w.writeEndElement();
                
                w.writeStartElement(null,'MsgType',null);
                    w.writeCData('voice');
                w.writeEndElement();
                
                w.writeStartElement(null,'Event',null);
                    w.writeCData('subscribe');
                w.writeEndElement();
                
           w.writeEndElement(); 
           //end
            xmlOutput = w.getXmlString();
            w.close();
        }else if(str == 'event')
        {
            XmlStreamWriter w = new XmlStreamWriter();
            //start
            w.writeStartElement(null,'xml',null);
            
                w.writeStartElement(null,'ToUserName',null);
                    w.writeCData('toUser');
                w.writeEndElement();
                
                w.writeStartElement(null,'FromUserName',null);
                    w.writeCData('fromUser');
                w.writeEndElement();
                
                w.writeStartElement(null,'CreateTime',null);
                    w.writeCharacters('123456789');
                w.writeEndElement();
                
                w.writeStartElement(null,'MsgType',null);
                    w.writeCData('event');
                w.writeEndElement();
                
                w.writeStartElement(null,'Event',null);
                    w.writeCData('subscribe');
                w.writeEndElement();
                
           w.writeEndElement(); 
           //end
            xmlOutput = w.getXmlString();
            w.close();
        }
        return xmlOutput;
    }
    static testmethod void Test_WechatException()
    {
    	WechatException.ErrorResponse er = new WechatException.ErrorResponse();
    	er.errcode = 40001;
    	er.errmsg = 'errmsg';
    	WechatException we = new WechatException(er);
    }
}