/*Author:leo.bi@celnet.com.cn
 *Created On:2014-4-18
 *function:The test class for SMSProxy
 *Apply To:CN
 */
@isTest
global class SMSProxyTest implements HttpCalloutMock
{
	global HTTPResponse respond(HTTPRequest req) 
	{
		String body = '<?xml version="1.0" encoding="utf-8"?><Root><head><state>001</state><verifystate>是否有验证未通过的号码</verifystate></head><body>' + '<phones>验证未通过的号码</phones></body></Root>';
        HttpResponse res = new HttpResponse();
        res.setBody(body);
        return res;
    }
    static testMethod void myUnitTest() 
    {
	    // TO DO: implement unit test
	    Test.setMock(HttpCalloutMock.class, new SMSProxyTest());
	    SMSProxy sp = new SMSProxy();
	    sp.send('15900000000', 'this is the test content');
    }
}