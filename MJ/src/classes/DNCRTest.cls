@isTest
public class DNCRTest{
    public static testMethod void testDNCR()
    {
        // Create test Households
		createTestHouseholds();
        
        // Create Test DNCR Data - simulate Data Load
        createTestDNCRs();
        
        //Set DNCR Offset Hours =0 in test calss
        createDNCRSetting();

        // Create DNCR Job Result - simulate failed
        DNCR_Job_Result__c result3 = new DNCR_Job_Result__c();
        result3.Status__c = 'In Progress';
        result3.Data_Imported__c = true;
        result3.Total_DNCR_Records__c = 2;
        result3.Failed_To_Load__c = 2;
        result3.Job_Start_Time__c = DateTime.now();
        insert result3;
        result3 = [SELECT ID,Remark__c FROM DNCR_Job_Result__c WHERE ID=:result3.ID];
        System.assertEquals(CommonHelper.DNCR_LOAD_FAILED,result3.Remark__c);
        
        // Create DNCR Job Result - this will trigger the job to run
        DNCR_Job_Result__c result = new DNCR_Job_Result__c();
        result.Status__c = 'In Progress';
        result.Data_Imported__c = true;
        result.Total_DNCR_Records__c = 2;
        result.Failed_To_Load__c = 0;
        result.Job_Start_Time__c = DateTime.now();
        
        // Create DNCR Job Result - simulate in progress
        DNCR_Job_Result__c result2 = new DNCR_Job_Result__c();
        result2.Status__c = 'In Progress';
        result2.Data_Imported__c = true;
        result2.Total_DNCR_Records__c = 2;
        result2.Failed_To_Load__c = 0;
        result2.Job_Start_Time__c = DateTime.now();
        insert result;
        result = [SELECT ID,New_DNCR_Batch_Job_ID__c FROM DNCR_Job_Result__c WHERE ID=:result.ID];
        System.assertEquals(true,(result.New_DNCR_Batch_Job_ID__c!=NULL));
        insert result2;
    }
    
    private static void createTestDNCRs()
    {
        List<DNCR__c> dncrs = new List<DNCR__c>();
        DNCR__c dncr1 = new DNCR__c();
        dncr1.Name = '18900908';
        DNCR__c dncr2 = new DNCR__c();
        dncr2.Name = '28966908';
        dncrs.add(dncr1);
        dncrs.add(dncr2);
        insert dncrs;
    }
    
    private static void createDNCRSetting()
    {
        //Set DNCR Offset Hours =0 in test calss
        Key_Value_Mapping__c setting = new Key_Value_Mapping__c();
        setting.Name=CommonHelper.KEY_DNCR_OFFSET_HOURS;
        setting.Value__c='0';
        insert setting;
    }
    
    private static void createTestHouseholds()
    {
        //Get RecordType ID for HouseHold and HouseHold Child
        RecordType typeHouseHold = [Select Id From RecordType Where DeveloperName ='HouseHold' and SobjectType ='Account' limit 1];
        
        //Insert custom setting value
        String curYear = String.valueOf(Date.today().year());
        Member_ID_Setting__c memberSetting = new Member_ID_Setting__c();
        memberSetting.name=curYear;
        memberSetting.Next_ID__c=1888;
        insert memberSetting;
        
        List<Account> households = new List<Account>();
        Account account1 = new Account();
        account1.Name = 'Account Name';
        account1.First_Name__c = 'Account';
        account1.Last_Name__c = 'Name';
        account1.RecordTypeId = typeHouseHold.Id;
        account1.Mobile_Phone__c = '18900908';
        account1.Mobile_DNCR__c = false;
        households.add(account1);
        
        Account account2 = new Account();
        account2.Name = 'Account Two';
        account2.First_Name__c = 'Account';
        account2.Last_Name__c = 'Two';
        account2.RecordTypeId = typeHouseHold.Id;
        account2.Mobile_Phone__c = '28966908';
        account2.Mobile_DNCR__c = true;
        households.add(account2);
        
        Account account3 = new Account();
        account3.Name = 'Account Three';
        account3.First_Name__c = 'Account';
        account3.Last_Name__c = 'Three';
        account3.RecordTypeId = typeHouseHold.Id;
        account3.Mobile_Phone__c = '36096688';
        account3.Mobile_DNCR__c = true;
        households.add(account3);
        
        insert households;
    }
}