/**
*PlatForm Mock Class for response
*/
@isTest
public class WechatPlatformRequestMock implements HttpCalloutMock
{
    protected Integer code;
    protected String status;
    protected Map<String, String> responseHeaders;
    protected final string GetOpenIdbyCode = '{"access_token":"awegaegfaefafawfawgaegawefawfawfawkfawlgkwaelkfawfaefaewfawnfwaeitejigrjobjdrnbserg4wjvcnrnwiutieowruweroifworgnaewgoaegnaoegnaoegnagoanegoabngoabgaoebgaoebgaoegbaoegbaeobgorabgorbgoaogaoegbaeoafgoaeogaoegaoegaoegoaegffnaoenfoanf===aegnorieaweiafeofnaoefneofnaoewn,snlsregnorigoirgitgnronvonaonfeoafoaenfaoefnbalgaekm,tsgknvkrt,knsbobosrlnerlnbosrebnseorbnosrglsrpognarogaegoaegoweageavevaeooaewfnaeifenldnvslknlrgersoriwgawjeogabvmsdcnmznnlvdkjaroivahoeewpjafkvsnfovlaknnlernfcaogelvjanobrglvnoerighwoaeghawgouaengroagnangenagoeig","expires_in":7200,"refresh_token":"REFRESH_TOKEN","openid":"OPENID","scope":"SCOPE"}';
    protected final string OK = '{"errcode":0,"errmsg":"ok"}';
    protected string RefreshAccessToken = '{"access_token":"awegaegfaefafawfawgaegawefawfawfawkfawlgkwaelkfawfaefaewfawnfwaeitejigrjobjdrnbserg4wjvcnrnwiutieowruweroifworgnaewgoaegnaoegnaoegnagoanegoabngoabgaoebgaoebgaoegbaoegbaeobgorabgorbgoaogaoegbaeoafgoaeogaoegaoegaoegoaegffnaoenfoanf===aegnorieaweiafeofnaoefneofnaoewn,snlsregnorigoirgitgnronvonaonfeoafoaenfaoefnbalgaekm,tsgknvkrt,knsbobosrlnerlnbosrebnseorbnosrglsrpognarogaegoaegoweageavevaeooaewfnaeifenldnvslknlrgersoriwgawjeogabvmsdcnmznnlvdkjaroivahoeewpjafkvsnfovlaknnlernfcaogelvjanobrglvnoerighwoaeghawgouaengroagnangenagoeig","expires_in":7200}';
    protected final string GetUserInfo = '{"subscribe": 1, "openid": "o6_bmjrPTlm6_2sgVt7hMZOPfL2M", "nickname": "Band", "sex": 1, "language": "zh_CN",  "city": "广州", "province": "广东",  "country": "中国",   "headimgurl":    "http://wx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/0",  "subscribe_time": 1382694957,   "unionid": " o6_bmasdasdsad6_2sgVt7hMZOPfL"}';
    
    public WechatPlatformRequestMock()
    {
        this.code =200;
        this.status = 'OK';
        this.responseHeaders = new Map<string,string>();
    }
    
    public WechatPlatformRequestMock(string RefreshToken)
    {
    	this.RefreshAccessToken = refreshToken;
        this.code =200;
        this.status = 'OK';
        this.responseHeaders = new Map<string,string>();
    }
    
    public HTTPResponse respond(HTTPRequest req)
    {
        HttpResponse resp = new HttpResponse();
        resp.setStatusCode(code);
        resp.setStatus(status);
        string endpoint = req.getEndpoint();
        system.debug('***************endpoint***************'+endpoint);
        if(endpoint.contains('/sns/oauth2/access_token'))
        {
            resp.setBody(GetOpenIdbyCode);
        }
        else if(endpoint.contains('/cgi-bin/message/custom/send'))
        {
            resp.setBody(OK);
        }
        else if(endpoint.contains('/cgi-bin/token'))
        {
            resp.setBody(RefreshAccessToken);
        }
        else if(endpoint.contains('/cgi-bin/menu/create'))
        {
            resp.setBody(OK);
        }
        else if(endpoint.contains('/cgi-bin/menu/delete'))
        {
            resp.setBody(OK);
        }
        else if(endpoint.contains('/cgi-bin/media/get'))
        {
            system.debug('***************###############3**#***********');
            resp.setBody('fefefefefefe');
        }
        else if(endpoint.contains('/cgi-bin/user/info'))
        {
            resp.setBody(GetUserInfo);
        }
        return resp;
    }
}