/**
*test for WechatSMSVerificationPageController
*/
@isTest
global  class WechatSMSVerificationPageControllerTest  implements HttpCalloutMock ,Database.AllowsCallouts
{
    static User HK_User = TestUtility.new_HK_User();
    static testMethod void myUnitTest() 
    {
        system.runas(HK_User)
        {
        	system.Test.loadData(Wechat_Reminding__c.sObjectType, 'WechatReminding');
            TestUtility.setupMemberIDCustomSettings();
            Id leadRecordType = [select id from RecordType where DeveloperName=:'LeadsHK' and SobjectType=:'Lead'].id;
            Lead lea = new Lead();
            lea.LastName = 'PARENT';
            lea.FirstName = 'TEST';
            lea.First_Name_1st_Child__c = 'BABY';
            lea.Last_Name_1st_Child__c = 'TEST';
            lea.Birthday_1st_Child__c = system.today();
            lea.RecordTypeId = leadRecordType;
            insert lea;
            Wechat_User__c wu2 = GenerateWechatUser('test2' , 'oKYGMt2uURzAQf2S6SV8jvkk-4m1');
            
            ApexPages.currentPage().getParameters().put('openId','oKYGMt2uURzAQf2S6SV8jvkk-4m1');
            WechatSMSVerificationPageController wpc2= new WechatSMSVerificationPageController();
            //wpc2.inputCode = '123456';
            wpc2.ReceivePhone = '111111';
            //wpc2.createCode = '123456';
            wpc2.CreateRandomCode();
            wpc2.inputCode = wpc2.createCode;
            system.debug('***************wpc2*****************' + wpc2.recordId);
            Pagereference p2 = wpc2.SubmitPage();
            
            Id accountRecordType = [select id from RecordType where DeveloperName=:'HouseHold' and SobjectType=:'Account'].id;
            Account acc = new Account();
            acc.Name = 'PRIMARY PARENT';
            acc.Last_Name__c = 'PRIMARY';
            acc.First_Name__c = 'PARENT';
            acc.Mobile_Phone__c = '22222222';
            acc.RecordTypeId = accountRecordType;
            insert acc;
            Wechat_User__c wu3 = GenerateWechatUser('test3' , 'oKYGMt2uURzAQf2S6SV8jvkk-4m2');
            
            ApexPages.currentPage().getParameters().put('openId','oKYGMt2uURzAQf2S6SV8jvkk-4m2');
            WechatSMSVerificationPageController wpc3= new WechatSMSVerificationPageController();
            //wpc3.inputCode = '123456';
            wpc3.ReceivePhone = '22222222';
            //wpc3.createCode = '123456';
            wpc3.CreateRandomCode();
            wpc3.inputCode = wpc3.createCode;
            system.debug('***************wpc3*****************' + wpc3.recordId);
            Pagereference p3 = wpc3.SubmitPage();
            
            Wechat_User__c wu4 = GenerateWechatUser('test6' , 'oKYGMt2uURzAQf2S6SV8jvkk-111');
            ApexPages.currentPage().getParameters().put('openId','oKYGMt2uURzAQf2S6SV8jvkk-111');
            WechatSMSVerificationPageController wpc4= new WechatSMSVerificationPageController();
            //wpc4.inputCode = '123456';
            wpc4.createCode = '123456';
            //wpc4.ReceivePhone = '000000';
            wpc4.CreateRandomCode();
            wpc4.inputCode = '000000';
            system.debug('***************wpc4*****************' + wpc4.recordId);
            Pagereference p4 = wpc4.SubmitPage();
            
            ApexPages.currentPage().getParameters().put('openId','oKYGMt2uURzAQf2S6SV8jvkk-000');
            WechatSMSVerificationPageController wpc5= new WechatSMSVerificationPageController();
        }
    }
    
    static testMethod void SMSTest()
    {
        system.runas(HK_User)
        {
        	system.Test.loadData(Wechat_Reminding__c.sObjectType, 'WechatReminding');
            Wechat_SMS_Setting__c sms = new Wechat_SMS_Setting__c();
            sms.name = 'Username';
            sms.value__c = 'username';
            insert sms;
            sms = new Wechat_SMS_Setting__c();
            sms.name = 'Password';
            sms.value__c = 'password';
            insert sms;
            sms = new Wechat_SMS_Setting__c();
            sms.name = 'SMSURL';
            sms.value__c = 'smsurl';
            insert sms;
            Wechat_User__c wu = GenerateWechatUser('test1' , 'oKYGMt2uURzAQf2S6SV8jvkk-4m0');
            WechatPlatformRequestMock fakeResponse = new WechatPlatformRequestMock();
            system.Test.setMock(HttpCalloutMock.class, fakeResponse);
            system.test.starttest();
            
            ApexPages.currentPage().getParameters().put('openId','oKYGMt2uURzAQf2S6SV8jvkk-4m0');
            WechatSMSVerificationPageController wpc= new WechatSMSVerificationPageController();
            wpc.CreateRandomCode();
            wpc.ReceivePhone = '123456';
            wpc.inputCode = '123456';
            wpc.createCode = '123456';
            //Pagereference pf = wpc.SubmitPage();
            system.test.stoptest();
        }
    }
    
    public static Wechat_User__c GenerateWechatUser(string name, string openId)
    {
        Wechat_User__c wu = new Wechat_User__c();
        wu.Name = name;
        wu.Open_Id__c = openId;
        insert wu;
        return wu;
    }
    
    global  HTTPResponse respond(HTTPRequest req)
    {
        HttpResponse resp = new HttpResponse();
        resp.setStatusCode(200);
        resp.setStatus('ok');
        string endpoint = req.getEndpoint();
        system.debug('***************endpoint***************'+endpoint);
        if(endpoint.contains('smcsmpptest.smartone.com/app/servlet/nGenSendSM'))
        {
            resp.setBody('0');
        }
        return resp;
    }
}