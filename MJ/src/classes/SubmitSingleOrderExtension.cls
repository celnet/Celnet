public class SubmitSingleOrderExtension{
    
    private List<Order_Item__c> orderItems;
    private Order__c selectedOrder;
    private Boolean displayOrder;

    public SubmitSingleOrderExtension(ApexPages.StandardController controller) 
    {
    	if (SF2SFHelper.checkManageConnectionPermission() == false){
        	displayOrder = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,CommonHelper.NO_PUSH_PERMISSION));
        	return;
        }
        
        ID orderID = ApexPages.currentPage().getParameters().get('id');

        List<Order__c> orders = [SELECT ID,Name,First_Name__c,Last_Name__c,Cancelled__c,Need_Send_to_SFA__c,Address_Flat_Floor_Block_Building__c,
                                 Address_Street_District_Territory__c,Mobile_Phone__c,Expected_Delivery_Date__c,Delivery_Instruction__c
                                 FROM Order__c WHERE ID=:orderID];

        if(orders == NULL || orders.isEmpty())
        {
            displayOrder = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,CommonHelper.ORDER_NOT_FOUND));
        }
        else
        {
            selectedOrder = Orders[0];
            
            //Validation 1: Cancelled orders can not be submitted
            if(selectedOrder.Cancelled__c)
            {
               displayOrder = false;
               ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,CommonHelper.CANCELLED_ORDER));
            }
            //Validation 2: Only orders with "Need_Send_to_SFA__c" as TRUE can be submitted to SFA
            else if(!selectedOrder.Need_Send_to_SFA__c)
            {
                displayOrder = false;
               	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,CommonHelper.SEND_TO_SFA_NOT_ALLOWED));
            }
            else
            {
                orderItems = [SELECT Id,Product__c,Quantity__c,Product_Code__c FROM Order_Item__c WHERE Order__c =: orderID];
                
                //Validation 3: Orders without lines items can not be submitted
                if(orderItems == NULL || orderItems.isEmpty())
                {
                   displayOrder = false;
                   ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,CommonHelper.BLANK_ORDER));
                }
                //Validation 4: Order can only be submitted once
                else if(SF2SFHelper.isOrderSubmitted(orderID))
                {
                   displayOrder = false;
                   ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,CommonHelper.ORDER_ALREADY_SUBMITTED));
                }
                else
                    displayOrder = true;
            }
        }
    }
    
    public List<Order_Item__c> getOrderItems()
    {
        return orderItems;
    }
        
    public Order__c getSelectedOrder()
    {
        return selectedOrder;
    }
    
    public Boolean getDisplayOrder()
    {
        return displayOrder;
    }
    
    public PageReference submitOrder() {
		//Send Order to SFA via SF2SF Connection
        try
        {
        	SF2SFHelper.submitOrderToSFA(selectedOrder.ID,false);
        }
        catch (Exception e)
        {
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
           return NULL;
        }
        //  Return to the Order Detail page:
        return new PageReference('/'+selectedOrder.ID);  
    }
}