/*Author:leo.bi@celnet.com.cn
 *Created On:2014-4-25
 *function:The Test Class of InvitationFilterController and InvitationBatch
 *Apply To:CN
 */
@isTest(SeeAllData =true)
private class InvitationFilterControllerTest 
{
	static testmethod void runJob()
	{
		system.runas(TestUtility.new_CN_User())
        {
			//address
	        Address_Management__c am = createAddress();
	        //account
	        Account acc = createAccount();
	        acc.Standard_City__c = am.Id;
	        insert acc;
	        Questionnaire__c que = createQuestionnaire();
	        //contact 
	        Contact con = createContact();
	        con.AccountId = acc.Id;
	        insert con;
	        //Campaign
	        Campaign c = createCamapaign();
	        c.Standard_City__c = am.Id;
	        insert c;
			 //Controller
	        InvitationFilterController invController42 = createController(am,c);
	        //Enter Filter Info
	        invController42.inv.Name = 'Test Invitation4';
			invController42.inv.Campaign_Member_Limit__c = 100;
			invController42.selectedCampaignRegion = am.Id;
			invController42.selectedCampaignSubRegion = am.Id;
			invController42.selectedCampaignArea = am.id;
			invController42.selectedCampaignCity = am.Id;
			invController42.inv.Campaign__c = c.Id;
	        
	        //Select Contact Regions
	        invController42.selectedSalesRegion = am.Region__c;
	        invController42.selectedSalesSubRegion = am.Id;
	        invController42.selectedSalesCity = am.Id;
	        invController42.run();
	        
	        Contact con1 = createContact();
	        con1.AccountId = acc.Id;
	        con1.Questionnaire__c = que.Id;
	        insert con1;
	        //Answer
	        Answer__c ans1 = createAnswer();
	        ans1.Questionnaire__c = que.Id;
	        ans1.Contact__c = con1.Id;
	        ans1.Q038__c = 'Y';
	        ans1.Q039__c = 'Y';
	        insert ans1;
	        Task t = new Task();
	        t.WhoId = con1.Id;
	        t.WhatId = que.id;
	        t.Result__c = 'No Response';
	        insert t;
	        ///Test Batch 
	        InvitationFilterController invController2 = createController(am,c);
			invController2.ans.Questionnaire__c = que.Id;
	        invController2.ans.Q038__c = 'Y';
	        invController2.ans.Q039__c = 'Y';
	        invController2.run();
	        //run difrent constructer 
	        Invitation__c inv = new Invitation__c();
			inv.SOQL_Script__c = invController42.inv.SOQL_Script__c;
			inv.Campaign__c = c.Id; 
			insert inv;
	        InvitationBatch invb = new InvitationBatch(inv);
	        invb.cutOffString('newString', 'oldString');
        }
	}
	
	static testMethod void testController() 
    {
        system.runas(TestUtility.new_CN_User())
	    {
	        //address
	        Address_Management__c am = createAddress();
	        //account
	        Account acc = createAccount();
	        acc.Standard_City__c = am.Id;
	        insert acc;
	        //questionnaire
	        Questionnaire__c que = createQuestionnaire();
	        //contact 
	        Contact con = createContact();
	        con.AccountId = acc.Id;
	        con.Questionnaire__c = que.Id;
	        insert con;
	        //Campaign
	        Campaign c = createCamapaign();
	        c.Standard_City__c = am.Id;
	        insert c;
	        //Answer
	        Answer__c ans = createAnswer();
	        ans.Questionnaire__c = que.Id;
	        ans.Contact__c = con.Id;
	        insert ans;
	        Task t = new Task();
	        t.WhoId = con.Id;
	        t.WhatId = que.id;
	        t.Result__c = 'No Response';
	        insert t;
	        Test.startTest();
	        //Controller
	        InvitationFilterController invController4 = createController(am,c);
	        //Enter Filter Info
	        invController4.inv.Name = 'Test Invitation4';
			invController4.inv.Campaign_Member_Limit__c = 100;
			invController4.selectedCampaignRegion = am.Id;
			invController4.selectedCampaignSubRegion = am.Id;
			invController4.selectedCampaignArea = am.id;
			invController4.selectedCampaignCity = am.Id;
			invController4.inv.Campaign__c = c.Id;
	        
	        //Select Contact Regions
	        invController4.selectedSalesRegion = am.Region__c;
	        invController4.selectedSalesSubRegion = am.Id;
	        invController4.selectedSalesCity = am.Id;
	        invController4.ans.Questionnaire__c = que.Id;
	        invController4.ans.Q038__c = 'Y';
	        invController4.ans.Q039__c = 'Y';
	        invController4.tas.Result__c = 'No Response';
	        invController4.babyEnd.Last_Routine_Call_Time__c = DateTime.now().addDays(-5);
	        invController4.babyEnd.Routine_Evaluated_On__c = DateTime.now().addDays(5);
	        invController4.run();
	        InvitationFilterController invController = new InvitationFilterController();
	        //Enter Filter Info
	        invController.inv.Name = 'Test Invitation';
			invController.inv.Campaign_Member_Limit__c = 100;
			system.debug('-------------------------am.id------------------' + am.Id);
			List<Selectoption> list_temp = invController.salesRegionItems;
			invController.selectedCampaignRegion = am.Id;
			
			invController.refreshCampaignSubRegion();
			list_temp = invController.campaignSubRegionItems;
			invController.selectedCampaignSubRegion = am.Id;
			
			invController.refreshCampaignArea();
			list_temp = invController.campaignAreaItems;
			invController.selectedCampaignArea = am.id;
			
			invController.refreshCampaignCity();
			list_temp = invController.campaignCityItems;
			invController.selectedCampaignCity = am.Id;
			
			invController.refreshCampaignItems();
			list_temp = invController.campaignItems;
			invController.inv.Campaign__c = c.Id;
	        invController.refreshRemark();
	        
	        //Select Contact Regions
	        invController.selectedSalesRegion = am.Region__c;
	        
	        invController.refreshSalesSubRegion();
	        list_temp = invController.salesSubRegionItems;
	        invController.selectedSalesSubRegion = am.Id;
	        
	        invController.refreshSalesArea();
	        list_temp = invController.salesAreaItems;
	        invController.selectedSalesArea = am.Id;
	        
	        invController.refreshSalesCity();
	        list_temp = invController.salesCityItems;
	        invController.selectedSalesCity = am.Id;
	        
	        //coverage method
	        List<SelectOption> list_tempOption = invController.list_AccountStatus;
	        String[] temp_String = invController.list_DigestiveSymptoms;
	        temp_String = invController.list_DigestiveSymptoms;
	        temp_String = invController.list_AllergySymptoms;
	        temp_String = invController.list_Survey_DigestiveSymptoms;
	        temp_String = invController.list_Survey_AllergySymptoms;
	        temp_String = invController.list_Survey_Last_DigestiveSymptoms;
	        temp_String = invController.list_Survey_Last_AllergySymptoms;
	        temp_String = invController.list_MultiSelect_Brand;
	        String specifiCode = invController.formatSpecificCode('H1234,H5678');
	        
	        //Contact Specify Filter Criteria
	        invController.selectedChannel = '手工/纸质hospital';
	        invController.AutoShowSubSource();
	        invController.selectedChannel = '手工/纸质Store';
	        invController.AutoShowSubSource();
	        invController.selectedSubChannel = '纸质单';
	        invController.baby.Birthdate = Date.today().addDays(-8);
	        invController.babyEnd.Birthdate = Date.today();
	        invController.baby.Register_Date__c = Date.today().addDays(-8);
	        invController.babyEnd.Register_Date__c = Date.today();
	        invController.baby.Specific_Code__c = 'HR1111';
	        invController.selectedAccStatus = 'Mother';
	        invController.baby.Purchase_Date__c = Date.today().addDays(-8);
	        invController.babyEnd.Purchase_Date__c = Date.today();
	        
	        //Contact Expand Information
	        invController.map_Digestive.put('Spits','Spits');
	        invController.map_Digestive.put('Cry','Cry');
	        invController.map_Digestive.put('Fidgety','Fidgety');
	        invController.map_Allergy.put('皮肤','皮肤');
	        invController.ans_Expanded.Q008_Invitation__c = 'MJN, Yili';
	        invController.ans_Expanded.Q008__c = 'MJN, Yili';
	        invController.baby.Last_Routine_Call_Time__c = DateTime.now().addDays(-8);
	        invController.baby.Routine_Evaluated_On__c = DateTime.now();
	        invController.baby.Last_Routine_Call_Result__c = 'No Answer';
	        invController.baby.Is_Last_Digestive_Symptoms__c = 'Y';
	        invController.baby.Is_Last_Allergy_Symptoms__c = 'Y';
	        invController.baby.Digestive_Symptoms_Type__c = 'Spits, Cry, Fidgety';
			invController.baby.Allergy_Symptoms_Type__c = '皮肤';
			
			//Contact Survey History Information
			invController.map_Survey_Digestive.put('哭闹','哭闹');
			invController.map_Survey_Allergy.put('皮肤','皮肤');
			invController.map_Survey_Last_Digestive.put('哭闹','哭闹');
			invController.map_Survey_Last_Digestive.put('腹泻','腹泻');
			invController.map_Survey_Last_Allergy.put('皮肤','皮肤');
			invController.map_Survey_Last_Allergy.put('湿疹','湿疹');
			invController.map_Multi_Brand.put('MJN','MJN');
			invController.ans.Questionnaire__c = que.Id;
	        invController.ans.Finished_On__c = DateTime.now().addDays(-8);
	        invController.ansEnd.Finished_On__c = DateTime.now();
	        invController.ans.Q008__c = 'MJN, Yili';
	        invController.ans.Q008_Invitation__c = 'MJN';
	        invController.ans.Q038__c = 'Y';
	        invController.ans.Q039__c = 'Y';
	        invController.ans.Q030__c = 'Y';
	        invController.selectedSurvey_DigestiveSymptoms = '哭闹';
	        invController.ans.Q045__c = 'Y';
	        invController.selectedSurvey_AllergySymptoms = '皮肤';
	        invController.ans.Q030_Last__c = 'Y';
	        invController.selectedSurvey_Last_DigestiveSymptoms = '哭闹, 腹泻';
	        invController.ans.Q045_Last__c = 'Y';
	        invController.selectedSurvey_Last_AllergySymptoms = '皮肤, 湿疹';
	        invController.ans.Q023__c = 'Y';
	        invController.selectedMultiSelect_Brand = 'MJN';
	        //update contact
			con.X2nd_Last_Consumption_Brand__c = 'MJN, Yili';
			con.Last_Consumption_Brand__c = 'MJN, Yili';
			con.Is_Last_Digestive_Symptoms__c = 'Y';
			con.Digestive_Symptoms_Type__c = 'Spits;Cry;Fidgety';
			con.Is_Last_Allergy_Symptoms__c = 'Y';
			con.Allergy_Symptoms_Type__c = '皮肤';
			update con; 
	        //run
	        invController.run();
	        Test.stopTest();
        }
    }
    
    private static InvitationFilterController createController(Address_Management__c am, Campaign c)
    {
    	 ///Test Batch
        InvitationFilterController invController = new InvitationFilterController();
        invController.map_Id_SalesSubRegion.put(am.Id,am.Sub_Region__c);
        invController.map_Id_SalesArea.put(am.Id,am.Area__c);
        invController.map_Id_SalesCity.put(am.Id,am.Sales_City__c);
        
        //Enter Filter Info
        invController.inv.Name = 'Test Invitation';
		invController.inv.Campaign_Member_Limit__c = 100;
		invController.selectedCampaignRegion = am.Id;
		invController.selectedCampaignSubRegion = am.Id;
		invController.selectedCampaignArea = am.id;
		invController.selectedCampaignCity = am.Id;
		invController.inv.Campaign__c = c.Id;
        //Select Contact Regions
        invController.selectedSalesRegion = am.Region__c;
        invController.selectedSalesSubRegion = am.Id;
        invController.selectedSalesArea = am.Id;
        invController.selectedSalesCity = am.Id;
        return invController;
    }
    
	private static Address_Management__c createAddress()
	{
		Address_Management__c am = new Address_Management__c();
		am.Region__c = '华北区';
        am.Sub_Region__c = '河北省';
        am.Area__c ='西区';
        am.Sales_City__c = '廊坊市';
        am.Name = '廊坊市';
        insert am;
        return am;
	}
	
	private static Account createAccount()
	{
		Account acc = new Account();
		acc.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_HouseHold' and SobjectType=:'Account'].id;
		acc.Name = 'HouseHold';
		acc.Last_Name__c ='HouseHold';
		acc.Other_Phone_1__c = '15012341234';
		acc.Other_Phone_2__c = '15011111111';
		acc.Mobile_Phone__c = '15411111111';
		acc.Phone = '15711111111';
		acc.status__c = 'Mother';
		return acc;
	}
	
	private static Questionnaire__c createQuestionnaire()
	{
		Questionnaire__c que = new Questionnaire__c();
		que.Name = 's0';
		insert que;
		return que;
	}
	
	private static Contact createContact()
	{
		Contact con = new Contact();
		con.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
		con.LastName = 'conLast';
		con.Birthdate = Date.today().addDays(-5);
		con.Register_Date__c = Date.today().addDays(-4);
		con.Last_Routine_Call_Time__c = Datetime.now().addDays(-4);
		con.X2nd_Last_Consumption_Brand__c = 'MJN, Yili';
		con.Last_Consumption_Brand__c = 'MJN, Yili';
		con.Last_Routine_Call_Result__c = 'No Answer';
		con.Is_Last_Digestive_Symptoms__c = 'Y';
		con.Digestive_Symptoms_Type__c = 'Spits;Cry;Fidgety';
		con.Is_Last_Allergy_Symptoms__c = 'Y';
		con.Allergy_Symptoms_Type__c = '皮肤';
		con.Primary_RecruitChannel_Type__c = '手工/纸质Store';
		con.Primary_RecruitChannel_Sub_type__c = '纸质单';
		con.Specific_Code__c = 'HR1111';
		con.Purchase_Date__c = Date.today().addDays(-5);
		con.DoNotCall = false;
		con.Do_Not_Invite__c = false;
		con.Inactive__c = false;
		return con;
	}
	
	private static Campaign createCamapaign ()
	{
		Campaign c = new Campaign();
        c.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Invitation' and SobjectType=:'Campaign'].id;
        c.Name = 'Test Campaign';
        c.Description = 'Test Description';
        c.Campaign_Member_Limit__c = 10000;
        c.Campaign_Type__c = 'Invitation';
        return c;
	}
	
	private static Answer__c createAnswer()
	{
		//new answer
        Answer__c ans = new Answer__c();
        ans.Status__c = 'Finish';
        ans.Finished_On__c = DateTime.now().addDays(-4);
        ans.Q008__c = 'MJN, Yili';
        ans.Q008_Invitation__c = 'MJN';
        ans.Q038__c = 'Y';
        ans.Q039__c = 'Y';
        ans.Q030__c = 'Y';
        ans.Q011_Text__c = '哭闹';
        ans.Q045__c = 'Y';
        ans.Q046__c = '皮肤';
        ans.Q030_Last__c = 'Y';
        ans.Q011_Last_Text__c = '哭闹;腹泻';
        ans.Q045_Last__c = 'Y';
        ans.Q046_Last__c = '皮肤;湿疹';
        ans.Q023__c = 'Y';
        ans.Q017_Text__c = 'MJN';
        return ans;
	}
}