/*Author:leo.bi@celnet.com.cn
 *Date:2014-6-20
 *function:Execute invitation job following invitation filter and add campaign members
 *Apply To:CN  
 */
global class InvitationBatch implements DataBase.Batchable<SObject> 
{
	private String query;
	private Campaign parentCampaign;
	private String campaignId;
	private Invitation__c inv = new Invitation__c();
	//private String status = 'Success';//record the status
	private String result = 'Success';
	private String log_Message = '';//record the log
	private DateTime jobStartTime = DateTime.now();
	//variables for task
	private DateTime callStartTime;
	private DateTime callEndTime;
	private String callResult;
	
	private String answerSOQL;
	
	public InvitationBatch(Invitation__c inv)
	{
		this.inv = inv;
		this.query = inv.SOQL_Script__c;
		parentCampaign = [select Id,Campaign_Type__c from Campaign where Id=:inv.Campaign__c][0];//????
	}
	
	public InvitationBatch(Invitation__c inv, DateTime callStartTime, DateTime callEndTime, String callResult,String answerSOQL)
	{
		this.inv = inv;
		this.query = inv.SOQL_Script__c;
		this.callStartTime = callStartTime;
		this.callEndTime = callEndTime;
		this.callResult = callResult;
		this.answerSOQL = answerSOQL;
		parentCampaign = [select Id,Campaign_Type__c from Campaign where Id=:inv.Campaign__c][0];
	}
	
	public InvitationBatch(Invitation__c inv, String answerSOQL)
	{
		this.inv = inv;
		this.query = inv.SOQL_Script__c;
		this.answerSOQL = answerSOQL;
		parentCampaign = [select Id,Campaign_Type__c from Campaign where Id=:inv.Campaign__c][0];
	}
	
	
	global DataBase.Querylocator start(DataBase.BatchableContext BC)
	{
		//update log
		inv.Async_Apex_Job__c = BC.getJobId();
		inv.Success_Compaign_Member_Count__c = 0;
		inv.Status__c = 'Running';
		inv.Total_Count__c = 0;
		update inv; 
		return DataBase.getQueryLocator(query);
	}
	
	global void execute(DataBase.BatchableContext BC,List<SObject> scope)
	{
		try
		{
			DateTime startTime;//start time
			DateTime endTime;// end time
			startTime = DateTime.now();
			List<CampaignMember> list_CampaignMember = new List<CampaignMember>();
			Map<Id,Contact> map_Id_Contact = new Map<Id,Contact>();
			Invitation__c temp_inv = [select Async_Apex_Job__c, Campaign_Member_Limit__c, Status__c, Message_Log__c,Success_Compaign_Member_Count__c,Total_Count__c 
								      from Invitation__c 
								      where Async_Apex_Job__c=:BC.getJobId()];
			if(temp_inv.Success_Compaign_Member_Count__c >= temp_inv.Campaign_Member_Limit__c)
			{
				return;
			}
			for(SObject s : scope)
			{
				Contact con = (Contact)s;
				if(con.Account.Duplicate_Status__c != null)
				{
					continue;
				}
				map_Id_Contact.put(con.Id,con);
			}
			//check answer SOQL and check CampaignMember
			this.getContactId(map_Id_Contact);
			system.debug('------------------------map_Id_Contact.size()--------------------------------' + map_Id_Contact.size()) ;
			if(map_Id_Contact.size() == 0)
			{
				return;
			}
			//add camapignmember
			this.packageCampaignMember(map_Id_Contact, list_CampaignMember,temp_inv);
			if(list_CampaignMember.size() == 0)
			{
				return;
			}
			//insert
			this.insertCampaignMember(list_CampaignMember, temp_inv);
			//update result
			endTime = DateTime.now();
			temp_inv.Result__c = this.Result;
			if(temp_inv.Success_Compaign_Member_Count__c != temp_inv.Total_Count__c)
			{
				String newString = 'StartTime:' + String.valueOf(startTime) + ' EndTime' + endTime + '\n' + log_Message;
				String oldString = temp_inv.Message_Log__c;
				temp_inv.Message_Log__c = cutOffString(newString,oldString);
			}
			update temp_inv;
		} catch(Exception e)
		  {
			system.debug('--------------------------exception---------------' + e);
		  }
	}
	
	global void finish(DataBase.BatchableContext BC)
	{
		//Remark:This section need following settings : Email Administration->Deliverability->Access to Send Email->"All Email"
		// Send an email to the Apex job's submitter notifying of job completion.
	    Invitation__c inv_Result = [Select Id, CreatedBy.Email,Status__c,Total_Count__c,Success_Compaign_Member_Count__c, Async_Apex_Job__c, Result__c 
	    							From Invitation__c 
	    							where id =:this.inv.Id];
	    inv_Result.Status__c = 'Completed';//Tommy added at 2014-6-20
	    //inv_Result.Result__c = this.result;//Tommy added at 2014-6-20
	    update inv_Result;//Tommy added at 2014-6-20
	   	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	   	String[] toAddresses = new String[] {inv_Result.CreatedBy.Email}; 
	   	String executeResult ='';
	   	if(inv_Result.Result__c == 'Success') {executeResult = '成功';}
	   	else if(inv_Result.Result__c == 'Failed'){executeResult = '失败';	}
	   	else {executeResult ='未筛选到宝宝';}
	   	mail.setSenderDisplayName('Salesforce');
	   	mail.setToAddresses(toAddresses);
	   	mail.setSubject('市场活动成员筛选完成');
	   	mail.setHtmlBody('您的市场活动成员筛选已经完成' + '<br>' 
	   					 + '开始时间: ' + String.valueOf(jobStartTime) + '<br>'
	   					 + '结束时间: ' + String.valueOf(DateTime.now()) + '<br>'
	   					 + '结果: ' + executeResult + '<br>'
	   					 + '总数: ' + inv_Result.Total_Count__c + '<br>'
	   					 + '成功数: ' + inv_Result.Success_Compaign_Member_Count__c);
	   	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
	
	public String cutOffString(String newString, String oldString)
	{
		String result;
		Integer newLength = newString.length();
		Integer oldLehgth = oldString.length();
		if(newLength + oldLehgth > 28000)
		{
			result = (newString + oldString).subString(0,27999);
		}
		else
		{
			result = newString;
		}
		return result;
	}
	
	public void getContactId(Map<Id,Contact> map_Id_Contact)
	{
		List<CampaignMember> list_ExistingCampaignMember = [select Id, ContactId from CampaignMember where ContactId in :map_Id_Contact.keySet() and CampaignId = :parentCampaign.Id];
		for(CampaignMember cm : list_ExistingCampaignMember)
		{
			if(map_Id_Contact.containsKey(cm.ContactId))
			{
				map_Id_Contact.remove(cm.ContactId);
			}
		}
		if(map_Id_Contact.size() == 0)
		{
			system.debug('-------is already a campaignmember---------');
			return;
		}
		if(this.answerSOQL != null)
		{
			Map<Id,Contact> map_temp = new Map<Id,Contact>();
			String contactId = this.formatContactId(map_Id_Contact.keySet());
			Database.QueryLocator q = Database.getQueryLocator(answerSOQL + ' and Contact__c in ' + contactId);
			Database.QueryLocatorIterator it =  q.iterator(); 
			while (it.hasNext())
			{
			    Answer__c ans = (Answer__c)it.next(); 
			    if(ans.Status__c != 'Finish')
			    {
			    	continue;
			    }
			    if(map_Id_Contact.containsKey(ans.Contact__c))
			    {
			    	map_temp.put(ans.Contact__c,map_Id_Contact.get(ans.Contact__c)); 
			    }
			}
			map_Id_Contact.clear();
			for(Id i : map_temp.keySet())
			{
				map_Id_Contact.put(i,map_temp.get(i));
			}
		} 
	}
	
	public CampaignMember createNewCampaignMember(Id ContactId)
	{
		CampaignMember temp_CampaignMember = new CampaignMember();
		temp_CampaignMember.ContactId = ContactId;
		temp_CampaignMember.CampaignId = this.parentCampaign.Id;
		temp_CampaignMember.Status = 'Sent';
		temp_CampaignMember.Call_Status__c = 'Waiting Invitation';
		if(this.parentCampaign.Campaign_Type__c == 'Invitation Special')
		{
			temp_CampaignMember.Call_Status__c = 'Invitation Special';
		}
		return temp_CampaignMember;
	}
	
	public void packageCampaignMember(Map<ID,Contact> map_Id_Contact,List<CampaignMember> list_CampaignMember,Invitation__c temp_inv)
	{
		for(ID contactId : map_Id_Contact.keySet())
		{
			Contact con = map_Id_Contact.get(contactId);
			if(this.callEndTime == null && this.callStartTime == null && this.callResult == null) 
			{
				CampaignMember cm = this.createNewCampaignMember(contactId);
				if(temp_inv.Success_Compaign_Member_Count__c < temp_inv.Campaign_Member_Limit__c)
				{
					list_CampaignMember.add(cm);
					temp_inv.Success_Compaign_Member_Count__c = temp_inv.Success_Compaign_Member_Count__c + 1;
					temp_inv.Total_Count__c = temp_inv.Total_Count__c + 1;
				}
			}
			if(this.callEndTime != null && this.callStartTime != null && this.callResult != null)
			{
				if(con.Tasks.size() == 0)
				{
					continue;
				}
				if(con.Tasks[0].Result__c == this.callResult 
				&& con.Tasks[0].CreatedDate > this.callStartTime 
				&& con.Tasks[0].CreatedDate < this.callEndTime)
				{
					CampaignMember cm = this.createNewCampaignMember(contactId);
					if(temp_inv.Success_Compaign_Member_Count__c < temp_inv.Campaign_Member_Limit__c)
					{
						list_CampaignMember.add(cm);
						temp_inv.Success_Compaign_Member_Count__c = temp_inv.Success_Compaign_Member_Count__c + 1;
						temp_inv.Total_Count__c = temp_inv.Total_Count__c + 1;
					}
				}
			}
		}
	}
	
	public void insertCampaignMember(List<CampaignMember> list_CampaignMember,Invitation__c tempInv)
	{
		CampaignMember[] cms = list_CampaignMember;
		DataBase.Saveresult[] srList = Database.insert(cms, false);
		Integer i = 0;//the subscript of the array
		for(DataBase.Saveresult sr : srList)
		{
		    if (!sr.isSuccess()) 
		    {   
		    	//If there is a record having error, regard the whole job as a failure and successCount minus one
		    	tempInv.Success_Compaign_Member_Count__c = tempInv.Success_Compaign_Member_Count__c - 1;
		    	this.Result = 'Failed';
		    	//check the errorCode, errorErrorMessage, Contact Id
				String errorItem = '';
		    	Map<String,String> map_Code_Message = new Map<String,String>();
		        for(Database.Error err : sr.getErrors())
		        {                  
		            //add error code
		            String statusCode = String.valueOf(err.getStatusCode());
		            String message = String.valueOf(err.getMessage());
		            //package the error message and code
	            	map_Code_Message.put(statusCode,message);
		        }
		        //record the error message
		        Integer errorFlag = 0;
	        	errorItem += 'Error ContactId:' + cms[i].ContactId;
	        	for(String e : map_Code_Message.keySet())
	        	{
	        		errorItem +='\n' + 'Error Code:' + e + ' ErrorMessage:' + map_Code_Message.get(e);
	        		if(errorFlag == map_Code_Message.size() - 1)
	        		{
	        			errorItem +='\n';
	        		}
	        		errorFlag++;
	        	}
		        this.log_Message += errorItem;
		    }
		    i++;
		}
	}
	
	public String formatContactId(Set<Id> set_ContactId)
	{
		string result = '';
		List<Id> list_Result = new List<Id>();
		list_Result.addAll(set_ContactId);
		if(list_Result.size() == 1)
		{
			result ='(\'' + list_Result[0] + '\')';
		}
		else
		{
			for(Integer i = 0; i < list_Result.size(); i++)
			{
				if(i == (list_Result.size()-1))
				{
					result += '\'' + list_Result[i] + '\'';
				}
				else
				{
					result += '\'' + list_Result[i] + '\'' + ',';
				}
			}
			result = '(' + result + ')';
		}
		return result;
	}
	
}