/*
By Tommy 2014/2/15  
Description: this is temp class for fixing data after imported data.

1，联系人数据修复
String query = ' Select '
				+ ' Id,'
				+ ' Gender__c,'
				+ ' Last_Consumption_Product_Name__c,'
				+ ' Last_Consumption_Brand__c,'
				+ ' Last_Consumption_Product_Nature__c,'
				+ ' Birthdate,'
				+ ' RecordType.Name,'
				+ ' Account.Can_be_called__c,'
				+ ' Account_can_be_called__c,'
				+ ' Inactive__c,'
				+ ' HouseHold_Inactive__c,'
				+ ' tmp_doctorID__c,'
				+ ' tmp_hospitalID__c,'
				+ ' Primary_RecruitChannel_Type__c,'
				+ ' Primary_RecruitChannel_Sub_type__c,'
				+ ' (Select Id, Channel__c,Sub_Channel__c From Recruitment_Channels__r Where Primary__c = true)'
				+ ' From Contact'
				+ ' Where RecordType.DeveloperName =\'Household_Child\''
				+ ' And Account.Region__c = \'Macao-澳門\'';
Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc(query);
job.addAction('FixContactGender');
job.addAction('FixContactLastConsumption');
job.addAction('FixContactBirthDayEvenOdd');
job.addAction('FixContactCanbeCalled');
job.addAction('FixContactHospitalAndDoctor');
job.addAction('FixContactRecruitmentChannel');
Database.ExecuteBatch(job, 2000);


2，Case数据修复
String query = 'Select Id, Description, Description__c From Case Where Account.Region__c = \'Macao-澳門\'';
Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc(query);
job.addAction('FixCaseDescription');
Database.ExecuteBatch(job, 2000);



//以下为单项调用方式

//Fix contact Gender: F->Female, M->Male, U->Unclassified, null->Unclassified
Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc('FixContactGender', 'Select Id, Gender__c From Contact Where Gender__c=\'F\' or Gender__c=\'M\' or Gender__c=\'U\' or Gender__c=null');
Database.ExecuteBatch(job, 2000);


//Fix contact: Last_Consumption_Brand__c=product__c.Brand__c, Last_Consumption_Product_Nature__c=product__c.Product_Nature__c
//根据联系人上的Last_Consumption_Product_Name__c更新Last_Consumption_Brand__c和Last_Consumption_Product_Nature__c
Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc('FixContactLastConsumption', 'Select Id, Last_Consumption_Product_Name__c, Last_Consumption_Brand__c, Last_Consumption_Product_Nature__c From Contact');
Database.ExecuteBatch(job, 2000);


//Fix contact.Birth_Day_Even_Odd__c by Birthdate Y
//根据生日算出并更新奇偶性
Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc('Select Id, Birthdate, RecordType.Name From Contact');
job.addAction('FixContactBirthDayEvenOdd');
Database.ExecuteBatch(job, 2000);


//Fix contact.Canbe_be_Called
//根据客户上的CanbeCall更新联系人的Canbecalled
Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc('Select Id, Account.Can_be_called__c, Account_can_be_called__c, RecordType.Name, Inactive__c, HouseHold_Inactive__c From Contact');
job.addAction('FixContactCanbeCalled');
Database.ExecuteBatch(job, 2000);

//Fix contact hospital & doctor
Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc('Select Id, Birthdate, tmp_doctorID__c, tmp_hospitalID__c, RecordType.Name From Contact Where RecordType.Name = \'Household Child\'');
job.addAction('FixContactHospitalAndDoctor');
Database.ExecuteBatch(job, 2000);

//fix Contact Primary recruitment Channel
Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc('Select Id, Primary_RecruitChannel_Type__c, Primary_RecruitChannel_Sub_type__c,(Select Id, Channel__c,Sub_Channel__c From Recruitment_Channels__r Where Primary__c = true) From Contact Where RecordType.DeveloperName =\'Household_Child\'');
job.addAction('FixContactRecruitmentChannel');
Database.ExecuteBatch(job, 2000);


//Fix case description
Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc('Select Id, Description, Description__c From Case');
job.addAction('FixCaseDescription');
Database.ExecuteBatch(job, 2000)


//Fix contact.age 
//已经将age改回公式字段了，所以不需要修复
Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc('Select Id, Birthdate, RecordType.Name From Contact');
job.addAction('FixContactAge');
Database.ExecuteBatch(job, 2000);

//Fix Order.Product_Ordered,
Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc('Select Cancelled__c, Product_Ordered__c, Order_Number__c, Legacy_ID__c (Select Order__c,Product__c,Product__r.Name, Product__r.Product_SKU__c From Order_Items__r where Product__c != null order by Product__r.Name) From Order__c ');
job.addAction('FixOrderProductOrdered');
Database.ExecuteBatch(job, 2000);

//fix Date
Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc('Select Id, Birthday_1st_Child__c, Birthday_2nd_Child__c, Birthday_3rd_Child__c, Birthday_4th_Child__c From Lead');
job.addAction('FixDate');
Database.ExecuteBatch(job, 2000);
Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc('Select Id, Birthdate__c From Account');
job.addAction('FixDate');
Database.ExecuteBatch(job, 2000);
Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc('Select Id, Birthdate From Contact');
job.addAction('FixDate');
Database.ExecuteBatch(job, 2000);
Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc('Select Id, StartDate, EndDate From Campaign');
job.addAction('FixDate');
Database.ExecuteBatch(job, 2000);
Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc('Select Id, Cancel_Date__c, Redeem_Date__c From Coupon__c');
job.addAction('FixDate');
Database.ExecuteBatch(job, 2000);
Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc('Select Id, Expected_Delivery_Date__c From Order__c');
job.addAction('FixDate');
Database.ExecuteBatch(job, 2000);

//fix OB Task Case
Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc('Select ID, Subject, WhatId From Task Where Subject=\'Outbound Call\' and WhatId != null');
job.addAction('FixTaskOBCase');
Database.ExecuteBatch(job, 2000);

//dddf
Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc('Select Id, Last_Consumption_Product_Name__c, Last_Consumption_Brand__c, Last_Consumption_Product_Nature__c, Gender__c, Birthdate, Account.Can_be_called__c, Account_can_be_called__c, RecordType.Name, Inactive__c, HouseHold_Inactive__c From Contact');
job.addAction('FixContactGender');
job.addAction('FixContactLastConsumption');
job.addAction('FixContactBirthDayEvenOdd');
job.addAction('FixContactCanbeCalled');
job.addAction('FixContactAge');

Database.ExecuteBatch(job, 5000);


*/ 
global class Tmp_DataActionBatch_Mc implements Database.Batchable<SObject>,Database.Stateful
{
    private List<String> actionList = new List<String>();
    global String Query {get; set;}
    global Integer failedCount;
    global String failedStr;
    global void addAction(String action)
    {
        this.actionList.add(action);
    }
    global Tmp_DataActionBatch_Mc(String query)
    { 
        this.failedCount = 0;
        this.failedStr = '';
        this.Query = query;
    }
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        this.failedCount = 0;
        this.failedStr = '';
        return Database.getQueryLocator(this.Query);
    }
    global void execute(Database.BatchableContext BC, List<SObject> scope)
    {
        Boolean changed = false;
        for(String action : this.actionList)
        {
            if(action == 'FixContactGender')
            {
                this.FixContactGender(BC,scope);
                changed = true;
            }
            else if(action == 'FixContactLastConsumption')
            {
                this.FixContactLastConsumption(BC,scope);
                changed = true;
            }
            else if(action == 'FixContactBirthDayEvenOdd')
            {
                this.FixContactBirthDayEvenOdd(BC,scope);
                changed = true;
            }
            else if(action == 'FixContactCanbeCalled')
            {
                this.FixContactCanbeCalled(BC,scope);
                changed = true;
            }
            else if(action == 'FixContactAge')
            {
                this.FixContactAge(BC,scope);
                changed = true;
            }
            else if(action == 'FixOrderProductOrdered')
            {
                this.FixOrderProductOrdered(BC,scope);
                changed = true;
            }
            else if(action == 'FixContactHospitalAndDoctor')
            {
                this.FixContactHospitalAndDoctor(BC,scope);
                changed = true;
            }
            else if(action == 'FixCaseDescription')
            {
                this.FixCaseDescription(BC,scope);
                changed = true;
            }
            else if(action == 'FixDate')
            {
            	this.FixDate(BC,scope);
            	changed = true;
            }
            else if(action == 'FixTaskOBCase')
            {
            	this.FixTaskOBCase(BC, scope);
				changed = true;
            }
            else if(action == 'FixContactRecruitmentChannel')
            {
            	this.FixContactRecruitmentChannel(BC, scope);
				changed = true;
            }
        }
        Boolean hasError = false;
        if(changed)
        {
            Database.SaveResult[] res = Database.update(scope, false);
            for(Database.SaveResult re : res)
            {
                if(!re.isSuccess())
                {
                    hasError = true;
                    failedCount ++;
                    //this.failedStr += re.getId() + ',';
                    List<Database.Error> erlist = re.getErrors();
                    if(erlist != null && erlist.size()>0)
                    {
                    	if(failedStr.length() < 10000)
                    	{
                    		this.failedStr += erlist[0].getMessage();
                    	}
                    }
                }
            }
        } 
    }
    private void FixContactRecruitmentChannel(Database.BatchableContext BC, List<SObject> scope)
    {
    	//new Tmp_DataActionBatch_Mc('Select Id, Primary_RecruitChannel_Type__c, Primary_RecruitChannel_Sub_type__c,(Select Id, Channel__c,Sub_Channel__c From Recruitment_Channels__r Where Primary__c = true) From Contact Where RecordType.DeveloperName =\'Household_Child\'');
    	for(sObject obj:scope)
		{
			Contact cc = (Contact)obj;
			if(cc.Recruitment_Channels__r != null && cc.Recruitment_Channels__r.size() > 0)
			{
				cc.Primary_RecruitChannel_Type__c = cc.Recruitment_Channels__r[0].Channel__c;
				cc.Primary_RecruitChannel_Sub_type__c = cc.Recruitment_Channels__r[0].Sub_Channel__c;
			}
			else
			{
				cc.Primary_RecruitChannel_Type__c = null;
				cc.Primary_RecruitChannel_Sub_type__c = null;
			}
		}
    }
    
    private void FixTaskOBCase(Database.BatchableContext BC, List<SObject> scope)
    {
    	for(sObject obj:scope)
		{
			Task t = (Task)obj;
			if(t.Subject == 'Outbound Call')
			{
				t.WhatId = null;
			}
		}
    }
    
    private void FixDate(Database.BatchableContext BC, List<SObject> scope)
    {
    	for(sObject obj:scope)
		{
			if(obj instanceof Lead)
			{
				Lead l = (Lead)obj;
				l.Birthday_1st_Child__c = this.resetDate(l.Birthday_1st_Child__c);
				l.Birthday_2nd_Child__c = this.resetDate(l.Birthday_2nd_Child__c);
				l.Birthday_3rd_Child__c = this.resetDate(l.Birthday_3rd_Child__c);
				l.Birthday_4th_Child__c = this.resetDate(l.Birthday_4th_Child__c);
			}
			else if(obj instanceof Account)
			{
				Account a = (Account)obj;
				a.Birthdate__c = this.resetDate(a.Birthdate__c);
			}
			else if(obj instanceof Contact)
			{
				Contact co = (Contact)obj;
				co.Birthdate = this.resetDate(co.Birthdate);
			}
			else if(obj instanceof Campaign)
			{
				Campaign cm = (Campaign)obj;
				cm.StartDate = this.resetDate(cm.StartDate);
				cm.EndDate = this.resetDate(cm.EndDate);
				cm.Event_Date__c = this.resetDate(cm.Event_Date__c);
			}
			else if(obj instanceof Coupon__c)
			{
				Coupon__c cop = (Coupon__c)obj;
				cop.Cancel_Date__c = this.resetDate(cop.Cancel_Date__c); 
				cop.Redeem_Date__c = this.resetDate(cop.Redeem_Date__c);
			}
			else if(obj instanceof Order__c)
			{
				Order__c od= (Order__c)obj;
				od.Expected_Delivery_Date__c = this.resetDate(od.Expected_Delivery_Date__c);
			}
		}
    }
    private Date resetDate(Date da)
    {
    	if(da == null)
    	{
    		return null;
    	}
		Date nullDate = Date.newInstance(1910, 12, 31);
		if(da <= nullDate)
		{
			return null;
		}
		return da;
    }
    
    private void FixCaseDescription(Database.BatchableContext BC, List<SObject> scope)
    {
    	for(sObject obj:scope)
		{
			Case cas = (Case)obj;
			if(cas.Description != null)
			{
				String des = cas.Description.Trim();
				if(des.length() > 255)
				{
					des = des.substring(0, 254);
				}
				cas.Description__c = des;
			}
		}
    }
    
 	private void FixContactHospitalAndDoctor(Database.BatchableContext BC, List<SObject> scope)
	{	
		set<string> docCodes = new set<string>();
		set<string> hosCodes = new set<string>();
		map<string, Account> docMap = new map<string, Account>();
		map<string, Account> hosMap = new map<string, Account>();
	  	for(sObject obj:scope)
		{
			Contact co = (Contact)obj;
			if(co.tmp_doctorID__c != null)
			{
				docCodes.add(co.tmp_doctorID__c);
			}
			if(co.tmp_hospitalID__c != null)
			{ 
				hosCodes.add(co.tmp_hospitalID__c);
			}
		}
		
		for(Account doc: [Select Id, Name, Member_ID__c From Account Where RecordType.Name = 'Doctor' And Member_ID__c in: docCodes])
		{
			docMap.put(doc.Member_ID__c, doc);
		}//Hospital
		for(Account hos: [Select Id, Name, Member_ID__c From Account Where RecordType.Name = 'Hospital' And Member_ID__c in: hosCodes])
		{
			hosMap.put(hos.Member_Id__c, hos);
		}//
		for(sObject obj:scope)
		{
			Contact co = (Contact)obj;
			if(co.tmp_doctorID__c != null)
			{
				Account doc = docMap.get(co.tmp_doctorID__c);
				if(doc != null)
				{
					co.Attending_Doctor__c = doc.Id;
				}
			}
			if(co.tmp_hospitalID__c != null)
			{
				Account hos = hosMap.get(co.tmp_hospitalID__c);
				if(hos != null)
				{
					co.Born_Hospital__c = hos.Id;
				}
			}
			//co.Birthdate = co.Birthdate.addHours(+8);
		}
	}

    
    private void FixOrderProductOrdered(Database.BatchableContext BC, List<SObject> scope)
    {
        for(sObject obj:scope)
        {
            Order__c ord = (Order__c)obj;
            ord.Legacy_ID__c = ord.Order_Number__c;
            ord.Product_Ordered__c = '';
            if(!ord.Cancelled__c)
            {
	            if(ord.Order_Items__r != null && ord.Order_Items__r.size() > 0)
	            {
	                for(Order_Item__c ordItem:ord.Order_Items__r)
	                {
	                    if(ord.Product_Ordered__c == '')
	                    {
	                    	if(ordItem.Product__r.Product_SKU__c != null)
	                    	{
	                        	ord.Product_Ordered__c = ordItem.Product__r.Product_SKU__c;
	                    	}
	                    }
	                    else
	                    {
	                    	if(ordItem.Product__r.Product_SKU__c != null)
	                    	{
	                        	ord.Product_Ordered__c +=(';'+ ordItem.Product__r.Product_SKU__c);
	                    	}
	                    }
	                }
	            }
            }
        }
    }
    public static testMethod void Test_FixOrderProductOrdered()
    {
        Product__c prod1 = new Product__c();
        prod1.Name = 'prod1';
        prod1.Product_SKU__c = 'prod1';
        insert prod1;
        Product__c prod2 = new Product__c();
        prod2.Name = 'prod2';
        prod2.Product_SKU__c = 'prod2';
        insert prod2;
        Product__c prod3 = new Product__c();
        prod3.Name = 'prod3';
        prod3.Product_SKU__c = 'prod3';
        insert prod3;
        Id accRecordTypeId = [Select Name, Id From RecordType Where SobjectType = 'Account' And DeveloperName='HouseHold_New'].Id;
        Account acc = new Account();
        acc.Last_Name__c = 'Test Account';
        acc.Name = 'Test Account';
        acc.RecordTypeId = accRecordTypeId;
        acc.Member_ID__c = Date.today().year()+'123';
        insert acc;
        Order__c ord = new Order__c();
        ord.Cancelled__c = false;
        ord.HouseHold__c = acc.id;
        insert ord;
        Order_Item__c ordItem1 = new Order_Item__c();
        ordItem1.Order__c = ord.id;
        ordItem1.Product__c = prod1.id;
        insert ordItem1;
        
        Order_Item__c ordItem2 = new Order_Item__c();
        ordItem2.Order__c = ord.id;
        ordItem2.Product__c = prod2.id;
        insert ordItem2;
        
        Order_Item__c ordItem3 = new Order_Item__c();
        ordItem3.Order__c = ord.id;
        ordItem3.Product__c = prod3.id;
        insert ordItem3;
        system.Test.startTest();
        Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc('Select Cancelled__c, Product_Ordered__c, Order_Number__c, Legacy_ID__c, (Select Order__c,Product__c,Product__r.Name, Product__r.Product_SKU__c From Order_Items__r where Product__c != null order by Product__r.Name) From Order__c ');
        job.addAction('FixOrderProductOrdered');
        Database.ExecuteBatch(job, 2000);
        system.Test.stopTest();
        ord = [select Product_Ordered__c from Order__c where id=:ord.id];
        system.assertEquals(ord.Product_Ordered__c, 'prod1;prod2;prod3');
    }
    private void FixContactGender(Database.BatchableContext BC, List<SObject> scope)
    {
        for(sObject obj : scope)
        {
            Contact cot = (Contact)obj;
            if(cot.Gender__c == 'F')
            {
                cot.Gender__c = 'Female';
            }
            else if(cot.Gender__c == 'M')
            {
                cot.Gender__c = 'Male';
            }
            else if(cot.Gender__c == 'U')
            {
                cot.Gender__c = 'Unclassified';
            }
            else if(cot.Gender__c == '' || cot.Gender__c == null)
            {
                cot.Gender__c = 'Unclassified';
            }
        }
    }
    public static testMethod void Test_FixContactGender()
    {
        Id accRecordTypeId = [Select Name, Id From RecordType Where SobjectType = 'Account' And Name='Hospital'].Id;
        Account acc = new Account();
        acc.Last_Name__c = 'Test Account';
        acc.Name = 'Test Account';
        acc.RecordTypeId = accRecordTypeId;
        insert acc;
        Contact MContact = new Contact(AccountId = acc.Id, LastName='MContact', Gender__c = 'M');
        Contact FContact = new Contact(AccountId = acc.Id, LastName='FContact', Gender__c = 'F');
        Contact UContact = new Contact(AccountId = acc.Id, LastName='UContact', Gender__c = 'U');
        Contact NullContact = new Contact(AccountId = acc.Id, LastName='NullContact', Gender__c = null);
        Contact[] conts = new Contact[]{MContact, FContact, UContact, NullContact};
        insert conts;
        test.startTest();
        Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc('Select Id, Gender__c From Contact Where Account.Name = \'Test Account\'');
        job.addAction('FixContactGender');
        Database.ExecuteBatch(job, 2000);
        test.stopTest();
        system.assertEquals('Male', [Select Gender__c From Contact Where Id=:MContact.Id].Gender__c);
        system.assertEquals('Female', [Select Gender__c From Contact Where Id=:FContact.Id].Gender__c);
        system.assertEquals('Unclassified', [Select Gender__c From Contact Where Id=:UContact.Id].Gender__c);
        system.assertEquals('Unclassified', [Select Gender__c From Contact Where Id=:NullContact.Id].Gender__c);
        
    }
    private void FixContactLastConsumption(Database.BatchableContext BC, List<SObject> scope)
    {
        map<String, Product__c> proMap = new map<String, Product__c>();
        for(Product__c pro: [Select Id, Name, Brand__c, Product_Nature__c From Product__c])
        {
            if(pro.Name != null)
            {
                proMap.put(pro.Name, pro);
            }
        }
        for(sObject obj : scope)
        {
            Contact cot = (Contact)obj;
            if(cot.Last_Consumption_Product_Name__c == null)
            {
                continue;
            }
            Product__c mappedPro = proMap.get(cot.Last_Consumption_Product_Name__c);
            if(mappedPro != null)
            {
                if(cot.Last_Consumption_Brand__c == null)
                {
                    cot.Last_Consumption_Brand__c = mappedPro.Brand__c;
                }
                if(cot.Last_Consumption_Product_Nature__c == null)
                {
                    cot.Last_Consumption_Product_Nature__c = mappedPro.Product_Nature__c;
                }
            }
        }
    }
    
    public static testMethod void Test_FixContactLastConsumption()
    {
        Id accRecordTypeId = [Select Name, Id From RecordType Where SobjectType = 'Account' And Name='Hospital'].Id;
        Account acc = new Account();
        acc.Last_Name__c = 'Test Account';
        acc.Name = 'Test Account';
        acc.RecordTypeId = accRecordTypeId;
        insert acc;
        Contact cont = new Contact(
            AccountId = acc.Id, 
            LastName='Test Contact', 
            Last_Consumption_Product_Name__c = 'Test Product',  
            Last_Consumption_Product_Nature__c = null,
            Last_Consumption_Brand__c = null);
        insert cont;
        Product__c pro = new Product__c(
            Name = 'Test Product',
            Product_Nature__c = 'MJ',
            Brand__c = 'Mead Johnson');
        insert pro;
        test.startTest();
        Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc('Select Id, Last_Consumption_Product_Name__c, Last_Consumption_Brand__c, Last_Consumption_Product_Nature__c From Contact Where Account.Name = \'Test Account\'');
        job.addAction('FixContactLastConsumption');
        Database.ExecuteBatch(job, 2000);
        test.stopTest();
        cont = [Select Id, Last_Consumption_Product_Nature__c, Last_Consumption_Brand__c From Contact Where Id=:cont.Id];
        system.assertEquals('MJ', cont.Last_Consumption_Product_Nature__c);
        system.assertEquals('Mead Johnson', cont.Last_Consumption_Brand__c);
    }
    
    private void FixContactBirthDayEvenOdd(Database.BatchableContext BC, List<SObject> scope)
    {
        for(sObject obj : scope)
        {
            Contact cot = (Contact)obj;
            if(cot.RecordType.Name == 'Household Child')
            {
                if(cot.Birthdate != null)
                {
                    Date birthdate = cot.Birthdate;
                    Integer theDay = birthdate.day();
                    Integer remainder = math.mod(theDay, 2);
                    if(remainder == 0)
                    {
                        cot.Birth_Day_Even_Odd__c = 'Even';
                    }
                    else 
                    {
                        cot.Birth_Day_Even_Odd__c = 'Odd';
                    }
                }
            }
        }
    }
    public static testMethod void Test_FixContactBirthDayEvenOdd()
    {
        Id recordTypeId = [Select Name, Id From RecordType Where SobjectType = 'Contact' And Name='Household Child'].Id;
        Id accRecordTypeId = [Select Name, Id From RecordType Where SobjectType = 'Account' And Name='Hospital'].Id;
        Account acc = new Account();
        acc.Last_Name__c = 'Test Account';
        acc.Name = 'Test Account';
        acc.RecordTypeId = accRecordTypeId;
        insert acc;
        Contact EvContact = new Contact(AccountId = acc.Id, RecordTypeId = recordTypeId, LastName='EvContact', Birthdate=Date.newInstance(2013, 8, 8));
        Contact OdContact = new Contact(AccountId = acc.Id, RecordTypeId = recordTypeId, LastName='OdContact', Birthdate=Date.newInstance(2013, 5, 5));
        Contact[] conts = new Contact[]{EvContact, OdContact};
        insert conts;
        //List<Contact> contList = [Select Id, Birthdate, Birth_Day_Even_Odd__c, RecordType.Name From Contact Where Account.Name = 'Test Account'];
        //System.Debug('sssssss' + contList.size());
        test.startTest();
        Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc('Select Id, Birthdate, Birth_Day_Even_Odd__c, RecordType.Name From Contact Where Account.Name = \'Test Account\'');
        job.addAction('FixContactBirthDayEvenOdd');
        Database.ExecuteBatch(job, 2000);
        test.stopTest();
        system.assertEquals('Even', [Select Birth_Day_Even_Odd__c From Contact Where Id =: EvContact.Id].Birth_Day_Even_Odd__c);
        system.assertEquals('Odd', [Select Birth_Day_Even_Odd__c From Contact Where Id =: OdContact.Id].Birth_Day_Even_Odd__c);
    }
    
    //AND(Account.Can_be_called__c, RecordType.Name=='Household Child', NOT Inactive__c, NOT HouseHold_Inactive__c )
    private void FixContactCanbeCalled(Database.BatchableContext BC, List<SObject> scope)
    {
        for(sObject obj : scope)
        {
            Contact cot = (Contact)obj;
            //if(cot.RecordType.Name == 'Household Child' && cot.Account.Can_be_called__c && !cot.Inactive__c && !cot.HouseHold_Inactive__c)
            if(cot.Account != null)
            {
                cot.Account_can_be_called__c = cot.Account.Can_be_called__c;
                if(cot.Account_can_be_called__c && !cot.Inactive__c)
                {
                    cot.Can_be_called__c = true;
                }
                else
                {
                    cot.Can_be_called__c = false;
                }
            }
        }
    }
    public static testMethod void Test_FixContactCanbeCalled()
    {
        Id recordTypeId = [Select Name, Id From RecordType Where SobjectType = 'Contact' And Name='Household Child'].Id;
        Id accRecordTypeId = [Select Name, Id From RecordType Where SobjectType = 'Account' And Name='Hospital'].Id;
        Account acc = new Account();
        acc.Last_Name__c = 'Test Account';
        acc.Name = 'Test Account';
        //acc.Can_be_called__c = true;
        acc.RecordTypeId = accRecordTypeId;
        insert acc;
        Contact canBeCalledContact = new Contact(AccountId = acc.Id, 
            RecordTypeId = recordTypeId, 
            LastName='CanBenCallContact', 
            Inactive__c = false,
            Inactive_Reason__c = 'Abortion'
            //HouseHold_Inactive__c = false
            );
        Contact[] conts = new Contact[]{canBeCalledContact};
        insert conts;

        test.startTest();
        Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc('Select Id, Account.Can_be_called__c, Account_Can_be_called__c, RecordType.Name, Inactive__c, HouseHold_Inactive__c From Contact Where Account.Name = \'Test Account\'');
        job.addAction('FixContactCanbeCalled');
        Database.ExecuteBatch(job, 2000);
        test.stopTest();
        //system.assertEquals(true, [Select Can_be_called__c From Contact Where Id =: canBeCalledContact.Id].Can_be_called__c);
    }
    
    //If(AND(NOT ISNULL( Birthdate ), RecordType.Name =='Household Child'): 
    //Year(Today())-YEAR(Birthdate))*12+Month(today())-Month(Birthdate)- If( Day(today())>=Day(Birthdate),0,1) 
    private void FixContactAge(Database.BatchableContext BC, List<SObject> scope)
    {
        for(sObject obj : scope)
        {
            Contact cot = (Contact)obj;
            if(cot.RecordType.Name == 'Household Child')
            {
                if(cot.Birthdate != null)
                {
                    Integer age;
                    Integer i;
                    if(Date.today().day() >= cot.Birthdate.day())
                    {
                        i = 0;
                    }
                    else
                    {
                        i = 1;
                    }
                    age = (Date.today().year() - cot.Birthdate.year()) * 12 + Date.today().month() - cot.Birthdate.month() - i;
             // Commented by Johnson to change field back to formula:      cot.Age__c = age;
                }
            }
        }
    }
    
    public static testMethod void Test_FixContactAge()
    {
        Id recordTypeId = [Select Name, Id From RecordType Where SobjectType = 'Contact' And Name='Household Child'].Id;
        Id accRecordTypeId = [Select Name, Id From RecordType Where SobjectType = 'Account' And Name='Hospital'].Id;
        Account acc = new Account();
        acc.Last_Name__c = 'Test Account';
        acc.Name = 'Test Account';
        acc.RecordTypeId = accRecordTypeId;
        insert acc;
        Contact AgeContact = new Contact(
            AccountId = acc.Id, 
            RecordTypeId = recordTypeId, 
            LastName='AgeContact', 
            Birthdate=Date.newInstance(2012, 5, 6));
        Contact[] conts = new Contact[]{AgeContact};
        insert conts;
        test.startTest();
        Tmp_DataActionBatch_Mc job = new Tmp_DataActionBatch_Mc('Select Id, Birthdate, Age__c, RecordType.Name From Contact Where Account.Name = \'Test Account\'');
        job.addAction('FixContactAge');
        Database.ExecuteBatch(job, 2000);
        test.stopTest();
        Integer i;
        if(Date.today().day() >= AgeContact.Birthdate.day())
        {
            i = 0;
        }
        else
        {
            i = 1;
        }
        Integer age = (Date.today().year() - AgeContact.Birthdate.year()) * 12 + Date.today().month() - AgeContact.Birthdate.month() - i;
        system.assertEquals(age, [Select Age__c From Contact Where Id =: AgeContact.Id].Age__c);
    }
    
  global void finish(Database.BatchableContext BC)
  {
      AsyncApexJob a =
            [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
               FROM AsyncApexJob WHERE Id = :BC.getJobId()];
               System.assertEquals('', this.failedStr);

/*
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    String[] toAddresses = new String[] {'tommy.liu@celnet.com.cn'};
    mail.setToAddresses(toAddresses);
    mail.setSubject('Action Batch report');
    mail.setPlainTextBody('Error:' + this.failedCount + ' Error Record Id:' + this.failedStr);
    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    */
  }
  
}