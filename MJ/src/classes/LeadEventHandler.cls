/*
Author: scott.zhang@celnet.com.cn 
Created On: 2014-06-09
Function: 
1.update Call Status from CampaignMember
2.Dispatch queue 
Apply To: CN 
*/
public class LeadEventHandler implements Triggers.Handler {
	public void Handle()
	{
		if(!Context.ApplyTo(new String[] {'CN'})) 
    	{
    		return;
		}
		if(trigger.isAfter && (trigger.isInsert || trigger.isUpdate))
		{
			if(trigger.new[0] instanceof CampaignMember)
			{
				this.UpdateFromCampaignMember();
			}
		}
		if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate))
		{
			if(trigger.new[0] instanceof Lead)
			{
				this.DispatchQueue();
			}
		}
		
	}
	//Update Call Status from CampaignMember
	private void UpdateFromCampaignMember()
	{
		List<CampaignMember> MemberList = trigger.new;
		Set<Id> Set_LeadIds = new Set<Id>();
		for(CampaignMember cm : MemberList)
		{
			if(cm.LeadId == null)
			{
				continue;
			}
			if(trigger.isInsert && cm.LeadId !=null)
			{
				Set_LeadIds.add(cm.LeadId);
			}
			else if(Trigger.isUpdate)
			{
				CampaignMember oldcm = (CampaignMember)trigger.oldMap.get(cm.Id);
				if(cm.Call_Status__c != oldcm.Call_Status__c && cm.LeadId != null)
				{
					Set_LeadIds.add(cm.LeadId);
				}
			}
		}
		if(Set_LeadIds.size()==0)
		return;
		
		List<Lead> List_UpLead = new List<Lead>();
		for(Lead lea : [select Id,Call_Status__c from Lead where Id in:Set_LeadIds and RecordType.DeveloperName='CN_Leads' and IsConverted=false])
		{
			for(CampaignMember cm : MemberList)
			{
				if(cm.LeadId != lea.Id)
				{
					continue;
				}
				lea.Call_Status__c = cm.Call_Status__c;
				List_UpLead.add(lea);
			}
		}
		if(List_UpLead.size()>0)
		update List_UpLead;
	}
	//Dispatch queue
	public void DispatchQueue()
	{
		List<Lead> LeadList = trigger.new;
		List<Lead> List_AttendanceLead = new List<Lead>();
		for(Lead lea : LeadList)
		{
			if(lea.Territory__c == null || !lea.Is_From_Attendance__c)
			continue;
			if(trigger.isInsert)
			{
				if(lea.Is_From_Attendance__c)
				List_AttendanceLead.add(lea);
			}
			else if(trigger.isUpdate)
			{
				Lead oldlea = (Lead)trigger.oldMap.get(lea.Id);
				if(lea.Is_From_Attendance__c != oldlea.Is_From_Attendance__c  || (lea.Territory__c != oldlea.Territory__c && lea.Territory__c!=null))
				List_AttendanceLead.add(lea);
			}
			
		}
		
		if(List_AttendanceLead.size()==0)
		return;
		
		Map<String,Group> Map_Queue = new Map<String,Group>();
		for(Group Gr : [Select Type,Name,Id,DeveloperName From Group where Type='Queue' and DeveloperName LIKE 'CN_OB%'])
		{
			Map_Queue.put(Gr.DeveloperName,Gr);
		}
		
		for(Lead lea : List_AttendanceLead)
		{
			if(lea.Territory__c.contains('华东') && Map_Queue.containsKey('CN_OB_Campaign_Follow_Up_Call_EC'))
			{
				lea.OwnerId = Map_Queue.get('CN_OB_Campaign_Follow_Up_Call_EC').Id;
			}
			else if(lea.Territory__c.contains('华南') && Map_Queue.containsKey('CN_OB_Campaign_Follow_Up_Call_SC'))
			{
				lea.OwnerId = Map_Queue.get('CN_OB_Campaign_Follow_Up_Call_SC').Id;
			}
			else if(lea.Territory__c.contains('华西') && Map_Queue.containsKey('CN_OB_Campaign_Follow_Up_Call_WC'))
			{	
				lea.OwnerId = Map_Queue.get('CN_OB_Campaign_Follow_Up_Call_WC').Id;
			}
			else if(lea.Territory__c.contains('华北') && Map_Queue.containsKey('CN_OB_Campaign_Follow_Up_Call_NC'))
			{
				lea.OwnerId = Map_Queue.get('CN_OB_Campaign_Follow_Up_Call_NC').Id;
			}
			else if( Map_Queue.containsKey('CN_OB_Undefined'))
			{
				lea.OwnerId = Map_Queue.get('CN_OB_Undefined').Id;
			}
		}
	}
	
}