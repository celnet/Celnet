/*
This class is used to Redirect all Edits on Order Item to their Mass Add Edit Page
*/
public with sharing class RedirectOrderItem {

	/* Need to use Standard Controller in order to overwrite Standard Edit Button*/
	public RedirectOrderItem(ApexPages.StandardController stdController) {
		//do nothing
	}
	
	/*
		Gets Order Id and redirects to Mass Add Edit Order Item page
	*/
	public PageReference redirect(){
		String id = ApexPages.currentPage().getParameters().get('id');
		if(id != null){
			Id orderId = [SELECT Order__c from Order_Item__c WHERE ID =:id][0].Order__c;
			PageReference p = Page.MassAddEditOrderItems;
			p.getParameters().put('id',orderId);
			return p;
		}
		return null;
	
	}
}