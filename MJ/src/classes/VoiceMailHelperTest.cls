/*Author:leo.bi@celent.com.cn
 *Date:2014-07-13
 *Function:test VoiceMailHelper
 */
@isTest
private class VoiceMailHelperTest 
{

    static testMethod void myUnitTest() 
    {
    	system.runas(TestUtility.new_CN_User())
    	{
	    	Account acc = new Account();
			acc.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_HouseHold' and SobjectType=:'Account'].id;
			acc.Name = 'HouseHold';
			acc.Last_Name__c ='HouseHold';
			acc.Phone = '15711111111';
			acc.status__c = 'Mother';
			insert acc;
			Id newLeadRecordTypeId = [select id from RecordType where DeveloperName=:'CN_Leads' and SobjectType=:'Lead'].id;
	        Lead lea = new Lead();
	        lea.LastName = String.valueOf(Date.today()) + 'newlead';
	        lea.RecordTypeId = newLeadRecordTypeId;
	        lea.Phone = '15122222222';
	        insert lea;
	        Task t = new Task();
	        t.WhoId = lea.Id;
	        t.CallObject = String.valueOf(DateTime.now()) + 'leo';
	        insert t; 
	       // VoiceMailHelper.createVoiceMailRecord('Task', 'test.com', '15711111111', '15711112222', 'type', 2, '北京');
	        //VoiceMailHelper.createVoiceMailRecord('Task2', 'test2.com', '15122222222', '15711112222', 'type2', 3, '北京');
	        CN_WebService.createVoiceMailMessage('Task', 'test.com', '15711111111', '15711112222', 'type', 2, '北京');
	        CN_WebService.createVoiceMailMessage('Task2', 'test2.com', '15122222222', '15711112222', 'type2', 3, '北京');
	        CN_WebService.updateSatisfactionSurveyRating(t.CallObject, '满意', DateTime.now().addMinutes(-5), DateTime.now().addMinutes(-1));
    	}
    }
}