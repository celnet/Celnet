/*
*Author:michael.mi@celnet.com.cn
*Date:2014-09-04
*Function:Entity Class for Wechat Xml & Json Serialize, Deserialize
*/
public class WechatEntity 
{  
	//message type   
	public static final string MESSAGE_TYPE_TEXT = 'text';
	public static final string MESSAGE_TYPE_IMAGE = 'image';
	public static final string MESSAGE_TYPE_EVENT = 'event';
	public static final string MESSAGE_TYPE_VOICE = 'voice';
	public static final string MESSAGE_TYPE_VIDEO = 'video';
	public static final string MESSAGE_TYPE_LOCATION = 'location';   
	public static final string MESSAGE_TYPE_LINK = 'link';
	public static final string MESSAGE_TYPE_TRANSFER = 'transfer_customer_service';
	public static final string MESSAGE_TYPE_NEWS = 'news';
	public static final string EVENT_TYPE_SUBSCRIBE = 'subscribe';
	public static final string EVENT_TYPE_UNSUBSCRIBE = 'unsubscribe';
	public static final string EVENT_TYPE_CLICK = 'CLICK'; 	
    
    //parse common callin information
    private void GetBaseMsg(CallinBaseMsg baseMsg, string xml)
    {
    	baseMsg.ToUserName = xml.substringBetween('<ToUserName><![CDATA[',']]></ToUserName>');
		baseMsg.FromUserName = xml.substringBetween('<FromUserName><![CDATA[',']]></FromUserName>');
		Integer duration = Integer.valueof(xml.substringBetween('<CreateTime>','</CreateTime>'));
		datetime timeBegin = DateTime.newInstanceGMT(1970,1,1,0,0,0);
		baseMsg.CreateTime =  timeBegin.addSeconds(duration);				
		baseMsg.MsgType = xml.substringBetween('<MsgType><![CDATA[',']]></MsgType>');
		baseMsg.MsgId = xml.substringBetween('<MsgId>','</MsgId>');
    }
    
    //write common xml part
	public XmlStreamWriter writeXMLUtil(XmlStreamWriter w, String msgType, CallinBaseMsg baseMsg)
    {
		w.writeStartElement(null,'ToUserName',null);
		w.writeCData(baseMsg.ToUserName);
		w.writeEndElement();
		w.writeStartElement(null,'FromUserName',null);
		w.writeCData(baseMsg.FromUserName);
		w.writeEndElement();
		w.writeStartElement(null,'CreateTime',null);
		w.writeCharacters(String.valueOf(baseMsg.CreateTime.getTime()));
		w.writeEndElement();
		w.writeStartElement(null,'MsgType',null);
		w.writeCData(msgType);
		w.writeEndElement();   		
		return w;
    }
    
    //Serialize(XML)
	public string SerializeCallinResponseMsg(CallinBaseMsg msg)
	{
		system.debug('********msg**********' + msg);
		XmlStreamWriter w = new XmlStreamWriter();
		w.writeStartElement(null,'xml',null);
		if(msg.MsgType == MESSAGE_TYPE_TEXT)
		{
			OutTextMsg outMsg = (OutTextMsg)msg;
			this.writeXMLUtil(w,MESSAGE_TYPE_TEXT,outMsg);
			w.writeStartElement(null,'Content',null);
			w.writeCData(outMsg.content);
			w.writeEndElement();
		}
		if(msg.MsgType == MESSAGE_TYPE_IMAGE)
		{
			OutImageMsg outImg = (OutImageMsg)msg;
			writeXMLUtil(w,MESSAGE_TYPE_IMAGE,outImg);
			w.writeStartElement(null,'Image',null);
			w.writeStartElement(null,'MediaId',null);
			w.writeCData(outImg.MediaId);
			w.writeEndElement();
			w.writeEndElement();
		}
		if(msg.MsgType == MESSAGE_TYPE_TRANSFER)
		{
			OutTextMsg outMsg = (OutTextMsg)msg;
			this.writeXMLUtil(w,MESSAGE_TYPE_TRANSFER,outMsg);
		}
		if(msg.MsgType == MESSAGE_TYPE_NEWS)
		{
			OutRichMediaMsg outRichMedia = (OutRichMediaMsg)msg;
			this.writeXMLUtil(w, MESSAGE_TYPE_NEWS, outRichMedia);
			w.writeStartElement(null,'ArticleCount',null);//<ArticleCount>
			w.writeCharacters(String.valueOf(outRichMedia.Articles.size()));//count
			w.writeEndElement();//</ArticleCount>
			w.writeStartElement(null,'Articles',null);//<Articles>
			for(Article art : outRichMedia.Articles)
			{
				w.writeStartElement(null,'item',null);//<item>
				w.writeStartElement(null,'Title',null);//<Title>
				w.writeCData((null != art.title ? art.title : ''));//<![CDATA[title1]]>
				w.writeEndElement();//</Title>
				w.writeStartElement(null,'Description',null);//<Description>
				w.writeCData((null != art.description ? art.description : ''));//<![CDATA[description1]]>
				w.writeEndElement();//</Description>
				w.writeStartElement(null,'PicUrl',null);//<PicUrl>
				w.writeCData((null != art.picUrl ? art.picUrl : ''));//<![CDATA[picurl]]>
				w.writeEndElement();//</PicUrl>
				w.writeStartElement(null,'Url',null);//<PicUrl>
				w.writeCData((null != art.url ? art.url : ''));//<![CDATA[url]]>
				w.writeEndElement();//</Url>
				w.writeEndElement();//</item>
			}
			w.writeEndElement();//</Articles>
		}
	    w.writeEndElement();
		String xmlOutput = w.getXmlString();
		system.debug('********xmlOutput**********' + xmlOutput);
		w.close();
		return xmlOutput;
	}
	
	//Deserialize
	public CallinBaseMsg DeserializeCallInRequestMsg(string xml)
	{
		//judge CallinBaseMsg Type
		string msgTypeName = xml.substringBetween('<MsgType><![CDATA[',']]></MsgType>');
		if(msgTypeName == MESSAGE_TYPE_TEXT) 
		{
			InTextMsg inText = new InTextMsg();
			this.GetBaseMsg(inText, xml);
			inText.Content = xml.substringBetween('<Content><![CDATA[',']]></Content>');
			return inText;
		}
		else if(msgTypeName == MESSAGE_TYPE_IMAGE)
		{
			InImageMsg inImage = new InImageMsg();
			this.GetBaseMsg(inImage, xml);
			inImage.PicUrl = xml.substringBetween('<PicUrl><![CDATA[',']]></PicUrl>');
			inImage.MediaId = xml.substringBetween('<MediaId><![CDATA[',']]></MediaId>');
			return inImage;
		}
		else if(msgTypeName == MESSAGE_TYPE_EVENT)
		{
			InEventMsg inEvent = new InEventMsg();
			this.GetBaseMsg(inEvent, xml);
			inEvent.Event = xml.substringBetween('<Event><![CDATA[',']]></Event>');
			inEvent.EventKey = xml.substringBetween('<EventKey><![CDATA[',']]></EventKey>');
			return inEvent;
		}
		else if(msgTypeName == MESSAGE_TYPE_VOICE)
		{
			InVoiceMsg inVoice = new InVoiceMsg();
			this.GetBaseMsg(inVoice,xml);
			inVoice.MediaId = xml.substringBetween('<MediaId><![CDATA[',']]></MediaId>');
			inVoice.Format = xml.substringBetween('<Format><![CDATA[',']]></Format>');
			inVoice.Recognition = xml.substringBetween('<Recognition><![CDATA[',']]></Recognition>');
			return inVoice;
		}
		else return null;
	}
	
	//Serialize(JSON)
	public static string SerializeSvcRequestMsg(SvcBaseMsg s)
	{
		return JSON.serialize(s);
	}
	
	public class User
	{
		public integer subscribe;
		public string openid;
		public string nickname;
		public integer sex;
		public string language;
		public string city; 
		public string province;
		public string country;
		public string headimgurl;
		public long subscribe_time;
		public string unionid;
		public string remark;
	}
	
	//super class of callin message
	public abstract class CallinBaseMsg
	{
	 	public string ToUserName {get; set;}
		public string FromUserName {get; set;}
		public datetime CreateTime {get; set;}
		public string MsgType {get; set;}
		public string MsgId {get; set;}
	}
	
	public class InTextMsg extends CallinBaseMsg
	{
		public string Content {get; set;}
	}
	
	public class OutTextMsg extends CallinBaseMsg
	{
		public string Content {get; set;}
	}
	
	public class InVoiceMsg extends CallinBaseMsg
	{
		public string Recognition {get; set;}
		public string MediaId {get; set;}
		public string Format {get; set;}
	}
	
	public class OutRichMediaMsg extends CallinBaseMsg
	{
		public list<Article> Articles {get; set;}
	}
	
	public static OutTextMsg GenerateOutMsg(CallinBaseMsg baseMsg, String content)
	{
		OutTextMsg outText = new OutTextMsg();
		outText.ToUserName = baseMsg.FromUserName;
		outText.FromUserName = baseMsg.ToUserName;
		outText.CreateTime = System.now();
		outText.MsgType = MESSAGE_TYPE_TEXT;
		outText.MsgId = baseMsg.MsgId;
		outText.Content = content;
		return outText;
	}
	
	public static void GetBaseMsgAtrributes(CallinBaseMsg inMsg,CallinBaseMsg outMsg)
	{
		outMsg.ToUserName = inMsg.FromUserName;
		outMsg.FromUserName = inMsg.ToUserName;
		outMsg.CreateTime = System.now();
	}
	
	public class InImageMsg extends CallinBaseMsg
	{
		public string PicUrl {get; set;}
		public string MediaId {get; set;} 	
	}
	
	public class OutImageMsg extends CallinBaseMsg
	{
		public string PicUrl {get; set;}
		public string MediaId {get; set;} 
	}
	
	public class InEventMsg extends CallinBaseMsg
	{
		public string Event {get; set;}
		public string EventKey {get; set;}
	}
	
	public class OutEventMsg extends CallinBaseMsg
	{
		public string Event {get; set;}
		public string EventKey {get; set;}
	}
	
	//super class of callout
	public abstract class SvcBaseMsg
	{
		public string touser {get; set;}
		public string msgtype {get; set;}
	}
	
	public class SvcTextMsg extends SvcBaseMsg
	{
		public Text text{get; set;}
	}
	
	public class Text
	{
		public string content {get; set;}
	}
	
	public class SvcImageMsg extends SvcBaseMsg
	{
		public Image Image {get; set;}
	}
	
	public class Image
	{
		public string media_id {get; set;}
	}
	
	public class SvcNewsMsg extends SvcBaseMsg
	{
		public News news {get; set;}
	}
	
	public class News
	{
		public list<Article> articles {get; set;}
	}
	
	public class Article
	{
		public string title {get; set;}
		public string description {get; set;}
		public string url {get; set;}
		public string picurl {get; set;}	
	}
	
	public class File
	{
		public string contentType{get;set;}
		public blob body{get;set;}
		public Date uploadTime{get;set;}
		public string description{get;set;}
	}
	//生成调用客服接口发送文本消息的JSON格式字符串
	public static string GenerateSvcTextMsg(string OpenId, string Content)
	{
		WechatEntity.SvcTextMsg outMsg = new WechatEntity.SvcTextMsg();
    	WechatEntity.Text out = new WechatEntity.Text();
		out.content = Content;
		outMsg.touser = OpenId;
        outMsg.text = out;
        outMsg.msgtype = 'text';
        return WechatEntity.SerializeSvcRequestMsg(outMsg);
	}
}