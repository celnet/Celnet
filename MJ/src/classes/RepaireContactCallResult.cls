global class RepaireContactCallResult implements DataBase.Batchable<SObject> 
{
	ID contactRecordTypeId = [select id 
							  from RecordType 
							  where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
	String query;							  
				 
	public RepaireContactCallResult(Integer limitNum)
	{
		query = 'select Id,Last_Routine_Call_Result__c from Contact where RecordTypeId=\'' 
				 + contactRecordTypeId +'\' and (Last_Routine_Call_Result__c =\'Abortion/Closed\' or Last_Routine_Call_Result__c =\'Have Been Born\') limit ' +  String.valueOf(limitNum);
	}
	
	public RepaireContactCallResult(Id conId)
	{
		query = 'select Id,Last_Routine_Call_Result__c from Contact where RecordTypeId=\'' 
				 + contactRecordTypeId +'\' and (Last_Routine_Call_Result__c =\'Abortion/Closed\' or Last_Routine_Call_Result__c =\'Have Been Born\') and Id=\'' + conId + '\'';
	}
	
	public RepaireContactCallResult()
	{
		query = 'select Id,Last_Routine_Call_Result__c from Contact where RecordTypeId=\'' 
				 + contactRecordTypeId +'\' and (Last_Routine_Call_Result__c =\'Abortion/Closed\' or Last_Routine_Call_Result__c =\'Have Been Born\')';
	}
	
	global DataBase.Querylocator start(DataBase.BatchableContext BC)
	{
		return DataBase.getQueryLocator(query);
	}
	
	global void execute(DataBase.BatchableContext BC,List<SObject> scope)
	{
		system.debug('------------------in-------------------');
		List<Contact> list_Contact = new List<Contact>();
		for(SObject s : scope)
		{
			Contact con = (Contact)s;
			if(con.Last_Routine_Call_Result__c == 'Abortion/Closed')
		    {
				con.Last_Routine_Call_Result__c = 'Inactive';        
		    }
		    else if(con.Last_Routine_Call_Result__c == 'Have Been Born')
		    {
				con.Last_Routine_Call_Result__c = 'Connected/Closed';        
		    }
			list_Contact.add(con);
		}
		if(!list_Contact.isEmpty()) update list_Contact;
	}
	
	global void finish(DataBase.BatchableContext BC)
	{}
}