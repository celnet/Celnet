public class BabyConsumptionHelper{

    public static void updateLast2Consumption(Set<ID> idSet)
    {
        List<Contact> babies = 
            [SELECT ID FROM Contact WHERE ID in :idSet];
        
        if(babies != NULL && !babies.isEmpty())
        {
            for(Contact baby : babies)
            {
        		List<Baby_Consumption__c> consumptions =
                    [SELECT ID,Product__r.Brand__c,Product__r.Product_Nature__c,Product__r.Name
                     FROM Baby_Consumption__c 
                     WHERE Contact__c = :baby.ID ORDER BY To_Age__c DESC,From_Age__c DESC Limit 2];
                
                if(consumptions != NULL && !consumptions.isEmpty())
                {
                    //Set "Last Consumption"
                    baby.Last_Consumption_Brand__c = consumptions[0].Product__r.Brand__c;
                    baby.Last_Consumption_Product_Nature__c = consumptions[0].Product__r.Product_Nature__c;
                    baby.Last_Consumption_Product_Name__c = consumptions[0].Product__r.Name;
                    
                    if(consumptions.size() > 1)
                    {
                      //Set "2nd Last Consumption"
                      baby.X2nd_Last_Consumption_Brand__c = consumptions[1].Product__r.Brand__c;
                      baby.X2nd_Last_Consumption_Product_Nature__c = consumptions[1].Product__r.Product_Nature__c;
                      baby.X2nd_Last_Consumption_Product_Name__c = consumptions[1].Product__r.Name;
                    }
                }
            }
            update babies;
        }
    }
}