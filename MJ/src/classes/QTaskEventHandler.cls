/*
 *Author: leo.bi@celnet.com.cn
 *Created On: 2014-05-23
 *Function: Close QTask when a campaignmember's call status is changed to 'Finish Call';
 *Support Event:  AfterInsert, AfterUpdate
 *Apply To: CN
 */
public class QTaskEventHandler implements Triggers.Handler{
    public void Handle()
    {
        if(!Context.ApplyTo(new String[] {'CN'})) 
        {
            return;
        }
        if(trigger.isAfter && (trigger.isUpdate || trigger.isInsert))
        {
            if(trigger.new[0] instanceof CampaignMember)
            {  
                generateQTaskFromInvitation();
            }
            if(trigger.new[0] instanceof Answer__c)
            {
                CloseQtaskAfterAnswer();
            }
            if(trigger.new[0] instanceof Task)
            {
            	try
	            {
	                this.taskCheckCallResult();
	            } catch(Exception e)
	            {
	            	Error_Log__c el = new Error_Log__c();
	            	el.Line_Number__c = e.getLineNumber();
	            	el.Message__c = e.getMessage();
	            	el.Stack_Trace_String__c = e.getStackTraceString();
	            	el.Type_Name__c = e.getTypeName();
	            	el.Error_Time__c = DateTime.now();
	            	el.Handler_Name__c = 'QTaskEventHandler(taskCheckCallResult)';
	            	el.User__c = UserInfo.getUserId();
	            	insert el;
	            }
	            try
	            {
                	this.taskUpdateCallResult();
	            } catch(Exception e)
	            {
	            	Error_Log__c el = new Error_Log__c();
	            	el.Line_Number__c = e.getLineNumber();
	            	el.Message__c = e.getMessage();
	            	el.Stack_Trace_String__c = e.getStackTraceString();
	            	el.Type_Name__c = e.getTypeName();
	            	el.Error_Time__c = DateTime.now();
	            	el.Handler_Name__c = 'QTaskEventHandler(taskUpdateCallResult)';
	            	el.User__c = UserInfo.getUserId();
	            	insert el;
	            }
            }
        }
        if(trigger.isAfter && trigger.isUpdate )
        {
            if(trigger.new[0] instanceof Q_Task__c)
            this.updateTaskFromQTask(); 
        }
    }
    
    /*
    Author: leo.bi@celnet.com.cn
    Created On: 2014-05-12
    Function: Generate and assign Q-Task, Call upsert to trigger possible upgrade flow
    Support Event:  AfterInsert, AfterUpdate
    Apply To: CN
    */
    public void generateQTaskFromInvitation()
    {
        List<CampaignMember> list_New_CampaignMember = trigger.new;
        List<Q_Task__c> list_Upsert_QTak = new List<Q_Task__c>();
        Set<Id> set_CampaignId = new Set<Id>();
        Map<Id,String> map_CampaignType = new Map<Id,String>();
        for(CampaignMember cm : list_New_CampaignMember)
        {
            if(trigger.oldMap != null && ((CampaignMember)trigger.oldMap.get(cm.Id)).Call_Status__c == cm.Call_Status__c)
            {
                continue;
            }
            if(cm.Call_Status__c != 'Waiting Invitation' && cm.Call_Status__c != 'Invitation Special' && cm.Call_Status__c != 'Waiting Reciprocal Call')
            {
                continue;
            }
            set_CampaignId.add(cm.CampaignId);
        }
        if(set_CampaignId.size() == 0)
        {
            return;
        }
        List<Campaign> list_Campaign = [select Id, Campaign_Type__c from Campaign where Id in :set_CampaignId];
        for(Campaign c : list_Campaign)
        {
            if(c.Campaign_Type__c != null)
            {
                map_CampaignType.put(c.Id,c.Campaign_Type__c);
            }
        }
        for(CampaignMember cm : list_New_CampaignMember)
        {
            if(cm.ContactId == null)
            {
                continue;
            }
            if(cm.LeadId != null)
            {
                continue;  
            }  
            if((cm.Call_Status__c == 'Waiting Invitation' || cm.Call_Status__c == 'Invitation Special') && cm.Status == 'Sent')
            {
                Q_Task__c invitation_QTask = new Q_Task__c(); 
                invitation_QTask.Campaign__c = cm.CampaignId;
                invitation_QTask.Campaign_Member__c = cm.Id;
                invitation_QTask.Contact__c = cm.ContactId;
                invitation_QTask.Campaign_Stage__c = 'Invitation';
                invitation_QTask.Type__c = 'Invitation';
                invitation_QTask.Business_Type__c = map_CampaignType.get(cm.CampaignId);
                invitation_QTask.Status__c = 'Open';
                invitation_QTask.ID__c = cm.ContactId + '-' + 'Open';
                list_Upsert_QTak.add(invitation_QTask);
                continue;
            }
            if(cm.Call_Status__c == 'Waiting Reciprocal Call' && cm.Status == 'Attended and waiting for back-visit')
            {
                //CampaignMember is contact, we ignored lead type compainmember
                Q_Task__c invitation_QTask = new Q_Task__c(); 
                invitation_QTask.Campaign__c = cm.CampaignId;
                invitation_QTask.Campaign_Member__c = cm.Id;
                invitation_QTask.Contact__c = cm.ContactId;
                invitation_QTask.Campaign_Stage__c = 'Reciprocal';
                invitation_QTask.Type__c = 'Invitation';
                invitation_QTask.Business_Type__c = 'Invitation Reciprocal';
                invitation_QTask.Status__c = 'Open';
                invitation_QTask.ID__c = cm.ContactId + '-' + 'Open';
                list_Upsert_QTak.add(invitation_QTask);
            }
        }
        if(list_Upsert_QTak.size() != 0)
        {
            upsert list_Upsert_QTak ID__c; 
        }
    }
    
    /*
     *1 Update Last_Routine_Call_Result__c
     *2 Update My_Last_Call_Result__c
    */
    private void taskUpdateCallResult()
    {
        Set<String> Set_Results = new Set<String>{'Not Started','Line Busy'};
        List<Task> list_Task = trigger.new;//Last_Routine_Call_Result__c
        Map<Id,String> map_Contact_Result = new Map<Id,String>();
        //Task关闭对应选项
		final Set<String> Set_CloseOption = new Set<String>{'Inactive','No Answer2','Do not Call'};
        for(Task t : list_Task)
        {
            //t.CallObject != null && 
            //CallObject 为空时进入
            if(t.CallObject != null||t.Status=='Completed')
			continue;
            
            if(t.WhoId == null || t.Result__c == null)
            {
                continue;
            }
            //过滤 'Not Started','Line Busy'
            if(Set_Results.contains(t.Result__c))
            {
                continue;
            }
            Task oldtaans = (trigger.isUpdate?(Task)trigger.oldMap.get(t.Id):new Task());
            
            if(trigger.isInsert || (trigger.isUpdate && t.Result__c != oldtaans.Result__c))
            {
                map_Contact_Result.put(t.WhoId,t.Result__c);
            }
        }
        if(map_Contact_Result.size() == 0)
        return;
         
        List<Q_Task__c> List_UpQtask = new List<Q_Task__c>();
        Id CurrentUserId = UserInfo.getUserId();
        for(Q_Task__c qt : [Select OwnerId,My_Last_Call_Result__c, Last_Routine_Call_Time__c,Last_Routine_Call_Result__c, Id,Contact__c 
        					From Q_Task__c 
        					where Status__c != 'Closed' 
        					and Contact__c in:map_Contact_Result.keySet() limit 49000])
        {
        	qt.Last_Routine_Call_Time__c = datetime.now();
            qt.Last_Routine_Call_Result__c = map_Contact_Result.get(qt.Contact__c);
            if(Set_CloseOption.contains(qt.Last_Routine_Call_Result__c))
            {
            	qt.Status__c = 'Closed';
				qt.ID__c = qt.Contact__c + '-Closed' + String.valueOf(DateTime.now());
            }
            if(qt.OwnerId == CurrentUserId)
            {
                qt.My_Last_Call_Result__c = map_Contact_Result.get(qt.Contact__c);
            }
            List_UpQtask.add(qt);
        }
        
        if(List_UpQtask.size()>0)
        {
        	try
        	{
        		update List_UpQtask;
        	}catch(Exception e)
        	{
        		List<Id> List_QtId = new List<Id>();
	        	List<String> Status = new List<String>();
	        	List<String> qtID = new List<String>();
	        	List<String> My_Last_Call_Result = new List<String>();
	        	List<DateTime> Last_Routine_Call_Time = new List<DateTime>();
	        	List<String> Last_Routine_Call_Result = new List<String>();
	        	
	        	for(Integer i=0;i<List_UpQtask.size();i++)
	        	{
	        		Q_Task__c qta = List_UpQtask[i];
	        		List_QtId.add(qta.Id);
	        		Status.add(qta.Status__c);
	        		qtID.add(qta.ID__c);
	        		My_Last_Call_Result.add(qta.My_Last_Call_Result__c);
	        		Last_Routine_Call_Time.add(qta.Last_Routine_Call_Time__c);
	        		Last_Routine_Call_Result.add(qta.Last_Routine_Call_Result__c);
	        	}
	        	//@future 
				FutureClass.futureTaskUpdateCallResult(List_QtId,Status,qtID,My_Last_Call_Result,Last_Routine_Call_Time,Last_Routine_Call_Result);
        	}
        }
        
    }
    
    /*
    *Q_Task after update Owner ---Update Task
    *When you change the QTask owner, update recently a reservation task
    *Task.Description = '该致电任务已经转移'
    */
    private void updateTaskFromQTask()
    {
        final String Mes = '该致电任务已经转移';
        //Set<String> Set_Close = new Set<String>{'Connected/Closed','Couldn\'t get in touch','Duplicate Call','Wrong Number','Abortion/Closed'};
        List<Q_Task__c> list_QTask = trigger.new;
        Set<Id> Set_ConIds = new Set<Id>();
        for(Q_Task__c qt : list_QTask)
        {
            Q_Task__c oldqt = (Q_Task__c)trigger.oldMap.get(qt.Id); 
            if(qt.OwnerId != oldqt.OwnerId && qt.Contact__c != null)
            {
                Set_ConIds.add(qt.Contact__c);
            }
        }
        
        if(Set_ConIds.size()==0)
        return;
        
        List<Task> List_UpTask = new List<Task>();
        for(Contact con : [select (Select Remark__c, LastModifiedDate From Tasks where Is_Appointment__c=true order by LastModifiedDate desc limit 1) from Contact where Id in:Set_ConIds])
        {
            if(con.Tasks.size()>0)
            {
                for(Task ta : con.Tasks)
                {
                    ta.Remark__c = Mes; 
                    List_UpTask.add(ta);
                }
            }
        }
        
        if(List_UpTask.size()>0)
        update List_UpTask;  
    }
    
    
    private void taskCheckCallResult()
    {
        /*Author:leo.bi@celent.com.cn
         *CreateDate:2014-05-26
         *Function:If one phone number was called 2times "No Response" or two times "Phone Closed",
         *         set the related Q_Task NextCallDate as tomorrow.
         *         This month, if one number was called 6 times "Phone Closed",call next month
         *         If 3 times "No Response"， close qtask
         */
        /*针对一次通话，通话开始时CIC会创建一条CallObject不为空的记录，通话结束后如果手工保存了记录会创建一条CallObeject为空的记录
         *对于一次通话应该只统计一条Task，所以只统计CIC创建的Task,因为手工创建的记录会在befroe insert 和 before update的时候同步到CIC创建的数据上
         *触发trigger的Task应该是手工创建的Task，其它的Task不应进入逻辑
         */
        List<Task> list_Task_New = trigger.new;
        Set<String> Set_Result = new Set<String>{'No Response','Phone Closed'};
        Set<Id> set_Contact_Id = new Set<Id>();
        List<Q_Task__c> list_QTask_NR_Two = new List<Q_Task__c>();
        List<Q_Task__c> list_QTask_NR_Three = new List<Q_Task__c>();
        List<Q_Task__c> list_QTask_PC_Two = new List<Q_Task__c>();
        List<Q_Task__c> list_QTask_PC_Six = new List<Q_Task__c>();
        //Id--Contact Id, String--Phone Number, Integer--call times in a day
        Map<Id,Map<String,Integer>> map_Contact_NR_Two = new Map<Id,Map<String,Integer>>();
        Map<Id,Map<String,Integer>> map_Contact_NR_Three = new Map<Id,Map<String,Integer>>();
        Map<Id,Map<String,Integer>> map_Contact_PC_Two = new Map<Id,Map<String,Integer>>();
        Map<Id,Map<String,Integer>> map_Contact_PC_Six = new Map<Id,Map<String,Integer>>();
        Date yesterdayDate = Date.today().addDays(-1);
        Date tomorrowDate = Date.today().addDays(1);
        DateTime thisMonthBegin = Date.today().toStartOfMonth();
        DateTime thisMonthEnd = Date.today().toStartOfMonth().addMonths(1).addDays(-1);
        for(Task t : list_Task_New)
        {
            //CIC创建的数据CallObject不为空，whoId为空，根据这两个特点，过滤CIC的Task,不允许触发Trigger
            if((t.CallObject != null && t.CallObject !='') || t.WhoId == null || t.Result__c ==null || t.Result__c == '' || (!Set_Result.contains(t.Result__c)))
            continue;
            Task oldtaans = (trigger.isUpdate?(Task)trigger.oldMap.get(t.Id):new Task());
            if(trigger.isInsert || (trigger.isUpdate && t.Result__c != oldtaans.Result__c)) 
            {
	            set_Contact_Id.add(t.WhoId);    
            }
        }
        if(set_Contact_Id.isEmpty())
        {
            return;  
        }
        //check today's task.
        // CallObject = null 即 手工创建的数据不统计，不在SOQL中过滤，在逻辑中过滤
        List<Task> list_Task_Today =[Select WhoId, Result__c, CallObject, CreatedDate,Caller_Phone_Number__c
                                     From Task 
                                     where WhoId in :set_Contact_Id 
                                     and Result__c in :Set_Result
                                     and CreatedDate > :yesterdayDate and CreatedDate < :tomorrowDate];
        if(list_Task_Today.size() != 0)
        {
            this.packageCallTimes(list_Task_Today,map_Contact_NR_Two,map_Contact_PC_Two);
        }
        if(map_Contact_NR_Two.size() != 0)
        {
            list_QTask_NR_Two = this.checkCallTimes(map_Contact_NR_Two, 1, false);
        }
        if(map_Contact_PC_Two.size() != 0)
        {
            list_QTask_PC_Two = this.checkCallTimes(map_Contact_PC_Two, 2, false);
        }
        if(list_QTask_NR_Two.size() != 0)
        {
            update list_QTask_NR_Two;
        }
        if(list_QTask_PC_Two.size() != 0)
        {
            update list_QTask_PC_Two;
        }
        //check this month
        //CallObject = null 即 手工创建的数据不统计，不在SOQL中过滤，在逻辑中过滤
        List<Task> list_Task_Month =[Select WhoId, Result__c, CallObject, CreatedDate,Caller_Phone_Number__c
                                     From Task 
                                     where WhoId in :set_Contact_Id 
                                     and Result__c in :Set_Result
                                     and CreatedDate > :thisMonthBegin and CreatedDate < :thisMonthEnd];
        if(list_Task_Month.size() != 0)
        {
            this.packageCallTimes(list_Task_Month,map_Contact_NR_Three,map_Contact_PC_Six);
        }
        if(map_Contact_NR_Three.size() != 0)
        {
            list_QTask_NR_Three = this.checkCallTimes(map_Contact_NR_Three, 5, true); 
        }
        if(map_Contact_PC_Six.size() != 0)
        {
            list_QTask_PC_Six = this.checkCallTimes(map_Contact_PC_Six, 6, false);
        }
        if(list_QTask_NR_Three.size() != 0)
        {
            update list_QTask_NR_Three;
        }
        if(list_QTask_PC_Six.size() != 0)
        {
            update list_QTask_PC_Six;
        }
    }
    
    private void packageCallTimes(List<Task> list_Task_All,Map<Id,Map<String,Integer>> map_Contact_NR,Map<Id,Map<String,Integer>> map_Contact_PC)
    {
        for(Task t : list_Task_All)
        {
            //如果CallObject为空，则证明，是手工创建的数据，不统计
            if(t.CallObject == null || t.CallObject == '' || t.Caller_Phone_Number__c == null)
            {
                continue;
            }
            Map<String,Integer> temp_Map = new Map<String,Integer>();
            Integer temp_Times;
            if(t.Result__c == 'No Response')
            {
                if(map_Contact_NR.containsKey(t.WhoId))
                {
                    temp_Map = map_Contact_NR.get(t.WhoId);
                    if(temp_Map.containsKey(t.Caller_Phone_Number__c))
                    {
                        temp_Map.put(t.Caller_Phone_Number__c,temp_Map.get(t.Caller_Phone_Number__c) + 1);
                    }
                    else
                    {
                        temp_Map.put(t.Caller_Phone_Number__c,1);
                    }
                }
                else
                {
                    temp_Map.put(t.Caller_Phone_Number__c,1);
                    map_Contact_NR.put(t.WhoId,temp_Map);
                }
            }
            if(t.Result__c == 'Phone Closed')
            {
                if(map_Contact_PC.containsKey(t.WhoId))
                {
                    temp_Map = map_Contact_PC.get(t.WhoId);
                    if(temp_Map.containsKey(t.Caller_Phone_Number__c))
                    {
                        temp_Map.put(t.Caller_Phone_Number__c,temp_Map.get(t.Caller_Phone_Number__c) + 1);
                    }
                    else
                    {
                        temp_Map.put(t.Caller_Phone_Number__c,1);
                    }
                }
                else
                {
                    temp_Map.put(t.Caller_Phone_Number__c,1);
                    map_Contact_PC.put(t.WhoId,temp_Map);
                }
            }
        }
    }
    
    private List<Q_Task__c> checkCallTimes(Map<Id,Map<String,Integer>> map_Contact,Integer limitCallTimes,Boolean isNoResponse)
    {
        List<Q_Task__c> list_QTask_Temp = new List<Q_Task__c>();
        for(Q_Task__c qt : [Select Status__c, Next_Call_Date__c, Id, Contact__c, ID__c From Q_Task__c 
                            where Contact__c in :map_Contact.keySet()
                            and Status__c != 'Closed'])
        {
            Map<String,Integer> map_temp = map_Contact.get(qt.Contact__c);
            Boolean isLimit = false;
            for(String phone : map_temp.keyset())
            {
                if(map_temp.get(phone) >= limitCallTimes)
                {
                    isLimit = true;
                    break;
                }
            }
            if(isLimit)
            {
                if(limitCallTimes == 5 && isNoResponse)
                {
                    qt.Status__c = 'Closed';
                    qt.ID__c = qt.Contact__c + '-'+qt.Status__c+String.valueOf(DateTime.now());
                }
                else if(limitCallTimes == 6)
                {
                    qt.Next_Call_Date__c = Date.today().toStartOfMonth().addMonths(1);
                }
                else
                {
                    qt.Next_Call_Date__c = Date.today().addDays(1);
                }
                list_QTask_Temp.add(qt);
            }
        }
        return list_QTask_Temp;
    }
    
    /*
     * Close Q_Task
     *Routine: 关闭QTask
     *Invitation: 更新对应CampaignMember Call status='Finish call';判断是否所有Invitation是否已经完成，如果完成关闭QTask，否则标记Has_Remaining_Call__c
     *Elite:验证 Routine+Invitation
    */
    
    public void CloseQtaskAfterAnswer()
    {
        List<Answer__c> list_AnswerNew = trigger.new;
        Set<Id> Set_ContactIds = new Set<Id>();
        Set<Id> Set_CampaignMemberIds = new Set<Id>();
        for(Answer__c ans : list_AnswerNew)
        {
            if(ans.Contact__c == null || ans.Status__c != 'Finish' )
            continue;
            
            Answer__c oldans = (trigger.isUpdate?(Answer__c)trigger.oldMap.get(ans.Id):new Answer__c());
            if(trigger.isInsert || (trigger.isUpdate && ans.Status__c != oldans.Status__c))
            {
                if(ans.Campaign_Member_Id__c != null)
                Set_CampaignMemberIds.add(ans.Campaign_Member_Id__c);
                
                Set_ContactIds.add(ans.Contact__c);
            }
        }
        
        //CampaignMember : Finish call
        List<CampaignMember> List_UpMember = new List<CampaignMember>();
        if(Set_CampaignMemberIds.size()>0)
        {
            for(CampaignMember cm : [select Id,Call_Status__c,Is_Finished_By_Answer__c from CampaignMember where id in:Set_CampaignMemberIds])
            {
                cm.Call_Status__c = 'Finish Call';
                cm.Is_Finished_By_Answer__c = true;
                List_UpMember.add(cm);
            }
        }
        
        
        //Q_Task : close QTask
        List<Q_Task__c> List_UpQtask = new List<Q_Task__c>();   
        if(Set_ContactIds.size()>0)
        {
            //Qtask
            Map<Id,Q_Task__c> Map_Qt = new Map<Id,Q_Task__c>();
            for(Q_Task__c qt: [select Has_Remaining_Call__c,Type__c,Campaign__c,Campaign_Member__c,Status__c,Contact__c,ID__c from Q_Task__c where Contact__c in :Set_ContactIds and Status__c != 'Closed'])
            {
                Map_Qt.put(qt.Contact__c ,qt);
            }
            
            if(Map_Qt.size()==0)
            return;
            
            
            for(Contact con: [Select Id,Questionnaire__c,Questionnaire__r.Type__c,Questionnaire__r.Business_Type__c,
                             (Select Contact__c,Questionnaire__c,Campaign_Member_Id__c,Status__c From Answers__r where Status__c =:'Finish'), 
                             (Select Id,Call_Status__c,CampaignId,Is_Finished_By_Answer__c From CampaignMembers where Call_Status__c !='Finish Call' and Id not in: Set_CampaignMemberIds) 
                             From Contact where Id in:Map_Qt.keySet()])
            {
                Q_Task__c qt = (Map_Qt.containsKey(con.Id)?Map_Qt.get(con.Id):new Q_Task__c()); 
                //判断Routine是否完成
                Boolean Routineflag = false;
                if(con.Questionnaire__c==null)
                Routineflag = true;  
                
                if(con.Questionnaire__c!=null && con.Answers__r.size()>0)
                {
                    for(Answer__c ans : con.Answers__r)
                    {
                        if(ans.Questionnaire__c == con.Questionnaire__c)
                        {
                            Routineflag = true;
                            break;
                        }
                    }
                }
                
                //判断Invitation是否完成
                Boolean InvitationFlag = false;
                
                if(con.CampaignMembers.size()>0)
                {
                    qt.Has_Remaining_Call__c = true;
                    List_UpQtask.add(qt);
                    InvitationFlag = false;
                }
                else
                {
                    InvitationFlag = true;
                }
                //Close Qtask
                if(Routineflag && InvitationFlag)
                {
                    qt.Status__c = 'Closed';
                    qt.ID__c = qt.Contact__c + '-'+qt.Status__c+String.valueOf(DateTime.now());
                    List_UpQtask.add(qt);
                }
            }
        }
        if(List_UpMember.size()>0)
        update List_UpMember;
    
        if(List_UpQtask.size()>0)
        update List_UpQtask;
    }
}