@isTest
private class RegularCampaignSettingExtensionTest {

    static RegularCampaignSettingExtension setupControllerExtension(){
        Campaign_Scheduler__c obj = new Campaign_Scheduler__c();
        ApexPages.StandardController stdCont = new  ApexPages.StandardController(obj);
        
        return  new RegularCampaignSettingExtension(stdCont); 
    }

    static testMethod void contructorTest() {
        //Set up 
         Campaign_Scheduler__c obj = new Campaign_Scheduler__c();
        ApexPages.StandardController stdCont = new  ApexPages.StandardController(obj);
        
        RegularCampaignSettingExtension ext =  new RegularCampaignSettingExtension(stdCont); 
        
        //assert extension successfully instansiated 
        
        system.assert(ext != null);  
    }
    

    
    static testMethod void onDataTypeChangeTest() {
        //Set up 
         Campaign_Scheduler__c obj = new Campaign_Scheduler__c();
         
        ApexPages.StandardController stdCont = new  ApexPages.StandardController(obj);
        
        RegularCampaignSettingExtension ext =  new RegularCampaignSettingExtension(stdCont); 
        
        //test case: Contacts
        obj.Data_Type__c = 'Contacts';
        ext.onDataTypeChange();
        
        /* Following breaks when list view name doesn't match due to locale 
        //assert
        system.assert(selectOptionContains(ext.listViewOptions,'Recently Viewed Contacts'));
        //test case: Leads
        obj.Data_Type__c = 'Leads';
        ext.onDataTypeChange();
        //assert
        system.assert(selectOptionContains(ext.listViewOptions,'Recently Viewed Leads'));
        //test case: null
        obj.Data_Type__c = null;
        ext.onDataTypeChange();
        //assert
        system.assertEquals(ext.listViewOptions[0], RegularCampaignSettingExtension.DEFAULT_OPTION);
        */
    }
    
    static boolean selectOptionContains(SelectOption[] options, String label){
        for (SelectOption opt : options){
            if (opt.getLabel()==label)
                return true;
        }
        return false;
    }
    
    //test method success save
    static testmethod void save_success(){
        //Set up page 
       PageReference pageRef = Page.RegularCampaignSetting;
       Test.setCurrentPage(pageRef);
       
        RegularCampaignSettingExtension ext = setupControllerExtension();
        
        //set up test records
        Campaign camp = new Campaign(name='Test Campaign', isActive=true);
        
        insert camp;
        //data entry
        ext.campaignScheduler.Data_Type__c = 'Lead';
        ext.campaignScheduler.List_view_Name__c = 'Recently Viewed Leads';
        ext.campaignScheduler.Master_Campaign__c = camp.id;
        
        //assert
       Campaign_Scheduler__c[] cs = [SELECT id FROM Campaign_Scheduler__c LIMIT 1]; 
       system.assert(cs.size()==0);
       
       ext.save();
       
       cs = [SELECT id FROM Campaign_Scheduler__c LIMIT 1]; 
       system.assert(cs.size()==1);
       system.assert(ApexPages.hasMessages() == false);
        
    }
    
    //test method fail save (validation rules, etc)
    static testmethod void save_fail(){
        
        //Set up page 
        
       PageReference pageRef = Page.RegularCampaignSetting;
       Test.setCurrentPage(pageRef);
       
        RegularCampaignSettingExtension ext = setupControllerExtension();
        //set up test records
        Campaign camp = new Campaign(name='Test Campaign', isActive=true);
        
        insert camp;
        //data entry
        ext.campaignScheduler.Data_Type__c = 'Lead';
        ext.save();
        
        system.assert(ApexPages.hasMessages());
        //system.assertEquals(ApexPages.getMessages()[0].getSummary(),'Required fields are missing: [Master_Campaign__c]');
   
        
    }
    
    
}