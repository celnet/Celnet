/*Author:leo.bi@celnet.com.cn
 *Date:2014-07-23
 *Function:Delete IntegrationLog whoes Delete_Flag__c is true
 */
global class IntegrationLogDeletionBatch implements DataBase.Batchable<SObject>,Schedulable 
{
	private final String query = 'select Id,CallObject,CreatedBy.LastName from Task where Delete_Flag__c = true'
								+' and CreatedDate = Today and CallObject=null';
	global void execute(SchedulableContext sc)
	{
		IntegrationLogDeletionBatch ildb = new IntegrationLogDeletionBatch();
		DataBase.executebatch(ildb);
	}
	
	global DataBase.Querylocator start (DataBase.BatchableContext BC)
	{
		return DataBase.getQueryLocator(query);
	}
	
	global void execute(DataBase.BatchableContext BC, List<SObject> scope)
	{
		delete scope;
	}
	
	global void finish(DataBase.BatchableContext BC)
	{
		system.debug('-------------------finish-----------------------------------');	
	}
}