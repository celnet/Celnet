/*Author: leo.bi@celnet.com.cn
 *Created On: 2014-05-06
 *Function: the test class of AnswerStatusSyncHandler
 *Apply To: CN
 */
@isTest
private class AnswerStatusSyncHandlerTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        //Create custom settings
        system.runAs(TestUtility.new_CN_User())
        {
        	Member_Entry__c me = new Member_Entry__c();
        	me.Name = 'Me1';
        	insert me;
	        //new Account
			Account acc = new Account();
			acc.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_HouseHold' and SobjectType=:'Account'].id;
			acc.Name = 'HouseHold123456';
			acc.Last_Name__c ='HouseHold123456';
			acc.Member_Entry__c = me.Id;
			insert acc;
	        //new Contact
	        Contact con = new Contact();
			con.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
			con.AccountId = acc.Id;
			con.LastName = 'conLast123456';
			con.Birthdate = Date.today().addDays(-5);
			con.Register_Date__c = Date.today().addDays(-4);
			con.Primary_RecruitChannel_Type__c = 'Hospital';
			con.Primary_RecruitChannel_Sub_type__c = 'PCBaby';
			insert con;
	        //new Answer1
	        Answer__c ans1 = new Answer__c();
	        ans1.Q008__c = '圣元M';
			ans1.Q011__c = '哭闹';
			ans1.Q030__c = 'Y';
			ans1.Q045__c = 'Y';
			ans1.Q046__c = '皮肤问题';
			ans1.Status__c = 'Finish';
			ans1.Finished_On__c = Datetime.now().addDays(-5);
			ans1.Contact__c = con.Id;
			insert ans1;
	        //new Answer2
	        Answer__c ans2 = new Answer__c();
	        ans2.Q008__c = '圣元M';
			ans2.Q011__c = '哭闹';
			ans2.Q030__c = 'Y';
			ans2.Q045__c = 'Y';
			ans2.Q046__c = '皮肤问题';
			ans2.Q032__c = 'N';
			ans2.Q036__c = 'N';
			ans2.Q037__c = 'N';
			ans2.Q056__c = 'Y';
			ans2.Q017__c = 'MJN';
			ans2.Status__c = 'Finish';
			ans2.Finished_On__c = Datetime.now().addDays(-4);
			ans2.Contact__c = con.Id;
			insert ans2;
			//new Answer2
	        Answer__c ans3 = new Answer__c();
	        ans3.Q008__c = '圣元M';
			ans3.Q011__c = '哭闹';
			ans3.Q030__c = 'Y';
			ans3.Q045__c = 'Y';
			ans3.Q046__c = '皮肤问题';
			ans3.Q032__c = 'Y';
			ans3.Q036__c = 'Y';
			ans3.Q037__c = 'Y';
			ans3.Q056__c = 'Y';
			ans3.Q014__c = 'MJN';
			ans3.Q024__c = 'MJN';
			ans3.Status__c = 'Finish';
			ans3.Finished_On__c = Datetime.now().addDays(-3);
			ans3.Contact__c = con.Id;
			insert ans3;
			me = [Select Q_Wish_Using_Baby_Milk_Powder__c, Q_Using_Mother_Milk_Powder__c, Id 
			      From Member_Entry__c where Id = :me.Id];
			system.assertEquals('MJN', me.Q_Wish_Using_Baby_Milk_Powder__c);
			system.assertEquals('MJN', me.Q_Using_Mother_Milk_Powder__c);
			Task t444 = new Task();
			t444.WhoId = con.Id;
			t444.Result__c = 'No Answer2';
			t444.Delete_Flag__c = true;
			t444.Caller_Phone_Number__c ='11511112222';
			t444.Status = 'Closed';
			insert t444;
			IntegrationLogDeletionBatch idb = new IntegrationLogDeletionBatch();
			DataBase.executeBatch(idb);
			DateTime nowTime = DateTime.now().addSeconds(2);
			String sch = String.valueOf(nowTime.second()) + ' ' + String.valueOf(nowTime.minute()) + ' '
						  + String.valueOf(nowTime.hour()) + ' ' + String.valueOf(nowTime.day()) + ' '
						  + String.valueOf(nowTime.month()) + ' ? ' + String.valueOf(nowTime.year());
			system.schedule('test1234' + String.valueOf(DateTime.now()), sch, idb);
			
        }
    }
    
    static testMethod void testQtaskDeletionBatch()
	{	
		system.runas(TestUtility.new_CN_User())
		{
			Q_Task__c qt = new Q_Task__c();
			qt.Status__c = 'Open';
			qt.ID__c = 'Leotest' + '-Open';
			insert qt;
			qt.Status__c = 'Closed';
			update qt;
			RoutineQTaskDeletionBatch r = new RoutineQTaskDeletionBatch();
			DataBase.executeBatch(r);
			DateTime nowTime = DateTime.now().addSeconds(2);
			String sch = String.valueOf(nowTime.second()) + ' ' + String.valueOf(nowTime.minute()) + ' '
						  + String.valueOf(nowTime.hour()) + ' ' + String.valueOf(nowTime.day()) + ' '
						  + String.valueOf(nowTime.month()) + ' ? ' + String.valueOf(nowTime.year());
			system.schedule('test1234' + String.valueOf(DateTime.now()), sch, r);
		}
	}
}