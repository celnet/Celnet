/*
*Author:michael.mi@celnet.com.cn
*Date:20141011
*Function:Download voice message from wechat server
*/
public class WechatCalloutGetVoiceProcessor extends WechatCalloutProcessor
{
	list<Wechat_Callout_Task_Queue__c> sendVoiceQueue = new list<Wechat_Callout_Task_Queue__c>();
    public override void DoCallout(Wechat_Callout_Task_Queue__c WechatTask)
    {
        WechatCalloutService wcs = new WechatCalloutService(WechatTask.Public_Account_Name__c);
        WechatEntity.File file = wcs.DowloadMedia(WechatTask.Media_ID__c);
        system.debug('**************************' + wcs);
        Attachment att = WechatFieldMatch.GenerateAttachment(file);
        List<Wechat_Message__c> wmList = [select id ,Name,Public_Account_Name__c,From_User_ID__c from Wechat_Message__c where Media_Id__c = :WechatTask.Media_ID__c];
        if(wmList.isempty() || wmList.size() > 1)
        {
            return;
        }
        att.Name =  wmList[0].Name + DateTime.now().format('yyyy-MM-dd HH:mm:ss', 'Asia/Shanghai');
        att.ParentId = wmList[0].id;
        ObjectList.add(att);
        //string sendBody = EncodingUtil.base64encode(att.body).replace('+','#'); 
		Wechat_Callout_Task_Queue__c voiceQueue = new Wechat_Callout_Task_Queue__c();
		voiceQueue.Name = 'WechatCalloutSendVoiceProcessor';
		voiceQueue.Public_Account_Name__c = wmList[0].Public_Account_Name__c;
		//voiceQueue.Msg_Body__c = sendBody;
		voiceQueue.Open_ID__c = wmList[0].From_User_ID__c;
		voiceQueue.Media_ID__c = wmList[0].id;
		voiceQueue.Processor_Name__c = 'WechatCalloutSendVoiceProcessor';
		voiceQueue.Type__c = 'SendVoice';
		sendVoiceQueue.add(voiceQueue);
    }
    
    public override void FinishData()
    {
        if(this.ObjectList != null && ObjectList.size() >0)
        {
            insert ObjectList;  
        }
        if(!sendVoiceQueue.isempty())
        {
        	insert sendVoiceQueue;
        }
    }
}