public class StagingReportUtil{
    public static Map<String, Schema.SObjectField> TS_objectFields = Schema.getGlobalDescribe().get('TS_Workload_Stats__c').getDescribe().fields.getMap();
    public static Map<String, Schema.SObjectField> TS_GC_objectFields = Schema.getGlobalDescribe().get('TS_GC_Workload_Stats__c').getDescribe().fields.getMap();
    public static Map<String, Schema.SObjectField> Welcome_objectFields = Schema.getGlobalDescribe().get('Welcome_Call_Staging__c').getDescribe().fields.getMap();
    public static Map<String, Schema.SObjectField> Recruitment_objectFields = Schema.getGlobalDescribe().get('Recruitment_Staging__c').getDescribe().fields.getMap();
    
    public static Set<String> Excluded_Reset_Fields = new Set<String>{			//Only Double types are reset to 0, specify excluded fields here
        'reporting_year__c','reporting_month__c','reporting_day__c','questionnaire_no__c'}; //Must be lowercase
    
    /*****	Common Methods *****/
    public static Map<Id,User> getUserMap(List<SObject> scope){
        Map<Id,User> userMap = new Map<Id,User>();
        for (SObject obj: scope){
            User u = (User)obj;
            userMap.put(u.Id, u);
        }
        return userMap;
    }
     /*Helper method for survey questions*/
    public static void setSurveyValues(
        SObject row,			//row to be updated
        String response,		//value for comparison
        boolean useContains,	//String.contains or for multi-picklist
        String[] options,		//list of options to compare to
        String[] fieldNames,	//corresponding field names to increment
        String totalField		//total field to be updated
        )
    {
        if (response != null){
            for (Integer index=0; index < options.size(); index++){
                if (!useContains && response == options[index]
                   || (useContains && response.contains(options[index])))
                {
                    if (index < fieldNames.size()){
                        if (row.get(fieldNames[index]) == null){
                            throw new CommonHelper.MJNCustomException('Field Name not found:'+fieldNames[index]);
                        }
                        Decimal count = (Decimal) row.get(fieldNames[index]);
                        if (count == null){
                            count = 0;
                        }
                    	row.put(fieldNames[index], ++count);
                    }
                    if (totalField != null){
                    	Decimal total = (Decimal) row.get(totalField);
                        if (totalField == null){
                            throw new CommonHelper.MJNCustomException('Field Name not found:'+totalField);
                        }
                        row.put(totalField, ++total);
                    }
                }
            }
        }    
    }
    /*Overloaded method*/
    public static void setSurveyValues(
        SObject row,
        String response,
        boolean useContains,
        String[] options,
        String[] fieldNames
        )
    {
    	setSurveyValues(row,response,useContains,options,fieldNames,null);       
    }
    
    /*Get Age in Days*/
    public static Integer getAgeInDays(DateTime today, DateTime birthday){
        Date todayDate = Date.newInstance(today.year(), today.month(), today.day());
        Date birthdayDate = Date.newInstance(birthday.year(), birthday.month(), birthday.day());
        return birthdayDate.daysBetween(todayDate);
    }
    /*Get Task by User and Date*/
    public static Map<Id,List<Task>> queryCurrentDayTasksByUser(Set<Id> userIds, Integer year, Integer month, Integer day){
        
        Date reportingDate = Date.newInstance(year, month, day);
        Map<Id,List<Task>> tasksMap = new Map<Id,List<Task>>();
        
        //Query and populate map
        for (Task task : [Select id , WhoId, Status, Result__c, OwnerId,
                            CreatedDate
                            from Task 
                            where Who.Type = 'Contact'
                            and OwnerId in :userIds 
                            and CIC_Valid_Data__c = true
                            and ActivityDate = :reportingDate
                         ])
        {
            List<Task> tasks = tasksMap.get(task.OwnerId);
            if (tasks == null){
                tasks = new List<Task>();
                tasksMap.put(task.OwnerId,tasks);
            }
            tasks.add(task);
        }
        return tasksMap;
    }
    
     /*Get Task by User and Date without CallObject!=null and use created Date instead of activiy date*/
    public static Map<Id,List<Task>> queryCurrentDayTasksByUser2(Set<Id> userIds, Integer year, Integer month, Integer day){
        
        Date reportingDate = Date.newInstance(year, month, day);
        Map<Id,List<Task>> tasksMap = new Map<Id,List<Task>>();
        
        //Query and populate map
        for (Task task : [Select id , WhoId, Status, Result__c, OwnerId,
                            CreatedDate
                            from Task 
                            where Who.Type = 'Contact'
                            and OwnerId in :userIds 
                            //and CallObject <> null
                            and DAY_ONLY(convertTimezone(CreatedDate))  = :reportingDate
                         ])
        {
            List<Task> tasks = tasksMap.get(task.OwnerId);
            if (tasks == null){
                tasks = new List<Task>();
                tasksMap.put(task.OwnerId,tasks);
            }
            tasks.add(task);
        }
        return tasksMap;
    }
    
	public static Map<Id,Contact> queryContactsFromTasks(List<Task> tasks, boolean filterSuccessOnly){
    	Set<Id> contactIds = new Set<Id>();
        for (Task task : tasks){
            if (!filterSuccessOnly || (filterSuccessOnly && task.Result__c == 'Connected/Closed')){
                contactIds.add(task.WhoId);
            }
        }
        return new Map<Id,Contact>([Select id ,
                                    Questionnaire__c, Questionnaire__r.Questionnaire_No__c,
                                    Account.Standard_City__r.Name,
                           			Account.Standard_City__r.Sales_City__c,
                                    BirthDate, Birthday__c
                            		from Contact 
                                  	where id in :contactIds
                          		 ]);
    }
    
    /*GC问卷调整
     *Scott调整：在原有逻辑基础上，从上述联系人中筛选出进行过GC问卷的
     *筛选条件：此联系人参加记录类型为‘CN_Invitation_Special’的Campaign  and 对应CampaignMember上的 Last_Call_Time__c为当前逻辑统计时间
    */
    public static Map<Id,Contact> queryContactsFromTasks2(List<Task> tasks, boolean filterSuccessOnly,Date reportDate,Integer QuestionnaireNo){
    	Set<Id> contactIds = new Set<Id>();
        for (Task task : tasks){
            if (!filterSuccessOnly || (filterSuccessOnly && task.Result__c == 'Connected/Closed')){
                contactIds.add(task.WhoId);
            }
        }
        //过滤目标联系人
        Set<Id> Set_ContactId = new Set<Id>();
        for(CampaignMember cm : [select Campaign.GC_Call_Type__c,ContactId from CampaignMember where ContactId in:contactIds 
        						and Campaign.RecordType.DeveloperName='CN_Invitation_Special' 
        						and Last_Call_Time__c!=null 
        						and DAY_ONLY(convertTimezone(Last_Call_Time__c))=:reportDate
        						])
        {
        	if(cm.ContactId == null)
        	continue;
        	
        	
        	if(QuestionnaireNo==13 && cm.Campaign.GC_Call_Type__c == 'GC初访')
        	{
        		Set_ContactId.add(cm.ContactId); 
        	}
        	else if(QuestionnaireNo==14 && cm.Campaign.GC_Call_Type__c == 'GC二次回访')
        	{
        		Set_ContactId.add(cm.ContactId); 
        	}
        }
        return new Map<Id,Contact>([Select id ,
                                    Questionnaire__c, Questionnaire__r.Questionnaire_No__c,
                           			Account.Standard_City__r.Sales_City__c,
                                    BirthDate, Birthday__c
                            		from Contact 
                                  	where id in :Set_ContactId
                          		 ]);
    }
    
    /*Generic method to take in parameters to join them into String External ID*/
    public static String generateExternalID(Object[] objects){
        List<String> stringArray = new List<String>();
        for (Object obj : objects){
            stringArray.add(String.valueOf(obj));
        }
        return String.join(stringArray, '_').toUpperCase();
    }
    /*Generic Reset Methods to reset non-key reporting fields to zeros */
    public static void resetRow(SObject row, Map<String, Schema.SObjectField> objectFields){
        for (String fieldName : objectFields.keySet() ){
            Schema.SObjectField field = objectFields.get(fieldName);
            
            //Skip Formula Fields or Excluded Fields
            if (field.getDescribe().isCalculated() || Excluded_Reset_Fields.contains(fieldName)){
                continue;
            }
            //Only Reset Double Fields to Zero
            if (field.getDescribe().getType() == Schema.DisplayType.DOUBLE){
                row.put(fieldName , 0 );
            }
        }
    }
    
    /*****************		TS WOrkload Specific		********************/

	/*Helper method to search for existing TS record, if not found then create new*/
 
    public static TS_Workload_Stats__c getTSWorkloadStats(
        List<TS_Workload_Stats__c> rows,
        Integer year,
        Integer month,
        Integer day, 
        User currentUser,
        String city)
    {
        //try search for existing record
        if (rows != null && rows.isEmpty() == false){
            for (TS_Workload_Stats__c row : rows){
                if ( row.Reporting_Day__c == day
                   && row.Reporting_Month__c == month
                   && row.Reporting_Year__c == year
                   && row.User__c == currentUser.Id
                   && row.Called_To_City__c == city){
                    return row;
                }
            }
        }
        //if not create new
        TS_Workload_Stats__c row = new TS_Workload_Stats__c();
        row.Reporting_Day__c = day;
        row.Reporting_Month__c = month;
        row.Reporting_Year__c = year;
        row.User__c = currentUser.Id;
        row.TS_City__c = currentUser.Division;
        row.TS_Department__c = currentUser.Department;
        row.TS_Employee_No__c = currentUser.EmployeeNumber;
        row.TS_Full_Name__c = currentUser.Name;
        row.Called_To_City__c = city;
        row.External_ID__c = StagingReportUtil.generateExternalID(
            new Object[]{year,month,day,currentUser.Id,city}
        );
        
        resetRowTS(row);
        
      
        rows.add(row);
        return row;
    }

    
    private static void resetRowTS(TS_Workload_Stats__c row){
    	resetRow(row,TS_objectFields);
    }
    
    /* Query all Answers (past and present) by Contacts*/
    public static Map<Id,List<Answer__c>> queryAnswersByContacts(Set<Id> contactIDs){
        Map<Id,List<Answer__c>> contactAnswers = new Map<Id,List<Answer__c>>();
        for (Answer__c answer : [Select id, 
                                 CreatedDate, Finished_On__c, 
                                 Questionnaire__c,
                                 Questionnaire__r.Questionnaire_No__c,
                                 Contact__r.Questionnaire__c, 
                                 //For Table 1.1
                                 Questionnaire__r.Type__c,
                                 Q008_Last__c, Q038__c, Q039__c , Q005__c,Q033__c, Q034__c ,
                                 //For Table 1.2
                                 Q054__c,Q056__c,Q056_GC_last__c,
                                 Q055__c,Q057__c,
                                 Q059__c,Q064__c,Q065__c,Q058__c,
                                 Q061__c,Q062__c,Q063__c,
                                 Q101__c,Q060__c,
                                 //Table 2.1
                                 Q001__c,Q028__c,Q002__c,Q004__c,Q017__c,Q023__c,Q008__c,Q026__c
                                 //Table 2.2
                                 ,Q024__c 
                                From Answer__c
                                Where Contact__c IN :contactIDs
                                and Status__c = 'Finish'
                                ])
        {
            //Put them into map
           	List<Answer__c> answers = contactAnswers.get(answer.Contact__c);
            if (answers == null){
                answers = new List<Answer__c>();
                contactAnswers.put(answer.Contact__c,answers);
            }
            answers.add(answer);
        }
        return contactAnswers;
    }
    
    /*Get Past Answers to Q033__c, Q034__c by Contact*/
    public static List<Answer__c> queryPastAnswers(Set<Id> contactIds, Date currentDate){
        
        return [Select Q033__c, Q034__c , 
                Questionnaire__r.Questionnaire_No__c, 
                CreatedById, Contact__c
                       From Answer__c 
                       Where Contact__c IN:contactIds 
                       And CreatedDate < :currentDate ];
    }
    
    /******		TS(GC) WOrkload Specific		******/
    
    /*Get GC Tasks only by filtering Questionnaire No = 13 or 14, for today*/
    public static List<Task> queryTodayGCTasksByCity(Set<String> citySet, Integer year, Integer month, Integer day){
        
        Date reportingDate = Date.newInstance(year, month, day);
        return  [Select id , WhoId, Status, Result__c,
                            CreatedDate, Who.Type
                            from Task 
                            where Who.Type = 'Contact'
                            and ActivityDate = :reportingDate
                            /*Scott修改*/
                            and WhoId IN 
                          	(Select Id From Contact
                            Where Account.Standard_City__r.Sales_City__c IN :citySet)
   
                         ];
    }
    
    /*Put Tasks into Map by City Name*/
    public static Map<String,List<Task>> getCityTasksMap(Map<Id,Contact> contacts, List<Task> tasks){
        Map<String,List<Task>> cityTasksMap = new Map<String,List<Task>>();
        for (Task t : tasks){
        	
        	if(!contacts.containsKey(t.WhoId))
        	continue;
        	
        	
            Contact c = contacts.get(t.WhoId);
            String cityName = c.Account.Standard_City__r.Sales_City__c;
            List<Task> thisTasks = cityTasksMap.get(cityName);
            if (thisTasks == null){
                thisTasks = new List<Task>();
                cityTasksMap.put(cityName,thisTasks);
            }
            thisTasks.add(t);
        }
        return cityTasksMap;
    }
    
    /*Helper method to search for existing TS GC record, if not found then create new*/    
    public static TS_GC_Workload_Stats__c getTSGCWorkloadStats(
        List<TS_GC_Workload_Stats__c> rows,
        Integer year, 
        Integer month,
        Integer day,
        String city,
    	Integer QuestionnaireNo)
    {
        //try search for existing record
        if (rows != null && rows.isEmpty() == false){
            for (TS_GC_Workload_Stats__c row : rows){
                if (row.Reporting_Day__c == day
                   && row.Reporting_Month__c == month
                   && row.Reporting_Year__c == year
                   && row.Called_To_City__c == city
                   && row.Questionnaire_no__c == QuestionnaireNo){
                    return row;
                }
            }
        }
        //if not create new
        TS_GC_Workload_Stats__c row = new TS_GC_Workload_Stats__c();
        row.Reporting_Day__c = day;
        row.Reporting_Month__c = month;
        row.Reporting_Year__c = year;
        row.Called_To_City__c = city;
        row.Questionnaire_no__c  = QuestionnaireNo;
        row.External_ID__c = generateExternalID(
                    new Object[]{year,month,day,city,QuestionnaireNo}
                                    );
        
        resetRowTSGC(row);
       
        rows.add(row);
        return row;
    }
    
    /*Reset TSGC Row*/
    public static void resetRowTSGC(TS_GC_Workload_Stats__c row){
        //Calls generic reset method
        resetRow(row,TS_GC_objectFields);
        
        
    }
    
    /***********************	Welcome Call Staging		*******************/
    
    /*Get Tasks created today where WhatId = Questionnaire*/
    public static List<Task> queryTodayTaskByQuestionnaire(Date reportingDate, Id questionnaireId){
        return [
            Select id, WhoId , Result__c, Status, CreatedDate
            From Task 
            Where 
            ActivityDate = :reportingDate
            And WhoId <> null 
			And What.Type = 'Questionnaire__c'
            And WhatId = :questionnaireId
            And CIC_Valid_Data__c <> true  
        ];
        
    }
    
    /*Get Tasks created today where WhatId = Questionnaire 
		-without CallObject!=null
		-use CreatedDate instead of activity Date */
    public static List<Task> queryTodayTaskByQuestionnaire2(Date reportingDate, Id questionnaireId){
        return [
            Select id, WhoId , Result__c, Status, CreatedDate
            From Task 
            Where 
            DAY_ONLY(convertTimezone(CreatedDate))  = :reportingDate
            And WhoId <> null 
			And What.Type = 'Questionnaire__c'
            And WhatId = :questionnaireId
            //And CallObject <> null  
        ];
        
    }
    
    /*Get Tasks by Contact.Specific Code , within this month*/
    public static List<Task> queryCurrentMonthTasksBySpecificCode(Set<String> specificCodeSet, Integer year, Integer month, Integer day){
        Date reportingDate = Date.newInstance(year, month, day);
        return  [Select id , WhoId, Status, Result__c,
                            CreatedDate, Who.Type
                            from Task 
                            where Who.Type = 'Contact'
                          	and ActivityDate = :reportingDate
                          	and WhoId IN 
                          	(Select Id From Contact
                            Where Specific_code__c IN :specificCodeSet
                            )
                         ];
        
    }
    
    /*Get Contact with sales area info from tasks*/
    public static Map<Id,Contact> queryContactsWithSalesAreaFromTasks(List<Task> tasks){
    	Set<Id> contactIds = new Set<Id>();
        for (Task task : tasks){
            contactIds.add(task.WhoId);
        }
        return new Map<Id,Contact>([Select id ,
                                    Questionnaire__c, Questionnaire__r.Questionnaire_No__c,
                                    Account.Standard_City__r.Citycode_AAreacode__c,
                           			Account.Standard_City__r.Name,
                                    Account.Standard_City__r.Sub_Region__c,
                                    Account.Standard_City__r.Sales_City__c,
                                    Account.Standard_City__r.Region__c,
                                    Account.Standard_City__r.Area__c,
                                    Primary_RecruitChannel_Sub_type__c,
                                    Channel_Name__c,Specific_Code__c,
                                    Duplicate_Status__c,Inactive_Reason__c,
                                    BirthDate, Birthday__c
                            		from Contact 
                                  	where id in :contactIds
                          		 ]);
    }
    

    /*Get or create new row*/
    public static Welcome_Call_Staging__c getWelcomeCallStaging(
        Map<String, Welcome_Call_Staging__c> rowMap,
        String specificCode,
        Decimal questionnaire_No,
        String cityAreaCode,
        Integer year,
        Integer month, 
        Integer day){
        
            String key = generateExternalID( new Object[]{
                year,month,day,questionnaire_No,specificCode,cityAreaCode}
            );    
           
            if (rowMap != null && rowMap.containsKey(key)){
                return rowMap.get(key);
            }
            
        //if not create new
        Welcome_Call_Staging__c row = new Welcome_Call_Staging__c();
        row.Reporting_Day__c = day;
        row.Reporting_Month__c = month;
        row.Reporting_Year__c = year;
        row.Questionnaire_no__c = questionnaire_No;
        row.Specific_Code__c = specificCode;
        row.External_ID__c = key;
        
        //and reset
        resetRowWelcomeCall(row);
            
        rowMap.put(key,row);
        return row;
        
    }
    
    private static void resetRowWelcomeCall(Welcome_Call_Staging__c row){
        resetRow(row, Welcome_objectFields);
    }
     /***********************	Recruitment Staging		*******************/  
    
     /* Get Contact, whose Recruitment channel created today, group by Specific Code*/
    public static Map<String,Map<Id,Contact>> queryContactsCreatedTodayBySpecificCode(Set<String> specificCodeSet, Integer year, Integer month, Integer day){
        Map<String,Map<Id,Contact>> result = new Map<String,Map<Id,Contact>>();
        
        system.debug('Before enter contact query');
        Date reportingDate = Date.newInstance(year, month, day);
        for (Contact c : [Select Id,
                 		  Account.Standard_City__r.Name,
                          Account.Standard_City__r.Sub_Region__c,
                          Account.Standard_City__r.Sales_City__c,
                          Account.Standard_City__r.Region__c,
                          Account.Standard_City__r.Area__c,
                          Account.Status__c,
                          Primary_RecruitChannel_Type__c,
                          Channel_Name__c,
                          Specific_Code__c,
                          Duplicate_Status__c,Inactive_Reason__c,
                          BirthDate, Birthday__c,
                          (Select CN_Channel__c, CN_Sub_Channel__c,Duplicate_Status__c
                           From Recruitment_Channels__r 
                           Where                           
                            DAY_ONLY(convertTimezone(CreatedDate))  =:reportingDate
                          ), 
                 		  (Select Id From Tasks limit 1)
                 From Contact
                 Where Specific_code__c IN :specificCodeSet   
                And Id in (Select Contact__c
                           From Recruitment_Channel__c 
                           where 
                          	DAY_ONLY(convertTimezone(CreatedDate))  =:reportingDate
                          ) 
                And Specific_Code__c != null  
                And RecordType.DeveloperName='CN_Child'    
                ])
        {
            String specificCode = c.Specific_Code__c;
            Map<Id,Contact> contacts = result.get(specificCode);
            if (contacts == null){
                contacts = new Map<Id,Contact>();
                result.put(specificCode, contacts);
            }
            contacts.put(c.id, c);
            system.debug('Putting contact id into specific code'+c.id);
        }
        return  result;
        
    }
        
    
    /*Get or create new row*/
    public static Recruitment_Staging__c getRecruitmentStaging(
        List<Recruitment_Staging__c> rows,
        String specificCode,
        Integer year,
        Integer month, 
        Integer day){
        
        //try search for existing record
        if (rows != null && rows.isEmpty() == false){
            for (Recruitment_Staging__c row : rows){
                if (row.Reporting_Day__c == day
                	&& row.Reporting_Month__c == month
                   && row.Reporting_Year__c == year
                   && row.Specific_Code__c == specificCode){
                    return row;
                }
            }
        }
        //if not create new
        Recruitment_Staging__c row = new Recruitment_Staging__c();
        row.Reporting_Day__c = day;
        row.Reporting_Month__c = month;
        row.Reporting_Year__c = year;
        row.Specific_Code__c = specificCode;
        row.External_ID__c = generateExternalID(
                    new Object[]{year,month,day,specificCode}
                                    );
        
        resetRowRecruitment(row);   
        //add newly created rows back to list so it can be re-used
        rows.add(row);
        return row;
        
    }
    
    /*Reset row value*/
    public static void resetRowRecruitment(Recruitment_Staging__c row){
        resetRow(row,Recruitment_objectFields);
    }
    
}