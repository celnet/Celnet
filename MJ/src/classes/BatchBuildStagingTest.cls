@isTest
public class BatchBuildStagingTest{
    static User testCNUser = TestUtility.new_CN_User();

    //setupTestData
    static {
        
        //Address Management
        Address_Management__c testAddress = new Address_Management__c();
        testAddress.Administrative_Area__c = 'AdminArea 1';
        testAddress.Name = 'City Name 1';
        testAddress.Region__c = 'Region 1';
        testAddress.Sub_Region__c = 'Sub Region 1';
        testAddress.Sales_City__c = 'SalesCity 1';
        insert testAddress;
        
        //create new housholdhold
        Account testHousehold = new Account();
        testHousehold.RecordTypeId = ObjectUtil.GetRecordTypeID(Account.SObjectType, 'CN_HouseHold');
        testHousehold.Name = 'Test CN Household';
        testHousehold.Standard_City__c = testAddress.Id;
        insert testHousehold;
        
        //create new hospital and store
        Account testHospital = new Account(
        	RecordTypeId = ObjectUtil.GetRecordTypeID(Account.SObjectType, 'CN_Hospital'),
            Name = 'Test CN HOSPITAL',
            Standard_City__c = testAddress.Id,
            Specific_Code__c = 'H123456'
        );
        insert testHospital;
        
        //create new routine questionnaire
        Questionnaire__c testRoutineQuestionnaire = new Questionnaire__c(
            Type__c = 'Routine', Questionnaire_No__c = 1);
        //create new GC 13/14 Questionnaire
        Questionnaire__c testGC13Questionnaire = new Questionnaire__c(
        	Type__c = 'Inviation', Questionnaire_No__c = 13
        );
        Questionnaire__c testGC14Questionnaire = new Questionnaire__c(
        	Type__c = 'Inviation', Questionnaire_No__c = 14
        );
        insert new Questionnaire__c[]{
            testRoutineQuestionnaire,testGC13Questionnaire,testGC14Questionnaire}; 
       
		/* create new contacts, for various months birthdate
		  contact[0] = -60 *
		  contact[1] = -30
		  contact[2] = 0  *
		  contact[3] = 30 *
		  contact[4] = 60
		  contact[5] = 90 *
		  contact[6] = 120
		  contact[7] = 150 *
		  contact[8] = 180
		  contact[9] = 210 *
		  contact[10] = 240
		  contact[11] = 270
		  contact[12] = 300
		  contact[13] = 330 *
		*/
        Contact[] testContacts = new List<Contact>();
        for (Integer i = -60; i <= 330; i+= 30 ){
            Contact testContact = new Contact();
            testContact.RecordTypeId = ObjectUtil.GetRecordTypeID(Contact.SObjectType, 'CN_Child'); 
            testContact.LastName = 'test contact';
            testContact.AccountId = testHousehold.Id;
            testContact.Birthdate = system.today().addDays(-i);
            //Table 3 fields
            testContact.Channel_Name__c = 'Channel 1';
            testContact.Primary_RecruitChannel_Sub_type__c = 'Sub Channel 1';
            testContact.Specific_code__c = 'H123456'; //Same as Test Hospital 
            testContacts.add(testContact);
        }
        
        //Assign some contacts with Questionnaire
        testContacts[13].Questionnaire__c = testGC13Questionnaire.Id;
        testContacts[12].Questionnaire__c = testGC14Questionnaire.Id;
        insert testContacts;
        
        
        //create Success Tasks to link to Contacts
        Task[] tasks = new List<Task>();
        for (Contact c : testContacts){
            tasks.add(
                new Task(
                OwnerId = testCNUser.Id,
                WhoId = c.Id,
                ActivityDate = system.today(),
                Result__c = 'Connected/Closed',
                CallObject = 'Some value')
            );
        }
        //Make one of them Inactive Task
		tasks[1].Result__c = 'Inactive';
        //Make another one with WhatId = Questionnaire No1
        tasks[2].WhatId = testRoutineQuestionnaire.id;
        insert tasks;
        
        
		//Create Campaign and Campaign Members 
		Campaign testCampaign13 = new Campaign(
            Name='Test Campaign GC初访', GC_Call_Type__c ='GC初访', 
            RecordTypeId = ObjectUtil.GetRecordTypeID(Campaign.SObjectType, 'CN_Invitation_Special')
        );
        Campaign testCampaign14 = new Campaign(
            Name='Test Campaign GC二次回访', GC_Call_Type__c ='GC二次回访', 
            RecordTypeId = ObjectUtil.GetRecordTypeID(Campaign.SObjectType, 'CN_Invitation_Special')
        );
        insert new Campaign[]{testCampaign13,testCampaign14};
        insert new CampaignMember[]{
            new CampaignMember( CampaignId = testCampaign13.Id, ContactId = testContacts[13].Id , Last_Call_Time__c = system.today() ),
            new CampaignMember( CampaignId = testCampaign14.Id, ContactId = testContacts[12].Id , Last_Call_Time__c = system.today() )
        };
        
        //create new answers
        Answer__c[] answers = new Answer__c[]{ 
            //For table 1
            new Answer__c(
            Questionnaire__c = testRoutineQuestionnaire.Id,
            Contact__c = testContacts[0].Id,
            Q008__c = 'MJN',
            Q008_Last__c = 'MJN',
            Q033__c = 'Y',
            Q034__c = 'Y',        
            Status__c = 'Finish',
            Finished_On__c = system.now()
        ),  new Answer__c(
            Questionnaire__c = testRoutineQuestionnaire.Id,
            Contact__c = testContacts[2].Id,
            Q008__c = 'Abbott',
            Q008_Last__c = 'MJN',
            Q033__c = 'Y',
            Q034__c = 'Y',        
            Status__c = 'Finish',
            Finished_On__c = system.now()
        ),  new Answer__c(
            Questionnaire__c = testRoutineQuestionnaire.Id,
            Contact__c = testContacts[3].Id,
            Q008__c = 'MJN(GC)',
            Q008_Last__c = 'Friso',
            Q033__c = 'N',
            Q034__c = 'N',        
            Status__c = 'Finish',
            Finished_On__c = system.now()
        ) ,  new Answer__c(
            Questionnaire__c = testRoutineQuestionnaire.Id,
            Contact__c = testContacts[5].Id,
            Q008__c = 'Wyeth',
            Q008_Last__c = 'Friso',
            Q033__c = 'N',
            Q034__c = 'N',        
            Status__c = 'Finish',
            Finished_On__c = system.now()
        ) ,  new Answer__c(
            Questionnaire__c = testRoutineQuestionnaire.Id,
            Contact__c = testContacts[7].Id,
            Q008__c = 'Wyeth',
            Q008_Last__c = 'Friso',
            Q033__c = 'N',
            Q034__c = 'N',        
            Status__c = 'Finish',
            Finished_On__c = system.now()
        ) ,  new Answer__c(
            Questionnaire__c = testRoutineQuestionnaire.Id,
            Contact__c = testContacts[9].Id,
            Q008__c = 'Wyeth',
            Q008_Last__c = 'Friso',
            Q033__c = 'N',
            Q034__c = 'N',        
            Status__c = 'Finish',
            Finished_On__c = system.now()
        ) ,  new Answer__c(
            Questionnaire__c = testRoutineQuestionnaire.Id,
            Contact__c = testContacts[13].Id,
            Q008__c = 'Wyeth',
            Q008_Last__c = 'Friso',
            Q033__c = 'N',
            Q034__c = 'N',        
            Status__c = 'Finish',
            Finished_On__c = system.now()
        ) ,
        //For table 2
         new Answer__c(
            Questionnaire__c = testGC13Questionnaire.Id,
            Contact__c = testContacts[13].Id,
            Q054__c = 'Abbott',
            Q101__c = 'Pregancy',
            Q056__c = 'Y',
            Q056_GC_last__c = 'Y', 
            Q057__c = 'Y',
             Q059__c ='Breastfeeding',
             Q064__c='Y',
             Q065__c='Y',
             Q058__c='General',
             Q060__c ='Not born yet',
             Q061__c='Y',
             Q062__c='Cry',
             Q063__c='Y',
            Status__c = 'Finish',
            Finished_On__c = system.now()
        ) , new Answer__c(
            Questionnaire__c = testGC14Questionnaire.Id,
            Contact__c = testContacts[12].Id,
            Q054__c = 'Abbott',
            Q101__c = 'Pregancy',
            Q056__c = 'N',
            Q056_GC_last__c = 'Y', 
            Q057__c = 'N',
             Q059__c ='No willing to change',
             Q064__c='Y',
             Q065__c='Y',
             Q058__c='N',
             Q060__c ='No digestion issue',
             Q061__c='Y',
             Q062__c='Cry',
             Q063__c='Y',       
            Status__c = 'Finish',
            Finished_On__c = system.now()
        ) 
                
                //for table 3 , 4?
        };       
        insert answers;
        
        //Create Recruitment Channels linking to Contacts
         Recruitment_Channel__c[] listRC = new List<Recruitment_Channel__c>();
        for (Contact c : testContacts){
            listRC.add(
                new Recruitment_Channel__c(
                Primary__c = true,
                Contact__c = c.Id,
                Specific_Code__c = 'H123456') //same as test hospital
            );
        }
        insert listRC;
    }
    
    /**	 Tests Methods	**/
    
    static testMethod void testScheduleTS(){
        Test.startTest();
        ScheduleStagingTS sch = new ScheduleStagingTS();
        System.schedule('testTS', '0 0 0 * * ?', sch);
        
        ScheduleStagingTS adhoc = new ScheduleStagingTS(system.today().Year(), system.today().Month(), system.today().Day());
        System.schedule('testTS adhoc', '0 0 0 * * ?', adhoc);
        Test.stopTest();
    }
    
    static testMethod void testBatchTS(){
        Test.startTest();
        String userQuery = 'Select Id, Division, Department, EmployeeNumber, Name ';
        userQuery += ' From User where id =\'' + testCNUser.id + '\''; 
        BatchBuildStagingTSWorkload ts = new BatchBuildStagingTSWorkload(
        	userQuery, system.today().Year(), system.today().Month(), system.today().Day()
        );
        Database.executeBatch(ts,10);
        Test.stopTest();
    }
    
    static testMethod void testBatchTSGC13(){
        Test.startTest();
        String cityQuery = 'SELECT Sales_City__c, Region__c FROM Address_Management__c Group By Region__c,Sales_City__c';
        BatchBuildStagingTSGCWorkload tsgc = new BatchBuildStagingTSGCWorkload(
        	cityQuery, system.today().Year(), system.today().Month(), system.today().Day(),13
        );
        Database.executeBatch(tsgc,10);
        Test.stopTest();
    }
    static testMethod void testBatchTSGC14(){
        Test.startTest();
        String cityQuery = 'SELECT Sales_City__c, Region__c FROM Address_Management__c Group By Region__c,Sales_City__c';
        BatchBuildStagingTSGCWorkload tsgc = new BatchBuildStagingTSGCWorkload(
        	cityQuery, system.today().Year(), system.today().Month(), system.today().Day(),14
        );
        Database.executeBatch(tsgc,10);
        Test.stopTest();
    }

    
    static testMethod void testScheduleWelcomeCall(){
        Test.startTest();
        ScheduleStagingWelcomeCall sch = new ScheduleStagingWelcomeCall();
        System.schedule('testSales', '0 0 0 * * ?', sch);
        
        ScheduleStagingWelcomeCall adhoc = new ScheduleStagingWelcomeCall(system.today().Year(), system.today().Month(), system.today().Day());
        System.schedule('testSales adhoc', '0 0 0 * * ?', adhoc);
        Test.stopTest();
    }
    
 
    
    static testMethod void testScheduleRecruitment(){
        Test.startTest();
        ScheduleStagingRecruitment sch = new ScheduleStagingRecruitment();
        System.schedule('testRecruit', '0 0 0 * * ?', sch);
        
        ScheduleStagingRecruitment adhoc = new ScheduleStagingRecruitment(system.today().Year(), system.today().Month(), system.today().Day());
        System.schedule('testRecruit adhoc', '0 0 0 * * ?', adhoc);
        Test.stopTest();
    }
    
    
}