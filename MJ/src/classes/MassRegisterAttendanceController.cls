/*Author:leo.bi@celnet.com.cn
 *Date:2014-5-8
 *function:The controller of MassRegisterAttendance page, Input Campaign attendee list, 
 *		   and trigger register attendance process
 *Alter Date:2014-05-29
 *alter function:add Address and Date Filter
 *Apply To:CN
 */
public class MassRegisterAttendanceController 
{
	public List<Attendance__c> list_att{get;set;}
	public Campaign parentCampaign{get;set;}
	public Boolean isValid {get;set;} {isValid = true;}
	public Boolean hasCampaign{get;set;}
	private string errorMessage = '';
	
	//variables for sales area lists
	public String selectedSalesRegion{get;set;}
	public String selectedSalesSubRegion{get;set;}
	public String selectedSalesArea{get;set;} 
	public String selectedSalesCity {get;set;}
	List<String> list_Region = new List<String>(); 
	public Map<Id,String> map_Id_SalesSubRegion = new Map<Id,String>();
	public Map<Id,String> map_Id_SalesArea = new Map<Id,String>();
	public Map<Id,String> map_Id_SalesCity = new Map<Id,String>();
	//adress disabled attribute
	public Boolean salesSubregionDisable{get;set;}{salesSubregionDisable = true;}
	public Boolean salesAreaDisable{get;set;}{salesAreaDisable = true;}
	public Boolean salesCityDisable{get;set;}{salesCityDisable = true;}
	//date
	public String selectedDate{get;set;}
	public Boolean campaignItemsDisable{get;set;}{campaignItemsDisable = true;}	
	
	//special invitation
	public Boolean isSpecial{get;set;}
	public string campaignType{get;set;}{campaignType = 'Invitation';}
	public Attendance__c attProduct{get;set;}{attProduct = new Attendance__c();}
	public Boolean specialProductShow{get;set;}
	
	public Boolean phoneNotValid{get;set;}{phoneNotValid = false;}
	
	public 	MassRegisterAttendanceController(ApexPages.StandardController controller)
	{
		initBusinessAddress();
		list_att = new List<Attendance__c>();
		parentCampaign = new Campaign();
		initTable();
	}
	
	public void initTable()
	{
		list_att.clear();
		errorMessage = '';
		if(parentCampaign.Id == null)
		{
			hasCampaign = false;
		}
		else
		{
			hasCampaign = true;
			parentCampaign = [select Id, Standard_City__r.Phone_Area_Code__c from Campaign where Id=:parentCampaign.Id];
			//If number of existing lines < 5, then set to 5
	        addNewLines(5 - list_att.size());
		}
		
	}
	
	public PageReference save()
    {
    	List<Attendance__c> list_InsertAtt = new List<Attendance__c>();
		isValid = true;
		errorMessage = '';
		if(campaignType == 'Invitation Special' && attProduct.Special_Product__c == null)
		{
			isValid = false;
			errorMessage = ' 特殊邀约必须填写特殊产品';
		}
		for(Attendance__c att : list_att)
		{
			if((att.Mother_Name__c == null || att.Baby_Birthday__c == null || att.Phone__c == null) 
			&& (att.Mother_Name__c != null || att.Baby_Birthday__c != null || att.Phone__c != null 
			 || att.Using_Product__c != null || att.Invitation_Channel__c != null || att.Remark__c != null
			 || att.Purchase_Date__c != null))
			{
				isValid = false;
				if(att.Mother_Name__c == null) att.Mother_Name__c.addError('请输入妈妈姓名');
				if(att.Baby_Birthday__c == null) att.Baby_Birthday__c.addError('请输入宝宝生日');
				if(att.Phone__c == null) att.Phone__c.addError('请输入手机号');
			}
			if( (att.Mother_Name__c != null || att.Baby_Birthday__c != null || att.Phone__c != null 
			 || att.Using_Product__c != null || att.Invitation_Channel__c != null || att.Remark__c != null
			 || att.Purchase_Date__c != null)&&campaignType == 'Invitation Special' && att.Purchase_Date__c == null)
			{
				isValid = false;
				att.Purchase_Date__c.addError(' 特殊邀约必须填写购买日期');
			}
			if(att.Purchase_Date__c > Date.today())
			{
				isValid = false;
				att.Purchase_Date__c.addError('购买日期不能大于今天，请填写正确的购买日期');
			}
			if(!phoneNotValid)
			{
				if(att.Phone__c != null && (!(Pattern.matches('^[0-9]{11}$',att.Phone__c) || Pattern.matches('^[0-9]{12}$',att.Phone__c) || Pattern.matches('^[0-9]{7}$',att.Phone__c) || Pattern.matches('^[0-9]{8}$',att.Phone__c))))
				{
					if(isValid)
					{//如果其他条件通过了验证，而电话报错，则允许强制保存，即不验证phone
						phoneNotValid = true;
					}
					isValid = false;
					att.Phone__c.addError('电话号码格式不正确');
				}
			}
			if(!isValid)
			{
				continue;
			}
			if(parentCampaign.Standard_City__r.Phone_Area_Code__c != null && att.Phone__c != null && (Pattern.matches('^[0-9]{7}$',att.Phone__c) || Pattern.matches('^[0-9]{8}$',att.Phone__c)))
			{
				att.Phone__c = parentCampaign.Standard_City__r.Phone_Area_Code__c + att.Phone__c;
			}
			if(att.Mother_Name__c == null && att.Baby_Birthday__c == null && att.Phone__c == null)
			{
				continue;
			}
			att.Special_Product__c = attProduct.Special_Product__c;
			att.Name = att.Mother_Name__c;
			list_InsertAtt.add(att);
		}
    	if(!isValid)
    	{
    		if(errorMessage != '')
    		{
	    		ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,errorMessage));
	    		errorMessage = '';
    		}
    		return null;
    	}
    	try
    	{
    		if(!list_InsertAtt.isEmpty())
    		{
                insert list_InsertAtt;
    		}
    		return new PageReference('/' + parentCampaign.Id);
    	}catch(Exception e)
    	{
    		ApexPages.addMessages(e);
            return null;
    	}
    	
    }
	
	public PageReference cancel()
    {
    	return new PageReference('/'+parentCampaign.Id);
    }
    
    public PageReference addNew()
    {
    	//Add 3 lines in one click
        addNewLines(3);
        return null; 
    }
	
	private void addNewLines(Integer numerOfLines)
    {
    	for(Integer count=0; count<numerOfLines; count++)
    	{
            list_att.add(createNewAttendance()); 
    	}
    }
	
	private Attendance__c createNewAttendance()
    {
        Attendance__c attendance = new Attendance__c();  
        attendance.Campaign__c = parentCampaign.Id;
        return attendance;
    }
	
	//adress section
	private void initBusinessAddress()
	{
		List<Address_Management__c> list_temp = [Select Region__c From Address_Management__c where Region__c != null];
		Set<String> set_temp = new Set<String>();
		if(list_temp.size()>0)
		{
			for(Address_Management__c a : list_temp)
			{
				set_temp.add(a.Region__c);
			}
			list_Region.addAll(set_temp);
		}
	}
	public List<SelectOption> salesRegionItems
	{
		get
		{
			List<SelectOption> region = new List<SelectOption>();
			region.add(new SelectOption('', '--None--'));
			if(list_Region.size() != 0) 
			{
				for(String a : list_Region)
				{
					region.add(new SelectOption(a,a));
				}
			}
			return region;
		}
	}
	
	public List<SelectOption> salesSubRegionItems
	{
		get
		{
			List<SelectOption> options = new List<SelectOption>();
			Set<String> set_De_Duplicate = new Set<String>();//de-duplicate
			options.add(new SelectOption('', '--None--'));
			if(selectedSalesRegion != null && selectedSalesRegion != '')
			{
				List<Address_Management__c> list_temp_am = [select Id, Region__c,Sub_Region__c 
																	 from Address_Management__c 
																	 where Region__c=:selectedSalesRegion 
																	 and Sub_Region__c!=null];
				for(Address_Management__c a : list_temp_am)
				{
					if(!set_De_Duplicate.contains(a.Sub_Region__c))
					{
						set_De_Duplicate.add(a.Sub_Region__c);
						options.add(new SelectOption(a.Id,a.Sub_Region__c));
						map_Id_SalesSubRegion.put(a.Id,a.Sub_Region__c);
					}
				}
			}
			return options;
		}
	}
	
	public List<SelectOption> salesAreaItems
	{
		get
		{
			List<SelectOption> options = new List<SelectOption>();
			Set<String> set_De_Duplicate = new Set<String>();//de-duplicate
			options.add(new SelectOption('', '--None--'));
			if(selectedSalesSubRegion != null && selectedSalesSubRegion != '')
			{
				List<Address_Management__c> list_temp_am = [select Id, Region__c,Sub_Region__c,Area__c
																     from Address_Management__c 
																     where Sub_Region__c=:map_Id_SalesSubRegion.get(selectedSalesSubRegion) 
																     and Region__c= :selectedSalesRegion and Area__c != null];
				for(Address_Management__c a : list_temp_am)
				{
					if(!set_De_Duplicate.contains(a.Area__c))
					{
						set_De_Duplicate.add(a.Area__c);
						options.add(new SelectOption(a.Id,a.Area__c));
						map_Id_SalesArea.put(a.Id,a.Area__c);
					}
				}
			}
			return options;
		}
	}
	
	public List<SelectOption> salesCityItems
	{
		get
		{
			List<SelectOption> options = new List<SelectOption>();
			Set<String> set_De_Duplicate = new Set<String>();//de-duplicate
			options.add(new SelectOption('', '--None--'));
			if(selectedSalesArea != null && selectedSalesArea != '')
			{
				List<Address_Management__c> list_temp_am = [select Id, Region__c, Sub_Region__c, Area__c, Sales_City__c 
														    from Address_Management__c 
														    where Region__c=:selectedSalesRegion 
															and Sub_Region__c=:map_Id_SalesSubRegion.get(selectedSalesSubRegion) 
															and Area__c=:map_Id_SalesArea.get(selectedSalesArea)
															and Sales_City__c!=null];
				for(Address_Management__c a : list_temp_am)
				{
					if(!set_De_Duplicate.contains(a.Sales_City__c))
					{
						set_De_Duplicate.add(a.Sales_City__c);
						options.add(new SelectOption(a.Id,a.Sales_City__c));
						map_Id_SalesCity.put(a.Id,a.Sales_City__c);
					}
				}
			}
			return options;
		} 
	}
	public void refreshSalesSubRegion()
	{
		selectedSalesSubRegion = null;
		selectedSalesArea = null;
		selectedSalesCity = null;
		if(selectedSalesRegion != null)
		{
			salesSubregionDisable = false;
			salesAreaDisable = true;
			salesCityDisable = true;
		}
		else
		{
			salesSubregionDisable = true;
			salesAreaDisable = true;
			salesCityDisable = true;
		}
		//refreshCampaignItems();
	}
	
	public void refreshSalesArea()
	{
		selectedSalesArea = null;
		selectedSalesCity = null;
		if(selectedSalesSubRegion != null)
		{
			salesSubregionDisable = false;
			salesAreaDisable = false;
			salesCityDisable = true;
		}
		else
		{
			salesAreaDisable = true;
			salesCityDisable = true;
		}
		//refreshCampaignItems();
	}
	
	public void refreshSalesCity()
	{
		selectedSalesCity = null;
		if(selectedSalesArea != null)
		{
			salesSubregionDisable = false;
			salesAreaDisable = false;
			salesCityDisable = false;
		}
		else
		{
			salesCityDisable = true;
		}
		refreshCampaignItems();
	}
	//date choose
	public List<SelectOption> dateItmes
	{
		get
		{
			List<SelectOption> options = new List<SelectOption>();
			options.add(new SelectOption('','--None--'));
			options.add(new SelectOption(String.valueOf(Date.today().addMonths(-5)),this.formatDate(Date.today().addMonths(-5))));
			options.add(new SelectOption(String.valueOf(Date.today().addMonths(-4)),this.formatDate(Date.today().addMonths(-4))));
			options.add(new SelectOption(String.valueOf(Date.today().addMonths(-3)),this.formatDate(Date.today().addMonths(-3))));
			options.add(new SelectOption(String.valueOf(Date.today().addMonths(-2)),this.formatDate(Date.today().addMonths(-2))));
			options.add(new SelectOption(String.valueOf(Date.today().addMonths(-1)),this.formatDate(Date.today().addMonths(-1))));
			options.add(new SelectOption(String.valueOf(Date.today()),this.formatDate(Date.today())));
			options.add(new SelectOption(String.valueOf(Date.today().addMonths(1)),this.formatDate(Date.today().addMonths(1))));
			options.add(new SelectOption(String.valueOf(Date.today().addMonths(2)),this.formatDate(Date.today().addMonths(2))));
			options.add(new SelectOption(String.valueOf(Date.today().addMonths(3)),this.formatDate(Date.today().addMonths(3))));
			options.add(new SelectOption(String.valueOf(Date.today().addMonths(4)),this.formatDate(Date.today().addMonths(4))));
			options.add(new SelectOption(String.valueOf(Date.today().addMonths(5)),this.formatDate(Date.today().addMonths(5))));
			options.add(new SelectOption(String.valueOf(Date.today().addMonths(6)),this.formatDate(Date.today().addMonths(6))));
			return options; 
		}
	}
	
	public List<SelectOption> campaignTypes
	{
		get
		{
			list<SelectOption> list_temp = new list<SelectOption>();
			Schema.DescribeFieldResult payList = Campaign.Campaign_Type__c.getDescribe();
			List<Schema.PicklistEntry> ListPay = payList.getPicklistValues();
			for(Schema.PicklistEntry sp : ListPay)
			{
				list_temp.add(new SelectOption(sp.getValue(),sp.getLabel()));
			}
			return list_temp;
		}
		set;
	}
	
	public List<SelectOption> campaignItmes
	{
		get
		{
			List<SelectOption> options = new List<SelectOption>();
			options.add(new SelectOption('','--None--'));
			if(selectedSalesCity != null && selectedDate != null)
			{
				Date dateBegin = Date.valueOf(selectedDate).toStartOfMonth();
				Date endDate = Date.valueOf(selectedDate).toStartOfMonth().addMonths(1).addDays(-1);
				List<Campaign> list_Campaign = [select Id, Name from Campaign 
												where Status !='Cancelled' and Standard_City__r.Sales_City__c = :map_Id_SalesCity.get(selectedSalesCity)
												and Event_Date__c >= :dateBegin and Event_Date__c <=:endDate
												and Campaign_Type__c = :campaignType
												and IsActive = true];
				system.debug('---------------------campaigntype----------------------' + campaignType);
				system.debug('---------------------map_Id_SalesCity.get(selectedSalesCity)---------------------------------' + map_Id_SalesCity.get(selectedSalesCity));
				for(Campaign c : list_Campaign)
				{
					options.add(new SelectOption(c.Id,c.Name));
				}
			}
			return options;
		}
	}
	
	private string formatDate(Date paraDate)
	{
		string dateYear = String.valueOf(paraDate.year());
		string dateMonth = String.valueOf(paraDate.month());
		return dateYear + '-' + dateMonth;
	}
	
	public void refreshCampaignItems()
	{
		parentCampaign = new Campaign();
		if(selectedSalesCity != null && selectedSalesCity != '' && selectedDate!= null && selectedDate != '')
		{
			campaignItemsDisable = false;
		}
		else
		{
			campaignItemsDisable = true; 
		}
		if(campaignType == 'Invitation')
		{
			specialProductShow = false;
		}
		else
		{
			specialProductShow = true;
		}
		initTable();
	}
}