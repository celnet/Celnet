/*
*Date:2014-10-15
*author:mark
*
*/
@isTest
global class WechatOrderDetailController_Test implements HttpCalloutMock {

  	static User HK_User = TestUtility.new_HK_User();
    static testMethod void myUnitTest() 
    {
    	system.runAs(HK_User)
    	{
    		system.Test.loadData(Wechat_Reminding__c.sObjectType, 'WechatReminding');
    		createWechatSetting();
    		createPublicHoliday();
    		Account a = CreateAccount();
    		Product__c p = CreateProduct();
    		Order__c lastOrder = CreateOrder(a, p, 3);
    		ApexPages.currentPage().getParameters().put('code','11000');
    		ApexPages.currentPage().getParameters().put('state','CideatechSFWeiXin');
    		WechatPlatformRequestMock fakeResponse = new WechatPlatformRequestMock();
    		system.Test.setMock(HttpCalloutMock.class, fakeResponse);
            system.test.starttest();
    		WechatOrderDetailController wodc = new WechatOrderDetailController();
    		wodc.accountId = a.id;
    		wodc.productName = '安嬰媽媽A+';
    		wodc.num = 3;
    		wodc.sku = '900克/罐';
    		wodc.sendOrder();
    		wodc.GetLastOrder();
    		wodc.ConfirmOrder();
    		//wodc.CreateOrder();
    		system.Test.stopTest();
    		
    	}
    }
    
    public static void createPublicHoliday()
    {
    	Public_Holiday__c ph = new Public_Holiday__c();
    	ph.Public_Holiday__c = date.today();
    	ph.name = 'test';
    	insert ph;
    }
    public static void createWechatSetting()
    {
    	Wechat_Setting__c ws = new Wechat_Setting__c();
    	ws.Name = 'CideatechSFWeiXin';
    	ws.App_Id__c = 'wx76c641c11a6462e5';
    	ws.App_Secret__c = '4ce571024be7e0093afbc2f536a6bda9';
    	ws.API_Base_Url__c = 'https://api.weixin.qq.com';
    	ws.Access_Token1__c = 'fkTEnPZQ2p7TaePdgDGRvY69Okdph-6fe2TUZlQ7IFlASghscp_mL465Eu12tKW9vGsyFWhKNCmpi_uTd4xNY6cIiKSOShANTbpHvHDAKE	';
    	ws.Token__c = 'Cideatech';
    	insert ws;
    }
    
    public static Account CreateAccount()
    {
    	Account a = new Account();
    	a.First_Name__c = 'li';
    	a.Last_Name__c = 'mark';
    	a.Address_Block__c = 'block';
    	a.Address_Street__c = 'street';
    	a.Address_Building__c = 'building';
    	a.Address_Flat__c = 'flat';
    	a.Address_City__c = 'HK-香港';
    	a.Address_District__c = 'Kam Tin-錦田';
    	a.Address_District__c = 'NT-新界';
    	a.Address_Floor__c = 'floor';
    	a.Name = 'mark li';
    	insert a;
    	return a;
    }
    
    public static Product__c CreateProduct()
    {
    	Product__c p = new Product__c();
    	p.Name = 'Enfamama A+';
    	p.Product_SKU__c = '900克/罐';
    	p.Product_Chinese_Name__c = '安嬰媽媽A+';
    	p.Product_Chinese_SKU__c = '900克/罐';
    	p.Unit_Price__c = 360;
    	p.Product_Nature__c = 'MJ';
    	p.Inactive__c = false;
    	p.Is_DSO_Product__c = true;
    	insert p;
    	return p;
    }
    
    public static Order__c CreateOrder(Account a , Product__c p , decimal quntity)
    {
    	Order__c o = new Order__c();
    	o.Address_Flat_Floor_Block_Building__c = 'Flat B, Floor 30, Block 15';
    	o.Address_Street_District_Territory__c = 'ABC house, Causeway street';
    	o.Address_Extra_Line__c = 'Causeway Bay, HK';
		o.HouseHold__c = a.Id;
		o.Expected_Delivery_Date__c = Date.today().addDays(3);
    	insert o;
    	Order_Item__c oi = new Order_Item__c();
    	oi.Product__c = p.Id;
    	oi.Quantity__c = quntity;
    	oi.Order__c = o.Id;
    	
    	
    	insert oi;
    	
    	return o;
    }
    
    global  HTTPResponse respond(HTTPRequest req)
    {
        HttpResponse resp = new HttpResponse();
        resp.setStatusCode(200);
        resp.setStatus('ok');
        string endpoint = req.getEndpoint();
        system.debug('***************endpoint***************'+endpoint);
        if(endpoint.contains('/sns/oauth2/access_token'))
        {
            resp.setBody('{"access_token":"ACCESS_TOKEN","expires_in":7200,"refresh_token":"REFRESH_TOKEN","openid":"OPENID","scope":"SCOPE"}');
        }
        return resp;
    }
}