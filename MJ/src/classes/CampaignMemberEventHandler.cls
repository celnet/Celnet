/*
Author: scott.zhang@celnet.com.cn 
Created On: 2014-05-19
Function: 
1.update Last Call Result from IntegrationLog
Support Event:AfterInsert, AfterUpdate
Apply To: CN 
*/
public class CampaignMemberEventHandler implements Triggers.Handler {
	public void Handle()
	{
		if(!Context.ApplyTo(new String[] {'CN'})) 
    	{
    		return;
		}
		if(trigger.isAfter && (trigger.isInsert || trigger.isUpdate))
		{
			if(trigger.new[0] instanceof Task)
			{
				try
	            {
	                this.updateToCampaignMember();
	            } catch(Exception e)
	            {
	            	Error_Log__c el = new Error_Log__c();
	            	el.Line_Number__c = e.getLineNumber();
	            	el.Message__c = e.getMessage();
	            	el.Stack_Trace_String__c = e.getStackTraceString();
	            	el.Type_Name__c = e.getTypeName();
	            	el.Error_Time__c = DateTime.now();
	            	el.Handler_Name__c = 'CampaignMemberEventHandler';
	            	el.User__c = UserInfo.getUserId();
	            	insert el;
	            }
			}
			if(trigger.new[0] instanceof Answer__c)
			{
				this.answerSyncCMAfterInvitation();
			}
		}
		if(trigger.isAfter && trigger.isInsert)
		{
			if(trigger.new[0] instanceof Campaign)
			{
				this.addCampaignMemberStatus();
			}
		}
		if(trigger.isBefore && trigger.isUpdate)
		{
			if(trigger.new[0] instanceof CampaignMember)
			{
				campaignFinishBackVisit();
			}
		}
	}
	
	private void campaignFinishBackVisit()
	{
		List<CampaignMember> list_CampaignMember_New = trigger.new;
		for(CampaignMember cm : list_CampaignMember_New)
		{
			if(cm.Call_Status__c != 'Finish Call')
			{ 
				continue;
			}
			CampaignMember cmOld = (CampaignMember)trigger.oldMap.get(cm.Id);
			if(cmOld.Call_Status__c == 'Waiting Reciprocal Call' && cmOld.Status == 'Attended and waiting for back-visit')
			{
				cm.Status = 'Finish back-visit';
			}
		}
	}
	
	/*
	 *Author: leo.bi@celnet.com.cn
	 *Created On: 2014-05-13
	 *Function: insert campaignmember status picklist value after a campaign is inserted
	 *Apply To: CN
	 */
	//add campaignmember status picklist value 
	private void addCampaignMemberStatus()
	{
		//First,set 'Sent' value isdefault=false. Second, delete all value.Third, insert new value.
		List<Campaign> list_New_Campaign = trigger.new;
		List<CampaignMemberStatus> list_Status = new List<CampaignMemberStatus>();
		List<CampaignMemberStatus> list_Delete_Status = new List<CampaignMemberStatus>();
		List<CampaignMemberStatus> list_Update_Status = new List<CampaignMemberStatus>();
		List<CampaignMemberStatus> list_Insert_Status = new List<CampaignMemberStatus>();
		List<Campaign_Member_Status__c> list_Custom_Setting = [Select Sort_Value__c,Has_Responded__c, Name, Is_Default__c From Campaign_Member_Status__c];
		if(list_Custom_Setting.size() == 0)
		{
			return;
		}
		Set<Id> set_campaignIds = new Set<Id>();
		for(Campaign c : list_New_Campaign)
		{
			set_campaignIds.add(c.Id);
		}
		if(set_campaignIds.size() == 0)
		{
			return;
		}
		list_Status =  [select ID, CampaignID, Label, IsDefault from CampaignMemberStatus where CampaignID in :set_campaignIds];
		list<CampaignMemberStatus> list_temp = [select ID, CampaignID, Label, IsDefault from CampaignMemberStatus];
		for(CampaignMemberStatus cms : list_Status)
		{
			//ignore non CN_Campaign_PageLayout type Campaign status
			if(!set_campaignIds.contains(cms.CampaignId))
			{
				continue;
			}
			if(cms.IsDefault)
			{
				cms.IsDefault = false;
				list_Update_Status.add(cms);
			}
			list_Delete_Status.add(cms);
		}
		//update list_Update_Status
		if(list_Update_Status.size() != 0)
		{
			update list_Update_Status;
		}
		//delete list_Delete_Status
		if(list_Delete_Status.size() != 0)
		{
			delete list_Delete_Status;
		}
		for(Campaign c : list_New_Campaign)
		{
			for(Campaign_Member_Status__c custom_cms : list_Custom_Setting)
			{
				CampaignMemberStatus cms = new CampaignMemberStatus();
				cms.CampaignId = c.Id;
				cms.Label = custom_cms.Name;
				cms.SortOrder = Integer.valueOf(custom_cms.Sort_Value__c);
				cms.IsDefault = custom_cms.Is_Default__c;
				cms.HasResponded = custom_cms.Has_Responded__c;
				list_Insert_Status.add(cms);
			}
		}
		if(list_Insert_Status.size() != 0)
		{
			insert list_Insert_Status;
		}
	}
	
	//afert answer's Q047 Q048 is answered, update campaignmember's status
	private void answerSyncCMAfterInvitation()
	{
		List<Answer__c> list_ans = trigger.new;
		List<Answer__c> list_Answer_New = new List<Answer__c>();
		List<CampaignMember> list_CM_Update = new List<CampaignMember>();
		Set<Id> set_CampaignMember_Id = new Set<Id>();
		
		
		Boolean IsInsert =(trigger.isInsert?true:false);
		Boolean IsUpdate =(trigger.isUpdate?true:false);
		for(Answer__c ans : list_ans)
		{
			if(ans.Contact__c == null || ans.Status__c != 'Finish' || ans.Finished_On__c == null || ans.Campaign_Member_Id__c == null)
			{
				continue;
			}
			
			Answer__c oldans = (IsUpdate?(Answer__c)trigger.oldMap.get(ans.Id):new Answer__c());
			
			if((IsInsert && (ans.Q047__c != null || ans.Q048__c != null || ans.Q096__c !=null || ans.Q097__c !=null)) || (IsUpdate && (ans.Q047__c !=oldans.Q047__c || ans.Q048__c !=oldans.Q048__c ||ans.Q096__c!=oldans.Q096__c||ans.Q097__c!=oldans.Q097__c || ans.Status__c != oldans.Status__c)))
			{
				set_CampaignMember_Id.add(ans.Campaign_Member_Id__c);
				list_Answer_New.add(ans);
			}
		}
		if(set_CampaignMember_Id.size() == 0)
		{
			return;
		}
		List<CampaignMember> list_CampaignMember = [select Status,Id,Willing_To_Join_2nd_Invitation__c,Attend_Time_2nd_Invitation__c from CampaignMember where Id in :set_CampaignMember_Id];
		for(Answer__c ans : list_Answer_New)
		{
			for(CampaignMember cm : list_CampaignMember)
			{
				if(cm.Id == ans.Campaign_Member_Id__c)
				{
					if(ans.Q047__c != null)
					{
						if(ans.Q047__c == 'Y')cm.Status = 'Connect and willing to join';
						if(ans.Q047__c == 'N')cm.Status = 'Connect and no willing to join';
						if(ans.Q047__c == 'Considering')cm.Status = 'Conect but undecide';
					}
					if(ans.Q048__c != null)
					{
						if(ans.Q048__c == 'Y')cm.Status = 'Connect and willing to join';
						if(ans.Q048__c == 'N')cm.Status = 'Connect and no willing to join'; 
						if(ans.Q048__c == 'Considering')cm.Status = 'Conect but undecide';
					}
					cm.Willing_To_Join_2nd_Invitation__c = ans.Q096__c;
					cm.Attend_Time_2nd_Invitation__c = ans.Q097__c;
					list_CM_Update.add(cm);
					break;
				}
				
			}
		}
		if(list_CM_Update.size() != 0)
		{
			update list_CM_Update;
		}
	}
	//Update CampaignMember
	//update Last Call Result from IntegrationLog 
	private void updateToCampaignMember()
	{
		//final Set<String> Set_Status = new Set<String>{'Finish Call'};
		List<Task> taskList = trigger.new;
		
		Set<Id>Set_ConIds = new Set<Id>();
		for(Task ta : taskList)
		{
			if(ta.CallObject != null||ta.Status=='Completed')
			continue;
			
			if(ta.WhoId != null && ta.Result__c !=null)
			Set_ConIds.add(ta.WhoId);
		}
		if(Set_ConIds.size()==0)
		return;
		
		List<CampaignMember> List_UpCamMe = new List<CampaignMember>();
		for(CampaignMember CamMe : [select Id,ContactId,Status,Last_Call_Result__c,Last_Call_Time__c from CampaignMember where ContactId in:Set_ConIds and (Call_Status__c !='Finish Call' or (Call_Status__c ='Finish Call' and LastModifiedDate=TODAY))])
		{
			for(Task ta : taskList)
			{
				//if(ta.CallObject == null)
				//continue;
				if(ta.WhoId == CamMe.ContactId)
				{
					CamMe.Last_Call_Result__c = ta.Result__c;
					CamMe.Last_Call_Time__c = datetime.now();
					List_UpCamMe.add(CamMe);
					break;
				}
			}
		}
		if(List_UpCamMe.size()>0)
		{
			try
			{
				update List_UpCamMe;
			}
			catch(Exception e)
			{
				List<Id> CampaignMemberId = new List<Id>();
				List<String> Last_Call_Result = new List<String>();
				List<DateTime> Last_Call_Time = new List<DateTime>();
				for(Integer i=0;i<List_UpCamMe.size();i++)
				{
					CampaignMember CamMe = List_UpCamMe[i];
					CampaignMemberId.add(CamMe.Id);
					Last_Call_Result.add(CamMe.Last_Call_Result__c);
					Last_Call_Time.add(CamMe.Last_Call_Time__c);
				}
				//@future 
				FutureClass.futureUpdateToCampaignMember(CampaignMemberId,Last_Call_Result,Last_Call_Time);
			}
		}
	}
}