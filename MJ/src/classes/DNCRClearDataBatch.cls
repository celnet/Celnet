/*
    Clean the uploaded DNCR records upon job finished
*/
public class DNCRClearDataBatch implements Database.Batchable<sObject> 
{
   private final String QUERY = 'SELECT ID FROM DNCR__c';
   
   public Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(QUERY);
   }
   
   public void execute( Database.BatchableContext BC, List<sObject> records){
      delete records;
      Database.emptyRecycleBin(records);
   }

   public void finish(Database.BatchableContext BC){
       AsyncApexJob jobResult = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                 FROM AsyncApexJob WHERE Id =:BC.getJobId()];
       
       //Update Job Execution Result and Send notification email
       DNCRHelper.updateClearBatchStatus(BC.getJobId(),jobResult);
   }

}