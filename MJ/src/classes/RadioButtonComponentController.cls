/**
Custom Radio Button VF Component to allow display checked and disabled state.
*/
public with sharing class RadioButtonComponentController {
	public Boolean the_checked{get;set;}
	public Boolean the_disabled{get;set;}
	public String the_value{get;set;}
	public String the_label{get;set;}
	public String the_name{get;set;}
	
	public RadioButtonComponentController(){
		the_checked = false;
		the_disabled = false;
	}
	
}