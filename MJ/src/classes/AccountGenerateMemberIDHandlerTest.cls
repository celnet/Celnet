/*
Apply to [HK/CN/ALL]: CN
Author: shiqi.ng@sg.fujitsu.com
Last Modified: 2014-04-29
*/
@isTest
private class AccountGenerateMemberIDHandlerTest {
	
    static testMethod void testCreateOneCNAccount() {
      system.runas(TestUtility.new_CN_User()){
        TestUtility.setupMemberIDCustomSettings();
        
    	Account acc = new Account();
    	acc.Name = 'Test Account';
    	insert acc;
    	acc = [Select Member_Id__c from Account where id = :acc.id limit 1];
    	/* commented by sq on 6jun14
    	system.assert(acc.Member_ID__c != null);
    	system.assert(acc.Member_Id__c.startsWith('CN'));
    	system.assert(acc.Member_Id__c.endsWith('1888'));*/
      }
    }
    static testMethod void testCreateManyCNAccount() {
      system.runas(TestUtility.new_CN_User()){
      	TestUtility.setupMemberIDCustomSettings();
    	List<Account> accList = new List<Account>();
    	for (Integer i=0;i<3;i++){
    		Account acc = new Account();
    		acc.Name = 'Test Account';
    		accList.add(acc);
    	}
    	insert accList;
    	accList = [Select Member_Id__c from Account where Name = 'Test Account'];
    	/* commented by sq on 6jun14
    	system.assert(accList[0].Member_ID__c != null);
    	system.assert(accList[0].Member_Id__c.startsWith('CN'));
    	system.assert(accList[0].Member_Id__c.endsWith('1888'));
    	system.assert(accList[1].Member_Id__c.startsWith('CN'));
    	system.assert(accList[1].Member_Id__c.endsWith('1889'));
    	system.assert(accList[2].Member_Id__c.startsWith('CN'));
    	system.assert(accList[2].Member_Id__c.endsWith('1890'));*/
      }
    }
}