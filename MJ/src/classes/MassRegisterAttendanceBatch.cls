/*Author:leo.bi@celnet.com.cn
 *Date:2014-5-9
 *function: The Batch input register attendance
 *Apply To:CN
 */
global class MassRegisterAttendanceBatch implements DataBase.Batchable<SObject>,Schedulable
{
	//query string
	private final string query;
	Id contactRecordTypeId = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
	Id newLeadRecordTypeId = [select id from RecordType where DeveloperName=:'CN_Leads' and SobjectType=:'Lead'].id;
	//private final Id adminId = [Select UserRole.Name,UserRoleId, Id From User where UserRole.Name = 'CN CRM' order by Name][0].Id;
	
	global void execute(SchedulableContext sc)
	{
		MassRegisterAttendanceBatch mrab = new MassRegisterAttendanceBatch();
		DataBase.executebatch(mrab,50);
	}
	
	public MassRegisterAttendanceBatch()
	{
		query = 'Select Special_Product__c, Purchase_Date__c, Using_Product__c, Campaign__r.Standard_City__c, Remark__c, Phone__c, Name, Mother_Name__c, Match_Status__c, Match_Result__c, Lead__c, Is_Pregnancy__c, Invitation_Channel__c, Id, Contact__c, Campaign__c, Baby_Birthday__c From Attendance__c where Match_Status__c = null';
	}
	
	public MassRegisterAttendanceBatch(Id campaignId)
	{
		query = 'Select Special_Product__c, Purchase_Date__c, Using_Product__c, Remark__c, Phone__c, Name, Mother_Name__c, Match_Status__c, Campaign__r.Standard_City__c, Match_Result__c, Lead__c, Is_Pregnancy__c, Invitation_Channel__c, Id, Contact__c, Campaign__c, Baby_Birthday__c From Attendance__c where Match_Status__c = null and Campaign__c=\'' + campaignId + '\'';
	}
	
	global DataBase.Querylocator start(DataBase.BatchableContext BC)
	{
		return DataBase.getQueryLocator(query);
	}
	
	global void execute(DataBase.BatchableContext BC,List<SObject> scope)
	{
		//all attendance list
		List<Attendance__c> list_Attendance = new List<Attendance__c>();
		//Invited member list from CampaignMember
		List<Attendance__c> list_Attendance_InvitedMember = new List<Attendance__c>();
		//Noninvited member list from Contact
		List<Attendance__c> list_Attendance_UninvitedMember = new List<Attendance__c>();
		//lead list
		List<Attendance__c> list_Attendance_Lead = new List<Attendance__c>();
		//non lead list
		List<Attendance__c> list_Attendance_Nonlead = new List<Attendance__c>();
		//attendance phone set
		Set<String> set_Phone = new Set<String>();
		//attendance campaign set
		Set<Id> set_Campaign = new Set<Id>();
		//DML list
		List<CampaignMember> list_Update_CampaignMember = new List<CampaignMember>();
		List<CampaignMember> list_Insert_CampaignMember_FromMember = new List<CampaignMember>();
		List<CampaignMember> list_Insert_CampaignMember_FromLead = new List<CampaignMember>();
		List<CampaignMember> list_Insert_CampaignMember_FromNonLead = new List<CampaignMember>();
		List<Lead> list_Insert_Lead = new List<Lead>();
		List<Lead> list_Update_Lead = new List<Lead>();
		
		//update purchase date and special product
		List<Attendance__c> list_SpecialAtt = new List<Attendance__c>();
		Map<Id,Attendance__c> map_Contact_Attendance = new Map<Id,Attendance__c>();
		//1.get all attendance
		for(SObject s : scope)
		{
			Attendance__c att = (Attendance__c)s;
			if(att.Phone__c == null || att.Phone__c == '')
			{
				continue;
			}
			set_Phone.add(att.Phone__c);
			set_Campaign.add(att.Campaign__c);
			list_Attendance.add(att);
		}
		if(list_Attendance.size() == 0)
		{
			return;
		}
		system.debug('---------------------set_Phone------------------------' + set_Phone);
		//2 get CampaignMember、Contact、Lead
		//campaignmember
		List<CampaignMember> list_CampaignMember = [select Id, CampaignId, Contact.Id, Contact.Account.Phone , Contact.Account.Duplicate_Status__c, Contact.Account.Mobile_Phone__c, Contact.Account.Other_Phone_2__c, Contact.Account.Other_Phone_1__c
								    			    from CampaignMember where CampaignId in :set_Campaign 
								    			    and Campaign.IsActive =true
								                    and (Contact.Account.Phone in :set_Phone 
								                    or Contact.Account.Mobile_Phone__c in :set_Phone
								                    or Contact.Account.Other_Phone_1__c in : set_Phone
								                    or Contact.Account.Other_Phone_2__c in : set_Phone)];
		system.debug('---------------------list_CampaignMember------------------------' + list_CampaignMember);	 							                    
		//contact							
		List<Contact> list_Contact = [select Id, Account.Phone, Account.Mobile_Phone__c,Account.Duplicate_Status__c, Account.Other_Phone_1__c, Account.Other_Phone_2__c
									  from Contact 
									  where RecordTypeId = :contactRecordTypeId and (Account.Phone in :set_Phone 
									  or Account.Mobile_Phone__c in :set_Phone
									  or Account.Other_Phone_1__c in :set_Phone
									  or Account.Other_Phone_2__c in :set_Phone)];
		system.debug('---------------------list_Contact------------------------' + list_Contact);
		//lead						              
		List<Lead> list_Lead = [Select Id, Is_From_Attendance__c, Phone 
								From Lead 
								where RecordTypeId = :newLeadRecordTypeId 
								and Phone in :set_Phone];
		system.debug('---------------------list_Lead------------------------' + list_Lead);
		//3.assign attendance to every list,first to list_Attendance_InvitedMember,second to list_Attendance_UninvitedMember
		// third to list_Attendance_Lead, finally to list_Attendance_Nonlead
		Set<Id> campaignMember_ID = new Set<Id>();//去掉命中的重复的CampaignMember
		Map<Id,Id> attId_LeadId = new Map<Id,Id>();//用于处理lead插入campaignmember失败，取消更新lead
		for(Attendance__c att : list_Attendance)
		{
			//3.1 Invited member		    	  
			if(list_CampaignMember.size() != 0)
			{
				Boolean matchCM = false;
				for(CampaignMember cm : list_CampaignMember)
				{
					if(cm.Contact.Account.Duplicate_Status__c != null || campaignMember_ID.contains(cm.Id))
					{
						continue;
					}
					if(att.Campaign__c == cm.CampaignId && (att.Phone__c == cm.Contact.Account.Phone 
					|| att.Phone__c == cm.Contact.Account.Mobile_Phone__c
					|| att.Phone__c == cm.Contact.Account.Other_Phone_1__c
					|| att.Phone__c == cm.Contact.Account.Other_Phone_2__c))
					{
						att.Contact__c = cm.Contact.Id;
						att.Match_Result__c = 'Invited Member';
						list_Attendance_InvitedMember.add(att);
						//update CampaignMember status
						if(att.Purchase_Date__c != null || att.Special_Product__c != null)
						{
							cm.Call_Status__c = 'Invitation Special';
						    cm.Status = 'Sent';
							map_Contact_Attendance.put(cm.Contact.Id,att);
						}
						else
						{
							cm.Call_Status__c = 'Waiting Reciprocal Call';
							cm.Status = 'Attended and waiting for back-visit';						
						}
						list_Update_CampaignMember.add(cm);
						campaignMember_ID.add(cm.Id);
						matchCM = true;
						break;
					}
				}
				if(matchCM)
				{
					continue;
				}
			}
			//2.2 Uninvited member
		    if(list_Contact.size() != 0)
		    {
		    	//if one attendance has two contact, it is a wrong record or one account has two baby ,choose the first one
		    	Boolean matchCon = false;
		    	for(Contact con : list_Contact)
		    	{
		    		if(con.Account.Duplicate_Status__c != null)
					{
						continue;
					}
		    		if(att.Phone__c == con.Account.Phone || att.Phone__c == con.Account.Mobile_Phone__c
		    		|| att.Phone__c == con.Account.Other_Phone_1__c || att.Phone__c == con.Account.Other_Phone_2__c)
		    		{
				    	att.Contact__c = con.Id;
				    	att.Match_Result__c = 'Uninvited Member';
				    	list_Attendance_UninvitedMember.add(att);
				    	//add insert campaignmember list
				    	CampaignMember cm = new CampaignMember();
				    	cm.ContactId = con.Id;
				    	cm.CampaignId = att.Campaign__c;
				    	if(att.Purchase_Date__c != null || att.Special_Product__c != null)
						{
							cm.Call_Status__c = 'Invitation Special';
						    cm.Status = 'Sent';
							map_Contact_Attendance.put(con.Id,att);
						}
						else
						{
							cm.Call_Status__c = 'Waiting Reciprocal Call';
				    		cm.Status = 'Attended and waiting for back-visit';
						}
				    	list_Insert_CampaignMember_FromMember.add(cm);
				    	matchCon = true;
				    	break;
		    		}
		    	}
		    	if(matchCon)
		    	{
		    		continue;
		    	}
		    }
			//2.3 Lead
			if(list_Lead.size() != 0)
			{
				Boolean matchLead = false;
				for(Lead lea : list_Lead)
				{
					if(lea.Phone == att.Phone__c)
					{
						attId_LeadId.put(att.Id,lea.Id);
						att.Lead__c = lea.Id;
						att.Match_Result__c = 'Lead';
						list_Attendance_Lead.add(att);
						CampaignMember cm = new CampaignMember();
						cm.LeadId = lea.Id;
						cm.CampaignId = att.Campaign__c;
						if(att.Purchase_Date__c != null || att.Special_Product__c != null)
						{
							cm.Call_Status__c = 'Invitation Special';
						    cm.Status = 'Sent';
						    lea.Purchase_Date__c = att.Purchase_Date__c;
						    lea.Special_Product__c = att.Special_Product__c;
						}
						else
						{
							cm.Call_Status__c = 'Waiting Reciprocal Call';
							cm.Status = 'Attended and waiting for back-visit';
						}
						list_Insert_CampaignMember_FromLead.add(cm); 
						matchLead = true;
						//flag the lead
						lea.Is_From_Attendance__c = true;
						list_Update_Lead.add(lea);
						break;
					}
				}
				if(matchLead)
				{
					continue;
				}
			}
			//2.4 non lead
			att.Match_Result__c = 'Non Lead';
			att.Match_Status__c = 'Success';//this value may be changed by the latter code
			list_Attendance_Nonlead.add(att);
			Lead newLead = new Lead();
			newLead.RecordTypeId = newLeadRecordTypeId;
			newLead.LastName = att.Mother_Name__c;
			newLead.Birthday_1st_Child__c = att.Baby_Birthday__c;
			newLead.Phone = att.Phone__c;
			newLead.LeadSource = 'Campaign Participant';
			newLead.Administrative_area__c = att.Campaign__r.Standard_City__c;
			newLead.Is_From_Attendance__c = true;
			//newLead.OwnerId = adminId; 
			CampaignMember cm =new CampaignMember();
			cm.CampaignId = att.Campaign__c;
			cm.Invitation_Channel__c = att.Invitation_Channel__c;
			cm.Is_Pregnancy__c = att.Is_Pregnancy__c;
			cm.Remark__c = att.Remark__c;
			cm.Using_Product__c = att.Using_Product__c;
			if(att.Purchase_Date__c != null || att.Special_Product__c != null)
			{
				cm.Call_Status__c = 'Invitation Special';
			    cm.Status = 'Sent';
			    newLead.Purchase_Date__c = att.Purchase_Date__c;
			    newLead.Special_Product__c = att.Special_Product__c;
			}
			else
			{
				cm.Call_Status__c = 'Waiting Reciprocal Call';
				cm.Status = 'Attended and waiting for back-visit';
			}
			list_Insert_Lead.add(newLead);
			list_Insert_CampaignMember_FromNonLead.add(cm);
		}
		//3 update invited member,first update campaignmember, second update attendance
		//3.1 update invited member's campaignmember
		if(list_Update_CampaignMember.size() != 0)
		{
			CampaignMember[] cms = list_Update_CampaignMember;
			DataBase.Saveresult[] srList = DataBase.update(cms,false);
			Integer i = 0; 
			for(DataBase.Saveresult sr : srList)
			{
				 if (sr.isSuccess())
				 {
				 	//If succeed, the attendance match status is success
				 	list_Attendance_InvitedMember[i].Match_Status__c = 'Success';
				 }
				 else
				 {
				 	//If failed, the attendance match statrs is failed
				 	list_Attendance_InvitedMember[i].Match_Status__c = 'Failed';
				 	String failReason = '';
				 	for(Database.Error err : sr.getErrors())
			        {                  
			            //add error code
			            String statusCode = String.valueOf(err.getStatusCode());
			            String message = String.valueOf(err.getMessage());
			            failReason = 'statusCode:' + statusCode +'\n' + 'message:' + message;
			        }
			        list_Attendance_InvitedMember[i].Fail_Reason__c = failReason;
				 }
				 i++;
			}
		}
		//3.2 update list_Attendance_InvitedMember
		if(list_Attendance_InvitedMember.size() != 0)
		{
			try
			{
				update list_Attendance_InvitedMember;
			}
			catch(Exception e)
			{
				system.debug('Exception***********list_Attendance_InvitedMember***************' + e);
			}
		}
		//4 insert uninvited member. First, insert campaignmember. Second, update attendance
		//4.1 insert uninvited membert to campaignmember
		if(list_Insert_CampaignMember_FromMember.size() != 0)
		{
			Integer i = 0;
			CampaignMember[] cms = list_Insert_CampaignMember_FromMember;
			DataBase.Saveresult[] srList = DataBase.insert(cms,false);
			for(DataBase.Saveresult sr : srList)
			{
				 if (sr.isSuccess())
				 {
				 	//If succeed, the attendance match status is success
				 	list_Attendance_UninvitedMember[i].Match_Status__c = 'Success';
				 }
				 else
				 {
				 	//If failed, the attendance match statrs is failed
				 	list_Attendance_UninvitedMember[i].Match_Status__c = 'Failed';
				 	String failReason = '';
				 	for(Database.Error err : sr.getErrors())
			        {                  
			            //add error code
			            String statusCode = String.valueOf(err.getStatusCode());
			            String message = String.valueOf(err.getMessage());
			            failReason = 'statusCode:' + statusCode +'\n' + 'message:' + message;
			        }
			        list_Attendance_UninvitedMember[i].Fail_Reason__c = failReason;
				 }
				 i++;
			}
		}
		//4.2 update attendance
		if(list_Attendance_UninvitedMember.size() != 0)
		{
			try
			{
				update list_Attendance_UninvitedMember;
			}
			catch(Exception e)
			{
				system.debug('Exception***********list_Attendance_UninvitedMember***************' + e);
			}
		}
		/*Author:leo.bi@celnet.com.cn
		 *Alter Date:2014-06-09
		 *Content: synchronize Special Product and Purchase Date to Contact
		 */
		if(map_Contact_Attendance.size() != 0)
		{
			List<Contact> list_synchronize = [Select Special_Product__c, Purchase_Date__c, Id From Contact where Id in :map_Contact_Attendance.keySet()];
			for(Contact con : list_synchronize)
			{
				Attendance__c att = map_Contact_Attendance.get(con.Id);
				con.Special_Product__c = att.Special_Product__c;
				con.Purchase_Date__c = att.Purchase_Date__c;
			}
			update list_synchronize;
		}
		//5 insert lead. First, insert lead to campaignmeber. Second, update lead attendance
		//5.1 insert lead to campaignmeber
		if(list_Insert_CampaignMember_FromLead.size() != 0)
		{
			Integer i = 0;
			CampaignMember[] cms = list_Insert_CampaignMember_FromLead;
			DataBase.Saveresult[] srList = DataBase.insert(cms,false);
			for(DataBase.Saveresult sr : srList)
			{
				 if (sr.isSuccess())
				 {
				 	//If succeed, the attendance match status is success
				 	list_Attendance_Lead[i].Match_Status__c = 'Success';
				 }
				 else
				 {
				 	//If failed, the attendance match statrs is failed
				 	list_Attendance_Lead[i].Match_Status__c = 'Failed';
				 	String failReason = '';
				 	for(Database.Error err : sr.getErrors())
			        {                  
			            //add error code
			            String statusCode = String.valueOf(err.getStatusCode());
			            String message = String.valueOf(err.getMessage());
			            failReason = 'statusCode:' + statusCode +'\n' + 'message:' + message;
			        }
			        list_Attendance_Lead[i].Fail_Reason__c = failReason;
			        List<Lead> list_Temp = new List<Lead>();
			        for(Lead lea : list_Update_Lead)
			        {
			        	if(attId_LeadId.get(list_Attendance_Lead[i].Id) != null && attId_LeadId.get(list_Attendance_Lead[i].Id) == lea.Id) continue;
				        list_Temp.add(lea);
			        }
			        list_Update_Lead.clear();
			        list_Update_Lead.addAll(list_Temp);
				 }
				 i++;
			}
		}
		//5.2 update lead attendance
		if(list_Attendance_Lead.size() != 0)
		{
			try
			{
				update list_Attendance_Lead;
			}
			catch(Exception e)
			{
				system.debug('Exception***********list_Attendance_Lead***************' + e);
			}
		}
		if(list_Update_Lead.size() > 0)
		{
			try
			{
				update list_Update_Lead;
			}
			catch(Exception e)
			{
				system.debug('Exception***********list_Update_Lead***************' + e);
			}
		}
		//6 insert nonlead. First, insert new lead. Second, insert new lead to campainmeber. Third, update attendance
		//6.1 insert lead
		if(list_Insert_Lead.size() != 0)
		{
			Lead[] leas = list_Insert_Lead;
			DataBase.Saveresult[] srList = DataBase.insert(leas, false);
			Integer i = 0;
			for(DataBase.Saveresult sr : srList)
			{
				 if (!sr.isSuccess())
				 {
				 	system.debug('----------------insert nonlead fail--------------');
				 	//if new lead insert opration failed, continue inserting campaignmember,let system generte error
				 	//list_Insert_CampaignMember_FromNonLead.remove(i);
				 	list_Attendance_Nonlead[i].Match_Status__c = 'Failed';
				 	String failReason = '';
				 	for(Database.Error err : sr.getErrors())
			        {                  
			            //add error code
			            String statusCode = String.valueOf(err.getStatusCode());
			            String message = String.valueOf(err.getMessage());
			            system.debug('-------------------statusCode------------------------' + statusCode);
			            system.debug('-------------------message------------------------' + message);
			            failReason = 'InsertLeadFailed:' + '\n' + 'statusCode:' + statusCode +'\n' + 'message:' + message;
			        }
			        list_Attendance_Nonlead[i].Fail_Reason__c = failReason;
				 	i--;
				 }
				 else
				 {
				 	system.debug('----------------insert nonlead success--------------');
				 	list_Insert_CampaignMember_FromNonLead[i].LeadId = list_Insert_Lead[i].Id;
				 	list_Attendance_Nonlead[i].Lead__c = list_Insert_Lead[i].Id;
				 }
				 i++;
			}
		}
		//6.2 insert campaign member
		if(list_Insert_CampaignMember_FromNonLead.size() != 0)
		{
			CampaignMember[] cms = list_Insert_CampaignMember_FromNonLead;
			DataBase.Saveresult[] srList = DataBase.insert(cms, false);
			Integer i = 0;
			for(DataBase.Saveresult sr : srList)
			{
				 if (sr.isSuccess())
				 {
				 	//If succeed, the attendance match status is success
				 	list_Attendance_Nonlead[i].Match_Status__c = 'Success';
				 }
				 else
				 {
				 	//If failed, the attendance match statrs is failed
				 	list_Attendance_Nonlead[i].Match_Status__c = 'Failed';
				 	String failReason = '';
				 	for(Database.Error err : sr.getErrors())
			        {                  
			            //add error code
			            String statusCode = String.valueOf(err.getStatusCode());
			            String message = String.valueOf(err.getMessage());
			            failReason = 'InsertCampaignMemberFailed:' + '\n' + 'statusCode:' + statusCode +'\n' + 'message:' + message;
			        }
			        list_Attendance_Nonlead[i].Fail_Reason__c = failReason;
				 }
				 i++;
			}
		}
		//6.3 update nonlead attendance and rest attendance in list_Attendance (Non lead which failed to insert a new lead)
		if(list_Attendance_Nonlead.size() != 0)
		{
			try
			{ 
				update list_Attendance_Nonlead;
			}
			catch(Exception e)
			{
				system.debug('Exception***********list_Attendance_Nonlead***************' + e);
			}
		}
	}
	
	global void finish(DataBase.BatchableContext BC)
	{
		system.debug('*******************finish***********************');
	}
}