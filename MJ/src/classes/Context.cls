/*
 Author tommyliu@celnet.com.cn
 Created on: 2014-4-16
 Function: Common context
 Apply To: ALL
 */   
public class Context 
{
    //Function: Test is curent user is belong to specified National by NCode
    //Optional National Code: HK, CN, ALL
    //Sample: 
    //if(Context.ApplyTo(new String[] {'CN, HK'})) 
    //{ 
    //  //Do somthing for CN and HK context user...
    //}
    public static Boolean ApplyTo(List<String> nCodes)
    {
    	if(nCodes == null || nCodes.size() == 0)
    	{
    		return false;
    	}
		ID roleId = UserInfo.getUserRoleId();//uses that without role, apply to all National
		if(roleId == null)
		{
			return true;
		}
		UserRole role = [Select ID, Name, DeveloperName From UserRole Where ID =: roleId];
		for(String nCode : nCodes)
		{
			if(nCode == 'ALL')
			{
				return true;
			}
			if(role.DeveloperName.startsWith(nCode))
			{
				return true;
			}
		}
		return false;
    }
    public static Boolean ApplyTo(String nCode)
    {
		return ApplyTo(new String[] {nCode});
    }
}