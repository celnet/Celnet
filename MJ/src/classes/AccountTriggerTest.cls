@isTest
private class AccountTriggerTest {

    static testMethod void accountTriggerTest() {
        //Create custom settings
        Member_ID_Setting__c s = new Member_ID_Setting__c();
        s.Name = String.valueOf(Date.today().year());
        s.Next_ID__c = 1888;
        insert s;
        
        //Create test household
        Account acc = new Account();
        acc.RecordTypeId =  ObjectUtil.GetRecordTypesByDevName(Account.SObjectType).get(CommonHelper.HOUSEHOLD_RT).Id;
        acc.Name = 'PRIMARY PARENT';
        acc.Last_Name__c = 'PRIMARY';
        acc.First_Name__c = 'PARENT';
        insert acc;
        Account a = [SELECT Member_ID__c,First_Name__c from Account where id =:acc.id];
        System.debug(a);
        
        //assert Member ID generated
        system.assert(a.Member_ID__c != null);
        
        //assert pri contact auto created
        Contact c = [Select id, FirstName,Is_Primary_Contact__c From contact where contact.account.id =: a.id];
        system.assertEquals(c.FirstName,'PARENT');
        system.assertEquals(c.Is_Primary_Contact__c,true);
        
        //Exceptions not tested here (Missing Custom setting and Invalid pri contact data on account)
    }
    
    static testMethod void syncCanBeCalledTest() {
        //Create custom settings
        Member_ID_Setting__c s = new Member_ID_Setting__c();
        s.Name = String.valueOf(Date.today().year());
        s.Next_ID__c = 1888;
        insert s;
        
        //Create test household
        Account acc = new Account();
        acc.RecordTypeId =  ObjectUtil.GetRecordTypesByDevName(Account.SObjectType).get(CommonHelper.HOUSEHOLD_RT).Id;
        acc.Name = 'PRIMARY PARENT';
        acc.Last_Name__c = 'PRIMARY';
        acc.First_Name__c = 'PARENT';
        
        insert acc;
        //Primary Contact should be created
      
        //Lets create another contact and baby
        ID parentRT = ObjectUtil.GetRecordTypeID(Contact.SObjectType, CommonHelper.HOUSEHOLD_PARENT_RT);
        ID childRT = ObjectUtil.GetRecordTypeID(Contact.SObjectType, CommonHelper.HOUSEHOLD_CHILD_RT);
        Contact parent = new Contact(LastName='Parent',AccountId=acc.id, RecordTypeId = parentRT);
        insert parent;
        Contact baby = new Contact(LastName='Child',AccountId=acc.id, RecordTypeId = childRT);
        insert baby;
        
        acc = [SELECT Home_Phone__c ,Mobile_Phone__c ,Other_Phone_1__c ,Can_be_called__c From Account];
        Contact[] contacts = [SELECT Account_can_be_called__c From Contact];
        system.debug(acc);
        //Assert 3 contacts (primary , parent, baby);
        system.assertEquals(3, contacts.size());
        //Assert Can be called
        system.assertEquals(false,acc.Can_be_called__c);   
        //system.assertEquals(false,contacts[0].Account_can_be_called__c); //tommy deleted
        //system.assertEquals(false,contacts[1].Account_can_be_called__c);  //tommy deleted
        //system.assertEquals(false,contacts[2].Account_can_be_called__c);  //tommy deleted
        //Set Can be called criteria to make formula field true
        acc.Home_Phone__c = '12344567';
        update acc;
        acc = [SELECT Can_be_called__c From Account];     
        contacts = [SELECT Account_can_be_called__c From Contact];
        //Assert Can be called
        system.assertEquals(true,acc.Can_be_called__c); 
        system.assertEquals(true,contacts[0].Account_can_be_called__c); 
        system.assertEquals(true,contacts[1].Account_can_be_called__c); 
        system.assertEquals(true,contacts[2].Account_can_be_called__c); 
    }
}