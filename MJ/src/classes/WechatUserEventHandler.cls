/*Author:leo.bi@celnet.com.cn   
 *Date:2014-09-23
 */
public class WechatUserEventHandler implements Triggers.Handler
{
    public void Handle()
    {
        if(!Context.ApplyTo(new String[] {'HK'}))
        {
            return;
        }
        if(trigger.isBefore &&  trigger.isUpdate)
        {
            if(trigger.new[0] instanceof Wechat_User__c)
            {
                this.autoSetMemberBindingInfo();
            }
        }
        if(trigger.isAfter &&  trigger.isUpdate)
        {
            if(trigger.new[0] instanceof Lead)
            {
                this.bindMemberToWechatUser();
            }
        }
    }
    
    private void autoSetMemberBindingInfo()
    {
        List<Wechat_User__c> list_New_WUser = trigger.new;
        for(Wechat_User__c wu : list_New_WUser)
        {
            Wechat_User__c old = (Wechat_User__c)trigger.oldmap.get(wu.id);
            if(wu.Binding_Member__c != old.Binding_Member__c)
            {
                if(wu.Binding_Member__c != null)
                {
                    wu.Binding_Status__c = 'Member';
                    wu.Member_Binding__c = true;
                    wu.Member_Binding_Time__c = datetime.now();
                }
                else
                {
                    wu.Binding_Status__c = null;
                    wu.Member_Binding__c = false;
                    wu.Member_Binding_Time__c = null;
                }
            }
            else if(wu.Binding_Non_Member__c != old.Binding_Non_Member__c)
            {
            	
                if(wu.Binding_Non_Member__c != null)
                {
                	wu.Non_Member_Binding__c = true;
                    wu.Non_Member_Binding_Time__c = datetime.now();
                	if(wu.Binding_Member__c != null)
	            	{
	            		continue;
	            	}
                   	wu.Binding_Status__c = 'Non-Member';
                }
                else
                {
                    wu.Binding_Status__c = null;
                    wu.Non_Member_Binding__c = false;
                    wu.Non_Member_Binding_Time__c = null;
                }
                
            }
        }
    }
    
    /*Function:After lead convert, bind converted member to WechatUser*/
    private void bindMemberToWechatUser()
    {
        List<Lead> list_Lead = trigger.new;
        Map<Id,Id> map_LeadId_AccountId = new Map<Id,Id>();
        for(Lead convertedLead : list_Lead)
        {
            Lead oldLead = (Lead)trigger.oldMap.get(convertedLead.Id);
            if(oldLead.IsConverted == false && convertedLead.IsConverted == true)
            {
                map_LeadId_AccountId.put(convertedLead.Id,convertedLead.ConvertedAccountId);
            }
        }
        if(map_LeadId_AccountId.isEmpty()) return;
        List<Wechat_User__c> list_WechatUser = [select Id,Binding_Non_Member__c
                                                from Wechat_User__c 
                                                where Binding_Non_Member__c in :map_LeadId_AccountId.keySet()
                                                and Binding_Member__c = null];
        if(list_WechatUser.isEmpty()) return;
        for(Wechat_User__c wu : list_WechatUser)                                                    
        {
            wu.Binding_Member__c = map_LeadId_AccountId.get(wu.Binding_Non_Member__c);
        }
        update list_WechatUser;
    }
}