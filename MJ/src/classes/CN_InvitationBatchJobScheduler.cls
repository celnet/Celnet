/*Author:leo.bi@celnet.com.cn
 *Date:2014-06-25
 *Function:the job Scheduler of invitation job
 */
global class CN_InvitationBatchJobScheduler implements Schedulable
{
	private Integer limitCount;
	public CN_InvitationBatchJobScheduler(Integer limitCount)
	{
		this.limitCount = limitCount;
	}
	public void execute(SchedulableContext sc)
	{
		CN_InvitationBatchJobManager bm = new CN_InvitationBatchJobManager(this.limitCount);
		bm.execute();
	}
}