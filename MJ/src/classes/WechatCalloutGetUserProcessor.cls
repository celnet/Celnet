/*
*Author:michael.mi@celnet.com.cn
*Date:2014-09-05
*Function:用户关注公众账号后，调用此Processor来call微信接口获取关注用户的详细信息
*/
public class WechatCalloutGetUserProcessor extends WechatCalloutProcessor
{
	public override void DoCallout(Wechat_Callout_Task_Queue__c WechatTask)
	{
		WechatCalloutService wcs = new WechatCalloutService(WechatTask.Public_Account_Name__c);
		WechatEntity.User user = wcs.getUserInfo(WechatTask.Open_ID__c);
		Wechat_User__c sfUser = WechatFieldMatch.GenerateWechatUser(user);
		ObjectList.add(sfUser);
	}
	 
	public override void FinishData()
	{
		if(this.ObjectList != null && ObjectList.size() >0)
		{
			map<string,Wechat_User__c> weUserMap = new map<string,Wechat_User__c>();
			for(sobject sobj : ObjectList)
			{
				Wechat_User__c wu = (Wechat_User__c)sobj;
				weUserMap.put(wu.Open_Id__c,wu);
			}
			upsert weUserMap.values() Open_Id__c;
		}
	}
}