/*Author:Leo
 *Date:2014-7-11
 *function: test LeadEventHandler
 */
@isTest
private class LeadEventHandlerTest 
{

    static testMethod void myUnitTest() 
    {
    	
    	Campaign c = new Campaign();
        c.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Invitation' and SobjectType=:'Campaign'].id;
        c.Name = 'Test Campaign';
        c.Description = 'Test Description';
        c.Campaign_Member_Limit__c = 10000;
        c.Campaign_Type__c = 'Invitation';
        insert c;
        
        Id newLeadRecordTypeId = [select id from RecordType where DeveloperName=:'CN_Leads' and SobjectType=:'Lead'].id;
        Lead lea = new Lead();
        lea.LastName = String.valueOf(Date.today()) + 'newlead';
        lea.RecordTypeId = newLeadRecordTypeId;
        insert lea;
        
        CampaignMember cm = new CampaignMember();
        cm.CampaignId = c.Id;
        cm.LeadId = lea.Id;
        cm.Call_Status__c = 'Waiting for Invitation';
        insert cm;
        cm.Call_Status__c = 'Finish Call';
        update cm;
        //DispatchQueue
        List<Address_Management__c> list_AM = new List<Address_Management__c>();
        Address_Management__c am1 = createAddress('华东'); 
        Address_Management__c am2 = createAddress('华西');
        Address_Management__c am3 = createAddress('华南');
        Address_Management__c am4 = createAddress('华北');
        Address_Management__c am5 = createAddress('华中');
        list_AM.add(am1);
        list_AM.add(am2);
        list_AM.add(am3);
        list_AM.add(am4);
        insert list_AM;
        /*
        Group gr1 = new Group();
        gr1.Type = 'Queue';
        gr1.Name = 'gr1';
        gr1.DeveloperName = 'CN_OB_Campaign_Follow_Up_Call_EC';
        insert gr1;
        Group gr2 = new Group();
        gr2.Type = 'Queue';
        gr2.Name = 'gr2';
        gr2.DeveloperName = 'CN_OB_Campaign_Follow_Up_Call_SC';
        insert gr2;
        Group gr3 = new Group();
        gr3.Type = 'Queue';
        gr3.Name = 'gr3';
        gr3.DeveloperName = 'CN_OB_Campaign_Follow_Up_Call_WC';
        insert gr3;
        Group gr4 = new Group();
        gr4.Type = 'Queue';
        gr4.Name = 'gr4';
        gr4.DeveloperName = 'CN_OB_Campaign_Follow_Up_Call_NC';
        insert gr4;
        Group gr5 = new Group();
        gr5.Name = 'gr5';
        gr5.Type = 'Queue';
        gr5.DeveloperName = 'CN_OB_Undefined';
        insert gr5;*/
        Lead lea1 = new Lead();
        lea1.LastName = String.valueOf(Date.today()) + 'newlead1';
        lea1.RecordTypeId = newLeadRecordTypeId;
        lea1.Is_From_Attendance__c = true;
        lea1.Administrative_area__c = am1.Id;
        insert lea1;
        Lead lea2 = new Lead();
        lea2.LastName = String.valueOf(Date.today()) + 'newlead2';
        lea2.RecordTypeId = newLeadRecordTypeId;
        lea2.Is_From_Attendance__c = true;
        lea2.Administrative_area__c = am2.Id;
        insert lea2;
        Lead lea3 = new Lead();
        lea3.LastName = String.valueOf(Date.today()) + 'newlead3'; 
        lea3.RecordTypeId = newLeadRecordTypeId;
        lea3.Is_From_Attendance__c = true;
        lea3.Administrative_area__c = am3.Id;
        insert lea3;
        Lead lea4 = new Lead();
        lea4.LastName = String.valueOf(Date.today()) + 'newlead4';
        lea4.RecordTypeId = newLeadRecordTypeId;
        lea4.Is_From_Attendance__c = true;
        lea4.Administrative_area__c = am4.Id;
        insert lea4;
        Lead lea5 = new Lead();
        lea5.LastName = String.valueOf(Date.today()) + 'newlead5';
        lea5.RecordTypeId = newLeadRecordTypeId;
        lea5.Is_From_Attendance__c = true;
        lea5.Administrative_area__c = am5.Id;
        insert lea5;
    }
    
    private static Address_Management__c createAddress(String territory)
	{
		Address_Management__c am = new Address_Management__c();
		am.Region__c = territory;
        am.Sub_Region__c = territory;
        am.Area__c =territory;
        am.Sales_City__c = territory;
        am.Name = territory;
        return am;
	}
}