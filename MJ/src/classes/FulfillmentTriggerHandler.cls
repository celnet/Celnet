/*
On before Insert:
	Create Coupons if needed
On before Update:
	Handle different Coupon type change scenarios
	Cancel Coupons if needed
*/

public with sharing class FulfillmentTriggerHandler {
	
	private boolean m_isExecuting = false;
	private integer BatchSize = 0;
	
	private static string MULTIPLE_COUPON_ERROR = 'Only Allow choose one coupon type for each fulfillment.';
    
	public FulfillmentTriggerHandler(boolean isExecuting, integer size){
		m_isExecuting = isExecuting;
		BatchSize = size;
	}
	public void OnBeforeInsert(Fulfillment__c[] newFulfillments){
        createCoupons(newFulfillments);
    }
    public void OnBeforeUpdate(Map<ID, Fulfillment__c> oldMapFulfillments, Fulfillment__c[] newFulfillments){
    	couponTypeChange(oldMapFulfillments,newFulfillments);
    }
    
    /** End Trigger Event Methods
		Start Helper Methods **/
    
    
	private void createCoupons(Fulfillment__c[] newFulfillments){
		
		Map<ID,ID> contactAccountMap = getContactAccountIDMap(newFulfillments);
		
		// Bulkify, group by material name, values = list of fulfillments.
		Map<String, List<Fulfillment__c>> fulfillmentsByCpnType = new Map<String,List<Fulfillment__c>>();
		for (Fulfillment__c record : newFulfillments) 
		{
             if(record.Cancelled__c){
             	continue;
             }
			 if(record.Type__c == 'Coupon' && record.Material_Name__c != NULL)
			 {
			 	//Skip procesing if more than one coupon selected.
			 	if (record.Material_Name__c.split(';').size()>1){
			 		record.Material_Name__c.addError(MULTIPLE_COUPON_ERROR);
			 		continue;
			 	}
				List<Fulfillment__c> fulfilmentList = fulfillmentsByCpnType.get(record.Material_Name__c);
				if (fulfilmentList==null){
					fulfilmentList = new List<Fulfillment__c>();
                    fulfillmentsByCpnType.put(record.Material_Name__c, fulfilmentList);
                }
                fulfilmentList.add(record);
			 }
		}
		
		//for each material name, bulk generate coupons then assign
		for (String couponType : fulfillmentsByCpnType.keySet()){
			List<Fulfillment__c> fulfilmentList = fulfillmentsByCpnType.get(couponType);
			List<Iterator<Coupon__c>> cpn = UniqueCodeGenerator.bulkGenerateFulfilmentCoupons(couponType, fulfilmentList, contactAccountMap);
			for (Fulfillment__c f : fulfilmentList){
                if (cpn!=null){
                    if (cpn.size() > 0 && cpn[0]!=null && cpn[0].hasNext())
                        f.Coupon_I__c = cpn[0].next().Id;
                    if (cpn.size() > 1 && cpn[1]!=null && cpn[1].hasNext())
                        f.Coupon_II__c = cpn[1].next().Id;
            	}        
            }
		}
		
	}
	/*
		Create Coupon - NOT bulk
		Cancel Coupons - Bulk
	*/
	private void couponTypeChange(Map<ID, Fulfillment__c> oldMapFulfillments, Fulfillment__c[] newFulfillments){
		Map<ID,ID> contactAccountMap = getContactAccountIDMap(newFulfillments);
		
		Map<ID,String> cancelID_Reasons = new Map<ID,String>();
            
        for (Fulfillment__c newRecord : newFulfillments) {
        	
            Fulfillment__c oldRecord = oldMapFulfillments.get(newRecord.ID);
            Id AccountId = contactAccountMap.get(newRecord.Contact__c);
            //Scneario 0: If Cancelled 
            if (newRecord.Cancelled__c){
                //Cancel exsting coupons
                if(oldRecord.Coupon_I__c!=NULL) 
                {
                    cancelID_Reasons.put(oldRecord.Coupon_I__c,CommonHelper.COUPON_CANCELLED);
                    newRecord.Coupon_I__c = NULL;
                }
                if(oldRecord.Coupon_II__c!=NULL)
                {
                    cancelID_Reasons.put(oldRecord.Coupon_II__c,CommonHelper.COUPON_CANCELLED);
                    newRecord.Coupon_II__c = NULL;
                }
            }else
            //Scenerio 1: Type from Non Coupon to Coupon
            if(oldRecord.Type__c != 'Coupon' && newRecord.Type__c == 'Coupon' && newRecord.Material_Name__c != NULL)
            {
            	//Skip processing if more than one coupon selected
                if (newRecord.Material_Name__c.split(';').size()>1){
                    newRecord.Material_Name__c.addError(MULTIPLE_COUPON_ERROR);
                    continue;
                }
                //Generate new coupons
                List<Coupon__c> coupons = UniqueCodeGenerator.generateCouponByType(newRecord.Material_Name__c,AccountId);
                if(!coupons.isEmpty())
                    newRecord.Coupon_I__c = coupons[0].ID;
                if(coupons.size()>1)
                    newRecord.Coupon_II__c = coupons[1].ID;
            }
            //Scenerio 2: Type from Coupon to Non Coupon
            else if(oldRecord.Type__c == 'Coupon' && newRecord.Type__c != 'Coupon')
            {
                //Cancel exsting coupons
                if(oldRecord.Coupon_I__c!=NULL) 
                {
                    cancelID_Reasons.put(oldRecord.Coupon_I__c,CommonHelper.COUPON_TYPE_CHANGED);
                    newRecord.Coupon_I__c = NULL;
                }
                if(oldRecord.Coupon_II__c!=NULL)
                {
                    cancelID_Reasons.put(oldRecord.Coupon_II__c,CommonHelper.COUPON_TYPE_CHANGED);
                    newRecord.Coupon_II__c = NULL;
                }
            }
            //Scenerio 3: Type as Coupon but Coupon Type Changed
            else if(oldRecord.Type__c == 'Coupon' && newRecord.Type__c == 'Coupon' 
                    && oldRecord.Material_Name__c != newRecord.Material_Name__c)
            {
                //Skip processing if more than one coupon selected
                if (newRecord.Material_Name__c.split(';').size()>1){
                    newRecord.Material_Name__c.addError(MULTIPLE_COUPON_ERROR);
                    continue;
                }
                //Cancel exsting coupons
                if(oldRecord.Coupon_I__c!=NULL) 
                {
                    cancelID_Reasons.put(oldRecord.Coupon_I__c,CommonHelper.COUPON_TYPE_CHANGED);
                    newRecord.Coupon_I__c = NULL;
                }
                if(oldRecord.Coupon_II__c!=NULL)
                {
                    cancelID_Reasons.put(oldRecord.Coupon_II__c,CommonHelper.COUPON_TYPE_CHANGED);
                    newRecord.Coupon_II__c = NULL;
                }
                
                //Generate new coupons
                List<Coupon__c> coupons = UniqueCodeGenerator.generateCouponByType(newRecord.Material_Name__c,AccountId);
                if(!coupons.isEmpty())
                    newRecord.Coupon_I__c = coupons[0].ID;
                if(coupons.size()>1)
                    newRecord.Coupon_II__c = coupons[1].ID;
            }
        }
        
        //Cancels coupons if list not empty. Calls future method.
        if (cancelID_Reasons.isEmpty() == false){
        	UniqueCodeGenerator.cancelCoupons(cancelID_Reasons);
        }
	
	}
	
	
	private Map<ID,ID> getContactAccountIDMap(Fulfillment__c[] newFulfillments){
		Map<ID,ID> contactAccountMap = new Map<ID,ID>();
		for (Fulfillment__c record : newFulfillments) 
		{
			 if(record.Type__c == 'Coupon' && record.Material_Name__c != NULL)
			 {
				contactAccountMap.put(record.Contact__c,null);
			 }
		}
		for (Contact c : [Select Account.Id from Contact WHERE id IN :contactAccountMap.keySet() ])
		{
			contactAccountMap.put(c.Id, c.AccountId);
		}
		return contactAccountMap;
	}

}