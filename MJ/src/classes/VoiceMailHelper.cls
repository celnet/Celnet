public with sharing class VoiceMailHelper {

    @future
    public static void createVoiceMailRecord(String  UCID, 
                                             String  voiceMailUrl, 
                                             String  callerPhoneNumber, 
                                             String  calledPhoneNumber,
                                             String  type,
                                             Integer duration,
                                             String  incomingCallCity) {
        Voice_Mail__c voiceMail = new Voice_Mail__c();
      
        voiceMail.Call_Object__c         = UCID;
        voiceMail.Voice_Mail_URL__c      = voiceMailUrl;
        voiceMail.Caller_Phone_Number__c = callerPhoneNumber;
        voiceMail.Called_Phone_Number__c = calledPhoneNumber;
        voiceMail.Type__c                = type;
        voiceMail.Duration__c            = duration;
        voicemail.Incoming_Call_City__c  = incomingCallCity;
                        
        List<Account> accounts = [SELECT Id 
                                  FROM   Account 
                                  WHERE  Phone           = :callerPhoneNumber
                                     OR  Mobile_Phone__c = :callerPhoneNumber
                                  LIMIT  1];
                                  
        if (!accounts.isEmpty())
            voiceMail.Account__c = accounts[0].Id;
        else {
            List<Lead> leads = [SELECT Id
                                FROM   Lead
                                WHERE  IsConverted = false
                                  AND  (Phone           = :callerPhoneNumber
                                        OR  MobilePhone = :callerPhoneNumber)
                                LIMIT  1];
            if (!leads.isEmpty())
                voiceMail.Lead__c = leads[0].Id;        
        }
        
        insert voiceMail;
    }


}