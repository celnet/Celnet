/**
	On Before update: (to do consider delete campaigns?)
		Update Campaign Member Status based on Campaign Status Change
		Assign New / Cancel coupons to all members under Campaign when campaign coupon type change.
		UPDATE MEMBERS DONE USING BATCH!
*/
public class CampaignTriggerHandler{
	private boolean m_isExecuting = false;
	private integer BatchSize = 0;
	
    
	public CampaignTriggerHandler(boolean isExecuting, integer size){
		m_isExecuting = isExecuting;
		BatchSize = size;
	}
		
	public void OnBeforeInsert(Campaign[] newCampaigns){
	}
	
	public void OnAfterInsert(Campaign[] newCampaigns){
	}
	
	@future public static void OnAfterInsertAsync(Set<ID> newCampaignIDs){
	}
	
	public void OnBeforeUpdate(Campaign[] oldCampaigns, Map<ID, Campaign> oldCampaignMap, Campaign[] updatedCampaigns, Map<ID, Campaign> CampaignMap){
        
       Map<Id,String> statusUpdateData = getUpdateMemberStatus(oldCampaignMap,updatedCampaigns);
       Map<Id,List<List<Coupon__c>>> couponUpdateData = getUpdateMemberCoupon(oldCampaignMap,updatedCampaigns);
       executeBatchUpdateMembers(statusUpdateData,couponUpdateData);
    }
	
	public void OnAfterUpdate(Campaign[] oldCampaigns, Campaign[] updatedCampaigns, Map<ID, Campaign> CampaignMap){
	}
	
	@future public static void OnAfterUpdateAsync(Set<ID> updatedCampaignIDs){
	}
	
	public boolean IsTriggerContext{
		get{ return m_isExecuting;}
	}
    
    /*
	Mass Update for Campaign Members Status on Campaign Status change.
	*/
    private Map<ID,String> getUpdateMemberStatus(Map<ID, Campaign> oldCampaignMap, Campaign[] updatedCampaigns){
        Map<ID,String> campaignId_memberStatus = new Map<Id,String>();
        //read mapping from Custom Settings
        List<Campaign_Member_Status_Mapping__c> settings = Campaign_Member_Status_Mapping__c.getAll().values();
        for (Campaign newCampaign : updatedCampaigns){
            Campaign oldCampaign = oldCampaignMap.get(newCampaign.ID);
            //enter only if there's change in status
            if (oldCampaign.Status != newCampaign.Status){
                //for each setting, find cm status based on c status, add to map
                for (Campaign_Member_Status_Mapping__c setting : settings){
                    if (setting.Campaign_Status__c == newCampaign.Status){
                        campaignId_memberStatus.put(newCampaign.ID, setting.Campaign_Member_Status__c);
                        break;
                    }
                }
            }    	
        }
        return (campaignId_memberStatus);
    }
    
    
    private void executeBatchUpdateMembers(Map<ID,String> campaignId_memberStatus,Map<Id,List<List<Coupon__c>>> allCoupons){ 
        if (allCoupons.size() > 0 || campaignId_memberStatus.size() > 0){
            if(checkMaxRunningJobOK()) { 
                Database.executeBatch(new BatchUpdateCampaignMember(campaignId_memberStatus, allCoupons));
            } else {
               Trigger.new[0].addError('Unable to mass update campaign members status/coupons. Please try again later.');
            }
        }
    }
    
    /*Ensure only max 5 concurrent running batch jobs */
    private boolean checkMaxRunningJobOK(){
        Integer MAX_BATCHES = 5;
   		Integer runningBatchJobs = [
             select count()
             from AsyncApexJob
             where JobType = 'BatchApex'
             and status in ('Queued','Processing','Preparing')
         ];
        
        return (runningBatchJobs < MAX_BATCHES);
    }
    
    /* Mass Update for Member coupon*/
    private Map<Id,List<List<Coupon__c>>> getUpdateMemberCoupon(Map<ID, Campaign> oldCampaignMap, Campaign[] updatedCampaigns){
    	Map<Id,Campaign> campaign_generateCoupon = new Map<Id,Campaign>(); 
        Map<Id, String> campaignId_CancelCouponReason = new Map<ID,String>();
        
        for (Campaign newCampaign : updatedCampaigns){
            Campaign oldCampaign = oldCampaignMap.get(newCampaign.ID);
            
            if (newCampaign.Status == 'Cancelled'){
                campaignId_CancelCouponReason.put(newCampaign.Id,CommonHelper.COUPON_CANCELLED);
            }else
            //enter only if there's change in type+coupon type.
            if (oldCampaign.Type != newCampaign.Type || oldCampaign.Coupon_Type__c != newCampaign.Coupon_Type__c){
            	//decide if case 1: From non coupon to coupon
            	if (oldCampaign.Type != 'Coupon' && newCampaign.Type == 'Coupon'){
            		//use campaign ID to generate coupon for all its members
            		campaign_generateCoupon.put(newCampaign.Id,newCampaign);
            	}else if(oldCampaign.Type == 'Coupon' && newCampaign.Type != 'Coupon'){
            		//case 2: from coupon to non-coupon
            		//cancel coupons from all members
            		campaignId_CancelCouponReason.put(newCampaign.Id,CommonHelper.COUPON_TYPE_CHANGED);
            	}else if(oldCampaign.Type == 'Coupon' && newCampaign.Type == 'Coupon'){
            		//case 3: change in coupon type
            		//cancel existing coupons, generate new type.
            		campaignId_CancelCouponReason.put(newCampaign.Id,CommonHelper.COUPON_TYPE_CHANGED);
            		campaign_generateCoupon.put(newCampaign.Id,newCampaign);
            	}
            }    	
            
        }
        
     	//Bulk generate+insert coupons
        Map<Id,List<List<Coupon__c>>> allCoupons = CampaignCouponHelper.generateAllCoupons(campaign_generateCoupon);
        
        //add blank Iterators to cancel existing coupons
        for (Id i : campaignId_CancelCouponReason.keySet()){
        	//careful not to overwrite new coupon iterators
        	if (allCoupons.keyset().contains(i) == false){
        		allCoupons.put(i, new List<List<Coupon__c>>());
        	}
        }
        //Cancelled coupons status
        CampaignCouponHelper.cancelCampaignCoupon(campaignId_CancelCouponReason);
        return (allCoupons);	
    }
}