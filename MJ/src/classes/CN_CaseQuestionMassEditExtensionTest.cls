/*Author:leo.bi@celnet.com.cn
 *Date:2014-5-5
 *function:The test class of CN_CN_CaseQuestionMassEditExtension
 *Apply To:CN
 */
@isTest
private class CN_CaseQuestionMassEditExtensionTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        system.runas(TestUtility.new_CN_User())
        {
	        // Create a test case
	        Case testCase = new Case();
	        testCase.type='Enquiry';
	        testCase.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Close_Case' and SobjectType=:'Case'].id;
	        testCase.Origin = 'Phone';
	        testCase.Status = 'New';
	        insert testCase;
	        
	        //Creat Product 1 - MJ & Is_DSO_Product__c = False
	        Product__c product1 = new Product__c();
	        product1.Name = 'Product 1';
	        product1.Product_Code__c = 'Product01';
	        product1.Product_Nature__c = 'MJ';
	        product1.Is_DSO_Product__c = false;
	        insert product1;
	        
	        //Creat Product 2 - MJ & Is_DSO_Product__c = False
	        Product__c product2 = new Product__c();
	        product2.Name = 'Product 2';
	        product2.Product_Code__c = 'Product02';
	        product2.Product_Nature__c = 'MJ';
	        product2.Is_DSO_Product__c = false;
	        insert product2;
	        
	        //Creat Product 3 - Non MJ
	        Product__c product3 = new Product__c();
	        product3.Name = 'Product 3';
	        product3.Product_Code__c = 'Product03';
	        product3.Product_Nature__c = 'Non MJ';
	        product3.Is_DSO_Product__c = false;
	        insert product3;
	        ApexPages.currentPage().getParameters().put('caseid', testCase.Id);
	        
	        //Instantiate and construct the controller class.   
	        ApexPages.StandardController thestandardController = new ApexPages.StandardController(new Case_Question__c()); 
	        CN_CaseQuestionMassEditExtension controller = new CN_CaseQuestionMassEditExtension(thestandardController); 
	        
	        List<Case_Question__c> questions = controller.questions;
	        System.assertEquals(5,questions.size());
	        
	        //clone
			controller.cloneParameter = 1;
			controller.cloneQuestion();        
	        
	        // Add 3 new lines
	        controller.addNew();
	        questions = controller.questions;
	        System.assertEquals(9,questions.size());
	        
	        // Reset
	        controller.reset();
	        questions = controller.questions;
	        System.assertEquals(5,questions.size());
	        
	        Case parentCase = controller.parentCase;
	        System.assertEquals(testCase.ID,parentCase.ID);
	        
	        // Cancel
	        String nextPage = controller.cancel().getUrl(); 
	        System.assertEquals('/'+testCase.ID, nextPage);
	        
	        //Instantiate a new controller
	        controller = new CN_CaseQuestionMassEditExtension(new ApexPages.StandardController(new Case_Question__c())); 
	        questions = controller.questions;
	        
	        //Create a case question
	        Case_Question__c question = questions.get(1);
	        question.Type__c = 'Enquiry';
	        question.Sub_Type__c  = 'Health';
	        question.Question__c = 'TEST Question';
	        question.Product__c = product1.ID;
	        questions.set(1, question);
	        
	        nextPage = controller.save().getUrl();
	        System.assertEquals('/'+testCase.ID, nextPage);
	        Integer count = [SELECT COUNT() FROM Case_Question__c WHERE Case__c = :testCase.ID];
	        System.assertEquals(1, count);
	        
	        //Test Edit Exsiting Record
	        controller = new CN_CaseQuestionMassEditExtension(new ApexPages.StandardController(new Case_Question__c())); 
	        questions = controller.questions;
	        
	        //Edit Existing Question 
	        Case_Question__c existingQuestion = questions.get(0);
	        existingQuestion.Remark__c = 'Updated';
	        questions.set(0, existingQuestion);
	        
	        //Create a new case question
	        Case_Question__c question2 = questions.get(1);
	        question2.Type__c = 'Compliant';
	        question2.Sub_Type__c  = 'Product';
	        question2.Question__c = 'TEST Question';
	        question2.Product__c = product2.ID;
	        questions.set(1, question2);
	        
        }
    }
}