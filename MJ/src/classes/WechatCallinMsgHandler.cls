/*
*Author:michael.mi@celnet.com.cn
*Date:2014-09-04
*Function:Abstract Class WechatCallinMsgHandler
*/
public abstract class WechatCallinMsgHandler 
{
	public Wechat_Handler_Binding__c BindingInfo {get; set;}
	public abstract void Handle(WechatCallinMsgPipelineContext Context);
}