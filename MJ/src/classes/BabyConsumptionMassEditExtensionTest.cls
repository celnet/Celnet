@isTest
public class BabyConsumptionMassEditExtensionTest{

    public static testMethod void testMassEditConsumptionExtension() 
    {
    	system.runas(TestUtility.new_HK_User()){
	        //Create custom settings
	        TestUtility.setupMemberIDCustomSettings();
	        
	        // Create a test Account
	        Account testAccount = new Account();
	        testAccount.Name = 'Account';
	        testAccount.Last_Name__c = 'Account';
	        insert testAccount;
	        
	        //Creat test contact
	        Contact contact = New Contact();
	        contact.FirstName='Test';
	        contact.LastName='Last';
	        contact.AccountId=testAccount.ID;
	        contact.Birthdate = system.today().addYears(-6); 
	        insert contact;
	        
	        //Creat Product 1
	        Product__c product1 = new Product__c();
	        product1.Name = 'Product 1';
	        product1.Product_Code__c = 'Product01';
	        insert product1;
	        
	        //Creat Product 2
	        Product__c product2 = new Product__c();
	        product2.Name = 'Product 2';
	        product2.Product_Code__c = 'Product02';
	        insert product2;
	        
	        //Creat Product 3
	        Product__c product3 = new Product__c();
	        product3.Name = 'Product 3';
	        product3.Product_Code__c = 'Product03';
	        insert product3;
	        
	        //Create consumption record 1
	        Baby_Consumption__c consumption1 = new Baby_Consumption__c();
	        consumption1.Contact__c=contact.ID;
	        consumption1.Product__c=product1.ID;
	        consumption1.From_Age__c=0;
	        consumption1.To_Age__c=1;
	        insert consumption1;
	
	        ApexPages.currentPage().getParameters().put('id', contact.Id);
	        
	        //Instantiate and construct the controller class.   
	        List<Baby_Consumption__c> consumptions = new List<Baby_Consumption__c>();
	        ApexPages.StandardSetController standardSetController = new ApexPages.StandardSetController(consumptions);
	        BabyConsumptionMassEditExtension controller = new BabyConsumptionMassEditExtension(standardSetController);
	        
	        List<Baby_Consumption__c> relatedConsumptions = controller.getRelatedConsumptions();
	        System.assertEquals(5,relatedConsumptions.size());
	        
	        // Add 3 new lines
	        controller.addNew();
	        relatedConsumptions = controller.getRelatedConsumptions();
	        System.assertEquals(8,relatedConsumptions.size());
	        
	        controller.reset();
	        relatedConsumptions = controller.getRelatedConsumptions();
	        System.assertEquals(5,relatedConsumptions.size());
	        
	        // Set consumption record 1 to empty value
	        // Set Line 3 "From Age" with leaving other fields blank
	        // Set Line 4 "To Age" with leaving other fields blank
	        // Set Line 5 "Product" with leaving other fields blank
	        relatedConsumptions[0].From_Age__c=NULL;
	        relatedConsumptions[0].To_Age__c=NULL;
	        relatedConsumptions[0].Product__c=NULL;
	        relatedConsumptions[2].From_Age__c=1;
	        relatedConsumptions[3].To_Age__c=3;
	        relatedConsumptions[4].Product__c=product2.ID;
	        System.assertEquals(NULL,controller.save());
	        
	        //Re-instantiate and construct the controller class.   
	        consumptions = new List<Baby_Consumption__c>();
	        standardSetController = new ApexPages.StandardSetController(consumptions);
	        controller = new BabyConsumptionMassEditExtension(standardSetController); 
	        
	        // Cancel
	        String nextPage = controller.cancel().getUrl(); 
	        System.assertEquals('/'+contact.Id, nextPage);
    	 }
   }
   
   //Test successful SAVE action
   public static testMethod void testMassEditConsumptionExtension2() 
   {
   	 system.runas(TestUtility.new_HK_User()){
        //Create custom settings
       TestUtility.setupMemberIDCustomSettings();
        
        // Create a test Account
        Account testAccount = new Account();
        testAccount.Name = 'Account';
        testAccount.Last_Name__c = 'Account';
        insert testAccount;
        
        //Creat test contact
        Contact contact = New Contact();
        contact.FirstName='Test';
        contact.LastName='Last';
        contact.AccountId=testAccount.ID;
       contact.Birthdate = system.today().addYears(-6);
        insert contact;
        
        //Creat Product 1
        Product__c product1 = new Product__c();
        product1.Name = 'Product 1';
        product1.Product_Code__c = 'Product01';
        insert product1;
        
        //Creat Product 2
        Product__c product2 = new Product__c();
        product2.Name = 'Product 2';
        product2.Product_Code__c = 'Product02';
        insert product2;
        
        //Creat Product 3
        Product__c product3 = new Product__c();
        product3.Name = 'Product 3';
        product3.Product_Code__c = 'Product03';
        insert product3;
        
        //Create consumption record 1
        Baby_Consumption__c consumption1 = new Baby_Consumption__c();
        consumption1.Contact__c=contact.ID;
        consumption1.Product__c=product1.ID;
        consumption1.From_Age__c=0;
        consumption1.To_Age__c=1;
        insert consumption1;
        
        ApexPages.currentPage().getParameters().put('id', contact.Id);
        
        //Instantiate and construct the controller class.   
        List<Baby_Consumption__c> consumptions = new List<Baby_Consumption__c>();
        ApexPages.StandardSetController standardSetController = new ApexPages.StandardSetController(consumptions);
        BabyConsumptionMassEditExtension controller = new BabyConsumptionMassEditExtension(standardSetController);
        
        List<Baby_Consumption__c> relatedConsumptions = controller.getRelatedConsumptions();
        
        relatedConsumptions[2].From_Age__c=2;
        relatedConsumptions[2].To_Age__c=3;
        relatedConsumptions[2].Product__c=product2.ID;
        
        String nextPage = controller.save().getUrl();
        System.assertEquals('/'+contact.Id, nextPage);
        
        Integer count = [SELECT COUNT() FROM Baby_Consumption__c WHERE Contact__c = :contact.ID];
        System.assertEquals(2, count);
   	 }
   }
}