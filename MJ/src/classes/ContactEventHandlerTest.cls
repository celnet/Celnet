/*Author:leo.bi@celnet.com.cn
 *Creat Date:2014-05-21
 *function:The test class of ContactEventHandler
 *Apply to:CN
 */
@isTest(SeeAllData = true)
private class ContactEventHandlerTest {
    static testMethod void myUnitTest() 
    {
    	system.runAs(TestUtility.new_CN_User())
    	{
    		Id contactRecordType = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
    		Id accountRecordType = [select id from RecordType where DeveloperName=:'CN_HouseHold' and SobjectType=:'Account'].id;
	        //Test Clear Questionnaire__c when Q-Task deleted
	        //new Account
			Account acc = new Account();
			acc.RecordTypeId = accountRecordType;
			acc.Name = 'HouseHold';
			acc.Last_Name__c ='HouseHold';
			insert acc;
			Questionnaire__c q = new Questionnaire__c();
			q.Name = 'Test';
			insert q;
	        //new Contact
	        Contact con = new Contact();
			con.RecordTypeId = contactRecordType;
			con.AccountId = acc.Id;
			con.LastName = 'conLast';
			con.Birthdate = Date.today().addDays(-5);
			con.Register_Date__c = Date.today().addDays(-4);
			con.Primary_RecruitChannel_Type__c = 'Hospital';
			con.Primary_RecruitChannel_Sub_type__c = 'PCBaby';
			con.Questionnaire__c = q.Id;
			insert con;
	        Q_Task__c qt = [select Status__c,ID__c, Id from Q_Task__c where Contact__c = :con.Id];
	        qt.Status__c = 'Closed';
	        qt.ID__c = con.Id + '-Closed' + String.valueOf(DateTime.now());
	        update qt;
	        delete qt;
	        //test Update Contact
	        //new Account
			Account acc1 = new Account();
			acc1.RecordTypeId = accountRecordType;
			acc1.Name = 'HouseHold';
			acc1.Last_Name__c ='HouseHold';
			insert acc1;
			Questionnaire__c q1 = new Questionnaire__c();
			q1.Name = 'Test';
			insert q1;
	        //new Contact
	        Contact con1 = new Contact();
			con1.RecordTypeId = contactRecordType;
			con1.AccountId = acc1.Id;
			con1.LastName = 'conLast';
			con1.Birthdate = Date.today().addDays(-5);
			con1.Register_Date__c = Date.today().addDays(-4);
			con1.Primary_RecruitChannel_Type__c = 'Hospital';
			con1.Primary_RecruitChannel_Sub_type__c = 'PCBaby';
			con1.Questionnaire__c = q1.Id;
			insert con1;
			Test.startTest();
			//new task
			Task t = new Task();
	        t.WhoId = con.Id;
	        insert t;
	        t.Result__c = 'Inactive';
	        t.Inactive_Reason__c = 'Others';
	        update t;
	        t.Result__c = 'Do not Call';
	        update t;
	        t.Result__c = 'No Answer2';
	        update t;
	        t.Result__c = 'Abortion/Closed';
	        update t;
	        Test.stopTest();
    	}
    }
}