@isTest
public class OrderItemMassAddEditExtensionTest{

static Product__c product1,product2,product3;
    public static Order__c setupOrder(){
    	//Get RecordType ID for HouseHold and HouseHold Child
        RecordType typeHouseHold = [Select Id From RecordType Where DeveloperName ='HouseHold' and SobjectType ='Account' limit 1];
        RecordType typeHouseHoldChild = [Select Id From RecordType Where DeveloperName ='Household_Child' and SobjectType ='Contact' limit 1];
        
        //Create custom settings
        Member_ID_Setting__c s = new Member_ID_Setting__c();
        s.Name = String.valueOf(Date.today().year());
        s.Next_ID__c = 1888;
        insert s;
        
        
        // Create a test Household
        Account testAccount = new Account();
        testAccount.Name = 'Account Name';
        testAccount.First_Name__c = 'Account';
        testAccount.Last_Name__c = 'Name';
        testAccount.RecordTypeId = typeHouseHold.Id;
        insert testAccount;
        
        //Creat test baby 1
        Contact contact = New Contact();
        contact.FirstName='Test';
        contact.LastName='Baby';
        contact.AccountId=testAccount.ID;
        contact.RecordTypeId = typeHouseHoldChild.Id;
        insert contact;
        
        //Creat test baby 2
        Contact contact2 = New Contact();
        contact2.FirstName='Two';
        contact2.LastName='Baby';
        contact2.AccountId=testAccount.ID;
        contact2.RecordTypeId = typeHouseHoldChild.Id;
        insert contact2;
        
        //Creat Product 1 - MJ & Is_DSO_Product__c = False
        product1 = new Product__c();
        product1.Name = 'Product 1';
        product1.Product_Code__c = 'Product01';
        product1.Product_SKU__c = '';
        product1.Product_Nature__c = 'MJ';
        product1.Is_DSO_Product__c = false;
        insert product1;
        
        //Creat Product 2 - MJ & Is_DSO_Product__c = True
        product2 = new Product__c();
        product2.Name = 'Product 2 900g';
        product2.Product_SKU__c = 'Product 2 900g';
        product2.Product_Code__c = 'Product02';
        product2.Product_Nature__c = 'MJ';
        product2.Is_DSO_Product__c = True;
        product2.Default_Order_Quantity__c = 10;
        product2.Allowed_Order_Period__c = 20;
        product2.Min_Order_Quantity__c = 1;
        product2.Max_Order_Quantity__c = 50;
        product2.Unit_Price__c = 8.88;
        insert product2;
        
        //Creat Product 3 - Non MJ
        product3 = new Product__c();
        product3.Name = 'Product 3';
        product3.Product_SKU__c = '';
        product3.Product_Code__c = 'Product03';
        product3.Product_Nature__c = 'Non MJ';
        product3.Is_DSO_Product__c = false;
        insert product3;

        //Create a Order
        Order__c order =new Order__c();
        order.Delivery_Center__c = 'DKSH';
        order.HouseHold__c = testAccount.ID;
        order.Delivery_Instruction__c = 'TEST';
        order.Expected_Delivery_Date__c = Date.today().addDays(7);
        insert order;
        
        ApexPages.currentPage().getParameters().put('id', order.Id);
        
        return order;
    }
    public static testmethod void testOrderItemMassAddEditExtension()
    {
        Order__c order = setupOrder();
        
        //Instantiate and construct the controller class.   
        OrderItemMassAddEditExtension controller = new OrderItemMassAddEditExtension();
        
        // Test Case: valiate related babies
        List<contact> relatedBabies = controller.getRelatedBabies();
        System.assertEquals(2,relatedBabies.size());
        
        List<OrderItemMassAddEditExtension.ItemWrapper> items = controller.getOrderItemWrappers();
        System.assertEquals(5,items.size());

        //Test case: validate Product Options
        List<SelectOption> options = controller.getDSOProductOptions();
        System.assertEquals(2,options.size());
       
        //Test case: parent Order
        Order__c parentOrder = controller.getParentOrder();
        System.assertEquals(true,(parentOrder!=NULL));
        
        // Add 3 new lines
        controller.addNew();
        items = controller.getOrderItemWrappers();
        System.assertEquals(8,items.size());
        
        //Assert OrderState = Draft
        System.assertEquals(true,controller.getIsOrderDraft());
        
        // Test Case - Simulate Product Change
        Order_Item__c item2 = items.get(1).item;
        item2.Product__c = product2.ID;
        controller.setCurIndex(1);
        controller.productChange();
        items = controller.getOrderItemWrappers();
        System.assertEquals(1,controller.getCurIndex());
        System.assertEquals(10,items.get(1).item.Quantity__c);
        System.assertEquals(8.88,items.get(1).unitPrice);
        System.assertEquals(88.8,controller.getTotalPrice());
        
        // Test Case - Simulate Quantity Change
        item2 = items.get(1).item;
        item2.Quantity__c = 20;
        controller.setCurIndex(1);
        controller.quantityChange();
        items = controller.getOrderItemWrappers();
        System.assertEquals(20,items.get(1).item.Quantity__c);
        System.assertEquals(8.88*20,items.get(1).getSubTotalPrice());
        System.assertEquals(8.88*20,controller.getTotalPrice());
        
        //Save the order item
        String nextPage = controller.saveItems().getUrl();
        System.assertEquals('/'+order.ID, nextPage);
        Integer count = [SELECT COUNT() FROM Order_Item__c WHERE Order__c = :order.ID];
        System.assertEquals(1, count);
        
        //Re-instantiate and construct the controller class.   
        controller = new OrderItemMassAddEditExtension(); 
        
        // Cancel
        nextPage = controller.cancel().getUrl(); 
        System.assertEquals('/'+order.Id, nextPage);
    }
    
    public static testMethod void testLoadInvalidOrder(){
    	//Simulate Invaid ID 
    	ApexPages.currentPage().getParameters().put('id', 'invalid ID');
    	//Instantiate and construct the controller class.   
        OrderItemMassAddEditExtension controller = new OrderItemMassAddEditExtension();
        //Assert Error: Order Not found 
        System.assertEquals(1,ApexPages.getMessages().size());
        System.assertEquals(CommonHelper.ORDER_NOT_FOUND, ApexPages.getMessages()[0].getSummary());
    }
    
    public static testMethod void testProductWithoutQtyValidation(){
    	Order__c order = setupOrder();
        //Instantiate and construct the controller class.   
        OrderItemMassAddEditExtension controller = new OrderItemMassAddEditExtension();
        List<OrderItemMassAddEditExtension.ItemWrapper> items = controller.getOrderItemWrappers();
        //Test case: Prod without Qty
        Order_Item__c item2 = items.get(1).item;
        item2.Product__c = product2.ID;
        controller.setCurIndex(1);
        controller.productChange();
        item2.Quantity__c = null;
        controller.quantityChange();
        controller.saveItems();
        System.assertEquals(1,controller.getCurIndex());
        System.assertEquals(null,items.get(1).item.Quantity__c);
        System.assertEquals(CommonHelper.VALUE_REQUIRED, ApexPages.getMessages()[0].getSummary());
    }
    
    public static testMethod void testQtyWithoutProductValidation(){
    	Order__c order = setupOrder();
        //Instantiate and construct the controller class.   
        OrderItemMassAddEditExtension controller = new OrderItemMassAddEditExtension();
        List<OrderItemMassAddEditExtension.ItemWrapper> items = controller.getOrderItemWrappers();
    	 //Test case Qty without Prod
    	 Order_Item__c item2 = items.get(1).item;
        item2.Product__c = null;
        controller.setCurIndex(1);
        controller.productChange();
        item2.Quantity__c = 5;
        controller.quantityChange();
        controller.saveItems();
        System.assertEquals(1,controller.getCurIndex());
        System.assertEquals(5,items.get(1).item.Quantity__c);
        System.assertEquals(CommonHelper.PRODUCT_REQUIRED, ApexPages.getMessages()[0].getSummary());
    }
    public static testMethod void testDuplicateProductValidation(){
    	Order__c order = setupOrder();
        //Instantiate and construct the controller class.   
        OrderItemMassAddEditExtension controller = new OrderItemMassAddEditExtension();
        List<OrderItemMassAddEditExtension.ItemWrapper> items = controller.getOrderItemWrappers();
    	 //Test duplicate product
    	 Order_Item__c item2 = items.get(1).item;
        item2.Product__c = product2.id;
        controller.setCurIndex(1);
        controller.productChange();
        item2.Quantity__c = 5;
        controller.quantityChange();
        
         Order_Item__c item3 = items.get(2).item;
        item3.Product__c = product2.id;
        controller.setCurIndex(2);
        controller.productChange();
        item3.Quantity__c = 1;
        controller.quantityChange();
        
        controller.saveItems();
        System.assert(ApexPages.getMessages()[0].getSummary().contains('Cannot place multiple order item of the same product'));
    }
    
    //push order to SFA, validations = delete, max allowed orders, reset , modify existing item without price/prod
    
    public static testMethod void testEditFailedOrder(){
    	Order__c order = setupOrder(); 
    	//Simulate Failed order
        order.Status__c = 'Rejected';
        update order;
        // construct the controller class.   
        OrderItemMassAddEditExtension controller = new OrderItemMassAddEditExtension();
        //Assert OrderState = Failed
        System.assertEquals(true,controller.getIsOrderFailed());
        ///Assert Warning: Order Cancelled
        System.assertEquals(1,ApexPages.getMessages().size());
        System.assertEquals(CommonHelper.FAILED_ORDER_ITEM_EDIT_NOT_ALLOWED, ApexPages.getMessages()[0].getSummary());
    	
    }
    public static testMethod void testEditCancelledOrder(){
    	Order__c order = setupOrder(); 
    	//Simulate Cancelled order 
        order.Cancelled__c = true;
        order.Cancel_Reason__c = 'Some reason';
        update order;
        //construct the controller class.   
        OrderItemMassAddEditExtension controller = new OrderItemMassAddEditExtension();
        //Assert OrderState = Failed
        System.assertEquals(true,controller.getIsOrderCancelled());
        ///Assert Warning: Order Cancelled
        System.assertEquals(1,ApexPages.getMessages().size());
        System.assertEquals(CommonHelper.CANCELLED_ORDER_ITEM_EDIT_NOT_ALLOWED, ApexPages.getMessages()[0].getSummary());
    }
    
}