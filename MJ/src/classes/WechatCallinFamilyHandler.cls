/*Author:Mark
 *Date:20140924
 *Function:Our A+ family  在微信客户端上点击A+ Family按钮时由此Handler来处理
*/
public class WechatCallinFamilyHandler extends WechatCallinMsgHandler
{
    public override void Handle(WechatCallinMsgPipelineContext Context)
    {
        if(Context.InMsg.MsgType == WechatEntity.MESSAGE_TYPE_EVENT || Context.InMsg.MsgType == WechatEntity.MESSAGE_TYPE_VOICE)
        {
            WechatEntity.OutRichMediaMsg outRichMedia = new WechatEntity.OutRichMediaMsg();
            if(Context.InMsg.MsgType == WechatEntity.MESSAGE_TYPE_EVENT)
            {
                WechatEntity.InEventMsg inEvent = (WechatEntity.InEventMsg)Context.InMsg;
                if(inEvent.EventKey != this.BindingInfo.Event_Key__c )
                {
                    return;
                }
            }
            WechatEntity.OutTextMsg outTextMsg = WechatEntity.GenerateOutMsg(Context.InMsg , '');
            Context.OutMsg = outTextMsg;
            if(Context.InMsg.MsgType == WechatEntity.MESSAGE_TYPE_EVENT)
            {   
                List<WechatEntity.Article> articleList = new List<WechatEntity.Article>();
                WechatEntity.GetBaseMsgAtrributes(Context.InMsg,outRichMedia);
                outRichMedia.MsgType = WechatEntity.MESSAGE_TYPE_NEWS;
                outRichMedia.Articles = articleList;    
                List<Wechat_Menu__c> wmList = [select id , Wechat_Content__r.Article_Url__c ,Wechat_Content__r.Description__c , Wechat_Content__r.Key_Word__c , Wechat_Content__r.Picture_Url__c , Wechat_Content__r.Title__c 
                                               from Wechat_Menu__c
                                               where Event_Key__c = 'WechatCallinFamilyHandler'];
                if(!wmList.isempty())
                {
                    Wechat_Menu__c wm = wmList[0];
                    WechatEntity.Article art = GenerateWechatArticle(wm);
                    outRichMedia.Articles.add(art);
                }
                Context.OutMsg = outRichMedia;
            }
            else
            {
                outTextMsg.Content = WechatBusinessUtility.QueryRemindingByKey('RN039');
                WechatEntity.InVoiceMsg inVoice = (WechatEntity.InVoiceMsg)Context.InMsg;
                Wechat_Message__c wm = GenerateWechatMessage(Context.PublicAccountName ,inVoice);
                upsert wm Msg_Id__c;
                Wechat_Callout_Task_Queue__c voiceQueue = GenerateWechatVoiceQueue(Context.PublicAccountName , inVoice);
                WechatCalloutQueueManager.EnQueue(voiceQueue);
                Context.OutMsg = outTextMsg;
            }
        }
    }
    //Save voice message to salesforce system
    public Wechat_Message__c GenerateWechatMessage(string publicAccountName ,WechatEntity.InVoiceMsg inVoice)
    {
        Wechat_Message__c wm = new Wechat_Message__c();
        Wechat_User__c wu = new Wechat_User__c();
        wu.Open_Id__c = inVoice.FromUserName;
        upsert wu Open_Id__c;
        wm.Create_Time__c = inVoice.CreateTime;
        wm.Direction__c = 'inBound';
        wm.Format__c = inVoice.Format;
        wm.From_User__c = wu.Id;
        wm.From_User_ID__c = inVoice.FromUserName;
        wm.Media_Id__c = inVoice.MediaId;
        wm.Related_User__c = wu.Id;
        wm.Recognition__c = inVoice.Recognition;
        wm.Type__c = 'voice';
        wm.Msg_Id__c = inVoice.MsgId;
        wm.Public_Account_Name__c = publicAccountName;
        return wm;
    }
    //generate
    public WechatEntity.Article GenerateWechatArticle(Wechat_Menu__c wm)
    {
        WechatEntity.Article art = new WechatEntity.Article();
        art.title = wm.Wechat_Content__r.Title__c;
        art.description = wm.Wechat_Content__r.Description__c;
        art.url = wm.Wechat_Content__r.Article_Url__c;
        art.picURL = (null != wm.Wechat_Content__r.Picture_Url__c ? wm.Wechat_Content__r.Picture_Url__c : '');
        return art;
    }
    //Create download voice media callout task queue
    public Wechat_Callout_Task_Queue__c GenerateWechatVoiceQueue(string publicAccountName , WechatEntity.InVoiceMsg inVoice)
    {
        Wechat_Callout_Task_Queue__c voiceQueue = new Wechat_Callout_Task_Queue__c();
        voiceQueue.Public_Account_Name__c = publicAccountName;
        voiceQueue.Open_ID__c = inVoice.FromUserName;
        voiceQueue.Media_ID__c = inVoice.MediaId;
        voiceQueue.Msg_ID__c = inVoice.MsgId;
        voiceQueue.Type__c = WechatEntity.MESSAGE_TYPE_VOICE;
        voiceQueue.Processor_Name__c = 'WechatCalloutGetVoiceProcessor';
        voiceQueue.Name = 'WechatCalloutGetVoiceProcessor';
        return voiceQueue;
    }
}