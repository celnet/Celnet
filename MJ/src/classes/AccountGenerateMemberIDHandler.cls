/*
Author: shiqi.ng@sg.fujitsu.com
Created On: 2014-04-28
Function: Generate member id for CN
Support Event: BeforeInsert
Apply To: CN (CN / HK / ALL)
*/
public class AccountGenerateMemberIDHandler implements Triggers.Handler
{
    public void Handle()
    {
        if(!trigger.isInsert)
        {
            return;
        }
        if(!trigger.isBefore)
        {
            return;
        }
        if(!Context.ApplyTo(new String[] {'CN'})) // make this code only apply to CN
        {
            return;
        }
        //generate member id here.
        
        Integer size = trigger.new.size();
        String[] nextMemberIDs  = generateNextMemberID(size);
        Integer i = 0;
        for (SObject sobj : trigger.new){
            Account newAccount = (Account) sobj;
            newAccount.Member_ID__c = nextMemberIDs[i++];
        }
    }
    
    private List<String> generateNextMemberID(Integer size)
    {
        String curYear = String.valueOf(Date.today().year());
        String key = curYear+'-CN';
        system.debug(key);
        //Get next Member ID running number from custom setting
        //Member_ID_Setting__c memberSetting = Member_ID_Setting__c.getValues(key);
        //Use SELECT FOR UPDATE to lock row for updating
        try{
        Member_ID_Setting__c[] memberSetting = [SELECT Next_ID__c FROM Member_ID_Setting__c WHERE name =:key LIMIT 1 FOR UPDATE];
        system.debug(memberSetting);
        if(memberSetting.size() == 0)
        {
            CommonHelper.MJNCustomException e = new CommonHelper.MJNCustomException('Custom Setting Key for Member ID Running Number not found: '+ key);
            CommonHelper.notifySystemException(e);
            throw e;
        }
        else
        {
            List<String> newMemberIDs = new List<String>();
            for (Integer i = 0; i < size; i++){
                String newMemberID = '';
                Integer nextNumber = Integer.valueOf(memberSetting[0].Next_ID__c) + i;
                //Member ID format will be "CN-{c}YY0NNNNNN"
                newMemberID = 'CN-'+UniqueCodeGenerator.getRandomChar()+curYear.substring(2)+'0'
                             +UniqueCodeGenerator.leftPadZero(String.valueOf(nextNumber),6);
                newMemberIDs.add(newMemberID);
            }
                        
            //Increase the Member Running Number
            memberSetting[0].Next_ID__c = memberSetting[0].Next_ID__c+size;
            update memberSetting[0];
            
            return newMemberIDs;
        }
        }catch(Exception e){
            system.debug('E!'+e.getMessage());
            throw e;
        }
    }
}