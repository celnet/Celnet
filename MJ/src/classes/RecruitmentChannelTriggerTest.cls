@isTest
public class RecruitmentChannelTriggerTest
{
    public static testMethod void testRecruitmentChannelTrigger()
    {
    	 //Create custom settings
        TestUtility.setupMemberIDCustomSettings();  
        
        //Create test account
        Account account = New Account();
        account.Name = 'Test';
        account.Last_Name__c = 'Test';
        insert account;
        
        //Creat test contact
        Contact contact = New Contact();
        contact.FirstName='Test';
        contact.LastName='Last';
        contact.AccountId=account.ID;
        insert contact;
        
        //Create recruitment channel 1 - Set as Primary
        Recruitment_Channel__c channel1 = new Recruitment_Channel__c();
        channel1.Contact__c=contact.ID;
        channel1.Channel__c='BB Expo';
        channel1.Sub_Channel__c='BB Expo';
        channel1.Primary__c=true;
        insert channel1;
        
        //Create recruitment channel 2 - Set as Primary
        Recruitment_Channel__c channel2 = new Recruitment_Channel__c();
        channel2.Contact__c=contact.ID;
        channel2.Channel__c='BB Expo';
        channel2.Sub_Channel__c='BB Expo';
        channel2.Primary__c=true;
        insert channel2;
        
        Recruitment_Channel__c updatedChannel1 = [SELECT ID,Primary__c FROM Recruitment_Channel__c WHERE ID =:channel1.ID];
        Recruitment_Channel__c updatedChannel2 = [SELECT ID,Primary__c FROM Recruitment_Channel__c WHERE ID =:channel2.ID];
        System.assertEquals(true,updatedChannel2.Primary__c);
        System.assertEquals(false,updatedChannel1.Primary__c);
        
        //Set Channel 1 as Primary
        channel1.Primary__c=true;
        update channel1;
        
        updatedChannel1 = [SELECT ID,Primary__c FROM Recruitment_Channel__c WHERE ID =:channel1.ID];
        updatedChannel2 = [SELECT ID,Primary__c FROM Recruitment_Channel__c WHERE ID =:channel2.ID];
        System.assertEquals(true,updatedChannel1.Primary__c);
        System.assertEquals(false,updatedChannel2.Primary__c);
    }
    
    static testMethod void testValidations(){
    	 //Create custom settings
        TestUtility.setupMemberIDCustomSettings();
        
        //Create test account
        Account account = New Account();
        account.Name = 'Test';
        account.Last_Name__c = 'Test';
        insert account;
        
        //Creat test contact
        Contact contact = New Contact();
        contact.FirstName='Test';
        contact.LastName='Last';
        contact.AccountId=account.ID;
        insert contact;
        
        //Create recruitment channel 1 - Set as Primary
        Recruitment_Channel__c channel1 = new Recruitment_Channel__c();
        channel1.Contact__c=contact.ID;
        channel1.Channel__c='BB Expo';
        channel1.Sub_Channel__c='BB Expo';
        channel1.Primary__c=false;
        try{
        	insert channel1;
        	//throw new CommonHelper.MJNCustomException('Validation check failed. Is Recruitment channel set as Primary Validation rule disabled?');
        }catch (DmlException  e){
        	//VR check: must set pri on first
        	system.assert(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
        }
        
        channel1.Primary__c = true;
        if(channel1.Id ==null)
        insert channel1;
        //assert ok
        channel1 = [SELECT ID,Primary__c FROM Recruitment_Channel__c WHERE ID =:channel1.ID];
        //System.assertEquals(true,channel1.Primary__c);
         
        //attempt to unset
        channel1.Primary__c = false;
        try{
        	update channel1;
        }catch (DmlException  e){
        	//Custom validation : prevent unset only primary
        	system.assert(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION, ' +
        		RecruitmentChannelHandler.CANNOT_UNSET_PRIMARY
        		));
        }
    }
    
    /* not applicable
    static testMethod void testBulkInsert(){
    	//Create custom settings
        TestUtility.setupMemberIDCustomSettings();
        
        //Create test account
        Account account = New Account();
        account.Name = 'Test';
        account.Last_Name__c = 'Test';
        insert account;
        
        //Creat test contact
        Contact contact = New Contact();
        contact.FirstName='Test';
        contact.LastName='Last';
        contact.AccountId=account.ID;
        insert contact;
        
        //Mass Create recruitment channels - Set as Primary
        List<Recruitment_Channel__c> massChannels = new List<Recruitment_Channel__c>();
        
        for (Integer i = 0; i < 100; i++){
            Recruitment_Channel__c channel1 = new Recruitment_Channel__c();
        	channel1.Contact__c=contact.ID;
       		 channel1.Channel__c='BB Expo';
        	channel1.Sub_Channel__c='BB Expo';
       		 channel1.Primary__c=true;
            massChannels.add(channel1);
        }
        Test.startTest();
        insert massChannels;
        Test.stopTest();
        
        //Select
        massChannels = [SELECT Primary__c FROM Recruitment_Channel__c];
        //Assert size, first, last
        system.assertEquals(100, massChannels.size());
        System.assertEquals(false,massChannels[0].Primary__c);
        System.assertEquals(false,massChannels[1].Primary__c);
        System.assertEquals(false,massChannels[98].Primary__c);
        System.assertEquals(true,massChannels[99].Primary__c);
        
    }*/
}