global class TMP_AnswerXF implements Database.Batchable<sObject>{
global String query;
public String Flag;
global TMP_AnswerXF ()
{
    
}
global Database.QueryLocator start(Database.BatchableContext BC) 
{
	//[select Contact__c from Answer__c where Questionnaire__r.Type__c ='Routine'  and CreatedDate >= 2014-08-03T00:00:01.000Z and CreatedDate <= 2014-08-09T00:00:01.000Z]
    return Database.getQueryLocator(query);
}
global void execute(Database.BatchableContext BC, List<Answer__c> ListAns) 
{
    Set<Id> Set_Contact = new Set<Id>();
	for(Answer__c ans : ListAns)
	{
		Set_Contact.add(ans.Contact__c);
	}
	System.debug('####################   '+Set_Contact.size());
	
	List<Answer__c> List_Del = new List<Answer__c>();
	for(Contact con : [select Id,(select Status__c,Questionnaire__c From Answers__r where Status__c ='Finish' and Questionnaire__r.Type__c ='Routine' order by CreatedDate asc ) from Contact where Id in: Set_Contact])
	{
		if(con.Answers__r.size()>=2)
		{
			Set<Id> Set_ans = new Set<Id>();
			Map<Id,List<Answer__c>> Map_QUES = new Map<Id,List<Answer__c>>();
			for(Answer__c ans : con.Answers__r)
			{
				if(Map_QUES.containsKey(ans.Questionnaire__c))
				{
					List<Answer__c> List_ans = Map_QUES.get(ans.Questionnaire__c);
					List_ans.add(ans);
					Map_QUES.put(ans.Questionnaire__c,List_ans);
				}
				else
				{
					List<Answer__c> List_ans = new List<Answer__c>();
					List_ans.add(ans);
					Map_QUES.put(ans.Questionnaire__c,List_ans);
				}
			}
			
			for(Id quesid : Map_QUES.KeySet())
			{
				if(Map_QUES.get(quesid).size()==1)
				continue;
				if(Map_QUES.get(quesid).size() == 2)
				{
					List_Del.add(Map_QUES.get(quesid)[1]);
					break;
				}
				else if(Map_QUES.get(quesid).size() == 3)
				{
					List_Del.add(Map_QUES.get(quesid)[1]);
					List_Del.add(Map_QUES.get(quesid)[2]);
					break;
				}
				else if(Map_QUES.get(quesid).size() == 4)
				{
					List_Del.add(Map_QUES.get(quesid)[1]);
					List_Del.add(Map_QUES.get(quesid)[2]);
					List_Del.add(Map_QUES.get(quesid)[3]);
					break;
				}
				else if(Map_QUES.get(quesid).size() == 5)
				{	
					List_Del.add(Map_QUES.get(quesid)[1]);
					List_Del.add(Map_QUES.get(quesid)[2]);
					List_Del.add(Map_QUES.get(quesid)[3]);
					List_Del.add(Map_QUES.get(quesid)[4]);
					break;
				}
				
				/*for(Answer__c ans : Map_QUES.get(quesid))
				{
					if(ans.Status__c == 'Draft')
					{
						List_Del.add(ans);
						break;
					}
				}注释去掉需改Sql 去掉 Status__c ='Finish'*/
			}
		}
	}
	System.debug('##################  '+List_Del.size());
	delete List_Del;
    
    
    
    
    
    /*Set<Id> Set_ConIds = new Set<Id>();
    for(Answer__c qt : ans)
    {
        Set_ConIds.add(qt.Contact__c);
    }
    List<Answer__c> List_An = new List<Answer__c>();
    for(Contact qt : [Select Id,(select Q008__c,Q008_Last__c,Q038__c,Q038_last__c,Q039_last__c,Q039__c,TS_Last__c,OwnerId
                     From Answers__r where Status__c ='Finish' AND Questionnaire__r.Type__c ='Routine' order by CreatedDate desc ) 
                     from Contact where Id in:Set_ConIds])
    {
        if(qt.Answers__r != null && qt.Answers__r.size()>=2)
        {
            if(qt.Answers__r[1].Q008__c != null || qt.Answers__r[1].Q038__c != null || qt.Answers__r[1].Q039__c != null)
            {
                qt.Answers__r[0].Q008_Last__c = qt.Answers__r[1].Q008__c;
                qt.Answers__r[0].Q038_last__c = qt.Answers__r[1].Q038__c;
                qt.Answers__r[0].Q039_last__c= qt.Answers__r[1].Q039__c;
                qt.Answers__r[0].TS_Last__c = qt.Answers__r[1].OwnerId;
                List_An.add(qt.Answers__r[0]);
            }
        }
    }
    if(List_An.size()>0)
    update List_An;    */    
    }
    
    global void finish(Database.BatchableContext BC)
    {
        
    }
}