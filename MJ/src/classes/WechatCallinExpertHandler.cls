/*Author:Mark
 *Date:20140924
 *Function:Expert chat ，在微信上点击专家聊天室按钮时由此Handler来处理
*/
public class WechatCallinExpertHandler extends WechatCallinMsgHandler
{
    public override void Handle(WechatCallinMsgPipelineContext Context)
    {
        WechatEntity.InEventMsg inEvent = (WechatEntity.InEventMsg)Context.InMsg;
        if(inEvent.EventKey != this.BindingInfo.Event_Key__c)
        {
            return;
        }
        WechatEntity.OutTextMsg outMsg = WechatEntity.GenerateOutMsg(Context.InMsg , '');
        outMsg.Content = WechatBusinessUtility.QueryRemindingByKey('RN019');
        Context.OutMsg = outMsg;
        /*string openId = Context.InMsg.FromUserName;
        string ti = WechatBusinessUtility.CheckWechatUserRelationshipByFields(openId);
        if(ti == null)
        {
            outMsg.Content = '您好，參加“專家聊天室”前需要您請先進行註冊或綁定，謝謝。<a href="'+WechatBusinessUtility.WECHAT_SITE+'WechatSMSVerificationPage?openId='+ openId +'">【註冊/綁定】</a>';
            Context.OutMsg = outMsg;
        }
        else if(ti == 'Non-Member')
        {
            WechatBusinessUtility.BirthCount bc = WechatBusinessUtility.GetBirthCertificateCount(openId);
            if(bc.birthCertificateCount > bc.hasBirthCount)
            {
                outMsg.Content = '您好，參加“專家聊天室”前需要您完成全部照片的上傳，謝謝。【請點“擊會員專區”->“上傳出生證明”】';
            }
            else
            {
                outMsg.Content = '您好，您的註冊信息及出生證明正在審核中，審核通過後我們將微信通知您，屆時您即可享受美贊臣的微信服務，謝謝。';
            }
            Context.OutMsg = outMsg;
        }
        else if(ti == 'Member')
        {
            WechatEntity.OutImageMsg outImage = new WechatEntity.OutImageMsg();
            outImage.ToUserName = inEvent.FromUserName;
            outImage.FromUserName = inEvent.ToUserName;
            outImage.CreateTime = datetime.now();
            outImage.MsgType = 'image';
            outImage.MediaId = 'gwB7ZjfgMv73VsNN777hn9igSZ4SeS3qz-dTtWqc5iu_JjOyp-koxn42YMquDf6c';
            
            WechatEntity.SvcTextMsg textMsg = new WechatEntity.SvcTextMsg();
            WechatEntity.Text out = new WechatEntity.Text();
            out.Content = '請打開給您推送的專家聊天室二維碼圖片，按照圖片中的說明加入群聊，進行互動！';
            textMsg.ToUser = inEvent.FromUserName;
            textMsg.MsgType =  WechatEntity.MESSAGE_TYPE_TEXT;
            textMsg.text = out;
            Context.OutMsg = outImage;
            
            string calloutMsg = WechatEntity.SerializeSvcRequestMsg(textMsg);
            system.debug('*********calloutMsg***********' + calloutMsg);
            Wechat_Callout_Task_Queue__c userInfoQueue = new Wechat_Callout_Task_Queue__c();
            userInfoQueue.Public_Account_Name__c = Context.PublicAccountName;
            userInfoQueue.Open_ID__c = inEvent.FromUserName;
            userInfoQueue.Type__c = WechatEntity.MESSAGE_TYPE_TEXT;
            userInfoQueue.Processor_Name__c = 'WechatCallOutSendTextMsgProcessor';
            userInfoQueue.Msg_Body__c = calloutMsg;
            WechatCalloutQueueManager.EnQueue(userInfoQueue);
        }*/
    }
}