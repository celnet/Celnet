/*
*绑定按钮：A+「智‧動‧情‧語」全發展
*/
public class WechatCallinWiseHandler extends WechatCallinMsgHandler
{
    public override void Handle(WechatCallinMsgPipelineContext Context)
    {
        WechatEntity.InEventMsg inEvent = (WechatEntity.InEventMsg)Context.InMsg;
        if(inEvent.EventKey != this.BindingInfo.Event_Key__c)
        {
            return;
        }
        string openId = Context.InMsg.FromUserName;
        WechatEntity.OutTextMsg outMsg = WechatEntity.GenerateOutMsg(Context.InMsg , '');
        string re = WechatBusinessUtility.CheckWechatUserRelationshipByFields(openId);
        outMsg.Content = WechatBusinessUtility.QueryRemindingByKey('RN031');
        Context.OutMsg = outMsg;
    }
}