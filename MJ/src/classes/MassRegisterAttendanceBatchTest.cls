/*Author:leo.bi@celnet.com.cn
 *Date:2014-5-9
 *function: The test case of MassRegisterAttendanceBatch
 *Apply To:CN
 */
@isTest(SeeAllData=true)
private class MassRegisterAttendanceBatchTest 
{
    static testMethod void myUnitTest() 
    {
    	system.runas(TestUtility.new_CN_User())
        {
	        String commenName = String.valueOf(DateTime.now());
	        //new campaign
	        Campaign c = new Campaign(); 
	        c.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Invitation' and SobjectType=:'Campaign'].id;
	        c.Name = commenName;
	        c.Description = 'Test Description';
	        c.IsActive = true;
	        insert c;
	        //new campaign invited
	        //new Account
			Account acc = new Account();
			acc.Name = commenName;
			acc.Last_Name__c =commenName; 
			acc.status__c = 'Mother';
			acc.Phone = '11000000000';
			insert acc;
	        //new Contact
	        Contact con = new Contact();
			con.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
			con.AccountId = acc.Id;
			con.LastName = commenName;
			con.Birthdate = Date.today().addDays(-5);
			insert con;
	        //new campainmember
	        CampaignMember cm = new CampaignMember();
	        cm.ContactId = con.Id;
	        cm.CampaignId = c.Id;
	        insert cm;
	        //new member uninvited
	        Account acc1 = new Account();
			acc1.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_HouseHold' and SobjectType=:'Account'].id;
			acc1.Name = commenName + '1';
			acc1.Last_Name__c =commenName + '1';
			acc1.status__c = 'Mother';
			acc1.Phone = '11100000000';
			insert acc1;
	        Contact uncon = new Contact();
			uncon.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
			uncon.AccountId = acc1.Id;
			uncon.LastName = commenName + '1';
			uncon.Birthdate = Date.today().addDays(-5);
			insert uncon;
	        //new lead
	        Id newLeadRecordTypeId = [select id from RecordType where DeveloperName=:'CN_Leads' and SobjectType=:'Lead'].id;
	        Lead newLead = new Lead();
			newLead.RecordTypeId = newLeadRecordTypeId;
			newLead.LastName = commenName;
			newLead.Birthday_1st_Child__c = Date.today().addDays(-5);
			newLead.Last_Name_1st_Child__c = commenName;
			newLead.Phone = '11200000000';
			insert newLead;
	        //Attendance
	        List<Attendance__c> list_Attendance = new List<Attendance__c>();
	        Attendance__c invited = new Attendance__c();
	        invited.Campaign__c = c.Id;
	        invited.Baby_Birthday__c = Date.today().addDays(-5);
	        invited.Phone__c = '11000000000';
	        list_Attendance.add(invited);  
	        Attendance__c uninvited = new Attendance__c();
	        uninvited.Campaign__c = c.Id;
	        uninvited.Phone__c = '11100000000';
	        uninvited.Baby_Birthday__c = Date.today().addDays(-5);
	        list_Attendance.add(uninvited);
	        Attendance__c attLead = new Attendance__c();
	        attLead.Campaign__c = c.Id;
	        attLead.Baby_Birthday__c = Date.today().addDays(-5);
	        attLead.Phone__c = '11200000000';
	        list_Attendance.add(attLead);
	        Attendance__c attUnlead = new Attendance__c();
	        attUnlead.Campaign__c = c.Id;
	        attUnlead.Baby_Birthday__c = Date.today().addDays(-4);
	        attUnlead.Phone__c = '11400000000';
	        attUnlead.Invitation_Channel__c = 'Other';
	        attUnlead.Is_Pregnancy__c = false;
	        attUnlead.Mother_Name__c = 'Mother1';
	        attUnlead.Using_Product__c = 'MJN';
	        list_Attendance.add(attUnlead);
	        insert list_Attendance;
	        //excecute batch
	        MassRegisterAttendanceBatch mab = new MassRegisterAttendanceBatch(c.Id);
	        DataBase.executeBatch(mab);
        }
    }
    
    static testMethod void testSpecialInvitation() 
    {
    	system.runas(TestUtility.new_CN_User())
        {
	        String commenName = String.valueOf(DateTime.now());
	        //new campaign
	        Campaign c = new Campaign(); 
	        c.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Invitation' and SobjectType=:'Campaign'].id;
	        c.Name = commenName;
	        c.Description = 'Test Description';
	        c.IsActive = true;
	        c.Campaign_Type__c = 'Invitation Special';
	        insert c;
	        //new campaign invited
	        //new Account
			Account acc = new Account();
			acc.Name = commenName;
			acc.Last_Name__c =commenName; 
			acc.status__c = 'Mother';
			acc.Phone = '11000000000';
			insert acc;
	        //new Contact
	        Contact con = new Contact();
			con.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
			con.AccountId = acc.Id;
			con.LastName = commenName;
			con.Birthdate = Date.today().addDays(-5);
			insert con;
	        //new campainmember
	        CampaignMember cm = new CampaignMember();
	        cm.ContactId = con.Id;
	        cm.CampaignId = c.Id;
	        insert cm;
	        //new member uninvited
	        Account acc1 = new Account();
			acc1.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_HouseHold' and SobjectType=:'Account'].id;
			acc1.Name = commenName + '1';
			acc1.Last_Name__c =commenName + '1';
			acc1.status__c = 'Mother';
			acc1.Phone = '11100000000';
			insert acc1;
	        Contact uncon = new Contact();
			uncon.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
			uncon.AccountId = acc1.Id;
			uncon.LastName = commenName + '1';
			uncon.Birthdate = Date.today().addDays(-5);
			insert uncon;
	        //new lead
	        Id newLeadRecordTypeId = [select id from RecordType where DeveloperName=:'CN_Leads' and SobjectType=:'Lead'].id;
	        Lead newLead = new Lead();
			newLead.RecordTypeId = newLeadRecordTypeId;
			newLead.LastName = commenName;
			newLead.Birthday_1st_Child__c = Date.today().addDays(-5);
			newLead.Last_Name_1st_Child__c = commenName;
			newLead.Phone = '11200000000';
			insert newLead;
	        //Attendance
	        List<Attendance__c> list_Attendance = new List<Attendance__c>();
	        Attendance__c invited = new Attendance__c();
	        invited.Campaign__c = c.Id;
	        invited.Baby_Birthday__c = Date.today().addDays(-5);
	        invited.Phone__c = '11000000000';
	        invited.Special_Product__c = 'GC';
	        invited.Purchase_Date__c = Date.today();
	        list_Attendance.add(invited);  
	        Attendance__c uninvited = new Attendance__c();
	        uninvited.Campaign__c = c.Id;
	        uninvited.Phone__c = '11100000000';
	        uninvited.Baby_Birthday__c = Date.today().addDays(-5);
	        uninvited.Special_Product__c = 'GC';
	        uninvited.Purchase_Date__c = Date.today();
	        list_Attendance.add(uninvited);
	        Attendance__c attLead = new Attendance__c();
	        attLead.Campaign__c = c.Id;
	        attLead.Baby_Birthday__c = Date.today().addDays(-5);
	        attLead.Phone__c = '11200000000';
	        attLead.Special_Product__c = 'GC';
	        attLead.Purchase_Date__c = Date.today();
	        list_Attendance.add(attLead);
	        Attendance__c attUnlead = new Attendance__c();
	        attUnlead.Campaign__c = c.Id;
	        attUnlead.Baby_Birthday__c = Date.today().addDays(-4);
	        attUnlead.Phone__c = '11400000000';
	        attUnlead.Invitation_Channel__c = 'Other';
	        attUnlead.Is_Pregnancy__c = false;
	        attUnlead.Special_Product__c = 'GC';
	        attUnlead.Purchase_Date__c = Date.today();
	        attUnlead.Mother_Name__c = 'Mother1';
	        attUnlead.Using_Product__c = 'MJN';
	        list_Attendance.add(attUnlead);
	        insert list_Attendance;
	        //excecute batch
	        MassRegisterAttendanceBatch mab = new MassRegisterAttendanceBatch(c.Id);
	        DataBase.executeBatch(mab);
        }
    }
}