/*
*Author:michael.mi@celnet.com.cn
*Date:2014-09-05
*Function:从微信服务器下载由用户上传的出生证明文件
*/
public class WechatCalloutGetImageProcessor extends WechatCalloutProcessor
{
	public string openId;
	public string publicAccountName;
	
	public override void DoCallout(Wechat_Callout_Task_Queue__c WechatTask)
	{
		openId = WechatTask.Open_ID__c;
		publicAccountName = WechatTask.Public_Account_Name__c;
		system.debug('*************Media_ID*****************' + WechatTask.Media_ID__c);
		WechatCalloutService wcs = new WechatCalloutService(WechatTask.Public_Account_Name__c);
        List<Wechat_User__c> wu = [select Binding_Non_Member__r.Baby_Count__c , Binding_Non_Member__r.Attachment_Count__c , Binding_Non_Member__c , Name from Wechat_User__c  where Open_Id__c =:WechatTask.Open_ID__c];
        if(wu.size() != 1) return;
        if(wu[0].Binding_Non_Member__c == null)return;
        system.debug('-------------Media_ID-----------------' + WechatTask.Media_ID__c);
        
        if(decimal.valueof(wu[0].Binding_Non_Member__r.Baby_Count__c) == wu[0].Binding_Non_Member__r.Attachment_Count__c)return;
        
		Attachment att = WechatFieldMatch.GenerateAttachment(wcs.DowloadMedia(WechatTask.Media_ID__c));
        att.Name =  wu[0].Name + DateTime.now().format('yyyy-MM-dd HH:mm:ss', 'Asia/Shanghai');
        att.ParentId = wu[0].Binding_Non_Member__c;
        ObjectList.add(att);
	}
	
	public override void FinishData()
	{
		system.debug('***********************ObjectList***********************' + ObjectList);

		WechatEntity.SvcTextMsg outMsg = new WechatEntity.SvcTextMsg();
        WechatEntity.Text out = new WechatEntity.Text();
       
    	if(this.ObjectList != null && ObjectList.size() >0)
		{
			insert ObjectList;	
		}
		
		List<Wechat_User__c> wuList = [select id , Binding_Non_Member__c from Wechat_User__c where open_Id__c = : openId];
		if(!wuList.isempty())
		{
			List<Lead> nonList = [Select Supporting_Document_4th_child__c, Supporting_Document_3rd_child__c, Supporting_Document_2nd_child__c, Supporting_Document_1st_child__c, 
								 (Select Id From Attachments) From Lead where id = :wuList[0].Binding_Non_Member__c ];
			if(!nonList.isempty())
			{
				Lead l = nonList[0];
				integer i = 0;
				if(i < l.Attachments.size())
				{
					l.Supporting_Document_1st_child__c = 'https://c.ap1.content.force.com/servlet/servlet.FileDownload?file=' + l.Attachments[i].id;	
					i++;
				}
				if(i < l.Attachments.size())
				{
					l.Supporting_Document_2nd_child__c = 'https://c.ap1.content.force.com/servlet/servlet.FileDownload?file=' + l.Attachments[i].id;	
					i++;
				}
				if(i < l.Attachments.size())
				{
					l.Supporting_Document_3rd_child__c = 'https://c.ap1.content.force.com/servlet/servlet.FileDownload?file=' + l.Attachments[i].id;	
					i++;
				}
				if(i < l.Attachments.size())
				{
					l.Supporting_Document_4th_child__c = 'https://c.ap1.content.force.com/servlet/servlet.FileDownload?file=' + l.Attachments[i].id;	
					i++;
				}

				update l;
			}
		}
		
		WechatBusinessUtility.BirthCount bc = WechatBusinessUtility.GetBirthCertificateCount(openId);
		
		if((bc.birthCertificateCount - bc.hasBirthCount) == 0)
		{
			if(!this.ObjectList.isempty())
			{
				out.content = WechatBusinessUtility.QueryRemindingByKey('RN012');
			}
		}
		else 
    	{
    		list<object> replaceKeywords = new list<object>();
			replaceKeywords.add(bc.hasBirthCount);
			replaceKeywords.add(bc.birthCertificateCount - bc.hasBirthCount);
			out.content = WechatBusinessUtility.QueryRemindingByKey('RN013',replaceKeywords);
    	}

        if(out.content != null)
        {
        	outMsg.touser = openId;
	        outMsg.text = out;
	        outMsg.msgtype = 'text';
        	string msgJson = WechatEntity.SerializeSvcRequestMsg(outMsg);
        	Wechat_Callout_Task_Queue__c textQueue = new Wechat_Callout_Task_Queue__c();
			textQueue.Public_Account_Name__c = publicAccountName;
			textQueue.Type__c = WechatEntity.MESSAGE_TYPE_TEXT;
			textQueue.Msg_Body__c = msgJson;
			textQueue.Processor_Name__c = 'WechatCallOutSendTextMsgProcessor';
			textQueue.Name = 'WechatCallOutSendTextMsgProcessor';
			system.debug('***********************imageQueue***********************' + textQueue);
			WechatTaskList.add(textQueue);
        }
	}
	
}