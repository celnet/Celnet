public without sharing class ContactUpdateHelper{

    static final Map<String,Boolean> statusCloseMap;
    
    static
    {
        if(statusCloseMap == NULL || statusCloseMap.isEmpty())
        {
            List<TaskStatus> taskStatusList = [Select Id, MasterLabel, IsClosed FROM TaskStatus];
            statusCloseMap = new Map<String,Boolean>();
            for (TaskStatus taskStatus:taskStatusList)
               statusCloseMap.put(taskStatus.MasterLabel,taskStatus.IsClosed);
        }
    }
    
    public static void updateBabyCallLog(List<ID> affectedIDs)
    {
       List<Contact> babyList =
            [SELECT ID,Finished_Calls__c,Continue_to_use__c,Willing_to_Try__c FROM contact WHERE ID in :affectedIDs];
        
       if(babyList != NULL && !babyList.isEmpty())
       {
           //Define map for supporing bulk processing
           Map<ID,List<Task>> babyCallLogsMap = new Map<ID,List<Task>>();
           for(Contact baby : babyList)
               babyCallLogsMap.put(baby.ID, new List<Task>());
           
           //Retieve all related tasks : task.OB_Call_List__c != NULL 
		   List<Task> taskList = 
                [SELECT ID,WhoId,CreatedDate,OB_Call_List__c,Status,Willing_to_Try__c,Continue_to_use__c
                 FROM TASK 
                 WHERE WhoId in :babyList AND OB_Call_List__c != NULL
                 ORDER BY CreatedDate ASC];
           
           //Group the tasks to each baby
           for(Task task : taskList)
               babyCallLogsMap.get(task.WhoId).add(task);
           
           //Processing update for each baby record
           for(Contact baby : babyList)
               baby = updateBabyCallLog (baby,babyCallLogsMap.get(baby.ID));
           
           update babyList;
       }
    }
    
    public static Contact updateBabyCallLog(Contact baby, List<Task> taskList)
    {
        String newFinishedCall = '';
        String newWillToTry = '';
        String newContinueToUse = '';
        
        //For each OBCall value, it can only have one status: either "Open" or "Closed"
        Map<String,String> finishedCallMap = new Map<String,String>();
        Map<String,String> willingToTryMap = new Map<String,String>();
        Map<String,String> continueToUseMap = new Map<String,String>();
        
        for(Task task : taskList)
        {   
            //Set finished call
            Boolean isClosed = isTaskClosed(task.Status);
            String callResult = mapCallResult(isClosed);
            
            if(finishedCallMap.get(task.OB_Call_List__c) == NULL || finishedCallMap.get(task.OB_Call_List__c) != 'Closed')
                finishedCallMap.put(task.OB_Call_List__c,callResult);
            
            //Set Willing to Try only when the call is "Connected" - The later task will override previous value
            if(task.Willing_to_Try__c != NULL && task.Status == CommonHelper.CALL_CONNECTED)
            	willingToTryMap.put(task.OB_Call_List__c,task.Willing_to_Try__c);
            
            //Set Continue to Use only when the call is "Connected" - The later task will override previous value
            if(task.Continue_to_use__c != NULL && task.Status == CommonHelper.CALL_CONNECTED)
            	continueToUseMap.put(task.OB_Call_List__c,task.Continue_to_use__c);
        }
        
        //Generate Finished Call String
        for(String key : finishedCallMap.keySet())
            newFinishedCall += key + '--' + finishedCallMap.get(key)+';';
        
        //Generate Willing To Try String
        for(String key : willingToTryMap.keySet())
            newWillToTry += key + '--' + willingToTryMap.get(key)+';';
        
        //Generate Continue to Use String
        for(String key : continueToUseMap.keySet())
            newContinueToUse += key + '--' + continueToUseMap.get(key)+';';
        
        //Set new values
        baby.Finished_Calls__c = newFinishedCall;
        baby.Willing_to_Try__c = newWillToTry;
        baby.Continue_to_use__c = newContinueToUse;
        
        return baby;
    }
    
    public static String mapCallResult (Boolean isClosed)
    {
         String callResult;
         if(isClosed == NULL)
            callResult = 'NA';
         else if(isClosed)
            callResult = 'Closed';
         else
            callResult = 'Open';
        
        return callResult;
    }
    
    public static Boolean isTaskClosed (String status)
    {
         return statusCloseMap.get(status);
    }
}