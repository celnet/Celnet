/*
Author: scott.zhang@celnet.com.cn
Created On: 2014-05-11
Function: Delete Q-Task that is Finished and Closed.Run at every Night 
Apply To: CN
*/
global class RoutineQTaskDeletionBatch implements Database.Batchable<sObject>,Schedulable{
	private final Set<String> Set_Status = new Set<String>{'Closed','Finished'};
	global void execute(SchedulableContext sc) 
	{
		RoutineQTaskDeletionBatch  QTaskDeletionBatch  = new RoutineQTaskDeletionBatch();
		Database.executeBatch(QTaskDeletionBatch,200);  
	}
	global Database.QueryLocator start(Database.BatchableContext BC) 
	{
		String status ;
		for(String sta : Set_Status)
		{
			if(status==null)
			{	
				status = '\''+sta+'\'';
			}	
			else
			{
				status += ',\''+sta+'\'';
			}
		}
		String query = 'Select Id from Q_Task__c where Status__c in('+status+') or Contact__c=null';
		return Database.getQueryLocator(query);
	}
	global void execute(Database.BatchableContext BC, List<Q_Task__c> scope) 
	{
		delete scope;
	}
	global void finish(Database.BatchableContext BC)
	{
    	
  	}
}