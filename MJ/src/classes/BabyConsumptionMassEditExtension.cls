/* Delete function commented
*/
public class BabyConsumptionMassEditExtension {

    private List<Baby_Consumption__c> relatedConsumptions;
    private List<Baby_Consumption__c> originalList;
    // Delete function commented
    //private Map<ID,Integer> idMap;
    private final ID contactID;
    private final Contact relatedBaby;
    
    private static List<Product__c> productList;
    private static List<SelectOption> productSelectOptions;
     
    static {
    	if(productList == NULL)
        {
            productList = CommonHelper.getBabyConsumptionProducts();
            productSelectOptions = new List<SelectOption>();
            //Initialise the Product Drop Down Selection List
            productSelectOptions.add(new SelectOption('','--Please select a Product--'));
            for(Product__c product : productList)
            {
                productSelectOptions.add(new SelectOption(product.ID,product.Name));
            }
        }
    }

    public BabyConsumptionMassEditExtension(ApexPages.StandardSetController controller) 
    {
        contactID= ApexPages.currentPage().getParameters().get('id');
        List<Contact> babies = [SELECT ID,Name,Birthdate,Age__c FROM Contact WHERE ID = :contactID];

        if(babies == NULL || babies.isEmpty())
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Baby record not found. Please make sure you are accessing from the right place!'));
        }
        else
        {
            relatedBaby = babies[0];
            relatedConsumptions = [SELECT ID,Product__c,From_Age__c,To_Age__c,CreatedBy.Name,CreatedDate,LastModifiedBy.Name,LastModifiedDate
                            FROM Baby_Consumption__c WHERE Contact__c = :contactID ORDER BY From_Age__c ASC];
            
            if(relatedConsumptions == NULL)
                relatedConsumptions = new List<Baby_Consumption__c>();
            
            //If number of existing records < 5, then set to 5
            addNewLines(5 - relatedConsumptions.size());
             
            originalList = relatedConsumptions.deepClone(true,true,true); 
            
            /* Delete function commented
            idMap = new Map<ID,Integer>();
            for(Integer count=0; count<originalList.size(); count++)
                idMap.put(originalList.get(count).ID, count);
            */
        }
    }
    
    public List<SelectOption> getProductOptions() {
        return productSelectOptions;
    }
    
    public List<Baby_Consumption__c> getRelatedConsumptions() {
        return relatedConsumptions;
    }
    
    public Contact getRelatedBaby() {
        return relatedBaby;
    }
    
    public PageReference save() {
        List<Baby_Consumption__c> newList = new List<Baby_Consumption__c>();
        List<Baby_Consumption__c> updateList = new List<Baby_Consumption__c>();
        //List<Baby_Consumption__c> delList = new List<Baby_Consumption__c>();
        
        for(Baby_Consumption__c line : relatedConsumptions)
        {
            if(line.ID == NULL) //New consumption records
            {
               if(line.Product__c == NULL && line.From_Age__c == NULL && line.To_Age__c == NULL)
               {
                   //Ignore empty lines - do nothing
               }
               else{
                   if(line.Product__c == NULL)
                       line.Product__c.addError(CommonHelper.PRODUCT_REQUIRED);
                   if(line.From_Age__c == NULL)
                       line.From_Age__c.addError(CommonHelper.VALUE_REQUIRED);
                   if(line.To_Age__c == NULL)
                       line.To_Age__c.addError(CommonHelper.VALUE_REQUIRED);
                 
                   line.Contact__c = contactID;
                   newList.add(line);
               }
            }
            else //Existing consumption records
            {  
                //validate Product is not NULL
                if(line.Product__c == NULL)
                    line.Product__c.addError(CommonHelper.PRODUCT_REQUIRED);
                //validate From Age is not NULL
                if(line.From_Age__c == NULL)
                    line.From_Age__c.addError(CommonHelper.VALUE_REQUIRED);
                //validate To Age is not NULL
                if(line.To_Age__c == NULL)
                    line.To_Age__c.addError(CommonHelper.VALUE_REQUIRED);
                
               updateList.add(line);
               //idMap.remove(line.ID);
            }
        }
        
        //Generate delete lines - Delete function commented
        /*
        if(!idMap.isEmpty())
        {
            System.debug('==========>Some existing consumption records are deleted!');
            List<Integer> deletedIndexes= new List<Integer>();
            deletedIndexes = idMap.values();
            for(Integer delIndex : deletedIndexes)
                delList.add(originalList.get(delIndex));
        } 
        */
 		
        //If there is any validation error, return to the form page
        if(ApexPages.hasMessages())
            return null;
        
        try
        {
            if(!newList.isEmpty())
                insert newList;
            if(!updateList.isEmpty())
                update updateList;
            /*
            if(!delList.isEmpty())
                delete delList;
            */
        }
        catch(DmlException ex){
            ApexPages.addMessages(ex);
            return null;
        }
        //  After Save, navigate to the Contact Detail view page:
        return new PageReference('/'+contactID);  
    }
    
   public PageReference cancel() {
        //  Return to the Contact Detail page:
        return new PageReference('/'+contactID);  
    }
    
    public PageReference reset() {
        relatedConsumptions = originalList.deepClone(true,true,true);
        return null; 
    }
    
    //Add 3 lines in one click
    public PageReference addNew() {
        addNewLines(3);
        return null;  
    }
   
    private void addNewLines(Integer numerOfLines) {
        for(Integer count=0; count<numerOfLines; count++)
        {
            Baby_Consumption__c newRecord = new Baby_Consumption__c();
            relatedConsumptions.add(newRecord);
        }
    }
    /* Delete function commented
   public PageReference deleteLine() {
        Integer index = Integer.valueOf(ApexPages.currentPage().getParameters().get('curIndex'));
        relatedConsumptions.remove(index);
        return null;  
    }
    */
}