/*
Author: scott.zhang@celnet.com.cn
Created On: 2014-04-23
Function: QuestionnaireController test class
Apply To: CN
*/
@isTest
private class QuestionnaireController_Test 
{
    static testMethod void testRest()
    {
    	system.runAs(TestUtility.new_CN_User())
		{
	        Questionnaire__c Questionnaire = creatQuestionnaire('Other', 'Second Invitation', 'CS');
	        Contact con = createContact();
		}
    }
    static testMethod void myUnitTest()
    {
    	system.runAs(TestUtility.new_CN_User())
		{
	        Questionnaire__c Questionnaire = creatQuestionnaire('Other', 'Second Invitation', 'PS');
	        Contact con = createContact();
	       
	        Contact con11 = createContact();
	        Contact con12 = createContact();
	       
	        //Answer
	        Answer__c ans = new Answer__c();
	        ans.Q001__c = '美赞臣';
	        ans.Contact__c = con.Id;
	        ans.Questionnaire__c = Questionnaire.Id;
	        insert ans;
	       
	       
	       
	        ApexPages.StandardController STController = new ApexPages.StandardController(new Questionnaire__c());
	        QuestionnaireController QuesCon1 = new QuestionnaireController(STController);
	        PageReference cancel = QuesCon1.Cancel();
	       
	        ApexPages.currentPage().getParameters().put('id', Questionnaire.Id);
	        QuestionnaireController QuesCon = new QuestionnaireController(STController);
	        cancel = QuesCon.Cancel();
	       
	        ApexPages.currentPage().getParameters().put('contactid', con.Id);
	        QuestionnaireController QuesCon2 = new QuestionnaireController(STController);
	        QuesCon2.SaveAnswer();
	        QuesCon2.Answer.Status__c = 'Finish';
	        QuesCon2.Answer.Q023__c = '1';
	        QuesCon2.List_Questionnaire[0].AnswerType = 'Date';
	        QuesCon2.List_Questionnaire[0].AnswerFieldValue = '2001090';
	        String requiredValidation = QuesCon2.RequiredValidation(null , QuesCon2.List_Questionnaire[0]);  
	        requiredValidation = QuesCon2.RequiredValidation('11' , QuesCon2.List_Questionnaire[0]);
	        QuesCon2.List_Questionnaire[0].Control_Question_NO = 'Q023,Q023,Q023';
	        QuesCon2.List_Questionnaire[0].Control_Question_Value = '1,2,3';
	        QuesCon2.List_Questionnaire[0].Control_Type = 'Required,Disabled,Required'; 
	        requiredValidation = QuesCon2.ControlQuestionValidation('11',QuesCon2.List_Questionnaire[0]);
	        QuesCon2.List_Questionnaire[0].Control_Type = 'Required,Disabled'; 
	        requiredValidation = QuesCon2.ControlQuestionValidation('11',QuesCon2.List_Questionnaire[0]);   
	       
	        QuesCon2.Answer.Status__c = 'Finish';
	        QuesCon2.Answer.Q033__c = 'Y';
	        QuesCon2.Answer.Contact__c = con.Id;
	        QuesCon2.SaveAnswer();
	        QuesCon2.Answer.Q034__c = 'Y';
	        QuesCon2.SaveAnswer();
	        cancel = QuesCon2.Cancel(); 
	        Test.startTest();
	        ApexPages.currentPage().getParameters().put('answerid', ans.Id);
	        QuestionnaireController QuesCon3 = new QuestionnaireController(STController);
	        cancel = QuesCon3.Cancel();
	        QuesCon3.SelectQuestionnaireId = Questionnaire.Id;
	        Id campaignRecordTypeId = [select id from RecordType where DeveloperName=:'CN_Invitation' and SobjectType=:'Campaign'].id;
	        Campaign c = createCampaign('Other', campaignRecordTypeId,'Second Invitation');
	        CampaignMember cm = createCampaignMember(c, con, 'Invitation Reciprocal');
	        ApexPages.currentPage().getParameters().clear();
	        ApexPages.currentPage().getParameters().put('isSecondInvitation','Y');
	        ApexPages.currentPage().getParameters().put('memberid',cm.Id);
	        QuestionnaireController QuesCon4 = new QuestionnaireController(STController);
	        ApexPages.currentPage().getParameters().clear();
	        
	        
	        CampaignMember cm11 = createCampaignMember(c, con11, 'Waiting Invitation');
	        ApexPages.currentPage().getParameters().put('memberid',cm11.Id);
	        QuestionnaireController QuesCon5 = new QuestionnaireController(STController);
	        QuesCon5.SaveAnswer();
	        QuesCon5.showItem();
	        
	        Campaign c2 = createCampaign('Other', campaignRecordTypeId,'Second Invitation');
	        c2.Nature__c = null;
	        update c2;
	        CampaignMember cm2 = createCampaignMember(c2, con, 'Invitation Reciprocal');
	        ApexPages.currentPage().getParameters().clear();
	        ApexPages.currentPage().getParameters().put('isSecondInvitation','Y');
	        ApexPages.currentPage().getParameters().put('memberid',cm2.Id);
	        QuestionnaireController QuesCon41 = new QuestionnaireController(STController);
	        
	        //create fullfillment
	        Id fullfillmentId = QuesCon5.CreateFulfillment(con.Id, 'testfull');
	        campaignRecordTypeId = [select id from RecordType where DeveloperName=:'CN_Invitation_Special' and SobjectType=:'Campaign'].id;
	        Campaign c1 = createCampaign('Other', campaignRecordTypeId,'Second Invitation');     
	        CampaignMember cm1 = createCampaignMember(c1, con, 'Invitation Reciprocal');
	        ApexPages.currentPage().getParameters().clear();
	        ApexPages.currentPage().getParameters().put('memberid',cm1.Id);
	        QuestionnaireController QuesCon6 = new QuestionnaireController(STController);  
	        Test.stopTest();
		}
    }
    
    
    
    private static Campaign createCampaign(String questionnaireType, String recordType, String campaignType) 
    {
        Campaign c = new Campaign();
        c.Name = 'Test Campaign';
        c.RecordTypeId = recordType;
        c.Campaign_Type__c = campaignType;
        c.Nature__c = 'Regular';
        insert c;
        return c;
    }
    
    private static CampaignMember createCampaignMember(Campaign c, Contact con,String callStatus)
    {
        CampaignMember cm = new CampaignMember();
        cm.CampaignId = c.Id;
        cm.ContactId = con.Id;
        cm.Call_Status__c = callStatus;
        insert cm;
        return cm;
    }
    
    private static Questionnaire__c creatQuestionnaire(String questionnaireType, String businessType, String invitationType)
    {
        //Questionnaire
       Questionnaire__c Questionnaire = new Questionnaire__c();
       Questionnaire.Name='Questionnaire Name';
       Questionnaire.Type__c = questionnaireType;
       Questionnaire.Business_Type__c = businessType;
       Questionnaire.Invitation_Type__c = invitationType;
       insert Questionnaire;
        //Question__c
       Question__c ques = new Question__c();
       ques.Name='Q001-您现在是以什么方式喂养宝宝的？';
       ques.NO__c = 'Q001';
       ques.Answer_Type__c = 'Picklist';
       insert ques;
       Question__c ques2 = new Question__c();
       ques2.Name='Q002-妈妈现在是给宝宝添加什么品牌的奶粉？';
       ques2.NO__c = 'Q002';
       ques2.Answer_Type__c = 'Picklist';
       insert ques2;
       Question__c ques3 = new Question__c();
       ques3.Name='Q004-是否有购买MJ？';
       ques3.NO__c = 'Q004';
       ques3.Answer_Type__c = 'Boolean';
       insert ques3;
       
       //Questionnaire Line Item
       Questionnaire_Line_Item__c qli = new Questionnaire_Line_Item__c();
       qli.Question__c = ques.Id;
       qli.Questionnaire__c = Questionnaire.Id;
       qli.Has_Remark__c = true;
       qli.Order__c = 1;
       insert qli;
       Questionnaire_Line_Item__c qli2 = new Questionnaire_Line_Item__c();
       qli2.Question__c = ques2.Id;
       qli2.Questionnaire__c = Questionnaire.Id;
       qli2.Has_Remark__c = true;
       qli2.Order__c = 2;
       qli2.Control_Question_NO__c = '1';
       qli2.Control_Question_Value__c = '美赞臣';
       qli2.Control_Type__c = 'Required';
       insert qli2;
       
       Questionnaire_Line_Item__c qli3 = new Questionnaire_Line_Item__c();
       qli3.Question__c = ques3.Id;
       qli3.Questionnaire__c = Questionnaire.Id;
       qli3.Has_Remark__c = true;
       qli3.Order__c = 2;
       qli3.Control_Question_NO__c = '1';
       qli3.Control_Question_Value__c = '美赞臣';
       qli3.Control_Type__c = 'Required';
       insert qli3;
       return Questionnaire;
    }
    
    private static Contact createContact()
    {
        //Account 
       Account acc = new Account();
       acc.Name = 'Acc Name';
       acc.RecordTypeId = ObjectUtil.GetRecordTypeID(Account.getSObjectType(), 'CN_HouseHold');
       insert acc;
       //Contact
       Contact con = new Contact();
       con.AccountId = acc.Id;
       con.RecordTypeId = ObjectUtil.GetRecordTypeID(Contact.getSObjectType(), 'CN_Child');
       con.LastName = 'Con Name';
       insert con;
       return con;
    }
}