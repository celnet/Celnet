/*
*Author:michael.mi@celnet.com.cn
*Date:2014-09-04
*Function:create all handler instance
*/
public class WechatCallinMsgPipeline 
{
	private list<WechatCallinMsgHandler> HandlerList;
	public WechatEntity.CallinBaseMsg OutMsg 
	{
		get
		{
			return this.Context.OutMsg;
		}
	}
	private WechatCallinMsgPipelineContext Context;
	
	public WechatCallinMsgPipeline(WechatEntity.CallinBaseMsg InMsg,string PublicAccountName)
	{
		this.Context = new WechatCallinMsgPipelineContext(InMsg,PublicAccountName);
		this.GetHandlerInstance();
	}
	  
	private void GetHandlerInstance()
	{
		
		this.HandlerList = new list<WechatCallinMsgHandler>();
		for(Wechat_Handler_Binding__c setting : [select Active__c,Execute_Order__c,
												 Handle_Msg_Type__c,Handler_Name__c,
												 Public_Account_Name__c,Name , Event_Key__c
												 from Wechat_Handler_Binding__c 
												 where Active__c = true and Handle_Msg_Type__c = :this.Context.InMsg.MsgType
												 and Public_Account_Name__c = :this.Context.PublicAccountName
												 order by Execute_Order__c asc])
		{
			system.debug('***********MsgType****************' + this.Context.InMsg.MsgType);
			system.debug('!!!!!!!!!!!!!!!!!!!!' + setting);
			type t = type.forname(setting.Handler_Name__c);
			WechatCallinMsgHandler handler = (WechatCallinMsgHandler)t.newInstance();
			handler.BindingInfo = setting;
			HandlerList.add(handler);
		}
	}
	
	public void execute()
	{
		for(WechatCallinMsgHandler handler : this.HandlerList)
		{
			handler.Handle(this.Context);
		}
	}
}