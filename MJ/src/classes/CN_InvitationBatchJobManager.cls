/*Author:leo.bi@celnet.com.cn
 *Date:2014-06-25
 *Function:the job manager of invitation job 
 */
public class CN_InvitationBatchJobManager
{
	private Integer runningCount;//the running batch count now
	private Integer limitCount;//the limited running batch number
	private Integer availableCount;
	private Integer remainingJobCount;
	private Invitation__c parentInvitation;
	List<Invitation__c> list_Invitation = new List<Invitation__c>(); 
	public CN_InvitationBatchJobManager(Integer limitPara)
	{
		this.limitCount = limitPara;
	}
	
	public void execute()
	{
		//1.If there is no invitation to execute, return.
		list_Invitation = [select Id , Answer_SOQL_Script__c, Campaign__c, SOQL_Script__c, Status__c, Task_Call_Result__c, Task_End_Time__c, Tast_Start_Time__c 
						   from Invitation__c 
						   where Status__c ='Queued' order by CreatedDate asc];
		system.debug('---------------list_Invitation.size()----------------' +list_Invitation.size());
		if(list_Invitation.isEmpty())
		{
			return;
		} 
		//2.If has unexecuted invitation job
		//2.1check running "Batch Apex" type Job
		this.runningCount = this.checkRunningBatch();
		system.debug('----------------runningCount---------------------------' + runningCount);
		//2.2check available batch executing count
		this.availableCount = this.limitCount - this.runningCount;
		system.debug('----------------availableCount---------------------------' + availableCount);
		if(this.availableCount >= 1)
		{
			//3.If there are unexecuted invitation and availableCount > 0
			//execute invitation batch
			system.debug('--------------------------------this.executeMassInvitationBatch----------------------');
			remainingJobCount = this.executeMassInvitationBatch();
			system.debug('----------------------remainingJobCount-------------------------------' + remainingJobCount);
			if(remainingJobCount > 0)
			{
				system.debug('---------------has remaining invitation---------reschedule----------------------');
				this.reschedule();
			}
		}
		else
		{
			//4.If there are unexecuted invitation and availableCount < 0
			//reschedule
			system.debug('---------------no available---------reschedule----------------------');
			this.reschedule();
		} 
	}
	
	/*Run a scheduler if has remaining invitation jobs to run(keep only one in system)
	 *If there are unused scheduler, abort them and the the job will disappear
	 *If there are running scheduler, do not create new scheduler
	 */
	private void reschedule()
	{
		List<CronTrigger> list_CronTrigger =   [select Id, NextFireTime from CronTrigger where CronJobDetail.Name like 'CN_InvitationBatchJobScheduler%'];
		List<CronTrigger> list_Running_Scheduler = new List<CronTrigger>();
		for(CronTrigger ct : list_CronTrigger)
		{
			if(ct.NextFireTime == null)
			{//the unuse job amount is not large
				system.debug('----------------------------abort unused scheduler----------------------');
				system.abortJob(ct.Id);
			}
			else
			{
				list_Running_Scheduler.add(ct);
			}
		}
		if(!list_Running_Scheduler.isEmpty())
		{
			system.debug('----------------------------has running scheduler----------------------');
			return;
		}
		system.debug('----------------------------new----scheduler----------------------');
		CN_InvitationBatchJobScheduler bs = new CN_InvitationBatchJobScheduler(this.limitCount);
		DateTime newDT = DateTime.now().addMinutes(10);
		String sch = '0 ' + String.valueOf(newDT.minute()) + ' '
						  + String.valueOf(newDT.hour()) + ' ' + String.valueOf(newDT.day()) + ' '
						  + String.valueOf(newDT.month()) + ' ? ' + String.valueOf(newDT.year());
		system.schedule('CN_InvitationBatchJobScheduler' + String.valueOf(DateTime.now()), sch, bs);
	}
	
	
	
	//count "Batch Apex" Type job count whose status is in ('Queued' ,'Processing','Preparing')
	private Integer checkRunningBatch()
	{
		Integer result = 0;
		//only batch apex limit is 5, so only count Batch Apex number
		List<AsyncApexJob> list_AsyncApexJob = [Select Status, Id From AsyncApexJob where JobType='BatchApex' and Status in ('Queued' ,'Processing','Preparing')]; 
		if(list_AsyncApexJob.size() > 0)
		{
			result = Integer.valueOf(list_AsyncApexJob.size());
		}
		return result;
	}
	
	//execute mass invitation job
	private Integer executeMassInvitationBatch()
	{
		Integer result = list_Invitation.size();
		Integer i = 0;
		while(i < list_Invitation.size() && i < this.availableCount)
		{
			i++;
			result --;
			system.debug('----------------------------execute invitaion---------------' + (i-1));
			this.executeInvitationBatch(list_Invitation[i-1]);
		}
		availableCount = availableCount - i;
		system.debug('---------------------------availableCount--------------------------' + availableCount);  
		return result;
	}
	
	//execute one invitation job
	private void executeInvitationBatch(Invitation__c inv)
	{
		if(inv.Task_Call_Result__c != null && inv.Task_End_Time__c != null && inv.Tast_Start_Time__c != null)
		{
			InvitationBatch ib = new InvitationBatch(inv, inv.Tast_Start_Time__c, inv.Task_End_Time__c, inv.Task_Call_Result__c, inv.Answer_SOQL_Script__c);
			DataBase.executeBatch(ib);
		}
		else if(inv.Answer_SOQL_Script__c != null && (inv.Task_Call_Result__c == null && inv.Task_End_Time__c == null && inv.Tast_Start_Time__c == null))
		{
			InvitationBatch ib = new InvitationBatch(inv , inv.Answer_SOQL_Script__c);
			DataBase.executeBatch(ib);
		}
		else if(inv.Answer_SOQL_Script__c == null && (inv.Task_Call_Result__c == null && inv.Task_End_Time__c == null && inv.Tast_Start_Time__c == null))
		{
			InvitationBatch ib = new InvitationBatch(inv);
			DataBase.executeBatch(ib);
		}
	}
}