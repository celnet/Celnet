/*
*Author:michael.mi@celnet.com.cn
*Date:2014-09-05
*Function:callout processor abstract class
*/
public abstract class WechatCalloutProcessor 
{
	public list<Sobject> ObjectList = new List<Sobject>();
	public list<Wechat_Callout_Task_Queue__c> WechatTaskList = new list<Wechat_Callout_Task_Queue__c>();
	
	public abstract void DoCallout(Wechat_Callout_Task_Queue__c WechatTask);
	
	public virtual void FinishData()
	{
		if(this.ObjectList != null && ObjectList.size() >0)
		{
			insert ObjectList;	
		}
	}
	
	public virtual void HandleNewQueue()
	{
		if(WechatTaskList.size() > 0)
		{
			WechatCalloutQueueManager.EnQueue(WechatTaskList);
		}
	}
}