/*Author: leo.bi@celnet.com.cn
 *Date:2014-08-15
 *Function: query middled table data 30 seconds before and synchronize them to CIC Task
 */
global class CN_WebServiceScheduler implements Schedulable  
{ 
	public void execute(SchedulableContext sc) 
	{
		DateTime timeLine = DateTime.now().addMinutes(-5);
		DateTime timeBegin = DateTime.Now().addMinutes(-20);
		DateTime timeEnd = DateTime.Now().addMinutes(20); 
		/*****synchronize manual task****/  
		List<Itegration_Log__c> list_ManualTask = [select Task_ID__c, Status__c, Sync_Times_Flag__c   
												   from Itegration_Log__c 
												   where (Status__c = 'Not Start' or Status__c = 'Faild') 
												   and Task_Type__c = 'Manual'
												   and LastModifiedDate <= :timeLine
												   and CreatedDate = Today
												   order by LastModifiedDate asc limit 200];
		if(!list_ManualTask.isEmpty()) this.synchronizeManualTask(list_ManualTask,timeBegin,timeEnd);//synchronize middle table data													   
		//query rest middle table data that are 'Not Start'  
		List<Itegration_Log__c> list_IntegrationLog = [select Call_Object__c, Status__c, Call_Start_Time__c, Call_Pickup_Time__c, Satisfaction_Survey_Rating__c 
													   from Itegration_Log__c 
													   where (Status__c = 'Not Start' or Status__c = 'Faild') 
													   and LastModifiedDate < :timeLine
													   and Call_Object__c != null
													   and CreatedDate = Today
													   and Task_Type__c = 'CIC'
													   order by LastModifiedDate asc limit 2000];
		if(!list_IntegrationLog.isEmpty()) this.synchronizeData(list_IntegrationLog);//synchronize middle table data
		this.newScheduler();//run new scheduler in 5 minutes 
	}
	
	private void synchronizeManualTask(List<Itegration_Log__c> list_ManualTask,DateTime timeBegin, DateTime timeEnd)
	{
		Map<Id,Itegration_Log__c> map_Task_Log = new Map<Id,Itegration_Log__c>();
		Map<Id,Itegration_Log__c> map_CIC_Log = new Map<Id,Itegration_Log__c>();
		Set<Task> set_Up_CICTask = new Set<Task>();
		Set<Id> createdId = new Set<Id>();
		for(Itegration_Log__c log : list_ManualTask)
		{
			map_Task_Log.put(log.Task_ID__c, log);
		}
		//query manual save task
		List<Task> list_Task = [select Id, Inactive_Reason__c, CreatedById, CreatedDate, Result__c, Customer_Type__c,Caller_Name__c, WhoId, WhatId, Remark__c
								from Task 
								where Id in :map_Task_Log.keySet()];
		for(Task t : list_Task)
		{
			createdId.add(t.CreatedById);
		}
		if(createdId.isEmpty()) return;
		//query cic task
		List<Task> ctiTasks = [select Id,Subject,CallType,LastModifiedDate,CreatedById,WhatId,WhoId
		                       from Task
		                       where CreatedById in :createdId
		                       and Type = 'Call'
		                       and LastModifiedDate >= :timeBegin
		                       and LastModifiedDate <= :timeEnd
		                       and CallObject != null
		                       and (WhatId != null or WhoId != null)
		                       order by LastModifiedDate desc];
		if(ctiTasks.isEmpty()) return;
		for(Task t : list_Task)
		{
			for(Task cti : ctiTasks)
			{
				DateTime tempTime = t.CreatedDate.addMinutes(5);
				//cic task and munual task must be created by the same person
				//whoid or whatid must be the same
				//cic lastmodifieddate must be within manual createddate's next 5 nimutes��
				if(cti.CreatedById == t.CreatedById && t.CreatedDate < cti.LastModifiedDate 
				&& tempTime > cti.LastModifiedDate  && (t.WhatId == cti.WhatId || t.WhoId == cti.WhoId))
				{
					cti.Inactive_Reason__c = t.Inactive_Reason__c;
		            cti.Result__c          = t.Result__c;
		            cti.Customer_Type__c   = t.Customer_Type__c; 
		            cti.Caller_Name__c     = t.Caller_Name__c;
		            cti.WhoId              = t.WhoId;
		            cti.WhatId             = t.WhatId; 
		            cti.Remark__c          = t.Remark__c;
		            set_Up_CICTask.add(cti);
		            map_CIC_Log.put(cti.Id,map_Task_Log.get(t.Id));
		            break;
				}
			}
		}
		if(set_Up_CICTask.isEmpty()) return;
		try
		{
			List<Itegration_Log__c> list_Update_Log = new List<Itegration_Log__c>();//the list to update sychonization status
			List<Itegration_Log__c> list_Delete_Log = new List<Itegration_Log__c>();//delete successful list
			List<Task> list_Up_CICTask = new List<Task>();
			list_Up_CICTask.addAll(set_Up_CICTask);
			Task[] array_Task = list_Up_CICTask;
			DataBase.Saveresult[] list_SR = Database.update(array_Task, false);//update Task with permission of partial success
			Integer i = 0;//the index of the array
			//store the error message
			for(DataBase.Saveresult sr : list_SR)
			{
				Itegration_Log__c temp_Log = map_CIC_Log.get(array_Task[i].Id);
			    if (!sr.isSuccess()) 
			    {   
			    	temp_Log.Status__c = 'Faild';
					String errorItem = '';
			        for(Database.Error err : sr.getErrors())
			        {
			            String statusCode = String.valueOf(err.getStatusCode());
			            String message = String.valueOf(err.getMessage());
			        	errorItem +='\n' + 'Error Code:' + statusCode + ' ErrorMessage:' + message;                  
			        }
		        	temp_Log.Failing_Reason__c = errorItem;
			        temp_Log.Sync_Times_Flag__c += 1;
					if(temp_Log.Sync_Times_Flag__c >= 3)
					{
						temp_Log.Status__c = 'Cannot Find CIC';
					}
		        	list_Update_Log.add(temp_Log);
			    }
			    else
			    {
					temp_Log.Status__c = 'Succeed';
		        	list_Delete_Log.add(temp_Log);//update failed log
			    }
			    i++;
			}
			for(Itegration_Log__c intlog : list_ManualTask)
			{
				if(intlog.Status__c == 'Not Start')
				{
					intlog.Sync_Times_Flag__c += 1;
					if(intlog.Sync_Times_Flag__c >= 3)
					{
						intlog.Status__c = 'Cannot Find CIC';
					}
					list_Update_Log.add(intlog);
				}
			}
			if(!list_Update_Log.isEmpty()) update list_Update_Log;
			if(!list_Delete_Log.isEmpty()) update list_Delete_Log;//��Ҫ�ĳ�delete
		} catch(Exception e)
		{
			system.debug('-------------------Exception------------------------' + e);
		}		                       
	}
	
	private void synchronizeData(List<Itegration_Log__c> list_IntegrationLog)
	{
		Map<String,Itegration_Log__c> map_CallObjdct_Log = new Map<String,Itegration_Log__c>();
		for(Itegration_Log__c log : list_IntegrationLog) map_CallObjdct_Log.put(log.Call_Object__c, log);	
		List<Task> list_Task = [select Id, Satisfaction_Survey_Rating__c, CallObject, Call_Start_Time__c,Call_Pickup_Time__c 
								from Task 
								where CallObject in :map_CallObjdct_Log.keySet()];
		if(list_Task.isEmpty()) return;
		for(Task t : list_Task)
		{
			Itegration_Log__c log_Temp = map_CallObjdct_Log.get(t.CallObject);
			t.Satisfaction_Survey_Rating__c = log_Temp.Satisfaction_Survey_Rating__c;
      		t.Call_Start_Time__c            = log_Temp.Call_Start_Time__c;
      		t.Call_Pickup_Time__c           = log_Temp.Call_Pickup_Time__c;
		}						
		this.updateTask(map_CallObjdct_Log, list_Task);
	}
	
	private void updateTask (Map<String,Itegration_Log__c> map_CallObjdct_Log, List<Task> list_Up_Task)
	{
		try
		{
			List<Itegration_Log__c> list_Update_Log = new List<Itegration_Log__c>();//the list to update sychonization status
			List<Itegration_Log__c> list_Delete_Log = new List<Itegration_Log__c>();//delete successful list
			List<Itegration_Log__c> list_Duplicate_Log = new List<Itegration_Log__c>();//duplicate list
			Set<Itegration_Log__c> set_Duplicate_Log = new Set<Itegration_Log__c>();//duplicate Set
			Set<String> set_Duplicate = new Set<String>();
			Task[] array_Task = list_Up_Task;
			DataBase.Saveresult[] list_SR = Database.update(array_Task, false);//update Task with permission of partial success
			Integer i = 0;//the index of the array
			//store the error message
			for(DataBase.Saveresult sr : list_SR)
			{
				Itegration_Log__c temp_Log = map_CallObjdct_Log.get(array_Task[i].CallObject);
			    if (!sr.isSuccess()) 
			    {   
			    	temp_Log.Status__c = 'Faild';
					String errorItem = '';
			        for(Database.Error err : sr.getErrors())
			        {
			            String statusCode = String.valueOf(err.getStatusCode());
			            String message = String.valueOf(err.getMessage());
			        	errorItem +='\n' + 'Error Code:' + statusCode + ' ErrorMessage:' + message;                  
			        }
		        	temp_Log.Failing_Reason__c = errorItem;
		        	if(set_Duplicate.contains(temp_Log.Call_Object__c))
					{
						temp_Log.Status__c = 'Duplicate CallObject';
						temp_Log.Failing_Reason__c = temp_Log.Failing_Reason__c + '\n' + errorItem;
						set_Duplicate_Log.add(temp_Log);
					}
					else
					{
						set_Duplicate.add(temp_Log.Call_Object__c);
			        	list_Update_Log.add(temp_Log);//update failed log
					}
			    }
			    else
			    {
			    	if(set_Duplicate.contains(temp_Log.Call_Object__c))
					{
						temp_Log.Status__c = 'Duplicate CallObject';
						set_Duplicate_Log.add(temp_Log);
					}
					else
					{
						temp_Log.Status__c = 'Succeed';
						set_Duplicate.add(temp_Log.Call_Object__c);
			        	list_Delete_Log.add(temp_Log);//update failed log
					}
			    }
			    i++;
			}
			//update sychonization status and reason
			if(!set_Duplicate_Log.isEmpty())
			{
				list_Duplicate_Log.addAll(set_Duplicate_Log);
				update list_Duplicate_Log;
			}
			if(!list_Update_Log.isEmpty()) update list_Update_Log;
			if(!list_Delete_Log.isEmpty()) update list_Delete_Log;//��Ҫ�ĳ�delete
		} catch(Exception e)
		{
			system.debug('-------------------Exception------------------------' + e);
		}
	}
	
	private void newScheduler()
    {
    	try
		{
			this.cancelUnusedScheduler();//abort unusing scheduler
			CN_WebServiceScheduler wss = new CN_WebServiceScheduler();//new scheduler
			DateTime newDT = DateTime.now().addMinutes(5);
			String sch = '0 ' + String.valueOf(newDT.minute()) + ' '
							  + String.valueOf(newDT.hour()) + ' ' + String.valueOf(newDT.day()) + ' '
							  + String.valueOf(newDT.month()) + ' ? ' + String.valueOf(newDT.year());
			system.schedule('CN_WebServiceScheduler' + String.valueOf(DateTime.now()), sch, wss);
		} catch(Exception e)
		{
			system.debug('----------------------------run scheduler Exception---------------------------------------');
		}
    } 
	
	private void cancelUnusedScheduler()
	{
		List<CronTrigger> list_CronTrigger =   [select Id, NextFireTime 
    											from CronTrigger 
    											where CronJobDetail.Name like 'CN_WebServiceScheduler%'];
		for(CronTrigger ct : list_CronTrigger)
		{
			if(ct.NextFireTime == null)
			{
				system.abortJob(ct.Id);
			}
		}
	}
}