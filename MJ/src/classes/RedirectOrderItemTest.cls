@isTest
private class RedirectOrderItemTest {

    static testMethod void redirectTest() {
    	//Get RecordType ID for HouseHold and HouseHold Child
        RecordType typeHouseHold = [Select Id From RecordType Where DeveloperName ='HouseHold' and SobjectType ='Account' limit 1];
        RecordType typeHouseHoldChild = [Select Id From RecordType Where DeveloperName ='Household_Child' and SobjectType ='Contact' limit 1];
        
        
    	//Create custom settings
        Member_ID_Setting__c s = new Member_ID_Setting__c();
        s.Name = String.valueOf(Date.today().year());
        s.Next_ID__c = 1888;
        insert s;
        
        // Create a test Household
        Account testAccount = new Account();
        testAccount.Name = 'Account Name';
        testAccount.First_Name__c = 'Account';
        testAccount.Last_Name__c = 'Name';
        testAccount.RecordTypeId = typeHouseHold.Id;
        insert testAccount;
        
        //Creat test baby 1
        Contact contact = New Contact();
        contact.FirstName='Test';
        contact.LastName='Baby';
        contact.AccountId=testAccount.ID;
        contact.RecordTypeId = typeHouseHoldChild.Id;
        insert contact;
       
        //Creat Product 1 - MJ & Is_DSO_Product__c = False
        Product__c product1 = new Product__c();
        product1.Name = 'Product 1';
        product1.Product_Code__c = 'Product01';
        product1.Product_SKU__c = '';
        product1.Product_Nature__c = 'MJ';
        product1.Is_DSO_Product__c = false;
        insert product1;
        
        //Create a Order
        Order__c order =new Order__c();
        order.Delivery_Center__c = 'DKSH';
        order.HouseHold__c = testAccount.ID;
        order.Delivery_Instruction__c = 'TEST';
        order.Expected_Delivery_Date__c = Date.today().addDays(7);
        insert order;
        
    
        //Create order item
        Order_Item__c orderItem = new Order_Item__c();
        orderItem.Order__c = order.id;
        orderItem.Product__c = product1.id;
        orderItem.Quantity__c = 10;
        insert orderItem;
        
        //Instantiate Redirect Controller
        ApexPages.currentPage().getParameters().put('id', orderItem.Id);
        ApexPages.StandardController cont = new ApexPages.StandardController(orderItem);
        RedirectOrderItem roi  = new RedirectOrderItem(cont);
        String url = roi.redirect().getUrl();
        //Assert Redirect
        system.assert(url.equalsIgnoreCase('/apex/MassAddEditOrderItems?id='+order.Id));
        
    }
}