/*
This job is to create new Member ID setting and reset Coupon ID for the new coming year
*/
public class UniqueIDYearlyRefreshJob implements Schedulable {
    
	public void execute(SchedulableContext sc) 
    {
        createNewMemberIDSetting();
		resetCouponRunningNumber();
   	}
    
    private void createNewMemberIDSetting()
    {
        String curYear = String.valueOf(Date.today().year());
        
		//Get next Member ID running number from custom setting
        Member_ID_Setting__c memberSetting = Member_ID_Setting__c.getValues(curYear);
        if(memberSetting == NULL)
        {
			memberSetting = New Member_ID_Setting__c();
        	memberSetting.Name=curYear;
        	memberSetting.Next_ID__c=1888;
            insert memberSetting;
        }
    }
    
    private void resetCouponRunningNumber()
    {
        //Retrieve existing Coupon Settings
        List<Coupon_ID_Setting__c> couponSettings = [SELECT ID,Name FROM Coupon_ID_Setting__c];
        
        //Reset Coupon Running Number to 1 for all settings
		for(Coupon_ID_Setting__c setting : couponSettings)
            setting.Next_ID__c = 1;
        
        update couponSettings;
    }
}