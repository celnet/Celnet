/*Author:Mark
 *Date:20140924
 *Function:click menu upload birth certificate event，
 *用户在微信客户端上点击上载出生证明按钮时由此Handler来处理
*/
public class WechatCallinImageHandler extends WechatCallinMsgHandler
{
	public override void Handle(WechatCallinMsgPipelineContext Context)
	{
		if(Context.InMsg.MsgType != WechatEntity.MESSAGE_TYPE_IMAGE) return;
		WechatEntity.InImageMsg inImage = (WechatEntity.InImageMsg)Context.InMsg;
		List<Wechat_User__c> wu = [select Binding_Status__c , Binding_Non_Member__r.Baby_Count__c , Binding_Non_Member__r.Attachment_Count__c , Binding_Non_Member__c from Wechat_User__c  where Open_Id__c =:inImage.FromUserName];
        if(wu.size() != 1) return;
        if(wu[0].Binding_Status__c == null)
        {
        	WechatEntity.OutTextMsg outMsg = WechatEntity.GenerateOutMsg(Context.InMsg , '');
        	string replaceStr = '<a href="'+WechatBusinessUtility.WECHAT_SITE+'WechatSMSVerificationPage?openId='+inImage.FromUserName+'">【註冊綁定】</a>';
        	outMsg.Content = WechatBusinessUtility.QueryRemindingByKey('RN010',replaceStr);
        	Context.OutMsg = outMsg;
        }
        else
        {
        	if(decimal.valueof((null != wu[0].Binding_Non_Member__r.Baby_Count__c ? wu[0].Binding_Non_Member__r.Baby_Count__c : '0')) <= (null != wu[0].Binding_Non_Member__r.Attachment_Count__c ? wu[0].Binding_Non_Member__r.Attachment_Count__c : 0))
        	{
        		WechatEntity.OutTextMsg outMsg = WechatEntity.GenerateOutMsg(Context.InMsg , '');
	        	outMsg.Content = WechatBusinessUtility.QueryRemindingByKey('RN011');
	        	Context.OutMsg = outMsg;
        	}
        	else
        	{
				Wechat_Callout_Task_Queue__c imageQueue = new Wechat_Callout_Task_Queue__c();
				imageQueue.Public_Account_Name__c = Context.PublicAccountName;
				imageQueue.Open_ID__c = inImage.FromUserName;
				imageQueue.Media_ID__c = inImage.MediaId;
				imageQueue.Type__c = WechatEntity.MESSAGE_TYPE_IMAGE;
				imageQueue.Name = 'WechatCalloutGetImageProcessor';
				imageQueue.Processor_Name__c = 'WechatCalloutGetImageProcessor';
				WechatCalloutQueueManager.EnQueue(imageQueue);
        	}
        }
	}
}