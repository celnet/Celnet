/*
    Covers LeadConversionHelper and Lead Staging(less VF page/validations)
    Apply to [HK/CN/ALL]: HK 
*/

@isTest
private class LeadConversionHelperTest {
    
    static{
        //Create custom settings
        TestUtility.setupMemberIDCustomSettings();
    }
    
    //test LEAD_STATUS_NEW_HOUSEHOLD
    static testMethod void testCreateNewHouseholdAndReferralOK() {
      system.runas(TestUtility.new_HK_User()){
        //Dummy Household with no child, for testing referral
        createExistingHousehold(0);
        String referrerId = [SELECT Member_ID__c from Account LIMIT 1].Member_ID__c;
        system.debug(referrerId);
        Lead lead = new Lead();
        lead.LastName = 'PARENT';
        lead.FirstName = 'TEST';
        lead.First_Name_1st_Child__c = 'BABY';
        lead.Last_Name_1st_Child__c = 'TEST';
        lead.Birthday_1st_Child__c = system.today();
        lead.Referral_Member_ID__c = referrerId;
        
        insert lead;
        
        LeadStaging staging = new LeadStaging(lead, LeadConversionHelper.isLastFirstName(lead));
        LeadConversionHelper.convertStaging(staging);
        
        //Assert newly created account and two contacts + one ref contact
        system.assertEquals(2,Database.countQuery('SELECT COUNT() FROM Account'));
        system.assertEquals(3,Database.countQuery('SELECT COUNT() FROM Contact'));
                
        //Assert Converted Status
        String leadID = lead.Id;
        system.assertEquals(LeadConversionHelper.LEAD_STATUS_NEW_HOUSEHOLD, [SELECT status FROM lead WHERE id=:leadID].status);
        //to be uncommented after impl of member id 
        //Assert Referral Status
        String refereeId = [SELECT id from Account WHERE First_Name__c ='TEST'].id;
        Member_Referral__c ref = [SELECT Status__c,Referee__r.Id,Referral__r.Member_ID__c FROM Member_Referral__c LIMIT 1];
        system.assertEquals('Success',ref.Status__c);
        system.assertEquals(referrerId,ref.Referral__r.Member_ID__c);
        system.assertEquals(refereeId,ref.Referee__r.Id);
      }
    }
    
    //test LEAD_STATUS_NEW_PARENT  - 1 new parent 1 existing child
    static testMethod void testMergeHouseholdWithNewParentOnlyAndReferralFail() {
       system.runas(TestUtility.new_HK_User()){ 
         //Dummy Household with no child, for testing referral
        createExistingHousehold(0);
        String referrerId = [SELECT Member_ID__c from Account LIMIT 1].Member_ID__c;
        
        Lead lead = new Lead();
        lead.LastName = 'PARENT';
        lead.FirstName = 'TEST';
        lead.First_Name_1st_Child__c = 'BABY';
        lead.Last_Name_1st_Child__c = 'TEST';
        lead.Birthday_1st_Child__c = system.today();
            
        lead.Referral_Member_ID__c = referrerId;
         
        //create one existing household with one child
        createExistingHousehold(1);
        
        Integer AccCountBefore = Database.countQuery('SELECT COUNT() FROM Account');
        Integer ConCountBefore = Database.countQuery('SELECT COUNT() FROM Contact');
        system.assertEquals(2,AccCountBefore); //dummy account + existing account
        system.assertEquals(3,ConCountBefore); //pri contact + 1 baby + ref parent
        //get existing household id and baby id
        String hhId = [Select id from Account Where name = 'PRIMARY PARENT' LIMIT 1].id;
        String babyId = [Select id from Contact Where LastName='Existing Baby'].id;
        insert lead;
        LeadStaging staging = new LeadStaging(lead,true);
        //set staging parameters to create new parent and merge existing baby
        ApexPages.currentPage().getParameters().put('radiogroup-parent',hhId+':');
        ApexPages.currentPage().getParameters().put('radiogroup-baby1',hhId+':'+babyId);
        system.assert(true,staging.validateNonNewHouseholdSelections());
        
        LeadConversionHelper.convertStaging(staging);
        
        //Assert count of newly created parent
        Integer AccCountAfter = Database.countQuery('SELECT COUNT() FROM Account');
        Integer ConCountAfter = Database.countQuery('SELECT COUNT() FROM Contact');
        system.assertEquals(AccCountBefore,AccCountAfter); //nochange 
        system.assertEquals(ConCountBefore+1,ConCountAfter); // + new parent        
        
        //Assert Converted Status
        String leadID = lead.Id;
        system.assertEquals(LeadConversionHelper.LEAD_STATUS_NEW_PARENT, [SELECT status FROM lead WHERE id=:leadID].status);        
 
        //Assert Referral Fail
        String refereeId = hhId; //[SELECT id from Account WHERE First_Name__c ='TEST'].id;
        Member_Referral__c ref = [SELECT Status__c,Referee__r.Id,Referral__r.Member_ID__c FROM Member_Referral__c LIMIT 1];
        system.assertEquals('Failure',ref.Status__c);
        system.assertEquals(referrerId,ref.Referral__r.Member_ID__c);
        system.assertEquals(refereeId,ref.Referee__r.Id);
       }
    }
    
    //test LEAD_STATUS_NEW_CHILD - 1 existing parent, 3 lead babies ( 1 new child + merge 2 existing children );
     static testMethod void testMergeHouseholdWithNewAndExistingChildren() {
        system.runas(TestUtility.new_HK_User()){
	        Lead lead = new Lead();
	        lead.LastName = 'PARENT';
	        lead.FirstName = 'TEST';
	        lead.First_Name_1st_Child__c = 'BABY';
	        lead.Last_Name_1st_Child__c = 'FIRST';
	        lead.Birthday_1st_Child__c = system.today();
	        lead.First_Name_2nd_Child__c = 'BABY';
	        lead.Last_Name_2nd_Child__c = 'SECOND';
	         lead.Birthday_2nd_Child__c = system.today();
	        lead.First_Name_3rd_Child__c = 'BABY';
	        lead.Last_Name_3rd_Child__c = 'THIRD';
	        lead.Birthday_3rd_Child__c = system.today();
	        
	        createExistingHousehold(2);
	        
	        Integer AccCountBefore = Database.countQuery('SELECT COUNT() FROM Account');
	        Integer ConCountBefore = Database.countQuery('SELECT COUNT() FROM Contact');
	        system.assertEquals(1,AccCountBefore);
	        system.assertEquals(3,ConCountBefore); //pri contact + 2 baby
	        //get existing household id and baby id
	        String hhId = [Select id from Account LIMIT 1].id;
	        String parentId = [Select id from Contact WHERE LastName='PRIMARY'].id;
	        List<Contact>babies = [Select id from Contact Where LastName='Existing Baby'];
	        system.assertEquals(2,babies.size());
	        
	        insert lead;
	        LeadStaging staging = new LeadStaging(lead,true);
	        //set staging parameters to merge existing parent and merge 2 existing babys and create one new baby
	        ApexPages.currentPage().getParameters().put('radiogroup-parent',hhId+':'+parentId);
	        ApexPages.currentPage().getParameters().put('radiogroup-baby1',hhId+':'+babies[0].id); //merge first one
	        ApexPages.currentPage().getParameters().put('radiogroup-baby2',hhId+':'); //create second one
	        ApexPages.currentPage().getParameters().put('radiogroup-baby3',hhId+':'+babies[1].id); //merge third one
	        system.assert(true,staging.validateNonNewHouseholdSelections());
	        
	        LeadConversionHelper.convertStaging(staging);
	        
	        //Assert count of newly created parent
	        Integer AccCountAfter = Database.countQuery('SELECT COUNT() FROM Account');
	        Integer ConCountAfter = Database.countQuery('SELECT COUNT() FROM Contact');
	        system.assertEquals(1,AccCountAfter);
	        system.assertEquals(4,ConCountAfter); //old pri parent + 2 old baby + new baby
	        //Assert second baby is created
	        system.assertNotEquals(null,[Select id from Contact Where FirstName='SECOND']);
	        
	        //Assert Converted Status
	        String leadID = lead.Id;
	        system.assertEquals(LeadConversionHelper.LEAD_STATUS_NEW_CHILD, [SELECT status FROM lead WHERE id=:leadID].status);
        }
    }
    
    //test LEAD_STATUS_NEW_PARENT_CHILD     - 1 new parent, 2 lead children (1 new child, 1 existing child)
     static testMethod void testMergeHouseholdWithNewParentNewAndExistingChildren() 
     {
     	system.runas(TestUtility.new_HK_User()){
	        Lead lead = new Lead();
	        lead.LastName = 'PARENT';
	        lead.FirstName = 'TEST';
	        lead.First_Name_1st_Child__c = 'BABY';
	        lead.Last_Name_1st_Child__c = 'FIRST';
	        lead.Birthday_1st_Child__c = system.today();
	        lead.First_Name_2nd_Child__c = 'BABY';
	        lead.Last_Name_2nd_Child__c = 'SECOND';
	        lead.Birthday_2nd_Child__c = system.today();
	
	        createExistingHousehold(1);
	        
	        Integer AccCountBefore = Database.countQuery('SELECT COUNT() FROM Account');
	        Integer ConCountBefore = Database.countQuery('SELECT COUNT() FROM Contact');
	        system.assertEquals(1,AccCountBefore);
	        system.assertEquals(2,ConCountBefore); //pri contact + 1 baby
	        //get existing household id and baby id
	        String hhId = [Select id from Account LIMIT 1].id;
	        String parentId = [Select id from Contact WHERE LastName='PRIMARY'].id;
	        List<Contact>babies = [Select id from Contact Where LastName='Existing Baby'];
	        system.assertEquals(1,babies.size());
	        
	        insert lead;
	        LeadStaging staging = new LeadStaging(lead,true);
	        //set staging parameters to create parent and merge 1 existin baby and create one new baby
	        ApexPages.currentPage().getParameters().put('radiogroup-parent',hhId+':');
	        ApexPages.currentPage().getParameters().put('radiogroup-baby1',hhId+':'); //create first one
	        ApexPages.currentPage().getParameters().put('radiogroup-baby2',hhId+':'+babies[0].id); //merge second one
	        system.assert(true,staging.validateNonNewHouseholdSelections());
	        
	        LeadConversionHelper.convertStaging(staging);
	        
	        //Assert count of newly created parent
	        Integer AccCountAfter = Database.countQuery('SELECT COUNT() FROM Account');
	        Integer ConCountAfter = Database.countQuery('SELECT COUNT() FROM Contact');
	        system.assertEquals(1,AccCountAfter);
	        system.assertEquals(4,ConCountAfter); //old pri parent +1 new parent + 1 old baby + new baby
	        //Assert second baby is created
	        system.assertNotEquals(null,[Select id from Contact Where FirstName='FIRST']);
	        
	        //Assert Converted Status
	        String leadID = lead.Id;
	        system.assertEquals(LeadConversionHelper.LEAD_STATUS_NEW_PARENT_CHILD, [SELECT status FROM lead WHERE id=:leadID].status);
     	}
    }
    
    //test LEAD_STATUS_OLD_HOUSEHOLD        - 1 existing parent, 2 existing children
     static testMethod void testMergeHouseholdWithoutNewParentChildren() {
     	system.runas(TestUtility.new_HK_User()){
	        Lead lead = new Lead();
	        lead.LastName = 'PARENT';
	        lead.FirstName = 'TEST';
	        lead.First_Name_1st_Child__c = 'BABY';
	        lead.Last_Name_1st_Child__c = 'FIRST';
	        lead.First_Name_2nd_Child__c = 'BABY';
	        lead.Last_Name_2nd_Child__c = 'SECOND';
	        lead.Birthday_1st_Child__c = system.today();
	        lead.Birthday_2nd_Child__c = system.today();
	
	        createExistingHousehold(2);
	        
	        Integer AccCountBefore = Database.countQuery('SELECT COUNT() FROM Account');
	        Integer ConCountBefore = Database.countQuery('SELECT COUNT() FROM Contact');
	        system.assertEquals(1,AccCountBefore);
	        system.assertEquals(3,ConCountBefore); //pri contact + 2 baby
	        //get existing household id and baby id
	        String hhId = [Select id from Account LIMIT 1].id;
	        String parentId = [Select id from Contact WHERE LastName='PRIMARY'].id;
	        List<Contact>babies = [Select id from Contact Where LastName='Existing Baby'];
	        system.assertEquals(2,babies.size());
	        
	        insert lead;
	        LeadStaging staging = new LeadStaging(lead,true);
	        //set staging parameters to create parent and merge 1 existin baby and create one new baby
	        ApexPages.currentPage().getParameters().put('radiogroup-parent',hhId+':'+parentId); //merge parent
	        ApexPages.currentPage().getParameters().put('radiogroup-baby1',hhId+':'+babies[1].id); //merge first one
	        ApexPages.currentPage().getParameters().put('radiogroup-baby2',hhId+':'+babies[0].id); //merge second one
	        system.assert(true,staging.validateNonNewHouseholdSelections());
	        
	        LeadConversionHelper.convertStaging(staging);
	        
	        //Assert count of newly created parent
	        Integer AccCountAfter = Database.countQuery('SELECT COUNT() FROM Account');
	        Integer ConCountAfter = Database.countQuery('SELECT COUNT() FROM Contact');
	        system.assertEquals(1,AccCountAfter);
	        system.assertEquals(3,ConCountAfter); //unchange
	        
	        //Assert Converted Status
	        String leadID = lead.Id;
	        system.assertEquals(LeadConversionHelper.LEAD_STATUS_OLD_HOUSEHOLD, [SELECT status FROM lead WHERE id=:leadID].status);
     	}
    }
    
     //test simple merge rules for home and mobile phone
     static testMethod void testSimpleMergeRules() {
     	system.runas(TestUtility.new_HK_User()){
	     	Lead lead = new Lead();
	        lead.LastName = 'PARENT';
	        lead.FirstName = 'TEST';
	        lead.Home_Phone__c = '11223344';
	        lead.MobilePhone = '88776655';
	        lead.First_Name_1st_Child__c = 'BABY';
	        lead.Last_Name_1st_Child__c = 'FIRST';
	        lead.First_Name_2nd_Child__c = 'BABY';
	        lead.Last_Name_2nd_Child__c = 'SECOND';
	        lead.Birthday_1st_Child__c = system.today();
	        lead.Birthday_2nd_Child__c = system.today();
	        lead.LeadSource = 'New Channel';
	        lead.Sub_Channel__c = 'New Sub Channel';
	
	        createExistingHousehold(2);
	
	        Integer AccCountBefore = Database.countQuery('SELECT COUNT() FROM Account');
	        Integer ConCountBefore = Database.countQuery('SELECT COUNT() FROM Contact');
	        system.assertEquals(1,AccCountBefore);
	        system.assertEquals(3,ConCountBefore); //pri contact + 2 baby
	        //get existing household id and baby id
	        Account hh = [Select id,Home_Phone__c,Mobile_Phone__c from Account LIMIT 1];
	        //assert existing household no house phone, no mobile
	        system.assertEquals(null,hh.Home_Phone__c);
	        system.assertEquals(null,hh.Mobile_Phone__c);
	        
	        String hhId = hh.id;
	        String parentId = [Select id from Contact WHERE LastName='PRIMARY'].id;
	        List<Contact>babies = [Select id from Contact Where LastName='Existing Baby'];
	        system.assertEquals(2,babies.size());
	        
	        
	        //asert existing rc
	        String firstBabyID = babies[0].id;
	        List<Recruitment_Channel__c> channels = [Select Converted_from_Lead__c, Primary__c, Sub_Channel__c, Channel__c
	                                                 FROM Recruitment_Channel__c 
	                                                 WHERE Contact__c = :firstBabyID
	                                                 order by createdDate];
	        system.assertEquals(1,channels.size());        
	        system.assertEquals(false,channels[0].Converted_from_Lead__c);
	        system.assertEquals(true,channels[0].Primary__c);
	        
	        insert lead;
	        LeadStaging staging = new LeadStaging(lead,true);
	        //set staging parameters to create parent and merge 1 existin baby and create one new baby
	        ApexPages.currentPage().getParameters().put('radiogroup-parent',hhId+':'+parentId); //merge parent
	        ApexPages.currentPage().getParameters().put('radiogroup-baby1',hhId+':'+babies[1].id); //merge first one
	        ApexPages.currentPage().getParameters().put('radiogroup-baby2',hhId+':'+babies[0].id); //merge second one
	        system.assert(true,staging.validateNonNewHouseholdSelections());
	
	        //Convert now!
	        LeadConversionHelper.convertStaging(staging);
	        
	        hh = [Select  id,Home_Phone__c,Mobile_Phone__c from Account LIMIT 1];
	        //assert merge household with updated house phone, mobile
	        system.assertEquals('11223344',hh.Home_Phone__c);
	        system.assertEquals('88776655',hh.Mobile_Phone__c);
	        
	         //asert rc
	        channels = channels = [Select Converted_from_Lead__c, Primary__c, Sub_Channel__c, Channel__c
	                                                 FROM Recruitment_Channel__c 
	                                                 WHERE Contact__c = :firstBabyID
	                                                 order by createdDate];
	       /* system.assertEquals(2,channels.size());        
	        system.assertEquals(false,channels[0].Converted_from_Lead__c);
	        system.assertEquals(true,channels[0].Primary__c);
	        
	        system.assertEquals('New Channel',channels[1].Channel__c);
	        system.assertEquals('New Sub Channel',channels[1].Sub_Channel__c);
	        system.assertEquals(true,channels[1].Converted_from_Lead__c);
	        system.assertEquals(false,channels[1].Primary__c);*/
     	}
        
        
    }
    
    //test simple merge rules for home and mobile phone, if there's already existing home n mobile
     static testMethod void testSimpleMergeRules2() {
     	system.runas(TestUtility.new_HK_User()){
	     	Lead lead = new Lead();
	        lead.LastName = 'PARENT';
	        lead.FirstName = 'TEST';
	        lead.Home_Phone__c = '11223344';
	        lead.MobilePhone = '88776655';
	        lead.First_Name_1st_Child__c = 'BABY';
	        lead.Last_Name_1st_Child__c = 'FIRST';
	        lead.First_Name_2nd_Child__c = 'BABY';
	        lead.Last_Name_2nd_Child__c = 'SECOND';
	        lead.Birthday_1st_Child__c = system.today();
	        lead.Birthday_2nd_Child__c = system.today();
	         
	        createExistingHousehold(2);
	
	        Integer AccCountBefore = Database.countQuery('SELECT COUNT() FROM Account');
	        Integer ConCountBefore = Database.countQuery('SELECT COUNT() FROM Contact');
	        system.assertEquals(1,AccCountBefore);
	        system.assertEquals(3,ConCountBefore); //pri contact + 2 baby
	        //get existing household id and baby id
	        Account hh = [Select id,Home_Phone__c,Mobile_Phone__c,Other_Phone_1__c,Other_Phone_1_Remark__c,Other_Phone_2__c,Other_Phone_2_Remark__c from Account LIMIT 1];
	        //pre-set exsiting with some numbers 
	        hh.Home_Phone__c = '44445555';
	        hh.Mobile_Phone__c = '99998888';
	        update hh;
	        //assert existing household house phone, mobile, empty other phone 1 and 2
	        system.assertEquals('44445555',hh.Home_Phone__c);
	        system.assertEquals('99998888',hh.Mobile_Phone__c);
	        system.assertEquals(null,hh.Other_Phone_1__c);
	        system.assertEquals(null,hh.Other_Phone_1_Remark__c);
	        system.assertEquals(null,hh.Other_Phone_2__c);
	        system.assertEquals(null,hh.Other_Phone_2_Remark__c);
	        
	        String hhId = hh.id;
	        String parentId = [Select id from Contact WHERE LastName='PRIMARY'].id;
	        List<Contact>babies = [Select id from Contact Where LastName='Existing Baby'];
	        system.assertEquals(2,babies.size());
	        
	        insert lead;
	        LeadStaging staging = new LeadStaging(lead,true);
	        //set staging parameters to create parent and merge 1 existin baby and create one new baby
	        ApexPages.currentPage().getParameters().put('radiogroup-parent',hhId+':'+parentId); //merge parent
	        ApexPages.currentPage().getParameters().put('radiogroup-baby1',hhId+':'+babies[1].id); //merge first one
	        ApexPages.currentPage().getParameters().put('radiogroup-baby2',hhId+':'+babies[0].id); //merge second one
	        system.assert(true,staging.validateNonNewHouseholdSelections());
	
	        //Convert now!
	        LeadConversionHelper.convertStaging(staging);
	        
	        hh = [Select id,Home_Phone__c,Mobile_Phone__c,Other_Phone_1__c,Other_Phone_1_Remark__c,Other_Phone_2__c,Other_Phone_2_Remark__c from Account LIMIT 1];
	        //assert merge household with updated house phone, mobile, and 'shifted' other phone 1,2
	        system.assertEquals('11223344',hh.Home_Phone__c);
	        system.assertEquals('88776655',hh.Mobile_Phone__c);
	        system.assertEquals('44445555',hh.Other_Phone_1__c);
	        system.assertEquals('Number added from Home Phone',hh.Other_Phone_1_Remark__c);
	        system.assertEquals('99998888',hh.Other_Phone_2__c);
	        system.assertEquals('Number added from Mobile Phone',hh.Other_Phone_2_Remark__c); 
     	}
        
    }
    
    static testMethod void testBCandRCandHospital(){
    	system.runas(TestUtility.new_HK_User()){
    		//Create custom settings
	        TestUtility.setupMemberIDCustomSettings();
	        
	        //Creat Product 1
	        Product__c product1 = new Product__c();
	        product1.Name = 'Product 1';
	        product1.Product_Code__c = 'Product01';
	        insert product1;
	        
	        //Creat Product 2
	        Product__c product2 = new Product__c();
	        product2.Name = 'Product 2';
	        product2.Product_Code__c = 'Product02';
	        insert product2;
	        
	        //Creat Product 3
	        Product__c product3 = new Product__c();
	        product3.Name = 'Product 3';
	        product3.Product_Code__c = 'Product03';
	        insert product3;
	        
	        Account hosp = new Account();
	        hosp.Name = 'TEST HOSPITAL';
	        hosp.RecordTypeId = ObjectUtil.GetRecordTypeID(Account.SObjectType, 'Hospital');
	        insert hosp;
	        
	        Lead lead = new Lead();
	        lead.LastName = 'PARENT';
	        lead.FirstName = 'TEST';
	        lead.First_Name_1st_Child__c = 'BABY';
	        lead.Last_Name_1st_Child__c = 'FIRST';
	        lead.First_Name_2nd_Child__c = 'BABY';
	        lead.Last_Name_2nd_Child__c = 'SECOND';
	        lead.Birthday_1st_Child__c = system.today().addYears(-6);
	        lead.Birthday_2nd_Child__c = system.today().addYears(-6);
	        lead.LeadSource = 'Website';
	        lead.Sub_Channel__c = 'MJN Website';
	        lead.Mother_Milk_1st_Child__c = 'Product 1';
	        lead.Child_Milk_0_6M_1st_Child__c = 'Product 2';
	        lead.Child_Milk_7_12M_1st_Child__c = 'Product 3';
	        lead.Child_Milk_13_36M_1st_Child__c = 'Product 1';
	        lead.Child_Milk_36M_1st_Child__c = 'Product 2';
	        lead.Born_Hospital_1st_Child__c = 'TEST HOSPITAL';
	        
	         insert lead;
	        LeadStaging staging = new LeadStaging(lead,true);
	        LeadConversionHelper.convertStaging(staging);
	        //asert rc and converted from lead
	        List<Recruitment_Channel__c> rcs = [SELECT Converted_from_Lead__c, Primary__c, Sub_Channel__c, Channel__c FROM Recruitment_Channel__c];
	        system.assertEquals(2, rcs.size());
	        system.assertEquals(true, rcs[0].Primary__c);
	        system.assertEquals(true, rcs[0].Converted_from_Lead__c);
	        system.assertEquals('MJN Website', rcs[0].Sub_Channel__c);
	        system.assertEquals('Website', rcs[0].Channel__c);
	        system.assertEquals(true, rcs[0].Converted_from_Lead__c);
	        
	        
	        system.assertEquals(true, rcs[1].Primary__c);
	        system.assertEquals('MJN Website', rcs[1].Sub_Channel__c);
	        system.assertEquals('Website', rcs[1].Channel__c);
	        //assert bc and converted from lead
	        List<Baby_Consumption__c> bcs = [SELECT From_Age__c,To_Age__c, Product__c, Converted_from_Lead__c from Baby_Consumption__c order by From_Age__c];
	        system.assertEquals(5,bcs.size());
	        system.assertEquals(-9 , bcs[0].From_Age__c);
	        system.assertEquals(0 , bcs[0].To_Age__c);
	        system.assertEquals(product1.Id , bcs[0].Product__c);
	        system.assertEquals(0 , bcs[1].From_Age__c);
	        system.assertEquals(6 , bcs[1].To_Age__c);
	        system.assertEquals(product2.Id , bcs[1].Product__c);
	        system.assertEquals(7, bcs[2].From_Age__c);
	        system.assertEquals(12 , bcs[2].To_Age__c);
	        system.assertEquals(product3.Id , bcs[2].Product__c);
	        system.assertEquals(13 , bcs[3].From_Age__c);
	        system.assertEquals(36 , bcs[3].To_Age__c);
	        system.assertEquals(product1.Id , bcs[3].Product__c);
	        system.assertEquals(37 , bcs[4].From_Age__c);
	        system.assertEquals(72 , bcs[4].To_Age__c);
	        system.assertEquals(product2.Id , bcs[4].Product__c);
	        system.assertEquals(true , bcs[4].Converted_from_Lead__c);
	        
	        //assert hospital
	        List<Contact> babies = [Select Born_Hospital__c FROM Contact WHERE Born_Hospital__c <> NULL];
	        system.assertEquals(1,babies.size());
	        system.assertEquals(hosp.Id, babies[0].Born_Hospital__c);  	
    	}
        
    }

    /*HELPER*/
    static void createExistingHousehold(Integer numExistingBabies){
        //Create household 
        Account acc = new Account();
        acc.RecordTypeId =  ObjectUtil.GetRecordTypesByDevName(Account.SObjectType).get(CommonHelper.HOUSEHOLD_RT).Id;
        
        acc.Name = 'PRIMARY PARENT';
        acc.Last_Name__c = 'PRIMARY';
        acc.First_Name__c = 'PARENT';
        insert acc;
        //pri parent contact shld be also created by trigger
        system.debug([SELECT Name, id from account where id =:acc.id]);
        system.debug([SELECT Name, id from contact where Accountid =:acc.id]);
        //create babies! 
        List<Contact> cons = new List<Contact>();
        for (Integer i = 0; i<numExistingBabies;i++){
            Contact con = new Contact();
            con.LastName = 'Existing Baby';
            con.FirstName = 'TEST';
            con.RecordTypeId = ObjectUtil.GetRecordTypesByDevName(Contact.SObjectType).get(CommonHelper.HOUSEHOLD_CHILD_RT).Id;
            con.AccountId = acc.Id;
            cons.add(con);
        }
        insert cons;
        
        //create existing RC for existing babies
        List<Recruitment_Channel__c> rcList = new List<Recruitment_Channel__c>();
        for (Contact con : cons){
            Recruitment_Channel__c rc = new Recruitment_Channel__c();
            rc.Primary__c = true;
            rc.Channel__c = 'Web';
            rc.Contact__c = con.Id;
            rcList.add(rc);
        }
        insert rcList;
    }
}