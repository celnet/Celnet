/*Author:leo.bi@celnet.com.cn
 *Created On:2014-4-21
 *function:The Class Controller for InvitationFilter visualforce page
 *Apply To:CN
 */ 
public class InvitationFilterController 
{
	public Invitation__c inv {get;set;} {inv= new Invitation__c();} //Invitation
	public Contact baby {get;set;} {baby = new Contact();}//CN_Child type Contact
	public Contact babyEnd{get;set;} {babyEnd = new Contact();}//CN_Child type Contact for Birthday endpoint and Register Date EndPoint
	public Answer__c ans {get;set;} {ans = new Answer__c();}//Answer
	public Answer__c ans_Expanded {get;set;} {ans_Expanded = new Answer__c();}//Answer for expand section
	public Task tas  {get;set;} {tas = new Task();tas.Result__c = '';}//task
	//public Address_Management__c am {get;set;} {am = new Address_Management__c();}//Address
	public Answer__c ansEnd {get;set;} {ansEnd = new Answer__c();}//answer for finishTime on
	
	//variables for sales area lists
	public String selectedSalesRegion{get;set;}
	public String selectedSalesSubRegion{get;set;}
	public String selectedSalesArea{get;set;} 
	public String selectedSalesCity {get;set;}
	List<String> list_Region = new List<String>(); 
	public Map<Id,String> map_Id_SalesSubRegion = new Map<Id,String>();
	public Map<Id,String> map_Id_SalesArea = new Map<Id,String>();
	public Map<Id,String> map_Id_SalesCity = new Map<Id,String>();
	
	public Boolean isShow{get;set;}{isShow = true;}//variables for components' visibility
	
	
	public Boolean salesSubregionDisable{get;set;}{salesSubregionDisable = true;}
	public Boolean salesAreaDisable{get;set;}{salesAreaDisable = true;}
	public Boolean salesCityDisable{get;set;}{salesCityDisable = true;}
	
	//variables for multiselect
	public string selectedDigestiveSymptoms{get;set;}
	public string selectedAllergySymptoms{get;set;}
	public string selectedSurvey_DigestiveSymptoms{get;set;}
	public string selectedSurvey_AllergySymptoms{get;set;}
	public string selectedSurvey_Last_DigestiveSymptoms{get;set;}
	public string selectedSurvey_Last_AllergySymptoms{get;set;}
	public string selectedMultiSelect_Brand{get;set;}
	public Map<String,String> map_Digestive{get;set;} {map_Digestive = new Map<String,String>();}
	public Map<String,String> map_Allergy{get;set;} {map_Allergy = new Map<String,String>();}
	public Map<String,String> map_Survey_Digestive{get;set;} {map_Survey_Digestive = new Map<String,String>();}
	public Map<String,String> map_Survey_Allergy{get;set;} {map_Survey_Allergy = new Map<String,String>();}
	public Map<String,String> map_Survey_Last_Digestive{get;set;} {map_Survey_Last_Digestive = new Map<String,String>();}
	public Map<String,String> map_Survey_Last_Allergy{get;set;} {map_Survey_Last_Allergy = new Map<String,String>();}
	public Map<String,String> map_Multi_Brand{get;set;} {map_Multi_Brand = new Map<String,String>();}
	
	//Account Status
	public string selectedAccStatus{get;set;}
	
	//variables for campaign region
	public String selectedCampaignRegion{get;set;}
	public String selectedCampaignSubRegion{get;set;}
	public String selectedCampaignArea{get;set;} 
	public String selectedCampaignCity {get;set;}
	
	public Boolean campaignSubregionDisable{get;set;}{campaignSubregionDisable = true;}
	public Boolean campaignAreaDisable{get;set;}{campaignAreaDisable = true;}
	public Boolean campaignCityDisable{get;set;}{campaignCityDisable = true;}
	
	public Boolean campaignItemsDisable{get;set;}{campaignItemsDisable = true;}
	
	public Boolean isAlert{get;set;}{isAlert = false;}
	
	public String selectedChannel{get;set;}
	public String selectedSubChannel{get;set;}
	public Recruitment_Channel__c rc{get;set;}
	public List<SelectOption> RegisterSourceList{get;set;}
	public List<SelectOption> RegisterSubSourceList{get;set;}
	public Map<String,Set<String>> Map_Register{get;set;}
	public Map<String,List<SelectOption>> Map_Source{get;set;}
	public Boolean chanelMessageIsShow{get;set;}{chanelMessageIsShow = true;}
	public Boolean channelNameHopitalIsShow{get;set;} {channelNameHopitalIsShow = false;}
	public Boolean channelNameStoreIsShow{get;set;} {channelNameStoreIsShow = false;}
	
	public String answerSOQL;
	  
	public InvitationFilterController()
	{
		rc = new Recruitment_Channel__c();
		this.initBusinessAddress();
		this.CrtRegisterSelectOption();
	}
	private void initBusinessAddress()
	{
		List<Address_Management__c> list_temp = [Select Region__c From Address_Management__c where Region__c != null];
		Set<String> set_temp = new Set<String>();
		if(list_temp.size()>0)
		{
			for(Address_Management__c a : list_temp)
			{
				set_temp.add(a.Region__c);
			}
			list_Region.addAll(set_temp);
		}
	}
	public List<SelectOption> salesRegionItems
	{
		get
		{
			List<SelectOption> region = new List<SelectOption>();
			region.add(new SelectOption('', ''));
			if(list_Region.size() != 0) 
			{
				for(String a : list_Region)
				{
					region.add(new SelectOption(a,a));
				}
			}
			return region;
		}
	}
	
	public List<SelectOption> salesSubRegionItems
	{
		get
		{
			List<SelectOption> options = new List<SelectOption>();
			Set<String> set_De_Duplicate = new Set<String>();//de-duplicate
			options.add(new SelectOption('', ''));
			if(selectedSalesRegion != null && selectedSalesRegion != '')
			{
				List<Address_Management__c> list_temp_am = [select Id, Region__c,Sub_Region__c 
																	 from Address_Management__c 
																	 where Region__c=:selectedSalesRegion 
																	 and Sub_Region__c!=null];
				for(Address_Management__c a : list_temp_am)
				{
					if(!set_De_Duplicate.contains(a.Sub_Region__c))
					{
						set_De_Duplicate.add(a.Sub_Region__c);
						options.add(new SelectOption(a.Id,a.Sub_Region__c));
						map_Id_SalesSubRegion.put(a.Id,a.Sub_Region__c);
					}
				}
			}
			return options;
		}
	}
	
	public List<SelectOption> salesAreaItems
	{
		get
		{
			List<SelectOption> options = new List<SelectOption>();
			Set<String> set_De_Duplicate = new Set<String>();//de-duplicate
			options.add(new SelectOption('', ''));
			if(selectedSalesSubRegion != null && selectedSalesSubRegion != '')
			{
				List<Address_Management__c> list_temp_am = [select Id, Region__c,Sub_Region__c,Area__c
																     from Address_Management__c 
																     where Sub_Region__c=:map_Id_SalesSubRegion.get(selectedSalesSubRegion) 
																     and Region__c= :selectedSalesRegion and Area__c != null];
				for(Address_Management__c a : list_temp_am)
				{
					if(!set_De_Duplicate.contains(a.Area__c))
					{
						set_De_Duplicate.add(a.Area__c);
						options.add(new SelectOption(a.Id,a.Area__c));
						map_Id_SalesArea.put(a.Id,a.Area__c);
					}
				}
			}
			return options;
		}
	}
	
	public List<SelectOption> salesCityItems
	{
		get
		{
			List<SelectOption> options = new List<SelectOption>();
			Set<String> set_De_Duplicate = new Set<String>();//de-duplicate
			options.add(new SelectOption('', ''));
			if(selectedSalesArea != null && selectedSalesArea != '')
			{
				List<Address_Management__c> list_temp_am = [select Id, Region__c, Sub_Region__c, Area__c, Sales_City__c 
														    from Address_Management__c 
														    where Region__c=:selectedSalesRegion 
															and Sub_Region__c=:map_Id_SalesSubRegion.get(selectedSalesSubRegion) 
															and Area__c=:map_Id_SalesArea.get(selectedSalesArea)
															and Sales_City__c!=null];
				for(Address_Management__c a : list_temp_am)
				{
					if(!set_De_Duplicate.contains(a.Sales_City__c))
					{
						set_De_Duplicate.add(a.Sales_City__c);
						options.add(new SelectOption(a.Id,a.Sales_City__c));
						map_Id_SalesCity.put(a.Id,a.Sales_City__c);
					}
				}
			}
			return options;
		}
	}
	public void refreshSalesSubRegion()
	{
		selectedSalesSubRegion = null;
		selectedSalesArea = null;
		selectedSalesCity = null;
		if(selectedSalesRegion != null)
		{
			salesSubregionDisable = false;
			salesAreaDisable = true;
			salesCityDisable = true;
		}
		else
		{
			salesSubregionDisable = true;
			salesAreaDisable = true;
			salesCityDisable = true;
		}
	}
	
	public void refreshSalesArea()
	{
		selectedSalesArea = null;
		selectedSalesCity = null;
		if(selectedSalesSubRegion != null)
		{
			salesSubregionDisable = false;
			salesAreaDisable = false;
			salesCityDisable = true;
		}
		else
		{
			salesAreaDisable = true;
			salesCityDisable = true;
		}
	}
	
	public void refreshSalesCity()
	{
		selectedSalesCity = null;
		if(selectedSalesArea != null)
		{
			salesSubregionDisable = false;
			salesAreaDisable = false;
			salesCityDisable = false;
		}
		else
		{
			salesCityDisable = true;
		}
	}
	
	public List<SelectOption> campaignSubRegionItems
	{
		get
		{
			List<SelectOption> options = new List<SelectOption>();
			Set<String> set_De_Duplicate = new Set<String>();//de-duplicate
			options.add(new SelectOption('', ''));
			if(selectedCampaignRegion != null && selectedCampaignRegion != '')
			{
				List<Address_Management__c> list_temp_am = [select Id, Region__c,Sub_Region__c 
																	 from Address_Management__c 
																	 where Region__c=:selectedCampaignRegion 
																	 and Sub_Region__c!=null];
				for(Address_Management__c a : list_temp_am)
				{
					if(!set_De_Duplicate.contains(a.Sub_Region__c))
					{
						set_De_Duplicate.add(a.Sub_Region__c);
						options.add(new SelectOption(a.Id,a.Sub_Region__c));
						map_Id_SalesSubRegion.put(a.Id,a.Sub_Region__c);
					}
				}
			}
			return options;
		}
	}
	
	public List<SelectOption> campaignAreaItems
	{
		get
		{
			List<SelectOption> options = new List<SelectOption>();
			Set<String> set_De_Duplicate = new Set<String>();//de-duplicate
			options.add(new SelectOption('', ''));
			if(selectedCampaignSubRegion != null && selectedCampaignSubRegion != '')
			{
				List<Address_Management__c> list_temp_am = [select Id, Region__c,Sub_Region__c,Area__c
																     from Address_Management__c 
																     where Sub_Region__c=:map_Id_SalesSubRegion.get(selectedCampaignSubRegion) 
																     and Region__c= :selectedCampaignRegion and Area__c != null];
				for(Address_Management__c a : list_temp_am)
				{
					if(!set_De_Duplicate.contains(a.Area__c))
					{
						set_De_Duplicate.add(a.Area__c);
						options.add(new SelectOption(a.Id,a.Area__c));
						map_Id_SalesArea.put(a.Id,a.Area__c);
					}
				}
			}
			return options;
		}
	}
	
	public List<SelectOption> campaignCityItems
	{
		get
		{
			List<SelectOption> options = new List<SelectOption>();
			Set<String> set_De_Duplicate = new Set<String>();//de-duplicate
			options.add(new SelectOption('', ''));
			if(selectedCampaignArea != null && selectedCampaignArea != '')
			{
				List<Address_Management__c> list_temp_am = [select Id, Region__c, Sub_Region__c, Area__c, Sales_City__c 
														    from Address_Management__c 
														    where Region__c=:selectedCampaignRegion 
															and Sub_Region__c=:map_Id_SalesSubRegion.get(selectedCampaignSubRegion) 
															and Area__c=:map_Id_SalesArea.get(selectedCampaignArea)
															and Sales_City__c!=null];
				for(Address_Management__c a : list_temp_am)
				{
					if(!set_De_Duplicate.contains(a.Sales_City__c))
					{
						set_De_Duplicate.add(a.Sales_City__c);
						options.add(new SelectOption(a.Id,a.Sales_City__c));
						map_Id_SalesCity.put(a.Id,a.Sales_City__c);
					}
				}
			}
			return options;
		}
	}
	
	public List<SelectOption> campaignItems
	{
		get
		{
			List<SelectOption> options = new List<SelectOption>();
			Set<String> set_De_Duplicate = new Set<String>();//de-duplicate
			options.add(new SelectOption('', ''));
			List<Campaign> list_Campaign = new List<Campaign>();
			if(selectedCampaignCity != null && selectedCampaignCity  != '')
			{
				list_Campaign = [select Id, Name from Campaign where Status !='Cancelled' and Status !='Completed' and Standard_City__r.Sales_City__c = :map_Id_SalesCity.get(selectedCampaignCity) and IsActive = true];
			}
			else if(selectedCampaignArea != null && selectedCampaignArea  != '')
			{
				list_Campaign = [select Id, Name from Campaign 
								 where Status !='Cancelled' and Status !='Completed' and Region_in_chinese__c = :selectedCampaignRegion 
								 and Sub_region_in_chinese__c = :map_Id_SalesSubRegion.get(selectedCampaignSubRegion)
								 and Area_in_chinese__c = :map_Id_SalesArea.get(selectedCampaignArea)
								 and IsActive = true];
			}
			else if(selectedCampaignSubRegion != null && selectedCampaignSubRegion  != '')
			{
				list_Campaign = [select Id, Name from Campaign 
								 where Status !='Cancelled' and Status !='Completed' and Region_in_chinese__c = :selectedCampaignRegion 
								 and Sub_region_in_chinese__c = :map_Id_SalesSubRegion.get(selectedCampaignSubRegion) and IsActive = true];
			}
			for(Campaign c : list_Campaign)
			{
				options.add(new SelectOption(c.Id,c.Name));
			}
			return options;
		}
	}
	
	public void refreshCampaignSubRegion()
	{
		selectedCampaignSubRegion = null;
		selectedCampaignArea = null;
		selectedCampaignCity = null;
		if(selectedCampaignRegion != null)
		{
			campaignSubregionDisable = false;
			campaignAreaDisable = true;
			campaignCityDisable = true;
			campaignItemsDisable = true;
		}
		else
		{
			campaignSubregionDisable = true;
			campaignAreaDisable = true;
			campaignCityDisable = true;
			campaignItemsDisable = true;
		}
	}
	
	public void refreshCampaignArea()
	{
		selectedCampaignArea = null;
		selectedCampaignCity = null;
		if(selectedCampaignSubRegion != null)
		{
			campaignItemsDisable = false;
			campaignAreaDisable = false;
			campaignCityDisable = true;
		}
		else
		{
			campaignAreaDisable = true;
			campaignCityDisable = true;
			campaignItemsDisable = true;
		}
	}
	
	public void refreshCampaignCity()
	{
		selectedCampaignCity = null;
		if(selectedCampaignArea != null)
		{
			campaignItemsDisable = false;
			campaignAreaDisable = false;
			campaignCityDisable = false;
		}
		else
		{
			campaignCityDisable = true;
		}
	}
	
	public void refreshCampaignItems()
	{
		inv.Campaign__c = null;
		if(selectedCampaignRegion != null && selectedCampaignRegion != '' && selectedCampaignSubRegion!= null && selectedCampaignSubRegion != '')
		{
			campaignItemsDisable = false;
		}
		else
		{
			campaignItemsDisable = true; 
		}
		this.refreshRemark();
	}
	
	public void refreshRemark()
	{
		if(inv.Campaign__c != null)
		{
			Campaign cm = [select Description, Id from Campaign where Id = :inv.Campaign__c];
			if(cm != null && cm.Description != null)
			{
				inv.Remark__c = cm.Description;
			}
		}
	}
	
	public List<SelectOption> list_AccountStatus
	{
		get
		{
			list<SelectOption> list_temp = new list<SelectOption>();
			list_temp.add(new SelectOption('',''));
			Schema.DescribeFieldResult payList = Account.status__c.getDescribe();
			List<Schema.PicklistEntry> ListPay = payList.getPicklistValues();
			for(Schema.PicklistEntry sp : ListPay)
			{
				list_temp.add(new SelectOption(sp.getValue(),sp.getLabel()));
			}
			return list_temp;
		}
		set;
	}
	
	public string[] list_DigestiveSymptoms
	{
		get
		{
			list<string> list_temp = new list<string>();
			Schema.DescribeFieldResult payList = Contact.Digestive_Symptoms_Type__c.getDescribe();
			List<Schema.PicklistEntry> ListPay = payList.getPicklistValues();
			for(Schema.PicklistEntry sp : ListPay)
			{
				list_temp.add(sp.getLabel());
				map_Digestive.put(sp.getLabel(),sp.getValue());
			}
			return list_temp;
		}
		set;
	}
	
	public string[] list_AllergySymptoms
	{
		get
		{
			list<string> list_temp = new list<string>();
			Schema.DescribeFieldResult payList = Contact.Allergy_Symptoms_Type__c.getDescribe();
			List<Schema.PicklistEntry> ListPay = payList.getPicklistValues();
			for(Schema.PicklistEntry sp : ListPay)
			{
				list_temp.add(sp.getLabel());
				map_Allergy.put(sp.getLabel(),sp.getValue());
			}
			return list_temp;
		}
		set;
	}
	
	public string[] list_Survey_DigestiveSymptoms
	{
		get
		{
			list<string> list_temp = new list<string>();
			Schema.DescribeFieldResult payList = Answer__c.Q011__c.getDescribe();
			List<Schema.PicklistEntry> ListPay = payList.getPicklistValues();
			for(Schema.PicklistEntry sp : ListPay)
			{
				list_temp.add(sp.getLabel());
				map_Survey_Digestive.put(sp.getLabel(),sp.getValue());
			}
			return list_temp;
		}
		set;
	}
	
	public string[] list_Survey_AllergySymptoms
	{
		get
		{
			list<string> list_temp = new list<string>();
			Schema.DescribeFieldResult payList = Answer__c.Q046__c.getDescribe();
			List<Schema.PicklistEntry> ListPay = payList.getPicklistValues();
			for(Schema.PicklistEntry sp : ListPay)
			{
				list_temp.add(sp.getLabel());
				map_Survey_Allergy.put(sp.getLabel(),sp.getValue());
			}
			return list_temp;
		}
		set;
	}
	
	public string[] list_Survey_Last_DigestiveSymptoms
	{
		get
		{
			list<string> list_temp = new list<string>();
			Schema.DescribeFieldResult payList = Answer__c.Q011_Last__c.getDescribe();
			List<Schema.PicklistEntry> ListPay = payList.getPicklistValues();
			for(Schema.PicklistEntry sp : ListPay)
			{
				list_temp.add(sp.getLabel());
				map_Survey_Last_Digestive.put(sp.getLabel(),sp.getValue());
			}
			return list_temp;
		}
		set;
	}
	
	public string[] list_Survey_Last_AllergySymptoms
	{
		get
		{
			list<string> list_temp = new list<string>();
			Schema.DescribeFieldResult payList = Answer__c.Q046_Last__c.getDescribe();
			List<Schema.PicklistEntry> ListPay = payList.getPicklistValues();
			for(Schema.PicklistEntry sp : ListPay)
			{
				list_temp.add(sp.getLabel());
				map_Survey_Last_Allergy.put(sp.getLabel(),sp.getValue());
			}
			return list_temp;
		}
		set;
	}
	
	//孕妇样包品牌
	public string[] list_MultiSelect_Brand
	{
		get
		{
			list<string> list_temp = new list<string>();
			Schema.DescribeFieldResult payList = Answer__c.Q017__c.getDescribe();
			list<Schema.PicklistEntry> ListPay = payList.getPicklistValues();
			for(Schema.PicklistEntry sp : ListPay)
			{
				list_temp.add(sp.getLabel());
				map_Multi_Brand.put(sp.getLabel(),sp.getValue());
			}
			return list_temp;
		}
		set;
	}
	
	public void CrtRegisterSelectOption()
	{
		//Register Source Mapping
		Map_Register = new Map<String,Set<String>>();
		for(BF_Register_Source_Mapping__c RS : BF_Register_Source_Mapping__c.getAll().values())
		{
			if(Map_Register.containsKey(RS.Register_Source_Label__c))
			{
				Set<String> Set_SubSource = Map_Register.get(RS.Register_Source_Label__c);
				Set_SubSource.add(RS.Register_Sub_Source_Label__c);
				Map_Register.put(RS.Register_Source_Label__c,Set_SubSource);   
			}
			else
			{
				Set<String> Set_SubSource = new Set<String>();
				if(RS.Register_Sub_Source_Label__c !=null)
				Set_SubSource.add(RS.Register_Sub_Source_Label__c);
				Map_Register.put(RS.Register_Source_Label__c,Set_SubSource);
			}
		}
		RegisterSourceList = new List<SelectOption>();
		RegisterSourceList.add(new SelectOption('',''));
		Map_Source =  new Map<String,List<SelectOption>>();
		for(String ReSource : Map_Register.KeySet())
		{
			RegisterSourceList.add(new SelectOption(ReSource,ReSource));
			List<SelectOption> List_SO = new List<SelectOption>();
			List_SO.add(new SelectOption('',''));
			for(String ReSubSurce : Map_Register.get(ReSource))
			{
				List_SO.add(new SelectOption(ReSubSurce,ReSubSurce));
			}
			Map_Source.put(ReSource,List_SO);
		}
		if(RegisterSourceList.size()>0)
		this.AutoShowSubSource();		
	}
	
	public void AutoShowSubSource()
	{
		RegisterSubSourceList = new List<SelectOption>();
		if(Map_Source.containsKey(selectedChannel))
		RegisterSubSourceList = Map_Source.get(selectedChannel);
		if(selectedChannel == '手工/纸质hospital')
		{
			channelNameHopitalIsShow = true;
			channelNameStoreIsShow = false;
			chanelMessageIsShow = false;
			rc.Store__c = null;
		}
		else if(selectedChannel == '手工/纸质Store' || selectedChannel =='NC User 手工/纸质Store')
		{    
			channelNameHopitalIsShow = false;
			channelNameStoreIsShow = true;
			chanelMessageIsShow = false;
			rc.Hospital__c = null;
		}
		else
		{
			channelNameHopitalIsShow = false;
			channelNameStoreIsShow = false;
			chanelMessageIsShow = true; 
			rc.Store__c = null;
			rc.Hospital__c = null;  
		}
	}
	
	//Tommy modifed at 2014-6-20, 将数据校验从主方法里面提取出来
	private Boolean dataValidation()
	{
		//error message
		string ErrorMessage = '';
		isShow = true;
		if(inv.Name==null || inv.Name=='')
		{
			system.debug('------------------inv.Name--------------------' + inv.Name);
			ErrorMessage +='请键入Invitation的名称 ' + '\n';
		}
		if(inv.Campaign_Member_Limit__c == null)
		{
			ErrorMessage +='请键入Invitation的限制数量 ' + '\n';
		}
		if(inv.Campaign__c==null)
		{
			ErrorMessage +=' 请选择市场活动 ' + '\n';
		}
		if(selectedSalesRegion ==null || selectedSalesRegion =='')
		{
			ErrorMessage += ' 请选择大区 ' + '\n';
		}
		if(selectedSalesSubRegion==null || selectedSalesSubRegion=='')
		{
			ErrorMessage += ' 请选择分区 ' + '\n';
		}
		if(baby.Birthdate != null && babyEnd.Birthdate==null)
		{
			ErrorMessage += '请选择出生日期筛选终点' + '\n';
		}
		if(baby.Birthdate == null && babyEnd.Birthdate != null)
		{
			ErrorMessage += '请选择出生日期起点' + '\n';
		}
		if(baby.Allergy_Symptoms_Type__c != null && baby.Is_Last_Allergy_Symptoms__c != 'Y')
		{
			ErrorMessage += '请先勾选是否有过敏问题（Expanded）' + '\n';
		}
		if(baby.Digestive_Symptoms_Type__c != null && baby.Is_Last_Digestive_Symptoms__c != 'Y')
		{
			ErrorMessage += '请先勾选是否有消化问题（Expanded）' + '\n';
		}
		if(baby.Last_Routine_Call_Time__c != null && baby.Routine_Evaluated_On__c == null)
		{
			ErrorMessage += '请选择上次致电时间终点' + '\n';
		}
		if(baby.Routine_Evaluated_On__c != null && baby.Last_Routine_Call_Time__c == null)
		{
			ErrorMessage += '请选择上次致电时间起点' + '\n';
		}
		if(baby.Purchase_Date__c != null && babyEnd.Purchase_Date__c == null)
		{
			ErrorMessage += '请选择购买日期终点' + '\n';
		}
		if(babyEnd.Purchase_Date__c != null && baby.Purchase_Date__c == null)
		{
			ErrorMessage += '请选择购买日期起点' + '\n';
		}
		if(ans.Questionnaire__c == null && (babyEnd.Last_Routine_Call_Time__c != null || babyEnd.Routine_Evaluated_On__c != null || (tas.Result__c != null && tas.Result__c != '')
											|| ans.Q008_Invitation__c !=null|| ans.Q008__c != null || ans.Q011__c != null 
											||ans.Q011_Last__c != null || ans.Q017__c != null ||ansEnd.Finished_On__c != null
											||ans.Finished_On__c != null || ans.Q046__c != null || ans.Q046_Last__c != null
											||ans.Q038__c != null || ans.Q039__c != null || ans.Q030__c != null
											||ans.Q045__c != null || ans.Q030_Last__c != null || ans.Q045_Last__c != null
											||ans.Q023__c != null))
		{
			system.debug(ans.Questionnaire__c);
			ErrorMessage += '请选择问卷编号 ';  
		}
		if(ans.Finished_On__c != null && ansEnd.Finished_On__c == null)
		{
			ErrorMessage += '请选择问卷完成电时间终点' + '\n';
		}
		if(ans.Finished_On__c == null && ansEnd.Finished_On__c != null)
		{
			ErrorMessage += '请选择问卷完成电时间起点' + '\n';
		}
		if(ans.Q017__c != null && ans.Q023__c != 'Y')
		{
			ErrorMessage += '请先勾选是否收到孕妇奶粉样包' + '\n';
		}
		if(ans.Q011__c !=null && ans.Q030__c != 'Y')
		{
			ErrorMessage += '请先勾选是否有消化问题' + '\n';
		}
		if(ans.Q011_Last__c != null && ans.Q030_Last__c != 'Y')
		{
			ErrorMessage += '请先勾选是否有消化问题（上次）' + '\n';
		}
		if(ans.Q046__c !=null && ans.Q045__c != 'Y')
		{
			ErrorMessage += '请先勾选是否有过敏问题' + '\n';
		}
		if(ans.Q046_Last__c != null && ans.Q045_Last__c != 'Y')
		{
			ErrorMessage += '请先勾选是否有过敏问题（上次）' + '\n';
		}
		if(babyEnd.Last_Routine_Call_Time__c != null && babyEnd.Routine_Evaluated_On__c == null)
		{
			ErrorMessage += '请选择致电结束日期 ' + '\n';
		}
		if(babyEnd.Routine_Evaluated_On__c != null && babyEnd.Last_Routine_Call_Time__c == null)
		{
			ErrorMessage += '请选择致电开始日期  ' + '\n';
		}
		if((babyEnd.Routine_Evaluated_On__c != null || babyEnd.Last_Routine_Call_Time__c != null) && (tas.Result__c == null || tas.Result__c == ''))
		{
			ErrorMessage += '请选择致电结果  ' + '\n';
		}
		if((babyEnd.Routine_Evaluated_On__c == null && babyEnd.Last_Routine_Call_Time__c == null) && tas.Result__c != null && tas.Result__c != '')
		{
			ErrorMessage += '请选择致电时间 ' + '\n';
		}
		if(ErrorMessage != '')
		{
			isShow = true;  
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.ERROR, ErrorMessage);
	    	ApexPages.addMessage(myMsg);
	    	ErrorMessage = '';
	    	return false;
		}
		isShow = false;
		return true;
	}
	
	//generate SOQL Script, Create and Launch Invitation Job Manager
	public void run()
	{
		if(!this.dataValidation())
		{
			return;
		}
		//Insert log to Invatation SObject
		inv.SOQL_Script__c = generateQueryString();
		if(this.answerSOQL != null && this.answerSOQL != '')
		{
			inv.Answer_SOQL_Script__c = this.answerSOQL;
		}
		if(babyEnd.Last_Routine_Call_Time__c != null)
		{
			inv.Tast_Start_Time__c = babyEnd.Last_Routine_Call_Time__c;
		}
		if(babyEnd.Routine_Evaluated_On__c != null)
		{
			inv.Task_End_Time__c = babyEnd.Routine_Evaluated_On__c;
		}
		if(tas.Result__c != null && tas.Result__c != '')
		{
			inv.Task_Call_Result__c = tas.Result__c;
		}
		inv.Status__c = 'Queued';
		insert inv;
		//execute Invatationbatch
		CN_InvitationBatchJobManager bm = new CN_InvitationBatchJobManager(this.checkInvitationJobLimit());
		bm.execute();
		isAlert = true; 
	}
	
	private Integer checkInvitationJobLimit()
	{
		Integer result = 4;
		Map<String, InvitationJobNumberLimit__c> map_limit = InvitationJobNumberLimit__c.getAll();
		if(map_limit.isEmpty())
		{
			return result;
		}
		for(String s : map_limit.keySet())
		{
			InvitationJobNumberLimit__c limitNum = map_limit.get(s);
			result = Integer.valueOf(limitNum.Limit_Number__c);
		}
		return result;
	}
	
	//generate SOQL Script
	public String generateQueryString()
	{
		String accountRecordType = [select id from RecordType where DeveloperName=:'CN_HouseHold' and SobjectType=:'Account'].id;
		//String contactRecordType = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
		//main query string
		String final_Query;
		String string_Query = 'select Id,Account.Duplicate_Status__c from Contact where Inactive__c=false and RecordType.DeveloperName=' + '\'CN_Child\' and Account.RecordTypeId=\''
								+ accountRecordType + '\'';
		String string_Region = '';//region section query string
		String string_Filter = '';//"Contact Specify Filter Criteria" section query string
		String string_Expanded = '';//"Contact Expanded Information" section query string
		String string_Survey_Task ='';//"Contact Survey History Information" section for Task query string
		String string_Survey_Answer ='';//"Contact Survey History Information" section for Answer query string
		
		//"Contact Survey History Information" section for Answer query string and Task
		//"Contact Survey History Information" section Task query string
		if(ans.Questionnaire__c != null && babyEnd.Routine_Evaluated_On__c != null && babyEnd.Last_Routine_Call_Time__c != null && tas.Result__c != null)
		{
			string_Survey_Task = '(Select WhatId, CreatedDate, Result__c From Tasks where WhatId=\'' + ans.Questionnaire__c
							   + '\' order by CreatedDate Desc limit 1)'; 
			string_Query = 'select Account.Duplicate_Status__c,Id,' + string_Survey_Task + ' from Contact where Inactive__c=false and RecordType.DeveloperName=' + '\'CN_Child\' and Account.RecordTypeId=\'' + accountRecordType + '\'';
		}
		//"Contact Survey History Information" section for Answer query string	
		if(ans.Questionnaire__c != null)
		{
			Boolean needAddAnd = false;
			Boolean onlychooseTask = true;
		    string_Survey_Answer += 'Select Contact__c ,Status__c From Answer__c where';
		    if(ans.Q008_Invitation__c != null && ans.Q008_Invitation__c != '')
			{
				String Q008_Invitation = ' Q008_Invitation__c=\'' + ans.Q008_Invitation__c + '\'';
				string_Survey_Answer += this.answerSOQLAddAnd(Q008_Invitation, false);
				needAddAnd = true;
				onlychooseTask = false;
			}
			if(ans.Q008__c != null && ans.Q008__c != '')
			{
				String Q008 = ' Q008__c=\'' + ans.Q008__c + '\'';
				string_Survey_Answer += this.answerSOQLAddAnd(Q008, needAddAnd);
				needAddAnd = true;
				onlychooseTask = false;
			}
			if(selectedSurvey_DigestiveSymptoms != null && selectedSurvey_DigestiveSymptoms != '')
			{
				String Q011_Text = ' Q011_Text__c like ' + this.formatExternalIdField(selectedSurvey_DigestiveSymptoms,map_Survey_Digestive);
				string_Survey_Answer += this.answerSOQLAddAnd(Q011_Text, needAddAnd);
				needAddAnd = true;
				onlychooseTask = false;
			}
			if(selectedSurvey_Last_DigestiveSymptoms != null && selectedSurvey_Last_DigestiveSymptoms != '')
			{
				String Q011_Text_Last = ' Q011_Last_Text__c like ' + this.formatExternalIdField(selectedSurvey_Last_DigestiveSymptoms,map_Survey_Last_Digestive);
				string_Survey_Answer += this.answerSOQLAddAnd(Q011_Text_Last, needAddAnd);
				needAddAnd = true;
				onlychooseTask = false;
			}
			if(selectedMultiSelect_Brand != null && selectedMultiSelect_Brand != '')
			{
				String Q017_Text = ' Q017_Text__c like ' + this.formatExternalIdField(selectedMultiSelect_Brand, map_Multi_Brand);
				string_Survey_Answer += this.answerSOQLAddAnd(Q017_Text, needAddAnd);
				needAddAnd = true;
				onlychooseTask = false;
			}
			if(babyEnd.Routine_Evaluated_On__c == null && babyEnd.Last_Routine_Call_Time__c == null && (tas.Result__c == null || tas.Result__c == ''))
			{
				onlychooseTask = false;
			}
			String questionnaier = ' Questionnaire__c=\'' + ans.Questionnaire__c + '\'';
			string_Survey_Answer += this.answerSOQLAddAnd(questionnaier, needAddAnd);
			if(ans.Q030_Last__c != null && ans.Q030_Last__c != '')
			{
				string_Survey_Answer += ' and Q030_Last__c=\'' + ans.Q030_Last__c + '\'';
				onlychooseTask = false;
			}
			if(ans.Q023__c != null && ans.Q023__c != '')
			{
				string_Survey_Answer += ' and Q023__c=\'' + ans.Q023__c + '\'';
				onlychooseTask = false;
			}
			if(ans.Q038__c != null && ans.Q038__c != '')
			{
				string_Survey_Answer += ' and Q038__c=\'' + ans.Q038__c + '\'';
				onlychooseTask = false;
			}
			if(ans.Q039__c != null && ans.Q039__c != '')
			{
				string_Survey_Answer += ' and Q039__c=\'' + ans.Q039__c + '\'';
				onlychooseTask = false;
			}
			if(ans.Q030__c != null && ans.Q030__c != '')
			{
				string_Survey_Answer += ' and Q030__c=\'' + ans.Q030__c + '\'';
				onlychooseTask = false;
			}
			if(ans.Q045__c != null && ans.Q045__c != '')
			{
				string_Survey_Answer += ' and Q045__c=\'' + ans.Q045__c + '\'';
				onlychooseTask = false;
			}
			if(ans.Q045_Last__c != null && ans.Q045_Last__c != '')
			{
				string_Survey_Answer += ' and Q045_Last__c=\'' + ans.Q045_Last__c + '\'';
				onlychooseTask = false;
			}
			if(selectedSurvey_AllergySymptoms != null && selectedSurvey_AllergySymptoms != '')
			{
				string_Survey_Answer += ' and Q046__c includes ' + this.formatMultiSelect(selectedSurvey_AllergySymptoms,map_Survey_Allergy);
				onlychooseTask = false;
			}
			if(selectedSurvey_Last_AllergySymptoms != null && selectedSurvey_Last_AllergySymptoms != '')
			{
				string_Survey_Answer += ' and Q046_Last__c includes' + this.formatMultiSelect(selectedSurvey_Last_AllergySymptoms,map_Survey_Last_Allergy);
				onlychooseTask = false;
			}
			if(ans.Finished_On__c != null && ansEnd.Finished_On__c != null)
			{
				string_Survey_Answer += ' and Finished_On__c >=' + formatDateTime(ans.Finished_On__c) + ' and Finished_On__c <=' + formatDateTime(ansEnd.Finished_On__c);
				onlychooseTask = false;
			}
			if(!onlychooseTask)
			this.answerSOQL = string_Survey_Answer;
		}
		//generate region select query string
		if(selectedSalesRegion != null && selectedSalesRegion != '')
		{
			string_Region += ' and Account.Region_in_China__c=\'' + selectedSalesRegion + '\'';
		}
		if(selectedSalesSubRegion != null && selectedSalesSubRegion !='')
		{
			string_Region += ' and Account.Sub_Region__c=\'' +map_Id_SalesSubRegion.get(selectedSalesSubRegion) +'\'';
		}
		if(selectedSalesArea != null && selectedSalesArea != '')
		{
			string_Region += ' and Account.Area__c=\'' + map_Id_SalesArea .get(selectedSalesArea) + '\'';
		}
		if(selectedSalesCity  != null && selectedSalesCity  != '')
		{
			string_Region += ' and Account.Sales_City__c=\'' + map_Id_SalesCity.get(selectedSalesCity) + '\'';
		}
		//generate "Contact Specify Filter Criteria" section query string
		if(selectedChannel != null)
		{
			string_Filter += ' and Primary_RecruitChannel_Type__c=\'' + selectedChannel +'\'';
		}
		if(selectedSubChannel != null)
		{
			string_Filter += ' and Primary_RecruitChannel_Sub_type__c=\'' + selectedSubChannel + '\'';
		}
		if(rc.Hospital__c != null && rc.Store__c == null)
		{
			string_Filter += ' and Channel_Name__c=\'' + rc.Hospital__c + '\'';
		}
		if(rc.Hospital__c == null && rc.Store__c != null)
		{
			string_Filter += ' and Channel_Name__c=\'' + rc.Store__c + '\'';
		}/*
		if(baby.Specific_Code__c != null && baby.Specific_Code__c != '')
		{
			string_Filter += ' and Specific_Code__c=\'' + baby.Specific_Code__c + '\'';
		}*/
		if(baby.Specific_Code__c != null && baby.Specific_Code__c != '')
		{
			string_Filter += ' and Specific_Code__c in ' + this.formatSpecificCode(baby.Specific_Code__c);
		}
		if(baby.Born_Hospital__c != null) 
		{
			string_Filter += ' and Born_Hospital__c=\'' + baby.Born_Hospital__c + '\'';
		}
		if(selectedAccStatus != null && selectedAccStatus != '')
		{
			string_Filter += ' and Account.status__c=\'' + selectedAccStatus + '\'';
		}
		if(baby.Birthdate != null && babyEnd.Birthdate != null)
		{
			string_Filter += ' and Birthdate >=' + formatDate(baby.Birthdate) + ' and Birthdate <=' + formatDate(babyEnd.Birthdate);
		}
		if(baby.Register_Date__c != null && babyEnd.Birthdate != null)
		{
			string_Filter += ' and Register_Date__c>=' + formatDate(baby.Register_Date__c) + ' and Register_Date__c<=' + formatDate(babyEnd.Register_Date__c);
		}
		if(babyEnd.Purchase_Date__c != null && baby.Purchase_Date__c != null) 
		{
			string_Filter += ' and Purchase_Date__c>=' + formatDate(baby.Purchase_Date__c) + ' and Purchase_Date__c<=' + formatDate(babyEnd.Purchase_Date__c);
		}
		if(baby.Special_Product__c != null && baby.Special_Product__c != '')
		{
			string_Filter += ' and Special_Product__c=\'' + baby.Special_Product__c +'\'';
		}
		if(baby.Is_Winning__c != null)
		{
			string_Filter += ' and Is_Winning__c=\'' + baby.Is_Winning__c + '\'';
		}
		if(baby.Is_P_Class__c != null)
		{
			string_Filter += ' and Is_P_Class__c=\'' + baby.Is_P_Class__c + '\'';
		}
		if(baby.Archive_Month__c != null && baby.Archive_Month__c != '')
		{
			string_Filter += ' and Archive_Month__c=\'' + baby.Archive_Month__c +'\'';
		}
		if(baby.Cannot_Be_Contacted__c != null)
		{
			string_Filter += ' and Cannot_Be_Contacted__c=' + baby.Cannot_Be_Contacted__c;
		}
		if(baby.DoNotCall != null)
		{
			string_Filter += ' and DoNotCall=' + baby.DoNotCall;
		}
		if(baby.Do_Not_Invite__c != null)
		{
			string_Filter += ' and Do_Not_Invite__c=' + baby.Do_Not_Invite__c;
		}
		
		//generate "Contact Expanded Information" section query string
		if(ans_Expanded.Q008_Last__c != null && ans_Expanded.Q008_Last__c != '')
		{
			string_Expanded += ' and X2nd_Last_Consumption_Brand__c=\'' + ans_Expanded.Q008_Last__c + '\'';
		}
		if(ans_Expanded.Q008__c != null && ans_Expanded.Q008__c != '')
		{
			string_Expanded += ' and Last_Consumption_Brand__c=\'' + ans_Expanded.Q008__c +'\'';
		}
		if(baby.Last_Routine_Call_Time__c != null)
		{
			string_Expanded += ' and Last_Routine_Call_Time__c >=' + formatDateTime(baby.Last_Routine_Call_Time__c) + ' and Last_Routine_Call_Time__c <=' + formatDateTime(baby.Routine_Evaluated_On__c);
		}
		if(baby.Last_Routine_Call_Result__c != null && baby.Last_Routine_Call_Result__c != '')
		{
			string_Expanded += ' and Last_Routine_Call_Result__c=\'' + baby.Last_Routine_Call_Result__c +'\'';
		}
		if(baby.Is_Last_Digestive_Symptoms__c != null)
		{
			string_Expanded += ' and Is_Last_Digestive_Symptoms__c=\'' + baby.Is_Last_Digestive_Symptoms__c + '\'';
		}
		if(baby.Is_Last_Allergy_Symptoms__c != null)
		{
			string_Expanded += ' and Is_Last_Allergy_Symptoms__c=\'' + baby.Is_Last_Allergy_Symptoms__c + '\'';
		}
		if(selectedDigestiveSymptoms != null && selectedDigestiveSymptoms != '')
		{
			string_Expanded += ' and Digestive_Symptoms_Type__c includes' + this.formatMultiSelect(selectedDigestiveSymptoms,map_Digestive);
		}
		
		if(selectedAllergySymptoms != null && selectedAllergySymptoms != '')
		{
			string_Expanded += ' and Allergy_Symptoms_Type__c includes ' + this.formatMultiSelect(selectedAllergySymptoms,map_Allergy);
		}
		
		//generate final query string
		final_Query = string_Query + string_Region + string_Filter + string_Expanded;
		return final_Query;
	}
	
	public String formatDate(Datetime dt)
    {
    	String result;
    	String year = String.valueOf(dt.year());
    	String month = '';
    	String day = '';
    	if(dt.month() > 9)
    	{
	    	month = String.valueOf(dt.month());
    	}
    	else
    	{
    		month = '0' + String.valueOf(dt.month());
    	}
    	if(dt.day() > 9)
    	{
	    	day = String.valueOf(dt.day());
    	}
    	else
    	{
    		day = '0' + String.valueOf(dt.day());
    	}
    	//result = Date.newInstance(year, month, day);
    	result = year + '-' + month + '-' + day;
    	return result;
    }
	
	public String formatDateTime(DateTime dt)
	{
		String result = '';
		String dateString = formatDate(dt);
		String timeString = String.valueOf(dt.time());
		result = dateString + 'T' + timeString;
		return result;
	}
	
	public String formatMultiSelect(String para,Map<String,String> map_Temp)
	{
		string result = '';
		List<String> list_Result_Temp = para.split(', ', -1);
		List<String> list_Result = new List<String>();
		for(String s : list_Result_Temp)
		{
			s = map_Temp.get(s);
			list_Result.add(s);
		}
		if(list_Result.size() == 1)
		{
			result ='(\'' + list_Result[0] + '\')';
		}
		else
		{
			for(Integer i = 0; i < list_Result.size(); i++)
			{
				if(i == (list_Result.size()-1))
				{
					result += list_Result[i];
				}
				else
				{
					result += list_Result[i] + ';';
				}
			}
			result = '(\'' + result + '\')';
		}
		return result;
	}
	
	public String formatSpecificCode(String string_Specific_Code)
	{
		string result = '';
		List<String> list_Result_Temp = string_Specific_Code.split(',', -1);
		List<String> list_Result = new List<String>();
		for(String s : list_Result_Temp)
		{
			list_Result.add(s);
		}
		if(list_Result.size() == 1)
		{
			result ='(\'' + list_Result[0] + '\')';
		}
		else
		{
			for(Integer i = 0; i < list_Result.size(); i++)
			{
				if(i == (list_Result.size()-1))
				{
					result += '\'' + list_Result[i] + '\'';
				}
				else
				{
					result += '\'' + list_Result[i] + '\'' + ',';
				}
			}
			result = '(' + result + ')';
		}
		return result;
	}
	
	public String formatExternalIdField(String para,Map<String,String> map_Temp)
	{
		string result = '';
		List<String> list_Result_Temp = para.split(', ', -1);
		List<String> list_Result = new List<String>();
		for(String s : list_Result_Temp)
		{
			s = map_Temp.get(s);
			list_Result.add(s);
		}
		if(list_Result.size() == 1)
		{
			result ='\'%' + list_Result[0] + '%\'';
		}
		else
		{
			for(Integer i = 0; i < list_Result.size(); i++)
			{
				if(i == (list_Result.size()-1))
				{
					result += '%' + list_Result[i] + '%';
				}
				else
				{
					result += '%' + list_Result[i];
				}
			}
			result = '\'' + result + '\'';
		}
		return result;
	}
	
	private String answerSOQLAddAnd(String queryString, Boolean needAddAnd)
	{
		if(needAddAnd)
		{
			return ' and ' + queryString;
		}
		else
		{
			return queryString;
		}
	}
	
}