/*
    This batch will retrieve all HouseHold records which have Mobile_DNCR as TRUE;
	Then this list of records will be verified against the latest DNCR data:
	if HouseHold.MobilePhone is no longer in the DNCR list, the Mobile_DNCR indicator will be set to FALSE.
*/
public class DNCRProcessDelBatch implements Database.Batchable<sObject>, Database.Stateful
{
   public Integer affectedRecordCount = 0;
   
   public Database.QueryLocator start(Database.BatchableContext BC){
      //Set offset hours which is to execlude latest updates by DNCRSetNewFlagBatch
      Datetime cutoffTime = Datetime.now();
      cutoffTime = cutoffTime.addHours(0-DNCRHelper.getOffSetHoursFromCustomSetting());
      
      //Using LastModifiedDate to execlude latest updates by DNCRSetNewFlagBatch
      String query = 'SELECT ID,Mobile_Phone__c,Mobile_DNCR__c FROM ACCOUNT'
       						  +' WHERE Mobile_DNCR__c = true and Mobile_Phone__c != NULL'
          					  +' and RecordType.DeveloperName=\'HouseHold\' and LastModifiedDate < :cutoffTime';
       
      return Database.getQueryLocator(query);
   }
   
   public void execute( Database.BatchableContext BC, List<sObject> records){
       
       //generate Phone Number String List
       List<String> phoneList = new List<String>();
       Set<String> dncrNumberSet = new Set<String>();
       List<Account> affectdAccounts = new List<Account>();
       
       for (sObject record:records)
       {
           Account account = (Account)record;
           phoneList.add(account.Mobile_Phone__c);
       }
       
       List<DNCR__c> dncrList = [SELECT ID,Name FROM DNCR__c WHERE Name in :phoneList];
       System.debug('=============> Number of DNCR Phone: ' + dncrList.size());
       
       for (DNCR__c dncr:dncrList)
       {
           dncrNumberSet.add(dncr.Name);
       }
       
       for (sObject record:records)
       {
           Account account = (Account)record; 
           if(!dncrNumberSet.contains(account.Mobile_Phone__c))
           {
               account.Mobile_DNCR__c = false;
               affectdAccounts.add(account);
           }
       }
       System.debug('=============> Number of affected accounts: ' + affectdAccounts.size());
       update affectdAccounts;
       affectedRecordCount += affectdAccounts.size();
   }

   public void finish(Database.BatchableContext BC){
       
       System.debug('=============> Todal Number of Accounts Updated: ' + affectedRecordCount);
                    
       AsyncApexJob jobResult = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                 FROM AsyncApexJob WHERE Id =:BC.getJobId()];
       
	   //Update Job Execution Result and start the DNCRClearDataBatch
       DNCRHelper.updateDelDNCRBatchStatus(BC.getJobId(),jobResult,affectedRecordCount);
   }
}