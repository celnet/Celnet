/*
Author: Ian Huang
Created On: 2014-07-17
Function: Merge Integration Log activities
Apply To: CN 
*/

public class CN_CTIIntegrationLogEventHandler implements Triggers.Handler {

    public void Handle() {
        if (!Context.ApplyTo(new String[] {'CN'})) return;
        if (Trigger.new.size() > 1) return; 

        if (trigger.isBefore && (trigger.isInsert || trigger.isUpdate)) 
        {
            if(trigger.new[0] instanceof Task)
            {
	            try
	            {
	                this.mergeInteractionLog((Task)trigger.new[0]);
	            } catch(Exception e)
	            {
	            	Error_Log__c el = new Error_Log__c();
	            	el.Line_Number__c = e.getLineNumber();
	            	el.Message__c = e.getMessage();
	            	el.Stack_Trace_String__c = e.getStackTraceString();
	            	el.Type_Name__c = e.getTypeName();
	            	el.Error_Time__c = DateTime.now();
	            	el.Handler_Name__c = 'CN_CTIIntegrationLogEventHandler';
	            	el.User__c = UserInfo.getUserId();  
	            	insert el;
	            }
            }
        }
        if(trigger.isInsert && trigger.isAfter)
        {
        	if(trigger.new[0] instanceof Task)
        	{
        		if(trigger.new[0] instanceof Task)
	            {
		            try
		            {
		                this.insertMiddleTableForCallResult();
		            } catch(Exception e)
		            {
		            	Error_Log__c el = new Error_Log__c();
		            	el.Line_Number__c = e.getLineNumber();
		            	el.Message__c = e.getMessage();
		            	el.Stack_Trace_String__c = e.getStackTraceString();
		            	el.Type_Name__c = e.getTypeName();
		            	el.Error_Time__c = DateTime.now();
		            	el.Handler_Name__c = 'CN_CTIIntegrationLogEventHandler(MiddleTable)';
		            	el.User__c = UserInfo.getUserId();  
		            	insert el;
		            }
	            }
        	}
        }
    }
    
    private void insertMiddleTableForCallResult()
    {
    	List<Task> list_T = trigger.new;
    	List<Itegration_Log__c> list_Log = new List<Itegration_Log__c>();
    	for(Task T : list_T)
    	{
    		if(t.Need_Sync_Call_Result__c)
    		{
		    	Itegration_Log__c il = new Itegration_Log__c();
				il.Task_ID__c = t.Id;
		        il.Status__c = 'Not Start'; 
		        il.Task_Type__c = 'Manual'; 
	    		list_Log.add(il);
    		}
    	}
    	if(!list_Log.isEmpty())
    	{
	        upsert list_Log Task_ID__c;
    	}
    }
 
    private void mergeInteractionLog(Task task) {
    	if(trigger.isUpdate && (task.CallObject != null||task.Status=='Completed') && task.WhatId != null)
    	{//if related questionnair , donnot change
    		Task t = (Task)trigger.oldMap.get(task.Id);
    		if(t.WhatId != null && String.valueOf(t.WhatId).startsWith('a0W') && String.valueOf(task.WhatId).startsWith('001'))
    		{
    			task.WhatId = t.WhatId;      
    		}                       
    	} 
    	//ԤԼ����
        if(task.Is_Appointment__c) return;
        if (task.Type != 'Call' || (task.WhatId == null && task.WhoId == null)) return;
        //ֻ���ֹ�����InteractionLog�Ž�����߼�
        if(task.CallObject != null||task.Status=='Completed') return;
        //ͨ�����ٴβ����µ绰
        if(trigger.isUpdate && !task.Delete_Flag__c) return;
        Task ctiTask = retrieveCTIIntegrationLog(task);
        //Auto Set Questionnaire
        List<Contact> current_Contact = [select Id,Questionnaire__c from Contact where Id =:task.WhoId and Questionnaire__c != null and RecordType.DeveloperName='CN_Child'];
        if(current_Contact != null && current_Contact.size()>0)	task.WhatId = current_Contact[0].Questionnaire__c;
        /*Author:leo.bi@celnet.com.cn
		 *Function;If found no ctiTask , save Task to middle table
		 *Date:2014-08-28
		 */
        if (ctiTask == null)
        {
	            task.Delete_Flag__c = true;
        	if(task.WhoId != null && task.Result__c != null)
        	{
        		
	            task.Need_Sync_Call_Result__c = true;
        	}
            return;
        }
            
        
        try {
            task.Delete_Flag__c = true;
            //ctiTask.Subject            = ctiTask.CallType + ' ' + ctiTask.Subject;
            ctiTask.Inactive_Reason__c = task.Inactive_Reason__c;
            ctiTask.Result__c          = task.Result__c;
            ctiTask.Customer_Type__c   = task.Customer_Type__c; 
            ctiTask.Caller_Name__c     = task.Caller_Name__c;
            ctiTask.WhoId              = task.WhoId;
            ctiTask.WhatId             = task.WhatId; 
            ctiTask.Remark__c          = task.Remark__c;
            update ctiTask;
        } catch (Exception ex) {
        }
   }
   
   
   private Task retrieveCTIIntegrationLog(Task task) {
        DateTime lastModifiedDate = System.Now().addMinutes(-5);
        /*��ֿ�ͨ��WhatId��һ����¼ �� whoId�ҵ�һ����¼��ȡ�޸�ʱ�������*/
 		
 		//whatid
 		List<Task> whatId_ctiTasks = [SELECT Id,Subject,CallType,LastModifiedDate
		                               FROM     Task
		                               WHERE    CreatedById = :UserInfo.getUserId()
		                               AND      Type = 'Call'
		                               AND      WhatId = :task.WhatId
		                               AND      LastModifiedDate >= :lastModifiedDate
		                               AND      CallObject != null
		                               ORDER BY LastModifiedDate DESC LIMIT 1];
		//whoid
		 List<Task> whoId_ctiTasks = [SELECT  Id,Subject,CallType,LastModifiedDate
		                               FROM     Task
		                               WHERE    CreatedById = :UserInfo.getUserId()
		                               AND      Type = 'Call'
		                               AND      WhoId = :task.WhoId
		                               AND      LastModifiedDate >= :lastModifiedDate
		                               AND      CallObject != null
		                               ORDER BY LastModifiedDate DESC LIMIT 1]; 
		                               
		                               
		if(whatId_ctiTasks.isEmpty() && whoId_ctiTasks.isEmpty()){return null; }
		else if(!whatId_ctiTasks.isEmpty() && whoId_ctiTasks.isEmpty()){return whatId_ctiTasks[0];}
		else if(!whoId_ctiTasks.isEmpty() && whatId_ctiTasks.isEmpty()){return whoId_ctiTasks[0];}
		else 
		{
			if(whoId_ctiTasks[0].LastModifiedDate > whatId_ctiTasks[0].LastModifiedDate){return whoId_ctiTasks[0];}
			else{return whatId_ctiTasks[0];}
		}
		
		/*
        List<Task> ctiTasks = [SELECT   Id,
                                        Subject,
                                        CallType
                               FROM     Task
                               WHERE    CreatedById = :UserInfo.getUserId()
                               AND      Type = 'Call'
                               AND      (WhatId = :task.WhatId OR WhoId = :task.WhoId)
                               AND      LastModifiedDate >= :lastModifiedDate
                               AND      CallObject != null
                               ORDER BY LastModifiedDate DESC LIMIT 1]; 
                              
       if (!ctiTasks.isEmpty())
           return ctiTasks[0];
       return null;*/    
   }
}