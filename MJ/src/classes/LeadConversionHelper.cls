public with sharing class LeadConversionHelper {
    
    public static final String LEAD_STATUS_NEW_HOUSEHOLD = 'New Household with New Parent & Baby';
    public static final String LEAD_STATUS_NEW_PARENT = 'Merge with New Parent';
    public static final String LEAD_STATUS_NEW_CHILD = 'Merge with New Child';
    public static final String LEAD_STATUS_NEW_PARENT_CHILD = 'Merge with New Parent & Child';
    public static final String LEAD_STATUS_OLD_HOUSEHOLD = 'Merge without New Parent & Child';
    
    public static boolean isLastFirstName(Lead lead){
        if (lead.Name == lead.FirstName + ' ' + lead.LastName){
            return false;
        }else{
            return true;
        }
    }
    public static String concatName(Boolean isFirstLast, String firstName, String lastName, String chineseName){
        firstName = (firstName==null) ? '' : firstName;
        lastName = (lastName==null) ? '' : lastName;
        
        String out;
        if (isFirstLast){
            out = lastName + ' ' + firstName;
        }else{
            out = firstName + ' ' + lastName;
        }
        
        if (chineseName != null){
            out += ' / ' + chineseName;
        }
        return out;
    }
    
    /* Set Company field of Lead if empty, else conversion will fail */
    public static void setLeadCompany(Lead lead){
        if (lead.Company == null){
            lead.Company = lead.LastName + ' ' + ((lead.FirstName==null)?'':lead.FirstName);
            update lead;
        }
    }
    
    /* Lead Convert Method */
    public static LeadConvertResult convertStaging(LeadStaging staging){
            
        Boolean isNewHousehold = (staging.householdMergeId == null);
        String convertedStatus;
        Account convertedAccount;
        Contact convertedContact;
        List<Contact> convertedBabies; 
        
        LeadConvertResult result = new LeadConvertResult();
        Savepoint sp;
        sp = Database.setSavepoint();   
        try{
            setLeadCompany(staging.leadToConvert);
            
            if (isNewHousehold){
                //insert new household
                insert staging.leadHousehold;
                //trigger auto create pri contact
                convertedAccount = staging.leadHousehold;
                String convertedAccountId = convertedAccount.id;
                convertedContact = [Select ID,Name,Email,Chinese_Name__c,Member_ID__c,Birthdate,Is_Primary_Contact__c
                                 from contact where contact.account.id = :convertedAccountId];
                //set householdmergeid = newhouseholdid so new babies can be assigned to the new hh id
                staging.householdMergeId = staging.leadHousehold.Id; 
                convertedBabies = staging.createOrMergeBabies(null);
                insert convertedBabies;
                convertedStatus = LEAD_STATUS_NEW_HOUSEHOLD;
                
                
            }else{ //use existing household/contacts
                convertedAccount = [Select ID,Name,Account_Email__c,Home_Phone__c,Mobile_Phone__c, Address_Label__c,            
                Address_Block__c,Address_Building__c,Address_District__c,
                Address_Flat__c,Address_Floor__c,Address_in_Chinese__c,Address_Street__c,Address_Territory__c,
                Preferred_Contact_Method__c,Preferred_Contact_Time__c,
                Preferred_Spoken_Language__c,Preferred_Written_Languge__c
                ,Other_Phone_1__c,Other_Phone_2__c
                 from Account where id= :staging.householdMergeId]; //might need more fields 
                //apply merge rule
                LeadConversionHelper.applyMergeRulesHousehold(convertedAccount,staging.leadHousehold);
                //update account
                update convertedAccount;
                
                List<String> contactIds = new List<String>();
                contactIds.add(staging.parentMergeId);
                contactIds.addAll(staging.getBabyMergeIds());
                Map<ID,Contact> existingContacts = new Map<Id,Contact>(
                            [SELECT id, Email, Birthdate, Name, Is_Primary_Contact__c, 
                             LastName, FirstName, Other_Name__c,Chinese_Name__c, Member_ID__c,
                             Gender__c, Age__c, Born_Hospital__c, contact.account.id, Primary_RecruitChannel_Type__c
                            FROM contact WHERE Id IN :contactIds] );
                            
                Contact parent = staging.createOrMergeParent(existingContacts);
                convertedBabies = staging.createOrMergeBabies(existingContacts);
                //DML upsert parent+ babies, to get ID for DB.leadConvert use
                List<Contact> bulkUpsert = new List<Contact>();
                bulkUpsert.add(parent);
                bulkUpsert.addAll(convertedBabies);
                upsert bulkUpsert;
                convertedContact = parent;
                
                Boolean isNewParent = (staging.parentMergeId == null);
                Boolean isNewBabies = staging.hasNewBabies;
                if (isNewParent && isNewBabies){
                    convertedStatus = LEAD_STATUS_NEW_PARENT_CHILD;
                }else if (isNewParent && !isNewBabies){
                    convertedStatus = LEAD_STATUS_NEW_PARENT;
                }else if (!isNewParent && isNewBabies){
                    convertedStatus = LEAD_STATUS_NEW_CHILD;
                }else if (!isNewParent && !isNewBabies){
                    convertedStatus = LEAD_STATUS_OLD_HOUSEHOLD;
                }
            }
            //Referral 
            insertReferral(staging.referralMemberId,convertedAccount.Id, isNewHousehold);
            
            Database.LeadConvert lc = new Database.LeadConvert();
            lc.setLeadId(staging.leadToConvert.id);
            lc.setConvertedStatus(convertedStatus);
            lc.setAccountId(convertedAccount.Id);
            lc.setContactId(convertedContact.Id);
            
            lc.setDoNotCreateOpportunity(true);
            Database.LeadConvertResult lcr = Database.convertLead(lc);
            result.isSuccess = lcr.isSuccess();
            result.databaseErrors = lcr.getErrors();
            
            //Link Cases
            linkCases(staging,convertedAccount.Id, convertedContact.Id);
            
            //create/update BC RC accordingly.
            staging.updateBabyIDs();
            insert staging.getBabyConsumptions();
            insert staging.getRecruitmentChannels();
            
            //Query babies and parent for displaying result (to get formula fields like age and Name, memberId)
            List<String> contactIds = new List<String>();
            for (Contact baby: convertedBabies){
                contactIds.add(baby.id);
            }
            contactIds.add(convertedContact.Id);
            Map<Id,Contact> updatedContacts = new Map<Id,Contact>([
                    SELECT ID, Name, Email, Chinese_Name__c, Birthdate,
                     Member_ID__c, Is_Primary_Contact__c, Gender__c, Age__c
                    FROM Contact WHERE ID IN :contactIds 
                    ]);
            //populate result
            result.addHousehold(convertedAccount,isNewHousehold);
            result.addParent(updatedContacts.get(convertedContact.Id),(staging.parentMergeId == null));
            for (LeadStaging.BabyWrapper wrapper : staging.leadBabyWrapper){
                String babyID = (wrapper.babyMergeId == null) ? wrapper.baby.Id : wrapper.babyMergeId;
                result.addBaby(updatedContacts.get(babyID), wrapper.babyMergeId==null);
            }
        }catch (Exception e){
            result.isSuccess = false;
            result.exceptionString = e.getMessage();
            Database.rollback(sp);
            //need to clone all DML insert variables due to ID is not cleared.
        }
        
        return result;
    }//end convert method
    
    /* Member Referral */
    private static void insertReferral(String referralMemberId, String refereeId, boolean isSuccess){
        if (referralMemberId==null){
            system.debug('referralMemberId null');
            return;
        }
        //lookup referralId => AccontId
        Account[] refAccount = [SELECT id from Account where Member_ID__c = :referralMemberId LIMIT 1];
        if (refAccount.size()==0) {
            system.debug('referralMemberId not found:'+referralMemberId);
            return;
        }
            
        String referrerId = refAccount[0].id;
        Member_Referral__c referral = new Member_Referral__c();
        
        referral.Status__c = (isSuccess) ? 'Success':'Failure';
        referral.Referee__c = refereeId;
        referral.Referral__c = referrerId;
        insert referral;
    }
    
    /* Link Cases*/
    private static void linkCases(LeadStaging staging, ID convertedAccID, ID primaryContactID){
        ID leadID = staging.leadToConvert.Id;
        system.debug('Lead ID'+leadID);
        system.debug('Converted Account ID'+convertedAccID);
        List<Case> cases = new List<Case>();
        for (Case c : [SELECT AccountId FROM Case WHERE Non_Member__c = :leadId]){
            c.AccountId = convertedAccId;
            c.ContactId = primaryContactID;
            cases.add(c); 
        }
        if (!cases.isEmpty())
            update cases;       
    }
    
    /* Merge Rules - Household*/
    public static void applyMergeRulesHousehold(Account household, Account stagingHousehold){
        //Home Phone
        if (stagingHousehold.Home_Phone__c !=null && stagingHousehold.Home_Phone__c!=household.Home_Phone__c){
            if (household.Home_Phone__c == null){
                household.Home_Phone__c = stagingHousehold.Home_Phone__c; //overwrite when dest is blank
            }else{ //dest not blank
                //try move dest to Other Phone 1, and 2 if they are blank
                if (household.Other_Phone_1__c ==null){
                    household.Other_Phone_1__c = household.Home_Phone__c;
                    household.Other_Phone_1_Remark__c = 'Number added from Home Phone';
                    household.Home_Phone__c = stagingHousehold.Home_Phone__c;
                }else if(household.Other_Phone_2__c ==null){
                    household.Other_Phone_2__c = household.Home_Phone__c;
                    household.Other_Phone_2_Remark__c = 'Number added from Home Phone';
                    household.Home_Phone__c = stagingHousehold.Home_Phone__c;
                }else{ //move dest to Remark
                    household.Other_Phone_1_Remark__c += '-Previous Number:'+household.Home_Phone__c;
                    household.Home_Phone__c = stagingHousehold.Home_Phone__c;
                }
            }   
        }
        //Mobile Phone
        if (stagingHousehold.Mobile_Phone__c !=null && stagingHousehold.Mobile_Phone__c!=household.Mobile_Phone__c){
            if (household.Mobile_Phone__c == null){
                household.Mobile_Phone__c = stagingHousehold.Mobile_Phone__c; //overwrite when dest is blank
            }else{ //dest not blank
                //try move dest to Other Phone 1, and 2 if they are blank
                if (household.Other_Phone_1__c ==null){
                    household.Other_Phone_1__c = household.Mobile_Phone__c;
                    household.Other_Phone_1_Remark__c = 'Number added from Mobile Phone';
                    household.Mobile_Phone__c = stagingHousehold.Mobile_Phone__c;
                }else if(household.Other_Phone_2__c ==null){
                    household.Other_Phone_2__c = household.Mobile_Phone__c;
                    household.Other_Phone_2_Remark__c = 'Number added from Mobile Phone';
                    household.Mobile_Phone__c = stagingHousehold.Mobile_Phone__c;
                }else{ //move dest to Remark
                    household.Other_Phone_1_Remark__c += '-Previous Number:'+household.Mobile_Phone__c;
                    household.Mobile_Phone__c = stagingHousehold.Mobile_Phone__c;
                }
            }   
        }
        //Other Phone 1
        if (stagingHousehold.Other_Phone_1__c !=null && stagingHousehold.Other_Phone_1__c!=household.Other_Phone_1__c){
            if (household.Other_Phone_1__c == null){
                household.Other_Phone_1__c = stagingHousehold.Other_Phone_1__c; //overwrite when dest is blank
            }else{ //dest not blank
                //try move dest to Other Phone2 if  blank
                if(household.Other_Phone_2__c ==null){
                    household.Other_Phone_2__c = stagingHousehold.Other_Phone_1__c;
                    household.Other_Phone_2_Remark__c = 'Number added from Other Phone1';
                    
                }else{ //move dest to Remark
                    household.Other_Phone_1_Remark__c += '-Previous Number:'+household.Other_Phone_1__c;
                    household.Other_Phone_1__c = stagingHousehold.Other_Phone_1__c;
                }
            }   
        }
        //Other Phone 2
        if (stagingHousehold.Other_Phone_2__c !=null && stagingHousehold.Other_Phone_2__c!=household.Other_Phone_2__c){
            if (household.Other_Phone_2__c == null){
                household.Other_Phone_2__c = stagingHousehold.Other_Phone_2__c; //overwrite when dest is blank
            }else{ //dest not blank
                //try move dest to Other Phone 1 if  blank
                if(household.Other_Phone_1__c ==null){
                    household.Other_Phone_1__c = stagingHousehold.Other_Phone_2__c;
                    household.Other_Phone_1_Remark__c = 'Number added from Other Phone2';
                }else{ //move dest to Remark
                    household.Other_Phone_2_Remark__c += '-Previous Number:'+household.Other_Phone_2__c;
                    household.Other_Phone_2__c = stagingHousehold.Other_Phone_2__c;
                }
            }   
        }
        //address..other fields..
        SObject sourceObj = stagingHousehold;
        SObject destObj = household;
        String[] fieldNames = new String[]{'Address_Block__c','Address_Building__c','Address_District__c',
            'Address_Flat__c','Address_Floor__c','Address_Street__c','Address_Territory__c',
            'Preferred_Contact_Method__c','Preferred_Contact_Time__c',
            'Preferred_Spoken_Language__c','Preferred_Written_Languge__c'
        };
        for (String field : fieldNames){
            String sourceValue = (String)sourceObj.get(field);
            String destValue = (String)destObj.get(field);
            if (sourceValue != null && sourceValue != destValue){
                destObj.put(field,sourceValue);
            }
        }
        //boolean fields?
        
    }//end apply merge rule household
    /* Merge Rules - Parent */
    public static void applyMergeRulesParent(Contact parent, Contact stagingParent){
        SObject sourceObj = stagingParent;
        SObject destObj = parent;
        String[] fieldNames = new String[]{'FirstName','LastName','Other_Name__c','Chinese_Name__c','Email'};
        for (String field : fieldNames){
            String sourceValue = (String)sourceObj.get(field);
            String destValue = (String)destObj.get(field);
            if (sourceValue != null && sourceValue != destValue){
                destObj.put(field,sourceValue);
            }
        }
    }
    /* Merge Rules - Baby */
    public static void applyMergeRulesBaby(Contact baby, Contact stagingBaby){
        //if source is not blank, overwrite dest
        SObject sourceObj = stagingBaby;
        SObject destObj = baby;
        String[] fieldNames = new String[]{'FirstName','LastName','Other_Name__c',
                'Chinese_Name__c','Born_Hospital__c','Gender__c'};
        for (String field : fieldNames){
            String sourceValue = (String)sourceObj.get(field);
            String destValue = (String)destObj.get(field);
            if (sourceValue != null && sourceValue != destValue){
                destObj.put(field,sourceValue);
            }
        }
        //Bug fix 29-Jan: to always update birthday from lead if lead's birthday is not blank
        //if (baby.Birthdate == null){ baby.Birthdate = stagingBaby.Birthdate;}
        if (stagingBaby.Birthdate != null) {baby.Birthdate = stagingBaby.Birthdate;}
    }
}//end class