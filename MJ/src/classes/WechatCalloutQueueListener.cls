/*
 *Author: leo@celnet.com.cn
 *Created On: 2014-09-05
 *Function: Launch WechatCalloutMessageProcessorBatch job to handle tasks in queue
 *Apply To: HK
 */
public class WechatCalloutQueueListener implements Triggers.Handler
{
	public void handle()
	{
		if(!Context.ApplyTo(new String[] {'HK'})) 
    	{
    		return;
		}
		if(trigger.isInsert && trigger.isAfter)
		{
			if(trigger.new[0] instanceof Wechat_Callout_Task_Queue__c)
			{
				this.executeWechatCalloutProcessorBatch();
			}
		}
	}
	
	public void executeWechatCalloutProcessorBatch()
	{
		WechatCalloutProcessorBatch.StartWechatCalloutProcessorBatch();
	}
}