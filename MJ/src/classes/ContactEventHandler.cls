/*
Author: scott.zhang@celnet.com.cn 
Created On: 2014-05-19
Function: 
	Update Contact from IntegrationLog
	1.con.Inactive__c
	2.con.Inactive_Reason__c
	3.con.DoNotCall
	4.con.Cannot_Be_Contacted__c
	5.last Routine call time,last Routine call result
	Update Contact from Q-Task
	1.Clear Questionnaire__c when Q-Task Closed
Support Event:AfterInsert, AfterUpdate
Apply To: CN 
*/
public class ContactEventHandler implements Triggers.Handler {
	
	public void Handle()
	{
		if(!Context.ApplyTo(new String[] {'CN'})) 
    	{
    		return;
		}
		if(trigger.isAfter && (trigger.isInsert || trigger.isUpdate))
		{
			if(trigger.new[0] instanceof Task)
			{
				try
	            {
	                this.updateFromIntegrationLog();
	            } catch(Exception e)
	            {
	            	Error_Log__c el = new Error_Log__c();
	            	el.Line_Number__c = e.getLineNumber();
	            	el.Message__c = e.getMessage();
	            	el.Stack_Trace_String__c = e.getStackTraceString();
	            	el.Type_Name__c = e.getTypeName();
	            	el.Error_Time__c = DateTime.now();
	            	el.Handler_Name__c = 'ContactEventHandler';
	            	el.User__c = UserInfo.getUserId();
	            	insert el;
	            }
			}
		}
		if(trigger.isAfter && trigger.isDelete)
		{
			if(trigger.old[0] instanceof Q_Task__c)
			{
				this.clearQuestionnaireAfterDeleteQTask();
			}
		}
	}
	
	//Update Contact
	//1.con.Inactive__c
	//2.con.Inactive_Reason__c
	//3.con.DoNotCall
	//4.con.Cannot_Be_Contacted__c
	//5.last Routine call time,last Routine call result
	private void updateFromIntegrationLog()
	{
		//Task废卡对应的选项
		final Set<String> Set_InactineOption = new Set<String>{'Inactive'};
		//Task据访对应选项
		final Set<String> Set_DoNotCallOption = new Set<String>{'Do not Call'};
		//Task无法联系对应选项
		final Set<String> Set_NoAnswer2Option = new Set<String>{'No Answer2'};
		List<Task> taskList = trigger.new;
		Set<Id> Set_ContainsIds = new Set<Id>();
		for(Task ta : taskList)
		{
			//过滤CIC创建记录
			if(ta.CallObject != null||ta.Status=='Completed')
			continue;
			
			Boolean IsInsert =(trigger.isInsert?true:false);
			Boolean IsUpdate =(trigger.isUpdate?true:false);
			Task oldTa = (IsUpdate?(Task)trigger.oldMap.get(ta.Id):new Task());
			if(IsInsert && ta.WhoId != null)
			{
				Set_ContainsIds.add(ta.WhoId);
			}
			
			if(IsUpdate && (ta.Result__c != oldTa.Result__c || ta.Inactive_Reason__c != oldTa.Inactive_Reason__c))
			{
				Set_ContainsIds.add(ta.WhoId);
			}
		}
		if(Set_ContainsIds.size()==0)
		return;
		Set<Id> Set_ClosedQtaskContactId = new Set<Id>();
		List<Contact> List_UpCon = new List<Contact>();
		for(Contact con : [select Id,Cannot_be_contacted__c,DoNotCall,Inactive__c,Inactive_Reason__c, 
						   Last_Routine_Call_Time__c,Last_Routine_Call_Result__c,Questionnaire__r.Type__c 
						   from Contact where id in:Set_ContainsIds and RecordType.DeveloperName='CN_Child'])
		{
			Integer flag= 0;
			for(Task ta : taskList)
			{
				//if(ta.CallObject == null)
				//continue;
				if(ta.WhoId == con.Id )
				{
					//Inactive
					if(Set_InactineOption.contains(ta.Result__c))
					{
						con.Inactive__c = true;
						con.Inactive_Reason__c = (ta.Inactive_Reason__c!=null?ta.Inactive_Reason__c:'未填');
						flag++;
					}
					//Do Not Call
					else if(Set_DoNotCallOption.contains(ta.Result__c))
					{
						con.DoNotCall = true;
						flag++;
					}
					//Cannot Be Contacted
					else if(Set_NoAnswer2Option.contains(ta.Result__c))
					{
						con.Cannot_Be_Contacted__c = true;
						flag++;
					}
					//Routine Questionnaire
					if(con.Questionnaire__r.Type__c == 'Routine')
					{
						con.Last_Routine_Call_Time__c = datetime.now();
						con.Last_Routine_Call_Result__c = ta.Result__c;
						flag++;
					}
					
				}
			}
			if(flag>0)
			List_UpCon.add(con);
		}
		
		if(List_UpCon.size()>0)
		update List_UpCon;
	}
	
	
	//Update Contact from Q-Task
	//1.Clear Questionnaire__c when Closed Q-Task deleted
	private void clearQuestionnaireAfterDeleteQTask()
	{
		List<Q_Task__c> QTaskList = trigger.old;
		Set<Id> Set_ConIds = new Set<Id>();
		for(Q_Task__c qt : QTaskList)
		{
			if(qt.Status__c=='Closed' && qt.Questionnaire__c != null && qt.Contact__c != null)
			{
				Set_ConIds.add(qt.Contact__c);		
			}
		}
		if(Set_ConIds.size()==0)
		return;
		List<Contact> List_UpContact = new List<Contact>();
		for(Contact con : [Select Id,Questionnaire__c from Contact where Id in:Set_ConIds and Questionnaire__c != null and RecordType.DeveloperName='CN_Child'])
		{
			for(Q_Task__c qt : QTaskList)
			{
				if(qt.Contact__c == con.Id && qt.Questionnaire__c == con.Questionnaire__c)
				{
					con.Questionnaire__c = null;
				}
			}
			if(con.Questionnaire__c == null)
			{
				List_UpContact.add(con);
			}
		}
		if(List_UpContact.size()>0)
		update List_UpContact;
	}
}