/*Author:leo.bi@celnet.com.cn
 *Date:2014-07-26
 *Function；test GetIPAddressController
 */
@isTest
global class GetIPAddressControllerTest implements HttpCalloutMock
{

    static testMethod void myUnitTest() 
    {
    	Test.setMock(HttpCalloutMock.class, new GetIPAddressControllerTest());
    	GetIPAddressController getIp = new GetIPAddressController();
    	getIp.run();
    }
    String body = '当前 IP：<code>' + '192.168.1.1</code>&nbsp;来自' 
    			+ '</code>&nbsp;来自：本机' + '</p><p>GeoIP:';
	global HTTPResponse respond(HTTPRequest req) 
	{
        HttpResponse res = new HttpResponse();
        res.setBody(body);
        return res;
    }
}