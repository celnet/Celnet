/*
Author: scott.zhang@celnet.com.cn
Created On: 2014-04-16
Function: MemberEntryEventHandler test class
Apply To: CN
*/
@isTest
private class MemberEntryEventHandlerTest {

     static testMethod void myUnitTest() { 
        system.runas(TestUtility.new_CN_User())
        {
	        /*=MemberEntry<-update by Account/Contact/Recruitment Channel__c=*/
	        //Create Member_Entry__c
	        Member_Entry__c MemberEntry = new Member_Entry__c();
	        MemberEntry.Name = 'Member Name';
	        insert MemberEntry;
	        //Create CN_HouseHold
	        Account Acc = new Account();
	        Acc.Name = 'HouseHoldTest';
	        Acc.RecordTypeId = ObjectUtil.GetRecordTypeID(Account.getSObjectType(), 'CN_HouseHold');
	        Acc.Member_Entry__c = MemberEntry.Id;
	        insert Acc;
	        
	        //Create CN_Child
	        Contact Con = new Contact();
	        Con.RecordTypeId = ObjectUtil.GetRecordTypeID(Contact.getSObjectType(), 'CN_Child');
	        Con.FirstName = 'leo';
	        Con.LastName = 'zhang';
	        Con.AccountId = Acc.Id;
	        insert Con;
	        
	        //Create Recruitment_Channel__c
	        Recruitment_Channel__c Rc = new Recruitment_Channel__c();
	        Rc.RecordTypeId = ObjectUtil.GetRecordTypeID(Recruitment_Channel__c.getSObjectType(), 'Recruitment_Channel_CN');
	        Rc.Contact__c = Con.Id;
	        Rc.Primary__c = true;
	        insert Rc;
	        
	        /*=Member Entry syncValue=*/
	        //Status Mapping
	        BF_Status_Mapping__c sta = new BF_Status_Mapping__c();
	        sta.Name = 'Father1';
	        sta.BF_Label__c = '宝宝爸爸1';
	        sta.BF_Value__c = '551';
	        sta.SFDC_Label__c = 'Father1';
	        insert sta;
	        //Register Source Mapping
	        BF_Register_Source_Mapping__c Reg = new BF_Register_Source_Mapping__c();
	        Reg.Name ='021 Call center1';
	        Reg.BF_Register_Source_Value__c = '221';
	        Reg.BF_Register_Sub_Source_Value__c = '551';
	        Reg.Register_Source_Label__c = '互联网1';
	        Reg.Register_Sub_Source_Label__c = '021 Call center1';
	        insert Reg;
	        //Brand Mapping
	        BF_Brand_Mapping__c bra = new BF_Brand_Mapping__c();
	        bra.Name='Baby-Karicare(可瑞康)1';
	        bra.BF_Label__c = '其他1';
	        bra.BF_Value__c = '991';
	        bra.SFDC_Label__c = 'Karicare(可瑞康)1';
	        bra.For__c = 'Baby';
	        insert bra;
	        BF_Brand_Mapping__c bra2 = new BF_Brand_Mapping__c();
	        bra2.Name='Mother-其他(请注明)1';
	        bra2.BF_Label__c = '其他1';
	        bra2.BF_Value__c = '171';
	        bra2.SFDC_Label__c = '其他(请注明)1';
	        bra2.For__c = 'Mother';
	        insert bra2;
	        //Area
	        Address_Management__c am = new Address_Management__c();
	        am.Name = '勃利县1';
	        am.Sales_City__c = '七台河市1';
	        am.Province__c = '黑龙江省1';
	        am.BF_Area_Value__c = '101011';
	        am.BF_City_Value__c='12411';
	        am.BF_Province_Value__c = '1111';
	        insert am;
	        //Specific Code 
	        //hospital
	        Account HosAcc = new Account();
	        HosAcc.Name = 'CNHospitalTest';
	        HosAcc.RecordTypeId = ObjectUtil.GetRecordTypeID(Account.getSObjectType(), 'CN_Hospital');
	        HosAcc.Specific_Code__c = 'H0001234';
	        insert HosAcc;
	        //Specific Code
	        //Store
	        Account StoreAcc = new Account();
	        StoreAcc.Name = 'CNStoreTest';
	        StoreAcc.RecordTypeId = ObjectUtil.GetRecordTypeID(Account.getSObjectType(), 'CN_Store');
	        StoreAcc.Specific_Code__c = '0001234';
	        insert StoreAcc;
	        test.startTest();
            Acc.Name='UpdateAcc';
            update Acc;
            Member_Entry__c Me = [select Name from Member_Entry__c where id=:MemberEntry.Id];
            System.assertEquals(Acc.Name,Me.Name);
            
            Con.Is_Winning__c ='Y';
            update Con;
            Me = [select Name,Is_Win__c from Member_Entry__c where id=:MemberEntry.Id];
            System.assertEquals(Con.Is_Winning__c,Me.Is_Win__c);
            
            Rc.Specific_Code__c = '1243';
            update Rc;
            Me = [select Name,Baby_Birthday__c,Specific_Code__c from Member_Entry__c where id=:MemberEntry.Id];
            System.assertEquals(Rc.Specific_Code__c,Me.Specific_Code__c);
            
            /*==syncValue==*/
            //Status   
            Member_Entry__c StatusMemberEntry = new Member_Entry__c();
            StatusMemberEntry.Name = 'StatusMemberName';
            StatusMemberEntry.Status__c = 'Father1';
            insert StatusMemberEntry; 
            StatusMemberEntry.BF_Status_Value__c = null;
            update StatusMemberEntry;
            StatusMemberEntry.BF_Status_Value__c = '2551';
            update StatusMemberEntry;
            //Register Source
            Member_Entry__c RegisterMemberEntry = new Member_Entry__c();
            RegisterMemberEntry.Name = 'RegisterMemberEntryName';
            RegisterMemberEntry.Register_Source__c = '互联网1';
            RegisterMemberEntry.Register_Sub_Source__c = '021 Call center1';
            insert RegisterMemberEntry;
            RegisterMemberEntry.BF_Register_Source_Value__c = NULL;
            RegisterMemberEntry.BF_Register_Sub_Source_Value__c = NULL;
            update RegisterMemberEntry;
            RegisterMemberEntry.BF_Register_Source_Value__c = '221';
            RegisterMemberEntry.BF_Register_Sub_Source_Value__c ='551';
            update RegisterMemberEntry;
            //Brand Mapping
            Member_Entry__c BrandMemberEntry = new Member_Entry__c();
            BrandMemberEntry.Name='BrandMemberEntryName';
            BrandMemberEntry.Q_Using_Mother_Milk_Powder__c = '其他(请注明)1';
            BrandMemberEntry.Q_Wish_Using_Baby_Milk_Powder__c = 'Karicare(可瑞康)1';
            insert BrandMemberEntry;
            BrandMemberEntry.BF_Q_Using_Mother_Milk_Powder_Value__c = null;
            BrandMemberEntry.BF_Q_Wish_Using_Baby_Milk_Powder_Value__c = null;
            update BrandMemberEntry;
            BrandMemberEntry.BF_Q_Using_Mother_Milk_Powder_Value__c = null;
            BrandMemberEntry.BF_Q_Wish_Using_Baby_Milk_Powder_Value__c = null;
            update BrandMemberEntry;
            //Area
            Member_Entry__c AreaMemberEntry = new Member_Entry__c();
            AreaMemberEntry.Name='AreaMemberEntry';
            AreaMemberEntry.Administrative_Area__c = am.Id;
            insert AreaMemberEntry;
            AreaMemberEntry.BF_Area_Value__c = NULL;
            update AreaMemberEntry;
            AreaMemberEntry.BF_Area_Value__c = '1010113';
            update AreaMemberEntry;
             
            //Specific Code 
            Member_Entry__c CodeMemberEntry = new Member_Entry__c();
            CodeMemberEntry.Name='CodeName';
            CodeMemberEntry.Specific_Code__c = 'H0001234';
            insert CodeMemberEntry;
            CodeMemberEntry.Specific_Code__c = '0001234';
            update CodeMemberEntry;
        	test.stopTest();
        }
    }
}