/*
Author: scott.zhang@celnet.com.cn
Created On: 2014-04-16
Function: MemberEntrySplitBatch
Apply To: CN 
*/
global class MemberEntrySplitBatch implements Database.Batchable<sObject>,Schedulable {
	final String DU0 = 'DU0:BF Client ID';
	final String DU1 = 'DU1:Register Phone';
	final String DU2 = 'DU2:Name';
	final String DU3 = 'DU3:In Use Phone';
	final String IstatusSplit='Split'; 
	final Integer NumberDaysDue = 30;
	final Integer NumberDayDiffer = 270;//Create Baby
	final String BabyDefaultName='宝宝';
	final String ConvertedStatus='Converting';
	public String MemberEntryId;
	//Map key:Id  value:Member_Entry__c 
	Map<Id,Member_Entry__c> Map_MemberEntry;
	List<Member_Entry__c> DU0DuplicateY;
	List<Member_Entry__c> DU0DuplicateN;
	List<Member_Entry__c> DU1DuplicateY;
	List<Member_Entry__c> DU1DuplicateN;
	List<Member_Entry__c> DU2DuplicateY;
	List<Member_Entry__c> DU2DuplicateN;
	List<Member_Entry__c> DU3DuplicateY;
	List<Member_Entry__c> DU3DuplicateN;
	Map<Id,Id>Map_MebereIdAccId;
	Map<Id,Lead>Map_MebereIdLead;
	//RecordType
	Id CN_RecChanRecordTypeId;//Recruitment_Channel_CN
	Id CN_AccHouseHoldRecordTypeId;//CN_HouseHold
	Id CN_ContactParentRecordTypeId;//CN_Parent
	Id CN_ContactChildRecordTypeId;//CN_Child
	Id CN_LeadsRecordTypeId;//CN_Leads
	//Check Lead Related
	Boolean HasRelatedLead; 
	global MemberEntrySplitBatch()
	{
		//RecordType:Recruitment_Channel_CN
		CN_RecChanRecordTypeId = ObjectUtil.GetRecordTypeID(Recruitment_Channel__c.getSObjectType(), 'Recruitment_Channel_CN');
		CN_AccHouseHoldRecordTypeId = ObjectUtil.GetRecordTypeID(Account.getSObjectType(), 'CN_HouseHold');
		CN_ContactParentRecordTypeId = ObjectUtil.GetRecordTypeID(Contact.getSObjectType(), 'CN_Parent');
		CN_ContactChildRecordTypeId = ObjectUtil.GetRecordTypeID(Contact.getSObjectType(), 'CN_Child');
		CN_LeadsRecordTypeId = ObjectUtil.GetRecordTypeID(Lead.getSObjectType(), 'CN_Leads');
	}
	global void execute(SchedulableContext sc)  
	{
		MemberEntrySplitBatch SplitBatch = new MemberEntrySplitBatch();
		Database.executeBatch(SplitBatch ,5); 
	}
	global Database.QueryLocator start(Database.BatchableContext BC) 
	{ 
		String query = 'SELECT ';
        for(String fieldApi : Schema.SobjectType.Member_Entry__c.fields.getMap().KeySet())
        {
        	if(fieldApi=='Id')
        	continue;
        	query += fieldApi + ', ';
        }
        if(MemberEntryId != null && MemberEntryId != '')
        {
        	query += 'Id FROM Member_Entry__c Where I_Status__c = \'Extracted New\' and id=\''+MemberEntryId+'\'';
        }
        else
        {
        	query += 'Id FROM Member_Entry__c Where I_Status__c = \'Extracted New\' and Inactive__c != true';
        }
        
        System.debug(query);
		return Database.getQueryLocator(query);
	}
	global void execute(Database.BatchableContext BC, List<Member_Entry__c> scope) 
	{
		/*==Split Type1: Non Related Lead==*/
		List<Member_Entry__c> List_NonRelatedLead = new List<Member_Entry__c>();
		/*==Split Type2: Has Related Lead==*/
		List<Member_Entry__c> List_HasRelatedLead = new List<Member_Entry__c>();
		Set<Id> Set_LeadIds = new Set<Id>();
		for(Member_Entry__c MemberEntry : scope)
		{
			if(MemberEntry.Lead__c != null)
			{
				List_HasRelatedLead.add(MemberEntry);
				Set_LeadIds.add(MemberEntry.Lead__c );
			}
			else
			{
				List_NonRelatedLead.add(MemberEntry);
			}
		}
		
		if(List_NonRelatedLead.size()>0)
		{
			this.NoRelatedLeadSplit(List_NonRelatedLead);
		}
		
		if(List_HasRelatedLead.size()>0)
		{
			this.HasRelatedLeadSplit(List_HasRelatedLead,Set_LeadIds);
		}
	}
	private void NoRelatedLeadSplit(List<Member_Entry__c> List_Members)
	{
		HasRelatedLead = false;
		Map_MebereIdAccId = new Map<Id,Id>();
		DU0DuplicateY = new List<Member_Entry__c>();
		DU0DuplicateN = new List<Member_Entry__c>();
		DU1DuplicateY = new List<Member_Entry__c>();
		DU1DuplicateN = new List<Member_Entry__c>();
		DU2DuplicateY = new List<Member_Entry__c>();
		DU2DuplicateN = new List<Member_Entry__c>();
		DU3DuplicateY = new List<Member_Entry__c>();
		DU3DuplicateN = new List<Member_Entry__c>();
		//key :BF_Client_ID__c  value:MemberEntry
		Map<String,Member_Entry__c> Map_Client_ID = new Map<String,Member_Entry__c>();
		for(Member_Entry__c MemberEntry : List_Members)
		{
			if(MemberEntry.BF_Duplicated__c)
	 		{
	 			Map_Client_ID.put(MemberEntry.BF_Client_ID__c,MemberEntry);
	 		}	
	 		else
	 		{
	 			DU0DuplicateN.add(MemberEntry);
	 		}
		}
		//Du0 Client Id
		if(Map_Client_ID.size()>0)
		this.DuplicateCheckByClientId(Map_Client_ID);
		//Du1 Phone
		if(DU0DuplicateN.size()>0)
		this.DuplicateCheckByPhone(DU0DuplicateN);
		/*去除Name City 查重逻辑
		//Du2 Name City
		if(DU1DuplicateN.size()>0)
		this.DuplicateCheckByName(DU1DuplicateN);
		*/
		//Du3 in use phone
		/*去除Name City 查重逻辑
		if(DU2DuplicateN.size()>0)
		this.DuplicateCheckByUsePhone(DU2DuplicateN);
		*/
		if(DU1DuplicateN.size()>0)
		this.DuplicateCheckByUsePhone(DU1DuplicateN);
		
		
		
		
		//Lead： Check Lead By Register Phone
		//if(DU1DuplicateY.size()>0)
		//this.CheckLeadByRegisterPhone(DU1DuplicateY);
		if(DU2DuplicateY.size()>0)
		this.CheckLeadByRegisterPhone(DU2DuplicateY);
		if(DU3DuplicateY.size()>0)
		this.CheckLeadByRegisterPhone(DU3DuplicateY);
		if(DU3DuplicateN.size()>0)
		this.CheckLeadByRegisterPhone(DU3DuplicateN);
		
		update List_Members;
	}
	private void HasRelatedLeadSplit(List<Member_Entry__c> List_Members,Set<Id> Set_LeadIds)
	{
		Map_MebereIdAccId = new Map<Id,Id>();
		DU0DuplicateY = new List<Member_Entry__c>();
		DU0DuplicateN = new List<Member_Entry__c>();
		DU1DuplicateY = new List<Member_Entry__c>();
		DU1DuplicateN = new List<Member_Entry__c>();
		DU2DuplicateY = new List<Member_Entry__c>();
		DU2DuplicateN = new List<Member_Entry__c>();
		DU3DuplicateY = new List<Member_Entry__c>();
		DU3DuplicateN = new List<Member_Entry__c>();
		
		//Leads//Member Entry & Lead
		Map_MebereIdLead = new Map<Id,Lead>();
		List<Lead> List_Leads = new List<Lead>();
		List_Leads = [select Id,Phone,LastName,Email,Status,Administrative_area__c,Address_Street__c,Specific_Code__c,Other_Phone_1__c,Other_Phone_2__c,
					 (Select Id,ContactId,AccountId From Cases__r),(Select Contact__c,Finished_On__c,Questionnaire__r.Type__c From Answers__r),
					 Recruitment_Channel__c,Sub_Recruitment_Channel__c,MobilePhone,Is_VIP__c,Gender_1st_Child__c 
					 from Lead where Id in: Set_LeadIds and IsConverted=false];
		//key :BF_Client_ID__c  value:MemberEntry
		Map<String,Member_Entry__c> Map_Client_ID = new Map<String,Member_Entry__c>();
		for(Member_Entry__c MemberEntry : List_Members)
		{
			if(MemberEntry.BF_Duplicated__c)
	 		{
	 			Map_Client_ID.put(MemberEntry.BF_Client_ID__c,MemberEntry);
	 		}	
	 		else
	 		{
	 			DU0DuplicateN.add(MemberEntry);
	 		}
	 		for(Lead lea : List_Leads)
	 		{
	 			if(lea.Id == MemberEntry.Lead__c)
	 			Map_MebereIdLead.put(MemberEntry.Id,lea);
	 		}
		}
		//Du0 Client Id
		if(Map_Client_ID.size()>0)
		this.DuplicateCheckByClientId(Map_Client_ID);
		//Du1 Phone
		if(DU0DuplicateN.size()>0)
		this.DuplicateCheckByPhone(DU0DuplicateN);
		
		/*去除Name City 查重逻辑
		//Du2 Name City
		if(DU1DuplicateN.size()>0)
		this.DuplicateCheckByName(DU1DuplicateN);
		*/
		
		//Du3 in use phone
		/*去除Name City 查重逻辑
		if(DU2DuplicateN.size()>0)
		this.DuplicateCheckByUsePhone(DU2DuplicateN);
		*/
		if(DU1DuplicateN.size()>0)
		this.DuplicateCheckByUsePhone(DU1DuplicateN);
		  
		if(DU2DuplicateY.size()>0)
		this.RelatedLeadConvert(DU2DuplicateY);
		if(DU3DuplicateY.size()>0)
		this.RelatedLeadConvert(DU3DuplicateY);
		if(DU3DuplicateN.size()>0)
		this.RelatedLeadConvert(DU3DuplicateN);
		
		System.debug('#########################################Map_MebereIdLead           '+Map_MebereIdLead.size());
		System.debug('#########################################Map_Client_ID           '+Map_Client_ID.size());
		System.debug('#########################################DU0DuplicateN           '+DU0DuplicateN.size());
		System.debug('#########################################DU1DuplicateN           '+DU1DuplicateN.size());
		System.debug('#########################################DU2DuplicateN           '+DU2DuplicateN.size());
		System.debug('#########################################DU3DuplicateY           '+DU3DuplicateY.size());
		System.debug('#########################################DU3DuplicateN           '+DU3DuplicateN.size());
		
		update List_Members;
	}
	//DU0 duplicate checking by BF_Client_ID__c
	private void DuplicateCheckByClientId(Map<String,Member_Entry__c> Map_Client_ID)
	{
		List<Recruitment_Channel__c> InsertRecChan = new List<Recruitment_Channel__c>();
		Set<String> Set_ClientIDs = new Set<String>();
		
		for(Account Acc : [select Id,BF_Client_ID__c,(Select Id,Birthdate From Contacts where RecordType.DeveloperName =:'CN_Child' and Birthdate != null order by Birthdate desc) from Account Where BF_Client_ID__c in:Map_Client_ID.keySet() and RecordType.DeveloperName=:'CN_HouseHold'])
	 	{
	 		Member_Entry__c MemberEntry = Map_Client_ID.get(Acc.BF_Client_ID__c);
	 		MemberEntry.Duplicate_Status__c = DU0;
	 		MemberEntry.I_Status__c = IstatusSplit;
	 		DU0DuplicateY.add(MemberEntry);
	 		Set_ClientIDs.add(Acc.BF_Client_ID__c);
	 		//if(Acc.Contacts != null && Acc.Contacts.size()>0)
	 		//{
		 		if(MemberEntry.Baby1_Birthday__c != null)
		 		{
		 			Contact con = this.BirthdateRangeValidator(Acc.Contacts,MemberEntry.Baby1_Birthday__c);
		 			
		 			//创建新Baby
		 			if(con.Id ==null)
		 			{
		 				con = this.CreateContact(Acc.Id,MemberEntry,'Baby',MemberEntry.Baby1_Birthday__c,MemberEntry.Baby1_ID__c);
		 				insert con;
		 			}
		 			//Create Recruitment_Channel__c
 					Recruitment_Channel__c RC = this.CreateRecruitmentChannel(con.Id,MemberEntry,MemberEntry.Baby1_Birthday__c);
 					InsertRecChan.add(RC);
 					
 					//HasRelatedLead:Move Case  Move Answer
 					if(Map_MebereIdLead!= null && Map_MebereIdLead.containsKey(MemberEntry.Id))
 					{
 						Lead lea = Map_MebereIdLead.get(MemberEntry.Id);
 						this.MoveCasesToBaby(Acc.Id,con.Id,lea.Cases__r);
 						this.MoveAnswerToBaby(con.Id,lea.Answers__r);
 					}
 					//break;
		 		}
		 		if(MemberEntry.Baby2_Birthday__c != null)
		 		{
		 			Contact con = this.BirthdateRangeValidator(Acc.Contacts,MemberEntry.Baby2_Birthday__c);
		 			
		 			//创建新Baby
		 			if(con.Id ==null)
		 			{
		 				con = this.CreateContact(Acc.Id,MemberEntry,'Baby',MemberEntry.Baby2_Birthday__c,MemberEntry.Baby2_ID__c);
		 				insert con;
		 			}
		 			//Create Recruitment_Channel__c
 					Recruitment_Channel__c RC = this.CreateRecruitmentChannel(con.Id,MemberEntry,MemberEntry.Baby2_Birthday__c);
 					InsertRecChan.add(RC);
 					//HasRelatedLead:Move Case  Move Answer
 					if(Map_MebereIdLead!= null && Map_MebereIdLead.containsKey(MemberEntry.Id))
 					{
 						Lead lea = Map_MebereIdLead.get(MemberEntry.Id);
 						this.MoveCasesToBaby(Acc.Id,con.Id,lea.Cases__r);
 						this.MoveAnswerToBaby(con.Id,lea.Answers__r);
 					}
 					//break;
		 		}
		 		if(MemberEntry.Baby3_Birthday__c != null)
		 		{
		 			Contact con = this.BirthdateRangeValidator(Acc.Contacts,MemberEntry.Baby3_Birthday__c);
		 			//创建新Baby
		 			if(con.Id ==null)
		 			{
		 				con = this.CreateContact(Acc.Id,MemberEntry,'Baby',MemberEntry.Baby3_Birthday__c,MemberEntry.Baby3_ID__c);
		 				insert con;
		 			}
		 			//Create Recruitment_Channel__c
 					Recruitment_Channel__c RC = this.CreateRecruitmentChannel(con.Id,MemberEntry,MemberEntry.Baby3_Birthday__c);
 					InsertRecChan.add(RC);
 					//HasRelatedLead:Move Case  Move Answer
 					if(Map_MebereIdLead!= null && Map_MebereIdLead.containsKey(MemberEntry.Id))
 					{
 						Lead lea = Map_MebereIdLead.get(MemberEntry.Id);
 						this.MoveCasesToBaby(Acc.Id,con.Id,lea.Cases__r);
 						this.MoveAnswerToBaby(con.Id,lea.Answers__r);
 					}
 					//break;
		 		}
	 		//}	
	 	}
	 	
	 	for(String key : Map_Client_ID.KeySet())
 		{
 			if(!Set_ClientIDs.contains(key))
 			DU0DuplicateN.add(Map_Client_ID.get(key));
 		}
 		
		if(InsertRecChan.size()>0)
	 	insert InsertRecChan;
	}
	//DU1 duplicate checking by Phone
	private void DuplicateCheckByPhone(List<Member_Entry__c> List_Members)
	{	
		Set<String> Set_Phones = new Set<String>();
		for(Member_Entry__c MemberEntry : List_Members)
		{
			if(MemberEntry.Phone__c != null)
			Set_Phones.add(MemberEntry.Phone__c);
		}
		if(Set_Phones.size()==0)
		{
			DU1DuplicateN.addAll(List_Members);
			return;
		}
		List<Recruitment_Channel__c> List_RC = new List<Recruitment_Channel__c>();
		List<Account> List_Acc = [select Id,Phone,(Select Id,Birthdate From Contacts where RecordType.DeveloperName =:'CN_Child' and Birthdate != null order by Birthdate desc) from Account where Phone in:Set_Phones  and Duplicate_Status__c = null and RecordType.DeveloperName=:'CN_HouseHold'];
		if(List_Acc != null && List_Acc.size()>0)
		{
			for(Member_Entry__c MemberEntry : List_Members)
			{
				Boolean flag = true; 
				for(Account acc : List_Acc)
				{
					if(acc.Phone == MemberEntry.Phone__c)
					{
						
						if(MemberEntry.Baby1_Birthday__c != null)
						{
							Contact con = this.BirthdateRangeValidator(Acc.Contacts,MemberEntry.Baby1_Birthday__c);
		 			
				 			//创建新Baby
				 			if(con.Id ==null)
				 			{
				 				con = this.CreateContact(Acc.Id,MemberEntry,'Baby',MemberEntry.Baby1_Birthday__c,MemberEntry.Baby1_ID__c);
				 				insert con;
				 			}
				 			//Create Recruitment_Channel__c
		 					Recruitment_Channel__c RC = this.CreateRecruitmentChannel(con.Id,MemberEntry,MemberEntry.Baby1_Birthday__c);
		 					List_RC.add(RC);
						}
						if(MemberEntry.Baby2_Birthday__c != null)
						{
							Contact con = this.BirthdateRangeValidator(Acc.Contacts,MemberEntry.Baby2_Birthday__c);
		 			
				 			//创建新Baby
				 			if(con.Id ==null)
				 			{
				 				con = this.CreateContact(Acc.Id,MemberEntry,'Baby',MemberEntry.Baby2_Birthday__c,MemberEntry.Baby2_ID__c);
				 				insert con;
				 			}
				 			//Create Recruitment_Channel__c
		 					Recruitment_Channel__c RC = this.CreateRecruitmentChannel(con.Id,MemberEntry,MemberEntry.Baby2_Birthday__c);
		 					List_RC.add(RC);
						}
						if(MemberEntry.Baby3_Birthday__c != null)
						{
							Contact con = this.BirthdateRangeValidator(Acc.Contacts,MemberEntry.Baby3_Birthday__c);
		 			
				 			//创建新Baby
				 			if(con.Id ==null)
				 			{
				 				con = this.CreateContact(Acc.Id,MemberEntry,'Baby',MemberEntry.Baby3_Birthday__c,MemberEntry.Baby3_ID__c);
				 				insert con;
				 			}
				 			//Create Recruitment_Channel__c
		 					Recruitment_Channel__c RC = this.CreateRecruitmentChannel(con.Id,MemberEntry,MemberEntry.Baby3_Birthday__c);
		 					List_RC.add(RC);
						}
						MemberEntry.Duplicate_Status__c = DU1;
		 				MemberEntry.I_Status__c = IstatusSplit;
						//DU1DuplicateY.add(MemberEntry);
						flag = false;
						break;
					}
				}
				if(flag)
				{
					DU1DuplicateN.add(MemberEntry);
				}
			}
			if(List_RC.size()>0)
			insert List_RC;
		}
		else
		{
			DU1DuplicateN.addAll(List_Members);
		}
	}
	//DU2 duplicate checking by Name
	/*private void DuplicateCheckByName(List<Member_Entry__c> List_Members)
	{
		//Name+City
		Set<String> Set_Name = new Set<String>();
		Set<String> Set_City = new Set<String>();
		for(Member_Entry__c MemberEntry : List_Members)
		{
			if(MemberEntry.Name != null)
			Set_Name.add(MemberEntry.Name); 
			if(MemberEntry.City__c != null)
	 		Set_City.add(MemberEntry.City__c); 
		}
		if(Set_Name.size()==0 && Set_City.size()==0)
		{
			DU2DuplicateN.addAll(List_Members);
			return;
		}
		
		List<Account> List_Acc = [select Id,Name,Standard_City__r.Name,Birthdate__c,(Select Id,Birthdate From Contacts where RecordType.DeveloperName =:'CN_Child' and Birthdate != null order by Birthdate desc) from Account where Name in:Set_Name and Standard_City__r.Name in:Set_City 
								  and Duplicate_Status__c = null and RecordType.DeveloperName=:'CN_HouseHold'];
		if(List_Acc != null && List_Acc.size()>0)
		{
			for(Member_Entry__c MemberEntry : List_Members)
			{
				Boolean flag = true; 
				for(Account acc : List_Acc)
				{
					if(acc.Standard_City__r.Name == MemberEntry.City__c && acc.Name == MemberEntry.Name && acc.Contacts != null && acc.Contacts.size()>0)
					{
						Boolean flag2 = false;
						for(Contact con : acc.Contacts)
						{
							if( MemberEntry.Baby1_Birthday__c != null && math.abs(MemberEntry.Baby1_Birthday__c.daysBetween(con.Birthdate)) <= NumberDaysDue
					  	  		|| MemberEntry.Baby2_Birthday__c != null && math.abs(MemberEntry.Baby2_Birthday__c.daysBetween(con.Birthdate)) <= NumberDaysDue
					  	  		|| MemberEntry.Baby3_Birthday__c != null && math.abs(MemberEntry.Baby3_Birthday__c.daysBetween(con.Birthdate)) <= NumberDaysDue)
							{
								flag2 = true;
								break;
							}
						}
						if(flag2)
						{
							MemberEntry.Duplicate_Status__c = DU2;
		 					MemberEntry.I_Status__c = IstatusSplit;
							DU2DuplicateY.add(MemberEntry);
							Map_MebereIdAccId.put(MemberEntry.Id,acc.Id);
							flag = false;
							break;
						}
					}
				}
				if(flag)
				{
					DU2DuplicateN.add(MemberEntry);
				}
			}
		}
		else
		{
			DU2DuplicateN.addAll(List_Members);
		}
	}*/
	//DU3 duplicate checking by In Use Phone
	private void DuplicateCheckByUsePhone(List<Member_Entry__c> List_Members)
	{
		Set<String> Set_UsePhone = new Set<String>();
		for(Member_Entry__c MemberEntry : List_Members)
		{
			if(MemberEntry.Mobile_Phone__c != null)
	 		Set_UsePhone.add(MemberEntry.Mobile_Phone__c);
		}
		if(Set_UsePhone.size()==0)
		{
			DU3DuplicateN.addAll(List_Members);
			return;
		}
		List<Account> List_Acc = [select Mobile_Phone__c from Account where Mobile_Phone__c in:Set_UsePhone and Duplicate_Status__c = null];
		if(List_Acc != null && List_Acc.size()>0)
		{
			for(Member_Entry__c MemberEntry : List_Members)
			{
				Boolean flag = true; 
				for(Account acc : List_Acc)
				{	
					if(acc.Mobile_Phone__c == MemberEntry.Phone__c)
					{
						MemberEntry.Duplicate_Status__c = DU3;
	 					MemberEntry.I_Status__c = IstatusSplit;
						DU3DuplicateY.add(MemberEntry);
						Map_MebereIdAccId.put(MemberEntry.Id,acc.Id);
						flag = false;
						break;
					}
				}
				if(flag)
				{
					DU3DuplicateN.add(MemberEntry);
				}
			}
		}
		else
		{
			DU3DuplicateN.addAll(List_Members);
		}
	}
	
	//Lead duplicate checking by Register Phone
	private void CheckLeadByRegisterPhone(List<Member_Entry__c> List_Members)
	{
		Set<Id> Set_CaseIds = new Set<Id>();
		List<Member_Entry__c> List_NonExistentLead = new List<Member_Entry__c>();
		Set<String> Set_RegisterPhone = new Set<String>();
		for(Member_Entry__c MemberEntry : List_Members)
		{
			if(MemberEntry.Phone__c != null)
			Set_RegisterPhone.add(MemberEntry.Phone__c);
		}
	
		List<Lead> List_Leade = [select Id,Phone,LastName,Email,Status,Administrative_area__c,Address_Street__c,Specific_Code__c,Other_Phone_1__c,Other_Phone_2__c,
								(Select Id,ContactId,AccountId From Cases__r),(Select Contact__c,Finished_On__c,Questionnaire__r.Type__c From Answers__r),Recruitment_Channel__c,Sub_Recruitment_Channel__c,MobilePhone,Is_VIP__c,Gender_1st_Child__c from Lead where Phone in: Set_RegisterPhone
								and RecordType.DeveloperName = 'CN_Leads' and IsConverted=false];
		if(List_Leade != null && List_Leade.size()>0)
		{
			for(Member_Entry__c MemberEntry : List_Members)
			{
				Boolean flag = true; 
				for(Lead lea : List_Leade)
				{
					if(lea.Phone == MemberEntry.Phone__c)
					{
						flag = false;
						//TODO Merge Member Entry data to Lead
		 				lea = this.ConvertRelatedLead(MemberEntry,lea);
		 				update lea;
		 				//TODO Convert Lead
		 				Database.LeadConvert lc = new database.LeadConvert();
		 				lc.setDoNotCreateOpportunity(true);
						lc.setLeadId(lea.id);
						lc.setConvertedStatus(ConvertedStatus);
						Database.LeadConvertResult lcr = Database.convertLead(lc);
						
						
						//differ 如果是空值则设置差值最大，以防命中
						Integer Differ1 = (MemberEntry.Baby1_Birthday__c != null?math.abs(date.today().daysBetween(MemberEntry.Baby1_Birthday__c)):1000000);
						Integer Differ2 = (MemberEntry.Baby2_Birthday__c != null?math.abs(date.today().daysBetween(MemberEntry.Baby2_Birthday__c)):1000000);
						Integer Differ3 = (MemberEntry.Baby3_Birthday__c != null?math.abs(date.today().daysBetween(MemberEntry.Baby3_Birthday__c)):1000000);
						
						//Create Baby2
						if(MemberEntry.Baby1_Birthday__c !=null)
						{
							Contact ConBaby = this.CreateContact(lcr.getAccountId(),MemberEntry,'Baby',MemberEntry.Baby1_Birthday__c,MemberEntry.Baby1_ID__c);
							insert ConBaby;
							//Recruitment Channel
							Recruitment_Channel__c Recruitment_Channel = this.CreateRecruitmentChannel(ConBaby.Id,MemberEntry,MemberEntry.Baby1_Birthday__c);
	 						Insert Recruitment_Channel;
	 						
	 						if(Differ1<Differ2 && Differ1<Differ3 && lea.Cases__r != null && lea.Cases__r.size()>0)
	 						{
	 							this.MoveCasesToBaby(lcr.getAccountId(),ConBaby.Id,lea.Cases__r);
	 						}
	 						if(Differ1<Differ2 && Differ1<Differ3 && lea.Answers__r != null && lea.Answers__r.size()>0)
	 						{
	 							this.MoveAnswerToBaby(ConBaby.Id,lea.Answers__r);
	 						}
						}
						if(MemberEntry.Baby2_Birthday__c !=null)
						{
							Contact ConBaby = this.CreateContact(lcr.getAccountId(),MemberEntry,'Baby',MemberEntry.Baby2_Birthday__c,MemberEntry.Baby2_ID__c);
							insert ConBaby;
							//Recruitment Channel
							Recruitment_Channel__c Recruitment_Channel = this.CreateRecruitmentChannel(ConBaby.Id,MemberEntry,MemberEntry.Baby2_Birthday__c);
	 						Insert Recruitment_Channel;
	 						
	 						if(Differ2<Differ1 && Differ2<Differ3 && lea.Cases__r != null && lea.Cases__r.size()>0)
	 						{
	 							this.MoveCasesToBaby(lcr.getAccountId(),ConBaby.Id,lea.Cases__r);
	 						}
	 						if(Differ2<Differ1 && Differ2<Differ3 && lea.Answers__r != null && lea.Answers__r.size()>0)
	 						{
	 							this.MoveAnswerToBaby(ConBaby.Id,lea.Answers__r);
	 						}
						}
						if(MemberEntry.Baby3_Birthday__c !=null)
						{
							Contact ConBaby = this.CreateContact(lcr.getAccountId(),MemberEntry,'Baby',MemberEntry.Baby3_Birthday__c,MemberEntry.Baby3_ID__c);
							insert ConBaby;
							//Recruitment Channel
							Recruitment_Channel__c Recruitment_Channel = this.CreateRecruitmentChannel(ConBaby.Id,MemberEntry,MemberEntry.Baby3_Birthday__c);
	 						Insert Recruitment_Channel;
	 						
	 						if(Differ3<Differ2 && Differ3<Differ1 && lea.Cases__r != null && lea.Cases__r.size()>0)
	 						{
	 							this.MoveCasesToBaby(lcr.getAccountId(),ConBaby.Id,lea.Cases__r);
	 						}
	 						if(Differ3<Differ2 && Differ3<Differ1 && lea.Answers__r != null && lea.Answers__r.size()>0)
	 						{
	 							this.MoveAnswerToBaby(ConBaby.Id,lea.Answers__r);
	 						}
						}
					}
				} 
				if(flag)
				{
					List_NonExistentLead.add(MemberEntry);
				}
			}
		}
		else
		{
			List_NonExistentLead.addAll(List_Members);
		}
		
		if(List_NonExistentLead.size()>0)
		this.NonExistentLeadFunction(List_NonExistentLead);
	}
	
	//Lead Non Exist Lead
	private void NonExistentLeadFunction(List<Member_Entry__c> List_Members)
	{
		for(Member_Entry__c MemberEntry : List_Members )
		{
			MemberEntry.I_Status__c = IstatusSplit;
			
			//Create Account 
			Id AccId = null;
			if(Map_MebereIdAccId != null && Map_MebereIdAccId.containsKey(MemberEntry.Id))
			AccId = Map_MebereIdAccId.get(MemberEntry.Id);
			Account acc = this.CreateAccount(MemberEntry,AccId);
			insert acc;
				
			//CreatePrimary Contact
			Contact ParentCon = this.CreateContact(acc.Id,MemberEntry,'Primary',null,null);
			insert ParentCon;
			//CreateBaby Contact
			if(MemberEntry.Baby1_Birthday__c != null)
			{
				Contact BabyCon = this.CreateContact(acc.Id,MemberEntry,'Baby',MemberEntry.Baby1_Birthday__c,MemberEntry.Baby1_ID__c);
				insert BabyCon;
				//Recruitment Channel
				Recruitment_Channel__c RC = this.CreateRecruitmentChannel(BabyCon.Id,MemberEntry,MemberEntry.Baby1_Birthday__c);
 				Insert RC;
			}
			if(MemberEntry.Baby2_Birthday__c != null)
			{
				Contact BabyCon = this.CreateContact(acc.Id,MemberEntry,'Baby',MemberEntry.Baby2_Birthday__c,MemberEntry.Baby2_ID__c);
				insert BabyCon;
				//Recruitment Channel
				Recruitment_Channel__c RC = this.CreateRecruitmentChannel(BabyCon.Id,MemberEntry,MemberEntry.Baby2_Birthday__c);
 				Insert RC;
			}
			if(MemberEntry.Baby3_Birthday__c != null)
			{
				Contact BabyCon = this.CreateContact(acc.Id,MemberEntry,'Baby',MemberEntry.Baby3_Birthday__c,MemberEntry.Baby3_ID__c);
				insert BabyCon;
				//Recruitment Channel
				Recruitment_Channel__c RC = this.CreateRecruitmentChannel(BabyCon.Id,MemberEntry,MemberEntry.Baby3_Birthday__c);
 				Insert RC;
			}
		}
	}
	
	/*
	 *HasRelatedLead
	*/
	//Lead Split
	public void RelatedLeadConvert(List<Member_Entry__c> List_Members)
	{
		for(Member_Entry__c MemberEntry : List_Members)
		{
			if(Map_MebereIdLead!= null && Map_MebereIdLead.containsKey(MemberEntry.Id))
			{
				MemberEntry.I_Status__c = IstatusSplit;
				
				Lead lea =  Map_MebereIdLead.get(MemberEntry.Id);
				//TODO Merge Member Entry data to Lead
	 			lea = this.ConvertRelatedLead(MemberEntry,lea);
	 			update lea;
	 			//TODO Convert Lead
	 			Database.LeadConvert lc = new database.LeadConvert();
	 			lc.setDoNotCreateOpportunity(true);
				lc.setLeadId(lea.id);
				lc.setConvertedStatus(ConvertedStatus);
				Database.LeadConvertResult lcr = Database.convertLead(lc);
					
					
				//differ
				Integer Differ1 = (MemberEntry.Baby1_Birthday__c != null?math.abs(date.today().daysBetween(MemberEntry.Baby1_Birthday__c)):1000000);
				Integer Differ2 = (MemberEntry.Baby2_Birthday__c != null?math.abs(date.today().daysBetween(MemberEntry.Baby2_Birthday__c)):1000000);
				Integer Differ3 = (MemberEntry.Baby3_Birthday__c != null?math.abs(date.today().daysBetween(MemberEntry.Baby3_Birthday__c)):1000000);
					
				//Create Baby2
				if(MemberEntry.Baby1_Birthday__c !=null)
				{
					Contact ConBaby = this.CreateContact(lcr.getAccountId(),MemberEntry,'Baby',MemberEntry.Baby1_Birthday__c,MemberEntry.Baby1_ID__c);
					insert ConBaby;
					//Recruitment Channel
					Recruitment_Channel__c Recruitment_Channel = this.CreateRecruitmentChannel(ConBaby.Id,MemberEntry,MemberEntry.Baby1_Birthday__c);
 					Insert Recruitment_Channel;
 					if(Differ1<Differ2 && Differ1<Differ3 && lea.Cases__r != null && lea.Cases__r.size()>0)
 					{
 						this.MoveCasesToBaby(lcr.getAccountId(),ConBaby.Id,lea.Cases__r);
 					}
 					if(Differ1<Differ2 && Differ1<Differ3 && lea.Answers__r != null && lea.Answers__r.size()>0)
 					{
 						this.MoveAnswerToBaby(ConBaby.Id,lea.Answers__r);
 					}
				}
				if(MemberEntry.Baby2_Birthday__c !=null)
				{
					Contact ConBaby = this.CreateContact(lcr.getAccountId(),MemberEntry,'Baby',MemberEntry.Baby2_Birthday__c,MemberEntry.Baby2_ID__c);
					insert ConBaby;
					//Recruitment Channel
					Recruitment_Channel__c Recruitment_Channel = this.CreateRecruitmentChannel(ConBaby.Id,MemberEntry,MemberEntry.Baby2_Birthday__c);
 					Insert Recruitment_Channel;
 						
 					if(Differ2<Differ1 && Differ2<Differ3 && lea.Cases__r != null && lea.Cases__r.size()>0)
 					{
 						this.MoveCasesToBaby(lcr.getAccountId(),ConBaby.Id,lea.Cases__r);
 					}
 					if(Differ2<Differ1 && Differ2<Differ3 && lea.Answers__r != null && lea.Answers__r.size()>0)
 					{
 						this.MoveAnswerToBaby(ConBaby.Id,lea.Answers__r);
 					}
				}
				if(MemberEntry.Baby3_Birthday__c !=null)
				{
					Contact ConBaby = this.CreateContact(lcr.getAccountId(),MemberEntry,'Baby',MemberEntry.Baby3_Birthday__c,MemberEntry.Baby3_ID__c);
					insert ConBaby;
					//Recruitment Channel
					Recruitment_Channel__c Recruitment_Channel = this.CreateRecruitmentChannel(ConBaby.Id,MemberEntry,MemberEntry.Baby3_Birthday__c);
 					Insert Recruitment_Channel;
 						
 					if(Differ3<Differ2 && Differ3<Differ1 && lea.Cases__r != null && lea.Cases__r.size()>0)
 					{
 						this.MoveCasesToBaby(lcr.getAccountId(),ConBaby.Id,lea.Cases__r);
 					}
 					if(Differ3<Differ2 && Differ3<Differ1 && lea.Answers__r != null && lea.Answers__r.size()>0)
 					{
 						this.MoveAnswerToBaby(ConBaby.Id,lea.Answers__r);
 					}
				}
			}
		}
	}
	
	/*=Move Cases to Baby=*/
	public void MoveCasesToBaby(Id AccountId,Id ContactId,List<Case> List_Case )
	{
		List<Case> List_UpCase = new List<Case>();
		for(Case ca:List_Case)
		{
			ca.ContactId = ContactId;
			ca.AccountId = AccountId;
			List_UpCase.add(ca);
		}
		update List_UpCase;
	}
	/*=Move Answer to Baby=*/
	public void MoveAnswerToBaby(Id ContactId,List<Answer__c> List_Answer )
	{
		List<Answer__c> List_UpAnswer = new List<Answer__c>();
		for(Answer__c an:List_Answer)
		{
			an.Contact__c = ContactId;
			List_UpAnswer.add(an);
		}
		update List_UpAnswer;
	}
	/*=Birthdate Match=*/
	/*=Birthdate 差值 270 创建newBaby=*/
	/*=create new baby=*/
	private Contact BirthdateRangeValidator(List<Contact> List_Contact,Date BirthdayDate)
	{
		Contact Retcon = new Contact();
		for(Contact Con : List_Contact)
		{
			if(Con.Birthdate == BirthdayDate)
			{
				Retcon = Con;
				break;
			}
		}
		if(Retcon.Id == null && List_Contact.size()>0 && List_Contact[0].Birthdate.daysBetween(BirthdayDate)<NumberDayDiffer )
		Retcon = List_Contact[0];
		
		return Retcon;
	}
	
	
	
	
	//Convert Related Lead=
	public Lead ConvertRelatedLead(Member_Entry__c MemberEntry,Lead lea)
	{
		//TODO Merge Member Entry data to Lead
 		if(lea.Phone == null)lea.Phone = MemberEntry.Phone__c;
 		if(lea.LastName == null)lea.LastName = MemberEntry.Name;
 		if(lea.Email == null)lea.Email = MemberEntry.Email__c;
 		if(lea.Status == null)lea.Status = MemberEntry.Status__c;
 		if(lea.Administrative_area__c == null)lea.Administrative_area__c = MemberEntry.Administrative_area__c;
 		if(lea.Address_Street__c == null)lea.Address_Street__c = MemberEntry.Address_Street__c;
 		if(lea.Specific_Code__c == null)lea.Specific_Code__c = MemberEntry.Specific_Code__c;
 		if(lea.Other_Phone_1__c == null)lea.Other_Phone_1__c = MemberEntry.Other1__c;
 		if(lea.Other_Phone_2__c == null)lea.Other_Phone_2__c = MemberEntry.Other1__c;
 		if(lea.Recruitment_Channel__c == null)lea.Recruitment_Channel__c = MemberEntry.Register_Source__c;
 		if(lea.Sub_Recruitment_Channel__c == null)lea.Sub_Recruitment_Channel__c = MemberEntry.Register_Sub_Source__c;
 		if(lea.MobilePhone == null)lea.MobilePhone = MemberEntry.Mobile_Phone__c;
 		if(lea.Is_VIP__c == null)lea.Is_VIP__c = MemberEntry.Is_VIP__c;
 		if(lea.Gender_1st_Child__c == null)lea.Gender_1st_Child__c = MemberEntry.Gender_1st_Child__c;
 		lea.OwnerId = UserInfo.getUserId();
 		return lea;
	}
	/*= Create Account =*/
	public Account CreateAccount(Member_Entry__c MemberEntry,Id Duplicate_For_AccId)
	{
		//Create Account 
		Account acc = new Account();
		acc.Duplicate_Status__c = MemberEntry.Duplicate_Status__c;
		acc.RecordTypeId = CN_AccHouseHoldRecordTypeId;
		acc.Duplicate_For__c = Duplicate_For_AccId; 
		acc.Hospital__c = MemberEntry.Hospital__c;
		acc.Member_Entry__c = MemberEntry.Id;
		acc.Phone = MemberEntry.Phone__c;
		acc.Name = MemberEntry.Name;
		acc.Account_Email__c = MemberEntry.Email__c;
		acc.Status__c = MemberEntry.Status__c;
		acc.Birthdate__c = MemberEntry.Baby1_Birthday__c;
		acc.Standard_City__c = MemberEntry.Administrative_Area__c;
		acc.Address_Street__c = MemberEntry.Address_Street__c;
		acc.Member_Register_Date__c = MemberEntry.Register_Date__c;
		acc.Other_Phone_1__c = MemberEntry.Other1__c;
		acc.Other_Phone_2__c = MemberEntry.Other2__c;
		acc.Mobile_Phone__c = MemberEntry.Mobile_Phone__c;
		acc.Is_VIP__c = MemberEntry.Is_VIP__c;
		acc.Signed__c = MemberEntry.Signed__c;
		return acc;
	}
	//Create Primary Contact
	//Create Baby Contact
	public Contact CreateContact(Id AccId,Member_Entry__c MemberEntry,String ContactType,Date Baby_Birthday,String Baby_Id)
	{
		Contact con = new Contact();
		if(ContactType=='Primary')
		{
			con.RecordTypeId = CN_ContactParentRecordTypeId;
			con.AccountId = AccId;
			con.LastName = MemberEntry.Name;
			con.Status__c =MemberEntry.Status__c;
			con.Is_Primary_Contact__c = true;
			con.Email = MemberEntry.Email__c;
		}
		else if(ContactType=='Baby' && Baby_Birthday != null)
		{
			con.Duplicate_Status__c = MemberEntry.Duplicate_Status__c;
			con.RecordTypeId = CN_ContactChildRecordTypeId;
			con.AccountId = AccId;
			con.LastName = MemberEntry.Name+'的'+BabyDefaultName;
			con.Birthdate = Baby_Birthday;
			con.BF_Baby_ID__c = Baby_Id;
			con.Register_Date__c = MemberEntry.Register_Date__c;
			con.Is_Winning__c = MemberEntry.Is_Win__c;
			con.Specific_Code__c = MemberEntry.Specific_Code__c;
			con.Is_Verified__c = MemberEntry.Is_Verified__c;
			con.Channel_Name__c = MemberEntry.Hospital__c;
			con.Channel_Name__c = MemberEntry.Store__c;
			con.Sales_Rep__c = MemberEntry.Sales_Rep__c;
			con.Archive_Month__c = MemberEntry.Archive_Month__c;
			con.Twins__c = MemberEntry.BF_Is_Twins__c;
			con.Gender__c = MemberEntry.Gender_1st_Child__c; 
			con.Primary_RecruitChannel_Type__c = MemberEntry.Register_Source__c;
 			con.Primary_RecruitChannel_Sub_type__c = MemberEntry.Register_Sub_Source__c;
 			
 			con.Special_Product__c = MemberEntry.Special_Product__c;
 			
 			con.DoNotCall = MemberEntry.DoNotCall__c;
 			con.Do_Not_Invite__c = MemberEntry.Do_Not_Invite__c;
 			con.Do_Not_SMS__c = MemberEntry.Do_Not_SMS__c;
 			con.Inactive__c = MemberEntry.Inactive__c;
 			con.Inactive_Reason__c = MemberEntry.Inactive_Reason__c;
		}
		return con;
	}
	
	
	/*=Create Recruitment_Channel=*/
	private Recruitment_Channel__c CreateRecruitmentChannel(Id ContactId,Member_Entry__c MemberEntry,Date Baby_Birthday)
	{
		//Create RC 
		Recruitment_Channel__c RC1 = new Recruitment_Channel__c();
		RC1.Duplicate_Status__c = MemberEntry.Duplicate_Status__c; 
 		RC1.RecordTypeId = CN_RecChanRecordTypeId;
 		RC1.Contact__c = ContactId;
 		//TODO Sync Field
 		RC1.Registration_Phone__c = MemberEntry.Phone__c;
 		RC1.Email__c = MemberEntry.Email__c;
 		RC1.Status__c = MemberEntry.Status__c;
 		RC1.Baby_Birthday__c = Baby_Birthday;
 		RC1.Standard_City__c = MemberEntry.Administrative_Area__c;
 		RC1.Address_Street__c = MemberEntry.Address_Street__c;
 		RC1.Register_Date__c = MemberEntry.Register_Date__c;
 		RC1.Is_Win__c = MemberEntry.Is_Win__c;
 		RC1.Specific_Code__c = MemberEntry.Specific_Code__c;
 		RC1.Other_Phone_1__c = MemberEntry.Other1__c;
 		RC1.Other_Phone_2__c = MemberEntry.Other2__c;
 		RC1.Hospital__c = MemberEntry.Hospital__c;
 		RC1.Store__c = MemberEntry.Store__c; 
 		RC1.Sales_Rep__c = MemberEntry.Sales_Rep__c;
 		RC1.Mobile_Phone__c = MemberEntry.Mobile_Phone__c ; 
 		RC1.Twins__c = MemberEntry.BF_Is_Twins__c;	
 		RC1.NC__c = MemberEntry.NC__C;
 		RC1.CN_Channel__c = MemberEntry.Register_Source__c;
 		RC1.CN_Sub_Channel__c = MemberEntry.Register_Sub_Source__c;
 		return RC1; 
	}
	global void finish(Database.BatchableContext BC)
	{
    	
    	
  	}
	 
}