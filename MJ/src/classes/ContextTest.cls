/*Author:leo.bi@celent.com.cn
 *Date:2014-07-13
 *Function:test context
 */
@isTest
private class ContextTest
{

    static testMethod void myUnitTest() 
    {
    	List<String> list_Roles = new List<String>();
    	Boolean result = Context.ApplyTo(list_Roles);
    	list_Roles.add('All');
    	result = Context.ApplyTo(list_Roles);
    	list_Roles.clear();
    	list_Roles.add('UK');
    	result = Context.ApplyTo(list_Roles);
    	list_Roles.add('CN');
    	list_Roles.add('System');
    	list_Roles.add('HongKong');
    	list_Roles.add('Macao');
    	ID roleId = UserInfo.getUserRoleId();
    	UserRole role = [Select ID, Name, DeveloperName From UserRole Where ID =: roleId];
    	list_Roles.add(role.DeveloperName);
    	result = Context.ApplyTo(list_Roles);
    	result = Context.ApplyTo(role.DeveloperName);
    }
}