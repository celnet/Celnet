/*
*Author:Peter
*Date:20140924
Function:Member register page controller
*/
public class WechatMemberRegisterPageController
{
	private Wechat_User__c WechatUser;       
	public string RegisterPhone {get; set;}  
	private final string OpenID; 
	public Lead wechatLead {get;set;}
	public string isSuccess{get;set;}
	public string errorMsg{get;set;}
	public string Salutation{get;set;}
	public string LastName {get;set;}
	public boolean showInput{get;set;}
	public boolean showOutput{get;set;}
	public string successMsg{get;set;}
	public string needToknow{get;set;}
	
	public WechatMemberRegisterPageController()
	{
		isSuccess = '';
		showInput = true;
		showOutput = false;
		needToknow = WechatBusinessUtility.WECHAT_SITE + 'WechatMemberNeedToknow';
		this.OpenID = Apexpages.currentPage().getParameters().get('OpenId');
		this.RegisterPhone = Apexpages.currentPage().getParameters().get('Phone');
		system.debug('*********openId*********' + openid);
		system.debug('*********RegisterPhone*********' + RegisterPhone);
		Id LeadRecordTypeId = [select Id from RecordType where DeveloperName='LeadsHK' and SobjectType=:'Lead'].Id;
		wechatLead = new Lead();
		wechatLead.RecordTypeId = LeadRecordTypeId;
		List<Wechat_User__c> list_WechatUser = [select Id from Wechat_User__c where Open_Id__c=:this.OpenID];
		if(list_WechatUser != null)
		{
			this.WechatUser = list_WechatUser[0];
		}
	}
	
	//保存注册的信息
	public void Register()
	{
		try
		{
			system.debug('*************Register*************');
			wechatLead.Salutation = this.Salutation;
			wechatLead.LastName = this.LastName;
			wechatLead.MobilePhone = RegisterPhone;
			wechatLead.LeadSource = 'Wechat';
			wechatLead.Sub_Channel__c = 'HK Mom';
			insert wechatLead;
			this.WechatUser.Binding_Non_Member__c = wechatLead.Id;
			update this.WechatUser;
			successMsg = WechatBusinessUtility.QueryRemindingByKey('RN032');
			isSuccess = 'success';
			showInput = false;
			showOutput = true;			
		}
		catch(exception e)
		{
			isSuccess = 'fail';
			errorMsg = e.getmessage();
		}
	}
}