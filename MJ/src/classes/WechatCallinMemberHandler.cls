/*Author:Mark
 *Date:20140924
 *Function:用户在微信客户端上点击会员优惠按钮时由此Handler来处理
*/
public class WechatCallinMemberHandler extends WechatCallinMsgHandler
{
    public override void Handle(WechatCallinMsgPipelineContext Context)
    {
        WechatEntity.InEventMsg inEvent = (WechatEntity.InEventMsg)Context.InMsg;
        if(inEvent.EventKey != this.BindingInfo.Event_Key__c)
        {
            return;
        }
        WechatEntity.OutTextMsg outMsg = WechatEntity.GenerateOutMsg(Context.InMsg , '');
        string openId = Context.InMsg.FromUserName;
        /*string ti = WechatBusinessUtility.CheckWechatUserRelationshipByFields(openId);
        if(ti == '')
        {
            outMsg.Content = '您好，參加“會員優惠”前需要您請先進行註冊或綁定，謝謝。<a href="'+WechatBusinessUtility.WECHAT_SITE+'apex/WechatSMSVerificationPage?openId='+ openId +'">【註冊/綁定】</a>';
            Context.OutMsg = outMsg;
        }
        else if(ti == 'Non-Member')
        {
            WechatBusinessUtility.BirthCount bc = WechatBusinessUtility.GetBirthCertificateCount(openId);
            if(bc.birthCertificateCount > bc.hasBirthCount)
            {
                outMsg.Content = '您好，參加“會員優惠”前需要您完成全部照片的上傳，謝謝。【請點“擊會員專區”->“上傳出生證明”】';
            }
            else
            {
                outMsg.Content = '您好，您的註冊信息及出生證明正在審核中，審核通過後我們將微信通知您，屆時您即可享受美贊臣的微信服務，謝謝。';
            }
            Context.OutMsg = outMsg;
        }
        else if(ti == 'Member')
        {*/
            List<WechatEntity.Article> articleList = new List<WechatEntity.Article>();
            WechatEntity.OutRichMediaMsg outRichMedia = new WechatEntity.OutRichMediaMsg();
            WechatEntity.GetBaseMsgAtrributes(Context.InMsg,outRichMedia);
            outRichMedia.MsgType = WechatEntity.MESSAGE_TYPE_NEWS;
            outRichMedia.Articles = articleList;    
            List<Wechat_Menu__c> wmList = [select id , Wechat_Content__r.Article_Url__c ,Wechat_Content__r.Description__c , Wechat_Content__r.Key_Word__c , Wechat_Content__r.Picture_Url__c , Wechat_Content__r.Title__c 
                                           from Wechat_Menu__c
                                           where Event_Key__c = :inEvent.EventKey];
            if(!wmList.isempty())
            {
                Wechat_Menu__c wm = wmList[0];
                WechatEntity.Article art = GenerateWechatArticle(wm);
                outRichMedia.Articles.add(art);
            }
            Context.OutMsg = outRichMedia;
        //}
    }
    
    public WechatEntity.Article GenerateWechatArticle(Wechat_Menu__c wm)
    {
        WechatEntity.Article art = new WechatEntity.Article();
        art.title = wm.Wechat_Content__r.Title__c;
        art.description = wm.Wechat_Content__r.Description__c;
        art.url = wm.Wechat_Content__r.Article_Url__c;
        art.picURL = (null != wm.Wechat_Content__r.Picture_Url__c ? wm.Wechat_Content__r.Picture_Url__c : '');
        return art;
    }
}