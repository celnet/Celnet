/*
*用户点击注册绑定按钮，由此Handler来处理
*/
public class WechatCallinRegisterHandler extends WechatCallinMsgHandler
{
    public override void Handle(WechatCallinMsgPipelineContext Context)
    {
        string openId = Context.InMsg.FromUserName;
        WechatEntity.OutTextMsg outMsg = WechatEntity.GenerateOutMsg(Context.InMsg , '');
        WechatEntity.InEventMsg inEvent = (WechatEntity.InEventMsg)Context.InMsg;
        if(inEvent.EventKey != this.BindingInfo.Event_Key__c)
        {
            return;
        }
        string re = WechatBusinessUtility.CheckWechatUserRelationshipByFields(openId);
        if(re == 'Member')
        {
            outMsg.Content = WechatBusinessUtility.QueryRemindingByKey('RN006');
        }
        else if(re == 'Non-Member')
        {
            WechatBusinessUtility.BirthCount bc = WechatBusinessUtility.GetBirthCertificateCount(openId);
            if(bc.birthCertificateCount == bc.hasBirthCount)
            {
                outMsg.Content = WechatBusinessUtility.QueryRemindingByKey('RN007');
            }
            else if(bc.birthCertificateCount != 0 && bc.hasBirthCount == 0)
            {
                outMsg.Content = WechatBusinessUtility.QueryRemindingByKey('RN016');
            }
            else
            {
                list<object> replaceKeywords = new list<object>();
				replaceKeywords.add(bc.hasBirthCount);
				replaceKeywords.add(bc.birthCertificateCount - bc.hasBirthCount);
                outMsg.Content = WechatBusinessUtility.QueryRemindingByKey('RN008',replaceKeywords);
            }
        }
        else
        {
        	string replace = '<a href="'+WechatBusinessUtility.WECHAT_SITE+'WechatSMSVerificationPage?openId='+Context.InMsg.FromUserName+'">【註冊/綁定】</a>';
        	outMsg.Content = WechatBusinessUtility.QueryRemindingByKey('RN009',replace);
        }
        Context.OutMsg = outMsg;
    }
}