/*
*Author:michael.mi@celnet.com.cn
*Date:2014-09-04
*Function:processor batch
*/
global class WechatCalloutProcessorBatch implements Database.Batchable<Wechat_Callout_Task_Queue__c>,Database.AllowsCallouts,Database.Stateful,Schedulable
{
	global WechatCalloutProcessor calloutProcessor;
	global list<Wechat_Callout_Task_Queue__c> taskList;
	global boolean TokenExpire = false;
	global void execute(SchedulableContext sc)  
	{
		system.abortjob(sc.getTriggerId());
		WechatCalloutProcessorBatch.StartWechatCalloutProcessorBatch();
	}
	
	global list<Wechat_Callout_Task_Queue__c> start(Database.BatchableContext BC)
	{
		taskList = WechatCalloutQueueManager.DeQueue();
		if(taskList.isempty())
		{
			return null;  
		}
		system.debug('*********batchStart********' + taskList);
		type t = type.forname(taskList[0].Processor_Name__c);
		calloutProcessor = (WechatCalloutProcessor)t.newInstance();
		return taskList;
	}	   
	
	global void execute(Database.BatchableContext BC, List<Wechat_Callout_Task_Queue__c> taskList)
	{
		system.debug('***********execute*********' + calloutProcessor);
		for(Wechat_Callout_Task_Queue__c task : taskList)
		{
			try
			{
				if(TokenExpire)
				{
					cloneTaskQueue(task,WechatCalloutQueueManager.TASK_TYPE_WAITING,'TokenExpire Retry Callout!');
					continue;
				}
				calloutProcessor.DoCallout(task);
			}	
			catch(WechatException we)
			{
				if(we.TokenExpire.contains(we.errorcode))  
				{
					TokenExpire = true;
					WechatRefreshAccessTokenScheduler.RefreshToken(task.Public_Account_Name__c);
				}
				if(we.RetryCode.contains(we.errorcode))
				{
					cloneTaskQueue(task,WechatCalloutQueueManager.TASK_TYPE_WAITING,we.errMsg);
				}
			}
			catch(CalloutException ce)
			{
				cloneTaskQueue(task,WechatCalloutQueueManager.TASK_TYPE_WAITING,ce.getmessage());
			}
			catch(exception ex)
			{
				cloneTaskQueue(task,WechatCalloutQueueManager.TASK_TYPE_FAILURE,ex.getmessage());
			}
		}
	}
	
	private void cloneTaskQueue(Wechat_Callout_Task_Queue__c task,string status,string errorMsg)
	{
		Wechat_Callout_Task_Queue__c errorQueue = task.clone(false,false,false,false);
		errorQueue.Status__c = status;
		errorQueue.Error_Log__c = errorMsg;
		calloutProcessor.WechatTaskList.add(errorQueue);
	}
	
	global void finish(Database.BatchableContext BC)
	{
		system.debug('***********Finish*********' + calloutProcessor);
		if(calloutProcessor == null)
		{
			return;
		}
		calloutProcessor.FinishData();	
		system.debug('*********** calloutProcessor.WechatTaskList***************' + calloutProcessor.WechatTaskList);
		calloutProcessor.HandleNewQueue();
		WechatCalloutQueueManager.FinalizeTasks(taskList);
		StartWechatCalloutProcessorBatch();
	}
	
	global static void StartWechatCalloutProcessorBatch()
	{
		if(system.test.isRunningTest())
		{
			return;
		}
		preRunJob();
	}
	
	public static void preRunJob()
	{
		//STEP1:Check whether there are rest queue member,if false, return
		if(!WechatCalloutQueueManager.HasTask()) return;
		//STEP2:Check whether there are running WechatCalloutProcessorBatch, if true, return
		set<string> set_status = new set<string>{'Queued','Processing','Preparing'};
		ApexClass ac = [select id from ApexClass  where Name = 'WechatCalloutProcessorBatch'];
		list<AsyncApexJob> list_job = [select Id from AsyncApexJob a where a.ApexClassId =:ac.Id and a.Status IN:set_status];
		if(!list_job.isEmpty()) return;
		//STEP3:Check running batch's count. If it runs up not to 5, run batch. Otherwise, STEP4; 
		Integer runningBatchNumber = WechatCalloutProcessorBatch.checkRunningBatch(set_status);
		if(runningBatchNumber < 5)
		{
			database.executeBatch(new WechatCalloutProcessorBatch(),10);
		}
		else
		{//STEP4:Check running scheduler if there is a running scheduler, return; Or, run new scheduler which runs in 5 minitues;
			WechatCalloutProcessorBatch.runSchedule();
		}
	}
	
	private static Integer checkRunningBatch(Set<string> set_status)
	{
		Integer result = 0;
		//only batch apex limit is 5, so only count Batch Apex number
		List<AsyncApexJob> list_AsyncApexJob = [Select Status, Id 
												From AsyncApexJob 
												where JobType='BatchApex' 
												and Status in :set_status]; 
		if(list_AsyncApexJob.size() > 0)
		{
			result = Integer.valueOf(list_AsyncApexJob.size());
		}
		return result;
	}
	
	public static void runScheduleForTestClass()
	{
		if(system.test.isRunningTest())
		{
			runSchedule();
		}
	}
	
	private static void runSchedule()
	{
		List<CronTrigger> list_CronTrigger =   [select Id, NextFireTime from CronTrigger where CronJobDetail.Name like 'HK_WechatCalloutProcessorScheduler%'];
		List<CronTrigger> list_Running_Scheduler = new List<CronTrigger>();
		for(CronTrigger ct : list_CronTrigger)
		{
			if(ct.NextFireTime == null)
			{//abort unused scheduler
				system.abortJob(ct.Id);
			}
			else
			{
				list_Running_Scheduler.add(ct);
			}
		}
		if(!list_Running_Scheduler.isEmpty())
		{//If there is running scheduler, return.
			return;
		}
		//run new scheduler
		WechatCalloutProcessorBatch bs = new WechatCalloutProcessorBatch();
		DateTime newDT = DateTime.now().addMinutes(2);
		String sch = '0 ' + String.valueOf(newDT.minute()) + ' '
						  + String.valueOf(newDT.hour()) + ' ' + String.valueOf(newDT.day()) + ' '
						  + String.valueOf(newDT.month()) + ' ? ' + String.valueOf(newDT.year());
		system.schedule('HK_WechatCalloutProcessorScheduler' + String.valueOf(DateTime.now()), sch, bs);
	}
}