/*Author:leo.bi@celnet.com.cn
 */
@isTest(SeeAllData = true)
private class QTaskEventHandlerTest {

	static testMethod void testTaskTrigger()
	{
		system.runAs(TestUtility.new_CN_User())
		{
			//test Task Trigger
			Id conRecordTypeId = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
			Account acc = new Account();
			acc.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_HouseHold' and SobjectType=:'Account'].id;
			acc.Name = 'HouseHold123456';
			acc.Last_Name__c ='HouseHold123456';
			insert acc;
			
			Contact con = new Contact();
			con.RecordTypeId = conRecordTypeId;
			con.AccountId = acc.Id;
			con.LastName = 'conTask';
			Contact con1 = new Contact(); 
			con1.RecordTypeId = conRecordTypeId;
			con1.AccountId = acc.Id;
			con1.LastName = 'conTask';
			List<Contact> list_Contact = new List<Contact>();
			list_Contact.add(con1);
			list_Contact.add(con);
			insert list_Contact;
			
			Q_Task__c qt1 = new Q_Task__c();
			qt1.Contact__c = con1.Id;
			qt1.Status__c = 'Open';
			qt1.ID__c = qt1.Contact__c + '-' + qt1.Status__c;
			Q_Task__c qt = new Q_Task__c();
			qt.Contact__c = con.Id;
			qt.Status__c = 'Open';
			qt.ID__c = qt.Contact__c + '-' + qt.Status__c;
			List<Q_Task__c> list_QTask = new List<Q_Task__c>();
			list_QTask.add(qt1);
			list_QTask.add(qt);
			insert list_QTask;
			
			//一次无人接，两次关机
			Task t = new Task();
			t.CallObject = 'QTask';
			t.WhoId = con.Id;
			t.Result__c = 'No Response';
			t.Caller_Phone_Number__c ='11511112222';
			
			Task t11 = new Task();
			t11.CallObject = 'QTask';
			t11.WhoId = con1.Id;
			t11.Result__c = 'Phone Closed';
			t11.Caller_Phone_Number__c ='11511112222';
			Task t12 = new Task();
			t12.CallObject = 'QTask';
			t12.WhoId = con1.Id;
			t12.Result__c = 'Phone Closed';
			t12.Caller_Phone_Number__c ='11511112222';
			
			List<Task> list_Task = new List<Task>();
			list_Task.add(t);
			list_Task.add(t11);
			list_Task.add(t12);
			insert list_Task;
			
			
			Task t1 = new Task();
			t1.WhoId = con.Id;
			t1.Result__c = 'No Response';
			t1.Caller_Phone_Number__c ='11511112222';
			Task t121 = new Task();
			t121.WhoId = con1.Id;
			t121.Result__c = 'Phone Closed';
			t121.Caller_Phone_Number__c ='11511112222';
			list_Task.add(t121);
			list_Task.clear();
			list_Task.add(t1);
			insert list_Task;
			
			//六次无人接，六次关机
			Task t2 = new Task();
			t2.CallObject = 'QTask';
			t2.WhoId = con.Id;
			t2.Result__c = 'No Response';
			t2.Caller_Phone_Number__c ='11511112222';
			Task t21 = new Task();
			t21.CallObject = 'QTask';
			t21.WhoId = con.Id;
			t21.Result__c = 'No Response';
			t21.Caller_Phone_Number__c ='11511112222';
			Task t31 = new Task();
			t31.CallObject = 'QTask';
			t31.WhoId = con.Id;
			t31.Result__c = 'No Response';
			t31.Caller_Phone_Number__c ='11511112222';
			Task t311 = new Task();
			t311.CallObject = 'QTask';
			t311.WhoId = con.Id;
			t311.Result__c = 'No Response';
			t311.Caller_Phone_Number__c ='11511112222';
			Task t32 = new Task();
			t32.CallObject = 'QTask';
			t32.WhoId = con.Id;
			t32.Result__c = 'No Response';
			t32.Caller_Phone_Number__c ='11511112222';
			
			Task t13 = new Task();
			t13.CallObject = 'QTask';
			t13.WhoId = con1.Id;
			t13.Result__c = 'Phone Closed';
			t13.Caller_Phone_Number__c ='11511112222';
			Task t14 = new Task();
			t14.CallObject = 'QTask';
			t14.WhoId = con1.Id;
			t14.Result__c = 'PhoneClosed';
			t14.Caller_Phone_Number__c ='11511112222';
			Task t15 = new Task();
			t15.CallObject = 'QTask';
			t15.WhoId = con1.Id;
			t15.Result__c = 'Phone Closed';
			t15.Caller_Phone_Number__c ='11511112222';
			Task t16 = new Task();
			t16.CallObject = 'QTask';
			t16.WhoId = con1.Id;
			t16.Result__c = 'Phone Closed';
			t16.Caller_Phone_Number__c ='11511112222';
			List<Task> list_Task1 = new List<Task>();
			list_Task1.add(t2);
			list_Task1.add(t21);
			list_Task1.add(t31);
			list_Task1.add(t311);
			list_Task1.add(t32);
			list_Task1.add(t13);
			list_Task1.add(t14);
			list_Task1.add(t15);
			list_Task1.add(t16);
			insert list_Task1;
			
			Task t212 = new Task();
			t212.WhoId = con.Id;
			t212.Result__c = 'No Response';
			t212.Caller_Phone_Number__c ='11511112222';
			Task t131 = new Task();
			t131.WhoId = con1.Id;
			t131.Result__c = 'Phone Closed';
			t131.Caller_Phone_Number__c ='11511112222';
			Task t444 = new Task();
			t444.WhoId = con1.Id;
			t444.Result__c = 'No Answer2';
			t444.Delete_Flag__c = true;
			t444.Caller_Phone_Number__c ='11511112222';
			t444.Status = 'Closed';
			list_Task1.clear();
			list_Task1.add(t212);
			list_Task1.add(t444);
			list_Task1.add(t131);
			insert list_Task1;
			Q_Task__c qt11 = [select Id, Contact__c, Status__c from Q_Task__c where Contact__c = :con.Id][0];
			system.assertEquals('Closed', qt11.Status__c);
		}
	}
    static testMethod void myUnitTest()
    {
    	system.runAs(TestUtility.new_CN_User())
		{
	        // TO DO: implement unit test
	        //test closeQTaskAfterFinishAnswer
			/*1.Routine
			 *2.Invitation
			 *3.Elite
			 */
	     	Account acc = new Account();
			acc.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_HouseHold' and SobjectType=:'Account'].id;
			acc.Name = 'HouseHold123456';
			acc.Last_Name__c ='HouseHold123456';
			insert acc;
			//1.Routine
			Contact conRoutine = new Contact();
			conRoutine.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
			conRoutine.AccountId = acc.Id;
			conRoutine.LastName = 'conRoutine';
			insert conRoutine;
			Answer__c routineAnswer = new Answer__c();
			routineAnswer.Contact__c = conRoutine.Id;
			routineAnswer.Status__c = 'Draft';
			insert routineAnswer;
			Q_Task__c routine = new Q_Task__c();
			routine.Type__c = 'Routine';
			routine.Contact__c = conRoutine.Id;
			routine.ID__c = routine.Contact__c + '-' + routine.Status__c;
			routine.Status__c = 'Open';
			insert routine;
			routineAnswer.Status__c = 'Finish';
			update routineAnswer;
			
			//2.Invitation
			Contact conInvitation = new Contact();
			conInvitation.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
			conInvitation.AccountId = acc.Id;
			conInvitation.LastName = 'conInvitation';
			insert conInvitation;
			Campaign c = new Campaign();
			c.Name = 'Test' + String.valueOf(DateTime.now());
			c.Campaign_Type__c = 'Invitation';
			insert c;
			CampaignMember cm = new CampaignMember();
			cm.ContactId = conInvitation.Id;
			cm.CampaignId = c.Id;
			cm.Call_Status__c = 'Waiting Invitation';
			cm.Status = 'Sent';
			insert cm;
			//after insert a campaignmember, the invitationQTaskDistributionHandler will insert a QTask
			Answer__c invitationAnswer = new Answer__c();
			invitationAnswer.Contact__c = conInvitation.Id;
			invitationAnswer.Status__c = 'Draft';
			invitationAnswer.Campaign_Member_Id__c = cm.Id;
			insert invitationAnswer;
			invitationAnswer.Status__c = 'Finish';
			update invitationAnswer;
			//3.Elite
			Test.startTest();
			Questionnaire__c que = new Questionnaire__c();
			que.Name = 'test que';
			insert que;
			Contact conElite = new Contact();
			conElite.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
			conElite.AccountId = acc.Id;
			conElite.Questionnaire__c = que.Id;
			conElite.LastName = 'conElite';
			insert conElite;
			Campaign c1 = new Campaign();
			c1.Name = 'Test1' + String.valueOf(DateTime.now());
			c1.Campaign_Type__c = 'Invitation';
			insert c1;
			CampaignMember cm1 = new CampaignMember();
			cm1.ContactId = conElite.Id;
			cm1.CampaignId = c1.Id;
			cm1.Call_Status__c = 'Waiting Invitation';
			cm1.Status = 'Sent';
			insert cm1;
			//upgrade elite
			Q_Task__c elite = [select Id, Type__c, Status__c,Has_Remaining_Call__c,Contact__c from Q_Task__c where Contact__c = :conElite.Id];
			elite.Type__c = 'Routine';
			update elite;
			
			Answer__c eliteInvitationAnswer = new Answer__c();
			eliteInvitationAnswer.Contact__c = conElite.Id;
			eliteInvitationAnswer.Questionnaire__c = que.Id;
			eliteInvitationAnswer.Campaign_Member_Id__c = cm1.Id;
			eliteInvitationAnswer.Status__c = 'Finish';
			insert eliteInvitationAnswer;
			
			//1.1finish a routine answer of a elite qtask
			Questionnaire__c que1 = new Questionnaire__c();
			que1.Name = 'test que1';
			insert que1;
			
			Campaign c2 = new Campaign();
			c2.Name = 'Test2' + String.valueOf(DateTime.now());
			c2.Campaign_Type__c = 'Invitation';
			insert c2;
			
			Contact conRoutineElite = new Contact();
			conRoutineElite.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
			conRoutineElite.AccountId = acc.Id;
			conRoutineElite.Questionnaire__c = que1.Id;
			conRoutineElite.LastName = 'conRoutineElite';
			insert conRoutineElite;
			
			CampaignMember cm2 = new CampaignMember();
			cm2.ContactId = conRoutineElite.Id;
			cm2.CampaignId = c2.Id;
			cm2.Call_Status__c = 'Waiting Invitation';
			cm2.Status = 'Sent';
			insert cm2;
			
			Answer__c invitationAnswer1 = new Answer__c();
			invitationAnswer1.Contact__c = conRoutineElite.Id;
			invitationAnswer1.Status__c = 'Draft';
			invitationAnswer1.Campaign_Member_Id__c = cm2.Id;
			insert invitationAnswer1;
			
			//upgrade elite
			Q_Task__c elite1 = [select Id, Type__c, Status__c,Has_Remaining_Call__c,Contact__c from Q_Task__c where Contact__c = :conRoutineElite.Id];
			elite1.Type__c = 'Routine';
			update elite1;
			
			Answer__c eliteInvitationAnswer1 = new Answer__c();
			eliteInvitationAnswer1.Contact__c = conRoutineElite.Id;
			eliteInvitationAnswer1.Questionnaire__c = que1.Id;
			eliteInvitationAnswer1.Status__c = 'Finish';
			insert eliteInvitationAnswer1;
			Test.stopTest();
		}
    }
    static testMethod void testRest()
	{	
		system.runas(TestUtility.new_CN_User())
		{
			Id conRecordTypeId = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
			Account acc = new Account();
			acc.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_HouseHold' and SobjectType=:'Account'].id;
			acc.Name = 'HouseHoldleo'+ String.valueOf(Date.today());
			acc.Last_Name__c ='HouseHoldleo'+ String.valueOf(Date.today());
			insert acc;
			Questionnaire__c que11 = new Questionnaire__c();
			que11.Name = 'test que11';
			insert que11;
			Contact conInvitation = new Contact();
			conInvitation.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
			conInvitation.AccountId = acc.Id;
			conInvitation.LastName = 'conInvitation' + String.valueOf(Date.today());
			conInvitation.Questionnaire__c = que11.Id;
			insert conInvitation;
			Campaign c = new Campaign();
			c.Name = 'Test' + String.valueOf(DateTime.now());
			c.Campaign_Type__c = 'Invitation';
			insert c;
			CampaignMember cm = new CampaignMember();
			cm.ContactId = conInvitation.Id;
			cm.CampaignId = c.Id;
			cm.Call_Status__c = 'Waiting Reciprocal Call';
			cm.Status = 'Attended and waiting for back-visit';
			insert cm;
			//test QTask close webservice 
			Q_Task__c qt = [select Id,Status__c,Contact__c,ID__c from Q_Task__c where Contact__c = :conInvitation.Id][0];
			update qt;
			List<Id> List_ids = new List<Id>();
	        List_ids.add(qt.Id);
	        Test.startTest();
	        String result = QTaskWebService.CloseQTask(List_ids);
			Test.stopTest();
		}
	}
}