/*
*用户点击潮媽互動教室按钮时由此Handler处理
*/
public class WechatCallinRoomHandler extends WechatCallinMsgHandler
{
    public override void Handle(WechatCallinMsgPipelineContext Context)
    {
        string openId = Context.InMsg.FromUserName;
        WechatEntity.OutTextMsg outMsg = WechatEntity.GenerateOutMsg(Context.InMsg , '');
        WechatEntity.InEventMsg inEvent = (WechatEntity.InEventMsg)Context.InMsg;
        if(inEvent.EventKey != this.BindingInfo.Event_Key__c)
        {
            return;
        }
        string re = WechatBusinessUtility.CheckWechatUserRelationshipByFields(openId);
        outMsg.Content = WechatBusinessUtility.QueryRemindingByKey('RN041');
        Context.OutMsg = outMsg;
    }
}