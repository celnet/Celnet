/*
 *Author:Michael
 *Date:20140924
 *Function:Utility Tools Class
*/
public class WechatBusinessUtility 
{
    public static string WECHAT_SITE;
    public static string Env;
    static
    {
    	//boolean IsSandbox = [Select o.IsSandbox From Organization o][0].IsSandbox;
    	string baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
    	boolean IsSandbox = baseUrl.contains('cs');
    	if(IsSandbox)
    	{
    		Env = 'SandBox';
    		WECHAT_SITE = 'https://wechat-mjn.cs6.force.com/';
    	}
    	else
    	{
    		Env = 'Production';
    		WECHAT_SITE = 'https://mjn.secure.force.com/hkwechat/';
    	}
    }
    //Check wechat user is member or nonmember by openid
    public static string CheckWechatUserRelationshipByFields(string openId)
    {
        List<Wechat_User__c> wuList = [Select Id, Binding_Member__c , Binding_Non_Member__c From Wechat_User__c where Open_Id__c = :openId];
        if(wuList.isEmpty()) return null;
        else
        {
            if(wuList[0].Binding_Member__c != null)
            {
                return 'Member';
            }
            else if(wuList[0].Binding_Non_Member__c != null)
            {
                return 'Non-Member';
            }
            else
            {
                return null;
            }
        }
    }
    //query baby count & certificate count by openid
    public static BirthCount GetBirthCertificateCount(string openId)
    {
        List<Wechat_User__c> wuList = [select Binding_Non_Member__c from Wechat_User__c where Open_Id__c = :openId];
        Wechat_User__c wu = wuList[0];
        List<Lead> leadList = [select Birthday_1st_Child__c, Birthday_2nd_Child__c, Birthday_3rd_Child__c, 
                               Birthday_4th_Child__c, Attachment_Count__c, Baby_Count__c 
                               from Lead 
                               where id = :wu.Binding_Non_Member__c];
        Lead l = leadList[0];
        BirthCount bc = new BirthCount();
        bc.birthCertificateCount = decimal.valueof(null != l.Baby_Count__c ? l.Baby_Count__c : '0');
        bc.hasBirthCount = (null!=l.Attachment_Count__c ?l.Attachment_Count__c : 0);
        return bc;
    }
    //Baby count & birthCeritficate count
    public class BirthCount
    {
        public decimal birthCertificateCount{get;set;}
        public decimal hasBirthCount{get;set;}
    }
    //query reminding message by reminding key
    private static Wechat_Reminding__c QueryReminding(string RemindingKey)
    {
    	list<Wechat_Reminding__c> remindingList = [select Reminding_Content__c,Dynamic_Keyword__c from Wechat_Reminding__c where Reminding_Key__c = : RemindingKey and Active__c = true];
    	if(!remindingList.isempty())
    	{
    		if(remindingList[0].Reminding_Content__c.contains('\r'))
	    	{
	    		remindingList[0].Reminding_Content__c = remindingList[0].Reminding_Content__c.replace('\r','');
	    	}
    		return remindingList[0];
    	}
    	return null;
    }
    //query reminding message by reminding key
    public static string QueryRemindingByKey(string RemindingKey)
    {
    	Wechat_Reminding__c reminding = QueryReminding(RemindingKey);
		return reminding==null?'':reminding.Reminding_Content__c;
    }
    //query and replace single dynamic keyword by reminding key
    public static string QueryRemindingByKey(string RemindingKey,object ReplaceKeyword)
    {
    	string remindingMsg = '';
    	Wechat_Reminding__c reminding = QueryReminding(RemindingKey);
    	if(reminding != null)
    	{
    		remindingMsg = reminding.Reminding_Content__c.replace(reminding.Dynamic_Keyword__c,string.valueof(ReplaceKeyword));
    	}
    	return remindingMsg;
    }
    //query and replace multi dynamic keyword by reminding key
    public static string QueryRemindingByKey(string RemindingKey, list<object> ReplaceKeywords)
    {
    	string remindingMsg = '';
    	Wechat_Reminding__c reminding = QueryReminding(RemindingKey);
    	if(reminding != null)
    	{
    		list<string> dynamicKeywords = reminding.Dynamic_Keyword__c.split(',');
    		remindingMsg = reminding.Reminding_Content__c;
    		for(integer i = 0; i< dynamicKeywords.size(); i++)
    		{
    			remindingMsg = remindingMsg.replace(dynamicKeywords[i],string.valueof(ReplaceKeywords[i]));
    		}
    	}
    	return remindingMsg;
    }
}