@isTest
private class WechatMemberRegisterPageControllerTest 
{
    static testMethod void myUnitTest() 
    {
        system.runAs(TestUtility.new_HK_User())
        {
            Wechat_User__c wu = new Wechat_User__c();
            wu.Name = 'testWechatUser';
            wu.Open_Id__c = '125457541254145112';
            insert wu;
            Apexpages.currentPage().getParameters().put('OpenId',wu.Open_Id__c);
            Apexpages.currentPage().getParameters().put('Phone','123456798');
            WechatMemberRegisterPageController wpc = new WechatMemberRegisterPageController();
            wpc.LastName = 'testLead';
            wpc.Register();
            wpc.LastName = null;
            try
            {
                wpc.Register();
            }
            catch(exception e){}
        }
    }
}