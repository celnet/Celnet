/*
 *Author: leo.bi@celnet.com.cn 
 *Created On: 2014-05-21
 *Function: Test Case of IntegrationLogEventHandler
 *Apply To: CN 
 */
@isTest
private class IntegrationLogEventHandlerTest 
{
    static testMethod void myUnitTest() 
    {
        system.runas(TestUtility.new_CN_User())
        {
	        //new Account
			Account acc = new Account();
			acc.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_HouseHold' and SobjectType=:'Account'].id;
			acc.Name = 'HouseHold';
			acc.Last_Name__c ='HouseHold';
			insert acc;
			Questionnaire__c greet1 = new Questionnaire__c();
			greet1.Name = 'greet1';
			greet1.Start_age__c = null;
			greet1.End_age__c = 10;
			greet1.Type__c = 'Routine';
			insert greet1;
	        //new Contact
	        Contact con = new Contact();
			con.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
			con.AccountId = acc.Id;
			con.LastName = 'conLast';
			con.Birthdate = Date.today().addDays(-5);
			con.Register_Date__c = Date.today().addDays(-4);
			con.Primary_RecruitChannel_Type__c = 'Hospital';
			con.Primary_RecruitChannel_Sub_type__c = 'PCBaby';
			con.Questionnaire__c = greet1.Id;
			insert con;
			//Cic
			Task t = new Task();
			t.CallObject = 'CallObject';
			t.WhatId = acc.Id;
			t.CallObject= 'Contact';
			insert t;
			//log
			Task ta = new Task();
			ta.WhatId = acc.Id;
			ta.WhoId = con.Id;
			insert ta;	
        }
    }
}