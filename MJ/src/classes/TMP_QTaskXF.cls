global class TMP_QTaskXF implements Database.Batchable<sObject>{
	public String query;
	public String Flag;
	Map<String,Group> Map_Queue = new Map<String,Group>();
	Map<String,Id> Map_GZ = new Map<String,Id>();
Map<String,Id> Map_SH = new Map<String,Id>();
Map<String,Id> Map_BJ = new Map<String,Id>();
	global TMP_QTaskXF() 
	{
		/*
		for(Group Gr : [Select Type,Name,Id,DeveloperName From Group where Type='Queue' ])
		{
			Map_Queue.put(Gr.DeveloperName,Gr);
		}*/
		for(User us : [SELECT Id,Employee_Number_History__c, IsActive, Alias, FirstName, Name, Region__c, Department FROM User where Employee_Number_History__c != null])
		{
			if(us.Department =='广州')
			{
				Map_GZ.put(us.Employee_Number_History__c,us.Id);
			}
			else if(us.Department =='上海')
			{ 
				Map_SH.put(us.Employee_Number_History__c,us.Id);
			}
			else if(us.Department =='北京')
			{
				Map_BJ.put(us.Employee_Number_History__c,us.Id);
			}
		}
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) 
	{
		//String query = 'Select Id from Q_Task__c where Status__c in('+status+') or Contact__c=null';
		Date myDate = date.newinstance(2014, 7, 31);
		String query = 'select OwnerId,TS_History__c,LastModifiedDate,Contact__r.Account.AMRegion_Hidden__c from Answer__c where TS_History__c!=null and tt__c ='+String.valueOf(myDate);
		return Database.getQueryLocator(query);
	}
	//global void execute(Database.BatchableContext BC, List<Answer__c> ans) 
	global void execute(Database.BatchableContext BC, List<Answer__c> AnswerS) 
	{
		List<Answer__c> listUp = new List<Answer__c>();
		for(Answer__c ans:Answers)
		{
			if(ans.Contact__r.Account.AMRegion_Hidden__c.contains('华南') || ans.Contact__r.Account.AMRegion_Hidden__c.contains('华西') )
			{
				if(Map_GZ.containsKey(ans.TS_History__c))
				{
					ans.OwnerId = Map_GZ.get(ans.TS_History__c); 
					listUp.add(ans);
				}
			}
			else if(ans.Contact__r.Account.AMRegion_Hidden__c.contains('华北') )
			{
				if(Map_BJ.containsKey(ans.TS_History__c))
				{
					ans.OwnerId = Map_BJ.get(ans.TS_History__c);
					listUp.add(ans);
				}
			}
			else if(ans.Contact__r.Account.AMRegion_Hidden__c.contains('华东') )
			{
				if(Map_SH.containsKey(ans.TS_History__c))
				{
					ans.OwnerId = Map_SH.get(ans.TS_History__c);
					listUp.add(ans);
				}
			}
		}
		System.debug('###############up '+listUp.size());
		update listUp;
		
		//(Select Questionnaire__c From Answers__r) 
		/*Set<Id> conids = new Set<Id>();
		for(Contact con : ContactS)
		{
			IF(con.Answers__r != null && con.Answers__r.size()>0)
			conids.add(con.Id);
		}
		*/
		
		
		/*
		for(Answer__c an : ans)
		{
			if(an.Questionnaire__c == an.Contact__r.Questionnaire__c)
			{
				conids.add(an.Contact__c);
			}
		}
		*/
		
		
		/*
		for(Answer__c an : ans)
		{
			if(an.Questionnaire__r.Business_Type__c =='Routine Greet' && an.Contact__r.Questionnaire__r.Business_Type__c =='Routine Greet')
			{
				conids.add(an.Contact__c);
			}
		}
		
		
		List<Q_Task__c> upss = new List<Q_Task__c>();
		for(Q_Task__c qt : [select Contact__c,Id,Status__c from Q_Task__c where Contact__c in:conids and Status__c != 'Closed'])
		{
			qt.Status__c =  'Closed';
			qt.ID__c = qt.Contact__c + '-'+qt.Status__c+String.valueOf(DateTime.now());
			upss.add(qt);
		}
		update upss;
		*/
		/*List<Q_Task__c> upss = new List<Q_Task__c>();
		for(Q_Task__c qt : ans)
		{
//System.debug('##############  '+qt.Contact__c);
			qt.Status__c =  'Closed';
			qt.ID__c = qt.Contact__c + '-'+qt.Status__c+String.valueOf(DateTime.now());
			upss.add(qt);
		}
//System.debug('#################3    '+upss.size());
		update upss;*/
		/*
		List<Q_Task__c> List_Up = new List<Q_Task__c>();
		for(Q_Task__c qtask : ans)
		{
			Group queue = this.GetQueue(qtask.Region__c,qtask.Type__c,qtask.Business_Type__c);
			qtask.OwnerId = queue.Id;
			qtask.Original_Queue__c = queue.Id;
			List_Up.add(qtask);
		}
		if(List_Up.size()>0)
		update List_Up;
		*/
		/*
		List<Q_Task__c> List_Up = new List<Q_Task__c>();
		for(Q_Task__c qtask : ans)
		{
			if(qtask.Contact__r.Appended_Log__c.contains('Message: Has Answer,Wait for Next Evaluation') || qtask.Contact__r.Appended_Log__c.contains('Match Questionnaire Failed'))
			
			qtask.Status__c =  'Closed';
			qtask.ID__c = qtask.Contact__c + '-'+qtask.Status__c+String.valueOf(DateTime.now()); 
			List_Up.add(qtask);
		}
		if(List_Up.size()>0)
		update List_Up;
		*/
		
	}
	/*
	private Group GetQueue(String Region,String Type,String BusinessType)
	{
		Group Rtu = (Map_Queue.containsKey('CN_Undefined')?Map_Queue.get('CN_Undefined'):null);
		String RegionFlag;
		if(Region == '华东区'){RegionFlag = 'EC';}
		else if(Region == '华南区'){RegionFlag = 'SC';}
		else if(Region == '华西区'){RegionFlag = 'WC';}
		else if(Region == '华北区'){RegionFlag = 'NC';}
		
		String QueueName;
		if(Type == 'Routine' && BusinessType == 'Routine Greet'){QueueName = 'CN_Routine_A_Greet';}
		else if(Type == 'Routine' && BusinessType == 'Routine Reciprocal'){QueueName ='CN_Routine_A_Reciprocal';}
		else if(Type == 'Routine' && BusinessType == 'Routine Distribution'){QueueName ='CN_Routine_Distribution';}
		else if(Type == 'Invitation' && (BusinessType == 'Invitation' || BusinessType == 'Invitation Reciprocal')){QueueName ='CN_Invitation';}
		else if(Type == 'Invitation' && BusinessType == 'Invitation Special'){QueueName ='CN_Special';}
		else if(Type == 'Elite' ){QueueName ='CN_Elite';}
		
		String QueueDevName = QueueName+'_'+RegionFlag;
		Rtu = (Map_Queue.containsKey(QueueDevName)?Map_Queue.get(QueueDevName):Rtu);
		
		return Rtu;
	}*/
	global void finish(Database.BatchableContext BC)
	{
    	
  	}
}