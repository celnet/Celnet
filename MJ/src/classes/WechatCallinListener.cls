/*
*Author:michael.mi@celnet.com.cn
*Date:2014-09-04
*Function:WechatCallinListener
*所有由微信服务器发送的请求都会先进入到当前Listener来分发Handler来处理
*/
@RestResource(urlMapping='/Wechat/*')
global class WechatCallinListener 
{
	@HttpGet
	global static long CheckSignNature()
	{
		RestRequest req = RestContext.request;
		long echostr = long.valueOf(req.params.get('echostr'));
		if(Authentiate(GetPublicAccountName(req)))
		{
			return echostr;
		}
    	return null;
	}
	
	@HttpPost
	global static void ReceiveCallinMsg()
	{
		RestRequest req = RestContext.request;
		string publicAccountName = GetPublicAccountName(req);
		if(!Authentiate(publicAccountName))
		{
			return;
		}
     	String strBody = req.requestBody.toString();
     	system.debug('***********callinBody*********' + strBody);
     	wechatEntity wechat = new wechatEntity();
     	wechatEntity.CallinBaseMsg inMsg = wechat.DeserializeCallInRequestMsg(strBody);
		WechatCallinMsgPipeline pipline = new WechatCallinMsgPipeline(inMsg,publicAccountName);
		pipline.Execute();
		wechatEntity.CallinBaseMsg outMsg = pipline.OutMsg;
		if(outMsg == null)   
		{
			return;
		}
		
		system.debug('-------outMsg--**---' + outMsg);
		string outMsgStr = wechat.SerializeCallinResponseMsg(outMsg);
		system.debug('outMsgStr-------------'+outMsgStr);
		RestResponse res = RestContext.response;
        res.addHeader('Content-Type','text/xml');
        res.responseBody = Blob.valueOf(outMsgStr);
	}
	
	//Validate message source 
	private static boolean Authentiate(string publicAccountName)
	{
		list<string> signData = new list<string>();
    	string signature = RestContext.request.params.get('signature');
    	string timestamp = RestContext.request.params.get('timestamp');
    	string nonce = RestContext.request.params.get('nonce');
    	system.debug('*****signature*****' + signature);
    	system.debug('******timestamp****' + timestamp);
    	system.debug('*****nonce*****' + nonce);
    	system.debug('*****requestURI*****' + RestContext.request.requestURI);
    	string token = Wechat_Setting__c.getvalues(publicAccountName).Token__c;
    	signData.add(token);
    	signData.add(timestamp);
    	signData.add(nonce);
    	signData.sort();
    	string validateStr = signData[0] + signData[1] + signData[2];
    	Blob hash = Crypto.generateDigest('SHA1',Blob.valueOf(validateStr));
		String validateSignature = EncodingUtil.convertToHex(hash);
		
		if(validateSignature == signature)
        {
        	return true;
        }
        return false;
	}
	
	//Get PublicAccountName
	private static string GetPublicAccountName(RestRequest req)
	{
		integer index = req.RequestURI.lastindexof('Wechat/');
		if(index == -1)
		{
			return ''; //TODO 未来可能要处理异常情况，无效的URL
		}
		string publicAccountName = req.RequestURI.substring(index + 7);
		if(publicAccountName == null)
		{
			return ''; //TODO 未来可能要处理异常情况，无法识别微信公众账户名称
		}
		return  publicAccountName;
	}
}