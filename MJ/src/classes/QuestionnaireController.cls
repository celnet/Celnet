/*
Author: scott.zhang@celnet.com.cn
Created On: 2014-04-16
Function:
Apply To: CN
*/
public class QuestionnaireController {
	public Id QuestionnaireId;//Questionnaire Id
	public Id ContactId;//Contact Id
	public Id MemberId;//Campaign Member Id
	public Id AnswerId;//Answer Id
	public Id LeadId;//Lead Id 
	public String isInConsole{get;set;}//in console
	public String isSecondInvitation{get;set;}//Second Invitation
	public List<ObjQuestionnaire> List_Questionnaire{get;set;}//Question List
	public Map<String, Schema.SobjectField> Answerfields{get;set;}//Answer Fields
	public Map<String,String> MapControlQuestion{get;set;}//key:Question_NO  value: field api
	public Answer__c Answer{get;set;}
	//Campaign
	public list<SelectOption> QuestionnaireTypelist{get;set;}
	public Id SelectQuestionnaireId{get;set;}
	public CampaignMember Member{get;set;}
	public Boolean IsInvitation{get;set;}
	public Boolean IsDisabled{get;set;}
	public String QuestionName{get;set;}
	public Boolean IsShowButton{get;set;}
	public QuestionnaireController(ApexPages.StandardController controller)
	{
		try
		{
			IsShowButton = true; 
			this.isInConsole = ApexPages.currentPage().getParameters().get('isInConsole');
			this.QuestionnaireId = ApexPages.currentPage().getParameters().get('id');//问卷Id QuestionnaireId
			this.ContactId = ApexPages.currentPage().getParameters().get('contactid');//联系人 Contact Id
			this.MemberId = ApexPages.currentPage().getParameters().get('memberid');//活动成员 Campaign Member Id
			this.AnswerId = ApexPages.currentPage().getParameters().get('answerid');//答案Answer id
			this.LeadId = ApexPages.currentPage().getParameters().get('leadid');//Lead id
			this.isSecondInvitation = ApexPages.currentPage().getParameters().get('isSecondInvitation');//Second Invitation
			
			//Review Questionnaire
			if(QuestionnaireId != null && ContactId == null && AnswerId==null && MemberId==null && LeadId==null)
			{
				IsShowButton = false;
				this.PageInitialization(QuestionnaireId,AnswerId);
			}
			//Routine Questionnaire
			else if(QuestionnaireId != null && ContactId != null)
			{
				List<Answer__c> List_Ans = [select Id from Answer__c where Contact__c=:ContactId and Questionnaire__c=:QuestionnaireId];
				if(List_Ans != null && List_Ans.size()>0)
				{
					this.AnswerId = List_Ans[0].Id;
				}
				this.PageInitialization(QuestionnaireId,AnswerId);
			}
			//Second Invitation
			else if(isSecondInvitation != null)
			{
				IsInvitation = true;
				this.Member = [select Id,Campaign.Name,Campaign.Questionnaire_Type__c,Call_Status__c,ContactId,CampaignId,Campaign_Type__c,Campaign.RecordType.DeveloperName from CampaignMember where id =:MemberId];
				QuestionName = Member.Campaign.Name;
				QuestionnaireTypelist = new list<SelectOption>();
				for(Questionnaire__c questionnaire : [select Id,Name from Questionnaire__c where Type__c ='Other' and Business_Type__c='Second Invitation' and Invitation_Type__c=:Member.Campaign.Questionnaire_Type__c])
				{
					if(SelectQuestionnaireId==null)
					SelectQuestionnaireId = questionnaire.Id;
					QuestionnaireTypelist.add(new SelectOption(questionnaire.Id,questionnaire.Name));
				}
				if(QuestionnaireTypelist.size()==0)
				{
					IsInvitation = false;
					ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, '无法确认问卷内容，请确认后再操作！');    		
    				ApexPages.addMessage(msg);
    				return;
				}
				this.showItem();
			}
			//Invitation Questionnaire 
			else if(MemberId != null)
			{
				IsInvitation = true;
				this.Member = [select Id,Campaign.Name,Campaign.Questionnaire_Type__c,Call_Status__c,ContactId,CampaignId,Campaign_Type__c,Campaign.RecordType.DeveloperName from CampaignMember where id =:MemberId];
				QuestionName = Member.Campaign.Name;
				String BusinessType;
				if(Member.Campaign.RecordType.DeveloperName =='CN_Invitation')
				{
					if(Member.Call_Status__c=='Waiting Invitation')
					{
						BusinessType='Invitation';
					}
					else if(Member.Call_Status__c=='Waiting Reciprocal Call')
					{
						BusinessType = 'Invitation Reciprocal';
					}
				}
				else if(Member.Campaign.RecordType.DeveloperName  =='CN_Invitation_Special')
				{
					BusinessType = 'Invitation Special';
				}
				
				QuestionnaireTypelist = new list<SelectOption>();
				for(Questionnaire__c questionnaire : [select Id,Name from Questionnaire__c where Type__c ='Invitation' and Business_Type__c=:BusinessType
													 and (Invitation_Type__c=:Member.Campaign.Questionnaire_Type__c or Invitation_Type__c=null)])
				{
					if(SelectQuestionnaireId==null)
					SelectQuestionnaireId = questionnaire.Id;
					QuestionnaireTypelist.add(new SelectOption(questionnaire.Id,questionnaire.Name));
				}
				if(QuestionnaireTypelist.size()==0)
				{
					IsInvitation = false;
					ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, '无法确认问卷内容，请确认后再操作！');    		
    				ApexPages.addMessage(msg);
    				return;
				}
				this.showItem();
			}
			else if(QuestionnaireId != null && LeadId != null)
			{
				List<Answer__c> List_Ans = [select Id from Answer__c where Lead__c=:LeadId and Questionnaire__c=:QuestionnaireId];
				if(List_Ans != null && List_Ans.size()>0)
				{
					this.AnswerId = List_Ans[0].Id;                  
				}
				this.PageInitialization(QuestionnaireId,AnswerId);
			}    
			//Edit Answer   
			else if(QuestionnaireId != null && AnswerId != null)
			{
				this.PageInitialization(QuestionnaireId,AnswerId);
			}
			else
			{
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, '无法确认问卷内容，请确认后再操作！');    		
    			ApexPages.addMessage(msg);
    			return;
			}
		}catch(Exception e)
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, 'Error:'+e.getmessage()+' LineNumber:'+e.getLineNumber());    		
    		ApexPages.addMessage(msg);
    		return; 
		}
		
	}
	/*页面初始化 initialization; Arguments：AnswerId*/
	public void PageInitialization(Id QuestionnaireId,Id AnswerId)
	{
		List_Questionnaire = new List<ObjQuestionnaire>();
		Answer = new Answer__c();
		MapControlQuestion = new Map<String,String> ();
		Answerfields = Schema.SobjectType.Answer__c.fields.getMap(); //Answer Fields
		//Get Answer Information
		if(AnswerId != null)
		{
			this.Answer = this.getAnswer(Answerid); 
			if(Answer.Status__c=='Finish')
			{
				IsDisabled = true;
			}
		}
		
		for(Questionnaire_Line_Item__c Item : [select Name,Questionnaire__r.Name,Question__r.Name,Question__r.Question_Detail__c,Question__r.Target_Answer_Field__c,Question__r.Target_Answer_Additional_Text_Field__c,Question_NO__c,Question__r.Answer_Type__c,Has_Remark__c,Order__c,
												  Control_Question_NO__c,Control_Question_Value__c,Control_Type__c,Required__c from Questionnaire_Line_Item__c where Questionnaire__c =:QuestionnaireId order by Order__c])
			{
				if(QuestionName==null)
				{
					QuestionName = Item.Questionnaire__r.Name;
				}
				ObjQuestionnaire Questionnaire = new ObjQuestionnaire();
				Questionnaire.QuestionName = (Item.Question__r.Question_Detail__c != null ? Item.Question__r.Question_Detail__c : Item.Question__r.Name);
				if(Questionnaire.QuestionName.contains('-')) 
				{
					Questionnaire.QuestionName =Item.Order__c+'-'+(Questionnaire.QuestionName.split('-').size()>=3?Questionnaire.QuestionName.split('-')[1]+'-'+Questionnaire.QuestionName.split('-')[2]:Questionnaire.QuestionName.split('-')[1]) ;
				}
				
				Questionnaire.AnswerType = Item.Question__r.Answer_Type__c;
				//Has Remark
				Questionnaire.Has_Remark = Item.Has_Remark__c;
				//Required__c
				Questionnaire.Required = Item.Required__c;
				//Control Question
				Questionnaire.Question_NO = Item.Question_NO__c;
				Questionnaire.Control_Question_NO = Item.Control_Question_NO__c;
				Questionnaire.Control_Question_Value = Item.Control_Question_Value__c;
				Questionnaire.Control_Type = Item.Control_Type__c;
				//AnswerFieldApi & AnswerFieldValue
				Questionnaire.AnswerFieldApi = Item.Question__r.Target_Answer_Field__c;
				//AdditionalFieldApi & AdditionalFieldValue
				Questionnaire.AdditionalFieldApi = Item.Question__r.Target_Answer_Additional_Text_Field__c;
				Questionnaire.MultiFieldValue = new String[]{};
				if(!Answerfields.containsKey(Questionnaire.AnswerFieldApi))
				{
					ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, '无法在Answer上找到问题  '+Questionnaire.QuestionName+'  对应字段'+Questionnaire.AnswerFieldApi);    		
    				ApexPages.addMessage(msg);
    				return; 
				}
				
				//Edit Answer
				if(AnswerId != null)
				{
					//AnswerFieldApi
					String FieldValue = String.valueOf(Answer.get(Questionnaire.AnswerFieldApi));
					if(Questionnaire.AnswerType == 'Picklist' || Questionnaire.AnswerType == 'Text' || Questionnaire.AnswerType == 'Date')
					{
						Questionnaire.AnswerFieldValue = FieldValue;
					}
					else if(Questionnaire.AnswerType == 'Multi-Picklist')
					{
						Questionnaire.MultiFieldValue = (FieldValue!=null?FieldValue.split(';'):new String[]{});
					}
					
					//AdditionalFieldApi 
					//判断Answer上是否存在此问题对应的字段
					if(Answerfields.containsKey(Questionnaire.AdditionalFieldApi))
					{
						String AdditionalValue = String.valueOf(Answer.get(Questionnaire.AdditionalFieldApi));
						Questionnaire.AdditionalFieldvalue = AdditionalValue;
					}
					else if(Questionnaire.Has_Remark)
					{
						Questionnaire.AdditionalFieldvalue ='在对象Answer中无法找到对应的匹配字段（FieldAPI ='+Questionnaire.AdditionalFieldApi+'），请联系系统管理员。';
					}
				}
				
				Questionnaire.options = new List<SelectOption>();
				if(Questionnaire.AnswerType == 'Picklist' || Questionnaire.AnswerType == 'Multi-Picklist')
				{
					for(Schema.PicklistEntry SP:Answerfields.get(Questionnaire.AnswerFieldApi).getDescribe().getpicklistvalues())
					{
						Questionnaire.options.add(new SelectOption(SP.getvalue(),SP.getLabel()));
					}
				}
				/*//Answer_Type__c =='Boolean'
				else if(Questionnaire.AnswerType == 'Boolean')
				{
					Questionnaire.options.add(new SelectOption('true','是'));
					Questionnaire.options.add(new SelectOption('false','否'));
				}*/
				MapControlQuestion.put(Questionnaire.Question_NO,Questionnaire.AnswerFieldApi);
				List_Questionnaire.add(Questionnaire);
			}
	}
	/*Invitation Questionnaire */
	public void showItem()
	{
		this.QuestionnaireId = SelectQuestionnaireId;
		List<Answer__c> List_Ans = new List<Answer__c>();
		//if(isSecondInvitation != null)
		//{
		//	List_Ans = [select Id,Questionnaire__c from Answer__c where Contact__c=:Member.ContactId and Questionnaire__c=:SelectQuestionnaireId];
		//}
		//else
		//{
			List_Ans = [select Id,Questionnaire__c from Answer__c where Contact__c=:Member.ContactId and Campaign__c =:Member.CampaignId and Questionnaire__c=:SelectQuestionnaireId];
			
		//}
		if(List_Ans != null && List_Ans.size()>0)
		{
			this.AnswerId = List_Ans[0].Id;
			if(MemberId != null)
			this.QuestionnaireId = List_Ans[0].Questionnaire__c;
		}

		this.PageInitialization(QuestionnaireId,AnswerId);
	}
	/*保存*/
	public PageReference SaveAnswer()
	{
		try
		{
			String message=null; 
			for(ObjQuestionnaire obj : List_Questionnaire)
			{
				
				if(ContactId != null && Answer.Contact__c == null)
				{
					Answer.Contact__c = ContactId;
				}
				if(MemberId != null && Answer.Contact__c == null)
				{
					Answer.Contact__c = Member.ContactId;
					Answer.Campaign_Member_Id__c = MemberId;
					Answer.Campaign__c = Member.CampaignId;
				}
				if(LeadId != null && Answer.Lead__c == null)
				{
					Answer.Lead__c = LeadId;
				}
				
				if(obj.AnswerType == 'Multi-Picklist' && obj.MultiFieldValue != null)
				{
					String flag=null;
					for(String str : obj.MultiFieldValue)
					{
						if(str=='')
						continue;
						if(flag==null)
						{
							flag=str;
						} 
						else
						{
							flag=flag+';'+str;
						}
					}
					Answer.put(obj.AnswerFieldApi,flag);
				}
				else if(obj.AnswerType == 'Picklist' || obj.AnswerType == 'Text' || obj.AnswerType == 'Date')
				{
					Answer.put(obj.AnswerFieldApi,obj.AnswerFieldValue);
				}
				/*else if(obj.AnswerType == 'Boolean')
				{
					Answer.put(obj.AnswerFieldApi,(obj.AnswerFieldValue=='true'&&obj.AnswerFieldValue!=''?true:false));
					
				}*/
				//AdditionalValue
				if(Answerfields.containsKey(obj.AdditionalFieldApi))
				Answer.put(obj.AdditionalFieldApi,obj.AdditionalFieldValue);  
				
				Answer.Questionnaire__c = QuestionnaireId;
				
				//Save verification0
				if(Answer.Status__c=='Finish' && obj.AnswerType == 'Date'&& obj.AnswerFieldValue!=null && obj.AnswerFieldValue!=''&&(!Pattern.matches('^[1-2][0-9][0-9][0-9]([0][0-9]|([1][0-2]))([0-2][0-9]|([3][0-1]))$',obj.AnswerFieldValue)))
				{
					if(message==null)
					{
						message ='问题  " '+obj.QuestionName+' " 输入格式错误应填写"YYYYMMDD 例如：20140801"需必填。\n     ';
					}
					else
					{
						message +='问题  " '+obj.QuestionName+' " 输入格式错误应填写"YYYYMMDD 例如：20140801"需必填。\n     ';
					}
				}
				//Save verification1
				if(Answer.Status__c=='Finish' && obj.Required && ((obj.AnswerType == 'Multi-Picklist' && obj.MultiFieldValue.size()==0) || (obj.AnswerType != 'Multi-Picklist' &&(obj.AnswerFieldValue==null || obj.AnswerFieldValue ==''))))
				{
					message = this.RequiredValidation(message,obj);
				}
				
				//Save verification2
				if(Answer.Status__c=='Finish' && obj.Control_Question_NO !=null && obj.Control_Question_Value!=null && obj.Control_Type !=null)
				{
					message = this.ControlQuestionValidation(message,obj);
				}
			}
			
			if(message!=null)
			{
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, message);    		
    			ApexPages.addMessage(msg);
    			return null; 
			}
			
			IF(Answer.Id != null)
			{
				if(Answer.Status__c=='Finish')
				{
					Answer.Finished_On__c =DateTime.now();
				}
				update Answer;
			}
			else
			{
				if(Answer.Status__c=='Finish')
				{
					Answer.Finished_On__c =DateTime.now();
				}
				insert Answer;
			}
			
			if(Answer.Status__c=='Finish')
			{
				if(Answer.Q033__c == 'Y' && Answer.Contact__c != null)
				{
					Id FulfillmentId = this.CreateFulfillment(Answer.Contact__c,'S2');
					String Consoleflag = (isInConsole!=null?'&isdtp=vw':'');
					PageReference pageRef = new PageReference(System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+FulfillmentId+'/e?retURL=%2F'+FulfillmentId+Consoleflag);
					return pageRef;
				}
				else if(Answer.Q034__c == 'Y' && Answer.Contact__c != null)
				{
					Id FulfillmentId = this.CreateFulfillment(Answer.Contact__c,'S3');
					String Consoleflag = (isInConsole!=null?'&isdtp=vw':'');
					PageReference pageRef = new PageReference(System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+FulfillmentId+'/e?retURL=%2F'+FulfillmentId+Consoleflag);
					return pageRef;
				} 
				else if(isInConsole!=null)
				{
					PageReference pageRef = new PageReference(System.URL.getSalesforceBaseUrl().toExternalForm()+'/apex/Questionnaire?id='+this.QuestionnaireId+'&answerid='+Answer.Id+'&isInConsole=isInConsole');
	        		return pageRef;
				}
				else
				{
					PageReference pageRef = new PageReference(System.URL.getSalesforceBaseUrl().toExternalForm()+'/apex/Questionnaire?id='+this.QuestionnaireId+'&answerid='+Answer.Id);
	        		return pageRef;
				}
			}
			else
			{
				return null;
			}
			
		}catch(Exception e)
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, 'Error:'+e.getmessage()+' LineNumber:'+e.getLineNumber());    		
    		ApexPages.addMessage(msg);
    		return null; 
		}
	}
	/*Save verification1 Required*/
	public String RequiredValidation(String message,ObjQuestionnaire obj)
	{
		if(message==null)
		{
			message='问题  " '+obj.QuestionName+' " 需必填。\n     ';
		}
		else
		{
			message+='问题  " '+obj.QuestionName+' " 需必填。\n    ';
		}
		return message;
	}
	/*Save verification2 ControlQuestion*/
	public String ControlQuestionValidation(String message,ObjQuestionnaire obj)
	{
		if(obj.Control_Question_NO.contains(','))
		{
			List<String> List_ControlNO = obj.Control_Question_NO.split(',');
			if(!obj.Control_Question_Value.contains(',') || !obj.Control_Type.contains(',') || obj.Control_Question_Value.split(',').size() != List_ControlNO.size() || obj.Control_Type.split(',').size() != List_ControlNO.size())
			{
				ApexPages.Message msgc = new ApexPages.Message(ApexPages.Severity.WARNING, '"Control Question NO"、"Control Question Value"、"Control Type" 不匹配。');    		
				ApexPages.addMessage(msgc);
				return null; 
			}
			
			List<String> List_ControlValue = obj.Control_Question_Value.split(',');
			List<String> List_ControlType = obj.Control_Type.split(',');
			for(Integer i=0;i<List_ControlNO.size();i++)
			{
				if(MapControlQuestion.containsKey(List_ControlNO[i]) && String.valueOf(Answer.get(MapControlQuestion.get(List_ControlNO[i])))!=null && String.valueOf(Answer.get(MapControlQuestion.get(List_ControlNO[i]))).contains(List_ControlValue[i]))
				{
					if(List_ControlType[i]=='Disabled')
					{
						Answer.put(obj.AnswerFieldApi,(obj.AnswerType == 'Boolean'?false:null));
					}
					else if(List_ControlType[i]=='Required' && obj.AnswerType != 'Boolean' && (Answer.get(obj.AnswerFieldApi)==null || Answer.get(obj.AnswerFieldApi)==''))
					{
						if(message==null)
						{
							message='当问题 ( '+obj.Control_Question_NO+' ) 的值为： " '+obj.Control_Question_Value+' " 时，问题 { '+obj.QuestionName+' } 必填。\n  ';
						}
						else
						{
							message+='当问题 ( '+obj.Control_Question_NO+' ) 的值为： " '+obj.Control_Question_Value+' " 时，问题 { '+obj.QuestionName+' } 必填。\n  ';
						}
					}
				}
			}
		}
		else
		{
			if(MapControlQuestion.containsKey(obj.Control_Question_NO) && String.valueOf(Answer.get(MapControlQuestion.get(obj.Control_Question_NO)))!= null && String.valueOf(Answer.get(MapControlQuestion.get(obj.Control_Question_NO))).contains(obj.Control_Question_Value))
			{
				if(obj.Control_Type=='Disabled')
				{
					Answer.put(obj.AnswerFieldApi,(obj.AnswerType == 'Boolean'?false:null));
				}
				else if(obj.Control_Type=='Required' && obj.AnswerType != 'Boolean' && (Answer.get(obj.AnswerFieldApi)==null || Answer.get(obj.AnswerFieldApi)==''))
				{
					if(message==null)
					{
						message='当问题 ( '+obj.Control_Question_NO+' ) 的值为： " '+obj.Control_Question_Value+' " 时，问题 { '+obj.QuestionName+' } 必填。\n  ';
					}
					else
					{
						message+='当问题 ( '+obj.Control_Question_NO+' ) 的值为： " '+obj.Control_Question_Value+' " 时，问题 { '+obj.QuestionName+' } 必填。\n  ';
					}
				}
			}
		}
		return message;
	}
	/*Answer Information*/
	public Answer__c getAnswer(Id Answerid)
    {
        if(Answerid == null)
        {
            return new Answer__c();
        }  
        String query = 'SELECT ';
        for(String fieldApi : Answerfields.keySet())
        {
        	if(fieldApi=='Id')
        	continue;
        	query += fieldApi + ', ';
        }
        query += 'Id FROM Answer__c Where Id=\''+Answerid+'\' LIMIT 1';
        return Database.query(query);
    }
    public PageReference Cancel()
    {
     	Id ReturnId = QuestionnaireId;
     	if(AnswerId != null)
     	ReturnId = AnswerId;
     	if(ContactId != null)
     	ReturnId = ContactId;
     	if(MemberId != null)
     	ReturnId = MemberId;
     	if(LeadId != null)
     	ReturnId = LeadId;
 		PageReference pageRef = new PageReference(System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+ReturnId);
 		return pageRef;
     	
    }
    /*Create Fulfillment*/
    public Id CreateFulfillment(Id contactId,String SampleName)
    {
     	Contact con = [select Id,Account.Province__c,Account.Standard_City__r.Name,Account.Address_Street__c from Contact where id =:contactId];
     	Fulfillment__c fulfillment = new Fulfillment__c();
     	fulfillment.RecordTypeId = ObjectUtil.GetRecordTypeID(Fulfillment__c.getSObjectType(), 'Fulfillment_CN');
     	fulfillment.Contact__c = contactId;
     	fulfillment.Sample_Name__c = SampleName;
     	fulfillment.Type__c = 'Sample';
     	fulfillment.Remark__c = (con.Account.Province__c!=null?con.Account.Province__c:'')+(con.Account.Standard_City__r.Name!=null?con.Account.Standard_City__r.Name:'')+(con.Account.Address_Street__c!=null?con.Account.Address_Street__c:'');
     	insert fulfillment;
     	return fulfillment.Id;
     	
    } 
	public Class ObjQuestionnaire
	{
		public String QuestionName{get;set;}//Question Name
		public String AnswerType{get;set;}//Answer Type
		public Boolean Has_Remark{get;set;}//Has Remark
		public Boolean Required{get;set;}//Required
		public List<SelectOption> options{get;set;}//Picklist || Check box
		public String Question_NO {get;set;}//Question NO.
		public String Control_Question_NO{get;set;}//Control Question NO
		public String Control_Question_Value{get;set;}//Control Question Value
		public String Control_Type{get;set;} //Control Type
		public String AnswerFieldApi{get;set;}//AnswerFieldApi
		public String[] MultiFieldValue{get;set;}//Multiple value
		public String AnswerFieldvalue{get;set;}//Radio value
		public String AdditionalFieldApi{get;set;}//AdditionalFieldApi
		public String AdditionalFieldvalue{get;set;}//Additional
		public Answer__c Ans{get;set;}//存储时间类型的问题
	}
}