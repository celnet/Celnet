/*Author:leo.bi@celnet.com.cn
 *Date:2014-07-25
 *Function:use salesforce server request IP test website to test server IP Address
 */
public class GetIPAddressController 
{
	public string result{get;set;}
	public string stringIP{get;set;}{result = ' ';}
	public string stringFrom{get;set;}{result = ' ';}
	public string innerIP{get;set;} {result = '';}
	public void run()
	{
		HttpRequest req = new HttpRequest();
		HttpResponse res = new HttpResponse();
		Http http = new Http();
		req.setEndpoint('http://www.ip.cn');
		req.setMethod('GET');
		res = http.send(req);
		result = res.getBody();
		stringIP = result.substringBetween('当前 IP：<code>', '</code>&nbsp;来自');
		stringFrom = result.substringBetween('</code>&nbsp;来自：', '</p><p>GeoIP:');
		innerIP = result.substringBetween('</p><p>内部 IP: <code>', '</code>&nbsp;来自');
	} 
}