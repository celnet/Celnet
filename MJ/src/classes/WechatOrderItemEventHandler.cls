/*
*Author:michael.mi@celnet.com.cn
*Date:20141011
*Function:用户通过微信下单后，推送一条下单成功的消息给到微信用户
*/
public class WechatOrderItemEventHandler implements Triggers.Handler
{
	public void Handle()
    {
    	if(!Context.ApplyTo(new String[] {'HK'}))
        {
            return;
        }
        if(trigger.isAfter &&  trigger.isInsert)
        {
            if(trigger.new[0] instanceof Order_Item__c)
            {
            	autoNotifyWechatUser();
            }
        }
    }
    //auto notify wechat user
    private void autoNotifyWechatUser()
    {
    	list<Order_Item__c> orders = trigger.new;
    	set<id> oids = new set<id>();
    	set<id> accIds = new set<id>();
    	for(Order_Item__c o : orders)
    	{
    		if(o.channel__c != 'Wechat')
    		{
    			continue;
    		}
    		oids.add(o.id);  
    		accIds.add(o.HouseHold__c);
    	}
    	map<id,Account> accMap = new map<id,Account>([select id,(select Open_Id__c,Public_Account_Name__c from Wechat_Users__r) 
    												  from Account where id in : accIds]);
    	list<Wechat_Callout_Task_Queue__c> outNotifyQueue = new list<Wechat_Callout_Task_Queue__c>();
    	for(Order_Item__c o : [select id,HouseHold__c,Order__r.Name,Order__r.Address_Flat_Floor_Block_Building__c,Order__r.Address_Street_District_Territory__c,
    					  	   Order__r.Address_Extra_Line__c,Order__r.Expected_Delivery_Date__c,Order__r.Mobile_Phone__c,Unit_Price__c,Product__r.Product_Chinese_SKU__c,
    					 	   Product_Chinese_Name__c,Quantity__c
    					 	   from Order_Item__c where id in :oids])
    	{
    		if(accMap.get(o.HouseHold__c).Wechat_Users__r.isempty())
    		{
    			continue;
    		}
    		string OrderNo = o.Order__r.Name;
    		string Address = o.Order__r.Address_Flat_Floor_Block_Building__c + ',' + o.Order__r.Address_Street_District_Territory__c + ',' + o.Order__r.Address_Extra_Line__c;
    		string Product = o.Product_Chinese_Name__c + '【' + o.Product__r.Product_Chinese_SKU__c + '】';
    		decimal Volume = o.Quantity__c;
    		string DeliveryDate = string.valueof(o.Order__r.Expected_Delivery_Date__c);
    		string Mobile = o.Order__r.Mobile_Phone__c;
    		string totalAmount = 'HK$' + o.Unit_Price__c + 'X' + o.Quantity__c + '=HK$' + (o.Unit_Price__c * o.Quantity__c);
    		list<object> ReplaceKeywords = new list<object>();
    		ReplaceKeywords.add(OrderNo);
	    	ReplaceKeywords.add(Address);
	    	ReplaceKeywords.add(Product);
	    	ReplaceKeywords.add(Volume);
	    	ReplaceKeywords.add(totalAmount);
	    	ReplaceKeywords.add(DeliveryDate);
	    	ReplaceKeywords.add(Mobile);
	    	string notifyMsg = WechatBusinessUtility.QueryRemindingByKey('RN033',ReplaceKeywords);
	    	string textBody = WechatEntity.GenerateSvcTextMsg(accMap.get(o.HouseHold__c).Wechat_Users__r[0].Open_Id__c,notifyMsg);
	    	Wechat_Callout_Task_Queue__c textQueue = WechatCalloutQueueManager.CreateSendSvcTextMsgTask(accMap.get(o.HouseHold__c).Wechat_Users__r[0].Public_Account_Name__c,textBody,'WechatCallOutSendTextMsgProcessor');
			outNotifyQueue.add(textQueue);
    	}
    	WechatCalloutQueueManager.EnQueue(outNotifyQueue);
    }
}