/*
Author: shiqi.ng@sg.fujitsu.com
Schedulable Class for Staging 
   2.2 《会员招募统计》 
*/

global class ScheduleStagingRecruitment implements Schedulable {
  	Integer thisDay;
    Integer thisMonth;
    Integer thisYear;
    boolean adHoc;
    
    /**
    Default contructor: Reporting Date = yesterday, since daily batch runs after midnight
    */
    public ScheduleStagingRecruitment (){
      	thisDay = null;
    	thisMonth = null;
        thisYear = null;
        adHoc = false;
    }
    
     
    /**
    Overload constructor with custom date parameter for ad-hoc runs!
    */
    public ScheduleStagingRecruitment (Integer year, Integer month, Integer day){
        thisDay = day;
        thisMonth = month;
        thisYear = year;
		adHoc = true;
    }
    global void execute(SchedulableContext SC) {
        if (adHoc == false){
            Date yesterday = Date.today().addDays(-1);
            thisDay = yesterday.day();
            thisMonth =  yesterday.month();
            thisYear = yesterday.year();
        }
        String specificCodeQuery = 'Select Contact__r.Specific_Code__c from Recruitment_Channel__c ';
        specificCodeQuery += ' Where Contact__r.Specific_Code__c != null '; 
        specificCodeQuery += ' And CALENDAR_YEAR(CreatedDate)  = '+thisYear;
        specificCodeQuery += ' And CALENDAR_MONTH(CreatedDate)  = '+thisMonth;
        specificCodeQuery += ' And DAY_IN_MONTH(CreatedDate)  = '+thisDay;
        specificCodeQuery += ' And Contact__r.RecordType.DeveloperName=\'CN_Child\'';
        specificCodeQuery += ' Group by Contact__r.Specific_Code__c';
    
        BatchBuildStagingRecruitment recruit = new BatchBuildStagingRecruitment(
          specificCodeQuery, thisYear, thisMonth, thisDay
        );
        Database.executeBatch(recruit,10);
    }
}