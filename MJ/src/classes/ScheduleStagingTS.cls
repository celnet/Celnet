/*Schedulable Class for Staging Table 1.1 《TS工作量统计分析报表》 and 1.2 《TS(GC)工作量统计分析报表》*/

global class ScheduleStagingTS implements Schedulable {
    Integer thisDay;
    Integer thisMonth;
    Integer thisYear;
    boolean adHoc;
    
    /**
    Default contructor: Reporting Date = yesterday, since daily batch runs after midnight
    */
    public ScheduleStagingTS(){
        thisDay = null;
    	thisMonth = null;
        thisYear = null;
        adHoc = false;
    }
    
    /**
    Overload constructor with custom date parameter for ad-hoc runs!
    */
    public ScheduleStagingTS(Integer year, Integer month, Integer day){
        thisDay = day;
        thisMonth = month;
        thisYear = year;
        adHoc = true;
    }
    
    global void execute(SchedulableContext SC) {
        if (adHoc == false){
            Date yesterday = Date.today().addDays(-1);
            thisDay = yesterday.day();
            thisMonth =  yesterday.month();
            thisYear = yesterday.year();
        }
        
        //for Report 1.1 《TS工作量统计分析报表》
        String userQuery = 'Select Id, Division, Department, EmployeeNumber, Name ';
        userQuery += ' From User where Profile.Name Like \'CN_Inbound%\' OR Profile.Name Like \'CN_Outbound%\'';
        BatchBuildStagingTSWorkload ts = new BatchBuildStagingTSWorkload(
            userQuery, thisYear, thisMonth, thisDay
        );
        if (!Test.isRunningTest()){
            Database.executeBatch(ts,10);
        }
        
        //for Report 1.2 《TS(GC)工作量统计分析报表》
        String cityQuery = 'SELECT Sales_City__c, Region__c FROM Address_Management__c Group By Region__c,Sales_City__c';
        //13号问卷
        BatchBuildStagingTSGCWorkload tsgc13 = new BatchBuildStagingTSGCWorkload(
            cityQuery, thisYear, thisMonth, thisDay,13
        );
        //14号问卷
        BatchBuildStagingTSGCWorkload tsgc14 = new BatchBuildStagingTSGCWorkload(
            cityQuery, thisYear, thisMonth, thisDay,14
        );
        if (!Test.isRunningTest()){
            Database.executeBatch(tsgc13,1);
            Database.executeBatch(tsgc14,1);
        }
    }
}