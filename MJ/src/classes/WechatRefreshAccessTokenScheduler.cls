/*Author:leo.bi@celnet.com.cn
 *Date:2014-09-10
 *Function:Refresh Wechat access token every 1 hour.
 *Code: System.schedule('Wechat Refresh AccessToken','0 40 0/1 * * ?', new WechatRefreshAccessTokenScheduler());
 *WechatRefreshAccessTokenScheduler.callOut('CideatechSFWeiXin');
 *WechatRefreshAccessTokenScheduler.RefreshToken();
 */
global class WechatRefreshAccessTokenScheduler implements Schedulable
{
    global void execute(SchedulableContext sc)  
    {
		Map<String, Wechat_Setting__c> map_WS = Wechat_Setting__c.getAll();
        if(map_WS.isEmpty())
        {
            return;
        }   
        for(String s : map_WS.keySet())
        {
            Wechat_Setting__c ws = map_WS.get(s);
            if(!ws.Active__c)
            {
            	continue;
            }
            if(!test.isRunningTest())
            {   
            	callOut(ws.Name);
            }
        } 
    }
    
    public static void RefreshToken(string publicAccountName)
    {
    	WechatCalloutService wcs = new WechatCalloutService(publicAccountName);
		WechatCalloutService.AccessToken wat = wcs.RefreshAccessToken();
		Wechat_Setting__c ws = [select Access_Token1__c , Access_Token2__c , Access_Token3__c , id from Wechat_Setting__c where Name = :publicAccountName];
		string accessToken = wat.access_token;
		if(accessToken.length() > 510)
		{
			ws.Access_Token1__c = accessToken.substring(0 , 255);
			ws.Access_Token2__c = accessToken.substring(255 , 510);
			if(accessToken.length() <= 765)
			{
				ws.Access_Token3__c = accessToken.substring(510);
			}
		}
		else if(accessToken.length() > 255 && accessToken.length() <= 510)
		{
			ws.Access_Token1__c = accessToken.substring(0 , 255);
			ws.Access_Token2__c = accessToken.substring(255);
		}
		else if(accessToken.length() <= 255)
		{
			ws.Access_Token1__c = accessToken;
		}
		update ws;
    }
    
    @Future(callout=true)
	public static void callOut(string publicAccountName)
	{
		RefreshToken(publicAccountName);
	}
}