/*
Author: scott.zhang@celnet.com.cn 
Created On: 2014-05-19
Function: 
1.Set one Recruitment channel as primary
Support Event:AfterInsert, AfterUpdate
Apply To: CN 
*/
public class RecruitmentChannelEventHandler implements Triggers.Handler {
	public void Handle()
	{
		if(!Context.ApplyTo(new String[] {'CN'})) 
    	{
    		return;
		}
		if(trigger.isBefore && trigger.isInsert)
		{
			if(trigger.new[0] instanceof Recruitment_Channel__c)
			{
				this.SetRCPrimary();
			}
		}
	}
	
	public void SetRCPrimary()
	{
		List<Recruitment_Channel__c> rcList = trigger.new;
		Set<Id> Set_ConIds = new Set<Id>();
		Map<Id,List<Recruitment_Channel__c>> Map_Rc = new Map<Id,List<Recruitment_Channel__c>>();
		Id RecordTypeId = ObjectUtil.GetRecordTypeID(Recruitment_Channel__c.getSObjectType(), 'Recruitment_Channel_CN');
		for(Recruitment_Channel__c rc : rcList)
		{
			if(rc.RecordTypeId != RecordTypeId || rc.Contact__c == null)
			{
				continue;
			}
			Set_ConIds.add(rc.Contact__c);			
		}
		if(Set_ConIds.size()==0)
		return;
		
		Set<Id> Set_HasPrimary = new Set<Id>();
		for(Contact con : [select Id,(Select Primary__c From Recruitment_Channels__r where Primary__c = true) from Contact where Id in:Set_ConIds])
		{
			for(Recruitment_Channel__c rc : rcList)
			{
				if(rc.RecordTypeId != RecordTypeId || rc.Contact__c != con.Id || con.Recruitment_Channels__r.size()>0)
				{
					continue;
				}
				
				if(Set_HasPrimary.size()== 0 || !Set_HasPrimary.contains(con.Id))
				{
					rc.Primary__c = true;
					Set_HasPrimary.add(con.Id);
				}
			}
		}  
	}
}