/**
 *Test for OrderItemEventHandler
 */
@isTest
private class WechatOrderItemEventHandlerTest 
{
    static testMethod void myUnitTest() 
    {
    	system.runAs(TestUtility.new_HK_User())
        {
        	//Create custom settings
            TestUtility.setupMemberIDCustomSettings();
            Id leadRecordType = [select id from RecordType where DeveloperName=:'LeadsHK' and SobjectType=:'Lead'].id;
            Id accountRecordType = [select id from RecordType where DeveloperName=:'HouseHold' and SobjectType=:'Account'].id;
            Account acc = new Account();
            acc.Name = 'PRIMARY PARENT';
            acc.Last_Name__c = 'PRIMARY';
            acc.First_Name__c = 'PARENT';
            acc.RecordTypeId = accountRecordType;
            insert acc;
            Lead lea = new Lead();
            lea.LastName = 'PARENT';
            lea.FirstName = 'TEST';
            lea.First_Name_1st_Child__c = 'BABY';
            lea.Last_Name_1st_Child__c = 'TEST';
            lea.Birthday_1st_Child__c = system.today();
            lea.RecordTypeId = leadRecordType;
            insert lea;
            Wechat_User__c wu = new Wechat_User__c();
            wu.Open_Id__c = 'leo' + System.now();
            wu.Name = 'leo';
            wu.Binding_Non_Member__c = lea.Id;
            wu.Binding_Member__c = acc.id;
            insert wu;
            Order__c o = new Order__c();
            o.HouseHold__c = acc.id;
            o.Order_Number__c = 'afaf';
            o.Channel__c = 'Wechat';
            o.Expected_Delivery_Date__c = date.today().adddays(3);
            insert o;
            Product__c pro = new Product__c();
            pro.Name = 'pro';
            pro.Product_Code__c = 'code';
            pro.Unit_Price__c = 400;
            insert pro;
            Order_Item__c oi = new Order_Item__c();
            oi.Order__c = o.id;
            oi.Product__c = pro.id;
            oi.Quantity__c = 4;
            insert oi;
        }
    }
}