/*
Batch job to build staging object: TS_GC_Workload_Stats__c
for Report 1.2 《TS(GC)工作量统计分析报表》
key: city, day, month, year
author: shiqi.ng@sg.fujitsu.com 
*/
global class BatchBuildStagingTSGCWorkload  implements Database.Batchable<AggregateResult>{
    
    private String cityQuery;
    private Integer reportingDay;
    private Integer reportingMonth;
    private Integer reportingYear;
    private Integer QuestionnaireNo;//13 or 14
    
    /*Contructor*/
    public  BatchBuildStagingTSGCWorkload(String query,Integer year, Integer month, Integer day,Integer questionNo){
        cityQuery = query;
        reportingDay = day;
        reportingMonth = month;
        reportingYear = year;
        QuestionnaireNo = questionNo;
    }
    /*Start Method*/
    global Iterable<AggregateResult> start(Database.BatchableContext bc){
        return new AggregateResultIterable(cityQuery);
    }
    /*Execute*/
    global void execute(Database.BatchableContext bc, List<SObject> scope){
        Set<String> citySet = new Set<String>();
        Map<String,String> cityRegionMap = new Map<String,String>();
        for (SObject obj : scope){
            AggregateResult ar = (AggregateResult) obj;
            String city = (String)ar.get('Sales_City__c');
            String region = (String)ar.get('Region__c');
            citySet.add(city);
            cityRegionMap.put(city,region);
        }
       
        //Tasks from Questionnaire 13 and 14 only
         List<Task> tasks = StagingReportUtil.queryTodayGCTasksByCity(citySet,reportingYear,reportingMonth,reportingDay);
       /*====2014-7-15注释--Start==
        //Get Contacts into Map, filter success only = false (i.e. all contacts)
        Map<Id,Contact> contactMap = StagingReportUtil.queryContactsFromTasks(tasks,false);
        
        //Get Contacts into Map, filter success only = true
        Map<Id,Contact> contactSuccessMap = StagingReportUtil.queryContactsFromTasks(tasks,true);
        ====2014-7-15注释-End==*/
        
        /*===2014-7-15新增-Start===*/
        Date reportDate = date.newInstance(reportingYear, reportingMonth, reportingDay);
        //Get Contacts into Map, filter success only = false (i.e. all contacts) 
        Map<Id,Contact> contactMap = StagingReportUtil.queryContactsFromTasks2(tasks,false,reportDate,QuestionnaireNo);
        System.debug('###############################contactMap  '+contactMap.keyset());
        //Get Contacts into Map, filter success only = true
        Map<Id,Contact> contactSuccessMap = StagingReportUtil.queryContactsFromTasks2(tasks,true,reportDate,QuestionnaireNo);
        System.debug('###############################contactSuccessMap  '+contactSuccessMap.keyset());
        /*===2014-7-15新增-End===*/
        
        //Put Tasks into City Map
        Map<String,List<Task>> cityTasksMap = StagingReportUtil.getCityTasksMap(contactMap,tasks);
        
        //Get Answers from Sucessful Task.Contact.Questionnaire
        Map<Id,List<Answer__c>> contactIdAnswers 
                    = StagingReportUtil.queryAnswersByContacts(contactSuccessMap.keySet());
        
         System.debug('###############################contactIdAnswers  '+contactIdAnswers.keyset());
         List<TS_GC_Workload_Stats__c> rows = new List<TS_GC_Workload_Stats__c>();
        //for each city
        for (String cityName : citySet)
        {            
            List<Task> cityTasks = cityTasksMap.get(cityName);
            if (cityTasks != null){
                TS_GC_Workload_Stats__c row =  StagingReportUtil.getTSGCWorkloadStats(
                	rows,reportingYear,reportingMonth,reportingDay,cityName, QuestionnaireNo);
                row.Called_To_Region__c = cityRegionMap.get(cityName);
                system.debug('City Tasks count:'+cityTasks.size());
                System.debug('###############################cityTasks  '+cityTasks.size());
                
                
                //一个Contact有多个Task，由Task找Contact再找Answer，可能导致同一个Contact及对应Answer被多次统计
                Set<Id> Set_ContactId = new Set<Id>();
                
                //for each task
                for (Task thisTask : cityTasks)
                {
                    system.debug('Task Id:'+thisTask.id);
                
                     System.debug('####################################################'+ thisTask.WhoId );
                    if(Set_ContactId.contains(thisTask.WhoId))
                    {
                    	continue;
                    }
                    else
                    {
                    	Set_ContactId.add(thisTask.WhoId);
                    }
                    
                    /*废卡数 , 致电数, 成功致电数 */
                    row.Total_Call__c ++;
                    if (thisTask.Result__c == 'Inactive'){
                        row.Invalid_Card__c ++; 
                    }else if (thisTask.Result__c == 'Connected/Closed'){
                        row.Total_Successful_Call__c ++;
                    }
                    
                    
                    List<Answer__c> thisContactAnswers = contactIdAnswers.get(thisTask.WhoId);
                    
                    system.debug('thisContactAnswers:'+thisContactAnswers);
                    
                    Date reportingDate = Date.newInstance(reportingYear,reportingMonth,reportingDay);
                    /*====一个联系人一天可以有多个answer 2014-7-15注释--Start */
                    //Answer__c thisAnswer = StagingReportUtil.getCurrentAnswer(thisContactAnswers,reportingDate);
                   // system.debug('thisAnswer:'+thisAnswer);
                    /*2014-7-15注释--End*/
                    /*2014-7-15调整*/
                    if(thisContactAnswers==null)
                    continue;
                    
                    System.debug('####################################################'+ thisTask.WhoId +'############################################'+thisContactAnswers.size());
                   
                    for(Answer__c thisAnswer : thisContactAnswers)
                    {
                    	
                    	if(QuestionnaireNo!=null && thisAnswer.Questionnaire__r.Questionnaire_No__c !=QuestionnaireNo )
                		continue; 
                    	
                    	DateTime d = thisAnswer.createdDate;
                		Date createdDate = Date.newInstance(d.year() , d.month() , d.day() );
                		if(createdDate != reportingDate)
                		continue;
                		
                		
                		
                		//row.Questionnaire_no__c = thisAnswer.Questionnaire__r.Questionnaire_No__c;
                		
                		
                		
                    	//原逻辑
                        /*现用品牌*/
                        String currentBrands = thisAnswer.Q054__c;
                        System.debug('########################################3    '+thisAnswer.Q054__c);
                        StagingReportUtil.setSurveyValues(
                            row, currentBrands,true,
                            new String[]{
                                	'Abbott',
                                    'Biostim',
                                    'Um Belle',
                                    'Dumex',
                                    'Friso',
                                    'MJ (GC)',
                                    'MJ (S1)',
                                    'MJ (S2)',
                                    'MJ (S3)',
                                    'MJ (Others)',
                                    'Nescafe',
                                    'Synutra',
                                    'Wyeth',
                                    'Yashili',
                                    'Yili',
                                    'Karicare',
                                    'Breast milk',
                                    'Others (please comment)'},	//possible missing meiji and beingmate
                            new String[]{
                                	'Current_Brand_Abbott__c',
                                    'Current_Brand_Biostine__c',
                                    'Current_Brand_Cow_Gate__c',
                                    'Current_Brand_Dumex__c',
                                    'Current_Brand_Friso__c',
                                    'Current_Brand_MJ__c',
                                    'Current_Brand_MJ_S1__c',
                                    'Current_Brand_MJ_S2__c',
                                    'Current_Brand_MJ_S3__c',
                                    'Current_Brand_MJ_Others__c',
                                    'Current_Brand_Nestle__c',
                                    'Current_Brand_Shengyuan__c',
                               		'Current_Brand_Wyeth__c',
                                    'Current_Brand_Yashili__c',
                                    'Current_Brand_Yili__c',
                                    'Current_Brand_Karicare__c',
                                    'Current_Brand_Breastfeeding__c',
                                    'Current_Brand_Other__c'}
                        );
                        /*现用品牌-孕期*/
                        String Current_Brand_Pregnancy_Week = thisAnswer.Q101__c;
                        StagingReportUtil.setSurveyValues(
                            row, Current_Brand_Pregnancy_Week,true,
                            new String[]{
                                	'Pregnancy'
                                    },	//possible missing meiji and beingmate
                            new String[]{
                                	'Current_Brand_Pregnancy_Week__c'
                                    }
                        );
                        row.Before_Purchase_Pregnancy_Week__c = row.Current_Brand_Pregnancy_Week__c;
                       /* 
                        if (thisAnswer.Questionnaire__r.Questionnaire_No__c == 1){
                            row.Current_Brand_Pregnancy_Week__c ++;
                        }*/
                        
                        
                        /*购买前使用...*/
                        String beforePurchase = thisAnswer.Q055__c;
                        StagingReportUtil.setSurveyValues(
                            row, beforePurchase,true,
                            new String[]{
                                	'Abbott',
                                    'Biostim',
                                    'Nutrilon',
                                    'Dumex',
                                    'Friso',
                                    'MJ (GC)',
                                    'MJ (S1)',
                                    'MJ (S2)',
                                    'MJ (S3)',
                                    'MJ (Others)',
                                    'Nescafe',
                                    'Synutra',
                                    'Wyeth',
                                    'Yashili',
                                    'Yili',
                                    'Karicare',
                                    'Breast milk',
                                    'Milupa',
                                    'Others (please comment)'},	//possible missing meiji and beingmate
                            new String[]{
                                	'Before_Purchase_Abbott__c',
                                    'Before_Purchase_Biostine__c',
                                    'Before_Purchase_Cow_Gate__c',
                                    'Before_Purchase_Dumex__c',
                                    'Before_Purchase_Friso__c',
                                    'Before_Purchase_MJ__c',
                                    'Before_Purchase_MJ_S1__c',
                                    'Before_Purchase_MJ_S2__c',
                                    'Before_Purchase_MJ_S3__c',
                                    'Before_Purchase_MJ_Others__c',
                                    'Before_Purchase_Nestle__c',
                                    'Before_Purchase_Shengyuan__c',
                               		'Before_Purchase_Wyeth__c',
                                    'Before_Purchase_Yashili__c',
                                    'Before_Purchase_Yili__c',
                                    'Before_Purchase_Karicare__c',
                                    'Before_Purchase_Breastfeeding__c',
                                    'Before_Purchase_Milupa__c',
                                    'Before_Purchase_Other__c'}
                        );
                        
                        /*原GC现GC , 原GC现非GC*/
                        String boughtGC = thisAnswer.Q056__c;
                        String beforeGC = thisAnswer.Q056_GC_last__c;
                        if (boughtGC == 'Y' && beforeGC=='Y'){
                            row.Before_GC_Now_GC__c ++;    
                        }
                        if (boughtGC == 'N' && beforeGC=='Y'){
                            row.Before_GC_Now_Non_GC__c ++;    
                        }
                        if (boughtGC == 'Y' && beforeGC=='N'){
                            row.Before_Non_GC_Now_GC__c ++;    
                        }
                        
                        /*有购买GC 没有购买GC*/
                        StagingReportUtil.setSurveyValues(
                            row, boughtGC,false,
                            new String[]{'Y','N'},
                            new String[]{'Bought_GC__c','Did_not_buy_GC__c'}
                        );
                        
                        /*试用过GC 没有试用过GC*/
                        String triedGC = thisAnswer.Q057__c;
                        StagingReportUtil.setSurveyValues(
                            row, triedGC,false,
                            new String[]{'Y','N'},
                            new String[]{'Tried_GC__c','Did_not_try_GC__c'}
                        );
                        
                        /*没试用原因-现用没用完 , 没试用原因-母乳充分, 没试用原因-目前消化正常, 
                         * 没试用原因-不会转奶, 没试用原因-其他 */
                        String reasonNotTrying = thisAnswer.Q059__c;
                        StagingReportUtil.setSurveyValues(
                            row,reasonNotTrying,false,
                            new String[]{'Not finish current product yet','Breastfeeding','No digestion issue',
                                'No willing to change','Others (please comment)'},
                            new String[]{'Reason_for_not_trying_Did_not_finish_cu__c',
                                'Reason_for_not_trying_Breastfeeding__c',
                                'Reason_for_not_trying_Digestion_OK__c',
                                'Reason_for_not_trying_Won_t_change_bran__c',
                                'Reason_for_not_trying_Others__c'}
                        );
                        
                        /*第一次购买后仍继续购买 , 第一次购买后不再购买 */
                        String continueAfterFirst = thisAnswer.Q064__c;
                        StagingReportUtil.setSurveyValues(
                            row, continueAfterFirst,false,
                            new String[]{'Y','N'},
                            new String[]{'Continue_purchase_after_first_buy__c',
                                'Discontinue_purchase_after_first_buy__c'}
                        );
                        
                        /* 购买1罐 ... 购买6罐以上 */
                        String qtyBought = thisAnswer.Q065__c;
                        StagingReportUtil.setSurveyValues(
                            row, qtyBought,false,
                            new String[]{'1Tin','2Tin','3Tin','4Tin','5Tin','6Tin Or More'},
                            new String[]{
                                'Bought_1_Can__c','Bought_2_Can__c','Bought_3_Can__c',
                                    'Bought_4_Can__c','Bought_5_Can__c','Bought_6_Can__c'}
                        );
                        /*是否接受GC口味-接受, 是否接受GC口味-一般 是否接受GC口味-不接受 */
                        String tasteRating = thisAnswer.Q058__c;
                        StagingReportUtil.setSurveyValues(
                            row, tasteRating,false,
                            new String[]{'Y','General','N'},
                            new String[]{
                                'GC_Taste_Rating_Acceptable__c',
                                    'GC_Taste_Rating_Normal__c',
                                    'GC_Taste_Rating_Not_acceptable__c'}
                        );
                        //Buying but not consuming 购买后没有使用的原因
                        String buyingButNotConsuming = thisAnswer.Q060__c; 
                        StagingReportUtil.setSurveyValues(
                            row,buyingButNotConsuming,false,
                            new String[]{
                            	'Not born yet',
                            	'Not finish current product yet', //'现用没用完',
                            	'Breastfeeding',//'母乳充分',
                            	'No digestion issue',//'目前消化正常',
                            	'No improvement after using GC',
                                'Improved after using GC, no need to cont',
                                'Others (please comment)',//'其他（注明)',
                                'Too expensive',
                                'Baby resists GC products'}, 
                            new String[]{
                            	'Not_Consuming_Baby_not_born__c',
                                'Not_Consuming_Havent_finish_cur__c',
                                'Not_Consuming_Breastfeeding__c',
                                'Not_Consuming_Digestion_OK__c',
                                'Not_Consuming_Digestion_no_improve__c',
                                'Not_Consuming_Digestion_Improved__c',
                                'Not_Consuming_Others__c',
                                'Not_Consuming_Expensive__c',
                                'Not_Consuming_Appetite_Change__c'}
                        );
                        //使用GC后消化有改善 , 使用GC后消化没有改善
                        String digestionImproved = thisAnswer.Q061__c;
                        StagingReportUtil.setSurveyValues(
                            row, digestionImproved,false,
                            new String[]{'Y','N'},
                            new String[]{'Used_GC_Digestion_improved__c',
                                'Used_GC_Digestion_did_not_improved__c'}
                        );
                        //消化不好的表现
                        String digestionSymtoms = thisAnswer.Q062__c;
                        StagingReportUtil.setSurveyValues(
                            row,digestionSymtoms,false,
                            new String[]{'Diarrhea','Constripation','Spits',
                                'Flatulence','Cry','Fidgety'},
                            new String[]{
                                'Digestion_symptom_diarrhoea__c',
                                    'Digestion_symptom_constipation__c',
                                    'Digestion_symptom_vomit__c',
                                    'Digestion_symptom_bloated__c',
                                    'Digestion_symptom_crying__c',
                                    'Digestion_symptom_irritated__c'}
                        );
                        //客户接受下次电访 , 客户不接受下次电访
                        String nextCall = thisAnswer.Q063__c;
                        StagingReportUtil.setSurveyValues(
                            row, nextCall,false,
                            new String[]{'Y','N'},
                            new String[]{'Customer_accept_next_call__c',
                                'Customer_do_not_accept_next_call__c'}
                        ); 
                    }
                    system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  '+row.Current_Brand_MJ_S1__c);
                   // if (thisAnswer != null)
                    //{  
                   // }//end if thisAnswer!=null
                }//end for each task
                //List_Rows.add(row);
            }//end if cityTasks!=null
        }//end for each city
        
        
        try{
        	upsert rows External_ID__c;
        	//upsert List_Rows External_ID__c;
        }catch (Exception ex){
            system.debug(system.LoggingLevel.ERROR, ex.getMessage());
            system.debug(system.LoggingLevel.ERROR, ex.getStackTraceString());
            throw ex;
        }
    }
    /*Finish*/
    global void finish(Database.BatchableContext bc){}

}