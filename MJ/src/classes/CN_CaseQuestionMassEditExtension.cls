/*Author:leo.bi@celnet.com.cn
 *Date:2014-5-4
 *function:The extension of CN_CaseQuestionMassEdit page
 *Apply To:CN
 */
public class CN_CaseQuestionMassEditExtension {
	public List<Case_Question__c> questions{get;set;}
    private List<Case_Question__c> originalQuestionsList{get;set;}
    public List<IndexedCaseQuestion> indexedQuestions{get;set;}
    private List<IndexedCaseQuestion> originalIndexedQuestionsList{get;set;}
    private Id casequestionRecordType;
    public Case parentCase{get;set;}
    public Integer cloneParameter{get;set;}
    
    class IndexedCaseQuestion
    {
    	public Integer index{get;set;}
    	public Case_Question__c cq{get;set;} 
    }
    
    //transform Case_Question__c to IndexedCaseQuestion
    private List<IndexedCaseQuestion> questionsToIndexed(List<Case_Question__c> cq)
    {
    	List<IndexedCaseQuestion> temp_IndexedCaseQuestion = new List<IndexedCaseQuestion>();
    	for(Integer i = 0; i < cq.size(); i++)
    	{
    		IndexedCaseQuestion icq = new IndexedCaseQuestion();
    		icq.index = i;
    		icq.cq = cq[i];
    		temp_IndexedCaseQuestion.add(icq);
    	}
    	return temp_IndexedCaseQuestion;
    }
    
    //transform IndexedCaseQuestion to Case_Question__c
    private List<Case_Question__c> indexedToQuestion(List<IndexedCaseQuestion> icqs)
    {
    	List<Case_Question__c> temp_CaseQuestion = new List<Case_Question__c>();
    	for(IndexedCaseQuestion icq : icqs)
    	{
    		temp_CaseQuestion.add(icq.cq);
    	}
    	return temp_CaseQuestion;
    }
    
    //Constructure
    public CN_CaseQuestionMassEditExtension(ApexPages.StandardController controller) 
    {
    	ID caseID = ApexPages.currentPage().getParameters().get('caseid');
        List<Case> cases = [SELECT ID,CaseNumber,Type FROM case WHERE ID=:caseID];
        casequestionRecordType = [select id from RecordType where DeveloperName=:'Case_Question_CN' and SobjectType=:'Case_Question__c'].id;            
        if(cases == NULL || cases.isEmpty())
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Parent Case not found. Please make sure you are accessing from the right place!'));
        }
        else
        {
            parentCase = cases.get(0);
            questions = [SELECT ID, Case__c, Type__c, Sub_Type__c,Related_Non_MJN__c,Related_Products__c, Format__c, Non_MJN_Prodcut__c, Question__c, Product__c,Remark__c, Non_Exact_Date_Of_Production__c, 
            			 Production_Date__c, Purchase_Place__c
                         FROM Case_Question__c 
                         WHERE Case__c =:caseID ORDER BY CreatedDate ASC];
            if(questions == null)
            {
                questions = new List<Case_Question__c>();
            }
            //If number of existing records < 5, then set to 5
            addNewLines(5 - questions.size());
            //Save default lines
            originalQuestionsList = questions.deepClone(true,true,true);
            originalIndexedQuestionsList = this.questionsToIndexed(originalQuestionsList); 
        }
    }
    
    public PageReference save()
    {
    	List<Case_Question__c> newList = new List<Case_Question__c>();
        List<Case_Question__c> updateList = new List<Case_Question__c>();
        questions = this.indexedToQuestion(indexedQuestions);
        for(Integer i = 0; i < questions.size(); i++)
        {
            if(questions[i].ID == NULL) //New case question
            {
               if(questions[i].Type__c == NULL && questions[i].Product__c == NULL && questions[i].Remark__c == NULL && questions[i].Purchase_Place__c == null && questions[i].Production_Date__c == null)
               {
                   //Ignore empty lines - do nothing
               }
               else
               {
                   newList.add(questions[i]);
               }
            }
            else //Existing case question record
            {  
               updateList.add(questions[i]);
            }
        }

        //If there is any validation error, return to the form page
        if(ApexPages.hasMessages())
        {
            return null;
        }
        try
        {
            if(!newList.isEmpty())
                insert newList;
            if(!updateList.isEmpty())
                update updateList;
	        //  Return to the Contact Detail page:
	        return new PageReference('/'+parentCase.ID); 
        }
        catch(DmlException ex)
        {
            ApexPages.addMessages(ex);
            return null;
        }
    }
    
    public PageReference reset()
    {
    	questions = originalQuestionsList.deepClone(true,true,true);
    	indexedQuestions = this.questionsToIndexed(questions);
        return null;
    }
    
    public PageReference cancel()
    {
    	return new PageReference('/'+parentCase.ID);
    }
    
    public PageReference addNew()
    {
    	//Add 3 lines in one click
        addNewLines(3);
        return null; 
    }
    
    private void addNewLines(Integer numerOfLines)
    {
    	for(Integer count=0; count<numerOfLines; count++)
    	{
            questions.add(createNewQuestion());
    	}
    	indexedQuestions = this.questionsToIndexed(questions);
    }
    
    private Case_Question__c createNewQuestion()
    {
        Case_Question__c question = new Case_Question__c();
        question.Case__c = parentCase.ID;
        question.RecordTypeId = casequestionRecordType;
        return question ;
    }
    
    public void cloneQuestion()
    {
    	Case_Question__c cq = new Case_Question__c();
    	cq.Case__c = parentCase.Id;
    	cq.RecordTypeId = casequestionRecordType;
    	cq.Format__c = questions[cloneParameter].Format__c;
    	cq.Production_Date__c = questions[cloneParameter].Production_Date__c;
    	cq.Non_Exact_Date_Of_Production__c = questions[cloneParameter].Non_Exact_Date_Of_Production__c;
    	cq.Related_Products__c = questions[cloneParameter].Related_Products__c;
    	cq.Related_Non_MJN__c = questions[cloneParameter].Related_Non_MJN__c;
    	if(questions.size() == (cloneParameter+1))
    	{
    		questions.add(cq);
    	}
    	else
    	{
	    	questions.add(cloneParameter + 1, cq);
	    	indexedQuestions = this.questionsToIndexed(questions);
    	}
    } 
    //private Boolean passValidation(Case_Question__c question)
    //{}
}