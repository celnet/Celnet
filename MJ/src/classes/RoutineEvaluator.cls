/*
Author: tommyliu@celnet.com.cn
Created On: 2014-5-5
Function: Encapsulate Greet and Reciprocal Questionnaire Evaluation flow.
  1. Routing Questionnaire
  2. Generate and assign Q-Task
  More detail, refer to design document : Routine Call Evaluation Flow
Apply To: CN (CN / HK / ALL) 
*/
public class RoutineEvaluator 
{
  private List<Contact> contacts;//a list of contact for Evaluation
  private List<Q_Task__c> qTasks;
  private Datetime evaluateOn;
  private Date startOfThisMonth;
  private Date endOfThisMonth;
  private Date currentDate;
  private List<Questionnaire__c> List_Routine;
  private final Integer MaxRegisterDate=365; 
  private final String MaxRegisterDateMes = 'Registration time more than '+MaxRegisterDate+' days from now to stop the assessment';
  private final Integer MaxAge=730;
  private final String MaxAgeMes = 'Birthday more than '+MaxAge+' days from now to stop the assessment';
  private final String QuestionnaireType='Routine';
  private final String EvaluationTypeGreet = 'Greet';
  private final String EvaluationTypeReciprocal = 'Reciprocal';
  private final String FinishMes = 'Evaluation is finished';//Fin
  private final String MatchQuestionnaireErrMes = 'Match Questionnaire Failed';
  private final String NotFoundGreetCallMes = 'Has not found Greet Call, go back to Greet Flow';
  private final String IntervalFailedMes ='Last Call is not more than 1 month interval, postpone evaluation to next moth';
  private final String HasAnswerMes = 'Has Answer,Wait for Next Evaluation';
  public String TimingType;
  public RoutineEvaluator(List<Contact> contactss,Date startOfThisMonth)
  {
    //contacts
    this.contacts = contactss;
    this.qTasks = new List<Q_Task__c>();
    this.evaluateOn = Datetime.now();  
    if(startOfThisMonth != null)
    {
    	this.currentDate = startOfThisMonth;
    	this.startOfThisMonth = startOfThisMonth;
    	this.evaluateOn =datetime.newInstance(startOfThisMonth.year(),startOfThisMonth.month(),startOfThisMonth.day(),evaluateOn.hour(),evaluateOn.minute(),evaluateOn.second());
    }
    else
    {
    	this.startOfThisMonth = this.evaluateOn.Date().toStartOfMonth();
    	this.currentDate = this.evaluateOn.Date();
    }
    this.endOfThisMonth = this.startOfThisMonth.addMonths(1).addDays(-1);
   
    this.List_Routine = new List<Questionnaire__c>();
    for(Questionnaire__c Questionnaire : [select Id,Name,Start_age__c,End_age__c,Type__c,Business_Type__c,Next_Routine_Questionnaire__c,
                        Next_Routine_Questionnaire__r.Name,Next_Routine_Questionnaire__r.Start_age__c,Next_Routine_Questionnaire__r.End_age__c 
                        from Questionnaire__c where Type__c =:QuestionnaireType order by Start_age__c ])
    {
      List_Routine.add(Questionnaire);
    }
  }
  
  //评估程序的调用入口，被RoutineQTaskDistributionHandler或RoutineQTaskDistributionBatch调用
  public void Run()
  {
    try
    {
      for(Contact cont: contacts)
      {
        //TODO：剔除那些不满足条件的联系人
        if(cont.Register_Date__c ==null || cont.Inactive__c || cont.Duplicate_Status__c != null || cont.Cannot_Be_Contacted__c || cont.Birthdate==NULL || cont.Verified__c)
        {
          continue;
        }
        //if(cont.Routine_Evaluated_On__c == null && !cont.Verified__c)//为首次评估，则按欢迎流程评估
        //if(!this.CheckHasGreetAnswerMatch(cont))//判断是否已经有已完成的欢迎问卷answer
        if(cont.Last_Routine_Call_Time__c==null && this.TimingType != 'Non-Real Time')//判断是否最近有过致电
        {  
          this.EvaluateGreet(cont);
        }
        else //不是首次评估，则按回访流程评估，（回访流程在条件满足后可能会转到欢迎流程）
        {
          this.EvaluateReciprocal(cont);
        }
      }
      //评估完成后，集中一起保存状态
      this.SaveQTasks();
      this.SaveStatus();
    }catch(Exception e)
    {
      System.debug('Error:'+e.getmessage()+' LineNumber:'+e.getLineNumber());  
      Error_Log__c el = new Error_Log__c();
		el.Line_Number__c = e.getLineNumber();
		el.Message__c = e.getMessage();
		el.Stack_Trace_String__c = e.getStackTraceString();
		el.Type_Name__c = e.getTypeName();
		el.Error_Time__c = DateTime.now();
		el.Handler_Name__c = 'RoutineEvaluator';
		el.User__c = UserInfo.getUserId();  
		insert el; 
    }
    
  }
  
  private void EvaluateGreet(Contact cont)//针对一个联系人评估欢迎问卷流程
  {
    if(cont.Register_Date__c != null && cont.Register_Date__c.daysBetween(currentDate)>=MaxRegisterDate)
    {
      cont.Routine_Evaluated_On__c = evaluateOn;
      cont.Next_Routine_Evaluation_Date__c = null;
      cont.Appended_Log__c = this.AppendedLog(cont, TimingType, EvaluationTypeGreet,null, 'No', MaxRegisterDateMes);
      return;
    }
    
    
    //TODO：实现欢迎问卷流程
    Questionnaire__c Routine = this.MatchQuestionnaire(cont.Birthdate,'Greet'); 
    if(Routine.Id != null)
    {
      //Contact
      cont.Questionnaire__c = Routine.Id;
      cont.Routine_Evaluated_On__c = evaluateOn;
      cont.Next_Routine_Evaluation_Date__c = startOfThisMonth.addMonths(2);
      cont.Appended_Log__c = this.AppendedLog(cont, TimingType, EvaluationTypeGreet, Routine.Name, 'Yes', FinishMes);
      //Q-Task
      qTasks.add(this.CreateQTask(cont.Id,cont.Birthdate,Routine.Id,Routine.Type__c,Routine.Business_Type__c));
    }
    else
    {
      cont.Next_Routine_Evaluation_Date__c = startOfThisMonth.addMonths(1);
      cont.Appended_Log__c = this.AppendedLog(cont, TimingType, EvaluationTypeGreet, null, 'No', MatchQuestionnaireErrMes);
    }
  }
  
  private void EvaluateReciprocal(Contact cont)//针对一个联系人评估回访问卷流程，条件满足后会调用
  {
    if(cont.Birthdate.daysBetween(currentDate)>=MaxAge)
    {
	      cont.Routine_Evaluated_On__c = evaluateOn;
	      cont.Next_Routine_Evaluation_Date__c = null;
	      cont.Appended_Log__c = this.AppendedLog(cont, TimingType, EvaluationTypeReciprocal,null, 'No', MaxAgeMes);
	      return;
    }
    //TODO：实现回访问卷流程
    //Contact
    cont.Routine_Evaluated_On__c = evaluateOn;
    //最近的Routine致电日期距现在是否为一个月
    if(date.newInstance(cont.Last_Routine_Call_Time__c.year(),cont.Last_Routine_Call_Time__c.month(),1).addMonths(1)<=startOfThisMonth)
    {
      	 Questionnaire__c Routine = this.MatchQuestionnaire(cont.Birthdate,'Reciprocal'); 
		 //是否有对应问卷
		 if(Routine.Id != null)
		 {
		   //cont.Next_Routine_Evaluation_Date__c = (Routine.Next_Routine_Questionnaire__c !=null?cont.Birthdate.addDays(Integer.valueOf(Routine.Next_Routine_Questionnaire__r.End_age__c)).toStartOfMonth():null);
		   cont.Next_Routine_Evaluation_Date__c = (Routine.Next_Routine_Questionnaire__c !=null?startOfThisMonth.addMonths(2):null);
		 
		   //对应的问卷是否已经有Finished的Answer
		   if(!this.CheckHasAnswerMatch(cont,Routine.Id))
		   {
		     //Q-Task
		     cont.Questionnaire__c = Routine.Id;
		     qTasks.add(this.CreateQTask(cont.Id,cont.Birthdate,Routine.Id,Routine.Type__c,Routine.Business_Type__c));
		     cont.Appended_Log__c = this.AppendedLog(cont, TimingType, EvaluationTypeReciprocal, Routine.Name, 'Yes', FinishMes);
		   }
		   else
		   {
		     cont.Appended_Log__c = this.AppendedLog(cont, TimingType, EvaluationTypeReciprocal,null, 'No', HasAnswerMes);
		   }
		   
		 }
		 else
		 {
		   cont.Next_Routine_Evaluation_Date__c = startOfThisMonth.addMonths(1);
		   cont.Appended_Log__c = this.AppendedLog(cont, TimingType, EvaluationTypeReciprocal, null, 'No', MatchQuestionnaireErrMes);
		 }
    }
    else
    {
      cont.Next_Routine_Evaluation_Date__c = startOfThisMonth.addMonths(1);
      cont.Appended_Log__c = this.AppendedLog(cont, TimingType, EvaluationTypeReciprocal, null, 'No',IntervalFailedMes);
    }
    
    /*
    //Has Greet Call?是否真正有Call过Greet
    if(this.CheckHasGreetAnswerMatch(cont))
    {
      
    }
    else
    {
      cont.Appended_Log__c = this.AppendedLog(cont, TimingType, EvaluationTypeReciprocal, null, 'No', NotFoundGreetCallMes);
      this.EvaluateGreet(cont);
    }*/
  } 
  private void SaveStatus()
  {
    update contacts;
  } 
  private void SaveQTasks()
  {
    upsert qTasks ID__c;
  }
  private Questionnaire__c MatchQuestionnaire(Date Birthdate,String QuestionnaireType)
  {
    Questionnaire__c returnQuestionnaire = new Questionnaire__c();
    
    if(List_Routine.size()>0 && Birthdate != null)
    {
      //Greet
      Integer AgeDays = Birthdate.daysBetween(currentDate);
      
      for(Questionnaire__c ques : List_Routine)
      {
        if(QuestionnaireType == 'Greet' && ques.Business_Type__c =='Routine Greet')
        {
          //S0  －∞  0
          if(ques.Start_age__c == null && ques.End_age__c != null && AgeDays<ques.End_age__c)
          {
            returnQuestionnaire = ques;
            break;
          }
          else if(ques.Start_age__c != null && ques.End_age__c != null && AgeDays >= ques.Start_age__c && AgeDays <= ques.End_age__c )
          {
            returnQuestionnaire = ques;
            break;
          }
          //6月以上
          else if(ques.Start_age__c != null && ques.End_age__c == null && AgeDays>=ques.Start_age__c)
          {
            returnQuestionnaire = ques;
            break;
          }
        }
        else if(QuestionnaireType == 'Reciprocal' && (ques.Business_Type__c =='Routine Reciprocal' || ques.Business_Type__c=='Routine Distribution'))
        {
        	//Reciprocal
        	Date StartDate =(ques.Start_age__c != null ? startOfThisMonth.addMonths(-Integer.valueOf(ques.Start_age__c/30)) : null);
        	Date EndDate = (ques.End_age__c != null ? endOfThisMonth.addMonths(-Integer.valueOf(ques.End_age__c/30)) : null);
        	EndDate = EndDate.toStartOfMonth().addMonths(1).addDays(-1);
	        //孕9： -30    //回访3：  30 - 60  //回访4： 90 - 120  //回访5： 180 - 180
	        //回访6： 240 - 240 //回访7： 330 - 330 //回访8： 390 - 390
	        if(Birthdate>=StartDate && Birthdate<= EndDate)
	        {
	         	returnQuestionnaire = ques;
	           	break;
	        }
        }
      }
    }
    return returnQuestionnaire; 
  }

  private Boolean CheckHasAnswerMatch(Contact cont,Id QuestionnaireId)
  {
    Boolean Flag = false;
    if(cont.Answers__r != null && cont.Answers__r.size()>0)
    {
      for(Answer__c ans : cont.Answers__r)
      {
        if(ans.Questionnaire__c == QuestionnaireId)
        {
          Flag = true;
          break;
        }
      }
    }
    return Flag;
    
  }
  private Q_Task__c CreateQTask(Id ConId,Date BabyBirthdate,Id QuestionnaireId,String Type,String BusinessType)
  {
    //Q-Task
    Q_Task__c QTask = new Q_Task__c();
    QTask.Contact__c = ConId;
    QTask.Birthdate__c =BabyBirthdate;
    QTask.Status__c = 'Open';
    QTask.Type__c = Type;
    QTask.Business_Type__c = BusinessType;
    QTask.ID__c = String.valueOf(ConId)+'-'+QTask.Status__c;
    QTask.Questionnaire__c = QuestionnaireId;
    QTask.Routine_Evaluated_On__c = startOfThisMonth;
    return QTask;
  }
  private String AppendedLog(Contact cont,String TimingType,String EvaluationType,String RoutineQuestionnaire,String QTGenerated,String message)
  {
    if(cont.Appended_Log__c!=null&&cont.Appended_Log__c.Length()>800)
    cont.Appended_Log__c = null;
    String RtString = '';
    if(message == FinishMes)
    {
      RtString =
      '************************************'+'\n'+ 
      'Routine Evaluated On: '+String.valueOf(cont.Routine_Evaluated_On__c)+'\n'+
      'Timing: '+TimingType+'\n'+
      'Evaluation Type: '+EvaluationType+'\n'+
      'Birthday: '+String.valueOf(cont.Birthdate)+'\n'+
      'Age: '+(cont.Birthdate.daysBetween(currentDate)>0?cont.Birthdate.daysBetween(currentDate):0)+'\n'+
      'Routine Questionnaire: '+RoutineQuestionnaire+'\n'+
      'Next Routine Evaluation Date: '+String.valueOf(cont.Next_Routine_Evaluation_Date__c)+'\n'+
      'Q-Task Generated: '+QTGenerated+'\n'+
      //'Q-Task Assigned to: '+QueuesName+'\n'+
      'Message: '+message+'\n'+(cont.Appended_Log__c!=null?cont.Appended_Log__c:'');
    }
    else if(message == MatchQuestionnaireErrMes || message == NotFoundGreetCallMes || message == MaxRegisterDateMes || message == MaxAgeMes || message == HasAnswerMes)
    {
      RtString =
      '************************************'+'\n'+
      'Routine Evaluated On: '+String.valueOf(cont.Routine_Evaluated_On__c)+'\n'+
      'Timing: '+TimingType+'\n'+
      'Evaluation Type: '+EvaluationType+'\n'+
      'Message: '+message+'\n'+(cont.Appended_Log__c!=null?cont.Appended_Log__c:'');
    }
    else if(message == IntervalFailedMes)
    {
      RtString =
      '************************************'+'\n'+
      'Routine Evaluated On: '+String.valueOf(cont.Routine_Evaluated_On__c)+'\n'+
      'Timing: '+TimingType+'\n'+
      'Evaluation Type: '+EvaluationType+'\n'+
      'Next Routine Evaluation Date: '+String.valueOf(cont.Next_Routine_Evaluation_Date__c)+'\n'+
      'Message: '+message+'\n'+(cont.Appended_Log__c!=null?cont.Appended_Log__c:'');
    }
    else 
    {
      RtString = cont.Appended_Log__c;
    }
    
    return RtString;
  }
}