/*Author:leo.bi@celnet.com.cn
 *Function:test WechtUserEventHandler
 *Date:2014-09-25
 */
@isTest
private class WechatUserEventHandlerTest 
{
    static testMethod void TestAutoSetMemberBindingInfoAndLeadConvert() 
    {
        system.runAs(TestUtility.new_HK_User())
        {
            //Create custom settings
            TestUtility.setupMemberIDCustomSettings();
            Id leadRecordType = [select id from RecordType where DeveloperName=:'LeadsHK' and SobjectType=:'Lead'].id;
            Id accountRecordType = [select id from RecordType where DeveloperName=:'HouseHold' and SobjectType=:'Account'].id;
            Account acc = new Account();
            acc.Name = 'PRIMARY PARENT';
            acc.Last_Name__c = 'PRIMARY';
            acc.First_Name__c = 'PARENT';
            acc.RecordTypeId = accountRecordType;
            insert acc;
            Lead lea = new Lead();
            lea.LastName = 'PARENT';
            lea.FirstName = 'TEST';
            lea.First_Name_1st_Child__c = 'BABY';
            lea.Last_Name_1st_Child__c = 'TEST';
            lea.Birthday_1st_Child__c = system.today();
            lea.RecordTypeId = leadRecordType;
            insert lea;
            Wechat_User__c wu = new Wechat_User__c();
            wu.Open_Id__c = 'leo' + System.now();
            wu.Name = 'leo';
            insert wu;
            wu.Binding_Non_Member__c = lea.Id;
            update wu;
            LeadStaging staging = new LeadStaging(lea,true);
            //set staging parameters to create new parent and merge existing baby
            LeadConversionHelper.convertStaging(staging);
            wu.Binding_Non_Member__c = null;
            update wu;
            wu.Binding_Member__c = null;
            update wu;
            
            wu.Binding_Member__c = acc.id;
            update wu;
            Lead lea1 = new Lead();
            lea1.LastName = 'PARENTa';
            lea1.FirstName = 'TEST2';
            lea1.First_Name_1st_Child__c = 'BABY';
            lea1.Last_Name_1st_Child__c = 'TEST';
            lea1.Birthday_1st_Child__c = system.today();
            lea1.RecordTypeId = leadRecordType;
            insert lea1;
            
            wu.Binding_Non_Member__c = lea1.id;
            update wu;
            Wechat_User__c w = [select id , Binding_Status__c from Wechat_User__c where id = :wu.id];
            system.assertequals('Member' , w.Binding_Status__c);
            
            WechatPlatformRequestMock fakeResponse = new WechatPlatformRequestMock();
            system.Test.setMock(HttpCalloutMock.class, fakeResponse);
            WechatCalloutProcessorBatch.preRunJob();
            WechatCalloutProcessorBatch.runScheduleForTestClass();
        }
    }
}