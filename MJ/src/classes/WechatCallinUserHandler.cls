/*
*Author:michael.mi@celnet.com.cn
*Date:2014-09-04
*Function:用户关注和取消关注时由此Handler来处理
*/
public class WechatCallinUserHandler extends WechatCallinMsgHandler
{
	public override void Handle(WechatCallinMsgPipelineContext Context)
	{
		WechatEntity.InEventMsg inEvent = (WechatEntity.InEventMsg)Context.InMsg;
		string replace = '<a href="'+WechatBusinessUtility.WECHAT_SITE+'WechatSMSVerificationPage?openId='+inEvent.FromUserName+'">【註冊/綁定】</a>';
		string link = WechatBusinessUtility.QueryRemindingByKey('RN001',replace);
		if(inEvent.MsgType != WechatEntity.MESSAGE_TYPE_EVENT)
		{
			return ;
		}
		if(inEvent.Event == WechatEntity.EVENT_TYPE_SUBSCRIBE)
		{
			List<Wechat_User__c> wuList = [Select Name, Id, Binding_Non_Member__c, Binding_Member__c , (Select Id From Attachments) From Wechat_User__c where Open_Id__c = :inEvent.FromUserName];
			Wechat_Callout_Task_Queue__c userInfoQueue = new Wechat_Callout_Task_Queue__c();
			userInfoQueue.Public_Account_Name__c = Context.PublicAccountName;
			userInfoQueue.Open_ID__c = inEvent.FromUserName;
			userInfoQueue.Type__c = WechatEntity.EVENT_TYPE_SUBSCRIBE;
			userInfoQueue.Processor_Name__c = 'WechatCalloutGetUserProcessor';
			userInfoQueue.Name = 'WechatCalloutGetUserProcessor';
			WechatCalloutQueueManager.EnQueue(userInfoQueue);
			if(wuList.size() == 0)
			{
				Wechat_User__c sfUser = new Wechat_User__c();
				sfUser.Open_Id__c = inEvent.FromUserName;
				sfUser.Public_Account_Name__c = Context.PublicAccountName;
				upsert sfUser Open_Id__c;
				WechatEntity.OutTextMsg outMsg = WechatEntity.GenerateOutMsg(inEvent, link);
				Context.OutMsg = outMsg;
			}
			else
			{
				Wechat_User__c sfUser = wuList[0];
				if(sfUser.Binding_Non_Member__c == null && sfUser.Binding_Member__c == null)
				{
					WechatEntity.OutTextMsg outMsg = WechatEntity.GenerateOutMsg(inEvent, link);
					Context.OutMsg = outMsg;
				}
				else if(sfUser.Binding_Member__c != null)
				{
					WechatEntity.OutTextMsg outMsg = WechatEntity.GenerateOutMsg(inEvent,WechatBusinessUtility.QueryRemindingByKey('RN002'));
					Context.OutMsg = outMsg;
				}
				else if(sfUser.Binding_Non_Member__c != null)
				{
					WechatBusinessUtility.BirthCount bc = WechatBusinessUtility.GetBirthCertificateCount(inEvent.FromUserName);
					if(bc.hasBirthCount == 0)
					{
						WechatEntity.OutTextMsg outMsg = WechatEntity.GenerateOutMsg(inEvent , WechatBusinessUtility.QueryRemindingByKey('RN003'));
						Context.OutMsg = outMsg;
					}
					else if(bc.hasBirthCount > 0 && bc.hasBirthCount < bc.birthCertificateCount)
					{
						list<object> replaceKeywords = new list<object>();
						replaceKeywords.add(bc.hasBirthCount);
						replaceKeywords.add(bc.birthCertificateCount - bc.hasBirthCount);
						WechatEntity.OutTextMsg outMsg = WechatEntity.GenerateOutMsg(inEvent , WechatBusinessUtility.QueryRemindingByKey('RN004',replaceKeywords));
						Context.OutMsg = outMsg;  
					}
					else
					{
						WechatEntity.OutTextMsg outMsg = WechatEntity.GenerateOutMsg(inEvent,WechatBusinessUtility.QueryRemindingByKey('RN005'));
						Context.OutMsg = outMsg;
					}
				}
			}
		}
		if(inEvent.Event == WechatEntity.EVENT_TYPE_UNSUBSCRIBE)
		{
			List<Wechat_User__c> wuList = [Select id From Wechat_User__c where Open_Id__c = :inEvent.FromUserName];
			if(!wuList.isempty())
			{
				Wechat_User__c wu = new Wechat_User__c();
				wu.Open_Id__c = inEvent.FromUserName;
				wu.UnSubscribe_Time__c = datetime.now();
				wu.Subscribe__c = false;
				upsert wu Open_Id__c;
			}
		}
	}
}