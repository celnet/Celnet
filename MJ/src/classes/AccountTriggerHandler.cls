/*
	On Before Insert:
		Assign Unique member ID to houshold of Member ID not null
	On After Insert:
		Create Primary Contact for Household
	on Before Update:
		Sync Can_be_called to all contacts if Can_be_called changed
*/
public with sharing class AccountTriggerHandler {
	private boolean m_isExecuting = false;
	private integer BatchSize = 0;
	
    
	public AccountTriggerHandler(boolean isExecuting, integer size){
		m_isExecuting = isExecuting;
		BatchSize = size;
	}
		
	public void OnBeforeInsert(Account[] newAccounts){
		assignMemberId(newAccounts);
	}
	
	public void OnAfterInsert(Account[] newAccounts){
		createPrimaryContact(newAccounts);
	}
	
	public void onAfterUpdate(Map<Id,Account> oldAccountMap, Account[] newAccounts){
		syncCanBeCalled(oldAccountMap,newAccounts);
	}
	
	/**
		Trigger Event method Ends
		Helper Methods Starts 
	**/
		
	private void assignMemberId(Account[] newAccounts){
		for (Account acc : newAccounts){		
			if(acc.Member_ID__c == NULL)
            {
                system.debug('RT in BEFORE Insert: ' + acc.RecordTypeId);
                RecordType householdNewRT 
                = ObjectUtil.GetRecordTypesByDevName(Account.SObjectType).get(CommonHelper.HOUSEHOLD_NEW_RT);
                RecordType householdRT 
                = ObjectUtil.GetRecordTypesByDevName(Account.SObjectType).get(CommonHelper.HOUSEHOLD_RT);
                
                if (acc.RecordTypeId == householdNewRT.Id || acc.RecordTypeId == householdRT.Id){
                    acc.Member_ID__c = UniqueCodeGenerator.generateNextMemberID();
                }
            }
		}
	}
	
	private void createPrimaryContact(List<Account> accounts){
		// Bulkify read new acounts and bulk create new primary contacts for every new account.
        List<Account> newHouseholds = new List<Account>();
        
        for (Account acc : accounts){
            system.debug('RT in After Insert: ' + acc.RecordTypeId);
            RecordType householdNewRT 
            = ObjectUtil.GetRecordTypesByDevName(Account.SObjectType).get(CommonHelper.HOUSEHOLD_NEW_RT);
            RecordType householdRT 
            = ObjectUtil.GetRecordTypesByDevName(Account.SObjectType).get(CommonHelper.HOUSEHOLD_RT);
            
            if (acc.RecordTypeId == householdNewRT.Id || acc.RecordTypeId == householdRT.Id){
                newHouseholds.add(acc);
            }
        }
        if (newHouseholds.isEmpty()){
            return;
        }
            
        List<Contact> contacts = new List<Contact>();
        for (Account account : newHouseholds){
            Contact con = new Contact();
            con.AccountId = account.Id; 
            con.RecordTypeId = ObjectUtil.GetRecordTypeID(Contact.SObjectType,CommonHelper.HOUSEHOLD_PARENT_RT);
            con.Is_Primary_Contact__c = true;
            con.LastName = account.Last_Name__c;
            con.FirstName = account.First_Name__c;
            con.Salutation = account.Salutation__c;
            con.Chinese_Name__c = account.Chinese_Name__c;
            con.Other_Name__c = account.Other_Name__c;
            con.Spouse_Name__c = account.Spouse_Name__c;
            con.Occupation__c = account.Occupation__c;
            con.Email = account.Account_Email__c;
            con.Birthdate = account.Birthdate__c;
			con.Preferred_Written_Languge__c = account.Preferred_Written_Languge__c;
			con.Preferred_Spoken_Language__c = account.Preferred_Spoken_Language__c;
			con.China_Mom__c =  account.China_Mom__c;
            contacts.add(con);
        }
        try{
			insert contacts;
        }catch (DMLException e){
		     for (Account account : accounts) {
				account.addError('There was a problem creating primary contacts. Make sure Primary Contact Information is filled. '+e.getMessage());
		     }
        }
	}
	
	private void syncCanBeCalled(Map<Id,Account> oldAccountMap, Account[] newAccounts){
		RecordType householdRT 
            = ObjectUtil.GetRecordTypesByDevName(Account.SObjectType).get(CommonHelper.HOUSEHOLD_RT);
        List<Contact> contactsToBeUpdated = new List<Contact>();
        
        for (Account newHousehold : newAccounts){
        	if (newHousehold.RecordTypeId != householdRT.Id){
        		continue;
        	}
        	ID householdId = newHousehold.Id;
        	Account oldHousehold = oldAccountMap.get(householdId);
        	if (oldHousehold.Can_be_called__c != newHousehold.Can_be_called__c){
        		for (Contact contact : [Select Account_can_be_called__c from Contact where AccountId = :householdId]){
        			contact.Account_can_be_called__c = newHousehold.Can_be_called__c;
        			contactsToBeUpdated.add(contact);
        		}
        	}
        }
        if (!contactsToBeUpdated.isEmpty()){
        	update contactsToBeUpdated;
        }
	}
	
}