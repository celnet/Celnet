public with sharing class HouseholdWrapper implements Comparable {
	public boolean firstHousehold{get;set;}
	public Account household {get;set;}
	public List<Contact> parents {get;set;}
	public List<Contact> babies {get;set;}
	
	public Integer rank{get;set;}
	
	public HouseholdWrapper(){
		parents = new List<Contact>();
		babies = new List<Contact>();
		rank = 0;
	}
	
	public void addHousehold(Account household){
		this.household = household;
	}
	
	public void addParent(Contact parent){
		parents.add(parent);
	}
	
	public void addBaby(Contact baby){
		babies.add(baby);
	}
	
	public void updateRank(Map<String,String> searchTerms){
		if (searchTerms == null) return;
		rank = 0;
		String matchingTerms = searchTerms.get('Phone');
		
		system.debug(matchingTerms);
		
		if (matchingTerms!=null){
			Set<String> matchingSet = new Set<String>();
			matchingSet.addAll(matchingTerms.split('|'));
			
			if (household.Home_Phone__c != null && matchingTerms.contains(household.Home_Phone__c)) rank+=10;
			if (household.Mobile_Phone__c != null && matchingTerms.contains(household.Mobile_Phone__c)) rank+=10;
			if (household.Other_Phone_1__c != null && matchingTerms.contains(household.Other_Phone_1__c)) rank+=10;
			if (household.Other_Phone_2__c != null && matchingTerms.contains(household.Other_Phone_2__c)) rank+=10;
		}
		system.debug(rank);
		matchingTerms = searchTerms.get('Email');
		if (matchingTerms!=null){
			Set<String> matchingSet = new Set<String>();
			matchingSet.addAll(matchingTerms.split('|'));
			if (household.Account_Email__c != null && matchingTerms.contains(household.Account_Email__c)) rank+=5;
		}
		
		matchingTerms = searchTerms.get('Parent Name');
		if (matchingTerms!=null){
			Set<String> matchingSet = new Set<String>();
			matchingSet.addAll(matchingTerms.split('|'));
			for (Contact parent : parents){
				if (parent.Name != null && matchingTerms.contains(parent.Name)) rank++;
				if (parent.Chinese_Name__c != null && matchingTerms.contains(parent.Chinese_Name__c)) rank++;
				if (parent.Other_Name__c != null && matchingTerms.contains(parent.Other_Name__c)) rank++;
			}
		}
		
		matchingTerms = searchTerms.get('Baby Name');
		if (matchingTerms!=null){
			Set<String> matchingSet = new Set<String>();
			matchingSet.addAll(matchingTerms.split('|'));
			for (Contact baby : babies){
				if (baby.Name != null && matchingTerms.contains(baby.Name)) rank++;
				if (baby.Other_Name__c != null && matchingTerms.contains(baby.Other_Name__c)) rank++;
			}
		}
		
		matchingTerms = searchTerms.get('Baby Chinese Name');
		if (matchingTerms!=null){
			Set<String> matchingSet = new Set<String>();
			matchingSet.addAll(matchingTerms.split('|'));
			for (Contact baby : babies){
				if (baby.Chinese_Name__c != null && matchingTerms.contains(baby.Chinese_Name__c)) rank++;
			}
		}
	}
	
	
	//Parent Name for data table output 
	public String getParentName(){
		String out = '';
		if (parents.size()>0){
			out += parents[0].Name;
			String chineseName= parents[0].Chinese_Name__c;
			if (chineseName != null){
				out += '/' + chineseName;
			}
		}
	
		if (parents.size()>1){
			out += '<br/>'+ parents[1].Name;
			String chineseName= parents[1].Chinese_Name__c;
			if (chineseName != null){
				out += '/' + chineseName;
			}
		}		
		return out;			
	}
	
	public Integer compareTo(Object compareTo) 
    {
        HouseholdWrapper hhwrapper = (HouseholdWrapper) compareTo;
        if (rank == hhwrapper.rank) return 0;
        if (rank < hhwrapper.rank) return 1;
        return -1;        
    }
	
}