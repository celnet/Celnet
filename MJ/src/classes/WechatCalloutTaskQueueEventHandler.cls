/*Author:michael
 *Date:20141015
 *Function:when wechat user binding to non-member and member, notify wechat user by wechat api
 */
public class WechatCalloutTaskQueueEventHandler implements Triggers.Handler
{
	public void Handle()
	{
		if(!Context.ApplyTo(new String[] {'HK'}))
		{
			return;
		}
		if(trigger.isAfter && trigger.isUpdate)
		{
			if(trigger.new[0] instanceof Wechat_User__c)
			{
				this.WechatCalloutAlertMember();
			}
		}
	}
	
	private void WechatCalloutAlertMember()
	{
		List<Wechat_Callout_Task_Queue__c> wechatTask = new List<Wechat_Callout_Task_Queue__c>();
		List<Wechat_User__c> list_WU = Trigger.new;
		for(Wechat_User__c wu : list_WU)
		{
			Wechat_User__c old = (Wechat_User__c)Trigger.oldMap.get(wu.id);
			if(old.Binding_Member__c == null && wu.Binding_Member__c !=null)
			{
				string textBody = WechatEntity.GenerateSvcTextMsg(wu.Open_Id__c,WechatBusinessUtility.QueryRemindingByKey('RN028'));
		        Wechat_Callout_Task_Queue__c textQueue = WechatCalloutQueueManager.CreateSendSvcTextMsgTask(wu.Public_Account_Name__c,textBody,'WechatCallOutSendTextMsgProcessor');
				wechatTask.add(textQueue);
			}
			else if(old.Binding_Non_Member__c == null && wu.Binding_Non_Member__c !=null)
			{
				string textBody = WechatEntity.GenerateSvcTextMsg(wu.Open_Id__c,WechatBusinessUtility.QueryRemindingByKey('RN029'));
		        Wechat_Callout_Task_Queue__c textQueue = WechatCalloutQueueManager.CreateSendSvcTextMsgTask(wu.Public_Account_Name__c,textBody,'WechatCallOutSendTextMsgProcessor');
				wechatTask.add(textQueue);
			}
		}
		if(!wechatTask.isempty())
		{
			WechatCalloutQueueManager.EnQueue(wechatTask);
		}
	}
}