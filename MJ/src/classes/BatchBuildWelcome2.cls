global class BatchBuildWelcome2 implements Database.Batchable<SObject>{

    private String questionnaireQuery;
    private Integer reportingDay;
    private Integer reportingMonth;
    private Integer reportingYear;
    
     private boolean chain = false;
    private Date endDate;
    
    /*Contructor
		
	*/
    public  BatchBuildWelcome2(String query,Integer year, Integer month, Integer day){
        questionnaireQuery = query;
        reportingDay = day;
        reportingMonth = month;
        reportingYear = year;
        chain = false;
    }
    
    public  BatchBuildWelcome2(String query,Integer year, Integer month, Integer day, Date endDate){
        questionnaireQuery = query;
        reportingDay = day;
        reportingMonth = month;
        reportingYear = year;
        if (Date.newInstance(year, month, day) < endDate){
            this.endDate = endDate;
            chain = true;
        }
    }
    
    /*Start Method*/
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(questionnaireQuery);
    }
    
    /*Execute*/
    global void execute(Database.BatchableContext bc, List<SObject> scope){
        
        /* Step 2: Questionnaire query batch size one*/
        Questionnaire__c currentQuestionnaire;
        Date reportingDate = Date.newInstance(reportingYear, reportingMonth, reportingDay);
        currentQuestionnaire = (Questionnaire__c)scope[0];
        
        /* Step 3: Get all today's Task where Task.WhatId = currentQuestionnaire*/
        List<Task> tasks = StagingReportUtil.queryTodayTaskByQuestionnaire2(reportingDate, currentQuestionnaire.Id);
        
        //Query Contacts with sales area info into Map
        Map<Id,Contact> contactMap = StagingReportUtil.queryContactsWithSalesAreaFromTasks(tasks);

        //Query Answers by Contacts
        Map<Id,List<Answer__c>> contactIdAnswers 
                    = StagingReportUtil.queryAnswersByContacts(contactMap.keySet());
        
        //Iniitialise rows - to change to map to improve performace
        //List<Welcome_Call_Staging__c> rows = new List<Welcome_Call_Staging__c>();
        Map<String, Welcome_Call_Staging__c> rows = new Map<String, Welcome_Call_Staging__c>();
        
        if (tasks!=null){
        	
        	//一个Contact有多个Task，由Task找Contact再找Answer，可能导致同一个Contact及对应Answer被多次统计
            Set<Id> Set_ContactId = new Set<Id>();
            
            //for each task
            for (Task thisTask : tasks){
                //get contact related to task
                Contact thisContact = contactMap.get(thisTask.WhoId);
                String thisSpecificCode = thisContact.Specific_Code__c;
                Decimal thisQuestionnaireNo = currentQuestionnaire.Questionnaire_No__c; //thisContact.Questionnaire__r.Questionnaire_No__c;
                String thisCityAreaCode = thisContact.Account.Standard_City__r.Citycode_AAreacode__c;
                //create, retrieve row based on specific code, questionnaire, day, month, year
                Welcome_Call_Staging__c row = StagingReportUtil.getWelcomeCallStaging(
                    rows,
                    thisSpecificCode,
                    thisQuestionnaireNo,
                    thisCityAreaCode,
                    reportingYear,
                    reportingMonth, 
                    reportingDay
                );
                
                //update row
                row.Region__c = thisContact.Account.Standard_City__r.Region__c;
                row.Sub_Region__c = thisContact.Account.Standard_City__r.Sub_Region__c;
                row.Sales_City__c = thisContact.Account.Standard_City__r.Sales_City__c;
                row.Area__c = thisContact.Account.Standard_City__r.Area__c;
                row.Source__c = thisContact.Primary_RecruitChannel_Sub_type__c;
                row.Channel_Name__c = thisContact.Channel_Name__c;
               /*
                if(Set_ContactId.contains(thisContact.Id))
                {
                	continue;
                }
                else
                {
                	Set_ContactId.add(thisContact.Id);
                }*/
                
                //名单数 , 废卡数 , 成功致电数
                row.No_of_contacts__c ++;
                if (thisTask.Result__c == 'Inactive'){
                    row.Invalid_Card__c ++; 
                }else if (thisTask.Result__c == 'Connected/Closed'){
                    row.Total_Successful_Call__c ++;
                }
                
                List<Answer__c> thisContactAnswers = contactIdAnswers.get(thisContact.Id);
               
                /*====一个联系人一天可以有多个answer 2014-7-15注释--Start */
                //Answer__c thisAnswer = StagingReportUtil.getCurrentAnswer(thisContactAnswers,reportingDate);
                /*2014-7-15注释--End*/
                /*2014-7-15调整*/
                if(thisContactAnswers==null)
                	continue;
                
                for(Answer__c thisAnswer : thisContactAnswers)
                {
                    if (thisAnswer.Questionnaire__r.Questionnaire_No__c != thisQuestionnaireNo)
                        continue;
                	DateTime d = thisAnswer.Finished_On__c;
                    if (d == null)
                        continue;
            		Date finishedOnDate = Date.newInstance(d.year() , d.month() , d.day() );
            		if(finishedOnDate != reportingDate)
                        continue;
            		
            		//成功致电品牌- Q008-现有品牌 (16)
                    String currentBrand = thisAnswer.Q008__c;
                    StagingReportUtil.setSurveyValues(
                            row, currentBrand,true,
                            new String[]{
                                    'Abbott',
                                    'Beingmate',
                                    'Biostim',
                                    'Um Belle',
                                    'Dumex',
                                    'Friso',
                                    'Meiji',
                                    'MJN',
                                    'Nescafe',
                                    'Others(180Above)',
                                    'Others(180Below)',
                                    'Synutra',
                                    'Wyeth',
                                    'Yashili',
                                    'Yili',
                                    'Karicare',
                                    'MJN(GC)'  },
                            new String[]{
                                'Connected_Call_Brand_Abbott__c',
                                'Connected_Call_Brand_BeiYinMei__c',
                                'Connected_Call_Brand_Biostime__c',
                                'Connected_Call_Brand_Cow_Gate__c',
                                'Connected_Call_Brand_Dumex__c',
                                'Connected_Call_Brand_Friso__c',
                                'Connected_Call_Brand_Meiji__c',
                                'Connected_Call_Brand_MJ_A__c',
                                'Connected_Call_Brand_Nestle__c',
                                'Connected_Call_Brand_Other_High_end__c',
                                'Connected_Call_Brand_Other_Low_end__c',
                                'Connected_Call_Brand_Shengyuan__c',
                                'Connected_Call_Brand_Wyeth__c',
                                'Connected_Call_Brand_Yashili__c',
                                'Connected_Call_Brand_Yili__c',
                                'Connected_Call_Brand_Karicare__c',
                                'Connected_Call_Brand_MJN_GC__c'}
                        );
                    
                    //成功致电品牌-纯母乳 / 无奶制品 - Q026
                    String connectedCallBrand = thisAnswer.Q026__c;
                    StagingReportUtil.setSurveyValues(
                        row, connectedCallBrand, true,
                        new String[]{'Breast milk','None milk'},
                        new String[]{'Connected_Call_Brand_Breast_milk__c','Connected_Call_Brand_None_milk__c'}
                    );
                    
                    //收到样包品牌MJ(GC)  - Q023 = Y		
                    String receivedS0 = thisAnswer.Q023__c;
                    StagingReportUtil.setSurveyValues(
                        row, receivedS0, true,
                        new String[]{'Y'},
                        new String[]{'S0_Sample_Received_no__c'}
                    );
                    
                    //收到样包品牌 - Q017
                   String samplesReceived = thisAnswer.Q017__c;			
                   StagingReportUtil.setSurveyValues(
                        row, samplesReceived,true,
                        new String[]{'MJN','Abbott','Dumex','Meiji','Nescafe','Wyeth','Synutra','Yashili','Yili','Other'
                            ,'Beingmate' //this  in report column but in picklist
                            },
                        new String[]{
                            'Sample_received_MJ_A__c',  
                            'Sample_received_Abbott__c',
                            'Sample_received_Dumex__c',
                            'Sample_received_Meiji__c',
                            'Sample_received_Nestle__c',
                            'Sample_received_Wyeth__c',
                            'Sample_received_Shengyuan__c',
                            'Sample_received_Yashili__c',
                            'Sample_received_Yili__c',
                            'Sample_received_Others__c',
                            'Sample_received_Beingmate__c'},
                       'Total_Sample_received__c'
                    );
                    	
                    //参加妈妈班的人数 -- Q001 = Y
                    String mamaAttendeeNum = thisAnswer.Q001__c;
                    StagingReportUtil.setSurveyValues(
                        row, mamaAttendeeNum, true,
                        new String[]{'Y'},
                        new String[]{'New_Mom_Class_attendee_number__c'}
                    );
                    //参加妈妈班品牌
                    String mamaClass = thisAnswer.Q004__c;
                    StagingReportUtil.setSurveyValues(
                        row, mamaClass,true,
                        new String[]{'MJN','Wyeth','Abbott','Dumex','Nescafe','No consider','Synutra','Yili','Yashili','Others'},
                        new String[]{
                            'Mom_Class_MJ_A__c',
                            'Mom_Class_Wyeth__c',
                            'Mom_Class_Abbott__c',
                            'Mom_Class_Dumex__c',
                            'Mom_Class_Nestle__c',
                            'Mom_Class_No_consider__c',
                            'Mom_Class_Shengyuan__c',
                            'Mom_Class_Yili__c',
                            'Mom_Class_Yashili__c',
                            'Mom_Class_Others__c'},
                        'Total_Mom_Class__c'
                    );
                    
                    
                    //使用妈妈奶粉人数 Q028-使用孕妇奶粉
                    String UsePregnantWomenMilk = thisAnswer.Q028__c; 
                    StagingReportUtil.setSurveyValues(
                        row, UsePregnantWomenMilk,false,
                        new String[]{'Y'},
                        new String[]{'Total_PregnantWomenMilk_Use__c'}
                    );
                    
                    //使用Q024-孕妇奶粉品牌
                    String PregnantWomenMilk = thisAnswer.Q024__c;
                    StagingReportUtil.setSurveyValues(
                        row, PregnantWomenMilk,true,
                        new String[]{'MJ','Wyeth','Abbott','Dumex','Nescafe','Friso','Biostim','Um Belle','Karicare','Other'},
                        new String[]{
                        	'Pregnant_Women_MJ__c',
                        	'Pregnant_Women_Wyeth__c',
                        	'Pregnant_Women_Abbott__c',
                        	'Pregnant_Women_Dumex__c',
                        	'Pregnant_Women_Nescafe__c',
                        	'Pregnant_Women_Friso__c',
                        	'Pregnant_Women_Biostim__c',
                        	'Pregnant_Women_Um_Belle__c',
                        	'Pregnant_Women_Karicare__c',
                        	'Pregnant_Women_Other__c'},
                        'Total_Pregnant_Women__c'
                    );
                }
                
            }//end for each task
        }//end if task != null
        try{
            upsert rows.values() External_ID__c;
        }catch (Exception ex){
            system.debug(system.LoggingLevel.ERROR, ex.getMessage());
            system.debug(system.LoggingLevel.ERROR, ex.getStackTraceString());
            throw ex;
        }	
        
    }
    
    /*Finish*/
    global void finish(Database.BatchableContext bc){
        if (chain){
            Date nextDate = Date.newInstance(reportingYear, reportingMonth, reportingDay).addDays(1);
            
            system.scheduleBatch(new BatchBuildWelcome2(questionnaireQuery, nextDate.year(), nextDate.month(), nextDate.day(),endDate), 
                                 'Report 3-'+nextDate, 
                                 1, //start in one minute
                                 1); //batch size 1
        }
        
        
    }
}