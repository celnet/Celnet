public with sharing class searchComponentController extends ComponentControllerBase{
    
    /* Query String shared by two queries below */
    private static String COMMON_QUERY_STRING { get{        
        //acount fields
        String q = 'SELECT contact.account.name, contact.account.account_email__c, contact.account.inactive__c, ';
        q +=  ' contact.account.mobile_phone__c, contact.Account.Home_Phone__c, contact.account.address_label__c, ';
        q +=  ' contact.Account.Other_Phone_1__c, contact.Account.Other_Phone_2__c ,contact.Other_Name__c, ';
        //contact....
        q += ' contact.Name, contact.chinese_name__c, contact.birthdate, Contact.LastName, ';
        q += ' contact.recordType.DeveloperName, contact.Is_Primary_Contact__c ';               
        q += ' FROM Contact WHERE ';
        
        //Switch to RecordTypeId to utilize index - Ian Huang 21/10/2014
        q += ' Contact.Account.RecordTypeId =\'01290000000XHmL\' ';  
        //q += ' contact.account.recordType.DeveloperName =\'HouseHold\' ';   
        return q;
        }
    }
    
    public Lead lead{get;set;}
    
    private Map<String,SearchFieldWrapper> searchFieldsMap;
    public List<SearchFieldWrapper> searchWrapperList {
        get{
            if (searchWrapperList==null){
                setupSearchFields();
            }
            return searchWrapperList;
        }
        set;
    }
    
    public String searchOperator{get;set;}
    public List<SelectOption> searchOperatorOptions {
        get{
            if (searchOperatorOptions == null){
                searchOperatorOptions = new List<SelectOption>{
                    new SelectOption('OR','OR'),
                    new SelectOption('AND','AND')
                    
                    };
                searchOperator='OR';
            }
            return searchOperatorOptions;
        }
        set;
    }
    
  
    public void setupSearchFields(){
        if (lead==null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Lead ID not found / invalid ID'));
            return;
        }
       searchWrapperList = new List<SearchFieldWrapper>();
       // put in list for ordered and use in custom component accessing.
       searchWrapperList.add(new SearchFieldWrapper(
                'Phone', true, new List<String>{
                    nonNullify(lead.Home_Phone__c),
                    nonNullify(lead.MobilePhone),
                    nonNullify(lead.Other_Phone_1__c),
                    nonNullify(lead.Other_Phone_2__c)
                    },
                     new List<String>{
                        'contact.Account.Home_Phone__c',
                        'contact.Account.Mobile_Phone__c',
                        'contact.Account.Other_Phone_1__c',
                        'contact.Account.Other_Phone_2__c'
                     }                    
                    ) ); 
       searchWrapperList.add(new SearchFieldWrapper(
                'Email', true, new List<String>{
                        nonNullify(lead.Email)},
                     new List<String>{
                        'contact.Account.Account_Email__c'
                     }                    
                    ) );
       searchWrapperList.add(new SearchFieldWrapper(
                'Parent Name', true, new List<String>{
                nonNullify(lead.Name),
                nonNullify(lead.Chinese_Name__c),
                nonNullify(lead.Other_Name__c)
                },
                     new List<String>{
                        'contact.Name',
                        'contact.Chinese_Name__c',
                        'contact.Other_Name__c',
                        'contact.Account.Name'
                     }                    
                    ) );
        searchWrapperList.add(new SearchFieldWrapper(
                'Child Name', false, new List<String>{
                concatName(lead.First_Name_1st_Child__c,lead.Last_Name_1st_Child__c)
                ,concatName(lead.First_Name_2nd_Child__c,lead.Last_Name_2nd_Child__c)
                ,concatName(lead.First_Name_3rd_Child__c,lead.Last_Name_3rd_Child__c)
                ,concatName(lead.First_Name_4th_Child__c,lead.Last_Name_4th_Child__c)
            
                },
                     new List<String>{
                        'contact.Name',
                        'contact.Other_Name__c'
                     }                    
                    ) );
        searchWrapperList.add(new SearchFieldWrapper(
                'Child Chinese Name', false, new List<String>{
                nonNullify(lead.Chinese_Name_1st_Child__c)
                ,nonNullify(lead.Chinese_Name_2nd_Child__c)
                ,nonNullify(lead.Chinese_Name_3rd_Child__c)
                ,nonNullify(lead.Chinese_Name_4th_Child__c)
                },
                     new List<String>{
                        'contact.Chinese_Name__c'
                     }                    
                    ) );
        
         //put everything in a map
         searchFieldsMap = new Map<String,SearchFieldWrapper>();
         for (SearchFieldWrapper wrapper : searchWrapperList){
                searchFieldsMap.put(wrapper.searchKey,wrapper);
         }
       
    }
    
    
    private String concatName(String firstName,String lastName){
        firstName = (firstName==null) ? '' : firstName;
        lastName = (lastName==null) ? '' : lastName;
         
        return firstName+' '+lastName;
    }
    //To make a search text box always appear even if field value was null
    private String nonNullify(String s){
        return (s==null) ? ' ' : s;
    } 
    
    /* search term map, key = field, values separated by |, for search term highlighting
    * E.g key = Name, value = |tom|jerry|
    */
    public Map<String,String> getSearchTerms(){
        Map<String,String> searchTermsMap = new Map<String,String>();
        for (String key : searchFieldsMap.keySet()){
            String terms = '|';
            //need to skip for unchecked.
            if (searchFieldsMap.get(key).checked){
                for (String term : searchFieldsMap.get(key).values){
                     terms += term + '|';
                }
            }
            searchTermsMap.put(key,terms);
        }
        return searchTermsMap;
    }
    
    
    public HouseholdWrapper[] doSearch(){
        //Validate at least one checkbox
        boolean checked = false;
        for (String key : searchFieldsMap.keySet()){
            if (searchFieldsMap.get(key).checked){
                checked = true;
                break;
            }
        }
        if (!checked){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select at least one search criteria.'));
            return null;
        }
        
        Map<Id,Contact> firstResults =  executeFirstQuery();
        List<Contact> results = executeSecondQuery(firstResults);
        
        //build map of households
        Map<Id,HouseholdWrapper> hhmap = new Map<Id,HouseholdWrapper>();
        Map<String,String> searchTerms =  getSearchTerms();
        for (Contact c : results){
            HouseholdWrapper wrapper = hhmap.get(c.account.id);
            if (wrapper==null){
                wrapper = new HouseholdWrapper();
                wrapper.addHousehold(c.account);
                hhmap.put(c.account.id,wrapper);
            }
            if (c.recordType.DeveloperName == 'Household_Child'){
                wrapper.addBaby(c);
            }else if (c.recordType.DeveloperName == 'Household_Parent'){
                wrapper.addParent(c);
            }
        }

        return hhmap.values();
    }//end doSearch();
   
    /*
    This second query is needed in case the matching contacts from the first query are not primary contacts,
    hence the other contacts in the same household would not be retrieved from the first query.
    */
    
    private List<Contact> executeSecondQuery(Map<Id,Contact> firstResults){
        Set<Id> foundContactIds = firstResults.keySet();
        List<Contact> foundContacts = firstResults.values();
        Set<Id> accountIdToRetrieve = new Set<Id>();
        
        for (Contact c : foundContacts){
            if (c.Is_Primary_Contact__c == false){
                accountIdToRetrieve.add(c.AccountId);
            }
        }
        if (!accountIdToRetrieve.isEmpty()){
            String q = COMMON_QUERY_STRING;
            //Apply Filters
            q += ' AND contact.Id NOT IN :foundContactIds ';
            q += ' AND contact.AccountId IN :accountIdToRetrieve ';
            //Set a limit just in case
            q += ' LIMIT 500';
            
            system.debug('Search Query 2:'+q);
            List<Contact> secondResults = Database.query(q);
            if (secondResults != null)
                foundContacts.addAll(secondResults);
        }
        
        return foundContacts;
    }
    
    /*
    * Build Query and whereClause helper
    */
    
    private Map<Id,Contact> executeFirstQuery(){
        List<String> phoneList, emailList, parentNameList, childNameList, childChineseNameList;
        String q = COMMON_QUERY_STRING;
        // FILTER....
       
        boolean firstFilter = true; 
        String[] keys = new String[]{'Phone','Email','Parent Name','Child Name','Child Chinese Name'};
        String[] bindVars = new String[]{'phoneList','emailList','parentNameList','childNameList','childChineseNameList'};
        String[] recordTypes = new String[]{null,null,'Household_Parent','Household_Child','Household_Child'};
        String[] recordTypeIds = new String[]{null,null,'01290000000XBD0','01290000000XBCl','01290000000XBCl'};
       
        phoneList = removeEmptyStrings(searchFieldsMap.get('Phone').values);
        emailList = removeEmptyStrings(searchFieldsMap.get('Email').values);
        parentNameList = removeEmptyStrings(searchFieldsMap.get('Parent Name').values);
        childNameList = removeEmptyStrings(searchFieldsMap.get('Child Name').values);
        
        childChineseNameList = searchFieldsMap.get('Child Chinese Name').values;
       
        for (Integer i=0;i<keys.size();i++){
            String key = keys[i];
            if (searchFieldsMap.get(key).checked){
                //insert AND at the front for the very first clause
                if (firstFilter){
                    q += ' AND (';
                    firstFilter = false;
                }else{
                    q += ' '+searchOperator + ' ';
                }
                String clause = whereClause(bindVars[i],searchFieldsMap.get(key).querySearchFields);
                //if record type is specified, add preceeding condition
                if (recordTypes[i]!=null){
                    //q += ' ( contact.recordType.DeveloperName = \''+ recordTypes[i]+'\'  AND '+clause+' ) ';
                    q += ' ( Contact.RecordTypeId = \''+ recordTypeIds[i]+'\'  AND '+clause+' ) ';
                }else{
                    q += clause;
                }
            }
        }
        if (!firstFilter){
            q += ')';
        }
        //Set a limit just in case
        q += ' LIMIT 500';
        
        system.debug('Search Query 1:'+q);
        
        return  new Map<Id,Contact>((List<Contact>) Database.query(q) );
    }
    
    private String[] removeEmptyStrings(String[] values){
        List<String> out = new List<String>();
        for (String s : values){
            if (s!=null && s.trim() != ''){
                out.add(s);
            }
        }
        return out;
    }
    
    private String whereClause(String placeholder, List<String> fieldNames){
        String s;
        boolean isFirst = true;
        s = '(';
        for (String name : fieldNames){
            String orOp = (isFirst) ? '' : 'OR';
            s += String.format(' {0} {1} IN :{2} ', 
                new List<String>{orOp ,name, placeholder});
            
            isFirst = false;
        }
        s += ')';
        return s;
    }
    
    
    /**
    * Search field Wrapper
    */
   
    public class SearchFieldWrapper{
        
        public boolean checked{get;set;}
        public String searchKey{get;set;}
        public final List<String> values {get;set;}
        public final List<String> querySearchFields {get;set;}
            
        
        public searchFieldWrapper(String searchKey, Boolean checked, List<String> values, List<String> fields){
                this.searchKey = searchKey; 
                this.values = values;
                this.checked = checked;
                this.querySearchFields = fields;
                
        }
        
        
        public List<Integer> iterator{
            get{
                List<Integer> iter = new List<Integer>();
                Integer i = 0;
                for (String v : values){
                    if (v!='' && v!=null){
                        iter.add(i++);
                    }
                }
                return iter;
            }
        }
    }
    
    /**
    * 
    */

}