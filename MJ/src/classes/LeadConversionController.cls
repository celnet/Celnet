public with sharing class LeadConversionController extends PageControllerBase {
	
    public static final String LEAD_ID_NOT_FOUND = 'ID not found / invalid ID';
    public static final String LEAD_ALREADY_CONVERTED = 'Lead has already converted.';
    public final Lead lead;
    public Lead currentLead{  
    	get{
    		return currentLead;
    	}
    	private set;
    }
    
    private final LeadStaging staging;
    public boolean firstLoad{get;set;}
    public boolean pageError{get;set;}
    public LeadConvertResult leadConvertResult {get;set;}
    public boolean isLastFirstName;
  
    public LeadConversionController(ApexPages.StandardController standardController){
        Id id = ApexPages.currentPage().getParameters().get('id');
        List<Lead> result;   
        if (id!=null){
        	String query = 'SELECT';
			Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get('Lead').getDescribe().fields.getMap();
			// Grab the fields from the describe method and append them to the queryString one by one.
			for(String s : objectFields.keySet()) {
			   query += ' ' + s + ',';
			}
			// Strip off the last comma if it exists.
			if (query.subString(query.Length()-1,query.Length()) == ','){
			    query = query.subString(0,query.Length()-1);
			}
			query += ' FROM Lead  WHERE Id = :id';
			result = database.query(query);
        }
        if (result == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, LEAD_ID_NOT_FOUND));
            pageError=true;    
            firstLoad=false;
        }else if(result[0].IsConverted){
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, LEAD_ALREADY_CONVERTED));
            pageError=true;    
            firstLoad=false;
        }else{
            currentLead = result[0];
            //use an exisitng lead as reference
            isLastFirstName = LeadConversionHelper.isLastFirstName(currentLead);
            
            staging = new LeadStaging(currentLead,isLastFirstName);
            
            lead = currentLead.clone();
            if (firstLoad==null)
            	firstLoad=true;
            showNewHouseholdBlock = false;
            pageError = false;
        }
    }
    
    
    public SearchComponentController searchController {
    	set;
	    get{
	        //get the map, then the component controller using the appropriate key
	        if(getComponentControllerMap()!=null){
	          SearchComponentController scc;
	          scc = (SearchComponentController )getComponentControllerMap().get('searchFormController');
	          
	          if(scc!= null)
	             return scc;
	        }
	        return null; 
	    }
	  } 
    
    public PageReference doCancel(){
    	return new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
    }
    
    public List<LeadStaging.BabyWrapper> leadBabyWrapper {
    	get{
    		return staging.leadBabyWrapper;
   		}
    }
    public String leadParentName {
    	get{
    		return LeadConversionHelper.concatName(
    			isLastFirstName,
	    		staging.leadParent.FirstName, 
	    		staging.leadParent.LastName, 
	    		staging.leadParent.Chinese_Name__c
	    		);
    	}
    }
    
    public Account leadHousehold{
    	get{ return staging.leadHousehold; }
    }
   
    public Lead getLead() {
        return lead;
    }
    
    public void onloadDoSearch(){
    	if (firstLoad && !pageError){
    		doSearch();
    		firstLoad=false;
    	}
    }
	
    public void doSearch(){
    	List<HouseholdWrapper> results = searchController.doSearch();
    	if (results!=null){
	    	searchTerms = searchController.getSearchTerms();
	    	householdResults = new List<HouseholdWrapper>();
	        householdResults.addAll(results);
	        
	        sortResults();
	        resultCount = results.size();
	        newHouseholdSelected = (resultCount==0);
	        
	        if (results.size()>0){
	        	resultText = results.size() + ' possible duplicate(s) found.';
	        	showResultList = true;
	        	newHouseholdDisabled = false;
	        	newHouseholdSelected = false;
	        }else{
	        	resultText = 'No possible duplicate found.';
	        	showResultList = false;
	        	newHouseholdDisabled = true;
	        	newHouseholdSelected = true;
	        }
	        showNewHouseholdBlock = true;
    	}
    }
    
    private void sortResults(){
    	for (HouseholdWrapper wrapper : householdResults){
    		wrapper.updateRank(searchTerms);
    	}
    	householdResults.sort();
    }
    
	
	transient 	Map<String,String> searchTerms;
    transient   List<HouseholdWrapper> householdResults;
	
	public Map<String,String> getSearchTerms() {return searchTerms;}
    public List<HouseholdWrapper> getHouseholdResults(){return householdResults;}
    private Integer resultCount;
	
	
	public Boolean showNewHouseholdBlock{get;set;}
    public Boolean showResultList{get;set;}
	public String resultText{get;set;}
	public Boolean newHouseholdSelected{ get;set;}
  	public Boolean newHouseholdDisabled{get;set;}
  
  	public void onNewHouseholdSelectedChange(){
  		if (newHouseholdSelected){
  			showResultList = false;
  			resultText = resultCount + ' possible duplicate(s) found. <br/> Uncheck "Create New Household" below to view duplicates';
  		}else{
  			showResultList = true;
  			doSearch();
  			resultText = resultCount + ' possible duplicate(s) found.';
  		}
  	}
  	
  	public PageReference  doConvertLead(){
  		firstLoad = true;
  		//validate radio selections 
  		if (staging != null){  		
	  		if (newHouseholdSelected || (!newHouseholdSelected && staging.validateNonNewHouseholdSelections() ) ){
	  			LeadConvertResult result = LeadConversionHelper.convertStaging(staging);	
	  			if (result.isSuccess){
	  				leadConvertResult = result;
	  				return Page.LeadConversionResult.setRedirect(false);
	  			}else{
	  				//display error messages
	  				if (result.exceptionString != null){
	  					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, result.exceptionString));
	  				}
	  				if (result.databaseErrors != null){
	  					for (Database.Error e : result.databaseErrors){  
					      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.message));  
					    } 
	  				}
	  				pageError = true;
	  			}
	  		}
  		}
  		return null;
  	}
  	
  	
}