@isTest
private class SetControllerFactoryTest {
	static testMethod void contactControllerTest() {
		//insert one test contact
		Contact c = new Contact(lastname='Test Contact');
		insert c;
		//get contact setcontroller from factory class
 		ApexPages.Standardsetcontroller cont = SetControllerFactory.newContactController();
 		//assert type record return by setController is Contact
 		System.assert(cont.getRecord() instanceof Contact);
	}
	
	static testMethod void leadControllerTest() {
		//insert one test lead
		Lead l = new Lead(lastname='Test Contact');
		insert l;
		//get lead setcontroller from factory class
 		ApexPages.Standardsetcontroller cont = SetControllerFactory.newLeadController();
 		//assert type record return by setController is Lead
 		System.assert(cont.getRecord() instanceof Lead);
	}
	
}