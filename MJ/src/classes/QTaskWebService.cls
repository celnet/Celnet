/*
Author: scott.zhang@celnet.com.cn
Created On: 2014-05-09
Function: User can mass Close Q-Task by Manual
Apply To: CN
*/
global class QTaskWebService {
	private static final String ClosedStatus ='Closed';
	
	webService static String CloseQTask(list<id> ids) 
	{
		String result = 'success';
        try
        {
        	List<Q_Task__c> List_UpTask = new List<Q_Task__c>();
        	for(Q_Task__c Qtask :[select Contact__c,Status__c from Q_Task__c where id in:ids])
        	{
        		Qtask.Status__c = ClosedStatus;
        		Qtask.ID__c = Qtask.Contact__c + '-Closed' + String.valueOf(DateTime.now());
        		List_UpTask.add(Qtask);
        	}
        	update List_UpTask;
        }catch(Exception e)
        {
        	 return 'Error: '+e.getmessage()+'  '+e.getLineNumber();
        }
        return result;
    }

}