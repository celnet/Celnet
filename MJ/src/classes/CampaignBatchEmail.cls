public with sharing class CampaignBatchEmail {
	
	public String status;
	public String createdCampaignName;
	public String createdCampaignId;
	public Integer memberCount;
	
	public String masterCampaignId;
	public String masterCampaignName;
	public String schedulerId;
	public String listViewName;
	
	public String jobId;
	public String startDateTime;
	public String endDateTime;
	public String exceptionMessage;
	
	private boolean emailSent;
	private String createdByEmail;
	private String modifiedByEmail;
	
	public CampaignBatchEmail(Campaign_Scheduler__c currentJobSetting){		
				
		schedulerId = String.valueOf(currentJobSetting.Id);
	    masterCampaignId = String.valueOf(currentJobSetting.Master_Campaign__c);
	    listViewName = currentJobSetting.List_view_Name__c;
	    
	    emailSent = false;
	    createdByEmail = currentJobSetting.CreatedBy.Email;
	    modifiedByEmail = currentJobSetting.LastModifiedBy.Email;
	}
	
	
	private List<String> getToAddresses(){
		List<String> emails = new List<String>();
		
		emails.add(createdByEmail);
		
		if (createdByEmail != modifiedByEmail)
		emails.add(modifiedByEmail);
		
		return emails;
	}
	
    public void sendEmail(){
    	if (emailSent)
    		return;
    		
    	try
       	{                
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
          
           mail.setToAddresses(getToAddresses());
           //Only send email to Sys Admin for failed jobs.
           if (status=='Failed'){
          	 mail.setCcAddresses(CommonHelper.getAdminEmailAddrs());
           }

           mail.setSubject('[Scheduled Campaign]:' +  ((createdCampaignName==null)?'':createdCampaignName)  +' | Status: ' +status);
           
           String htmlBoby = '<HTML><body>';
           htmlBoby += '<b>Scheduled Campaign Status:</b> '+ status + '<br/><br/>';
           htmlBoby += '<b>Created Campaign Name:</b> '+ getLink(createdCampaignName,createdCampaignId) + '<br/>';
           htmlBoby += '<b>List View Name:</b> '+ listViewName + '<br/>';
           htmlBoby += '<b>Campaign Member Count:</b> '+ ((memberCount==null)?'N/A': String.valueOf(memberCount))+ '<br/><br/>';
           
		   htmlBoby += 'Debug Info:<br/>';
		   htmlBoby += 'Job Start Time: '+ startDateTime + '<br/>';
           htmlBoby += 'Job End Time: '+ endDateTime + '<br/><br/>';
           htmlBoby += 'Campaign Scheduler Id:  '+ getLink(schedulerId,schedulerId) + '<br/>';
           htmlBoby += 'Job Id: '+ jobId + '<br/>';
           htmlBoby += 'Master Campaign: '+ getLink(masterCampaignName,masterCampaignId) + ' <br/>';
		   htmlBoby += 'Exception Message (if any): '+ ((exceptionMessage==null)?'N/A': exceptionMessage)+ '<br/><br/>';
		   
           htmlBoby += '</body></html>';
           mail.setHtmlBody(htmlBoby);
           
           Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
           emailSent=true;
        }
        catch(System.EmailException ex)
        {
          system.debug('Sending Email Failed :' + ex.getMessage());
        }
    }
    
    private String getLink(String text, String ID){
    	if (ID == null)
    		return 'N/A';
        String url = System.URL.getSalesforceBaseURL().toExternalForm() + '/'+Id;
        return '<a href="'+url+'">'+text+'</a>';
   }
}