/*Author:leo.bi@celnet.com.cn
 *Created On:2014-5-15
 *function:test class of RoutineEvaluator
 *Apply To:CN
 */
@isTest  
private class RoutineEvaluatorTest {
 
 	static testMethod void testRest()
    {
    	system.runAs(TestUtility.new_CN_User())
		{
	    	List<Contact> list_RestCon = new List<Contact>();
	    	Contact restCon = new Contact();
	    	restCon.LastName = 'restCon1';
	    	restCon.Inactive__c = false;
	    	restCon.Cannot_Be_Contacted__c = false;
	    	restCon.Birthdate = Date.today().addDays(-1000);
	    	restCon.Routine_Evaluated_On__c  = Date.today();
	    	list_RestCon.add(restCon);  
	    	Contact trueCon = new Contact();
	    	trueCon.Inactive__c = false;
	    	trueCon.LastName = 'restCon2';
	    	trueCon.Cannot_Be_Contacted__c = false;
	    	trueCon.Birthdate = Date.today().addDays(-50);
	    	trueCon.Routine_Evaluated_On__c  = Date.today();
	    	trueCon.Last_Routine_Call_Time__c = DateTime.now().addMonths(-3);
	    	list_RestCon.add(trueCon); 
	    	insert list_RestCon;
	    	Questionnaire__c que = new Questionnaire__c();
	    	que.Name = 'Test Rest';
	    	que.Business_Type__c ='Routine Reciprocal';
	    	que.Start_age__c = 20;
	        que.Type__c = 'Routine';
	    	insert que;
	    	RoutineEvaluator restRE = new RoutineEvaluator(list_RestCon,null);
	    	restRE.Run();
		}
    }
    
     static testMethod void myUnitTest() {
        system.runAs(TestUtility.new_CN_User())
		{
	        /*1 greet
	         *1.1 S0  －∞  0 ques.Start_age__c == null && ques.End_age__c != null && AgeDays<ques.End_age__c
	         *1.2 S0  －∞  0 ques.Start_age__c != null && ques.End_age__c != null && AgeDays >= ques.Start_age__c && AgeDays <= ques.End_age__c
	         *1.3 more than 6 months
	         *2 Reciprocal
	         *2.1 has greet call
	         *2.1.1 between one month  
	         *2.1.2 not between one month
	         *2.2.1 has questionnaire
	         *2.2.1 has no questionnaire
	         *2.3.1 has answer
	         *2.3.2 has no answer
	         *2.2 has no greet call
	         */
	        //1.1
	        //new Account
			Account acc = new Account();
			acc.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_HouseHold' and SobjectType=:'Account'].id;
			acc.Name = 'HouseHold';
			acc.Last_Name__c ='HouseHold';
			acc.status__c = 'Mother';
			insert acc;
			
			Id ConRecordTypeId = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
			List<Contact> List_Con = new List<Contact>();
			//1.6 has no qustionnaire
			Contact con16 = new Contact();
			con16.RecordTypeId = ConRecordTypeId;
			con16.AccountId = acc.Id;
			con16.LastName = 'con16';
			con16.Birthdate = Date.today().addDays(-5);
			con16.Register_Date__c = Date.today().addDays(-10);
			con16.Inactive__c = false;
			//insert con16;
			List_Con.add(con16);
			RoutineQTaskDistributionBatch rqdbwrong = new RoutineQTaskDistributionBatch(); 
			DataBase.executeBatch(rqdbwrong);
	        //new Contact
	        Contact con = new Contact();
			con.RecordTypeId = ConRecordTypeId;
			con.AccountId = acc.Id;
			con.LastName = 'conLast';
			con.Birthdate = Date.today().addDays(-5);
			con.Register_Date__c = Date.today().addDays(-3);
			con.Inactive__c = false;
			//insert con;
			List_Con.add(con);
			//greet questionaire
			Questionnaire__c greet1 = new Questionnaire__c();
			greet1.Name = 'greet1';
			greet1.Start_age__c = null;
			greet1.End_age__c = 10;
			greet1.Type__c = 'Routine';
	        greet1.Business_Type__c = 'Routine Greet'; 
			insert greet1;
			//1.2
	        //new Contact
	        Contact con12 = new Contact();
			con12.RecordTypeId = ConRecordTypeId;
			con12.AccountId = acc.Id;
			con12.LastName = 'conLast12';
			con12.Birthdate = Date.today().addDays(-20);
			con12.Register_Date__c = Date.today().addDays(-1);
			con12.Inactive__c = false;
			//insert con12;
			List_Con.add(con12);
			//greet questionaire
			Questionnaire__c greet2 = new Questionnaire__c();
			greet2.Name = 'greet1';
			greet2.Start_age__c = 1;
			greet2.End_age__c = 25;
			greet2.Type__c = 'Routine';
	        greet2.Business_Type__c = 'Routine Greet';
			insert greet2;
			//1.3 
			//new Contact
	        Contact con13 = new Contact();
			con13.RecordTypeId = ConRecordTypeId;
			con13.AccountId = acc.Id;
			con13.LastName = 'con13';
			con13.Birthdate = Date.today().addDays(-190);
			con13.Register_Date__c = Date.today().addDays(-180);
			con13.Inactive__c = false;
			//insert con13;
			List_Con.add(con13);
			//greet questionaire
			Questionnaire__c greet3 = new Questionnaire__c();
			greet3.Name = 'greet3';
			greet3.Start_age__c = 180;
			greet3.End_age__c = null;
			greet3.Type__c = 'Routine';
	        greet3.Business_Type__c = 'Routine Greet';
			insert greet3;
			//1.4 too old
	        Contact con14 = new Contact();
			con14.RecordTypeId = ConRecordTypeId;
			con14.AccountId = acc.Id;
			con14.LastName = 'con14';
			con14.Birthdate = Date.today().addDays(-5);
			con14.Register_Date__c = Date.today().addDays(-1000);
			con14.Inactive__c = false;
			//insert con14;
			List_Con.add(con14);
			//1.5 Inactive__c = true
			Contact con15 = new Contact();
			con15.RecordTypeId = ConRecordTypeId;
			con15.AccountId = acc.Id;
			con15.LastName = 'con15';
			con15.Birthdate = Date.today().addDays(-5);
			con15.Register_Date__c = Date.today().addDays(-1000);
			con15.Inactive__c = false;
			insert con15;
			con15.Inactive__c = true;
			con15.Inactive_Reason__c = 'Others';
			update con15;
	        //2.1.1 between one month 
	        //2.1.2 not between one month
	        //new Contact
	        Contact con212 = new Contact();
			con212.RecordTypeId = ConRecordTypeId;
			con212.AccountId = acc.Id;
			con212.LastName = 'con212';
			con212.Birthdate = Date.today().addDays(-5);
			con212.Register_Date__c = Date.today().addDays(-3);
			con212.Inactive__c = false;
			con212.Last_Routine_Call_Time__c = Date.today().addDays(-10);
			con212.Routine_Evaluated_On__c = Date.today().addDays(-2);
			//insert con212; 
			List_Con.add(con212);
			Questionnaire__c reciprocal = new Questionnaire__c();
			reciprocal.Name = 'Reciprocal';
			reciprocal.Business_Type__c = 'Routine Reciprocal';
			reciprocal.Start_age__c = 0;
			reciprocal.End_age__c = 100;
			reciprocal.Type__c = 'Routine';
			insert reciprocal;
			Task t = new Task();
			t.WhoId = con212.Id;
			t.WhoID__c = con212.Id;
			insert t;
	        //2.2.1 has questionnaire
	        //2.2.1 has no questionnaire
	        //2.3.1 has answer
	        //2.3.2 has no answer
	        //2.2 has no greet call
	        //2.3 too old
	        Contact con23 = new Contact();
			con23.RecordTypeId = ConRecordTypeId;
			con23.AccountId = acc.Id;
			con23.LastName = 'con23';
			con23.Birthdate = Date.today().addDays(-500);
			con23.Register_Date__c = Date.today().addDays(-3);
			con23.Inactive__c = false;
			con23.Routine_Evaluated_On__c = Date.today().addDays(-2);
			//insert con23;
			List_Con.add(con23);
			
			insert List_Con;
			Test.startTest();
			RoutineQTaskDistributionBatch rqdb = new RoutineQTaskDistributionBatch();
			DataBase.executeBatch(rqdb);
			Test.stopTest();
			//cont.Questionnaire__c = Routine.Id;
			RoutineQTaskDistributionBatch m = new RoutineQTaskDistributionBatch();
			String nowhour = String.valueOf(DateTime.now().hour());
			String nownimute = String.valueOf(DateTime.now().hour());
			String nowsecond = String.valueOf(DateTime.now().hour());
			String sch = nowsecond + ' ' + nownimute  + ' ' +nowhour  + ' * * ?';
			String jobID = system.schedule('Merge Job', sch, m);
		}
    }
}