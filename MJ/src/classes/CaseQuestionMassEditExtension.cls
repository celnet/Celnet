public class CaseQuestionMassEditExtension{

private List<Case_Question__c> questions;
    private List<Case_Question__c> originalList;
    private final Case parentCase;
    private Id HK_RT;
    
    public CaseQuestionMassEditExtension(ApexPages.StandardSetController controller) 
    {
        ID caseID = ApexPages.currentPage().getParameters().get('id');
        List<Case> cases = [SELECT ID,CaseNumber,Type FROM case WHERE ID=:caseID];
        HK_RT = ObjectUtil.GetRecordTypeID(Case_Question__c.SObjectType, 'Case_Question_HK');            
        if(cases == NULL || cases.isEmpty())
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Parent Case not found. Please make sure you are accessing from the right place!'));
        }
        else
        {
            parentCase = cases.get(0);
            questions = [SELECT ID,Type__c,Sub_Type__c,Question__c,Product__c,Remark__c
                         FROM Case_Question__c WHERE Case__c =: caseID ORDER BY CreatedDate ASC];
            if(questions == null)
                questions = new List<Case_Question__c>();
            
            //If number of existing records < 5, then set to 5
            addNewLines(5 - questions.size());
            
            //Save default lines
            originalList = questions.deepClone(true,true,true); 
        }
    }
    
    public List<Case_Question__c> getQuestions() {
        return questions;
    }
    
    public Case getParentCase() {
        return parentCase;
    }
    
    public List<SelectOption> getMJNProducts() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        List<Product__c> products = CommonHelper.getMJNProducts();
        for(Product__c product : products)
            options.add(new SelectOption(product.ID,product.Name));
        return options;
    }
    
    public PageReference save() {
        List<Case_Question__c> newList = new List<Case_Question__c>();
        List<Case_Question__c> updateList = new List<Case_Question__c>();
        
        for(Case_Question__c question : questions)
        {
            if(question.ID == NULL) //New case question
            {
               if(question.Type__c == NULL && question.Product__c == NULL && question.Remark__c == NULL)
               {
                   //Ignore empty lines - do nothing
               }
               else if(passValidation(question))
                   newList.add(question);
            }
            else //Existing case question record
            {  
                if(passValidation(question))
                   updateList.add(question);
            }
        }

        //If there is any validation error, return to the form page
        if(ApexPages.hasMessages())
            return null;
        
        try
        {
            if(!newList.isEmpty())
                insert newList;
            if(!updateList.isEmpty())
                update updateList;
        }
        catch(DmlException ex){
            ApexPages.addMessages(ex);
            return null;
        }
        
        //  Return to the Contact Detail page:
        return new PageReference('/'+parentCase.ID);  
    }
    
    public PageReference reset() {
        questions = originalList.deepClone(true,true,true);
        return null; 
    }
    
    public PageReference cancel() {
        //  Return to the Contact Detail page:
        return new PageReference('/'+parentCase.ID);  
    }
    
    public PageReference addNew() {
        //Add 3 lines in one click
        addNewLines(3);
        return null;  
    }
    
    private void addNewLines(Integer numerOfLines) {
        for(Integer count=0; count<numerOfLines; count++)
            questions.add(createNewQuestion());
    }
    
    private Boolean passValidation(Case_Question__c question)
    {
        Boolean result = false;
        
        if(question.Type__c != NULL && question.Sub_Type__c == NULL)
            question.Sub_Type__c.addError(CommonHelper.VALUE_REQUIRED);
        else if(question.Type__c != NULL && question.Sub_Type__c != NULL && question.Question__c == NULL)
            question.Question__c.addError(CommonHelper.VALUE_REQUIRED);
        else if(question.Type__c == 'Complaint' && question.Sub_Type__c != NULL 
                && question.Question__c != NULL && question.Product__c == NULL)
            question.Product__c.addError(CommonHelper.PRODUCT_REQUIRED_FOR_COMPLAINT);
        else
            result = true;
        
        return result;
    }
    
    private Case_Question__c createNewQuestion()
    {
        Case_Question__c question = new Case_Question__c();
        question.Case__c = parentCase.ID;
        //question.Type__c = parentCase.Type;
        question.RecordTypeId = HK_RT;
        return question ;
    }

}