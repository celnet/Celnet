/*
*Author:michael.mi@celnet.com.cn
*Date:2014-09-04
*Function:call out service 
*/
public class WechatCalloutService
{
	private string publicAccountName;
	private Wechat_Setting__c setting;
	
	public WechatCalloutService(string publicAccountName)
	{
		this.publicAccountName = publicAccountName;
	}  
	
	public WechatCalloutService()
	{   
		
	}
	
	public void SetPublicAccountName(string publicAccountName)
	{
		this.publicAccountName = publicAccountName;
	}
	
	public string GetOpenIdbyCode(string code)
	{
		string accessToken = getAccessToken();
		List<PostParameter> params = new List<PostParameter>();
		params.add(new PostParameter('appid', setting.App_Id__c));
		params.add(new PostParameter('secret' , setting.App_Secret__c));
		params.add(new PostParameter('code' , code));
		params.add(new PostParameter('grant_type' , 'authorization_code'));
		
		Httpresponse response = get(setting.API_Base_Url__c + '/sns/oauth2/access_token' , params);
		OpenIdAccess oia = (OpenIdAccess)Json.deserialize(response.getBody(), OpenIdAccess.class);
		system.debug('----------------------OpenIdAccess---------------' + oia);
		return oia.openid;
	}
	
	//send a msg to WeiXin
	public void SendMsg(string msg)
	{
		string accessToken = getAccessToken();
		system.debug('*******sendMsg********' + accessToken);
		HttpResponse response = post(setting.API_Base_Url__c + '/cgi-bin/message/custom/send?access_token=' + accessToken , msg);
		system.debug('--------------------response----------------' + response);
	}
	
	//get userInfo by openid
	public WechatEntity.User GetUserInfo(string openid)
	{
		string accessToken = getAccessToken();
		List<PostParameter> params = new List<PostParameter>();
		params.add(new PostParameter('access_token', accessToken));
		params.add(new PostParameter('openid' , openid));
		params.add(new PostParameter('lang' , 'zh_CN'));
		
		Httpresponse response = get(setting.API_Base_Url__c + '/cgi-bin/user/info' , params);
		
		return (WechatEntity.User)JSON.deserialize(response.getBody() , WechatEntity.User.class);
	}
	
	//Oauth to refresh AccessToken
	public AccessToken RefreshAccessToken()
	{
		string acc = getAccessToken();
		List<PostParameter> params = new List<PostParameter>();
		params.add(new PostParameter('grant_type', 'client_credential'));
		params.add(new PostParameter('appid' , setting.App_Id__c));
		params.add(new PostParameter('secret' , setting.App_Secret__c));
		Httpresponse response = get(setting.API_Base_Url__c + '/cgi-bin/token' , params);
		return (AccessToken)JSON.deserialize(response.getBody(), AccessToken.class);
	}
	
	//create menu
	public void CreateMenu(string Json)
	{
		string accessToken = getAccessToken();
		List<PostParameter> params = new List<PostParameter>();
		params.add(new PostParameter('access_token', accessToken));
		Httpresponse response = post(setting.API_Base_Url__c + '/cgi-bin/menu/create?access_token=' + accessToken, Json);
	}
	
	//delete menu
	public void DeleteMenu()
	{
		string accessToken = getAccessToken();
		List<PostParameter> params = new List<PostParameter>();
		params.add(new PostParameter('access_token', accessToken));
		Httpresponse response = get(setting.API_Base_Url__c + '/cgi-bin/menu/delete' , params);
	}
	
	//download media by media_id
	public WechatEntity.File DowloadMedia(string media_id)
	{
		system.debug('-------media_id-------------' + media_id);
		string accessToken = getAccessToken();
		List<PostParameter> params = new List<PostParameter>();
		params.add(new PostParameter('access_token', accessToken));
		params.add(new PostParameter('media_id' , media_id));
		Httpresponse response = get(setting.File_Api_Url__c + '/cgi-bin/media/get', params);
		WechatEntity.File fi = new WechatEntity.File();
		fi.body = response.getBodyAsBlob();
		fi.contentType = response.getHeader('Content-Type');
		fi.description = response.getHeader('Content-disposition');
		system.debug('*******Content-Type********' + fi.contentType);
		system.debug('*******Content-disposition********' + fi.description);
		return fi;
	}
	
	//check accessToken in the Wechat_Setting__c
	public string GetAccessToken()   
	{
		string accessToken = '';
		setting = [select App_Id__c , App_Secret__c , Access_Token1__c , Access_Token2__c , Access_Token3__c , API_Base_Url__c , File_Api_Url__c from Wechat_Setting__c where Name = :publicAccountName];
		accessToken = ((null != setting.Access_Token1__c ? setting.Access_Token1__c : '') + (null != setting.Access_Token2__c ? setting.Access_Token2__c : '') + (null != setting.Access_Token3__c ? setting.Access_Token3__c : ''));
		return accessToken;
	}	
	
	public void UploadNews()
	{
		
	}
	
	public void UploadMedia()
	{
		
	}
	
	//send a "GET" request
	public HttpResponse get(string url , List<PostParameter> params)
	{
		return execute('GET' , url , null , params);
	}
	
	//send a "POST" request
	public HttpResponse post(string url , string body)
	{
		return execute('POST' , url , body , null);
	} 
	
	//send a request ,and get the reponse
	public HttpResponse execute(string method , string url , string body , List<PostParameter> params)
	{
		Http h = new Http();						
		HttpRequest req = new HttpRequest();		
		req.setMethod(method);						
		req.setTimeout(60000);						
		system.debug('*********body**********' + body);
		if(method.equals('POST'))
		{
			req.setBody(body);
		}
		else
		{
			url = url +'?'+encodeParameters(params);
		}
		req.setEndpoint(url);
		HttpResponse response = h.send(req);
		system.debug('*********execute response***********' + response.getbody());
		if(response.getStatus() == 'OK' && response.getStatusCode() == 200 && response.getBody().contains('errmsg') && response.getBody().contains('errcode'))
		{
			system.debug('*********execute response***********' + response.getbody());
			WechatException.ErrorResponse er = (WechatException.ErrorResponse)JSON.deserializeStrict(response.getBody(),WechatException.ErrorResponse.class);
			if(er.errcode != 0 && er.errmsg != 'ok')    
			{
				throw new WechatException(er);	
			}
		}		
		system.debug('*****************new res******************'+response.getBody());
		return response;	
	} 
	
	// create url
	public static String encodeParameters(list<PostParameter> postParams) 
	{
		String buf = '';
		for (Integer j = 0; j < postParams.size(); j++) 
		{
			if (j != 0) 
			{
				buf+='&';
			}
			buf+=EncodingUtil.urlEncode(postParams[j].getName(), 'UTF-8');
			buf+='=';
			buf+=EncodingUtil.urlEncode(postParams[j].getValue(),'UTF-8');
		}
		return buf;
	}
	
	class PostParameter
	{
		String name;
	    String value;
	    
	    public PostParameter(String name, String value) 
	    {
	        this.name = name;
	        this.value = value;
	    }
	    public PostParameter(String name, double value) 
	    {
	        this.name = name;
	        this.value = String.valueOf(value);
	    }
	    public PostParameter(String name, integer value) 
	    {
	        this.name = name;
	        this.value = String.valueOf(value);
	    }
	    public String getName()
	    {
	        return name;
	    }
	    public String getValue()
	    {
	        return value;
    	}
	}
	
    public class AccessToken
    {
    	public string access_token;
    	public string expires_in;
    }
    
    public class OpenIdAccess
    {
    	public string access_token;
    	public string expires_in;
    	public string refresh_token;
    	public string openid;
    	public string scope;
    }
}