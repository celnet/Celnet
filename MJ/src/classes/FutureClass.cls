/*
 *The maximum number of future method invocations per a 24-hour period is 250,000 
  or the number of user licenses in your organization multiplied by 200, whichever is greater. 
 *
*/
global class FutureClass {

  @future 
  //CampaignMemberEventHandler :updateToCampaignMember
  public static void futureUpdateToCampaignMember( List<Id> CampaignMemberId,List<String> Last_Call_Result,List<DateTime> Last_Call_Time) 
  {
    List<CampaignMember> List_UpCamMem = new List<CampaignMember>();
		
		for(Integer i=0;i<CampaignMemberId.size();i++)
		{
			CampaignMember CamMem = new CampaignMember(id=CampaignMemberId[i],Last_Call_Result__c = Last_Call_Result[i],Last_Call_Time__c = Last_Call_Time[i]);
			List_UpCamMem.add(CamMem);
		}
		update List_UpCamMem; 
  }
  
  @future
  //QTaskEventHandler : taskUpdateCallResult
  public static void futureTaskUpdateCallResult(List<Id> List_QtId,List<String> Status,List<String> qtID,List<String> My_Last_Call_Result,List<DateTime> Last_Routine_Call_Time,List<String> Last_Routine_Call_Result)
  {
  	List<Q_Task__c> List_Upqt = new List<Q_Task__c>();
  	for(Integer i=0;i<List_QtId.size();i++)
  	{
  		if(qtID[i] != null)
  		{
  			Q_Task__c qta = new Q_Task__c(Id=List_QtId[i],Status__c=Status[i],ID__c=qtID[i],My_Last_Call_Result__c=My_Last_Call_Result[i],Last_Routine_Call_Time__c=Last_Routine_Call_Time[i],Last_Routine_Call_Result__c=Last_Routine_Call_Result[i]);
  			List_Upqt.add(qta);
  		}
  		else
  		{
  			Q_Task__c qta = new Q_Task__c(Id=List_QtId[i],Status__c=Status[i],My_Last_Call_Result__c=My_Last_Call_Result[i],Last_Routine_Call_Time__c=Last_Routine_Call_Time[i],Last_Routine_Call_Result__c=Last_Routine_Call_Result[i]);
  			List_Upqt.add(qta);
  		}
  		
  	}
  	update List_Upqt;
  }
  
}