/*Author:leo.bi@celnet.com.cn
 *Created On:2014-4-17
 *function:The Class used for sending SMS to Blue Focus and getting the returned result
 *Apply To:CN
 */
public with sharing class SMSProxy 
{
	public SMSReult sr = new SMSReult();
	public String getState(){return sr.state;}
	public String returnBody;
	//define a class to store the result after parsing xml
	class SMSReult
	{
		String state;
		String verifyState;
		String Phones;
	}
	public String testDebug()
	{
		return 'state is ' + sr.state + 'verifyState is' + sr.verifyState + 'Phones' + sr.Phones;
	}
	//send SMS
	public void send(String Mobile,String Content)
	{
		//send SMS
		HttpRequest req = new HttpRequest();
	    HttpResponse res = new HttpResponse(); 
	    Http http = new Http(); 
	    req.setEndpoint('http://svr.meadjohnson.com.cn/sfs/batchsmssend.action'); 
	    req.setMethod('POST'); 
	    if(Content != null)
	    {
	    	Content = this.formateContent(Content);
	    }
		req.setBody('CorpID=106575020437&Pwd=7534A8170A571687A15B61117C4A469A&Mobile=' + Mobile + '&Content=' + Content);
	    res = http.send(req);
		//Parse the result        
		this.returnBody = res.getBody();           
		if(returnBody != null && returnBody != '') 
		{
			this.parse(returnBody); 
		}
	} 
	
	private String formateContent(String originalContent)
	{
		String result = originalContent;
		//formate % to 525
		result = result.replace('%','%25');
		//formate + to %2B
		result = result.replace('+','%2B');
		//formate space to %20
		result = result.replace(' ','%20');
		//formate / to %2F
		result = result.replace('/','%2F');
		//formate ? to %3F
		result = result.replace('?','%3F');
		//formate & to %26
		result = result.replace('&','%26');
		//formate # to %23
		result = result.replace('#','%23');
		//formate = to %3D
		result = result.replace('=','%3D');
		return result;
	}
	
	//parse the xml returned from Blue Focus
	private void parse(String xmlString)
	{
		XmlStreamReader reader = new XmlStreamReader(xmlString);
		while(reader.hasNext())
		{
			if (reader.getEventType() != XmlTag.START_ELEMENT) 
			{
            	reader.next();
            	continue;
            }
            if(reader.getLocalName()=='state')
            {
               sr.state = parseCharacter(reader);
            }
            if(reader.getLocalName()!=null && reader.getLocalName() =='verifystate')
            {
                sr.verifyState = parseCharacter(reader);
            }
            if(reader.getLocalName()!=null && reader.getLocalName() =='Phones')
            {
                sr.Phones = parseCharacter(reader);
            }
        	reader.next();
		}
	}
	
	//parse the character in the xml
    private string parseCharacter(XmlStreamReader reader)
    {
         string value = '';
         while(reader.hasNext()) 
         {
            if (reader.getEventType() == XmlTag.END_ELEMENT) 
            {
               break;
            } 
            else if (reader.getEventType() == XmlTag.CHARACTERS)
            {
               value = reader.getText();
            }
            reader.next();
         }
         return value;
    }
}