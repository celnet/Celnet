/*
Author: tommyliu@celnet.com.cn
Created On: 2014-5-5
Function: 
	1. Upgrade Q-Task to Elite, when someone try to Update Q-Task form Routine to Invitation
	2. Upgrade Q-Task to Elite, when someone try to Update Q-Task form Invitation to Routine
	3. Upgrade Q-Task to Elite, when someone try to Update Q-Task 'Business Type' form Invitation to Invitation Special
Support Event: BeferInsert, BeforeUpdate
Apply To: CN
*/
public class QTaskUpgradeHandler implements Triggers.Handler
{
	public void Handle()
	{
		if(!Context.ApplyTo(new String[] {'CN'})) // make this code only apply to CN
    	{
    		return;
		}
		if(trigger.isUpdate && trigger.isBefore)
		{
			this.UpGrade();
		}
		
	}
	public void UpGrade()
	{
		List<Q_Task__c> qTaskList = trigger.new;
		
		for(Q_Task__c newQTask : qTaskList)
		{
			Q_Task__c oldQTask = (Q_Task__c)trigger.oldMap.get(newQTask.Id);
			//约束对于Q-Task，永远也不要同时修改Status && Type
			if(newQTask.Contact__c != oldQTask.Contact__c)
			{ 
				newQTask.addError('Cannot change Contact of Q-Task');
				continue;
			}
			if(newQTask.Status__c != oldQTask.Status__c && newQTask.Type__c != oldQTask.Type__c)
			{
				newQTask.addError('Cannot change Status and Type at same time');
				continue;
			}
			
			if((oldQTask.Status__c == 'Closed' || oldQTask.Status__c == 'Finished') && newQTask.Type__c != oldQTask.Type__c)
			{
				newQTask.addError('Cannot change type of Q-Task that Status is Closed or Finished');
				continue;
			}
			
			//===Up Grade
			//if try to change type to Invitation, then upgrade to Elite
			if(newQTask.Type__c == oldQTask.Type__c && newQTask.Business_Type__c == oldQTask.Business_Type__c)
			{
				continue;
			}
			//Demotion
			if(newQTask.Is_Demotion__c)
			{
				newQTask.Is_Demotion__c = false;
				continue;
			}
			
			
			if(oldQTask.Type__c == 'Routine' && newQTask.Type__c == 'Invitation')
			{
				newQTask.Type__c = 'Elite';//Upgrade to Elite
				newQTask.Business_Type__c =null;
			}
			else if(oldQTask.Type__c == 'Invitation' && newQTask.Type__c == 'Routine')
			{
				newQTask.Type__c = 'Elite';//Upgrade to Elite
				newQTask.Business_Type__c =null;
			}
			else if(oldQTask.Business_Type__c=='Invitation' && newQTask.Business_Type__c=='Invitation Special')
			{
				newQTask.Type__c = 'Elite';//Upgrade to Elite
				newQTask.Business_Type__c =null;
			}
			else if(oldQTask.Business_Type__c=='Invitation Special' && newQTask.Business_Type__c=='Invitation')
			{
				newQTask.Type__c = 'Elite';//Upgrade to Elite
				newQTask.Business_Type__c =null;
			}
			else if(oldQTask.Type__c=='Elite')
			{
				newQTask.Type__c = 'Elite';//Upgrade to Elite
				newQTask.Business_Type__c =null;
			}
		}
	}
}