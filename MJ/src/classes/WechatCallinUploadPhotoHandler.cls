//Author:Michael
//Date:2014-10-09
//Function:用户上传照片调用此Handler
public class WechatCallinUploadPhotoHandler extends WechatCallinMsgHandler
{
    public override void Handle(WechatCallinMsgPipelineContext Context)
    {
        string openId = Context.InMsg.FromUserName;
        WechatEntity.OutTextMsg outMsg = WechatEntity.GenerateOutMsg(Context.InMsg , '');
        WechatEntity.InEventMsg inEvent = (WechatEntity.InEventMsg)Context.InMsg;
        if(inEvent.EventKey != this.BindingInfo.Event_Key__c)
        {
            return;
        }
        string re = WechatBusinessUtility.CheckWechatUserRelationshipByFields(openId);
        system.debug('***********************' + openId);
        system.debug('***********************' + re);
        if(re == 'Member')
        {
            outMsg.Content = WechatBusinessUtility.QueryRemindingByKey('RN014');
        }
        else if(re == 'Non-Member')
        {
            WechatBusinessUtility.BirthCount bc = WechatBusinessUtility.GetBirthCertificateCount(openId);
            if(bc.birthCertificateCount == bc.hasBirthCount)
            {
                outMsg.Content = WechatBusinessUtility.QueryRemindingByKey('RN015');
            }
            else
            {
            	outMsg.Content = WechatBusinessUtility.QueryRemindingByKey('RN017',(bc.birthCertificateCount - bc.hasBirthCount));
            } 
        }
        else
        {
            string replaceStr = '<a href="'+WechatBusinessUtility.WECHAT_SITE+'WechatSMSVerificationPage?openId='+ openId +'">【註冊/綁定】</a>';
            outMsg.Content = WechatBusinessUtility.QueryRemindingByKey('RN018',replaceStr);
        }
        Context.OutMsg = outMsg;
    }
}