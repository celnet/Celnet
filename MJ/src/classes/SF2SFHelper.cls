public class SF2SFHelper{
    
    private static PartnerNetworkConnection sfaConnection;
    
    static
    {
        System.debug('In SF2SFHelper Static Code Block...');
        List<PartnerNetworkConnection> connMap = new List<PartnerNetworkConnection>(
    		[SELECT Id, ConnectionStatus, ConnectionName FROM PartnerNetworkConnection
        	 WHERE ConnectionStatus = 'Accepted' AND ConnectionName='Mead Johnson Nutrition (Thailand) Ltd' LIMIT 1]);
        
        if(connMap != NULL && !connMap.isEmpty())
        	sfaConnection = connMap[0];
    }
    
    public static boolean checkManageConnectionPermission(){
    	String currentUserId = UserInfo.getUserId();
    	Integer count = [Select count() from PermissionSetAssignment 
    	where PermissionSetID IN 
    	(SELECT Id from PermissionSet Where PermissionsManagePartnerNetConn = true)
    	And AssigneeId =:currentUserId];
    	
    	return (count != 0);
    }

    public static void submitOrderToSFA(ID orderID, Boolean sendEmail)
    {
        if(sfaConnection == NULL)
            throw new CommonHelper.MJNCustomException('Unable to estalish connection with SFA. Please contact your administrator!');
		else
        {
            PartnerNetworkRecordConnection newRecord = new PartnerNetworkRecordConnection();
            newRecord.ConnectionId = sfaConnection.Id;
            newRecord.LocalRecordId = orderID;  
            newRecord.RelatedRecords = 'Order_Item__c';
            newRecord.SendClosedTasks = false;
            newRecord.SendOpenTasks = false;
            newRecord.SendEmails = sendEmail;   
        
            insert newRecord; 
            
            //Update Order
            Order__c order = [SELECT ID FROM Order__c WHERE ID=:orderID];
            order.Push_Date__c = Datetime.now();
            update order;
        }
    }
    
    public static Boolean isOrderSubmitted(ID orderID)
    {
        Integer count = [SELECT COUNT() from PartnerNetworkRecordConnection
                         WHERE LocalRecordId =:orderID];
        if(count>0)
            return true;
        else
            return false;
    }
    
    public static Set<ID> getSubmittedOrders(List<Order__c> orderList)
    {
        Set<ID> submittedIDSet = new Set<ID>();
        List<PartnerNetworkRecordConnection> records = 
            [SELECT ID,LocalRecordId from PartnerNetworkRecordConnection
                         WHERE LocalRecordId in :orderList];
		
        if(records != NULL && !records.isEmpty())
        {
            for(PartnerNetworkRecordConnection record : records)
                submittedIDSet.add(record.LocalRecordId);
        }
        return submittedIDSet;
    }
    
    public static void submitOrdersToSFA(List<Order__c> orderList, Boolean sendEmail)
    {
        if(sfaConnection == NULL)
            throw new CommonHelper.MJNCustomException('Unable to estalish connection with SFA. Please contact your administrator!');
		else
        {
            List<PartnerNetworkRecordConnection> newRecordList = new List<PartnerNetworkRecordConnection>();
            for(Order__c order: orderList)
            {
                PartnerNetworkRecordConnection newRecord = new PartnerNetworkRecordConnection();
                newRecord.ConnectionId = sfaConnection.Id;
                newRecord.LocalRecordId = order.ID;  
                newRecord.RelatedRecords = 'Order_Item__c';
                newRecord.SendClosedTasks = false;
                newRecord.SendOpenTasks = false;
                newRecord.SendEmails = sendEmail;
                
                //Update order - Pushed Date
                order.Push_Date__c = Datetime.now();
                
                newRecordList.add(newRecord); 
            }
        
            insert newRecordList; 
            
            //Update Orders
            update orderList;
        }
    }
}