public class UniqueCodeGenerator{
    
    static final List<String> letters = new List<String> {'A','B','C','D','E','F','G','H','I','J','K','L','M','N',
                                                          'O','P','Q','R','S','T','U','V','W','X','Y','Z'};
                                                       
    public static String generateNextMemberID()
    {
        String curYear = String.valueOf(Date.today().year());
        
        //Get next Member ID running number from custom setting
        Member_ID_Setting__c memberSetting = Member_ID_Setting__c.getValues(curYear);
        if(memberSetting == NULL)
        {
            CommonHelper.MJNCustomException e = new CommonHelper.MJNCustomException('Member ID Running Number not found for Year: '+ curYear);
            CommonHelper.notifySystemException(e);
            throw e;
        }
        else
        {
            String newMemberID = '';
            Integer nextNumber = Integer.valueOf(memberSetting.Next_ID__c);
            //Member ID format will be "{c}YY0NNNNN"
            newMemberID = UniqueCodeGenerator.getRandomChar()+curYear.substring(2)+'0'
                         +UniqueCodeGenerator.leftPadZero(String.valueOf(nextNumber),5);
            
            //Increase the Member Running Number
            memberSetting.Next_ID__c = nextNumber +1;
            update memberSetting;
            return newMemberID;
        }
    }
    
    
    public static List<Iterator<Coupon__c>> bulkGenerateCouponByType(String couponType, Integer qty){
        List<Iterator<Coupon__c>> couponList = new List<Iterator<Coupon__c>>();
        String curYear = String.valueOf(Date.today().year());
        String YY = curYear.substring(2);
        
        //Get next Coupon ID running number from custom setting
        Coupon_ID_Setting__c couponSetting = Coupon_ID_Setting__c.getValues(couponType);
        if(couponSetting == NULL)
        {
            CommonHelper.MJNCustomException e = new CommonHelper.MJNCustomException('Coupon Running Number not found for type: '+ couponType);
            CommonHelper.notifySystemException(e);
            throw e;
        }
        else
        {
            Integer nextNumber = Integer.valueOf(couponSetting.Next_ID__c);
            List<String> postfixList = UniqueCodeGenerator.getPostfixSplits(couponSetting.Postfix__c);
            List<Coupon__c> couponsToBeInserted = new List<Coupon__c>();
         
            for(String postfix : postfixList)
            {
                List<Coupon__c> coupons = new List<Coupon__c>();
                for (Integer i = 0 ; i < qty; i++)
                {
                    String formattedNumber = UniqueCodeGenerator.leftPadZero(String.valueOf(nextNumber + i),6);
                    String newCouponID = couponSetting.Prefix__c + YY + formattedNumber + postfix;
                    
                    Coupon__c coupon = new Coupon__c();
                    coupon.Bar_Code__c = newCouponID;
                    coupons.add(coupon);
                }
                couponList.add(coupons.iterator());
                couponsToBeInserted.addAll(coupons);
            }
            
            //Create counpons
            insert couponsToBeInserted;
            
            //Update the Coupon Running Number
            couponSetting.Next_ID__c = nextNumber + qty;
            update couponSetting;
            
            return couponList;          
        }
    }
    
    
    /* For Before Insert Campaign Members */
    public static List<Iterator<Coupon__c>> bulkGenerateCouponForCampaignMembers(String couponType, List<CampaignMember> members, Map<ID,ID> contactAccountMap){
        List<Iterator<Coupon__c>> couponList = new List<Iterator<Coupon__c>>();
        String curYear = String.valueOf(Date.today().year());
        String YY = curYear.substring(2);
        
        //Get next Coupon ID running number from custom setting
        Coupon_ID_Setting__c couponSetting = Coupon_ID_Setting__c.getValues(couponType);
        if(couponSetting == NULL)
        {
            CommonHelper.MJNCustomException e = new CommonHelper.MJNCustomException('Coupon Running Number not found for type: '+ couponType);
            CommonHelper.notifySystemException(e);
            throw e;
        }
        else
        {
            Integer nextNumber = Integer.valueOf(couponSetting.Next_ID__c);
            List<String> postfixList = UniqueCodeGenerator.getPostfixSplits(couponSetting.Postfix__c);
            List<Coupon__c> couponsToBeInserted = new List<Coupon__c>();
            Integer qty = members.size();
            
            for(String postfix : postfixList)
            {
                List<Coupon__c> coupons = new List<Coupon__c>();
                for (Integer i = 0 ; i < qty; i++)
                {
                    CampaignMember member = members[i];
                    String formattedNumber = UniqueCodeGenerator.leftPadZero(String.valueOf(nextNumber + i),6);
                    String newCouponID = couponSetting.Prefix__c + YY + formattedNumber + postfix;
                    
                    Coupon__c coupon = new Coupon__c();
                    coupon.Bar_Code__c = newCouponID;
                    coupon.Coupon_Type__c = couponType;
                    if (member.LeadId != null){
                        coupon.Lead__c = member.LeadId;
                    }else {
                        ID accId = contactAccountMap.get(member.ContactId);
                        if (accId != null){
                            coupon.Account__c = accId;
                            coupon.Contact__c = member.ContactId;
                        }   
                    }
                    coupons.add(coupon);
                }
                couponList.add(coupons.iterator());
                couponsToBeInserted.addAll(coupons);
            }
            
            //DML insert counpons
            insert couponsToBeInserted;
            
            //Update the Coupon Running Number
            couponSetting.Next_ID__c = nextNumber + qty;
            update couponSetting;
            
            return couponList;
            
        }
    }
    
    /* Bulkified (by Coupon Type) for Fulfilment Coupons, Trigger Before Insert */
    public static List<Iterator<Coupon__c>> bulkGenerateFulfilmentCoupons(
                            String couponType, List<Fulfillment__c> fulfillmentList, Map<ID,ID> contactAccountMap){
         
        Integer qty = fulfillmentList.size();
        List<Iterator<Coupon__c>> couponList = new List<Iterator<Coupon__c>>();
        String curYear = String.valueOf(Date.today().year());
        String YY = curYear.substring(2);
        
        //Get next Coupon ID running number from custom setting
        Coupon_ID_Setting__c couponSetting = Coupon_ID_Setting__c.getValues(couponType);
        if(couponSetting == NULL)
        {
            CommonHelper.MJNCustomException e = new CommonHelper.MJNCustomException('Coupon Running Number not found for type: '+ couponType);
            CommonHelper.notifySystemException(e);
            throw e;
        }
        else
        {
            Integer nextNumber = Integer.valueOf(couponSetting.Next_ID__c);
            List<String> postfixList = UniqueCodeGenerator.getPostfixSplits(couponSetting.Postfix__c);
            List<Coupon__c> couponsToBeInserted = new List<Coupon__c>();
         
            for(String postfix : postfixList)
            {
                List<Coupon__c> coupons = new List<Coupon__c>();
                for (Integer i = 0 ; i < qty; i++)
                {
                    Fulfillment__c f = fulfillmentList[i];
                    String formattedNumber = UniqueCodeGenerator.leftPadZero(String.valueOf(nextNumber + i),6);
                    String newCouponID = couponSetting.Prefix__c + YY + formattedNumber + postfix;
                    
                    Coupon__c coupon = new Coupon__c();
                    coupon.Bar_Code__c = newCouponID;
                    coupon.Coupon_Type__c = couponType;
                    coupon.Account__c = contactAccountMap.get(f.Contact__c);
                    coupon.Contact__c = f.Contact__c;
                    coupons.add(coupon);
                }
                couponList.add(coupons.iterator());
                couponsToBeInserted.addAll(coupons);
            }
            
            //Create counpons
            insert couponsToBeInserted;
            
            //Update the Coupon Running Number
            couponSetting.Next_ID__c = nextNumber + qty;
            update couponSetting;
            
            return couponList;          
        }
    }
    
    /* NON-BULKIFIED method for Fulfilment for trigger: before update */
    public static List<Coupon__c> generateCouponByType(String couponType, ID AccountId)
    {
        List<Coupon__c> coupons = new List<Coupon__c>();
        String curYear = String.valueOf(Date.today().year());
        String YY = curYear.substring(2);
        
        //Get next Coupon ID running number from custom setting
        Coupon_ID_Setting__c couponSetting = Coupon_ID_Setting__c.getValues(couponType);
        if(couponSetting == NULL)
        {
            CommonHelper.MJNCustomException e = new CommonHelper.MJNCustomException('Coupon Running Number not found for type: '+ couponType);
            CommonHelper.notifySystemException(e);
            throw e;
        }
        else
        {
            Integer nextNumber = Integer.valueOf(couponSetting.Next_ID__c);
            List<String> postfixList = UniqueCodeGenerator.getPostfixSplits(couponSetting.Postfix__c);
            String formattedNumber = UniqueCodeGenerator.leftPadZero(String.valueOf(nextNumber),6);
            
            for(String postfix : postfixList)
            {

                String newCouponID = couponSetting.Prefix__c + YY + formattedNumber + postfix;
                
                Coupon__c coupon = new Coupon__c();
                coupon.Bar_Code__c = newCouponID;
                coupon.Coupon_Type__c = couponType;
                coupon.Account__c = AccountId;
                coupons.add(coupon);
            }
            
            //Create counpons
            insert coupons;
            
            //Increase the Coupon Running Number
            couponSetting.Next_ID__c = nextNumber + 1;
            update couponSetting;
            
            return coupons;
        }
    }
    

    
    @future
    public static void cancelCoupons(Map<ID,String> idReason)
    {
        List<Coupon__c> coupons = [SELECT ID FROM Coupon__c WHERE ID in :idReason.keySet()];
        
        for(Coupon__c coupon : coupons)
        {
            coupon.Status__c = 'Cancelled';
            coupon.Cancel_Date__c = Date.today();
            coupon.Cancel_Reason__c = idReason.get(coupon.Id);
        }
        
        update coupons;
    }
    
    public static String leftPadZero(String fromStr,Integer size)
    {
        if(fromStr == NULL) return NULL;
        
        String resultStr = fromStr;
        Integer count = size - fromStr.length();
        for(Integer i=0; i<count; i++)
            resultStr = '0'+resultStr;
        
        return resultStr;
    }
    
    private static List<String> getPostfixSplits(String postfix)
    {
        List<String> resultList = new List<String>();
        if(postfix != NULL)
         {
             List<String> splitStrs = postfix.split(';');
             for(String value : splitStrs)
             {
                 if(value != NULL && value.length()>0)
                     resultList.add(value);
             }
        }
        return resultList;
    }
    
    public static String getRandomChar()
    {
        Integer m = Integer.valueOf(Math.ceil(26.00 * Math.random()));

        return letters[m-1];
    }
}