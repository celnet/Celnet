@isTest
private class CampaignBatchJobTest {
    
    static testMethod void simpleTestOneExecute(){
        system.runas(TestUtility.new_HK_User()){
        //prepare 
        Integer size = 200;
        List <Contact> cons = TestUtility.generateTestContacts(size);
          
       Campaign camp1 = TestUtility.generateTestCampaigns(1)[0];
       
       Campaign_Scheduler__c cs1 = new Campaign_Scheduler__c();
       cs1.List_View_Id__c = TestUtility.getContactListViewId('All Contacts'); 
       cs1.Master_Campaign__c = camp1.Id;
       
       List<Campaign_Scheduler__c> css = new List<Campaign_Scheduler__c>{cs1};
       CampaignBatchJob job= new CampaignBatchJob(css);
    
        //start test   
        Test.startTest();
        ID batchprocessid = Database.executeBatch(job);
        Test.stopTest();
        
        
        //Assert newly created campaign name, master campaign, members count
        String testName = camp1.name+'_'+system.now().format('YYYYMMdd');
        List<Campaign> newCamp = [SELECT parentID from Campaign where name = :testName];
        system.assertEquals(newCamp.size(), 1);
        system.assertEquals(newCamp[0].parentID, camp1.Id);
       /* system.AssertEquals(
           database.countquery('SELECT COUNT()  FROM CampaignMember'),
           size);  */
       }    
    }
    
    static testMethod void chainingTest(){
        //prepare 
        Integer size = 200;
        List <Contact> cons = TestUtility.generateTestContacts(size);
        Campaign camp1 = TestUtility.generateTestCampaigns(1)[0];
        
        Campaign_Scheduler__c cs1 = new Campaign_Scheduler__c();
        cs1.List_View_Id__c = TestUtility.getContactRecentlyViewedId();
        cs1.Master_Campaign__c = camp1.Id;
        Campaign_Scheduler__c cs2 = new Campaign_Scheduler__c();
        cs2.List_View_Id__c = TestUtility.getContactRecentlyViewedId();
        cs2.Master_Campaign__c = camp1.Id;
        
        
        integer initialCount = 
           database.countquery('SELECT COUNT()  FROM CronTrigger')
        ; 
        
        
        Test.startTest();
        
        List<Campaign_Scheduler__c> css = new List<Campaign_Scheduler__c>{cs1,cs2};
        CampaignBatchJob job= new CampaignBatchJob(css);
        //simiulate start and finish
        DummyBatchContext bc = new DummyBatchContext(); 
        job.start(bc);
        job.finish(bc);
        
        Test.stopTest();
        
        //assert one additional cron job has been scheduled, (within finish() method)
        //note that due to chaining, it will always be at most only one job schedule ahead
        //regardless then pending jobs in the chain
       system.AssertEquals(
           database.countquery('SELECT COUNT()  FROM CronTrigger'),
           initialCount + 1); 
        
    }
    
    class DummyBatchContext implements Database.BatchableContext {
        public Id getChildJobId() { return null;}
        public Id getJobId() {return null;}
    }

}