/*
*用户向公众号发送一些文字关键字时由此Handler来处理
*/
public class WechatCallinTextHandler extends WechatCallinMsgHandler
{
    public override void Handle(WechatCallinMsgPipelineContext Context)
    {
        WechatEntity.InTextMsg inMsg = (WechatEntity.InTextMsg)Context.InMsg;
        set<string> keywords = new set<string>();
        for(Wechat_Content__c wc : [select key_word__c from Wechat_Content__c where key_Word__c != null])
        {
            keywords.add(wc.Key_word__c);
        }       
        if(!keywords.contains(inMsg.Content))
        {
            WechatEntity.OutTextMsg outMsg = WechatEntity.GenerateOutMsg(inMsg,WechatBusinessUtility.QueryRemindingByKey('RN030'));
            Context.OutMsg = outMsg;
            return;
        }
        List<Wechat_Content__c> contentList = new List<Wechat_Content__c>();
        string soql = 'select Article_Url__c , Description__c , Key_Word__c , Picture_Url__c, Title__c from Wechat_Content__c where Key_Word__c = '; 
        if(inMsg.Content == '0')
        {
            soql += '\'0\'';
        }
        else if(inMsg.Content == '1')
        {
            soql += '\'1\'';
        }
        else if(inMsg.Content == '2')
        {
            soql += '\'2\'';
        }
        else if(inMsg.Content == '3')
        {
            soql += '\'3\'';
        }
        else if(inMsg.Content == '4')
        {
            soql += '\'4\'';
        }
        else if(inMsg.Content == '5')
        {
            soql += '\'5\'';
        }
        else if(inMsg.Content == '6')
        {
            soql += '\'6\'';
        }
        else if(inMsg.Content == '7')
        {
            soql += '\'7\'';
        }
        else if(inMsg.Content == '8')
        {
            soql += '\'8\'';
        }
        else if(inMsg.Content == '9')
        {
            soql += '\'9\'';
        }
        else
        {
            return;
        }
        soql += ' order by Show_Order__c asc';
        WechatEntity.OutRichMediaMsg outRichMedia = new WechatEntity.OutRichMediaMsg();
        List<WechatEntity.Article> articleList = new List<WechatEntity.Article>();
        WechatEntity.GetBaseMsgAtrributes(inMsg,outRichMedia);
        outRichMedia.MsgType = WechatEntity.MESSAGE_TYPE_NEWS;
        outRichMedia.Articles = articleList;    
        for(Wechat_Content__c wc :database.query(soql))
        {
            WechatEntity.Article art = new WechatEntity.Article();
            art.title = wc.Title__c;
            art.description = wc.Description__c;
            art.url = wc.Article_Url__c;
            art.picURL = (null != wc.Picture_Url__c ? wc.Picture_Url__c : '');
            outRichMedia.Articles.add(art);
        }
        Context.OutMsg = outRichMedia;
    }
}