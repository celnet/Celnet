public with sharing class LeadStaging {
    public final Lead leadToConvert;
    public List<BabyWrapper> leadBabyWrapper {get; private set;}
    public Contact leadParent {get;set;} 
    public Account leadHousehold {get;set;}
    
    
    private Integer numberOfBabies{
        get { return leadBabyWrapper.size();} 
    }
    public String parentMergeId; 
    public String householdMergeId;
    public Boolean hasNewBabies;
 
    private boolean isLastFirstName;
  
    public LeadStaging(Lead lead, boolean isLastFirstName){
        leadToConvert = lead;
        this.isLastFirstName = isLastFirstName;
        
        setupLeadsBabies();
        setupParent();
        setupHousehold();
        resetMergeIds();
        hasNewBabies = false;
    }
    
    private void resetMergeIds(){
        householdMergeId = null;
        parentMergeId = null;
    }
    
    private void setupLeadsBabies(){ 
        leadBabyWrapper = new List<BabyWrapper>();
        List<String> babyNumber = new List<String>{'1st','2nd','3rd','4th'};
        SObject sObj = leadToConvert;
        for (String num : babyNumber){
            String suffix = num+'_Child__c';
            //use last name of every lead's baby as indicator if this new baby exists
            if (sObj.get('Last_Name_'+suffix)==null)
                continue;
            Contact baby = new Contact(); 
            baby.RecordTypeId = ObjectUtil.GetRecordTypeID(Contact.SObjectType,CommonHelper.HOUSEHOLD_CHILD_RT);
            baby.FirstName = (String)sObj.get('First_Name_'+suffix);
            baby.LastName = (String)sObj.get('Last_Name_'+suffix);
            baby.Chinese_Name__c = (String)sObj.get('Chinese_Name_'+suffix);
            baby.Other_Name__c = (String)sObj.get('Other_Name_'+suffix);
            baby.Birthdate = (Date)sObj.get('Birthday_'+suffix);
            baby.Gender__c = (String)sObj.get('Gender_'+suffix);
            String hospital = (String)sObj.get('Born_Hospital_'+suffix);
            if (hospital != null){
                //do look up 
                Account[] hosp = [Select id From account where Name=:hospital LIMIT 1];
                if (hosp.size() == 1){
                    baby.Born_Hospital__c = hosp[0].id;
                }
            }
            //supp doc?
            BabyWrapper wrapper = new BabyWrapper(baby,isLastFirstName);
            
            wrapper.addBabyConsumption(-9, 0, (String)sObj.get('Mother_Milk_'+suffix));
            wrapper.addBabyConsumption(0, 6, (String)sObj.get('Child_Milk_0_6M_'+suffix));
            wrapper.addBabyConsumption(7, 12, (String)sObj.get('Child_Milk_7_12M_'+suffix));
            wrapper.addBabyConsumption(13, 36, (String)sObj.get('Child_Milk_13_36M_'+suffix));
            wrapper.addBabyConsumption(37, 72, (String)sObj.get('Child_Milk_36M_'+suffix));
            
            Recruitment_Channel__c rc = new Recruitment_Channel__c();
            rc.Channel__c = leadToConvert.LeadSource; 
            rc.Sub_Channel__c = leadToConvert.Sub_Channel__c;
            rc.Converted_from_Lead__c = true;
            
            wrapper.recruitmentChannel = rc;
            
            leadBabyWrapper.add(wrapper);
        }
    }
    
    private void setupParent(){ 
        //more for non-pri contact
        leadParent = new Contact(); 
        leadParent.RecordTypeId = ObjectUtil.GetRecordTypeID(Contact.SObjectType,CommonHelper.HOUSEHOLD_PARENT_RT);
        leadParent.LastName = leadToConvert.LastName;
        leadParent.FirstName = leadToConvert.FirstName;
        leadParent.Other_Name__c = leadToConvert.Other_Name__c;
        leadParent.Chinese_Name__c = leadToConvert.Chinese_Name__c;
        leadParent.Email = leadToConvert.Email;
        //confirm that's all the fields
    }
    
    private void setupHousehold(){
        leadHousehold = new Account();
        
        leadHousehold.RecordTypeId = ObjectUtil.GetRecordTypeID(Account.SObjectType, CommonHelper.HOUSEHOLD_NEW_RT);
        
        leadHousehold.Name = LeadConversionHelper.concatName(true, leadToConvert.FirstName, leadToConvert.LastName,null);
        //UAT Fix: No need Read Locale Setting for First/Last Name order.
        //LeadConversionHelper.concatName(isLastFirstName, leadToConvert.FirstName, leadToConvert.LastName,null);
        //Pri contact details                 
        leadHousehold.Salutation__c = leadToConvert.Salutation;
        leadHousehold.Last_Name__c = leadToConvert.LastName;
        leadHousehold.First_Name__c = leadToConvert.FirstName;
        leadHousehold.Other_Name__c = leadToConvert.Other_Name__c;
        leadHousehold.Preferred_Spoken_Language__c = leadToConvert.Preferred_Spoken_Language__c;
        leadHousehold.Preferred_Written_Languge__c = leadToConvert.Preferred_Written_Languge__c;
        leadHousehold.Chinese_Name__c = leadToConvert.Chinese_Name__c; //check
        //Contact info 
        leadHousehold.Home_Phone__c = leadToConvert.Home_Phone__c;
        leadHousehold.Mobile_Phone__c = leadToConvert.MobilePhone;
        leadHousehold.Other_Phone_1__c = leadToConvert.Other_Phone_1__c;
        leadHousehold.Other_Phone_2__c = leadToConvert.Other_Phone_2__c;
        leadHousehold.Other_Phone_1_Remark__c = leadToConvert.Other_Phone_1_Remark__c;
        leadHousehold.Other_Phone_2_Remark__c = leadToConvert.Other_Phone_2_Remark__c;
        leadHousehold.Preferred_Contact_Method__c = leadToConvert.Preferred_Contact_Method__c;
        leadHousehold.Do_Not_Call_Home_Phone__c = leadToConvert.Do_Not_Call_Home_Phone__c;
        leadHousehold.Do_Not_Call_Mobile__c = leadToConvert.Do_Not_Call_Mobile__c;
        leadHousehold.Do_Not_Call_Other_Phone_1__c = leadToConvert.Do_Not_Call_Other_Phone_1__c;
        leadHousehold.Do_Not_Call_Other_Phone_2__c = leadToConvert.Do_Not_Call_Other_Phone_2__c;
        leadHousehold.Do_not_SMS_Mobile__c = leadToConvert.Do_not_SMS_Mobile__c;
        leadHousehold.Account_Email_Opt_out__c = leadToConvert.HasOptedOutOfEmail;
        leadHousehold.Preferred_Contact_Time__c = leadToConvert.Preferred_Contact_Time__c;
        leadHousehold.Account_Email__c = leadToConvert.Email;
        //Address
        leadHousehold.Region__c = leadToConvert.Region__c;
        leadHousehold.Address_Flat__c = leadToConvert.Address_Flat__c;
        leadHousehold.Address_Floor__c = leadToConvert.Address_Floor__c;
        leadHousehold.Address_Building__c = leadToConvert.Address_Building__c;
        leadHousehold.Address_Block__c =  leadToConvert.Address_Block__c;
        leadHousehold.Address_Street__c = leadToConvert.Address_Street__c;
        leadHousehold.Address_District__c = leadToConvert.Address_District__c;
        leadHousehold.Address_Territory__c = leadToConvert.Address_Territory__c;
        leadHousehold.Mailing_Opt_Out__c = leadToConvert.Mailing_Opt_Out__c;
        leadHousehold.Address_in_Chinese__c = leadToConvert.Address_in_Chinese__c;
        leadHousehold.China_Address__c = leadToConvert.China_Address__c;
        
        //reminder: a new parent (primary) contact and member ID will be created upon insert of account
    }
     
    
    /*
        Validate and set staging parameters user selection (radio buttons) by getting from 
        ApexPages.currentPage().getParameters().get(...radiogroups..)
    */
    public boolean validateNonNewHouseholdSelections(){
        resetMergeIds();
        String parent = ApexPages.currentPage().getParameters().get('radiogroup-parent');
        if (parent == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select which household parent to create/merge.'));
            return false;
        }
        //use parent household as reference, making sure babies selection are under the same household
        householdMergeId = parent.split(':')[0];
        //for each baby in lead, make sure an action is selected, and no merging of same babies
        String[] babyInfo = new String[numberOfBabies];
        Set<String> nonOverlapBabies = new Set<String>();
        for (Integer index=0; index < numberOfBabies; index++){
            babyInfo[index] = ApexPages.currentPage().getParameters().get('radiogroup-baby'+(index+1));
            if (babyInfo[index] == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select which household child to create/merge.'));
                return false;
            }
            if (babyInfo[index].split(':')[0]!=householdMergeId){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Parent and Child Contacts must be from the same household!'));
                return false;
            }
            //if action = merge, check if overlap
            if (babyInfo[index].split(':').size() >1){
                String babyId = babyInfo[index].split(':')[1];
                if (nonOverlapBabies.contains( babyId)){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Cannot merge multiple lead children into same child!'));
                    return false;
                }else{
                    nonOverlapBabies.add(babyId);
                    leadBabyWrapper.get(index).babyMergeId =  babyId;
                }
            }
        }//end for loop
        //check if need to create new parent
        if (parent.split(':').size() > 1){
            parentMergeId = parent.split(':')[1];
        }
        //Congrats if you made it to here!
        return true;
    } //end validate method
    
    /* Create new parent or use+merge existing parent, based on staging parameters */
    public Contact createOrMergeParent(Map<ID,Contact> existingContacts){
        Contact parent;
        if (parentMergeId == null){
            parent = leadParent;
        }else{
            parent = existingContacts.get(parentMergeId);
            LeadConversionHelper.applyMergeRulesParent(parent, leadParent);
        }
        parent.AccountId = householdMergeId;
        return parent;
    }
    
    /* Create new babies or use+merge existing babies, based on staging parameters */
    public List<Contact> createOrMergeBabies(Map<ID,Contact> existingContacts){
        List<Contact> babies = new List<Contact>();
        for (BabyWrapper wrapper : leadBabyWrapper){
            if (wrapper.babyMergeId != null){
                Contact baby = existingContacts.get(wrapper.babyMergeId);
                LeadConversionHelper.applyMergeRulesBaby(baby, wrapper.baby);
                babies.add(baby);
                //update wrapper's existingBaby 
                wrapper.existingBaby = baby;
            }else{
                babies.add(wrapper.baby);
                hasNewBabies = true;
            }
        }
        for (Contact b : babies){
            b.AccountId = householdMergeId;
        }
        return babies;
    }
    
    public List<String> getBabyMergeIds(){
        List<String> Ids = new List<String>();
        for (BabyWrapper wrapper : leadBabyWrapper){
            Ids.add(wrapper.babyMergeId);
        }
        return Ids;
    }
    
    public void updateBabyIDs(){
        for (BabyWrapper wrapper : leadBabyWrapper){
            wrapper.updateBabyID();
        }
    }
    
    public List<Baby_Consumption__c> getBabyConsumptions(){
        List<Baby_Consumption__c> BCs = new List<Baby_Consumption__c>();
        for (BabyWrapper wrapper : leadBabyWrapper){
            BCs.addAll(wrapper.babyConsumptions);
        }
        return BCs;
    }
    public List<Recruitment_Channel__c> getRecruitmentChannels(){
        List<Recruitment_Channel__c> RCs = new List<Recruitment_Channel__c>();
        for (BabyWrapper wrapper : leadBabyWrapper){
            RCs.add(wrapper.recruitmentChannel);
        }
        return RCs;
    }
    
    public String referralMemberId{
        get{
            return leadToConvert.Referral_Member_ID__c;
        }
    }
    
    
    public class BabyWrapper{
        public Contact baby {get;set;}
        public String babyMergeId;
        public Contact existingBaby{get;set;}
        public Recruitment_Channel__c recruitmentChannel;
        public List<Baby_Consumption__c> babyConsumptions;
        private boolean isLastFirstName;
        public boolean isNewBaby{
            get { return (babyMergeId == null); }
        }
        
        public BabyWrapper(Contact baby, boolean isLastFirstName){
            this.baby = baby;
            this.babyConsumptions = new List<Baby_Consumption__c>();
            this.babyMergeId = null;
            this.isLastFirstName = isLastFirstName;
        }
        
        public String getBabyName(){
            return LeadConversionHelper.concatName(
                isLastFirstName,
                baby.FirstName, 
                baby.LastName, 
                baby.Chinese_Name__c
                );
        }
        
        public void addBabyConsumption(Decimal fromAge, Decimal toAge, String product){
            if (product==null){
                return;
            }
            
            //do look up product, only add consumption if there's product
            //Bug fix 29 Jan: to include only DSO=false 
            Product__c[] p = [Select id From Product__c where Name =:product and Is_DSO_Product__c=false LIMIT 1];
            if (p.size() == 1){
                
                Baby_Consumption__c bc = new Baby_Consumption__c();
                bc.Product__c = p[0].id;
                bc.From_Age__c = fromAge;
                bc.To_Age__c = toAge;
                bc.Converted_from_Lead__c = true;
                babyConsumptions.add(bc);
            }
        }
        
        /*
        Called after Babies have been inserted and Id has been generated
        To assign the newly generated Id to BC and RC.
        */
        public void updateBabyID(){
            String babyId = isNewBaby ? baby.Id : babyMergeId;
            for (Baby_Consumption__c bc : babyConsumptions){
                bc.Contact__c = babyId;
            }
            recruitmentChannel.Contact__c = babyId;
            if (isNewBaby){
                recruitmentChannel.Primary__c = true;
            }else{
                //check if there's existing primary recruitment channel set
                //bug fix 24 jan 2014 - should check existing baby's Primary Recruitment channel instead of staging's.
                //if (baby.Primary_RecruitChannel_Type__c == null){
                if (existingBaby != null && existingBaby.Primary_RecruitChannel_Type__c == null){
                    recruitmentChannel.Primary__c = true;   
                }else{
                    recruitmentChannel.Primary__c = false;
                }
            }
        }
    }
}