/*
 *注册绑定时，手机验证页面后台控制类
*/
public class WechatSMSVerificationPageController 
{
	public string openId;
	public Wechat_User__c wu;
	public boolean formIsShow{get;set;}
	public string message{get;set;}
	public string ReceivePhone{get;set;}
	public string createCode{get;set;}
	public string inputCode{get;set;}
	public string DisableBtn{get;set;}
	public string btnStyle{get;set;}
	public string recordId;
	
	public WechatSMSVerificationPageController()
	{
		btnStyle = 'BACKGROUND-COLOR:#7b98b3;margin-bottom:50px;';
		ReceivePhone = '手提電話號碼';
		inputCode = '手機驗證碼';
		formIsShow = true;
		message = '';
		DisableBtn = '';
		recordId = '';
		openId = Apexpages.currentPage().getParameters().get('openId');
		List<Wechat_User__c> wuList = RetireveUserByOpenId(openId);
		if(wuList.isempty())
		{
			message = WechatBusinessUtility.QueryRemindingByKey('RN020');
		}
		else if(wuList[0].Binding_Non_Member__c != null)
		{
			message = WechatBusinessUtility.QueryRemindingByKey('RN021');
		}
		else if(wuList[0].Binding_Member__c != null)
		{
			message = WechatBusinessUtility.QueryRemindingByKey('RN022');
		}
		if(message != '')
		{
			this.formIsShow = false;
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.WARNING, message);
	        ApexPages.addMessage(myMsg);
	        return;
		}
		else
		{
			formIsShow = true;
			wu = wuList[0];
		}
	}
	//生成短信验证码随机6位数，并且发送短信
	public void CreateRandomCode()
	{
		createCode = CreateRandomCode(6);
		system.debug('**********createCode****************' + createCode);
		recordId = SendSMS(ReceivePhone);
		system.debug('********recordId*****recordId*****recordId*****************' + recordId);
		//inputCode = createCode;
	}
	//验证用户有效性
	public string pageValid()
	{
		system.debug('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$4');
		string errMsg = '';
		SMS_Log__c ssLog = new SMS_Log__c();
		ssLog.id = recordId;
		try
		{
			system.debug('************recordId******************' + recordId);
			ssLog.Submit_Time__c = Datetime.now();
			ssLog.InputCode__c = inputCode;
			update ssLog;
			if(inputCode != createCode)
			{
				errMsg = WechatBusinessUtility.QueryRemindingByKey('RN023');
				return errMsg;
			}
			ssLog.Verification_Status__c = true;
			update ssLog;
		}
		catch(DMLException ex)
		{
			ssLog.Exception__c = ex.getMessage();
			update ssLog;
		}
		catch(Exception e)
		{
			ssLog.Exception__c = e.getMessage();
			update ssLog;
		}
		
		string isBinding = IsPhoneBinding(ReceivePhone);
		if(isBinding != '')
		{
			errMsg = WechatBusinessUtility.QueryRemindingByKey('RN024',isBinding);
			DisableBtn = 'true';
			btnStyle = 'BACKGROUND-COLOR:#CCCCCC;margin-bottom:50px;';
			return errMsg;
		}
		if(!CheckOpenId(ReceivePhone , openId))
		{
			errMsg = WechatBusinessUtility.QueryRemindingByKey('RN025');
			DisableBtn = 'true';	
			btnStyle = 'BACKGROUND-COLOR:#CCCCCC;margin-bottom:50px;';
			return errMsg;
		}
		if(!CheckLead(ReceivePhone , openId))
		{
			errMsg = WechatBusinessUtility.QueryRemindingByKey('RN026');
			DisableBtn = 'true';
			btnStyle = 'BACKGROUND-COLOR:#CCCCCC;margin-bottom:50px;';
			return errMsg;
		}
		return errMsg;
	}
	//提交到注册界面
	public PageReference SubmitPage()
	{
		string errMsg = pageValid();
		system.debug('************errMsg*****errMsg********************' + errMsg);
		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.ERROR , errMsg);
	    ApexPages.addMessage(myMsg);
	   	Wechat_User__c wechatUser = new Wechat_User__c();
	    wechatUser.Id = wu.Id;  
	    wechatUser.Mobile_Phone__c = ReceivePhone;
		update wechatUser;
		if(errMsg == '')
		{
			return new PageReference('/WechatRedirectWelcomePage?openId=' + openId + '&Phone=' + ReceivePhone + '&Env=' + WechatBusinessUtility.Env);
		}
		else
		{
			return null;
		}
	}
	//随机生成验证码
	public string CreateRandomCode(integer codeLength)
	{
		string code = '';
		for(integer i = 0; i < codeLength; i++)
		{
			code += String.valueOf(Math.round(Math.random() * (9 - 0)));
		}
		return code;
	}
	//调用web接口发送短信
	public string SendSMS(string Mobile)
	{
		String Content = WechatBusinessUtility.QueryRemindingByKey('RN027',createCode);
		string error = '';
		SMS_Log__c smsLog = new SMS_Log__c();
		smsLog.CreateCode__c = createCode;
		smsLog.Send_Content__c = Content;
		smsLog.Send_Phone_Number__c = Mobile;
		smsLog.SMS_Send_Time__c = Datetime.now();
		smsLog.WechatUser__c = wu.Id;
		smsLog.Name = Datetime.now().format('yyyy-MM-dd hh:mm:ss');
		
		string responseBody = '';
		try
		{
			responseBody = WechatSMSService.SendSMS(Mobile,Content); 
			smsLog.Receive_Time__c = Datetime.now();
			smsLog.Smartone_Response__c = responseBody;
			insert smsLog;
		}
		catch(DMLException ex)
		{
			error = ex.getMessage();
		}
		catch(Exception e)
		{
			error = e.getMessage();
		}
		if(error != '')
		{
			smsLog.Exception__c = error;
			insert smsLog;
		}

		return smsLog.id;
	}
	//根据openId获取Wechat_User__c
	public List<Wechat_User__c> RetireveUserByOpenId(string oId)
	{
		List<Wechat_User__c> wuList = [select Id , Name, Binding_Non_Member__c , Binding_Member__c ,  Mobile_Phone__c from Wechat_User__c where Open_Id__c= :oId];
		system.debug('*********************RetireveUserByOpenId******************' + wuList);
		return wuList;
	}
	//判断手机号是否绑定Lead或Account
	public string IsPhoneBinding(string tel)
	{
		List<Lead> leadList = [select id , (select id , Name from Wechat_Users__r) from Lead where MobilePhone = :tel and LeadSource != 'Telephone' and IsConverted = false];
		List<Account> accountList = [select id , (select id, Name from Wechat_Users__r) from Account where Mobile_Phone__c = :tel or Home_Phone__c = :tel or Other_Phone_1__c = :tel or Other_Phone_2__c = :tel];
		if(leadList.isempty() && accountList.isempty())
		{
			return '';
		}
		else
		{
			if(!leadList.isempty())
			{
				if(!leadList[0].Wechat_Users__r.isempty())
				{
					return leadList[0].Wechat_Users__r[0].Name;
				}
				else
				{
					return '';
				}
			}
			else
			{
				return '';
			}
			if(!accountList.isempty())
			{
				if(!accountList[0].Wechat_Users__r.isempty())
				{
					return accountList[0].Wechat_Users__r[0].Name;
				}
				else
				{
					return '';
				}
			}
			else
			{
				return '';
			}
		}
	}
	//判断检查手机号是否关联到Account
	public boolean CheckOpenId(string tel , string oId)
	{
		system.debug('************oId*****oId********************' + oId);
		List<Account> aList =  [select id , (select id from Wechat_Users__r) from Account where (Mobile_Phone__c = :tel or Home_Phone__c = :tel or Other_Phone_1__c = :tel or Other_Phone_2__c = :tel) and Inactive__c = false order by CreatedDate desc];
		system.debug('*********aList**************' + aList);
		List<Wechat_User__c> userList = [select id , Binding_Member__c from Wechat_User__c where Open_Id__c = :oId];
		if(!aList.isempty() && aList[0].Wechat_Users__r.isempty() && !userList.isempty() && (userList[0] != null))
		{
			system.debug('*************aList******************' + userList);
			userList[0].Binding_Member__c = aList[0].id;
			update userList[0];
			return false;
		}
		else if(!userList.isempty() && userList[0] != null && userList[0].Binding_Member__c != null)
		{
			system.debug('*************aList******************' + userList);
			return false;
		}
		else
		{
			return true;
		}
	}
	//检查手机号是否关联到Lead
	public boolean CheckLead(string tel , string oId)
	{
		List<Lead> aList =  [select id , (select id from Wechat_Users__r) from Lead where MobilePhone = :tel and IsConverted = false and LeadSource != 'Telephone' order by CreatedDate desc];
		List<Wechat_User__c> userList = [select id , Binding_Non_Member__c from Wechat_User__c where Open_Id__c = :oId];
		if(!aList.isempty() && aList[0].Wechat_Users__r.isempty() && (!userList.isempty()))
		{
			userList[0].Binding_Non_Member__c = aList[0].id;
			system.debug('*************lList******************' + aList);
			update userList;
			return false;
		}
		else
		{
			return true;
		}
	}
}