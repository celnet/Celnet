/*
*create wechat menu controller
*/
public class WechatMenuController 
{
    //public static final string firstUrl = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx76c641c11a6462e5&redirect_uri=';
    //public static final string secUrl = '&response_type=code&scope=snsapi_base&state=CideatechSFWeiXin#wechat_redirect';
    
    public static final string firstUrl = '';
    public static final string secUrl = '';
    
    public static WechatMenuApi.Menu GenerateMenuList()
    {
        WechatMenuApi.Menu me = new WechatMenuApi.Menu();
        
        List<Wechat_Menu__c> menuList = [Select View_Url__c, Name, Menu_Type__c, Menu_Order__c, Id, Event_Key__c, 
                                            (Select Name, Menu_Type__c, Event_Key__c, View_Url__c, Menu_Level__c, Menu_Order__c 
                                            From Wechat_Sub_Menu__r 
                                            where Active__c = true
                                            order by Menu_Order__c)
                                        From Wechat_Menu__c 
                                        where Active__c = true 
                                        and Menu_Level__c = 'Menu'
                                        order by Menu_Order__c];
        system.debug('********menuList*****menuList*****menuList*********' + menuList);
        WechatMenuApi.Button[] button = new WechatMenuApi.Button[menuList.size()];
        for(Integer j =0 ; j < menuList.size(); j++)
        {
            Wechat_Menu__c wm = menuList[j];
            if(wm.Wechat_Sub_Menu__r.isempty())
            {
                if(wm.Menu_Type__c == 'Click')
                {
                    WechatMenuApi.ClickButton cb = new WechatMenuApi.ClickButton();
                    cb.name = wm.Name;
                    cb.key = wm.Event_Key__c;
                    button[j] = cb;
                }
                else
                {
                    WechatMenuApi.ViewButton vb = new WechatMenuApi.ViewButton();
                    vb.name = wm.Name;
                    vb.url = firstUrl + wm.View_Url__c + secUrl;
                    button[j] = vb;
                }
            }
            else
            {
                WechatMenuApi.Super_Button_Subs sbs = new WechatMenuApi.Super_Button_Subs();
                sbs.name = wm.Name;
                WechatMenuApi.Super_SubButton[] ssb = new WechatMenuApi.Super_SubButton[wm.Wechat_Sub_Menu__r.size()];
                for(Integer i = 0; i < wm.Wechat_Sub_Menu__r.size(); i++)
                {
                    Wechat_Menu__c sub = wm.Wechat_Sub_Menu__r[i];
                    if(sub.Menu_Type__c == 'Click')
                    {
                        WechatMenuApi.ClickSubButton csb =new WechatMenuApi.ClickSubButton();
                        csb.name = sub.Name;
                        csb.key = sub.Event_Key__c;
                        ssb[i] = csb;
                    }
                    else
                    {
                        WechatMenuApi.ViewSubButton vsb = new WechatMenuApi.ViewSubButton();
                        vsb.name = sub.name;
                        vsb.url = firstUrl + sub.View_Url__c + secUrl;
                        ssb[i] = vsb;
                    }
                }
                sbs.sub_button = ssb;
                button[j] = sbs;
            }
        }
        me.button = button;
        return me;
    }
}