/*
Author: leo.bi@celnet.com.cn
Created On: 2014-06-09
Function: execute massfinishcampaigncallbatch
Apply To: CN
*/
global class MassFinishCampaignCallWebService {
	webService static void ExetuteBatch(Id campaignId) 
	{
		MassFinishCampaignCallBatch mfccb = new MassFinishCampaignCallBatch(campaignId);
		DataBase.executeBatch(mfccb,1500);
	}
}