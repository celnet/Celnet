public class QTaskRoutineVisitCon 
{
	public final String QuestionnaireType='Routine';
	public final Set<String> BusTypes=new Set<String>{'Routine Reciprocal','Routine Distribution'};
	public List<CusSobj> ListSobjs{get;set;}
	public Date startOfThisMonth{get;set;}
	public Date endOfThisMonth{get;set;}
	public String Month{get;set;} 
	public String SelectDate{get;set;}
	public List<Questionnaire__c> List_ques{get;set;}
	public List<SelectOption> ListMonths{get;set;}
	public QTaskRoutineVisitCon()
	{
		try
		{
			ListMonths = new List<SelectOption>();
	        Date today = date.today().addMonths(1);
	        for (Integer n = 0; n <3; n++ )
	        {
	            String nextDate = String.valueOf(today.toStartOfMonth().addMonths(-n));
	            SelectOption option = new SelectOption(nextDate,nextDate);
	            ListMonths.add(option);
	    	}
	    	
			
			List_ques = [select Id,Name,Start_age__c,End_age__c,Type__c,Business_Type__c,Next_Routine_Questionnaire__c,
	                        Next_Routine_Questionnaire__r.Name,Next_Routine_Questionnaire__r.Start_age__c,Next_Routine_Questionnaire__r.End_age__c 
	                        from Questionnaire__c where Type__c =:QuestionnaireType and Business_Type__c in:BusTypes order by Start_age__c ];
			
			this.SelectDate = String.valueOf(date.today().toStartOfMonth());
			this.showQTaskRoutineVisit();
		}
		catch(Exception e)
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, '查询问卷出错，请检查问卷是否有过调整。');    		
    		ApexPages.addMessage(msg);
    		return;
		}
	} 
	
	public void showQTaskRoutineVisit()
	{
		Date CurrentDate = date.valueOf(SelectDate);
		Month = String.valueOf(CurrentDate.Month());
		startOfThisMonth = CurrentDate;
  		endOfThisMonth = startOfThisMonth.addMonths(1).addDays(-1);
		ListSobjs = new List<CusSobj>();
	    for(Questionnaire__c ques : List_ques)
	    {
	      String Startage = String.valueOf(ques.Start_age__c );
	      String Endage = String.valueOf(ques.End_age__c);
	      //Reciprocal
          Date StartDate =(ques.End_age__c != null ? startOfThisMonth.addMonths(-Integer.valueOf(ques.End_age__c/30)) : null);
          Date EndDate = (ques.Start_age__c != null ? endOfThisMonth.addMonths(-Integer.valueOf(ques.Start_age__c/30)) : null);
          EndDate = EndDate.toStartOfMonth().addMonths(1).addDays(-1);
          
          
	      CusSobj sob = new CusSobj();
	      sob.QuestionnairesName = ques.Name;
	      sob.BabyAge = (Startage == Endage?Startage:Startage+' - '+Endage);
	      sob.DateSection = String.valueOf(StartDate)+' 至 '+String.valueOf(EndDate);
	      ListSobjs.add(sob);
	    }
	}
	public class CusSobj
	{
		public String QuestionnairesName{get;set;}
		public String BabyAge{get;set;}
		public String DateSection{get;set;}
	}
	
}