/*
	list objects created with result(new or merged) and error messages 
*/

public with sharing class LeadConvertResult {
	public boolean isSuccess;
	
	public List<HouseholdWrapper> householdWrapper {get;set;}
	public List<ParentWrapper> parentWrapper {get;set;}
	public List<BabyWrapper> babyWrapper {get;set;}
	
	public Database.Error[] databaseErrors {get;set;}
	public String exceptionString {get;set;}
	
	public LeadConvertResult(){
		babyWrapper = new List<BabyWrapper>();
		parentWrapper = new List<ParentWrapper>();
		householdWrapper = new List<HouseholdWrapper>();
	}
	
	public void addBaby(Contact baby, boolean isNew){
		babyWrapper.add(new BabyWrapper(baby,isNew));
	}
	public void addParent(Contact parent, boolean isNew){
		parentWrapper.add(new ParentWrapper(parent,isNew));
	}
	
	public void addHousehold(Account household, boolean isNew){
		householdWrapper.add(new HouseholdWrapper(household,isNew));
	}
	
	public class BabyWrapper{
		public Contact baby {get;set;}
		public boolean isNewBaby{get;set;}
		
		public BabyWrapper (Contact baby, Boolean isNew){
			this.baby = baby;
			this.isNewBaby = isNew;
		}
	}
	
	public class HouseholdWrapper{
		public boolean isNewHousehold{get;set;}
		public Account household {get;set;}
		
		public HouseholdWrapper (Account household, Boolean isNew){
			this.household = household;
			this.isNewHousehold = isNew;
		}
	}
	
	public class ParentWrapper{
		public boolean isNewParent{get;set;}
		public Contact parent{get;set;}
		
		public ParentWrapper (Contact parent, Boolean isNew){
			this.parent = parent;
			this.isNewParent = isNew;
		}
	}
	
}