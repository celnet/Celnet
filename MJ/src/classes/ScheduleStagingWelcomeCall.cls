/*
Author: shiqi.ng@sg.fujitsu.com
Schedulable Class for Staging 
    Table 2.1 《欢迎致电分析报表》 
*/

global class ScheduleStagingWelcomeCall implements Schedulable {
    Integer thisDay;
    Integer thisMonth;
    Integer thisYear;
    boolean adHoc;
    
    /**
    Default contructor: Reporting Date = yesterday, since daily batch runs after midnight
    */
    public ScheduleStagingWelcomeCall(){
        thisDay = null;
    	thisMonth = null;
        thisYear = null;
        adHoc = false;
    }
    
    /**
    Overload constructor with custom date parameter for ad-hoc runs!
    */
    public ScheduleStagingWelcomeCall(Integer year, Integer month, Integer day){
        thisDay = day;
        thisMonth = month;
        thisYear = year;
        adHoc = true;
    }
    global void execute(SchedulableContext SC) {
        if (adHoc == false){
            Date yesterday = Date.today().addDays(-1);
            thisDay = yesterday.day();
            thisMonth =  yesterday.month();
            thisYear = yesterday.year();
        }
        
        //Step 1: Select 12 Questionnaires, execute batch size of 1 to process one questionnaire at a time
        String query = 'select id, questionnaire_No__c ';
        query+= ' from questionnaire__c where questionnaire_No__c >= 1 and questionnaire_No__c  <= 12';
        
        BatchBuildStagingWelcomeCall welcome = new BatchBuildStagingWelcomeCall(
            query, thisYear, thisMonth, thisDay);
        Database.executeBatch(welcome,1);
        
    }
}