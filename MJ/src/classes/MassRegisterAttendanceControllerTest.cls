/*Author:leo.bi@celnet.com.cn
 *Date:2014-5-9
 *function: The Test Class of MassRegisterAttendanceController
 *Apply To:CN
 */
@isTest(SeeAllData=true)
private class MassRegisterAttendanceControllerTest 
{
    static testMethod void myUnitTest() 
    {
    	system.runas(TestUtility.new_CN_User())
        {
	        //new campaign
	       	Address_Management__c am = new Address_Management__c();
	        am.Region__c = '华北区';
	        am.Sub_Region__c = '河北省';
	        am.Area__c ='西区';
	        am.Sales_City__c = '廊坊市';
	        am.Name = '廊坊市';
	        insert am;
	        Campaign cam =new Campaign();
	        cam.Name = 'test campaign' + String.valueOf(DateTime.now());
	        cam.Standard_City__c = am.Id;
	        cam.Event_Date__c = Date.today();
	        insert cam;
	        //save
	        ApexPages.StandardController standardcontroller = new ApexPages.StandardController(new Attendance__c());
	        MassRegisterAttendanceController controller = new MassRegisterAttendanceController(standardcontroller);
	        
	        //Select Regions
	        List<Selectoption> list_temp = controller.salesRegionItems;
	        controller.selectedSalesRegion = am.Region__c;
	        controller.refreshSalesSubRegion();
	        
	        list_temp = controller.salesSubRegionItems;
	        controller.selectedSalesSubRegion = am.Id;
	        controller.refreshSalesArea();
	        
	        list_temp = controller.salesAreaItems;
	        controller.selectedSalesArea = am.Id;
	        controller.refreshSalesCity();
	        
	        list_temp = controller.salesCityItems;
	        controller.selectedSalesCity = am.Id;
	        
	        list_temp = controller.dateItmes;
	        controller.selectedDate = String.valueOf(Date.today());
	        controller.refreshCampaignItems();
	        
	        list_temp = controller.campaignItmes;
	        controller.parentCampaign.Id = cam.Id;
	        controller.initTable();
	        
	        controller.addNew();
	        controller.list_att[0].Mother_Name__c = 'Test Mother'; 
	        controller.list_att[0].Is_Pregnancy__c = true;
	        controller.list_att[0].Baby_Birthday__c = Date.today(); 
	        controller.list_att[0].Phone__c = '15900000000'; 
	        controller.list_att[0].Using_Product__c = 'MJN';
	        controller.list_att[0].Invitation_Channel__c = 'Invitation';
	        controller.list_att[0].Remark__c = 'test remark';
	        controller.save();
	        
	        //cancel
	        standardcontroller = new ApexPages.StandardController(new Attendance__c());
	        controller = new MassRegisterAttendanceController(standardcontroller);
	        controller.cancel();
	        //valid
	        standardcontroller = new ApexPages.StandardController(new Attendance__c()); 
	        controller = new MassRegisterAttendanceController(standardcontroller);
	        controller.addNew();
	        controller.list_att[0].Remark__c = 'test remark';
	        controller.save();
        }
    }
}