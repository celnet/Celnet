/*Author:leo.bi@celnet.com.cn
 *Function:Lead event handler for HK Wechat
 */
public class WechatLeadEventHandler implements Triggers.Handler
{
    public void Handle()
    {
        if(!Context.ApplyTo(new String[] {'HK'}))
        {
            return;
        }
        if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate))
        {
            if(trigger.new[0] instanceof Lead)
            {
                this.CountLeadBabyNumber();
            }
        }
        if(trigger.isAfter && (trigger.isInsert || trigger.isUndelete))
        {
            if(trigger.new[0] instanceof Attachment)
            {
                this.CountLeadBirthCertNumber();
            }
        }
        if(trigger.isAfter && trigger.isDelete)
        {
            if(trigger.old[0] instanceof Attachment)
            {
                this.CountLeadBirthCertNumber();
            }
        }
    }
    /*Count Baby number by birthday*/
    private void CountLeadBabyNumber()
    {
        List<Lead> list_Lead = trigger.new;
        Boolean isUpdate = trigger.isUpdate;
        for(Lead lea : list_Lead)
        {
            if(isUpdate)
            {
                Lead leaOld = (Lead)trigger.oldMap.get(lea.Id);
                if(leaOld.Birthday_1st_Child__c == lea.Birthday_1st_Child__c 
                && leaOld.Birthday_2nd_Child__c == lea.Birthday_2nd_Child__c 
                &&leaOld.Birthday_3rd_Child__c == lea.Birthday_3rd_Child__c 
                &&leaOld.Birthday_4th_Child__c == lea.Birthday_4th_Child__c)
                continue;
            }
            else
            {
                if(lea.Birthday_1st_Child__c == null 
                && lea.Birthday_2nd_Child__c == null 
                && lea.Birthday_3rd_Child__c == null
                && lea.Birthday_4th_Child__c == null)
                continue;
            }
            Integer i = 0;
            if(lea.Birthday_1st_Child__c != null) i++;
            if(lea.Birthday_2nd_Child__c != null) i++;
            if(lea.Birthday_3rd_Child__c != null) i++;
            if(lea.Birthday_4th_Child__c != null) i++;
            lea.Baby_Count__c = String.valueOf(i);
        }
    }
    
    /*Count Lead Birth Cert Numbers  by a field named Attachment_Count__c*/
    private void CountLeadBirthCertNumber()
    {
        List<Attachment> list_Attachment = new List<Attachment>();
        if(trigger.isDelete) list_Attachment = trigger.old;
        else list_Attachment = trigger.new;
        Set<Id> set_LeadId = new Set<Id>();
        for(Attachment att : list_Attachment)
        {//get LeadId
            if(att.ContentType == 'image/jpeg' && String.valueOf(att.ParentId).startsWith('00Q'))
            {
                set_LeadId.add(att.ParentId);
            }
        }
        if(set_LeadId.isEmpty()) return;
        List<Lead> list_Leads = [select Id, Attachment_Count__c,(select Id from Attachments where ContentType = 'image/jpeg') 
                                 from Lead where Id in :set_LeadId];
        if(list_Leads.isEmpty()) return;
        for(Lead lea : list_Leads)
        {
            lea.Attachment_Count__c = lea.Attachments.size();
            if(trigger.isDelete && lea.Attachment_Count__c == 0) lea.First_Birth_Cert_Modify_Time__c = DateTime.now();
            else if((!trigger.isDelete) && lea.Attachment_Count__c == 1) lea.First_Birth_Cert_Modify_Time__c = DateTime.now();
        }
        update list_Leads;
    }
}