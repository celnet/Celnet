/*
*owner:mark.li@celnet.com.cn
*date:2014-09-11
*
*/
global class WechatMenuApi 
{
    public class Menu
    {
        public Button[] button;
    }   
    
    public abstract class Button
    {
        public string name{get;set;}
    }
    
    public class Super_Button_Subs extends Button
    {
        public Super_SubButton[] sub_button;
    }
    
    public abstract class Super_Button_NoSubs extends Button
    {
        
    }
    
    public class ClickButton extends Super_Button_NoSubs
    {
        public string key{get;set;}
        public final string type = 'click';
    }
    
    public class ViewButton extends Super_Button_NoSubs
    {
        public string url{get;set;}
        public final string type = 'wiew';
    }
    
    public abstract class Super_SubButton extends Button
    {

    }
    
    public class ClickSubButton extends Super_SubButton
    {
        public string key{get;set;}
        public final string type = 'click';
    }
    
    public class ViewSubButton extends Super_SubButton
    {
        public string url{get;set;}
        public final string type = 'view';
    }
    
    //Create menu
    Webservice static void CreateMenu(string publicAccountName)
    {
        Menu me = WechatMenuController.GenerateMenuList();
        string meJson = JSON.serialize(me);
        WechatCalloutService wcs =new WechatCalloutService(publicAccountName);
        wcs.CreateMenu(meJson);
    }
    
    //Delete Menu
    Webservice static void DeleteMenu(string publicAccountName)
    {
        WechatCalloutService wcs =new WechatCalloutService(publicAccountName);
        wcs.DeleteMenu();
    }
}