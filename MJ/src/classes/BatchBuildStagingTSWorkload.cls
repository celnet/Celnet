/*
Batch job to build staging object: TS_Workload_Stats__c
for Report 1.1 《TS工作量统计分析报表》
key: User, Day, Month, Year and Called-To City
author: shiqi.ng@sg.fujitsu.com 
*/

global class BatchBuildStagingTSWorkload implements Database.Batchable<SObject>{
	private String userQuery;
	private Integer reportingDay;
    private Integer reportingMonth;
    private Integer reportingYear;
    
    private boolean chain = false;
    private Date endDate;
    
    /*Contructor: single day run*/
    public  BatchBuildStagingTSWorkload(String query,Integer year, Integer month, Integer day){
        userQuery = query;
        reportingDay = day;
        reportingMonth = month;
        reportingYear = year;
        chain = false;
    }
    /*Contructor: Multi Day run using chain batch (see finish method, double check bach size) 
	Usage example: system.scheduleBatch(new BatchBuildStagingTSWorkload(userQuery, 2014, 7, 16,endDate), 'Report 3-716', 1, 10); */
    public  BatchBuildStagingTSWorkload(String query,Integer year, Integer month, Integer day, Date endDate){
        userQuery = query;
        reportingDay = day;
        reportingMonth = month;
        reportingYear = year;
        if (Date.newInstance(year, month, day) < endDate){
            this.endDate = endDate;
            chain = true;
        }
        
    }
    
    /*Start Method*/
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(userQuery);
    }
    
    /*Execute*/
    global void execute(Database.BatchableContext bc, List<SObject> scope){

        //Put Users into Map
        Map<Id,User> userMap =  StagingReportUtil.getUserMap(scope);
        Set<Id> userIdSet = userMap.keySet();
        
        //Iniitialise rows
        List<TS_Workload_Stats__c> rows = new List<TS_Workload_Stats__c>();
        
        //Put Tasks by User Id into Map
        Map<Id,List<Task>> userTasks = 
            StagingReportUtil.queryCurrentDayTasksByUser(userIdSet,reportingYear,reportingMonth, reportingDay);
        
        //Flatten Tasks map into list
        List<Task> tasks = new List<Task>();
        for (List<Task> t : userTasks.values()){
        	tasks.addAll(t);
        }
        //Get Contacts into Map, filter success only = false (i.e. all contacts)
        Map<Id,Contact> contactMap = StagingReportUtil.queryContactsFromTasks(tasks,false);
        
        //Get Contacts into Map, filter success only = true 
        Map<Id,Contact> contactSuccessMap = StagingReportUtil.queryContactsFromTasks(tasks,true);
        
        //Get Answers from Sucessful Task.Contact.Questionnaire
        Map<Id,List<Answer__c>> contactIdAnswers 
                    = StagingReportUtil.queryAnswersByContacts(contactSuccessMap.keySet());
        //Query Past Answers
        List<Answer__c> pastAnswers 
             = StagingReportUtil.queryPastAnswers(
                 contactMap.keySet(),
                 Date.newInstance(reportingYear, reportingMonth, reportingDay)
             );
                
        //For each User 
        for (Id userId : userTasks.keySet()){
            User currentUser = userMap.get(userId);
            
            //一个Contact有多个Task，由Task找Contact再找Answer，可能导致同一个Contact及对应Answer被多次统计
            //Set<Id> Set_ContactId = new Set<Id>();
                
        	//for each task
            for (Task thisTask : userTasks.get(userId)){
            	//get contact related to task
            	Contact thisContact = contactMap.get(thisTask.WhoId);
                //get contact.household.city
                String city = thisContact.Account.Standard_City__r.Sales_City__c;
                //get/create staging row based on city, user, month,year
                TS_Workload_Stats__c row 
                    = StagingReportUtil.getTSWorkloadStats(rows,reportingYear,reportingMonth,reportingDay,currentUser,city);

                debug('entering U:'+userId+'task:'+thisTask.id);
                
                
                String lastProduct = null;
                String currentProduct = null;
                boolean lastNonMJNowMJ = false;
                
               /*  if(Set_ContactId.contains(thisContact.Id))
                {
                	continue;
                }
                else
                {
                	Set_ContactId.add(thisContact.Id);
                }*/
                
                /*废卡数 , 致电数, 成功完成问卷数, 成功致电率*/
                row.Total_Call__c ++;
                if (thisTask.Result__c == 'Inactive'){
                    row.Invalid_Card__c ++; 
                }else if (thisTask.Result__c == 'Connected/Closed'){
                    row.Total_Successful_Call__c ++;
                }
                
                
                List<Answer__c> thisContactAnswers = contactIdAnswers.get(thisContact.Id);
                system.debug('All answers for this con: '+thisContactAnswers);
                Date reportingDate = Date.newInstance(reportingYear,reportingMonth,reportingDay);
                /*====一个联系人一天可以有多个answer 2014-7-15注释--Start */
                //Answer__c thisAnswer = StagingReportUtil.getCurrentAnswer(thisContactAnswers,reportingDate);
                 /*2014-7-15注释--End*/
                /*2014-7-15调整*/
                if(thisContactAnswers==null)
                continue;
                
                for(Answer__c thisAnswer : thisContactAnswers)
                {
                	if(thisAnswer.Questionnaire__r.Type__c !='Routine' )
                		continue; 
                    	
                	DateTime d = thisAnswer.createdDate;
            		Date createdDate = Date.newInstance(d.year() , d.month() , d.day() );
            		if(createdDate != reportingDate)
            		continue;
            		
            		
            		//原逻辑
            		lastProduct = thisAnswer.Q008_Last__c;
                    currentProduct = thisAnswer.Q008__c;
                    /*上次非MJ数 , 本次转MJ数*/
                    if (lastProduct != 'MJN' && lastProduct != 'MJN(GC)' && lastProduct !=NULL){  
                        row.Total_Last_NonMJ__c ++;
                        if (currentProduct == 'MJN' || currentProduct == 'MJN(GC)'){
                        	row.Total_Converted_MJ__c ++;    
                            lastNonMJNowMJ = true;
                        }
                    }
                    /*上次MJ数 , 本次MJ数 */
                    if (lastProduct == 'MJN' || lastProduct == 'MJN(GC)'){
                        row.Total_Last_MJ__c ++;
                        if (currentProduct == 'MJN' || currentProduct == 'MJN(GC)'){
                            row.Total_MJ__c ++;
                        }
                    }
                    /*S2派发数, S3派发数*/
                    
                    if (thisAnswer.Q033__c == 'Y'){
                        row.Total_S2_given__c ++;
                    }
                    if (thisAnswer.Q034__c == 'Y'){
                        row.Total_S3_given__c ++;
                    }
                    
                    /*S2上次派发数, S2转换新客数*/
                    if (thisAnswer.Q038__c == 'Y'){ //今天拨打成功产生问卷且有收到S2(Q38=是)
                        //该TS 非今天创建的问卷中，Q033=是 
                        for (Answer__c pastAns : pastAnswers){
                            //check if past answers same created by as TS?
                            if (pastAns.Contact__c == thisContact.Id && pastAns.Q033__c == 'Y'){ 
                                row.Total_S2_given_previous__c ++;
                                if (lastNonMJNowMJ){
                                    row.S2_New_Customer_Converted__c ++;    
                                }
                            }
                        }
                    }
                    /* S3上次派发数, S3转换新客数 */
                    if (thisAnswer.Q039__c == 'Y'){ //今天拨打成功产生问卷且有收到S2(Q38=是)
                        //该TS 非今天创建的问卷中，Q033=是 
                        for (Answer__c pastAns : pastAnswers){
                            //check if past answers same created by as TS?
                            if (pastAns.Contact__c == thisContact.Id && pastAns.Q034__c == 'Y'){ 
                                row.Total_S3_given_previous__c ++;
                                if (lastNonMJNowMJ){
                                    row.S3_New_Customer_Converted__c ++;    
                                }
                            }
                        }
                    }
                }
                
               // if (thisAnswer != null){
                    
               // }//end if thisAnswer!=null
                
                /*[Columns: Age -3 to 11 months]*/
                debug('C id:'+thisContact.id);
                debug('C birthday:'+thisContact.birthdate);
                debug('task created date:'+thisTask.CreatedDate);
                if (thisContact.Birthdate != null){
                    Integer ageInDays = StagingReportUtil.getAgeInDays(thisTask.CreatedDate,thisContact.Birthdate);
                    debug('ageInDays:'+ageInDays);
                    if (ageInDays > -90 && ageInDays <= -60 ){
                        row.total_age_minus3__c++;
                    }else if (ageInDays > -30 && ageInDays <= 0 ){
                        row.total_age_minus1__c++;
                    }else if (ageInDays > 0 && ageInDays <= 30 ){
                        row.total_age_1__c++;
                    }else if (ageInDays > 60 && ageInDays <= 90 ){
                        row.total_age_3__c++;
                    }else if (ageInDays > 120 && ageInDays <= 150 ){
                        row.total_age_5__c++;
                    }else if (ageInDays > 180 && ageInDays <= 210 ){
                        row.total_age_7__c++;
                    }else if (ageInDays > 300 && ageInDays <= 330 ){
                        row.total_age_11__c++;
                    }
                }//end if birthday != null
                
        	}//end for each task    
        }//end for each user
        
        try{
            system.debug(rows.size());
            system.debug(rows);
        	upsert rows External_ID__c;
        }catch (Exception ex){
            system.debug(system.LoggingLevel.ERROR, ex.getMessage());
            system.debug(system.LoggingLevel.ERROR, ex.getStackTraceString());
            throw ex;
        }
            
    }
    global void finish(Database.BatchableContext bc){
        if (chain){
            Date nextDate = Date.newInstance(reportingYear, reportingMonth, reportingDay).addDays(1);
            
            system.scheduleBatch(new BatchBuildStagingTSWorkload(userQuery, nextDate.year(), nextDate.month(), nextDate.day(),endDate), 
                                 'Report 1-'+nextDate, 
                                 1, //start in one minute
                                 10); //batch size 10
        }
    }
    
    private void debug(String msg){
        system.debug(System.LoggingLevel.INFO, msg);
    }
}