public with sharing virtual class ComponentControllerBase {
  
  public String componentKey{ get;
    set{
      if(value != null){
         componentKey  = value;
         if(parentPageController != null){
            parentPageController.setComponentControllerMap(componentKey, this);
         }
      }
    }
  }

  public PageControllerBase parentPageController { get; 
    set {
	    if (value != null) {
			parentPageController = value;
			parentPageController.setComponentController(this);
		}
    }
  }
  
}