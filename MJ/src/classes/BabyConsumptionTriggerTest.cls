/*
Apply to [HK/CN/ALL]: HK
*/
@isTest
public class BabyConsumptionTriggerTest{
    public static testMethod void testBabyConsumptionTrigger()
    {
    	system.runas(TestUtility.new_HK_User()){
    	//Create custom settings
        TestUtility.setupMemberIDCustomSettings();
        
        //Create test account
        Account account = New Account();
        account.Name = 'Test';
        account.Last_Name__c = 'Test';
        insert account;
        
        //Creat test baby contact
        Contact contact = New Contact();
        contact.FirstName='Baby';
        contact.LastName='Last';
        contact.AccountId=account.ID;
        contact.Birthdate = system.today().addYears(-6);
        insert contact;
        
        //Creat Product 1
        Product__c product1 = new Product__c();
        product1.Name = 'Product 1';
        product1.Product_Code__c = 'Product01';
        insert product1;
        
        //Creat Product 2
        Product__c product2 = new Product__c();
        product2.Name = 'Product 2';
        product2.Product_Code__c = 'Product02';
        insert product2;
        
        //Creat Product 3
        Product__c product3 = new Product__c();
        product3.Name = 'Product 3';
        product3.Product_Code__c = 'Product03';
        insert product3;
        
        //Create consumption record 1
        Baby_Consumption__c consumption1 = new Baby_Consumption__c();
        consumption1.Contact__c=contact.ID;
        consumption1.Product__c=product1.ID;
        consumption1.From_Age__c=0;
        consumption1.To_Age__c=1;
        insert consumption1;
        
        Contact savedContact = [SELECT ID,Last_Consumption_Product_Name__c,X2nd_Last_Consumption_Product_Name__c
                                FROM Contact WHERE ID = : contact.ID];
        System.assertEquals('Product 1',savedContact.Last_Consumption_Product_Name__c);
        
        //Create consumption record 2
        Baby_Consumption__c consumption2 = new Baby_Consumption__c();
        consumption2.Contact__c=contact.ID;
        consumption2.Product__c=product2.ID;
        consumption2.From_Age__c=1;
        consumption2.To_Age__c=2;
        insert consumption2;
        
        savedContact = [SELECT ID,Last_Consumption_Product_Name__c,X2nd_Last_Consumption_Product_Name__c
                                FROM Contact WHERE ID = : contact.ID];
        System.assertEquals('Product 2',savedContact.Last_Consumption_Product_Name__c);
        System.assertEquals('Product 1',savedContact.X2nd_Last_Consumption_Product_Name__c);
        
        //Create consumption record 3
        Baby_Consumption__c consumption3 = new Baby_Consumption__c();
        consumption3.Contact__c=contact.ID;
        consumption3.Product__c=product3.ID;
        consumption3.From_Age__c=2;
        consumption3.To_Age__c=3;
        insert consumption3;
        
        savedContact = [SELECT ID,Last_Consumption_Product_Name__c,X2nd_Last_Consumption_Product_Name__c
                                FROM Contact WHERE ID = : contact.ID];
        System.assertEquals('Product 3',savedContact.Last_Consumption_Product_Name__c);
        System.assertEquals('Product 2',savedContact.X2nd_Last_Consumption_Product_Name__c);
        
        //Update consumption record 1
        consumption1.From_Age__c=3;
        consumption1.To_Age__c=4;
        update consumption1;
        
        savedContact = [SELECT ID,Last_Consumption_Product_Name__c,X2nd_Last_Consumption_Product_Name__c
                                FROM Contact WHERE ID = : contact.ID];
        System.assertEquals('Product 1',savedContact.Last_Consumption_Product_Name__c);
        System.assertEquals('Product 3',savedContact.X2nd_Last_Consumption_Product_Name__c);
        
        //Delete consumption record 3
        delete consumption3;
        
        savedContact = [SELECT ID,Last_Consumption_Product_Name__c,X2nd_Last_Consumption_Product_Name__c
                                FROM Contact WHERE ID = : contact.ID];
        System.assertEquals('Product 1',savedContact.Last_Consumption_Product_Name__c);
        System.assertEquals('Product 2',savedContact.X2nd_Last_Consumption_Product_Name__c);
    	}
    }
}