/* Common Utility Class */
public class CommonHelper{
    
    //Global String Variable
    public static final String CALL_CONNECTED = 'Connected/Closed';
        
    public static final String HOUSEHOLD_RT = 'HouseHold';
    public static final String HOUSEHOLD_NEW_RT = 'HouseHold_New';
    public static final String HOUSEHOLD_CHILD_RT = 'Household_Child';
    public static final String HOUSEHOLD_PARENT_RT = 'Household_Parent';
    
    public static final String KEY_DNCR_EMAIL = 'DNCR Support Email';
    public static final String KEY_DNCR_OFFSET_HOURS = 'DNCR Offset Hours';
    public static final String KEY_ADMIN_EMAIL = 'System Administrator Email';
    public static final String EMAIL_DELIMITER = ';';
    
    public static final String DNCR_LOAD_FAILED = 'Bulk Import Data Failed';
    public static final String DNCR_DUPLICATED_JOB = 'There is another job running in progress';
    
    //Validation Error Messages
    public static final String VALUE_REQUIRED = 'You must enter a value';
    public static final String PRODUCT_REQUIRED = 'You must select a Product';
    public static final String PRODUCT_REQUIRED_FOR_COMPLAINT = 'You must select a Product for Complaint';
    public static final String ORDER_NOT_FOUND = 'Associated Order not found. Please make sure you are accessing from the right place!';
    public static final String BLANK_ORDER = 'This order can not be submitted as there are no order items.';
    public static final String ORDER_ALREADY_SUBMITTED = 'This order has already been submitted!';
    public static final String NO_ORDER_SELECTED = 'No order is selected!';
    public static final String NO_VALID_ORDER = 'There are no valid order(s) can be submitted';
    public static final String CANCELLED_ORDER = 'Cancelled Order can not be submitted';
    public static final String SEND_TO_SFA_NOT_ALLOWED = 'This Order can not be submitted to SFA. Please verify the "Need Send to SFA?" Status: e.g.Delivery Center.';
    public static final String CANCELLED_ORDER_NOT_ALLOWED = 'This order has been cancelled, no deletion allowed';
    public static final String PUSHED_ORDER_NOT_ALLOWED = 'This order has been pushed to SFA, no deletion allowed';
    //public static final String PUSHED_ORDER_ITEM_EDIT_NOT_ALLOWED = 'This order has been pushed to SFA. ';
    public static final String FAILED_ORDER_ITEM_EDIT_NOT_ALLOWED = 'This order has been rejected, no modifications allowed';
    public static final String CANCELLED_ORDER_ITEM_EDIT_NOT_ALLOWED = 'This order has been cancelled, no modifications allowed';
    public static final String NO_PUSH_PERMISSION = 'You have no permssion to use this function';
    
    //Coupon Cancel Reasons
    public static final String COUPON_TYPE_CHANGED = 'Fulfillment/Campaign type changed';
    public static final String COUPON_CANCELLED = 'Fulfillment/Campaign cancelled';
    public static List<Product__c> getMJNProducts()
    {
        List<Product__c> MJNProducts = 
            [SELECT ID,Name,Default_Order_Quantity__c,Allowed_Order_Period__c,Unit_Price__c
             FROM Product__c 
             WHERE Product_Nature__c = 'MJ' AND Inactive__c = false AND Is_DSO_Product__c = false
             Order By Name ASC];
        return MJNProducts;
    }
    
    public static List<Product__c> getDSOProducts()
    {
        List<Product__c> DSOProducts = 
            [SELECT ID,Name,Product_SKU__c,Default_Order_Quantity__c,Allowed_Order_Period__c,Unit_Price__c,Max_Order_Quantity__c
             FROM Product__c 
             WHERE Product_Nature__c = 'MJ' AND Inactive__c = false AND Is_DSO_Product__c = true
             Order By Name ASC];
        return DSOProducts;
    }
    
    public static List<Product__c> getBabyConsumptionProducts()
    {
        List<Product__c> BCProducts = 
            [SELECT ID,Name,Default_Order_Quantity__c,Allowed_Order_Period__c,Unit_Price__c
             FROM Product__c 
             WHERE Inactive__c = false AND Is_DSO_Product__c = false
             Order By Name ASC];
        return BCProducts;
    }
    
	public static void notifySystemException(MJNCustomException newException)
    {
        try
       	{                
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
           // Get email addresses from custom seeting
           List<String> toAddresses = CommonHelper.getAdminEmailAddrs();

           if(toAddresses != NULL || toAddresses.size()>0)
           {
               mail.setToAddresses(toAddresses);
               mail.setSubject('[MJN] System Exception:' + newException.getMessage());
               String htmlBoby = '<HTML><body>';
               htmlBoby += '<b>Error Message:</b> '+ newException.getMessage() + '<br/>';
               htmlBoby += '<b>Stack Trace:</b> '+ newException.getStackTraceString() + '<br/>';
               htmlBoby += '</body></html>';
               mail.setHtmlBody(htmlBoby);
               Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
           }
        }
        catch(System.EmailException ex)
        {
          system.debug('Sending Email Failed :' + ex.getMessage());
        }
    }
    
    public static List<String> getAdminEmailAddrs()
    {
        List<String> emailList = new List <String>();
        Key_Value_Mapping__c setting = Key_Value_Mapping__c.getValues(KEY_ADMIN_EMAIL);
        if(setting != NULL)
        {
        	String value = setting.Value__c;
            if(value != NULL)
            {
                List<String> splitStrs = value.split(EMAIL_DELIMITER);
                for(String email : splitStrs)
                {
                    if(email != NULL && email.length()>0)
                        emailList.add(email);
                }
            }    
        }
        return emailList;
    }
    
    public static Boolean isTriggerDisabled(){
    	Rule_Settings__c settings = Rule_Settings__c.getInstance(); //default to Current Logged In User
    	system.debug('Trigger Rule Setting DISABLED = '+settings.Disable_Trigger__c);
    	return settings.Disable_Trigger__c;
    	
    }
    public class MJNCustomException extends Exception{}
}