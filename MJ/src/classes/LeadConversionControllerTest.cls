/*
	VF Test Class
		Assert - No Lead ID validation
		Simulate search
			assert test result
			assert ranking
			assert parent display
		
		Not done/less urgent:
		Simulate different search criteria selections
		Simulate user radio/check box selection
			assert validations
			test for actual conversion is done over at LeadConversionHelperTest
	Apply to [HK/CN/ALL]: HK 
*/
@isTest
private class LeadConversionControllerTest {

    static testMethod void IDisNullTest() {
    	LeadConversionController controller = setupPageController(null);
    	system.assertEquals(LeadConversionController.LEAD_ID_NOT_FOUND ,
    				 ApexPages.getMessages()[0].getSummary());
    }
    
    static testMethod void zeroResultCase(){
      system.runas(TestUtility.new_HK_User()){
    	Lead lead = new Lead();
        lead.LastName = 'PARENT';
        lead.FirstName = 'TEST';
        lead.First_Name_1st_Child__c = 'BABY';
        lead.Last_Name_1st_Child__c = 'FIRST';
        lead.Birthday_1st_Child__c = system.today();
        lead.First_Name_2nd_Child__c = 'BABY';
        lead.Last_Name_2nd_Child__c = 'SECOND';
         lead.Birthday_2nd_Child__c = system.today();
        lead.First_Name_3rd_Child__c = 'BABY';
        lead.Last_Name_3rd_Child__c = 'THIRD';
         lead.Birthday_3rd_Child__c = system.today();
        
        insert lead;
        LeadConversionController controller = setupPageController(lead);
        
        //assert show result
        system.assertEquals(false, controller.showNewHouseholdBlock);
        
        controller.doSearch();
        
        //assert zero results
        system.assertEquals(0,controller.getHouseholdResults().size());
        //assert show result
        system.assertEquals(true, controller.showNewHouseholdBlock);
        system.assertEquals(true, controller.newHouseholdDisabled);
        system.assertEquals(true, controller.newHouseholdSelected);
                
        //assert Cancel Button
        String cancelURL = controller.doCancel().getURL();
        system.assertEquals('/'+lead.Id,cancelURL);
      }
    }
    
    static testMethod void zeroResultCreateNewHouseholdCase(){
      system.runas(TestUtility.new_HK_User()){
    	//Create custom settings
        TestUtility.setupMemberIDCustomSettings();
        
    	Lead lead = new Lead();
        lead.LastName = 'PARENT';
        lead.FirstName = 'TEST';
        lead.First_Name_1st_Child__c = 'BABY';
        lead.Last_Name_1st_Child__c = 'FIRST';
        lead.Birthday_1st_Child__c = system.today();
        lead.First_Name_2nd_Child__c = 'BABY';
        lead.Last_Name_2nd_Child__c = 'SECOND';
         lead.Birthday_2nd_Child__c = system.today();
        lead.First_Name_3rd_Child__c = 'BABY';
        lead.Last_Name_3rd_Child__c = 'THIRD';
         lead.Birthday_3rd_Child__c = system.today();
        
        insert lead;
        LeadConversionController controller = setupPageController(lead);
        
        //assert show result
        system.assertEquals(false, controller.showNewHouseholdBlock);
        
        controller.doSearch();
        
        //assert zero results
        system.assertEquals(0,controller.getHouseholdResults().size());
        //assert show result
        system.assertEquals(true, controller.showNewHouseholdBlock);
        system.assertEquals(true, controller.newHouseholdDisabled);
        system.assertEquals(true, controller.newHouseholdSelected);
                
        //assert Convert Button
        controller.doConvertLead();
        system.assertEquals(0,ApexPages.getMessages().size()); 
      }
    }
    static testMethod void someResultCase(){
      system.runas(TestUtility.new_HK_User()){
    	Lead lead = new Lead();
        lead.LastName = 'LAM';
        lead.FirstName = 'MARY';
        lead.Home_Phone__c = '12345678';
        
        lead.First_Name_1st_Child__c = 'BABY';
        lead.Last_Name_1st_Child__c = 'FIRST';
        lead.Birthday_1st_Child__c = system.today();
        lead.First_Name_2nd_Child__c = 'BABY';
        lead.Last_Name_2nd_Child__c = 'SECOND';
         lead.Birthday_2nd_Child__c = system.today();
        lead.First_Name_3rd_Child__c = 'BABY';
        lead.Last_Name_3rd_Child__c = 'THIRD';
         lead.Birthday_3rd_Child__c = system.today();
        
        insert lead;
        Id leadId = lead.id;
        
        LeadConversionController controller = setupPageController(lead);
        
        //Create custom settings
        TestUtility.setupMemberIDCustomSettings();
        
        //Create 4 exisiting households, 3 are possible duplicates
        setupExistingHouseholdA();
        setupExistingHouseholdB();
        setupExistingHouseholdC();
        setupExistingHouseholdD();
        //assert 4 hh
        system.assertEquals(4,[SELECT COUNT() FROM account]);
        
        controller.doSearch();
        List<HouseholdWrapper> results = controller.getHouseholdResults(); 
         
         //assert show result
        system.assertEquals(true, controller.showNewHouseholdBlock);
        system.assertEquals(false, controller.newHouseholdDisabled);
        system.assertEquals(false, controller.newHouseholdSelected);
        
        //assert results
        system.assertEquals(3,results.size());
        system.debug(results);
        
        //assert ranking
        //household B, match phone and account name
        system.assert(results[0].household.Name.contains('MARY'));
        system.assertEquals('12345678',results[0].household.Home_Phone__c);        
        system.assert(results[0].babies[0].Name.contains('JACK'));
        
        //household C, match mobile phone
        system.assert(results[1].household.Name.contains('LILY'));
        system.assertEquals('12345678',results[1].household.Mobile_Phone__c);
        //household A, match account name
        system.assert(results[2].household.Name.contains('MARY'));
        //assert display parent name
        system.assert(results[1].getParentName().contains('LILY'));
      }
    }
    
    static LeadConversionController setupPageController(Lead lead){
      
    	PageReference leadConvertPage = Page.LeadConversion;
    	
        if (lead==null){
        	lead = TestUtility.generateTestLeads(1)[0];
        }else{
        	leadConvertPage.getParameters().put('ID',lead.Id);
        	Test.setCurrentPageReference(leadConvertPage);
        }
        	
        ApexPages.StandardController stdCont = new ApexPages.StandardController(lead);
        LeadConversionController controller = new LeadConversionController(stdCont);
        lead = controller.getLead(); //read all fields from query(constructor)
        //add search comp        
        SearchComponentController searchController = new SearchComponentController();
        searchController.lead = lead;
        searchController.parentPageController = controller;
        searchController.componentKey='searchFormController';
        searchController.setupSearchFields();
        searchController.searchOperator = 'OR';
        controller.setComponentControllerMap('searchFormController',searchController);
        
        //assert base class methods
        system.assertEquals(controller,controller.getThis());
        system.assertEquals(searchController,controller.getMyComponentController());
        
        return controller;
      
    }
    
    static void setupExistingHouseholdA(){
        //Create household 
        Account acc = new Account();
        acc.RecordTypeId =  ObjectUtil.GetRecordTypesByDevName(Account.SObjectType).get(CommonHelper.HOUSEHOLD_RT).Id;
        
        acc.Name = 'LAM MARY';
        acc.Last_Name__c = 'LAM';
        acc.First_Name__c = 'MARY';
        insert acc;
        system.debug([SELECT Name, FirstName, LastName from Contact where AccountId = :acc.Id]);
       	//create another parent + baby! 
       	List<Contact> cons = new List<Contact>();
       		Contact baby = new Contact();
       		baby.LastName = 'LAM';
       		baby.FirstName = 'BILLY';
       		baby.RecordTypeId = ObjectUtil.GetRecordTypesByDevName(Contact.SObjectType).get(CommonHelper.HOUSEHOLD_CHILD_RT).Id;
       		baby.AccountId = acc.Id;
       		cons.add(baby);
        Contact parent = new Contact();
       		parent.LastName = 'LAM';
       		parent.FirstName = 'PETER';
       		parent.Chinese_Name__c = '蓝 陂德';
       		parent.RecordTypeId = ObjectUtil.GetRecordTypesByDevName(Contact.SObjectType).get(CommonHelper.HOUSEHOLD_PARENT_RT).Id;
       		parent.AccountId = acc.Id;
       		cons.add(parent);
       	
       	insert cons;
    }
    
    static void setupExistingHouseholdB(){ 
        //Create household 
        Account acc = new Account();
        acc.RecordTypeId =  ObjectUtil.GetRecordTypesByDevName(Account.SObjectType).get(CommonHelper.HOUSEHOLD_RT).Id;
        
        acc.Name = 'LAM MARY';
        acc.Last_Name__c = 'LAM';
        acc.First_Name__c = 'MARY';
        acc.Home_Phone__c = '12345678';
        insert acc;
       	//create  baby! 
       	List<Contact> cons = new List<Contact>();
       		Contact baby = new Contact();
       		baby.LastName = 'LAM';
       		baby.FirstName = 'JACK';
       		baby.RecordTypeId = ObjectUtil.GetRecordTypesByDevName(Contact.SObjectType).get(CommonHelper.HOUSEHOLD_CHILD_RT).Id;
       		baby.AccountId = acc.Id;
       		cons.add(baby);
       	
       	insert cons;
    } 
    
    static void setupExistingHouseholdC(){
        //Create household 
        Account acc = new Account();
        acc.RecordTypeId =  ObjectUtil.GetRecordTypesByDevName(Account.SObjectType).get(CommonHelper.HOUSEHOLD_RT).Id;
        
        acc.Name = 'CHAN LILY';
        acc.Last_Name__c = 'CHAN';
        acc.First_Name__c = 'LILY';
        acc.Mobile_Phone__c = '12345678';
        insert acc;
       	//create  baby! 
       	List<Contact> cons = new List<Contact>();
       		Contact baby = new Contact();
       		baby.LastName = 'Chan';
       		baby.FirstName = 'Nancy';
       		baby.RecordTypeId = ObjectUtil.GetRecordTypesByDevName(Contact.SObjectType).get(CommonHelper.HOUSEHOLD_CHILD_RT).Id;
       		baby.AccountId = acc.Id;
       		cons.add(baby);
       	
       	insert cons;
    }
    
    static void setupExistingHouseholdD(){
        //Create household 
        Account acc = new Account();
        acc.RecordTypeId =  ObjectUtil.GetRecordTypesByDevName(Account.SObjectType).get(CommonHelper.HOUSEHOLD_RT).Id;
        
        acc.Name = 'Liu Sandy';
        acc.Last_Name__c = 'Liu';
        acc.First_Name__c = 'Sandy';
        acc.Mobile_Phone__c = '123456789';
        insert acc;
       	
    }
}