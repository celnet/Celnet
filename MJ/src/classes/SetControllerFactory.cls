/**
* This factory class is for getting new instances of SetController for the Contact and Lead object
*  Called by VF page (Recurring Campaign Setting) and Apex Batch (CreateBatchCampaign)
*/
public with sharing class SetControllerFactory {
	
	
   /**
   *	SetController for Contact
   */
   public static ApexPages.StandardSetController newContactController() {
        
            return new ApexPages.StandardSetController(Database.getQueryLocator(
                [SELECT id FROM Contact LIMIT 1]));
        
    }
    
    /**
    * SetController for Lead
    */
    public static ApexPages.StandardSetController newLeadController(){ 
        
             return new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT id FROM Lead LIMIT 1]));
        
    }
}