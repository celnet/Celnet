/*
*Author:michael.mi@celnet.com.cn
*Date:2014-09-04
*Function:Wechat callout Queue Manager Method:EnQueue(),DeQueue(),FinalizeTasks(),HasTask()
*/
public class WechatCalloutQueueManager 
{
	public static final string TASK_TYPE_WAITING = 'Waiting';
	private static final string TASK_TYPE_EXECUTING = 'Executing';
	public static final string TASK_TYPE_FAILURE = 'Failure';
	
	//DeQueue Order:1.User 2.Image
	public static list<Wechat_Callout_Task_Queue__c> DeQueue()
	{
		list<Wechat_Callout_Task_Queue__c> tasks = new list<Wechat_Callout_Task_Queue__c>();
		//出队类型：获取微信用户详细信息
		tasks = [select Type__c, Status__c, Public_Account_Name__c, Processor_Name__c, Prioroty__c, Open_ID__c, Name, Msg_Body__c, Media_ID__c, Id, Error_Log__c
				from Wechat_Callout_Task_Queue__c 
				where Type__c = :WechatEntity.EVENT_TYPE_SUBSCRIBE
				and Status__c = :TASK_TYPE_WAITING
				order by CreatedDate asc];
		//出队类型：下载照片队列
		if(tasks.isempty())
		{
			tasks = [select Type__c, Status__c, Public_Account_Name__c, Processor_Name__c, Prioroty__c, Open_ID__c, Name, Msg_Body__c, Media_ID__c, Id, Error_Log__c
					from Wechat_Callout_Task_Queue__c 
					where Type__c = :WechatEntity.MESSAGE_TYPE_IMAGE
					and Status__c = :TASK_TYPE_WAITING
					order by CreatedDate asc limit 1];
		}
		//出队类型：发送文本信息通过客服接口
		if(tasks.isempty())
		{
			tasks = [select Type__c, Status__c, Public_Account_Name__c, Processor_Name__c, Prioroty__c, Open_ID__c, Name, Msg_Body__c, Media_ID__c, Id, Error_Log__c
					from Wechat_Callout_Task_Queue__c 
					where Type__c = :WechatEntity.MESSAGE_TYPE_TEXT
					and Status__c = :TASK_TYPE_WAITING
					order by CreatedDate asc];
		}
		//出队类型：下载语音信息
		if(tasks.isempty())
		{
			tasks = [select Type__c, Status__c, Public_Account_Name__c, Processor_Name__c, Prioroty__c, Open_ID__c, Name, Msg_Body__c, Media_ID__c, Id, Error_Log__c
					from Wechat_Callout_Task_Queue__c 
					where Type__c = :WechatEntity.MESSAGE_TYPE_VOICE
					and Status__c = :TASK_TYPE_WAITING
					order by CreatedDate asc];
		}
		//出队类型：调用微信客服接口发送图文信息
		if(tasks.isempty())
		{
			tasks = [select Type__c, Status__c, Public_Account_Name__c, Processor_Name__c, Prioroty__c, Open_ID__c, Name, Msg_Body__c, Media_ID__c, Id, Error_Log__c
					from Wechat_Callout_Task_Queue__c 
					where Type__c = :WechatEntity.MESSAGE_TYPE_News
					and Status__c = :TASK_TYPE_WAITING
					order by CreatedDate asc];
		}
		//这个队列是往CMRS的接口发送语语音文件，跟底层代码没有关系，但是共用了一个calloutprocessor所以迁移的时候要注意不用迁移这个
		if(tasks.isempty())
		{
			tasks = [select Type__c, Status__c, Public_Account_Name__c, Processor_Name__c, Prioroty__c, Open_ID__c, Name, Msg_Body__c, Media_ID__c, Id, Error_Log__c
					from Wechat_Callout_Task_Queue__c 
					where Type__c = 'SendVoice'
					and Status__c = :TASK_TYPE_WAITING
					order by CreatedDate asc limit 1];
		}
		//执行出队操作：更新Task状态为Executing
		for(Wechat_Callout_Task_Queue__c task : tasks)
		{
			task.Status__c = TASK_TYPE_EXECUTING;
		}
		//Update status to Executing
		update tasks;
		return tasks;
	}
	//EnQueue Parameter:Task
	public static void EnQueue(Wechat_Callout_Task_Queue__c task)
	{
		if(task == null)
		{	
			return;
		}
		if(task.Type__c == WechatEntity.MESSAGE_TYPE_VOICE)
		{
			if(![Select ID From Wechat_Callout_Task_Queue__c where Msg_ID__c = :task.Msg_ID__c].isempty())
			{
				return;
			}
		}
		insert task;
	}
	//EnQueue Parameter:Tasks
	public static void EnQueue(list<Wechat_Callout_Task_Queue__c> tasks)
	{
		if(!tasks.isempty())
		{
			insert tasks;	
		}
	}
	//Delete Success Task
	public static void FinalizeTasks(list<Wechat_Callout_Task_Queue__c> tasks)
	{
		if(!tasks.isempty())
		{
			delete tasks;	
		}
	}
	//Delete Success Task
	public static void FinalizeTasks(Wechat_Callout_Task_Queue__c task)
	{
		if(task != null)
		{
			delete task;	
		}
	}
	//Validate has Wating Task
	public static boolean HasTask()
	{
		list<Wechat_Callout_Task_Queue__c> taskList = [Select ID
													  From Wechat_Callout_Task_Queue__c t 
													  where Status__c = :TASK_TYPE_WAITING];
		if(!taskList.isempty())
		{
			return true;
		}
		return false;
	}
	//生成调用微信客服接口发送文本类型消息的队列参数：公众号名称，消息内容，处理此队列的Processor
	public static Wechat_Callout_Task_Queue__c CreateSendSvcTextMsgTask(string PublicAccountName,string MsgBody,string ProcessorName)
	{
		Wechat_Callout_Task_Queue__c textQueue = new Wechat_Callout_Task_Queue__c();
		textQueue.Public_Account_Name__c = PublicAccountName;
		textQueue.Type__c = WechatEntity.MESSAGE_TYPE_TEXT;
		textQueue.Msg_Body__c = MsgBody;
		textQueue.Processor_Name__c = ProcessorName;
		textQueue.Name = ProcessorName;
		return textQueue;
	}
}