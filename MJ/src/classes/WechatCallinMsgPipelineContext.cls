/*
*Author:michael.mi@celnet.com.cn
*Date:2014-09-04
*Function:runtime parameter
*/
public class WechatCallinMsgPipelineContext 
{
	private WechatEntity.CallinBaseMsg BaseInMsg;
	public WechatEntity.CallinBaseMsg OutMsg {get; set;}
	private map<string,object> BaseParameterMap;
	private string BasePublicAccountName;
	public WechatEntity.CallinBaseMsg InMsg
	{
		get
		{
			return this.BaseInMsg;
		}
	}
	public map<string,object> ParameterMap 
	{
		get
		{
			return this.BaseParameterMap;
		}
	}
	public string PublicAccountName
	{
		get
		{
			return this.BasePublicAccountName;
		}
	}
	
	public WechatCallinMsgPipelineContext(WechatEntity.CallinBaseMsg BaseInMsg,string BasePublicAccountName)
	{
		this.BaseInMsg = BaseInMsg;
		this.BasePublicAccountName = BasePublicAccountName;
		this.BaseParameterMap = new map<string,object>();
	}
}