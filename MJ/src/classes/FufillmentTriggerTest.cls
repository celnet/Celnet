/* The org must have Coupon_ID_Setting__c configured
*/
@isTest
public class FufillmentTriggerTest{
    
    public static testmethod void testFufillmentNewCoupon()
    {
        Contact contact = setup();        
        //Test case 1: New Fulfillment Record
        Fulfillment__c newRecord = new Fulfillment__c();
        newRecord.Contact__c = contact.ID;
        newRecord.Type__c = 'Coupon'; 
        newRecord.Material_Name__c = 'TEST'; 
        newRecord.Reason_for_fulfillment__c = 'Reason';
        insert newRecord;
	
        newRecord = [SELECT ID,Coupon_I__c,Coupon_I__r.Bar_Code__c,Coupon_II__c,Coupon_II__r.Bar_Code__c,Cancelled__c 
                     FROM Fulfillment__c WHERE ID =:newRecord.ID];
        String curYear = String.valueOf(Date.today().year());
        String YY = curYear.substring(2);
        ID coupon1ID = newRecord.Coupon_I__c;
        ID coupon2ID = newRecord.Coupon_II__c;
        System.assertEquals('SS'+YY+'001234AA',newRecord.Coupon_I__r.Bar_Code__c);
        System.assertEquals('SS'+YY+'001234BB',newRecord.Coupon_II__r.Bar_Code__c);
        
        //Test case 5: Cancel Fulfillment
        newRecord.Cancelled__c = true;
        Test.startTest();
        update newRecord;
        Test.stopTest();
        Coupon__c coupon1 = [SELECT ID,Status__c,Cancel_Reason__c FROM Coupon__c WHERE ID =:coupon1ID];
        System.assertEquals('Cancelled',coupon1.Status__c);
        System.assertEquals(CommonHelper.COUPON_CANCELLED,coupon1.Cancel_Reason__c);
        
    }
    
     public static testmethod void testCouponToNonCouponToCoupon()
    {
        Contact contact = setup(); 
        //Test case 1: New Fulfillment Record
        Fulfillment__c newRecord = new Fulfillment__c();
        newRecord.Contact__c = contact.ID;
        newRecord.Type__c = 'Coupon'; 
        newRecord.Material_Name__c = 'TEST'; 
        newRecord.Reason_for_fulfillment__c = 'Reason';
        insert newRecord;
	
        newRecord = [SELECT ID,Coupon_I__c,Coupon_I__r.Bar_Code__c,Coupon_II__c,Coupon_II__r.Bar_Code__c 
                     FROM Fulfillment__c WHERE ID =:newRecord.ID];
        String curYear = String.valueOf(Date.today().year());
        String YY = curYear.substring(2);
        ID coupon1ID = newRecord.Coupon_I__c;
        ID coupon2ID = newRecord.Coupon_II__c;
        System.assertEquals('SS'+YY+'001234AA',newRecord.Coupon_I__r.Bar_Code__c);
        System.assertEquals('SS'+YY+'001234BB',newRecord.Coupon_II__r.Bar_Code__c);
        
        //Test case 2: Update to Non Coupon
        newRecord.Type__c = 'DM1'; 
        newRecord.Material_Name__c = 'NON-TEST'; 
        Test.startTest();
        update newRecord;
        Test.stopTest();
        newRecord = [SELECT ID,Coupon_I__c,Coupon_II__c FROM Fulfillment__c 
                     WHERE ID =:newRecord.ID];
        Coupon__c coupon1 = [SELECT ID,Status__c FROM Coupon__c WHERE ID =:coupon1ID];
        System.assertEquals(NULL,newRecord.Coupon_I__c);
        System.assertEquals(NULL,newRecord.Coupon_II__c);
        System.assertEquals('Cancelled',coupon1.Status__c);
        
        //Test case 3: Update back to Coupon Type
        newRecord.Type__c = 'Coupon'; 
        newRecord.Material_Name__c = 'TEST'; 
        update newRecord;
        newRecord = [SELECT ID,Coupon_I__c,Coupon_I__r.Bar_Code__c,Coupon_II__c,Coupon_II__r.Bar_Code__c 
                     FROM Fulfillment__c WHERE ID =:newRecord.ID];
        coupon1ID = newRecord.Coupon_I__c;
        coupon2ID = newRecord.Coupon_II__c;
        System.assertEquals('SS'+YY+'001235AA',newRecord.Coupon_I__r.Bar_Code__c);
        System.assertEquals('SS'+YY+'001235BB',newRecord.Coupon_II__r.Bar_Code__c);
    }
    
    public static testmethod void testCouponMaterialNameChange()
    {
        Contact contact = setup();  
        //Test case 1: New Fulfillment Record
        Fulfillment__c newRecord = new Fulfillment__c();
        newRecord.Contact__c = contact.ID;
        newRecord.Type__c = 'Coupon'; 
        newRecord.Material_Name__c = 'TEST'; 
        newRecord.Reason_for_fulfillment__c = 'Reason';
        insert newRecord;
	
        newRecord = [SELECT ID,Coupon_I__c,Coupon_I__r.Bar_Code__c,Coupon_II__c,Coupon_II__r.Bar_Code__c 
                     FROM Fulfillment__c WHERE ID =:newRecord.ID];
        String curYear = String.valueOf(Date.today().year());
        String YY = curYear.substring(2);
        ID coupon1ID = newRecord.Coupon_I__c;
        ID coupon2ID = newRecord.Coupon_II__c;
        System.assertEquals('SS'+YY+'001234AA',newRecord.Coupon_I__r.Bar_Code__c);
        System.assertEquals('SS'+YY+'001234BB',newRecord.Coupon_II__r.Bar_Code__c);
        
        //Test case 4: Update Coupon Type - Material_Name_C
        newRecord.Material_Name__c = 'TEST2'; 
       Test.startTest();
        update newRecord;
        Test.stopTest();
        //Assert Fulfilment with changed Coupons
        newRecord = [SELECT ID,Coupon_I__c,Coupon_I__r.Bar_Code__c,Coupon_II__c 
                     FROM Fulfillment__c WHERE ID =:newRecord.ID];
		Coupon__c coupon1 = [SELECT ID,Status__c,Cancel_Reason__c FROM Coupon__c WHERE ID =:coupon1ID];
        Coupon__c coupon2 = [SELECT ID,Status__c,Cancel_Reason__c FROM Coupon__c WHERE ID =:coupon2ID];
        System.assertEquals('KK'+YY+'001888AA',newRecord.Coupon_I__r.Bar_Code__c);
        System.assertEquals(NULL,newRecord.Coupon_II__c);
        //Assert Previous Coupons cancelled 
        System.assertEquals('Cancelled',coupon1.Status__c);
        System.assertEquals(CommonHelper.COUPON_TYPE_CHANGED,coupon1.Cancel_Reason__c);
        System.assertEquals('Cancelled',coupon2.Status__c);
        System.assertEquals(CommonHelper.COUPON_TYPE_CHANGED,coupon1.Cancel_Reason__c);
    }
    
    private static Contact setup(){
    	//Create custom settings
        Member_ID_Setting__c s = new Member_ID_Setting__c();
        s.Name = String.valueOf(Date.today().year());
        s.Next_ID__c = 1888;
        insert s;
        
        //Create test account
        Account account = New Account();
        account.Name = 'Last';
        account.Last_Name__c = 'Last';
        insert account;
        
        //Creat test baby contact
        Contact contact = New Contact();
        contact.FirstName='Baby';
        contact.LastName='Last';
        contact.AccountId=account.ID;
        insert contact;
        
        //Creat test custom setting 1
		Coupon_ID_Setting__c couponSetting = new Coupon_ID_Setting__c();
        couponSetting.Name = 'TEST';
        couponSetting.Next_ID__c = 1234;
        couponSetting.Prefix__c = 'SS';
        couponSetting.Postfix__c = 'AA;BB';
        insert couponSetting;
        
        //Creat test custom setting 2
		Coupon_ID_Setting__c couponSetting2 = new Coupon_ID_Setting__c();
        couponSetting2.Name = 'TEST2';
        couponSetting2.Next_ID__c = 1888;
        couponSetting2.Prefix__c = 'KK';
        couponSetting2.Postfix__c = 'AA';
        insert couponSetting2;
        
        return contact;
    }
}