/*
	Before Update:
    Make sure cannot unset the only primary.
    
    After Insert/Update:
    After set a contact as primary, 
    	1) if there’re already another contact as primary, un-set the previous one
    	2) update linked household
    
    Before Insert:
    none
    
    
	Apply to [HK/CN/ALL]: ALL 
	Author: shiqi.ng@sg.fujitsu.com
	Last Modified: 2014-04-29

*/
public with sharing class ContactTriggerHandler {
	private boolean m_isExecuting = false;
	private integer BatchSize = 0;
	
    
	public ContactTriggerHandler(boolean isExecuting, integer size){
		m_isExecuting = isExecuting;
		BatchSize = size;
	}
	
	public void OnBeforeInsert(Contact[] newContacts){
	}
	
	public void OnAfterInsert(Contact[] newContacts){
		//
		resetPrimaryContact(newContacts);
		updateLinkedHousehold(newContacts);
		
	}
	
	public void OnBeforeUpdate(Contact[] oldContacts, Map<ID, Contact> oldContactMap, Contact[] newContacts, Map<ID, Contact> contactMap){
		preventUnsetPrimaryContact(oldContactMap, newContacts);
	}
	
	public void OnAfterUpdate(Contact[] oldContacts, Contact[] newContacts, Map<ID, Contact> oldContactMap){
		resetPrimaryContact(newContacts);
		updateLinkedHousehold(newContacts);
	}


	private void resetPrimaryContact(Contact[] newContacts){
		
		Map<Id,List<Contact>> priContactByAccId 
			= getExistingPrimaryContactsByAccount(newContacts);
		List<Contact> contactsToUpdate = new List<Contact>();
		for (Contact con : newContacts) 
    	{
    		if (con.Is_Primary_Contact__c == true)
    		{
    			List<Contact> priConList = priContactByAccId.get(con.AccountId);
    			if (priConList != null){ 
    				for (Contact priCon : priConList){
    					if (priCon.Id != con.Id){
    						priCon.Is_Primary_Contact__c = false;
    						contactsToUpdate.add(priCon);
    					}
    				}
    			}
    		}
    	}
    	if (contactsToUpdate.isEmpty()==false)
    	update contactsToUpdate;
		
		
	}
	private void preventUnsetPrimaryContact(Map<Id,Contact> oldContactsMap, Contact[] newContacts ){
		Map<Id,List<Contact>> priContactByAccId
    		 = getExistingPrimaryContactsByAccount(newContacts); 
        
        //Allow true to false only if there's other Primary Contact
        for (Contact newContact : newContacts) 
        {
            Contact oldContact = oldContactsMap.get(newContact.id);
            if (oldContact.Is_Primary_Contact__c == true && newContact.Is_Primary_Contact__c == false)
            {
                List<Contact> priConList = priContactByAccId.get(oldContact.AccountId);
                
    			if (priConList != null  && priConList.size() == 1 && priConList[0].id == oldContact.Id){
                    newContact.addError('Cannot unset Primary Contact.');
                }
            }
        }
	}
	
	//get existing List of Primary Contacts (values) of each Account ID (keys)
    private Map<Id,List<Contact>> getExistingPrimaryContactsByAccount(Contact[] contacts){
    	Set<Id> accountIds = new Set<Id>();
        for (Contact con : contacts) 
        {
            accountIds.add(con.AccountId);
        }
        
        Map<Id,List<Contact>> priContactByAccId = new Map<Id,List<Contact>>(); 
        
        for (Contact con  : 
            [SELECT AccountId, Id, Is_Primary_Contact__c
            FROM Contact 
            WHERE Contact.Is_Primary_Contact__c = true
            AND Contact.AccountId IN :accountIds]
            )
        {
        	List<Contact> conList = priContactByAccId.get(con.AccountId);
        	if (conList == null){
        		conList = new List<Contact>();
        	}
        	conList.add(con);
            priContactByAccId.put(con.AccountId, conList);
        }
        return priContactByAccId;
    }
    
      private void updateLinkedHousehold(List<Contact> contacts){
        List<Account> accounts = new List<Account>();
        
        try{
            for (Contact con : contacts){
            	if (con.Is_Primary_Contact__c == false)
            		continue;
                Account account = [SELECT ID FROM Account WHERE ID = :con.AccountId];

                account.Last_Name__c = con.LastName;
                account.First_Name__c = con.FirstName;
                account.Chinese_Name__c = con.Chinese_Name__c;
                account.Other_Name__c = con.Other_Name__c;
                account.Spouse_Name__c = con.Spouse_Name__c;
                account.Occupation__c = con.Occupation__c;
                account.Birthdate__c = con.Birthdate;
                account.Salutation__c = con.Salutation;
                
	            account.Salutation__c =  con.Salutation;
	            //account.Account_Email__c = con.Email; removed by SQ 12 Feb 2014. 
				account.Preferred_Written_Languge__c = con.Preferred_Written_Languge__c;
				account.Preferred_Spoken_Language__c = con.Preferred_Spoken_Language__c;
				account.China_Mom__c = con.China_Mom__c;
				String fName = (con.FirstName==null) ? '' : con.FirstName;
				//if (con.Name == (con.LastName + ' ' + fName).trim()){
				account.Name = con.LastName + ' ' + fName;
				//}else{
				//	account.Name = fName + ' ' +  con.LastName;
				//}
			
                accounts.add(account);
            }
    
           update accounts;
        }
        catch (Exception e){
		     for (Contact con : contacts) {
				con.addError('There was a problem updating primary contacts on Household:'+e.getMessage());
		     }
        }
	}
    
    

}