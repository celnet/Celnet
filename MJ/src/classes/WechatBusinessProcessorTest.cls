/**
*test all business processor class
*/
@isTest
private class WechatBusinessProcessorTest 
{
    static User HKUser = TestUtility.new_HK_User();
    static testMethod void WechatImageProcessor() 
    {
        system.runas(HKUser)
        {
            TestUtility.setupMemberIDCustomSettings();
            Lead le =new Lead();
            le.LastName = 'aaa';
            le.MobilePhone = '111111';
            le.Attachment_Count__c = 0;
            le.Baby_Count__c = '1';
            insert le;
            Wechat_User__c wu = new Wechat_User__c();
            wu.Name = 'testr';
            wu.Open_Id__c = '123456';
            wu.Binding_Non_Member__c = le.Id;
            insert wu;
            Wechat_Callout_Task_Queue__c wct = new Wechat_Callout_Task_Queue__c();
            wct.Public_Account_Name__c =  'CideaatechSF';
            wct.Type__c = WechatEntity.MESSAGE_TYPE_IMAGE;
            wct.Msg_Body__c = '';
            wct.Processor_Name__c = 'WechatCalloutGetImageProcessor';
            wct.Open_ID__c = '123456';
            wct.Name = 'WechatCalloutGetImageProcessor';
            wct.Status__c = 'waiting';
            insert wct;
            GenerateWechatSetting();
            WechatPlatformRequestMock fakeResponse = new WechatPlatformRequestMock();
            system.Test.setMock(HttpCalloutMock.class, fakeResponse);
            system.test.starttest();
            WechatCalloutProcessorBatch.preRunJob();
            system.test.stoptest();
        }
    }
    
    static testMethod void WechatVoiceProcessor()
    {
        system.runas(HKUser)
        {
            TestUtility.setupMemberIDCustomSettings();
            Lead le =new Lead();
            le.LastName = 'aaa';
            le.MobilePhone = '111111';
            le.Attachment_Count__c = 0;
            le.Baby_Count__c = '1';
            insert le;
            
            Wechat_User__c wu = new Wechat_User__c();
            wu.Name = 'testr';
            wu.Open_Id__c = '123456';
            wu.Binding_Non_Member__c = le.Id;
            insert wu;
            
            Wechat_Message__c wm = new Wechat_Message__c();
            wm.Media_ID__c = 'asdfasdfadfadsf';
            wm.Related_User__c = wu.id;
            insert wm;
            GenerateWechatSetting();
            Wechat_Callout_Task_Queue__c wct = new Wechat_Callout_Task_Queue__c();
            wct.Public_Account_Name__c =  'CideaatechSF';
            wct.Type__c = WechatEntity.MESSAGE_TYPE_VOICE;
            wct.Media_ID__c = '101010';
            wct.Msg_Body__c = '';
            wct.Processor_Name__c = 'WechatCalloutGetVoiceProcessor';
            wct.Open_ID__c = '123456';
            wct.Name = 'WechatCalloutGetVoiceProcessor';
            wct.Status__c = 'waiting';
            insert wct;
            
            WechatPlatformRequestMock fakeResponse = new WechatPlatformRequestMock();
            WechatCalloutGetVoiceProcessor wcp = new WechatCalloutGetVoiceProcessor();
            system.Test.setMock(HttpCalloutMock.class, fakeResponse);
            system.test.starttest();
            wcp.DoCallout(wct);
            system.test.stoptest();
            wcp.FinishData();
        }
    }
    
    static testMethod void WechatUserProcessor()
    {
        system.runas(HKUser)
        {
            TestUtility.setupMemberIDCustomSettings();
            Wechat_User__c wu = new Wechat_User__c();
            wu.Name = 'testr';
            wu.Open_Id__c = '123456';
            insert wu;
            
            GenerateWechatSetting();
            
            Wechat_Callout_Task_Queue__c wct = new Wechat_Callout_Task_Queue__c();
            wct.Public_Account_Name__c =  'CideaatechSF';
            wct.Type__c = WechatEntity.MESSAGE_TYPE_VOICE;
            wct.Media_ID__c = '101010';
            wct.Msg_Body__c = '';
            wct.Processor_Name__c = 'WechatCalloutGetUserProcessor';
            wct.Open_ID__c = '123456';
            wct.Name = 'WechatCalloutGetUserProcessor';
            wct.Status__c = 'waiting';
            insert wct;
            
            WechatPlatformRequestMock fakeResponse = new WechatPlatformRequestMock();
            WechatCalloutGetUserProcessor wcp = new WechatCalloutGetUserProcessor();
            system.Test.setMock(HttpCalloutMock.class, fakeResponse);
            system.test.starttest();
            wcp.DoCallout(wct);
            system.test.stoptest();
            wcp.FinishData();
        }
    }
    
    public static void GenerateWechatSetting()
    {
        system.runas(HKUser)
        {
            TestUtility.setupMemberIDCustomSettings();
            Wechat_Setting__c ws = new Wechat_Setting__c();
            ws.Name = 'CideaatechSF';
            ws.File_Api_Url__c = 'http://file.weixin.qq.com';
            ws.App_Id__c = 'wx76c641c11a6462e5';
            ws.App_Secret__c = '4ce571024be7e0093afbc2f536a6bda9';
            ws.Token__c = 'Token';
            ws.API_Base_Url__c = 'https://api.weixin.qq.com k0MF5eSNtr1z5vCU7S1NKCQj_z1LWhyMxGQQf6UpHN3tlbD71Yf04XK15ziBfSH9NP-';
            ws.Access_Token1__c = 'k0MF5eSNtr1z5vCU7S1NKCQj_z1LWhyMxGQQf6UpHN3tlbD71Yf04XK15ziBfSH9NP-__XS6R0U196t21JMLjA';
            insert ws;
        }
    }
    
	public class WechatSendVoiceRequestMock implements HttpCalloutMock
	{
		protected Integer code;
	    protected String status;
	    protected Map<String, String> responseHeaders;
	    protected final string SendVoiceResponse = '{"WechatUserID":"asdf","SFVoiceID":"asdf","PublicAccountName":"asdf","Status":"Success","ReturnLink":""}';
	    
	    public WechatSendVoiceRequestMock()
	    {
	        this.code =200;
	        this.status = 'OK';
	        this.responseHeaders = new Map<string,string>();
	    }
	    public HTTPResponse respond(HTTPRequest req)
	    {
	    	HttpResponse resp = new HttpResponse();
	        resp.setStatusCode(code);
	        resp.setStatus(status);
	    	resp.setbody(SendVoiceResponse);
	    	return resp;
	    }
	}
    
    static testmethod void Test_WechatCalloutSendVoiceProcessor()
    {
    	Wechat_User__c wu = new Wechat_User__c();
        wu.Name = 'testr';
        wu.Open_Id__c = '123456';
        insert wu;
    	Wechat_Message__c msg = new Wechat_Message__c();
    	msg.Related_User__c = wu.id;
    	insert msg;
    	Wechat_Callout_Task_Queue__c wct = new Wechat_Callout_Task_Queue__c();
        wct.Public_Account_Name__c =  'CideaatechSF';
        wct.Type__c = '';
        wct.Media_ID__c = msg.id;
        wct.Msg_Body__c = '';
        wct.Processor_Name__c = 'WechatCalloutSendVoiceProcessor';
        wct.Open_ID__c = '123456';
        wct.Name = 'WechatCalloutSendVoiceProcessor';
        wct.Status__c = 'waiting';
        insert wct;
        WechatCalloutSendVoiceProcessor.APlusResponse aplus = new WechatCalloutSendVoiceProcessor.APlusResponse();
        WechatSendVoiceRequestMock fakeResponse = new WechatSendVoiceRequestMock();
        WechatCalloutSendVoiceProcessor wcp = new WechatCalloutSendVoiceProcessor();
        system.Test.setMock(HttpCalloutMock.class, fakeResponse);
        system.test.starttest();
        wcp.DoCallout(wct);
        system.test.stoptest();
        wcp.FinishData();
    }
}