public class DNCRHelper
{
    public static ID startDNCRNewCheck()
    {
        DNCRProcessNewBatch newBatch = new DNCRProcessNewBatch();
        //set batch size to max 2000
		return Database.executeBatch(newBatch,2000);
    }
    
    public static ID startDNCRDelCheck()
    {
        DNCRProcessDelBatch removeBatch = new DNCRProcessDelBatch();
        //set batch size to max 2000
		return Database.executeBatch(removeBatch,2000);
    }
    
    public static ID startClearData()
    {
        DNCRClearDataBatch cleanBatch = new DNCRClearDataBatch();
        //set batch size to max 2000
		return Database.executeBatch(cleanBatch,2000);
    }
        
    public static Boolean isJobAlreadyInProgress()
    {
        Integer count = [SELECT COUNT() FROM DNCR_Job_Result__c WHERE Status__c = 'In Progress'];
        if(count > 0)
            return true;
        else
            return false;
    }
    
    public static void updateNewDNCRBatchStatus(ID jobID, AsyncApexJob jobResult, Integer affectedRecordCount)
    {
        List<DNCR_Job_Result__c> jobList = 
            [SELECT ID,Remark__c FROM DNCR_Job_Result__c WHERE New_DNCR_Batch_Job_ID__c = :jobID];
        
        if(jobList!=NULL && !jobList.isEmpty() && jobResult!= NULL)
        {
            DNCR_Job_Result__c dncrJob =jobList.get(0);
            
            dncrJob.New_DNCR_Processed__c=TRUE;
            dncrJob.New_DNCR_Batch_Job_Status__c=jobResult.Status;
            dncrJob.New_DNCR_Batch_Job_Total_Items__c=jobResult.TotalJobItems;
            dncrJob.New_DNCR_Batch_Job_Failed_Items__c=jobResult.NumberOfErrors;
            dncrJob.New_DNCR_Affected_Records__c=affectedRecordCount;
            
            //check job status
            if(jobResult.TotalJobItems == jobResult.NumberOfErrors)
            {
                dncrJob.Status__c='Failed';
                if(dncrJob.Remark__c != NULL)
                    dncrJob.Remark__c +='DNCRProcessNewBatch '+jobID+ ' Failed\n';
                else
                    dncrJob.Remark__c ='DNCRProcessNewBatch '+jobID+ ' Failed\n';
            }            

            //Continue executing the DNCR Delete Job to Set Member_DNCR from True to False
            dncrJob.Del_DNCR_Batch_Job_ID__c = DNCRHelper.startDNCRDelCheck();
              
            update dncrJob;
        }
    }
    
    public static void updateDelDNCRBatchStatus(ID jobID, AsyncApexJob jobResult, Integer affectedRecordCount)
    {
        List<DNCR_Job_Result__c> jobList = 
            [SELECT ID,Remark__c FROM DNCR_Job_Result__c WHERE Del_DNCR_Batch_Job_ID__c = :jobID];
        
        if(jobList!=NULL && !jobList.isEmpty() && jobResult!=NULL)
        {
            DNCR_Job_Result__c dncrJob =jobList.get(0);
                
            dncrJob.Deleted_DNCR_Processed__c=TRUE;
            dncrJob.Del_DNCR_Batch_Job_Status__c=jobResult.Status;
            dncrJob.Del_DNCR_Batch_Job_Total_Items__c=jobResult.TotalJobItems;
            dncrJob.Del_DNCR_Batch_Job_Failed_Items__c=jobResult.NumberOfErrors;
            dncrJob.Del_DNCR_Affected_Records__c=affectedRecordCount;
            
            //check job status
            if(jobResult.TotalJobItems == jobResult.NumberOfErrors)
            {
                dncrJob.Status__c='Failed';
                if(dncrJob.Remark__c != NULL)
                    dncrJob.Remark__c +='DNCRProcessDelBatch '+jobID+ ' Failed\n';
                else
                    dncrJob.Remark__c ='DNCRProcessDelBatch '+jobID+ ' Failed\n';
            }

            //Continue executing the DNCR Clear Job to delete DNCR Records
            dncrJob.Clear_Data_Batch_Job_ID__c = DNCRHelper.startClearData();
            
        	update dncrJob;
        }
    }
    
    /*
		This menthod will update Clear Data Process Batch Job Status. 
    	Also it will compile the overall job status for DNCR Processing from Data Import to Data Clear.
		Email notifications will be sent.
	*/
    public static void updateClearBatchStatus(ID jobID, AsyncApexJob jobResult)
    {
        List<DNCR_Job_Result__c> jobList = 
        [SELECT ID,Failed_To_Load__c,New_DNCR_Batch_Job_Failed_Items__c,Del_DNCR_Batch_Job_Failed_Items__c,
         		Job_Execution_Date__c,Total_DNCR_Records__c,New_DNCR_Batch_Job_Total_Items__c,
         		Del_DNCR_Batch_Job_Total_Items__c,New_DNCR_Affected_Records__c,Del_DNCR_Affected_Records__c,
         		Job_Start_Time__c,Total_Processing_Time__c,Remark__c,Status__c,CreatedBy.Email
         FROM DNCR_Job_Result__c 
         WHERE Clear_Data_Batch_Job_ID__c = :jobID];
        
        if(jobList!=NULL && !jobList.isEmpty() && jobResult!=NULL)
        {
            DNCR_Job_Result__c dncrJob =jobList.get(0);
            
            dncrJob.Data_Cleared__c=TRUE;
            dncrJob.Clear_Data_Batch_Job_Status__c=jobResult.Status;
            dncrJob.Clear_Data_Batch_Job_Total_Items__c=jobResult.TotalJobItems;
            dncrJob.Clear_Data_Batch_Job_Failed_Items__c=jobResult.NumberOfErrors;
            
            //Compile the overall job status for DNCR Processing
            if(dncrJob.Failed_To_Load__c >0 || dncrJob.New_DNCR_Batch_Job_Failed_Items__c >0 
              || dncrJob.Del_DNCR_Batch_Job_Failed_Items__c >0 || dncrJob.Clear_Data_Batch_Job_Failed_Items__c >0)
            {
                dncrJob.Status__c='Partial Success';
            }
            else
                dncrJob.Status__c='Success';
            
            dncrJob.Job_End_Time__c=DateTime.now();
            dncrJob.Total_Processing_Time__c = 
                DNCRHelper.getTimeDiffInMinutes(dncrJob.Job_Start_Time__c,dncrJob.Job_End_Time__c);
        	update dncrJob;
            
            //Send Email Notification
        	DNCRHelper.sendEmailNotification(dncrJob);
        }
    }
    
    public static void sendEmailNotification(DNCR_Job_Result__c dncrJob)
    {
        dncrJob = DNCRHelper.deNullDNCRJob(dncrJob);

		try
       	{                
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
           // Get email addresses from custom seeting
           List<String> toAddresses = DNCRHelper.getEmailAddrFromCustomSetting();

           if(toAddresses == NULL || toAddresses.size()<1)
            	toAddresses = new String[] {dncrJob.CreatedBy.Email};
    
           mail.setToAddresses(toAddresses);

           mail.setSubject('[DNCR Batch] Date:' + dncrJob.Job_Execution_Date__c.format() +' | Status: ' +dncrJob.Status__c);
           
           String htmlBoby = '<HTML><body>';
           htmlBoby += '<b>DNCR Job Status:</b> '+ dncrJob.Status__c + '<br/><br/>';
           htmlBoby += '<b>Total Number of DNCR Records:</b> '+ dncrJob.Total_DNCR_Records__c + '<br/>';
           htmlBoby += '<b>Number of DNCR Records Failed to Loaded:</b> '+ dncrJob.Failed_To_Load__c + '<br/><br/>';
		   
		   htmlBoby += '<b>Number of Member DNCR Set from False to True:</b> '+ dncrJob.New_DNCR_Affected_Records__c + '<br/>';
           htmlBoby += '<b>Number of Member DNCR Set from True to False:</b> '+ dncrJob.Del_DNCR_Affected_Records__c + '<br/><br/>';
           htmlBoby += '<b>Total Processing Time:</b> '+ dncrJob.Total_Processing_Time__c + ' Minutes <br/><br/>';
		   htmlBoby += '<b>Remark:</b> '+ dncrJob.Remark__c + '<br/><br/>';
		   /*
			   htmlBoby += '<b>Total Number of New Member DNCR Apex Job:</b> '+ dncrJob.New_DNCR_Batch_Job_Total_Items__c + '<br/>';
			   htmlBoby += '<b>Number of New Member DNCR Apex Job Failed:</b> '+ dncrJobNew_DNCR_Batch_Job_Failed_Items__cStatus__c + '<br/><br/>';
			   htmlBoby += '<b>Total Number of Deleted Member DNCR Apex Job:</b> '+ dncrJob.Del_DNCR_Batch_Job_Total_Items__c + '<br/>';
			   htmlBoby += '<b>Number of Deleted Member DNCR Apex Job Failed:</b> '+ dncrJob.Del_DNCR_Batch_Job_Failed_Items__c + '<br/>';
			   htmlBoby += '<b>Total Number of Clear Data Apex Job:</b> '+ dncrJob.Clear_Data_Batch_Job_Total_Items__c + '<br/>';
			   htmlBoby += '<b>Number of Clear Data Apex Job Failed:</b> '+ dncrJob.Clear_Data_Batch_Job_Failed_Items__c + '<br/>';
		   */
           htmlBoby += '</body></html>';
           mail.setHtmlBody(htmlBoby);
           
           Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        catch(System.EmailException ex)
        {
          system.debug('Sending Email Failed :' + ex.getMessage());
        }
    }
    
    public static Decimal getTimeDiffInMinutes(DateTime startTime, DateTime endTime)
    {
        Decimal result = -1;
        if(startTime != NULL && endTime != NULL)
        {
            result = Decimal.valueOf(endTime.getTime()-startTime.getTime())/(1000*60);
        }
        result = result.setScale(2);
        return result;
    }
    
    public static DNCR_Job_Result__c deNullDNCRJob(DNCR_Job_Result__c dncrJob)
    {
        if(dncrJob != NULL)
        {
            if(dncrJob.Remark__c == NULL)
                dncrJob.Remark__c = '';
        }
        return dncrJob;
    }
    
    public static Integer getOffSetHoursFromCustomSetting()
    {
        //Set default value to 1 hour
        Integer offsetHours = 1;
        Key_Value_Mapping__c setting = Key_Value_Mapping__c.getValues(CommonHelper.KEY_DNCR_OFFSET_HOURS);
        if(setting != NULL)
        {
            offsetHours = Integer.valueOf(setting.Value__c);
        }
        return offsetHours;
    }
    
    public static List<String> getEmailAddrFromCustomSetting()
    {
        List<String> emailList = new List <String>();
        Key_Value_Mapping__c setting = Key_Value_Mapping__c.getValues(CommonHelper.KEY_DNCR_EMAIL);
        if(setting != NULL)
        {
        	String value = setting.Value__c;
            if(value != NULL)
            {
                List<String> splitStrs = value.split(CommonHelper.EMAIL_DELIMITER);
                for(String email : splitStrs)
                {
                    if(email != NULL && email.length()>0)
                        emailList.add(email);
                }
            }    
        }
        return emailList;
    }
}