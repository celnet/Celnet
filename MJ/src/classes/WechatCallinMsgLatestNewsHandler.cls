/*Author:leo.bi@celent.com.cn
 *Function:Receive Click and response with a document
 *用户在微信客户端点击最新资讯时由此Handler来处理
 *Date:2014-09-16
 */
public class WechatCallinMsgLatestNewsHandler extends WechatCallinMsgHandler
{
    public override void Handle(WechatCallinMsgPipelineContext Context)
    {
        WechatEntity.InEventMsg inEvent = (WechatEntity.InEventMsg)Context.InMsg;
        if(inEvent.EventKey != this.BindingInfo.Event_Key__c)
        {
            return;
        }
        List<Wechat_Content__c> contentList = [select Article_Url__c , Description__c , Key_Word__c , Picture_Url__c, Title__c 
                                               from Wechat_Content__c 
                                               where Key_Word__c = 'News' order by createddate desc];
        WechatEntity.OutRichMediaMsg outRichMedia = new WechatEntity.OutRichMediaMsg();
        List<WechatEntity.Article> articleList = new List<WechatEntity.Article>();
        WechatEntity.GetBaseMsgAtrributes(inEvent,outRichMedia);
        outRichMedia.MsgType = WechatEntity.MESSAGE_TYPE_NEWS;
        outRichMedia.Articles = articleList;    
        for(Wechat_Content__c wc :contentList)
        {
            WechatEntity.Article art = new WechatEntity.Article();
            art.title = wc.Title__c;
            art.description = wc.Description__c;
            art.url = wc.Article_Url__c;
            art.picURL = (null != wc.Picture_Url__c ? wc.Picture_Url__c : '');
            outRichMedia.Articles.add(art);
        }
        Context.OutMsg = outRichMedia;
    }
}