public with sharing class ObjectUtil {

	private static Map<Schema.SObjectType, Map<String,RecordType>> recordTypesCache = new Map<Schema.SObjectType, Map<String,RecordType>>();
	
	public static ID GetRecordTypeID(SObjectType token, String recordTypeName){
		Map<String,RecordType> rtMap = GetRecordTypesByDevName(token);
		
		if(!rtMap.containsKey(recordTypeName)) 
			throw new RecordTypeException('Record type "'+ recordTypeName +'" does not exist.'); 
		
		return rtMap.get(recordTypeName).Id; 
	}
	
	public static Map<String,RecordType> GetRecordTypesByDevName(Schema.SObjectType token){
		Map<String,RecordType> recordTypes = recordTypesCache.get(token);
		
		if (recordTypes == null){
			recordTypes = new Map<String,RecordType>();
			recordTypesCache.put(token,recordTypes);
			Schema.DescribeSObjectResult obj = token.getDescribe();
			String objName = obj.getName();
			
			for (RecordType rt : 
				[Select Id, Name, DeveloperName FROM RecordType WHERE SObjectType = :objName])
			{
				recordTypes.put(rt.DeveloperName,rt);
			}
		}
		return recordTypes;
	}
  
      
      public class RecordTypeException extends Exception{} 
}