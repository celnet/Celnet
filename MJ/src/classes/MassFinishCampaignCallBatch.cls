/*Author:leo.bi@celnet.com.cn
 *CreateDate:2014-05-28
 *function:1.set all campaignmember's call status to Finish Call.This part of logic may lead to a qtask downgraded.
 *		   2.If a invitation is finished,check the remaining call,if all of the remaining call are 
 *		   3.the same (invitation,routine,or special invitation),downgrade it according to the rest calls' type.
 *		   4.When downgraded(this qtask is surely a elite),if the rest is routine,clear related campaign,campaign member,
 *		   If the rest are invitation(Special or non Special),clear related questionnaire.Don't assign the bussinesstype
 *         (elite type qtask's bussiness type is null.After downgraded, the bussiness type will be assigned in the QTaskUpdtadeHanler)
 */ 
global class MassFinishCampaignCallBatch implements DataBase.Batchable<SObject>
{
	private final string query;
	private final DateTime jobStartTime = DateTime.now();
	private final Id campaignId;
	private List<Q_Task__c> List_UpdateQTask;
	public MassFinishCampaignCallBatch(Id campaignId)
	{
		this.campaignId = campaignId;
		query = 'Select Id, CampaignId, Call_Status__c, ContactId From CampaignMember where CampaignId=\''+ this.campaignId + '\' and Call_Status__c != \'Finish Call\'';  
	}
	global DataBase.Querylocator start(DataBase.BatchableContext BC)
	{
		return DataBase.getQueryLocator(query);
	}
	global void execute(DataBase.BatchableContext BC,List<SObject> scope)
	{
		List<CampaignMember> list_CampaignMember = new List<CampaignMember>();
		Set<Id> set_contactId = new Set<Id>();
		for(SObject s : scope)
		{
			CampaignMember cm = (CampaignMember)s;
			cm.Call_Status__c = 'Finish Call';
			set_contactId.add(cm.ContactId);
			list_CampaignMember.add(cm);
		}
		update list_CampaignMember;
		
		/*
		 *Scott 
		*/
		if(set_contactId.size() == 0)
		{
			return;
		}
		List_UpdateQTask = new List<Q_Task__c>();
		//Invitation
		Map<Id,Q_Task__c>  Map_InvitationQtask = new Map<Id,Q_Task__c>();
		for(Q_Task__c qtask : [select Id,Contact__c,Status__c,ID__c,Type__c,Business_Type__c,Contact__r.Questionnaire__c from Q_Task__c where Contact__c in:set_contactId and Status__c != 'Closed'])
		{
			Map_InvitationQtask.put(qtask.Contact__c,qtask);
		}
		
		if(Map_InvitationQtask.size()>0)
		{
			this.InvitationCheck(Map_InvitationQtask);
		}
		
		if(List_UpdateQTask.size()>0)
		update List_UpdateQTask;
	}
	//Invitation
	public void  InvitationCheck(Map<Id,Q_Task__c> Map_InvitationQtask)
	{
		//Invitation
		//只剩下同一类的 Invitation才会降级
		for(Contact con: [Select Id,Questionnaire__c,Questionnaire__r.Type__c,Questionnaire__r.Business_Type__c,
						 (Select Contact__c,Questionnaire__c,Status__c From Answers__r where Status__c =:'Finish'),
						  (Select Call_Status__c,Campaign.RecordType.DeveloperName 
						   From CampaignMembers where CampaignId !=: campaignId and Call_Status__c !='Finish Call') 
						 From Contact where id in:Map_InvitationQtask.KeySet()])
		{
			Q_Task__c qtask = Map_InvitationQtask.get(con.Id);
			
			//判断Routine是否完成
			Boolean Routineflag = false;
			if(con.Questionnaire__c==null)
			Routineflag = true;    
			
			if(con.Questionnaire__c != null && con.Answers__r.size()>0)
			{
				for(Answer__c ans: con.Answers__r)
				{
					if(ans.Questionnaire__c == con.Questionnaire__c)
					Routineflag= true;
					break;
				}
			}
			
			//Invitation
			if(con.CampaignMembers.size()>0)
			{
				if(Routineflag)
				{
					Integer InvFlag = 0;
					Integer SpecInvFlag = 0;
					for(CampaignMember cm : con.CampaignMembers)
					{
						if(cm.Campaign.RecordType.DeveloperName =='CN_Invitation_Special')
						{
							SpecInvFlag++;
						}
						else if(cm.Campaign.RecordType.DeveloperName =='CN_Invitation')
						{
							InvFlag++;
						}
					}
					//Special Invitation
					if(SpecInvFlag>0 && InvFlag==0)
					{
						qtask.Is_Demotion__c = true;
						qtask.Type__c = 'Invitation';
						qtask.Business_Type__c = 'Invitation Special';
						List_UpdateQTask.add(qtask);
					}
					//Invitation
					else if(InvFlag>0 && SpecInvFlag==0)
					{
						qtask.Is_Demotion__c = true;
						qtask.Type__c = 'Invitation';
						qtask.Business_Type__c = 'Invitation';
						List_UpdateQTask.add(qtask);
					}
					else
					{
						qtask.Is_Demotion__c = true;
						qtask.Type__c = 'Elite';//Upgrade to Elite
						qtask.Business_Type__c =null;
						List_UpdateQTask.add(qtask);
					}
				}
				else
				{
					qtask.Is_Demotion__c = true;
					qtask.Type__c = 'Elite';//Upgrade to Elite
					qtask.Business_Type__c =null;
					List_UpdateQTask.add(qtask);
				}
			}
			else
			{
				if(Routineflag)
				{
					qtask.Status__c = 'Closed';
					qtask.ID__c = qtask.Contact__c + '-' + qtask.Status__c + String.valueOf(DateTime.now());
					List_UpdateQTask.add(qtask);
				}
				else
				{
					qtask.Is_Demotion__c = true;
					qtask.Type__c = con.Questionnaire__r.Type__c;
					qtask.Business_Type__c = con.Questionnaire__r.Business_Type__c;
					List_UpdateQTask.add(qtask);
				}
			}
		}
	}
	global void finish(DataBase.BatchableContext BC)
	{
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	   	String[] toAddresses = new String[] {UserInfo.getUserEmail()};
	   	//String[] toAddresses = new String[] {'97690215@qq.com'};
	   	mail.setToAddresses(toAddresses);
	   	mail.setSubject('CampaignMembers\' call status have been finished');
	   	mail.setHtmlBody('<br>' 
	   					 + 'Job Id: ' + String.valueOf(BC.getJobId()) + '<br>'
	   					 + 'Start Time: ' + String.valueOf(jobStartTime) + '<br>'
	   					 + 'Campaign Id' + String.valueOf(this.campaignId) + '<br>'
	   					 + 'End Time: ' + String.valueOf(DateTime.now()) + '<br>');
	   	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}	
}