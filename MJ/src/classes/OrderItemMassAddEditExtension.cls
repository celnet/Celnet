public without sharing class OrderItemMassAddEditExtension {
    private Order__c parentOrder;
    private List<Contact> relatedBabies;
    private static List<Product__c> DSOProducts;
    private static Map<ID,Product__c> productMap;
    private static List<SelectOption> productSelectOptions;
    private Integer curIndex;
    private List<Order_Item__c> orderItemsToBeDeleted;
    
    //The following price list are used for displaying on Visusalforce Page only
    private Decimal totalPrice;
    private List<ItemWrapper> itemWrappers;
    private List<ItemWrapper> originalItemWrappers;
    
    public enum State {DRAFT,PROCESSING,CANCELLED,FAILED}
    private State orderState;
    
    static
    {
        if(DSOProducts == NULL)
        {
            DSOProducts = CommonHelper.getDSOProducts();
            productMap = new Map<ID,Product__c>();
            productSelectOptions = new List<SelectOption>();
            //Initialise the Product Drop Down Selection List
            productSelectOptions.add(new SelectOption('','--Please select a Product--'));
            for(Product__c product : DSOProducts)
            {
                //Check NULL to avoid exception caused by dirty Product data
                if(product.Product_SKU__c != NULL)
                {
                	productSelectOptions.add(new SelectOption(product.ID,product.Product_SKU__c));
                	productMap.put(product.ID,product);
                }
            }
        }
    }
    
    //public OrderItemMassAddEditExtension(ApexPages.StandardSetController controller) 
    public OrderItemMassAddEditExtension() 
    {
        String orderID = ApexPages.currentPage().getParameters().get('id');
        List<Order__c> orders = [SELECT ID,HouseHold__c,Name,Product_Ordered__c,Cancelled__c,Order_Failed__c,Push_Date__c 
                                     FROM Order__c WHERE ID=:orderID];

        itemWrappers = new List<ItemWrapper>();
        relatedBabies = new List<Contact> ();
        orderItemsToBeDeleted = new List<Order_Item__c>();
                    
        if(orders == NULL || orders.isEmpty())
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,CommonHelper.ORDER_NOT_FOUND));
        }
        else
        {
            List<Order_Item__c> orderItems;
            orderItems = [SELECT Product__c, Product__r.Product_SKU__c, Quantity__c,Order_Line_Canncelled__c,
                                CreatedBy.Name,CreatedDate,LastModifiedBy.Name,LastModifiedDate
                             FROM Order_Item__c WHERE Order__c = :orderID];
        
            if (orderItems.isEmpty()){
                orderItems = new List<Order_Item__c>();
            }
            
            parentOrder = orders.get(0);
            totalPrice = 0.00;
            relatedBabies = OrderHelper.getRelatedBabies(parentOrder);
            
            readOrderState();
            if (orderState == State.FAILED || orderState == State.CANCELLED )
            	return;
            
            for (Order_Item__c item : orderItems){
                //Check NULL to cater wrong Product setup or deactiveted Product
                if(productMap.get(item.Product__c)!=NULL)
                {
                	Decimal unitPrice = productMap.get(item.Product__c).Unit_Price__c;
                	itemWrappers.add(new ItemWrapper(item,unitPrice));
                }
            }
            updateTotalPrice();
            
            if (orderState == State.DRAFT){
                //If number of existing records < 5, then set to 5
                addNewLines(5 - orderItems.size());
            }
            
            //Clone Wrapper Original
            originalItemWrappers = cloneWrappers(itemWrappers);
        }
    }
    
    private void readOrderState(){
        if (parentOrder.Order_Failed__c){
            orderState = State.FAILED;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,CommonHelper.FAILED_ORDER_ITEM_EDIT_NOT_ALLOWED));
        }else if (parentOrder.Cancelled__c){
            orderState = State.CANCELLED;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,CommonHelper.CANCELLED_ORDER_ITEM_EDIT_NOT_ALLOWED));
        }else if (parentOrder.Push_Date__c != NULL){
            orderState = State.PROCESSING;
        }else{
            orderState = State.DRAFT;
        }
    }
    
    public Boolean getIsOrderProcessing(){
        return orderState == State.PROCESSING;
    }
    
    public Boolean getIsOrderDraft(){
        return orderState == State.DRAFT;
    }
    
    public Boolean getIsOrderCancelled(){
        return orderState == State.CANCELLED;
    }
    
    public Boolean getIsOrderFailed(){
        return orderState == State.FAILED;
    }
    
    public List<SelectOption> getDSOProductOptions() {
        return productSelectOptions;
    }
    
    private List<ItemWrapper> cloneWrappers(List<ItemWrapper> wrappers){
        List<ItemWrapper> clonedWrapper = new List<ItemWrapper>();
        for (ItemWrapper wrapper : wrappers){
            clonedWrapper.add(wrapper.deepClone());
        }
        return clonedWrapper;
    }
    
    public List<ItemWrapper> getOrderItemWrappers() {
        return itemWrappers;
    }
    
    public Integer getCurIndex() {
        return curIndex;
    }
    
    public void setCurIndex(Integer value) {
        curIndex = value;
    }
    
    public List<Contact> getRelatedBabies() {
        return relatedBabies;
    }
    
    public Order__c getParentOrder() {
        return parentOrder;
    }
    
    public Decimal getTotalPrice() {
        return totalPrice;
    }
    
    public PageReference cancel() {
        //  Return to the Order Detail page:
        return new PageReference('/'+parentOrder.ID);  
    }
    
    public PageReference reset() {
        itemWrappers = cloneWrappers(originalItemWrappers);
        orderItemsToBeDeleted.clear();
        return null; 
    }
    
    public PageReference addNew() {
        //Add 3 lines in one click
        addNewLines(3);
        return null;  
    }
    
    private void addNewLines(Integer numerOfLines) {
        for(Integer count=0; count<numerOfLines; count++)
        {
            itemWrappers.add(new ItemWrapper(createNewOrderItem()));
        }
    }
    
    private Order_Item__c createNewOrderItem()
    {
        Order_Item__c item = new Order_Item__c();
        item.Order__c = parentOrder.ID;
        return item;
    }
    
    public PageReference deleteLine() {
        Integer index = Integer.valueOf(ApexPages.currentPage().getParameters().get('curIndex'));
        orderItemsToBeDeleted.add(itemWrappers.remove(index).item);
        return null;  
    }
    
    //Valid only for "isOrderDraft==true"
    public PageReference saveItems() {
        List<Order_Item__c> newList = new List<Order_Item__c>();
        
        for (Integer index = 0 ; index<itemWrappers.size(); index++){        
            ItemWrapper wrapper = itemWrappers.get(index);
            wrapper.hasProductError = false;
            Order_Item__c item = wrapper.item;
            if(item.Product__c == NULL && item.Quantity__c == NULL)
            {
                   if (wrapper.isExisting){
                        item.Quantity__c.addError('Product & Quantity is required for existing order item');
                        wrapper.hasProductError = true;
                   }//Else Ignore empty lines - do nothing
            }
            else{
                if(item.Product__c == NULL){
                    wrapper.addProductError(CommonHelper.PRODUCT_REQUIRED);
                }
                else if(item.Quantity__c == NULL)
                   item.Quantity__c.addError(CommonHelper.VALUE_REQUIRED);
                else if (checkRepeatedOrderItem(index)==false){
                    wrapper.addProductError('Cannot place multiple order item of the same product: '+productMap.get(item.Product__c).Product_SKU__c);
                }else{
                    String errorMessage = validateMaxAllowOrderPeriod(item);
                    if (errorMessage != null){
                        wrapper.addProductError(errorMessage);
                    }else{
                        newList.add(item);
                    }  
                }              
            }
        }
        
        //If there is any validation error, return to the form page
        if(ApexPages.hasMessages())
            return null;
        
        try
        {
            if(!newList.isEmpty())
            {
                upsert newList;
                
            }
            if(!orderItemsToBeDeleted.isEmpty())
            {
                delete orderItemsToBeDeleted;
            }
            updateProductOrdered();
        }
        catch(DmlException ex){
            ApexPages.addMessages(ex);
            return null;
        }
        
        //  Return to the Order Detail page:
        return new PageReference('/'+parentOrder.ID);  
    }
    
    //Valid only for "isOrderProcessing==true"
    public PageReference updateProcessedtems() {
        List<Order_Item__c> newList = new List<Order_Item__c>();
        
        for (ItemWrapper wrapper : itemWrappers){
            newList.add(wrapper.item);
        }
        
        if(ApexPages.hasMessages())
            return null;
        
        try
        {
            if(!newList.isEmpty())
            {
                update newList;
                
            }
            updateProductOrdered();
        }
        catch(DmlException ex){
            ApexPages.addMessages(ex);
            return null;
        }
        
        //  Return to the Order Detail page:
        return new PageReference('/'+parentOrder.ID);  
    }
    
    /* Returns error message if validation fails. Else returns null*/
    private String validateMaxAllowOrderPeriod(Order_Item__c item){
        //#1 If Allowed_Order_Period__c ==NULL, bypass checking
        if(item.Product__c!= NULL && productMap.get(item.Product__c) != NULL
           && productMap.get(item.Product__c).Allowed_Order_Period__c !=NULL)
        {
            Integer allowedDays = Integer.valueOf(productMap.get(item.Product__c).Allowed_Order_Period__c);
            Integer maxQuantity = Integer.valueOf(productMap.get(item.Product__c).Max_Order_Quantity__c);
            //Bypass if allowed days = 0 
            if (allowedDays == 0) { return null; }
            Integer numOfBabies = relatedBabies.size();
            Integer maxAllowedQuantity = maxQuantity * numOfBabies;
            Integer offset = getDeletedQuantity(item.Product__c);
            Boolean isAllowed = 
                OrderHelper.isMaxOrderPeriodAllowed(item, parentOrder.HouseHold__c, allowedDays, maxAllowedQuantity,offset);
            
            if(!isAllowed){
               String productName = productMap.get(item.Product__c).Product_SKU__c;
               String message  = '{0} has exceed the maximum order quantity ({1}) of this order period within last {2} day(s), you cannot place this product order now.';
              
               return String.format(message, new String[]{
                        productName, String.valueOf(maxAllowedQuantity), String.valueOf(allowedDays)
                        });
            }
        }
        return null;
    }

    private void updateProductOrdered() 
    {
        String productOrdered = '';
        for (ItemWrapper wrapper : itemWrappers){
            Order_Item__c item = wrapper.item;
            if (item.Product__c != null && productMap.get(item.Product__c)!=NULL && item.Order_Line_Canncelled__c == false){
                String p = productMap.get(item.Product__c).Product_SKU__c;
                productOrdered += p + ';';
            }
        }
        
        parentOrder.Product_Ordered__c = productOrdered;
        
		//Allow Field Truncation
        Database.DMLOptions dml = new Database.DMLOptions();
		dml.allowFieldTruncation = true;
        Database.update(parentOrder,dml);
    }
    
    public PageReference productChange()
    {
        if(curIndex != NULL)
        {
            ItemWrapper wrapper = itemWrappers.get(curIndex);
            wrapper.hasProductError = false;
            Order_Item__c item = wrapper.item;
            if(item.Product__c!=NULL && productMap.get(item.Product__c)!=NULL)
            {
                //Check for repeated order item
                if (checkRepeatedOrderItem(curIndex)==false){
                    wrapper.addProductError('Cannot place multiple order item of the same product: '+productMap.get(item.Product__c).Product_SKU__c);
                }
                //Get Default Quantity & Unit Price from Product Map
                Integer defaultQty = Integer.valueOf(productMap.get(item.Product__c).Default_Order_Quantity__c);
                wrapper.item.Quantity__c = defaultQty;
                wrapper.unitPrice = productMap.get(item.Product__c).Unit_Price__c;
                updateTotalPrice();
                 
            }else{
                if (wrapper.isExisting){
                    wrapper.addProductError('Product is required for Existing Order Item');
                }else{
                    item.Quantity__c = null;
                    wrapper.unitPrice = null;
                    updateTotalPrice();
                }
            }
        }
        return null;
    }
    
    public PageReference quantityChange()
    {
        if(curIndex != NULL)
        {
            ItemWrapper wrapper = itemWrappers.get(curIndex);
            wrapper.hasProductError = false;
            Order_Item__c item = wrapper.item;
            if(item.Product__c==NULL)
                wrapper.addProductError(CommonHelper.PRODUCT_REQUIRED);
            else if(item.Quantity__c!=NULL && productMap.get(item.Product__c)!=NULL)
            {               
                //Get Unit Price from Product Map
                wrapper.unitPrice = productMap.get(item.Product__c).Unit_Price__c;
                updateTotalPrice();
            }
        }
        return null;
    }

    private Boolean checkRepeatedOrderItem(Integer index){
        
        Order_Item__c targetItem = itemWrappers.get(index).item;
        
        for (Integer i = 0 ; i<itemWrappers.size(); i++){
            if (i==index)
                continue;
            Order_Item__c item = itemWrappers.get(i).item;
            if(item.Product__c!=NULL && item.Product__c==targetItem.Product__c)
                return false;
        }
        return true;
    }
    
    private Integer getDeletedQuantity(Id product){
        for (Order_Item__c item : orderItemsToBeDeleted){
            if (item.Product__c == product)
                return Integer.valueOf(item.Quantity__c);
        }
        return 0;
    }
    
    private void updateTotalPrice()
    {
        totalPrice = 0.00;
        for (ItemWrapper wrapper : itemWrappers)
        {
            Order_Item__c item = wrapper.item;
            if(item.Product__c != NULL && item.Quantity__c !=NULL)
                totalPrice +=  wrapper.getSubTotalPrice();
        }
    }
      
    public without sharing class ItemWrapper
    {
        public Order_Item__c item {get;set;}
        public Decimal unitPrice {get;set;}
        
        public ItemWrapper(Order_Item__c item){
            this.item = item;
        }
        
        public ItemWrapper(Order_Item__c item, Decimal value1)
        {
            this.item = item;
            unitPrice=value1;
        }
        
        public ItemWrapper deepClone(){
            Order_Item__c clonedItem = item.clone(true,true,true);
            return new ItemWrapper(clonedItem,unitPrice);
        }
        
        public Decimal getSubTotalPrice(){
            if (item != null && item.Quantity__c != null && unitPrice != null){
                return item.Quantity__c * unitPrice;
            }
            return null;
        }
        
        public Boolean isExisting{ 
            get{
            	return (item!=null && item.ID != null);
       		 }
        }
        
        public Boolean hasProductError{get;set;}
        
        private void addProductError(String message){
            item.Product__c.addError(message);
            hasProductError = true;
        }
    }
}