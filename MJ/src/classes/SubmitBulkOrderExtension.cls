public class SubmitBulkOrderExtension{
    
    private List<Order__c> selectedOrders;
    private List<Order__c> validOrders;
    private List<Order__c> cancelledOrders;
    private List<Order__c> noSFAOrders;
    private List<Order__c> blankOrders;
    private List<Order__c> submittedOrders;
    private Boolean displayDetail;
    
    public SubmitBulkOrderExtension(ApexPages.StandardSetController controller) 
    {
        selectedOrders = controller.getSelected();
        
        List<Order__c> nonCancelList = new List<Order__c>();
        List<Order__c> tempList = new List<Order__c>();
        validOrders = new List<Order__c>();
        blankOrders = new List<Order__c>();
        submittedOrders = new List<Order__c>();
        cancelledOrders = new List<Order__c>();
        noSFAOrders = new List<Order__c>();
        if (SF2SFHelper.checkManageConnectionPermission() == false){
        	displayDetail = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,CommonHelper.NO_PUSH_PERMISSION));
        }
        else if(selectedOrders == NULL || selectedOrders.isEmpty())
        {
            displayDetail = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,CommonHelper.NO_ORDER_SELECTED));
        }
        else
        {
            displayDetail = true;
            //Get order full detail
            selectedOrders = [SELECT ID,Name,First_Name__c,Last_Name__c,HouseHold__c,Cancelled__c,Need_Send_to_SFA__c,Address_Flat_Floor_Block_Building__c,
                                 Address_Street_District_Territory__c,Mobile_Phone__c,Expected_Delivery_Date__c,Delivery_Instruction__c,Delivery_Center__c
                                 FROM Order__c WHERE ID in :selectedOrders];

            //Filter Cancelled Orders and "Need Send to SFA?"
            for(Order__c order : selectedOrders)
            {
                if(order.Cancelled__c)
                    cancelledOrders.add(order);
                else if(!order.Need_Send_to_SFA__c)
                    noSFAOrders.add(order);
                else
                    nonCancelList.add(order);
            }
            
            //Check Order Item(s) exist for Non-cancelled Orders
            if(!nonCancelList.isEmpty())
            {
                AggregateResult[] groupedResults = [SELECT Order__c,Count(ID) itemCount FROM Order_Item__c 
                                                    WHERE Order__c in : nonCancelList GROUP BY Order__c];
                
                Set<String> idSet = new Set<String>();
                for (AggregateResult result : groupedResults)
                    idSet.add(String.valueOf(result.get('Order__c')));
                
                for(Order__c order : nonCancelList)
                {
                    if(idSet.contains(order.ID))
                        tempList.add(order);
                    else
                        blankOrders.add(order);
                }
            }
            
            //Check resubmisstion 
            if(!tempList.isEmpty())
            {
                Set<ID> submittedIDSet = SF2SFHelper.getSubmittedOrders(tempList);
                for(Order__c order : tempList)
                {
                    if(submittedIDSet.contains(order.ID))
                        submittedOrders.add(order);
                    else
                        validOrders.add(order);
                }
            }
        }
    }

    public List<Order__c> getValidOrders()
    {
        return validOrders;
    }
    public Integer getValidOrdersSize()
    {
        return validOrders.size();
    }
    
    public List<Order__c> getBlankOrders()
    {
        return blankOrders;
    }
    public Integer getBlankOrdersSize()
    {
        return blankOrders.size();
    }
    
    public List<Order__c> getSubmittedOrders()
    {
        return submittedOrders;
    }
    public Integer getSubmittedOrdersSize()
    {
        return submittedOrders.size();
    }
    
    public List<Order__c> getCancelledOrders()
    {
        return cancelledOrders;
    }
    public Integer getCancelledOrdersSize()
    {
        return cancelledOrders.size();
    }
    
    public List<Order__c> getNoSFAOrders()
    {
        return noSFAOrders;
    }
    public Integer getNoSFAOrdersSize()
    {
        return noSFAOrders.size();
    }
    
    public Boolean getDisplayDetail()
    {
        return displayDetail;
    }
    
    public Boolean getShowBlankOrders()
    {
        return !blankOrders.isEmpty();
    }
    
    public Boolean getShowCancelledOrders()
    {
        return !cancelledOrders.isEmpty();
    }
    
    public Boolean getShowSubmittedOrders()
    {
        return !submittedOrders.isEmpty();
    }
    
    public Boolean getShowNoSFAOrders()
    {
        return !noSFAOrders.isEmpty();
    }
    
    public PageReference submitOrders() {
        if(validOrders.isEmpty())
        {
            ApexPages.addmessage(new ApexPages.message(
                ApexPages.severity.WARNING,CommonHelper.NO_VALID_ORDER));
            return NULL;
        }
        
		//Send Orders to SFA via SF2SF Connection
        try
        {
        	SF2SFHelper.submitOrdersToSFA(validOrders,false);
        }
        catch (Exception e)
        {
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
           return NULL;
        }
        //  Redirect to success page
        return new PageReference('/apex/SubmitBulkOrdersSuccess');  
    }
}