global class CN_WebService {

    webService static Boolean updateSatisfactionSurveyRating(String UCID, String surveyRating, DateTime callStartTime, DateTime callPickupTime) {
        
        //System.debug('updateSatisfactionSurveyRating');
        //System.debug('UCID = ' + UCID);
        //System.debug('surveyRating = ' + surveyRating);
        
        try {
           updateTaskSatisfactionSurveyRating(UCID, surveyRating, callStartTime, callPickupTime);
        } catch(Exception ex) {
           return false; 
        }
        return true;  
    }
    
    webService static Boolean createVoiceMailMessage(String  UCID, 
                                                     String  voiceMailUrl, 
                                                     String  callerPhoneNumber, 
                                                     String  calledPhoneNumber,
                                                     String  type,
                                                     Integer duration,
                                                     String  incomingCallCity) {
        //System.debug('createVoiceMailMessage');
        //System.debug('UCID = ' + UCID);
        //System.debug('callerPhoneNumber = ' + callerPhoneNumber);
        //System.debug('calledPhoneNumber = ' + calledPhoneNumber);
        
        try {      
           VoiceMailHelper.createVoiceMailRecord(UCID, voiceMailUrl, callerPhoneNumber, calledPhoneNumber, type, duration, incomingCallCity);
        } catch(Exception ex) {
           return false; 
        }
        return true;
    }
    
   
    //@future
    private static void updateTaskSatisfactionSurveyRating(String UCID, String surveyRating, DateTime callStartTime, DateTime callPickupTime) {
     /*   List<Task> tasks = [SELECT Id, Satisfaction_Survey_Rating__c FROM Task WHERE CallObject = :UCID FOR UPDATE];
        
        if (tasks.isEmpty()) return;
        
        try {
          tasks[0].Satisfaction_Survey_Rating__c = surveyRating;
          tasks[0].Call_Start_Time__c            = callStartTime;
          tasks[0].Call_Pickup_Time__c           = callPickupTime;

        } catch(Exception ex) {
         
        }
        update tasks[0];*/
        Itegration_Log__c iteLog = new Itegration_Log__c();
        if(UCID == null || UCID == '')
        {
        	iteLog.Call_Object__c = 'Insert Null CallObject at' + String.valueOf(DateTime.now());
        	iteLog.Status__c = 'Null CallObject';
        	iteLog.Failing_Reason__c = iteLog.Call_Object__c;
        	iteLog.Task_Type__c = 'CIC';
        	insert iteLog;
        	return;
        }
        iteLog.Call_Object__c = UCID;
    	iteLog.Call_Start_Time__c = callStartTime;
    	iteLog.Call_Pickup_Time__c = callPickupTime;
    	iteLog.Satisfaction_Survey_Rating__c = surveyRating;
    	iteLog.Task_Type__c = 'CIC';
    	iteLog.Status__c = 'Not Start';
        try
        {
	        upsert iteLog Call_Object__c;
        } catch(Exception e)
        {
        	Itegration_Log__c errorIteLog = new Itegration_Log__c();
        	errorIteLog.Call_Object__c = 'Upsert Itegration Log Faild at' + String.valueOf(DateTime.now());
        	errorIteLog.Status__c = 'Upsert Faild';
        	errorIteLog.Task_Type__c = 'CIC';
        	errorIteLog.Failing_Reason__c = e.getMessage();
        	insert errorIteLog;
        }
    }
}