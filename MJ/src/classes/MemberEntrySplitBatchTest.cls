/**
Author: scott.zhang@celnet.com.cn
Created On: 2014-04-29
Function: MemberEntrySplitBatch test class
Apply To: CN
 */
@isTest
private class MemberEntrySplitBatchTest 
{

    static testMethod void myUnitTest() 
    {
    	User u = TestUtility.new_CN_User();
    	system.runas(u)   
        {
        	system.debug('-------------------------------' + UserInfo.getName());  
	    	Address_Management__c am = new Address_Management__c();
			am.Region__c = '华北区';                 
	        am.Sub_Region__c = '河北省';  
	        am.Area__c ='西区'; 
	        am.Sales_City__c = '廊坊市'; 
	        am.Name = '廊坊市';        
	        insert am; 
	        /*-DU0:Client Id 查重-*/   
	       //Account CN_HouseHold         
	     	Account DU0_Acc = new Account();
	        DU0_Acc.Name = 'HouseHoldTest';
	        DU0_Acc.RecordTypeId = ObjectUtil.GetRecordTypeID(Account.getSObjectType(), 'CN_HouseHold');
	        DU0_Acc.BF_Client_ID__c = '12345FF';
	        DU0_Acc.Standard_City__c = Am.Id;
	        insert DU0_Acc;
	        //Baby Contact
	        Contact DU0_Con = new Contact();
	        DU0_Con.RecordTypeId = ObjectUtil.GetRecordTypeID(Contact.getSObjectType(), 'CN_Child');
	        DU0_Con.LastName = 'zhang';
	        DU0_Con.AccountId = DU0_Acc.Id;
	        insert DU0_Con;
	        //Member Entry
	        Member_Entry__c DU0_member = new Member_Entry__c();
	    	DU0_member.I_Status__c = 'Extracted New';
	    	DU0_member.BF_Duplicated__c = true;
	    	DU0_member.BF_Client_ID__c = '12345FF';
	    	DU0_member.Baby1_Birthday__c = Date.today().addYears(-1);
	    	DU0_member.Baby2_Birthday__c = Date.today().addYears(-2);
	    	DU0_member.Baby3_Birthday__c = Date.today().addYears(-3);
	    	DU0_member.Administrative_Area__c = Am.Id;
	    	insert DU0_member;
	    	Member_Entry__c DU0_member2 = new Member_Entry__c();
	    	DU0_member2.I_Status__c = 'Extracted New';
	    	DU0_member2.BF_Duplicated__c = true;
	    	DU0_member2.BF_Client_ID__c = '12345FF';
	    	DU0_member2.Baby1_Birthday__c = Date.today().addYears(-281);
	    	DU0_member2.Baby2_Birthday__c = Date.today().addYears(-282);
	    	DU0_member2.Baby3_Birthday__c = Date.today().addYears(-283);
	    	DU0_member2.Administrative_Area__c = Am.Id;
	    	insert DU0_member2;
	    	 
	    	/*-DU1:Register Phone查重*/
	        //Member Entry
	        //Account CN_HouseHold
	     	Account DU1_Acc = new Account();
	        DU1_Acc.Name = 'DU1';
	        DU1_Acc.RecordTypeId = ObjectUtil.GetRecordTypeID(Account.getSObjectType(), 'CN_HouseHold');
	        DU1_Acc.BF_Client_ID__c = '12345FE';
	        DU1_Acc.Phone = '18500563061';
	        DU1_Acc.Standard_City__c = Am.Id;
	        insert DU1_Acc;
	        //Baby Contact
	        Contact DU1_Con = new Contact();
	        DU1_Con.RecordTypeId = ObjectUtil.GetRecordTypeID(Contact.getSObjectType(), 'CN_Child');
	        DU1_Con.LastName = 'zhang';
	        DU1_Con.Birthdate = Date.today();
	        DU1_Con.Primary_RecruitChannel_Type__c = '1';
	        DU1_Con.AccountId = DU1_Acc.Id;
	        insert DU1_Con;
	        Member_Entry__c DU1_member = new Member_Entry__c();
	    	DU1_member.I_Status__c = 'Extracted New';
	    	DU1_member.BF_Duplicated__c = true;
	    	DU1_member.BF_Client_ID__c = '12345FG';
	    	DU1_member.Phone__c = '18500563061';
	    	DU1_member.Baby1_Birthday__c = Date.today().addYears(-1);
	    	DU1_member.Register_Source__c = '1';
	    	DU1_member.Register_Sub_Source__c = '2';
	    	DU1_member.Baby2_Birthday__c = Date.today().addYears(-2);
	    	DU1_member.Baby3_Birthday__c = Date.today().addYears(-3);
	    	DU1_member.Administrative_Area__c = Am.Id;
	    	insert DU1_member;
	    	/*-DU2:Name+City+生日差值30天查重-*/ 
	    	Account DU2_Acc = new Account();
	        DU2_Acc.Name = 'DU2';
	        DU2_Acc.RecordTypeId = ObjectUtil.GetRecordTypeID(Account.getSObjectType(), 'CN_HouseHold');
	        DU2_Acc.BF_Client_ID__c = '22345FE';
	        DU2_Acc.Phone = '18400563061';
	        DU2_Acc.Standard_City__c = Am.Id;
	        insert DU2_Acc;
	        //Baby Contact
	        Contact DU2_Con = new Contact();
	        DU2_Con.RecordTypeId = ObjectUtil.GetRecordTypeID(Contact.getSObjectType(), 'CN_Child');
	        DU2_Con.LastName = 'zhang';
	        DU2_Con.Birthdate = Date.today();
	        DU2_Con.Primary_RecruitChannel_Type__c = '1';
	        DU2_Con.AccountId = DU2_Acc.Id;
	        insert DU2_Con;
	        Member_Entry__c DU2_member = new Member_Entry__c();
	        DU2_member.Name = DU2_Acc.Name;
	    	DU2_member.I_Status__c = 'Extracted New';
	    	DU2_member.BF_Duplicated__c = true;
	    	DU2_member.BF_Client_ID__c = '22345FG';
	    	DU2_member.Phone__c = '18400563062';
	    	DU2_member.Baby1_Birthday__c = Date.today();
	    	DU2_member.Baby2_Birthday__c = Date.today().addYears(-2);
	    	DU2_member.Baby3_Birthday__c = Date.today().addYears(-3);
	    	DU2_member.Administrative_Area__c = Am.Id;
	    	insert DU2_member;
	    	Lead lea = new Lead();
	    	lea.LastName = 'lea test';
	    	lea.Phone = '18500563062';
	    	insert lea;
	    	
	    	/*-DU3:In Use Phone查重-*/
	    	/*Account*/
	    	//Account CN_HouseHold
	     	Account DU3_Acc = new Account();
	        DU3_Acc.Name = 'DU3_Acc';
	        DU3_Acc.RecordTypeId = ObjectUtil.GetRecordTypeID(Account.getSObjectType(), 'CN_HouseHold');
	        DU3_Acc.BF_Client_ID__c = '12345FM';
	        DU3_Acc.Phone = '18500563061';
	        DU3_Acc.Standard_City__c = Am.Id;
	        DU3_Acc.Mobile_Phone__c = '11122223333';
	        insert DU3_Acc;
	        //Baby Contact
	        Contact DU3_Con = new Contact();
	        DU3_Con.RecordTypeId = ObjectUtil.GetRecordTypeID(Contact.getSObjectType(), 'CN_Child');
	        DU3_Con.LastName = 'zhang';
	        DU3_Con.Birthdate = Date.today();
	        DU3_Con.Primary_RecruitChannel_Type__c = '1';
	        DU3_Con.AccountId = DU3_Acc.Id;
	        insert DU3_Con;
	        Member_Entry__c DU3_member = new Member_Entry__c();
	        DU3_member.Name = 'DU3_member';
	    	DU3_member.I_Status__c = 'Extracted New';
	    	DU3_member.BF_Duplicated__c = true;
	    	DU3_member.BF_Client_ID__c = '12345FG';
	    	DU3_member.Phone__c = '18500563069';
	    	DU3_member.Mobile_Phone__c = '11122223333';
	    	DU3_member.Baby1_Birthday__c = Date.today().addYears(-1);
	    	DU3_member.Register_Source__c = '1';
	    	DU3_member.Register_Sub_Source__c = '2';
	    	DU3_member.Baby2_Birthday__c = Date.today().addYears(-2);
	    	DU3_member.Baby3_Birthday__c = Date.today().addYears(-3);
	    	DU3_member.Administrative_Area__c = Am.Id;
	    	insert DU3_member;
	    	Lead DU3_lead = new Lead();
	    	DU3_lead.LastName = 'du3lead';
	    	DU3_lead.Phone = DU3_member.Phone__c;
	    	insert DU3_lead;
			/*-has related lead-*/
			Member_Entry__c related_lead_member = new Member_Entry__c();
	    	related_lead_member.I_Status__c = 'Extracted New';
	    	related_lead_member.BF_Duplicated__c = true;
	    	related_lead_member.BF_Client_ID__c = '12345FG';
	    	related_lead_member.Phone__c = '18500563061';
	    	related_lead_member.Baby1_Birthday__c = Date.today().addYears(-1);
	    	related_lead_member.Register_Source__c = '1';
	    	related_lead_member.Register_Sub_Source__c = '2';
	    	related_lead_member.Baby2_Birthday__c = Date.today().addYears(-2);
	    	related_lead_member.Baby3_Birthday__c = Date.today().addYears(-3);
	    	related_lead_member.Administrative_Area__c = Am.Id;
	    	related_lead_member.Lead__c = DU3_lead.Id;
	    	insert related_lead_member;
	    	MemberEntrySplitBatch MemberSplitBatch = new MemberEntrySplitBatch();
			ID batchprocessid = Database.executeBatch(MemberSplitBatch);
			//method code coverage
			List<Answer__c> list_ans = new List<Answer__c>();
			Answer__c ans1 = new Answer__c();
			ans1.Q017__c = '1';
			list_ans.add(ans1);
			Answer__c ans2 = new Answer__c();
			ans2.Q017__c = '2';
			list_ans.add(ans2);
			insert list_ans;
			MemberSplitBatch.MoveAnswerToBaby(DU1_con.Id, list_ans);
			
			Case s = new Case();
			s.Subject = 'test';
			insert s;
			List<Case> list_Case = new List<Case>();
			list_Case.add(s);
			MemberSplitBatch.MoveCasesToBaby(DU1_Acc.Id,DU1_con.Id, list_Case);
        }
    }
    
}