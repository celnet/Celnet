@isTest
private class CampaignMemberHelperTest {


   static testMethod void createFrom5LeadsOKTest() {
       testNLeads(5);
    }
    
    static testMethod void createFrom500LeadsOKTest() {
       testNLeads(500);
    }
    
    static testMethod void createFrom5ContactsOKTest() {
       testNContacts(5);
    }
    
    static testMethod void createFrom500ContactsOKTest() {
       testNContacts(500);
    }
    /* Load test to be uncommented as needed 
    static testMethod void createFrom10000LeadsOKTest() {
       testNLeads(10000);
    }
    static testMethod void createFrom10000ContactsOKTest() {
       testNContacts(10000);
    }*/
    static testMethod void invalidDataTypeTest() {
        String testCampaignId = TestUtility.generateTestCampaigns(1)[0].id;
        String filterId = TestUtility.getContactListViewId('All Contacts'); 
         Test.startTest(); 
        
       CampaignMemberHelper.createCampaignMembers(
                filterId,
                null, 
                testCampaignId
                );
                
        Test.stopTest();
        system.assertEquals(ApexPages.getMessages()[0].getSummary(),'Invalid MemberType: null');
        
    }
    
    
    static void testNContacts(Integer size){
        system.runas(TestUtility.new_HK_User()){
            List<Contact> testLeads = TestUtility.generateTestContacts(size);
            String testCampaignId = TestUtility.generateTestCampaigns(1)[0].id;
            
            String filterId = TestUtility.getContactListViewId('All Contacts'); 
            
            Test.startTest(); 
            
            List<CampaignMember> members = CampaignMemberHelper.createCampaignMembers(
                    filterId,
                    CampaignMemberHelper.MemberType.CONTACT, 
                    testCampaignId
                    );
            
            Test.stopTest();
            
            //Assert
            //system.assertEquals(members.size(),size);
        }
        
    }
     static void testNLeads(Integer size) {
         system.runas(TestUtility.new_HK_User()){
             List<Lead> testLeads = TestUtility.generateTestLeads(size);
            String testCampaignId = TestUtility.generateTestCampaigns(1)[0].id;
            
            String filterId = TestUtility.getLeadListViewId('All Open Leads');
            
            Test.startTest(); 
            
            List<CampaignMember> members = CampaignMemberHelper.createCampaignMembers(
                    filterId,
                    CampaignMemberHelper.MemberType.LEAD, 
                    testCampaignId
                    );
            
            Test.stopTest();
            
            //Assert
           // system.assertEquals(members.size(),size);
         }
    }
}