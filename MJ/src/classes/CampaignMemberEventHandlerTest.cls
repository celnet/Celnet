/*Author:leo.bi@celnet.com.cn
 *Created On:2014-5-28
 *function:The Test Class of CampaignMemberEventHandler
 *Apply To:CN
 */
@isTest(SeeAllData = true)
private class CampaignMemberEventHandlerTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        system.runAs(TestUtility.new_CN_User())
        {
	        Campaign c = new Campaign();
	        c.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Invitation' and SobjectType=:'Campaign'].id;
	        c.Name = 'Test Campaign';
	        c.Description = 'Test Description';
	        c.Campaign_Member_Limit__c = 10000;
	        insert c;
	        //new Account
			Account acc = new Account();
			acc.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_HouseHold' and SobjectType=:'Account'].id;
			acc.Name = 'HouseHold' + String.valueOf(DateTime.now());
			acc.Last_Name__c ='HouseHold';
			acc.status__c = 'Mother';
			insert acc;
	        //new Contact
	        Contact con = new Contact();
			con.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
			con.AccountId = acc.Id;
			con.LastName = 'conLast' + String.valueOf(DateTime.now());
			con.Birthdate = Date.today().addDays(-5);
			insert con;
			CampaignMember cm = new CampaignMember();
			cm.ContactId = con.Id;
			cm.CampaignId = c.Id;
			insert cm;
			Answer__c ans = new Answer__c();
			ans.Contact__c = con.Id;
			ans.Status__c = 'Finish';
			ans.Finished_On__c = DateTime.now();
			ans.Campaign_Member_Id__c = cm.Id;
			ans.Q048__c = '参加';
			ans.Q047__c = '参加';
			insert ans;
			//cm = [select Status, Id from CampaignMember where Id in :set_CampaignMember_Id];
			Answer__c ans1 = new Answer__c();
			ans1.Contact__c = con.Id;
			ans1.Status__c = 'Finish';
			ans1.Finished_On__c = DateTime.now();
			ans.Campaign_Member_Id__c = cm.Id;
			ans1.Q048__c = '不参加';
			ans1.Q047__c = '不参加';
			insert ans1;
			Answer__c ans2 = new Answer__c();
			ans2.Contact__c = con.Id;
			ans2.Status__c = 'Finish';
			ans2.Finished_On__c = DateTime.now();
			ans.Campaign_Member_Id__c = cm.Id;
			ans2.Q048__c = '考虑';
			ans2.Q047__c = '考虑';
			insert ans2;
			Test.startTest();
			//test finish back-visit
			Contact conBack = new Contact();
			conBack.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
			conBack.AccountId = acc.Id;
			conBack.LastName = 'conLast' + String.valueOf(DateTime.now());
			conBack.Birthdate = Date.today().addDays(-5);
			insert conBack;
			CampaignMember cmbackvisit = new CampaignMember();
			cmbackvisit.ContactId = conBack.Id;
			cmbackvisit.CampaignId = c.Id;
			cmbackvisit.Status = 'Attended and waiting for back-visit';
			cmbackvisit.Call_Status__c = 'Waiting Reciprocal Call';
			insert cmbackvisit;
			cmbackvisit.Call_Status__c = 'Finish Call';
			update cmbackvisit;
			cmbackvisit = [select Id, Call_Status__c,Status from CampaignMember where Id = :cmbackvisit.Id];
			system.assertEquals('Finish back-visit', cmbackvisit.Status);
			Test.stopTest();
        }
    }
}