/*
*绑定按钮：小百科
*/
public class WechatCallinWikiHandler extends WechatCallinMsgHandler
{
    public override void Handle(WechatCallinMsgPipelineContext Context)
    {
        string openId = Context.InMsg.FromUserName;
        WechatEntity.OutTextMsg outMsg = WechatEntity.GenerateOutMsg(Context.InMsg , '');
        WechatEntity.InEventMsg inEvent = (WechatEntity.InEventMsg)Context.InMsg;
        if(inEvent.EventKey != this.BindingInfo.Event_Key__c)
        {
            return;
        }
        string re = WechatBusinessUtility.CheckWechatUserRelationshipByFields(openId);
        outMsg.Content = WechatBusinessUtility.QueryRemindingByKey('RN040');
        Context.OutMsg = outMsg;
    }
}