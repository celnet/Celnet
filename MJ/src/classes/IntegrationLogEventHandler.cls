/*
Author: scott.zhang@celnet.com.cn 
Created On: 2014-05-19
Function: 
1.If the associated questionnaires on the contact will be assigned to the questionnaire on the task
Support Event: BeforeInsert,BeforeUpdate, AfterInsert, AfterUpdate
Apply To: CN 
*/
public class IntegrationLogEventHandler implements Triggers.Handler{
    public void Handle()
    {
        if(!Context.ApplyTo(new String[] {'CN'})) 
        {
            return;
        }
        if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate))
        {
            if(trigger.new[0] instanceof Task)
            {
            	try
	            {
	                this.autoSetQuestionnaire();
	            } catch(Exception e)
	            {
	            	Error_Log__c el = new Error_Log__c();
	            	el.Line_Number__c = e.getLineNumber();
	            	el.Message__c = e.getMessage();
	            	el.Stack_Trace_String__c = e.getStackTraceString();
	            	el.Type_Name__c = e.getTypeName();
	            	el.Error_Time__c = DateTime.now();
	            	el.Handler_Name__c = 'IntegrationLogEventHandler';
	            	el.User__c = UserInfo.getUserId();
	            	insert el;
	            }
            }
        }
 
    }
    //Auto Set Questionnaire__c
    private void autoSetQuestionnaire()
    {
        List<Task> taskList = trigger.new;
        Set<Id> Set_ContactIds = new Set<Id>();
        for(Task ta : taskList)
        {
        	//CallObject 为空时进入
			if(ta.CallObject != null || ta.Status=='Completed')
			continue;
        	if(trigger.isInsert)
        	{
        		//Auto Set Questionnaire__c
        		/*if(ta.WhoId != null && ta.CN_Team__c=='Outbound')// && ta.CallType =='Outbound'
           		Set_ContactIds.add(ta.WhoId);*/
           		
           		//Author:leo.bi@celnet.com.cn  2014-07-06  Function:If whtid is a case id, sync it to Case_Id__c
           		if(ta.WhatId != null && String.valueOf(ta.WhatId).startsWith('500'))
				{
					ta.Case_Id__c = ta.WhatId; 
				}
        	}
            
            
            if(trigger.isUpdate)
            {
                Task oldta = (Task)trigger.oldMap.get(ta.Id);
                //Auto Set Questionnaire__c
                if(oldta.WhatId != null && ta.WhatId!=null && String.valueOf(ta.WhatId).startsWith('001'))
                {
                    ta.WhatId= oldta.WhatId;
                }
                
                //*Author:leo.bi@celnet.com.cn  2014-07-06  Function:If whtid is a case id, sync it to Case_Id__c
                if(ta.WhoId != null && oldta.WhoId != null && ta.Case_Id__c != null && String.valueOf(ta.WhoId).startsWith('003') && String.valueOf(oldta.WhoId).startsWith('00Q'))
				{
					ta.WhatId = ta.Case_Id__c;
				}
                
            }
        }
    }
    
}