public without sharing class CampaignMemberHelper {
	
 	//Type of CompaignMember
    public enum MemberType {LEAD,CONTACT}
	
	public static List<CampaignMember> createCampaignMembers(String filterId, MemberType dataType, String campaignId){
		//Get leads/contacts from list view
		ApexPages.StandardSetController controller = getController(dataType);
		if (controller == null){
			return null;
		}
        controller.setPageSize(250);
        
        //Since SetFilterID throws fatal exception
        //when given a private list view, we gotta do a check here
        Set<String> visibleListViewIds = new Set<String>();
        for (System.SelectOption opt : controller.getListViewOptions()){
        	visibleListViewIds.add(opt.getValue());
        }
        if (visibleListViewIds.contains(filterID)){
        	controller.setFilterId(filterID);
        }else{
        	return null; //let caller handle. 
        }
        
        List<SObject> records = new List<SObject>();
        
        System.debug(LoggingLevel.INFO,'Entry');
        //add first page of records
        records.addAll(controller.getRecords().clone());
        //check and add subsequent pages 
        while (controller.getHasNext()){
            controller.next();
            records.addAll(controller.getRecords().clone());
        }
        System.debug(LoggingLevel.INFO,'End, size' + records.size());
        
        //create campaign members from leads/contacts
        List<CampaignMember> campaignmembersToCreate = new List<CampaignMember>();

       for(SObject obj:records){
               CampaignMember campaignmemberToAdd = new CampaignMember();

               campaignmemberToAdd.CampaignId = campaignID;
               if (dataType == MemberType.LEAD){
                   campaignmemberToAdd.LeadId = obj.id;
               }else if (dataType == MemberType.CONTACT){
                   campaignmemberToAdd.ContactId = obj.id;
               }
               campaignmembersToCreate.add(campaignmemberToAdd);

       }
       return campaignmembersToCreate;
	}
	
	
	// get controller based on MemberType
    private static ApexPages.StandardSetController getController(MemberType dataType){
        if (dataType == MemberType.LEAD ){
            return SetControllerFactory.newLeadController();
        } else if (dataType == MemberType.CONTACT){ 
            return SetControllerFactory.newContactController();
        } else {
        	ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.FATAL, 'Invalid MemberType: '+dataType));
            return null; 
        }
    }
}