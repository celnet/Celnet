/*
Author: shiqi.ng@sg.fujitsu.com
Purpose: Close QTask Operation by batch, called from QTaskDistributorController
*/
global class BatchCloseQTask implements Database.Batchable<SObject>{
	private Set<Id> qtaskIds;
    
    /*Constructor*/
    
    public BatchCloseQTask(Set<Id> qtaskIds){
        this.qtaskIds = qtaskIds;
    }
    /*Start Method*/
    global Database.QueryLocator start(Database.BatchableContext bc){
        String query = 'Select id, Original_Queue__c, Priority__c, OwnerId';
        query += '                     From Q_Task__c ';
        query += '                      Where Id in :qtaskIds';
        query += '                      And Original_Queue__c != null';
        
        return Database.getQueryLocator(query);
    }
    
    /*Execute*/
    global void execute(Database.BatchableContext bc, List<SObject> scope){
        for (SObject s : scope)
        {    
            Q_Task__c qt = (Q_Task__c) s;
            Id originalQueueId = Id.valueOf(qt.Original_Queue__c);
            qt.OwnerId = originalQueueId;
            qt.Priority__c = null; // clear priority when recalled.
            qt.Status__c = 'Closed';
        }
        update scope;
    }
    
    /**Finish*/
    global void finish(Database.BatchableContext bc){}
    
}