/*
 *用户在微信客户端点击在线咨询时由此Handler来处理
*/
public class WechatCallinOnlineHandler extends WechatCallinMsgHandler
{
    public override void Handle(WechatCallinMsgPipelineContext Context)
    {
        string openId = Context.InMsg.FromUserName;
        WechatEntity.OutTextMsg outMsg = WechatEntity.GenerateOutMsg(Context.InMsg , '');
        WechatEntity.InEventMsg inEvent = (WechatEntity.InEventMsg)Context.InMsg;
        if(inEvent.EventKey != this.BindingInfo.Event_Key__c)
        {
            return;
        }
        outMsg.Content = '將快推出，敬請期待。';
        Context.OutMsg = outMsg;
    }
}