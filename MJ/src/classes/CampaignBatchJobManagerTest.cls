@isTest
private class CampaignBatchJobManagerTest {

	/**
	Positive test for gettting Daily, weekly, and monthly jobs for today
	*/
    static testMethod void getTodayEntriesOKTest() {
        // prepare today's entries
        DateTime d = System.now();
    	
		String dayOfWeek = d.format('EEEE'); //Returns Monday, Tuesday, etc
		String day = d.format('d');//Returns 1,2,...30, 31 
		
		Campaign camp1 = TestUtility.generateTestCampaigns(1)[0];
		
        Campaign_Scheduler__c cs1 = new Campaign_Scheduler__c(
        	Preferred_Execution_Start_Hour__c = 0,
       		Execute_Day__c = 'Daily',
       		Master_Campaign__c = camp1.id
        );
        
        Campaign_Scheduler__c cs2 = new Campaign_Scheduler__c(
        	Preferred_Execution_Start_Hour__c = 0,
       		Execute_Day__c = dayOfWeek,
       		Master_Campaign__c = camp1.id
        );
        
        Campaign_Scheduler__c cs3 = new Campaign_Scheduler__c(
        	Preferred_Execution_Start_Hour__c = 0,
       		Execute_Day__c = day,
       		Master_Campaign__c = camp1.id
        );
        List<Campaign_Scheduler__c> cs = new List<Campaign_Scheduler__c>{cs1,cs2,cs3};
		insert cs;
		
		Test.startTest();
		CampaignBatchJobManager mgr = new CampaignBatchJobManager();
		List<Campaign_Scheduler__c> entries = mgr.getTodayEntries();
		        
		Test.stopTest();
		//assert getting 3 entries
		System.assertEquals(entries.size(), 3);
        
    }
    
    /**
	Positive test getting 1 daily for today, not getting jobs for tomorrow
	*/
    static testMethod void getTodayEntriesTomorrowOKTest() {
        // prepare only tomorrow's entries + one daily
        DateTime d = System.now().addDays(1);
    	
		String dayOfWeek = d.format('EEEE'); //Returns Monday, Tuesday, etc
		String day = d.format('d');//Returns 1,2,...30, 31 
		
		Campaign camp1 = TestUtility.generateTestCampaigns(1)[0];
		
        Campaign_Scheduler__c cs1 = new Campaign_Scheduler__c(
        	Preferred_Execution_Start_Hour__c = 0,
       		Execute_Day__c = 'Daily',
       		Master_Campaign__c = camp1.id
        );
        
        Campaign_Scheduler__c cs2 = new Campaign_Scheduler__c(
        	Preferred_Execution_Start_Hour__c = 0,
       		Execute_Day__c = dayOfWeek,
       		Master_Campaign__c = camp1.id
        );
        
        Campaign_Scheduler__c cs3 = new Campaign_Scheduler__c(
        	Preferred_Execution_Start_Hour__c = 0,
       		Execute_Day__c = day,
       		Master_Campaign__c = camp1.id
        );
        List<Campaign_Scheduler__c> cs = new List<Campaign_Scheduler__c>{cs1,cs2,cs3};
		insert cs;
		
		Test.startTest();
		CampaignBatchJobManager mgr = new CampaignBatchJobManager();
		List<Campaign_Scheduler__c> entries = mgr.getTodayEntries();
		        
		Test.stopTest();
		//assert getting only 1 entry, which is the daily one.
		System.assertEquals(entries.size(), 1);
		System.assertEquals(entries[0].Execute_Day__c, 'Daily');
        
    }
    
    /**
	Positive test grouping of jobs by window
	*/
    static testMethod void groupJobTest() {
        // prepare multiple jobs in a window for today
        DateTime d = System.now();
    	
		String dayOfWeek = d.format('EEEE'); //Returns Monday, Tuesday, etc
		String day = d.format('d');//Returns 1,2,...30, 31 
		
		Campaign camp1 = TestUtility.generateTestCampaigns(1)[0];
		// Start hour = 0, 3 jobs
        Campaign_Scheduler__c cs0_1 = new Campaign_Scheduler__c(
        	Preferred_Execution_Start_Hour__c = 0,
        	Preferred_Execution_Time__c = '0AM-8AM',
       		Execute_Day__c = 'Daily',
       		Master_Campaign__c = camp1.id
        );
        
        Campaign_Scheduler__c cs0_2 = cs0_1.clone();
        Campaign_Scheduler__c cs0_3 = cs0_2.clone();
        
        // Start hour = 8, 2 jobs
        Campaign_Scheduler__c cs8_1 = new Campaign_Scheduler__c(
        	Preferred_Execution_Start_Hour__c = 8,
        	Preferred_Execution_Time__c = '8AM-12PM',
       		Execute_Day__c = dayOfWeek,
       		Master_Campaign__c = camp1.id
        );
        
        Campaign_Scheduler__c cs8_2 = cs8_1.clone();
        
        // Start hour = 12, 4 jobs
        Campaign_Scheduler__c cs12_1 = new Campaign_Scheduler__c(
        	Preferred_Execution_Start_Hour__c = 12,
        	Preferred_Execution_Time__c = '12PM-18PM',
       		Execute_Day__c = dayOfWeek,
       		Master_Campaign__c = camp1.id
        );
        
        Campaign_Scheduler__c cs12_2 = cs12_1.clone();
        Campaign_Scheduler__c cs12_3 = cs12_1.clone();
        Campaign_Scheduler__c cs12_4 = cs12_1.clone();
        
        //Start hour = 18, 1 job
        Campaign_Scheduler__c cs18_1 = new Campaign_Scheduler__c(
        	Preferred_Execution_Start_Hour__c = 18,
        	Preferred_Execution_Time__c = '18PM-0AM',
       		Execute_Day__c = dayOfWeek,
       		Master_Campaign__c = camp1.id
        );
        
        List<Campaign_Scheduler__c> cs = new List<Campaign_Scheduler__c>{
				cs0_1,cs0_2,cs0_3,
				cs8_1,cs8_2,
				cs12_1,cs12_2,cs12_3,cs12_4,
				cs18_1
			};
		insert cs;
		integer initialCronCount = 
           database.countquery('SELECT COUNT()  FROM CronTrigger')
        ; 
		Test.startTest();
		CampaignBatchJobManager mgr = new CampaignBatchJobManager();
		List<Campaign_Scheduler__c> entries = mgr.getTodayEntries();
		Map <String, List<Campaign_Scheduler__c>> groupedJobs = mgr.groupJobsByWindow(entries);   
		
		mgr.scheduleChainedJobs(groupedJobs);
		Test.stopTest();
		//assert getting 4 grouped jobs by window
		System.assertEquals(groupedJobs.size(),4);
		//assert jobs size for each window
		System.assertEquals(groupedJobs.get('0').size(), 3);
		System.assertEquals(groupedJobs.get('8').size(), 2);
		System.assertEquals(groupedJobs.get('12').size(), 4);
		System.assertEquals(groupedJobs.get('18').size(), 1);
		
		//assert 4 jobs has been scheduled
		system.AssertEquals(
           database.countquery('SELECT COUNT()  FROM CronTrigger'),
           initialCronCount + 4); 
		
        
    }
    
    /* 
    * Positive test getting minutes
    */
    static testMethod void getMinutesOK() {
    	CampaignBatchJobManager mgr = new CampaignBatchJobManager();
    	system.assertEquals(mgr.minsFromMidnight('0'),0);
    	system.assertEquals(mgr.minsFromMidnight('8'), 8*60);
    	system.assertEquals(mgr.minsFromMidnight('12'),12*60);
    }
    
    /* 
    * Negative test getting minutes
    */
    static testMethod void getMinutesFail() {
    	CampaignBatchJobManager mgr = new CampaignBatchJobManager();
    	system.assertEquals(mgr.minsFromMidnight(''),null);
    	system.assert(ApexPages.hasMessages());
        system.assertEquals(ApexPages.getMessages()[0].getSummary(),'Schedule hour not found');
    }
    
    /* 
    * Negative test getting minutes
    */
    static testMethod void testScheduleChainedJobs(){
    	
    }
    
    //test scheduling, negative chain
}