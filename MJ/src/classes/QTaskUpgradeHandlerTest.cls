/*Author: leo.bi@celnet.com.cn
 *Created On: 2014-05-12
 *Function: QTaskUpgradeHandler Test Class
 *Apply To: ALL
 */
@isTest(SeeAllData = true)
private class QTaskUpgradeHandlerTest {

    static testMethod void myUnitTest() 
    {
    	system.runAs(TestUtility.new_CN_User())
		{
	        String commenName = 'LeoTest' + String.valueOf(DateTime.now());
	        //test case 1:insert an invitation qtask while there is already exsisting a Routine qtask
	        //new campaign
	        Campaign c = new Campaign();
	        c.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Invitation' and SobjectType=:'Campaign'].id;
	        c.Name = commenName;
	        c.Description = 'Test Description';
	        c.Campaign_Type__c = 'Invitation Special';
	        insert c;
	        //new Account
			Account acc = new Account();
			acc.Name = commenName;
			acc.Last_Name__c =commenName;
			acc.status__c = 'Mother';
			acc.Phone = '11000000000';
			insert acc;
	        //new Contact
	        Contact con = new Contact();
			con.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
			con.AccountId = acc.Id;
			con.LastName = commenName;
			con.Birthdate = Date.today().addDays(-5);
			insert con;
	        //new campainmember Waiting for invitation
	        CampaignMember cm = new CampaignMember();
	        cm.ContactId = con.Id;
	        cm.Call_Status__c = 'Waiting Invitation';
	        cm.Status = 'Sent';
	        cm.CampaignId = c.Id; 
	        insert cm;
	        Q_Task__c[] qtasks = [select Id,Business_Type__c, Contact__c, Campaign_Stage__c, Type__c, Status__c, ID__c from Q_Task__c where Contact__c = :con.Id]; 
	        Q_Task__c qtask = qtasks[0];
	        qtask.Type__c = 'Routine';
	        qtask.Business_Type__c = 'Routine';
	        update qtask;
	        qtasks = [select Id, Contact__c, Campaign_Stage__c, Type__c, Status__c, ID__c from Q_Task__c where Contact__c = :con.Id]; 
	        qtask = qtasks[0];
	        system.assertEquals('Elite', qtask.Type__c);
	        //test case 2:insert an Routine qtask while there is already a invitation qtask
	        //new Account
			Account routine_acc = new Account();
			routine_acc.Name = 'routine_HouseHold' + commenName;
			routine_acc.Last_Name__c ='routine_HouseHold' + commenName;
			routine_acc.status__c = 'Mother';
			insert routine_acc;
	        //new Contact
	        Contact routine_con = new Contact();
			routine_con.RecordTypeId = [select id from RecordType where DeveloperName=:'CN_Child' and SobjectType=:'Contact'].id;
			routine_con.AccountId = routine_acc.Id;
			routine_con.LastName = 'conLast' +commenName;
			routine_con.Birthdate = Date.today().addDays(-5);
			routine_con.Register_Date__c = Date.today().addDays(-3);
			insert routine_con;
			Q_Task__c[] routine_qtasks = [select Id,Business_Type__c, Contact__c, Campaign_Stage__c, Type__c, Status__c, ID__c from Q_Task__c where Contact__c = :routine_con.Id];
			Q_Task__c routine_qtask = routine_qtasks[0];
	        routine_QTask.Type__c = 'Invitation';
	        routine_QTask.Business_Type__c = 'Invitation';
	        update routine_QTask;
	        routine_qtasks = [select Id, Contact__c, Campaign_Stage__c, Type__c, Status__c, ID__c from Q_Task__c where Contact__c = :routine_con.Id]; 
	        routine_qtask = routine_qtasks[0]; 
	        system.assertEquals('Elite', routine_qtask.Type__c);
	        
	        //test error
	        routine_QTask.Contact__c = con.Id;
	        try
	        {
	        	update routine_QTask;
	        	
	        } catch(Exception e)
	        {
	        	system.debug(e.getMessage());
	        }
	        routine_QTask.Contact__c = routine_con.Id;
	        routine_QTask.Status__c = 'Closed';
	        routine_QTask.Type__c = 'Routine';
	        try
	        {
	        	update routine_QTask;
	        	
	        } catch(Exception e)
	        {
	        	system.debug(e.getMessage());
	        }
	        qtask.Status__c = 'Closed';
	        update qtask;
	        qtask.Status__c = 'Open';
	        try
	        {
	        	update qtask;
	        	
	        } catch(Exception e)
	        {
	        	system.debug(e.getMessage());
	        }
	    }
    }
}