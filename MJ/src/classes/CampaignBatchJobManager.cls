/**
* This class should be scheduled to run daily at midnight
* to retrieve all active campaign jobs to be schedule 
* for that day using "batch chaining"
*/

global without sharing class CampaignBatchJobManager implements Schedulable{
	
	global void execute(SchedulableContext sc) {
    	System.debug(LoggingLevel.INFO, 'Scheduler wakes up, getting entries');
    	Campaign_Scheduler__c[] entries = getTodayEntries();
    	
    	scheduleChainedJobs(groupJobsByWindow(entries));
    	System.debug(LoggingLevel.INFO, 'Scheduler done');
    }
    
    /**
    * Get entries to be excuted today based on daily, weekly (mon-sun), and monthly (1-31) jobs
    */
    @TestVisible 
    private Campaign_Scheduler__c[] getTodayEntries(){
    	DateTime d = System.now();
    	
		String dayOfWeek = d.format('EEEE'); //Returns Monday, Tuesday, etc
		String day = d.format('d');//Returns 1,2,...30, 31 
		System.debug(LoggingLevel.Info, 'dayOfWeek:'+dayOfWeek+'day'+day+'sys.now'+d);
    	Campaign_Scheduler__c[] entries = 
    				[SELECT 
						Id, Data_Type__c, Execute_Day__c, Master_Campaign__c,List_view_Name__c,
						Preferred_Execution_Time__c, List_view_Id__c, Preferred_Execution_Start_Hour__c,
						CreatedBy.Email, LastModifiedBy.Email
					FROM Campaign_Scheduler__c
					WHERE
						Inactive__c = false
						AND
						(
						Execute_Day__c = 'Daily'
						OR
						Execute_Day__c = :dayOfWeek
						OR
						Execute_Day__c = :day
						)
                     ORDER BY CreatedDate DESC
                    ];
						
		System.debug(LoggingLevel.Info, 'retrieved entries' + entries.size());
		return entries;
    }
    
    /**
    Using map, group entries in a list for each window, with Preferred_Execution_Start_Hour__c as keys (type:string)
    */
    @TestVisible
    private Map <String, List<Campaign_Scheduler__c>> groupJobsByWindow(Campaign_Scheduler__c[] entries){
    	Map <String, List<Campaign_Scheduler__c>> chainJobs = new map<String,List<Campaign_Scheduler__c>>();
    	
    	for (Campaign_Scheduler__c entry : entries){
    		String key = entry.Preferred_Execution_Start_Hour__c+'';
    		if (chainJobs.containsKey(key)){
    			chainJobs.get(key).add(entry);
    		}else{
    			List<Campaign_Scheduler__c> value = new List<Campaign_Scheduler__c>();
    			value.add(entry);
    			chainJobs.put(key, value);
    		}
    	}
    	return chainJobs;
    }
    
    /**
    * chain entries and schedule the first job of each window 
    */
    @TestVisible 
    private void scheduleChainedJobs(Map <String, List<Campaign_Scheduler__c>> chainJobs) {
    	for (String key : chainJobs.keySet()){
    		List<Campaign_Scheduler__c> jobs = chainJobs.get(key);
    		Integer minutesFromNow = minsFromMidnight(key);
    		String jobID = CampaignBatchJobManager.scheduleBatch(jobs,minutesFromNow);
    		System.debug(LoggingLevel.INFO,'Scheduling job by window:'+jobID);
    	}
    }
    
	/** 
	* retrieve schedule time (in hour) for the given window, return in minutes		
    */
    @TestVisible 
    private Integer minsFromMidnight(String startHour){
    	if (startHour != null && startHour != ''){
            return Integer.valueOf(startHour) * 60;
    	}
    	ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.FATAL, 'Schedule hour not found'));
    	return null;
	}
	
	private static String scheduleBatch(List<Campaign_Scheduler__c> chainedJobSettings, Integer minsFromNow){
		if (chainedJobSettings != null && chainedJobSettings.size() > 0 ){
			
			String window = chainedJobSettings[0].Preferred_Execution_Time__c;
			CampaignBatchJob scheduledJob= new CampaignBatchJob(chainedJobSettings);
			String jobName = 'Window:'+window +'('+chainedJobSettings.size()+' chained job) created on '+System.now().format('dd-MM-YY hh:mm');
			
			return system.schedulebatch(scheduledJob,jobName,minsFromNow);
			
		}else{
			ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.FATAL, 'Campaign scheduler setting not found.'));
			return null;
		}
	}
	
	/**
	* schedule the next chained job immediately (after a minute). Called by CampaingBatchJob.finish() 
	*/
	public static String scheduleNextChain(List<Campaign_Scheduler__c> chainedJobSettings){
		return scheduleBatch(chainedJobSettings , 1);
	}

}