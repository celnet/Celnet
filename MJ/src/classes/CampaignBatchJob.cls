/*
* Object of this class represent a campaign batch job instance
*/
global without sharing class CampaignBatchJob implements Database.Batchable<CampaignMember> ,  Database.Stateful
{
	 global final List<Campaign_Scheduler__c> chainedJobSettings;
	 private final Campaign_Scheduler__c currentJobSetting;
	 private final CampaignBatchEmail emailMessage;
	 private boolean hasError;
	 
	 //Constructor
	global CampaignBatchJob(List<Campaign_Scheduler__c> chainedJobSettings){
		this.chainedJobSettings = chainedJobSettings.clone();
		//set current job setting to first item of chained job settings, trimming the list for subsequent jobs 
		if (chainedJobSettings.size() > 0){
			currentJobSetting = this.chainedJobSettings.remove(0);
		}
		
		emailMessage = new CampaignBatchEmail(currentJobSetting);
		hasError = false;
	}
    

    // "Start" runs when the class in instantiated. Creates Campaign Members to be inserted 
    global Iterable<CampaignMember> start(Database.BatchableContext bcMain) {
    	System.debug(LoggingLevel.INFO,'Job Start:'+bcMain.getJobId());
    	try
    	{	
	    	emailMessage.startDateTime = system.now().format();
	    	emailMessage.jobId = String.valueOf(bcMain.getJobId());
	    	
			//create new campaign, date appended to name, link to master
			Campaign newCampaign = createCampaign(currentJobSetting.Master_Campaign__c);
			String newCampaignId = newCampaign.id;
			
			// pass in newly created campaign ID, memberType, filterID to retrieve campaignMembers using Helper Class
			String filterId = currentJobSetting.List_View_Id__c;
			
			CampaignMemberHelper.MemberType memberType = 
				(currentJobSetting.Data_Type__c == 'Leads') ? CampaignMemberHelper.MemberType.LEAD : CampaignMemberHelper.MemberType.CONTACT;
			List<CampaignMember> members = CampaignMemberHelper.createCampaignMembers(filterId, memberType, newCampaignId);
			
			if (members!=null){
				emailMessage.createdCampaignName = newCampaign.Name;
	            emailMessage.createdCampaignId = newCampaignId;
				emailMessage.memberCount = members.size();
	        	return members;
			}else{
				throw new BatchCampaignException('Error getting campaign members. Please ensure List View is not set as "Visible only to me". ');
			}
    	}
    	catch(Exception e)
    	{
    		hasError = true;
    		emailMessage.status='Failed';
    		emailMessage.exceptionMessage = e.getMessage();
    		emailMessage.endDateTime = system.now().format();
    		emailMessage.sendEmail();
    		
    		system.debug(LoggingLevel.ERROR,'Exception caught:'+e);
    		return new List<CampaignMember>();
    	}
    }
    
    // "Execute" is what is being run as a separate process per batch
    global void execute(Database.BatchableContext bcMain, List<CampaignMember> records) {
    	insert records;
    }
    
    // 'Finish'
     global void finish(Database.BatchableContext bcMain) {
        //report and send email notify job finish status
        System.debug(LoggingLevel.INFO,'Job End'+bcMain.getJobId());
        if (!hasError){
	        emailMessage.status='Success';
	        emailMessage.endDateTime = system.now().format();
	        emailMessage.sendEmail();
	    }
        //scheule pending chained jobs if any
        if (chainedJobSettings.size() > 0){
        	//schedule next job.
        	System.debug(LoggingLevel.INFO,'Scheduling next chained job!! Size:'+chainedJobSettings.size());
        	String jobID = CampaignBatchJobManager.scheduleNextChain(chainedJobSettings);
        	System.debug(LoggingLevel.INFO,'Scheduled next chained:'+jobID);
        } else{
        	System.debug(LoggingLevel.INFO,'Chained Job End');
        }
    }
    
   
    
    /*
    * Helper method to clone Campaign from masterCampaign
    */
     private Campaign createCampaign(String masterCampaignID){
    	List<Campaign> masterCampaign = [SELECT name, type, Coupon_Type__c, Nature__c, Target__c, Channel__c
                                         
                                         from Campaign WHERE id =:masterCampaignID];
    	if (masterCampaign == null || masterCampaign.isEmpty()){
    		throw new BatchCampaignException('Master Campaign (ID: ' + masterCampaignID + ') not found');
    		return null;
    	}
    	emailMessage.masterCampaignName = masterCampaign[0].Name;
    	Campaign campaign = masterCampaign[0].clone();
    	campaign.IsActive = true;
    	campaign.Is_Master_Compaign__c = false; 
    	campaign.ParentId = masterCampaignID;
    	campaign.Description = 'Batched Created on ' + system.now().format('EEEE d MMM YYYY hh:mm');    	
    	String appendDate = system.now().format('YYYYMMdd');
    	campaign.Name = campaign.Name + '_'+appendDate;
    	
    	insert campaign;
    	return campaign;
    }
    
    public class BatchCampaignException extends Exception{}
    
}