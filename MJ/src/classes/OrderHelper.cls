public without sharing class OrderHelper{

    public static List<Contact> getRelatedBabies(Order__c orderHeader)
    {
        List<Contact> relatedBabies = new List<Contact>();
        relatedBabies = 
            [SELECT ID,Name,Chinese_Name__c,Gender__c,Age__c,Birthdate FROM Contact 
             WHERE AccountId=:orderHeader.HouseHold__c AND RecordType.DeveloperName='Household_Child'
            AND Inactive__c = false
            ];
        return relatedBabies;
    }
    
    public static Boolean isMaxOrderPeriodAllowed(Order_Item__c currentItem, 
    	ID accountID,  Integer allowedDays, Integer maxAllowedQuantity, Integer offset)
    {
    	ID orderItemId = currentItem.Id;
    	ID ProductID = currentItem.Product__c;
    	Integer currentQuantity =  Integer.valueOf(currentItem.Quantity__c);
    	    	
        //Using Cancelled_c which is a fourmla field to consolidate mutiple status
        Datetime cutOffDate = Date.today().addDays(-allowedDays);
        //Sum orders since cutOffDate, excluding current order item if editing on existing 
        List<AggregateResult> ar = 
            [SELECT SUM(Quantity__c) sum FROM Order_Item__c 
             WHERE Order__r.HouseHold__c =:accountID AND Product__c =:ProductID AND Cancelled__c = false
             AND CreatedDate >= :cutOffDate AND ID <> :orderItemId];
        /* AR is never empty        
        if(ar.isEmpty()) 
            return true;
        else 
        {*/
            Integer totalItemsOrdered = Integer.valueOf(ar[0].get('sum'));
            if (totalItemsOrdered == null)
            	totalItemsOrdered = 0;
            if(totalItemsOrdered - offset + currentQuantity > maxAllowedQuantity)
                return false;
            else 
                return true;
        //}
    }
}