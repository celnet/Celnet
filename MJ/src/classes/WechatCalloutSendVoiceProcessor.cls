/*
*Author:michaelmi@celnet.com.cn
*Date:20141011
*Function:send voice message to cmrs interface and receive link save to voice message in salesforce
*/ 
public class WechatCalloutSendVoiceProcessor extends WechatCalloutProcessor 
{
	list<Wechat_Callout_Task_Queue__c> sendNewsQueue = new list<Wechat_Callout_Task_Queue__c>();
	public override void DoCallout(Wechat_Callout_Task_Queue__c WechatTask)
    {
    	List<Wechat_Message__c> wmList = [select id,(select id from attachments) from Wechat_Message__c where id = :WechatTask.Media_ID__c];
        if(wmList.isempty() || wmList.size() > 1 || wmList[0].attachments.isempty())
        {
            return;  
        }
        Attachment voice = [select body from Attachment where id = :wmList[0].attachments[0].id];
        string sendBody = EncodingUtil.base64encode(voice.body).replace('+','#'); 
    	system.debug('*****************Start DoCallout**********************' + WechatTask);
    	Http h = new Http();
    	HttpRequest req = new HttpRequest();
    	req.setMethod('POST');
    	req.setEndpoint('http://www.meadjohnsonhkwechat.com/buldcard.php');   
    	string body = 'WechatUserID='+WechatTask.Open_ID__c
    				  +'&SFVoiceID='+WechatTask.Media_ID__c
    				  +'&PublicAccountName='+WechatTask.Public_Account_Name__c
    				  +'&file='+sendBody;
    				  //+'&file='+WechatTask.Msg_Body__c;
    	system.debug('******************get the request Body*****************'+ body);
    	req.setBody(body);
    	HttpResponse res = h.send(req);
    	system.debug('******************Sent the request*********************' + res);
    	if(res.getStatus() == 'OK' && res.getStatusCode() == 200)
    	{
    		APlusResponse aPlus = (APlusResponse)JSON.deserialize(res.getBody(), APlusResponse.class);
    		system.debug('***************get the APlusResponse***************' + aPlus);
    		if(aPlus.Status == 'Success')
    		{
    			Wechat_Message__c msg = new Wechat_Message__c(ID=WechatTask.Media_ID__c);
    			msg.A_Family_Url__c = aPlus.ReturnLink;
    			ObjectList.add(msg);
    			string replace = '<a href="'+aPlus.ReturnLink+'">【這裡】</a>';
    			string reminding = WechatBusinessUtility.QueryRemindingByKey('RN038',replace);
    			string textBody = WechatEntity.GenerateSvcTextMsg(WechatTask.Open_Id__c,reminding);
    			Wechat_Callout_Task_Queue__c textQueue = WechatCalloutQueueManager.CreateSendSvcTextMsgTask(WechatTask.Public_Account_Name__c,textBody,'WechatCallOutSendTextMsgProcessor');
				sendNewsQueue.add(textQueue);
    		}	
    	}
    }
    //保存数据
    public override void FinishData()
    {
    	if(!ObjectList.isempty())
    	{
    		update ObjectList;	
    	}
    	if(!sendNewsQueue.isempty())
    	{
    		insert sendNewsQueue;
    	}
    }
    //封装由CMRS返回值的Class
	public class APlusResponse
	{
		public string WechatUserID{get;set;}
		public string SFVoiceID{get;set;}
		public string Status{get;set;}
		public string ReturnLink{get;set;}
		public string PublicAccountName{get;set;}
	}
}