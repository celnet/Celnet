@isTest
public with sharing class TestUtility {
	
	/*** New CN / HK users for testing ***/
		
	public static User new_CN_User(){
		User u;
		//To avoid Mixed DML operation bug, run as current user
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		System.runAs ( thisUser ) {
	    	Profile p = [Select id from Profile where Name like 'CN_API ONLY%' limit 1];
	    	UserRole r = [Select id from UserRole where Name = 'CN CRM'];
	    	u = new User(Alias = 'CNuser', Email='CNuser@testorg.com', 
	      		EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='zh_CN', 
	      		LocaleSidKey='zh_CN', ProfileId = p.Id, UserRoleId = r.id,
	      		TimeZoneSidKey='Asia/Shanghai', UserName='CNuser@testorg.com', Region__c='China');
	    	insert u;
		}
    	return u;
    }
	public static User new_HK_User(){
		User u;
		//To avoid Mixed DML operation bug, run as current user
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
    	System.runAs ( thisUser ) {
	    	Profile p = [Select id from Profile where Name = 'Health Consultant User' limit 1];
	    	UserRole r = [Select id from UserRole where Name = 'HK CRM'];
	    	u = new User(Alias = 'HKuser', Email='CHKuser@testorg.com', 
	      		EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	      		LocaleSidKey='en_US', ProfileId = p.Id, UserRoleId = r.id,
	      		TimeZoneSidKey='Asia/Hong_Kong', UserName='HKuser@testorg.com');
	    	insert u;
    	}
    	return u;
    }
    
    /** Custom Settings for Member_ID (HK and CN), Starting number 1888 **/
    public static void setupMemberIDCustomSettings(){
    	//China
    	Member_ID_Setting__c cn_setting = new Member_ID_Setting__c();
    	String year = String.valueOf(Date.today().year());
    	cn_setting.name = year+'-CN'; //key is YYYY-CN
    	cn_setting.Next_ID__c = 1888;
    	insert cn_setting;
    	//HK
    	Member_ID_Setting__c hk_setting = new Member_ID_Setting__c();
        hk_setting.Name = String.valueOf(Date.today().year());
        hk_setting.Next_ID__c = 1888;
        insert hk_setting;
    }
	/** Campaign Scheduler Related Test Helper methods ***/
	public static String getLeadRecentlyViewedId(){
		return getLeadListViewId('Recently Viewed Leads');
	}
	
	public static String getLeadListViewId(String label){
		for ( SelectOption opt : SetControllerFactory.newLeadController().getListViewOptions()){
			if (opt.getLabel()==label){
				return opt.getValue();
			}
		}
		return null;
	}
	
	public static String getContactRecentlyViewedId(){
		return getContactListViewId('Recently Viewed Contacts');
	}
	
	public static String getContactListViewId(String label){
		for ( SelectOption opt : SetControllerFactory.newContactController().getListViewOptions()){
			if (opt.getLabel()==label){
				return opt.getValue();
			}
		}
		return null;
	}
	
	public static List<Contact> generateTestContacts(Integer size){
		List <Contact> objList  = new List<Contact>();
		for(integer i = 0; i<size; i++){
	         Contact a = new Contact(LastName='testContact', title='test',Description='Num:'+i);
	         objList.add(a);
	      }
	   insert objList;
	   return objList;
	}
	
	public static List<Lead> generateTestLeads(Integer size){
		List <Lead> objList  = new List<Lead>();
		for(integer i = 0; i<size; i++){
	         Lead a = new Lead(LastName='testContact', title='test',Description='Num:'+i); 
	         a.Status = 'Open';
	         objList.add(a);
	      }
	   insert objList;
	   return objList;
	}
	
	public static List<Campaign> generateTestCampaigns(Integer size){
		List <Campaign> objList  = new List<Campaign>();
		for(integer i = 0; i<size; i++){
	         Campaign a = new Campaign(name='testCampaign'+i, isActive=true, Status='Planned'); 
	         objList.add(a);
	      }
	   insert objList;
	   return objList;
	}
}