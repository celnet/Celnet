/*
Author: scott.zhang@celnet.com.cn
Created On: 2014-05-11
Function: Run at start day of every month, launch non-real time Evaluation Flow for Reciprocal, 
		 that might be go back to Greet or Postphone
Apply To: CN
*/
global class RoutineQTaskDistributionBatch implements Database.Batchable<sObject>,Schedulable{         
	public Date StartOfMonth{get;set;}
	public String StrSql{get;set;}
	global void execute(SchedulableContext sc)  
	{
		RoutineQTaskDistributionBatch  RoutineBatch  = new RoutineQTaskDistributionBatch();
		RoutineBatch.StartOfMonth = date.today().toStartOfMonth();
		Database.executeBatch(RoutineBatch,200); 
	}
	global Database.QueryLocator start(Database.BatchableContext BC)   
	{
		String query = 'Select Questionnaire__c,Id,Cannot_Be_Contacted__c,Last_Routine_Call_Time__c,Verified__c,Register_Date__c,Birthdate,Routine_Evaluated_On__c,Next_Routine_Evaluation_Date__c,Appended_Log__c,Duplicate_Status__c,Inactive__c,RecordTypeId,'
					   +'(Select Questionnaire__c,Questionnaire__r.Business_Type__c,Finished_On__c From Answers__r where Status__c =\'Finish\' and Finished_On__c !=null ) ' 
					   +'from Contact where RecordType.DeveloperName=\'CN_Child\' and Days_Of_Age__c<=830 and Inactive__c = false and Cannot_Be_Contacted__c = false and Register_Date__c != null '
					   +' and (Next_Routine_Evaluation_Date__c='+String.valueOf(StartOfMonth)
					   +' or (Next_Routine_Evaluation_Date__c =null and Routine_Evaluated_On__c =null))';
		if(StrSql != null)
		{
			//data
			return Database.getQueryLocator(StrSql);
		}
		else
		{
			return Database.getQueryLocator(query);
		}
		
	} 
	global void execute(Database.BatchableContext BC, List<Contact> scope)   
	{
		RoutineEvaluator RoutineEva = new RoutineEvaluator(scope,StartOfMonth);
		RoutineEva.TimingType = 'Non-Real Time'; 
		RoutineEva.Run();
	}
	global void finish(Database.BatchableContext BC)
	{
    	
  	}
}