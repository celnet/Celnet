@isTest
public class TaskTriggerTest{
    public static testMethod void testTaskTrigger()
    {
    	 system.runas(TestUtility.new_HK_User()){
    	//Create custom settings
        TestUtility.setupMemberIDCustomSettings();
        
        //Get RecordType ID for HouseHold and HouseHold Child
        RecordType typeHouseHold = [Select Id From RecordType Where DeveloperName ='HouseHold' and SobjectType ='Account' limit 1];
        RecordType typeHouseHoldChild = [Select Id From RecordType Where DeveloperName ='Household_Child' and SobjectType ='Contact' limit 1];

        // Create a test Household
        Account account = new Account();
        account.Name = 'Account';
        account.Last_Name__c = 'Account';
        account.RecordTypeId = typeHouseHold.Id;
        insert account;
        
        //Creat test baby 1
        Contact contact = New Contact();
        contact.FirstName='Test';
        contact.LastName='Baby';
        contact.AccountId=account.ID;
        contact.RecordTypeId = typeHouseHoldChild.Id;
        insert contact;
        
        test.startTest();
        
        //Test Case 1: insert new Task 1
        Task task1 = new Task();
        task1.WhoId = contact.ID;
        task1.whatId = account.ID;
        task1.Subject = 'Outbound Call';
        task1.Status = 'Connected/Closed';
        task1.OB_Call_List__c = 'EMA+ (-2 month)';
        task1.Where_did_you_heard_from_our_product__c ='others';
        task1.Willing_to_Try__c='Yes';
        //task1.Continue_to_use__c='No';
        //task1.Reason_not_continue_to_use__c='Others';
        insert task1;
        
        Task createdTask1 = [SELECT ID,CreatedDate,OB_Call_List__c,Status,Willing_to_Try__c,Continue_to_use__c 
                             FROM TASK WHERE ID = :task1.ID];
        Contact createdContact = [SELECT ID,Finished_Calls__c,Willing_to_Try__c,Continue_to_use__c 
                                  FROM CONTACT WHERE ID = :contact.ID];
        //String timeString1 = createdTask1.CreatedDate.format('yyyy/MM/dd HH:mm');
        System.assertEquals('EMA+ (-2 month)--Closed',createdContact.Finished_Calls__c);
        System.assertEquals('EMA+ (-2 month)--Yes',createdContact.Willing_to_Try__c);
        System.assertEquals(NULL,createdContact.Continue_to_use__c);
        
        //Test Case 2: delete Task 1
        delete task1;
      
        createdContact = [SELECT ID,Finished_Calls__c,Willing_to_Try__c,Continue_to_use__c 
                          FROM CONTACT WHERE ID = :contact.ID];
        System.assertEquals(NULL,createdContact.Finished_Calls__c);
        System.assertEquals(NULL,createdContact.Willing_to_Try__c);
        System.assertEquals(NULL,createdContact.Continue_to_use__c);
        
       
        //Test Case 3: insert new Task 2
        Task task2 = new Task();
        task2.WhoId = contact.ID;
        task2.whatId = account.ID;
        task2.Subject = 'Outbound Call';
        task2.Status = 'Line Busy';
        task2.OB_Call_List__c = 'Welcome call Priority One List';
        insert task2;
        
        Task createdTask2 = [SELECT ID,CreatedDate,OB_Call_List__c,Status,Willing_to_Try__c,Continue_to_use__c 
                             FROM TASK WHERE ID = :task2.ID];
        //String timeString2 = createdTask2.CreatedDate.format('yyyy/MM/dd HH:mm');
        createdContact = [SELECT ID,Finished_Calls__c,Willing_to_Try__c,Continue_to_use__c 
                          FROM CONTACT WHERE ID = :contact.ID];

        System.assertEquals('Welcome call Priority One List--Open',createdContact.Finished_Calls__c);
        System.assertEquals(NULL,createdContact.Willing_to_Try__c);
        System.assertEquals(NULL,createdContact.Continue_to_use__c);
        
        
        //Test Case 4: Update Task 2 - OBCallList changed to NULL
        task2.Subject = 'Inbound Call';
        task2.OB_Call_List__c = NULL;
        update task2;
        
        createdContact = [SELECT ID,Finished_Calls__c,Willing_to_Try__c,Continue_to_use__c 
                          FROM CONTACT WHERE ID = :contact.ID];
        System.assertEquals(NULL,createdContact.Finished_Calls__c);
        System.assertEquals(NULL,createdContact.Willing_to_Try__c);
        System.assertEquals(NULL,createdContact.Continue_to_use__c);
        
        
        //Test Case 5: Update Task 2 - OBCallList changed from NULL to Not NULL
        task2.Subject = 'Outbound Call';
        task2.OB_Call_List__c = 'NT SS Follow-up';
        update task2;
        
        createdContact = [SELECT ID,Finished_Calls__c,Willing_to_Try__c,Continue_to_use__c 
                          FROM CONTACT WHERE ID = :contact.ID];
        System.assertEquals('NT SS Follow-up--Open',createdContact.Finished_Calls__c);
        System.assertEquals(NULL,createdContact.Willing_to_Try__c);
        System.assertEquals(NULL,createdContact.Continue_to_use__c);
        
        
        //Test Case 6: Update Task 2 - Change Status
        task2.Status = 'Invalid Status';
        update task2;
        
        createdContact = [SELECT ID,Finished_Calls__c,Willing_to_Try__c,Continue_to_use__c 
                          FROM CONTACT WHERE ID = :contact.ID];
        System.assertEquals('NT SS Follow-up--NA',createdContact.Finished_Calls__c);
        System.assertEquals(NULL,createdContact.Willing_to_Try__c);
        System.assertEquals(NULL,createdContact.Continue_to_use__c);
        
       //Test Case 7: Update Task 2 - Change Contact
        
        //Creat test baby 2
        Contact contact2 = New Contact();
        contact2.FirstName='Two';
        contact2.LastName='Baby';
        contact2.AccountId=account.ID;
        contact2.RecordTypeId = typeHouseHoldChild.Id;
        insert contact2;
        
        
        task2.WhoId = contact2.Id;
        update task2;
        
        createdContact = [SELECT ID,Finished_Calls__c,Willing_to_Try__c,Continue_to_use__c 
                          FROM CONTACT WHERE ID = :contact.ID];
        System.assertEquals(NULL,createdContact.Finished_Calls__c);
        System.assertEquals(NULL,createdContact.Willing_to_Try__c);
        System.assertEquals(NULL,createdContact.Continue_to_use__c);
        
        Contact createdContact2 = [SELECT ID,Finished_Calls__c,Willing_to_Try__c,Continue_to_use__c 
                          FROM CONTACT WHERE ID = :contact2.ID];
        System.assertEquals('NT SS Follow-up--NA',createdContact2.Finished_Calls__c);
        System.assertEquals(NULL,createdContact2.Willing_to_Try__c);
        System.assertEquals(NULL,createdContact2.Continue_to_use__c);
        
        
        //Test Case 8: insert new Task 3 with same OBCallList but Status - Closed
        Task task3 = new Task();
        task3.WhoId = contact2.ID;
        task3.whatId = account.ID;
        task3.Subject = 'Outbound Call';
        task3.Status = 'Connected/Closed';
        task3.OB_Call_List__c = 'NT SS Follow-up';
        task3.Where_did_you_heard_from_our_product__c ='others';
        task3.Continue_to_use__c='No';
        task3.Reason_not_continue_to_use__c='Others';
        insert task3;
        test.stopTest();
        createdContact2 = [SELECT ID,Finished_Calls__c,Willing_to_Try__c,Continue_to_use__c 
                          FROM CONTACT WHERE ID = :contact2.ID];
        System.assertEquals('NT SS Follow-up--Closed',createdContact2.Finished_Calls__c);
        System.assertEquals(NULL,createdContact2.Willing_to_Try__c);
        System.assertEquals('NT SS Follow-up--No',createdContact2.Continue_to_use__c);
    	 }
    }
}