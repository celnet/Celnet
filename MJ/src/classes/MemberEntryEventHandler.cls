/*
Author: scott.zhang@celnet.com.cn 
Created On: 2014-04-16
Function: Demonstrate how to use trigger handler pattern
Support Event: BeforeInsert,BeforeUpdate, BeforeDelete, AfterUpdate
Apply To: CN 
*/
public class MemberEntryEventHandler implements Triggers.Handler 
{
	public void Handle()
	{
		if(!Context.ApplyTo(new String[] {'CN'})) 
    	{
    		return;
		}
		if(trigger.isAfter && trigger.isUpdate)
		{
			if(trigger.new[0] instanceof Account)
			{
				this.updateFromAccount();
			}
			else if(trigger.new[0] instanceof Contact)
			{
				this.updateFromContact();
			}
			else if(trigger.new[0] instanceof Recruitment_Channel__c)
			{
				this.updateFromRC();
			}
		}
		if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate))
		{
			if(trigger.new[0] instanceof Member_Entry__c)
			{
				this.modifyDateValue();
				this.syncValue();
			}
		}
	}
	
	private void modifyDateValue()
	{
		String userName = Userinfo.getName();
		if(!userName.contains('BF System'))
		{
			system.debug('---------------------1-------------------------');
			return;
		}
		List<Member_Entry__c> list_ME_New = trigger.new;
		Boolean isUpdateME = trigger.isUpdate;
		for(Member_Entry__c me :list_ME_New)
		{
			if(isUpdateME)
			{
				system.debug('---------------------2------------------------');
				Member_Entry__c oldME = (Member_Entry__c)trigger.oldMap.get(me.Id);
				if(oldME.Baby1_Birthday__c == me.Baby1_Birthday__c 
				&& oldME.Baby2_Birthday__c == me.Baby2_Birthday__c 
				&& oldME.Baby_Birthday__c == me.Baby_Birthday__c 
				&& oldME.Baby3_Birthday__c == me.Baby3_Birthday__c 
				&& oldME.Register_Date__c == me.Register_Date__c)
				{
					continue;
				}
			}
			if(me.Baby_Birthday__c != null)
			{
				me.Baby_Birthday__c = Date.valueOf(me.Baby_Birthday__c).addDays(1);
			}
			if(me.Baby1_Birthday__c != null)
			{
				me.Baby1_Birthday__c = Date.valueOf(me.Baby1_Birthday__c).addDays(1);
			}
			if(me.Baby2_Birthday__c != null)
			{
				me.Baby2_Birthday__c = Date.valueOf(me.Baby2_Birthday__c).addDays(1);
			}
			if(me.Baby3_Birthday__c != null)
			{
				me.Baby3_Birthday__c = Date.valueOf(me.Baby3_Birthday__c).addDays(1);
			}
			if(me.Register_Date__c != null)
			{
				me.Register_Date__c = Date.valueOf(me.Register_Date__c).addDays(1);
			}
		}
		
	} 
	
	private void updateFromAccount()
	{
		List<Account> accList = trigger.new;
		//Define map: Key:Member_Entry__Id    value:Account
		Map<Id,Account> Map_MemberEntry = new Map<Id,Account>();
		Id RecordTypeId = ObjectUtil.GetRecordTypeID(Account.getSObjectType(), 'CN_HouseHold');
		for(Account newAcc : accList)
		{
			Account oldAcc = (Account)trigger.oldMap.get(newAcc.Id); 
			if(newAcc.Member_Entry__c == null || newAcc.RecordTypeId != RecordTypeId)
			{
				continue;
			}
			if(newAcc.phone != oldAcc.phone || newAcc.name != oldAcc.name || newAcc.PersonEmail != oldAcc.PersonEmail || newAcc.status__c != oldAcc.status__c || newAcc.Birthdate__c != oldAcc.Birthdate__c || newAcc.Standard_City__c != oldAcc.Standard_City__c || newAcc.Address_Street__c != oldAcc.Address_Street__c || newAcc.Other_Phone_1__c != oldAcc.Other_Phone_1__c || newAcc.Other_Phone_2__c != oldAcc.Other_Phone_2__c )
			{
				Map_MemberEntry.put(newAcc.Member_Entry__c,newAcc);
			} 
			
		}
		if(Map_MemberEntry.size()==0)
		{
			return;
		}
		//Update Member_Entry__c List
		List<Member_Entry__c> List_UpMemberEntry = new List<Member_Entry__c>();
		for(Member_Entry__c member : [select Id,I_Status__c,Phone__c,Name,Email__c,Status__c,Administrative_Area__c,Address_Street__c,Other1__c,Other2__c from Member_Entry__c where Id in:Map_MemberEntry.keySet()])
		{
			member.I_Status__c = 'Update';
			Account Acc = Map_MemberEntry.get(member.Id);
			member.Phone__c = Acc.phone;
			member.Name = Acc.name;
			member.Email__c = Acc.PersonEmail;
			member.Status__c = Acc.status__c;
			member.Administrative_Area__c = Acc.Standard_City__c;
			member.Address_Street__c = Acc.Address_Street__c;
			member.Other1__c = Acc.Other_Phone_1__c;
			member.Other2__c = Acc.Other_Phone_2__c;
			List_UpMemberEntry.add(member);
		}
		update List_UpMemberEntry;
		
	}
	
	private void updateFromContact()
	{
		List<Contact> conList = trigger.new;
		
		//Define map: Key:AccountId    value:Account
		Map<Id,Contact> Map_AccIdContact = new Map<Id,Contact>();
		Id RecordTypeId = ObjectUtil.GetRecordTypeID(Contact.getSObjectType(), 'CN_Child');
		for(Contact newCon : conList)
		{
			Contact oldCon = (Contact)trigger.oldMap.get(newCon.Id);
			if(newCon.RecordTypeId != RecordTypeId || newCon.AccountId == null)
			{
				continue; 
			}
			if(newCon.DoNotCall != oldCon.DoNotCall || newCon.Do_Not_Invite__c != oldCon.Do_Not_Invite__c || newCon.Do_Not_SMS__c != oldCon.Do_Not_SMS__c || newCon.Inactive__c != oldCon.Inactive__c || newCon.Inactive_Reason__c != oldCon.Inactive_Reason__c || 
			   newCon.Is_Winning__c != oldCon.Is_Winning__c || newCon.Specific_Code__c != oldCon.Specific_Code__c || newCon.Verified__c != oldCon.Verified__c || newCon.Last_Routine_Call_Time__c != oldCon.Last_Routine_Call_Time__c || 
			   newCon.Last_Routine_Call_Result__c != oldCon.Last_Routine_Call_Result__c || newCon.Archive_Month__c != oldCon.Archive_Month__c ||
			   newCon.Birthdate != oldCon.Birthdate)
			{
				Map_AccIdContact.put(newCon.AccountId,newCon);
			}
		}
		if(Map_AccIdContact.size()==0)
		{ 
			return;
		}
		//Define map: Key:Member_Entry__Id    value:Contact
		Map<Id,Contact> Map_MemberEntry = new Map<Id,Contact>();
		for(Account acc : [select Id,Member_Entry__c from Account where Id in:Map_AccIdContact.keySet() and Member_Entry__c != null])
		{
			Map_MemberEntry.put(acc.Member_Entry__c,Map_AccIdContact.get(acc.Id));
		}
		if(Map_MemberEntry.size()==0)
		{
			return;
		}
		//Update Member_Entry__c List 
		List<Member_Entry__c> List_UpMemberEntry = new List<Member_Entry__c>();
		for(Member_Entry__c member : [select Id,I_Status__c,Register_Date__c,Is_Win__c,Specific_Code__c,Is_Verified__c,Hospital__c,Last_Call_Time__c,Last_Call_Result__c,Archive_Month__c, 
									  DoNotCall__c,Do_Not_Invite__c,Do_Not_SMS__c,Inactive__c,Inactive_Reason__c,Baby1_Birthday__c 
									  from Member_Entry__c where Id in:Map_MemberEntry.keySet()])
		{
			member.I_Status__c = 'Update';
			Contact Con = Map_MemberEntry.get(member.Id);
			member.Baby1_Birthday__c = Con.Birthdate;
			//member.Register_Date__c = Con.Register_Date__c;
			member.Is_Win__c = Con.Is_Winning__c;
			member.Specific_Code__c = Con.Specific_Code__c;
			member.Is_Verified__c = Con.Verified__c;
			//member.Hospital__c = Con.Born_Hospital__c;
			member.Last_Call_Time__c = Con.Last_Routine_Call_Time__c;
			member.Last_Call_Result__c = Con.Last_Routine_Call_Result__c;
			member.Archive_Month__c = Con.Archive_Month__c;
			
			member.DoNotCall__c = Con.DoNotCall;
			member.Do_Not_Invite__c = Con.Do_Not_Invite__c;
			member.Do_Not_SMS__c = Con.Do_Not_SMS__c;
			member.Inactive__c = Con.Inactive__c;
			member.Inactive_Reason__c = Con.Inactive_Reason__c;
			
			List_UpMemberEntry.add(member);
		}
		try
		{
			update List_UpMemberEntry;
		}catch(Exception e)
		{
		}
		
	}
	
	
	private void updateFromRC()
	{
		List<Recruitment_Channel__c> rcList = trigger.new;
		//Define map: Key:AccountId    value:Account
		Map<Id,Recruitment_Channel__c> Map_ConIdRecChannel = new Map<Id,Recruitment_Channel__c>();
		Id RecordTypeId = ObjectUtil.GetRecordTypeID(Recruitment_Channel__c.getSObjectType(), 'Recruitment_Channel_CN');
		for(Recruitment_Channel__c newRec : rcList)
		{
			Recruitment_Channel__c oldRec = (Recruitment_Channel__c)trigger.oldMap.get(newRec.Id);
			if(newRec.RecordTypeId != RecordTypeId || newRec.Contact__c == null)
			{
				continue;
			}
			if(newRec.Specific_Code__c !=oldRec.Specific_Code__c || newRec.Store__c != oldRec.Store__c || newRec.Sales_Rep__c != oldRec.Sales_Rep__c)
			{
				Map_ConIdRecChannel.put(newRec.Contact__c,newRec);
			}
		}
		if(Map_ConIdRecChannel.size()==0)
		{
			return;
		}
		//Define map: Key:Member_Entry__Id    value:Recruitment_Channel__c
		Map<Id,Recruitment_Channel__c> Map_MemberEntry = new Map<Id,Recruitment_Channel__c>();
		for(Contact con : [select Id,Account.Member_Entry__c from Contact where Id in:Map_ConIdRecChannel.keySet() and Account.Member_Entry__c != null])
		{
			Map_MemberEntry.put(con.Account.Member_Entry__c,Map_ConIdRecChannel.get(con.Id));
		}
		if(Map_MemberEntry.size()==0)
		{
			return;
		}
		//Update Member_Entry__c List
		List<Member_Entry__c> List_UpMemberEntry = new List<Member_Entry__c>();
		for(Member_Entry__c member : [select Id,I_Status__c,Specific_Code__c,Store__c,Sales_Rep__c from Member_Entry__c where Id in:Map_MemberEntry.keySet()])
		{
			member.I_Status__c = 'Update';
			Recruitment_Channel__c RecChannel = Map_MemberEntry.get(member.Id);
			member.Specific_Code__c = RecChannel.Specific_Code__c;
			member.Store__c = RecChannel.Store__c;
			member.Sales_Rep__c = RecChannel.Sales_Rep__c;
			List_UpMemberEntry.add(member);
		}
		update List_UpMemberEntry;
		
	}
	
	private void syncValue()
	{
		List<Member_Entry__c> MemberEntryList = trigger.new;
		
		//Status Mapping
		Map<String,String> Map_Status = new Map<String,String>();
		for(BF_Status_Mapping__c Status : BF_Status_Mapping__c.getAll().values())
		{
			Map_Status.put(Status.SFDC_Label__c,Status.BF_Value__c);
			Map_Status.put(Status.BF_Value__c,Status.SFDC_Label__c);
		}
		//Register Source Mapping
		Map<String,String> Map_Source = new Map<String,String>();
		Map<String,String> Map_SubSource = new Map<String,String>();
		for(BF_Register_Source_Mapping__c RS : BF_Register_Source_Mapping__c.getAll().values())
		{
			Map_Source.put(RS.Register_Source_Label__c,RS.BF_Register_Source_Value__c);
			Map_Source.put(RS.BF_Register_Source_Value__c,RS.Register_Source_Label__c);
			Map_SubSource.put(RS.BF_Register_Sub_Source_Value__c,RS.Register_Sub_Source_Label__c);
			Map_SubSource.put(RS.Register_Sub_Source_Label__c,RS.BF_Register_Sub_Source_Value__c);
		}
		//Brand Mapping
		Map<String,String> Map_MotherBrand = new Map<String,String>();
		Map<String,String> Map_BabyBrand = new Map<String,String>();
		for(BF_Brand_Mapping__c brand : BF_Brand_Mapping__c.getAll().values())
		{
			if(brand.For__c =='Mother')
			Map_MotherBrand.put(brand.SFDC_Label__c,brand.BF_Value__c);
			if(brand.For__c =='Baby')
			Map_BabyBrand.put(brand.SFDC_Label__c,brand.BF_Value__c);
		} 
		
		//Area
		Set<Id> Set_AreaId = new Set<Id>();
		Set<String> Set_CityValue = new Set<String>();
		
		//Specific Code
		Set<String> Set_Code = new Set<String>();
		for(Member_Entry__c member : MemberEntryList)
		{
			Boolean IsInsert =(trigger.isInsert?true:false);
			Boolean IsUpdate =(trigger.isUpdate?true:false);
			Member_Entry__c oldmember = (IsUpdate?(Member_Entry__c)trigger.oldMap.get(member.Id):new Member_Entry__c());
		
			//Archive_Month__c
			if(IsInsert && member.Archive_Month__c == null)
			{
				Date YesterdayDate = Date.Today().addDays(-1);
				String year = String.valueOf(YesterdayDate.Year());
				String month = (YesterdayDate.Month()>=10?String.valueof(YesterdayDate.Month()):'0'+String.valueof(YesterdayDate.Month()));
				member.Archive_Month__c = year+month;
			}
			//Status Mapping
			if((IsInsert && member.Status__c != null) || (IsUpdate && member.Status__c != oldmember.Status__c))
			{
				member.BF_Status_Value__c = (Map_Status.containsKey(member.Status__c)?Map_Status.get(member.Status__c):null);
			}
			else if((IsInsert && member.BF_Status_Value__c != null) || (IsUpdate && member.BF_Status_Value__c != oldmember.BF_Status_Value__c) )
			{
				member.Status__c = (Map_Status.containsKey(member.BF_Status_Value__c)?Map_Status.get(member.BF_Status_Value__c):null);
			}
			//Register Source Mapping
			if((IsInsert && member.Register_Source__c != null) || (IsUpdate && member.Register_Source__c != oldmember.Register_Source__c) )
			{
				member.BF_Register_Source_Value__c = (Map_Source.containsKey(member.Register_Source__c)?Map_Source.get(member.Register_Source__c):null);
			}
			else if((IsInsert && member.BF_Register_Source_Value__c != null) || (IsUpdate && member.BF_Register_Source_Value__c != oldmember.BF_Register_Source_Value__c) )
			{
				member.Register_Source__c = (Map_Source.containsKey(member.BF_Register_Source_Value__c)?Map_Source.get(member.BF_Register_Source_Value__c):null);
			}
			if((IsInsert && member.Register_Sub_Source__c != null) || (IsUpdate && member.Register_Sub_Source__c != oldmember.Register_Sub_Source__c) )
			{
				member.BF_Register_Sub_Source_Value__c = (Map_SubSource.containsKey(member.Register_Sub_Source__c)?Map_SubSource.get(member.Register_Sub_Source__c):null);
			}
			else if((IsInsert && member.BF_Register_Sub_Source_Value__c != null) || (IsUpdate && member.BF_Register_Sub_Source_Value__c != oldmember.BF_Register_Sub_Source_Value__c) )
			{
				member.Register_Sub_Source__c = (Map_SubSource.containsKey(member.BF_Register_Sub_Source_Value__c)?Map_SubSource.get(member.BF_Register_Sub_Source_Value__c):null);
			}
			//Brand Mapping
			if((IsInsert && member.Q_Using_Mother_Milk_Powder__c != null) || (IsUpdate && member.Q_Using_Mother_Milk_Powder__c != oldmember.Q_Using_Mother_Milk_Powder__c) )
			{
				member.BF_Q_Using_Mother_Milk_Powder_Value__c = (Map_MotherBrand.containsKey(member.Q_Using_Mother_Milk_Powder__c)?Map_MotherBrand.get(member.Q_Using_Mother_Milk_Powder__c):null);
			}
			if((IsInsert && member.Q_Wish_Using_Baby_Milk_Powder__c !=null) || (IsUpdate && member.Q_Wish_Using_Baby_Milk_Powder__c != oldmember.Q_Wish_Using_Baby_Milk_Powder__c) )
			{
				member.BF_Q_Wish_Using_Baby_Milk_Powder_Value__c = (Map_BabyBrand.containsKey(member.Q_Wish_Using_Baby_Milk_Powder__c)?Map_BabyBrand.get(member.Q_Wish_Using_Baby_Milk_Powder__c):null);
			}
			//Area Mapping
			if((IsInsert && member.Administrative_Area__c != null) || (IsUpdate && member.Administrative_Area__c != oldmember.Administrative_Area__c))
			{
				member.BF_Area_Value__c = null;
				member.BF_City_Value__c = null;
				member.BF_Province_Value__c = null;
				Set_AreaId.add(member.Administrative_Area__c);
			}
			else if((IsInsert && member.BF_City_Value__c != null) || (IsUpdate && member.BF_City_Value__c != oldmember.BF_City_Value__c && member.BF_City_Value__c != null))
			{
				//member.BF_Area_Value__c = null;
				//member.BF_Province_Value__c = null;
				//member.Administrative_Area__c = null;
				Set_CityValue.add(member.BF_City_Value__c);
			}
			//Specific Code
			if((IsInsert && member.Specific_Code__c != null) || (IsUpdate && member.Specific_Code__c != oldmember.Specific_Code__c) )
			{
				member.Hospital__c = null;
				member.Store__c = null;
				Set_Code.add(member.Specific_Code__c);
			}
		}
		//Area 
		if(Set_AreaId.size()>0)
		{
			for(Address_Management__c AddMan : [select Id,BF_Area_Value__c,BF_City_Value__c,BF_Province_Value__c from Address_Management__c where id in:Set_AreaId])
			{
				for(Member_Entry__c member : MemberEntryList)
				{
					if(member.Administrative_Area__c == AddMan.Id)
					{
						member.BF_Area_Value__c = AddMan.BF_Area_Value__c;
						member.BF_City_Value__c = AddMan.BF_City_Value__c;
						member.BF_Province_Value__c = AddMan.BF_Province_Value__c;
					}
				}
			}
		}
		if(Set_CityValue.size()>0)
		{
			Map<String,List<Address_Management__c>> Map_AddMang = new Map<String,List<Address_Management__c>>();
			Map<String,Address_Management__c> Map_Other = new Map<String,Address_Management__c>();
			for(Address_Management__c AddMan : [select Administrative_Area__c,Id,BF_Area_Value__c,BF_City_Value__c,BF_Province_Value__c from Address_Management__c where BF_City_Value__c in:Set_CityValue])
			{
				if(Map_AddMang.containsKey(AddMan.BF_City_Value__c))
				{
					List<Address_Management__c> List_AddMan = Map_AddMang.get(AddMan.BF_City_Value__c);
					List_AddMan.add(AddMan);
					Map_AddMang.put(AddMan.BF_City_Value__c,List_AddMan);
				}
				else
				{
					List<Address_Management__c> List_AddMan = new List<Address_Management__c>();
					List_AddMan.add(AddMan);
					Map_AddMang.put(AddMan.BF_City_Value__c,List_AddMan);
				}
				
				if(!Map_Other.containsKey(AddMan.BF_City_Value__c) && AddMan.Administrative_Area__c=='其他')
				{
					Map_Other.put(AddMan.BF_City_Value__c,AddMan);
				}
			}
			for(Member_Entry__c member : MemberEntryList)
			{
				Boolean IsInsert =(trigger.isInsert?true:false);
				Boolean IsUpdate =(trigger.isUpdate?true:false);
				Member_Entry__c oldmember = (IsUpdate?(Member_Entry__c)trigger.oldMap.get(member.Id):new Member_Entry__c());
				if((IsInsert && member.BF_City_Value__c != null) || (IsUpdate && member.BF_City_Value__c != oldmember.BF_City_Value__c))
				{
					Boolean flag = true;
					if(Map_AddMang.containsKey(member.BF_City_Value__c))
					{
						for(Address_Management__c am:Map_AddMang.get(member.BF_City_Value__c))
						{
							if(member.BF_Area_Value__c == am.BF_Area_Value__c)
							{
								member.Administrative_Area__c = am.Id;
								member.BF_Area_Value__c = am.BF_Area_Value__c;
								member.BF_Province_Value__c = am.BF_Province_Value__c;
								flag = false;
								break;
							}
						}
					}
					
					if(flag && Map_Other.containsKey(member.BF_City_Value__c))
					{
						//member.addError('Match Sales City Error.');
						//continue;
						Address_Management__c am = Map_Other.get(member.BF_City_Value__c);
						member.Administrative_Area__c = am.Id;
						member.BF_Area_Value__c = am.BF_Area_Value__c;
						member.BF_Province_Value__c = am.BF_Province_Value__c;
					}
				}
			}
		}
		//Specific Code
		if(Set_Code.size()>0)
		{
			for(Account acc: [ select Id,Specific_Code__c,RecordType.DeveloperName,SalesReps__r.SALES_REP_NAME_EN__c from Account where (RecordType.DeveloperName='CN_Hospital' or RecordType.DeveloperName='CN_Store') and Specific_Code__c in:Set_Code])
			{
				for(Member_Entry__c member : MemberEntryList)
				{
					if(member.Specific_Code__c == acc.Specific_Code__c)
					{
						member.Hospital__c = (acc.RecordType.DeveloperName=='CN_Hospital'?acc.Id:null);
						if(acc.SalesReps__r.SALES_REP_NAME_EN__c != null)
						member.Sales_Rep__c = acc.SalesReps__r.SALES_REP_NAME_EN__c;
						member.Store__c=(acc.RecordType.DeveloperName=='CN_Store'?acc.Id:null);
					}
				}
			}
		}
	}
	
	
	
}