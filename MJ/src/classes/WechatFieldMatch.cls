/*
*Author:mark.li@celnet.com.cn
*Date:2014-09-10
*Function:Field Match
*/
public class WechatFieldMatch 
{
    public static attachment GenerateAttachment(WechatEntity.File file)
    {
        attachment att = new attachment();
        att.Body = file.body;
        att.ContentType = file.contentType;
        att.Description = file.description;
        return att;
    }
    
    public static Wechat_User__c GenerateWechatUser(WechatEntity.User user)
    {
        Wechat_User__c wu = new Wechat_User__c();
        wu.Subscribe__c = (user.subscribe==0 ? false : true);
        wu.Sex__c = (user.sex==1 ? 'Male' : 'Female');
        wu.Nickname__c = user.nickname;
        wu.Name = user.nickname;
        wu.Language__c = user.language;
        wu.City__c = user.city;
        wu.Province__c = user.province;
        wu.Country__c = user.country;
        wu.Head_Image_Url__c = user.headimgurl;
        wu.Union_Id__c = user.unionid;
        wu.Open_Id__c = user.openid;
        datetime timeBegin = DateTime.newInstanceGMT(1970,1,1,0,0,0);
        integer duration = Integer.valueOf(user.subscribe_time);
        wu.Subscribe_Time__c =  timeBegin.addSeconds(duration);
        return wu;
    }
}