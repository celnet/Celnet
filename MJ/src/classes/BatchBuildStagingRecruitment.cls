/*
Batch job to build staging object: Recruitment_Staging__c
for Report 2.2 《会员招募统计》 
指定代码 代码名称(这两个字段一一对应)
key: specific code,   no, day, month, year
author: shiqi.ng@sg.fujitsu.com 
*/

global class BatchBuildStagingRecruitment  implements Database.Batchable<AggregateResult>{
    
    private String specificCodeQuery;
    private Integer reportingDay;
    private Integer reportingMonth;
    private Integer reportingYear;
    
    public  BatchBuildStagingRecruitment(String query,Integer year, Integer month, Integer day){
        specificCodeQuery = query;
        reportingDay = day;
        reportingMonth = month;
        reportingYear = year;
    }
    
    /*Start Method*/
    global Iterable<AggregateResult> start(Database.BatchableContext bc){
        return new AggregateResultIterable(specificCodeQuery);
        //return Database.getQueryLocator(specificCodeQuery);
    }
    
    /*Execute*/
    global void execute(Database.BatchableContext bc, List<SObject> scope){
        //Recruitment_Staging__c
        
        //Put Specific Code into a set
        Set<String> specificCodeSet = new Set<String>();
        for (SObject obj : scope){
            AggregateResult ar = (AggregateResult) obj;
            String specificCode = (String)ar.get('Specific_Code__c');
            specificCodeSet.add(specificCode);
            System.debug('Add Specific Code in Scope:'+specificCode);
        }
        
        //Query Contacts created today based on Specific Code
        Map<String,Map<Id,Contact>> specificCodeContacts =
            StagingReportUtil.queryContactsCreatedTodayBySpecificCode(
                specificCodeSet,reportingYear,reportingMonth,reportingDay);

        //Retrieve existing rows
        List<Recruitment_Staging__c> rows = new List<Recruitment_Staging__c>();
            //StagingReportSalesHelper.queryRecruitmentStaging(specificCodeSet,reportingYear,reportingMonth,reportingDay);
      
        /*Comment by SQ */ //List<Recruitment_Staging__c> List_rows = new List<Recruitment_Staging__c>();
        
        //for each specific code
        for (String thisSpecificCode : specificCodeContacts.keySet()){
            Map<Id,Contact> contacts = specificCodeContacts.get(thisSpecificCode);
            
            //get or create row
            Recruitment_Staging__c row = StagingReportUtil.getRecruitmentStaging(
            	rows,
        		thisSpecificCode,
                reportingYear,
                reportingMonth, 
                reportingDay
            );
            
            /* Comment by SQ
            Recruitment_Staging__c row = new Recruitment_Staging__c ();
            row.Reporting_Day__c = reportingDay;
            row.Reporting_Month__c = reportingMonth;
            row.Reporting_Year__c = reportingYear;
            row.Specific_Code__c = thisSpecificCode;
            StagingReportSalesHelper.resetRowRecruitment(row); 
            row.External_ID__c= String.valueOf(row.Reporting_Year__c)+String.valueOf(row.Reporting_Month__c)+String.valueOf(row.Reporting_Day__c)+row.Specific_Code__c;
                */
            
            
            //for each contact in RC, 
            for (Contact thisContact : contacts.values()){
               
                //update row
                row.Region__c = thisContact.Account.Standard_City__r.Region__c;
                row.Sub_Region__c = thisContact.Account.Standard_City__r.Sub_Region__c;
                row.Sales_City__c = thisContact.Account.Standard_City__r.Sales_City__c;
                row.Area__c = thisContact.Account.Standard_City__r.Area__c;
                row.Channel_Name__c = thisContact.Channel_Name__c;
                
                
               /* String recruitType = thisContact.Primary_RecruitChannel_Type__c;
                StagingReportUtil.setSurveyValues(
                            row, recruitType,true,
                            new String[]{'手工/纸质','互联网','短信'},
                            new String[]{'Actual_Recruited_no__c',
                                'Non_paper_recruited_no__c','SMS_Recruited_No'}
                        );
                boolean paperBased = (recruitType != null  && recruitType.contains('手工/纸质'));
                
				//身份-妈妈,...
                if (paperBased){
                    String householdStatus = thisContact.Account.status__c;
                    if (householdStatus == 'Mother'){
                        row.Status_Mother__c ++;
                    }else {
                        row.Status_Non_Mother__c ++;
                    }
                    if (householdStatus == 'Pregancy Mother'){
                        row.Status_Pregnancy_Mother__c ++;
                    }else if (householdStatus == 'Father'){
                        row.Status_Father__c ++;
                    }else if (householdStatus == 'Pregancy Father'){
                        row.Status_Pregnancy_Father__c ++;
                    }else if (householdStatus == 'Other'){
                        row.Status_Other__c ++;
                    }
                }*/
                
                boolean paperBased = false;                
                if(thisContact.Recruitment_Channels__r != null)
                {
                	for(Recruitment_Channel__c rc : thisContact.Recruitment_Channels__r)
                	{
                        //实际录入量 , 非纸质单录入数量 , SMS注册数
                        String recruitType = rc.CN_Channel__c;
                        StagingReportUtil.setSurveyValues(
                            row, recruitType,true,
                            new String[]{'手工/纸质','互联网','短信','短信'},
                            new String[]{
                                'Actual_Recruited_no__c',
                                'Non_paper_recruited_no__c', //Non paper = 互联网 or 短信
                                'SMS_Recruited_No__c',
                                'Non_paper_recruited_no__c'} //Non paper = 互联网 or 短信
                        ); 
                        paperBased = (recruitType != null  && recruitType.contains('手工/纸质'));
                	}
                }//end if thisContact.Recruitment_Channels__r != null
                
                
                String householdStatus = thisContact.Account.status__c;
                //身份-妈妈, 身份-非妈妈数
                if (householdStatus == 'Mother'){
                    row.Status_Mother__c ++;
                }else {
                    row.Status_Non_Mother__c ++;
                }
                //身份-准妈妈, 身份-爸爸, 身份-准爸爸, 身份-其他
                if (householdStatus == 'Pregancy Mother'){ 
                    row.Status_Pregnancy_Mother__c ++;
                }else if (householdStatus == 'Father'){
                    row.Status_Father__c ++;
                }else if (householdStatus == 'Pregancy Father'){
                    row.Status_Pregnancy_Father__c ++;
                }else if (householdStatus == 'Other'){
                    row.Status_Other__c ++;
                }
                //身份-未怀孕数
                if (thisContact.Inactive_Reason__c == 'No Baby'){
                    row.Status_Not_pregnant__c ++;
                }
                    
                //未拨打数 - see below
               /* for (AggregateResult result :numOfTasksByContacts){
                    if (result.get(thisContact.Id) == null ){
                        row.Not_called__c ++;
                    }    
                }*/
                //未拨打数
                if(thisContact.Tasks == null)
                {
                	row.Not_called__c ++;
                }
                
                 if ( thisContact.Birthdate != null)
                 {
                 	Integer ageInDays = thisContact.Birthdate.daysBetween(Date.Today());
                 	//S0数量,非S0(0-30天),非S0(31-60天),非S0(61-180天),非S0(181天以上)
                    //废卡 ,无法接通, 非准妈妈, 致电成功
                    if(ageInDays<0){
                    	row.No_of_S0__c ++;
                    }else if (ageInDays > 0 && ageInDays <= 30)
                    {
                        row.Non_S0_0_30_days__c ++;
                    }else if (ageInDays >= 31 && ageInDays <= 60){
                        row.Non_S0_31_60_days__c ++;
                    }else if (ageInDays >= 61 && ageInDays <= 180){
                        row.Non_S0_61_180_days__c ++;
                    }else if (ageInDays >180){
                        row.Non_S0_181_days__c ++;
                    } 
                 }                
                
                /* 暂时不做 3 July 2014
                //Birth day, last routine call result
                if ( thisContact.Birthdate != null){
                    Integer ageInDays = StagingReportUtil.getAgeInDays(
                        thisContact.CreatedDate,
                        thisContact.Birthdate);
                    String lastCallResult = thisContact.Last_Routine_Call_Result__c;
                    
                    //boolean approvedMom = (householdStatus == 'Mother');
                    
                    //S0数量,非S0(0-30天),非S0(31-60天),非S0(61-180天),非S0(181天以上)
                    //废卡 ,无法接通, 非准妈妈, 致电成功
                    if (ageInDays >= 0 && ageInDays >= 30){
                        row.Non_S0_0_30_days__c ++;
                        if (ageInDays == 0){
                             row.No_of_S0__c ++;
                        }
                        if (lastCallResult =='inactive'){
                            row.Invalid__c ++;
                        }else if (lastCallResult=='no response'){
                            row.Not_connected__c ++;
                        }else if (lastCallResult=='success connect'){
                            row.Connected_Calls__c ++;
                        }
                       if (approvedMom){
                           row.Non_Mom_Household__c++;
                       }
                    }else if (ageInDays >= 31 && ageInDays >= 60){
                        row.Non_S0_31_60_days__c ++;
                        if (lastCallResult =='inactive'){
                            row.Invalid_31_60_days__c ++;
                        }else if (lastCallResult=='no response'){
                            row.Not_Connected_31_60_days__c ++;
                        }else if (lastCallResult=='success connect'){
                            row.Connected_31_60_days__c ++;
                        }
                        //if (approvedMom){
                        //    row.Not_Approved_31_60_days__c++;
                        //}
                    }else if (ageInDays >= 61 && ageInDays >= 180){
                        row.Non_S0_61_180_days__c ++;
                        if (lastCallResult =='inactive'){
                            row.Invalid_Card_61_180_days__c ++;
                        }else if (lastCallResult=='no response'){
                            row.Not_Connected_61_180_days__c ++;
                        }else if (lastCallResult=='success connect'){
                            row.Connected_61_180_days__c ++;
                        }
                        f (approvedMom){
                           row.Not_Approved_61_180_days__c++;
                        }
                    }else if (ageInDays >180){
                        row.Non_S0_181_days__c ++;
                    }                   
                }//end if birthday != null
				end comment 暂时不做*/
                
            }//end for each contact
 //           List_rows.add(row);
        }//end for each specific code        
        
        
    	try{
           upsert rows External_ID__c;
           //upsert List_rows External_ID__c;
        }catch (Exception ex){
            system.debug(system.LoggingLevel.ERROR, ex.getMessage());
            system.debug(system.LoggingLevel.ERROR, ex.getStackTraceString());
            throw ex;
        }	
        
    }
    
    /*Finish*/
    global void finish(Database.BatchableContext bc){}

}