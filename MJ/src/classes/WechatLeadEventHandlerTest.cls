/*Author:leo.bi@celnet.com.cn
 *Date:2014-09-25
 *Function:Test WechatLeadEventHandler
 */
@isTest
private class WechatLeadEventHandlerTest 
{
    static testMethod void TestWechatLeadEventHandler() 
    {
        system.runAs(TestUtility.new_HK_User())
        {
            //Create custom settings
            TestUtility.setupMemberIDCustomSettings();
            Id leadRecordType = [select id from RecordType where DeveloperName=:'LeadsHK' and SobjectType=:'Lead'].id;
            //test CountLeadBabyNumber
            Lead lea = new Lead();
            lea.LastName = 'PARENT';
            lea.FirstName = 'TEST';
            lea.RecordTypeId = leadRecordType;
            insert lea;
            lea.FirstName = 'TEST1';
            update lea;
            lea.First_Name_1st_Child__c = 'BABY'; 
            lea.Last_Name_1st_Child__c = 'TEST';
            lea.Birthday_1st_Child__c = system.today();
            lea.First_Name_2nd_Child__c = 'BABY';
            lea.Last_Name_2nd_Child__c = 'TEST';
            lea.Birthday_2nd_Child__c = system.today();
            lea.First_Name_3rd_Child__c = 'BABY';
            lea.Last_Name_3rd_Child__c = 'TEST';
            lea.Birthday_3rd_Child__c = system.today();
            lea.First_Name_4th_Child__c = 'BABY';
            lea.Last_Name_4th_Child__c = 'TEST';
            lea.Birthday_4th_Child__c = system.today();
            update lea;
            //test CountLeadBirthCertNumber
            Attachment att = new Attachment();
            att.ParentId = lea.Id;
            att.ContentType = 'image/jpeg';
            att.Name = 'Test Att Name';
            att.Body = Blob.valueOf('Test Att Body');
            insert att;
            delete att;
        }
    }
}