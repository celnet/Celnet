/*
Author: leo.bi@celnet.com.cn
Created On: 2014-04-22
Function: Sync Answer to Answer Contact MemberEntry
Support Event:  AfterInsert, AfterUpdate, BeforeInsert, BeforeUpdate
Apply To: CN
*/
public class AnswerStatusSyncHandler implements Triggers.Handler {
	public void Handle()
	{
		if(!Context.ApplyTo(new String[] {'CN'})) 
    	{
    		return;
		}
		
		if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate))
		{
			if(trigger.new[0] instanceof Answer__c)
			{
				//this.syncStatusFromLastAnswerBeforeInsert();
				syncAnswerStatus();
				syncMemberEntry();
			}
		}
		if(trigger.isAfter && (trigger.isUpdate || trigger.isInsert))
		{
			if(trigger.new[0] instanceof Answer__c)
			{
				syncStatusToContactAfter();
			}
		}
	}
	
	public void syncAnswerStatus()
	{
		List<Answer__c> list_Ans = trigger.new;
		List<Answer__c> list_AnswerNew = new List<Answer__c>();
		Map<Id,List<Answer__c>> map_ContactId_AnswerNew = new Map<Id,List<Answer__c>>();
		Map<Id,List<Answer__c>> map_ContactId_AnswerHistory = new Map<Id,List<Answer__c>>();
		Map<Id,Set<Id>> map_ContactId_Task = new Map<Id,Set<Id>>();
		Set<Id> set_QuesId = new Set<Id>();
		//ignore no contact answer and unfinished answer
		for(Answer__c ans : list_Ans)
		{
			if(ans.Contact__c == null || ans.Status__c != 'Finish' || ans.Finished_On__c == null)
			{
				continue;
			}
			/*Author:leo.bi@celnet.com.cn
			 *Alter Date:2014-06-29
			 *Function:sync q011 to q011_text
			 */
			ans.Q011_Text__c = ans.Q011__c;
			ans.q017_Text__c = ans.Q017__c;
			ans.Q011_Last_Text__c = ans.Q011_Last__c;
			list_AnswerNew.add(ans);
			List<Answer__c> list_temp = new List<Answer__c>();
			if(map_ContactId_AnswerNew.containsKey(ans.Contact__c))
			{
				list_temp = map_ContactId_AnswerNew.get(ans.Contact__c);
				list_temp.add(ans);
			}
			else
			{
				list_temp.add(ans);
				map_ContactId_AnswerNew.put(ans.Contact__c,list_temp);
			}
		}
		if(map_ContactId_AnswerNew.keySet().isEmpty())
		{
			return;
		}
		//get the history record which have the same contact with trigger.new
		List<Answer__c> list_Answer_History = [select CreatedBy.Id, Contact__c, Finished_On__c, Questionnaire__r.Next_Routine_Questionnaire__c, Status__c, Q008__c, Q011__c, Q030__c , Q045__c, Q046__c, Q056__c
										       from Answer__c 
										       where Contact__c != null 
										       and Contact__c in :map_ContactId_AnswerNew.keySet()
										       order by Finished_On__c desc];
		for(Answer__c ans : list_Answer_History)
		{
			if(ans.Status__c !='Finish' || ans.Finished_On__c == null)
			{
				continue;
			}
			List<Answer__c> list_temp = new List<Answer__c>();
			if(map_ContactId_AnswerHistory.containsKey(ans.Contact__c))
			{
				list_temp = map_ContactId_AnswerHistory.get(ans.Contact__c);
				list_temp.add(ans);
			}
			else
			{
				list_temp.add(ans);
				map_ContactId_AnswerHistory.put(ans.Contact__c,list_temp);
			}
		}
		//get questionnaire id set
		List<Questionnaire__c> list_questionnaire = [select Id from Questionnaire__c];
		for(Questionnaire__c que : list_questionnaire)
		{
			set_QuesId.add(que.Id);
		}
		//get the tasks which has the same look up  contact with answer
		List<Task> list_Task = [Select WhoId, WhatId, Id From Task where WhoId in :map_ContactId_AnswerNew.keySet() and WhatId in :set_QuesId];
		for(Task t : list_Task)
		{
			Set<Id> set_Temp = new Set<Id>();
			if(map_ContactId_Task.containsKey(t.WhoId))
			{
				set_Temp = map_ContactId_Task.get(t.WhoId);
				set_Temp.add(t.WhatId);
			}
			else
			{
				set_Temp.add(t.WhatId);
				map_ContactId_Task.put(t.WhoId,set_Temp);
			}
		}
		//sync status
		for(Answer__c ans : list_AnswerNew)
		{
			if(!map_ContactId_AnswerHistory.containsKey(ans.Contact__c))
			{
				//If there is no history record,this record is the first one, cancel sync
				continue;
			}
			List<Answer__c> history_Answer = map_ContactId_AnswerHistory.get(ans.Contact__c);
			Integer flag_WhichLastAnswer = 1;
			for(Answer__c ans_History : history_Answer)
			{
				if(ans_History.Finished_On__c >= ans.Finished_On__c)
				{
					continue;
				}
				if(ans.Q030_Last__c == null)
				{
					ans.Q030_Last__c = ans_History.Q030__c;
					ans.Q011_Last__c = ans_History.Q011__c;
				}
				if(ans.Q056_GC_last__c == null)
				{
					ans.Q056_GC_last__c = ans_History.Q056__c;
				}
				if(ans.Q045_Last__c == null)
				{
					ans.Q045_Last__c = ans_History.Q045__c;
					ans.Q046_Last__c = ans_History.Q046__c;
				}
				if(ans.TS_Last__c == null)
				{
					ans.TS_Last__c = ans_History.CreatedBy.Id;
				}
				if(ans.Q008_Invitation__c == null)
				{
					ans.Q008_Invitation__c = ans_History.Q008__c;
				}
				if(ans.Q008_Last__c == null && flag_WhichLastAnswer <= 2)
				{
					if(ans_History.Q008__c != null)
					{
						ans.Q008_Last__c = ans_History.Q008__c;
					}
					else
						if(map_ContactId_Task.containsKey(ans.Contact__c) && flag_WhichLastAnswer == 1)
						{//有问题，应该找下一个问卷号
							Set<Id> questionnaireId = map_ContactId_Task.get(ans.Contact__c);
							if(questionnaireId.contains(ans_History.Questionnaire__r.Next_Routine_Questionnaire__c))
							{
								ans.Q008_Last__c = null;
							}
						}
					flag_WhichLastAnswer ++;
				}
				/*Author:leo.bi@celnet.com.cn
				 *Alter Date:2014-06-29
				 *Function:sync q011_last to q011_last_text,sync q046_last to q046_last_text.Don't check if Q011 last text is null
				 * 		   Q011 last may change after above logic, so update q011 last text again
				 */
				ans.Q011_Last_Text__c = ans.Q011_Last__c;
				if(ans.Q011_Last__c != null && ans.Q008_Invitation__c != null && ans.Q030_Last__c != null && ans.Q045_Last__c != null && ans.Q046_Last__c != null && ans.TS_Last__c != null && flag_WhichLastAnswer >=2)
				{
					break;
				}     
			}
		} 
	}
	
	private void syncMemberEntry()
	{
		List<Answer__c> list_ans = trigger.new;
		List<Answer__c> list_AnswerNew = new List<Answer__c>();
		Set<Id> set_ContactId = new Set<Id>();
		Set<Id> set_MemberEntry = new Set<Id>();
		Map<Id,Id> map_Contact_MemberEntry = new Map<Id,Id>();
		Map<Id,Member_Entry__c> map_Id_MemberEntry = new Map<Id,Member_Entry__c>();
		for(Answer__c ans : list_ans)
		{
			if(ans.Contact__c == null || ans.Status__c != 'Finish' || ans.Finished_On__c == null)
			{
				continue;
			}
			if(ans.Q014__c == null && ans.Q024__c == null)
			{
				continue;
			}
			list_AnswerNew.add(ans);
			set_ContactId.add(ans.Contact__c);
		}
		if(set_ContactId.size() == 0)
		{
			return;
		}
		List<Contact> list_Contact = [Select Id, Account.Member_Entry__c, AccountId From Contact where Id in :set_ContactId];
		for(Contact con : list_Contact)
		{
			if(con.Account.Member_Entry__c != null)
			{
				set_MemberEntry.add(con.Account.Member_Entry__c);
				map_Contact_MemberEntry.put(con.Id,con.Account.Member_Entry__c);
			}
		}
		if(set_MemberEntry.IsEmpty())
		{
			return;
		}
		List<Member_Entry__c> list_MemberEntry = [Select Q_Wish_Using_Baby_Milk_Powder__c, Q_Using_Mother_Milk_Powder__c, Id From Member_Entry__c where Id in :set_MemberEntry];
		if(list_MemberEntry.size() == 0)
		{
			return;
		}
		for(Member_Entry__c me : list_MemberEntry)
		{
			if(!map_Id_MemberEntry.containsKey(me.Id))
			{
				map_Id_MemberEntry.put(me.Id,me);
			}
		}
		for(Answer__c ans : list_AnswerNew)
		{
			if(!map_Contact_MemberEntry.containsKey(ans.Contact__c))
			{
				continue;
			}
			if(!map_Id_MemberEntry.containsKey(map_Contact_MemberEntry.get(ans.Contact__c)))
			{
				continue;
			}
			Member_Entry__c me = map_Id_MemberEntry.get(map_Contact_MemberEntry.get(ans.Contact__c));
			me.Q_Wish_Using_Baby_Milk_Powder__c = ans.Q014__c;
			me.Q_Using_Mother_Milk_Powder__c = ans.Q024__c;
		}
		update list_MemberEntry;
	}
	
	public void syncStatusToContactAfter()
	{
		List<Answer__c> list_TriggerNew = trigger.new;
		List<Answer__c> list_ans = new List<Answer__c>();
		Map<Id,List<Answer__c>> map_ContactId_AnswerHistory = new Map<Id,List<Answer__c>>();
		Set<Id> set_ContactId = new Set<Id>();
		//get the collections of updated answers' contact Id
		for(Answer__c ans : list_TriggerNew)
		{
			if(ans.Contact__c == null || ans.Status__c != 'Finish' || ans.Finished_On__c == null)
			{
				continue;
			}
			if((ans.Q036__c==null || ans.Q036__c=='') && (ans.Q037__c==null || ans.Q037__c=='') && (ans.Q032__c==null || ans.Q032__c=='') &&
			   (ans.Q045__c==null || ans.Q045__c=='') && (ans.Q046__c==null || ans.Q046__c=='') && (ans.Q030__c==null || ans.Q030__c=='')
			&& (ans.Q011__c==null || ans.Q011__c=='') && (ans.Q008__c==null || ans.Q008__c==''))
			{
				continue;
			}
			list_ans.add(ans);
			set_ContactId.add(ans.Contact__c);
		}
		if(set_ContactId.size() == 0)
		{
			return;
		}
		//get the collections of to-update contact
		List<Contact> list_Contact = [Select X2nd_Last_Consumption_Brand__c, Last_Consumption_Brand__c, Is_Last_Digestive_Symptoms__c, Is_Last_Allergy_Symptoms__c, Id, Digestive_Symptoms_Type__c, Allergy_Symptoms_Type__c 
									  From Contact 
									  where Id in :set_ContactId];
		for(Answer__c ans : list_ans)
		{
			//find the contact the current answer is related to
			Contact con;
			for(Contact c : list_Contact)
			{
				if(c.Id == ans.Contact__c)
				{
					con = c;
					break;
				}
			}
			if(ans.Q036__c == 'N')
			{
				con.Do_Not_SMS__c = true;
			}
			if(ans.Q036__c == 'Y')
			{
				con.Do_Not_SMS__c = false;
			}
			if(ans.Q037__c == 'N')
			{
				con.DoNotCall = true;
			}
			if(ans.Q037__c == 'Y')
			{
				con.DoNotCall = false;
			}
			if(ans.Q032__c == 'N')
			{
				con.Do_Not_Invite__c = true;
			}
			if(ans.Q032__c == 'Y')
			{
				con.Do_Not_Invite__c = false;
			}
			if(ans.Q045__c != null && ans.Q046__c != null)
			{
				con.Is_Last_Allergy_Symptoms__c = ans.Q045__c;
				con.Allergy_Symptoms_Type__c = ans.Q046__c;
			}
			if(ans.Q030__c != null  && ans.Q011__c != null)
			{
				con.Is_Last_Digestive_Symptoms__c = ans.Q030__c;
				con.Digestive_Symptoms_Type__c = ans.Q011__c;
			}
			if(ans.Q008__c != null)
			{
				con.Last_Consumption_Brand__c = ans.Q008__c;
			}
		}
		//X2nd_Last_Consumption_Brand__c
		List<Answer__c> list_Answer_History = [select Contact__c, Finished_On__c, Status__c, Q008__c 
										       from Answer__c 
										       where Status__c ='Finish' and Contact__c != null and Finished_On__c != null
										       and Contact__c in :set_ContactId
										       order by Finished_On__c desc];
		for(Answer__c ans : list_Answer_History)
		{
			List<Answer__c> list_temp = new List<Answer__c>();
			//remove the updated answer from the answer_history map
			Boolean flag = true;
			for(Answer__c ansNew : list_ans)
			{
				if(ansNew.Id == ans.Id)
				{
					flag = false;
					break;
				}
			}
			if(flag)
			{
				if(map_ContactId_AnswerHistory.containsKey(ans.Contact__c))
				{
					list_temp = map_ContactId_AnswerHistory.get(ans.Contact__c);
					list_temp.add(ans);
				}
				else
				{
					list_temp.add(ans);
					map_ContactId_AnswerHistory.put(ans.Contact__c,list_temp);
				}
			}
		}	
		for(Contact con : list_Contact)
		{
			if(!map_ContactId_AnswerHistory.containsKey(con.Id))
			{
				continue;
			}
			List<Answer__c> list_temp_Answer = map_ContactId_AnswerHistory.get(con.Id);
			for(Answer__c ans : list_temp_Answer)
			{
				if(con.X2nd_Last_Consumption_Brand__c == null)
				{
					con.X2nd_Last_Consumption_Brand__c = ans.Q008__c;
				}
			}
		}
		//update
		update list_Contact;
	}
}