global with sharing class BatchUpdateCampaignMember
		implements Database.Batchable<sObject>, Database.Stateful{


	global Map<Id,List<List<Coupon__c>>> campaignId_CouponList;
	global final Map<ID,String> campaignId_memberStatus;
	global final Set<ID> campaignIds;
	
	/*contructor
		Map<Campaign ID, campaignId_CouponList>
			Map<ID,String> campaignId_memberStatus
 	*/
	global BatchUpdateCampaignMember( 	Map<ID,String> campaignId_memberStatus, 
											Map<Id,List<List<Coupon__c>>> campaignId_CouponList){
 		
 		this.campaignId_CouponList = campaignId_CouponList;
 		this.campaignId_memberStatus = campaignId_memberStatus;
 		campaignIds = new Set<ID>();
     	campaignIds.addAll(campaignId_CouponList.keySet());
     	campaignIds.addAll(campaignId_memberStatus.keySet());
  	}
 	
 	// Start Method
     global Database.QueryLocator start(Database.BatchableContext BC){
     	
      	return Database.getQueryLocator([SELECT Id, Status, Coupon_I__c, Coupon_II__c, CampaignId
      									FROM CampaignMember
      									 WHERE CampaignId IN :campaignIds
      									 order by Id asc]);
     }
    
   // Execute Logic
    global void execute(Database.BatchableContext BC, List<sObject>scope){
    	for (sObject s : scope){
        	CampaignMember cm = (CampaignMember)s;
        	
        	if (campaignId_CouponList.keySet().contains(cm.CampaignId))
        		updateCoupons(cm);
        	if (campaignId_memberStatus.keySet().contains(cm.CampaignId))
        		updateStatus(cm);
        
        }
    
        update scope;
    }
    
    private void updateCoupons(CampaignMember cm){
    	//check if assigning coupons
    	List<List<Coupon__c>> couponList = campaignId_CouponList.get(cm.campaignId);
    	//Assigning generated Coupons to Members
    	if (couponList!=null){
    		//set to null first
    		cm.Coupon_I__c = null;
    		cm.Coupon_II__c = null;
    		//assign new coupon I and II, if any
            if (couponList.size() > 0 && couponList[0].size()>0){
           		cm.Coupon_I__c = couponList[0][0].Id;
           		couponList[0].remove(0);
            }
            if (couponList.size() > 1 && couponList[1].size()>0){
           		cm.Coupon_II__c = couponList[1][0].Id;
           		couponList[1].remove(0);
            }
        }
    }
    
    private void updateStatus(CampaignMember cm){
    	cm.Status = campaignId_memberStatus.get(cm.campaignId);
    }
   
    global void finish(Database.BatchableContext BC){
         //notify logic  
    }

}