/*
 *Author:michael.mi@celnet.com.cn
 *Date:2014-10-13
 *Function:Wechat sms service
*/
public class WechatSMSService 
{
	public static string SendSMS(string Mobile, string Content)
	{
		system.debug('*******content*********' + Content);
		map<string,Wechat_SMS_Setting__c> smsConfig = Wechat_SMS_Setting__c.getAll();
		string body = '<Request><acc>'+smsConfig.get('Username').value__c+'</acc><pwd>'+smsConfig.get('Password').value__c+'</pwd><msisdn>'+Mobile+'</msisdn><suffix>0</suffix><msg>'+Content+'</msg><xmlResp>y</xmlResp></Request>';
		string SMSEndPoint = smsConfig.get('SMSURL').value__c;
		Http http = new Http(); 
		HttpRequest req = new HttpRequest();
		req.setHeader('Content-Type', ' text/xml;charset=UTF-8');
		req.setBody(body);
	    req.setEndpoint(SMSEndPoint); 
	    req.setMethod('POST');   
	    HttpResponse res = http.send(req);
		return res.getBody();   
	}
}