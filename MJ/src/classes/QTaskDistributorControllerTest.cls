/*
Author: shiqi.ng@sg.fujitu.com
*/
@isTest(SeeAllData=true)
private class QTaskDistributorControllerTest{
    static testmethod void testDistributeStep1(){
        createSubordinateUsers();
        User cnTestUser = [ select Id, UserRoleId from User where Alias = 'CNuser1'];
        System.runAs ( cnTestUser ) {
        	QTaskDistributorController controller = new QTaskDistributorController(null);
	        ApexPages.currentPage().getParameters().put('debug','1');
	        // init all values
	        List<SelectOption> opsOpts = controller.getOperationOptions();
	        List<SelectOption> queueOpts = controller.getQueueOptions();
            List<SelectOption> ownerOpts = controller.getOwnerOptions();
	        List<SelectOption> monthOpts = controller.getArchiveMonthOptions();
	        List<SelectOption> subregionOpts = controller.getSubRegionOptions();
	        List<SelectOption> provinceOpts = controller.getProvinceOptions();
	        List<String> digestiveSymptoms = controller.list_DigestiveSymptoms;
	        List<String> allergySymptoms = controller.list_AllergySymptoms;
            List<SelectOption> priChannelOpts = controller.getPrimaryChannelOptions();
            
	        
	        //create qtask with questionnaire based on first queue
	        Questionnaire__c qn1 = new Questionnaire__c();
	        qn1.Name = 'Questionnaire1';
	        qn1.Type__c = 'Routine';
	        insert qn1;
	        Q_Task__c qt1 = new Q_Task__c();
	        qt1.questionnaire__c = qn1.id;
	        qt1.OwnerId = queueOpts[1].getValue();
	        qt1.Id__c = 'testId';
	        insert qt1;
	        
	        //simulate onchange: queue
	        controller.SelectedOwnerOrQueue = queueOpts[1].getValue();
	        controller.onQueueChange();
	       
	        //simulate onchage: subregion
	        controller.onSubRegionChange();
	        controller.SelectedSubRegion = subregionOpts[1].getValue();
	        controller.onSubRegionChange();
	        //simulate onchange: area
	        controller.onAreaChange();
	        controller.SelectedArea = controller.AreaOptions[1].getValue();
            controller.onAreaChange();
            controller.SelectedSalesCity = controller.SalesCityOptions[1].getValue();
	        
	        //simulate onchange: province
	        controller.onProvinceChange();
	        controller.SelectedProvince = provinceOpts[1].getValue();
	        controller.onProvinceChange();
	        //simluate onChange: city
	        controller.onCityChange();
	        controller.SelectedCity = controller.CityOptions[1].getValue();
	        controller.onCityChange();
            //controller.SelectedAdminArea = controller.AdminAreaOptions[1].getValue();
            //simulate onChange: Primary Channel
            controller.SelectedPrimaryChannel = priChannelOpts[1].getValue();
            controller.onPrimaryChannelChange();
            controller.SelectedSubChannel = controller.SubChannelOptions[1].getValue();
	        
	        //simulate doQuery
	        controller.SelectedOwnerOrQueue = null;
	        controller.doQuery();
	        //simulate selected entries , by nature, by business
	        controller.SelectedOwnerOrQueue = queueOpts[1].getValue();
	        controller.dummyQTask.Archive_Month__c = monthOpts[1].getValue();
	        controller.dummyQTask.Specific_Code__c = 'Dummy code';
	        controller.dummyQTask.Type__c = 'Routine';
	        controller.dummyQTask.Business_Type__c = 'Routine Greet';
	        controller.dummyEvent1.ActivityDate = system.today();
	        controller.dummyEvent2.ActivityDate = system.today();
            controller.dummyContactFrom.BirthDate = system.today();
            controller.dummyContactTo.BirthDate = system.today();
	        controller.dummyQTask.Last_Routine_Call_Result__c = 'No Answer';
	        controller.dummyQTask.Member_Status__c = 'Others';
	        controller.dummyQtask.Is_Win__c = 'Y';
	        controller.dummyQTask.Allergy_Symptoms__c = 'Y';
	        controller.dummyQTask.Symptoms_Type__c = 'Skin';
	        controller.dummyQTask.Digestive_Symptoms__c = 'Y';
	        controller.dummyQTask.Digestive_Symptoms_Type__c = 'Spits';
            controller.dummyQTask.Is_Distribute_S0__c = 'Y';
            controller.dummyQTask.Is_Distribute_S2__c = 'Y';
            controller.dummyQTask.Is_Distribute_S3__c = 'Y';
            
	        ApexPages.currentPage().getParameters().put('by','nature');
	        controller.SelectedProvince = provinceOpts[1].getValue();
	        controller.doQuery();
	        ApexPages.currentPage().getParameters().put('by','business');
	        controller.doQuery();
        }
        
    }
    
    static testmethod void testAllOperations(){
        createSubordinateUsers();
        User cnTestUser = [ select Id, UserRoleId from User where Alias = 'CNuser1'];
        System.runAs ( cnTestUser ) {
        	Test.startTest();
	        QTaskDistributorController controller = new QTaskDistributorController(null);
	        commonSetup(controller);
	        //Init Variables
	        String currName = controller.CurrentUserFullname;
	        List<QTaskDistributorController.AssignmentWrapper> wrappers = controller.AssignmentWrapperData;
	        system.debug('Subordinates: '+wrappers);
	        
	        //Step 1: Query
	        controller.doQuery();
	        system.debug('Record Count:'+controller.RecordCount); 
	        controller.dummyQTask.Priority__c = 'High';
	        //Try assign none
	        controller.doAssign();
	        
            //Try assign more than query
	        controller.AssignFirstLines = 100;
            wrappers[0].IsSelected = true;
            wrappers[0].QuantityAssigned = 100;
            wrappers[1].IsSelected = false;
            wrappers[1].QuantityAssigned = 0;
            controller.doAssign();
            
            //Try assign zero to all
	        controller.AssignFirstLines = 10;
            wrappers[0].IsSelected = false;
            wrappers[0].QuantityAssigned = 0;
            wrappers[1].IsSelected = false;
            wrappers[1].QuantityAssigned = 0;
            controller.doAssign();
            
            //Try assign > 10000
            controller.AssignFirstLines = 100001;
            controller.doAssign();
            
	        //Assign QTask to TEST Users
	        controller.AssignFirstLines = 10;
            for (QTaskDistributorController.AssignmentWrapper w: wrappers){
	             w.IsSelected = true;
                 w.QuantityAssigned = 5;
             }
	        controller.doAssign();
	        //system.debug(ApexPages.getMessages());
	        //Set Quantity to Test users, 5 each
	        Set<Id> testUserIds = new Set<Id>();
	        for (QTaskDistributorController.AssignmentWrapper w: wrappers){
	            if (w.User.Name=='Testing1'){
	                w.IsSelected = true;
	                w.QuantityAssigned = 5;
	                testUserIds.add(w.User.Id);
	            }else if (w.User.Name=='Testing2'){
	                w.IsSelected = true;
	                w.QuantityAssigned = 5;
	                testUserIds.add(w.User.Id);
	            }
	        }
	        controller.doAssign();
	        for (AggregateResult result : [Select OwnerId, count(id) cnt
	                                        From Q_Task__c
	                                        Where OwnerId IN :testUserIds
	                                        Group by OwnerId]){
	            Integer count = (Integer)result.get('cnt');
	            //Assert Test Users OWN new QTASKS, 5 each
	            system.assertEquals(5, count);
	        }
	        system.debug('POPUP:'+controller.popupMessage);
	        controller.closePopup();
            //End Assign
            //Start Set Priority
            controller.SelectedOperation = 'SetPriority';
            controller.onOperationChange();
            controller.SelectedQuestionnaire = testQuestionnaire.Id;
            controller.SelectedOwnerOrQueue =  'All';
            controller.doQuery();
            List<QTaskDistributorController.PriorityWrapper> pw =  controller.getPriorityWrapperData();
            controller.dummyQTask.Priority__c = 'Low';
            controller.doSetPriority();
            controller.dummyQTask.Priority__c = null;
            controller.doSetPriority();
            //End Set Priority
            //  Start Recall
            controller.SelectedOperation = 'Recall';
            controller.onOperationChange();
            controller.SelectedQuestionnaire = testQuestionnaire.Id;
            controller.SelectedOwnerOrQueue =  'All';
            controller.doQuery();
            //Enter Recall Qty
            for (QTaskDistributorController.RecallWrapper rw : controller.RecallWrapperData){
                rw.RecallQuantity = rw.QueryQuantity + 1;
            }
            controller.doRecall();
            for (QTaskDistributorController.RecallWrapper rw : controller.RecallWrapperData){
                rw.RecallQuantity = rw.QueryQuantity;
            }
            controller.doRecall();
            
            //End Recall
            
	        Test.stopTest();
        }
        
    }
    
    static testmethod void testCloseTasks(){
        createSubordinateUsers();
        User cnTestUser = [ select Id, UserRoleId from User where Alias = 'CNuser1'];
        System.runAs ( cnTestUser ) {
        	Test.startTest();
	        QTaskDistributorController controller = new QTaskDistributorController(null);
	        commonSetup(controller);
	        //Init Variables
	        String currName = controller.CurrentUserFullname;
	        List<QTaskDistributorController.AssignmentWrapper> wrappers = controller.AssignmentWrapperData;
	        system.debug('Subordinates: '+wrappers);
	        
	        //Step 1: Query
	        controller.doQuery();
	        system.debug('Record Count:'+controller.RecordCount); 
	        controller.dummyQTask.Priority__c = 'Low';
            //Set Quantity to Test users, 5 each
            controller.AssignFirstLines = 10;
	        Set<Id> testUserIds = new Set<Id>();
	        for (QTaskDistributorController.AssignmentWrapper w: wrappers){
	            if (w.User.Name=='Testing1'){
	                w.IsSelected = true;
	                w.QuantityAssigned = 5;
	                testUserIds.add(w.User.Id);
	            }else if (w.User.Name=='Testing2'){
	                w.IsSelected = true;
	                w.QuantityAssigned = 5;
	                testUserIds.add(w.User.Id);
	            }
	        }
	        controller.doAssign();
	        for (AggregateResult result : [Select OwnerId, count(id) cnt
	                                        From Q_Task__c
	                                        Where OwnerId IN :testUserIds
	                                        Group by OwnerId]){
	            Integer count = (Integer)result.get('cnt');
	            //Assert Test Users OWN new QTASKS, 5 each
	            system.assertEquals(5, count);
	        }
	        system.debug('POPUP:'+controller.popupMessage);
	        controller.closePopup();
            system.debug('CLSE'+[Select status__c
	                                        From Q_Task__c
	                                        Where OwnerId IN :testUserIds]);
            
            //End Assign
            
            //Start Close Tasks
            controller.SelectedOperation = 'CloseTasks';
            controller.onOperationChange();
            controller.SelectedQuestionnaire = testQuestionnaire.Id;
            controller.doQuery();
            controller.doCloseTasks();
            
	        Test.stopTest();
        }
        
    }
    static Questionnaire__c testQuestionnaire;
    
    static void commonSetup(QTaskDistributorController controller){
         ApexPages.currentPage().getParameters().put('debug','1');
	        
	        //create 10 qtask with questionnaire based on first queue
	        testQuestionnaire = new Questionnaire__c();
	        testQuestionnaire.Name = 'Questionnaire1';
	        testQuestionnaire.Type__c = 'Routine';
	        insert testQuestionnaire;
	        //create 1 dummy contact
	        Contact c = new Contact();
	        c.LastName = 'Test Contact';
	        insert c;
	        
	        List<Q_Task__c> tasks = new List<Q_Task__c> ();
	        for (integer i = 0; i<10; i++){
	            Q_Task__c qt = new Q_Task__c();
	            qt.questionnaire__c = testQuestionnaire.id;
	            qt.Contact__c = c.id;
	            qt.Type__c = 'Routine';
	            qt.OwnerId = controller.getQueueOptions()[1].getValue();
	            qt.Id__c='testID'+i;
                qt.Status__c = 'Open';
	            tasks.add(qt);
	        }
	        insert tasks;
	        tasks = [Select OwnerId from Q_Task__c where questionnaire__c = :testQuestionnaire.id]; //get  actual ownerid
	        Id actualId = tasks[0].OwnerId;
        
        	controller.SelectedOwnerOrQueue = actualId;
        	controller.SelectedQuestionnaire = testQuestionnaire.Id;
	        system.debug('OWNERID:'+actualId);
    }
    
    static void createSubordinateUsers(){
        User u1,u2;
        User thisUser = [ select Id, UserRoleId from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            Profile p = [Select id from Profile where Name like 'CN_Outbound City Supervisor' limit 1];
            UserRole r1 = new UserRole(Name='Sub1', ParentRoleId = thisUser.UserRoleId);
            insert r1;
            UserRole r2 = new UserRole(Name='Sub1', ParentRoleId = r1.Id);
            insert r2;
            u1 = new User(Alias = 'CNuser1', Email='CNuser@testorg.com', 
                EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='zh_CN', 
                LocaleSidKey='zh_CN', ProfileId = p.Id, UserRoleId = r1.id,
                TimeZoneSidKey='Asia/Hong_Kong', UserName='CNuser1@testorg.com');
            insert u1;
            u2 = new User(Alias = 'CNuser2', Email='CNuser@testorg.com', 
                EmailEncodingKey='UTF-8', LastName='Testing2', LanguageLocaleKey='zh_CN', 
                LocaleSidKey='zh_CN', ProfileId = p.Id, UserRoleId = r2.id,
                TimeZoneSidKey='Asia/Hong_Kong', UserName='CNuser2@testorg.com');
            insert u2;
            //add to queue member of the first queue
            List<QueueSObject> queueList = [Select QueueId from QueueSObject where SobjectType = 'Q_Task__c' order by Queue.Name Asc Limit 1];
            
            GroupMember g = new GroupMember();
            g.UserOrGroupId = u1.Id;
            g.groupId = queueList[0].queueId;
            insert g;
        }
    }
    
}