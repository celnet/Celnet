/**
 * This class contains all of business handler test case
 */
@isTest
private class WechatBusinessHandler_Test 
{
	static User HKUser = TestUtility.new_HK_User();
	//PreparePipelineContext
	static WechatCallinMsgPipelineContext PreparePipelineContext(string eventKey,string msgType)
	{
		WechatEntity.CallinBaseMsg inMsg ;
		if(msgType == WechatEntity.MESSAGE_TYPE_EVENT)
		{
			WechatEntity.InEventMsg inEvent = new WechatEntity.InEventMsg();
			inEvent.EventKey = eventKey;
			inEvent.FromUserName = 'openid';
			inEvent.MsgType = msgType;
			inMsg = inEvent;
		}
		if(msgType == WechatEntity.MESSAGE_TYPE_IMAGE)
		{
			WechatEntity.InImageMsg inImage = new WechatEntity.InImageMsg();
			inImage.FromUserName = 'openid';
			inImage.MsgType = msgType;
			inMsg = inImage;
		}
		if(msgType == WechatEntity.MESSAGE_TYPE_TEXT)
		{
			WechatEntity.InTextMsg inTextMsg = new WechatEntity.InTextMsg();
			inTextMsg.FromUserName = 'openid';
			inTextMsg.MsgType = msgType;
			inTextMsg.Content = '0';
			inMsg = inTextMsg;
		}
		if(msgType == WechatEntity.EVENT_TYPE_SUBSCRIBE)
		{
			WechatEntity.InEventMsg inEvent = new WechatEntity.InEventMsg();
			inEvent.EventKey = eventKey;
			inEvent.FromUserName = 'openid';
			inEvent.MsgType = WechatEntity.MESSAGE_TYPE_EVENT;
			inEvent.Event = WechatEntity.EVENT_TYPE_SUBSCRIBE;
			inMsg = inEvent;
		}
		if(msgType == WechatEntity.MESSAGE_TYPE_VOICE)
		{
			WechatEntity.InVoiceMsg inVoice = new WechatEntity.InVoiceMsg();
			inVoice.FromUserName = 'openid';
			inVoice.MsgType = msgType;
			inVoice.MsgId = 'MsgId';
			inMsg = inVoice;
		}
		WechatCallinMsgPipelineContext context = new WechatCallinMsgPipelineContext(inMsg,'CideatechSFWeiXin');
		return context;
	}
	//prepare pipeline context 
	static WechatCallinMsgPipelineContext PreparePipelineContext(string eventKey,string msgType,string content)
	{
		WechatEntity.CallinBaseMsg inMsg ;
		if(msgType == WechatEntity.MESSAGE_TYPE_TEXT)
		{
			WechatEntity.InTextMsg inTextMsg = new WechatEntity.InTextMsg();
			inTextMsg.FromUserName = 'openid';
			inTextMsg.MsgType = msgType;
			inTextMsg.Content = content;
			inMsg = inTextMsg;
		}
		WechatCallinMsgPipelineContext context = new WechatCallinMsgPipelineContext(inMsg,'CideatechSFWeiXin');
		return context;
	} 
	//PrepareHandlerBindingInfo
	static Wechat_Handler_Binding__c PrepareBindingInfo(string msgType,string handlerName)
	{
		Wechat_Handler_Binding__c buyHandlerBinding = new Wechat_Handler_Binding__c();
		buyHandlerBinding.name = handlerName;
		buyHandlerBinding.Event_Key__c = handlerName;
		buyHandlerBinding.Handle_Msg_Type__c = msgType;
		buyHandlerBinding.Public_Account_Name__c = 'CideatechSFWeiXin';
		buyHandlerBinding.Handler_Name__c = handlerName;
		insert buyHandlerBinding;
		return buyHandlerBinding;
	}
	//Test_WechatCallinBuyHandler
    static testMethod void Test_WechatCallinBuyHandler() 
    {
    	system.runAs(HKUser)
    	{
    		WechatCallinBuyHandler buyHandler = new WechatCallinBuyHandler();
    		buyHandler.bindingInfo = PrepareBindingInfo(WechatEntity.MESSAGE_TYPE_EVENT,'WechatCallinBuyHandler');
    		buyHandler.Handle(PreparePipelineContext('WechatCallinBuyHandler',WechatEntity.MESSAGE_TYPE_EVENT));
    	}
    }
    //Test_WechatCallinExpertHandler
    static testMethod void Test_WechatCallinExpertHandler() 
    {
    	system.runAs(HKUser)
    	{
    		WechatCallinExpertHandler expertHandler = new WechatCallinExpertHandler();
    		expertHandler.bindingInfo = PrepareBindingInfo(WechatEntity.MESSAGE_TYPE_EVENT,'WechatCallinExpertHandler');
    		expertHandler.Handle(PreparePipelineContext('WechatCallinExpertHandler',WechatEntity.MESSAGE_TYPE_EVENT));
    	}
    }
    //Test_WechatCallinFamilyHandler
    static testMethod void Test_WechatCallinFamilyHandler() 
    {
    	system.runAs(HKUser)
    	{
    		WechatCallinFamilyHandler familyHandler = new WechatCallinFamilyHandler();
    		familyHandler.bindingInfo = PrepareBindingInfo(WechatEntity.MESSAGE_TYPE_EVENT,'WechatCallinFamilyHandler');
    		Wechat_Content__c wc = new Wechat_Content__c();
    		wc.Article_Url__c = 'Article_Url';
    		wc.Description__c = 'Description';
    		wc.Key_Word__c = 'Key_Word';
    		wc.Picture_Url__c = 'Picture_Url';
    		wc.Title__c = 'Title';
    		insert wc;
    		Wechat_Menu__c wm = new Wechat_Menu__c();
    		wm.Event_Key__c = 'WechatCallinFamilyHandler';
    		wm.name = 'menu';
    		wm.Wechat_Content__c = wc.id;
    		insert wm;
    		familyHandler.Handle(PreparePipelineContext('WechatCallinFamilyHandler',WechatEntity.MESSAGE_TYPE_EVENT));
    		familyHandler.Handle(PreparePipelineContext('WechatCallinFamilyHandler',WechatEntity.MESSAGE_TYPE_VOICE));
    	}
    }
    //Test_WechatCallinImageHandler
    static testMethod void Test_WechatCallinImageHandler() 
    {
    	system.runAs(HKUser)
    	{
    		//test if
    		wechat_user__c wu = new wechat_user__c();
    		wu.name = 'testwu';
    		wu.Open_id__c = 'openid';
    		insert wu;
    		WechatCallinImageHandler imageHandler = new WechatCallinImageHandler();
    		imageHandler.bindingInfo = PrepareBindingInfo(WechatEntity.MESSAGE_TYPE_EVENT,'WechatCallinImageHandler');
    		WechatCallinMsgPipelineContext context = PreparePipelineContext('WechatCallinImageHandler',WechatEntity.MESSAGE_TYPE_IMAGE); 
    		imageHandler.Handle(context);
    		//test if
    		lead l = new lead();
    		l.Baby_Count__c = '1';
    		l.Attachment_Count__c = 1;
    		l.Lastname = 'testlead';
    		insert l;
    		wu.binding_non_member__c = l.id;
    		update wu;
    		imageHandler.Handle(context);
    		//test
    		l.Attachment_Count__c = 0;
    		update l;
    		imageHandler.Handle(context);
    	}
    }
    //Test_WechatCallinMemberHandler
    static testMethod void Test_WechatCallinMemberHandler() 
    {
    	system.runAs(HKUser)
    	{
    		Wechat_Menu__c wm = new Wechat_Menu__c();
    		wm.Event_Key__c = 'WechatCallinMemberHandler';
    		wm.name = 'menu';
    		insert wm;
    		WechatCallinMemberHandler memberHandler = new WechatCallinMemberHandler();
    		memberHandler.bindingInfo = PrepareBindingInfo(WechatEntity.MESSAGE_TYPE_EVENT,'WechatCallinMemberHandler');
    		memberHandler.Handle(PreparePipelineContext('WechatCallinMemberHandler',WechatEntity.MESSAGE_TYPE_EVENT));
    		
    		Wechat_Content__c wc = new Wechat_Content__c();
    		wc.Article_Url__c = 'Article_Url';
    		wc.Description__c = 'Description';
    		wc.Key_Word__c = 'Key_Word';
    		wc.Picture_Url__c = 'Picture_Url';
    		wc.Title__c = 'Title';
    		insert wc;
    		wm.Wechat_Content__c = wc.id;
    		update wm;
    		memberHandler.Handle(PreparePipelineContext('WechatCallinMemberHandler',WechatEntity.MESSAGE_TYPE_EVENT));
    	}
    }
    //Test_WechatCallinMsgLatestNewsHandler
    static testMethod void Test_WechatCallinMsgLatestNewsHandler() 
    {
    	system.runAs(HKUser)
    	{
    		WechatCallinMsgLatestNewsHandler msgLatestNewsHandler = new WechatCallinMsgLatestNewsHandler();
    		msgLatestNewsHandler.bindingInfo = PrepareBindingInfo(WechatEntity.MESSAGE_TYPE_EVENT,'WechatCallinMsgLatestNewsHandler');
    		msgLatestNewsHandler.Handle(PreparePipelineContext('WechatCallinMsgLatestNewsHandler',WechatEntity.MESSAGE_TYPE_EVENT));
    		Wechat_Content__c wc = new Wechat_Content__c();
    		wc.Article_Url__c = 'Article_Url';
    		wc.Description__c = 'Description';
    		wc.Key_Word__c = 'Key_Word';
    		wc.Picture_Url__c = 'Picture_Url';
    		wc.Title__c = 'Title';
    		wc.Key_Word__c = 'News';
    		insert wc;
    		msgLatestNewsHandler.Handle(PreparePipelineContext('WechatCallinMsgLatestNewsHandler',WechatEntity.MESSAGE_TYPE_EVENT));
    	}
    }
    //Test_WechatCallinMsgRepeaterHandler
    static testMethod void Test_WechatCallinMsgRepeaterHandler() 
    {
    	system.runAs(HKUser)
    	{
    		WechatCallinMsgRepeaterHandler msgRepeaterHandler = new WechatCallinMsgRepeaterHandler();
    		msgRepeaterHandler.bindingInfo = PrepareBindingInfo(WechatEntity.MESSAGE_TYPE_EVENT,'WechatCallinMsgRepeaterHandler');
    		msgRepeaterHandler.Handle(PreparePipelineContext('WechatCallinMsgRepeaterHandler',WechatEntity.MESSAGE_TYPE_TEXT));
    		msgRepeaterHandler.Handle(PreparePipelineContext('WechatCallinMsgLatestNewsHandler',WechatEntity.MESSAGE_TYPE_EVENT));
    	}
    }
    //Test_WechatCallinOnlineHandler
    static testMethod void Test_WechatCallinOnlineHandler() 
    {
    	system.runAs(HKUser)
    	{
    		WechatCallinOnlineHandler onlineHandler = new WechatCallinOnlineHandler();
    		onlineHandler.bindingInfo = PrepareBindingInfo(WechatEntity.MESSAGE_TYPE_EVENT,'WechatCallinOnlineHandler');
    		onlineHandler.Handle(PreparePipelineContext('WechatCallinOnlineHandler',WechatEntity.MESSAGE_TYPE_EVENT));
    	}
    }
    //Test_WechatCallinRegisterHandler
    static testMethod void Test_WechatCallinRegisterHandler() 
    {
    	system.runAs(HKUser)
    	{
    		TestUtility.setupMemberIDCustomSettings();
    		Id leadRecordType = [select id from RecordType where DeveloperName=:'LeadsHK' and SobjectType=:'Lead'].id;
    		Id accountRecordType = [select id from RecordType where DeveloperName=:'HouseHold' and SobjectType=:'Account'].id;
    		WechatCallinRegisterHandler registerHandler = new WechatCallinRegisterHandler();
    		registerHandler.bindingInfo = PrepareBindingInfo(WechatEntity.MESSAGE_TYPE_EVENT,'WechatCallinRegisterHandler');
    		WechatCallinMsgPipelineContext context = PreparePipelineContext('WechatCallinRegisterHandler',WechatEntity.MESSAGE_TYPE_EVENT);
    		//test wechatuser
    		Wechat_User__c wu = new Wechat_User__c();
    		wu.Open_Id__c = 'openId';
    		insert wu;
    		registerHandler.Handle(context);
			//test nonmember    		
    		Lead lea = new Lead();
    		lea.LastName = 'PARENT';
	        lea.FirstName = 'TEST';
	        lea.First_Name_1st_Child__c = 'BABY';
	        lea.Last_Name_1st_Child__c = 'TEST';
	        lea.Birthday_1st_Child__c = system.today();
    		lea.RecordTypeId = leadRecordType;
    		insert lea;
			wu.Binding_Non_Member__c = lea.id;
			update wu;
			registerHandler.Handle(context);
			lea.attachment_count__c = 1;
			update lea;
			registerHandler.Handle(context);
			//test member
			Account acc = new Account();
    		acc.Name = 'PRIMARY PARENT';
	        acc.Last_Name__c = 'PRIMARY';
	        acc.First_Name__c = 'PARENT';
    		acc.RecordTypeId = accountRecordType;
    		insert acc;
			wu.Binding_Member__c = acc.id;
			update wu;
			registerHandler.Handle(context);
    	}
    }
    //Test_WechatCallinRoomHandler
    static testMethod void Test_WechatCallinRoomHandler() 
    {
    	system.runAs(HKUser)
    	{
    		WechatCallinRoomHandler roomHandler = new WechatCallinRoomHandler();
    		roomHandler.bindingInfo = PrepareBindingInfo(WechatEntity.MESSAGE_TYPE_EVENT,'WechatCallinRoomHandler');
    		roomHandler.Handle(PreparePipelineContext('WechatCallinRoomHandler',WechatEntity.MESSAGE_TYPE_EVENT));
    	}
    }
    //Test_WechatCallinTextHandler
    static testMethod void Test_WechatCallinTextHandler() 
    {
    	system.runAs(HKUser)
    	{
    		WechatCallinTextHandler textHandler = new WechatCallinTextHandler();
    		textHandler.bindingInfo = PrepareBindingInfo(WechatEntity.MESSAGE_TYPE_EVENT,'WechatCallinTextHandler');
    		//test for keyword 0
    		Wechat_Content__c wc = new Wechat_Content__c();
    		wc.Article_Url__c = 'Article_Url';
    		wc.Description__c = 'Description';
    		wc.Picture_Url__c = 'Picture_Url';
    		wc.Title__c = 'Title';
    		wc.Key_Word__c = '0';
    		insert wc;
    		textHandler.Handle(PreparePipelineContext('WechatCallinTextHandler',WechatEntity.MESSAGE_TYPE_TEXT,'0'));
    		//test for keyword 1
    		wc.Key_Word__c = '1';
    		update wc;
    		textHandler.Handle(PreparePipelineContext('WechatCallinTextHandler',WechatEntity.MESSAGE_TYPE_TEXT,'1'));
    		//test for keyword 2
    		wc.Key_Word__c = '2';
    		update wc;
    		textHandler.Handle(PreparePipelineContext('WechatCallinTextHandler',WechatEntity.MESSAGE_TYPE_TEXT,'2'));
    		//test for keyword 3
    		wc.Key_Word__c = '3';
    		update wc;
    		textHandler.Handle(PreparePipelineContext('WechatCallinTextHandler',WechatEntity.MESSAGE_TYPE_TEXT,'3'));
    		//test for keyword 4
    		wc.Key_Word__c = '4';
    		update wc;
    		textHandler.Handle(PreparePipelineContext('WechatCallinTextHandler',WechatEntity.MESSAGE_TYPE_TEXT,'4'));
    		//test for keyword 5
    		wc.Key_Word__c = '5';
    		update wc;
    		textHandler.Handle(PreparePipelineContext('WechatCallinTextHandler',WechatEntity.MESSAGE_TYPE_TEXT,'5'));
    		//test for keyword 6
    		wc.Key_Word__c = '6';
    		update wc;
    		textHandler.Handle(PreparePipelineContext('WechatCallinTextHandler',WechatEntity.MESSAGE_TYPE_TEXT,'6'));
    		//test for keyword 7
    		wc.Key_Word__c = '7';
    		update wc;
    		textHandler.Handle(PreparePipelineContext('WechatCallinTextHandler',WechatEntity.MESSAGE_TYPE_TEXT,'7'));
    		//test for keyword 8
    		wc.Key_Word__c = '8';
    		update wc;
    		textHandler.Handle(PreparePipelineContext('WechatCallinTextHandler',WechatEntity.MESSAGE_TYPE_TEXT,'8'));
    		//test for keyword 9
    		wc.Key_Word__c = '9';
    		update wc;
    		textHandler.Handle(PreparePipelineContext('WechatCallinTextHandler',WechatEntity.MESSAGE_TYPE_TEXT,'9'));
    		textHandler.Handle(PreparePipelineContext('WechatCallinTextHandler',WechatEntity.MESSAGE_TYPE_TEXT,'non'));
    	}
    }
    //Test_WechatCallinUploadPhotoHandler
    static testMethod void Test_WechatCallinUploadPhotoHandler() 
    {
    	system.runAs(HKUser)
    	{
    		TestUtility.setupMemberIDCustomSettings();
    		Id leadRecordType = [select id from RecordType where DeveloperName=:'LeadsHK' and SobjectType=:'Lead'].id;
    		Id accountRecordType = [select id from RecordType where DeveloperName=:'HouseHold' and SobjectType=:'Account'].id;
    		WechatCallinUploadPhotoHandler uploadPhotoHandler = new WechatCallinUploadPhotoHandler();
    		uploadPhotoHandler.bindingInfo = PrepareBindingInfo(WechatEntity.MESSAGE_TYPE_EVENT,'WechatCallinUploadPhotoHandler');
    		WechatCallinMsgPipelineContext context = PreparePipelineContext('WechatCallinUploadPhotoHandler',WechatEntity.MESSAGE_TYPE_EVENT);
    		//test wechatuser
    		Wechat_User__c wu = new Wechat_User__c();
    		wu.Open_Id__c = 'openId';
    		insert wu;
    		uploadPhotoHandler.Handle(context);
			//test nonmember    		
    		Lead lea = new Lead();
    		lea.LastName = 'PARENT';
	        lea.FirstName = 'TEST';
	        lea.First_Name_1st_Child__c = 'BABY';
	        lea.Last_Name_1st_Child__c = 'TEST';
	        lea.Birthday_1st_Child__c = system.today();
    		lea.RecordTypeId = leadRecordType;
    		insert lea;
    		wu.Binding_Non_Member__c = lea.id;
			update wu;
    		uploadPhotoHandler.Handle(context);
    		lea.attachment_count__c = 1;
			update lea;
 			uploadPhotoHandler.Handle(context);
 			lea.Baby_Count__c = '2';
			update lea;
 			uploadPhotoHandler.Handle(context);	
    	}
    }
     //Test_WechatCallinUserHandler
    static testMethod void Test_WechatCallinUserHandler() 
    {
    	system.runAs(HKUser)
    	{
    		TestUtility.setupMemberIDCustomSettings();
    		Id leadRecordType = [select id from RecordType where DeveloperName=:'LeadsHK' and SobjectType=:'Lead'].id;
    		Id accountRecordType = [select id from RecordType where DeveloperName=:'HouseHold' and SobjectType=:'Account'].id;
    		
    		WechatCallinUserHandler userHandler = new WechatCallinUserHandler();
    		userHandler.bindingInfo = PrepareBindingInfo(WechatEntity.MESSAGE_TYPE_EVENT,'WechatCallinUserHandler');
    		//test return if
    		userHandler.Handle(PreparePipelineContext('WechatCallinUserHandler',WechatEntity.MESSAGE_TYPE_EVENT));
			WechatCallinMsgPipelineContext context = PreparePipelineContext('WechatCallinUserHandler',WechatEntity.EVENT_TYPE_SUBSCRIBE);
			context.InMsg.FromUserName = 'openId';
			//test wulist empty
			userHandler.Handle(context);
    		//test wechatuser
    		Wechat_User__c wu = [select Binding_Member__c , Binding_Non_Member__c , Open_Id__c, id from Wechat_User__c where Open_Id__c = 'openId'];
    		/*insert wu;*/
    		userHandler.Handle(context);
    		//test nonmember has no attachment
    		Lead lea = new Lead();
    		lea.LastName = 'PARENT';
	        lea.FirstName = 'TEST';
	        lea.First_Name_1st_Child__c = 'BABY';
	        lea.Last_Name_1st_Child__c = 'TEST';
	        lea.Birthday_1st_Child__c = system.today();
    		lea.RecordTypeId = leadRecordType;
    		insert lea;
    		Account acc = new Account();
    		acc.Name = 'PRIMARY PARENT';
	        acc.Last_Name__c = 'PRIMARY';
	        acc.First_Name__c = 'PARENT';
    		acc.RecordTypeId = accountRecordType;
    		insert acc;
    		wu.Binding_Non_Member__c = lea.id;
			update wu;
			userHandler.Handle(context);
			//test nonmember attachment count equals babycount
			lea.Attachment_count__c = 1;
			update lea;
			userHandler.Handle(context);
			//test nonmember baby count > attachment count
			lea.Baby_count__c = '2';
			update lea;
			userHandler.Handle(context);
			
			lea.Attachment_count__c = 2;
			update lea;
			userHandler.Handle(context);
			
			wu.Binding_Member__c = acc.id;
			update wu;
			userHandler.Handle(context);
    	}
    }
    //Test_WechatCallinWikiHandler
    static testMethod void Test_WechatCallinWikiHandler() 
    {
    	system.runAs(HKUser)
    	{
    		WechatCallinWikiHandler wikiHandler = new WechatCallinWikiHandler();
    		wikiHandler.bindingInfo = PrepareBindingInfo(WechatEntity.MESSAGE_TYPE_EVENT,'WechatCallinWikiHandler');
    		wikiHandler.Handle(PreparePipelineContext('WechatCallinWikiHandler',WechatEntity.MESSAGE_TYPE_EVENT));
    	}
    }
    //Test_WechatCallinWiseHandler
    static testMethod void Test_WechatCallinWiseHandler() 
    {
    	system.runAs(HKUser)
    	{
    		WechatCallinWiseHandler wiseHandler = new WechatCallinWiseHandler();
    		wiseHandler.bindingInfo = PrepareBindingInfo(WechatEntity.MESSAGE_TYPE_EVENT,'WechatCallinWiseHandler');
    		wiseHandler.Handle(PreparePipelineContext('WechatCallinWiseHandler',WechatEntity.MESSAGE_TYPE_EVENT));
    	}
    }
    static testMethod void Test_WechatFieldMatch()
    {
    	system.runAs(HKUser)
    	{
    		//WechatFieldMatch wfm = new WechatFieldMatch();
    		WechatEntity.File file = new WechatEntity.File();
    		file.body = Blob.valueOf('body');
    		file.contentType = 'contentType';
    		file.description = 'description';
    		WechatFieldMatch.GenerateAttachment(file);
    		WechatEntity.User user = new WechatEntity.User();
    		user.subscribe = 0;
    		user.sex = 1;
    		user.nickname = 'nickname';
    		user.language = 'language';
    		user.city = 'city';
    		user.province = 'province';
    		user.country = 'country';
    		user.headimgurl = 'headimgurl';
    		user.unionid = 'unionid';
    		user.openid = 'openid';
    		user.subscribe_time = 23456666;
    		WechatFieldMatch.GenerateWechatUser(user);
    	}
    }
}