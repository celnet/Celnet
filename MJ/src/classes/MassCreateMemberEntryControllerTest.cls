/*
Author: scott.zhang@celnet.com.cn
Created On: 2014-04-16
Function: MassCreateMemberEntryController test class
Apply To: CN
*/
@isTest
private class MassCreateMemberEntryControllerTest 
{
    static testMethod void myUnitTest() 
    {
    	system.runas(TestUtility.new_CN_User())
        {
	        ApexPages.StandardController STController = new ApexPages.StandardController(new Member_Entry__c());
			MassCreateMemberEntryController MassCreateMemberEntry = new MassCreateMemberEntryController(STController);
			MassCreateMemberEntry.AddMember();
			MassCreateMemberEntry.CancelRow();
			MassCreateMemberEntry.DelRowId = 0;
			MassCreateMemberEntry.CancelRow();
			Address_Management__c am = new Address_Management__c();
			am.Region__c = '华北区';
	        am.Sub_Region__c = '河北省';
	        am.Area__c ='西区';
	        am.Sales_City__c = '廊坊市';
	        am.Name = '廊坊市';
	        insert am;
	        Account acc = new Account();
	        acc.LastName = 'lastname';
	        acc.Standard_City__c = am.Id;
	        acc.Specific_Code__c = 'HR1234';
	        insert acc;
			MassCreateMemberEntry.List_ObjMembers[0].MemberEntry.Name = null;
			MassCreateMemberEntry.SaveMember();
			MassCreateMemberEntry.List_ObjMembers[0].MemberEntry.Name='Member Name';
			MassCreateMemberEntry.List_ObjMembers[0].MemberEntry.Sales_Rep__c='Rep Name';
			MassCreateMemberEntry.List_ObjMembers[0].MemberEntry.Phone__c='18511111111';
			MassCreateMemberEntry.List_ObjMembers[0].MemberEntry.Baby1_Birthday__c = Date.today().addDays(-110);
			MassCreateMemberEntry.PublicMember.Administrative_Area__c = am.Id;
			MassCreateMemberEntry.PublicMember.Register_Date__c = Date.today();
			MassCreateMemberEntry.PublicMember.Specific_Code__c = 'HR1234';
			MassCreateMemberEntry.PublicMember.Archive_Month__c = '201309';
			MassCreateMemberEntry.SaveMember();
			//leo add on 07 11
			List<BF_Register_Source_Mapping__c> list_BF = new List<BF_Register_Source_Mapping__c>();
			BF_Register_Source_Mapping__c rsm1 = new BF_Register_Source_Mapping__c();
			rsm1.Name = 'NC 会员';
			rsm1.BF_Register_Source_Value__c = '4';
			rsm1.BF_Register_Sub_Source_Value__c = '22';
			rsm1.Register_Source_Label__c = 'NC 手工/纸质Store';
			rsm1.Register_Sub_Source_Label__c = 'NC 会员';
			list_BF.add(rsm1);
			BF_Register_Source_Mapping__c rsm2 = new BF_Register_Source_Mapping__c();
			rsm2.Name = 'NC 订阅';
			rsm2.BF_Register_Source_Value__c = '4';
			rsm2.BF_Register_Sub_Source_Value__c = '23';
			rsm2.Register_Source_Label__c = 'NC 手工/纸质Store';
			rsm2.Register_Sub_Source_Label__c = 'NC 订阅';
			list_BF.add(rsm2);
			BF_Register_Source_Mapping__c rsm3 = new BF_Register_Source_Mapping__c();
			rsm3.Name = '院内P-class';
			rsm3.BF_Register_Source_Value__c = '3';
			rsm3.BF_Register_Sub_Source_Value__c = '11';
			rsm3.Register_Source_Label__c = '手工/纸质hospital';
			rsm3.Register_Sub_Source_Label__c = '院内P-class';
			list_BF.add(rsm3);
			BF_Register_Source_Mapping__c rsm4 = new BF_Register_Source_Mapping__c();
			rsm4.Name = '院外P-class';
			rsm4.BF_Register_Source_Value__c = '3';
			rsm4.BF_Register_Sub_Source_Value__c = '13';
			rsm4.Register_Source_Label__c = '手工/纸质hospital';
			rsm4.Register_Sub_Source_Label__c = '院外P-class';
			list_BF.add(rsm4);
			insert list_BF;
			MassCreateMemberEntry.CrtRegisterSelectOption();
			MassCreateMemberEntry.AutoShowSubSource();
        }
    }
}