public class WechatOrderDetailController 
{
    public Account acc{get;set;}
    public Account ac{get;set;}
    public string lastName{get;set;}
    public string firstName{get;set;}
    public Order__c o{get;set;}
    public string accountId;
    public string productName{get;set;}
    public decimal num{get;set;}
    public string sku{get;set;}
    public decimal price{get;set;}
    public decimal sum{get;set;}
    public Product__c p{get;set;}
    public Order__c lastOrder{get;set;}
    public string code;
    public string publicAccountName;
    public boolean isShow{get;set;}
    public string hid{get;set;}
    public boolean isSuccessShow{get;set;}
    public string activeNo{get;set;}
    public string temAcitve{get;set;}
    private Order__c parentOrder;
    private static List<Product__c> DSOProducts;
    private static Map<ID,Product__c> productMap;
    private List<Contact> relatedBabies;
    
    public string needToknowLink{get;set;}
    public string publicHoliday{get;set;}
    public boolean ischecked{get;set;}
    
    static
    {
        if(DSOProducts == NULL)
        {
            DSOProducts = [SELECT ID,Name,Product_Chinese_SKU__c,Default_Order_Quantity__c,Allowed_Order_Period__c,Unit_Price__c,Max_Order_Quantity__c,Product_Chinese_Name__c
                                                         FROM Product__c 
                                                         WHERE Product_Nature__c = 'MJ' AND Inactive__c = false AND Is_DSO_Product__c = true
                                                         Order By Name ASC];
            productMap = new Map<ID,Product__c>();
            for(Product__c product : DSOProducts)
            {
                //Check NULL to avoid exception caused by dirty Product data
                if(product.Product_Chinese_SKU__c != NULL)
                {
                  productMap.put(product.ID,product);
                }
            }
        }
    }
    
    public WechatOrderDetailController()
    {
        temAcitve = '0';
        activeNo = '0';
        ischecked = false;
        o = new Order__c();
        this.isSuccessShow = true;
        
        needToknowLink = WechatBusinessUtility.WECHAT_SITE + 'WechatBuyNeedToKnow';
        
        if(acc == null && accountId == null)
        {
            string message = '';
            code =  Apexpages.currentPage().getParameters().get('code');
            if(code != null)
            {
	            publicAccountName = Apexpages.currentPage().getParameters().get('state');
	            WechatCalloutService wcs = new WechatCalloutService(publicAccountName);
	            string openId = wcs.GetOpenIdbyCode(code);
	            string status = WechatBusinessUtility.CheckWechatUserRelationshipByFields(openId);
	            
	            if(status == 'Non-Member')
	            {
	                message = WechatBusinessUtility.QueryRemindingByKey('RN036');
	            }
	            else if(status == null)
	            {
	                message = WechatBusinessUtility.QueryRemindingByKey('RN037');
	            }
	            else
	            {
	                List<Wechat_User__c> wu = [Select id , Binding_Member__c From Wechat_User__c where open_Id__c = :openId];
	                if(!wu.isempty()) 
	                {
	                    accountId = wu[0].Binding_Member__c;
	                }
	            }
	            if(message != '')
	            {
	                this.isShow = false;
	                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.WARNING, message);
	                ApexPages.addMessage(myMsg);
	                return;
	            }
	            else
	            {
	                this.isShow = true;
	            }
            }
            else
            {
            	accountId = '001N000000LTwXn';
	    		this.isShow = true;
            }
        }
        parentOrder = new Order__c(HouseHold__c=accountId);
        relatedBabies = OrderHelper.getRelatedBabies(parentOrder);
    }
    
    public Pagereference sendOrder()
    {
        List<Order__c> orderList = [select Address_Street_District_Territory__c,Address_Flat_Floor_Block_Building__c ,Address_Extra_Line__c , First_Name__c, Last_Name__c
                                from Order__c
                                where HouseHold__c = :accountId
                                order by CreatedDate desc];
        if(!orderList.isempty())
        {
            lastOrder = orderList[0];
        }
        else
        {
            lastOrder = new Order__c();
        }

        ac = [Select Region__c, Mobile_Phone__c, Last_Name__c, First_Name__c,
                      Address_Street__c, Address_Floor__c, Address_Flat__c, Address_District__c, 
                      Address_Building__c, Address_Block__c , Address_Territory__c 
               From Account
               where id = :accountId];
        acc = new Account(); 
        if(ac.Region__c != 'HK-香港')
        {
        	acc.Region__c = '';
		    acc.Address_Territory__c = '';
		    acc.Address_District__c = '';
        }
        else
        {
        	acc.Region__c = ac.Region__c;
		    acc.Address_Territory__c = ac.Address_Territory__c;
		    acc.Address_District__c = ac.Address_District__c;
        }
        
        acc.Mobile_Phone__c = ac.Mobile_Phone__c;
        acc.Address_Street__c = ac.Address_Street__c;
        acc.Address_Building__c = ac.Address_Building__c;
        acc.Address_Block__c = ac.Address_Block__c;
        acc.Address_Floor__c = ac.Address_Floor__c;
        acc.Address_Flat__c = ac.Address_Flat__c;
        lastName = ac.Last_Name__c;
        firstName = ac.First_Name__c;
        
        publicHoliday = '';
        for(Public_Holiday__c ph : [select Name , id , Public_Holiday__c from Public_Holiday__c])
        {
            string d =  string.valueof(ph.Public_Holiday__c.year()) + string.valueof(ph.Public_Holiday__c.month()) + string.valueof(ph.Public_Holiday__c.day());
            publicHoliday = publicHoliday + ',' + d;
        }
        ischecked = true;   
        
        return Page.WechatOrderDetail;
    }
    
    public Pagereference ConfirmOrder()
    {
        List<Product__c> pList = [select id , Unit_Price__c,Product_SKU__c from Product__c where Product_Chinese_Name__c = :productName and Product_Chinese_SKU__c = :sku];    
        if(!pList.isempty())            
        {
            p = plist[0];
            price = p.Unit_Price__c;
            sum = price * num;
        }
        return Page.WechatOrderSuccess;
    }
    
    public PageReference GoodSelect()
    {
        return page.WechatBuySelectGood;
    }
    
    public PageReference OrderDetail() 
    {
        return Page.WechatOrderDetail;
    }
    
    public void GetLastOrder()
    {
        string flat = '';
        string floor = '';
        string block = '';
        string building = '';
        string street = '';
        string district = '';
        string territory = '';
        
        string fsub = lastOrder.Address_Flat_Floor_Block_Building__c;
        List<string> fsubList = fsub.split(',');
        if(!fsubList.isempty())
        {
            flat = fsubList[0].substring(5);
            if(fsubList.size() >= 2)
            {
                if(fsubList[1].indexof('Floor ') == 0)
                {
                    floor = fsubList[1].substring(6);
                }
            }
            if(fsubList.size() >= 3)
            {
                if(fsubList[2].indexof('Block ') == 0)
                {
                    block = fsubList[2].substring(6);
                }
            }
        }
        
        string bsub = lastOrder.Address_Street_District_Territory__c;
        List<string> bsubList = bsub.split(',');
        if(!bsubList.isempty())
        {
            building = bsubList[0];
            if(bsubList.size() == 2)
            {
                street = bsubList[1];
            }
        }
        
        Pattern p3 = Pattern.compile('(.*?),(.*?),Exception (.*?)');
        Matcher m3 = p3.matcher(lastOrder.Address_Extra_Line__c +',Exception e');
        if(m3.find())
        {
            district = m3.group(1);
            territory = m3.group(2);
        }
        
        string region = 'HK-香港';
        WechatAddressUitity wau = new WechatAddressUitity();
        for(string t : wau.territoryList)
        {
            if(t.contains(territory))
            {
                territory = t;
                break;
            } 
        }
        if(territory != '')
        {
            for(string d : wau.districtList)
            {
                if(d.contains(district))
                {
                    district = d;
                    break;
                }
            }
        }
        
        if(district != '')
        {
            acc.Region__c = region;
            acc.Address_Territory__c = territory;
            acc.Address_District__c = district;
        }
        
        acc.Address_Street__c = street;
        acc.Address_Building__c = building;
        acc.Address_Block__c = block;
        acc.Address_Floor__c = floor;
        acc.Address_Flat__c = flat;
        firstName = lastOrder.First_Name__c;
        lastName = lastOrder.Last_Name__c;
    }
    
    /* Returns error message if validation fails. Else returns null*/
    private String validateMaxAllowOrderPeriod(Order_Item__c item){
        //#1 If Allowed_Order_Period__c ==NULL, bypass checking
        if(item.Product__c!= NULL && productMap.get(item.Product__c) != NULL
           && productMap.get(item.Product__c).Allowed_Order_Period__c !=NULL)
        { 
            Integer allowedDays = Integer.valueOf(productMap.get(item.Product__c).Allowed_Order_Period__c);
            Integer maxQuantity = Integer.valueOf(productMap.get(item.Product__c).Max_Order_Quantity__c);
            //Bypass if allowed days = 0 
            if (allowedDays == 0) { return null; }
            Integer numOfBabies = relatedBabies.size();
            system.debug('******numOfBabies************'+numOfBabies);
            Integer maxAllowedQuantity = maxQuantity * numOfBabies;
            Integer offset = 0;
            Boolean isAllowed = OrderHelper.isMaxOrderPeriodAllowed(item, parentOrder.HouseHold__c, allowedDays, maxAllowedQuantity,offset);
            if(!isAllowed)
            {
                //String productName = productMap.get(item.Product__c).Product_Chinese_Name__c;
                //list<object> ReplaceKeywords = new list<object>();
                //ReplaceKeywords.add(productName);
                //ReplaceKeywords.add(maxAllowedQuantity);
                //ReplaceKeywords.add(allowedDays);
                //ReplaceKeywords.add(allowedDays);
                string message = WechatBusinessUtility.QueryRemindingByKey('RN035');
                return message;
            }
        }
        return null;
    }
    
    public Pagereference CreateOrder()
    {
    	try
    	{
    		system.debug('******acc.Address_Territory__c*********************' +acc.Address_Territory__c);
	        string addressTerritory = acc.Address_Territory__c.substring(0 , acc.Address_Territory__c.lastIndexof('-'));
	        string addressDistrict = acc.Address_District__c.substring(0 , acc.Address_District__c.lastIndexof('-'));
	        
	        string FlatFloorBlock = 'Flat '+ acc.Address_Flat__c + ',Floor ' + acc.Address_Floor__c + (null != acc.Address_Block__c ? (',Block ' + acc.Address_Block__c) : ''); 
	        string BuildingStreet = acc.Address_Building__c + (null != acc.Address_Street__c ? (',' + acc.Address_Street__c) : '');
	        string DistrictTerritory = addressDistrict + ',' + addressTerritory;
	        
	        if(FlatFloorBlock.length() > 35)
	        {
	            o.Address_Flat_Floor_Block_Building__c = FlatFloorBlock.substring(0, 35);
	        }
	        else
	        {
	            o.Address_Flat_Floor_Block_Building__c = FlatFloorBlock;
	        }
	        
	        if(BuildingStreet.length() > 35)
	        {
	            o.Address_Street_District_Territory__c = BuildingStreet.substring(0, 35);
	        }
	        else
	        {
	            o.Address_Street_District_Territory__c = BuildingStreet;
	        }
	        
	        o.Address_Extra_Line__c = DistrictTerritory;
	        
	        Id OrderRecordTypeId = [select Id from RecordType where DeveloperName='HK_Order' and SobjectType= 'Order__c'].Id;
	        o.First_Name__c = firstName;
	        o.Last_Name__c = lastName;
	        o.Mobile_Phone__c = acc.Mobile_Phone__c;
	        o.HouseHold__c = accountId;
	        o.RecordTypeId = OrderRecordTypeId;
	        o.Channel__c = 'Wechat';
	        o.Delivery_Center__c = 'DKSH';
	        o.Product_Ordered__c = p.Product_SKU__c;
	        Order_Item__c oi = new Order_Item__c();
	        
	        oi.Product__c = p.Id;
	        oi.Quantity__c = num;
	        
	        String errorMessage = validateMaxAllowOrderPeriod(oi);
	        if (errorMessage != null){
	            this.isSuccessShow = false;
	            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.WARNING, errorMessage);
	            ApexPages.addMessage(myMsg);
	            return null;
	        }
	        insert o;
	        oi.Order__c = o.Id;
	        insert oi;  
    	}
    	catch(DMLException ex)
    	{
    		this.isSuccessShow = false;
    		String strErr = '';
    		  for (Integer i = 0; i < ex.getNumDml(); i++) 
    		  {
				   if(ex.getDmlMessage(i) != null && ex.getDmlMessage(i) != '')
				   {
				       strErr = ex.getDmlMessage(i);
				   }
    		  }
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.ERROR, strErr);
            ApexPages.addMessage(myMsg);
            return null;
    	}
    	catch(Exception e)
    	{
		 	this.isSuccessShow = false;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.WARNING, e.getMessage());
            ApexPages.addMessage(myMsg);
            return null;
    	}
        
        return Page.WechatSuccess;
    }
    
    public string GetStrByIndex(string index ,List<string> strList)
    {
        //string str =  'flat:testFlat,floor:testFloor,block:3,building:shds,street:hhnj,district:gbd,territory:cyq,region:bj';
        string va = '';
        for(string s : strList)
        {
            if(s.contains(index))
            {
                va = s;
                break;
            }
        }
        if(va == '')
        {
            return '';
        }
        else
        {
            return va.substring(va.lastIndexof(':')+1);
        }
    }
    
    public class WechatAddressUitity
    {
        public List<string> territoryList
        {
            get
            {
                List<string> tList = new List<string>();
                Schema.DescribeFieldResult F = Account.Address_Territory__c.getDescribe();
				List<Schema.PicklistEntry> p = F.getPicklistValues();
				for(Schema.PicklistEntry pe : p)
				{
					 tList.add(pe.getValue());
				}
                return tList;
            }
        }
        
        public List<string> districtList
        {
            get
            {
            	List<string> dList = new List<string>();
            	Schema.DescribeFieldResult F = Account.Address_District__c.getDescribe();
				List<Schema.PicklistEntry> p = F.getPicklistValues();
				for(Schema.PicklistEntry pe : p)
				{
					 dList.add(pe.getValue());
				}
                return dList;
            }
        }
    }
}