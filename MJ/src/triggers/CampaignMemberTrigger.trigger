/*
Before Insert: Generate and assign coupons to campaign members

Apply to [HK/CN/ALL]: HK
  
Author: shiqi.ng@sg.fujitsu.com
Last Modified: 2014-04-29
*/
trigger CampaignMemberTrigger on CampaignMember (before insert) {
   
   if (CommonHelper.isTriggerDisabled()){
        return;
    }
    
    if(!Context.ApplyTo(new String[] {'HK'})) // make this code only apply to HK
   	{
    	return;
	}
    
    if (Trigger.isBefore){
        if (Trigger.isInsert){
            
            Map<ID,List<CampaignMember>> campaignId_Members = new Map<ID,List<CampaignMember>>();
            for (CampaignMember m : Trigger.new){
                
                List<CampaignMember> members = campaignId_Members.get(m.CampaignId);
                if (members==null){
                    members =  new List<CampaignMember>();
                    campaignId_Members.put(m.CampaignId, members);
                }
                
                members.add(m);
            }
            CampaignCouponHelper.generateAndAssignCoupons(campaignId_Members);
         
        }
    }
}