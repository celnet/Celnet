/*
 *Author: scott.zhang@celnet.com.cn
 *Created On: 2014-06-18
 *Function: Lead event handler entrance
 *Apply To: ALL
 */
trigger Lead on Lead (before insert, before update) {
	if (CommonHelper.isTriggerDisabled())
    {
        return;
    }
    new Triggers()
    	.bind(Triggers.Evt.BeforeInsert, new LeadEventHandler())//Dispatch queue
    	.bind(Triggers.Evt.BeforeUpdate, new LeadEventHandler())//Dispatch queue
        .Execute(); 
}