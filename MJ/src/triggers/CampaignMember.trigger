/*
Author: leo.bi@celnet.com.cn
Created On: 2014-05-08
Function: CampaignMember event handler entrance
Apply To: ALL
*/
trigger CampaignMember on CampaignMember (after insert, after update,before insert,before update) {
	if (CommonHelper.isTriggerDisabled())
    {
        return;
    }
    new Triggers()
    	//Binding concrete handlers to envents
    	.bind(Triggers.Evt.BeforeUpdate, new CampaignMemberEventHandler())
        .bind(Triggers.Evt.AfterInsert, new QTaskEventHandler()) 
        .bind(Triggers.Evt.AfterUpdate, new QTaskEventHandler())
        .bind(Triggers.Evt.AfterInsert, new LeadEventHandler()) //update Call Status from CampaignMember
        .bind(Triggers.Evt.AfterUpdate, new LeadEventHandler())//update Call Status from CampaignMember
        .Execute(); 
}