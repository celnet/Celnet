/* 
  Trigger Event: new consumption is created or existing consumption is updated
  Trigger Action: Update affected Baby record for Last consumption /2nd last consumption 
*/
trigger BabyConsumptionTrigger on Baby_Consumption__c (after insert, after update, after delete) 
{
    if (CommonHelper.isTriggerDisabled()){
        return;
    }
    
  if (Trigger.isAfter) 
    {
        Set<ID> affetcedContactIDSet = new Set<ID>();
        if (Trigger.isInsert) 
        {
            for (Baby_Consumption__c consumption : Trigger.new) 
                affetcedContactIDSet.add(consumption.Contact__c);
        }
        else if (Trigger.isUpdate)
        {
            for (Baby_Consumption__c consumption : Trigger.new) 
            {
                 affetcedContactIDSet.add(consumption.Contact__c);
            }
        }
        else if(Trigger.isDelete)
        {
            for (Baby_Consumption__c deletedRecord : Trigger.old) 
            {
                affetcedContactIDSet.add(deletedRecord.Contact__c);
            }
        } 
        if(!affetcedContactIDSet.isEmpty())
            BabyConsumptionHelper.updateLast2Consumption(affetcedContactIDSet);
    }
}