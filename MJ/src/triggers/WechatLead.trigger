/*
 *Author: leo.bi@celnet.com.cn
 *Created On: 2014-09-16
 *Function: Lead event handler entrance
 *Apply To: HK
 */
trigger WechatLead on Lead (before insert, before update,after update) 
{
	if (CommonHelper.isTriggerDisabled())
    {
        return;
    }
    new Triggers()
        .bind(Triggers.Evt.BeforeInsert, new WechatLeadEventHandler()) 
        .bind(Triggers.Evt.BeforeUpdate, new WechatLeadEventHandler()) 
        .bind(Triggers.Evt.AfterUpdate, new WechatUserEventHandler()) 
        .Execute();
}