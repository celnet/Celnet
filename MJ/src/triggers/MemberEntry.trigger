/*
Author: tommyliu@celnet.com.cn
Created On: 2014-04-8
Function: Memeber entry event handler entrance
Apply To: ALL
*/
trigger MemberEntry on Member_Entry__c (before insert, before update) 
{
 	if (CommonHelper.isTriggerDisabled())
    {
        return;
    }
    new Triggers()
    	//Binding concrete handlers to envents
    	.bind(Triggers.Evt.BeforeInsert, new MemberEntryEventHandler())//CN BF Integration 
        .bind(Triggers.Evt.BeforeUpdate, new MemberEntryEventHandler())//CN BF Integration 
        .Execute();   
}