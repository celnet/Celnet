/*
    Generic trigger template. See RecruitmentChannelHandler for handler actions.
*/
trigger RecruitmentChannelTrigger on Recruitment_Channel__c (before insert, after insert, after update, before update) 
{
    if (CommonHelper.isTriggerDisabled()){
        return;
    }
    
    
    RecruitmentChannelHandler handler = new RecruitmentChannelHandler(Trigger.isExecuting, Trigger.size);
    
    if(Trigger.isInsert && Trigger.isBefore){
        handler.OnBeforeInsert(Trigger.new);
    }
    else if(Trigger.isInsert && Trigger.isAfter){
        handler.OnAfterInsert(Trigger.new);
    }
    
    else if(Trigger.isUpdate && Trigger.isBefore){
        handler.OnBeforeUpdate(Trigger.old, Trigger.oldMap, Trigger.new, Trigger.newMap);
    }
    else if(Trigger.isUpdate && Trigger.isAfter){
        handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.newMap);
    }   
    
}