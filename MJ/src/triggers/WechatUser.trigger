/*Author:leo.bi@celnet.com.cn	
 *Function:Wechat User Trigger Entrance
 *Date:2014-09-22
 */
trigger WechatUser on Wechat_User__c (before update, after update) 
{
	if (CommonHelper.isTriggerDisabled())
    {
        return;
    }
    new Triggers()
    	.bind(Triggers.Evt.BeforeUpdate, new WechatUserEventHandler())
        .bind(Triggers.Evt.AfterUpdate, new WechatCalloutTaskQueueEventHandler()) 
        .Execute();
}