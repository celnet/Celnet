/*
*author:michael.mi@celnet.com.cn
*Date:20141011
*function:Order Trigger
*/
trigger WechatOrderItem on Order_Item__c (after insert) 
{
	if(CommonHelper.isTriggerDisabled())
    {
        return;
    }
    new Triggers()
        .bind(Triggers.Evt.AfterInsert, new WechatOrderItemEventHandler())
        .Execute();
}