/*
Calls CampaignTriggerHandler

Apply to [HK/CN/ALL]: HK
  
Author: shiqi.ng@sg.fujitsu.com
Last Modified: 2014-04-29
*/

trigger CampaignTrigger on Campaign (after insert, after update, before insert, before update) {
    if (CommonHelper.isTriggerDisabled()){
        return;
    }
    if(!Context.ApplyTo(new String[] {'HK'})) // make this code only apply to HK
   	{
    	return;
	}
    CampaignTriggerHandler handler = new CampaignTriggerHandler(Trigger.isExecuting, Trigger.size);
    if(Trigger.isInsert && Trigger.isBefore){
        handler.OnBeforeInsert(Trigger.new);
    }
    else if(Trigger.isInsert && Trigger.isAfter){
        handler.OnAfterInsert(Trigger.new);
    }
    
    else if(Trigger.isUpdate && Trigger.isBefore){
        handler.OnBeforeUpdate(Trigger.old, Trigger.oldMap, Trigger.new, Trigger.newMap);
    }
    else if(Trigger.isUpdate && Trigger.isAfter){
        handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.newMap);
    }   
    
}