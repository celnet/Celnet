/*
 *Author: leo.bi@celnet.com.cn
 *Created On: 2014-05-06
 *Function: Answer event handler entrance
 *Apply To: ALL
 */
trigger Answer on Answer__c (after insert, after update, before insert, before update)
{
	if (CommonHelper.isTriggerDisabled())
    {
        return;
    }
    new Triggers()
    	//Binding concrete handlers to envents
    	.bind(Triggers.Evt.BeforeUpdate, new AnswerStatusSyncHandler())
        .bind(Triggers.Evt.BeforeInsert, new AnswerStatusSyncHandler())
        //.bind(Triggers.Evt.AfterUpdate, new AnswerStatusSyncHandler())
        //.bind(Triggers.Evt.AfterInsert, new AnswerStatusSyncHandler())
    	.bind(Triggers.Evt.AfterUpdate, new QTaskEventHandler())
        .bind(Triggers.Evt.AfterInsert, new QTaskEventHandler())
    	.bind(Triggers.Evt.AfterUpdate, new CampaignMemberEventHandler())
        .bind(Triggers.Evt.AfterInsert, new CampaignMemberEventHandler())
     
        .Execute();
}