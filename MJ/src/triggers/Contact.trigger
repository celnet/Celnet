/*
Author: tommyliu@celnet.com.cn
Created On: 2014-04-8
Function: Contact event handler entrance
Apply To: ALL
*/
trigger Contact on Contact (after update,after insert)
{
 if (CommonHelper.isTriggerDisabled())
    {
        return;
    }
    
    new Triggers()
        //Binding concrete handlers to envents
        .bind(Triggers.Evt.AfterUpdate, new MemberEntryEventHandler())//CN BF Integration 
      .bind(Triggers.Evt.AfterInsert, new RoutineQTaskDistributionHandler())//CN Evaluation Flow for Greet 
        .Execute();    
}