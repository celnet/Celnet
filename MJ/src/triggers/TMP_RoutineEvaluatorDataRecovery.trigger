/*
数据修复
*/
trigger TMP_RoutineEvaluatorDataRecovery on Contact (before update) {
	//修复数据用户Id  
	//return;
	
	
	
	if(!Context.ApplyTo(new String[] {'CN'})) 
	{
		return;
	}
	System.debug('######################################################################进入trigger'+UserInfo.getUserId());
	if(UserInfo.getUserId()!='00590000002vKLoAAM')
	return;
	
	Id RecordTypeId = ObjectUtil.GetRecordTypeID(Contact.getSObjectType(), 'CN_Child');
	Set<Id> Set_ConIds = new Set<Id>();
	for(Contact con : trigger.new)
	{
		Contact oldcon = trigger.oldMap.get(con.Id);
		Datetime myDate = datetime.newInstance(2014, 8, 19, 2, 0, 0);
		/*
		con.Is_Winning__c = (con.Is_Winning__c=='1'?'Y':'N');
		
		if(con.Register_Date__c != null && con.Archive_Month__c==null)
		{
			String year = String.valueOf(con.Register_Date__c.Year());
			String month = (con.Register_Date__c.Month()>=10?String.valueof(con.Register_Date__c.Month()):'0'+String.valueof(con.Register_Date__c.Month()));
			con.Archive_Month__c = year+month;
		}
		*/
		if(con.RecordTypeId != RecordTypeId || con.Inactive__c || con.Duplicate_Status__c != null || con.Cannot_Be_Contacted__c )
		{
			continue;
		}
		//CN_Flag__c用于标示此Contact是否已经进行过数据修复
		//为避免每月都使用此方法修复，所以加入间隔时间至少要一周  
		if(con.CN_Flag__c == null || con.CN_Flag__c< myDate )
		{
			con.Appended_Log__c = null;
			Set_ConIds.add(con.Id);
		}
		
	}
	if(Set_ConIds.size()==0)
	{
		return;
	}
	
	List<Contact> List_Contact =[Select CN_Flag__c,Questionnaire__c,Id,Last_Routine_Call_Time__c,Cannot_Be_Contacted__c,Verified__c,Register_Date__c,Birthdate,Routine_Evaluated_On__c,Next_Routine_Evaluation_Date__c,Appended_Log__c,Duplicate_Status__c,Inactive__c,RecordTypeId,
								(Select Questionnaire__c,Questionnaire__r.Business_Type__c,Finished_On__c From Answers__r where Status__c ='Finish' and Finished_On__c !=null) 
								from Contact where Id in:Set_ConIds];
		
	if(List_Contact != null && List_Contact.size()>0)
	{
		Date startOfThisMonth = Date.newInstance(2014, 9, 1); 
		RoutineEvaluatorDataRecovery RoutineEva = new RoutineEvaluatorDataRecovery(List_Contact,startOfThisMonth);
		RoutineEva.TimingType = 'Non-Real Time'; 
		RoutineEva.Run();
	}
}