trigger DNCRJobResultTrigger on DNCR_Job_Result__c (before insert) 
{

    System.debug('=====> In DNCRJobResultTrigger.beforeInsert');
    
    for (DNCR_Job_Result__c result : Trigger.new) 
    {
        result.Job_Execution_Date__c=Date.today();
        
        Integer dncrLoaded = Integer.valueOf(result.Total_DNCR_Records__c)-Integer.valueOf(result.Failed_To_Load__c);
        Boolean isInProgress = DNCRHelper.isJobAlreadyInProgress();
        if(dncrLoaded > 0 && result.Status__c != 'Failed' && !isInProgress)
        {
            //Invoke DNCRMain to process DNCR against Member Contact in chain Apex Jobs:
            //Process DNCR False to True (New)-> Process DNCR True to False (Delete Existing) -> Delete DNCR Data
            ID jobID = DNCRHelper.startDNCRNewCheck();
            result.New_DNCR_Batch_Job_ID__c = jobID;
        }
        else //Handle error
        {
            if(isInProgress)
            {
                result.Status__c='Failed';
                result.Remark__c=CommonHelper.DNCR_DUPLICATED_JOB;
            }
            else if(result.Status__c != 'Failed')
            {
                result.Status__c='Failed';
                result.Remark__c=CommonHelper.DNCR_LOAD_FAILED;
            }
            
            try
            {
                //Send Email Notification
                DNCRHelper.sendEmailNotification(result);
            }
            catch(System.EmailException ex)
            {
                System.debug('Exception when sending email...' + ex.getMessage());
            }
        }
    }
}