/*
Author: tommyliu@celnet.com.cn
Created On: 2014-5-5
Function: QTask event handler entrance
Apply To: ALL
*/
trigger QTask on Q_Task__c (before insert, before update, after update, after delete) 
{
    if (CommonHelper.isTriggerDisabled())
    {
        return;
    } 
    new Triggers()
    	//Binding concrete handlers to envents
        .bind(Triggers.Evt.BeforeUpdate, new QTaskUpgradeHandler()) 
        .bind(Triggers.Evt.BeforeInsert, new QTaskDistributorHandler())
        .bind(Triggers.Evt.BeforeUpdate, new QTaskDistributorHandler())
        .bind(Triggers.Evt.AfterDelete, new ContactEventHandler())//Clear Questionnaire__c when Q-Task Closed  
        .bind(Triggers.Evt.AfterUpdate,new QTaskEventHandler())//ask.Description = '该致电任务已经转移'
        .Execute();  
}