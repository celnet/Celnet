/*
Author: tommyliu@celnet.com.cn
Created On: 2014-04-8
Function: Account event handler entrance
Apply To: ALL
*/
trigger Recruitment_Channel on Recruitment_Channel__c (after update,before Insert) {
     if (CommonHelper.isTriggerDisabled())
    {
        return;
    } 
    new Triggers()
        //Binding concrete handlers to envents
        .bind(Triggers.Evt.AfterUpdate, new MemberEntryEventHandler())//CN BF Integration 
        .bind(Triggers.Evt.BeforeInsert, new RecruitmentChannelEventHandler())//Set one Recruitment channel as primary
        .Execute();   
}