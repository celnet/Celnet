/*
 *Author: leo.bi@celnet.com.cn
 *Created On: 2014-05-13
 *Function: Campaign event handler entrance
 *Apply To: CN
 */
trigger Campaign on Campaign (after insert) {
	if (CommonHelper.isTriggerDisabled())
    {
        return;
    }
    new Triggers()
	    .bind(Triggers.Evt.AfterInsert, new CampaignMemberEventHandler()) 
	    .Execute(); 
}