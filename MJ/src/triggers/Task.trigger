/*
 *Author: leo.bi@celnet.com.cn
 *Created On: 2014-05-14
 *Function: Task event handler entrance
 *Apply To: ALL  
 */
trigger Task on Task (after update,before insert,before update,after insert) {  
    if (CommonHelper.isTriggerDisabled())
    {
        return;
    }
    new Triggers()
    	.bind(Triggers.Evt.BeforeInsert, new CN_CTIIntegrationLogEventHandler())
    	.bind(Triggers.Evt.BeforeUpdate, new CN_CTIIntegrationLogEventHandler())
        .bind(Triggers.Evt.BeforeInsert, new IntegrationLogEventHandler())
        .bind(Triggers.Evt.BeforeUpdate, new IntegrationLogEventHandler())
        
        .bind(Triggers.Evt.AfterInsert, new CN_CTIIntegrationLogEventHandler())
        .bind(Triggers.Evt.AfterInsert, new ContactEventHandler())//Update Contact
        .bind(Triggers.Evt.AfterUpdate, new ContactEventHandler())//Update Contact
        .bind(Triggers.Evt.AfterInsert, new CampaignMemberEventHandler())//Update Campaign Member
        .bind(Triggers.Evt.AfterInsert, new QTaskEventHandler())//Check history task result
        .bind(Triggers.Evt.AfterUpdate, new QTaskEventHandler())//Check history task result
        .Execute(); 
}