/*
Author: tommyliu@celnet.com.cn
Created On: 2014-04-8
Function: Account event handler entrance
Apply To: ALL
*/
trigger Account on Account (after update, before insert) 
{
    if (CommonHelper.isTriggerDisabled())
    {
        return;
    }
    new Triggers()
        //Binding concrete handlers to envents
        .bind(Triggers.Evt.AfterUpdate, new MemberEntryEventHandler())//CN BF Integration 
        //.bind(Triggers.Evt.BeforeInsert, new AccountGenerateMemberIDHandler()) 
        .Execute();    
}