trigger FulfillmentTrigger on Fulfillment__c (before insert, before update) {
    if (CommonHelper.isTriggerDisabled()){
        return;
    }
    
    FulfillmentTriggerHandler handler = new FulfillmentTriggerHandler(Trigger.isExecuting, Trigger.size);
    
    if (Trigger.isBefore && Trigger.isInsert){
        handler.OnBeforeInsert(Trigger.new);
    }else if (Trigger.isBefore && Trigger.isUpdate){
        handler.OnBeforeUpdate(Trigger.oldMap, Trigger.new);
    }

}