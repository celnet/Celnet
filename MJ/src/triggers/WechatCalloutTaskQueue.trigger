/*
 *Author: leo@celnet.com.cn
 *Created On: 2014-09-05
 *Function: WechatCalloutTaskQueue handler entrance
 *Apply To: HK
 */
trigger WechatCalloutTaskQueue on Wechat_Callout_Task_Queue__c (after insert) 
{
	if (CommonHelper.isTriggerDisabled())
    {
        return;
    }
    new Triggers().bind(Triggers.Evt.AfterInsert, new WechatCalloutQueueListener()).Execute(); 
}