/*
 *Author: leo.bi@celnet.com.cn
 *Created On: 2014-09-16
 *Function: Attachment event handler entrance
 *Apply To: HK
 */
trigger WechatAttachment on Attachment (after delete, after insert, after undelete) 
{
	if (CommonHelper.isTriggerDisabled())
    {
        return;
    }
    new Triggers()
        .bind(Triggers.Evt.AfterInsert, new WechatLeadEventHandler()) 
        .bind(Triggers.Evt.AfterDelete, new WechatLeadEventHandler()) 
        .bind(Triggers.Evt.AfterUndelete, new WechatLeadEventHandler()) 
        .Execute();
}