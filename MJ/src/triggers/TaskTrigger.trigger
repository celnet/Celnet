/*
    This trigger supports Bulk Processing
    Update Outbound Call Result to Contact Record
    Handles various scenerios: change to OB_Call_List__c / status / related contact etc
    
    Apply to [HK/CN/ALL]: ALL
*/
trigger TaskTrigger on Task (after insert, after update, after delete) 
{
    if (CommonHelper.isTriggerDisabled()){
        return;
    }
    
    
    List<ID> affectedBabies = new List<ID>();
    
    if (Trigger.isAfter) 
    {
        if (Trigger.isInsert) 
        {
            for (Task task : Trigger.new) {
                //Check if OBCallList is not empty and Type is Outbound Call
                //Outbound Call need to be checked when integrating with CTI in Phase 2. 
                //Currently just check OBCallList is not empty
                //if(task.OB_Call_List__c != NULL && task.Subject == 'Outbound Call')
                //过滤CIC记录
                if(task.CallObject != null||task.Status=='Completed')
				continue;
                
                if(task.OB_Call_List__c != NULL)
                    affectedBabies.add(task.WhoId);
            }
        }
        else if (Trigger.isUpdate)
        {
            for (Task newtTask : Trigger.new) 
            {
            	//过滤CIC记录
				if(newtTask.CallObject != null||newtTask.Status=='Completed')
				continue;
                
                Task oldTask = Trigger.oldMap.get(newtTask.ID);
                
                // Note: Call Type check will be needed in Phase 2 after CTI Integration
                
                // Scenerio 1:OBCallList from NULL to not NULL
                if(oldTask.OB_Call_List__c == NULL && newtTask.OB_Call_List__c != NULL)
                    affectedBabies.add(newtTask.WhoId);
                // Scenerio 2:OBCallList from not NULL to NULL 
                else if(oldTask.OB_Call_List__c != NULL && newtTask.OB_Call_List__c == NULL)
                    affectedBabies.add(oldTask.WhoId);
                // Scenerio 3:OBCallList is not NULL for both old and new
                else if (oldTask.OB_Call_List__c != NULL && newtTask.OB_Call_List__c != NULL)
                {
                    //Scenerio 3.1: Link to Same Contact and one of the following field is changed
                    if(oldTask.WhoId == newtTask.WhoId)
                    {
                        if((newtTask.OB_Call_List__c != oldTask.OB_Call_List__c) || (newtTask.Status != oldTask.Status)
                          ||newtTask.Willing_to_Try__c != oldTask.Willing_to_Try__c
                          ||newtTask.Continue_to_use__c != oldTask.Continue_to_use__c)
                            affectedBabies.add(oldTask.WhoId);
                    }
                    //Scenerio 3.2: Realted Contact Changed
                    else
                    {
                        affectedBabies.add(oldTask.WhoId);
                        affectedBabies.add(newtTask.WhoId);
                    }
                }
                // Others: do nothing
            }
        }
        else if(Trigger.isDelete)
        {
            for (Task deletedTask : Trigger.old) 
            {
                //Check if OBCallList is not empty for deleted task
                // Note: Call Type check will be needed in Phase 2 after CTI Integration
                //if(deletedTask.OB_Call_List__c != NULL && deletedTask.Subject == 'Outbound Call')
                //过滤CIC记录
				if(deletedTask.CallObject != null||deletedTask.Status=='Completed')
				continue;
                if(deletedTask.OB_Call_List__c != NULL)
                    affectedBabies.add(deletedTask.WhoId);
            }
        }          
    }
    
    if(!affectedBabies.isEmpty())
        ContactUpdateHelper.updateBabyCallLog(affectedBabies);
}