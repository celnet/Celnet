global class TAAccountScheduler implements Schedulable {
    public static String CON_EXP = '0 0 15 * * ?';
    global void execute(SchedulableContext SC) {
        List<TA_ACCOUNT__c> taaccs = [SELECT Id, OwnerId FROM TA_ACCOUNT__c WHERE OwnerId = '00G90000000FE7HEAW'];
        if ( taaccs != null && taaccs.size() > 0 ) {
        	for ( TA_ACCOUNT__c taacc : taaccs ) {
        		taacc.OwnerId = '00G90000000FE7CEAW';
        	}
            update taaccs;
            System.debug('From TAAccountScheduler: Transfer TA Account From Queue 2 to Queue 1 completed.');
        }
    }
}