global  class WeeklySummarySchedule implements Schedulable  {
global void execute(SchedulableContext ctx2){
Date begindate=System.today().toStartOfWeek();
    	Date endDate=begindate+7;
    	/**产生WeeklySummary
    	*  wscra 是RA Lead 的Weeklysummary
    	*  wscsourcingLeads 是SOURCING 的Weeklysummary
    	*  wscDsLeads是DS Leads的Weeklysummary
    	*/
    	WeeklySummary__c wscra=new WeeklySummary__c();
    	wscra.Week_Day__c=begindate;
    	wscra.Lead_Type__c='RA Leads';
    	
    	RecordType rtra=[select Id,Name from RecordType where RecordType.Name = 'Sourcing Leads'];
    	wscra.New_Lead__c=[Select count() from  Lead   where createdDate>=:Datetime.newInstance(beginDate,Time.newInstance(0,0,0,0)) and createdDate<=:Datetime.newInstance(endDate,Time.newInstance(0,0,0,0))and RecordType.Name='RA Leads'];
        wscra.New_CRU__c=[Select count() from Lead  where ConvertedAccount.USER_CRE_DATE__c>=:beginDate and ConvertedAccount.USER_CRE_DATE__c<=:endDate and RecordType.Name='RA Leads'];
        wscra.New_Seller__c=[Select count() from Lead where ConvertedAccount.FIRST_TRANS_DATE__c>=:beginDate and ConvertedAccount.FIRST_TRANS_DATE__c<=:endDate and RecordType.Name='RA Leads'];
        insert wscra;
        System.debug('***RA Leads WeeklySummary Created and Id is****'+wscra.Id+'********');   
        
        
        WeeklySummary__c wscsourcingLeads=new WeeklySummary__c();
                  
        wscsourcingLeads.Week_Day__c=begindate;
        wscsourcingLeads.Lead_Type__c ='Sourcing Leads';
    	RecordType rtsc=[select Id,Name from RecordType where RecordType.Name = 'Sourcing Leads'];
    	wscsourcingLeads.New_Lead__c=[Select count() from  Lead   where createdDate>=:Datetime.newInstance(beginDate,Time.newInstance(0,0,0,0))  
                                                   and createdDate<=:Datetime.newInstance(endDate,Time.newInstance(0,0,0,0))  
                                                   and RecordType.Name='Sourcing Leads'];
        wscsourcingLeads.New_CRU__c=[Select count() from Lead where ConvertedAccount.USER_CRE_DATE__c>=:beginDate and ConvertedAccount.USER_CRE_DATE__c<=:endDate 
                                                    and RecordType.Name='Sourcing Leads'];
        wscsourcingLeads.New_Seller__c=[Select count() from Lead where ConvertedAccount.FIRST_TRANS_DATE__c>=:beginDate and ConvertedAccount.FIRST_TRANS_DATE__c<=:endDate 
                                                    and RecordType.Name='Sourcing Leads'];
        insert wscsourcingLeads;
        System.debug('***Sourcing Leads WeeklySummary Created and Id is****'+wscsourcingLeads.Id+'********');                                          
         WeeklySummary__c wscDsLeads=new WeeklySummary__c();
                  
        wscDsLeads.Week_Day__c=begindate;
        wscDsLeads.Lead_Type__c='DS Leads';
    	RecordType rtDS=[select Id,Name from RecordType where RecordType.Name = 'DS Leads'];
    	wscDsLeads.New_Lead__c=[Select count() from  Lead   where createdDate>=:Datetime.newInstance(beginDate,Time.newInstance(0,0,0,0))  
                                                   and createdDate<=:Datetime.newInstance(endDate,Time.newInstance(0,0,0,0))  
                                                   and RecordType.Name='DS Leads'];
        wscDsLeads.New_CRU__c=[Select count() from Lead where ConvertedAccount.USER_CRE_DATE__c>=:beginDate and ConvertedAccount.USER_CRE_DATE__c<=:endDate 
                                                    and RecordType.Name='DS Leads'];
        wscDsLeads.New_Seller__c=[Select count() from Lead where ConvertedAccount.FIRST_TRANS_DATE__c>=:beginDate and ConvertedAccount.FIRST_TRANS_DATE__c<=:endDate 
                                                    and RecordType.Name='DS Leads'];
        insert wscDsLeads;
        System.debug('***DS Leads WeeklySummary Created and Id is****'+wscDsLeads.Id+'********');                                          
        }
        static testMethod void testWeekly()
        {
           Test.startTest();
       //String jobid2=System.schedule('TestScheduleWeeklySummary2', '0 0 10 ? * SAT', new ScheduleNewSummary());
       String jobidstring=System.schedule('TestWeeklySummary','0 0 10 ? * SAT',new WeeklySummarySchedule());
       Test.stopTest();
        }
}