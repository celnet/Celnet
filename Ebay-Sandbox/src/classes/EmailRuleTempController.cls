public with sharing class EmailRuleTempController {
	public String id{get;set;}
	
	public EmailRuleTempController(ApexPages.StandardController controller){
		this.id = controller.getId();
		System.debug('id:'+id);
	}
	
	public PageReference retURL(){
		//查看当前id是否有Batch在运行
		//List<EmailBatch__c> batch = [select id from EmailBatch__c where EmailRule__c =:this.id and (Status__c = 'New' or Status__c = 'IsPending' or Status__c = 'Proessing')];
		//if(batch.size()>0){
			return new PageReference('/apex/EmailRule?id='+this.id);
		//}else{
			//ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info,'当前规则有Batch在运行,不允许编辑!');
        	//ApexPages.addMessage(msg);
			//return null;
		//}
	}
	
	public PageReference retURLNew(){
		return new PageReference('/apex/EmailRule?id=none');
	}
	
}