/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestLeadTrigger {
    static testMethod void myUnitTest() {
        Lead l1=[select Name,Email from Lead limit 1];
        l1.Email='a22222222222222@a1.com';
        update l1;
    }
    
    
    static testMethod void testConvertion() {
    	Lead l1 = new Lead( FirstName = 'FN', LastName = 'LN', Company = 'Company', Sourcing__c = 'test');
    	l1.Sourcing_Group__c = 'test';
    	l1.Email = 'test2010@test.org';
    	l1.Lead_first_contact_time__c = System.Today();
    	//flag to trigger the trigger 'myLeadTrigger'  
    	l1.Email_Sync_Status__c = 'eBay id existed';
    	l1.HI_PO_Category__c = 'Credential Seller (B2C sites, platforms)';
    	insert l1;
    	
    	//test the convertion 
    	Database.LeadConvert lc = new database.LeadConvert();
		lc.setLeadId(l1.id);
		LeadStatus convertStatus = [Select Id, MasterLabel from LeadStatus where IsConverted=true limit 1];
		lc.setConvertedStatus(convertStatus.MasterLabel);
		Database.LeadConvertResult lcr = Database.convertLead(lc);
		System.assert(lcr.isSuccess());
		
		//test the duplication
		Lead l2 = new Lead( FirstName = 'FN', LastName = 'LN', Company = 'Company', Sourcing__c = 'test');
    	l1.Sourcing_Group__c = 'test';
    	l1.Email = 'test2010@test.org';
    	try {
    		insert l2;
    	} catch ( DMLException ex ){
    		System.assertEquals( true, ex.getMessage().contains('Duplicate Email found.'));
    	}
    	 
    } 
}