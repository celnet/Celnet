/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 */
@isTest
private class AddUserToTaTest {
/*
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        try{
        String UserId='mchuah@ebay.com';
        User u=[select id,Flag__c, NumberofHour__c from user where Username = :UserId  limit 1];
        Datetime dt=System.now();
        Datetime d2=dt.addHours(8);
        Integer timebeforenow=u.NumberofHour__c.intValue();
        Datetime d1=dt.addHours(8-timebeforenow);
        Integer TN=[select count()from TA_ACCOUNT__c  t where t.LastModifiedDate >:d1 and t.LastModifiedDate< :d2 and t.OwnerId=:UserId ];
        System.debug(TN);
        if(u.Flag__c > TN){
        System.debug('Flag is '+u.Flag__c+'\t TN is '+TN);
        }
        Account testa=[select Id,Name,OwnerId from Account where Name='ra test'];
        
        TA_ACCOUNT__c taaccount=new TA_ACCOUNT__c();
        taaccount.Account__c=testa.Id;
        taaccount.ACCOUNT_NAME__c=testa.Name;
        taaccount.Account_DS_Owner__c=testa.OwnerId;
        taaccount.OwnerId='00G90000000FE7CEAW';
        insert taaccount;
        TA_ACCOUNT__c ta1=[select DS_ACCOUNT__c from TA_ACCOUNT__c  where  OwnerId = '00G90000000FE7CEAW' or OwnerId = '00G90000000FE7HEAW' or OwnerId ='00G90000000FE7jEAG' limit 1 ];
        ta1.OwnerId=u.Id;
        update ta1;
        Integer TN2=[select count()from TA_ACCOUNT__C  t where t.LastModifiedDate >:d1 and t.LastModifiedDate< :d2 and t.OwnerId=:UserId ];
        System.debug(TN2);
        
       }catch(Exception e){
        System.debug(e.getMessage());
        }
    }
    */
     static testMethod void testAdduserTota(){
        RecordType rt=[select RecordType.Id from RecordType where RecordType.Name='TA Account'];
     try{
        Profile p = [SELECT Id FROM profile WHERE name='DS BE'];
        User u2 = new User(alias = 'newUser', email='newuser@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.Id,
        timezonesidkey='America/Los_Angeles', username='newuser@testorg.com');
     
        System.runAs(u2){
        
        Account dstest=new Account();
        dstest.Name='testdsaccount';
        dstest.Account_Status__c='Willingness but not CRU yet';
        dstest.Sourcing__c='Portal';
        dstest.Sourcing_Group__c='Portal';
        dstest.Create_time_in_ebay__c=Date.valueOf('2010-03-15');
        dstest.Lead_first_contact_time__c=Date.valueOf('2010-03-15');
        dstest.RecordTypeId=rt.Id;
        dstest.OwnerId=u2.Id;
        insert dstest;
        
        TA_ACCOUNT__c taaccount=new TA_ACCOUNT__c();
        taaccount.Account__c=dstest.Id;
        taaccount.DS_ACCOUNT__c=dstest.Id;
        taaccount.ACCOUNT_NAME__c=dstest.Name;
        taaccount.Account_DS_Owner__c=dstest.OwnerId;
        taaccount.OwnerId='00G90000000FE7CEAW';
        insert taaccount;
        }
        
       
        Profile p2 = [SELECT Id FROM profile WHERE name='TA'];
        User u3 = [select user.Id,user.Name,user.NumberofHour__c,user.Flag__c from User where user.ProfileId=:p2.Id and user.IsActive=true limit 1];
        System.runAs(u3){
        TA_ACCOUNT__c ta1=[select Id,OwnerId,DS_ACCOUNT__c from TA_ACCOUNT__c  where TA_ACCOUNT__c.Account_DS_Owner__c=:u2.Id];
        ta1.OwnerId=u3.Id;
        update ta1;
        }
        
       }catch(Exception e){
        System.debug(e.getMessage());
        }
       
     
     }
     static testMethod void UnitTestCreateTaAccount() {
        // TO DO: implement unit test
        try{
        Integer T1=[select count() from TA_ACCOUNT__C];
        Account a1=[select Name,RecordTypeId from Account where RecordTypeId='012900000000RwOAAU' limit 1];
        a1.RecordTypeId='012900000000RwTAAU';
        update a1;
        Integer T2=[select count() from TA_ACCOUNT__C];
    
        System.debug(T1);
        System.debug(T2);
        }catch(Exception e){
        System.debug(e.getMessage());
        }
        
    }
       static testMethod void tetMyleadsTrigger() {
        Lead l1=[select Name,Email from Lead where isConverted=false limit 1];
        l1.Email='a111111111@a.com';
        update l1;
    }
    
    
   
     static testMethod void testWeekySummary() {
        WeeklySummary__c ws = new WeeklySummary__c();
        ws.Week_Day__c = System.Today() + 7;
        insert ws;
    }
}