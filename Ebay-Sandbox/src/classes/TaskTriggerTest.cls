@isTest
private class TaskTriggerTest {
	static testmethod void myUnitTest(){
		Account acc = new Account();
		acc.Name = 'test';
		insert acc;
		
		Task t = new Task();
		t.WhatId = acc.Id;
		insert t;
		
		User u = [Select Id From User Where IsActive = true 
					And UserType = 'Standard' 
					And Profile.Name != '系统管理员' 
					And Profile.Name != 'System Administrator' limit 1];
		
		System.runAs(u){
			Account acc1 = new Account();
			acc1.Name = 'test';
			insert acc1;
			
			Task t1 = new Task();
			t1.WhatId = acc1.Id;
			insert t1;
		}
		
	}
}