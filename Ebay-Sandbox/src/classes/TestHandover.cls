@isTest
private class TestHandover {    
	static testMethod void theTest() {        
		Test.startTest();        
		Lead l1 = new Lead(LastName='情况1',Company='celnet',status='Contacted',recordtypeid='012900000003D3x',Vertical__c = 'Auto Parts & Accessory (汽车零部件及车用产品)');        
		insert l1;        
		ApexPages.StandardController sc = new ApexPages.StandardController(l1);        
		Handover handover = new Handover(sc);        
		ApexPages.currentPage().getParameters().put('id', l1.id);        
		handover.updateLead();        
		Lead l2 = new Lead(LastName='情况2',Company='celnet',status='Contacted',recordtypeid='012900000003D3x',Vertical__c = 'Game (游戏虚拟)');        
		insert l2;        
		sc = new ApexPages.StandardController(l2);        
		handover = new Handover(sc);        
		ApexPages.currentPage().getParameters().put('id', l2.id);        
		handover.updateLead();        
		Lead l3 = new Lead(LastName='情况3',Company='celnet',status='Contacted',recordtypeid='012900000003D3x',Vertical__c = 'Clothing/Shoes/Accessory (服装、鞋帽及佩饰产品)',state='广东省');        
		insert l3;        
		sc = new ApexPages.StandardController(l3);        
		handover = new Handover(sc);        
		ApexPages.currentPage().getParameters().put('id', l3.id);        
		handover.updateLead();        
		Lead l4 = new Lead(LastName='情况4',Company='celnet',status='Contacted',recordtypeid='012900000003D3x',Vertical__c = 'Clothing/Shoes/Accessory (服装、鞋帽及佩饰产品)',state='河北省');        
		insert l4;        
		sc = new ApexPages.StandardController(l4);        
		handover = new Handover(sc);        
		ApexPages.currentPage().getParameters().put('id', l4.id);        
		handover.updateLead();  
		Lead l5 = new Lead(LastName='情况5',Company='celnet',status='Contacted',recordtypeid='012900000003D3x');        
		insert l5;        
		sc = new ApexPages.StandardController(l5);        
		handover = new Handover(sc);        
		ApexPages.currentPage().getParameters().put('id', l5.id);        
		handover.updateLead();        
		Test.stopTest();    
	}
}