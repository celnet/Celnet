public class Handover {
	    public Handover(ApexPages.StandardController controller) {    
	    }            
	    public PageReference updateLead(){        
	    	Lid=ApexPages.currentPage().getParameters().get('id');        
	    	Lead lead = new Lead();        
	    	lead = [SELECT recordtypeid, state,ownerid,status,Vertical__c FROM LEAD WHERE ID=:Lid];        
	    	if(lead.Vertical__c != null){            
	    		System.debug(lead.state);            
	    		lead.status='Open';                            
	    		lead.recordtypeid='012900000002980';  
	    		lead.LeadSource='Telesales/Call Center';          
	    		if(lead.Vertical__c == 'Auto Parts & Accessory (汽车零部件及车用产品)') {                
	    			lead.ownerid='00G90000000GcQJ';            
	    		}            
	    		else if(lead.Vertical__c == 'Game (游戏虚拟)' || lead.Vertical__c == 'TECH (计算机产品及周边配件、手机产品及周边配件、其它电子类产品)') {                
	    			lead.ownerid='00G90000000FY9P';                        
	    		}            
	    		else if((lead.Vertical__c == 'Clothing/Shoes/Accessory (服装、鞋帽及佩饰产品)' || lead.Vertical__c == 'Home & Garden (家居用品)' || lead.Vertical__c == 'Outdoor & Sports (户外运动)' || lead.Vertical__c == 'Others (其它类别产品)' || lead.Vertical__c == 'Health & Beauty (健身美容)')&&(lead.state=='广东省'||lead.state=='福建省')) {                
	    			lead.ownerid='00G90000000FY9U';                        
	    		}            
	    		else {                
	    			lead.ownerid='00G90000000FY9Z';                         
	    		}              
		    	update lead;                
		    	PageReference page = new PageReference('https://ap1.salesforce.com/00Q/o');        
		    	page.setRedirect(false);        
		    	return page;  
	    	}         
	    	else {
           	    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Vertical为空，不能进行HandOver'));
          	    return ApexPages.currentPage();
	    	
	    	}       
	    }        
	    public String Lid {get;set;}    
}