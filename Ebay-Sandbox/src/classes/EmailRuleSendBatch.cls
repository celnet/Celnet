/**
 * Author: Steven
 * Date: 2014-12-10
 * Description: send EmailRule email
 **/
public with sharing class EmailRuleSendBatch implements Database.Batchable<Sobject>, Database.Stateful{
	
	// Batch start方法中的查询语句
	private String batchQuery;
	
	// 测试发送时的测试收件地址
	private String testAddress;
	
	// 当前的EmailBatch
	private EmailBatch__c eb;
	
	// 当前EmailBatch的邮件模版
	private EmailTemplate et;
	
	// 是否未失败重试的Batch
	private boolean isFailureResendBatch;
	
	/**
	 * 发送失败重试邮件，参数为EmailBatch，和 boolean值 标记为失败重试邮件
	 **/
	public EmailRuleSendBatch(EmailBatch__c emailBatch, boolean isResend){
		isFailureReSendBatch = true;
		batchQuery = 'Select sentTime__c, Subject__c, Status__c, Sener__c, Id, FailReason__c, EmailBatch__c, Content__c, Account__c ';
		batchQuery += 'From EmailLog__c Where Status__c = \'Failure\' And EmailBatch__c = \'' + emailBatch.ReEmailBatch__c + '\'';
	}

	/**
	 * 正常发送邮件，参数为EmailBatch
	 **/
	public EmailRuleSendBatch(EmailBatch__c emailBatch){
		eb = emailBatch;
		et = retrieveEmailTemplate();
		
		isFailureReSendBatch = false;
		
		if(eb.AccountId__c != null){
			batchQuery = 'Select Id, Name From Account Where Id = \'' + eb.AccountId__c + '\'';
		} else {
			batchQuery = retrieveObjectQuery('Account',true,false);
		}
	}
	
	/**
	 * 测试邮件递送，构造方法，参数为EmailBatch 和 测试邮件的地址
	 **/
	public EmailRuleSendBatch(EmailBatch__c emailBatch, String address){
		eb = emailBatch;
		et = retrieveEmailTemplate();
		testAddress = address;
		isFailureReSendBatch = false;
		
		if(eb.AccountId__c != null){
			batchQuery = 'Select Id, Name From Account Where Id =\'' + eb.AccountId__c + '\'';
		} else {
			batchQuery = retrieveObjectQuery('Account',true,false);
			batchQuery += ' limit 1';
		}
		
	}
	
	/**
	 * Batch
	 **/
	public Database.QueryLocator start(Database.BatchableContext bc){
		
		// 正常发送时设置当前EmailBatch的Status
		if(!isFailureReSendBatch)
		updateEmailBatchStatus('Processing');
		
		return Database.getQueryLocator(batchQuery);
	}
	
	public void execute(Database.BatchableContext bc, List<Sobject> scope){
		if(isFailureReSendBatch){		// 失败重发
			for(EmailLog__c el : (List<EmailLog__c>)scope){
				resendFailureEmail(el);
			}
		} else {						// 正常发送
			for(Account acc : (List<Account>)scope){
				sendAccountEmail(acc);
			}
		}
	}
	
	public void finish(Database.BatchableContext bc){
		
		// 正常发送时设置当前EmailBatch的Status
		if(!isFailureReSendBatch)
		updateEmailBatchStatus('Finish');
		
		// 发送确认邮件
		sendFinishInformEmail();
		
		// 启用下一个Batch
		checkNextBatch();
	}
	
	/**
	 * 获取对象的查询语句
	 **/
	private String retrieveObjectQuery(String sobjType, boolean isAccount, boolean isAttachment){
		if(isAccount){					// Account
			return retrieveAccountQuery();
		} else {						// Table or Attachment Sobject
			return retrieveObjectQuery(sobjType, isAttachment);
		} 
	} 
	
	private String retrieveAccountQuery(){	// isAccount = true
		String query = 'Select Id, Name From Account';
		
		Map<Integer, String> filterRuleMap = retrieveFilterRuleMap('Account', true);
		List<FilterLogic__c> filterLogics = queryFilterLogics('Account');
		
		if(!filterRuleMap.isEmpty()){
			query += retrieveQueryConditionals(filterRuleMap, filterLogics);
		}
		
		return query;
	} 
	
	private String retrieveObjectQuery(String sobjType, boolean isAttachment){
		String query = 'Select Id, Name ';
		query += retrieveQueryFields(sobjType, isAttachment);
		query +=' From ';
		query += sobjType;
		
		Map<Integer, String> filterRuleMap = retrieveFilterRuleMap(sobjType, false);			
		List<FilterLogic__c> filterLogics = queryFilterLogics(sobjType);
		
		if(!filterRuleMap.isEmpty()){
			query += retrieveQueryConditionals(filterRuleMap, filterLogics);
		}
		
		if(!isAttachment)
		query += ' limit 50';
		
		return query;
	}
	
	// 拼接查询的字段
	private String retrieveQueryFields(String sobjType, boolean isAttachment){
		String queryFields = '';
		List<FieldSetting__c> fieldSettings = queryFieldSettings(sobjType, isAttachment);
		
		for(FieldSetting__c fs : fieldSettings){
			queryFields +=  ', ';
			queryFields += fs.Field__c;
		}
		
		return queryFields;
	}
	
	// 把当前EmailRule下的 Account 或 Object 各个筛选条件 装成Map
	private Map<Integer, String> retrieveFilterRuleMap(String sobjType, boolean isAccount){
		
		Map<Integer, String> filterRuleMap = new Map<Integer, String>();
		
		List<FilterRule__c> filterRules = queryFilterRules(sobjType, isAccount);
		
		for(FilterRule__c fr : filterRules){
			filterRuleMap.put((Integer)fr.index__c, fr.SOQL__c);
		}
		
		return filterRuleMap;
	}
	
	// 查询 FieldSetting
	private List<FieldSetting__c> queryFieldSettings(String sobjType, boolean isAttachment){
		List<FieldSetting__c> fieldSettings = [Select 
												FieldLable__c, 
												Index__c, 
												Object__c, 
												Field__c
												From FieldSetting__c
												Where Object__c =: sobjType
												And isChecked__c = true
												And EmailRule__c =: eb.EmailRule__c];
		
		return fieldSettings;
	}
	
	// 查询当前EmailRule下的 Account 或 Object 各个筛选条件
	private List<FilterRule__c> queryFilterRules(String sobjType, boolean isAccount){
		
		List<FilterRule__c> filterRules = [Select index__c, Value__c, Operator__c, Field__c, FilterRule__c, SOQL__c
												From FilterRule__c 
												Where Object__c =: sobjType 
												And EmailRule__c =: eb.EmailRule__c
												And isAccount__c =: isAccount];
		
		return filterRules;
	}
	
	// 查询当前EmailRule下的 Account 或 Object 各个筛选条件的关系	
	private List<FilterLogic__c> queryFilterLogics(String sobjType){				
		List<FilterLogic__c> filterLogics = [Select index__c, Logic__c 
												From FilterLogic__c 
												Where Object__c =: sobjType
												And EmailRule__c =: eb.EmailRule__c];
		
		return filterLogics;
	}
	
	// 查询条件
	private String retrieveQueryConditionals(Map<Integer, String> filterRuleMap, List<FilterLogic__c> filterLogics){
		
		String queryConditionals = ' Where ';
		
		if(filterLogics.isEmpty()){		// 默认FilterRule各条件为 And关系
			System.debug(filterRuleMap);
			for(String filterRule : filterRuleMap.values()){
				queryConditionals += filterRule;
				queryConditionals += ' And ';
			}
			
			queryConditionals = queryConditionals.substring(0, queryConditionals.length() - 4);
			System.debug(queryConditionals);
			
		} else {						// 替换 index为各条件
			
			// 待替换的逻辑条件
			queryConditionals += filterLogics[0].Logic__c;
			
			// filterRuleMap 取 index__c 为 key，取 Field Operator Value 为 value
			List<Integer> indexes = new List<Integer>(filterRuleMap.keySet());
			
			// index的大小
			Integer length = indexes.size();
			
			// 替换
			for(Integer i = 0; i < length; i++){
				queryConditionals = queryConditionals.replaceFirst(String.valueOf(length - i), filterRuleMap.get(length - i));
			}
		}
		
		return queryConditionals;
	}
	
	/**
	 * 获取邮件模版
	 **/
	private EmailTemplate retrieveEmailTemplate(){
		
		List<EmailTemplate> ets = [Select Id, Subject, HtmlValue, BrandTemplateId From EmailTemplate Where Id =: eb.ETemplateId__c];
		
		if(ets.isEmpty()){
			return null;
		} else {
			return ets[0];
		}
	}
	
	/**
	 * 获取收件人地址
	 **/
	private String[] retrieveAddresses(Id accId){
		String[] addresses = new String[]{};
		
		if(testAddress != null){		// 测试邮件
			addresses.add(testAddress);
		} else {						// 发送给客户的联系人
			List<Contact> contacts = [Select Id, Email From Contact Where AccountId =: accId And Key_Contact__c = true];
			
			for(Contact con : contacts){
				addresses.add(con.Email);
			}
		}
		
		return addresses;
	}
	
	/**
	 * 生成邮件模版后的Table
	 **/
	private String generateDataTable(Account acc){
		return processData(eb.Object__c, acc, false);
	}
	
	/**
	 * 拼接DataTable
	 **/
	private String concatenateDataTable(Map<Integer, String> indexFieldMap, Map<Integer, String> indexLabelMap, 
																			List<Sobject> sobjs, Account acc){
		String table = concatenateThElement('Account Name');
		table += concatenateThElement('Sobject Name');
		
		for(Integer i : indexLabelMap.keySet()){
			table += concatenateThElement(indexLabelMap.get(i));
		}
		
		table = concatenateTrElement(table);
		
		for(Sobject sobj : sobjs){
			String tableRow = concatenateTdElement(acc.Name);
			tableRow += concatenateTdElement(String.valueOf(sobj.get('Name')));
			
			for(Integer i : indexFieldMap.keySet()){
				tableRow += concatenateTdElement(String.valueOf(sobj.get(indexFieldMap.get(i))));
			}
			
			table += concatenateTrElement(tableRow);
		}
		
		table = concatenateTableElement(table);
		return table;
	}
	
	private String concatenateTableElement(String content){
		return concatenateElement('table','style="border-collapse: collapse;border: 1px solid black;"',content);
	}
	
	private String concatenateTdElement(String content){
		return concatenateElement('td','style="border: 1px solid black;"', content);
	}
	
	private String concatenateTrElement(String content){
		return concatenateElement('tr', 'style="border: 1px solid black;"', content);
	}
	
	private String concatenateThElement(String content){
		return concatenateElement('th', 'style="border: 1px solid black;"', content);
	}
	
	private String concatenateElement(String tagName, String attributes, String content){
		if(content == null){
			content = '';
		}
		String element = '<' + tagName + ' ' + attributes + '>' + content + '</' + tagName + '>';
		return element;
	}
	
	/**
	 * 生成Csv附件
	 **/
	// 重发失败邮件时，取EmailLog的Attachment作为附件
	private List<Messaging.EmailFileAttachment> generateCsvAttachments(EmailLog__c el){
		List<Messaging.EmailFileAttachment> efas = new List<Messaging.EmailFileAttachment>();
		
		for(Attachment att : [Select Id, Body, Name From Attachment Where ParentId =: el.Id]){
			Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
			efa.setFileName(att.Name);
		    efa.setBody(att.Body);
		    if(att.Body != null && att.Body.size()>0)
		    efas.add(efa);
		}
		
		return efas;
	}
	
	// 正常发送时，从FieldSetting中获取对象字段后生成csv文件
	private List<Messaging.EmailFileAttachment> generateCsvAttachments(Account acc){
		
		Set<String> sobjTypes = new Set<String>();
		
		List<FieldSetting__c> fss = [Select Id, Object__c From FieldSetting__c where EmailRule__c =: eb.EmailRule__c];
		
		for(FieldSetting__c fs : fss){
			sobjTypes.add(fs.Object__c);
		}
		
		List<Messaging.EmailFileAttachment> efas = new List<Messaging.EmailFileAttachment>();
		
		String strd = system.Today().year()+'-'+system.Today().month()+'-'+system.Today().day();
		for(String sobjType : sobjTypes){
			Blob csvFile = generateCsvFile(sobjType, acc);
			
			Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
			efa.setFileName(acc.Name + '_' +sobjType + '_' + strd + '.xls');
		    efa.setBody(csvFile);
		    
		    efas.add(efa);
		}
		
		return efas;
	}
	
	private Blob generateCsvFile(String sobjType, Account acc){
		String csvString = processData(sobjType, acc, true);
		
		Blob csvFile = Blob.valueOf(csvString);
		return csvFile;
	}
	
	private String concatenateCsvFile(Map<Integer, String> indexFieldMap, Map<Integer, String> indexLabelMap, 
																						List<Sobject> sobjs, Account acc){
		String csvString = concatenateCsvValue('Account Name');
		if(sobjs == null || sobjs.size()==0)
		{
			return '';
		}
		csvString += concatenateCsvValue(sobjs[0].getSobjecttype() + ' Name');
		
		for(Integer i : indexLabelMap.keySet()){
			csvString += concatenateCsvValue(indexLabelMap.get(i));
		}
		
		csvString = csvString.subString(0, csvString.length() - 1);
		csvString = concatenateCsvRow(csvString);
		
		for(Sobject sobj : sobjs){
			String csvRow = concatenateCsvValue(acc.Name);
			csvRow += concatenateCsvValue(String.valueOf(sobj.get('Name')));
			
			for(Integer i : indexFieldMap.keySet()){
				csvRow += concatenateCsvValue(String.valueOf(sobj.get(indexFieldMap.get(i))));
			}
			
			csvRow = csvRow.substring(0, csvRow.length() - 1);
			csvString += concatenateCsvRow(csvRow);
		}
		
		csvString = '\ufeff' + csvString;		// csv字符串前加上字节序标记，Microsoft Excel
		
		return csvString;
	}
	
	private String concatenateCsvRow(String row){
		return row + '\u000a';					// 每条记录用换行符分隔
	}
	
	private String concatenateCsvValue(String value){
		if(value == null){
			value = '';
		} else if(value.contains('"')){
			value = value.replace('"','""');	// 如果csv的值存在双引号，前面加一个双引号 escape
		}
		return '"' + value + '",';				// 对每个值首尾加上双引号，避免值中存在逗号，双引号，换行符影响csv文件
	}
	
	/**
	 * 处理数据，生成 HTML table 或者 Csv文件
	 **/
	private String processData(String sobjType, Account acc, boolean isAttachment){
		if(sobjType == null)
		return '';
		
		// 查询出需要显示的列
		List<FieldSetting__c> fss = queryFieldSettings(sobjType, isAttachment);
		
		// 字段编号Map
		Map<String, Integer> orderedFieldIndexMap = new Map<String, Integer>();
		
		// 编号字段Map
		Map<Integer, String> orderedIndexFieldMap = new Map<Integer, String>();
		
		// 未编号的字段
		List<String> unorderedFields = new List<String>();
		
		// 字段标签Map
		Map<String, String> fieldLabelMap = new Map<String, String>();
		
		// 编号字段Map, 最终显示的
		Map<Integer, String> indexFieldMap = new Map<Integer, String>();
		
		// 编号标签Map, 最终显示的
		Map<Integer, String> indexLabelMap = new Map<Integer, String>();
		
		// 遍历该Sobject的FieldSetting 得到 字段编号Map，字段标签Map，未编号的字段
		for(FieldSetting__c fs : fss){
			fieldLabelMap.put(fs.Field__c, fs.FieldLable__c);
			
			if(fs.index__c == null || fs.index__c == 0){ // 判断该filter rule 是否有编号
				unorderedFields.add(fs.Field__c);
			} else {
				orderedFieldIndexMap.put(fs.Field__c, ((Integer)fs.index__c));
			}
		}
		
		System.debug(unorderedFields);
		
		// 遍历字段编号Map 得到 编号字段Map
		for(String field : orderedFieldIndexMap.keySet()){
			orderedIndexFieldMap.put(orderedFieldIndexMap.get(field), field);
		}
		
		// 对编号进行排序
		List<Integer> indexes = orderedFieldIndexMap.values();
		indexes.sort();
		
		// 最终显示的编号
		Integer ord = 1;
		
		// 得到编号字段Map 和 编号标签Map 已编号的字段在前
		for(Integer i : indexes){
			indexFieldMap.put(ord, orderedIndexFieldMap.get(i));
			indexLabelMap.put(ord, fieldLabelMap.get(orderedIndexFieldMap.get(i)));
			ord++;
		}
		
		// 得到编号字段Map 和 编号标签Map 未编号的字段在后
		for(String uf : unorderedFields){
			indexFieldMap.put(ord, uf);
			indexLabelMap.put(ord, fieldLabelMap.get(uf));
			ord++;
		}
		
		// 查询语句
		String objQuery = retrieveObjectQuery(sobjType, false, isAttachment);
		
		// 查询得到需要显示的记录
		List<Sobject> sobjs = Database.query(objQuery);
		
		if(isAttachment){	// 生成 csv附件
			return concatenateCsvFile(indexFieldMap, indexLabelMap, sobjs, acc);
		} else {					// 生成附在邮件模版后面的 Html Table
			return concatenateDataTable(indexFieldMap, indexLabelMap, sobjs, acc);
		}
	}
	
	/**
	 * 生成邮件正文
	 **/
	private String generateEmailHtmlBody(Account acc){
		
		// 生成模版后面的Table
		String dataTable = generateDataTable(acc);
		
		// 获取信头
		BrandTemplate bt;
		if(et != null && et.BrandTemplateId != null)
		{
			for(BrandTemplate bTem : [Select Id, Value From BrandTemplate Where Id =: et.BrandTemplateId])
			{
				bt = bTem;
			}
		}
		
		// 信头中模版正文的部分
		String templateBodyStyle = '<style color="#000000" font-size="12pt" background-color="#FFFFFF" font-family="arial" bLabel="main" bEditID="r3st1" >';
		templateBodyStyle += '\n</style>';
		String templateBody = templateBodyStyle + '\n<![CDATA[]]>';
		
		//system.debug('替换模版中的CDATA，显示模版正文内容' + et);
		// 替换模版中的CDATA，显示模版正文内容
		String templateHtmlValue = '';
		if(et.HtmlValue != null)
		{
			templateHtmlValue = et.HtmlValue.replace('<![CDATA[','');
			templateHtmlValue = templateHtmlValue.replace(']]>','');
		}
		String emailHtmlValue = '';
		if(bt != null)
		{
			// 替换信头中的模版正文部分，显示模版正文
			emailHtmlValue = bt.Value.replace(templateBody,templateBodyStyle + templateHtmlValue);
			
			// 替换信头中其他CDATA
			emailHtmlValue = emailHtmlValue.replace('<![CDATA[]]>','');
			
			// 替换信头中头图片和尾图片的CDATA，显示图片
			emailHtmlValue = emailHtmlValue.replace('<![CDATA[','<img src="');
			emailHtmlValue = emailHtmlValue.replace(']]>','" />');
		}
		// 返回信头 和 模版后面的Table
		return emailHtmlValue + dataTable;
	}
	
	/**
	 * 生成待发送的邮件
	 **/
	private Messaging.SingleEmailMessage generateEmail(String subject, String[] addresses, String body, List<Messaging.EmailFileAttachment> efas){
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		
		// 收件人地址
		mail.setToAddresses(addresses);
		//不生成活动
		mail.setSaveAsActivity(false);
		// 发件人显示名字
		mail.setSenderDisplayName('ebay');
		// 邮件主题
		mail.setSubject(subject);
		// 邮件正文
		mail.setHtmlBody(body);
		// 附件
		if(efas != null && !efas.isEmpty())
	    mail.setFileAttachments(new List<Messaging.EmailFileAttachment>(efas));          	
		return mail;
	}
	
	/**
	 * 发送邮件
	 **/
	private void sendAccountEmail(Account acc){
		String[] addresses = retrieveAddresses(acc.Id);
		
		if(addresses.isEmpty()) // 收件人地址为空则不发送
		return;
		
		// 生成邮件正文
		String emailBody = generateEmailHtmlBody(acc);
		
		// 生成csv文件
		List<Messaging.EmailFileAttachment> csvFileAtts = generateCsvAttachments(acc);
		
		// 发送邮件
		Messaging.SingleEmailMessage mail = generateEmail(et.Subject, addresses, emailBody, csvFileAtts);
		
		mail.setCharset('UTF-8');
		try{
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
			
			insertEmailLog(mail, acc, '');
		} catch(Exception e){
			
		    String msgError = 'A.  Exception type caught: ' + e.getTypeName();    
		    msgError += '\n\n B.  Message: ' + e.getMessage();    
		    msgError += '\n\n C.  Cause: ' + e.getCause();    // returns null
		    msgError += '\n\n D.  Line number: ' + e.getLineNumber();    
		    msgError += '\n\n E.  Stack trace: ' + e.getStackTraceString(); 
		    //msgError += '\n\n ' +string.valueOf(mail);
			insertEmailLog(mail, acc, msgError);
		}
	}
	
	/**
	 * 发送失败重试邮件
	 **/
	private void resendFailureEmail(EmailLog__c el){
		String[] addresses = retrieveAddresses(el.Account__c);
		
		if(addresses.isEmpty())
		return;
		
		List<Messaging.EmailFileAttachment> efas = generateCsvAttachments(el);
		
		Messaging.SingleEmailMessage mail = generateEmail(el.Subject__c, addresses, el.Content__c, efas);
		
		Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
	}
	
	/**
	 * 插入EmailLog
	 **/
	private void insertEmailLog(Messaging.SingleEmailMessage mail, Account acc, String errorMsg){
		if(testAddress != null)
		return;
		
		EmailLog__c el = new EmailLog__c(
			Content__c = mail.HtmlBody,
			toAddresses__c = String.valueOf(mail.toAddresses),
			FailReason__c = errorMsg,
			Sener__c = UserInfo.getUserEmail(),
			sentTime__c = Date.today(),
			Status__c = errorMsg == ''?'Success':'Failure',
			Subject__c = mail.Subject,
			Account__c = acc.Id,
			EmailBatch__c = eb.Id
		);
		
		insert el;
		
		// 插入附件到EmailLog
		List<Attachment> attList = new List<Attachment>();
		for(Messaging.EmailFileAttachment efa : mail.FileAttachments){
			Attachment att = new Attachment(
					ParentId = el.Id,
					Body = efa.Body,
					Name = efa.FileName
				);
			if(efa.Body != null)attList.add(att);
		}
		
		if(!attList.isEmpty()){
			insert attList;
		}
	}
	
	/**
	 * 更新EmailBatch状态
	 **/ 
	private void updateEmailBatchStatus(String status){
		if(testAddress != null)
		return;
		eb.Status__c = status;
		update eb;
	}
	
	/**
	 * 发送成功提醒邮件
	 **/
	private void sendFinishInformEmail(){
		
	}
	
	/**
	 * 检查EmailBatch和EmailLog，启用下一个Batch
	 **/
	private void checkNextBatch(){
		List<String> jobStatus = new List<String>{'Preparing','Queued','Processing'};
		List<AsyncApexJob> asyncjobs = [Select Id From AsyncApexJob Where Status IN: jobStatus And JobType = 'BatchApex'];
		
		if(asyncjobs.size() < 5){
			// 查询系统中待发送下的 EmailBatch
			List<EmailBatch__c> pendingEmailBatches = [Select Object__c, ETemplateId__c, EmailRule__c, AccountId__c 
														From EmailBatch__c 
														Where Status__c = 'IsPending'];
			
			if(!pendingEmailBatches.isEmpty()){			// 启用新的Batch继续Run下一个EmailBatch
				EmailRuleSendBatch ersb = new EmailRuleSendBatch(pendingEmailBatches[0]);
				Database.executeBatch(ersb,1);
			} 
		} else {
			// 当前运行的 Batch 超过了5个
		}
	}
}