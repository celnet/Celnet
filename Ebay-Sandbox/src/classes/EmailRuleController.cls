/********************************************
 *20141209
 *Spring
 *邮件规则控制类
 ********************************************/
public with sharing class EmailRuleController {
	//@saveOrUpdate 需要持久化到 DB
	//@saveOrUpdate1
	public EmailRule__c emailRule{get;set;}
	//编辑还是新建 true=>edit false=>create
	public Boolean isEdit{get;set;}
	//显示运算符
	public list<SelectOption> operatorList{get;set;}
	//客户对象 字段
	public list<SelectOption> AccFieldList{get;set;}
	//@saveOrUpdate2 客户对象 选择的字段
	public List<FilterRule__c> AccSelectFieldList{get;set;}
	//@saveOrUpdate3 客户对象 逻辑
	public FilterLogic__c AccLogic{get;set;}
	//显示隐藏
	public Integer showFlag{get;set;}
	//筛选的sObject
	public list<SelectOption> selectObjectList{get;set;}
	
	//筛选对象 字段
	public list<SelectOption> SelectObjFieldList{get;set;}
	//@saveOrUpdate4筛选对象 选择的字段
	public List<FilterRule__c> findObjFieldList{get;set;}
	//@saveOrUpdate5筛选对象 逻辑
	public FilterLogic__c SelectObjLogic{get;set;} 
	
	//@saveOrUpdate6 发送Email中 正文中需要添加的字段
	public List<FieldSetting__c> sendEmailFieldList{get;set;}
	//发送Email中  附件CSV中显示字段  供用户选择
	public List<FieldSetting__c> sendEmailAttachList{get;set;}
	//发送Email中  附件CSV中显示字段 之前选择了的 key:objectName value:object 勾选了那些字段 所有操作
	//都要修改map 最后保存时是保存Map
	//@saveOrUpdate7
	public Map<String,List<FieldSetting__c>> sendEmailAttachMap{get;set;}
	//发送Email中 附件选择对象名
	public String sendEmailAttachObjName{get;set;}
	//发送Email中 附件选择对象时，罗列出选择了那些对象和字段
	public List<assist> sendEmailAssistList{get;set;}
	
	//选择的电子邮件模板
	//public String sendEmailTemplate{get;set;} 
	//可用的电子邮件模板
	public list<SelectOption> sendEmailTemplateList{get;set;}
	
	//测试发送邮件的EmailAddress
	public String TestEmailAddress{get;set;}
	
	//第二个步骤中客户的删除
	//(大于 5个)id 不为空 且3个字段有一个为空 则做删除处理【放外面主要是在移除行时放入集合】
    public List<FilterRule__c> delAccountList = new List<FilterRule__c>();
    
    //第三个步骤中对象字段的删除
    //(大于 5个)id 不为空 且3个字段有一个为空 则做删除处理【放外面主要是在移除行时放入集合】
    public List<FilterRule__c> delScreenObjectList = new List<FilterRule__c>();
    
    //当运行Batch时，运行按钮，测试按钮 禁用
    public boolean buttonStauts{get;set;}
    //保存按钮
	public boolean saveButtonStauts{get;set;}
	
	
//*********************Step1*****************************************
	
	public EmailRuleController(){
		String id = Apexpages.currentPage().getParameters().get('id');
		List<EmailRule__c> temp_emailRule = [select id,Name,Description__c,Object__c,EmailTemplateId__c  from EmailRule__c where id =:id];
		if(temp_emailRule != null && temp_emailRule.size()>0){
			 System.debug('编辑:EmailRuleId=>'+id);
			 emailRule = temp_emailRule[0];
			 isEdit = true;
			 //sep:2 准备数据 -----------start
			  System.debug('sep2=>开始');
			 List<FilterLogic__c> tempAcc = [select id,Logic__c,Object__c from FilterLogic__c where EmailRule__c =:id and Object__c = 'Account'];
			 if(tempAcc == null || tempAcc.size() ==0){
			 	AccLogic = new FilterLogic__c();
			 	AccLogic.EmailRule__c = emailRule.Id;
			 	AccLogic.Object__c = 'Account';
			 }else{
			 	AccLogic = tempAcc[0];
			 }
			 
			 AccSelectFieldList = [select id,Field__c,Index__c,Operator__c,FieldAndFieldType__c,Value__c,isAccount__c from FilterRule__c where EmailRule__c =:id and Object__c = 'Account' and isAccount__c = true order by Index__c];
			 //不足5个补全
			 integer acclen = AccSelectFieldList.size()+1;
			 for(integer i = acclen;i<=5;i++){
			 	FilterRule__c  fr = new FilterRule__c();
				fr.Index__c = i;
				fr.Object__c = 'Account';
				fr.EmailRule__c = emailRule.id;
				//当sep3选择了客户对象时，条件就乱了
				fr.isAccount__c = true;
				AccSelectFieldList.add(fr);
			 }
			 
			 //sep:2 准备数据 -----------end
			
			 //sep:3 准备数据 -----------start
			 System.debug('sep3=>开始');
			 findObjFieldList = [select id,Field__c,Index__c,Operator__c,Value__c,FieldAndFieldType__c,isAccount__c from FilterRule__c where EmailRule__c =:id and Object__c =:emailRule.Object__c and isAccount__c = false order by Index__c];
			 //不足5个补全
			 integer len = findObjFieldList.size()+1;
			 for(integer i = len;i<=5;i++){
			 	FilterRule__c  fr = new FilterRule__c();
				fr.Index__c = i;
				fr.Object__c = emailRule.Object__c;
				fr.EmailRule__c = emailRule.Id;
				fr.isAccount__c = false;
				findObjFieldList.add(fr);
			 }
			 List<FilterLogic__c> temp = [select id,Logic__c,Object__c from FilterLogic__c where EmailRule__c =:id and Object__c =:emailRule.Object__c ];
			 if(temp == null || temp.size() ==0){
			 	SelectObjLogic = new FilterLogic__c();
			 	SelectObjLogic.EmailRule__c = emailRule.Id;
			 	SelectObjLogic.Object__c = emailRule.Object__c;
			 }else{
			 	SelectObjLogic = temp[0];
			 }
			 
			 SelectObjFieldList = getObjField(emailRule.Object__c);
			 //sep:3 准备数据 -----------end
			 //sep:4 准备数据 -----------start
			 System.debug('sep4=>开始');
			 sendEmailFieldList = new List<FieldSetting__c>();
			 
			 List<FieldSetting__c> sendEmailFieldTemp = [Select f.Object__c, f.Id,f.FieldLable__c, f.Field__c,Index__c, f.EmailRule__c,isChecked__c,isAttachment__c From FieldSetting__c f where f.EmailRule__c =: emailRule.Id and Object__c =:emailRule.Object__c and isAttachment__c = false order by Index__c];
			 List<String []> objFieldName = getObjFieldNameList(emailRule.Object__c);
			 boolean isExist = false;
			 //判断那些属性是之前选择过的，没有的补全
			 for(String[] s : objFieldName){
			 	isExist = false;
			 	String lable= s[1];
			 	String apiName = s[0];
			 	FieldSetting__c addFiled;
			 	for(FieldSetting__c obj : sendEmailFieldTemp){
			 		if(apiName == obj.Field__c){
			 			addFiled = obj;
			 			isExist = true;
			 		}
			 		if(isExist)break;
			 	}
			 	if(!isExist){
			 		addFiled = new FieldSetting__c();
			 		addFiled.Object__c = emailRule.Object__c;
			 		addFiled.EmailRule__c = emailRule.id;
			 		addFiled.isChecked__c = false;
			 		addFiled.Field__c = apiName;
			 		addFiled.FieldLable__c = lable;
			 		addFiled.Index__c = 0;
			 		addFiled.isAttachment__c = false;
			 	}
			 	sendEmailFieldList.add(addFiled);
			 }
			 //sep:4 准备数据 -----------end
			 
			 //sep:5 准备数据 -----------start
			 System.debug('sep5=>开始');
			 sendEmailAttachMap = new Map<String,List<FieldSetting__c>>();
			 sendEmailAssistList = new List<Assist>();
			 List<FieldSetting__c> sendEmailAttachTemp = [Select f.Object__c, f.Id,f.FieldLable__c, f.Field__c,Index__c, f.EmailRule__c,isChecked__c,isAttachment__c From FieldSetting__c f where f.EmailRule__c =: emailRule.Id and isAttachment__c = true order by Index__c];
			 //先拿到之前勾选过的所有对象,value:初始化
			 for(FieldSetting__c obj : sendEmailAttachTemp){
			 	sendEmailAttachMap.put(obj.Object__c,new List<FieldSetting__c>());
			 }
			 //拿到每个对象勾选过的字段
			 for(FieldSetting__c obj : sendEmailAttachTemp){
			 	sendEmailAttachMap.get(obj.Object__c).add(obj);
			 }
			 //默认为当前选择对象
			 sendEmailAttachObjName = emailRule.Object__c;
			 List<String []> objFieldNameAttach = getObjFieldNameList(emailRule.Object__c);
			 boolean isExistAttach = false;
			 //判断那些属性是之前选择过的，没有的补全
			 //当Map中不存在记录时
			 //补全显示当前对象，后面的采用当选择不同对象时，再处理
			 sendEmailAttachList = sendEmailAttachMap.get(emailRule.Object__c);
			 if(sendEmailAttachList == null){
			 	sendEmailAttachList = new List<FieldSetting__c>();
			 }
			
			 
			 for(String[] s : objFieldNameAttach){
			 	String lable= s[1];
			 	String apiName = s[0];
			 	FieldSetting__c addFiled;
			 	for(FieldSetting__c obj : sendEmailAttachList){
			 		if(apiName == obj.Field__c){
			 			addFiled = obj;
			 			isExist = true;
			 		}
			 		if(isExistAttach)break;
			 	}
			 	if(!isExistAttach){
			 		addFiled = new FieldSetting__c();
			 		addFiled.Object__c = emailRule.Object__c;
			 		addFiled.EmailRule__c = emailRule.id;
			 		addFiled.isChecked__c = false;
			 		addFiled.Field__c = apiName;
			 		addFiled.FieldLable__c = lable;
			 		addFiled.Index__c = 0;
			 		addFiled.isAttachment__c = true;
			 	}
			 	sendEmailAttachList.add(addFiled);
			 }
			 //放入集合中
			 List<FieldSetting__c> joinSE = new List<FieldSetting__c>(sendEmailAttachList);
			 sendEmailAttachMap.put(emailRule.Object__c,joinSE);
			 //组装 前台显示需要
			 getAssist();
			 //sep:5 准备数据 -----------end
		}else{
			emailRule = new EmailRule__c();
			isEdit = false;
			//sep:2  准备数据 -----------start
			//初始化5个
			AccSelectFieldList = new List<FilterRule__c>();
			for(integer i = 1;i<=5;i++){
				FilterRule__c fr = new FilterRule__c();
				fr.Index__c = i;
				fr.Object__c = 'Account';
				fr.isAccount__c = true;
				AccSelectFieldList.add(fr);
			}
			AccLogic = new FilterLogic__c();
			AccLogic.Object__c = 'Account';
			//sep:2  准备数据 -----------start
			
			findObjFieldList = new List<FilterRule__c>();
			SelectObjLogic = new FilterLogic__c();
			sendEmailFieldList = new List<FieldSetting__c>();
			sendEmailAttachMap = new Map<String,List<FieldSetting__c>>();
			sendEmailAssistList = new List<Assist>();
		}
		
		showFlag = 1;
		operatorList = new list<SelectOption>();
		initOperator();
		AccFieldList = getObjField('Account');
		selectObjectList = getExportObject();
		sendEmailTemplateList = getEmailTemplate();
		buttonStauts = false;
		saveButtonStauts = false;
	}
	
	
	public void NewEmailRuleNext(){
		showFlag = 2;
		//如果是新建，则点next时，要改变 step3中对象的属性
		if(!isEdit){
			if(emailRule.Object__c == 'none'){
				showFlag = 1;
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,'请选择一个对象');
        		ApexPages.addMessage(msg);
				return;
			}
			//sep:3 准备数据 -----------start
			findObjFieldList.clear();
			for(integer i = 1;i<=5;i++){
			 	FilterRule__c  fr = new FilterRule__c();
				fr.Index__c = i;
				fr.Object__c = emailRule.Object__c;
				fr.isAccount__c = false;
				findObjFieldList.add(fr);
			 }
			
			//save时再去赋值id
		 	//SelectObjLogic.EmailRule__c = emailRule.Id;
		 	SelectObjLogic.Object__c = emailRule.Object__c;
		 	
		 	SelectObjFieldList = getObjField(emailRule.Object__c);
		 	//sep:3 准备数据 -----------end
		 	
		 	//sep:4 准备数据 -----------start
		 	sendEmailFieldList.clear();
		 	 List<String []> objFieldName = getObjFieldNameList(emailRule.Object__c);
		 	for(String[] s : objFieldName){
			 	String lable= s[1];
			 	String apiName = s[0];
			 	FieldSetting__c addFiled = new FieldSetting__c();
			 	addFiled = new FieldSetting__c();
		 		addFiled.Object__c = emailRule.Object__c;
		 		addFiled.isChecked__c = false;
		 		addFiled.Field__c = apiName;
		 		addFiled.FieldLable__c = lable;
		 		addFiled.Index__c = 0;
		 		addFiled.isAttachment__c = false;
		 		sendEmailFieldList.add(addFiled);
		 	}
		 	//sep:4 准备数据 -----------end
			//sep:5 准备数据 -----------start
			 sendEmailAssistList.clear();
			 sendEmailAttachObjName = emailRule.Object__c;
			 List<String []> objFieldNameAttach = getObjFieldNameList(emailRule.Object__c);
			 sendEmailAttachList = sendEmailAttachMap.get(emailRule.Object__c);
			 if(sendEmailAttachList == null){
			 	sendEmailAttachList = new List<FieldSetting__c>();
			 }
			 
			 for(String[] s : objFieldNameAttach){
			 	String lable= s[1];
			 	String apiName = s[0];
			 	FieldSetting__c addFiled = new FieldSetting__c();
		 		addFiled.Object__c = emailRule.Object__c;
		 		addFiled.EmailRule__c = emailRule.id;
		 		addFiled.isChecked__c = false;
		 		addFiled.Field__c = apiName;
		 		addFiled.FieldLable__c = lable;
		 		addFiled.Index__c = 0;
		 		addFiled.isAttachment__c = true;
			 	sendEmailAttachList.add(addFiled);
			 }
			 //放入集合中
			 List<FieldSetting__c> joinSE = new List<FieldSetting__c>(sendEmailAttachList);
			 sendEmailAttachMap.put(emailRule.Object__c,joinSE);
			 //组装 前台显示需要
			 getAssist();
			//sep:5 准备数据 -----------end
		}
	}
	
//*********************Step2*****************************************	
	public void ChoiceAccountsNext(){
		showFlag = 3;
	}
	
	public void ChoiceAccountsPrv(){
		showFlag = 1;
	}
	//添加行 max = 10
	public void ChoiceAccountsAddRow(){
		if(AccSelectFieldList.size()<10){
			FilterRule__c  fr = new FilterRule__c();
			fr.Index__c = AccSelectFieldList.size()+1;
			fr.Object__c = 'Account';
			fr.isAccount__c = true;
			AccSelectFieldList.add(fr);
		}
	}
	//移除最后一行，min=5
	public void ChoiceAccountsRemoveRow(){
		if(AccSelectFieldList.size()>5){
			//加入删除的集合
			FilterRule__c reFR = AccSelectFieldList.get(AccSelectFieldList.size() - 1);
			//当移除的对象id不为空时才放入
			if(reFR.id !=null){
				delAccountList.add(reFR);
			}
			AccSelectFieldList.remove(AccSelectFieldList.size()-1);
		}
	}
//*********************Step3*****************************************	
	public void ScreenObjectNext(){
		showFlag = 4;
	}
	
	public void ScreenObjectPrv(){
		showFlag = 2;
	}
	
	public void ScreenObjectAddRow(){
		if(findObjFieldList.size()<10){
			FilterRule__c  fr = new FilterRule__c();
			fr.Index__c = findObjFieldList.size()+1;
			fr.Object__c = emailRule.Object__c;
			fr.EmailRule__c = emailRule.Id;
			findObjFieldList.add(fr);
		}
	}
	
	public void ScreenObjectRemoveRow(){
		if(findObjFieldList.size()>5){
			FilterRule__c fr = findObjFieldList.get(findObjFieldList.size()-1);
			if(fr.id !=null){
				delScreenObjectList.add(fr);
			}
			findObjFieldList.remove(findObjFieldList.size() - 1);
		}
	}
	
//*********************Step4*****************************************
	public void ChioceObjectFieldNext(){
		showFlag = 5;
	}
	
	public void ChioceObjectFieldPrv(){
		showFlag = 3;
	}

//*********************Step5*****************************************
	public void ChioceObjectNext(){
		showFlag = 6;
	}
	
	public void ChioceObjectPrv(){
		showFlag = 4;
	}
	
	
	public void changeFieldByObjName(){
		if(sendEmailAttachObjName == 'none'){
			return;
		}
		//sendEmailAttachList 用户勾选的先替换到sendEmailAttachMap 再清空
		if(sendEmailAttachList.size()!=0){
			FieldSetting__c fs = sendEmailAttachList.get(0);
			String objName = fs.Object__c;
			List<FieldSetting__c> joinSE_temp = new List<FieldSetting__c>(sendEmailAttachList);
			sendEmailAttachMap.put(objName,joinSE_temp);
			System.debug('joinSE_temp:'+joinSE_temp);
		}
		sendEmailAttachList.clear();
		List<String[]> getFieldByObjName = getObjFieldNameList(sendEmailAttachObjName);
		//数据库中存在的
		List<FieldSetting__c> temp = sendEmailAttachMap.get(sendEmailAttachObjName);
		if(temp == null){
			temp = new List<FieldSetting__c>();
		}
		boolean isExistAttach = false;
		 //判断那些属性是之前选择过的，没有的补全
		 //当数据库中不存在记录时
		 for(String[] s : getFieldByObjName){
		 	isExistAttach = false;
		 	String lable= s[1];
		 	String apiName = s[0];
		 	FieldSetting__c addFiled;
		 	for(FieldSetting__c obj : temp){
		 		if(apiName == obj.Field__c){
		 			addFiled = obj;
		 			isExistAttach = true;
		 		}
		 		if(isExistAttach)break;
		 	}
		 	if(!isExistAttach){
		 		addFiled = new FieldSetting__c();
		 		addFiled.Object__c = emailRule.Object__c;
		 		addFiled.EmailRule__c = emailRule.id;
		 		addFiled.isChecked__c = false;
		 		addFiled.Field__c = apiName;
		 		addFiled.FieldLable__c = lable;
		 		addFiled.Index__c = 0;
		 	}
		 	sendEmailAttachList.add(addFiled);
		 }
		 //放入集合中
		 List<FieldSetting__c> joinSE = new List<FieldSetting__c>(sendEmailAttachList);
		 sendEmailAttachMap.put(sendEmailAttachObjName,joinSE);
		 //组装 前台显示需要
		 getAssist();
	}
//*********************Step6*****************************************

	public void EmailTemplatePrv(){
		showFlag = 5;
	}
	
	//持久化到DB  先验证所有字段是否正确
	public void save(){
        String errorMsg = '';
        boolean errorFlag = false;
        //邮件模板
        //emailRule.EmailTemplateId__c = sendEmailTemplate;
        //emailRule 的验证
        if(emailRule.Object__c == null || emailRule.Object__c.trim() == 'none'){
        	errorMsg = 'EmailRule__c 对象中 Object__c 字段不能为none';
        	showFlag = 1;
        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,errorMsg);
        	ApexPages.addMessage(msg);
        	return;
        }
        
        if(emailRule.EmailTemplateId__c == null || emailRule.EmailTemplateId__c.trim() == 'none'){
        	errorMsg = '请选择电子邮件模板';
        	showFlag = 6;
        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,errorMsg);
        	ApexPages.addMessage(msg);
        	return;
        }
        try{
        	upsert emailRule;
        }catch(Exception e){
        	errorFlag = true;
        }
        
        if(errorFlag){
        	errorMsg = 'EmailRule__c 规则创建失败!';
        	showFlag = 1;
        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,errorMsg);
        	ApexPages.addMessage(msg);
        	return;
        }
        
        //sep:2 AccSelectFieldList and AccLogic 验证
        //3个字段不为空 则做upset 操作
        List<FilterRule__c> upsetList = new List<FilterRule__c>();
        for(FilterRule__c obj :AccSelectFieldList){
        	if(obj.FieldAndFieldType__c != 'none' && obj.Operator__c !='none'){
        		//处理 下拉列表中FieldAndFieldType__c值是由" FieldName=FieldType
        		String[] temp = obj.FieldAndFieldType__c.split('=');
        		obj.Field__c = temp[0];
        		obj.Value__c=joinStringByType(temp[1],obj.Value__c);
        		upsetList.add(obj);
        	}else if(obj.id != null && (obj.FieldAndFieldType__c == 'none' || obj.Operator__c == 'none')){
        		delAccountList.add(obj);
        	}
        }
        //为空 默认为and 
        if(AccLogic.Logic__c == null || AccLogic.Logic__c.trim() ==''){
        	String logic = '(';
        	if(upsetList.size()>0){
        		for(Integer i = 0;i<upsetList.size();i++){
    				FilterRule__c f = upsetList.get(i);
        			if(i == upsetList.size() -1){
        				logic += f.index__c +' ) ';
        				continue;
        			}
    				logic += f.index__c +' and ';
        		}
        	}
        	AccLogic.Logic__c = logic;
        }
        //拼接sql语句去查询，如果异常则提示用户Logic有错误
        //替换[1 and 2(or(3,4,5))]
        String accWhere = AccLogic.Logic__c;
        Map<Integer,String> filterRuleMap = new Map<Integer,String>();
        for(Integer i = 0;i<upsetList.size();i++){
        	FilterRule__c a =upsetList.get(i);
        	filterRuleMap.put((Integer)a.index__c,' ( '+a.Field__c+' '+a.Operator__c+' '+a.Value__c + ' ) ');
        }
        List<Integer> indexes = new List<Integer>(filterRuleMap.keySet());
        Integer length = indexes.size();
        // 替换
		for(Integer i = 0; i < length; i++){
			accWhere = accWhere.replaceFirst(String.valueOf(length - i), filterRuleMap.get(length - i));
		}
		
        String checkWhere ='';
        if(upsetList.size()>0){
        	checkWhere = ' select id from Account where '+accWhere +' limit 1';
        }else{
        	checkWhere = ' select id from Account limit 1';
        }
        System.debug('checkWhere=>'+checkWhere);
        try{
        	List<sObject> L = Database.query(checkWhere);
        }catch(Exception e){
        	errorFlag = true;
        }
        if(errorFlag){
        	showFlag = 2;
        	errorMsg = '验证失败,第二步请检查逻辑语句或者字段的筛选值是否合理,所有字符只能是英文字符';
        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,errorMsg);
        	ApexPages.addMessage(msg);
        	return;
        }
        
        //更新
        try{
        	//新建时，把与 EmailRule相关的对象关联起来
        	if(!isEdit){
        		for(Integer i = 0;i<upsetList.size();i++){
		        	FilterRule__c a =upsetList.get(i);
		        	a.EmailRule__c = emailRule.id;
        		}
        		
        		AccLogic.EmailRule__c = emailRule.id;
        		
        	}
        	 upsert upsetList;
        	 if(upsetList.size()>0){
        	 	upsert AccLogic;
        	 }else{
        	 	AccLogic.Logic__c = '';
        	 	if(AccLogic.id !=null){
        	 		delete AccLogic;
        	 	}
        	 }
        	 //去重复
        	 Map<String,FilterRule__c> temp_cf = new Map<String,FilterRule__c>();
        	 for(FilterRule__c cf : delAccountList){
        	 	temp_cf.put(cf.id,cf);
        	 }
        	 delAccountList = temp_cf.values();
        	 temp_cf.clear();
        	 delete delAccountList;
        	 upsetList.clear();
        	 delAccountList.clear();
        }catch(Exception e){
        	errorFlag = true;
        }
       
       if(errorFlag){
        	showFlag = 2;
        	errorMsg = '客户筛选条件持久化失败!';
        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,errorMsg);
        	ApexPages.addMessage(msg);
        	return;
        }
        
        //sep:3  findObjFieldList  SelectObjLogic 持久化
        List<FilterRule__c> upsetObjList = new List<FilterRule__c>();
        for(FilterRule__c obj :findObjFieldList){
        	if(obj.FieldAndFieldType__c != 'none' && obj.Operator__c !='none' ){
        		String[] temp = obj.FieldAndFieldType__c.split('=');
        		obj.Field__c = temp[0];
        		obj.Value__c=joinStringByType(temp[1],obj.Value__c);
        		upsetObjList.add(obj);
        	}else if(obj.id != null && (obj.Field__c == 'none' || obj.Operator__c == 'none')){
        		delScreenObjectList.add(obj);
        	}
        }
        //为空 默认为and 
        if(SelectObjLogic.Logic__c == null || SelectObjLogic.Logic__c.trim() ==''){
        	String logicObj = '(';
        	if(upsetObjList.size()>0){
        		for(Integer i = 0;i<upsetObjList.size();i++){
    				FilterRule__c f = upsetObjList.get(i);
        			if(i == upsetObjList.size() -1){
        				logicObj += f.index__c +' ) ';
        				continue;
        			}
    				logicObj += f.index__c +' and ';
        		}
        	}
        	SelectObjLogic.Logic__c = logicObj;
        }
        //拼接sql语句去查询，如果异常则提示用户Logic有错误
        //替换[1 and 2(or(3,4,5))]
        String ObjeWhere = SelectObjLogic.Logic__c;
        Map<Integer,String> SelectMap = new Map<Integer,String>();
        for(Integer i = 0;i<upsetObjList.size();i++){
        	FilterRule__c a =upsetObjList.get(i);
        	SelectMap.put((Integer)a.index__c,' ( '+a.Field__c+' '+a.Operator__c+' '+a.Value__c + ' ) ');
        }
        List<Integer> Selectindexes = new List<Integer>(SelectMap.keySet());
        Integer selectlength = Selectindexes.size();
        // 替换
		for(Integer i = 0; i < selectlength; i++){
			ObjeWhere = ObjeWhere.replaceFirst(String.valueOf(selectlength - i), SelectMap.get(selectlength - i));
		}
        String checkObjWhere = '';
        if(upsetObjList.size()>0){
        	checkObjWhere = ' select id from '+emailRule.Object__c+' where '+ObjeWhere +' limit 1';
        }else{
        	checkObjWhere = ' select id from '+emailRule.Object__c+' limit 1';
        }
        System.debug('checkObjWhere=>'+checkObjWhere);
        try{
        	List<sObject> L = Database.query(checkObjWhere);
        }catch(Exception e){
        	errorFlag = true;
        }
        if(errorFlag){
        	showFlag = 3;
        	errorMsg = '验证失败,第三步请检查逻辑语句字段的筛选值是否合理,所有字符只能是英文字符';
        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,errorMsg);
        	ApexPages.addMessage(msg);
        	return;
        }
        //更新
        try{
        	 if(!isEdit){
        	 	//新建时，把与 EmailRule相关的对象关联起来
        	 	for(Integer i = 0;i<upsetObjList.size();i++){
		        	FilterRule__c a =upsetObjList.get(i);
		        	a.EmailRule__c = emailRule.id;
        		}
        		
        		SelectObjLogic.EmailRule__c = emailRule.id;
        	 }
        	 upsert upsetObjList;
        	 if(upsetObjList.size()>0){
        	 	upsert SelectObjLogic;
        	 }else{
        	 	SelectObjLogic.Logic__c = '';
        	 	if(SelectObjLogic.id != null){
        	 		delete SelectObjLogic;
        	 	}
        	 }
        	 //去重复
        	 Map<String,FilterRule__c> temp_cf_1 = new Map<String,FilterRule__c>();
        	 for(FilterRule__c cf : delScreenObjectList){
        	 	temp_cf_1.put(cf.id,cf);
        	 }
        	 delAccountList = temp_cf_1.values();
        	 temp_cf_1.clear();
        	 delete delScreenObjectList;
        	 upsetObjList.clear();
        	 delScreenObjectList.clear();
        }catch(Exception e){
        	errorFlag = true;
        }
       
       if(errorFlag){
        	showFlag = 3;
        	errorMsg = '自定义对象筛选条件持久化失败!';
        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,errorMsg);
        	ApexPages.addMessage(msg);
        	return;
        }
        
        //验证 sendEmailFieldList
        List<FieldSetting__c> upsertEmailFieldList = new List<FieldSetting__c>();
        List<FieldSetting__c> delEmailFieldList = new List<FieldSetting__c>();
        for(FieldSetting__c a : sendEmailFieldList){
        	//只有勾选的加入
        	if(a.isChecked__c){
        		//关联主对象
        		if(!isEdit){
        			a.EmailRule__c = emailRule.id;
        		}
        		upsertEmailFieldList.add(a);
        	}else{
        		//没有勾选且id不为null的要删除
        		if(a.id !=null){
        			delEmailFieldList.add(a);
        		}
        	}
        }
        try{
        	upsert upsertEmailFieldList;
        	delete delEmailFieldList;
        	upsertEmailFieldList.clear();
        	delEmailFieldList.clear();
        }catch(Exception e){
        	errorFlag = true;
        }
        if(errorFlag){
        	showFlag = 4;
        	errorMsg = '自定义对象发送邮件正文字段持久化失败!';
        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,errorMsg);
        	ApexPages.addMessage(msg);
        	return;
        }
        
        //验证 sendEmailAttachMap
        List<FieldSetting__c> upsertEmailAttList = new List<FieldSetting__c>();
        List<FieldSetting__c> delEmailAttList = new List<FieldSetting__c>();
        Set<String> keys = sendEmailAttachMap.keySet();
        for(String key : keys){
        	List<FieldSetting__c> objFiledList = sendEmailAttachMap.get(key);
        	for(FieldSetting__c tempFile : objFiledList){
	        	//只有勾选的加入
	        	if(tempFile.isChecked__c){
	        		if(!isEdit){
	        			tempFile.EmailRule__c = emailRule.id;
	        		}
	        		upsertEmailAttList.add(tempFile);
	        	}else{
	        		//没有勾选且id不为null的要删除
	        		if(tempFile.Id !=null){
	        			delEmailAttList.add(tempFile);
	        		}
	        	}
        	}
        }
        
        try{
        	upsert upsertEmailAttList;
        	delete delEmailAttList;
        	upsertEmailAttList.clear();
        	delEmailAttList.clear();
        }catch(Exception e){
        	errorFlag = true;
        }
        if(errorFlag){
        	showFlag = 5;
        	errorMsg = '自定义对象发送邮件附件段持久化失败!';
        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,errorMsg);
        	ApexPages.addMessage(msg);
        	return;
        }
        //数据保存后，不允许再保存
        saveButtonStauts = true;
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info,'邮件规则配置成功!');
        ApexPages.addMessage(msg);
	}
	
	public void EmailTemplateNext(){
		showFlag = 7;
	}
	
	
//*********************Step7*****************************************
	public void RunBatchEmailPrv(){
		showFlag = 6;
	}
	
	public void RunBatchEmail(){
		//当数据准备才能运行
		if(emailRule.id == null){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,'请先保存数据!');
        	ApexPages.addMessage(msg);
        	showFlag = 6;
        	return;
		}
		EmailBatch__c emailBatch = new EmailBatch__c();
		emailBatch.EmailRule__c = emailRule.id;
		try{
			buttonStauts = true;
			saveButtonStauts = true;
			List<AsyncApexJob> batchSize = [Select a.Id From AsyncApexJob a where Status = 'Queued' or Status = 'Processing' or Status = 'Preparing' ];
			System.debug('当前系统待运行Batch数量:'+batchSize.size());
			if(batchSize.size()<5){
				emailBatch.Status__c = 'IsPending';
			}else{
				emailBatch.Status__c = 'New';
			}
			insert emailBatch;
			System.debug('xxx:'+buttonStauts);
			//调Batch
			//EmailRuleSendBatch attBatch = new EmailRuleSendBatch(emailBatch);
        	//database.executeBatch(attBatch,1);
			
		}catch(Exception e){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,'Batch运行失败!');
        	ApexPages.addMessage(msg);
		}
		
		
	}
	
	public void TestEmail(){
		if(TestEmailAddress == null || TestEmailAddress.trim() == ''){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,'TestEmail Address 不能为空!');
        	ApexPages.addMessage(msg);
		}else{
			//当数据准备才能运行
			if(emailRule.id == null){
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,'请先保存数据!');
	        	ApexPages.addMessage(msg);
	        	showFlag = 6;
	        	return;
			}
			try{
				buttonStauts = true;
				List<AsyncApexJob> batchSize = [Select a.Id From AsyncApexJob a where Status = 'Queued' or Status = 'Processing' or Status = 'Preparing' ];
				System.debug('当前系统待运行Batch数量:'+batchSize.size());
				
				//调Batch 测试
				//EmailRuleSendBatch attBatch = new EmailRuleSendBatch(emailBatch,TestEmailAddress);
        		//database.executeBatch(attBatch,1);
			}catch(Exception e){
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error,'Batch运行失败!');
	        	ApexPages.addMessage(msg);
			}
		}
	}

//*********************public*****************************************
	//获得运算符
	public void initOperator(){
		operatorList.add(new SelectOption('none','--无--'));
		operatorList.add(new SelectOption('=','等于'));
		operatorList.add(new SelectOption('!=','不等于'));
		/*
		operatorList.add(new SelectOption('%','起始字符'));
		operatorList.add(new SelectOption('4','包含'));
		operatorList.add(new SelectOption('5','不包含'));
		*/
		operatorList.add(new SelectOption('<','小于'));
		operatorList.add(new SelectOption('>','大于'));
		operatorList.add(new SelectOption('<=','小于或等于'));
		operatorList.add(new SelectOption('>=','大于或等于'));
	}
	
	//获得对象的属性
	public list<SelectOption>  getObjField(String objName){
		list<SelectOption> ope=new list<SelectOption>();
		Schema.DescribeSobjectResult[] results = Schema.describeSObjects(new String[]{objName});
		Schema.DescribeSobjectResult res = results[0];
		ope.add(new SelectOption('none','--无--'));
		for(Schema.SObjectField field : res.fields.getMap().values())
		{
			DescribeFieldResult f = field.getDescribe();
			ope.add(new SelectOption(f.getName()+'='+f.getType(),f.getLabel()));
		}
		return ope;
	} 
	//获得对象的属性
	public list<String[]> getObjFieldNameList(String objName){
		list<String[]> ope=new list<String[]>();
		Schema.DescribeSobjectResult[] results = Schema.describeSObjects(new String[]{objName});
		Schema.DescribeSobjectResult res = results[0];
		for(Schema.SObjectField field : res.fields.getMap().values())
		{
			String [] tt = new String[3];
			DescribeFieldResult f = field.getDescribe();
			//自定义字段 且 不能以 Z_开头
			if(f.isCustom() && !((f.getName()).startsWith('Z_'))){
				tt[0] = f.getName();
				tt[1] = f.getLabel();
				ope.add(tt);
			}
		}
		return ope;
	}
	
	//根据类型拼接字符串
	public String joinStringByType(String filedType,String str){
		if(filedType == 'DOUBLE' || filedType =='INTEGER' || filedType =='CURRENCY' ){
			return str;
		}else{
			if(str.startsWith('\'') && str.endsWith('\'')){
				return str;
			}else if(!str.startsWith('\'') && str.endsWith('\'')){
				return '\''+str;
			}else if(str.startsWith('\'') && !str.endsWith('\'')){
				return str + '\'';
			}else{
				return '\'' +str + '\'';
			}
		}
	} 
	
	//获取可以建邮件规则的对象
	public list<SelectOption> getExportObject(){
		list<SelectOption> ope=new list<SelectOption>();
		Map<String,EmailRuleObject__c> map_allsObject = EmailRuleObject__c.getAll();
		Set<String> set_allsObject = map_allsObject.keySet();
		ope.add(new SelectOption('none','--无--'));
		for(String s : set_allsObject){
			ope.add(new SelectOption(s,s));
		}
		return ope;
	}
	
	//当前用户创建的电子邮件模板
	public List<SelectOption> getEmailTemplate(){
		List<EmailTemplate> myEmailtemp = [Select  e.Name, e.Id From EmailTemplate e where OwnerId =:UserInfo.getUserId() and e.IsActive = true];
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('none','--none--'));
		for(EmailTemplate obj : myEmailtemp){
			options.add(new SelectOption(obj.Id,obj.Name));
		}
		return options;
	}
	
	
	public list<SelectOption> getOperatorList(){
		return this.operatorList;
	}
	
	//
	public void getAssist(){
		sendEmailAssistList.clear();
		if(sendEmailAttachMap.size() != 0){
			Set<String> temp = sendEmailAttachMap.keySet();
			for(String key : temp){
				assist obj = new assist();
				obj.objectName = key;
				obj.objectField = sendEmailAttachMap.get(key);
				sendEmailAssistList.add(obj);
			}
		}else{
		}
	}
	
	class assist{
		public String objectName{get;set;}
		public List<FieldSetting__c> objectField{get;set;}
	}
}