/**
 * Author: Steven
 * Date: 2014-12-15
 * Description: test class
 **/
@isTest
private class MassTaskImportEmailBatchTest {
	static testmethod void myUnitTest(){
		Task t = new Task();
		t.Subject = 'ttt';
		insert t;
		
		MassTaskImportEmailBatch b = new MassTaskImportEmailBatch();
		b.runBatch();
	}
}