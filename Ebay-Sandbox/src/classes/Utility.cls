/********************************************
 *20141212
 *Bill
 *system Utility Class
 ********************************************/
public class Utility 
{
	//Operator Text Number REFERENCE BOOLEAN MULTIPICKLIST
    private Map<String,String> map_ftype = new Map<String,String>
    {
    	 'PHONE' => 'Text',
    	 'PICKLIST' => 'Text',
    	 'STRING' => 'Text',
    	 'URL' => 'Text',
    	 'ID' => 'Text',
    	 'DOUBLE' => 'Number',
    	 'REFERENCE' => 'REFERENCE',
    	 'DATE' => 'DATE',
    	 'DATETIME' => 'DATE',
    	 'TEXTAREA' => 'Text',
    	 'BOOLEAN' => 'BOOLEAN',
    	 'CURRENCY' => 'Number',
    	 'INTEGER' => 'Number',
    	 'EMAIL' => 'Text',
    	 'PERCENT' => 'Number',
    	 'MULTIPICKLIST' => 'MULTIPICKLIST'  	 
    };	
    
    private Map<String,String> map_Operator = new Map<String,String>
    {
		'equals' => '=',
		'not equal to' => '!=',
		'less than' => '<',
		'greater than' => '>',
		'less or equal' => '<=',
		'greater or equal' => '>=',
		'contains' => ' LIKE ',
		'does not contains' => ' NOT LIKE ',
		'starts with' => ' LIKE ', 
		'includes' => ' includes ',
		'excludes' => ' includes '
    };
	
	public FilterRule__c SpellingSoqlFilter(FilterRule__c rule,String ObjName)
	{
		if(!map_ftype.ContainsKey(rule.FieldType__c))
		{
			rule.Value__c = rule.Value__c.replace('\'','');
			rule.SOQL__c = rule.Field__c + map_Operator.get(rule.Operator__c) + '\'' + rule.Value__c + '\'';
			return rule;
		}
		if(rule.Value__c == null)rule.Value__c='';
		//Text Number REFERENCE BOOLEAN MULTIPICKLIST Date
		String ftype = map_ftype.get(rule.FieldType__c);
		string sobjType;
		if(ftype=='REFERENCE')
		{
			DescribeFieldResult f = getDescribeField(ObjName,rule.Field__c);
			sobjType = string.valueOf(f.getReferenceTo());
			if(sobjType.Contains('__c'))
			{
				rule.Field__c = rule.Field__c.replace('__c','__r.Name');
			}else{
				rule.Field__c = rule.Field__c.replace('Id','.Name');
			}
		}
		//equals
		if(rule.Operator__c == 'equals')
		{
			if(ftype=='Text' || ftype=='MULTIPICKLIST' || ftype=='DATE' || ftype=='REFERENCE')
			{
				EqualsMethod(rule);
			}else if(ftype=='Number' || ftype=='BOOLEAN' ){
				rule.SOQL__c = rule.Field__c + '='+ rule.Value__c;
			}
		}
		//not equal to
		if(rule.Operator__c == 'not equal to')
		{
			if(ftype=='Text' || ftype=='MULTIPICKLIST' || ftype=='DATE' || ftype=='REFERENCE')
			{
				NotEqualsMethod(rule);
			}else if(ftype=='Number' || ftype=='BOOLEAN'){
				rule.SOQL__c = rule.Field__c + '!='+ rule.Value__c;
			}
		}		
		//contains
		if(rule.Operator__c == 'contains')
		{
			rule.SOQL__c = rule.Field__c + ' like \'%'+ rule.Value__c + '%\'';
		}		
		//does not contains
		if(rule.Operator__c == 'does not contains')
		{
			rule.SOQL__c = ' NOT IN (Select id from '+ ObjName +' Where ' + rule.Field__c + ' like \'%'+ rule.Value__c + '%\') ';
		}	
		//starts with
		if(rule.Operator__c == 'starts with')
		{
			rule.SOQL__c = rule.Field__c + ' like \'%'+ rule.Value__c + '\'';
		}					
		//includes
		if(rule.Operator__c == 'starts with')
		{
			rule.SOQL__c = rule.Field__c + ' includes ' + splitValue(rule.Value__c);
		}		
		//excludes
		if(rule.Operator__c == 'starts with')
		{
			rule.SOQL__c = rule.Field__c + ' excludes ' + splitValue(rule.Value__c);
		}
		
		rule.Value__c = rule.Value__c.replace('\'','');
		//less than
		if(rule.Operator__c == 'less than')
		{
			rule.SOQL__c = rule.Field__c + '<'+ rule.Value__c;
		}		
		//greater than
		if(rule.Operator__c == 'greater than')
		{
			rule.SOQL__c = rule.Field__c + '>'+ rule.Value__c;
		}			
		//less or equal
		if(rule.Operator__c == 'less or equal')
		{
			rule.SOQL__c = rule.Field__c + '<='+ rule.Value__c;
		}			
		//greater or equal
		if(rule.Operator__c == 'greater or equal')
		{
			rule.SOQL__c = rule.Field__c + '>='+ rule.Value__c;
		}		
		if(ftype=='REFERENCE')
		{
			if(sobjType.Contains('__c'))
			{
				rule.Field__c = rule.Field__c.replace('__r.Name','__c');
			}else{
				rule.Field__c = rule.Field__c.replace('.Name','.Id');
			}
		}					
		return rule;
	}	
	private String NotEqualsMethod(FilterRule__c rule)
	{
		if(rule.Value__c == null || rule.Value__c.Trim().length()==0)
		{
			return rule.Field__c +'!=NULL';
		}else{
			return rule.Field__c +' NOT IN ' + splitValue(rule.Value__c);		
		}
	}	
	private String EqualsMethod(FilterRule__c rule)
	{
		if(rule.Value__c == null || rule.Value__c.Trim().length()==0)
		{
			return rule.Field__c +'=NULL';
		}else{
			return rule.Field__c +' IN ' + splitValue(rule.Value__c);		
		}
	}	
	
	private String splitValue(String v)
	{
		String val =' (';
		if(v.contains(','))
		{
			for(String s : v.split(','))
			{
				val +=  '\'' + s + '\',';
			}
			return val.substring(0, v.length()-1) + ')';
		}else{
			return '(\'' + v + '\') ';
		}
	}	
	
	public DescribeFieldResult getDescribeField(String objName,String faName)
	{
		DescribeFieldResult f;
		// sObject types to describe
		String[] types = new String[]{objName};
		
		// Make the describe call
		Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
		
		// For each returned result, get some info
		for(Schema.DescribeSobjectResult res : results) {
			Schema.SObjectField field = res.fields.getMap().get(faName);
			f = field.getDescribe();
		}
		return f;
	}	
}