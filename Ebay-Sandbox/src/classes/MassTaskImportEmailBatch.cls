/**
 * Author: Steven
 * Date: 2014-12-15
 * Description: 发送任务邮件给被分配人
 **/
public with sharing class MassTaskImportEmailBatch implements Database.Batchable<Sobject>, Database.Stateful{
	
	private String currentUserEmail; // 用于发送运行完毕确认邮件
	
	public MassTaskImportEmailBatch(){
		currentUserEmail = UserInfo.getUserEmail();
	}
	
	public Database.QueryLocator start(Database.BatchableContext bc){
		return Database.getQueryLocator('Select Id, Subject, OwnerId From Task Where IsEmail__c = false');
	}
	
	public void execute(Database.BatchableContext bc, List<Sobject> scope){
		for(Sobject sobj : scope){
			Task t = (Task) sobj;
			
			if(t.OwnerId == null)
			continue;
			
			List<User> u = [Select Id, Email From User Where Id =: t.OwnerId];
			
			if(u.isEmpty() || u[0].Email == null)
			continue;
			
			String mailBody = 'Task ' + t.Subject + '已分配给您，请查看：' + '<br/>';
			mailBody += '<a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + t.Id + '">';
			mailBody += URL.getSalesforceBaseUrl().toExternalForm() + '/' + t.Id + '</a>';
			
			Messaging.SingleEmailMessage mail = generateEmail(u[0].Email, t.Subject, mailBody);
			
			try {
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
				
				t.IsEmail__c = true;
				update t;
				
			} catch(Exception e){
				System.debug(e.getMessage());
			}
		}
	}
	
	public void finish(Database.BatchableContext bc){
		// 发送确认邮件
		Messaging.SingleEmailMessage mail = generateEmail(currentUserEmail, 'Task 邮件给分配人已发送完毕', '');
		Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
	}
	
	private Messaging.SingleEmailMessage generateEmail(String address, String subject, String htmlBody){
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		
		// 收件人地址
		mail.setToAddresses(new List<String>{address});
		
		// 发件人显示名字
		mail.setSenderDisplayName('Ebay');
		
		// 邮件主题
		mail.setSubject(subject);
		
		// 邮件正文
		mail.setHtmlBody(htmlBody);
		
		return mail;
	}
	
	// 运行Batch
	public void runBatch(){
		ApexPages.Message info;
		
		try{
			Database.executeBatch(this,1); // 一批次一条
			info = new ApexPages.Message(ApexPages.severity.CONFIRM, '邮件运行已经开始，运行结束后，您将会收到邮件通知');
		} catch(Exception e){
			info = new ApexPages.Message(ApexPages.severity.ERROR, '运行失败，请重试：' + e.getMessage());
		}
		
		ApexPages.addMessage(info);
	}
}