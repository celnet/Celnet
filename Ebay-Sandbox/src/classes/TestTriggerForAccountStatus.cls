@isTest
public class TestTriggerForAccountStatus{
    static testMethod void myTest(){
        //case1. 当{eBay IDs}提交审批时，如果相关联的{Account}页面的{Green Channel Status}为{UnSubmitted}或者{Green Channel Failed}，则将其更新为{Submitted for Green Channel}，并自动记录{Submit Green Channel Date} ;
        //case2. 当{eBay IDs}审批不通过，如果相关联的{Account}页面的{Green Channel Status}为{Submitted for Green Channel}，则将其更新为{Green Channel Failed};
        //case3. 当{eBay IDs}审批通过，如果相关联的{Account}页面的{Green Channel Status}为{Submitted for Green Channel}，则将其更新为{Green Channel Succeeded}并自动记录{Acceptance Date By eBay})
        ID rid = [Select Id From RecordType Where Name='BD Accounts' limit 1][0].Id;
        Account acc = new Account(Name='test account1',RecordTypeId=rid,Vertical__c='Others',Operation_Model__c='Own',Green_Channel_Status__c='UnSubmitted');
        insert acc;
        ID aid = [Select Id From Account Where Name='test account1' limit 1][0].Id;
        eBay_ID__c ebayid = new eBay_ID__c(Account_Name__c=aid,eBay_ID__c='201202201327',Oracle_ID__c='101',Green_Channel_Status__c='UnSubmitted');
        insert ebayid;
        ebayid.Green_Channel_Status__c='Submitted for Green Channel';
        update ebayid;
        eBay_ID__c ebayid1 = [Select Id ,Name,Account_Name__c, Account_Name__r.Green_Channel_Status__c, Account_Name__r.Submit_Green_Channel_Date__c,Account_Name__r.Acceptance_Date_By_eBay__c from eBay_ID__c Where eBay_Id__c='201202201327' limit 1];
        String status1 = ebayid1.Account_Name__r.Green_Channel_Status__c;
        System.debug('-----------status1 :'+status1);
        System.debug('Submit_Green_Channel_Date__c:'+ebayid1.Account_Name__r.Submit_Green_Channel_Date__c);
        System.debug('Acceptance_Date_By_eBay__c:'+ebayid1.Account_Name__r.Acceptance_Date_By_eBay__c);
        ebayid.Green_Channel_Status__c='Green Channel Failed';
        update ebayid;
        eBay_ID__c ebayid2 = [Select Id ,Name,Account_Name__c, Account_Name__r.Green_Channel_Status__c, Account_Name__r.Submit_Green_Channel_Date__c,Account_Name__r.Acceptance_Date_By_eBay__c from eBay_ID__c Where eBay_Id__c='201202201327' limit 1];
        String status2 = ebayid2.Account_Name__r.Green_Channel_Status__c;
        System.debug('-----------status2 :'+status2);
        System.debug('Submit_Green_Channel_Date__c:'+ebayid2.Account_Name__r.Submit_Green_Channel_Date__c);
        System.debug('Acceptance_Date_By_eBay__c:'+ebayid2.Account_Name__r.Acceptance_Date_By_eBay__c);
        ebayid.Green_Channel_Status__c='Submitted for Green Channel';
        update ebayid;
        eBay_ID__c ebayid3 = [Select Id ,Name,Account_Name__c, Account_Name__r.Green_Channel_Status__c, Account_Name__r.Submit_Green_Channel_Date__c,Account_Name__r.Acceptance_Date_By_eBay__c from eBay_ID__c Where eBay_Id__c='201202201327' limit 1];
        String status3 = ebayid3.Account_Name__r.Green_Channel_Status__c;
        System.debug('-----------status3 :'+status3);
        System.debug('Submit_Green_Channel_Date__c:'+ebayid3.Account_Name__r.Submit_Green_Channel_Date__c);
        System.debug('Acceptance_Date_By_eBay__c:'+ebayid3.Account_Name__r.Acceptance_Date_By_eBay__c);
        ebayid.Green_Channel_Status__c='Green Channel Succeeded';
        update ebayid;
        eBay_ID__c ebayid4 = [Select Id ,Name,Account_Name__c, Account_Name__r.Green_Channel_Status__c, Account_Name__r.Submit_Green_Channel_Date__c,Account_Name__r.Acceptance_Date_By_eBay__c from eBay_ID__c Where eBay_Id__c='201202201327' limit 1];
        String status4 = ebayid4.Account_Name__r.Green_Channel_Status__c;
        System.debug('-----------status4 :'+status4);
        System.debug('Submit_Green_Channel_Date__c:'+ebayid4.Account_Name__r.Submit_Green_Channel_Date__c);
        System.debug('Acceptance_Date_By_eBay__c:'+ebayid4.Account_Name__r.Acceptance_Date_By_eBay__c);
    }
}