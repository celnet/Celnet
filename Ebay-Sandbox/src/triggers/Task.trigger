/**
 * Author: Steven
 * Date: 2014-12-15
 * Description: 添加trigger，如果创建人是系统管理员，被分配人如果为null，则把account的owner赋值给被分配人，否则不处理。
 **/
trigger Task on Task (before insert) {
	
	List<Profile> p = [Select Id, Name From Profile Where Id =: UserInfo.getProfileId()];
	
	if(p.isEmpty() || (p[0].Name != 'System Administrator' && p[0].Name != '系统管理员'))
	return;
	
	Set<Id> accountIds = new Set<Id>();
	for(Task t : trigger.new){
		if(t.OwnerId == null && t.WhatId != null && t.WhatId.getSobjecttype() == Account.Sobjecttype)
		accountIds.add(t.WhatId);
	}
	
	Map<Id, Account> accounts = new Map<Id, Account>([Select Id, OwnerId From Account Where Id IN: accountIds]);
	
	for(Task t : trigger.new){
		if(t.OwnerId == null && t.WhatId != null && t.WhatId.getSobjecttype() == Account.Sobjecttype && accounts.get(t.WhatId) != null && accounts.get(t.WhatId).OwnerId != null)
		t.OwnerId = accounts.get(t.WhatId).OwnerId;
	}
}