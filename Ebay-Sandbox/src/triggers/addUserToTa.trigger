trigger addUserToTa on TA_ACCOUNT__c (before update) {
if(Trigger.isBefore)
{
   if(Trigger.isUpdate)
   {
     System.debug('Begin testing***************************************!');
     String UserId=UserInfo.getUserId();
     System.debug(UserId);
     User u=[select id,Flag__c, NumberofHour__c from user where id = :UserId  limit 1];
     Datetime dt=System.now();
     Datetime d2=dt.addHours(8);
     Integer hourbeforenow=u.NumberofHour__c.intValue();
     Datetime d1=dt.addHours(8- hourbeforenow);
     Integer TN=[select count()from TA_ACCOUNT__C  t where t.LastModifiedDate >:d1 and t.LastModifiedDate< :d2 and t.OwnerId=:UserId ];
     System.debug(TN);
      TA_ACCOUNT__c taold=Trigger.old[0];
      TA_ACCOUNT__c tanew=Trigger.new[0];
      String accountid=taold.DS_ACCOUNT__c;
      System.debug('*******AccountId='+accountid+'*********');
      System.debug(tanew.OwnerId);
      System.debug(UserId);
    if(taold.OwnerId!=tanew.OwnerId && tanew.OwnerId==UserId) {
         if(u.Flag__c>TN){
          if(taold.OwnerId=='00G90000000FE7CEAW'||taold.OwnerId=='00G90000000FE7HEAW'||taold.OwnerId=='00G90000000FE7jEAG'){
              AccountTeamMember atm=new AccountTeamMember(AccountId=accountid,UserId=UserId,TeamMemberRole='TA');
              insert atm;
              AccountShare ase=new AccountShare(AccountId=accountid,UserOrGroupId=UserId,AccountAccessLevel='Edit',OpportunityAccessLevel='Edit',CaseAccessLevel='Edit');
              insert ase;
              Account taupdateaccount=[select Id,Name,TA_Name__c from Account where Id=:accountid];
              taupdateaccount.TA_Name__c=UserId;
              taupdateaccount.TASecondOwner__c=UserId;
              update taupdateaccount;
          }else{
           tanew.addError('You must get a TA Supplier from a queue');
           }
       }else{
        tanew.addError('You can not get a TA Supplier any more in '+ u.NumberofHour__c+'hours');
       }
        System.debug('end testing***************************************!');
   }}
     }
   }