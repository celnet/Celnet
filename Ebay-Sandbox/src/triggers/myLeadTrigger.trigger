trigger myLeadTrigger on Lead (before insert, before update) {
    if (Trigger.isBefore) {
         String str = Trigger.new[0].Email;
         String strId = Trigger.new[0].id;
         
        /***********************************************************************
        ****** (zhihao song 2010-8-12) Start 
        ****** When lead is create, copy creat by to field 'F_Create_By'
        ***********************************************************************/
         if(Trigger.isInsert){
         	trigger.new[0].F_Create_By__c = trigger.new[0].CreatedById;
         }
        /***********************************************************************
        ****** (zhihao song 2010-8-12) End 
        ***********************************************************************/
        
        if (Trigger.isUpdate && Trigger.old[0].Email != str) {
            
	        for (Lead o : [select Name from Lead where Email=:str and Email!='' and id!=:strId limit 1]) {
	        	if(o != null) {
	            	Trigger.new[0].Email.addError('Duplicate Email found.');
	        	}    
	        } 
        }
        
        /***********************************************************************
        ****** (Jimmy Yang 2010-3-29) Start 
        ****** When lead is converted, if the value of 'Email_Sync_Status__c' is
        ****** 'eBay id existed', then update the the value of 'Account_Status__c'
        of the converted account to 'CRU'
        ***********************************************************************/
        if ( Trigger.isUpdate ) {
        	Lead newLead = Trigger.new[0];
        	Lead oldLead = Trigger.old[0];
        	//if lead is converted, and the value of 'Email_Sync_Status__c' is 'eBay id existed'
        	if ( newLead.IsConverted == true 
        			&& oldLead.IsConverted != newLead.IsConverted 
        			&& newLead.Email_Sync_Status__c == 'eBay id existed') {
        		List<Account> accs = [SELECT Id, Account_Status__c FROM Account WHERE Id = :newLead.ConvertedAccountId];
        		System.debug('From myLeadTrigger: ************************* 1 ');
        		if ( accs != null && accs.size() == 1 ) {
        			accs[0].Account_Status__c = 'CRU';
        			System.debug('From myLeadTrigger: ************************* 2 ');
        		}
        		update accs; 
        	}
        	
        }
        /***********************************************************************
        ****** (Jimmy Yang 2010-3-29) End 
        ***********************************************************************/
      }
}