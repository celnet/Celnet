trigger addLeadsAndAccounts on Lead (after update) {
    Lead lead = trigger.new[0];
    if(lead.isConverted){
        List<LeadsAndAccounts__c> laas = [select id from LeadsAndAccounts__c where Account_ID__c=:lead.ConvertedAccountId];
        if(laas==null || laas.size()<=0){
            LeadsAndAccounts__c la = new LeadsAndAccounts__c();
            la.Account__c = lead.ConvertedAccountId;
            la.Company__c = lead.Company;
            la.Converted__c = lead.IsConverted;
            la.Lead_Converted_Date__c = lead.ConvertedDate;
            la.Lead_Created_By__c = lead.CreatedBy.Name;
            la.Lead_Created_Date__c = lead.CreatedDate;
            la.Lead_ID__c = lead.Id;
            la.Lead_Record_Type__c = lead.RecordType.Name;
            la.Lead_Source__c = lead.LeadSource;
            la.Lead_Status__c = lead.Status;
            la.Sub_Source__c = lead.Sub_Source__c ;
            Id uid = [select OwnerId from Account where id=:la.Account__c limit 1 ][0].OwnerId;         
            User accOwner = [Select Id, Name,UserRoleId, UserRole.Name from User u Where id=:uid];
            la.Account_Owner__c = accOwner.Name;
            la.Account_Owner_Role__c = accOwner.UserRole.Name;
            User leadOwner = [select Id,Name,UserRoleId,UserRole.Name from User where Id =:lead.OwnerId];
            la.Lead_Owner__c = leadOwner.Name;
            la.Lead_Owner_Role__c = leadOwner.UserRole.Name;      
            insert la;       
            System.debug('la.Account__c:'+la.Account__c+
            'la.Account_Owner__c:'+la.Account_Owner__c+
            'la.Account_Owner_Role__c:'+la.Account_Owner_Role__c+
            'la.Lead_Owner__c:'+la.Lead_Owner__c+
            'la.Lead_Owner_Role__c:'+la.Lead_Owner_Role__c); 
        }
    }
}