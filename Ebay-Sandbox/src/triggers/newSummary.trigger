trigger newSummary on WeeklySummary__c (before insert) 
{
    Date beginDate= Trigger.new[0].Week_Day__c.toStartOfWeek();
    Date endDate=beginDate+7;
    
    Trigger.new[0].New_Lead__c=[Select count() from Lead l  where createdDate>=:Datetime.newInstance(beginDate,Time.newInstance(0,0,0,0))  
                                                   and createdDate<=:Datetime.newInstance(endDate,Time.newInstance(0,0,0,0))  
                                                   and RecordType.Name=:Trigger.new[0].Lead_Type__c];
    Trigger.new[0].New_CRU__c=[Select count() from Lead where ConvertedAccount.USER_CRE_DATE__c>=:beginDate and ConvertedAccount.USER_CRE_DATE__c<=:endDate 
                                                    and RecordType.Name=:Trigger.new[0].Lead_Type__c];
    Trigger.new[0].New_Seller__c=[Select count() from Lead where ConvertedAccount.FIRST_TRANS_DATE__c>=:beginDate and ConvertedAccount.FIRST_TRANS_DATE__c<=:endDate 
                                                    and RecordType.Name=:Trigger.new[0].Lead_Type__c];
    Trigger.new[0].Week_Day__c= beginDate;                                               
}