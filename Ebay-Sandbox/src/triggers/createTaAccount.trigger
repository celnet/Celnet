trigger createTaAccount on Account (before update) {
if(Trigger.isBefore)
{
  if(Trigger.isUpdate)
  {
     Account newaccount=Trigger.new[0];
     Account oldaccount=Trigger.old[0];
     if(oldaccount.RecordTypeId=='012900000000RwOAAU'&&newaccount.RecordTypeId=='012900000000RwTAAU')
     {
       
        TA_ACCOUNT__c ta=new TA_ACCOUNT__c();
        ta.ACCOUNT_NAME__c=oldaccount.Name;
        ta.DS_ACCOUNT__c=oldaccount.Id;
        ta.OwnerId='00G90000000FE7CEAW';
        ta.Account__c=oldaccount.Id;
        ta.Description__c=oldaccount.Description;
        ta.Account_DS_Owner__c=oldaccount.OwnerId;
        insert ta;
     }
  }
}

}