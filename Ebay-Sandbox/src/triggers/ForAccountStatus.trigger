trigger ForAccountStatus on eBay_ID__c (after update) {
    if(Trigger.isUpdate){
        //case1. 当{eBay IDs}提交审批时，如果相关联的{Account}页面的{Green Channel Status}为{UnSubmitted}或者{Green Channel Failed}，则将其更新为{Submitted for Green Channel}，并自动记录{Submit Green Channel Date} ;
        //case2. 当{eBay IDs}审批不通过，如果相关联的{Account}页面的{Green Channel Status}为{Submitted for Green Channel}，则将其更新为{Green Channel Failed};
        //case3. 当{eBay IDs}审批通过，如果相关联的{Account}页面的{Green Channel Status}为{Submitted for Green Channel}，则将其更新为{Green Channel Succeeded}并自动记录{Acceptance Date By eBay})
        eBay_ID__c ebayNew = trigger.new[0];
        eBay_ID__c ebayOld = trigger.old[0];
        Account acc = [Select Id,Name,Green_Channel_Status__c,Submit_Green_Channel_Date__c,Acceptance_Date_By_eBay__c From Account Where Id=:ebayNew.Account_Name__c];
        if(ebayNew.Green_Channel_Status__c !=ebayOld.Green_Channel_Status__c){
            if(ebayNew.Green_Channel_Status__c=='Submitted for Green Channel'&& (acc.Green_Channel_Status__c=='UnSubmitted'||acc.Green_Channel_Status__c=='Green Channel Failed')){
                acc.Green_Channel_Status__c='Submitted for Green Channel';
                acc.Submit_Green_Channel_Date__c = Date.today();
            }else if(ebayNew.Green_Channel_Status__c=='Green Channel Failed'&&acc.Green_Channel_Status__c=='Submitted for Green Channel'){
                acc.Green_Channel_Status__c='Green Channel Failed';
            }else if(ebayNew.Green_Channel_Status__c=='Green Channel Succeeded'&&acc.Green_Channel_Status__c=='Submitted for Green Channel'){
                acc.Green_Channel_Status__c='Green Channel Succeeded';
                acc.Acceptance_Date_By_eBay__c = Date.today();
            }
            update acc;
        }
    }
}