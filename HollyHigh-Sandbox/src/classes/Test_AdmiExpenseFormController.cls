/**
 * Test AdmiExpenseFormController
 */
@isTest
private class Test_AdmiExpenseFormController {

    static testMethod void myUnitTest() {
        //行政费用预算
        AdmiExpenseBudget__c aeb = new AdmiExpenseBudget__c();
        aeb.Name = 'test budget';
        aeb.ApproveStatus__c = '审批通过';
        aeb.BudgetEndDate__c = date.today().addDays(2);
        aeb.BudgetStartDate__c = date.today().addDays(-1);
        insert aeb;
        
        //行政费用支出
        List<AdmiExpense__c> la = new List<AdmiExpense__c>();
        AdmiExpense__c ae1 = new AdmiExpense__c();
        ae1.InvoiceNum__c =2 ;
        ae1.Expenses__c = 200;
        ae1.Approvestatus__c = '审批通过';
        ae1.CostType__c = '工资';
        ae1.Administrative_expenses__c = aeb.Id;
        la.add(ae1);
        AdmiExpense__c ae2 = new AdmiExpense__c();
        ae2.InvoiceNum__c =2 ;
        ae2.Expenses__c = 200;
        ae2.Approvestatus__c = '审批通过';
        ae2.CostType__c = '工资';
        ae2.Administrative_expenses__c = aeb.Id;
        la.add(ae2);
        AdmiExpense__c ae3 = new AdmiExpense__c();
        ae3.InvoiceNum__c =2 ;
        ae3.Expenses__c = 200;
        ae3.Approvestatus__c = '审批通过';
        ae3.CostType__c = '体检费';
        ae3.Administrative_expenses__c = aeb.Id;
        la.add(ae3);
        insert la;
        
        system.Test.startTest();
        Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(aeb);
        AdmiExpenseFormController ae = new AdmiExpenseFormController(controller);
        system.Test.stopTest();
    }
}