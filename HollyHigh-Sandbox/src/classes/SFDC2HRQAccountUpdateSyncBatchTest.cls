@isTest
private class SFDC2HRQAccountUpdateSyncBatchTest {

    static testMethod void myUnitTest() 
    {
        StructureMapData.StructureMap();
        
        Account  acc = new Account();
        acc.HRQ_ID__c = '123';
        acc.Name = 'name';
        acc.Description = 'hrq描述11';
        acc.Industry = '医药';
        acc.IndustryType__c = '制药';
        acc.HRQ_Industry_ID__c = '12131';
        acc.HRQ_Area_ID__c = '45611';
        acc.FirstAddress__c = '北京市朝阳1区';
        //acc.Main_Business__c = 'IT1t';
        acc.HRQ_TotalAssets__c = '1150001';
        acc.HRQ_NetAssets__c = '25010q';
        acc.HRQ_OperatingIncome__c = '12100q';
        acc.HRQ_NetProfit__c = '123155q';
        acc.Content__c = '描述内容1q';
        acc.HRQ_Uploader_ID__c = '11254q';
        acc.HRQ_UploadedOn__c = DateTime.now();
        acc.HRQ_OtherInfo__c = '其他信息q';
        acc.HRQ_Sync_Exception__c = 'N';
        insert acc;
        
        system.test.starttest();
        system.test.setMock(HttpCalloutMock.class,new HRQRequestMock());
        SFDC2HRQAccountUpdateSyncBatch his = new SFDC2HRQAccountUpdateSyncBatch();
        his.IsChain = false;
        Database.QueryLocator dq = his.start(null);
        String query = dq.getQuery();
        SObject[] res = Database.Query(query);
        his.execute(null,(List<Account>)res);
        his.finish(null);
        //database.executebatch(his);
        system.test.stoptest();
        
        //测试类代码覆盖率为49% 2014.8.11 22:38
    }
}