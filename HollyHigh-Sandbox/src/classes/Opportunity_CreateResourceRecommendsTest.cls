/**
	作者：winter
	时间：2014-8-29
	功能：Opportunity_CreateResourceRecommends（trigger）测试类
	覆盖率：100%
 */
@isTest
private class Opportunity_CreateResourceRecommendsTest {

    static testMethod void myUnitTest() {
        /*@@@@@@@@@@@@@@@@@@插入@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
        Account a1 = new Account();
        a1.Name = 'test';
        a1.Industry = 'test';
        insert a1;
        
        Account a567 = new Account();
        a567.Name = 'a567';
        a567.Industry = 'test';
        insert a567;
        
        Account a777 = new Account();
        a777.Name = 'a777';
        a777.Industry = 'test';
        insert a777;
        
        Account a54455 = new Account();
        a54455.Name = 'a54455';
        a54455.Industry = 'test';
        insert a54455;
        
        Opportunity o54455 = new Opportunity();
        o54455.Name = 'test';
        o54455.HRQ_ID__c = '54455';
        o54455.AccountId = a54455.Id;
        o54455.StageName = 'test';
        o54455.CloseDate = Date.today();
        insert o54455;
        
        Opportunity o777 = new Opportunity();
        o777.Name = 'test';
        o777.HRQ_ID__c = '777';
        o777.AccountId = a777.Id;
        o777.StageName = 'test';
        o777.CloseDate = Date.today();
        insert o777;
        
        Opportunity o567 = new Opportunity();
        o567.Name = 'test';
        o567.HRQ_ID__c = '567';
        o567.AccountId = a567.Id;
        o567.StageName = 'test';
        o567.CloseDate = Date.today();
        insert o567;
        
        Opportunity o1 = new Opportunity();
        o1.Name = 'test';
        o1.AccountId = a1.Id;
        o1.StageName = 'test';
        o1.HRQ_Pairids__c = '567,777';
        o1.CloseDate = Date.today();
        insert o1;
        
        
        
        
       
        
        /*@@@@@@@@@@@@@@@@@@修改@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
        
        o1.HRQ_Pairids__c = '567,777,54455';
        update o1;
    }
}