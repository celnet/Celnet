/*
作者：Tommy Liu
电子油箱：tommyliu@cideatech.com
创建时间：2014-07-08
功能：同步华尔圈的行业字典表到SFDC
同步方向：华尔圈到SFDC

**此Batch为日常链的首节点**

//单次手动启动方式：
HRQ2SFDCIndustrySyncBatch startBc = new HRQ2SFDCIndustrySyncBatch();
startBc.IsChain = true;
database.executeBatch(startBc);
*/

global class HRQ2SFDCIndustrySyncBatch extends HRQ2SFDCDataSyncBatch implements Schedulable  
{	
	private List<HRQIndustry> industries;//待保存的行业
	private List<HRQ_Industry_Mapping__c> hrq_Industry_list;//行业映射
    
	global HRQ2SFDCIndustrySyncBatch()
	{
		super();
		this.hrq_Industry_list = new List<HRQ_Industry_Mapping__c>();
		this.industries = new List<HRQIndustry>();
	}
   
	global override void getDataFromHRQ()
	{
		industries = this.hrqService.GetIndustryList();
	}
	
	global override void convertData()
	{
		
		system.debug(this.industries.size()  + '     ___dfkhadfk___');

		for(HRQIndustry industry : industries)
		{
			
			HRQ_Industry_Mapping__c hrq_industry = new HRQ_Industry_Mapping__c();
			hrq_industry.HRQ_ID__c = industry.Id;
			hrq_industry.Name = industry.Id;
			hrq_industry.HRQ_Parent_ID__c = industry.ParentId;
			hrq_industry.HRQ_Name__c = industry.Name;
			hrq_industry.HRQ_Content__c = industry.Content;
			hrq_industry.HRQ_LastModifiedOn__c = DateTime.now();
			hrq_Industry_list.add(hrq_industry);
						
		}
	}
	
	global override void saveData()
	{
		system.debug( ' ********342***********');
		upsert hrq_Industry_list HRQ_ID__c; 
	}
	
	global void execute(SchedulableContext SC) 
	{
		Database.executeBatch(this, 1);
   	}
	
	global override void handleNext()
	{
		if(this.IsChain)
        {
            HRQ2SFDCAreaSyncBatch nextBatch = new HRQ2SFDCAreaSyncBatch();
            nextBatch.IsChain = this.IsChain;
            database.executeBatch(nextBatch);
        }
	}
}