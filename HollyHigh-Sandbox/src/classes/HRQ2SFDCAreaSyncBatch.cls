/*
修改人：Peter
修改时间：2014-07-15
功能：同步华尔圈的Area字典表到SFDC
同步方向：华尔圈到SFDC
*/
global class HRQ2SFDCAreaSyncBatch extends HRQ2SFDCDataSyncBatch
{	
	private List<HRQArea> areaslist;//待保存的区域
	private List<HRQ_Area_Mapping__c> hamList;
    
	global HRQ2SFDCAreaSyncBatch()
	{
		super();
		this.areaslist = new List<HRQArea>(); 
		hamList = new List<HRQ_Area_Mapping__c>();
	}
   
	global override void getDataFromHRQ()
	{
		areaslist = this.hrqService.GetAreaList();//按照ID从小到大排序
	}
	
	global override void convertData()
	{
		for(HRQArea area : areaslist)
		{
			HRQ_Area_Mapping__c ham = new HRQ_Area_Mapping__c();
			ham.HRQ_ID__c = area.Id;
			ham.HRQ_Name__c = area.Name;
			ham.SFDC_Name__c = area.Name;
			ham.Name = area.Id;
			ham.HRQ_Parent_ID__c = area.ParentAreaId;
			//ham.HRQ_Root__c = area.Root;
			ham.HRQ_LastModifiedOn__c = DateTime.now();
			hamList.add(ham);
		}
	}
	
	global override void saveData()
	{
		system.debug( ' *******************');
		upsert this.hamList HRQ_ID__c;
	}
	
	global override void handleNext()
	{
		if(this.IsChain)
        {
            HRQ2SFDCMemberSyncBatch nextBatch = new HRQ2SFDCMemberSyncBatch(0,100);
            nextBatch.IsChain = this.IsChain;
            database.executeBatch(nextBatch);
        }
	}
}