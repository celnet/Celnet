/*
作者：Tommy Liu
电子油箱：tommyliu@cideatech.com
创建时间：2014-07-08 
功能：推送有变化的项目到华尔圈
同步方向：SFDC到华尔圈

**此Batch为日常链的首节点**

//单次手动启动方式：
SFDC2HRQProjectCreateSyncBatch startBc = new SFDC2HRQProjectCreateSyncBatch();
startBc.IsChain = true;
startBc.BatchSize = 100; 
database.executeBatch(startBc, startBc.BatchSize);

//计划周期启动，每2个小时启动一次
SFDC2HRQProjectCreateSyncBatch startBc = new SFDC2HRQProjectCreateSyncBatch();
startBc.IsChain = true;
startBc.BatchSize = 100; 
System.schedule('华尔圈日常数据同步','0 20 0/2 * * ?', startBc);
*/
global class SFDC2HRQProjectCreateSyncBatch extends SFDC2HRQDataSyncBatch
{  
    private HRQProject[] projectsList;//存储每批需要创建到HRQ的项目
    private HRQRelatedContact[] conList;//存储每批需要创建到HRQ的联系人
    private Opportunity[] sfAllProjectList;//存储所有需要创建到HRQ的SFDC项目，用来保存返回的HRQ_ID__c
    private Contact[] sfAllContactList;//存储所有需要创建到HRQ的SFDC项目，用来保存返回的HRQ_ID__c
    private Opportunity[] sfProjectList;//存储每批需要创建到HRQ的SFDC项目，用来对应产生的HRQ_ID__c与SFDC项目
    private Contact[] sfContactList;//存储每批需要创建到HRQ的SFDC项目，用来对应产生的HRQ_ID__c与SFDC联系人
    private List<Account> accList;//By Winter 在opp获得saveresult的ID后，同时要赋予给OPP相关联的ACC的HRQ_ID__c
    private Datetime pushTime;
    
    private Map<string,string> hrqWantOppMap;
    private Map<string,string> hrqAreaMap;
    private list<HRQ_Industry_Mapping__c > industryMapLists;
    
    
    
    global SFDC2HRQProjectCreateSyncBatch()
    {
        super();
        this.hrqWantOppMap = new Map<String,String>();
        for(HRQ_WantType_Mapping__c hwm : [Select Id, 
                                                SFDC_Object_Name__c,
                                                SFDC_RecordType_Developer_Name__c,
                                                HRQ_WantType__c 
                                            From HRQ_WantType_Mapping__c
                                            Where SFDC_Object_Name__c = 'Opportunity'
                                            And Primary__c = true])
        {
            HRQWantOppMap.put(hwm.SFDC_RecordType_Developer_Name__c, hwm.HRQ_WantType__c);
        }
        //地区表的映射
       /* this.hrqAreaMap = new Map<string,string>();
        for(HRQ_Area_Mapping__c  ham : [Select Id,
                                            SFDC_Name__c,
                                            HRQ_ID__c,
                                            HRQ_Parent_ID__c 
                                        From  HRQ_Area_Mapping__c])
        {
            hrqAreaMap.put(ham.SFDC_Name__c,ham.HRQ_Parent_ID__c);//TODO:区域Mapping
        }*/
        
        this.industryMapLists = [Select Id, 
                                    HRQ_ID__c,
                                    SFDC_Industry_Name__c,
                                    SFDC_Sub_Industry_Name__c,
                                    HRQ_Parent_ID__c,
                                    HRQ_Name__c,
                                    HRQ_LastModifiedOn__c 
                                From HRQ_Industry_Mapping__c];
        this.sfAllProjectList = new List<Opportunity>();
        this.sfAllContactList = new List<Contact>();
        this.pushTime = datetime.now();
    }
    global override Database.QueryLocator getDataScop()
    {
    	this.currentTime = Datetime.now();
        String sqlString = 'Select '
                            +'Account.Id, '
                            +'Account.Name, '
                            +'Account.HRQ_ID__c, '
                            +'Account.Industry, '
                            +'Account.IndustryType__c, '
                            +'Account.HRQ_Industry_ID__c, '
                            +'Account.HRQ_Area__c, '
                            +'Account.HRQ_Area_ID__c, '
                            +'Account.FirstAddress__c, '
                            +'Account.Main_Business__c, '
                            +'Account.HRQ_TotalAssets__c, '
                            +'Account.TotalAssets__c, '
                            +'Account.HRQ_NetAssets__c, '
                            +'Account.NetAssets__c, '
                            +'Account.HRQ_OperatingIncome__c, '
                            +'Account.OperatingIncome__c, '
                            +'Account.HRQ_NetProfit__c, '
                            +'Account.NetProfit__c, '
                            +'Account.Content__c, '
                            +'Account.HRQ_OtherInfo__c, '
                            +'Account.HRQ_Sync_Exception__c, '//新添字段“是否排除”，用以数据同步
                            +'Account.Owner.HRQ_Member_ID__c, '
                            +'Id, '
                            +'Name, '
                            +'RecordType.DeveloperName, '
                            +'HRQ_ID__c, '
                            +'Description, '
                            +'Credibility__c, '
                            +'CreatedDate, '
                            +'HRQ_Uploader_ID__c, '
                            +'HRQ_UploadedOn__c, '
                            +'HRQ_Pairids__c, '
                            +'HRQ_LastModifiedOn__c, '
                            +'HRQ_Sync_Exception__c,'//新添字段“是否排除”，用以数据同步
                            +'Owner.HRQ_Member_ID__c, '
                            +'(Select '
                                +'Contact.Id, '
                                +'Contact.HRQ_ID__c, '
                                +'Contact.Name, '
                                +'Contact.Owner.HRQ_Member_ID__c, '
                                +'Contact.LastName, '
                                +'Contact.Email, '
                                +'Contact.CreatedDate, '
                                +'Contact.Title, '
                                +'Contact.MobilePhone,'
                                +'Contact.Phone,'
                                +'Contact.HRQ_Sync_Exception__c '//新添字段“是否排除”，用以数据同步
                            +'From OpportunityContactRoles) '
                        +'From Opportunity '
                        +'Where HRQ_ID__c = null ';
        return Database.getQueryLocator(sqlString);
    }

    global override void convertData(List<SObject> scope)
    {
    	system.debug('convertData#################');
        this.conList = new List<HRQRelatedContact>();
        this.projectsList = new List<HRQProject>();
        this.sfContactList = new List<Contact>();
        this.sfProjectList = new List<Opportunity>();
        for(Opportunity opp : (List<Opportunity>)scope)
        {
        	//如果“是否排除”字段的值为Y，说明不将此数据推送到HRQ，反之，推送到HRQ	by witner 2014.8.11
        	System.debug('_____________这是'+opp.HRQ_Sync_Exception__c);
        	if(opp.HRQ_Sync_Exception__c != 'Y')
        	{
        		HRQProject pro = new HRQProject();
                
                pro.Title = opp.Name;
                pro.Wanttype = hrqWantOppMap.get(opp.RecordType.DeveloperName);
                System.debug(hrqWantOppMap.get(opp.RecordType.DeveloperName)+'!!!!!!!!!!!!!!!!!!!!__this is wanttype');
                pro.Wantdo = opp.Description;
                pro.Credibility = opp.Credibility__c;
                if(opp.Owner != null)
                {
                    pro.UploaderId =  opp.Owner.HRQ_Member_ID__c;
                }
                pro.UploadedOn = String.valueOf(opp.CreatedDate);
                if(opp.Account != null)
                {
                    //pro.AreaId =hrqIndustryMap.get(opp.Account.HRQ_Area__c);//TODO:处理区域
                    pro.Address = opp.Account.FirstAddress__c;
                    pro.MainBusiness = opp.Account.Main_Business__c;
                    //从SFDC传过去要放置SFDC的四个资产字段,当为空值时候，传入为“0万元”
                    /*总资产*/
                    if(opp.Account.TotalAssets__c != null)
                    {
	                    pro.TotalAssets = opp.Account.TotalAssets__c + '万元（RMB）';
                    }
                    else
                    {
                    	pro.TotalAssets = ' ';
                    }
                    /*净资产*/
                    if(opp.Account.NetAssets__c != null)
                    {
	                    pro.NetAssets = opp.Account.NetAssets__c + '万元（RMB）';
                    }
                    else
                    {
                    	pro.NetAssets = ' ';
                    }
                    /*营业收入*/
                    if(opp.Account.OperatingIncome__c != null)
                    {
	                    pro.Businessincome = opp.Account.OperatingIncome__c + '万元（RMB）';
                    }
                    else
                    {
                    	pro.Businessincome = ' ';
                    }
                    /*净利润*/
                    if(opp.Account.NetProfit__c != null)
                    {
	                    pro.NetProfit = opp.Account.NetProfit__c + '万元（RMB）';
                    }
                    else
                    {
                    	pro.NetProfit = ' ';
                    }
                    pro.OtherInfo = opp.Account.HRQ_OtherInfo__c;
                    if(opp.Account.Content__c == null)
                    {
                       pro.Content = '来自Salesforce, Project ID: '+ opp.ID ;
                    }
                    else
                    {
                       pro.Content = '来自Salesforce, Project ID: '+ opp.ID  + ' ' + opp.Account.Content__c ;
                    }
                    
                    pro.IndustryId = this.rebackIndustyId(opp.Account.Industry,opp.Account.IndustryType__c);
                }
                this.projectsList.add(pro);
                this.sfProjectList.add(opp);
        	}
               
        }
    	
        
    }
    
    private string rebackIndustyId(string indus,string subindus)
    {
        if(indus == null && subindus == null)
        {
            return null;
        }
        for( HRQ_Industry_Mapping__c hrqIndusMap : industryMapLists)
        {
            if(indus == hrqIndusMap.SFDC_Industry_Name__c && subindus == hrqIndusMap.SFDC_Sub_Industry_Name__c)
            {
                return hrqIndusMap.HRQ_ID__c;
            }
            if(subindus == null && hrqIndusMap.SFDC_Sub_Industry_Name__c == null && indus == hrqIndusMap.SFDC_Industry_Name__c)
            {
                return hrqIndusMap.HRQ_ID__c;
            }
            if(indus == null && hrqIndusMap.SFDC_Industry_Name__c == null && subindus == hrqIndusMap.SFDC_Sub_Industry_Name__c)
            {
                return hrqIndusMap.HRQ_ID__c;
            }
        }
        return null;
    }
    global override void pushDataToHRQ()
    {
       //发送项目，并处理项目ID
        HRQSaveResult[] saveProjectResults = this.hrqService.CreateProject(this.projectsList);//需要接口保证结果与Project数组下标一一对应
        this.accList = new List<Account>();
        for(Integer i = 0; i < saveProjectResults.size(); i ++)
        {
            HRQSaveResult re = saveProjectResults[i];
            Opportunity sfProject = this.sfProjectList[i];
            sfProject.HRQ_PushedOn__c = this.pushTime;
            sfProject.Entrance_System__c = 'SFDC';
            if(re.IsSuccess == 1)
            {
                sfProject.HRQ_ID__c = re.Id;
                //需要给Account的hrqID赋值
                accList.add(new Account(ID = sfProject.AccountID,HRQ_ID__c = re.Id));
            }
            else
            {
                sfProject.HRQ_PushedError__c = re.Error;
            }
            this.sfAllProjectList.add(sfProject);
            
            //组装联系人
            List<OpportunityContactRole> oppContactRoles = sfProject.OpportunityContactRoles;
            if(oppContactRoles != null)
            {
                for(OpportunityContactRole contRole : oppContactRoles)
                {
                	//查询出联系人，并查出联系人的“是否排除”字段，如果该字段等于Y，说明不创建此联系人到HRQ，反之创建到HRQ 2014.8.11 by winter
                	if(contRole.Contact.HRQ_Sync_Exception__c != 'Y')
                	{
	                	HRQRelatedContact hrqContact = new HRQRelatedContact();
	                    hrqContact.Name = contRole.Contact.name;
	                    hrqContact.JobTitle = contRole.Contact.Title;
	                    hrqContact.Email = contRole.Contact.Email;
	                    //hrqContact.Mobile = contRole.Contact.MobilePhone;
	                    hrqContact.Mobile = contRole.Contact.Phone;//2014-10-10	修改：phone（电话（手机2））和mobilephone调换（手机）	winter
	                    hrqContact.ProjectId = sfProject.HRQ_ID__c;//设置华尔圈联系人到Project的关系
	                    contRole.Contact.HRQ_Project_ID__c = sfProject.HRQ_ID__c;
						system.debug(hrqContact.ProjectId  + '+++++++++++++++++++++');
	                    if(contRole.Contact.Owner != null)
	                    {
	                        hrqContact.UploaderId = contRole.Contact.Owner.HRQ_Member_ID__c;
	                    }
	                    hrqContact.UploadedOn = string.valueOf(contRole.Contact.CreatedDate);
	                    if(contRole.Contact.HRQ_ID__c == null && hrqContact.ProjectId != null)//如果有HRQ ID说明是已经在HRQ和SFDC系统中都存在的联系人，不需要创建
	                    {
	                        this.conList.add(hrqContact);
	                        this.sfContactList.add(contRole.Contact);
	                    }
                	}
                    
                }
            }
        }
        
        HRQSaveResult[] saveConResults = this.hrqService.CreateRelatedContact(this.conList);//需要接口保证结果与contact数组下标一一对应
        for(Integer i = 0; i < saveConResults.size(); i ++)
        {
            HRQSaveResult re = saveConResults[i];
            Contact sfContact = this.sfContactList[i];
            sfContact.HRQ_PushedOn__c = this.pushTime;
            sfContact.Entrance_System__c = 'SFDC'; 
            if(re.IsSuccess == 1)
            {
                sfContact.HRQ_ID__c = re.Id;
            }
            else
            {
                sfContact.HRQ_PushedError__c = re.Error;
            }
            this.sfAllContactList.add(sfContact);
        }
        
    } 
    
    private HRQ_Sync_Log__c[] toLogs(sObject[] records, Database.SaveResult[] res)
    {
    	List<HRQ_Sync_Log__c> logs = new List<HRQ_Sync_Log__c>();
    	if(records == null || res == null)
    	{
    		return logs;
    	}
        for(Integer i=0; i < res.size(); i ++)
        {
        	Database.SaveResult re = res[i];
        	Sobject record = records[i];
        	if(!re.isSuccess())
        	{
        		HRQ_Sync_Log__c log = this.toLog(re);
        		object hrqId = record.get('HRQ_ID__c');
        		if(hrqId != null)
        		{
        			log.HRQ_ID__c = (String)hrqId;
        		}
        		object sfId = record.Id;
        		if(sfId != null)
        		{
        			log.SFDC_ID__c = (String)sfId;
        		}
        		 object sfName = record.get('Name');
        		if(sfName != null)
        		{
        			log.SFDC_Name__c = (String)sfName;
        		}
        		Schema.SObjectType objType = record.getSObjectType();
        		log.Object__c = objType.getDescribe().getName();
        		log.Sub_Type__c = 'SFDC2HRQ Update HRQ ID';
        		log.Com__c = 'SFDC2HRQProjectCreateSyncBatch';
        		logs.add(log);
        	}
        }
        return logs;
    }
    
    global override void finish(Database.BatchableContext BC)
    {	
		//处理重复的account
    	map<ID, Account> noDupAccountMap = new map<ID, Account>();
    	if(this.accList != null)
    	{
    		for(Account acc: this.accList)
	    	{
	    		noDupAccountMap.put(acc.ID, acc);
	    	}
	    	List<Account> accs = noDupAccountMap.values();
	    	if(accs != null)
	    	{
	    		Database.SaveResult[] accRes = Database.update(accs, false);
	    		this.Logs(this.toLogs(accs, accRes));
	    	}
    	}
    	

    	if(this.sfAllProjectList != null)
    	{
    		Database.SaveResult[] proRes = Database.update(this.sfAllProjectList, false);
	    	this.Logs(this.toLogs(this.sfAllProjectList, proRes));
    	}
    	
    	//处理重复的contact
    	Map<ID,Contact> noDupContactMap = new Map<ID,Contact>(); 
    	if(this.sfAllContactList != null)
    	{
    		for(Contact con : this.sfAllContactList)
	    	{
				noDupContactMap.put(con.ID,con);
	    	}
	    	List<Contact> cots = noDupContactMap.values();
	    	if(cots != null)
	    	{
	    		Database.SaveResult[] conRes = Database.update(cots, false);
	    		this.Logs(this.toLogs(cots, conRes));
	    	}
    	}
    	
    	
        this.handleNext();
    }
    
    global override void handleNext()
    {
        if(this.IsChain)
        {
        	system.debug('***************CreateProject*************handlenext!!!!');
            SFDC2HRQContactCreateSyncBatch nextBatch = new SFDC2HRQContactCreateSyncBatch();
            nextBatch.currentTime = this.currentTime;
            nextBatch.IsChain = this.IsChain;
            nextBatch.BatchSize = this.BatchSize; 
            database.executeBatch(nextBatch, nextBatch.BatchSize);
        }
    }
    
    global override String getComName()
    {
    	return 'SFDC2HRQProjectCreateSyncBatch';
    }
}