/**
 * Author : Sunny
 * Des:项目成本打印PDF
 **/
public class ProjectCostFormController 
{
	public ProjectCost__c objProjectCost{get;set;}
	private ID projectCostId;
	public String sYear{get{return String.valueOf(date.today().year());}set;}
    public String sMonth{get{return String.valueOf(date.today().month());}set;}
    public String sDay{get{return String.valueOf(date.today().Day());}set;}
    public String sEexpenseTotal{get;set;}//大写金额
	public ProjectCostFormController(Apexpages.Standardcontroller controller)
	{
		projectCostId = controller.getId();
		this.initProjectCost();
	}
	private void initProjectCost()
	{
		objProjectCost = [Select Id,OwnerDepartment__c,ProjectCostType__c,ExpendMoney__c,ProjectName__c,Name,ProjectName__r.Name,Owner.Name From ProjectCost__c Where Id=:projectCostId] ;
		if(objProjectCost.ExpendMoney__c != null)
		sEexpenseTotal = UtilToolClass.translate(objProjectCost.ExpendMoney__c);
	}
}