/*
 * Author: Steven
 * Date: 2014-2-17
 * Description: 测试Trigger OpportunityMemberNotify
 */
@isTest
private class Test_OpportunityMemberNotify {
	static testmethod void testcase()
	{
		Account acc1 = new Account();
   		acc1.Name = '雨花石1';
   		acc1.IndustryType__c = '健康';
	    acc1.industry= '食品';
   		acc1.RecordTypeId = [select id from RecordType where DeveloperName=:'Seller' and SobjectType=:'Account'].id;
	    insert acc1;
	    
		Opportunity opp = new Opportunity();
   		opp.AccountId = acc1.Id;
   		opp.Name = '雨花石';
   		opp.RecordTypeId = [select id from RecordType where DeveloperName=:'Seller' and SobjectType=:'Opportunity'].id;
   		opp.StageName ='创新阶段';
   		opp.CloseDate = Date.today();
   		insert opp;
   		
   		OpportunityTeamMember otm = new OpportunityTeamMember();
   		otm.TeamMemberRole = '拿单来源人';
   		otm.OpportunityId = opp.Id;
   		List<User> u = [Select Id, Name, Email From User Where Name = '司徒远纵'];
   		otm.UserId = u[0].id;
   		
   		insert otm;
	}
}