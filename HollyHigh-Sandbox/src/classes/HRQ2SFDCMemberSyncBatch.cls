/*
修改人：Peter
修改时间：2014-07-15
功能：同步华尔圈的发布人字典表到SFDC
同步方向：华尔圈到SFDC
*/ 

global class HRQ2SFDCMemberSyncBatch extends HRQ2SFDCDataSyncBatch
{  
    private List<HRQMember> memberlist;//待保存的区域
    private List<HRQ_Member__c> hrqmemberlist;//待保存的区域
    protected Integer pageCursor;
    protected final Integer pageRows;
    private Integer max = 0;
    
    global HRQ2SFDCMemberSyncBatch(Integer pageCursor, Integer pageRows)
    {
        super();
        this.pageCursor = pageCursor;
        this.pageRows = pageRows;
        this.hrqmemberlist = new List<HRQ_Member__c>();
        this.memberlist = new List<HRQMember>(); 
    }
    global override void getDataFromHRQ()
    {
        this.memberlist = this.hrqService.GetMemberList(this.pageCursor, this.pageRows);
        for(Integer i=0;i<memberlist.size();i++)
        {
            if(max<Integer.valueOf(memberlist[i].Id))
            {
                max=Integer.valueOf(memberlist[i].Id);
            }
        }
        this.pageCursor = max;
        system.debug(this.pageCursor + '____________________this.pageCursor!!!!!!!!!!!!!');
    }
    
    global override void convertData()
    {
        for(HRQMember member : memberlist)
        {
            HRQ_Member__c hrqMember = new HRQ_Member__c();
            hrqMember.HRQ_ID__c = member.Id;
            hrqMember.Name = member.Name;
            //2014.8.11 将成员名字段删除，改用标准字段Name
            if(member.Gender == 0)
            {
                hrqMember.HRQ_Gender__c = '男';
            }else
            {
                hrqMember.HRQ_Gender__c = '女';
            }
            hrqMember.HRQ_Reg_IP__c = member.Regip;
            hrqMember.HRQ_Reg_On__c = DateTime.valueOf(member.Regdate);
            //使用正则表达式判断邮箱
            hrqMember.HRQ_EmailText__c = member.Email;
            
            string MemberEmailRes = member.Email;

			if(MemberEmailRes != null && MemberEmailRes.trim() != '')
			{
				boolean isEmail = Pattern.matches('[a-zA-Z0-9\\.\\!\\#\\$\\%\\&\\*\\/\\=\\?\\^\\_\\+\\-\\`\\{\\|\\}\\~\'._%+-]+@[a-zA-Z0-9\\-.-]+\\.[a-zA-Z]+', MemberEmailRes);
				if(isEmail)
				{
					hrqMember.HRQ_Email__c = MemberEmailRes;
				}
			}            
            //将华尔圈Member的电话信息完整保存到“电话文本”
            hrqMember.HRQ_MobilePhoneText__c = member.Mobile;
            //如果“电话文本”长度不大于40，存入正式电话信息中
            if(member.Mobile.length() <= 40 && member.Mobile != null && member.Mobile.trim() !='')
	        {
		    	hrqMember.HRQ_MobilePhone__c = member.Mobile;
	        }
            hrqMember.HRQ_MobilePhone__c =member.Mobile;
            hrqMember.HRQ_Phone__c = member.Telephone;
            hrqMember.HRQ_LastModifiedOn__c = DateTime.now();
            hrqmemberlist.add(hrqMember);
        }
    }
    
    global override void saveData()
    {
        upsert this.hrqmemberlist HRQ_ID__c;
    }
    
    global override void handleNext()
    {
        if(memberlist.size() != 0 && memberlist.size() == 100 )//如果本页不为空则取下一页，
        {
            HRQ2SFDCMemberSyncBatch nextPageBatch = new HRQ2SFDCMemberSyncBatch(this.pageCursor, this.pageRows);
            system.debug(this.pageCursor + '_______________peter');
            DataBase.executeBatch(nextPageBatch);
        }
    }
}