/*
作者：Peter Winter 
创建时间：2014-08-06 
功能：Opp的hrqId 不为空 ，但是Opp下的联系人角色的HrqId为空  将此类的Contact数据同步到华尔圈。
同步方向：SFDC到华尔圈

*/
global class SFDC2HRQContactCreateSyncBatch extends SFDC2HRQDataSyncBatch
{  
    private List<HRQRelatedContact> hrqRelConList;
    private List<Contact> sfAllConList;//接受saveResult Id
    private List<Contact> sfconList;
    private DateTime pushTime;
    global SFDC2HRQContactCreateSyncBatch()
    {
        super();
        this.sfAllConList =  new List<Contact>();
        this.pushTime = datetime.now();
    }
    global override Database.QueryLocator getDataScop()
    {
    	this.currentTime = Datetime.now();
    	/*
        String sqlString = 'Select '
                            +'Id, '
                            +'Name, '
                            +'HRQ_ID__c, '
                            +'(Select '
                                +'Contact.Id, '
                                +'Contact.HRQ_ID__c, '
                                +'Contact.Name, '
                                +'Contact.Owner.HRQ_Member_ID__c, '
                                +'Contact.LastName, '
                                +'Contact.Email, '
                                +'Contact.CreatedDate, '
                                +'Contact.Title, '
                                +'Contact.HRQ_Sync_Exception__c, '//新添字段“是否排除”
                                +'Contact.MobilePhone '
                            +'From OpportunityContactRoles) '
                        +'From Opportunity '
                        +'Where HRQ_ID__c != null ';
       	*/
        //将查询opp改为查询contact，提高效率
        String sqlString = 'Select '
        	 			+'c.Title, '
        	 			+'c.Name, '
        	 			+'c.Email, '
        	 			+'c.MobilePhone, '
        	 			+'c.Phone, '
        	 			+'c.LastName, '
        	 			+'c.Id, '
        	 			+'c.HRQ_Sync_Exception__c, '
        	 			+'c.HRQ_ID__c, '
        	 			+'c.FirstName, '
        	 			+'c.CreatedDate, '
        	 			+'c.Owner.HRQ_Member_ID__c, '
        	 				+'(Select '
        	 				+'Opportunity.Id,'
        	 				+'Opportunity.Name,'
        	 				+'Opportunity.HRQ_ID__c '
        	 				+'From OpportunityContactRoles) '
        	 		+'From Contact c '
        	 		+'where c.HRQ_ID__c = null';
        return Database.getQueryLocator(sqlString);
    }

    global override void convertData(List<SObject> scope)
    {
        /*
        //this.sfProjectList = new List<Opportunity>();
        this.hrqRelConList = new List<HRQRelatedContact>();
        this.sfConList = new List<Contact>();
        for(Opportunity opp : (List<Opportunity>)scope)
        {
        	List<OpportunityContactRole> oppContactRoles = opp.OpportunityContactRoles;
        	if(oppContactRoles != null)
            {
                for(OpportunityContactRole contRole : oppContactRoles)
                {
                	if(contRole.Contact.HRQ_ID__c == null || contRole.Contact.HRQ_ID__c.trim() == '')
                	{
                		//查询出联系人，并查出联系人的“是否排除”字段，如果该字段等于Y，说明不创建此联系人到HRQ，反之创建到HRQ 2014.8.11 by winter
                		if(contRole.Contact.HRQ_Sync_Exception__c != 'Y')
                		{
                			HRQRelatedContact hrqContact = new HRQRelatedContact();
		                    contRole.Contact.HRQ_Project_ID__c = opp.HRQ_ID__c;
		                    hrqContact.ProjectId = opp.HRQ_ID__c;
		                    hrqContact.Name = contRole.Contact.Name;
		                    hrqContact.JobTitle = contRole.Contact.Title;
		                    hrqContact.Mobile = contRole.Contact.MobilePhone;
		                    hrqContact.Email = contRole.Contact.Email;
		                    if(contRole.Contact.Owner != null)
		                    {
		                        hrqContact.UploaderId = contRole.Contact.Owner.HRQ_Member_ID__c;
		                    }
		                    hrqContact.UploadedOn = string.valueOf(contRole.Contact.CreatedDate);
		                    this.hrqRelConList.add(hrqContact);
		                    this.sfConList.add(contRole.Contact);
                		}
                	}
                }
            }
            
        }
        */
        this.hrqRelConList = new List<HRQRelatedContact>();
        this.sfConList = new List<Contact>();
    	for(Contact con : (List<Contact>)scope)
    	{
    		if(con.HRQ_Sync_Exception__c != 'Y')
    		{
    			List<OpportunityContactRole> oppContactRoles = con.OpportunityContactRoles;
    			if(oppContactRoles != null)
    			{
    				for(OpportunityContactRole oppRole : oppContactRoles)
    				{
    					HRQRelatedContact hrqContact = new HRQRelatedContact();
    					con.HRQ_Project_ID__c = oppRole.Opportunity.HRQ_ID__c;
    					hrqContact.ProjectId = oppRole.Opportunity.HRQ_ID__c;
    					hrqContact.Name = con.Name;
    					hrqContact.JobTitle = con.Title;
    					hrqContact.Email = con.Email;
    					//hrqContact.Mobile = con.MobilePhone;
    					hrqContact.Mobile = con.Phone;//2014-10-10	修改：phone（电话（手机2））和mobilephone调换（手机）	winter
    					if(con.Owner != null)
    					{
    						hrqContact.UploaderId = con.Owner.HRQ_Member_ID__c;
    					}
    					this.hrqRelConList.add(hrqContact);
    					this.sfConList.add(con);
    				}
    			}
    		}
    	}
        
    }
    
    global override void pushDataToHRQ()
    {
    	if(this.hrqRelConList.size() == 0)
    	{
    		return;
    	}
    	System.debug('####for Contact ');
    	System.debug(this.hrqRelConList);
        HRQSaveResult[] saveConResults = this.hrqService.CreateRelatedContact(this.hrqRelConList);//需要接口保证结果与contact数组下标一一对应
        
        for(Integer i = 0; i < saveConResults.size(); i ++)
        {
            HRQSaveResult re = saveConResults[i];
            Contact sfContact = this.sfConList[i];
            system.debug(sfContact.Name + '  &&&&&&&&&&&&&&');
            sfContact.HRQ_PushedOn__c = this.pushTime;
            sfContact.Entrance_System__c = 'SFDC'; 
            if(re.IsSuccess == 1)
            {
                sfContact.HRQ_ID__c = re.Id;
            }
            else
            {
                sfContact.HRQ_PushedError__c = re.Error;
            }
            this.sfAllConList.add(sfContact);
        }
        
    } 
    
    //抓捕日志	2014.8.17添加	by winter
    private HRQ_Sync_Log__c[] toLogs(sObject[] records, Database.SaveResult[] res)
    {
    	List<HRQ_Sync_Log__c> logs = new List<HRQ_Sync_Log__c>();
    	if(records == null || res == null)
    	{
    		return logs;
    	}
        for(Integer i=0; i < res.size(); i ++)
        {
        	Database.SaveResult re = res[i];
        	Sobject record = records[i];
        	if(!re.isSuccess())
        	{
        		HRQ_Sync_Log__c log = this.toLog(re);
        		object hrqId = record.get('HRQ_ID__c');
        		if(hrqId != null)
        		{
        			log.HRQ_ID__c = (String)hrqId;
        		}
        		object sfId = record.Id;
        		if(sfId != null)
        		{
        			log.SFDC_ID__c = (String)sfId;
        		}
        		 object sfName = record.get('Name');
        		if(sfName != null)
        		{
        			log.SFDC_Name__c = (String)sfName;
        		}
        		Schema.SObjectType objType = record.getSObjectType();
        		log.Object__c = objType.getDescribe().getName();
        		log.Sub_Type__c = 'SFDC2HRQ Update HRQ ID';
        		log.Com__c = 'SFDC2HRQContactCreateSyncBatch';
        		logs.add(log);
        	}
        }
        return logs;
    }
    
    global override void finish(Database.BatchableContext BC)
    {
    	//处理重复联系人
    	Map<ID,Contact> noDupContactMap = new Map<ID,Contact>(); 
    	if(this.sfAllConList != null)
    	{
    		for(Contact con : this.sfAllConList)
	    	{
				noDupContactMap.put(con.ID,con);
	    	}
	    	List<Contact> cons = noDupContactMap.values();
	    	if(cons != null)
	    	{
		    	Database.SaveResult[] conRes = Database.update(cons, false);
	    		this.Logs(this.toLogs(cons,conRes));
	    	}
    	}
    
        this.handleNext();
    }
    
    global override void handleNext()
    {
        if(this.IsChain)
        {
            SFDC2HRQContactUpdateSyncBatch nextBatch = new SFDC2HRQContactUpdateSyncBatch();
            nextBatch.currentTime = this.currentTime;
            nextBatch.IsChain = this.IsChain;
            nextBatch.BatchSize = this.BatchSize; 
            database.executeBatch(nextBatch, nextBatch.BatchSize);
        }
    }
}