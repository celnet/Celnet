/**
 * Author: Steven
 * Date: 2014-12-15
 * Description: 查询
 **/
public without sharing class AccountLastActivityDateScheduleJob implements Schedulable{
	public void execute(SchedulableContext sc){
		
		List<Account> updateaccs = new List<Account>();
		Set<Id> accs = new Set<Id>();
		
		Datetime startDatetime = Datetime.newInstance(Datetime.now().year(), Datetime.now().month(), Datetime.now().day(), 0,0,0);
		Datetime endDatetime = Datetime.newInstance(Datetime.now().year(), Datetime.now().month(), Datetime.now().day(),23,59,59);
		
		for(EmailInfo__c ei :[Select Id, Account__c From EmailInfo__c Where CreatedDate >=: startDatetime And CreatedDate <=: endDatetime]){
			accs.add(ei.Account__c);
		}
		
		for(Contact con : [Select Id, AccountId From Contact Where CreatedDate >=: startDatetime And CreatedDate <=: endDatetime]){
			accs.add(con.AccountId);
		}
		
		for(Attachment att : [Select Id, ParentId From Attachment Where CreatedDate >=: startDatetime And CreatedDate <=: endDatetime]){
			if(att.ParentId.getSobjecttype() == Account.Sobjecttype){
				accs.add(att.ParentId);
			}
		}
		
		for(Note n : [Select Id, ParentId From Note Where CreatedDate >=: startDatetime And CreatedDate <=: endDatetime]){
			if(n.ParentId.getSobjecttype() == Account.Sobjecttype){
				accs.add(n.ParentId);
			}
		}
		
		for(Task t : [Select Id, AccountId From Task Where CreatedDate >=: startDatetime And CreatedDate <=: endDatetime]){
			accs.add(t.AccountId);
		}
		
		for(Id i : accs){
			updateaccs.add(new Account(Id = i, LastActivityDate__c = Date.today()));
		}
		
		if(!updateaccs.isEmpty())
		update updateaccs;
	}
	
	// 历史数据全都更新一次
	public void runTotal(){
		Map<Id, Date> accIdDateMap = new Map<Id, Date>();
		
		List<Account> accounts = [Select Id,
									(Select Id, CreatedDate From Contacts order by CreatedDate desc limit 1),
									(Select Id, CreatedDate From Notes order by CreatedDate desc limit 1),
									(Select Id, CreatedDate From Tasks order by CreatedDate desc limit 1),
									(Select Id, CreatedDate From Account__r order by CreatedDate desc limit 1),
									(Select Id, CreatedDate From Attachments order by CreatedDate desc limit 1) 
								  	From Account];
		
		for(Account acc : accounts){
			System.debug(acc.Contacts + '-' + acc.Notes + '-' + acc.Tasks + '-' + acc.Account__r + '-' + acc.Attachments);
			System.debug(acc.Contacts.isEmpty() + '-' + acc.Notes.isEmpty() + '-' + acc.Tasks.isEmpty() + '-' + acc.Account__r.isEmpty() + '-' + acc.Attachments.isEmpty());
			if(!acc.Contacts.isEmpty()){
				if(accIdDateMap.get(acc.Id) == null || accIdDateMap.get(acc.Id) < acc.Contacts[0].CreatedDate.date()){
					accIdDateMap.put(acc.Id, acc.Contacts[0].CreatedDate.date());
				} 
			}
			
			if(!acc.Notes.isEmpty()){
				if(accIdDateMap.get(acc.Id) == null || accIdDateMap.get(acc.Id) < acc.Notes[0].CreatedDate.date()){
					accIdDateMap.put(acc.Id, acc.Notes[0].CreatedDate.date());
				} 
			}
			
			if(!acc.Tasks.isEmpty()){
				if(accIdDateMap.get(acc.Id) == null || accIdDateMap.get(acc.Id) < acc.Tasks[0].CreatedDate.date()){
					accIdDateMap.put(acc.Id, acc.Tasks[0].CreatedDate.date());
				} 
			}
			
			if(!acc.Account__r.isEmpty()){
				if(accIdDateMap.get(acc.Id) == null || accIdDateMap.get(acc.Id) < acc.Account__r[0].CreatedDate.date()){
					accIdDateMap.put(acc.Id, acc.Account__r[0].CreatedDate.date());
				} 
			}
			
			if(!acc.Attachments.isEmpty()){
				if(accIdDateMap.get(acc.Id) == null || accIdDateMap.get(acc.Id) < acc.Attachments[0].CreatedDate.date()){
					accIdDateMap.put(acc.Id, acc.Attachments[0].CreatedDate.date());
				} 
			}
		}
		
		if(!accIdDateMap.isEmpty()) {
			List<Account> accs = new List<Account>();
			
			for(Id accId : accIdDateMap.keySet()) {
				Account acc = new Account(Id = accId, LastActivityDate__c = accIdDateMap.get(accId));
				accs.add(acc);
			}
			
			update accs;
		}
	}
}