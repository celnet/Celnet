/**
 * Author:Sunny
 * Test ExpenseClaimFormController
 */
@isTest
private class Test_ExpenseClaimFormController {

    static testMethod void myUnitTest() {
        CostReimbursement__c cr = new CostReimbursement__c();
        cr.Name = 'test ss';
        cr.Year__c='2014';
        cr.reimbursementMonth__c = '3';
        insert cr;
        
        List<CostReimbursementDetail__c> list_crd = new List<CostReimbursementDetail__c>();
        ID RTfee = [Select Id From RecordType Where DeveloperName = 'GeneralFeeDetail' and SobjectType = 'CostReimbursementDetail__c'].Id;
        ID RToutfee = [Select Id From RecordType Where DeveloperName = 'outfeedetail' and SobjectType = 'CostReimbursementDetail__c'].Id;
        CostReimbursementDetail__c crd = new CostReimbursementDetail__c();
        crd.CostName__c = cr.Id;
        crd.CostAmount__c = 1234567;
        crd.GeneralCostType__c = '通讯费';
        crd.RecordTypeId = RTfee;
        list_crd.add(crd);
        CostReimbursementDetail__c crd2 = new CostReimbursementDetail__c();
        crd2.CostName__c = cr.Id;
        crd2.CostAmount__c = 1234567;
        crd2.GeneralCostType__c = '交通费';
        crd2.RecordTypeId = RTfee;
        list_crd.add(crd2);
        CostReimbursementDetail__c crd3 = new CostReimbursementDetail__c();
        crd3.CostName__c = cr.Id;
        crd3.CostAmount__c = 1234567;
        crd3.GeneralCostType__c = '招待费';
        crd3.RecordTypeId = RTfee;
        list_crd.add(crd3);
        
        CostReimbursementDetail__c crd4 = new CostReimbursementDetail__c();
        crd4.CostName__c = cr.Id;
        crd4.CostAmount__c = 1234567;
        crd4.TravelCostType__c = '住宿费';
        crd4.TravelAddress__c = '上海';
        crd4.RecordTypeId = RToutfee;
        crd4.TravelStartDate__c=date.today().addDays(-1);
        crd4.TravleEndDate__c=date.today().addDays(1);
        list_crd.add(crd4);
        CostReimbursementDetail__c crd5 = new CostReimbursementDetail__c();
        crd5.CostName__c = cr.Id;
        crd5.CostAmount__c = 1234567;
        crd5.TravelCostType__c = '招待费';
        crd5.TravelAddress__c = '上海';
        crd5.TravelStartDate__c=date.today().addDays(-1);
        crd5.TravleEndDate__c=date.today().addDays(1);
        crd5.RecordTypeId = RToutfee;
        list_crd.add(crd5);
        CostReimbursementDetail__c crd6 = new CostReimbursementDetail__c();
        crd6.CostName__c = cr.Id;
        crd6.CostAmount__c = 1234567;
        crd6.TravelCostType__c = '火车、汽车票';
        crd6.TravelAddress__c = '上海';
        crd6.RecordTypeId = RToutfee;
        crd6.TravelStartDate__c=date.today().addDays(-1);
        crd6.TravleEndDate__c=date.today().addDays(1);
        list_crd.add(crd6);
        CostReimbursementDetail__c crd7 = new CostReimbursementDetail__c();
        crd7.CostName__c = cr.Id;
        crd7.CostAmount__c = 1234567;
        crd7.TravelCostType__c = '飞机、轮船票';
        crd7.TravelAddress__c = '上海';
        crd7.RecordTypeId = RToutfee;
        crd7.TravelStartDate__c=date.today().addDays(-1);
        crd7.TravleEndDate__c=date.today().addDays(1);
        list_crd.add(crd7);
        CostReimbursementDetail__c crd8 = new CostReimbursementDetail__c();
        crd8.CostName__c = cr.Id;
        crd8.CostAmount__c = 1234567;
        crd8.TravelCostType__c = '其他';
        crd8.TravelAddress__c = '上海';
        crd8.RecordTypeId = RToutfee;
        crd8.TravelStartDate__c=date.today().addDays(-1);
        crd8.TravleEndDate__c=date.today().addDays(1);
        list_crd.add(crd8);
        insert list_crd;
        
        system.Test.startTest();
        Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(cr);
        ExpenseClaimFormController ecfc = new ExpenseClaimFormController(controller);
        system.Test.stopTest();
    }
    
    static testMethod void myUnitTest2() {
    	CostReimbursement__c cr = new CostReimbursement__c();
        cr.Name = 'test ss';
        cr.Year__c='2014';
        cr.reimbursementMonth__c = '3';
        insert cr;
        
        List<CostReimbursementDetail__c> list_crd = new List<CostReimbursementDetail__c>();
        ID RTfee = [Select Id From RecordType Where DeveloperName = 'GeneralFeeDetail' and SobjectType = 'CostReimbursementDetail__c'].Id;
        ID RToutfee = [Select Id From RecordType Where DeveloperName = 'outfeedetail' and SobjectType = 'CostReimbursementDetail__c'].Id;
        CostReimbursementDetail__c crd = new CostReimbursementDetail__c();
        crd.CostName__c = cr.Id;
        crd.CostAmount__c = 1234567;
        crd.GeneralCostType__c = '通讯费';
        crd.RecordTypeId = RTfee;
        list_crd.add(crd);
        
        
        CostReimbursementDetail__c crd4 = new CostReimbursementDetail__c();
        crd4.CostName__c = cr.Id;
        crd4.CostAmount__c = 1234567;
        crd4.TravelCostType__c = '住宿费';
        crd4.TravelAddress__c = '上海';
        crd.RecordTypeId = RToutfee;
        list_crd.add(crd4);
        
        insert list_crd;
        
        system.Test.startTest();
        Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(cr);
        ExpenseClaimFormController ecfc = new ExpenseClaimFormController(controller);
        system.Test.stopTest();
    }
    static testMethod void myUnitTest3() {
    	system.test.startTest();
    	string s1 = UtilToolClass.translate(400.01);
    	s1 = UtilToolClass.translate(1234567.23);
    	s1 = UtilToolClass.translate(-1);
    	s1 = UtilToolClass.translate(0);
    	system.test.stopTest();
    }
}