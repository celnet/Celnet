/*
作者：winter
创建时间：2014-07-16
功能：封装HRQMember
*/
public class HRQMember {
	public  string Id{get;set;}
	public  string Name{get;set;}
	public  Integer Gender{get;set;}
	public  string Regip{get;set;}
	public  string Regdate{get;set;}
	public  string Email{get;set;}
	public  string Mobile{get;set;}
	public  string Telephone{get;set;}
}