/*
作者：Tommy Liu
电子油箱：tommyliu@cideatech.com
创建时间：2014-07-08
功能：华尔圈数据同步基础类，此类为抽象基础类，规定了数据同步的规范，所有从SFDC推送到华尔圈的具体同步类都需要实现此类
同步方向：SFDC到华尔圈
*/
global abstract class SFDC2HRQDataSyncBatch implements Database.Batchable<SObject>, Database.Stateful, Database.AllowsCallouts, Schedulable 
{
    protected HRQService hrqService; 
    public Boolean IsChain {get; set;}
    public Integer BatchSize {get; set;}
    
    public HRQ_Sync_Setting__c syncSetting;
    
    public DateTime currentTime;
    
    private void initHRQService()
    {
    	try
    	{
        	this.hrqService = new HRQService();
       	}
		catch (CalloutException exc)
		{
			this.Log(exc); 
			throw exc;
		}
    }
    
    public SFDC2HRQDataSyncBatch()
    {
        this.IsChain = true;
        this.BatchSize = 100;
        this.syncSetting = [Select Id, 
                                HRQ2SFDC_Last_Synced_EndOn__c,
                                HRQ2SFDC_Last_Synced_StartOn__c,
                                SFDC2HRQ_Update_By_History__c,
                                SFDC2HRQ_Last_Synced_EndOn__c,
                                SFDC2HRQ_Last_Synced_StartOn__c
                            From HRQ_Sync_Setting__c
                            Where IsActive__c = true Limit 1];
       System.debug('时间'+syncSetting.HRQ2SFDC_Last_Synced_EndOn__c+'____________!!!');
    }
    global virtual Database.QueryLocator start(Database.BatchableContext BC)
    {
        this.initHRQService();
        system.debug('start!!!!!!!!!!!');
        return this.getDataScop();
    }

    global virtual void execute(Database.BatchableContext BC, List<SObject> scope)
    {
        this.convertData(scope); 
        
        try
        {
        	this.pushDataToHRQ();
        }
		catch (CalloutException exc)
		{
			this.Log(exc); 
			throw exc;
		}
    }

    global virtual void finish(Database.BatchableContext BC)
    {
        this.handleNext();
    }
    
    //实现Schedulable 接口，使得所有的SFDC2HRQ数据处理程序都可以被计划执行，子类和一重写此方法以给出更特殊的实现方式
    global virtual void execute(SchedulableContext SC) 
    {
        Database.executeBatch(this, this.BatchSize);//TODO:可以考虑将批包含的数量变为可配置 
    }
    
    global virtual String getComName()
    {
    	return 'SFDC2HRQDataSyncBatch';
    }
   
    global abstract Database.QueryLocator getDataScop();//需要实现确定同步到华尔圈的数据范围，记录更新时间〉=上次同步时间
    
    global abstract void convertData(List<SObject> scope);//需要实现将从SFDC数据转化为华尔圈类型数据
    
    global abstract void pushDataToHRQ();//需要实现将转化后的华尔圈数据推送到华尔圈Callout
    
    global abstract void handleNext();//需要实现启动其它数据的Batch，将过程连接起来
    
    
    	//保存单个log
	public void Log(HRQ_Sync_Log__c log)
	{
		if(log == null)
		{
			return;
		}
		database.insert(log, false);
	}
	
	//保存多个Log
	public void Logs(HRQ_Sync_Log__c[] logs)
	{
		database.insert(logs, false);
	}
	
	//将Excecption转化为Log并保存
	public void Log(Exception exc)
	{
		if(exc == null)
		{
			return;
		}
		HRQ_Sync_Log__c log = this.toLog(exc);
		this.log(log);
	}
	
	//将SaveResult转化为Log
	protected HRQ_Sync_Log__c toLog(Database.SaveResult re)
	{
		HRQ_Sync_Log__c log = new HRQ_Sync_Log__c();
		log.Direction__c = 'SFDC2HRQ';
		log.Com__c = this.getComName();
		Database.Error[] errors;
		if(!re.isSuccess())
		{
			log.Type__c = '错误';
			errors = re.getErrors();
		}
		else
		{
			log.Type__c = '信息';
		}
		log.SFDC_ID__c = re.getId();
		log.Detail__c = '';
		if(errors != null)
		{
			for(Database.Error error : errors)
			{
				log.Detail__c += 'Message:';
				log.Detail__c += error.getMessage();
				log.Detail__c += '\r\n';
				log.Detail__c += 'Status Code:';
				log.Detail__c += error.getStatusCode();
				log.Detail__c += '\r\n';
			}
		}
		return log;
	}
	
	//将Exception转化为Log
	protected HRQ_Sync_Log__c toLog(Exception exc)
	{
		Exception cause = exc.getCause();
		Integer line = exc.getLineNumber();
		HRQ_Sync_Log__c log = new HRQ_Sync_Log__c();
		log.Com__c = this.getComName();
		log.Type__c = '错误';
		log.Direction__c = 'SFDC2HRQ';
		log.Sub_Type__c = exc.getTypeName();
		if(line != null)
		{
			log.Line_Number__c = String.valueof(line);
		}
		log.Detail__c = '';
		log.Detail__c += '第' + String.valueof(line) + '行\n\r';
		log.Detail__c += exc.getTypeName();
		log.Detail__c += '\n\r';
		log.Detail__c += exc.getMessage();
		log.Detail__c += '\n\r';
		log.Detail__c += exc.getStackTraceString();
		log.Detail__c += '\n\r';
		if(cause != null)
		{
			Integer line2 = exc.getLineNumber();
			log.Detail__c += '-------------------------------------------------------\n\r';
			if(line2 != null)
			{
				log.Detail__c += '第' + String.valueof(line2) + '行\n\r';
			}
			log.Detail__c += cause.getTypeName();
			log.Detail__c += '\n\r';
			log.Detail__c += cause.getMessage();
			log.Detail__c += '\n\r';
			log.Detail__c += cause.getStackTraceString();
			log.Detail__c += '\n\r';
		}
		
		return log;
	}
}