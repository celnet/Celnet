/**
 * Author : Sunny
 * Des:借款单PDF
 **/
public class LoanFormController 
{
	private ID LoanId;
	public Loan__c oLoan{get;set;}
	public String sYear{get{return String.valueOf(oLoan.LoanDate__c.year());}}
	public String sMonth{get{return String.valueOf(oLoan.LoanDate__c.month());}}
	public String sDay{get{return String.valueOf(oLoan.LoanDate__c.day());}}
	public String sLoanMoney{get{return UtilToolClass.translate(oLoan.LoanMoney__c);}}
	
	public LoanFormController(Apexpages.Standardcontroller controller)
	{
		LoanId = controller.getId();
		initLoanInfo();
	}
	private void initLoanInfo()
	{
		oLoan = [Select Id,LoanMoney__c,ApplyDepartment__c,Borrower__c,LoanReason__c,LoanDate__c,Borrower__r.Alias 
				From Loan__c Where Id =: this.LoanId];
	}
	
}