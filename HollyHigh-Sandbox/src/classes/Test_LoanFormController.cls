/**
 * Test LoanFormController
 */
@isTest
private class Test_LoanFormController {

    static testMethod void myUnitTest() {
        //Loan__c
        Loan__c  l = new Loan__c();
        l.LoanMoney__c = 13423;
        l.ApplyDepartment__c = '跨境并购部';
        l.Borrower__c = userinfo.getUserId();
        l.LoanReason__c = 'bulabulabula';
        l.ApproveState__c = '审批通过';
        l.LoanDate__c = date.today();
        insert l;
        system.Test.startTest();
        Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(l);
        LoanFormController lfc = new LoanFormController(controller);
        system.Test.stopTest();
    }
}