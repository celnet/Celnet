/**
 * test MailServerInboundEmailHandler
 */
@isTest
private class Test_MailServerInboundEmailHandler {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Account a = new Account();
        a.Name = 'Test Acc';
        a.Industry = '医药';
        insert a;
        List<Contact> list_c = new List<Contact>();
        Contact c = New Contact();
        c.FirstName = 'test';
        c.LastName = 'wef';
        c.AccountId = a.Id;
        c.LeadSource = '官网';
        c.MobilePhone='123324';
        c.Email = 'test@test.com';
        list_c.add(c);
        Contact c1 = New Contact();
        c1.FirstName = 'test';
        c1.LastName = 'wef';
        c1.AccountId = a.Id;
        c1.LeadSource = '官网';
        c1.MobilePhone='123324';
        c1.Email = 'test2@test.com';
        list_c.add(c1);
        insert list_c;
        //自定义设置
        Internal_Email_Domain__c ied = new Internal_Email_Domain__c();
        ied.Name = 'cideatech.com';
        insert ied;
        Internal_Email_Domain__c ied2 = new Internal_Email_Domain__c();
        ied2.Name = 'hollyhigh.cn';
        insert ied2;
        system.Test.startTest();
        //CC列表
        List<String> list_cc = new List<String>();
        //发送邮箱
        String sFrom = [Select Id,Email From User Where Id =: UserInfo.getUserId()].Email;
        //to列表
        List<String> list_to = new List<String>();
        list_to.add('test@test.com');
        list_to.add('test2@test.com');
        Messaging.InboundEmail ie = new Messaging.InboundEmail();
        ie.fromAddress = sFrom;
        ie.toAddresses = list_to;
        ie.ccAddresses = list_cc;
        ie.plainTextBody = 'test';
        
        
        MailServerInboundEmailHandler msieh = new MailServerInboundEmailHandler();
        msieh.handleInboundEmail(ie, new Messaging.InboundEnvelope());
        
        //检验
        List<EmailInfo__c> list_email = [Select Id,OwnerId,Date__c,Email_Body__c,Email_To_Addresses__c,	Contact__c,	Account__c From EmailInfo__c];
        system.debug('Email Info List :'+list_email);
        
        String sFrom2 = 'xxx@xxx.com';
        List<String> list_to2 = new List<String>();
        list_to2.add('xxnd.wnfe@dfw.com');
        Messaging.InboundEmail ie2 = new Messaging.InboundEmail();
        ie2.fromAddress = sFrom2;
        ie2.toAddresses = list_to2;
        ie2.ccAddresses = list_cc;
        ie2.plainTextBody = 'test2';
        
        
        MailServerInboundEmailHandler msieh2 = new MailServerInboundEmailHandler();
        msieh2.handleInboundEmail(ie2, new Messaging.InboundEnvelope());
        system.Test.stopTest();
        
    }
    static testMethod void myUnitTest2() {
        // TO DO: implement unit test
        Account a = new Account();
        a.Name = 'Test Acc';
        a.Industry = '医药';
        insert a;
        List<Contact> list_c = new List<Contact>();
        Contact c = New Contact();
        c.FirstName = 'test';
        c.LastName = 'wef';
        c.AccountId = a.Id;
        c.LeadSource = '官网';
        c.MobilePhone='123324';
        c.Email = 'test@test.com';
        list_c.add(c);
        Contact c1 = New Contact();
        c1.FirstName = 'test';
        c1.LastName = 'wef';
        c1.AccountId = a.Id;
        c1.LeadSource = '官网';
        c1.MobilePhone='123324';
        c1.Email = 'test2@test.com';
        list_c.add(c1);
        insert list_c;
        //自定义设置
        Internal_Email_Domain__c ied = new Internal_Email_Domain__c();
        ied.Name = 'cideatech.com';
        insert ied;
        Internal_Email_Domain__c ied2 = new Internal_Email_Domain__c();
        ied2.Name = 'hollyhigh.cn';
        insert ied2;
        system.Test.startTest();
        //CC列表
        List<String> list_cc = new List<String>();
        //发送邮箱
        String sFrom = 'test2@test.com';
        //to列表
        List<String> list_to = new List<String>();
        list_to.add([Select Id,Email From User Where Id =: UserInfo.getUserId()].Email);
        Messaging.InboundEmail ie = new Messaging.InboundEmail();
        ie.fromAddress = sFrom;
        ie.toAddresses = list_to;
        ie.ccAddresses = list_cc;
        ie.plainTextBody = 'test';
        
        
        MailServerInboundEmailHandler msieh = new MailServerInboundEmailHandler();
        msieh.handleInboundEmail(ie, new Messaging.InboundEnvelope());
        
        //检验
        List<EmailInfo__c> list_email = [Select Id,OwnerId,Date__c,Email_Body__c,Email_To_Addresses__c,	Contact__c,	Account__c From EmailInfo__c];
        system.debug('Email Info List :'+list_email);
        
        String sFrom2 = [Select Id,Email From User Where Id =: UserInfo.getUserId()].Email;
        List<String> list_to2 = new List<String>();
        list_to2.add('xxnd.wnfe@dfw.com');
        Messaging.InboundEmail ie2 = new Messaging.InboundEmail();
        ie2.fromAddress = sFrom2;
        ie2.toAddresses = list_to2;
        ie2.ccAddresses = list_cc;
        ie2.htmlBody = 'test2';
        
        
        MailServerInboundEmailHandler msieh2 = new MailServerInboundEmailHandler();
        msieh2.handleInboundEmail(ie2, new Messaging.InboundEnvelope());
        system.Test.stopTest();
        
    }
}