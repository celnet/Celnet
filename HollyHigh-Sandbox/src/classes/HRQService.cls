/*
作者：winter
创建时间：2014-07-16
功能：request获得JSON并将JSON内容属性封装各个类.
*/
public class HRQService 
{
	private string username;
	private string password;
	private string HRQURL;
	public class HRQWapper
	{
		public List<HRQProject> Projects {get; set;}
		public List<HRQRelatedContact> RelatedContacts {get; set;}
		public List<HRQMember> Members {get; set;}
		public List<HRQIndustry> Industries {get; set;}
		public List<HRQArea> Areas {get; set;}
		public List<HRQSaveResult> SaveResults {get; set;}
	}
	
	//初始化账号、密码、URL
	public HRQService()
	{
		HRQ_Setting__c set1 = HRQ_Setting__c.getValues('HRQLoginName');
       	this.username = set1.Value__c;
       	HRQ_Setting__c set2 = HRQ_Setting__c.getValues('HRQLoginPassword');
       	this.password = set2.Value__c;
       	HRQ_Setting__c set3 = HRQ_Setting__c.getValues('HRQServiceUrl');
       	this.HRQURL = set3.Value__c;
	}

	//GET
	public HttpResponse getInfoFromExternalService(String endpoint) {
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('GET');
	
        Blob headerValue = Blob.valueOf(this.username + ':' + this.password);
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        req.setTimeout(30000);
        
        HttpResponse res = h.send(req);
        return res;
    }
    
    //POST
    public HttpResponse PostMessageToHRQService(String endpoint,String jsontext,String postdatatype) {
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('POST');
        
        //string s1 = 'Cideatech';
	 	//string s2 = 'PyWHY5WjKqCrqq2c';
        Blob headerValue = Blob.valueOf(this.username + ':' + this.password);
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        req.setTimeout(30000);
        
        req.setBody(postdatatype+EncodingUtil.urlEncode(jsontext,'utf-8'));
        HttpResponse res = h.send(req);
        return res;
    }
	
	//接收HRQProject //TODO	2014.7.22 17:18 测试通过。应该想一个好的解决方案解决转义字符问题
	public List<HRQProject> GetProjectList(Integer pageCursor, Integer pageRows)
	{
		Httpresponse res = getInfoFromExternalService(this.HRQURL+'?action=GetProjectList&pageCursor='+pageCursor+'&pageRows='+pageRows);
		string jsontext = '{' + '"Projects":'+ res.getBody()+'}';
		System.debug('#####'+jsontext);
		HRQWapper wapper = (HRQWapper)JSON.deserialize(jsontext,HRQWapper.class);
		HRQProject[] projects = wapper.Projects;
		
		return projects;
	}
	
	//接收HRQRelatedContact	2014.7.22	测试通过的	
	public List<HRQRelatedContact> GetRelatedContactList(Integer pageCursor, Integer pageRows)
	{
		HttpResponse res = getInfoFromExternalService(this.HRQURL+'?action=GetRelatedContactList&pageCursor='+pageCursor+'&pageRows='+pageRows);
		string jsontext = '{' + '"RelatedContacts":'+res.getBody()+'}';
		system.debug(jsontext  + '!!!!!!!!!!!');
		HRQWapper wapper = (HRQWapper)JSON.deserialize(jsontext,HRQWapper.class);
		HRQRelatedContact[] relatedcontacts= wapper.RelatedContacts;
		return relatedcontacts;
	}
	
	//接收HRQMember 2014.7.21 17:00 测试有效
	public List<HRQMember> GetMemberList(Integer pageCursor, Integer pageRows)
	{
		HttpResponse res = getInfoFromExternalService(this.HRQURL+'?action=GetMemberList&pageCursor='+pageCursor+'&pageRows='+pageRows);
		string jsontext = '{' + '"Members":'+ res.getBody()+'}';
		System.debug('____this is json text____'+jsontext);
		HRQWapper wapper = (HRQWapper)JSON.deserialize(jsontext,HRQWapper.class);
		HRQMember[] members = wapper.Members;
		return members;
	}
	
	//接收HRQIndustry 2014.7.21 16:16 测试有效
	public List<HRQIndustry> GetIndustryList()
	{
		HttpResponse res = getInfoFromExternalService(this.HRQURL+'?action=GetIndustryList');
		string jsontext ='{' + '"Industries":'+ res.getBody()+'}';
		system.debug(jsontext + 'this is a json!!');
		HRQWapper wapper = (HRQWapper)JSON.deserialize(jsontext,HRQWapper.class);
		List<HRQIndustry> industrys = wapper.Industries;
		return industrys;
	}
	
	//接收HRQArea 2014.7.21 16:16 测试有效
	public List<HRQArea> GetAreaList()
	{
		HttpResponse res = getInfoFromExternalService(this.HRQURL+'?action=GetAreaList');
		string jsontext ='{' + '"Areas":'+ res.getBody()+'}';
		system.debug(jsontext + 'this is a json!!');
		HRQWapper wapper = (HRQWapper)JSON.deserialize(jsontext,HRQWapper.class);
		HRQArea[] areas = wapper.Areas;
		return areas;
	}
	
	//RelatedContactUpdate接口	2014.7.24	15:52	测试通过
	public HRQSaveResult[] UpdateRelatedContact(HRQRelatedContact[] contactList)
	{
		string jsontext = '{' + '"RelatedContacts":'+ JSON.serialize(contactList) + '}';
		System.debug('!!!!!!!'+jsontext+'!!!!!!!!');
		Httpresponse res =  PostMessageToHRQService(this.HRQURL+'?action= UpdateRelatedContact', jsontext, 'contactList=');
		string backtext = '{' + '"SaveResults":'+ res.getBody() + '}';
		System.debug(backtext+'_____________这是返回JSON');
		HRQWapper wapper = (HRQWapper)JSON.deserialize(backtext,HRQWapper.class);
		HRQSaveResult[] saveresults = wapper.SaveResults;
		for(HRQSaveResult sr : saveresults)
		{
			System.debug('$$$$$$$$'+sr.Error+'$$$$$$$$'+sr.Id+'$$$$$$$$'+sr.IsSuccess);
		}
		return saveresults;
	}
	
	//RelatedContactCreate接口	2014.7.24	15:19	测试通过
	public HRQSaveResult[] CreateRelatedContact(HRQRelatedContact[] contactList)
	{
		string jsontext = '{' + '"RelatedContacts":'+ JSON.serialize(contactList) + '}';
		Httpresponse res =  PostMessageToHRQService(this.HRQURL+'?action=CreateRelatedContact', jsontext, 'contactList=');
		string backtext = '{' + '"SaveResults":'+ res.getBody() + '}';
		System.debug(backtext+'_____________这是返回JSON');
		HRQWapper wapper = (HRQWapper)JSON.deserialize(backtext,HRQWapper.class);
		HRQSaveResult[] saveresults = wapper.SaveResults;
		for(HRQSaveResult sr : saveresults)
		{
			System.debug('$$$$$$$$'+sr.Error+'$$$$$$$$'+sr.Id+'$$$$$$$$'+sr.IsSuccess);
		}
		return saveresults;
	}
	
	//ProjectUpdate接口	2014.7.24	13:02	测试通过：空值是否要忽略？？？
	public HRQSaveResult[] UpdateProject(HRQProject[] projectList)
	{
		string jsontext = '{' + '"Projects":'+ JSON.serialize(projectList) + '}';
		system.debug(jsontext + '我们转的json！！');
		Httpresponse res = PostMessageToHRQService(this.HRQURL+'?action=UpdateProject', jsontext ,'projectList=');
		string backtext = '{' + '"SaveResults":'+ res.getBody() + '}';
		System.debug(backtext+'_____________这是返回JSON');
		HRQWapper wapper = (HRQWapper)JSON.deserialize(backtext,HRQWapper.class);
		HRQSaveResult[] saveresults = wapper.SaveResults;
		for(HRQSaveResult sr : saveresults)
		{
			if(sr.IsSuccess == 0)
			{
				System.debug('$$$$$$$$'+sr.Error+'$$$$$$$$'+sr.Id+'$$$$$$$$');
			}
		}
		return saveresults;
	}
	
	//ProjectCreate接口调用	2014.7.24	12:00	测试通过:创建成功，返回ID为HRQ里的projectID；创建失败，返回ID为：-1
	public HRQSaveResult[] CreateProject(HRQProject[] projectList)
	{
		string jsontext = '{' + '"Projects":'+ JSON.serialize(projectList) + '}';
		System.debug(jsontext+'________________这是json');
		Httpresponse res = PostMessageToHRQService(this.HRQURL+'?action=CreateProject', jsontext,'projectList=');
		string backtext = '{' + '"SaveResults":'+ res.getBody() + '}';
		System.debug(backtext+'_____________这是返回JSON');
		HRQWapper wapper = (HRQWapper)JSON.deserialize(backtext,HRQWapper.class);
		HRQSaveResult[] saveresults = wapper.SaveResults;
		for(HRQSaveResult sr : saveresults)
		{
			System.debug('$$$$$$$$'+sr.Error+'$$$$$$$$'+sr.Id+'$$$$$$$$'+sr.IsSuccess);
		}
		return saveresults;
	}
}