public class HRQArea {
	public string Id{get;set;}
	public string Name{get;set;}
	public string ParentAreaId{get;set;}
	public Boolean Root{get;set;}
}