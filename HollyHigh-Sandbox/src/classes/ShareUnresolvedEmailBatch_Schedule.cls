/**
 * Schedulable ShareUnresolvedEmailBatch
 * 
 */
public class ShareUnresolvedEmailBatch_Schedule implements Schedulable{
	public void execute(SchedulableContext sc) {
    	ShareUnresolvedEmailBatch sueb = new ShareUnresolvedEmailBatch();
        Database.executeBatch(sueb);
    }
}