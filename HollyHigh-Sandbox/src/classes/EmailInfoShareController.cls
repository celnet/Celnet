/*
 * Author: Steven
 * Date: 2014-4-9
 * Description: 手动共享电子邮件(EmailInfo__c)并创建任务
 */
public class EmailInfoShareController {
	public boolean hasPermission{get;set;}
	public boolean hasNoPermission{get;set;}
	
	// 当前EmailInfo
	public EmailInfo__c emailInfo{get;set;}
	public Id eiId{get;set;}
	
	public List<EmailInfo__Share> eisList{get;set;}
	public List<User> userList{get;set;}
	
	public List<EmailInfoSharePack> eispList{get;set;}
	public List<UserPack> upList{get;set;}
	
	public class UserPack
	{
		public boolean isCheck{get;set;}
		public User user{get;set;}
		
		public UserPack(User u)
		{
			isCheck = false;
			user = u;
		}
	}
	
	public class EmailInfoSharePack
	{
		public boolean isCheck{get;set;}
		public EmailInfo__Share eis{get;set;}
		public String name{get;set;}
		public String rowCause{get;set;}
		public String accessLevel{get;set;}
		
		public EmailInfoSharePack(EmailInfo__Share e)
		{
			isCheck = false;
			rowCause = '手动共享';
			accessLevel = '只读';
			User u1 = [Select Id, Name From User Where Id =: e.UserOrGroupId];
			name = u1.Name;
			eis = e;
		}
	}
	
	public EmailInfoShareController(ApexPages.standardController controller)
	{
		this.eiId = ApexPages.currentPage().getParameters().get('id');
		List<EmailInfo__Share> currentUserShareList = [Select Id, AccessLevel From EmailInfo__Share Where UserOrGroupId =: UserInfo.getUserId() And ParentId =: this.eiId];
		if(currentUserShareList.size() > 0  && currentUserShareList[0].AccessLevel != 'Read')
		{
			this.hasPermission = true;
			this.hasNoPermission = false;	
		}
		else
		{
			this.hasNoPermission = true;
			this.hasPermission = false;
		}
		
		this.emailInfo = new EmailInfo__c();
		this.emailInfo = [Select OwnerId, Name From EmailInfo__c Where Id =: this.eiId];
		this.eispList = new List<EmailInfoSharePack>();
		
		eisList = new List<EmailInfo__Share>();
		eisList = [Select AccessLevel, UserOrGroupId, RowCause 
					From EmailInfo__Share 
					Where IsDeleted = false 
					And ParentId =: controller.getId()
					And RowCause != 'Owner'];
		
		Set<Id> idSet = new Set<Id>();
		if(eisList.size() > 0)
		{
			for(EmailInfo__Share eis : eisList)
			{
				idSet.add(eis.UserOrGroupId);
				eispList.add(new EmailInfoSharePack(eis));
			}
		}
		else
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, '该邮件暂未共享给任何用户');         
   			ApexPages.addMessage(msg);
		}
		
		userList = new List<User>();
		upList = new List<UserPack>();
		userList = [Select Id, Name From User Where UserType = 'Standard' And IsActive = true And Id Not IN: idSet And Id !=: this.emailInfo.OwnerId];
		if(userList.size() > 0)
		{
			for(User u : userList)
			{
				upList.add(new UserPack(u));
			}
		}
	}
	
	public PageReference deleteShare()
	{
		List<EmailInfo__Share> eisDeleteList = new List<EmailInfo__Share>();
		if(this.eisList.size() > 0)
		{
			for(EmailInfoSharePack eisp : this.eispList)
			{
				if(eisp.isCheck)
				{
					eisDeleteList.add(eisp.eis);
				}
			}
		}
		
		if(eisDeleteList.size() > 0)
		{
			delete eisDeleteList;
		}
		
		PageReference redirectPag = new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/' + this.eiId);
        redirectPag.setRedirect(true);
        return redirectPag;
	}
	
	public PageReference addShare()
	{
		List<EmailInfo__Share> eisInsertList = new List<EmailInfo__Share>();
		List<Task> taskList = new List<Task>();
		if(this.upList.size() > 0)
		{
			for(UserPack up : this.upList)
			{
				if(up.isCheck)
				{
					EmailInfo__Share e = new EmailInfo__Share();
					e.ParentId = this.eiId;
					e.UserOrGroupId = up.user.Id;
					e.AccessLevel = 'Read';
					
					eisInsertList.add(e);
					
					Task t1 = new Task();
					t1.OwnerId = up.user.Id;
					t1.WhatId = this.eiId;
					t1.Description = '现有电子邮件 “' + this.emailInfo.Name + '” 共享给您，请您及时查阅。';
					t1.ActivityDateTime__c = Datetime.now();
					t1.ActivityDate = Date.today();
					t1.Subject = '邮件共享提醒';
					t1.IsReminderSet = true;
					t1.ReminderDateTime = Datetime.newInstance(Date.today().year(), Date.today().month(), Date.today().day(), 8, 0, 0); 
					t1.Recommender__c = UserInfo.getName();
					t1.Receiver__c = up.user.Name;
					
					taskList.add(t1);
				}
			}
		}
		
		if(eisInsertList.size() > 0)
		{
			insert eisInsertList;
			insert taskList;
		}
		
		PageReference redirectPag = new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/' + this.eiId);
        redirectPag.setRedirect(true);
        return redirectPag;
	}
	
	public PageReference cancel()
	{
		PageReference redirectPag = new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/' + this.eiId);
        redirectPag.setRedirect(true);
        return redirectPag; 
	}
}