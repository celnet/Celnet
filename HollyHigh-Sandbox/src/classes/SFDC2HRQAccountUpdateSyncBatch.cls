/*
作者：Tommy Liu 
电子油箱：tommyliu@cideatech.com
创建时间：2014-07-08
功能：推送有变化的客户到华尔圈
同步方向：SFDC到华尔圈
*/
global class SFDC2HRQAccountUpdateSyncBatch extends SFDC2HRQDataSyncBatch
{
	private HRQProject[] projectsList;
	
	private Map<string,string> industry_1_0Maps;//1级行业反映射行业ID 	by winter
    private Map<string,string> industry_1_2Maps;//2级行业反映射行业ID		by winter
	
	global SFDC2HRQAccountUpdateSyncBatch()
	{
		super();
		//this.projectsList = new List<HRQProject>();
		
        this.industry_1_2Maps = new Map<string,string>();
        //2级行业，通过行业名称出行业ID by winter
        for(HRQ_Industry_Mapping__c him : [Select Id,HRQ_ID__c,SFDC_Industry_Name__c,SFDC_Sub_Industry_Name__c From HRQ_Industry_Mapping__c where SFDC_Sub_Industry_Name__c != null])
        {
        	industry_1_2Maps.put(him.SFDC_Sub_Industry_Name__c,him.HRQ_ID__c);
        }
        //1级行业,通过行业名称映射出行业ID by winter 
        this.industry_1_0Maps = new Map<string,string>();
        for(HRQ_Industry_Mapping__c him : [Select Id,HRQ_ID__c,SFDC_Industry_Name__c,HRQ_Parent_ID__c
        								  From HRQ_Industry_Mapping__c 
        								  where SFDC_Sub_Industry_Name__c =: null ])
        {
        	industry_1_0Maps.put(him.SFDC_Industry_Name__c,him.HRQ_ID__c);
        }
	}
	global override Database.QueryLocator getDataScop()
	{	
		//需要实现确定同步到华尔圈的数据范围，记录更新时间〉=上次同步时间
		//查询“是否排除”不等于Y的account数据，不将重复数据更新到HRQ 2014.8.11	by winter
		if(this.syncSetting.SFDC2HRQ_Update_By_History__c)
		{
			return Database.getQueryLocator([Select Id, Name,HRQ_ID__c,RecordTypeId,Description,Industry,IndustryType__c,HRQ_Industry_ID__c,HRQ_Area_ID__c,FirstAddress__c,Main_Business__c,
											HRQ_TotalAssets__c,HRQ_NetAssets__c,HRQ_OperatingIncome__c,HRQ_NetProfit__c,Content__c,HRQ_Uploader_ID__c,HRQ_UploadedOn__c,HRQ_OtherInfo__c,HRQ_Sync_Exception__c,
											TotalAssets__c,NetAssets__c,OperatingIncome__c,NetProfit__c,
											(Select Id, AccountId, CreatedById, CreatedDate, Field, OldValue, NewValue From Histories Where CreatedDate >=: syncSetting.SFDC2HRQ_Last_Synced_StartOn__c)
											From Account
											Where ID In(Select AccountId From AccountHistory Where CreatedDate >=: syncSetting.SFDC2HRQ_Last_Synced_StartOn__c)
											And HRQ_ID__c != null And HRQ_Sync_Exception__c !='Y']);
		}
		else
		{
			return Database.getQueryLocator([Select Id, Name,HRQ_ID__c,RecordTypeId,Description,Industry,IndustryType__c,HRQ_Industry_ID__c,HRQ_Area_ID__c,FirstAddress__c,Main_Business__c,HRQ_TotalAssets__c,HRQ_NetAssets__c,HRQ_OperatingIncome__c,HRQ_NetProfit__c,Content__c,HRQ_Uploader_ID__c,HRQ_UploadedOn__c,HRQ_OtherInfo__c,TotalAssets__c,NetAssets__c,OperatingIncome__c,NetProfit__c
											From Account 
											Where HRQ_ID__c != null And HRQ_Sync_Exception__c !='Y']);
		}
	}
	
	private Boolean isFieldInHistories(String Field, List<AccountHistory> histories)
	{
		if(histories == null)
		{
			return false;
		}
		for(AccountHistory h : histories)
		{
			if(h.Field.equalsIgnoreCase(Field))
			{
				return true;
			}
		}
		return false;
	}
	
	global override void convertData(List<SObject> scope)
	{
		this.projectsList = new List<HRQProject>();
		if(this.syncSetting.SFDC2HRQ_Update_By_History__c)
		{
			for(Account acc : (List<Account>)scope)
		  {
		  		HRQProject pro = new HRQProject();
			  	pro.Id = acc.HRQ_ID__c;
			   
			   	/*	反映射行业ID	
			    if(isFieldInHistories('HRQ_Industry_ID__c', acc.Histories))
			    {
				    pro.IndustryId =  acc.HRQ_Industry_ID__c;
			    }
			    */
			    /*	TODO	反映射地区ID
			    if(isFieldInHistories('HRQ_Area_ID__c', acc.Histories))
			    {
					pro.AreaId = acc.HRQ_Area_ID__c;
			    }
			    */
			    system.debug('************************%%%%按照历史更新@@@@@');
			    //反映射行业ID	by winter
			    if(isFieldInHistories('Industry',acc.Histories))
			    {
			    	if(acc.IndustryType__c == null)
			    	{
			    		pro.IndustryId = industry_1_0Maps.get(acc.Industry);
			    	}
			    	else
			    	{
			    		pro.IndustryId = industry_1_2Maps.get(acc.IndustryType__c);
			    	}
			    }	
			    if(isFieldInHistories('FirstAddress__c', acc.Histories))
			    {
					pro.Address = acc.FirstAddress__c;
			    }
			    if(isFieldInHistories('Main_Business__c', acc.Histories))
			    {
					pro.MainBusiness = acc.Main_Business__c;
			    }
			    if(isFieldInHistories('TotalAssets__c', acc.Histories))
			    {
			    	if(acc.TotalAssets__c != null)
	                {
		                pro.TotalAssets = acc.TotalAssets__c + '万元（RMB）';
	                }
	                else
	                {
	                	pro.TotalAssets = ' ';
	                }
			    }
			    if(isFieldInHistories('NetAssets__c', acc.Histories))
			    {
			    	if(acc.NetAssets__c != null)
			    	{
						pro.NetAssets = acc.NetAssets__c + '万元（RMB）';  
			    	}
			    	else
			    	{
			    		pro.NetAssets = ' ';
			    	}
			    }
			    if(isFieldInHistories('OperatingIncome__c', acc.Histories))
			    {
			    	if(acc.OperatingIncome__c != null)
			    	{
						pro.Businessincome = acc.OperatingIncome__c + '万元（RMB）';
			    	}
			    	else
			    	{
			    		pro.Businessincome = ' ';
			    	}
			    }
			    if(isFieldInHistories('NetProfit__c', acc.Histories))
			    {
			    	if(acc.NetProfit__c != null)
			    	{
						pro.NetProfit = acc.NetProfit__c + '万元（RMB）';
			    	}
			    	else
			    	{
			    		pro.NetProfit = ' ';
			    	}
			    }
			    if(isFieldInHistories('Content__c', acc.Histories))
			    {
					pro.Content = acc.Content__c;
			    }
			    if(isFieldInHistories('HRQ_OtherInfo__c', acc.Histories))
			    {
				    pro.OtherInfo = acc.HRQ_OtherInfo__c;
			    }
			    
			    /*	华尔圈发布人ID不应该手动更改
			    if(isFieldInHistories('HRQ_Uploader_ID__c', acc.Histories))
			    {
					pro.UploaderId = acc.HRQ_Uploader_ID__c; 
			    }
			    */
			    
			    /*	华尔圈加入时间属于不可更改的数据
			    if(isFieldInHistories('HRQ_UploadedOn__c', acc.Histories))
			    {
				    pro.UploadedOn = string.valueOf(acc.HRQ_UploadedOn__c);
			    }
			    */
			    /*	华尔圈加入时间:该项目加入到华尔圈的时间，属于华尔圈内部属性，与salesforce无关
				pro.UploadedOn = string.valueOf(DateTime.now());
				*/
			    projectsList.add(pro);
		   }
			
		}else
		{
		  system.debug('____*_*_____!');
		  for(Account acc : (List<Account>)scope)
			{	
				HRQProject pro = new HRQProject();
			    pro.Id = acc.HRQ_ID__c;
			    pro.Title = acc.Name;
			    //反映射行业ID by winter
			    if(acc.Industry != null)
			    {
			    	if(acc.IndustryType__c ==null)
			    	{
			    		pro.IndustryId = industry_1_0Maps.get(acc.Industry);
			    	}
			    	else
			    	{
			    		pro.IndustryId = industry_1_2Maps.get(acc.IndustryType__c);
			    	}
			    }
			    //pro.IndustryId =  acc.HRQ_Industry_ID__c;
				//pro.AreaId = acc.HRQ_Area_ID__c;
				pro.Address = acc.FirstAddress__c;
				pro.MainBusiness = acc.Main_Business__c;
				/*总资产*/
                if(acc.TotalAssets__c != null)
                {
	                pro.TotalAssets = acc.TotalAssets__c + '万元（RMB）';
                }
                else
                {
                	pro.TotalAssets = ' ';
                }
                /*净资产*/
                if(acc.NetAssets__c != null)
                {
	                pro.NetAssets = acc.NetAssets__c + '万元（RMB）';
                }
                else
                {
                    pro.NetAssets = ' ';
                }
                /*营业收入*/
                if(acc.OperatingIncome__c != null)
                {
	                pro.Businessincome = acc.OperatingIncome__c + '万元（RMB）';
                }
                else
                {
                    pro.Businessincome = ' ';
                }
                /*净利润*/
                if(acc.NetProfit__c != null)
                {
	                pro.NetProfit = acc.NetProfit__c + '万元（RMB）';
                }
                else
                {
                    pro.NetProfit = ' ';
                }
				pro.Content = acc.Content__c;
				//pro.UploaderId = acc.HRQ_Uploader_ID__c;
			    
			    //pro.UploadedOn = string.valueOf(acc.HRQ_UploadedOn__c);
				pro.OtherInfo = acc.HRQ_OtherInfo__c;
				pro.UploadedOn =string.valueOf(DateTime.now());
				projectsList.add(pro);
			}
		}
	}
	
	global override void pushDataToHRQ()
	{
		this.hrqService.UpdateProject(projectsList);
	}
	
	global override void handleNext()
	{
		if(this.IsChain)
        {
            SFDC2HRQProjectUpdateSyncBatch nextBatch = new SFDC2HRQProjectUpdateSyncBatch();
             nextBatch.currentTime = this.currentTime;
            nextBatch.IsChain = this.IsChain;
            nextBatch.BatchSize = this.BatchSize; 
            database.executeBatch(nextBatch, nextBatch.BatchSize);
        }
	}
}