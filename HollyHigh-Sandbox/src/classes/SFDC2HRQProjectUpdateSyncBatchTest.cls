/*
作者：winter
创建时间：2014-07-27
功能：SFDC2HRQProjectUpdateSyncBatch测试类
*/
@isTest
private class SFDC2HRQProjectUpdateSyncBatchTest {

    static testMethod void myUnitTest() {
       	StructureMapData.StructureMap();
       	//RecordType rt = [select ID,Name from RecordType where Name = '买/借壳方'];
       	List<Opportunity> oppList = new List<Opportunity>();
		Opportunity opp = new Opportunity();			
		opp.HRQ_ID__c = '123';
		opp.Name = '测试project';
		//opp.RecordType.DeveloperName = '';
		opp.Description = 'LastName';
		opp.Credibility__c = 'hight';
		opp.HRQ_Uploader_ID__c = '2222';
		opp.HRQ_UploadedOn__c = Datetime.now();
		opp.HRQ_Pairids__c = '11111';
		opp.HRQ_Sync_Exception__c = 'N';
		opp.HRQ_LastModifiedOn__c = DateTime.now();
		opplist.add(opp);
		
		insert opplist;
       	
        system.test.starttest();
        system.test.setMock(HttpCalloutMock.class,new HRQRequestMock());
		SFDC2HRQProjectUpdateSyncBatch batch = new SFDC2HRQProjectUpdateSyncBatch();
		batch.IsChain = false; 
		Database.Querylocator dq = batch.start(null);
		string query = dq.getQuery();
		SObject[] res = Database.Query(query);
		batch.execute(null,(List<Opportunity>)res);
		batch.finish(null);
		//database.executebatch(batch);
		system.test.stoptest();
		
		//代码覆盖率为54% 2014.8.11 23:06
    }
}