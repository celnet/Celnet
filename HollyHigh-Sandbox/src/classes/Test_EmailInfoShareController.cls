/**
 * Author: Steven
 * Date: 2014-4-9
 * Description: 测试EmailInfoShareController
 */
@isTest
private class Test_EmailInfoShareController {

    static testMethod void myUnitTest() {
    	// 准备数据
    	EmailInfo__c eic = new EmailInfo__c();
    	EmailInfo__Share eis = new EmailInfo__Share();
    	Id currentUserId = UserInfo.getUserId();
    	
    	User u = [Select Id From User Where IsActive = true And UserType = 'Standard' limit 1];
    	System.runAs(u){
    		
	    	eic.Name = 'Test1';
	    	insert eic;
	    	
	    	eis.AccessLevel = 'Read';
	    	eis.ParentId = eic.Id;
	    	eis.UserOrGroupId = currentuserId;
	    	insert eis;
    	}
    	
    	// 执行测试
    	
   		ApexPages.currentPage().getParameters().put('id',eic.Id);
    	EmailInfoShareController eisc = new EmailInfoShareController(new ApexPages.Standardcontroller(eic));
    	eisc.upList[0].isCheck = true;
    	eisc.addShare();
    	eisc.deleteShare();
    	eisc.cancel();
    	
    	// 验证结果
        
    }
}