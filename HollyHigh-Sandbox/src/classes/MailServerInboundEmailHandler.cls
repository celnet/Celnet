/*
* Author: Tommy Liu
* Created on: 2014-1-7
* Function:
1. 将系统中所有用户的来往邮件，记录到Salesforce系统中
   1） 通过匹配Email地址将和联系人的来往邮件记录到联系人以及自己的活动列表中
   2） 记录的范围包括：发件人、收件人、抄送列表、密送列表
2. 在系统中联系人有两个Email地址，要分别进行匹配
3. 如果Email地址匹配不成功，将Email放到 无法解析的项目中。如果放到无法解析的
2014-03-13 Sunny 邮件body目前只处理了plainTextBody，如果是HTML的邮件还需要处理htmlbody
2014-03-25 Sunny 邮件接收到之后，不是创建Task而是创建到一个新的对象中"电子邮件"(Email__c)
*/
global class MailServerInboundEmailHandler implements Messaging.InboundEmailHandler
{
    public Boolean isInternalEmailAddress(String emailAddress)
    {
        Boolean blnIsInternal = false;
        if(emailAddress == null || emailAddress == '')
        {
            return blnIsInternal;
        }
        Set<String> internalEmailDomains = Internal_Email_Domain__c.getAll().keySet();
        system.debug(internalEmailDomains);
        for(String domain : internalEmailDomains)
        {system.debug('eeeeee:'+domain);
            system.debug(emailAddress.endsWithIgnoreCase(domain));
            if(emailAddress.endsWithIgnoreCase(domain))
            {
                blnIsInternal = true;
            }
        }
        system.debug(blnIsInternal+' fachuyouxiang:'+emailAddress);
        return blnIsInternal;
    }
    
    private List<Contact> matchContacts(String[] emailAddresses)
    {
        List<Contact> contacts = new List<Contact>();
        if(emailAddresses == null || emailAddresses.size() == 0)
        {
            return contacts;
        }
        contacts = [Select Id, Name, AccountId From Contact Where Email In: emailAddresses Or Email2__c In: emailAddresses];
        return contacts;
    }
    
    private List<User> matchUsers(String[] emailAddresses)
    {
        List<User> users = new List<User>();
        if(emailAddresses == null || emailAddresses.size() == 0)
        {
            return users;
        }
        users = [Select Id, Name From User Where Email In: emailAddresses];
        return users;
    }
    
    private Unresolved_Email__c convertToUnresolvedEmail(Messaging.InboundEmail email, String direction, String reason)
    {
        Unresolved_Email__c unEmail = new Unresolved_Email__c();
        unEmail.Name = email.Subject;
        unEmail.CC_Addresses__c = toString(email.ccAddresses);
        unEmail.From_Address__c = email.fromAddress;
        unEmail.In_Reply_To__c = email.inReplyTo;
        //2014-03-13 Sunny修改邮件主体处理
        if(email.plainTextBody != null && email.plainTextBody != '')
            unEmail.Body__c = email.plainTextBody;
        else if(email.htmlBody != null && email.htmlBody != '')
            unEmail.Email_HtmlBody__c = email.htmlBody;
        unEmail.To_Addresses__c = toString(email.toAddresses);
        unEmail.Message_Id__c = email.messageId;
        unEmail.Direction__c = direction;
        unEmail.Unsolved_Reason__c = reason;
        return unEmail;
    }
    /*
    private Task[] convertToTasks(Messaging.InboundEmail email, Contact[] contacts, ID ownerId, String direction)
    {
        List<Task> taskList = new List<Task>();
        if(contacts == null || contacts.size() == 0)
        {
            Task iTask = new Task();
            iTask.OwnerId = ownerId;
            //iTask.WhoId = cont.Id; 
            iTask.Subject ='电子邮件：' + email.subject;
            iTask.ActivityDate = date.today();
            //2014-03-13 Sunny修改邮件主体处理
            if(email.plainTextBody != null && email.plainTextBody != '')
                iTask.Description = email.plainTextBody;
            else if(email.htmlBody != null && email.htmlBody != '')
                iTask.Description = email.htmlBody;
            iTask.Status = '已完成';
            iTask.Email_CC_Addresses__c = toString(email.ccAddresses);
            iTask.Email_From_Address__c = email.fromAddress;
            iTask.Email_In_Reply_To__c = email.inReplyTo;
            iTask.Email_To_Addresses__c = toString(email.toAddresses);
            iTask.Email_Message_Id__c = email.messageId;
            iTask.Email_Direction__c = direction;
            taskList.add(iTask);
            return taskList;
        }
        for(Contact cont: contacts)
        {
            Task iTask = new Task();
            iTask.OwnerId = ownerId;
            iTask.WhoId = cont.Id; 
            iTask.Subject ='电子邮件：' + email.subject;
            iTask.ActivityDate = date.today();
            //2014-03-13 Sunny修改邮件主体处理
            if(email.plainTextBody != null && email.plainTextBody != '')
                iTask.Description = email.plainTextBody;
            else if(email.htmlBody != null && email.htmlBody != '')
                iTask.Description = email.htmlBody;
            iTask.Status = '已完成';
            iTask.Email_CC_Addresses__c = toString(email.ccAddresses);
            iTask.Email_From_Address__c = email.fromAddress;
            iTask.Email_In_Reply_To__c = email.inReplyTo;
            iTask.Email_To_Addresses__c = toString(email.toAddresses);
            iTask.Email_Message_Id__c = email.messageId;
            iTask.Email_Direction__c = direction;
            taskList.add(iTask);
        }
        
        return taskList;
        
    }
    */
    private EmailInfo__c[] convertToEmailInfos(Messaging.InboundEmail email, Contact[] contacts, ID ownerId, String direction)
    {
        List<EmailInfo__c> emailInfoList = new List<EmailInfo__c>();
        if(contacts == null || contacts.size() == 0)
        {
            EmailInfo__c eInfo = new EmailInfo__c();
            eInfo.OwnerId = ownerId;
            //iTask.WhoId = cont.Id; 
            eInfo.Name ='电子邮件：' + email.subject;
            eInfo.Date__c = date.today();
            //2014-03-13 Sunny修改邮件主体处理
            if(email.plainTextBody != null && email.plainTextBody != '')
                eInfo.Email_TextBody__c = email.plainTextBody;
            else if(email.htmlBody != null && email.htmlBody != '')
                eInfo.Email_Body__c = email.htmlBody;
            eInfo.Email_CC_Addresses__c = toString(email.ccAddresses);
            eInfo.Email_From_Address__c = email.fromAddress;
            eInfo.Email_In_Reply_To__c = email.inReplyTo;
            eInfo.Email_To_Addresses__c = toString(email.toAddresses);
            eInfo.Email_Message_Id__c = email.messageId;
            eInfo.Email_Direction__c = direction;
            emailInfoList.add(eInfo);
            return emailInfoList;
        }
        for(Contact cont: contacts)
        {
            EmailInfo__c eInfo = new EmailInfo__c();
            eInfo.OwnerId = ownerId;
            eInfo.Contact__c = cont.Id; 
            eInfo.Account__c = cont.AccountId;
            eInfo.Name ='电子邮件：' + email.subject;
            eInfo.Date__c = date.today();
            //2014-03-13 Sunny修改邮件主体处理
            if(email.plainTextBody != null && email.plainTextBody != '')
                eInfo.Email_TextBody__c = email.plainTextBody;
            else if(email.htmlBody != null && email.htmlBody != '')
                eInfo.Email_Body__c = email.htmlBody;
            eInfo.Email_CC_Addresses__c = toString(email.ccAddresses);
            eInfo.Email_From_Address__c = email.fromAddress;
            eInfo.Email_In_Reply_To__c = email.inReplyTo;
            eInfo.Email_To_Addresses__c = toString(email.toAddresses);
            eInfo.Email_Message_Id__c = email.messageId;
            eInfo.Email_Direction__c = direction;
            emailInfoList.add(eInfo);
        }
        
        return emailInfoList;
        
    }
    
    private String toString(String[] addresses)
    {
        if(addresses == null)
        {
            return null;
        }
        String str = '';
        for(String address : addresses)
        {
            str += address + '; ';
        }
        return str;
    }
    /*
    private void CreateTask(Task[] taskList, Messaging.InboundEmail.BinaryAttachment[] emailAttList)
    {
        if(taskList == null)
        {
            return;
        }
        insert taskList;
        if(emailAttList == null)
        {
            return;
        }
        List<Attachment> attList = new List<Attachment>();
        for(Task task : taskList)
        {
            for(Messaging.InboundEmail.BinaryAttachment emailAtt : emailAttList)
            {
                Attachment att = new Attachment();
                att.body = emailAtt.body;
                att.ParentId = task.Id;
                att.Name = '邮件附件：'+ emailAtt.fileName;
                if(task.OwnerId != null)
                {
                    att.OwnerId = task.OwnerId;
                }
                else
                {
                    att.OwnerId = UserInfo.getUserId();
                }
                attList.add(att);
            }
        }
        insert attList;
    } 
    */
    private void CreateEmailInfo(EmailInfo__c[] emailInfoList, Messaging.InboundEmail.BinaryAttachment[] emailAttList)
    {
        if(emailInfoList == null)
        {
            return;
        }
        insert emailInfoList;
        if(emailAttList == null)
        {
            return;
        }
        List<Attachment> attList = new List<Attachment>();
        for(EmailInfo__c eInfo : emailInfoList)
        {
            for(Messaging.InboundEmail.BinaryAttachment emailAtt : emailAttList)
            {
                Attachment att = new Attachment();
                att.body = emailAtt.body;
                att.ParentId = eInfo.Id;
                att.Name = '邮件附件：'+ emailAtt.fileName;
                if(eInfo.OwnerId != null)
                {
                    att.OwnerId = eInfo.OwnerId;
                }
                else
                {
                    att.OwnerId = UserInfo.getUserId();
                }
                attList.add(att);
            }
        }
        insert attList;
    } 
    private void CreateUnEmail(Unresolved_Email__c[] unMailList, Messaging.InboundEmail.BinaryAttachment[] emailAttList)
    {
        if(unMailList == null)
        {
            return;
        }
        insert unMailList;
        if(emailAttList == null)
        {
            return;
        }
        List<Attachment> attList = new List<Attachment>();
        for(Unresolved_Email__c unMail : unMailList)
        {
            for(Messaging.InboundEmail.BinaryAttachment emailAtt : emailAttList)
            {
                Attachment att = new Attachment();
                att.body = emailAtt.body;
                att.ParentId = unMail.Id;
                att.Name = '邮件附件：'+ emailAtt.fileName;
                if(unMail.OwnerId != null)
                {
                    att.OwnerId = unMail.OwnerId;
                }
                else
                {
                    att.OwnerId = UserInfo.getUserId();
                }
                attList.add(att);
            }
        }
        insert attList;
    } 
    
    //需要考虑内部发给内部的情况
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
    {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        result.success = true;
        system.debug('Email info:'+email);
        
        //List<Task> taskList = new List<Task>();
        List<EmailInfo__c> emailList = new List<EmailInfo__c>();
        List<Unresolved_Email__c> unEmailList = new List<Unresolved_Email__c>();
        if(this.isInternalEmailAddress(email.FromAddress))//处理发送的情况
        {
            User[] formUsers = this.matchUsers(new String[] {email.FromAddress});
            Contact[] toContacts = this.matchContacts(email.toAddresses);
            toContacts.addAll(this.matchContacts(email.ccAddresses));
            
            if(formUsers == null || formUsers.size() == 0)//未解决：找不到用户
            {
                unEmailList.add( convertToUnresolvedEmail(email, '发送', '发件人地址匹配不到用户'));
            }
            else //创建Task，并关联到用户，
            {
                //跟据toContacts可能会产生多个Task，如果toContacts为空在产生一个不关联到联系人的Task
                //taskList.addAll(convertToTasks(email, toContacts, formUsers[0].Id, '发送'));
                emailList.addAll(convertToEmailInfos(email, toContacts, formUsers[0].Id, '发送'));
            }
        }
        else //处理接收的情况
        {
            Contact[] formContacts = this.matchContacts(new String[] {email.FromAddress});
            User[] toUsers = this.matchUsers(email.toAddresses);
            toUsers.addAll(this.matchUsers(email.ccAddresses));
            if(toUsers == null || toUsers.size() == 0)//未解决：找不到用户
            {
                unEmailList.add(convertToUnresolvedEmail(email, '接收', '收件人地址列表或抄送地址列表中的任意地址匹配不到用户'));
            }
            else //创建Task，并关联到用户
            {
                for(User u : toUsers)
                {
                    //taskList.addAll(convertToTasks(email, formContacts, u.Id, '接收'));//跟据tolist可能会产生多个Task
                    emailList.addAll(convertToEmailInfos(email, formContacts, u.Id, '接收'));
                }
            }
        }
        //this.CreateTask(taskList, email.binaryAttachments);
        this.CreateEmailInfo(emailList, email.binaryAttachments);
        this.CreateUnEmail(unEmailList, email.binaryAttachments);
        return result;
    }
}