@isTest
public  class HRQRequestMock implements HttpCalloutMock 
{
	protected string IndusJsonstr;
	protected string AreaJsonstr;
	protected string MemberJsonstr;
	protected string ProjectJsonstr;
	protected string RelatedContactJsonstr;
	protected string SaveResultJsonstr;
	
	public HRQRequestMock() 
	{
		this.IndusJsonstr = '[{"id":"2","name":"行业类型","parentid":"0"},{"id":"3","name":"Industry","parentid":"0"},{"id":"23","name":"医药","parentid":"2"},{"id":"48","name":"食品","parentid":"2"},{"id":"109","name":"其他行业","parentid":"2"}]';
		this.AreaJsonstr = '[{"id":"0001","name":"中国","parentareaid":"0000"},{"id":"100","name":" 海外地区","parentareaid":"0001"},{"id":"10001","name":"亚洲","parentareaid":"100"},{"id":"10002","name":"欧洲","parentareaid":"100"},{"id":"10003","name":"北美洲","parentareaid":"100"}]';
		this.MemberJsonstr = '[{"id":"3","name":"admin","gender":"0","regip":"","regdate":"1970-01-01 07:00:00","email":"","mobile":"0","telephone":""},{"id":"7","name":"党卫东","gender":"0","regip":"192.168.0.10","regdate":"2011-07-12 17:03:14","email":"user@user.cn","mobile":"","telephone":"018601179611"},{"id":"8","name":"","gender":"0","regip":"192.168.0.10","regdate":"2011-07-12 17:08:40","email":"user@user.cn","mobile":"","telephone":""}]';
		this.ProjectJsonstr = '[{"id":"466","title":"法国城堡酒店出售","wanttype":"欲出售","wantdo":"10家高级酒店出售：","credibility":"","attachmentid":"","industryid":"27","areaid":"10002","address":"","mainbusiness":"运营权及地产所有权同时出售，可整体收购或单独收购 均由城堡转型而成，房间总数超过400间，可改成私人公寓 如感兴趣，请直接联系其卖方财务顾问","totalassets":"","netassets":"","businessincome":"","netprofit":"","content":"","uploaderid":"148","uploadedon":"2012-07-01 08:17:30","otherinfo":"","pairids":"28,72,147,263,466,476,542,544,736,737,773,802,964,965,992,1079,1087,1354,1441,1442,1443,1444,1445,1451,1540,1577,1595,1678","relatedcontacts":[{"id":"499","projectid":"466","name":"拉麦特 弗朗索瓦","jobtitle":"其他","mobile":"0033615500593","email":"framette@deomenos.com","uploadedon":"2012-07-01 08:17:30","uploaderid":"148"}],"uploader":{"id":"148","name":"李杨","gender":"0","regip":"114.112.195.142","regdate":"2012-02-20 18:06:36","email":"huaerquan@hollyhigh.cn","mobile":"15821585404","telephone":""}},{"id":"467","title":"华然资本","wanttype":"欲购买","wantdo":"汽车电机","credibility":"","attachmentid":"","industryid":"43","areaid":"31","address":"","mainbusiness":"1.重点关注：制动系统（ABS电机，EBA电机）；转向及悬挂系统（EPS电机）泵类系统（电动燃油泵电机；汽车水泵-水泵电机）2.收购规模：收购对象规模1到15个亿。如有特别优质企业，可以酌情放宽。3、对象：希望收购对象，盈利情况较好，成长性较好。4、性质：中外合资或外商独资在中国有工厂的为佳，其他类型企业亦可考虑。","totalassets":"","netassets":"","businessincome":"","netprofit":"","content":"","uploaderid":"148","uploadedon":"2012-07-01 08:48:17","otherinfo":"","pairids":"14,21,91,100,148,177,391,399,467,508,512,536,580,708,951,1028,1089,1122,1391,1415,1421,1474,1505,1583,1625,1804,1894","relatedcontacts":[{"id":"500","projectid":"467","name":"赵辰","jobtitle":"其他","mobile":"021-35322166","email":"","uploadedon":"2012-07-01 08:48:17","uploaderid":"148"}],"uploader":{"id":"148","name":"李杨","gender":"0","regip":"114.112.195.142","regdate":"2012-02-20 18:06:36","email":"huaerquan@hollyhigh.cn","mobile":"15821585404","telephone":""}}]';
		this.RelatedContactJsonstr = '[{"id":"1","projectid":"1","name":"仇思念","jobtitle":"总经理","mobile":"13810728227","email":"sinian","uploadedon":"2011-12-01 22:48:13","uploaderid":"31"},{"id":"5","projectid":"3","name":"刘俊","jobtitle":"其他","mobile":"13621810511","email":"","uploadedon":"2011-12-12 10:48:49","uploaderid":"49"},{"id":"6","projectid":"4","name":"boka","jobtitle":"董事长","mobile":"1868888888","email":"donggq@boka,cn","uploadedon":"2011-12-12 10:53:04","uploaderid":"3"}]';
		this.SaveResultJsonstr = '[{"id":"1","issuccess":"1","error":"!!!!!!!!!!1"}]';
		
	}
	
	public HTTPResponse respond(HTTPRequest req) 
	{
       	HttpResponse resp = new HttpResponse();
       	resp.setHeader('Content-Type', 'application/json');
		string endpoint = req.getEndpoint();
		if(endpoint.contains('GetIndustryList'))
		{
			resp.setBody(IndusJsonstr);
		}
		if(endpoint.contains('GetRelatedContactList'))
		{
			resp.setBody(RelatedContactJsonstr);
		}
		if(endpoint.contains('GetAreaList'))
		{
			resp.setBody(AreaJsonstr);
		}
		if(endpoint.contains('GetMemberList'))
		{
			resp.setBody(MemberJsonstr);
		}
		if(endpoint.contains('GetProjectList'))
		{
			resp.setBody(ProjectJsonstr);
		}if(endpoint.contains('UpdateRelatedContact'))
		{
			resp.setBody(SaveResultJsonstr);
		}if(endpoint.contains('CreateRelatedContact'))
		{
			resp.setBody(SaveResultJsonstr);
		}if(endpoint.contains('UpdateProject'))
		{
			resp.setBody(SaveResultJsonstr);
		}if(endpoint.contains('CreateProject'))
		{
			resp.setBody(SaveResultJsonstr);
		}
		system.debug(req.getEndpoint()  +'               this  is  get a endPoint ');
		system.debug(resp.getBody()  +'                    this  is  get a body ');
		return resp;
    }
}