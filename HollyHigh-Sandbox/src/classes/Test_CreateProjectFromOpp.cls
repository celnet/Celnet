/*
*功能：测试类-----CreateProjectFromOpp
*作者：Alisa
*时间：2013/12/19
*/
@isTest(SeeAllData=true)
private class Test_CreateProjectFromOpp {

    static testMethod void myUnitTest() 
    {
        list<RecordType> listOppRT = new list<RecordType>();
        listOppRT = [select Id,SobjectType from RecordType where SobjectType = 'Opportunity'];
        list<Opportunity> listOpp = new list<Opportunity>();
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = '业务机会Test1';
        opp1.StageName = '协议签署';
        opp1.RecordTypeId = listOppRT[0].Id;
        opp1.CloseDate = date.today().addMonths(1);
        listOpp.add(opp1);
        Opportunity opp2 = new Opportunity();
        opp2.Name = '业务机会Test2';
        opp2.StageName = '初次接触';
        opp2.RecordTypeId = listOppRT[1].Id;
        opp2.CloseDate = date.today().addMonths(1);
        listOpp.add(opp2);
        Opportunity opp3 = new Opportunity();
        opp3.Name = '业务机会Test3';
        opp3.StageName = '初次接触';
        opp3.RecordTypeId = listOppRT[1].Id;
        opp3.CloseDate = date.today().addMonths(1);
        listOpp.add(opp3);
        Opportunity opp4 = new Opportunity();
        opp4.Name = '业务机会Test4';
        opp4.StageName = '初次接触';
        opp4.CloseDate = date.today().addMonths(1);
        listOpp.add(opp4);
        insert listOpp;
        
        ProjectHollyhigh__c pro = new ProjectHollyhigh__c();
        pro.Name = '项目test';
        pro.Opportunity__c = opp3.Id;
        insert pro; 
        
        system.Test.startTest();
        CreateProjectFromOpp.createProject(opp1.Id);
        CreateProjectFromOpp.createProject(opp2.Id);
        CreateProjectFromOpp.createProject(opp3.Id);
        CreateProjectFromOpp.createProject(opp4.Id);
        system.Test.stopTest();
    }
}