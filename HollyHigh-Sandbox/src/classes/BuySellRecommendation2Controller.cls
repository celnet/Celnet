/*
 * Author: Steven
 * Date: 2014-1-14
 * Description: 做单项目中根据客户类型以及所选择的的行业类型来推荐相应的客户
*/
public class BuySellRecommendation2Controller 
{
	// 加载页面时传递的值：做单项目的ID,做单项目,客户
    private ID projectId;
    public ProjectHollyhigh__c projectc{get;set;}
    public Account acc{get;set;}
    
    private Set<ID> existTargetAccountIds;
    private Map<ID,RecordType> projReIdMap;
    private Map<String, RecordType> accReDevMap;
    private Map<String, RecordType> recommReDevMap;
    private ID recommRecordTypeId;
    public boolean isSelected{get;set;}
    
    public Boolean isRenderTab{get;set;}
    public Boolean isRenderPageSection{get;set;}
    public String query{get;set;}
    
    public List<AccountWrapper> aw {get;set;}
    
    public class AccountWrapper 
    {
        public Boolean isChecked{get;set;}
        public Account account{get;set;}
        public string description{get;set;}
        public AccountWrapper()
        {
            isChecked = false;
            account = new Account();
        }
        
        public AccountWrapper(Account a)
        {
            isChecked = false;
            account = a;
            if(account.Description != null)
			{
				description=account.Description;
				
				if(description.length() > 10)
				{
					description = description.substring(0,10)+'...';
				}
			}
			else
			{
				description = '';
			}
        }
    }
    
    
    //设置客户类型默认值
    private void setRecordTypeValue()
    {
        this.projReIdMap = new Map<ID, RecordType>();
        this.accReDevMap = new Map<String, RecordType>();
        this.recommReDevMap = new Map<String, RecordType>();
        
        for(RecordType reType: [Select ID, Name, DeveloperName From RecordType Where SObjectType = 'ProjectHollyhigh__c'])
        {
            this.projReIdMap.put(reType.ID, reType);
        }
        for(RecordType reType: [Select ID, Name, DeveloperName From RecordType Where SObjectType = 'Account'])
        {
            this.accReDevMap.put(reType.DeveloperName, reType);
        }
        for(RecordType reType: [Select ID, Name, DeveloperName From RecordType Where SObjectType = 'ResourceRecommend__c'])
        {
            this.recommReDevMap.put(reType.DeveloperName, reType);
        }
        
        //设置筛选条件客户的默认记录类型,设置将要生成的推荐的记录类型
        if(projReIdMap.containsKey(this.projectc.RecordTypeId))
        {
            RecordType currentProjRe = projReIdMap.get(this.projectc.RecordTypeId);
            if(currentProjRe.DeveloperName == 'Seller')
            {
                this.acc.RecordTypeId = this.accReDevMap.get('Buyer').Id;
                this.recommRecordTypeId = this.recommReDevMap.get('Buyer').Id;
            }
            else if (currentProjRe.DeveloperName == 'Buyer')
            {
                this.acc.RecordTypeId = this.accReDevMap.get('Seller').Id;
                this.recommRecordTypeId = this.recommReDevMap.get('Seller').Id;
            }
        }
    }
    
    public BuySellRecommendation2Controller(ApexPages.standardController controller)
    {
        this.isRenderTab = false;
        this.isRenderPageSection = false;
        // accList = new List<Account>();
        // aw = new List<AccountWrapper>();
        this.existTargetAccountIds = new Set<ID>();
        this.projectId = ApexPages.currentPage().getParameters().get('id');
        this.projectc = [Select ID, 
                        Name, 
                        RecordTypeId,
                        Account__r.Name, 
                        Account__r.OwnerId,
                        Account__r.ID, 
                        Account__r.Type, 
                        Account__r.IndustryType__c, 
                        Account__r.industry, 
                        Account__r.RecordTypeId,
                        Account__r.RecordType.ID,
                        Account__r.RecordType.Name,
                        (Select ID, 
                            Target_Account__c
                        From relatedproject__r)
                    From ProjectHollyhigh__c 
                    Where Id =: this.projectId];
        if(this.projectc.Account__r == null)
        {
            this.projectc.Account__r = new Account();
        }
        this.acc = projectc.Account__r;
        this.setRecordTypeValue();
       
        if(this.projectc.relatedproject__r != null)
     	{
     		for(ResourceRecommend__c rec : this.projectc.relatedproject__r)
     		{
     			if(rec.Target_Account__c != null)
     			{
     				this.existTargetAccountIds.add(rec.Target_Account__c);
     			}
     		}
     	}
       
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, '请合理设置筛选条件后点击 “搜索”按钮开始检索目标客户');         
        ApexPages.addMessage(msg);
    }
    
    
    public PageReference Search()
    {
        if(this.acc.industry==null)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '请选择行业信息');         
            ApexPages.addMessage(msg);
            return null;        
        }
        
        if(this.acc.IndustryType__c != null)
        {
	        conset = new ApexPages.Standardsetcontroller(Database.getQueryLocator([Select Id, Name, OwnerId, Owner.Name, Owner.Email,Description
	        																		From Account 
	        																		Where RecordTypeId =: this.acc.RecordTypeId
	        																		And IndustryType__c =: this.acc.IndustryType__c
	        																		And Industry =: this.acc.industry
	        																		And Id Not IN: this.existTargetAccountIds]));
	        conset.setPageSize(20);
        }
        
        if(this.acc.IndustryType__c == null)
        {
        	conset = new ApexPages.Standardsetcontroller(Database.getQueryLocator([Select Id, Name, OwnerId, Owner.Name, Owner.Email,Description
	        																		From Account 
	        																		Where RecordTypeId =: this.acc.RecordTypeId
	        																		And Industry =: this.acc.industry
	        																		And Id Not IN: this.existTargetAccountIds]));
	        conset.setPageSize(20);
        }
        
        result = conset.getRecords().size();
        
        aw = new List<AccountWrapper>();
        for(Account a : (List<Account>) conset.getRecords())
        {
        	aw.add(new AccountWrapper(a));
        }
        
        if(conset.getRecords().size() <= 0)
        {
            this.isRenderTab = false;
            this.isRenderPageSection = false;
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, '按照您设置的条件，没有找到任何可推荐的客户，请合理设置条件后重新搜索');         
            ApexPages.addMessage(msg);
        }
        else
        {
            this.isRenderTab = true;
            this.isRenderPageSection = true;
        }
        
        return null;
    }
    
    public PageReference Sure()
    {
        List<ResourceRecommend__c> newRecommendList = new List<ResourceRecommend__c>();
        
        List<Task> taskList = new List<Task>();
        Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
        String[] toAddresses = new List<String>();

        List<Attachment> attachmentList = new List<Attachment>();
        List<Attachment> newAttachmentList = new List<Attachment>();
        
        Set<Id> userIds = new Set<Id>();
        
        if(aw != null)
        {
	        for(AccountWrapper awr : aw)
	        {
	            if(awr.isChecked)
	            {
	                ResourceRecommend__c recommend = new ResourceRecommend__c();
	                recommend.Account__c = this.acc.Id;
	                recommend.Project__c = this.projectc.Id;
	                recommend.Target_Account__c = awr.account.Id;
	                recommend.RecordTypeId = this.recommRecordTypeId;
	                newRecommendList.add(recommend);
	                
	                userIds.add(awr.account.OwnerId);
	                
	                User u1 = [Select Email,Name From User Where Id =: awr.account.OwnerId];
	                toAddresses.add(u1.Email);
	                
					attachmentList = [Select 
										Id,OwnerId,Name,IsPrivate,Body,Description,ContentType 
									  From 
									  	Attachment 
									  Where 
									  	IsDeleted = false And ParentId =: this.projectId];
	                
	                Task t1 = new Task();
	                t1.OwnerId = awr.account.OwnerId;
	                t1.WhatId = awr.account.Id;
	                t1.Description = '现有做单项目"' + this.projectc.Name + '"需要您的关注。';
	                t1.ActivityDate = Date.today();
	                t1.ActivityDateTime__c = Datetime.now();
	                t1.Subject = '请关注“' + this.projectc.Name + '” 项目';
	                t1.Receiver__c = u1.Name;
	                t1.Recommender__c = UserInfo.getName();
	                t1.IsReminderSet = true;
					t1.ReminderDateTime = Datetime.newInstance(Date.today().year(), Date.today().month(), Date.today().day(), 8, 0, 0);
	                
	                taskList.add(t1);
	            }
	        }
        }
        
        // 共享项目给收件人用户
        shareProjectToUsers(userIds);
        
        if(newRecommendList.size() == 0)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '请先勾选客户');         
            ApexPages.addMessage(msg);
            return null;        
        }
        
        
        String content = '现有做单项目"' + this.projectc.Name + '"需要您的关注。';
        content += '以下是项目链接: ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + this.projectId;
        mail.setPlainTextBody(content);
        mail.setSubject('做单项目关注');
        mail.setToAddresses(toAddresses);
        Messaging.sendEmail(new Messaging.Singleemailmessage[]{mail});
        insert taskList;
        insert newRecommendList;
       
        if(attachmentList.size() != 0)
        {
        	for(Attachment a : attachmentList)
        	{
        		for(Task t : taskList)
        		{
        			Attachment a1 = new Attachment();
        			a1.ParentId = t.Id;
        			a1.Body = a.Body;
        			a1.ContentType = a.ContentType;
        			a1.Description = a.Description;
        			a1.IsPrivate = a.IsPrivate;
        			a1.Name = a.Name;
        			a1.OwnerId = t.OwnerId;
        			newAttachmentList.add(a1);
        		}
        	}
        	insert newAttachmentList;
        }
        
        
        PageReference redirectPag = new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/' + this.projectId);
        redirectPag.setRedirect(true);
        return redirectPag;
    }
    
    public PageReference Cancel()
    {
        PageReference redirectPag = new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/' + this.projectId);
        redirectPag.setRedirect(true);
        return redirectPag; 
    }
    
    // 共享Opp的读取权限给指定的用户
	public void shareProjectToUsers(Set<Id> userIds){
		List<ProjectHollyhigh__Share> pss = new List<ProjectHollyhigh__Share>();
		
		for(Id uid : userIds){
			if(!checkUserProjectReadAccess(uid)){
				ProjectHollyhigh__Share ps = new ProjectHollyhigh__Share();
				ps.ParentId = this.projectId;
				ps.AccessLevel = 'Read';
				ps.UserOrGroupId = uid;
				pss.add(ps);
			}
		}
		
		if(pss.size() > 0)
		insert pss;
	}
	
	// 检查收件人用户是否对推荐的拿单项目Opp有读取权限
	public boolean checkUserProjectReadAccess(Id userId){
		List<UserRecordAccess> uraList = [Select RecordId, HasReadAccess
										  	From UserRecordAccess 
										  	Where UserId =: userId 
										  	and RecordId =: this.projectId];
		
		if(uraList.size() > 0)
			return uraList[0].HasReadAccess;
		else
			return false;
	}

    public ApexPages.Standardsetcontroller conset{get;set;}
    
    public integer result{get;set;}

    public boolean hasPrevious
    {
        get
        {
            return conset.getHasPrevious();
        }
        set;
    }
    
    public boolean hasNext
    {
        get
        {
            return conset.getHasNext();
        }
        set;
    }
    
    public Integer pageNumber
    {
        get
        {
            return conset.getPageNumber();
        }
        set;
    }

    public void first()
    {
    	conset.first();
    	result = conset.getRecords().size();
    	aw = new List<AccountWrapper>();
        for(Account a : (List<Account>) conset.getRecords())
        {
        	aw.add(new AccountWrapper(a));
        }
    }
    
    public void last()
    {
    	conset.last();
    	result = conset.getRecords().size();
    	aw = new List<AccountWrapper>();
        for(Account a : (List<Account>) conset.getRecords())
        {
        	aw.add(new AccountWrapper(a));
        }
    }
    
    public void previous()
    {
    	conset.previous();
    	result = conset.getRecords().size();
    	aw = new List<AccountWrapper>();
        for(Account a : (List<Account>) conset.getRecords())
        {
        	aw.add(new AccountWrapper(a));
        }
    }
    
    public void next()
    {
    	conset.next();
    	result = conset.getRecords().size();
    	aw = new List<AccountWrapper>();
        for(Account a : (List<Account>) conset.getRecords())
        {
        	aw.add(new AccountWrapper(a));
        }
    }
    
    public Integer categoryNumber
    {
        get
        {
            return conset.getResultSize();
        }
        set;
    }
    
    public Integer totalPageNumber
    {
    	get
    	{
    		
    		if(Math.mod(categoryNumber,20) == 0)
    		{
    			return (categoryNumber / 20);
    		}
    		else
    		{
    			return ((categoryNumber / 20) + 1);
    		}
    	}
    }
}