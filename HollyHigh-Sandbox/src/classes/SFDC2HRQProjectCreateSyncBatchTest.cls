@isTest
private class SFDC2HRQProjectCreateSyncBatchTest {

    static testMethod void myUnitTest() {
   		StructureMapData.StructureMap();
   		Account acc = new Account();
		acc.Name = '123';
		acc.HRQ_ID__c = null;
		acc.Industry = '行业';
		acc.IndustryType__c = '行业';
		acc.HRQ_Industry_ID__c = '行业';
		acc.FirstAddress__c = '北京市';
		acc.Main_Business__c = '1123';
		acc.HRQ_TotalAssets__c = '124';
		acc.TotalAssets__c = 1212;
		acc.HRQ_NetAssets__c = '4242';
		acc.NetAssets__c = 123123;
		acc.HRQ_OperatingIncome__c = '4244';
		acc.OperatingIncome__c = 123213;
		acc.HRQ_NetProfit__c = '4242';
		acc.NetProfit__c = 234234;
		acc.Content__c = '行业';
		acc.HRQ_OtherInfo__c = '行业';
		acc.HRQ_Sync_Exception__c = 'N';
		insert acc;
		
		
   		Account acc1 = new Account();
		acc1.Name = '123x';
		acc1.HRQ_ID__c = null;
		acc1.Industry = '行业';
		acc1.IndustryType__c = '行业';
		acc1.HRQ_Industry_ID__c = '行业';
		acc1.FirstAddress__c = '北京市';
		acc1.HRQ_OtherInfo__c = '行业';
		acc1.HRQ_Sync_Exception__c = 'N';

		insert acc1;
		
		Opportunity opp = new Opportunity();
		opp.AccountId = acc.Id;
		opp.Name = '行业';
		//RecordType rec = [Select Id,DeveloperName From RecordType];
		//opp.RecordType.DeveloperName = rec.DeveloperName;
		opp.HRQ_ID__c =null;
		opp.Description = '行业';
		opp.Credibility__c = '行业';
		opp.HRQ_Uploader_ID__c = '行业';
		opp.HRQ_UploadedOn__c = Datetime.now();
		opp.HRQ_Pairids__c = '行业';
		opp.HRQ_LastModifiedOn__c = Datetime.now();
		opp.StageName = 'stageName';
		opp.CloseDate = Date.today();
		opp.HRQ_Sync_Exception__c = 'N';
		
		//opp.Owner.HRQ_Member_ID__c = user.HRQ_Member_ID__c;
		opp.HRQ_Pairids__c = '行业';
		insert opp;
		
		
		Opportunity opp1 = new Opportunity();
		opp1.AccountId = acc1.Id;
		opp1.Name = '行业';
		//RecordType rec = [Select Id,DeveloperName From RecordType];
		//opp.RecordType.DeveloperName = rec.DeveloperName;
		opp1.HRQ_ID__c =null;
		opp1.Description = '行业';
		opp1.Credibility__c = '行业';
		opp1.HRQ_Uploader_ID__c = '行业';
		opp1.HRQ_UploadedOn__c = Datetime.now();
		opp1.HRQ_Pairids__c = '行业';
		opp1.HRQ_LastModifiedOn__c = Datetime.now();
		opp1.StageName = 'stageName';
		opp1.CloseDate = Date.today();
		opp1.HRQ_Sync_Exception__c = 'N';
		
		//opp.Owner.HRQ_Member_ID__c = user.HRQ_Member_ID__c;
		opp1.HRQ_Pairids__c = '行业';
		insert opp1;
		
		Contact con =  new Contact();
		//con.AccountId = opp.Id;
		con.HRQ_ID__c = null;	
		//con.Owner.HRQ_Member_ID__c = user.HRQ_Member_ID__c;	
		con.LastName = '123';		
		con.Email = '12@qq.com';	
		con.Title = '1233';	
		con.MobilePhone = '1233';
		con.Phone = '1233';//手机2	2014-10-10
		con.HRQ_Sync_Exception__c = 'N'; 
		insert con;
		
		List<OpportunityContactRole> oppConList = new List<OpportunityContactRole>();
		OpportunityContactRole oppCon = new OpportunityContactRole();
		oppCon.ContactId = con.Id;
		oppCon.OpportunityId = opp.Id;
		insert oppCon;
		
			
		SFDC2HRQProjectCreateSyncBatch his = new SFDC2HRQProjectCreateSyncBatch();
		his.IsChain = false;
		Database.QueryLocator dq = his.start(null);
        system.test.starttest();
		system.test.setMock(HttpCalloutMock.class,new HRQRequestMock());
		String query = dq.getQuery();
		SObject[] res = Database.Query(query);
		his.execute(null,(List<Opportunity>)res);
    	//database.executebatch(his);
		system.test.stoptest();
		his.finish(null);
		
		//代码覆盖率为90% 2014.8.11 22:44
		
		
		//代码覆盖率为82% 2014.9.3 winter	如果四个资产为空，传过去的值应为'0万元'
    }
}