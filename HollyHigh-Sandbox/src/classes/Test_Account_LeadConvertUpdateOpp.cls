/*
*功能：test Account_LeadCovertUpdateOppRecordType
*作者：Crazy
*时间：2014-1-23
*/
@isTest
private class Test_Account_LeadConvertUpdateOpp {

    static testMethod void myUnitTest() {
        RecordType rt = new RecordType();
        Account acc = new Account();
        acc.Name = 'cc';
        acc.AccRecordType__c = '买/借壳方';
        insert acc;
        Opportunity opp =  new Opportunity();
        opp.AccountId = acc.Id;
        opp.Name = 'aa';
        opp.StageName = 'aa';
        opp.CloseDate = date.today();
        insert opp;
        System.assertEquals(true, opp.RecordTypeId == rt.Id);
        
        Account acc1 = new Account();
        acc1.Name = 'cc1';
        acc1.AccRecordType__c = '合作伙伴';
        insert acc1;
        Opportunity opp1 =  new Opportunity();
        opp1.AccountId = acc1.Id;
        opp1.Name = 'aa';
        opp1.StageName = 'aa';
        opp1.CloseDate = date.today();
        insert opp1;
        System.assertEquals(true, opp1.RecordTypeId == rt.Id);
    }
}