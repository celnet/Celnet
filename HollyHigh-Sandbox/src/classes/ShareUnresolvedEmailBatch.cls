/**
 * Author:Sunny Sun
 * Des:对未处理的邮件进行共享，共享原则，用收件人、抄送人的邮箱与系统用户进行匹配，匹配上之后进行共享
 * 
 **/
global class ShareUnresolvedEmailBatch implements Database.Batchable<sObject>
{
	public Date dataDate ;
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		if(dataDate==null)
		{
			dataDate = date.today();
		}
		system.debug(dataDate);
		return Database.getQueryLocator([Select Id,CC_Addresses__c,From_Address__c,To_Addresses__c From Unresolved_Email__c Where CreatedDate >=: dataDate]);
	}
	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		Map<String , ID> map_emailRecord = new Map<String , ID>();
		Map<String , User> map_emailUser = new Map<String , User>(); 
		List<Unresolved_Email__Share> list_share = new List<Unresolved_Email__Share>();
		for(sObject sObj : scope)
		{
			Unresolved_Email__c ue = (Unresolved_Email__c)sObj;
			if(ue.From_Address__c != null)
			{
				map_emailRecord.put(ue.From_Address__c , ue.Id);
			}
			if(ue.CC_Addresses__c != null && ue.CC_Addresses__c != '')
			{
				for(String sEmail : splitEmail(ue.CC_Addresses__c))
				{
					map_emailRecord.put(sEmail,ue.Id);
				}
			}
			if(ue.To_Addresses__c != null && ue.To_Addresses__c != '')
			{
				for(String sEmail : splitEmail(ue.To_Addresses__c))
				{
					map_emailRecord.put(sEmail,ue.Id);
				}
			}
		}
		map_emailUser = this.queryUser(map_emailRecord.keySet());
		for(String sMail : map_emailRecord.keySet())
		{
			if(map_emailUser.containsKey(sMail))
			{
				Unresolved_Email__Share ueShare = new Unresolved_Email__Share();
				ueShare.ParentId = map_emailRecord.get(sMail);
				ueShare.AccessLevel = 'Edit';
				ueShare.UserOrGroupId = map_emailUser.get(sMail).Id;
				list_share.add(ueShare);
			}
		}
		if(list_share.size() > 0)
		{
			insert list_share;
		}
	}
	private List<String> splitEmail(String sEmail)
	{
		List<String> list_email = new List<String>() ;
		if(sEmail.contains(';')){
			list_email.addAll(sEmail.split(';'));
		}else{
			list_email.add(sEmail);
		}
		return list_email;
	}
	private Map<String , User> queryUser(Set<String> set_email)
	{
		Map<String , User> map_emailUser = new Map<String , User>();
		for(User u:[Select Id,Email From User Where Email in: set_email And IsActive = true])
		{
			map_emailUser.put(u.Email,u);
		}
		return map_emailUser;
	}
	global void finish(Database.BatchableContext BC)
	{
		
	}
}