/*
*Author: Tommy Liu
*Created On: 2014-1-21
*Function: 测试类
*/
@isTest
private class Test_CostMonthYear 
{
    static testMethod void testManyCreateCostCase() 
    {
    	CostReimbursement__c cost1 = new CostReimbursement__c(Name = 'Cost1');
    	CostReimbursement__c cost2 = new CostReimbursement__c(Name = 'Cost2');
    	
    	insert new CostReimbursement__c[] {cost1, cost2};
    	
	    Date thisDay = Date.today();
		Integer year = thisDay.year();
		Integer month = thisDay.month();
		for(CostReimbursement__c cost: [Select Id, Name, Year__c, reimbursementMonth__c From CostReimbursement__c Where Id =: cost1.Id Or Id =: cost2.Id])
		{
			system.assertEquals(year + '', cost.Year__c);
			system.assertEquals(month + '月', cost.reimbursementMonth__c);
		}
    }
}