/**
	作者:winter
	时间：2014-8-29
 	功能：HRQMember_StageName（trigger）测试类
 	覆盖率：100%
 */
@isTest
private class HRQMember_StageNameTest {

    static testMethod void myUnitTest() 
    {
    	Account a1 = new Account();
    	a1.Name = 'test';
    	a1.Industry = 'test';
    	insert a1;
    	
    	Opportunity o1 = new Opportunity();
    	o1.Name = 'test';
        o1.HRQ_ID__c = '1';
        o1.AccountId = a1.Id;
        o1.StageName = 'test';
        o1.CloseDate = Date.today();
        insert o1;
    }
}