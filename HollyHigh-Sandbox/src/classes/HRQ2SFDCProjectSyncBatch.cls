/*
作者：Tommy Liu
电子油箱：tommyliu@cideatech.com
创建时间：2014-07-08 
功能：同步华尔圈的项目到SFDC
同步方向：华尔圈到SFDC
*/
global class HRQ2SFDCProjectSyncBatch extends HRQ2SFDCDataSyncBatch
{   
    protected Integer pageCursor;//此游标需要在每次从华尔圈获取数据后将游标设置为返回结果的最大的ID
    protected final Integer pageRows;
    private List<HRQProject> projectsList;
    private List<Opportunity> oppsList;//待保存的项目
    private List<Contact> contactsList;//待保存的联系人
    private List<Account> accountsList;//待保存的客户
    private List<HRQ_Member__c> HRQMembersList;//待保存的member
    
    private Map<string,HRQ_Member__c> memberMap;
    
    private list<OpportunityContactRole> oppConRoleList;//待保存的联系人角色

    private Map<string,HRQ_Industry_Mapping__c> industryMaps;// 映射表行业
    private Map<string,string> areaMaps;//待保存的区域
    private Map<string,boolean> OppConRoleMaps;//联系人角色 用于判断联系人角色是否重复
    private list<OpportunityContactRole> OppConList; //存放联系人的角色
    
    private Map<String,ID> mapUser;
    private Map<String,String> HRQWantAccMap;
    private Map<String,String> HRQWantOppMap;
    private Map<String,ID> HRQAccMap;
    private Map<String,ID> HRQOppMap;
    private Integer max = 0;
    
    private static final  String Account_Default_RecordType_Developer_Name = 'WallQ';
    private static final  String Opp_Default_RecordType_Developer_Name = 'Other';
        
    global HRQ2SFDCProjectSyncBatch()
    {
        this(0, 100);
    }
    global HRQ2SFDCProjectSyncBatch(Integer pageCursor, Integer pageRows)
    {
        super();
        this.pageCursor = pageCursor;
        this.pageRows = pageRows;
		//联系人角色
		this.OppConRoleMaps = new Map<string,boolean>();
		
		this.OppConList = [Select Id,Opportunity.HRQ_ID__c,Contact.HRQ_ID__c From OpportunityContactRole];
		system.debug('&&&&&&&&&&&&&&&&&');
		if(OppConList != null)
		{
			for(OpportunityContactRole  oc :  [Select Opportunity.HRQ_ID__c,Contact.HRQ_ID__c From OpportunityContactRole])
			{
				string str = oc.Opportunity.HRQ_ID__c + oc.Contact.HRQ_ID__c;
				
				OppConRoleMaps.put(str,false);
			}
		}
        //地区
        this.areaMaps = new Map<string,string>();
        for(HRQ_Area_Mapping__c ham : [Select Id,HRQ_ID__c,SFDC_Name__c From HRQ_Area_Mapping__c])
        {
        			areaMaps.put(ham.HRQ_ID__c,ham.SFDC_Name__c);
        }
        //行业
        this.industryMaps = new map<String, HRQ_Industry_Mapping__c>();
        for(HRQ_Industry_Mapping__c him : [Select Id,
	        									HRQ_ID__c,
	        									SFDC_Industry_Name__c,
	        									SFDC_Sub_Industry_Name__c 
        									From HRQ_Industry_Mapping__c 
        									where HRQ_ID__c != null])
        {
        	this.industryMaps.put(him.HRQ_ID__c, him);
        }

        this.memberMap = new Map<string,HRQ_Member__c>();
        //构造的User Map
        this.mapUser = new Map<String,ID>();
        for(User u : [select Id,HRQ_Member_ID__c from User Where HRQ_Member_ID__c != null And IsActive = true])
        {
            mapUser.put(u.HRQ_Member_ID__c,u.Id);
        }
        
        //从HRQ项目类型映射表中找到客户的HRQType 和 Developer_Name
        HRQWantAccMap = new Map<String,String>();
        for(HRQ_WantType_Mapping__c hwm : [Select Id,
                                                SFDC_Object_Name__c,
                                                SFDC_RecordType_Developer_Name__c,
                                                HRQ_WantType__c 
                                            From HRQ_WantType_Mapping__c
                                            Where SFDC_Object_Name__c = 'Account'])
        {
            HRQWantAccMap.put(hwm.HRQ_WantType__c, hwm.SFDC_RecordType_Developer_Name__c);
        }
        //从HRQ项目类型映射表表中找到业务机会的HRQType 和 Developer_Name
        HRQWantOppMap = new Map<String,String>();
        for(HRQ_WantType_Mapping__c hwm : [Select Id, 
                                                SFDC_Object_Name__c,
                                                SFDC_RecordType_Developer_Name__c,
                                                HRQ_WantType__c 
                                            From HRQ_WantType_Mapping__c
                                            Where SFDC_Object_Name__c = 'Opportunity'])
        {
            HRQWantOppMap.put(hwm.HRQ_WantType__c,hwm.SFDC_RecordType_Developer_Name__c);
        }
        
        //从RecordType表中找到客户的DeveloperName和 record ID
        HRQAccMap = new Map<string,ID>();
        for(RecordType rt : [Select SobjectType,Name,Id,DeveloperName,Description From RecordType   
                            Where SobjectType ='Account' And IsActive = true])
        {
            HRQAccMap.put(rt.DeveloperName,rt.Id);
        }
        //从RecordType表中找到业务机会的DeveloperName和 record ID
        HRQOppMap = new Map<string,ID>();
        for(RecordType rt : [Select SobjectType,Name,Id,DeveloperName,Description From RecordType   
                            where SobjectType ='Opportunity' And IsActive = true])
        {
            HRQOppMap.put(rt.DeveloperName,rt.Id);
        }
        
        this.oppsList = new List<Opportunity>();
        this.contactsList = new List<Contact>();
        this.accountsList = new List<Account>();
        this.HRQMembersList = new List<HRQ_Member__c>();
        this.oppConRoleList = new list<OpportunityContactRole>();
    }
    
    global override List<Integer> start(Database.BatchableContext BC)
    {
        List<Integer> rl = super.start(BC);
        if(this.pageCursor == 0)//第一页需要存储开始时间
        {
            this.syncSetting.HRQ2SFDC_Last_Synced_StartOn__c = Datetime.now();
            update this.syncSetting;
        }
        return rl;
    }
   
    global override void getDataFromHRQ()
    {
    	try
    	{
        	this.projectsList = this.hrqService.GetProjectList(this.pageCursor, this.pageRows);
    	}
    	catch (CalloutException exc)
    	{
    		HRQ_Sync_Log__c log = this.toLog(exc);
    		log.Will_Recover__c = true;
    		this.Log(exc);
    		
    		//Callout 失败重试
    		Integer thisRecover = this.Recover + 1;
    		if(thisRecover <= this.RecoverTime)//如果重试次数没有超过允许的值，则重试
    		{
	    		HRQ2SFDCProjectSyncBatch recoverBatch = new HRQ2SFDCProjectSyncBatch(this.pageCursor, this.pageRows);
	        	recoverBatch.IsChain = this.IsChain;
	        	recoverBatch.Recover = thisRecover;
	        	database.executeBatch(recoverBatch);
	        	this.isStopNext = true;
    		}
    		else
    		{
    			throw exc;
    		}
    		
    		return;
    	}
        for(HRQProject project: projectsList)
        {
            if(project.Id != null)
            {
                if(max < Integer.valueOf(project.Id))
                {
                    max = Integer.valueOf(project.Id);
                }
            }
        }
        this.pageCursor = max;
        system.debug(this.projectsList.size() + '________________size dyn!!!');
    }
    
    global override void convertData()
    {
    	system.debug('this is  in !!');
        for(HRQProject pro : this.projectsList)
        {
            //Member从HRQ更新到SF
            //pro.Uploader = new HRQMember();
            HRQMember hrqMember = pro.Uploader;
            
            if(hrqMember != null)
            {
            	if(!memberMap.containsKey(hrqMember.Id))   
                {
	                HRQ_Member__c sfMember = new HRQ_Member__c();
	                
	                sfMember.HRQ_ID__c = hrqMember.Id;
	                if(hrqMember.Name == null || hrqMember.Name.trim() == '')
	                {
	                	sfMember.Name = '未知';
	                }
	                else
	                {
	                	sfMember.Name = hrqMember.Name;
	                }
	                //2014.8.11 将成员名字段删除，改用标准字段Name
	                if(hrqMember.Gender == 1)
	                {
	                    sfMember.HRQ_Gender__c = '男';
	                }
	                else
	                {
	                    sfMember.HRQ_Gender__c = '女';
	                }
	                sfMember.HRQ_Reg_IP__c = hrqMember.Regip;
	                sfMember.HRQ_Reg_On__c = DateTime.valueOf(hrqMember.Regdate);
	                
	                sfMember.HRQ_EmailText__c = hrqMember.Email;
	                string MemberEmailRes = hrqMember.Email;
					if(MemberEmailRes != null && MemberEmailRes.trim() != '')
					{
						boolean isEmail = Pattern.matches('[a-zA-Z0-9\\.\\!\\#\\$\\%\\&\\*\\/\\=\\?\\^\\_\\+\\-\\`\\{\\|\\}\\~\'._%+-]+@[a-zA-Z0-9\\-.-]+\\.[a-zA-Z]+', MemberEmailRes);
						if(isEmail)
						{
							sfMember.HRQ_Email__c = MemberEmailRes;
						}
					}
					
	                //将华尔圈Member的电话信息完整保存到“电话文本”
	           		sfMember.HRQ_MobilePhoneText__c = hrqMember.Mobile;
	            	//如果“电话文本”长度不大于40，存入正式电话信息中
	            	if(hrqMember.Mobile.length() <= 40 && hrqMember.Mobile != null && hrqMember.Mobile.trim() !='')
		        	{
			    		sfMember.HRQ_MobilePhone__c = hrqMember.Mobile;
		        	}
	                sfMember.HRQ_LastModifiedOn__c = DateTime.now();
                    memberMap.put(hrqMember.Id,sfMember);
                }
                
                //hrqMembersList.add(sfMember);
            }
            
            //客户从HRQ更新到SF
            Account acc = new Account();
            acc.HRQ_ID__c = pro.Id;
            //string str = pro.Id ;//华尔圈项目的Title可以重复的，sf的Account的name不能重复 ， 为了区别不同的项目 ，  在sf的Acc的Name 上加上hrq的Id。
            
            if(pro.Title == null || pro.Title.trim() == '')
            {
            	acc.Name = '未知';
            }
            else
            {
            	acc.Name = pro.Title;
            }
            system.debug(pro.Title + '_______________________________dyn');
            //插入recordId到客户里
            ID defAccRtId = HRQAccMap.get(Account_Default_RecordType_Developer_Name);
            string Acc_SFDCDevName = HRQWantAccMap.get(pro.Wanttype);
            if(Acc_SFDCDevName != null)
            {
                ID Acc_recordId = HRQAccMap.get(Acc_SFDCDevName);
                if(Acc_recordId != null)
                {
                    acc.RecordTypeId = Acc_recordId;
                }
                else
                {
                    acc.RecordTypeId = defAccRtId;
                }
            }
            else
            {
                acc.RecordTypeId = defAccRtId;
            }
            
            system.debug(pro.IndustryId + '  ____________hrq的行业Id！！！');
            acc.Description = pro.Wantdo;
            //##########处理行业映射 by winter
            acc.HRQ_Industry_ID__c = pro.IndustryId;
            HRQ_Industry_Mapping__c industry = this.industryMaps.get(pro.IndustryId);
            if(industry != null)
            {
            	if(industry.SFDC_Industry_Name__c != null)
            	{
            		acc.Industry = industry.SFDC_Industry_Name__c;
            	}
            	if(industry.SFDC_Sub_Industry_Name__c != null)
            	{
            		acc.Industry = industry.SFDC_Sub_Industry_Name__c;
            	}
            }
            if(this.areaMaps.containsKey(pro.AreaId))
            {
            	acc.HRQ_Area__c = this.areaMaps.get(pro.AreaId);
            }
            acc.HRQ_Area_ID__c = pro.AreaId;
            acc.FirstAddress__c = pro.Address;
            acc.Main_Business__c = pro.MainBusiness;
            acc.HRQ_TotalAssets__c = pro.TotalAssets;
            acc.HRQ_NetAssets__c = pro.NetAssets;
            acc.HRQ_OperatingIncome__c = pro.Businessincome;
            acc.HRQ_NetProfit__c = pro.NetProfit;
            acc.Content__c = pro.Content;
			if(hrqMember != null)
			{
				acc.HRQ_Uploader_ID__c = pro.Uploader.Id;
			}
            
            
            if(pro.Uploader != null && pro.Uploader.Id != null)
            {
                acc.HRQ_Uploader__r = new HRQ_Member__c();
                acc.HRQ_Uploader__r.HRQ_ID__c = pro.Uploader.Id;
	            ID sfAccUploadId = mapUser.get(pro.Uploader.Id);
	            if(sfAccUploadId != null)
	            {
	                acc.OwnerId  = sfAccUploadId;
	            }
            }
            //如果HRQ时间传入为空，将其时间处理为当前时间 	by winter peter
            if(pro.UploadedOn == null)
            {
            	acc.HRQ_UploadedOn__c = DateTime.now();
            }
            {
	            acc.HRQ_UploadedOn__c = DateTime.valueOf(pro.UploadedOn);
            }
            acc.HRQ_OtherInfo__c = pro.OtherInfo;
            acc.HRQ_LastModifiedOn__c = DateTime.now();
            accountsList.add(acc);
            
            //业务机会从HRQ更新到SF
            Opportunity opp = new Opportunity();
            opp.HRQ_ID__c = pro.Id;
            opp.Account = new Account();
            opp.Account.HRQ_ID__c = pro.Id;
            
            if(pro.Title!=null && pro.Title.trim()!='')
            {
	            opp.Name = pro.Title;
            }else
            {
            	opp.Name = '未知';
            }
            //插入recordId到业务机会里
            ID defOppRtId = HRQOppMap.get(Opp_Default_RecordType_Developer_Name);
            string Opp_SFDCDevName = HRQWantOppMap.get(pro.Wanttype);
            if(Opp_SFDCDevName != null)
            {
                ID Opp_recordId = HRQOppMap.get(Opp_SFDCDevName);
                if(Opp_recordId != null)
                {
                    opp.RecordTypeId = Opp_recordId;
                }else
                {
                    opp.RecordTypeId = defOppRtId;
                }
            }else
            {
                opp.RecordTypeId = defOppRtId;
            }
            opp.Description = pro.Wantdo;
            opp.Credibility__c = pro.Credibility;
           if(hrqMember != null)
           {
           	 opp.HRQ_Uploader_ID__c = pro.Uploader.Id; 
           }
           
            
            if(pro.Uploader != null && pro.Uploader.Id != null)
            {
                opp.HRQ_Uploader__r = new HRQ_Member__c();
                opp.HRQ_Uploader__r.HRQ_ID__c = pro.Uploader.Id;
                
           	    ID sfOppUploadId = mapUser.get(pro.Uploader.Id);
	            if(sfOppUploadId != null)
	            {
	                opp.OwnerId = sfOppUploadId; 
	            }
            }
            
           	//如果HRQ时间传入为空，将其时间处理为当前时间 	by winter peter
            if(pro.UploadedOn == null)
            {
            	opp.HRQ_UploadedOn__c = DateTime.now();
            }
            {
	            opp.HRQ_UploadedOn__c = DateTime.valueOf(pro.UploadedOn);
            }
            opp.HRQ_Pairids__c = pro.Pairids;
            opp.HRQ_LastModifiedOn__c = DateTime.now();
            oppsList.add(opp);
            
            
            //联系人
            List<HRQRelatedContact> relcontactsList =  pro.RelatedContacts;
            if(relcontactsList != null)
            {
                for(HRQRelatedContact relCon : relcontactsList)
                {
                    Contact con = new Contact();
                    con.HRQ_ID__c = relcon.Id;
                    con.HRQ_Project_ID__c = relcon.ProjectId;
                    //判断联系角色是否重复
                    string oppConStr = pro.Id+relCon.Id;
                    system.debug(oppConStr + '!!!!!!!!!!!!!!');
                    system.debug(OppConRoleMaps.get(oppConStr) + 'zhe shi Map');
                    if(OppConRoleMaps.get(oppConStr) == null || !(OppConRoleMaps.containsKey(oppConStr)))
                    {
	                    OpportunityContactRole oppconRole = new OpportunityContactRole();
	                    oppconRole.Opportunity = new Opportunity();
	                    oppconRole.Opportunity.HRQ_ID__c = pro.id;

	                    oppconRole.Contact = new Contact();
	                    oppconRole.Contact.HRQ_ID__c = relcon.Id;
	                    oppConRoleList.add(oppconRole);
                    }
                    if(relcon.Name == null || relcon.Name.trim() == '')
                    {
                        con.LastName = '未知';
                    }else
                    {
                        con.LastName = relcon.Name;
                        //传回撒了salesforce时把firstName清空	8.15 11:07 by winter
                    	con.FirstName = '';
                    }
                    system.debug(con.LastName + '      +++++++++');
                    con.Title = relcon.JobTitle;
                    con.MobilePhoneText__c = relcon.Mobile;
                    if(relcon.Mobile.length() <= 40 && relcon.Mobile != null && relcon.Mobile.trim() !='')
                    {
	                    //con.MobilePhone = relcon.Mobile;
	                    con.Phone = relcon.Mobile;//2014-10-10	修改：phone（电话（手机2））和mobilephone调换（手机）	winter
                    }
                    
                    //********************************
                    con.EmailText__c = relcon.Email;
                    
		            string ConEmailRes = relcon.Email;
					if(ConEmailRes != null && ConEmailRes.trim() != '')
					{
						boolean isEmail = Pattern.matches('[a-zA-Z0-9\\.\\!\\#\\$\\%\\&\\*\\/\\=\\?\\^\\_\\+\\-\\`\\{\\|\\}\\~\'._%+-]+@[a-zA-Z0-9\\-.-]+\\.[a-zA-Z]+', ConEmailRes);
						if(isEmail)
						{
							con.Email = ConEmailRes;
						}
					}
                    
                    // 需要党总将接口补充，再执行~
                    if(relcon.Uploader != null && relcon.Uploader.Id != null)
                    {
                        con.HRQ_Uploader__r = new HRQ_Member__c();
                        con.HRQ_Uploader__r.HRQ_ID__c = relcon.Uploader.Id;
	                    con.HRQ_Uploader_ID__c = relcon.Uploader.Id;
                        
                        HRQ_Member__c member = new HRQ_Member__c();
                        //######################################
                        if(relCon.Uploader.Name == null || relCon.Uploader.Name.trim() == '')
                        {
                        	member.Name = '未知';
                        }
                        else
                        {
                        	member.Name = relCon.Uploader.Name;
                        }
                        //2014.8.11 将成员名字段删除，改用标准字段Name
						if(relcon.Uploader.Gender == 1)
                		{
                    		member.HRQ_Gender__c = '男';
                		}
		                else
		                {
		                    member.HRQ_Gender__c = '女';
		                }
						member.HRQ_Reg_IP__c = relcon.Uploader.Regip;
                		member.HRQ_Reg_On__c = DateTime.valueOf(relcon.Uploader.Regdate);
						
						//将HRQ的邮件信息先保存到邮件文本中，在trigger里用正则表达式判断格式化再保存到HRQ_Email__c中
		                member.HRQ_EmailText__c = relcon.Uploader.Email;
		                
		                //********************************
		                string ConMemberEmailRes = relcon.Uploader.Email;
						if(ConMemberEmailRes != null && ConMemberEmailRes.trim() != '')
						{
							boolean isEmail = Pattern.matches('[a-zA-Z0-9\\.\\!\\#\\$\\%\\&\\*\\/\\=\\?\\^\\_\\+\\-\\`\\{\\|\\}\\~\'._%+-]+@[a-zA-Z0-9\\-.-]+\\.[a-zA-Z]+', ConMemberEmailRes);
							if(isEmail)
							{
								member.HRQ_Email__c = ConMemberEmailRes;
							}
						}
		                //将华尔圈Member的电话信息完整保存到“电话文本”
		           		member.HRQ_MobilePhoneText__c = relcon.Uploader.Mobile;
		            	//如果“电话文本”长度不大于40，存入正式电话信息中
		            	if(relcon.Uploader.Mobile.length() <= 40 && relcon.Uploader.Mobile != null && relcon.Uploader.Mobile.trim() !='')
			        	{
				    		member.HRQ_MobilePhone__c = relcon.Uploader.Mobile;
			        	}
						member.HRQ_ID__c = relcon.Uploader.Id;
						if(!memberMap.containsKey(relcon.Uploader.Id))   
		                {
		                    member.HRQ_ID__c = relcon.Uploader.Id;
		                    this.memberMap.put(relcon.Uploader.Id,member);
		                }	                    
	                    
	                    //########################
	                    ID conId = mapUser.get(relcon.Uploader.Id);
	                    if(conId != null)
	                    {
	                        con.OwnerId = conId; 
	                    }
                    }
                    
                    //如果HRQ时间传入为空，将其时间处理为当前时间 	by winter peter
		            if(pro.UploadedOn == null)
		            {
		            	con.HRQ_UploadedOn__c = DateTime.now();
		            }
		            {
			            con.HRQ_UploadedOn__c = DateTime.valueOf(relcon.UploadedOn);
		            }
                    
                    //con.HRQ_UploadedOn__c = DateTime.valueOf(relcon.UploadedOn);
                    con.HRQ_LastModifiedOn__c = DateTime.now();
                    
                    contactsList.add(con);
                    
                    
                }
           }
            
        }
    }
    
    private HRQ_Sync_Log__c[] toLogs(sObject[] records, Database.UpsertResult[] res)
    {
    	List<HRQ_Sync_Log__c> logs = new List<HRQ_Sync_Log__c>();
    	if(records == null || res == null)
    	{
    		return logs;
    	}
        for(Integer i=0; i < res.size(); i ++)
        {
        	Database.UpsertResult re = res[i];
        	Sobject record = records[i];
        	if(!re.isSuccess())
        	{
        		HRQ_Sync_Log__c log = this.toLog(re);
        		object hrqId = record.get('HRQ_ID__c');
        		if(hrqId != null)
        		{
        			log.HRQ_ID__c = (String)hrqId;
        		}
        		//日志里是HRQ_Name__c，account里应该是Name 18:03 by winter
        		object hrqName = record.get('Name');
        		if(hrqName != null)
        		{
        			log.HRQ_Name__c = (String)hrqName;
        		}
        		object sfId = record.Id;
        		if(sfId != null)
        		{
        			log.SFDC_ID__c = (String)sfId;
        		}
        		Schema.SObjectType objType = record.getSObjectType();
        		log.Object__c = objType.getDescribe().getName();
        		log.Sub_Type__c = 'HRQ2SFDC Upsert';
        		log.Com__c = 'HRQ2SFDCProjectSyncBatch';
        		logs.add(log);
        	}
        }
        return logs;
    }
    
    global override void saveData()
    {
        system.debug('************$$$$$$$$$$$$$');
        List<HRQ_Member__c> memberList = this.memberMap.values();
        if(memberList != null)
        {
	        Database.UpsertResult[] memberRes = database.upsert(memberList, HRQ_Member__c.Fields.HRQ_ID__c, false);
	        this.Logs(this.toLogs(memberList, memberRes));
        }
        
        Database.UpsertResult[] accRes = database.upsert(this.accountsList, Account.Fields.HRQ_ID__c, false);
        this.Logs(this.toLogs(this.accountsList, accRes));
        
        Database.UpsertResult[] oppRes = database.upsert(this.oppsList, Opportunity.Fields.HRQ_ID__c, false);
        this.Logs(this.toLogs(this.oppsList, oppRes));
        
        Database.UpsertResult[] contRes = database.upsert(this.contactsList, Contact.Fields.HRQ_ID__c, false); 
        this.Logs(this.toLogs(this.contactsList, contRes));
        
        database.insert(this.oppConRoleList, false);
    }
    
    global override void handleNext()
    {	
        if(projectsList.size() != 0)//如果本页不为空则取下一页，
        {
            HRQ2SFDCProjectSyncBatch nextBatch = new HRQ2SFDCProjectSyncBatch(this.pageCursor, this.pageRows);
            nextBatch.IsChain = this.IsChain;
            database.executeBatch(nextBatch);
        }
        else//页为空，则结束并记录结束时间
        {
            this.syncSetting.HRQ2SFDC_Last_Synced_EndOn__c = Datetime.now();
            update this.syncSetting;
        }
        
    }
}