/*
 * Author: Steven
 * Date: 2014-2-10
 * Description: 测试类
 */
@isTest
private class Test_ReceivablesStats 
{
	static testMethod void testReceivablesStats()
	{
		// 准备数据
		Collection__c c1 = new Collection__c();
		c1.ActualDate__c = Date.today();
		c1.ActualAmount__c = 100;
		
		// 执行方法
		insert c1;
		
		// 验证结果
		ProfitInfo__c[] p1 = new List<ProfitInfo__c>();
		p1 = [Select p.Year__c, p.ReimbursementMonthly__c, p.ReceivablesMonthly__c, p.Month__c, p.OwnerId 
			  From ProfitInfo__c p];
		Double rim = p1[0].ReimbursementMonthly__c;
		Double rcm = p1[0].ReceivablesMonthly__c;
		String y = p1[0].Year__c;
		String m = p1[0].Month__c;
		Id oid = p1[0].OwnerId;
		
		System.assertEquals(rcm, 100);
		System.assertEquals(rim, 0);
		System.assertEquals(y, String.valueOf(Date.today().year()));
		// System.assertEquals(m, '2月');
		
		c1.ActualDate__c = Date.today().addMonths(3);
		c1.ActualAmount__c = null;
		
		update c1;
	}
	
	static testMethod void testReceivablesStats2()
	{
		Collection__c c2 = new Collection__c();
		
		insert c2;
		
		c2.ActualDate__c = Date.today();
		c2.ActualAmount__c = 20;
		
		update c2;
	}
}