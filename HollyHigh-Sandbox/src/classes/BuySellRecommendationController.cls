/*
   Author:Crazy
 * Date:2014-1-14
 * Description:拿单项目中根据客户类型以及所选择的的行业类型来推荐相应的客户
*/
public class BuySellRecommendationController 
{
	private ID oppId;
	public Opportunity Opp {get; set;}
	public Account acc{get;set;}
	private Map<ID, RecordType> projReIdMap;
	private Map<String, RecordType> accReDevMap;
	private Map<String, RecordType> recommReDevMap;
	private ID recommRecordTypeId;
	
	public Boolean isRenderTab {get; set;}
	public Boolean isRenderPageSection{get;set;}
	public String query{get;set;}

	public List<AccountPack> targetAccountList {get; set;}
	
	private Set<ID> existTargetAccountIds;
	
	
	public class AccountPack
	{
		public boolean isCheck{get;set;}
		public Account acc{get;set;}
		public string description{set;get;}
		public AccountPack()
		{
			isCheck = false;
			acc = new Account();
		}
		
		public AccountPack(Account account)
		{
			isCheck = false;
			acc = account;
			if(account.Description != null)
			{
				description=account.Description;
				
				if(description.length() > 10)
				{
					description = description.substring(0,10)+'...';
				}
			}
			else
			{
				description = '';
			}
		}
	}
	
	//设置客户类型默认值
	public void setRecordTypeValue()
	{
		this.projReIdMap = new Map<ID, RecordType>();
		this.accReDevMap = new Map<String, RecordType>();
		this.recommReDevMap = new Map<String, RecordType>();
        for(RecordType reType: [Select ID, Name, DeveloperName From RecordType Where SObjectType = 'Opportunity'])
        {
        	this.projReIdMap.put(reType.ID, reType);
        }
        for(RecordType reType: [Select ID, Name, DeveloperName From RecordType Where SObjectType = 'Account'])
        {
        	this.accReDevMap.put(reType.DeveloperName, reType);
        }
        for(RecordType reType: [Select ID, Name, DeveloperName From RecordType Where SObjectType = 'ResourceRecommend__c'])
        {
        	this.recommReDevMap.put(reType.DeveloperName, reType);
        }
        
        //设置筛选条件客户的默认记录类型,设置将要生成的推荐的记录类型
        if(projReIdMap.containsKey(this.Opp.RecordTypeId))
        {
        	RecordType currentProjRe = projReIdMap.get(this.Opp.RecordTypeId);
        	if(currentProjRe.DeveloperName == 'Seller')
        	{
        		this.acc.RecordTypeId = this.accReDevMap.get('Buyer').Id;
        		this.recommRecordTypeId = this.recommReDevMap.get('Buyer').Id;
        	}
        	else if (currentProjRe.DeveloperName == 'Buyer')
        	{
        		this.acc.RecordTypeId = this.accReDevMap.get('Seller').Id;
        		this.recommRecordTypeId = this.recommReDevMap.get('Seller').Id;
        	}
        }
	}
	
	public BuySellRecommendationController(ApexPages.standardController controller)
    {
    	this.isRenderTab = false;
    	this.isRenderPageSection = false;
    	
    	this.existTargetAccountIds = new Set<ID>();
      	this.oppId = ApexPages.currentPage().getParameters().get('id');
     	this.Opp = [Select ID, 
			     		Name,
			     		Type, 
			     		AccountId, 
			     		RecordTypeId,
			     		Account.OwnerId,
			     		Account.Name, 
			     		Account.ID, 
			     		Account.Type, 
			     		Account.IndustryType__c, 
			     		Account.industry, 
			     		Account.RecordTypeId,
			     		Account.RecordType.ID,
			     		Account.RecordType.Name,
			     		(Select ID, 
			     			Target_Account__c
			     		From ResourceRecommends__r)
			     	From Opportunity 
			     	Where Id =: this.oppId];
     	if(this.Opp.Account == null)
     	{
     		this.Opp.Account = new Account();
     	}
     	this.acc = Opp.Account;
     	this.setRecordTypeValue();
     	
     	if(this.Opp.ResourceRecommends__r != null)
     	{
     		for(ResourceRecommend__c rec : this.opp.ResourceRecommends__r)
     		{
     			if(rec.Target_Account__c != null)
     			{
     				this.existTargetAccountIds.add(rec.Target_Account__c);
     			}
     		}
     	}
     	
     	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, '请合理设置筛选条件后点击 “搜索”按钮开始检索目标客户');         
   		ApexPages.addMessage(msg);
    }
	
	public PageReference Search()
	{
		// this.targetAccountList.clear();
		if(this.acc.industry==null )
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '请选择行业信息');         
       		ApexPages.addMessage(msg);
       		return null;		
		}
		
        if(this.acc.IndustryType__c == null)
        {
        	conset = new ApexPages.Standardsetcontroller(Database.getQueryLocator([Select Id, Name, OwnerId, Owner.Name, Owner.Email,Description
	        																		From Account 
	        																		Where RecordTypeId =: this.acc.RecordTypeId
	        																		And Industry =: this.acc.industry
	        																		And Id Not IN: this.existTargetAccountIds]));
			conset.setPageSize(20);
        }
        
        if(this.acc.IndustryType__c != null)
        {
        	conset = new ApexPages.Standardsetcontroller(Database.getQueryLocator([Select Id, Name, OwnerId, Owner.Name, Owner.Email,Description 
	        																		From Account 
	        																		Where RecordTypeId =: this.acc.RecordTypeId
	        																		And IndustryType__c =: this.acc.IndustryType__c
	        																		And Industry =: this.acc.industry
	        																		And Id Not IN: this.existTargetAccountIds]));
			conset.setPageSize(20);
        }
		
		result = conset.getRecords().size();
		
		this.targetAccountList = new List<AccountPack>();
		for(Account a : (List<Account>) conset.getRecords())
		{
			this.targetAccountList.add(new AccountPack(a));
		}
		
		if(conset.getRecords().size() <= 0)
		{
			this.isRenderTab = false;
			this.isRenderPageSection = false;
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, '按照您设置的条件，没有找到任何可推荐的客户，请合理设置条件后重新搜索');         
   			ApexPages.addMessage(msg);
		}
		else
		{
			this.isRenderTab = true;
			this.isRenderPageSection = true;
		}
		return null;
	}
	
	public PageReference Sure()
	{
		List<ResourceRecommend__c> newRecommendList = new List<ResourceRecommend__c>();
		
		List<Task> taskList = new List<Task>();
		Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
		String[] toAddresses = new List<String>();
		
		
        List<Attachment> attachmentList = new List<Attachment>();
        List<Attachment> newAttachmentList = new List<Attachment>();
		
		Set<Id> userIds = new Set<Id>();
		
		if(this.targetAccountList != null)
		{
			for(AccountPack ap : this.targetAccountList)
			{
				if(ap.isCheck)
				{
					ResourceRecommend__c recommend = new ResourceRecommend__c();
					recommend.Account__c = this.acc.Id;
					recommend.Opportunity__c = this.Opp.Id;
					recommend.Target_Account__c = ap.acc.Id;
					recommend.RecordTypeId = this.recommRecordTypeId;
					newRecommendList.add(recommend);
					
					userIds.add(ap.acc.OwnerId);
					
					User u1 = [Select Email,Name From User Where Id =: ap.acc.OwnerId];
					toAddresses.add(u1.Email);
					
					attachmentList = [Select 
										Id,OwnerId,Name,IsPrivate,Body,Description,ContentType 
									  From 
									  	Attachment 
									  Where 
									  	IsDeleted = false And ParentId =: this.oppId];
					
					Task t1 = new Task();
					t1.OwnerId = ap.acc.OwnerId;
					t1.WhatId = ap.acc.Id;
					t1.Description = '现有拿单项目"' + this.Opp.Name + '"需要您的关注。';
					t1.ActivityDateTime__c = Datetime.now();
					t1.ActivityDate = Date.today();
					t1.Subject = '请关注“' + this.Opp.Name + '” 项目';
					t1.Recommender__c = UserInfo.getName();
					t1.Receiver__c = u1.Name;
					t1.IsReminderSet = true;
					t1.ReminderDateTime = Datetime.newInstance(Date.today().year(), Date.today().month(), Date.today().day(), 8, 0, 0); 
					
					taskList.add(t1);
				}
			}
		}
		
		if(newRecommendList.size() == 0)
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '请先勾选客户');         
       		ApexPages.addMessage(msg);
       		return null;		
		}
		
		String content = '现有拿单项目"' + this.Opp.Name + '"需要您的关注。';
		content += '以下是项目链接: ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + this.Opp.Id;
		mail.setPlainTextBody(content);
		mail.setSubject('拿单项目关注');
		mail.setToAddresses(toAddresses);
		Messaging.sendEmail(new Messaging.Singleemailmessage[]{mail});
		insert taskList;
		insert newRecommendList;
		
		// 把当前Opp共享给收件人用户
		shareOppToUsers(userIds);
		
		if(attachmentList.size() != 0)
        {
        	for(Attachment a : attachmentList)
        	{
        		for(Task t : taskList)
        		{
        			Attachment a1 = new Attachment();
        			a1.ParentId = t.Id;
        			a1.Body = a.Body;
        			a1.ContentType = a.ContentType;
        			a1.Description = a.Description;
        			a1.IsPrivate = a.IsPrivate;
        			a1.Name = a.Name;
        			a1.OwnerId = t.OwnerId;
        			newAttachmentList.add(a1);
        		}
        	}
        	insert newAttachmentList;
        }
		
		PageReference redirectPag = new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/' + this.oppId);
        redirectPag.setRedirect(true);
        return redirectPag;
	}
	public PageReference Cancel()
	{
		PageReference redirectPag = new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/' + this.oppId);
        redirectPag.setRedirect(true);
        return redirectPag; 
	}
	
	// 共享Opp的读取权限给指定的用户
	public void shareOppToUsers(Set<Id> userIds){
		List<OpportunityShare> oss = new List<OpportunityShare>();
		
		for(Id uid : userIds){
			if(!checkUserOppReadAccess(uid)){
				OpportunityShare os = new OpportunityShare();
				os.OpportunityId = this.Opp.Id;
				os.OpportunityAccessLevel = 'Read';
				os.UserOrGroupId = uid;
				oss.add(os);
			}
		}
		
		if(oss.size() > 0)
		insert oss;
	}
	
	// 检查收件人用户是否对推荐的拿单项目Opp有读取权限
	public boolean checkUserOppReadAccess(Id userId){
		List<UserRecordAccess> uraList = [Select RecordId, HasReadAccess
										  	From UserRecordAccess 
										  	Where UserId =: userId 
										  	and RecordId =: this.Opp.Id];
		
		if(uraList.size() > 0)
			return uraList[0].HasReadAccess;
		else
			return false;
	}
	
	public ApexPages.StandardSetController conset{get;set;}
	
	public Integer result{get;set;}
	public boolean hasPrevious
	{
		get
		{
			return conset.getHasPrevious();
		}
		set;
	}
	
	public boolean hasNext
	{
		get
		{
			return conset.getHasNext();
		}
		set;
	}
	
	public Integer pageNumber
	{
		get
		{
			return conset.getPageNumber();
		}
		set;
	}
	
	public void first()
	{
		conset.first();
		result = conset.getRecords().size();
		this.targetAccountList = new List<AccountPack>();
		for(Account a : (List<Account>) conset.getRecords())
		{
			this.targetAccountList.add(new AccountPack(a));
		}
	}
	
	public void last()
	{
		conset.last();
		result = conset.getRecords().size();
		this.targetAccountList = new List<AccountPack>();
		for(Account a : (List<Account>) conset.getRecords())
		{
			this.targetAccountList.add(new AccountPack(a));
		}
	}
	
	public void previous()
	{
		conset.previous();
		result = conset.getRecords().size();
		this.targetAccountList = new List<AccountPack>();
		for(Account a : (List<Account>) conset.getRecords())
		{
			this.targetAccountList.add(new AccountPack(a));
		}
	}
	
	public void next()
	{
		conset.next();
		result = conset.getRecords().size();
		this.targetAccountList = new List<AccountPack>();
		for(Account a : (List<Account>) conset.getRecords())
		{
			this.targetAccountList.add(new AccountPack(a));
		}
	}
	
	public Integer categoryNumber
	{
		get
		{
			return conset.getResultSize();
		}
		set;
	}
	
	public Integer totalPageNumber
	{
		get
		{
			if(Math.mod(categoryNumber, 20) == 0)
			{
				return (categoryNumber / 20);
			}
			else
			{
				return ((categoryNumber / 20) + 1);
			}
		}
	}
}