/*
作者：winter
创建时间：2014-07-25
功能：为测试类补充数据
*/
@IsTest
public class StructureMapData {
	public static void StructureMap()
	{
		HRQ_Sync_Setting__c hrsSet = new HRQ_Sync_Setting__c();
		List<HRQ_Sync_Setting__c> hssList =  new List<HRQ_Sync_Setting__c>();
    	hrsSet.Name='华尔圈数据同步设置项';
    	hrsSet.HRQ2SFDC_Last_Synced_EndOn__c = DateTime.newInstance(2000, 11, 1, 12, 30, 2);
    	hrsSet.HRQ2SFDC_Last_Synced_StartOn__c = DateTime.newInstance(2000, 11, 1, 12, 30, 2);
    	hrsSet.SFDC2HRQ_Update_By_History__c = false;
    	hrsSet.SFDC2HRQ_Last_Synced_StartOn__c = DateTime.newInstance(2000, 11, 1, 12, 30, 2);
    	hrsSet.HRQ2SFDC_Last_Synced_EndOn__c = DateTime.newInstance(2000, 11, 1, 12, 30, 2);
    	hrsSet.IsActive__c = true;
    	hssList.add(hrsSet);
    	insert hssList;
		
		List<HRQ_Setting__c> settings = new List<HRQ_Setting__c>();
		for(Integer i = 0 ; i < 3 ; i++)
		{
			HRQ_Setting__c hs = new HRQ_Setting__c();
			if(i==0)
			{
				hs.Name = 'HRQLoginName';
				hs.Value__c = 'Cideatech';
			}
			if(i==1)
			{
				hs.Name = 'HRQLoginPassword';
				hs.Value__c = 'PyWHY5WjKqCrqq2c';
			}
			if(i==2)
			{
				hs.Name = 'HRQServiceUrl';
				hs.Value__c = 'http://www.demo308.bsa.cn/PortalService.php';
			}
			settings.add(hs);
		}
		insert settings;
		
    	List<HRQ_Area_Mapping__c> hamlist = new List<HRQ_Area_Mapping__c>();
		for(Integer i = 0; i < 5 ; i++)
		{
			HRQ_Area_Mapping__c  ham = new HRQ_Area_Mapping__c();
			ham.HRQ_Name__c = '地图';
			ham.HRQ_ID__c = string.valueOf(i);
			ham.SFDC_Name__c = 'SFDC_Name__c';
			ham.Name = string.valueOf(i);
			hamlist.add(ham);
		}
		insert hamlist;
		
		List<HRQ_WantType_Mapping__c> hwtlist = new List<HRQ_WantType_Mapping__c>();
		for(Integer i = 0; i < 5 ; i++)
		{
			HRQ_WantType_Mapping__c  ham = new HRQ_WantType_Mapping__c();
			if(i==1)
			{
				ham.SFDC_Object_Name__c = 'Account';
			}
			else
			{
				ham.SFDC_Object_Name__c = 'Opportunity';
			}
			ham.SFDC_Object_Name__c = 'Account';
			ham.HRQ_WantType__c = 'Sell';
			ham.SFDC_RecordType_Developer_Name__c = 'Buyer';
			ham.Name = string.valueOf(i);
			hwtlist.add(ham);
		}
		insert hwtlist;
		
		
		List<HRQ_Industry_Mapping__c> himlist = new List<HRQ_Industry_Mapping__c>();
		for(Integer i = 0; i < 5 ; i++)
		{
			HRQ_Industry_Mapping__c him = new HRQ_Industry_Mapping__c();
			him.HRQ_ID__c = string.valueOf(i);
			him.HRQ_Name__c = '行业';
			him.SFDC_Industry_Name__c = '一级行业';
			him.Name = string.valueOf(i);
			him.SFDC_Sub_Industry_Name__c = '二级行业';
			himlist.add(him);
		}
		insert himlist;
		
		List<HRQ_WantType_Mapping__c> hwmlist = new List<HRQ_WantType_Mapping__c>();
		for(Integer i = 0 ; i < 5 ; i++)
		{
			HRQ_WantType_Mapping__c hwm = new HRQ_WantType_Mapping__c();
			hwm.SFDC_Object_Name__c = 'Account';
			hwm.Name = string.valueOf(i);
			hwm.SFDC_RecordType_Developer_Name__c = 'Seller';
			hwm.HRQ_WantType__c = 'Sell';
			hwmlist.add(hwm);
		}
		insert hwmlist;
		
		
		
	}
	
}