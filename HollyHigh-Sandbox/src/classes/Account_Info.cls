/*
 *Author:Leo
 *Date:2014-03-26
 *Function： 显示客户的基本信息
 */
public with sharing class Account_Info {
	Account acc{get;set;}{acc = new Account();}
	string id{get;set;}
	public Decimal dealsnum{get;set;}
	public string WhetherListed{get;set;}
	public string Website{get;set;}
	public Decimal TotalAssets{get;set;}
	public string SecondAddress{get;set;}
	public string OwnerName{get;set;}
	public string OwnerId{get;set;}
	public Decimal OperatingIncome{get;set;}
	public Decimal NetProfit{get;set;}
	public Decimal NetAssets{get;set;}
	public string accName{get;set;}
	public Date Lastsignindate{get;set;}
	public string IndustryType{get;set;}
	public string Industry{get;set;}
	public string FirstAddress{get;set;}
	public string Description{get;set;}
	public string CorporationCode{get;set;}
	public string AccRecordType{get;set;}
	public string AccDep{get;set;}
	public Account_Info(ApexPages.standardController controller)
	{
		string currentAccountId = ApexPages.currentPage().getParameters().get('Id');
		acc = [Select id, dealsnum__c, WhetherListed__c, Website, TotalAssets__c, 
					  SecondAddress__c, Owner.Name, OperatingIncome__c, 
					  NetProfit__c, NetAssets__c, Name, Lastsignindate__c, IndustryType__c,
					  Industry, FirstAddress__c, Description, CorporationCode__c, 
					  AccRecordType__c, AccDep__c, RecordTypeId, RecordType.Name 
		       From Account 
		       where id=:currentAccountId];
		       
		id = acc.id;
		dealsnum = acc.dealsnum__c;
		Website = acc.Website;
		TotalAssets = acc.TotalAssets__c;
		
		SecondAddress = acc.SecondAddress__c;
		OwnerName = acc.Owner.Name;
		OperatingIncome = acc.OperatingIncome__c;
		
		NetProfit = acc.NetProfit__c;
		NetAssets =acc.NetAssets__c; 
		accName = acc.Name;
		Lastsignindate = acc.Lastsignindate__c;
		IndustryType = acc.IndustryType__c;
		
		Industry = acc.Industry;
		FirstAddress = acc.FirstAddress__c;
		Description = acc.Description;
		CorporationCode = acc.CorporationCode__c;
		
		AccRecordType = acc.RecordType.Name;
		AccDep = acc.AccDep__c;
		
		if(acc.WhetherListed__c != null && acc.WhetherListed__c == true)
		{
			WhetherListed = '是';
		}
		else
			if(acc.WhetherListed__c != null && acc.WhetherListed__c == false)
			{
				WhetherListed = '否';
			}
			else
			{
				WhetherListed = '';
			}
	}
}