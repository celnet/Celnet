/**
 * Test ShareUnresolvedEmailBatch
 * Test ShareUnresolvedEmailBatch_Schedule
 */
@isTest
private class Test_ShareUnresolvedEmailBatch {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        //user
        
        //Unresolved_Email__c
        List<Unresolved_Email__c> list_ue = new List<Unresolved_Email__c>();
        Unresolved_Email__c ue1 = new Unresolved_Email__c();
        ue1.Body__c = 'xxx';
        ue1.From_Address__c = 'test@test.com';
        ue1.CC_Addresses__c = [Select Id,Email From User Where (not Id =: Userinfo.getUserId()) and IsActive = true limit 1].Email;
        ue1.To_Addresses__c = 'add@test.com';
        list_ue.add(ue1);
        Unresolved_Email__c ue2 = new Unresolved_Email__c();
        ue2.Body__c = 'xxx';
        ue2.From_Address__c = 'test@test.com';
        ue2.CC_Addresses__c = [Select Id,Email From User Where (not Id =: Userinfo.getUserId()) and IsActive = true limit 1].Email;
        ue2.To_Addresses__c = 'add@test.com';
        list_ue.add(ue2);
        insert list_ue;
        system.debug([Select CreatedDate From Unresolved_Email__c]);
        system.test.startTest();
        ShareUnresolvedEmailBatch sueb = new ShareUnresolvedEmailBatch();
        Database.executeBatch(sueb);
        system.test.stopTest();
    }
    static testMethod void test2()
    {
    	string sch=DateTime.now().second()+' '+DateTime.now().minute()+' '+DateTime.now().hour()+' * * ?';
        ShareUnresolvedEmailBatch_Schedule att = new ShareUnresolvedEmailBatch_Schedule();
        System.schedule('test', sch , att);
    }
}