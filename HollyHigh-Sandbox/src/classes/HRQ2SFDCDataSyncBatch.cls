/*
作者：Tommy Liu
电子油箱：tommyliu@cideatech.com
创建时间：2014-07-08
功能：华尔圈数据同步基础类，此类为抽象基础类，规定了数据同步的规范，所有从华尔圈获取数据到SFDC的具体同步类都需要实现此类
同步方向：华尔圈到SFDC
*/
global abstract class HRQ2SFDCDataSyncBatch implements Database.Batchable<Integer>, Database.Stateful, Database.AllowsCallouts
{
	protected HRQService hrqService; 
	protected Boolean isStopNext;
	public Boolean IsChain {get; set;}//是否形成链式结构，执行完当前对象的同步，自动启动下一个对象的同步
	public Integer Recover {get; set;}//表示为第几次重试，空或0表示为正常执行，1表示第一次重试....
	protected final Integer RecoverTime;//出现连接错误后最重试几次
	
	protected HRQ_Sync_Setting__c syncSetting;
	
	private void initHRQService()
	{
		try
		{
			this.hrqService = new HRQService();
		}
		catch (CalloutException exc)
		{
			this.Log(exc); 
			throw exc;
		}
	}
	
	public HRQ2SFDCDataSyncBatch()
	{
		this.Recover = 0;
		this.RecoverTime = 2;//TODO：可以修改为后台配置
		this.IsChain = true;
		this.isStopNext = false;
		this.syncSetting = [Select Id, 
							HRQ2SFDC_Last_Synced_EndOn__c,
							HRQ2SFDC_Last_Synced_StartOn__c,
							SFDC2HRQ_Update_By_History__c,
							SFDC2HRQ_Last_Synced_EndOn__c,
							SFDC2HRQ_Last_Synced_StartOn__c
					From HRQ_Sync_Setting__c
					Where IsActive__c = true Limit 1];
	}
	
	global virtual List<Integer> start(Database.BatchableContext BC)
	{
		system.debug(' start !!!!!!!');
		this.initHRQService();
    	return new Integer[] {1};
	}

	global virtual void execute(Database.BatchableContext BC, List<Integer> scope)
	{
		system.debug('   execute !!!!!!!');
		try
		{
			this.getDataFromHRQ();
		}
		catch (CalloutException exc)
		{
			this.Log(exc); 
			throw exc;
		}
		this.convertData();
	}

	global virtual void finish(Database.BatchableContext BC)
	{
		try
		{
			this.saveData();
		}
		catch (DMLException exc)
		{
			this.Log(exc);
			throw exc; 
		}
		if(!this.isStopNext)
		{
			this.handleNext();
		}
	}
	
	global virtual String getComName()
    {
    	return 'HRQ2SFDCDataSyncBatch';
    }
   
	global abstract void getDataFromHRQ();//需要实现通过WebService从华尔圈获取数据
	
	global abstract void convertData();//需要实现将从华尔圈获取的数据转化为SFDC数据
	
	global abstract void saveData();//需要实现将转化后的SFDC数据保存到SFDC数据库
	
	global abstract void handleNext();//需要实现启动获取下一页的Batch，或启动其它数据的Batch，将过程连接起来
	
	//保存单个log
	public void Log(HRQ_Sync_Log__c log)
	{
		if(log == null)
		{
			return;
		}
		database.insert(log, false);
	}
	
	//保存多个Log
	public void Logs(HRQ_Sync_Log__c[] logs)
	{
		database.insert(logs, false);
	}
	
	//将Excecption转化为Log并保存
	public void Log(Exception exc)
	{
		if(exc == null)
		{
			return;
		}
		HRQ_Sync_Log__c log = this.toLog(exc);
		this.log(log);
	}
	
	//将UpsertResult转化为Log
	protected HRQ_Sync_Log__c toLog(Database.UpsertResult re)
	{
		HRQ_Sync_Log__c log = new HRQ_Sync_Log__c();
		log.Com__c = this.getComName();
		log.Direction__c = 'HRQ2SFDC';
		log.Recover__c = this.Recover;
		Database.Error[] errors;
		if(!re.isSuccess())
		{
			log.Type__c = '错误';
			errors = re.getErrors();
		}
		else
		{
			log.Type__c = '信息';
		}
		log.SFDC_ID__c = re.getId();
		log.Detail__c = '';
		if(re.isCreated())
		{
			log.Detail__c += 'Action: Create';
		}
		else
		{
			log.Detail__c += 'Action: Update';
		}
		log.Detail__c += '\r\n';
		if(errors != null)
		{
			for(Database.Error error : errors)
			{
				log.Detail__c += 'Message:';
				log.Detail__c += error.getMessage();
				log.Detail__c += '\r\n';
				log.Detail__c += 'Status Code:';
				log.Detail__c += error.getStatusCode();
				log.Detail__c += '\r\n';
			}
		}
		return log;
	}
	
	//将Exception转化为Log
	protected HRQ_Sync_Log__c toLog(Exception exc)
	{
		Exception cause = exc.getCause();
		Integer line = exc.getLineNumber();
		HRQ_Sync_Log__c log = new HRQ_Sync_Log__c();
		log.Recover__c = this.Recover;
		log.Type__c = '错误';
		log.Com__c = this.getComName();
		log.Direction__c = 'HRQ2SFDC';
		log.Sub_Type__c = exc.getTypeName();
		if(line != null)
		{
			log.Line_Number__c = String.valueof(line);
		}
		log.Detail__c = '';
		log.Detail__c += '第' + String.valueof(line) + '行\n\r';
		log.Detail__c += exc.getTypeName();
		log.Detail__c += '\n\r';
		log.Detail__c += exc.getMessage();
		log.Detail__c += '\n\r';
		log.Detail__c += exc.getStackTraceString();
		log.Detail__c += '\n\r';
		if(cause != null)
		{
			Integer line2 = exc.getLineNumber();
			log.Detail__c += '-------------------------------------------------------\n\r';
			if(line2 != null)
			{
				log.Detail__c += '第' + String.valueof(line2) + '行\n\r';
			}
			log.Detail__c += cause.getTypeName();
			log.Detail__c += '\n\r';
			log.Detail__c += cause.getMessage();
			log.Detail__c += '\n\r';
			log.Detail__c += cause.getStackTraceString();
			log.Detail__c += '\n\r';
		}
		
		return log;
	}
}