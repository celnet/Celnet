/*
  Author: Crazy ma
* Created On: 2014-1-15
* Function:Test ProjectHollyhigh_Roller
*/
@isTest
private class Test_ProjectHollyhigh_Rollup 
{
	//测试常见情况下(更改项目阶段类型)的case
    static testMethod void usualTest() 
    {
       //准备数据
       Account acc = new Account();
       acc.Name = '雨花石';
       acc.dealsnum__c = 2;
       insert acc;
       
       ProjectHollyhigh__c pro1 = new ProjectHollyhigh__c();
       pro1.Account__c = acc.Id;
       pro1.ProjectState__c = '意向书';
       
       ProjectHollyhigh__c pro2 = new ProjectHollyhigh__c();
       pro2.Account__c = acc.Id;
       pro2.ProjectState__c = '意向书';
       insert new ProjectHollyhigh__c[] {pro1, pro2};
       
       pro1.ProjectState__c = '收款';
       pro2.ProjectState__c = '签约成交';
       update new ProjectHollyhigh__c[] {pro1, pro2};
       
       //验证签约次数和最后一次签约时间
      acc = [select Id,dealsnum__c,Lastsignindate__c from Account where Id=:acc.Id];
      System.assertEquals(2,acc.dealsnum__c );
      System.assertEquals(Date.today(),acc.Lastsignindate__c );
    }
    
    //测试少见情况下的case
    static testMethod void specialTest()
    {
    	ProjectHollyhigh__c  pp = new ProjectHollyhigh__c();
       //测试用户新建了签约成交的做单项目
       Account acc = new Account();
       acc.Name = '雨花石';
       insert acc;
       
       ProjectHollyhigh__c pro1 = new ProjectHollyhigh__c();
       pro1.Account__c = acc.Id;
       pro1.ProjectState__c = '收款';
       insert pro1;
       
       //验证数据
       acc = [select Id,dealsnum__c,Lastsignindate__c from Account where Id=:acc.Id];
       System.assertEquals(1,acc.dealsnum__c );
       System.assertEquals(Date.today(),acc.Lastsignindate__c );
       
       
       //测试由签约状态修改为之前的状态的情况
       pro1.ProjectState__c = '意向书';
       update pro1;
       
       //验证数据
       acc = [select Id,dealsnum__c,Lastsignindate__c from Account where Id=:acc.Id];
       System.assertEquals(0,acc.dealsnum__c );
       
       
    }
}