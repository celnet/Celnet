/*
作者：Tommy Liu
电子油箱：tommyliu@cideatech.com
创建时间：2014-07-08
功能：推送有变化的联系人到华尔圈
同步方向：SFDC到华尔圈 
*/
global class SFDC2HRQContactUpdateSyncBatch extends SFDC2HRQDataSyncBatch
{  
	private HRQRelatedContact[] relContactList;
	
	global SFDC2HRQContactUpdateSyncBatch()
	{
		super();
		//this.relContactList = new List<HRQRelatedContact>();
	}
		
	global override Database.QueryLocator getDataScop()
	{
		//查询“是否排除”不等于Y的contact数据，不将重复数据更新到HRQ 2014.8.11	by winter
		if(this.syncSetting.SFDC2HRQ_Update_By_History__c)
		{
			return Database.getQueryLocator([select ID,Name,HRQ_ID__c,HRQ_Project_ID__c,LastName,Title,MobilePhone,Phone,Email,HRQ_Uploader_ID__c,HRQ_UploadedOn__c,HRQ_Sync_Exception__c,
											(Select Id, ContactId, CreatedById, CreatedDate, Field, OldValue, NewValue From Histories where CreatedDate >=: syncSetting.SFDC2HRQ_Last_Synced_StartOn__c)
											from Contact 
											where ID In
											(Select ContactId From ContactHistory Where CreatedDate >=:syncSetting.SFDC2HRQ_Last_Synced_StartOn__c )
											and HRQ_ID__c !=null and HRQ_Sync_Exception__c !='Y']);
		}else
		{
			return Database.getQueryLocator([select ID,Name,HRQ_ID__c,HRQ_Project_ID__c,LastName__c,Title,MobilePhone,Phone,Email,HRQ_Uploader_ID__c,HRQ_UploadedOn__c,HRQ_Sync_Exception__c from Contact where HRQ_ID__c !=null and HRQ_Sync_Exception__c !='Y']);
		}
		
		
	}

	global override void convertData(List<SObject> scope)
	{
		this.relContactList = new List<HRQRelatedContact>();
		if(this.syncSetting.SFDC2HRQ_Update_By_History__c)
		{
			System.debug('_____________按照历史记录更新');
			for(Contact con : (List<Contact>)scope)
			{
				HRQRelatedContact relcontact = new HRQRelatedContact();
				//在修改联系人的时候不应该改动HRQID
				relcontact.Id = con.HRQ_ID__c;
				/*
				//如果只有以下的逻辑，会导致传过去的ID为空，导致修改失败
				if(isFieldInHistories('HRQ_ID__c', con.Histories))
				{
					relcontact.Id = con.HRQ_ID__c;
				}
				
				if(isFieldInHistories('HRQ_Project_ID__c', con.Histories))
				{
					relcontact.ProjectId = con.HRQ_Project_ID__c;
				}*/
				if(isFieldInHistories('LastName', con.Histories))
				{
					relcontact.Name = con.Name;
				}
				if(isFieldInHistories('FirstName', con.Histories))
				{
					relcontact.Name = con.Name;
				}
				if(isFieldInHistories('Title', con.Histories))
				{
					relcontact.JobTitle	 = con.Title;
				}
				/*
				if(isFieldInHistories('MobilePhone', con.Histories))
				{
					relcontact.Mobile = con.MobilePhone;
				}
				*/
				if(isFieldInHistories('Phone', con.Histories))
				{
					relcontact.Mobile = con.Phone;//2014-10-10	修改：phone（电话（手机2））和mobilephone调换（手机）	winter
				}
				if(isFieldInHistories('Email', con.Histories))
				{
					relcontact.Email = con.Email;
				}
				/* 华尔圈发布人的Id 不能通过手动去修改更新
				if(isFieldInHistories('HRQ_Uploader_ID__c', con.Histories))
				{
					relcontact.UploaderId = con.HRQ_Uploader_ID__c;
				}
				华尔圈发布人时间是华尔圈项目进入华尔圈的时间  不能在sf进行更改
				if(isFieldInHistories('HRQ_UploadedOn__c', con.Histories))
				{
					relcontact.UploadedOn = con.HRQ_UploadedOn__c.format('yyyy-MM-dd HH:mm:ss');
				}
				*/
				relContactList.add(relcontact);
			}
		}else
		{
			System.debug('_____________不按照历史记录更新');
			for(Contact con : (List<Contact>)scope)
			{
				HRQRelatedContact relcontact = new HRQRelatedContact();
				relcontact.Id = con.HRQ_ID__c;
				//relcontact.ProjectId = con.HRQ_Project_ID__c;
				relcontact.Name = con.Name;
				relcontact.JobTitle	 = con.Title;
				//relcontact.Mobile = con.MobilePhone;
				relcontact.Mobile = con.Phone;//2014-10-10	修改：phone（电话（手机2））和mobilephone调换（手机）	winter
				relcontact.Email = con.Email;
				/*relcontact.UploaderId = con.HRQ_Uploader_ID__c;
				if(con.HRQ_UploadedOn__c != null)
				{
				relcontact.UploadedOn = con.HRQ_UploadedOn__c.format('yyyy-MM-dd HH:mm:ss');
				}*/
				relContactList.add(relcontact);
			}
		}
		
	}
	
		private Boolean isFieldInHistories(String Field, List<ContactHistory> histories)
	{
		if(histories == null)
		{
			return false;
		}
		for(ContactHistory h : histories)
		{
			if(h.Field.equalsIgnoreCase(Field))
			{
				return true;
			}
		}
		return false;
	}
	
	global override void pushDataToHRQ()
	{
		this.hrqService.UpdateRelatedContact(relContactList);
	}
	
	global override void handleNext()
	{
		if(this.IsChain)
        {
            SFDC2HRQAccountUpdateSyncBatch nextBatch = new SFDC2HRQAccountUpdateSyncBatch();
             nextBatch.currentTime = this.currentTime;
            nextBatch.IsChain = this.IsChain;
            nextBatch.BatchSize = this.BatchSize; 
            database.executeBatch(nextBatch, nextBatch.BatchSize);
        }
	}
}