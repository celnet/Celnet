/*
 * Author: Steven
 * Created On: 2014-1-15
 * Description: Test BuySellRecommendationController
 */

@isTest
public class Test_BuySellRecommendation2Controller 
{
	
	// Assume users to click sure button
	static testmethod void testPositiveCase(){
		//准备数据
		Account testAcc1 = new Account();
		RecordType seller = [Select Id From RecordType Where SobjectType = 'Account' And DeveloperName = 'Seller'];
		testAcc1.RecordTypeId = seller.Id;
		testAcc1.Name = 'Account1Seller';
		testAcc1.Industry = '食品';
		testAcc1.IndustryType__c = '健康';
		insert testAcc1;
		
		Account testAcc2 = new Account();
		RecordType buyer = [Select Id From RecordType Where SobjectType = 'Account' And DeveloperName = 'Buyer'];
		testAcc2.RecordTypeId = buyer.Id;
		testAcc2.Name = 'Account2Buyer';
		testAcc2.Industry = '食品';
		testAcc2.IndustryType__c = '健康';
		insert testAcc2;
		
		Account testAcc3 = new Account();
		testAcc3.RecordTypeId = buyer.id;
		testAcc3.Name = 'Account3Buyer';
		testAcc3.Industry = '食品';
		testAcc3.IndustryType__c = '健康';
		insert testAcc3;
		
		ProjectHollyhigh__c testProjectc = new ProjectHollyhigh__c();
		testProjectc.Account__c = testAcc1.Id;
		RecordType projectRT = [Select Id From RecordType Where SobjectType = 'ProjectHollyhigh__c' And DeveloperName = 'Seller'];
		testProjectc.RecordTypeId = projectRT.id;
		testProjectc.Name = 'testProject';
		insert testProjectc;
        
        // 执行构造函数设置默认值
        ApexPages.currentPage().getParameters().put('id',testProjectc.Id);
		BuySellRecommendation2Controller testController = new BuySellRecommendation2Controller(new ApexPages.Standardcontroller(testProjectc));
		
		// 验证做单项目及客户信息
		System.assertEquals('testProject',testController.projectc.Name);
	 	System.assertEquals('Account1Seller',testController.acc.Name);
	 	//修改为'卖/融资方/卖壳方'（正式系统改动）	by winter 2014-8-29
	 	System.assertEquals('卖方/融资方/卖壳方',testController.acc.RecordType.Name);
		
		// 验证推荐客户和行业信息
		System.assertEquals([select id from RecordType where DeveloperName=:'Buyer' and SobjectType=:'Account'].Id, testController.acc.RecordTypeId);
	 	System.assertEquals('食品',testController.acc.industry);
	    System.assertEquals('健康',testController.acc.IndustryType__c );
		
		// 调用搜索方法
		testController.Search();
		
		boolean test_hp = testController.hasPrevious;
		boolean test_hn = testController.hasNext;
		Integer test_pn = testController.pageNumber;
		testController.previous();
		testController.next();
		testController.first();
		testController.last();
		Integer test_cn = testController.categoryNumber;
		Integer test_tp = testController.totalPageNumber;
		
		// 验证搜索结果
		System.assertEquals(2, testController.aw.size());
		System.assertEquals('Account2Buyer', testController.aw[0].account.name);
		System.assertEquals('Account3Buyer', testController.aw[1].account.name);
		
		// 勾选复选框
		testController.aw[0].isChecked = true;
		
		// 调用确认方法
		PageReference pageRef = testController.Sure();
		
		// 验证推荐结果
		ID recommRecordTypeId = [select id from RecordType where DeveloperName=:'Buyer' and SobjectType=:'ResourceRecommend__c'].id;
		List<ResourceRecommend__c> recommends = [select 
	 												Id
	 											from ResourceRecommend__c
	 											Where Account__c =: testAcc1.Id
	 											And Project__c =: testProjectc.Id
	 											And Target_Account__c =: testAcc2.Id
	 											And RecordTypeId =: recommRecordTypeId];
		
	 	// 验证URL地址
	 	System.assertEquals(1, recommends.size());
	 	System.assertEquals(true, pageRef.getRedirect());
	 	System.assertEquals(URL.getSalesforceBaseUrl().toExternalForm() + '/' + testProjectc.Id, pageRef.getUrl());
	}
	
	// Assume users to click cancel button
	static testmethod void testNegativeCase(){
		// 准备数据
		Account testAcc1 = new Account();
		RecordType seller = [Select Id From RecordType Where SobjectType = 'Account' And DeveloperName = 'Seller'];
		testAcc1.RecordTypeId = seller.Id;
		testAcc1.Name = 'Account1Seller';
		testAcc1.Industry = '食品';
		testAcc1.IndustryType__c = '健康';
		insert testAcc1;
		
		ProjectHollyhigh__c testProjectc = new ProjectHollyhigh__c();
		testProjectc.Account__c = testAcc1.Id;
		RecordType projectRT = [Select Id From RecordType r Where SobjectType = 'ProjectHollyhigh__c' And DeveloperName = 'Seller'];
		testProjectc.RecordTypeId = projectRT.id;
		testProjectc.Name = 'testProject';
		insert testProjectc;
        
        
        ApexPages.currentPage().getParameters().put('id',testProjectc.Id);
        BuySellRecommendation2Controller testController = new BuySellRecommendation2Controller(new ApexPages.Standardcontroller(testProjectc));
        
        testController.Search();
        
        //预期找不到数据
	    System.assertEquals(0, testController.aw.size());
	    ApexPages.Message notFoundMsg;
	    List<ApexPages.Message> msgs = ApexPages.getMessages();
	    System.assertEquals('按照您设置的条件，没有找到任何可推荐的客户，请合理设置条件后重新搜索', msgs[msgs.size()-1].getSummary());
	    System.assertEquals(ApexPages.Severity.INFO, msgs[msgs.size()-1].getSeverity());
        
        // 调用取消按钮
        PageReference pageRef = testController.Cancel();
        
        // 验证返回连接
	   	System.assertEquals(true, pageRef.getRedirect());
        System.assertEquals(URL.getSalesforceBaseUrl().toExternalForm() + '/' + testProjectc.id, pageRef.getUrl());
        
        testController.next();
        testController.first();
        testController.last();
        
	}
}