/*
   Author:Crazy
 * Date:2014-1-14
 * Description:测试BuySellRecommendationController类
*/
@isTest
private class Test_BuySellRecommendationPage {
	//测试搜索方法
    static testMethod void searchTest() 
    {
        Opportunity opp = NEW Opportunity();
   		opp.Name = '雨花石';
   		opp.StageName ='创新';
   		opp.CloseDate = Date.today();
   		insert opp;
   		
   		ApexPages.currentPage().getParameters().put('id',opp.Id);
	    BuySellRecommendationController bs = new BuySellRecommendationController(new ApexPages.StandardController(opp));
	    
	    RecordType re = new RecordType();
   		re.DeveloperName = 'Seller';
   		re.SObjectType = 'Account';
   		re.Name = 'Name';
   		
   		Account acc = new Account();
	    acc.Name = 'Cideatech';
	    acc.IndustryType__c = '健康';
	    acc.industry= '食品';
	    insert acc;
	    
   		BuySellRecommendationController.AccountPack aa = new BuySellRecommendationController.AccountPack();
   		aa.isCheck = true;
   		aa.acc = acc;
   		
   		bs.targetAccountList = new List<BuySellRecommendationController.AccountPack>();
   		bs.targetAccountList.add(aa);
	    
	    bs.Search();
    }
    
    //测试确认方法
     static testMethod void sureTest() 
     {
     	Opportunity opp = NEW Opportunity();
   		opp.Name = '雨花石';
   		opp.StageName ='创新';
   		opp.CloseDate = Date.today();
   		insert opp;
   		
   		Account acc1 = new Account();
	    acc1.Name = 'Cideatech';
	    acc1.IndustryType__c = '健康';
	    acc1.industry= '食品';
	    insert acc1;
   		
   		ApexPages.currentPage().getParameters().put('id',opp.Id);
	    BuySellRecommendationController bs = new BuySellRecommendationController(new ApexPages.StandardController(opp));
	    
	    BuySellRecommendationController.AccountPack aa = new BuySellRecommendationController.AccountPack();
   		aa.isCheck = true;
   		aa.acc = acc1;
   		
   		bs.targetAccountList = new List<BuySellRecommendationController.AccountPack>();
   		bs.targetAccountList.add(aa);
	    
	    //bs.Sure();
     }
     
     //测试取消方法
     static testMethod void cancleTest()
     {
     	Opportunity opp = NEW Opportunity();
   		opp.Name = '雨花石';
   		opp.StageName ='创新';
   		opp.CloseDate = Date.today();
   		insert opp;
   		
   		ApexPages.currentPage().getParameters().put('id',opp.Id);
	    BuySellRecommendationController bs = new BuySellRecommendationController(new ApexPages.StandardController(opp));
	    
	    bs.Cancel();
     }
     
     
}