public class HRQRelatedContact {
	public string Id{get;set;}
	public string ProjectId{get;set;}
	public string Name{get;set;}
	public string JobTitle{get;set;}
	public string Mobile{get;set;}
	public string Email{get;set;}
	public string UploaderId{get;set;}
	public string UploadedOn{get;set;} 
	public HRQMember Uploader {get; set;}
}