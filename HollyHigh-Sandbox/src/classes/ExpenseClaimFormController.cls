/**
 * Author:Sunny 
 * 
 * 
**/
public class ExpenseClaimFormController 
{
    private ID CostReiId;
    public  CostReimbursement__c CostRei{get;set;}
    public List<List<ExpenseClaimDetail>> list_Detail{get;set;}
    public List<TravelExpenseClaimDetail> list_TravelDetail{get;set;}
    public String strCostTotalAmount{get;set;}//业务费用大写
    public String strTravelCostAmount{get;set;}//差旅费用大写
    public String sYear{get{return String.valueOf(date.today().year());}set;}
    public String sMonth{get{return String.valueOf(date.today().month());}set;}
    public String sDay{get{return String.valueOf(date.today().Day());}set;}
    
    public Boolean displayTravelCost{get;set;}
    public Boolean displayBusinessCost{get;set;}
    
    public ExpenseClaimFormController(Apexpages.Standardcontroller controller)
    {
    	this.displayBusinessCost = true;
    	this.displayTravelCost = true;
    	
        CostReiId = controller.getId();
        this.initCostReiDetail();
    }
    private void initCostReiDetail()
    {
        CostRei = [Select Id,OwnerId,Owner.Alias,Owner.Name,CostTotalAmountLower__c,TravelInvoiceNum__c,CostInvoiceNum__c,IndusDepartment__c,TravelCostAmount__c, (Select RecordTypeId,RecordType.DeveloperName, GeneralCostType__c, CostAmount__c,TravleEndDate__c,TravelStartDate__c,TravelAddress__c,TravelCostType__c From CostDetail_Cost__r ) From CostReimbursement__c where Id =: CostReiId];
        
        if(CostRei != null)
        {
        	if(CostRei.CostTotalAmountLower__c == 0)
        	{
        		this.displayBusinessCost = false;
        	}
        	
        	if(CostRei.TravelCostAmount__c == 0)
        	{
        		this.displayTravelCost = false;
        	}
        }
        
        strCostTotalAmount = UtilToolClass.translate(CostRei.CostTotalAmountLower__c);
        strTravelCostAmount = UtilToolClass.translate(CostRei.TravelCostAmount__c);
        this.initDetailList();
        //2014-1-17 
        for(integer i=0;i<2;i++){
            List<ExpenseClaimDetail> list_ecd = new List<ExpenseClaimDetail>();
            list_ecd.add(new ExpenseClaimDetail(''));
            list_ecd.add(new ExpenseClaimDetail(''));
            list_ecd.add(new ExpenseClaimDetail(''));
            list_Detail.add(list_ecd);
        }
        this.initTravelDetailList();
    }
    //一般报销
    private void initDetailList()
    {
        list_Detail = new List<List<ExpenseClaimDetail>>();
        Map<String , Decimal> Map_Cost = new Map<String , Decimal>();
        Map<String, Schema.SobjectField> CostReiFields =
           Schema.SobjectType.CostReimbursementDetail__c.fields.getMap();
        Schema.SobjectField sObjField = CostReiFields.get('GeneralCostType__c');
        List<Schema.PicklistEntry> listPicklistEntry = sObjField.getDescribe().getPicklistValues();
        for(Schema.PicklistEntry pe : listPicklistEntry)
        {
            if(pe.isActive())
            {
                Decimal d;
                Map_Cost.put(pe.getLabel(), d);
            }
        }
        for(CostReimbursementDetail__c crDetail : CostRei.CostDetail_Cost__r)
        {
            if(crDetail.RecordType.DeveloperName != 'GeneralFeeDetail')
            {
                continue;
            }
            if(Map_Cost.containsKey(crDetail.GeneralCostType__c))
            {
                Decimal dAmount = Map_Cost.get(crDetail.GeneralCostType__c);
                dAmount=(dAmount==null?0:dAmount)+(crDetail.CostAmount__c==null?0:crDetail.CostAmount__c);
                Map_Cost.put(crDetail.GeneralCostType__c,dAmount);
            }else
            {
                Decimal dAmount = 0;
                dAmount+=(crDetail.CostAmount__c==null?0:crDetail.CostAmount__c);
                Map_Cost.put(crDetail.GeneralCostType__c,dAmount);
            }
            
            
        }
        system.debug('^^^^^'+Map_Cost);
        List<ExpenseClaimDetail> list_ecd = new List<ExpenseClaimDetail>();
        for(String sType : Map_Cost.keySet())
        {
            ExpenseClaimDetail ecd = new ExpenseClaimDetail();
            ecd.CostType = sType;
            ecd.CostAmount = Map_Cost.get(sType);
            list_ecd.add(ecd);
            if(list_ecd.size() == 3)
            {
                list_Detail.add(list_ecd);
                list_ecd = new List<ExpenseClaimDetail>();
            }
        }
        if(list_ecd.size() == 1){
            list_ecd.add(new ExpenseClaimDetail());
            list_ecd.add(new ExpenseClaimDetail());
            list_Detail.add(list_ecd);
        }else if(list_ecd.size() == 2){
            list_ecd.add(new ExpenseClaimDetail());
            list_Detail.add(list_ecd);
        }
    }
    public class ExpenseClaimDetail
    {
        public String CostType{get;set;}
        public Decimal CostAmount{get;set;}
        public ExpenseClaimDetail(){}
        public ExpenseClaimDetail(String sCostType ){CostType = sCostType;}
    }
    //差旅费报销
    private void initTravelDetailList()
    {
        Map<String , TravelExpenseClaimDetail> Map_Ecd = new Map<String , TravelExpenseClaimDetail>();
        for(CostReimbursementDetail__c crDetail : CostRei.CostDetail_Cost__r)
        {
            if(crDetail.RecordType.DeveloperName != 'outfeedetail')
            {
                continue;
            }
            SYSTEM.DEBUG('HHHH'+crDetail.TravelAddress__c);
            if(Map_Ecd.containsKey(crDetail.TravelAddress__c))
            {
                TravelExpenseClaimDetail ecd = Map_Ecd.get(crDetail.TravelAddress__c);
                ecd = this.setDetail(ecd, crDetail);
                Map_Ecd.put(crDetail.TravelAddress__c , ecd);
            }else{
                TravelExpenseClaimDetail ecd = new TravelExpenseClaimDetail();
                ecd = this.setDetail(ecd, crDetail);
                Map_Ecd.put(crDetail.TravelAddress__c , ecd);
            }
        }
        list_TravelDetail = Map_Ecd.values();
        system.debug('List Size :'+list_TravelDetail.size());
        for(Integer i=0;i<=6-list_TravelDetail.size();i++)
        {
            list_TravelDetail.add(new TravelExpenseClaimDetail());
        }
        system.debug('List Size :'+list_TravelDetail.size());
    }
    private TravelExpenseClaimDetail setDetail(TravelExpenseClaimDetail ecd , CostReimbursementDetail__c crDetail)
    {
        ecd.Area = crDetail.TravelAddress__c;
        ecd.StartDate = crDetail.TravelStartDate__c;
        ecd.EndDate = crDetail.TravleEndDate__c;
        
        if(crDetail.TravelCostType__c == '飞机、轮船票'){
            ecd.AirTicket = crDetail.CostAmount__c;
        }else if(crDetail.TravelCostType__c == '火车、汽车票'){
            ecd.RailWayTicket = crDetail.CostAmount__c;
        }else if(crDetail.TravelCostType__c == '住宿费'){
            ecd.HotelExpense = crDetail.CostAmount__c;
        }else if(crDetail.TravelCostType__c == '交通费'){
            ecd.Transportation = crDetail.CostAmount__c;
        }else if(crDetail.TravelCostType__c == '招待费'){
            ecd.Entertainment = crDetail.CostAmount__c;
        }else if(crDetail.TravelCostType__c == '其他'){
            ecd.Other = crDetail.CostAmount__c;
        } 
        return ecd;
    }
    public class TravelExpenseClaimDetail
    {
        public String Area{get;set;}
        public Date StartDate{get;set;}
        public String sDate{get{
            String s = '';
            if(StartDate != null){
                s += (StartDate.year()+'/'+StartDate.month()+'/'+StartDate.day());
                s += '-';
            }
            if(EndDate != null){
                if(s != '') s+='<br/>';
                s += (EndDate.year()+'/'+EndDate.month()+'/'+EndDate.day());
            }
            return s;
        }set;}
        public Date EndDate{get;set;}
        public Decimal AirTicket{get;set;}
        public Decimal RailWayTicket{get;set;}
        public Decimal HotelExpense{get;set;}
        public Decimal Transportation{get;set;}//交通
        public Decimal Entertainment{get;set;}//招待
        public Decimal Subsidy{get;set;}//补助
        public Decimal Other{get;set;}
        
    }
}