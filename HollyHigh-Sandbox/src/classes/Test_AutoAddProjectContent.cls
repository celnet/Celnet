@isTest
private class Test_AutoAddProjectContent {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Account rc = new Account();
		rc.dealsnum__c = 5;
		rc.Website = 'www.test.com';
		rc.TotalAssets__c = 11.11;
		rc.SecondAddress__c = '第二地址';
		rc.OperatingIncome__c = 11.11;
		rc.NetProfit__c = 11.11;
		rc.NetAssets__c = 11.11; 
		rc.Name = 'Account222';
		rc.Lastsignindate__c = Date.today();
		rc.IndustryType__c = '保健品';
		rc.Industry = '食品';
		rc.FirstAddress__c = '第一地址';
		rc.Description = 'this is the test Description';
		rc.CorporationCode__c = '55555';
		rc.AccRecordType__c = '买/借壳方';
		insert rc;
		Account rcc = [select Website, Id from Account where Website = :rc.Website];
		Opportunity op = new Opportunity();
		op.Name = 'op11111111111';
		op.AccountId = rcc.Id;
		op.StageName = '签约';
		op.CloseDate = Date.today();
//		insert op;
		
		ProjectHollyhigh__c ph = new ProjectHollyhigh__c();
		ph.Name = 'ph1';
		ph.Account__c = rc.Id;
		ph.Opportunity__c = op.Id;
		ph.ProjectState__c = '签约成交';
		ph.ProjectCode__c = 'ph000001';
		insert ph;
    }
}