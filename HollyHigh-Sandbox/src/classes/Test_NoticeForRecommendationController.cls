@isTest
public class Test_NoticeForRecommendationController 
{
	private static User createUser(String Email, String Username, String Alias, String CommunityNickname)
	{
		List<User> userList = [Select 
								Email, Username, LastName, Alias, CommunityNickname, TimeZoneSidkey,LocaleSidKey, EmailEncodingKey, ProfileId, LanguageLocaleKey From User 
								Where IsActive = true
								And Id=: UserInfo.getUserId()
								limit 1];
		User testUser = new User();
		testUser.Email = Email;
		testUser.Username = Username;			
		testUser.LastName = userList[0].LastName;
		testUser.Alias = Alias;
		testUser.CommunityNickname = CommunityNickname;
		testUser.TimeZoneSidKey = userList[0].TimeZoneSidKey;
		testUser.LocaleSidKey = userList[0].LocaleSidKey;
		testUser.EmailEncodingKey = userList[0].EmailEncodingKey;
		testUser.ProfileId = userList[0].ProfileId;
		testUser.LanguageLocaleKey = userList[0].LanguageLocaleKey;
		return testUser;
	}
		
	private static ProjectHollyhigh__c createProject(String Name, Id OwnerId)
	{
		ProjectHollyhigh__c testProject = new ProjectHollyhigh__c();
		testProject.Name = Name;
		testProject.OwnerId = OwnerId;
		return testProject;
	}
		
	static testmethod void testSendEmailWithCheckedBox()
	{
		// 准备数据
		User testUser1 = createUser('testuser1@example.com.test','testuser1@example.com.test', 'tu1','testuser1');
		User testUser2 = createUser('testuser2@example.com.test','testuser2@example.com.test', 'tu2','testuser2');
		User testUser3 = createUser('testuser3@example.com.test','testuser3@example.com.test', 'tu3','testuser3');
		insert new User[]{testUser1, testUser2, testUser3};
		
		ProjectHollyhigh__c testProjectc = createProject('testProjectc',testUser1.Id);
		insert testProjectc;
		
		// 执行构造函数
		ApexPages.currentPage().getParameters().put('id',testProjectc.Id);
		NoticeForRecommendationController testController = new NoticeForRecommendationController(new ApexPages.Standardcontroller(testProjectc));
		
		// 验证显示用户
		NoticeForRecommendationController.UserPack findTestUser1, findTestUser2, findTestUser3;
		System.assert(testController.userPackList.size() > 0);
		for(NoticeForRecommendationController.UserPack up : testController.userPackList)
		{
			if(up.user.Id == testUser1.Id)
			{
				findTestUser1 = up;
			}
			else if(up.user.Id == testUser2.Id)
			{
				findTestUser2 = up;
			}
			else if(up.user.Id == testUser3.Id)
			{
				findTestUser3 = up;
			}
		}
		System.assert(findTestUser1 == null);
		System.assert(findTestUser2 != null);
		System.assert(findTestUser3 != null);
		
		// 勾选用户
		findTestUser2.isCheck = true;
		
		// 点击发送按钮
		PageReference pageRef = testController.Send();
		
		// 验证分享
		ProjectHollyhigh__Share[] testProjectShare = [Select 
        												parentId, 
        												UserorGroupId, 
        												AccessLevel, 
        												RowCause 
        											  From 
        											  	ProjectHollyhigh__Share 
        											  Where parentId =: testProjectc.Id
        											  And UserorGroupId =: findTestUser2.user.Id];
		System.assertEquals(1, testProjectShare.size());
		
		// 验证发送结果
		System.assertEquals(true, pageRef.getRedirect());
	 	System.assertEquals(URL.getSalesforceBaseUrl().toExternalForm() + '/' + testProjectc.Id, pageRef.getUrl());
		
	}
	
	static testmethod void testSendEmailWithoutCheckedBox()
	{
		// 准备数据
		User testUser1 = createUser('testuser1@example.com.test','testuser1@example.com.test', 'tu1','testuser1');
		User testUser2 = createUser('testuser2@example.com.test','testuser2@example.com.test', 'tu2','testuser2');
		User testUser3 = createUser('testuser3@example.com.test','testuser3@example.com.test', 'tu3','testuser3');
		insert new User[]{testUser1, testUser2, testUser3};
		
		ProjectHollyhigh__c testProjectc = createProject('testProjectc',testUser1.Id);
		insert testProjectc;
		
		// 执行构造函数
		ApexPages.currentPage().getParameters().put('id',testProjectc.Id);
		NoticeForRecommendationController testController = new NoticeForRecommendationController(new ApexPages.Standardcontroller(testProjectc));
		
		// 验证显示用户
		NoticeForRecommendationController.UserPack findTestUser1, findTestUser2, findTestUser3;
		System.assert(testController.userPackList.size() > 0);
		for(NoticeForRecommendationController.UserPack up : testController.userPackList)
		{
			if(up.user.Id == testUser1.Id)
			{
				findTestUser1 = up;
			}
			else if(up.user.Id == testUser2.Id)
			{
				findTestUser2 = up;
			}
			else if(up.user.Id == testUser3.Id)
			{
				findTestUser3 = up;
			}
		}
		System.assert(findTestUser1 == null);
		System.assert(findTestUser2 != null);
		System.assert(findTestUser3 != null);
		
		// 点击发送按钮
		PageReference pageRef = testController.Send();
		
		// 未勾选的错误信息
		List<ApexPages.Message> msgs = ApexPages.getMessages();
	    System.assertEquals('请先勾选用户', msgs[msgs.size()-1].getSummary());
	    System.assertEquals(ApexPages.Severity.ERROR, msgs[msgs.size()-1].getSeverity());
	}
	
	static testmethod void testCancel()
	{
		// 准备数据
		User testUser1 = createUser('testuser1@example.com.test','testuser1@example.com.test', 'tu1','testuser1');
		User testUser2 = createUser('testuser2@example.com.test','testuser2@example.com.test', 'tu2','testuser2');
		User testUser3 = createUser('testuser3@example.com.test','testuser3@example.com.test', 'tu3','testuser3');
		insert new User[]{testUser1, testUser2, testUser3};
		
		ProjectHollyhigh__c testProjectc = createProject('testProjectc',testUser1.Id);
		insert testProjectc;
		
		// 执行构造函数
		ApexPages.currentPage().getParameters().put('id',testProjectc.Id);
		NoticeForRecommendationController testController = new NoticeForRecommendationController(new ApexPages.Standardcontroller(testProjectc));
		
		// 点击取消按钮
        PageReference pageRef = testController.cancel();
        
        // 验证发送结果
		System.assertEquals(true, pageRef.getRedirect());
	 	System.assertEquals(URL.getSalesforceBaseUrl().toExternalForm() + '/' + testProjectc.Id, pageRef.getUrl());
	}
}