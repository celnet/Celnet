/*
Author:Crazy
Time:2014-1-26
Function:当新建或修改客户信息时，若新客户的名称、电话、上市代码有一个和数据库中的一样，则该客户已经存在，创建失败
*/
@isTest
private class Test_AccountCheckDuplicate {

    static testMethod void accountCheckDuplicateTest() {
        
        list<Account> accList = new list<Account>();
        Account acc1 = new Account();
        acc1.Name = '雨花石科技22';
        acc1.Phone = '123';
        acc1.CorporationCode__c = '123';
        accList.add(acc1);
        
        Account acc2 = new Account();
        acc2.Name = '雨花石1111';
        acc2.Phone = '234';
        acc2.CorporationCode__c = '234';
        accList.add(acc2);
        
       // Account acc3 = new Account();
        //acc3.Name = 'yuhuashi';
        //acc3.Phone = '123';
        //acc3.CorporationCode__c = '346';
        //accList.add(acc3);
        insert accList;
        
        Account acc4 = new Account();
        acc4.Name = '北京雨花石';
        acc4.Phone = '1230';
        acc4.CorporationCode__c = '123';
        //insert acc4;
       
       try{ 
	        Account acc5 = new Account();
	        acc5.Name = '雨花石';
	        acc5.Phone = '432';
	        acc5.CorporationCode__c = '432';
	        insert acc5;
      	 }
       catch(Exception e)
       {
       	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, '错误信息：'+e.getMessage());         	
       }
	
	try{ 
	        Account acc6 = new Account();
	        acc6.Name = '雨花石有限';
	        acc6.Phone = '123';
	        acc6.CorporationCode__c = '123';
	        insert acc6;
      	 }
       catch(Exception e)
       {
       	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, '错误信息：'+e.getMessage());         	
       }        
        
      try{ 
	        Account acc7 = new Account();
	        acc7.Name = '雨花石';
	        acc7.Phone = '';
	        acc7.CorporationCode__c = '';
	        insert acc7;
      	 }
       catch(Exception e)
       {
       	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, '错误信息：'+e.getMessage());         	
       } 
       
       try{ 
	        Account acc8 = new Account();
	        acc8.Name = '雨花石';
	        acc8.Phone = '432';
	        acc8.CorporationCode__c = '';
	        insert acc8;
      	 }
       catch(Exception e)
       {
       	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, '错误信息：'+e.getMessage());         	
       } 
    }
}