/*
*功能：test Account_LeadCovertToAccCreateDiffRecordType
*作者：Crazy
*时间：2014-1-23
*/
@isTest
private class Test_Account_LeadCovertToAccCreateDiff {

    static testMethod void myUnitTest() {
    	RecordType rt = new RecordType();
        Account acc = new Account();
        acc.Name = 'cc';
        acc.AccRecordType__c = '买/借壳方';
        insert acc;
        System.assertEquals(true, acc.RecordTypeId == rt.Id);
        
        Account acc1 = new Account();
        acc1.Name = 'cc1';
        acc1.AccRecordType__c = '合作伙伴';
        insert acc1;
        System.assertEquals(true, acc1.RecordTypeId == rt.Id);
    }
}