/**
 * Author : Sunny
 * Des:行政费用支出PDF
 **/
public class AdmiExpenseFormController 
{
	private ID BudgetId ;
	public AdmiExpenseBudget__c admiBudget{get;set;}
	public List<List<AdmiExpenseItem>> list_Item {get;set;}
	public String sEexpenseTotal{get;set;}//大写金额
	public String sYear{get{return String.valueOf(date.today().year());}set;}
    public String sMonth{get{return String.valueOf(date.today().month());}set;}
    public String sDay{get{return String.valueOf(date.today().Day());}set;}
	private Map<String,Decimal> map_ItemAmount = new Map<String , Decimal>();
	private List<String> list_CostType = new List<String>();
	public AdmiExpenseFormController(Apexpages.Standardcontroller controller)
	{
		BudgetId = controller.getId();
		this.initAdmiExpense();
	}
	private void initAdmiExpense()
	{
		admiBudget = [Select Id,Name,OwnerId,InvoiceNum__c,EexpenseTotalMoney__c,(Select RecordTypeId, Approvestatus__c, CostType__c, Expenses__c, InvoiceNum__c 
				From Administrative_expenses__r Where Approvestatus__c = '审批通过') 
			FROM AdmiExpenseBudget__c where id =: BudgetId];
		sEexpenseTotal = UtilToolClass.translate(admiBudget.EexpenseTotalMoney__c);
		initItemMap();
	} 
	private void initItemMap()
	{
		Map<String, Schema.SobjectField> CostReiFields =
           Schema.SobjectType.AdmiExpense__c.fields.getMap();
        Schema.SobjectField sObjField = CostReiFields.get('CostType__c');
        List<Schema.PicklistEntry> listPicklistEntry = sObjField.getDescribe().getPicklistValues();
        for(Schema.PicklistEntry pe : listPicklistEntry)
        {
            if(pe.isActive())
            {
                Decimal d;
                map_ItemAmount.put(pe.getLabel(), d);
                list_CostType.add(pe.getLabel());
            }
        }
        Decimal dAmount ;
        for(AdmiExpense__c ae : admiBudget.Administrative_expenses__r)
        {
			if(map_ItemAmount.containsKey(ae.CostType__c))
			{
				dAmount = map_ItemAmount.get(ae.CostType__c);
			}
			dAmount = (dAmount==null?0:dAmount)+(ae.Expenses__c==null?0:ae.Expenses__c);
			map_ItemAmount.put(ae.CostType__c , dAmount);
		}
        system.debug('list cost type:'+list_CostType);
        system.debug('cost maps::'+map_ItemAmount);
        buildAdmiExpenseItem();
	}
	private void buildAdmiExpenseItem()
	{
		list_Item = new List<List<AdmiExpenseItem>>();
		List<AdmiExpenseItem> list_aei = new List<AdmiExpenseItem>();
		for(String sType : list_CostType)
		{
			AdmiExpenseItem aei = new AdmiExpenseItem(sType , map_ItemAmount.get(sType));
			list_aei.add(aei);
			if(list_aei.size() == 3)
            {
                list_Item.add(list_aei);
                list_aei = new List<AdmiExpenseItem>();
            }
		}
		system.debug('list items:sss '+list_Item);
		if(list_aei.size() < 3 && list_aei.size() > 0)
		{
			for(integer i=list_aei.size();i<3;i++)
			{
				list_aei.add(new AdmiExpenseItem('',null));
			}
			list_Item.add(list_aei);
		}
	}
	public class AdmiExpenseItem{
		public String ItemName{get;set;}
		public Decimal ItemAmount{get;set;}
		public AdmiExpenseItem(String sName , Decimal dAmonut){
			ItemName = sName;
			ItemAmount = dAmonut;
		}
	}
}