/*
 * Author: Steven
 * Date: 2014-2-10
 * Description: 测试类
 */
@isTest
private class TestReimbursementStats 
{
	static testMethod void testReimbursementStats()
	{
		CostReimbursement__c cr1 = new CostReimbursement__c();
		cr1.Year__c = '2014';
		cr1.reimbursementMonth__c = '4月';
		insert cr1;
		
		CostReimbursementDetail__c crd1 = new CostReimbursementDetail__c();
		crd1.CostAmount__c = 100;
		crd1.CostName__c = cr1.Id;
		insert crd1;
		
		cr1.ReimbursementState__c = '审批通过';
		update cr1;
		
		ProfitInfo__c[] pi1 = [Select p.Year__c, p.ReimbursementMonthly__c, p.ReceivablesMonthly__c, p.Month__c From ProfitInfo__c p Where p.Month__c =: '4月'];
		System.assertEquals(pi1[0].Year__c, cr1.Year__c);
		System.assertEquals(pi1[0].Month__c, cr1.reimbursementMonth__c);
		System.assertEquals(pi1[0].ReimbursementMonthly__c, 100);
		System.assertEquals(pi1[0].ReceivablesMonthly__c, 0);
		
		cr1.reimbursementMonth__c = '5月';
		update cr1;
	}
}