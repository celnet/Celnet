/*
 * Author: Steven
 * Date: 2014-2-17
 * Description: 测试Trigger ProjectMemberShare
 */
@isTest
private class Test_ProjectMemberShare 
{
	static testmethod void testcase()
	{
		// 准备数据
		Account testAcc1 = new Account();
		RecordType seller = [Select Id From RecordType Where SobjectType = 'Account' And DeveloperName = 'Seller'];
		testAcc1.RecordTypeId = seller.Id;
		testAcc1.Name = 'Account1Seller';
		testAcc1.Industry = '食品';
		testAcc1.IndustryType__c = '健康';
		insert testAcc1;
		
		Account testAcc2 = new Account();
		RecordType buyer = [Select Id From RecordType Where SobjectType = 'Account' And DeveloperName = 'Buyer'];
		testAcc2.RecordTypeId = buyer.Id;
		testAcc2.Name = 'Account2Buyer';
		testAcc2.Industry = '食品';
		testAcc2.IndustryType__c = '健康';
		insert testAcc2;
		
		Account testAcc3 = new Account();
		testAcc3.RecordTypeId = buyer.id;
		testAcc3.Name = 'Account3Buyer';
		testAcc3.Industry = '食品';
		testAcc3.IndustryType__c = '健康';
		insert testAcc3;
		
		ProjectHollyhigh__c testProjectc = new ProjectHollyhigh__c();
		testProjectc.Account__c = testAcc1.Id;
		RecordType projectRT = [Select Id From RecordType Where SobjectType = 'ProjectHollyhigh__c' And DeveloperName = 'Seller'];
		testProjectc.RecordTypeId = projectRT.id;
		testProjectc.Name = 'testProject';
		insert testProjectc;
		
		ProjectMember__c pm = new ProjectMember__c();
		List<User> u = [Select Id, Name, Email From User Where Name = '司徒远纵'];
		pm.GroupMember__c = u[0].id;
		pm.ProjectName__c = testProjectc.id;
		// 执行方法
		insert pm;
		// 验证结果
		
	}
}