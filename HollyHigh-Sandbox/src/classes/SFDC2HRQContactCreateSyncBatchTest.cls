@isTest
private class SFDC2HRQContactCreateSyncBatchTest {

    static testMethod void myUnitTest() {
        StructureMapData.StructureMap();

		Opportunity opp = new Opportunity();
		opp.Name = '行业';
		opp.HRQ_ID__c = '1234';
		opp.Description = '行业';
		opp.Credibility__c = '行业';
		opp.HRQ_Uploader_ID__c = '行业';
		opp.HRQ_UploadedOn__c = Datetime.now();
		opp.HRQ_Pairids__c = '行业';
		opp.HRQ_LastModifiedOn__c = Datetime.now();
		opp.StageName = 'stageName';
		opp.CloseDate = Date.today();
		
		opp.HRQ_Pairids__c = '行业';
		insert opp;
		
		Contact con =  new Contact();
		con.HRQ_ID__c = '';
		con.LastName = '123';
		con.Email = '12@qq.com';	
		con.Title = '1233';	
		con.MobilePhone = '1233';
		con.Phone = '1233';//手机2	2014-10-10
		con.HRQ_Sync_Exception__c = 'N';
		insert con;
		
		List<OpportunityContactRole> oppConList = new List<OpportunityContactRole>();
		OpportunityContactRole oppCon = new OpportunityContactRole();
		oppCon.ContactId = con.Id;
		oppCon.OpportunityId = opp.Id;
		insert oppCon;
		
			
		SFDC2HRQContactCreateSyncBatch his = new SFDC2HRQContactCreateSyncBatch();
		his.IsChain = false;
		Database.QueryLocator dq = his.start(null);
        system.test.starttest();
		system.test.setMock(HttpCalloutMock.class,new HRQRequestMock());
		String query = dq.getQuery();
		SObject[] res = Database.Query(query);
		his.execute(null,(List<Contact>)res);
    	//database.executebatch(his);
		system.test.stoptest();
		his.finish(null);
		
		//代码覆盖率为89% 2014.8.11 22:44
    }
}