/**
 * Test AutoConnectAccount
 */
@isTest
private class Test_AutoConnectAccount {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Account a = new Account();
        a.Name = 'test';
        a.Industry = '医药';
        insert a;
        
        Unresolved_Email__c ue = new Unresolved_Email__c();
        ue.Name = 'test';
        ue.Body__c = 'wef';
        insert ue;
        
        system.Test.startTest();
        ue.Account__c = a.Id ;
        update ue;
        system.Test.stopTest();
    }
}