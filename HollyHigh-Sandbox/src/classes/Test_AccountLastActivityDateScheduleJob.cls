@isTest
private class Test_AccountLastActivityDateScheduleJob {
	static testmethod void myUnitTest(){
		Account testAcc1 = new Account();
		RecordType seller = [Select Id From RecordType Where SobjectType = 'Account' And DeveloperName = 'Seller'];
		testAcc1.RecordTypeId = seller.Id;
		testAcc1.Name = 'Account1Seller';
		testAcc1.Industry = '食品';
		testAcc1.IndustryType__c = '健康';
		insert testAcc1;
		
		EmailInfo__c eic = new EmailInfo__c();
    	eic.Name = 'Test1';
    	eic.Account__c = testAcc1.Id;
    	insert eic;
    	
    	Contact con = new Contact();
    	con.LastName = 'dfasaf';
    	con.AccountId = testAcc1.Id;
    	insert con;
    	
    	Attachment att = new Attachment();
    	att.Parentid = testAcc1.Id;
    	att.Body = Blob.valueOf('test');
    	att.Name = 'test.jpg';
    	insert att;
		
		AccountLastActivityDateScheduleJob j = new AccountLastActivityDateScheduleJob();
		j.execute(null);
	}
}