/**
 * Author : Sunny
 * Des:行政费用支出PDF
 **/
public class AdministrativeCostsFormController 
{
	public AdmiExpense__c objAdmiExpense{get;set;}
	private ID admiExpenseId;
	public String sExpenses{get;set;}
	public String sYear{get{return String.valueOf(date.today().year());}set;}
    public String sMonth{get{return String.valueOf(date.today().month());}set;}
    public String sDay{get{return String.valueOf(date.today().Day());}set;}
    public String sExpensesTime{get;set;}
    public String sRemark{get;set;}
	public AdministrativeCostsFormController(Apexpages.Standardcontroller controller)
	{
		admiExpenseId = controller.getId();
		this.initAdmiExpense();
	}
	private void initAdmiExpense()
	{
		objAdmiExpense=[Select Id,OwnerDepartment__c,InvoiceNum__c,CostType__c,remark__c,Expenses__c,Expensestime__c,Createdby.Name From AdmiExpense__c Where Id =: admiExpenseId];
		if(objAdmiExpense.Expenses__c != null)
		sExpenses = UtilToolClass.translate(objAdmiExpense.Expenses__c);
		if(objAdmiExpense.Expensestime__c != null)
		sExpensesTime = objAdmiExpense.Expensestime__c.year()+'-'+objAdmiExpense.Expensestime__c.month()+'-'+objAdmiExpense.Expensestime__c.day();
		sRemark = wrapLineinner(objAdmiExpense.remark__c,20);
	}
	private String wrapLineinner(String oldStr, Integer chartNum)
    {
        if(oldStr!=null) //Brave add (If)
        {
            Integer flag = 0;
            String retStr = '';
            List<String> lineList = oldStr.split('\n');
            for(Integer i=0;i<lineList.size(); i++){
                    String lineStr = lineList[i];
                    while(lineStr.length() > chartNum){
                             retStr += lineStr.substring(0,chartNum) + '<br/>';
                             lineStr = lineStr.substring(chartNum);
                    }
                    if(lineStr.length() > 0){
                             retStr += lineStr + '<br/>';
                    }
            }
            return retStr;
        }else
        {
            return '';
        }
    }
}