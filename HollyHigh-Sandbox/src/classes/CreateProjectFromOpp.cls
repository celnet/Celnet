/*
*功能：点击业务机会上的按钮“生成项目”进入该类的createProject方法，进行生成对应的项目
*作者：Alisa
*时间：2013/12/18
*条件：卖方业务机会生成卖方项目，买方业务机会生成买方项目
*Tommy Modify at 2014-1-13: 
*1,拿单项目和做单项目的记录类型的DeveloperName有改变，所以修改代码
*2,另外对代码结构作了调整
*/
global without sharing class CreateProjectFromOpp 
{
	webservice static String createProject(Id opportunityId)
	{
		//获取点击这个按钮的业务机会
		Opportunity  opp = [Select Name, 
								StageName, 
								RecordTypeId, 
								RecordType.Id,
								RecordType.DeveloperName,
								OwnerId, IsDeleted, 
								IsClosed, 
								Id, 
								CloseDate, 
								AccountId,
								(Select 
									Id
								From Project_Opptunity__r),
								(Select 
									Id, 
									Project__c
								From ResourceRecommends__r)
							From Opportunity
							Where Id = :opportunityId];
		 					
		//list<ProjectHollyhigh__c> listProjects = new list<ProjectHollyhigh__c>();
		
		//查该业务机会下是否已创建项目	
		//listProjects = [Select RecordTypeId, OwnerId, Opportunity__c, Name, Id, Account__c 
		//				From ProjectHollyhigh__c p
		//				Where Opportunity__c = :opportunityId];
		if(opp.Project_Opptunity__r != null && opp.Project_Opptunity__r.size() > 0)
		{
			return '当前拿单项目已经存在做单项目，无法生成新的做单项目';
		}
		else
		{
			//点击这个按钮的业务机会的记录类型
			RecordType oppRT = opp.RecordType;
			if(opp.RecordType == null || (opp.RecordType.DeveloperName != 'Seller' && opp.RecordType.DeveloperName != 'Buyer'))
			{
				return '无法识别拿单项目的记录类型，请联系系统管理员';
			}
			map<String, RecordType> map_ProjRt = new map<String, RecordType>();
			for(RecordType r : [Select Id, DeveloperName From RecordType Where SObjectType = 'ProjectHollyhigh__c'])
			{
				map_ProjRt.put(r.DeveloperName, r);
			}
			if(map_ProjRt.size() == 0)
			{
				return '无法识别做单项目的记录类型，请联系系统管理员';
			}
			
			String projRtDevName;
			if(opp.RecordType.DeveloperName == 'Seller')
			{
				projRtDevName = 'Seller';
			}
			else if (opp.RecordType.DeveloperName == 'Buyer')
			{
				projRtDevName = 'Buyer';
			}
			RecordType projRt = map_ProjRt.get(opp.RecordType.DeveloperName);
			if(projRt == null)
			{
				return '无法识别做单项目的记录类型，请联系系统管理员';
			}
			
			try
			{
				//创建做单项目
				ProjectHollyhigh__c pro = new ProjectHollyhigh__c();
				pro.Name = opp.Name;
				pro.Opportunity__c = opp.Id;
				pro.Account__c = opp.AccountId;
				pro.RecordTypeId = projRt.Id;
				insert pro;
				
				//将现存的推荐关联到做单项目
				if(opp.ResourceRecommends__r != null && opp.ResourceRecommends__r.size() >0)
				{
					for(ResourceRecommend__c re : opp.ResourceRecommends__r)
					{
						re.Project__c = pro.Id;
					}
				}
				update opp.ResourceRecommends__r;
				return 'ok'+pro.Id;
			}
			catch(Exception e)
			{
				return '系统错误：' + e.getMessage();
			}
		}								
		
	}
}