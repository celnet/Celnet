/*
 *Author:Leo
 *Date:2014-03-26
 *fuction:控制Account_List页
 */
public without sharing class Account_List { 
	public string query;
    public integer result{get;set;}
    
    public Account account{get;set;}
    
    public List<Account> acc 
    {
        get
        {
            list<Account> acc = new list<Account>();
            for(Account c : (list<Account>)conset.getRecords())
            {
              acc.add(c);
            }  
            result = acc.size();
            return acc;    
        }
    }
    
	public Account_List(ApexPages.standardController controller)
	{
		account = new Account();
		Id currentID = UserInfo.getUserId();
		User currentUser = [Select Id, Department, Name From User where Id=:currentID];
		
		string currentDepartment = currentUser.Department;
		
        query = 'Select Owner.Department,Owner.Name, Name, Industry, Id, RecordTypeId, RecordType.Name From Account';
		
		conset = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        conset.setPageSize(25);
		
	}
    
    public PageReference Search()
    {
    	if(this.account.industry==null )
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '请选择行业信息');         
       		ApexPages.addMessage(msg);
       		return null;		
		}
		
    	if(this.account.IndustryType__c == null)
    	{
    		query = 'Select Owner.Department,Owner.Name, Name, Industry, Id, RecordTypeId, RecordType.Name From Account Where RecordTypeId = \'' + this.account.RecordTypeId + '\' And Industry = \'' + this.account.industry + '\'';
    		conset = new ApexPages.StandardSetController(Database.getQueryLocator(query));
            conset.setPageSize(25);
    	}
    	
    	if(this.account.IndustryType__c != null)
    	{
    		query = 'Select Owner.Department,Owner.Name, Name, Industry, Id, RecordTypeId, RecordType.Name From Account Where RecordTypeId = \'' + this.account.RecordTypeId + '\' And Industry = \'' + this.account.industry + '\' And IndustryType__c = \'' + this.account.IndustryType__c + '\'';
    		conset = new ApexPages.StandardSetController(Database.getQueryLocator(query));
            conset.setPageSize(25);
    	}
    	
    	if(conset.getRecords().size() <= 0)
		{
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, '按照您设置的条件，没有找到任何客户，请合理设置条件后重新搜索');         
   			ApexPages.addMessage(msg);
		}
    	
    	return null;
    }
    
    public ApexPages.StandardSetController conset{get;set;}
    
    /*
    {
        get 
        {
          if(conset == null) 
          {
            conset = new ApexPages.StandardSetController(Database.getQueryLocator(query));
            conset.setPageSize(25);
          }
          return conset;
        }
        set;
  	}
  	*/
  
    
      //public integer result{get;set;}
      
        
      public Boolean hasPrevious 
      {
        get 
        {
          return conset.getHasPrevious();
        }
        set;
      }
      
      public Boolean hasNext 
      {
        get 
        {
          return conset.getHasNext();
        }
        set;
        }
    
        // returns the page number of the current page set
      public Integer pageNumber 
      {
        get 
        {
          return conset.getPageNumber();
        }
        set;
      }
        
    
       // returns the first page of records
      public void first() 
      {
        conset.first();
      }
    
       // returns the last page of records
      public void last() 
      {
        conset.last();
      }
    
       // returns the previous page of records
      public void previous() 
      {
        conset.previous();
      }
    
       // returns the next page of records
      public void next() 
      {
        conset.next();
      }
       //returns the number of 
      public Integer categoryNumber 
      {
        get 
        {
            Integer pageNumbers = conset.getResultSize();
            if(Math.mod(pageNumbers, 25)== 0)
            {
            	return pageNumbers/25;
            }
            else
            {
            	return (conset.getResultSize() / 25) + 1;
            }
          
        }
        set;
       }
    
    public Integer recordNumber
    {
    	get
        {
        	return conset.getResultSize();
        }
    }
	
	public string currentRecord
	{
		get
		{
			Integer currentPage = conset.getPageNumber();
			Integer fromNumber = currentPage *25 - 24;
			Integer toNumber;
			if(currentPage != categoryNumber)
			{//如果当前页不是最后一页
				toNumber = currentPage * 25;
			}
			else
			{
				toNumber =  conset.getResultSize();
			}
			return  fromNumber +'-'+ toNumber;
		}
	}
}