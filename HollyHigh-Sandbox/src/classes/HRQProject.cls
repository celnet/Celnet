/*
作者：winter
创建时间：2014-07-16
功能：HRQProject，调用HRQService解析XML获得List<HRQProject>
*/
public class HRQProject 
{
	public string Id {get; set;}
	public string Title {get; set;}
	public string Wanttype {get; set;}
	public string Wantdo {get; set;}
	public string Credibility {get; set;}
	public string IndustryId {get; set;}
	public string AreaId {get; set;}
	public string Address {get; set;}
	public string MainBusiness {get; set;}
	public string TotalAssets {get; set;}
	public string NetAssets {get; set;}
	public string Businessincome {get; set;}
	public string NetProfit {get; set;}
	public string Content {get; set;}
	public string UploaderId {get; set;}
	public HRQMember Uploader {get; set;}
	public string UploadedOn {get; set;}
	public string Pairids {get; set;}
	public string OtherInfo {get; set;}
	public List<HRQRelatedContact> RelatedContacts {get; set;}
}