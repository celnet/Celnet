/*
作者：winter
创建时间：2014-07-27
功能：SFDC2HRQContactUpdateSyncBatch测试类
*/
@isTest
private class SFDC2HRQContactUpdateSyncBatchTest {

    static testMethod void myUnitTest() {
       	StructureMapData.StructureMap();
		List<Contact> conList = new List<Contact>();
		
		Contact con = new Contact();
		con.LastName = '这是姓名';
		con.HRQ_ID__c = '123';
		con.HRQ_Project_ID__c = '1213121';
		con.LastName__c = '这是名称';
		con.Title = '经理';
		con.MobilePhone = '123123';
		con.Phone = '123123';//2014-10-10	修改：phone（电话（手机2））和mobilephone调换（手机）	winter
		con.Email = '123@123.com';
		con.HRQ_Uploader_ID__c = '123';
		con.HRQ_UploadedOn__c = Datetime.now();
		con.HRQ_Sync_Exception__c = 'N';
		conList.add(con);
		
		insert conList;

        system.test.starttest();
        system.test.setMock(HttpCalloutMock.class,new HRQRequestMock());
		SFDC2HRQContactUpdateSyncBatch batch = new SFDC2HRQContactUpdateSyncBatch();
		batch.IsChain = false; 
		Database.Querylocator dq = batch.start(null);
		string query = dq.getQuery();
		SObject[] res = Database.Query(query);
		batch.execute(null,(List<Contact>)res);
		batch.finish(null);
		system.test.stoptest();
		//代码覆盖率为46% 2014.8.11 22:59
    }
}