/*
作者：Tommy Liu
电子油箱：tommyliu@cideatech.com
创建时间：2014-07-08
功能：推送有变化的项目到华尔圈
同步方向：SFDC到华尔圈
*/

global class SFDC2HRQProjectUpdateSyncBatch extends SFDC2HRQDataSyncBatch
{  
	private HRQProject[] projectsList;
	private Map<string,string> hrqWantOppMap;
	global SFDC2HRQProjectUpdateSyncBatch()
	{
		super();
		
		//需求类型映射
		this.hrqWantOppMap = new Map<String,String>();
        for(HRQ_WantType_Mapping__c hwm : [Select Id, 
                                                SFDC_Object_Name__c,
                                                SFDC_RecordType_Developer_Name__c,
                                                HRQ_WantType__c 
                                            From HRQ_WantType_Mapping__c
                                            Where SFDC_Object_Name__c = 'Opportunity'
                                            And Primary__c = true])
        {
            HRQWantOppMap.put(hwm.SFDC_RecordType_Developer_Name__c, hwm.HRQ_WantType__c);
        }
	}
	global override Database.QueryLocator getDataScop()
	{
		
		if(syncSetting.SFDC2HRQ_Update_By_History__c)
		{
			//添加了查询记录类型
			//查询“是否排除”不等于Y的opportunity数据，不将重复数据更新到HRQ 2014.8.11	by winter
			return Database.getQueryLocator([Select Id,HRQ_ID__c,Name,RecordType.DeveloperName,Description,Credibility__c,HRQ_Uploader_ID__c,HRQ_UploadedOn__c,HRQ_Pairids__c,HRQ_LastModifiedOn__c,HRQ_Sync_Exception__c,	
											(Select Id, OpportunityId, 	Field, 	OldValue, NewValue, CreatedById, CreatedDate From Histories where CreatedDate >=: syncSetting.SFDC2HRQ_Last_Synced_StartOn__c)
											From Opportunity 	
											Where ID In
											(Select OpportunityId From OpportunityFieldHistory Where CreatedDate >=:syncSetting.SFDC2HRQ_Last_Synced_StartOn__c )
											And HRQ_ID__c != null And HRQ_Sync_Exception__c != 'Y']);
		}else
		{
			return Database.getQueryLocator([Select Id, HRQ_ID__c,Name,Description,RecordType.DeveloperName,Credibility__c,HRQ_Uploader_ID__c,HRQ_UploadedOn__c,HRQ_Pairids__c,HRQ_LastModifiedOn__c,HRQ_Sync_Exception__c
											 From Opportunity 
											 Where HRQ_ID__c != null And HRQ_Sync_Exception__c != 'Y']);
		}
	}

	
	
	global override void convertData(List<SObject> scope)
	{
		this.projectsList = new List<HRQProject>();
		system.debug(syncSetting.SFDC2HRQ_Update_By_History__c + '   映射表的值convertData！！！！');
		
		if(syncSetting.SFDC2HRQ_Update_By_History__c)
		{
			System.debug('______________按照历史记录更新');
			for(Opportunity opp : (List<Opportunity>)scope)
			{
				HRQProject pro = new HRQProject();
				pro.Id = opp.HRQ_ID__c;
				if(isFieldInHistories('Name', opp.Histories))
				{
					pro.Title = opp.Name;
				}
				if(isFieldInHistories('RecordType', opp.Histories))
				{
					pro.Wanttype = hrqWantOppMap.get(opp.RecordType.DeveloperName);
				}
				if(isFieldInHistories('Description', opp.Histories))
				{
					pro.Wantdo = opp.Description;
				}
			    if(isFieldInHistories('Credibility__c', opp.Histories))
				{
					pro.Credibility = opp.Credibility__c;
				}
				/*
				if(isFieldInHistories('HRQ_Uploader__ID__c', opp.Histories))
				{
					pro.UploaderId =  opp.HRQ_Uploader_ID__c;
				}
				if(isFieldInHistories('HRQ_UploadedOn__c', opp.Histories))
				{
					pro.UploadedOn = string.valueOf(opp.HRQ_UploadedOn__c);
				}
				if(isFieldInHistories('HRQ_Pairids__c', opp.Histories))
				{
					pro.Pairids = opp.HRQ_Pairids__c;
				}
				*/
				opp.HRQ_LastModifiedOn__c = DateTime.now();
				system.debug(opp.Name + '    +++++++++Opp  Name   ');
				system.debug(pro.Title + '    +++++++++pro  title   ');
				projectsList.add(pro);
			}
			
		}else
		{
			System.debug('______________不按照历史记录更新');
			for(Opportunity opp : (List<Opportunity>)scope)
			{
				HRQProject pro = new HRQProject();
				pro.Id = opp.HRQ_ID__c;
				pro.Title = opp.Name;
				//处理记录类型
				pro.Wanttype = hrqWantOppMap.get(opp.RecordType.DeveloperName);
				pro.Wantdo = opp.Description;
				pro.Credibility = opp.Credibility__c;
				//pro.UploaderId =  opp.HRQ_Uploader_ID__c;
				//pro.UploadedOn = string.valueOf(opp.HRQ_UploadedOn__c);
				//pro.Pairids = opp.HRQ_Pairids__c;
				opp.HRQ_LastModifiedOn__c = DateTime.now();
				system.debug('**********************不按历史更新！！！');
				projectsList.add(pro);
			}
		}
	}
	
	private Boolean isFieldInHistories(String Field, List<OpportunityFieldHistory> histories)
	{
		if(histories == null)
		{
			return false;
		}
		for(OpportunityFieldHistory h : histories)
		{
			if(h.Field.equalsIgnoreCase(Field))
			{
				return true;
			}
		}
		return false;
	}
	
	global override void pushDataToHRQ()
	{
		this.hrqService.UpdateProject(projectsList);
	}
	
	global override void handleNext()
	{
		this.syncSetting.SFDC2HRQ_Last_Synced_StartOn__c = this.currentTime;
    	update this.syncSetting;
    	system.debug(this.syncSetting.SFDC2HRQ_Last_Synced_StartOn__c + '      start time !!');
		this.syncSetting.SFDC2HRQ_Last_Synced_EndOn__c = Datetime.now();
		update syncSetting;
		if(this.IsChain)
        {
           HRQ2SFDCProjectSyncBatch nextBatch = new HRQ2SFDCProjectSyncBatch(0, this.BatchSize);
           nextBatch.IsChain = this.IsChain;
           database.executeBatch(nextBatch);
        }
	}
}