/*
	Author: crazy ma
* 	Created On: 2014-1-15
* 	Function:Test BuySellRecommendaionController
*/
@isTest
private class Test_BuySellRecommendationController {

   static testMethod void testPositiveCase()
   { 
   	//准备数据
   		Account acc1 = new Account();
   		acc1.Name = 'cideatech';
   		acc1.RecordTypeId = [select id from RecordType where DeveloperName=:'Seller' and SobjectType=:'Account'].id;
   		acc1.IndustryType__c = '健康';
	    acc1.industry= '食品';
	    insert acc1;
 
   		Account acc2 = new Account();
   		acc2.Name = '雨花石2';
   		acc2.RecordTypeId = [select id from RecordType where DeveloperName=:'Buyer' and SobjectType=:'Account'].id;
   		acc2.IndustryType__c = '健康';
	    acc2.industry= '食品';
	    insert acc2;
	    
	    Account acc3 = new Account();
	    acc3.Name = '雨花石3';
   		acc3.RecordTypeId = [select id from RecordType where DeveloperName=:'Buyer' and SobjectType=:'Account'].id;
   		acc3.IndustryType__c = '健康';
	    acc3.industry= '食品';
	    insert acc3;
	    
   		Opportunity opp = new Opportunity();
   		opp.AccountId = acc1.Id;
   		opp.Name = '雨花石';
   		opp.RecordTypeId = [select id from RecordType where DeveloperName=:'Seller' and SobjectType=:'Opportunity'].id;
   		opp.StageName ='创新阶段';
   		opp.CloseDate = Date.today();
   		insert opp;
   		
   		//执行构造函数及设置默认值
   		ApexPages.currentPage().getParameters().put('id',opp.Id);
	    BuySellRecommendationController bs = new BuySellRecommendationController(new ApexPages.StandardController(opp));
	 	
	 	//验证拿单项目及客户信息
	 	System.assertEquals('雨花石',bs.opp.Name);
	 	System.assertEquals('cideatech',bs.acc.Name);
	 	//修改为'卖/融资方/卖壳方'（正式系统改动）	by winter 2014-8-29
	 	System.assertEquals('卖方/融资方/卖壳方',bs.acc.RecordType.Name);
	 	
	 	//验证推荐客户方及行业信息
	 	System.assertEquals([select id from RecordType where DeveloperName=:'Buyer' and SobjectType=:'Account'].Id, bs.acc.RecordTypeId);
	 	System.assertEquals('食品',bs.acc.industry);
	    System.assertEquals('健康',bs.acc.IndustryType__c );
	    
	    //调用搜索方法
	    bs.Search();
	    
	    List<BuySellRecommendationController.AccountPack> accPacks = bs.targetAccountList;
	    System.assertEquals(2, accPacks.size());
	    System.assertEquals(acc2.Id,accPacks[0].acc.Id);
	    System.assertEquals(acc3.Id,accPacks[1].acc.Id);

	    //勾选客户2
   		accPacks[0].isCheck = true;
	    
	    //调用确认方法
	 	PageReference pageRef = bs.Sure();
	 	
	 	bs.first();
	 	bs.last();
	 	bs.previous();
	 	bs.next();
	 	Integer test_cn = bs.categoryNumber;
	 	Integer test_tp = bs.totalPageNumber;
	 	Boolean test_hp = bs.hasPrevious;
	 	Boolean test_hn = bs.hasNext;
	 	Integer test_pn = bs.pageNumber;
	 	
	 	//验证推荐的客户信息
	 	ID recommRecordTypeId = [select id from RecordType where DeveloperName=:'Buyer' and SobjectType=:'ResourceRecommend__c'].Id;
	 	List<ResourceRecommend__c> recommends = [select 
	 												Id
	 											from ResourceRecommend__c
	 											Where Account__c =: acc1.Id
	 											And Opportunity__c =: opp.Id
	 											And Target_Account__c =: acc2.Id
	 											And RecordTypeId =: recommRecordTypeId];
	 	//验证URL地址
	 	System.assertEquals(1, recommends.size());
	 	System.assertEquals(true, pageRef.getRedirect());
	 	System.assertEquals(URL.getSalesforceBaseUrl().toExternalForm() + '/' + opp.Id, pageRef.getUrl());
   }
   
  static testMethod void testNegativeCase()
  {
  	//准备数据
   		Account acc1 = new Account();
   		acc1.Name = '雨花石1';
   		acc1.IndustryType__c = '健康';
	    acc1.industry= '食品';
   		acc1.RecordTypeId = [select id from RecordType where DeveloperName=:'Seller' and SobjectType=:'Account'].id;
	    insert acc1;
	    
   		Opportunity opp = new Opportunity();
   		opp.AccountId = acc1.Id;
   		opp.Name = '雨花石';
   		opp.RecordTypeId = [select id from RecordType where DeveloperName=:'Seller' and SobjectType=:'Opportunity'].id;
   		opp.StageName ='创新阶段';
   		opp.CloseDate = Date.today();
   		insert opp;
   		
   		//执行程序
   		ApexPages.currentPage().getParameters().put('id',opp.Id);
   		BuySellRecommendationController bs = new BuySellRecommendationController(new ApexPages.StandardController(opp));
	    
	    bs.Search();//预期找不到数据
	   	List<BuySellRecommendationController.AccountPack> accPacks = bs.targetAccountList;
	    System.assertEquals(0, accPacks.size());
	    ApexPages.Message notFoundMsg;
	    List<ApexPages.Message> msgs = ApexPages.getMessages();
	    System.assertEquals('按照您设置的条件，没有找到任何可推荐的客户，请合理设置条件后重新搜索', msgs[msgs.size()-1].getSummary());
	    System.assertEquals(ApexPages.Severity.INFO, msgs[msgs.size()-1].getSeverity());
	    
	    PageReference pageRef = bs.Cancel();
	   	System.assertEquals(true, pageRef.getRedirect());
	 	System.assertEquals(URL.getSalesforceBaseUrl().toExternalForm() + '/' + opp.Id, pageRef.getUrl());
  }
}