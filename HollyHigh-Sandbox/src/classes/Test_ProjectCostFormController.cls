/**
 * 测试ProjectCostFormController和AdministrativeCostsFormController
 */
@isTest
private class Test_ProjectCostFormController {

    static testMethod void testProjectCostFormController() {
        //Account
        Account acc = new Account();
        acc.Name = 'Test acc';
        insert acc;
        //Opp
        Opportunity opp = new Opportunity();
        opp.Name = 'test opp';
        opp.AccountId = acc.Id;
        opp.CloseDate = date.today().addMonths(2);
        opp.StageName = '其他';
        insert opp;
        //ProjectHollyhigh__c
        ProjectHollyhigh__c projectHo = new ProjectHollyhigh__c();
        projectHo.Account__c = acc.Id;
        projectHo.Name = 'test pro';
        projectHo.ProjectState__c = '意向书';
        projectHo.Opportunity__c = opp.Id;
        projectHo.ProjectCode__c = 'wefwe';
        insert projectHo;
        //ProjectCost__c
        ProjectCost__c pc = new ProjectCost__c();
        pc.Account_Name__c = acc.Id;
        pc.ProjectCostType__c = '拿单提成';
        pc.ExpendMoney__c = 12323;
        pc.ProjectName__c = projectHo.Id;
        insert pc;
        
        system.Test.startTest();
        Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(pc);
        ProjectCostFormController pcfc = new ProjectCostFormController(controller);
        system.Test.stopTest();
    }
    static testMethod void testAdministrativeCostsFormController() {
    	//AdmiExpenseBudget__c
    	AdmiExpenseBudget__c aeb = new AdmiExpenseBudget__c();
    	aeb.Name = 'test aeb';
    	insert aeb;
    	//AdmiExpense__c
    	AdmiExpense__c ae = new AdmiExpense__c();
    	ae.Administrative_expenses__c = aeb.Id;
    	ae.remark__c = 'sfdsfeijoeijfowiejfowijeofiwjefneroinfkernflkenlkrnfe';
    	ae.InvoiceNum__c = 2;
    	ae.CostType__c = '工资';
    	ae.Expenses__c = 2345;
    	ae.Expensestime__c = date.today();
    	insert ae;
    	system.Test.startTest();
    	Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(ae);
        AdministrativeCostsFormController acfc = new AdministrativeCostsFormController(controller);
    	system.Test.stopTest();
    }
}