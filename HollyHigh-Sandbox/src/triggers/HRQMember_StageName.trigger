/*
作者：Peter
创建时间：2014-07-23
功能：同步HRQProject到SF的业务机会,华尔圈没有StageName和closeDate字段 这两个字段在sf是必填的  只要是从华尔圈过来的 就进行赋值。
同步方向：华尔圈到SFDC
*/
trigger HRQMember_StageName on Opportunity (before insert) 
{
	for(Opportunity opp : trigger.new)
	{
		if(opp.HRQ_ID__c != null)
		{
			opp.StageName = '华尔圈情报';
			opp.CloseDate = Date.today();
		}
	}
	
	
}