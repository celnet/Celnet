/*
*Author: Tommy Liu
*Created On: 2014-1-21
*Function: 在费用报销创建时，自动设置月份和年份，按当前时间
*/
trigger CostMonthYear on CostReimbursement__c (before insert) 
{
	Date thisDay = Date.today();
	Integer year = thisDay.year();
	Integer month = thisDay.month();
	for(CostReimbursement__c cost : trigger.new)
	{
		cost.Year__c = year + '';
		cost.reimbursementMonth__c = month + '月';
	}
}