/*
作者：winter
创建时间：2014-08-25
功能：HRQ2SFDC创建account的时候，将“客户来源”设置为华尔圈
同步方向：华尔圈到SFDC
*/
trigger HRQAccount_AccountSource on Account (before insert) 
{
	for(Account acc : trigger.new)
	{
		if(acc.HRQ_ID__c != null)
		{
			acc.AccountSource = '华尔圈';
		}
	}
}