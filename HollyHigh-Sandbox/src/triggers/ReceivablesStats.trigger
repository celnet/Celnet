/*
 * Author: Steven
 * Date: 2014-2-8
 * Description: 统计收款到利润信息中
 */
trigger ReceivablesStats on Collection__c (after delete, after update, after insert) 
{
    if(trigger.old != null)
    {
        for(Collection__c cc : trigger.old)
        {
            // 判断实际收款日期是否为空
            if(cc.ActualDate__c == null)
            {
                continue;
            }
            
            // 若不为空, 获取该条信息的年月
            Integer yyyy = cc.ActualDate__c.year();
            Integer mm = cc.ActualDate__c.month();
            
            String year = String.valueOf(cc.ActualDate__c.year());
            String month = cc.ActualDate__c.month() + '月';
            
            // 查询出该月份的所有收款信息
            Date firstdate = cc.ActualDate__c.toStartOfMonth();
            Integer numberDays = Date.daysInMonth(yyyy, mm);
            Date lastdate = firstdate.addDays(numberDays - 1);
            
            Collection__c[] c = new List<Collection__c>();
            c = [Select ActualAmount__c From Collection__c Where ActualDate__c >=: firstdate And ActualDate__c <=: lastdate];
            
            // 统计该月份的收款总金额
            Double amount = 0;
            if(c.size() > 0)
            {
                for(Collection__c co : c)
                {
                    if(co.ActualAmount__c == null)
                    {
                        co.ActualAmount__c = 0;
                    }
                    amount += co.ActualAmount__c;
                }
            }
        
            // 根据该年月查找利润信息
            ProfitInfo__c[] pi = [Select ReceivablesMonthly__c From ProfitInfo__c Where Year__c =: year And Month__c =: month limit 1];
            
            // 若没有该月份的利润信息, 则新创建一条 insert
            /*
            if(pi.size() == 0)
            {
                ProfitInfo__c p = new ProfitInfo__c();
                p.Year__c = year;
                p.Month__c = month;
                p.ReceivablesMonthly__c = amount;
                p.ReimbursementMonthly__c = 0;
                User u = [Select Name, Id From User Where UserRole.Name = '财务经理' limit 1];
                p.OwnerId = u.Id;
                insert p;
            }
            */
            // 若有该月份的利润信息, 则把该月份的收款金额赋值 update
            //else
            //{
                pi[0].ReceivablesMonthly__c = amount;
                update pi[0];
            //}
        }
    }
    if(trigger.new != null)
    {
        for(Collection__c cc : trigger.new)
        {
            // 判断实际收款日期是否为空
            if(cc.ActualDate__c == null)
            {
                continue;
            }
            
            // 若不为空, 获取该条信息的年月
            Integer yyyy = cc.ActualDate__c.year();
            Integer mm = cc.ActualDate__c.month();
            
            String year = String.valueOf(cc.ActualDate__c.year());
            String month = cc.ActualDate__c.month() + '月';
            
            // 查询出该月份的所有收款信息
            Date firstdate = cc.ActualDate__c.toStartOfMonth();
            Integer numberDays = Date.daysInMonth(yyyy, mm);
            Date lastdate = firstdate.addDays(numberDays - 1);
            
            Collection__c[] c = new List<Collection__c>();
            c = [Select ActualAmount__c From Collection__c Where ActualDate__c >=: firstdate And ActualDate__c <=: lastdate];
            
            // 统计该月份的收款总金额
            Double amount = 0;
            if(c.size() > 0)
            {
                for(Collection__c co : c)
                {
                    if(co.ActualAmount__c == null)
                    {
                        co.ActualAmount__c = 0;
                    }
                    amount += co.ActualAmount__c;
                }
            }
            
            // 根据该年月查找利润信息
            ProfitInfo__c[] pi = [Select ReceivablesMonthly__c From ProfitInfo__c Where Year__c =: year And Month__c =: month limit 1];
            
            // 若没有该月份的利润信息, 则新创建一条 insert
            if(pi.size() == 0)
            {
                ProfitInfo__c p = new ProfitInfo__c();
                p.Year__c = year;
                p.Month__c = month;
                p.ReceivablesMonthly__c = amount;
                p.ReimbursementMonthly__c = 0;
                User u = [Select Name, Id From User Where UserRole.Name = '财务经理' limit 1];
                p.OwnerId = u.Id;
                insert p;
            }
            
            // 若有该月份的利润信息, 则把该月份的收款金额赋值 update
            else
            {
                pi[0].ReceivablesMonthly__c = amount;
                update pi[0];
            }
        }
    }
}