/*
 * Author: Steven Ke
 * Date: 2014-2-17
 * Description: 创建拿单项目小组成员时发送邮件及任务提醒
 */
trigger OpportunityMemberNotify on OpportunityTeamMember (after insert, after update, after delete) 
{
	if(trigger.old != null)
	{
		for(OpportunityTeamMember otm : trigger.old)
		{
			Opportunity o = [Select Name, AccountId From Opportunity Where Id =: otm.OpportunityId];
			List<EmailInfo__c> existEIList = [Select Id From EmailInfo__c Where Account__c =: o.AccountId And OwnerId !=: otm.UserId];
			EmailInfo__Share[] eisList = [Select 
												ParentId, 
												AccessLevel, 
												UserOrGroupId 
											From EmailInfo__Share 
											Where UserOrGroupId =: otm.UserId
									 		And ParentId IN: existEIList];
			if(eisList.size() > 0)
			delete eisList;
		}
	}
	
	if(trigger.new != null)
	{
		List<Task> taskList = new List<Task>();
	    Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
	    String[] toAddresses = new List<String>();
	    
	    List<EmailInfo__c> eiList = new List<EmailInfo__c>();
	    List<EmailInfo__c> updateEIList = new List<EmailInfo__c>();
	    List<EmailInfo__Share> eisList = new List<EmailInfo__Share>();
	    List<EmailInfo__Share> existEISList = new List<EmailInfo__Share>();
	    
	    /*=========================================Scott添加 begin================*/
	    Set<Id> Set_Temporary = new Set<Id>();
	    /*=========================================Scott添加 end==================*/
	    /*
	     *报错原因：相同的EmailInfo__c记录被加入到updateEIList集合，导致update 时报错Duplicate id in list:
	     *注意：FOR中不能写SQL 
	    */
	    for(OpportunityTeamMember otm : trigger.new)
	    {
	        User u1 = [Select Email From User Where Id =: otm.UserId];
	        Opportunity o1 = [Select Name,AccountId From Opportunity Where Id =: otm.OpportunityId];
	        
	        existEISList = [Select ParentId,Parent.OwnerId From EmailInfo__Share Where UserOrGroupId =: otm.UserId And Parent.Account__c =: o1.AccountId];
	        Set<Id> idSet = new Set<Id>();
	        
	        if(existEISList.size() > 0)
	        {
	        	for(EmailInfo__Share eis : existEISList)
	        	{
	        		idSet.add(eis.ParentId);
	        	}
	        }
	        
	        eiList = [Select Id,OwnerId From EmailInfo__c Where Account__c =: o1.AccountId And Id Not IN: idSet];
	        
	        for(EmailInfo__c ei : eiList)
	        {
	        	if(otm.UserId != ei.OwnerId)
	        	{
	        		/*=Scott注释 begin=*/
	        		//EmailInfo__c einfo = new EmailInfo__c();
	        		//einfo.Id = ei.Id;
	        		//einfo.isApexShared__c = true;
	        		//updateEIList.add(einfo);
	        		
	        		//EmailInfo__Share e = new EmailInfo__Share();
	        		//e.ParentId = ei.Id;
	        		//e.AccessLevel = 'Read';
	        		//e.UserOrGroupId = otm.UserId;
	        		//eisList.add(e);
	        		/*=Scott注释  end=*/
	        		
	        		/*=Scott添加 begin=*/
	        		if(!Set_Temporary.contains(ei.Id))
	        		{
	        			Set_Temporary.add(ei.Id);
	        			ei.isApexShared__c = true;
	        			updateEIList.add(ei);
	        			
	        			
	        			EmailInfo__Share e = new EmailInfo__Share();
	        			e.ParentId = ei.Id;
	        			e.AccessLevel = 'Read';
	        			e.UserOrGroupId = otm.UserId;
	        			eisList.add(e);
	        		}
	        		/*=Scott添加 end=*/
	        		
	        		
	        		
	        	}
	        }
	        
	        toAddresses.add(u1.Email);
	        String content = '现有拿单项目"' + o1.Name + '"需要您的关注。';
	        content+= '以下是项目链接: ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + otm.OpportunityId;
	        mail.setPlainTextBody(content);
	        mail.setSubject('拿单项目关注');
	        
	        Task t1 = new Task();
	        t1.OwnerId = otm.UserId;
	        t1.WhatId = o1.AccountId;
	        t1.Description = '现有拿单项目"' + o1.Name + '"需要您的关注。';
	        t1.ActivityDate = Date.today();
	        t1.ActivityDateTime__c = Datetime.now();
	        t1.Subject = '请关注“' + o1.Name + '” 项目';
	        t1.IsReminderSet = true;
			t1.ReminderDateTime = Datetime.newInstance(Date.today().year(), Date.today().month(), Date.today().day(), 8, 0, 0);
	        
	        taskList.add(t1);
	    }
	    
	    mail.setToAddresses(toAddresses);
	    Messaging.sendEmail(new Messaging.Singleemailmessage[]{mail});
	    insert taskList;
	    
	    if(eisList.size() > 0)
	    {
	    	insert eisList;
	    }
	    
	    if(updateEIList.size() > 0)
	    {
	    	update updateEIList;
	    }
	}
    
}