/*
*功能：当潜在客户转客户时根据客户上的“#客户记录类型”判断该客户属于那种记录类型的客户
*作者：Alisa
*时间：2013/12/17
*/
trigger Account_LeadCovertToAccCreateDiffRecordType on Account (before insert)  
{
    list<Account> listAcc = new list<Account>();//存放要插入的客户
    
    //获得客户的所有记录类型n 买/借壳 卖/融资 其他
    list<RecordType> listAccRecordType = [select Id,Name,SobjectType 
                                          from RecordType 
                                          where SobjectType = 'Account'];
                                          
    map<String,RecordType> mapNameAndRecordType = new map<String,RecordType>();
    
    //把客户的所有记录类型的Name和对应的记录类型放入Map----mapNameAndRecordType中                           
    if(listAccRecordType != null && listAccRecordType.size()>0)
    {
        for(RecordType accRT : listAccRecordType)
        {
            if(!mapNameAndRecordType.containsKey(accRT.Name))
            {
                mapNameAndRecordType.put(accRT.Name,accRT);
            }
        }    
    }
    
    for(Account acc : trigger.new)
    {
        //获取“#客户记录类型”的值
        String strAccRecordTypeName = acc.AccRecordType__c;
        if(strAccRecordTypeName != null)
        {
            //根据“#客户记录类型”的值获得对应的记录类型
            if(mapNameAndRecordType != null && mapNameAndRecordType.size()>0 && mapNameAndRecordType.containsKey(strAccRecordTypeName))
            {
                RecordType accRecordType = mapNameAndRecordType.get(strAccRecordTypeName);
                acc.RecordTypeId = accRecordType.Id;
            }
            
            else if(mapNameAndRecordType != null && mapNameAndRecordType.size()>0)
            {
                RecordType accRecordType = mapNameAndRecordType.get('其他');
                acc.RecordTypeId = accRecordType.Id;
            }
            
        }
    }
}