/*
*功能：当潜在客户转客户时根据客户上的“#客户记录类型”来改变拿单项目上的记录类型
*作者：Crazy
*时间：2014-1-23
*/
trigger Account_LeadConvertUpdateOppRecordTtype on Opportunity (before insert) 
{
    set<ID> oppSet = new set<ID>();
    for(Opportunity opp:trigger.new)
    {
        oppSet.add(opp.AccountId);
    }
    list<Account> accList = [select id,AccRecordType__c from Account where id in:oppSet];
    list<RecordType> oppRecordTypeList = [select Id,Name,SobjectType 
                                          from RecordType 
                                          where SobjectType = 'Opportunity'];                                     
    map<String,RecordType> mapOppNameAndRecordType = new map<String,RecordType>();
    //把客户的所有记录类型的Name和对应的记录类型放入Map----mapNameAndRecordType中                           
    if(oppRecordTypeList != null && oppRecordTypeList.size()>0)
    {
        for(RecordType oppRT : oppRecordTypeList)
        {
            if(!mapOppNameAndRecordType.containsKey(oppRT.Name))
            {
                mapOppNameAndRecordType.put(oppRT.Name,oppRT);
            }
        }    
    }   
    for(Opportunity opp:trigger.new)
    {
        for(Account acc:accList)
        {
            string strAccRecordTypeName = acc.AccRecordType__c;
            if(strAccRecordTypeName != null)
            {
                if(mapOppNameAndRecordType!=null&&mapOppNameAndRecordType.size()>0&&mapOppNameAndRecordType.containsKey(strAccRecordTypeName))
                {
                    opp.RecordTypeId= mapOppNameAndRecordType.get(strAccRecordTypeName).id;
                }
                else if(mapOppNameAndRecordType!=null&&mapOppNameAndRecordType.size()>0)
                {
                    opp.RecordTypeId = mapOppNameAndRecordType.get('其他').id;
                }
            }
        }
    }                             
}