/**
 * Author : Sunny
 * Des : 组织未解决邮件设置客户之后， 自动将记录复制到Account下的电子邮件
 **/
trigger AutoConnectAccount on Unresolved_Email__c (after update) {
	List<EmailInfo__c> list_emailInfo = new List<EmailInfo__c>();
	for(Unresolved_Email__c ue : trigger.new)
	{
		if(ue.Account__c != null && trigger.oldMap.get(ue.Id).Account__c == null)
		{
			EmailInfo__c e = new EmailInfo__c();
			e.Name = ue.Name;
			e.Email_CC_Addresses__c = ue.CC_Addresses__c;
			e.Email_From_Address__c = ue.From_Address__c;
			e.Email_To_Addresses__c = ue.To_Addresses__c;
			e.Email_Direction__c = ue.Direction__c;
			e.Email_In_Reply_To__c = ue.In_Reply_To__c;
			e.Account__c = ue.Account__c;
			e.Date__c = ue.CreatedDate.date() ;
			e.Email_Message_Id__c = ue.Message_Id__c;
			e.Email_TextBody__c = ue.Body__c;
			e.Email_Body__c = ue.Email_HtmlBody__c;
			list_emailInfo.add(e);
		}
	}
	if(list_emailInfo.size() > 0)
	{
		insert list_emailInfo;
	}
}