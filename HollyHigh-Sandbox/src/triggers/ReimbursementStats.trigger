/*
 * Author: Steven
 * Date: 2014-2-8
 * Description: 统计费用报销金额到利润信息
 */
trigger ReimbursementStats on CostReimbursement__c (after delete, after update) 
{
    
    for(CostReimbursement__c cr : trigger.old)
    {
        String year = cr.Year__c;
        String month = cr.reimbursementMonth__c;
        if(month == null)
        {
            continue;
        }
        
        CostReimbursement__c[] crs = new List<CostReimbursement__c>();
        crs = [Select AllCost__c 
               From CostReimbursement__c 
               Where Year__c =: year 
               And reimbursementMonth__c =: month 
               And ReimbursementState__c =: '审批通过'];
        
        Double amount = 0;
        if(crs.size() > 0)
        {
            for(CostReimbursement__c c : crs)
            {
                amount += c.AllCost__c;
            }
        }
        
        ProfitInfo__c[] pi = [Select OwnerId, ReceivablesMonthly__c, Year__c, Month__c, ReimbursementMonthly__c 
                              From ProfitInfo__c 
                              Where Year__c =: year 
                              And Month__c =: month limit 1];
        
        // 若没有该月份信息, 新建一条利润信息 insert
        if(pi.size() == 0)
        {
            ProfitInfo__c p = new ProfitInfo__c();
            p.Year__c = year;
            p.Month__c = month;
            p.ReimbursementMonthly__c = amount;
            p.ReceivablesMonthly__c = 0;
            User u = [Select Name, Id From User Where UserRole.Name = '财务经理' limit 1];
            p.OwnerId = u.Id;
            insert p;
        }
            
        // 若有该月份信息把金额赋值给该记录 update
        else
        {
            pi[0].ReimbursementMonthly__c = amount;
            update pi[0];
        }
    }
    if(trigger.new != null)
    {
        for(CostReimbursement__c cr : trigger.new)
        {
            // 获取年月
            String year = cr.Year__c;
            String month = cr.reimbursementMonth__c;
            if(month == null)
            {
                continue;
            }
            
            // 查找该月份所有费用报销
            CostReimbursement__c[] crs = new List<CostReimbursement__c>();
            crs = [Select AllCost__c 
                   From CostReimbursement__c 
                   Where Year__c =: year 
                   And reimbursementMonth__c =: month 
                   And ReimbursementState__c =: '审批通过'];
            
            // 统计该月份所有费用报销金额
            Double amount = 0;
            if(crs.size() > 0)
            {
                for(CostReimbursement__c c : crs)
                {
                    amount += c.AllCost__c;
                }
            }
                
            // 根据年月查找利润信息
            ProfitInfo__c[] pi = [Select OwnerId, ReceivablesMonthly__c, Year__c, Month__c, ReimbursementMonthly__c 
                                  From ProfitInfo__c 
                                  Where Year__c =: year 
                                  And Month__c =: month limit 1];
            
            // 若没有该月份信息, 新建一条利润信息 insert
            if(pi.size() == 0)
            {
                ProfitInfo__c p = new ProfitInfo__c();
                p.Year__c = year;
                p.Month__c = month;
                p.ReimbursementMonthly__c = amount;
                p.ReceivablesMonthly__c = 0;
                User u = [Select Name, Id From User Where UserRole.Name = '财务经理' limit 1];
                p.OwnerId = u.Id;
                insert p;
            }
                
            // 若有该月份信息把金额赋值给该记录 update
            else
            {
                pi[0].ReimbursementMonthly__c = amount;
                update pi[0];
            }
        }
    }
}