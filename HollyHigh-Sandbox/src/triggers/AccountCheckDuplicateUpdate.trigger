/*
Author:Crazy
Time:2014-1-26 update 2014-2-14 Steven
Function:当新建或修改客户信息时，若新客户的名称、电话、上市代码有一个和数据库中的一样，则该客户已经存在，创建失败
*/
trigger AccountCheckDuplicateUpdate on Account (before update) 
{
    //存放的客户名称
    list<String> List_AccName = new list<String>();
    
    //存放的客户电话
    map<String,Account> map_AccPhone = new map<String,Account>();
    
    //存放的客户的上市代码
    map<String,Account> map_AccCorporationCode = new map<String,Account>();
    list<Account> list_Acc = [select Name,Phone,CorporationCode__c from Account];
    for(Account acc : list_Acc)
    {
        List_AccName.add(acc.Name);
        map_AccPhone.put(acc.Phone,acc);
        map_AccCorporationCode.put(acc.CorporationCode__c,acc);
    }
    for(Account acc : trigger.new) 
    {
        Account acco = trigger.oldMap.get(acc.Id);
        if(acco != null)
        {
            System.debug('>>>>>>>>>' + acco.Name.contains(acc.Name));
            if(acco.Name != acc.Name)
            {
	            if(!(acco.Name.contains(acc.Name)))
	            {
	                if(acc.Name != null && acc.Name !='')
	                {
	                    String strAccName = acc.Name;
	                    for(String str : List_AccName)
	                    {
	                        if(str.contains(strAccName))
	                        {
	                            acc.AddError('该客户名称已经存在，请重新输入');
	                            continue;
	                        }
	                    }
	                }
	            }
	            else
	            {
	                // 包含修改前的字段值时, 和数据库比对剔除修改前的字段值
	                for(String str : List_AccName)
	                {
	                    if(str == acco.Name)
	                    {
	                        continue;
	                    }
	                    if(str.contains(acc.Name))
	                    {
	                        acc.AddError('该客户名称已经存在，请重新输入');
	                        continue;                       
	                    }
	                }
	            }
            }
            if(acco.Phone != acc.Phone)
            {
                if(acc.Phone !=null && acc.Phone !='' && map_AccPhone.containsKey(acc.Phone))
                {
                        acc.AddError('该客户电话号码已经存在，请重新输入'); 
                        continue;
                } 
            }
            
            if(acco.CorporationCode__c != acc.CorporationCode__c)
            {
                if(acc.CorporationCode__c != null && acc.CorporationCode__c != '' && map_AccCorporationCode.containsKey(acc.CorporationCode__c))
                {
                        acc.AddError('该客户上市代码已经存在，请重新输入');
                        continue;
                }
            }
            
        }
    }
}