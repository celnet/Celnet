/*
 * Author: Steven Ke
 * Date: 2014-2-17
 * Description: 添加做单项目团队成员后将该项目共享给这些成员赋予只读的权限, 同时发送邮件及任务提醒, 删除成员后删除共享的权限
 */
trigger ProjectMemberShare on ProjectMember__c (after delete, after insert, after update) 
{
    if(trigger.old != null)
    {
        for(ProjectMember__c pmc : trigger.old)
        {
            
            ProjectHollyhigh__c p1 = [Select Name,OwnerId,Account__c,Account__r.OwnerId,Opportunity__c From ProjectHollyhigh__c Where Id =: pmc.ProjectName__c];
            
            if(pmc.GroupMember__c != p1.OwnerId)
            {
                List<OpportunityTeamMember> otmList = [Select UserId, Id From OpportunityTeamMember Where OpportunityId =: p1.Opportunity__c];
                
                Boolean isOppTeamMember = false;
                
                if(otmList != null)
                {
                    for(OpportunityTeamMember otm : otmList)
                    {
                        if(pmc.GroupMember__c == otm.UserId)
                        {
                            isOppTeamMember = true;
                        }
                    }
                }
                
                if(isOppTeamMember == false)
                {
                    //Opportunity o = [Select Name, AccountId From Opportunity Where Id =: otm.OpportunityId];
                    List<EmailInfo__c> existEIList = [Select Id From EmailInfo__c Where Account__c =: p1.Account__c And OwnerId !=: pmc.GroupMember__c];
                    EmailInfo__Share[] eisList = [Select
                                                        Id, 
                                                        ParentId, 
                                                        AccessLevel, 
                                                        UserOrGroupId 
                                                    From EmailInfo__Share 
                                                    Where UserOrGroupId =: pmc.GroupMember__c
                                                    And ParentId IN: existEIList];
                    if(eisList.size() > 0)
                    delete eisList;
                    
                    
                    List<AccountShare> asList = [Select Id 
                                                    From AccountShare 
                                                    Where UserOrGroupId =: pmc.GroupMember__c 
                                                    And AccountId =: p1.Account__c
                                                    And Account.OwnerId !=: pmc.GroupMember__c];
                    if(asList.size() > 0)
                    delete asList;
                }
            
            
                ProjectHollyhigh__Share[] projectShare = [Select 
                                                                ParentId, 
                                                                UserOrGroupId, 
                                                                AccessLevel 
                                                            From 
                                                                ProjectHollyhigh__Share 
                                                            Where 
                                                                UserOrGroupId =: pmc.GroupMember__c
                                                            And Parent.OwnerId !=: pmc.GroupMember__c];
                if(projectShare.size() > 0)
                
                delete projectShare;
            }
        }
    }
    
    if(trigger.new != null)
    {
        List<Task> taskList = new List<Task>();
        Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
        String[] toAddresses = new List<String>();
        
        // 该用户已存在的AccountShare
        List<AccountShare> existAccShareList = new List<AccountShare>();
        
        // 准备插入的EmailInfo__Share
        List<EmailInfo__Share> eisList = new List<EmailInfo__Share>();
        
        // 该用户已存在的EmailInfo__Share
        List<EmailInfo__Share> existEISList = new List<EmailInfo__Share>();
        
        // 该项目的客户的所有EmailInfo__c
        List<EmailInfo__c> eiList = new List<EmailInfo__c>();
        
        // 要更新的EmailInfo__c
        List<EmailInfo__c> updateEIList = new List<EmailInfo__c>();
        
        for(ProjectMember__c pmc : trigger.new)
        {
            User u1 = [Select Email From User Where User.Id =: pmc.GroupMember__c];
            ProjectHollyhigh__c p1 = [Select Name,OwnerId,Account__c, Account__r.OwnerId,Opportunity__c From ProjectHollyhigh__c Where Id =: pmc.ProjectName__c];
            
            if(pmc.GroupMember__c != p1.OwnerId)
            {
                ProjectHollyhigh__Share projectShare = new ProjectHollyhigh__Share();
                projectShare.ParentId = pmc.ProjectName__c;
                projectShare.UserOrGroupId = pmc.GroupMember__c;
                projectShare.AccessLevel = 'Read';
                
                insert projectShare;
            }
            
            // 判断该成员是否在拿单项目中已经存在，若存在则不共享电子邮件和客户
            List<OpportunityTeamMember> otmList = [Select UserId, Id From OpportunityTeamMember Where OpportunityId =: p1.Opportunity__c];
            
            Boolean isOppTeamMember = false;
            
            if(otmList != null)
            {
                for(OpportunityTeamMember otm : otmList)
                {
                    if(pmc.GroupMember__c == otm.UserId)
                    {
                        isOppTeamMember = true;
                    }
                }
            }
            
            if(isOppTeamMember == false)
            {
                existEISList = [Select ParentId, Parent.OwnerId From EmailInfo__Share Where UserOrGroupId =: pmc.GroupMember__c And Parent.Account__c =: p1.Account__c];
                Set<Id> idSet = new Set<Id>();
                
                if(existEISList.size() > 0)
                {
                    for(EmailInfo__Share eis : existEISList)
                    {
                        idSet.add(eis.ParentId);
                    }
                }
                
                eiList = [Select Id,OwnerId From EmailInfo__c Where Account__c =: p1.Account__c And Id Not IN: idSet];
                
                for(EmailInfo__c ei : eiList)
                {
                    if(pmc.GroupMember__c != ei.OwnerId)
                    {
                        EmailInfo__c einfo = new EmailInfo__c();
                        einfo.Id = ei.Id;
                        einfo.isApexShared__c = true;
                        updateEIList.add(einfo);
                        
                        EmailInfo__Share e = new EmailInfo__Share();
                        e.ParentId = ei.Id;
                        e.AccessLevel = 'Read';
                        e.UserOrGroupId = pmc.GroupMember__c;
                        eisList.add(e);
                    }
                }
                
                existAccShareList = [Select Id,AccountId,Account.OwnerId From AccountShare Where UserOrGroupId =: pmc.GroupMember__c And AccountId =: p1.Account__c];
                
                if(existAccShareList.size() == 0 && pmc.GroupMember__c != p1.Account__r.OwnerId)
                {
                    AccountShare accShare = new AccountShare();
                    accShare.AccountId = p1.Account__c;
                    accShare.AccountAccessLevel = 'Read';
                    accShare.UserOrGroupId = pmc.GroupMember__c;
                    accShare.OpportunityAccessLevel = 'Read';
                    
                    insert accShare;
                }
            }
            
            toAddresses.add(u1.Email);
            String content = '现有做单项目"' + p1.Name + '"需要您的关注。';
            content += '以下是项目链接: ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + pmc.ProjectName__c;
            mail.setPlainTextBody(content);
            mail.setSubject('做单项目关注');          
                
            Task t1 = new Task();
            t1.OwnerId = pmc.GroupMember__c;
            t1.WhatId = p1.Account__c;
            t1.Description = '现有做单项目"' + p1.Name + '"需要您的关注。';
            t1.ActivityDate = Date.today();
            t1.ActivityDateTime__c = Datetime.now();
            t1.Subject = '请关注“' + p1.Name + '” 项目';
            t1.IsReminderSet = true;
            t1.ReminderDateTime = Datetime.newInstance(Date.today().year(), Date.today().month(), Date.today().day(), 8, 0, 0);
            
            taskList.add(t1);
        }
        
        mail.setToAddresses(toAddresses);
        Messaging.sendEmail(new Messaging.Singleemailmessage[]{mail});
        insert taskList;
        
        if(eisList.size() > 0)
        {
            insert eisList;
        }
        
        if(updateEIList.size() > 0)
        {
            update updateEIList;
        }
    } 
}