/*
作者：winter
时间：2014-8-25
功能：创建opportunity的时候，根据“华尔圈匹配项目IDs”来创建“买方/卖方推荐”关系
*/
trigger Opportunity_CreateResourceRecommends on Opportunity (after insert,after update) 
{
	List<string> hrqPairIdList = new List<string>();//存放“推介买方卖方”ID
	Map<string,Opportunity> recommendOppMap = new Map<string,Opportunity>();//存放“推介买方卖方”拿单项目
	//Set<string> noRepeatIdSet = new Set<string>();//将ID去重复
	Map<string,Opportunity> conRecommendOpp = new Map<string,Opportunity>();//存放创建或是更新的OPP
	
	List<ResourceRecommend__c> recommendList = new List<ResourceRecommend__c>();
	List<Opportunity> allOpp = [select Id,Name,HRQ_ID__c,AccountId from Opportunity];
	for(Opportunity opp : allOpp)
	{
		recommendOppMap.put(opp.HRQ_ID__c,opp);
	}
	
	if(trigger.isInsert)
	{
		System.debug('插入新数据@@@@@@@@@@@@@@');
		for(Opportunity opp : trigger.new)
		{
			Set<string> noRepeatIdSet = new Set<string>();//Id去重
			if(opp.HRQ_Pairids__c != null && opp.HRQ_Pairids__c.trim() != '')
			{
				hrqPairIdList = opp.HRQ_Pairids__c.split(',');
				
				for(string noRepeatId : hrqPairIdList)
				{
					noRepeatIdSet.add(noRepeatId);
				}	
			}
			for(string noRepeatId : noRepeatIdSet)
			{
				if(recommendOppMap.get(noRepeatId) != null)
				{
					ResourceRecommend__c recommend = new ResourceRecommend__c();
					recommend.ConnectionID__c = opp.HRQ_ID__c + '---' + noRepeatId;
					recommend.Opportunity__c = opp.Id;
					recommend.Account__c = opp.AccountId;
							
					recommend.Target_Account__c = recommendOppMap.get(noRepeatId).AccountId;
					recommendList.add(recommend);
				}
			}
		}
	}
	
	if(trigger.isUpdate)
	{
		System.debug('修改数据###############');
		for(Opportunity opp : trigger.new)
		{
			Set<string> noRepeatIdSet = new Set<string>();
			if(opp.HRQ_Pairids__c != null && opp.HRQ_Pairids__c.trim() != '')
			{
				hrqPairIdList = opp.HRQ_Pairids__c.split(',');
				
				for(string noRepeatId : hrqPairIdList)
				{
					noRepeatIdSet.add(noRepeatId);
				}	
			}
			for(string noRepeatId : noRepeatIdSet)
			{
				if(recommendOppMap.get(noRepeatId) != null)
				{
					ResourceRecommend__c recommend = new ResourceRecommend__c();
					recommend.ConnectionID__c = opp.HRQ_ID__c + '---' + noRepeatId;
					recommend.Opportunity__c = opp.Id;
					recommend.Account__c = opp.AccountId;
							
					recommend.Target_Account__c = recommendOppMap.get(noRepeatId).AccountId;
					recommendList.add(recommend);
				}
			}		
		}
	}
	
	/*
	for(string noRepeatId : noRepeatIdSet)
	{
		if(recommendOppMap.get(noRepeatId) != null)
		{
			ResourceRecommend__c recommend = new ResourceRecommend__c();
			recommend.ConnectionID__c = conRecommendOpp.get(noRepeatId).HRQ_ID__c + '---' + noRepeatId;
			recommend.Opportunity__c = conRecommendOpp.get(noRepeatId).Id;
			recommend.Account__c = conRecommendOpp.get(noRepeatId).AccountId;
			
			recommend.Target_Account__c = recommendOppMap.get(noRepeatId).AccountId;
			recommendList.add(recommend);
		}
	}
	*/
	
	upsert recommendList ConnectionID__c;
}