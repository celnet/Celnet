/*
* Author: Tommy Liu
* Created On: 2014-1-14
* Function:
* 统计客户相关的做单项目的数量以及最近一次的签约时间。在客户上能体现客户和我们的签约次数，以及最近的一次签约时间。
* 因此在客户下一旦产生了 做单项目，并且做单项目阶段为 签约成交，要在客户中成交次数上累加1并记录日期到上次签约日期
*/
trigger ProjectHollyhigh_Rollup on ProjectHollyhigh__c (after insert, after update) 
{ 
    set<String> projectStages = new set<String> {'签约成交', '相关审批', '收款', '结项'};//存储用于表示签约成功的状态
    set<ID> accIds = new set<ID>();//存储需要重新计算做单数量的客户ID
    Date executeDate = Date.today();
    set<ID> accIdsNeedUpdateLastSignDate = new Set<ID>();//存储需要更新上次签约时间的客户ID
    if(trigger.isInsert)
    {
        for(ProjectHollyhigh__c newProj : trigger.new)//适用于用户新建了签约成交的做单项目
        {
            if(projectStages.contains(newProj.ProjectState__c ))
            {
                if(newProj.Account__c != null)
                {
                    accIds.add(newProj.Account__c);
                    accIdsNeedUpdateLastSignDate.add(newProj.Account__c);
                }
            }
        }
    }
    else if (trigger.isUpdate)
    {
        for(ProjectHollyhigh__c newProj : trigger.new)//适用于用户将一个做单项目修改为签约成交（包括签约成交后期的状态）
        {
            ProjectHollyhigh__c oldProj = trigger.oldMap.get(newProj.Id);
            if(newProj.ProjectState__c != oldProj.ProjectState__c)
            {
                //表示变化前的阶段是签约成交之前的状态，变化后的阶段是签约成交或之后的状态
                if(!projectStages.contains(oldProj.ProjectState__c ) && projectStages.contains(newProj.ProjectState__c ))
                {
                    if(newProj.Account__c != null)
                    {
                        accIds.add(newProj.Account__c);
                        accIdsNeedUpdateLastSignDate.add(newProj.Account__c);
                    }
                }
                //考虑由签约状态修改为之前的状态的情况，需要重新统计签约数量
                else if(projectStages.contains(oldProj.ProjectState__c ) && !projectStages.contains(newProj.ProjectState__c ))
                {
                    if(newProj.Account__c != null)
                    {
                        accIds.add(newProj.Account__c);
                    }
                }
                
            }
        }
    }
    
    //开始计算客户的成交次数和最后成交时间
    List<Account> accs = [Select 
                            Id, 
                            dealsnum__c, 
                            Lastsignindate__c, 
                            (Select 
                                Id, 
                                ProjectState__c 
                            From Project_Account__r
                            Where ProjectState__c IN: projectStages)
                        From Account
                        Where Id IN:accIds
                        ];
    for(Account acc : accs)
    {
        if(acc.Project_Account__r != null)
        {
            acc.dealsnum__c = acc.Project_Account__r.size();
        }
        if(accIdsNeedUpdateLastSignDate.contains(acc.Id))
        {
            acc.Lastsignindate__c = executeDate;
        }
    }
    update accs;
}