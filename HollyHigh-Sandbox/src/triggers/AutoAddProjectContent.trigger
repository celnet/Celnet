/*
 *Author：Leo
 *Date：2014-03-26
 *Function:生成做单项目时，自动将客户下的 客户需求 带到项目基本内容下
 *Logic:查询所有客户的id和客户需求存储在map中。根据待插入的做单项目关联的客户id得到需要插入的项目基本内容信息，将得到的
 *		项目基本信息赋值到待插入的做单项目信息中
 */
trigger AutoAddProjectContent on ProjectHollyhigh__c (before insert) {
	Map<Id,string> accDescription = new Map<Id,string>();
	List<Account> list_Account = [Select Id, Description From Account];
	for(Account acc : list_Account)
	{
		accDescription.put(acc.Id,acc.Description);
	}
	for(ProjectHollyhigh__c ph : trigger.new)
	{
		if(ph.Account__c != null && ph.BasicContent__c==null)
		{
			//如果做单项目关联的客户不为空，并且做单项目未填写项目基本内容,将关联的客户的客户需求带到项目基本内容中
			ph.BasicContent__c = accDescription.get(ph.Account__c);
		}
	}
}