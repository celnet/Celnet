/*
 * Author: Steven
 * Date: 2014-2-17
 * Description: Check Duplicate ProjectMember__c
 */
trigger ProjectMemberShareDuplicateCheck on ProjectMember__c (before insert, before update) 
{
	for(ProjectMember__c pmc : trigger.new)
	{
		if(pmc.GroupMember__c == null)
		{
			pmc.addError('请添加小组成员');	
		}
		
		List<ProjectMember__c> pmList = [Select GroupMember__c From ProjectMember__c Where ProjectName__c =: pmc.ProjectName__c];
		
		ProjectHollyhigh__c ph = [Select OwnerId From ProjectHollyhigh__c Where Id =: pmc.ProjectName__c];
		User u1 = [Select Id, Name From User Where Id =: ph.OwnerId];
		
		/*
		if(pmc.GroupMember__c == u1.Id)
		{
			pmc.addError('该成员是项目所有人, 请重新选择');
		}
		*/
		
		if(trigger.old == null || trigger.oldMap.get(pmc.Id) == null) 
		{
			for(ProjectMember__c pm : pmList)
			{
				if(pmc.GroupMember__c == pm.GroupMember__c)
				{
					pmc.addError('该成员已添加, 请重新选择');
				}			
			}
		}
		else
		{
			for(ProjectMember__c pm : pmList)
			{
				if(pmc.GroupMember__c != trigger.oldMap.get(pmc.Id).GroupMember__c)
				{
					if(pmc.GroupMember__c == pm.GroupMember__c)
					{
						pmc.addError('该成员已添加, 请重新选择');
					}
				}		
			}
		}
	}
}