/*
 * Description: 插入或更新支出记录时,判断是否超出预算,超出则更新超出预算的两个字段
 * Author: Steven
 * Date: 2014-1-23
 */
trigger ExceedBudgetNotify on AdmiExpense__c (before insert, before update) 
{
    for(AdmiExpense__c a : trigger.new)
    {
        
        List<AdmiExpense__c> aeList = new List<AdmiExpense__c>();
        aeList = [Select 
                    Expenses__c, 
                    Administrative_expenses__c, 
                    Id,
                    CostType__c,
                    ExceedBudgetMoney__c
                  From 
                    AdmiExpense__c 
                  Where 
                    Administrative_expenses__r.Id =: a.Administrative_expenses__c 
                  And 
                    CostType__c =: a.CostType__c
                  Order by 
                    LastModifiedDate asc];
        
        List<AdmiExpenseBudgetDetail__c> aebdList = new List<AdmiExpenseBudgetDetail__c>();
        aebdList = [Select 
                        Forecast__c 
                    From 
                        AdmiExpenseBudgetDetail__c 
                    Where 
                        AdmiExpenseBudgetName__r.Id =: a.Administrative_expenses__c 
                    And 
                        Administrative_expenses_type__c =: a.CostType__c];
                        
        Double totalExpense = a.Expenses__c;
        for(AdmiExpense__c aec : aeList)
        {
            if(a.id != aec.Id)
            {
                totalExpense += aec.Expenses__c;
            }
        }
        
        Double totalBudget = 0;
        for(AdmiExpenseBudgetDetail__c aebd : aebdList)
        {
            totalBudget += aebd.Forecast__c;
        }
        
        if(totalExpense > totalBudget)
        {
            a.ExceedBudgetMoney__c = (totalExpense - totalBudget);
            a.ExceedBudgetId__c = true;
        } 
        else 
        {
            a.ExceedBudgetMoney__c = null;
            a.ExceedBudgetId__c = false;
        }
        
        totalExpense = 0;
        totalBudget = 0;
    }
}