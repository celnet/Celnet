import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.IOException;

import com.sforce.soap.enterprise.DeleteResult;
import com.sforce.soap.enterprise.DescribeGlobalResult;
import com.sforce.soap.enterprise.DescribeGlobalSObjectResult;
import com.sforce.soap.enterprise.DescribeSObjectResult;
import com.sforce.soap.enterprise.EnterpriseConnection;
import com.sforce.soap.enterprise.Error;
import com.sforce.soap.enterprise.Field;
import com.sforce.soap.enterprise.FieldType;
import com.sforce.soap.enterprise.GetUserInfoResult;
import com.sforce.soap.enterprise.InvalidateSessionsResult;
import com.sforce.soap.enterprise.LoginResult;
import com.sforce.soap.enterprise.PicklistEntry;
import com.sforce.soap.enterprise.QueryResult;
import com.sforce.soap.enterprise.SaveResult;
import com.sforce.soap.enterprise.sobject.Account;
import com.sforce.soap.enterprise.sobject.Contact;
import com.sforce.soap.enterprise.sobject.SObject;
import com.sforce.ws.ConnectorConfig;
import com.sforce.ws.ConnectionException;

public class Sample {
	 static EnterpriseConnection connection;
	 static String sessionId;
	static ConnectorConfig config;
	 public static void main(String[] args) throws ConnectionException
	 {
		 
		 loginSample();
	 }
	public static boolean loginSample() throws ConnectionException {
		   boolean success = false;
		   String username = "kevin.li@ilife3.com.testing";
		   String password = "1q2w3e4r~";
		   String authEndPoint = "https://test.salesforce.com/services/Soap/c/24.0/";
		   
		   try {
			      config = new ConnectorConfig();
			      config.setUsername(username);
			      config.setPassword(password);        
			      config.setAuthEndpoint(authEndPoint);
			      connection = new EnterpriseConnection(config);
			      System.out.print(connection.getUserInfo()); 
			   } catch (ConnectionException ce) {
			      ce.printStackTrace();
			   }finally
			   {
				   connection.logout();
				   System.out.println(connection.getSessionHeader());
				   //connection = new EnterpriseConnection(config);
				   System.out.print(connection.getUserInfo()); 

			   }
		   return success;
	}
}