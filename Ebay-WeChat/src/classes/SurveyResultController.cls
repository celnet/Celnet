public without sharing class SurveyResultController {
	public String surveyEndMessage{get;set;}
	
	public SurveyResultController(){
		String surveyId = ApexPages.currentPage().getParameters().get('surveyId');
		List<Survey__c> sc = [Select Id, End_Message__c from Survey__c Where Id =: surveyId];
		
		if(sc.isEmpty()){
			return;
		}
		surveyEndMessage = sc[0].End_Message__c;
	}
}