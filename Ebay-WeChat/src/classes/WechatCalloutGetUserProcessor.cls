global class WechatCalloutGetUserProcessor extends Welink.WechatCalloutProcessor{
	global override void DoCallout(Welink__Wechat_Callout_Task_Queue__c WechatTask)
	{
		Welink.WechatCalloutService wcs = new Welink.WechatCalloutService(WechatTask.Welink__Public_Account_Name__c);
		Welink.WechatEntity.User user = wcs.getUserInfo(WechatTask.Welink__Open_ID__c);
		Welink__Wechat_User__c sfUser = Welink.WechatFieldMatch.GenerateWechatUser(user);
		ObjectList.add(sfUser);
	}
	 
	global override void FinishData()
	{
		if(this.ObjectList != null && ObjectList.size() >0)
		{
			map<string,Welink__Wechat_User__c> weUserMap = new map<string,Welink__Wechat_User__c>();
			for(sobject sobj : ObjectList)
			{
				Welink__Wechat_User__c wu = (Welink__Wechat_User__c)sobj;
				weUserMap.put(wu.Welink__Open_Id__c,wu);
			}
			upsert weUserMap.values() Welink__Open_Id__c;
		}
	}
}