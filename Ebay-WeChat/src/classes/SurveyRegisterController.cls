public without sharing class SurveyRegisterController {
	public Survey_Taker__c st{get;set;}
	public Id sId{get;set;}
	public String openId{get;set;}
	public SurveyTakerWrapper stw{get;set;}
	
	public SurveyRegisterController(){
		String code = ApexPages.currentPage().getParameters().get('code');
		String state = ApexPages.currentPage().getParameters().get('state');
		
		Welink.WechatCalloutService wcs = new Welink.WechatCalloutService('gh_44177dbcb8b3');
		openId = wcs.GetOpenIdbyCode(code);
		
		st = new Survey_Taker__c();
		st.WeChat_OpenId__c = openId;
	}
	
	@RemoteAction
	public static String register(SurveyTakerWrapper stw){
		insert stw.survey_taker;
		return stw.survey_taker.Id;
	}
	
	public class SurveyTakerWrapper{
		public Survey_Taker__c survey_taker{get;set;}
	}
	
	public List<String> ageList{
		get{
			if(ageList == null){
				List<String> ages = new List<String>();
				Schema.DescribeFieldResult fieldResult = Survey_Taker__c.Age__c.getDescribe();
			    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
			        
			    for( Schema.PicklistEntry f : ple){
			      ages.add(f.getValue());
			    }       
			    return ages;
			} else {
				return ageList;
			}
		}
	}
	
	public List<String> genderList{
		get{
			if(genderList == null){
				List<String> ages = new List<String>();
				Schema.DescribeFieldResult fieldResult = Survey_Taker__c.Gender__c.getDescribe();
			    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
			        
			    for( Schema.PicklistEntry f : ple){
			      ages.add(f.getValue());
			    }       
			    return ages;
			} else {
				return genderList;
			}
		}
	}
	
	public List<String> cityList{
		get{
			if(cityList == null){
				List<String> ages = new List<String>();
				Schema.DescribeFieldResult fieldResult = Survey_Taker__c.City__c.getDescribe();
			    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
			        
			    for( Schema.PicklistEntry f : ple){
			      ages.add(f.getValue());
			    }       
			    return ages;
			} else {
				return cityList;
			}
		}
	}
}