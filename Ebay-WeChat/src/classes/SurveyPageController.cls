public without sharing class SurveyPageController {
	public List<SurveyQuestionWrapper> sqwList{get;set;}
	public List<String> qIdList{get;set;}
	public String stId{get;set;}
	public String surveyName{get;set;}
	public String surveyId{get;set;}
	
	public SurveyPageController(){
		String takerId = ApexPages.currentPage().getParameters().get('takerId');
		
		sqwList = new List<SurveyQuestionWrapper>();
		qIdList = new List<String>();
		stId = takerId == null?'a039000000QrnEVAAZ':takerId; // 若不存在则取一个随机的takerId
		
		List<Survey__c> surveys = [Select Id, Name From Survey__c order by CreatedDate desc limit 1];
		
		if(surveys.isEmpty()){
			return;
		} 
		
		surveyId = surveys[0].Id;
		surveyName = surveys[0].Name;
		List<Survey_Question__c> questions = [Select Id, Choices__c, Question__c, Survey__c, Required__c, Question_Type__c, OrderNumber__c 
												From Survey_Question__c Where Survey__c =: surveys[0].Id order by OrderNumber__c];
		
		for(Survey_Question__c sq : questions){
			SurveyQuestionWrapper sqw = new SurveyQuestionWrapper();
			sqw.question = sq.Question__c;
			sqw.question_type = sq.Question_Type__c;
			sqw.isRequired = sq.Required__c;
			sqw.orderNumber = Integer.valueOf(sq.OrderNumber__c == null?0:sq.OrderNumber__c);
			sqw.picklist = sq.Choices__c == null?new List<String>():sq.Choices__c.split('\r\n');
			sqw.questionId = sq.Id;
			qIdList.add(sq.Id);
			
			sqwList.add(sqw);
		}
		
		
	}
	
	public class SurveyQuestionWrapper{
		public String questionId{get;set;}
		public String question{get;set;}
		public String question_type{get;set;}
		public Integer orderNumber{get;set;}
		public boolean isRequired{get;set;}
		public List<String> picklist{get;set;}
		public SurveyQuestionWrapper(){}
	}
	
	@RemoteAction
	public static void submitResponse(String raw_response){
		// 获取SurveyTaker的Id
		String surveyTakerId = raw_response.substring(0,18);
		
		// 获取Response
		String response = raw_response.substring(18);
		Map<String, String> responseMap = retrieveResponseMap(response);
		
		List<Survey_Response__c> srList = new List<Survey_Response__c>();
		for(String qId : responseMap.keySet()){
			Survey_Response__c sr = new Survey_Response__c();
			sr.Survey_Question__c = qId;
			sr.Response__c = responseMap.get(qId);
			sr.Survey_Taker__c = surveyTakerId;
			srList.add(sr);
		}
		
		insert srList;
	}
	
	public static Map<String, String> retrieveResponseMap(String response){
		// 对每条回应 用 --- 分开
		List<String> response_list = response.split('---');
		Map<String, String> responseMap = new Map<String, String>();
		for(String res : response_list){
			String questionId = res.substring(0,18);
			String responseContent = res.substring(18);
			
			if(responseMap.get(questionId) == null){
				responseMap.put(questionId, responseContent);
			} else {
				String newResponseContent = responseMap.get(questionId) + '\r\n' + responseContent;
				responseMap.put(questionId, newResponseContent);
			}
		}
		return responseMap;
	}
}