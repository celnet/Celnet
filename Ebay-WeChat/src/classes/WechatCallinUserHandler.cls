/**
 *
 *
 * Description: 判断是否位关注事件，是的话获取用户OpenId，根据用户OpenId获取微信用户信息保存到WeChatUser对象
 **/
global class WechatCallinUserHandler extends Welink.WechatCallinMsgHandler{
	global override void Handle(Welink.WechatCallinMsgPipelineContext context){
		Welink.WechatEntity.InEventMsg inEvent = (Welink.WechatEntity.InEventMsg)Context.InMsg;
		
		String oauth_base_url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=';
		oauth_base_url += 'wx815519a118946ab0';
		oauth_base_url += '&redirect_uri=';
		oauth_base_url += 'https://ebay-wechat-developer-edition.ap1.force.com/SurveyRegister';
		oauth_base_url += '&response_type=code&scope=snsapi_base';
		oauth_base_url += '&state=' + context.PublicAccountName;
		oauth_base_url += '#wechat_redirect';
		
		string link_mk = '感谢关注Ebay官方微信，<a href="' + oauth_base_url + '">填写问卷</a>';
		
		if(inEvent.MsgType != Welink.WechatEntity.MESSAGE_TYPE_EVENT)
		{
			return ;
		}
		
		if(inEvent.Event == Welink.WechatEntity.EVENT_TYPE_SUBSCRIBE)
		{
			Welink__Wechat_Callout_Task_Queue__c userInfoQueue = new Welink__Wechat_Callout_Task_Queue__c();
			userInfoQueue.Welink__Public_Account_Name__c = context.PublicAccountName;
			userInfoQueue.Welink__Open_ID__c = inEvent.FromUserName;
			userInfoQueue.Welink__Type__c = Welink.WechatEntity.EVENT_TYPE_SUBSCRIBE;
			userInfoQueue.Welink__Processor_Name__c = 'WechatCalloutGetUserProcessor';
			userInfoQueue.Name = 'WechatCalloutGetUserProcessor';
			Welink.WechatCalloutQueueManager.EnQueue(userInfoQueue);
			Welink__Wechat_User__c sfUser = new Welink__Wechat_User__c();
			sfUser.Welink__Open_Id__c = inEvent.FromUserName;
			sfUser.Welink__Public_Account_Name__c = Context.PublicAccountName;
			upsert sfUser Welink__Open_Id__c;
			
			Welink.WechatEntity.OutTextMsg outMsg = Welink.WechatEntity.GenerateOutMsg(inEvent, link_mk);
			Context.OutMsg = outMsg;
		}
		
	}
}